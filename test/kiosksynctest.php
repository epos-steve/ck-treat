#!/usr/bin/php
<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

require_once('bootstrap.php');
require_once('parseargs.php');
$promo_id = 365;
$sync = FALSE;

if ($argc <= 1) {
	echo "Usage: {$argv[0]} (PROMOTION ID | PROMOTION NAME) --sync\n";
	exit(0);
}

$parsed_args = parseArgs($argv);

if (isset($argv[1])) {
        $promo_ident = $argv[1];
}
if (isset($argv[2])) {
        $sync = (bool)$argv[2];
}
$promo_id = (int)$promo_ident;
try {
	$promotion = Treat_Model_Promotion::get($promo_id, TRUE);
	/*/*if (is_int($promo_ident)) {
        $promotion = Treat_Model_Promotion::get($promo_ident, TRUE);
	} else {
		//$promotion = Treat_Model_Promotion::get("NAME LIKE '%$promo_ident%'");
		$promotion = Treat_Model_Promotion::get("name='$promo_ident'");
	}*/
} catch (Exception $e) {
        echo "Cannot find promotion $promo_ident: $e\n";
        exit(1);
}
if ($promotion->businessid != NULL) {
        echo "Error: Not a global promotion\n";
        exit(1);
}
$promo_id = $promotion->id;

$ritems = $promotion->getItemsRelativeIds();
$gitems = $promotion->getItemsGlobalIds();
$businesses = $promotion->getAppliesTo();

echo "Promotion #$promo_id\nName\t\t'{$promotion->name}'\ntype\t\t{$promotion->promo_trigger->target_id}\n";
echo "Relative items:\t" ;

for ($i = 0; $i < count($ritems); $i++) {
        if ($i > 0)
                echo ', ';
        echo $ritems[$i]['id']
			. '->'
			. $ritems[$i]['master_id']
		;
}
        //print_r( $ritems, TRUE) .
echo PHP_EOL;
if ($promotion->hasGroups()) {
	echo "Groups:\t\t" . implode(',', $promotion->getGroups()) . "\n";
} else  {
	echo "does not have groups\n";
}

echo "Global items:\t" . implode(',', $gitems) . PHP_EOL . PHP_EOL;

if ($sync) {
        echo "Syncing kiosks... ";
        $result = $promotion->kioskSyncAll();
        echo "done\n";
}

$syncs = Array();
$client_programids = Array();
$kiosks = Array();
foreach ($businesses as $business_id) {
        $business_kiosks = Treat_Model_ClientPrograms::getKiosksByBusiness($business_id);
	foreach ($business_kiosks as $kiosk) {
		$kiosks[] = $kiosk;
                $query = "SELECT item_id, item_type FROM kiosk_sync WHERE client_programid='$kiosk->client_programid'";
                $result = Treat_Model_Promotion::db()->query($query);
                $result->setFetchMode(PDO::FETCH_NAMED);
				$kdata = $result->fetchAll();
				$client_programids[] = $kiosk->client_programid;
				//$kdata['promotions'] = Treat_Model_KioskSync::getPromotions($kiosk, FALSE);
                $syncs[$business_id] = $kdata;

        }
}

echo "###### KIOSK SYNC ENTRIES #######\n";
#print_r($syncs);
foreach ($syncs as $srecord) {
        echo "## Business #307:\n";
        foreach ($srecord as $sitem) {
                if ($sitem['item_type'] == Treat_Model_Promotion::TRIGGER_ITEM) {
                        echo "\tItem #" . $sitem['item_id']
								. "\n"
						;
                }
                else if ($sitem['item_type'] == Treat_Model_Promotion::TRIGGER_COMBO_GROUP || Treat_Model_Promotion::TRIGGER_SINGLE_COMBO_GROUP) {
                        echo "\tGroup #" . $sitem['item_id'] . "\n";
                }
                else  {
                        echo "\t(type " . $sitem['item_type'] . ") #" . $sitem['item_id'] . "\n";
                }
				//echo "\t\tPromotions:" . print_r($sitem['promotions'], TRUE);

        }
}
echo PHP_EOL;

foreach ($kiosks as $kiosk) {
	echo "KioskSync::getPromotions(client_programid=" . $kiosk->client_programid . ")\n";
	$get_promotions = Treat_Model_KioskSync::getPromotions($kiosk, FALSE);
	print_r($get_promotions);
	foreach ($get_promotions as $kiosk_promotion) {
		echo "\ttrigger_data_array:\t" . implode(',', $kiosk_promotion['trigger_data_array']) . "\n";
	}
}

//echo "client_programids:\t" . implode(',', $client_programids) . "\n";