<?php

/*
if ( defined( 'DOC_ROOT' ) ) {
	require_once( DOC_ROOT . '/bootstrap.php' );
}
else if ( defined('SITECH_APP_PATH') ) {
        require_once( realpath(SITECH_APP_PATH . 'bootstrap.php') );
}
else {
	require_once( realpath( dirname( __FILE__ ) . '/../../application/bootstrap.php' ) );
}
*/

if( !function_exists( 'isLeapYear' ) ) {
	# This function name doesn't make sense considering what it does - would seem
	# to return boolean, but is expected to return the # of days in Feb O.o
	function isLeapYear($year) {
		# In the Gregorian calendar there is a leap year every year divisible by four 
		# except for years which are both divisible by 100 and not divisible by 400. 
		$date = Treat_Date::getValidDateStamp( $year . '-02-01' );
		if ( false === $date ) {
			$date = Treat_Date::getValidDateStamp( $year );
			if ( false === $date ) {
				$date = time();
			}
		}
		
		if ( date( 'L', $date ) ) {
			return 29;
		}
		else {
			return 28;
		}
	}
}

if( !function_exists( 'arrayToJson' ) ) {
	function arrayToJson( $array )
	{
		$returnJson = '';
		$returnJsonArray = array();
		
		foreach( $array as $key => $value )
		{
			$key = addslashes( $key );
			$svalue = addslashes( $value );
			if( is_array( $value ) )
			{
				$returnJsonArray[] = "\"$key\" : ".arrayToJson( $value );
			} else {
				$returnJsonArray[] = "\"$key\" : \"$svalue\"";
			}
		}
		
		$returnJson = implode( ",\n", $returnJsonArray );
		return "{\n".$returnJson."\n}";
	}
}

if(!function_exists('check_mobile')) {
	function checkmobile(){
		/*
		 echo $_SERVER["HTTP_USER_AGENT"];
		 echo "<br>";
		 echo $_SERVER["HTTP_X_WAP_PROFILE"];
		 echo "<br>";
		 echo $_SERVER["HTTP_ACCEPT"];
		 * */

		if(isset($_SERVER["HTTP_X_WAP_PROFILE"])) return true;

		if(preg_match("/wap\.|\.wap/i",$_SERVER["HTTP_ACCEPT"])) return true;

		if(isset($_SERVER["HTTP_USER_AGENT"])){

			if(preg_match("/Creative\ AutoUpdate/i",$_SERVER["HTTP_USER_AGENT"])) return false;

			//$uamatches = array("midp", "j2me", "avantg", "docomo", "novarra", "palmos", "palmsource", "240x320", "opwv", "chtml", "pda", "windows\ ce", "mmp\/", "blackberry", "mib\/", "symbian", "wireless", "nokia", "hand", "mobi", "phone", "cdm", "up\.b", "audio", "SIE\-", "SEC\-", "samsung", "HTC", "mot\-", "mitsu", "sagem", "sony", "alcatel", "lg", "erics", "vx", "NEC", "philips", "mmm", "xx", "panasonic", "sharp", "wap", "sch", "rover", "pocket", "benq", "java", "pt", "pg", "vox", "amoi", "bird", "compal", "kg", "voda", "sany", "kdd", "dbt", "sendo", "sgh", "gradi", "jb", "\d\d\di", "moto");
			//$uamatches = array("blackberry", "mobi", "moto", "windows\ ce", "pda", "phone", "samsung");
			$uamatches = array("blackberry", "windows\ ce", "samsung");

			foreach($uamatches as $uastring){
				if(preg_match("/".$uastring."/i",$_SERVER["HTTP_USER_AGENT"])) return true;
			}

		}
		return false;
	}
}

if(!function_exists('money')) {
	function money($diff){
		if ( $diff < 0 ) {
			$diff = ceil( $diff * 100 ) / 100;
		}
		else {
			$diff = floor( $diff * 100 ) / 100;
		}
		return sprintf( '%01.2f', $diff );
	}
}

if (!function_exists('nextday')){
	function nextday($nextd, $day_format = '')
	{
		if($day_format==""){$day_format="Y-m-d";}
		$nextd = Treat_Date::getNextDay( $nextd );
		return date( $day_format, $nextd );
	}


}

if(!function_exists('prevday')) {

	function prevday($prevd,$day_format=''){ //Function returns previous day of passed date and formats accordingly.
		if($day_format==""){$day_format="Y-m-d";}
		$prevd = Treat_Date::getPreviousDay( $prevd );
		return date( $day_format, $prevd );
	}
}

if(!function_exists('dayofweek')){
	function dayofweek($date1)
	{
		$date1 = Treat_Date::getValidDateStamp( $date1 );
		return date( 'l', $date1 );
	}
}

if(!function_exists('futureday')){
	function futureday($num) {
		return date( 'Y-m-d', strtotime( sprintf( '+%d Days', $num ) ) );
	}
}
if(!function_exists('pastday')){
	function pastday($num) {
		return date( 'Y-m-d', strtotime( sprintf( '+%d Days', $num * -1 ) ) );
	}
}

if(!function_exists('creategraph')){
	function creategraph($sales,$budget,$color,$showtitle){

		if($budget!=0){
			$diff=round($sales/($budget*2)*200,0);
			if($diff>200){$diff=200;}
		}
		elseif($sales>0){$diff=200;}
		else{$diff=0;}

		if($sales>=$budget&&$sales!=0){$graphcolor=$color;}
		else{$graphcolor="#CCCCCC";}

		$showdiff=number_format($sales-$budget,2);
		if($showdiff>0){$showdiff="+$showdiff";}

		//if($showtitle==1){$showbudg=number_format($budget,2);$showavg="Daily Average: $showbudg";}
		//else{$showbudg=number_format($budget,2);$showavg="Budget: $showbudg";}

		if($showtitle==1){$showtitle=number_format($sales,2);}
		else{$showtitle=$showdiff;}

		$graph="<table cellspacing=0 cellpadding=0 width=200 title='$showtitle'>";
		$graph="$graph<tr height=2 bgcolor=#EEEEEE><td colspan=200><img src=clear.gif height=1 width=1></td></tr>";
		$graph="$graph<tr height=2 bgcolor=#EEEEEE><td colspan=99><img src=clear.gif height=1 width=1></td><td colspan=2 bgcolor=$graphcolor title='$showavg'><img src=clear.gif height=1 width=1></td><img src=clear.gif height=1 width=1><td colspan=99><img src=clear.gif height=1 width=1></td></tr>";
		if($diff<1){
			$graph="$graph<tr height=6><td colspan=99 bgcolor=#EEEEEE><img src=clear.gif height=1 width=1></td><td colspan=2 bgcolor=$graphcolor title='$showavg'><img src=clear.gif height=1 width=1></td><td colspan=99 bgcolor=#EEEEEE><img src=clear.gif height=1 width=1></td></tr>";
		}
		elseif($diff<=98){
			$diff2=99-$diff;
			$graph="$graph<tr height=6><td colspan=$diff bgcolor=#444455><img src=clear.gif height=1 width=1></td><td colspan=$diff2 bgcolor=#EEEEEE><img src=clear.gif height=1 width=1></td><td colspan=2 bgcolor=$graphcolor title='$showavg'><img src=clear.gif height=1 width=1></td><td colspan=99 bgcolor=#EEEEEE><img src=clear.gif height=1 width=1></td></tr>";
		}
		elseif($diff>98&&$diff<102){
			$graph="$graph<tr height=6><td colspan=99 bgcolor=#444455><img src=clear.gif height=1 width=1></td><td colspan=2 bgcolor=$graphcolor title='$showavg'><img src=clear.gif height=1 width=1></td><td colspan=99 bgcolor=#EEEEEE><img src=clear.gif height=1 width=1></td></tr>";
		}
		elseif($diff>=102){
			$diff2=$diff-101;
			if($diff2>99){$diff2=99;}
			$diff3=99-$diff2;
			$graph="$graph<tr height=6><td colspan=99 bgcolor=#444455><img src=clear.gif height=1 width=1></td><td colspan=2 bgcolor=$graphcolor title='$showavg'><img src=clear.gif height=1 width=1></td><td colspan=$diff2 bgcolor=#444455><img src=clear.gif height=1 width=1></td><td colspan=$diff3 bgcolor=#EEEEEE><img src=clear.gif height=1 width=1></td></tr>";
		}
		$graph="$graph<tr height=2 bgcolor=#EEEEEE><td colspan=99><img src=clear.gif height=1 width=1></td><td colspan=2 bgcolor=$graphcolor title='$showavg'><img src=clear.gif height=1 width=1></td><td colspan=99><img src=clear.gif height=1 width=1></td></tr>";
		$graph="$graph<tr height=2 bgcolor=#EEEEEE><td colspan=200><img src=clear.gif height=1 width=1></td></tr></table>";

		return $graph;
	}
}
if(!function_exists('hgraph')){
	function hgraph($number,$total,$color,$width,$diff,$bgcolor,$showpercent,$divid){
		if($divid!=""){$color="white";}

		if($diff!=0){
			if($diff>=0){$diff=number_format($diff,2); $showdiff="<img src=budget_good.gif height=16 width=13 alt=$diff>";}
			else{$diff=number_format($diff,2); $showdiff="<img src=budget_bad.gif height=16 width=15 alt=$diff>";}
		}

		$showheight=round($number/($total/200),0);
		if($showheight>200){$showheight=200;}
		$showtitle=number_format($number,2);
		if($number<=0){$color="";}
		if($showpercent>0){
			$newheight=round($showheight/$showpercent,1);

			$graph="<td valign=bottom width=$width title='$showtitle' bgcolor=$bgcolor>$showdiff";

			for($counter=100;$counter>=(100/$showpercent);$counter=$counter-(100/$showpercent)){
				$graph.="<div style=\"height:$newheight px; width:100%; background-color:$color;\"><div style=\"height:1 px; width:100%; background-color:black;\"><img src=clear.gif></div><font size=1 face=arial><b>$counter</b></font></div>";
			}

			$graph.="</td>";
		}
		elseif($showheight>0){$graph="<td valign=bottom width=$width title='$showtitle' bgcolor=$bgcolor>$showdiff<div id='$divid' style=\"height:$showheight px; width:100%; background-color:$color;\">&nbsp;</div></td>";}
		else{$graph="<td valign=bottom width=$width title='$showtitle' bgcolor=$bgcolor>$showdiff</td>";}

		return $graph;
	}
}

if (!defined('TREAT_BASEURL')) define('TREAT_BASEURL', 'http://treatamerica.essentialpos.com/ta/');
if (!defined('TREAT_DEV')) define('TREAT_DEV', false); // Change to true for a development server

if (!function_exists('google_page_track')) {
	function google_page_track(){
		if (isset($_COOKIE['kiosk_mode'])) return;
?>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-10305664-1");
pageTracker._trackPageview();
} catch(err) {}</script>
<?php
	}
}

if( !function_exists( 'rndm_color_code' ) ) {
	function rndm_color_code($b_safe = TRUE) {
	    //make sure the parameter is boolean
	    if(!is_bool($b_safe)) {return FALSE;}
	    
	    //if a browser safe color is requested then set the array up
	    //so that only a browser safe color can be returned
	    if($b_saafe) {
	        $ary_codes = array('00','33','66','99','CC','FF');
	        $max = 5; //the highest array offest
	    //if a browser safe color is not requested then set the array
	    //up so that any color can be returned.
	    } else {
	        $ary_codes = array();
	        for($i=0;$i<16;$i++) {
	            $t_1 = dechex($i);
	            for($j=0;$j<16;$j++) {
	                $t_2 = dechex($j);
	                $ary_codes[] = "$t_1$t_2";
	            } //end for j
	        } //end for i
	        $max = 256; //the highest array offset
	    } //end if
	    
	    $retVal = '';
	    
	    //generate a random color code
	    for($i=0;$i<3;$i++) {
	        $offset = rand(0,$max);
	        $retVal .= $ary_codes[$offset];
	    } //end for i
	    
	    return $retVal;
	} //end rndm_color_code
}
