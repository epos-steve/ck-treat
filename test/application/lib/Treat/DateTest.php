<?php
define('DOC_ROOT', realpath(__DIR__.'/../../../../htdocs'));
require_once __DIR__.'/../../../../lib/Treat/Date.php';
require_once 'PHPUnit/Framework/TestCase.php';

class DateTest extends PHPUnit_Framework_TestCase
{
	public function test_getValidTimeStamp_validMysqlFormattedDate()
	{
		$data = '2012-01-01';
		$expected = strtotime($data);
		$actual = Treat_Date::getValidTimeStamp($data);
		$this->assertEquals($expected, $actual);
	}

	public function test_getValidTimeStamp_validUsFormatedDate()
	{
		$data = '08/01/2012';
		$expected = strtotime('2012-08-01');
		$actual = Treat_Date::getValidTimeStamp($data);
		$this->assertEquals($expected, $actual);
	}

	public function test_getValidTimeStamp_invalidDateFormattedEuro()
	{
		$data = '30/08/2012';
		$actual = Treat_Date::getValidTimeStamp($data);
		$this->assertFalse($actual);
	}

	public function test_getValidTimeStamp_invalidDateString()
	{
		$data = 'test';
		$actual = Treat_Date::getValidTimeStamp($data);
		$this->assertFalse($actual);
	}

	public function test_getValidTimeStamp_invalidDateNumber()
	{
		$data = -1;
		$actual = Treat_Date::getValidTimeStamp($data);
		$this->assertFalse($actual);
	}

	public function test_getValidTimeStamp_NumericTimestampEpoch()
	{
		$expected = 0;
		$actual = Treat_Date::getValidTimeStamp($data);
		$this->assertEquals($expected, $actual);
	}

	public function test_getValidTimeStamp_NumericTimestampJuly4th2012()
	{
		$expected = strtotime('2012-07-04');
		$actual = Treat_Date::getValidTimeStamp($expected);
		$this->assertEquals($expected, $actual);
	}

	public function test_getNextDay_ValidUsDate()
	{
		$data = '08/02/2012';
		$expected = '2012-08-03';
		$actual = Treat_Date::getNextDay($data);
		$this->assertEquals($expected, $actual);
	}

	public function test_getNextDay_ValidMySqlDate()
	{
		$data = '2012-08-02';
		$expected = '2012-08-03';
		$actual = Treat_Date::getNextDay($data);
		$this->assertEquals($expected, $actual);
	}
}