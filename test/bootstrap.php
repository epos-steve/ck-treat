<?php

if( !defined( 'LEGACY' ) ) {
    define( 'LEGACY', false );
}

// ** Start of Code from typical index.php **
// this code should be removed once Treat is fully switched over to EEF/SiTech
if ( !defined( 'SITECH_APP_PATH' ) ) {
    define( 'SITECH_APP_PATH', realpath( dirname(__FILE__) . '/./application' ) );
}
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'testing'));
if ( !defined( 'EEF_APP_PATH' ) ) {
    define( 'EEF_APP_PATH', SITECH_APP_PATH );
}
/*
define('SITECH_LIB_PATH', dirname( dirname( __FILE__ ) ) . '/../lib');
define('SITECH_VENDOR_PATH', dirname( dirname( __FILE__ ) ) . '/../vendors');
define('SITECH_BASEPATH', dirname( dirname( __FILE__ ) ) );
define('TESTS_PATH',			realpath(dirname(__FILE__)));
*/
define('SITECH_BASEPATH',       realpath( dirname( __FILE__ )  . '/../') );
define('SITECH_LIB_PATH',		realpath(dirname(__FILE__) . '/../lib'));
define('TESTS_PATH',			realpath(dirname(__FILE__)));
define('SITECH_VENDOR_PATH',	realpath(dirname(__FILE__) . '/../vendors'));
//define('PHPUNIT_PATH',			'/usr/share/php/PHPUnit');
//define('DATASET_PATH',			realpath(__DIR__ . '/dataset'));
define('zSITECH_LIB_PATH', dirname( dirname( __FILE__ ) ) . '/lib');
define('zSITECH_VENDOR_PATH', dirname( dirname( __FILE__ ) ) . '/vendors');

//required or set by the original Treat bootstrap.

if ( !defined( 'DOC_ROOT' ) ) {
    define( 'DOC_ROOT', realpath( dirname( SITECH_APP_PATH ) . '/htdocs' ) );
}
set_include_path(implode(PATH_SEPARATOR, array(
    SITECH_APP_PATH,
    realpath(SITECH_APP_PATH . '/lib/'),
    SITECH_LIB_PATH,
    realpath(SITECH_VENDOR_PATH . '/SiTech/lib/'),
    realpath(SITECH_VENDOR_PATH . '/EE/library/'),
    realpath(DOC_ROOT . '/ta/'),
    TESTS_PATH,
    get_include_path(),
)));


// Set up our Load Handler [For auto_load]
require_once( 'EE/Loader.php' );
\EE\Loader::registerAutoload();
// ** End of Code from typical index.php **

// Start Config && DB
Treat_Config::singleton();
Treat_DB::singleton();
Treat_DB::singleton('manage');

// Set up Path Prefix
if ( !defined( 'EEF_PATH_PREFIX' ) ) {
    $prefix = Treat_Config::singleton()->getValue( 'application', 'path_prefix' );
    if ( is_null( $prefix ) ) {
        $prefix = '';
    }
    define( 'EEF_PATH_PREFIX', $prefix );
}

if ( !defined( 'IN_PRODUCTION' ) ) {
    define( 'IN_PRODUCTION', Treat_Config::singleton()->getValue( 'application', 'in_production' ) );
}
// if using DEBUG_STOP_REDIRECT you should check for !IN_PRODUCTION first
if ( !defined( 'DEBUG_STOP_REDIRECT' ) ) {
    define( 'DEBUG_STOP_REDIRECT', Treat_Config::singleton()->getValue( 'application', 'debug_stop_redirect' ) );
    if ( !defined( 'DEBUG_OUTPUT' ) ) {
        define( 'DEBUG_OUTPUT', !IN_PRODUCTION );
    }
}
if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
    // TODO check to see if it would be better to put this in Treat_DB instead
    # setting PDO::ATTR_ERRMODE to PDO::ERRMODE_EXCEPTION so that if there is an
    # error with the SQL query PDO will actually throw an exception instead of
    # being silent & allowing the page to continue causing lots of time being
    # wasted trying to hunt down what the problem is.
    Treat_DB::singleton()->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
}

if( !defined( 'UPLOAD_PATH' ) ) {
    define( 'UPLOAD_PATH', '/uploads' );
}

// create directory macros
$directories = array(
    'BUDGET_UPLOAD' => '/20budget10',
    'COMKPI' => '/comkpi',
    'CUSTOMER_FILES' => '/customerFiles',
    'DASHBOARD_FILES' => '/dashboardfiles',
    'EXPORTS' => '/exports',
    'LABOR_FILES' => '/labor',
    'NUTRITION_IMPORT' => '/nutrition-import',
    'PAYROLL_FILES' => '/payroll',
    'REPORT_FILES' => '/reports',
    'SMARTMENU_FILES' => '/smartmenu',
    'UPLOADS' => '/control-sheet',
    'UTILITY' => '/utility',
    'VENDOR_IMPORTS' => '/import',
);

// HTTP_* -- http access path (relative to document root)
// DIR_* -- php access path (absolute)
foreach( $directories as $name => $path ) {
    if( !defined( 'HTTP_'.$name ) ) {
        define( 'HTTP_'.$name, UPLOAD_PATH . $path );
    }
    if( !defined( 'DIR_'.$name ) ) {
        define( 'DIR_'.$name, DOC_ROOT . UPLOAD_PATH . $path );
    }
}

// clients upload data to this location via scp/ftp:
//  /home/espos/www/CAISO/OutlookCalenderExcel.csv
if( !defined( 'DIR_FTP_UPLOADS' ) ) {
    if ( file_exists( '/srv/home/espos/www' ) ) {
        define( 'DIR_FTP_UPLOADS', '/srv/home/espos/www' );
    }
    else {
        define( 'DIR_FTP_UPLOADS', '/home/espos/www' );
    }
}

if( !defined( 'DIR_TMP_FILES' ) ) {
    define( 'DIR_TMP_FILES', DOC_ROOT . '/../tmp' );
}

// older names, kept for backward compatibility
if ( !defined( 'UPLOADS_DIRECTORY' ) ) {
    define( 'UPLOADS_DIRECTORY', DOC_ROOT . UPLOAD_PATH );
}
if ( !defined( 'ROUTE_EXPORTS_DIRECTORY' ) ) {
    define( 'ROUTE_EXPORTS_DIRECTORY', DOC_ROOT . UPLOAD_PATH . '/exports' );
}

// Tell Models what PDO to use
Treat_Model_Abstract::db( Treat_DB::singleton() );	// models using businesstracks database
Treat_Model_Manage_Abstract::db(Treat_DB::singleton('manage'));	// for models using manage database
Treat_Loader::loadHelpers();

# can't start a session when in cli mode
if ( 'cli' != php_sapi_name() ) {
    // Start Session
#	if ( 'memcache' == ini_get( 'session.save_handler' ) ) {
#		SiTech_Session::registerHandler( new SiTech_Session_Handler_Memcache() );
#	}
#	else {
    SiTech_Session::registerHandler( new SiTech_Session_Handler_DB( Treat_Model_Abstract::db() ) );
#	}
    Treat_Session::start();
    #Treat_Session::singleton();
}

function require_ta_bootstrap() {
    // required or set by the original Treat bootstrap.
    if ( !defined( 'TREAT_DEV' ) ) {
        define( 'TREAT_DEV', !IN_PRODUCTION ); // Change to true for a development server
    }
    $var = 'bootstrap.php';
    require_once( DOC_ROOT . DIRECTORY_SEPARATOR . $var );
}

// need the google_page_track() function out of this functions file for the default template
//require_once( dirname( SITECH_APP_PATH ) . '/lib/inc/functions.php' );
require_once( realpath(SITECH_APP_PATH . '/lib/inc/functions.php') );

# addition of routes via SiTech_Controller::addRoute() is done in index2.php
# instead of here as would normally be the case because there are some pages
# that pull in this bootstrap that don't go through the controller.

