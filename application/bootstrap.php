<?php

if( !defined( 'LEGACY' ) ) {
	define( 'LEGACY', false );
}

// ** Start of Code from typical index.php **
// this code should be removed once Treat is fully switched over to EEF/SiTech
if ( !defined( 'SITECH_APP_PATH' ) ) {
	define( 'SITECH_APP_PATH', realpath( dirname(__FILE__) . '/../application' ) );
}
if ( !defined( 'EEF_APP_PATH' ) ) {
	define( 'EEF_APP_PATH', SITECH_APP_PATH );
}
define('SITECH_LIB_PATH', dirname( dirname( __FILE__ ) ) . '/lib');
define('SITECH_VENDOR_PATH', dirname( dirname( __FILE__ ) ) . '/vendors');
define('SITECH_BASEPATH', dirname( dirname( __FILE__ ) ) );
// required or set by the original Treat bootstrap.
if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname( SITECH_APP_PATH ) . '/htdocs' ) );
}


// Tell PHP to look into our library for includes as well
/*
*/
set_include_path( implode( PATH_SEPARATOR, array(
	SITECH_VENDOR_PATH . '/SiTech/lib/',
	SITECH_VENDOR_PATH . '/EE/library/',
	SITECH_VENDOR_PATH . '/GearmanManager/',
	SITECH_VENDOR_PATH . '/treat-lib/',
	SITECH_VENDOR_PATH . '/treat-lib/src',
	SITECH_VENDOR_PATH . '/lib-treat/src/',
	SITECH_VENDOR_PATH . '/lib-treat/lib/',
	SITECH_VENDOR_PATH . '/PHPExcel/Classes/',
	SITECH_LIB_PATH,
	DOC_ROOT . '/ta/',
	get_include_path(),
) ) );
/*
*/

// Set up our Load Handler [For auto_load]
require_once __DIR__ . '/../vendor/autoload.php';
require_once( 'EE/Loader.php' );
\EE\Loader::registerAutoload();
// ** End of Code from typical index.php **

// Start Config && DB
\EE\Config::setDefaultConfigDirectory( 'confs' );
\EE\Config::setDefaultConfigIni( 'default.ini' );
\EE\Config::singleton();
	\EE\DB::setAlwaysUsePdoException( true );
	\EE\DB::singleton();
	\EE\DB::singleton('manage');
#	\EE\DB::singleton( 'treat' );
\EE\Model\Base::setDbConfigSectionTreat( 'treat' );
\EE\Controller\Base::setHasLegacy( true );

// Set up Path Prefix
if ( !defined( 'EEF_PATH_PREFIX' ) ) {
	$prefix = \EE\Config::singleton()->getValue( 'application', 'path_prefix' );
	if ( is_null( $prefix ) ) {
		$prefix = '';
	}
	define( 'EEF_PATH_PREFIX', $prefix );
}

if ( !defined( 'IN_PRODUCTION' ) ) {
	define( 'IN_PRODUCTION', \EE\Config::singleton()->getValue( 'application', 'in_production' ) );
}
// if using DEBUG_STOP_REDIRECT you should check for !IN_PRODUCTION first
if ( !defined( 'DEBUG_STOP_REDIRECT' ) ) {
	define( 'DEBUG_STOP_REDIRECT', \EE\Config::singleton()->getValue( 'application', 'debug_stop_redirect' ) );
	if ( !defined( 'DEBUG_OUTPUT' ) ) {
		define( 'DEBUG_OUTPUT', !IN_PRODUCTION );
	}
}
if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
	// TODO check to see if it would be better to put this in Treat_DB instead
	# setting PDO::ATTR_ERRMODE to PDO::ERRMODE_EXCEPTION so that if there is an
	# error with the SQL query PDO will actually throw an exception instead of
	# being silent & allowing the page to continue causing lots of time being
	# wasted trying to hunt down what the problem is.
	\EE\DB::singleton()->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
}

if( !defined( 'UPLOAD_PATH' ) ) {
	define( 'UPLOAD_PATH', '/uploads' );
}

// create directory macros
$directories = array(
	'BUDGET_UPLOAD' => '/20budget10',
	'COMKPI' => '/comkpi',
	'CUSTOMER_FILES' => '/customerFiles',
	'DASHBOARD_FILES' => '/dashboardfiles',
	'EXPORTS' => '/exports',
	'LABOR_FILES' => '/labor',
	'NUTRITION_IMPORT' => '/nutrition-import',
	'PAYROLL_FILES' => '/payroll',
	'REPORT_FILES' => '/reports',
	'SMARTMENU_FILES' => '/smartmenu',
	'UPLOADS' => '/control-sheet',
	'UTILITY' => '/utility',
	'VENDOR_IMPORTS' => '/import',
);

// HTTP_* -- http access path (relative to document root)
// DIR_* -- php access path (absolute)
foreach( $directories as $name => $path ) {
	if( !defined( 'HTTP_'.$name ) ) {
		define( 'HTTP_'.$name, UPLOAD_PATH . $path );
	}
	if( !defined( 'DIR_'.$name ) ) {
		define( 'DIR_'.$name, DOC_ROOT . UPLOAD_PATH . $path );
	}
}

// clients upload data to this location via scp/ftp:
//  /home/espos/www/CAISO/OutlookCalenderExcel.csv
if( !defined( 'DIR_FTP_UPLOADS' ) ) {
	if ( file_exists( '/srv/home/espos/www' ) ) {
		define( 'DIR_FTP_UPLOADS', '/srv/home/espos/www' );
	}
	else {
		define( 'DIR_FTP_UPLOADS', '/home/espos/www' );
	}
}

if( !defined( 'DIR_TMP_FILES' ) ) {
	define( 'DIR_TMP_FILES', DOC_ROOT . '/../tmp' );
}

// older names, kept for backward compatibility
if ( !defined( 'UPLOADS_DIRECTORY' ) ) {
	define( 'UPLOADS_DIRECTORY', DOC_ROOT . UPLOAD_PATH );
}
if ( !defined( 'ROUTE_EXPORTS_DIRECTORY' ) ) {
	define( 'ROUTE_EXPORTS_DIRECTORY', DOC_ROOT . UPLOAD_PATH . '/exports' );
}

// Tell Models what PDO to use
\EE\Model\Base::db( \EE\DB::singleton() );	// models using businesstracks database
\EE\Model\AbstractModel\Manage::db(\EE\DB::singleton('manage'));	// for models using manage database
\EE\Loader::loadHelpers();

# can't start a session when in cli mode
if ( 'cli' != php_sapi_name() ) {
	// Start Session
#	if ( 'memcache' == ini_get( 'session.save_handler' ) ) {
#		SiTech_Session::registerHandler( new SiTech_Session_Handler_Memcache() );
#	}
#	else {
		\EE\Session::registerHandler( new \EE\Session\Handler\DB( \EE\Model\AbstractModel\Treat::db() ) );
#	}
	\EE\Session::start();
	#Treat_Session::singleton();
}

function require_ta_bootstrap() {
	// required or set by the original Treat bootstrap.
	if ( !defined( 'TREAT_DEV' ) ) {
		define( 'TREAT_DEV', !IN_PRODUCTION ); // Change to true for a development server
	}
	$var = 'bootstrap.php';
	require_once( DOC_ROOT . DIRECTORY_SEPARATOR . $var );
}

// need the google_page_track() function out of this functions file for the default template
require_once 'inc/functions.php';

# addition of routes via SiTech_Controller::addRoute() is done in index2.php
# instead of here as would normally be the case because there are some pages
# that pull in this bootstrap that don't go through the controller.

