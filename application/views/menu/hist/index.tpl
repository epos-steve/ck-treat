<?php

?>



<?php
#	$weeknum = 5;
#	$today = date("Y-m-d");

	/////////SETTINGS LINKS
?>

			<div class="option_settings">
				<a href="/menu/printhistory.php" target="_blank">
					Printable Version
				</a> ::
<?php
	if ( $set ) {
?>
				<a href="/menu/history">
					Hide Settings
				</a>
<?php
	}
	else {
?>
				<a href="/menu/history?set=1">
					My Settings
				</a>
<?php
	}
?>
			</div>

<?php
	/////////SETTINGS DISPLAY
	
	if ( $set ) {
?>
			<table width="100%" class="custom">
				<tbody>
					<tr valign="top">
						<td colspan="2" class="option_block" width="33%">
							<div class="caption">
								Display:
							</div>
							<form action="/menu/history/display_set" method="post">
								<input type="hidden" name="customerid" value="<?php echo $_SESSION->getUser()->customerid; ?>" />
								<div>
									<label>
										# of Weeks to Show:
										<input type="text" size="1" name="weeknum" value="<?php
											echo $customer_limits->weeknum;
										?>" maxlength="1" />
									</label>
								</div>
<?php
		foreach ( $nutrition_types as $nutrition_type ) {
			if ( !$nutrition_type->is_deleted ) {
?>
								<div>
									<label>
										<input type="checkbox" name="display_type[<?php
											echo $nutrition_type->nutritionid;
										?>]" value="1" <?php
											if (
												array_key_exists( $nutrition_type->column_key, $customer_limits )
												&& $customer_limits[ $nutrition_type->column_key ]->display
											) {
												echo ' checked="checked"';
											}
											elseif ( !array_key_exists( $nutrition_type->column_key, $customer_limits ) ) {
												$customer_limits[ $nutrition_type->column_key ] = new Treat_ArrayObject();
											}
										?> />
										<?php echo $nutrition_type->name; ?> 
									</label>
								</div>
<?php
			}
		}
?>
								<div class="submit_button">
									<input type="submit" value="Save" />
								</div>
							</form>
						</td>


						<td colspan="2" class="option_block" width="33%">
							<div class="caption">
								My Limits:
							</div>
							<table width="95%" class="usda_daily">
								<tbody>
									<tr>
										<td colspan="2" class="type">
											<i>
												Example: Calories 2000
											</i>
										</td>
									</tr>
<?php
		$existing_types = array();
		foreach ( $customer_limits as $key => $display_limit ) {
			if ( !$display_limit->nutrition_type_is_deleted && null !== $display_limit->value ) {
				$existing_types[ $display_limit->nutrition_type_id ] = $key;
?>
									<tr>
										<td class="type">
											<?php
												echo Treat_Model_Nutrition_TypesObject::getLabelByKey( $key );
											?> 
										</td>
										<td width="40%" class="amount">
											<?php echo $display_limit->value; ?> 
											<?php
												echo Treat_Model_Nutrition_TypesObject::getUnitsByKey( $key );
											?> 
											<form action="/menu/history/limit_delete" method="post" class="delete_item">
												<input type="hidden" name="del" value="1" />
												<input type="hidden" name="customerid" value="<?php
													echo $customer_limits->customerid;
												?>" />
												<input type="hidden" name="type_id" value="<?php
													echo Treat_Model_Nutrition_TypesObject::getIdByKey( $key );
												?>" />
												
												<button type="submit" class="delete_item" title="Delete Item">
													<img src="/assets/images/nutrition/delete.gif"
														height="16" width="16" border="0"
														alt="Delete Item"
													/>
												</button>
											</form>
										</td>
									</tr>
<?php
			}
		}
?>
									<tr>
										<td colspan="2" class="type">
											<form action="/menu/history/limit_set" method="post">
												<input type="hidden" name="customerid" value="<?php
													echo $customer_limits->customerid;
												?>" />
												<select name="type">
<?php
		foreach ( $nutrition_types as $type ) {
			if ( !in_array( $type->column_key, $existing_types ) ) {
?>
													<option value="<?php
														echo $type->nutritionid;
													?>"><?php
														echo $type->name;
#														var_dump( $type );
													?></option>
<?php
			}
		}
?>
												</select>
												<input type="text" size="4" name="value" />
												<input type="submit" value="Add" />
											</form>
										</td>
									</tr>
									<tr>
										<td colspan="2" style="border:0px;">
											<font size="1">
												*Exceeded Limits will show in red. 
												It is recommended that you choose 
												limits that correspond to your 
												eating habits (Breakfast, Breakfast 
												and Lunch, or Lunch), which may be 
												less than your total USDA 
												recommended daily intake.
											</font>
										</td>
									</tr>
								</tbody>
							</table>
						</td>


						<td colspan="2" class="option_block" width="33%">
							<div class="caption">
								USDA Recommended Daily Intake*:
							</div>
							<table width="95%" class="usda_daily">
								<tbody>
									<tr>
										<td class="type">Total Fat</td>
										<td width="40%" class="amount">65 g</td>
									</tr>
									<tr>
										<td class="type">Saturated Fat</td>
										<td class="amount">20 g</td>
									</tr>
									<tr>
										<td class="type">Cholesterol</td>
										<td class="amount">300 mg</td>
									</tr>
									<tr>
										<td class="type">Sodium</td>
										<td class="amount">2400 mg</td>
									</tr>
									<tr>
										<td class="type">Potassium</td>
										<td class="amount">3500 mg</td>
									</tr>
									<tr>
										<td class="type">Carbs</td>
										<td class="amount">300 g</td>
									</tr>
									<tr>
										<td class="type">Fiber</td>
										<td class="amount">25 g</td>
									</tr>
									<tr>
										<td class="type">Protein</td>
										<td class="amount">50 g</td>
									</tr>
									<tr>
										<td colspan="2">
											<font size="1">
												*Based on a daily 2000 calorie diet.
											</font>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
<?php
	}
	/////////END SETTINGS
?>


			<table width="100%" cellspacing="1" cellpadding="1" class="">
				<thead>
					<tr class="days_of_week">
						<td width="16.5%">
							Monday
						</td>
						<td width="16.5%">
							Tuesday
						</td>
						<td width="16.5%">
							Wednesday
						</td>
						<td width="16.5%">
							Thursday
						</td>
						<td width="16.5%">
							Friday
						</td>
						<td width="16.5%" class="totals">
							Totals
						</td>
					</tr>
				</thead>
				<tbody>
<?php
	$history_total_days = 0;
	foreach ( $week_sets as $week ) {
?>
					<tr class="week">

<?php
		$days = $week->getRecipeItems();
		$num_days_of_week = count( $days );
		$history_total_days += $num_days_of_week;
		foreach ( $days as $day ) {
			/////////////////////////RECIPES////////////////
?>
						<td class="block_day<?php
							if ( Treat_Date::getValidDateStamp( $showdate ) == $day->getDay() ) {
								echo ' showdate';
							}
						?>">
							<table cellspacing="0" width="100%"
								onclick="document.location.href='/menu/showreserve.php?date=<?php echo date( 'Y-m-d', $day->getDay() ); ?>'"
							>
								<thead>
									<tr>
										<td colspan="2">
											<a name="<?php echo date( 'Y-m-d', $day->getDay() ); ?>"></a>
											<a href="/menu/showreserve.php?date=<?php
												echo date( 'Y-m-d', $day->getDay() );
											?>" title="View Details">
												<?php echo date( 'Y-m-d', $day->getDay() ); ?> 
											</a>
										</td>
									</tr>
								</thead>
								<tbody>
<?php
			$row_count = 0;
			foreach ( $customer_limits as $key => $display_limit ) {
				if (
					!$display_limit->nutrition_type_is_deleted
					&& $display_limit->display
	#				&& null !== $display_limit->value
				) {
					$row_classes = array();
					if ( $row_count % 2 ) {
						$row_classes[] = 'even';
					}
					else {
						$row_classes[] = 'odd';
					}
					
					if ( 'treat_score' == $key ) {
						$score = round( $day->getTreatScore(), 0 );
					}
					else {
						$score = round( $day->getNutritionValue( $key ), 0 );
					}
					
					if ( null !== $display_limit->value && $score > $display_limit->value ) {
							$row_classes[] = 'overlimit';
					}
					///////////nutrition
?>
									<tr class="<?php echo implode( ' ', $row_classes ); ?>">
										<td class="type">
											<?php
												echo Treat_Model_Nutrition_TypesObject::getLabelByKey( $key );
											?> 
										</td>
										<td class="amount">
											<?php echo $score; ?> 
										</td>
									</tr>
<?php
					$row_count++;
				}
			}
			////////////end nutrition
?>
								</tbody>
							</table>


						</td>
<?php
		}
?>









						<td class="block_day week_total">
							<table cellspacing="0" width="100%">
								<thead>
									<tr>
										<td colspan="2">
											Week Total
										</td>
									</tr>
								</thead>
								<tbody>
<?php
		////////////////////////////////////
		////////ROW TOTALS//////////////////
		////////////////////////////////////
			$row_count = 0;
			foreach ( $customer_limits as $key => $display_limit ) {
				if (
					!$display_limit->nutrition_type_is_deleted
					&& $display_limit->display
	#				&& null !== $display_limit->value
				) {
					$row_classes = array();
					if ( $row_count % 2 ) {
						$row_classes[] = 'even';
					}
					else {
						$row_classes[] = 'odd';
					}
					
					if ( 'treat_score' == $key ) {
						$score = round( $week->getTreatScore(), 0 );
					}
					else {
						$score = round( $week->getNutritionValue( $key ), 0 );
					}
					
					if ( null !== $display_limit->value && $score > ( $display_limit->value * $num_days_of_week ) ) {
							$row_classes[] = 'overlimit';
					}
?>
									<tr class="<?php echo implode( ' ', $row_classes ); ?>">
										<td class="type">
											<?php
												echo Treat_Model_Nutrition_TypesObject::getLabelByKey( $key );
											?> 
										</td>
										<td class="amount">
											<?php echo $score; ?> 
										</td>
									</tr>
<?php
					$row_count++;
				}
			}
?>
								</tbody>
							</table>
						</td>
					</tr>
<?php
	}
?>
					<tr class="week">
						<td colspan="5" valign="top">
							<font face="arial" size="2"> Click on the Date to view details. </font>
						</td>
						<td class="block_day week_total">
							<table cellspacing="0" width="100%">
								<thead>
									<tr>
										<td colspan="2">
											Period Total
										</td>
									</tr>
								</thead>
								<tbody>
<?php
	//////////////////////////////////////
	//////////////SHEET TOTALS////////////
	//////////////////////////////////////
			$row_count = 0;
			foreach ( $customer_limits as $key => $display_limit ) {
				if (
					!$display_limit->nutrition_type_is_deleted
					&& $display_limit->display
#					&& null !== $display_limit->value
				) {
					$row_classes = array();
					if ( $row_count % 2 ) {
						$row_classes[] = 'even';
					}
					else {
						$row_classes[] = 'odd';
					}
					
					if ( 'treat_score' == $key ) {
						$score = round( $history->getTreatScore(), 0 );
					}
					else {
						$score = round( $history->getNutritionValue( $key ), 0 );
					}
					
					if ( null !== $display_limit->value && $score > ( $display_limit->value * $history_total_days ) ) {
							$row_classes[] = 'overlimit';
					}
?>
									<tr class="<?php echo implode( ' ', $row_classes ); ?>">
										<td class="type">
											<?php
												echo Treat_Model_Nutrition_TypesObject::getLabelByKey( $key );
											?> 
										</td>
										<td class="amount">
											<?php echo $score; ?> 
										</td>
									</tr>
<?php
					$row_count++;
				}
			}
?>
								</tbody>
							</table>
						</td>
					</tr>
<?php
	//////////////////////////////
	////////////END SHEET TOTALS//
	//////////////////////////////
?>

				</tbody>
			</table>


			<p></p>
			<table width="90%">
				<tr>
					<td>
						<font size="1" face="arial" color="gray">
							Nutritional information is provided as a general guide only. 
							Variation in serving sizes, preparation techniques and 
							sources of supply, as well seasonal differences may affect 
							the nutrition values for each product. In addition, product 
							formulations change periodically. You should expect some 
							variation in the nutrient content of our products. If you are 
							on a restricted diet for the treatment of a disease or other 
							condition, you should consult your physician, or registered 
							dietician for more specific nutritional guidelines.
						</font>
					</td>
				</tr>
			</table>
<?php
