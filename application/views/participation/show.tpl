<?php

/** @var $program Treat_Model_Participation */
/** @var $conflicts array[] */
/** @var $businesses array[] */
?>
<script type="text/javascript">

    jQuery(function() {

        jQuery("#businessAccordion").accordion({
            autoHeight: false,
            navigation: true,
            collapsible: true
        });

        // Initialize the tree in the onload event
        jQuery("#businessMappingTree").dynatree({
            checkbox:       true,
            selectMode:     3,
            activeVisible:  true
        });

    });

    function pprg_update() {
        var ppid = jQuery('#ppid').val();
        var checkedItems = jQuery('#pprgItemForm input:checked'); // input:checked');
        var data = 'ppid=' + ppid;

        jQuery.each(checkedItems, function(key, value) {
            itemId = value.id.substr(3);
            data += "&UPC[]=" + itemId;
        });
        var treediv = jQuery("#businessMappingTree");
        if (treediv.length != 0) {

            var tree = treediv.dynatree("getTree");
            var selKeys = jQuery.map(tree.getSelectedNodes(), function(node) {
                return node.data.key;
            });
            jQuery.each(selKeys, function(key, value) {
                var barpos = value.indexOf("_");
                var type = value.substr(0,barpos);
                var id = value.substr(barpos+1);
                data += '&' + type + '[]=' + id;
            });
        }

        if (data != "") {
            jQuery.post(
                '/participation/save/' + ppid + '/',
                encodeURI(data),
                function(response, textStatus, xhr){
                }
            );
        } else {
            alert('No data sent.');
        }
        alert('data = "' + data + '"');
    }

</script>

<style type="text/css">
    .conflictList {
        list-style: circle;
    }
    .conflictList li {
        display: inline;
        font-size: 12px;
        font-weight: bold;
    }
</style>

<h2>The following UPCs are already registered in one or more applicable businesses and cannot be restricted by this program</h2>

<div id="businessAccordion" class="ui-accordion ui-widget ui-helper-reset">
<?php foreach ($conflicts as $UPC =>  $carray) : ?>
    <h3 class="ui-accordion-header ui-helper-reset ui-state-active ui-corner-top"><a href='#'><?=$UPC?> (<?=$names[$UPC]?>)</a></h3>
        <div style="padding:0px;" class="ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom"><ul class='conflictList'>
    <?php foreach ($carray as $conflict) : ?>
        <li> • <?=$conflict['businessname']?> (bid #<?=$conflict['businessid']?>)</li>
    <?php endforeach; ?>
    </ul></div>
<?php endforeach; ?>
</div>

<h1><?=$program->name?></h1>
<form method="post" id='pprgItemForm' onsubmit='pprg_update();return false;'>
    <input type="hidden" id="ppid" value="<?=$ppid?>" />
    <input type="button" id="Save" value="Save" onclick='pprg_update();' /><br />
    <h2>Whitelisted items</h2>


<div class='settingsTable' id='pprgItems'>
<?php
for ($c = 0; $c < count($items); $c++) {
    $column_num = ($c % 3) + 1;
    if ($column_num == 1) {
        echo "<div class='settingsRow'>\n";
    }

    echo "<div class='settingsThird'>\n";
    echo "\t<div class='settingsHeader ui-state-default'>";
    echo "<a href='javascript:' onclick='jQuery(\"#category" . $items[$c]['category_id'] . "\").toggle();'>" . $items[$c]['category_name'] . "</a>";
    echo "</div>\n";
    echo "\t<div class='settingsForm' id='category{$items[$c]['category_id']}'>\n";
    foreach ($items[$c]['category_items'] as $item) {
        $id = $item['id'];
        $name = $item['name'];
        $upc = $item['upc'];
        if (in_array($upc, $upcs)) {
            $is_checked = "CHECKED";
        } else {
            $is_checked = NULL;
        }
        echo "\t\t<div class='settingsItem'><input type=checkbox class='pprgItem' name=mid$upc id='mid$upc' value='1' $is_checked /><label for='mid$upc'>$name</label></div>\n";
    }
    echo "\t</div>\n</div>\n";
    if ($column_num == 3) {
        echo "</div>\n";
    }
}
?>
</form>
</div>

<h2>Businesses</h2>
<div id="pprg_businesses">
    <div class="settingsContainer">
        <div class="settingsHeader ui-state-default">Businesses</div>
        <div class="settingsForm">
            <div id="businessMappingTree" name="selBusinesses">
                <ul>
                    <?php foreach ($companies as $company) { ?>
                                <li id="company_<?=$company->companyid?>" class="folder"><?=strip_tags($company->companyname)?>
                    <ul>
                        <?php foreach ($company->getDistricts() as $district) { ?>
                                                <li id="district_<?=$district->districtid?>" class="folder"><?=strip_tags($district->districtname)?>
                        <ul>
                            <?php foreach ($district->getBusinesses() as $business) {
                            if ($business->categoryid != Treat_Model_Business::CATEGORY_KIOSK) {    // skip if not kiosk
                                continue;
                            }
                            if (in_array($business->businessid, $enabled)) {
                                $selected = " selected";
                            } else {
                                $selected = "";
                            }
                            echo "<li id='business_{$business->businessid}' class='$selected'>" . strip_tags($business->businessname);
                            echo "\n";
                            ?>
                            <?php } ?>
                        </ul>
                        <?php } ?>
                    </ul>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>

</div>
