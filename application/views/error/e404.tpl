<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title><?php
			if ( isset( $title ) ) {
				echo '404 Not Found :: ', $title, ' :: ', Treat_Config::singleton()->getValue( 'application', 'page_title' );
			}
			else {
				echo '404 Not Found :: ', Treat_Config::singleton()->getValue( 'application', 'page_title' );
			}
		?></title>
	</head>
	<body>
		<h1>Not Found</h1>
		<p>The requested URL <?php echo $path; ?> was not found on this server.</p>
		<hr/>
		<address>Apache/2.2.14 (Ubuntu) Server at <?php echo $host; ?> Port <?php echo $port; ?></address>
	</body>
</html>