<div class="divTable"><div class="divTableRow">
		<input type='hidden' id='currentFunctionPanel' value='' />
		<input type='hidden' name='bid' id='functionPanelbid' value='<?=$businessid ?>' />
		<input type='hidden' name='cid' id='functionPanelcid' value='<?=$companyid ?>' />
		<input type='hidden' name='mi.group_id' value='0'>

		<div id='functionPanelEditGroupDialog' style='display: none;'>
			<form action='functionPanelEditGroup' id='functionPanelEditGroupForm' onsubmit='return false;' method='post'>
				<input type='hidden' name='bid' value='<?=$businessid ?>' />
				<input type='hidden' name='functionPanelGroup_id' id='functionPanelGroup_id' value='0' />
				<div>
					<label for='functionPanelGroup_name'>Group Name:</label>
					<input type='text' class='screenOption' name='functionPanelGroup_name' id='functionPanelGroup_name' size='25' value='New Group' />
				</div>
			</form>
		</div>

		<div id='functionPanelAddGroupDialog' style='display: none;'>
			<form action='functionPanelAddGroup' id='functionPanelAddGroupForm' onsubmit='return false;' method='post'>
				<input type='hidden' name='bid' value='<?=$businessid ?>' />
				<div id='functionPanel_AddGroup'>
					<div class='itemListHeader' style='position: relative;'>
						<label><input type='text' id='functionPanel_AddGroupFilter' class='itemListFilter' value='' /></label>
						<span id='functionPanel_AddGroupFilterReset' class='itemListFilterReset' style='float: right;'>Reset</span>
					</div>
					<select id='functionPanel_AddGroupSelect' style='width: 100%' multiple='multiple'></select>
				</div>
				<img class='bigSpinner bigSpinnerCenter' id='functionPanel_AddGroupSpinner' style='display: none;' src='/assets/images/ajax-loader-redmond-big.gif' />
			</form>
		</div>

		<div id='functionPanelAddItemDialog' style='display: none;'>
			<form action='functionPanelAddItem' id='functionPanelAddItemForm' onsubmit='return false;' method='post'>
				<input type='hidden' name='bid' value='<?=$businessid ?>' />
				<div id='functionPanel_AddItem'>
					<div class='itemListHeader' style='position: relative;'>
						<input type='text' id='functionPanel_AddItemFilter' class='itemListFilter' value='' />
						<span id='functionPanel_AddItemFilterReset' class='itemListFilterReset' style='float: right;'>Reset</span>
					</div>
					<select id='functionPanel_AddItemSelect' style='width: 100%' multiple='multiple'></select>
				</div>
				<img class='bigSpinner bigSpinnerCenter' id='functionPanel_AddItemSpinner' style='display: none;' src='/assets/images/ajax-loader-redmond-big.gif' />
			</form>
		</div>

		<div id='functionPanelPage' class='divTableCell'>

			<div id='functionPanelContainer' class='cashier' style='width:400px;'>
				<select style='width: 100%;' size='8' id='functionPanelList'></select>
				<div>
					<input type="checkbox" name="showInactive" id="functionPanelShowInactive" /> Show inactive
				</div>
				<div id='functionPanelItems' style='height:220px;' class='functionPanelItems'></div>
			</div>

			<div id='functionPanelProperties' class='ui-widget'>

				<div id='functionPanelEditContainer' class='ui-widget-content ui-corner-all ui-state-default'>
					<p class="editNone">No function panel selected</p>
					<div class='editItem ui-helper-hidden-accessible'>
						<form class='ajaxeditBusy' action='functionPanelEditItem' id='functionPanelForm' method='post'>
							<fieldset>
								<legend>Panel Name</legend>
								<input type='text' name='name' alt='Panel name' value='New Function Panel' size='32' class='functionPanelOption validate-notempty'  />
							</fieldset>
							<fieldset>
								<legend>Size</legend>
								<input type='text' name='rows' alt='rows' value='3' size='32' class='functionPanelOption slider-1-12 validate-notempty validate-type-integer' />
								<input type='text' name='cols' alt='columns' value='1' size='32' class='functionPanelOption slider-1-12 validate-notempty validate-type-integer' />
							</fieldset>
							<fieldset>
								<input type='hidden' name='companyid' value='<?=$companyid?>' />
								<input type='hidden' name='id' value='' />
								<input type='submit' value='Save' />
								<input type='reset' value='Cancel' />
								<input id="duplicateFunctionPanelButton" type="button" value="Duplicate" />
								<input id='addFunctionPanelButton' type='button' value='Add Button' />
								<input id='functionPanelToggleActivationButton' type='button' value='Deactivate' style='float:right;' />
							</fieldset>
						</form>
						<!--<textarea cols='80' rows='10' id='debug-out'></textarea>-->
					</div>


					<div class='editButton ui-helper-hidden-accessible'>
						<form class='ajaxeditBusy' action='functionPanelEditButton' id='functionPanelButtonForm' method='post'>
							<fieldset>
								<legend>Button Name</legend>
								<input type='text' name='name' alt='Button name' value='New Button' size='32' class='functionPanelOption validate-notempty'  />
							</fieldset>
							<fieldset>
								<select name='pos_color' alt='Button Color' class='picker validate-type-color picker-prefix-product-button-color-'>
									<?php for( $i = 0; $i < 12; $i++ ) { ?>
									<option><?=$i?></option>
									<?php } ?>
								</select>
							</fieldset>
							<fieldset>
								<legend>Function</legend>
								<select id='function_id' name='function_id' alt='Function'></select>
								<div style="clear: both;" id="functionPanelFunctionDescription" />
							</fieldset>
							<fieldset>
								<legend>Arguments</legend>
								<input type='text' name='args' alt='args' value='' size='32' class='functionPanelOption' />
							</fieldset>
							<fieldset>
								<input type='hidden' name='companyid' value='<?=$companyid?>' />
								<input type='hidden' name='id' value='' />
								<input type='hidden' name='fpanel_id' value='' />
								<input type='hidden' name='page' value='' />
								<input type='submit' value='Save' />
								<input type='reset' value='Cancel' />
								<input type='button' value='Delete' />
							</fieldset>
						</form>
					</div>

				</div>

			</div>
		</div>

	</div></div>
