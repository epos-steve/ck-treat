<html>
<head>
	<script type="text/javascript" src="/assets/js/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="/assets/js/jquery-ui-1.7.2.custom.min.js"></script>
    <script type="text/javascript">$(document).ready(function (){$('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' });});</script>
	<link rel="stylesheet" href="/assets/css/jq_themes/redmond/jquery-ui-1.7.2.custom.css">
</head>
<body>

<form action="" method="get">
	<input type="text" name="date" value="<?php echo $date->format( 'Y-m-d' ); ?>" size="40" class="datepicker">
	<input type="submit" value="GO">
</form>

<table width="100%" cellspacing="0" cellpadding="1" style="border:1px solid black;">
	<tr bgcolor="#FFFF99">
		<td style="border:1px solid black;">Item Code</td>
		<td style="border:1px solid black;">Item</td>
		<td style="border:1px solid black;">Qty</td>
		<td style="border:1px solid black;">Price</td>
		<td style="border:1px solid black;">Sales</td>
	</tr>

<?php
	foreach ( $items as $item ) {
?>
	<tr>
		<td style="border:1px solid black;"><?php echo $item["item_code"]; ?></td>
		<td style="border:1px solid black;"><?php echo $item["name"]; ?></td>
		<td style="border:1px solid black;"><?php echo $item["sold"]; ?></td>
		<td style="border:1px solid black;">$<?php echo number_format( $item["price"], 2 ); ?></td>
		<td style="border:1px solid black;">$<?php echo number_format( $item["sales"], 2 ); ?></td>
	</tr>
<?php
	}
?>

</table>

</body>
</html>