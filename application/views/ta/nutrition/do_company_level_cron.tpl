
<?php
$types = Treat_Model_Nutrition_TypesObject::getTypeKeys();
if ( $wrapper && $types ) {
?>
<table border="1">
	<thead>
		<tr>
			<th>companyid</th>
			<th>districtid</th>
			<th>businessid</th>
			<th><?php echo Treat_Model_Nutrition_BusinessLevel_Abstract::FIELD_DATE_STAMP; ?></th>
			<th>payment_type</th>
			<th>unique_user</th>
<?php
				foreach ( $types as $type ) {
?>
			<th><?php echo $type; ?></th>
<?php
				}
?>
		</tr>
	</thead>
	<tbody>
<?php
	function output( $item, $key = null ) {
		if ( $item->hasChildren ) {
			foreach ( $item as $key => $child ) {
				output( $child, $key );
			}
		}
		else {
			$types = Treat_Model_Nutrition_TypesObject::getTypeKeys();
?>
		<tr valign="top">
			<td><?php echo $item->companyid; ?></td>
			<td><?php echo $item->districtid; ?></td>
			<td><?php echo $item->businessid; ?></td>
			<td><?php echo $item->{Treat_Model_Nutrition_BusinessLevel_Abstract::FIELD_DATE_STAMP}; ?></td>
			<td><?php echo $key; ?></td>
			<td><?php echo $item->unique_user; ?></td>
<?php
			foreach ( $types as $type ) {
				$per_user = 0;
				if ( $item->unique_user ) {
					$per_user = $item->{$type} / $item->unique_user;
				}
?>
			<td nowrap="nowrap" title="<?php
				echo round( $item->{$type}, 2 );
			?>"><?php
				echo round( $per_user, 2 );
			?></td>
<?php
			}
?>
		</tr>
<?php
		}
	}
	output( $wrapper );
?>
	</tbody>
</table>
<?php
}
?>

