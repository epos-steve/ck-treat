
<?php
$types = Treat_Model_Nutrition_TypesObject::getTypeKeys();
if ( $wrapper && $types ) {
?>
<?php
/*
* /
?>
<table border="1">
	<thead>
		<tr>
			<th>companyid</th>
			<th>districtid</th>
			<th>businessid</th>
			<th><?php echo Treat_Model_Nutrition_BusinessLevel_Abstract::FIELD_DATE_STAMP; ?></th>
			<th><?php echo Treat_Model_Nutrition_BusinessLevel_Abstract::FIELD_WEEK_NUMBER; ?></th>
			<th>payment_type</th>
			<th>unique_user</th>
<?php
				foreach ( $types as $type ) {
?>
			<th><?php echo $type; ?></th>
<?php
				}
?>
		</tr>
	</thead>
	<tbody>
<?php
	function output( $item, $key = null ) {
		if ( $item->hasChildren ) {
			foreach ( $item as $key => $child ) {
				output( $child, $key );
			}
		}
		else {
			$types = Treat_Model_Nutrition_TypesObject::getTypeKeys();
?>
		<tr valign="top">
			<td><?php echo $item->companyid; ?></td>
			<td><?php echo $item->districtid; ?></td>
			<td><?php echo $item->businessid; ?></td>
			<td><?php echo $item->{Treat_Model_Nutrition_BusinessLevel_Abstract::FIELD_DATE_STAMP}; ?></td>
			<td><?php echo $item->{Treat_Model_Nutrition_BusinessLevel_Abstract::FIELD_WEEK_NUMBER}; ?></td>
			<td><?php echo $key; ?></td>
			<td><?php echo $item->unique_user; ?></td>
<?php
				foreach ( $types as $type ) {
					$per_user = 0;
					if ( $item->unique_user ) {
						$per_user = $item->{$type} / $item->unique_user;
					}
?>
			<td nowrap="nowrap" title="<?php
				echo round( $item->{$type}, 2 );
			?>"><?php
				echo round( $per_user, 2 );
			?></td>
<?php
				}
?>
		</tr>
<?php
		}
	}
	output( $wrapper );
?>
	</tbody>
</table>
<?php
/*
*/
?>


			<table width="100%" cellspacing="1" cellpadding="1" class="" border="1">
				<thead>
					<tr class="days_of_week">
						<td width="12.5%">Sunday</td>
						<td width="12.5%">Monday</td>
						<td width="12.5%">Tuesday</td>
						<td width="12.5%">Wednesday</td>
						<td width="12.5%">Thursday</td>
						<td width="12.5%">Friday</td>
						<td width="12.5%">Saturday</td>
						<td width="12.5%" class="totals">Totals</td>
					</tr>
				</thead>
				<tbody>
<?php
	function out( $item, $key = null, $override = false ) {
		if ( $item instanceof Treat_Model_Nutrition_BusinessLevel_Week && !$override ) {
			$types = Treat_Model_Nutrition_TypesObject::getTypes();
?>
					<tr class="week">
<?php
			foreach ( $item as $key => $child ) {
?>
						<td class="block_day">
<?php
				out( $child, $key );
?>
						</td>
<?php
			}
?>
						<td class="block_day week_total">
<?php
			out( $item, $key, true );
?>
						</td>
					</tr>
<?php
		}
		elseif ( $item->hasChildren() && !$override ) {
			foreach ( $item as $key => $child ) {
				out( $child, $key );
			}
		}
		elseif (
			Treat_Model_Catapult_TenderType::TENDER_TYPE_GIFT_CARD == $item->{Treat_Model_Nutrition_BusinessLevel_Abstract::FIELD_PAYMENT_TYPE}
			|| $override
		) {
			#<editor-fold desc="elseif ( Treat_Model_Catapult_TenderType::TENDER_TYPE_GIFT_CARD == $item->{FIELD_PAYMENT_TYPE} ) {">
			$types = (array)Treat_Model_Nutrition_TypesObject::getTypes();
			$tmp = new Treat_ArrayObject();
			$tmp->label = 'Unique Users';
			$tmp->key = Treat_Model_Nutrition_BusinessLevel_Abstract::FIELD_UNIQUE_USER;
			$types[] = $tmp;
?>
							<table cellspacing="0" width="100%" border="1">
								<thead>
									<tr>
										<th colspan="2"><?php
											echo $item->{Treat_Model_Nutrition_BusinessLevel_Abstract::FIELD_DATE_STAMP};
										?></th>
									</tr>
								</thead>
								<tbody>
<?php
			$row_count = 0;
			foreach ( $types as $type ) {
				$row_classes = array();
				if ( $row_count % 2 ) {
					$row_classes[] = 'even';
				}
				else {
					$row_classes[] = 'odd';
				}
				$per_user = 0;
				if ( 'unique_user' == $type->key ) {
					$per_user = $item->unique_user;
				}
				elseif ( $item->unique_user ) {
/*
* /
					if ( $override ) {
						echo '<pre style="border: 1px solid red;">';
#						echo '$type = ';
#						var_dump( $type );
						echo '$item->unique_user = ';
						var_dump( $item->unique_user );
						echo '$type->key = ';
						var_dump( $type->key );
						echo '$item->{$type->key} = ';
						var_dump( $item->{$type->key} );
						echo '</pre>';
					}
/*
*/
					$per_user = $item->{$type->key} / $item->unique_user;
				}
?>
									<tr class="<?php echo implode( ' ', $row_classes ); ?>">
										<td class="type"><?php
											echo $type->label;
										?></td>
										<td class="amount" title="<?php
											echo round( $item->{$type->key}, 2 );
										?>"><?php
											echo round( $per_user, 2 );
										?></td>
									</tr>
<?php
				$row_count++;
			}
?>
								</tbody>
							</table>
<?php
			#</editor-fold>
		}
	}
	out( $wrapper );
		
?>
<?php
	//////////////////////////////
	////////////END SHEET TOTALS//
	//////////////////////////////
?>

				</tbody>
			</table>

<?php
}
?>

