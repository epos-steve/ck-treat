<style>
.highlight{background-color:yellow;}
.note{font-size:12px;color:green;font-weight:bold;}
</style>

<div id="loading" style="display:none;">
	<div id="overlay"></div>
	<div id="message">
		Loading... Please Wait<br/><img src="/assets/images/preload/loading.gif" height="22" width="126">
	</div>
</div>

<table cellspacing='0' cellpadding='0' border='0' width='100%'>
	<tr bgcolor='#F6F9FA'>
		<td colspan='2' align='right'>&nbsp;</td>
	</tr>
	<tr style='height: 1px; background-color: black; vertical-align: top;'>
		<td colspan='2'>
			<img src='/assets/images/spacer.gif' alt='' />
		</td>
	</tr>
	
	<tr valign=top bgcolor=#E8E7E7>
		<td width=25% rowspan=3>
			<form action="#" method="post" style="margin: 0; padding: 0; display: inline;">
				<center>
					<select size="18" name="menu_item" id="menu_item" style="width: 255px;">
						<?php print $item_select; ?>
					</select>
					
					<p class="note"><?php print $nutrition_per; ?>% <?php print $nutrition_num; ?> of items have nutrition information.<br />* = item does not have nutrition information.</p>
				</center>
			</form>			
		</td>
		<td width=75%>
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td valign="top">
					
						<form name="item_form" id="item_form" style="margin:0;padding:0;display:inline;">
							<table style="width:425px;" cellspacing="0" cellpadding="0">
								<tr>
									<td colspan="4">&nbsp;</td>
								</tr>
								
								<tr>
									<td align="center" colspan="2" bgcolor="#cccccc"><b>General Information</b></td>
								</tr>
					
								<tr>
									<td align="right">UPC:</td>
									<td><input type="text" name="upc" id="upc" value="" size="40" /></td>
								</tr>					
								<tr>
									<td align="right">Item Name:</td>
									<td><input type="text" name="item_name" id="item_name" value="" size="40" /></td>
								</tr>					
								<tr>
									<td align="right">Manufacturer:</td>
									<td>
										<select name="manufacturer" id="manufacturer" style="width:262px;">
											<?php print $man_select; ?>
										</select>
									</td>
								</tr>					
								<tr>
									<td align="right">Category:</td>
									<td>
										<select name="category" id="category" style="width:262px;">
											<?php print $cat_select; ?>
										</select>
									</td>
								</tr>					
						
						
								<tr>
									<td colspan="2">&nbsp;</td>
								</tr>
								<tr>
									<td align="center" colspan="2" bgcolor="#cccccc"><b>Nutritional Information</b></td>
								</tr>
					
								<tr>
									<td colspan="2">
										<table style="width:425px;" cellspacing="0" cellpadding="0">
											<?php											
											$counter = 0;
											unset($nutrition_types->complex_cabs);
											foreach ( $nutrition_types as $nutrition_type ) {
												if ( !('treat_score' == $nutrition_type->column_key	|| $nutrition_type->is_deleted) ) {
													$counter++;
													
													if($counter % 2 != 0){
														?><tr><?php
													}
													?>
														<td width="25%" align="right">
															<label for="<?php echo $nutrition_type->column_key;	?>"><?php echo $nutrition_type->name; ?>:</label>
														</td>
														<td width="25%">
															<input type="text" size="12" id="<?php echo $nutrition_type->column_key; ?>" name="nutrition[<?php echo $nutrition_type->id; ?>]" />
														</td>							
													<?php
													if($counter % 2 == 0){
														?></tr><?php
													}
												}
											}
											
											if($counter % 2 != 0){
												?><td>&nbsp;</td><td>&nbsp;</td></tr><?php
											}
											?>
										</table>
									</td>
								</tr>
								<tr>
									<td colspan="2">&nbsp;</td>
								</tr>
								<tr>
									<td align="center" colspan="2" bgcolor="#cccccc"><b>Additional Categories</b></td>
								</tr>
								<tr>
									<td colspan=2>
										<?php print $html; ?>
									</td>
								</tr>
								<tr>
									<td colspan="2">&nbsp;</td>
								</tr>
								<tr>
									<td colspan="2">
										<input type="button" name="clear" id="clear" value="Clear Form" style="margin-right:35px;" />										
										<input type="button" name="save" id="save" value="Save" />										
										<input type="button" name="delete" id="delete" value="Delete" />
									</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td><div id="res_message" style="font-weight:bold;"></div></td>
								</tr>
							</table>						
						</form>
					
					</td>
					<td width="2%">&nbsp;</td>
					<td valign="top">
					
						<form name="search_form" id="search_form" action="#" method="post">
							<div style="border:2px solid #CCCCCC; width:375px; margin-top:20px; background: #FFFFFF;">
								<div style="background:#CCCCCC;">
									<center>
										<b>Search</b>
									</center>
								</div>
					
								<table width="98%" cellspacing="0" cellpadding="0">
									<tr><td align="right">UPC:</td><td><input type="text" name="search_upc" id="search_upc" size="40" /></td></tr>
									<tr><td align="right">Item Name:</td><td><input type="text" name="search_name" id="search_name" size="40" /></td></tr>
									<tr><td align="right">Manufacturer:</td><td>
										<select name="search_man" id="search_man" style="width:262px;">
											<?php print $man_select; ?>
										</select>
									</td></tr>
									<tr><td align="right">Category:</td><td>
										<select name="search_cat" id="search_cat" style="width:262px;">
											<?php print $cat_select; ?>
										</select>
									</td></tr>
									<tr>
										<td>&nbsp;</td>
										<td>
											<input type="submit" name="search" id="search" style="margin-right:35px;" value='Search'>
											<input type="button" name="reset_list" id="reset_list" value='Reset List'>
										</td>
									</tr>
								</table>
							</div>
						</form>
					
					</td>
				</tr>
			</table>
		</td>
	</tr>	
	
	<tr valign="bottom" bgcolor="#E8E7E7">
		<td align="right">
			<form id="manage" action="/ta/update_pos.php" method="post" style="margin: 0; padding: 0; display: inline;">
				<div class="info"></div>
				<input type="hidden" id="businessid" name="businessid" value="<?php echo $GLOBALS['businessid']; ?>" />
			</form>
			&nbsp;
		</td>
	</tr>
</table>