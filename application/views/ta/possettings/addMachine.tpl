<style type="text/css">
label {
	clear: both;
	display: block;
	float: left;
	width: 150px;
}
</style>
<form id="add" method="post" action="<?=$base_href?>/addMachine/">
	<input type="hidden" name="bid" value="<?=$bid?>" />
	<input type="hidden" name="cid" value="<?=$cid?>" />
	<label for="name">Machine Name</label>
	<span class="error"></span>
	<div><input type="text" size="35" id="name" name="name" value="" /></div>
	<label for="name">Parent</label>
	<span class="error"></span>
	<div>
		<select id="parent" name="parent">
			<option value="0">(No Parent)</option>
<?php foreach ($machines as $machine): ?>
			<option value="<?php echo $machine->client_programid; ?>"><?php echo $machine->name; ?></option>
<?php endforeach; ?>
		</select>
	</div>
<?php if (!$_isXHR): ?>
	<button type="submit" class="ui-widget ui-state-default ui-corner-all ui-button ui-button-text-icon-primary">
		<span class="ui-button-icon-primary ui-icon-circle-check" />
		<span class="ui-button-text">Save</span>
	</button>
	<button type="reset" class="ui-widget ui-state-default ui-corner-all ui-button ui-button-text-icon-primary">
		<span class="ui-button-icon-primary ui-icon-circle-close" />
		<span class="ui-button-text">Reset</span>
	</button>
<?php endif; ?>
</form>