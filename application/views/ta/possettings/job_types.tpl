<style type="text/css">
	label {
		display: inline-block;
		width: 200px;
	}
</style>
<div id="job_types">
	<form method="post" action="<?=$base_href?>/saveJobtypes">
		<input type="hidden" name="bid" value="<?=$bid?>" />
		<input type="hidden" name="cid" value="<?=$cid?>" />
		<input type="hidden" name="pid" value="<?php echo $client->client_programid; ?>" />
<?php foreach ($jobTypes as $job): ?>
		<div>
			<label for="jt<?php echo $job->posid; ?>"><?php echo $job->name; ?></label>
			<select id="jt<?php echo $job->posid; ?>" name="jobtype[<?php echo $job->id; ?>]">
				<option value="0"></option>
		<?php foreach ($btJobTypes as $btJob): ?>
				<option value="<?php echo $btJob->jobtypeid; ?>"<?php if ($job->localid == $btJob->jobtypeid) echo ' selected="selected"'; ?>><?php echo $btJob->name; ?></option>
		<?php endforeach; ?>
			</select>
		</div>
<?php endforeach; ?>
		<button type="submit" class="ui-button ui-button-text-icon-primary">
			<span class="ui-button-text">Save</span>
			<span class="ui-button-icon-primary ui-icon ui-icon-disk"></span>
		</button>
	</form>
</div>