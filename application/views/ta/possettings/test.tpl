<?php
//$db = Treat_Model_ClientPrograms::db();

/**
 * var_dump wrapper
 * @param string $name
 * @param mixed $value
 */
function dump($name, $value)
{
	echo "<h1>\$$name</h1>\n";
	var_dump($value);
}

$isAuthorized = TRUE;
$clientId = (int)$_REQUEST['clientId'];
//$client_programid = 25;
$clients = \Treat_Model_ClientPrograms::getClients($clientId, $isAuthorized);

//dump("Treat_Model_ClientPrograms::getClients(" . $clientId . ", " . $isAuthorized . ")\n", $clients);
$client = $clients[0];
/*
$query = "SELECT pos_type FROM mburris_manage.client_programs WHERE client_programid = " . $client_programid;
$stmnt = $db->query($query);
$pos_type_id = $stmnt->fetchColumn();
$pos_type = \Treat_Model_PosType::get("pos_type = $pos_type_id", TRUE);
*/
//$pos_type = $client->posType();
//dump("xdebug_get_headers()", xdebug_get_headers());

/*
dump("posType()", $client->posType());
dump("posType()->name", $client->posType()->name);
dump("client", $client);
*/

dump("session_object",$session_object);
dump("uri",$uri);
dump("bid",$bid);
dump("cid",$cid);
dump("clients",$clients);
dump("user",$user);

// go back to primry db connection
if (Treat_Config::singleton()->hasSection('db') !== false) {
	Treat_Model_Abstract::db(Treat_DB::singleton('db'));
}
?>
