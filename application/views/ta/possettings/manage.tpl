<style type="text/css">
	/*

tr {
	vertical-align: top;
	background-color: white;
}

thead tr {
	background-color: #eaeaea;
}

#main {
	width:90%;
}

#main #header {
	margin-bottom: 10px;
}

#main .ui-widget-header {
	padding: 0 10px;
}

#main table {
	margin-bottom: 10px;
}

#main td.left {
	width: 70%;
}

#main td.right {
	width: 30%;
}

#main thead {
	font-weight: bold;
	text-align: left;
}

#main thead#header, #main thead#header td {
	background-color: black;
	color: white;
}

#main thead.sub {
	background-color: white;
	font-style: italic;
}

ul {
	list-style: none;
	margin: 0;
	padding: 0;
}

li {
	color: black;
	display: inline;
	margin: 0;
	padding: 0;
}

li a {
	display:  block;
	float: left;
	margin: 2px;
	padding: 0 2px;
}

strong {
	color: black;
}

td.icons {
	width: 5%;
}

.right {
	text-align: right;
}
*/
</style>
<script type="text/javascript">
$(document).ready(function() {
	$('.tabs').tabs();
	$('#mapping_tabs').tabs();
	$('.datepicker').datepicker();
	$('table.client').addClass('ui-corner-all');
	$('form#schedule input.cancel').hide();
	$('form#details input.cancel').hide();

	$('a#delete').click(function (event) {
		event.preventDefault();

		if (confirm('Are you sure you want to delete this machine?')) {
			$('<form method="post" action="' + this.href + '?<?=$query_vars?>" />').submit();
		}
	});

	$('form#schedule :input, form#schedule :selected').each(function () {
		$(this).data('initalValue', $(this).val());
		$(this).keyup(function () {
			var isDirty = false;
			$('form#schedule :input, form#schedule :selected').each(function() {
				if ($(this).data('initalValue') != $(this).val()) {
					//alert($(this).data('initalValue')+' '+$(this).val());
					isDirty = true;
				}
			});

			if (isDirty) {
				$('form#schedule input.cancel:hidden').show();
			} else {
				$('form#schedule input.cancel:visible').hide();
			}
		});
	});

	$('form#details :input, form#details :selected').each(function () {
		$(this).data('initalValue', $(this).val());
		$(this).keyup(function () {
			var isDirty = false;
			$('form#details :input, form#details :selected').each(function() {
				if ($(this).data('initalValue') != $(this).val()) {
					//alert($(this).data('initalValue')+' '+$(this).val());
					isDirty = true;
				}
			});

			if (isDirty) {
				$('form#details input.cancel:hidden').show();
			} else {
				$('form#details input.cancel:visible').hide();
			}
		});
	});

	$('.edit').click(function () {
		var id = $(this).closest('tr').attr('id');
		if ($('form#schedule input#id').val()) {
			$('tr#'+$('form#schedule input#id').val()).removeClass('highlight');
		}
		$('tr#'+id).addClass('highlight');
		$.getJSON('<?=$base_href?>/getschedule?<?=$query_vars?>&id=' + id, function (data, reply) {
			$('form#schedule thead td').html('Edit Schedule');
			$('form#schedule input[type=submit]').attr('value', 'Save');
			$('form#schedule input.cancel').show();
			$('form#schedule input#id').attr('value', data.id);
			$('form#schedule input#default').attr('checked', ((data.isDefault == 1)? 'checked' : ''));
			$('form#schedule select#day option[value=\''+data.day+'\']').attr('selected', 'selected');
			$('form#schedule select#time_hour option[value="'+data.time_hour+'"]').attr('selected', 'selected');
			$('form#schedule select#time_min option[value="'+data.time_min+'"]').attr('selected', 'selected');
			$('form#schedule select#start_hour option[value="'+data.start_hour+'"]').attr('selected', 'selected');
			$('form#schedule select#start_min option[value="'+data.start_min+'"]').attr('selected', 'selected');
			$('form#schedule select#end_hour option[value="'+data.end_hour+'"]').attr('selected', 'selected');
			$('form#schedule select#end_min option[value="'+data.end_min+'"]').attr('selected', 'selected');
			$('form#schedule input#num_days').attr('value', data.days);
			$('form#schedule input#days_back').attr('value', data.days_back);
		});
	});

	$('.delete').click(function () {
		if (confirm('Are you sure you want to delete this schedule?')) {
			var pid = <?php echo $client->client_programid; ?>;
			var id = $(this).closest('tr').attr('id');
			window.location.href = '<?=$base_href?>/saveSchedule?<?=$query_vars?>&pid='+pid+'&del=1&id='+id;
		}
	});

	$('form#schedule input.cancel').click(resetScheduleForm);
	$('form#details input.cancel').click(resetDetailsForm);
});

function resetScheduleForm()
{
	var id = $('form#schedule input#id').attr('value');
	if (id) {
		$('tr#'+id).removeClass('highlight');
	}

	$('form#schedule thead td').html('Add Schedule');
	$('form#schedule input[type=submit]').attr('value', 'Add');
	$('form#schedule input.cancel').hide();
	$('form#schedule input#id').attr('value', '');
	$('form#schedule input#default').attr('checked', '');
	$('form#schedule select#day option[value="0"]').attr('selected', 'selected');
	$('form#schedule select#time_hour option[value="0"]').attr('selected', 'selected');
	$('form#schedule select#time_min option[value="00"]').attr('selected', 'selected');
	$('form#schedule select#start_hour option[value="0"]').attr('selected', 'selected');
	$('form#schedule select#start_min option[value="00"]').attr('selected', 'selected');
	$('form#schedule select#end_hour option[value="0"]').attr('selected', 'selected');
	$('form#schedule select#end_min option[value="00"]').attr('selected', 'selected');
	$('form#schedule input#num_days').attr('value', '');
	$('form#schedule input#days_back').attr('value', '');
}

function resetDetailsForm()
{
	$('form#details input.cancel').hide();
	$('form#details input#name').val($('form#details input#name').data('initalValue'));
	$('form#details select#parent option[value='+$('form#details select#parent').data('initalValue')+']').attr('selected', 'selected');
	$('form#details input#day_start').val($('form#details input#day_start').data('initalValue'));
	$('form#details select#pos_type option[value='+$('form#details select#pos_type').data('initalValue')+']').attr('selected', 'selected');
	$('form#details input#businessid').val($('form#details input#businessid').data('initalValue'));
	$('form#details input#db_ip').val($('form#details input#db_ip').data('initalValue'));
	$('form#details input#db_name').val($('form#details input#db_name').data('initalValue'));
	$('form#details input#db_user').val($('form#details input#db_user').data('initalValue'));
	$('form#details input#db_pass').val($('form#details input#db_pass').data('initalValue'));
}
</script>
<div id="main">
	<a href="<?=$base_href?>?<?=$query_vars?>" title="back to POS Settings main page">Back</a>
	<div id="header" class="ui-widget ui-widget-header ui-corner-all">
		<h2 style="float: right">Last Action: <?php echo $client->receiveStatus()->name.' '.$client->receiveTime(); ?></h2>
		<a id="delete" href="delete?<?=$query_vars?>&pid=<?php echo $client->client_programid; ?>" title="Delete Machine"><h2 class="ui-widget ui-icon ui-icon-circle-close" style="float: left; margin: 18px 10px;">Delete</h2></a>
		<h2>(<?php echo $client->client_programid; ?>) <?php echo $client->name; ?> <img src="/assets/images/<?php echo $client->statusImg(); ?>" /></h2>
		<?php if (!empty($client->client_ip)): ?>
		<br />
		<h3>IP Address: <?php echo $client->client_ip; ?></h3>
		<?php endif; ?>
	</div>
	<div class="tabs">
		<ul>
			<li><a href="#client">Schedule</a></li>
			<li><a href="#client_mapping">Mapping</a></li>
			<li><a href="#job_types">Job Types</a></li>
			<li><a href="#client_link">Credit/Debit Link</a></li>
			<li><a href="#client_details">Client Details</a></li>
			<li><a href="#client_errors">Client Errors</a></li>
		</ul>
		<div id="client">
			<table class="client" cellpadding="2" cellspacing="0">
				<thead>
					<tr>
						<th colspan="8">Existing Schedule(s)</td>
					</tr>
				</thead>
					<thead class="sub">
						<tr>
							<th>ID</th>
							<th>Day</th>
							<th>Time</th>
							<th>Start Time</th>
							<th>End time</th>
							<th># of Days</th>
							<th># of Days Back</th>
							<th class="icons">&nbsp;</th>
						</tr>
					</thead>
<?php
if (empty($schedules)) {
	echo '<tr><td colspan="8"><em>** No Schedules Defined</em></td></tr>';
} else {
	foreach ($schedules as $schedule):
?>
				<tr id="<?php echo $schedule->id; if ($schedule->default) echo '" title="Default Schedule'; ?>">
					<td><?php echo (($schedule->default == 1)? '*': '').$schedule->id; ?></td>
					<td><?php echo $schedule->dayToString(); ?></td>
					<td><?php echo $schedule->time; ?></td>
					<td><?php echo $schedule->starttime; ?></td>
					<td><?php echo $schedule->endtime; ?></td>
					<td><?php echo $schedule->num_days; ?></td>
					<td><?php echo $schedule->days_back; ?></td>
					<td class="icons">
						<img class="clickable edit" src="/assets/images/edit.gif" alt="Edit Schedule" title="Edit Schedule" />
						<img class="clickable delete" src="/assets/images/delete.gif" alt="Delete Schedule" title="Delete Schedule" />
					</td>
				</tr>
<?php
	endforeach;
}
?>
			</table>
			<form id="schedule" method="post" action="<?=$base_href?>/saveSchedule">
				<input type="hidden" name="bid" value="<?=$bid?>" />
				<input type="hidden" name="cid" value="<?=$cid?>" />
				<input type="hidden" id="pid" name="pid" value="<?php echo $client->client_programid; ?>" />
				<input type="hidden" id="id" name="id" value="" />
				<table cellpadding="2" cellspacing="0" class="client add_schedule">
					<thead>
						<tr>
							<th colspan="2">Add Schedule</th>
						</tr>
					</thead>
					<tr>
						<td class="right">Run:</td>
						<td>
							<select id="day" name="day">
								<option value="0" selected="selcted">Every Day</option>
								<option value="1">Mon</option>
								<option value="2">Tue</option>
								<option value="3">Wed</option>
								<option value="4">Thu</option>
								<option value="5">Fri</option>
								<option value="6">Sat</option>
								<option value="7">Sun</option>
							</select>
							at
							<select id="time_hour" name="time_hour">
<?php
for ($i = 0; $i < 24; $i++):
?>
								<option value="<?php echo $i; if ($i === 0) echo '" selected="selcted'; ?>"><?php echo $i; ?></option>
<?php
endfor;
?>
							</select>
							:
							<select id="time_min" name="time_min">
<?php
for ($i = 0; $i < 60; $i+=5):
?>
								<option value="<?php printf('%02d', $i); if ($i === 0) echo '" selected="selcted'; ?>"><?php printf('%02d', $i); ?></option>
<?php
endfor;
?>
							</select></td>
					</tr>
					<tr>
						<td class="right"><input id="default" name="default" type="checkbox" value="1" /></td>
						<td>Default Schedule?</td>
					</tr>
					<tr>
						<td class="right">Start Time:</td>
						<td>
							<select id="start_hour" name="start_hour">
<?php
for ($i = 0; $i < 24; $i++):
?>
								<option value="<?php echo $i; if ($i === 2) echo '" selected="selcted'; ?>"><?php echo $i; ?></option>
<?php
endfor;
?>
							</select>
							:
							<select id="start_min" name="start_min">
<?php
for ($i = 0; $i < 60; $i+=5):
?>
								<option value="<?php printf('%02d', $i); if ($i === 0) echo '" selected="selcted'; ?>"><?php printf('%02d', $i); ?></option>
<?php
endfor;
?>
							</select>
						</td>
					</tr>
					<tr>
						<td class="right">End Time:</td>
						<td>
							<select id="end_hour" name="end_hour">
<?php
for ($i = 0; $i < 24; $i++):
?>
								<option value="<?php echo $i; if ($i === 1) echo '" selected="selcted'; ?>"><?php echo $i; ?></option>
<?php
endfor;
?>
							</select>
							:
							<select id="end_min" name="end_min">
<?php
for ($i = 0; $i < 60; $i+=5):
?>
								<option value="<?php printf('%02d', $i); if ($i === 55) echo '" selected="selcted'; ?>"><?php printf('%02d', $i); ?></option>
<?php
endfor;
?>
						</select></td>
				</tr>
				<tr>
					<td class="right"># of Days:</td>
					<td><input id="num_days" name="num_days" type="text" size="8" value="" /></td>
				</tr>
				<tr>
					<td class="right"># of Days Back:</td>
					<td><input id="days_back" name="days_back" type="text" size="8" value="" /> (from today)</td>
				</tr>
				<tr>
					<td class="right" colspan="2">
						<input id="submit" type="submit" value="Add" title="Save Schedule" />
						<input class="cancel" type="button" value="Cancel" />
					</td>
				</tr>
			</table>
		</form>
	</div>
	<!-- Mapping section for credits/debits -->
	<?php include('mapping.tpl'); ?>
	<!-- Mapping for the job types -->
	<?php include('job_types.tpl'); ?>
		<div id="client_link">
			<form id="link" name="link" method="post" action="<?=$base_href?>/saveTransmapping">
				<input type="hidden" name="bid" value="<?=$bid?>" />
				<input type="hidden" name="cid" value="<?=$cid?>" />
				<input type="hidden" name="pid" value="<?php echo $client->client_programid; ?>" />
				<?php foreach ($transtypes as $type): ?>
				<label for="<?php echo $type->Id; ?>" style="float: left; display: block; width: 150px; clear: both;"><?php echo $type->Name; ?></label>
				<div>
					<span>Credit: </span>
					<select name="credit[<?php echo $type->Id; ?>]">
						<option value="">(None)</option>
					<?php foreach ($credits as $credit): ?>
						<option value="<?php echo $credit['creditid']; ?>"<?php if (isset($transmapping[$type->Id]) && $transmapping[$type->Id]->creditid == $credit['creditid']) echo ' selected="selected"'; ?>><?php echo $credit['creditname']; ?></option>
					<?php endforeach; ?>
					</select>
					<span>Debit: </span>
					<select name="debit[<?php echo $type->Id; ?>]">
						<option value="">(None)</option>
					<?php foreach ($debits as $debit): ?>
						<option value="<?php echo $debit['debitid']; ?>"<?php if (isset($transmapping[$type->Id]) && $transmapping[$type->Id]->debitid == $debit['debitid']) echo ' selected="selected"'; ?>><?php echo $debit['debitname']; ?></option>
					<?php endforeach; ?>
					</select>
				</div>
				<?php endforeach; ?>
				<br style="clear: both;" />
				<button type="submit" class="ui-button ui-widget ui-corner-all ui-state-default ui-button-text-icon-primary">
					<span class="ui-button-text">Save</span>
					<span class="ui-button-icon-primary ui-icon ui-icon-circle-check" />
				</button>
				<button type="reset" class="ui-button ui-widget ui-corner-all ui-state-default ui-button-text-icon-primary">
					<span class="ui-button-text">Reset</span>
					<span class="ui-button-icon-primary ui-icon ui-icon-circle-close" />
				</button>
			</form>
		</div>
		<div id="client_details">
			<form id="details" name="details" method="post" action="<?=$base_href?>/saveClient">
				<input type="hidden" name="bid" value="<?=$bid?>" />
				<input type="hidden" name="cid" value="<?=$cid?>" />
				<input type="hidden" name="pid" value="<?php echo $client->client_programid; ?>" />
				<table cellpadding="0" cellspacing="0" class="client">
					<thead>
						<tr>
							<th colspan="2">Client Details</th>
						</tr>
					</thead>
					<tr>
						<td class="right">Name:</td>
						<td><input type="text" id="name" name="name" value="<?php echo $client->name; ?>" /></td>
					</tr>
					<tr>
						<td class="right">Parent:</td>
						<td>
							<select id="parent" name="parent">
<?php
foreach ($clients as $parent):
?>
								<option value="<?php echo $parent->client_programid; if ($client->parent == $parent->client_programid) echo '" selected="selected'; ?>"><?php echo $parent->name; ?></option>
<?php
endforeach;
?>
							</select>
						</td>
					</tr>
					<tr>
						<td class="right">Day End/Start:</td>
						<td><input id="day_start" name="day_start" type="text" value="<?php echo $client->day_start; ?>" /></td>
					</tr>
					<tr>
						<td class="right">POS Type:</td>
						<td>
							<select id="pos_type" name="pos_type" onchange="document.getElementById('pos_mode_div').style.display = ( this.value == 7 ? 'inline-block' : 'none' );">
<?php
foreach ($posTypes as $posType):
?>
								<option value="<?php echo $posType->pos_type; if ($client->pos_type->pos_type == $posType->pos_type) echo '" selected="selected'; ?>"><?php echo $posType->name; ?></option>
<?php
endforeach;
?>
							</select>
							<div id='pos_mode_div' style="display: <?=$client->pos_type->pos_type == 7 ? 'inline-block' : 'none' ?>;">
							<label for='pos_mode' style='width: auto;'>Mode:</label>
							<select id="pos_mode" name="pos_mode">
								<option value=''>None</option>
								<?php foreach( $posModes as $mode ) : ?>
								<option value='<?=$mode->id?>' <?=$client->pos_mode == $mode->id ? 'selected="selected"' : ''?>><?=$mode->name?></option>
								<?php endforeach; ?>
							</select>
							</div>
						</td>
					</tr>
					<tr>
						<td class="right">Business ID:</td>
						<td><input id="businessid" name="businessid" type="text" value="<?php echo $client->businessid; ?>" /></td>
					</tr>
					<tr>
						<td class="right">DB IP:</td>
						<td><input id="db_ip" name="db_ip" type="text" value="<?php echo $client->db_ip; ?>" /></td>
					</tr>
					<tr>
						<td class="right">DB Name:</td>
						<td><input id="db_name" name="db_name" type="text" value="<?php echo $client->db_name; ?>" /></td>
					</tr>
					<tr>
						<td class="right">DB User:</td>
						<td><input id="db_user" name="db_user" type="text" value="<?php echo $client->db_user; ?>" /></td>
					</tr>
					<tr>
						<td class="right">DB Pass:</td>
						<td><input id="db_pass" name="db_pass" type="text" value="<?php echo $client->db_pass; ?>" /></td>
					</tr>
					<tr>
						<td class="right">Skip Client?</td>
						<td><input id="skip_client" name="skip_client" type="checkbox" value="1"<?php if ($client->canSkip()) echo ' checked="checked"'; ?> /></td>
					</tr>
					<tr>
						<td class="right" colspan="2">
							<input type="submit" value="Save" />
							<input class="cancel" type="button" value="Cancel" />
						</td>
					</tr>
				</table>
			</form>
		</div>
		<div id="client_errors">
			<div class="tabs">
				<ul>
<?php
for ($i = 1; $i <= $pages; $i++):
?>
					<li><a href="errors?<?=$query_vars?>&pid=<?php echo $client->client_programid.'/'.$i; ?>" title="errors-list"><?php echo $i; ?></a></li>
<?php
endfor;
?>
				</ul>
				<div id="errors-list">
				</div>
			</div>
		</div>
	</div>
</div>
