<script type="text/javascript">
$.strPad = function(i,l,s) {
	var o = i.toString();
	if (!s) { s = '0'; }
	while (o.length < l) {
		o = s + o;
	}
	return o;
};

function expand()
{
	var id = $(this).parent().parent().attr('id');
	$.getJSON('<?=$base_href?>/getChildren/?bid=<?=$bid?>&cid=<?=$cid?>pid=' + id, function (data) {
		$.each(data, function (i, item) {
			// ID for client
			var tr = $('<tr></tr>')
				.attr('bgcolor', '#eee')
				.mouseover(function(){$(this).attr('bgcolor', '#ccc')})
				.mouseout(function(){$(this).attr('bgcolor', '#eee')})
				.addClass('parent-'+id).attr('id', item[0]);

			var tdid = $('<td></td>').attr('width', '10%').html('&nbsp;'+item[0]);
			$(tr).append(tdid);

			// Name and expand if children
			var myParent = id;
			var tdname = $('<td></td>').attr('width', '30%').html('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
			do {
				if ($('tr#'+myParent).attr('class').indexOf('parent-') == -1) {
					myParent = null;
				} else {
					myParent = $('tr#'+myParent).attr('class').substr(7);
					$(tdname).append('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
				}
			} while(myParent != null);

			if (item[9]) {
				$(tdname).append($('<img />').attr('src', 'plus.png').addClass('click expand').bind('click', expand));
				$(tdname).append($('<span></span>').css('color', 'blue').html('&nbsp; ' + item[1]));
			} else {
				$(tdname).append($('<a></a>').attr('href', '<?=$base_href?>/manage/?bid=<?=$bid?>&cid=<?=$cid?>&pid='+item[0]).attr('name', item[0]).text(item[1]));
				if (item[12]) {
					$(tdname).append($('<span></span>').css({'font-size': '0.8em'}).html('&nbsp;IP: '+item[12]));
				}
			}

			$(tr).append(tdname);

			// POS Type
			var tdType = $('<td></td>').html('&nbsp;'+item[2]);
			$(tr).append(tdType);

			// Last Action
			if (item[4].length == 0 && !item[9]) item[4] = 'None';
			var tdLast = $('<td></td>').html('&nbsp;'+item[3]);
			if (!item[9]) tdLast.append($('<span></span>').addClass('recv_status').html(item[4]+'&nbsp;'+item[5]));
			$(tr).append(tdLast);

			// Actions
			var tdActions = $('<td></td>').html('&nbsp;');
			<?php if ($is_admin): ?>
			if (!item[9]) {
				$(tdActions).append("\n"+'<a target="_blank" href=<?=$base_href?>/data/'+item[0]+'><img src="/assets/images/folder.gif" border="0" height="16" width="16" alt="View Data" title="View Data"></a> <a href="update.php?pid='+item[0]+'"><img src="/assets/images/wrench.gif" border="0" height="17" width="16" alt="Send Updates" title="Send Updates"></a> <a href="update.php?pid='+item[0]+'&action=1"><img src="/assets/images/calendar.gif" border="0" height="16" width="16" alt="Request Data" title="Request Data"></a> <a href="update.php?pid=' +item[0]+ '&action=2"><img src="/assets/images/clear.gif" border="0" height="16" width="16" alt="Cancel Request" title="Cancel Request"></a>');
				if (item[11]) {
					$(tdActions).append("\n"+'<a href="<?=$base_href?>/restartKiosk/?bid=<?=$bid?>&cid=<?=$cid?>&pid='+item[0]+'&action=3"><img src="/assets/images/restart.png" height="16" width="16" alt="Restart Kiosk" title="Restart Kiosk" /></a>');
				}
				$(tdActions).append($('<img />').addClass('errors click').css('display', ((item[10] == '1')? 'inline' : 'none')).attr('src', '/assets/images/error.gif').bind('click', showErrors));
			}
			<?php endif; ?>
			$(tr).append(tdActions);

			// Status
			var tdStatus = $('<td></td>').attr('align','right').html('');
			if (!item[9]) {
				$(tdStatus)
					.append($('<span></span>').addClass('timer').text(item[6]))
					.append($('<img />').attr('src', item[7]).addClass('status').attr('height', 16).attr('width', 16))
					.append(item[8]).append('&nbsp;');
			}
			$(tr).append(tdStatus);

			$('tr#'+id).after(tr);
			$('tr#'+item[0]+' img.expand').click();
		});
	});

	this.src = 'minus.png';
	$(this).unbind('click', expand);
	$(this).bind('click', collapse);
}

function collapse()
{
	var id  = $(this).parent().parent().attr('id');
	$('tr.parent-'+id).each(removeChildren);
	this.src = 'plus.png';
	$(this).unbind('click', collapse);
	$(this).bind('click',expand);
}

function removeChildren()
{
	var id = $(this).attr('id');

	$('tr.parent-'+id).each(removeChildren);
	$(this).remove();
}

function showErrors()
{
	var pid = $(this).parent().parent().attr('id');
	//$.getJSON('ajax/getErrors/' + pid, function (data) {
	$.getJSON('<?=$base_href?>/getErrors/?bid=<?=$bid?>&cid=<?=$cid?>&pid=' + pid, function (data) {
		var errorHtml = '<table width="100%" cellspacing="0"><tr><th width="180">Date/Time</th><th>Message</th><th>Stack Trace</th></tr>';
		$.each(data, function (i, item) {
			errorHtml += '<tr><td valign="top">' + item[0] + '</td><td valign="top">' + item[1] + '</td><td valign="top">' + item[2] + '</td></tr>';
		});
		errorHtml += '</table>';
		$('<div></div>').html(errorHtml).dialog({
			buttons: {
				'OK': function () { $(this).dialog("close"); }
			},
			dialogbeforeclose: function(event, ui) {
				$('tr#' + pid + ' img.errors').hide();
			},
			height: 400,
			title: 'Client Errors',
			width: 800
		});
	});
}

function updateTimer(i)
{
	$('span.timer').each( function () {
		var diff = $(this).attr('id');
		var seconds = 0;
		var minutes = 0;
		var hours = 0;

		if (diff.length == 0) {
			diff = $(this).html();
			if (diff.length == 0) return;
			diff = diff.split(':');
			seconds = (((Number(diff[0]) * 60) + Number(diff[1]) * 1) * 60) + diff[2] * 1;
		} else {
			seconds = Number(diff);
		}

		seconds++;
		$(this).attr('id', seconds);

		while (seconds >= 60) {
			seconds = seconds - 60;
			minutes++;
		}

		while (minutes >= 60) {
			minutes = minutes - 60;
			hours++;
		}

		$(this).text($.strPad(hours, 2) + ':' + $.strPad(minutes, 2) + ':' + $.strPad(seconds, 2));
	});
}

$(document).ready(function () {
	$('img.expand').bind('click', expand);
	$('img.expand').each(function() {
		if ($(this).parent().parent().attr('id') <?php echo (IN_PRODUCTION)? '!=' : '=='; ?> 22) {
			$(this).trigger('click');
		}
	});
	$(document).everyTime(1000, updateTimer);
	$(document).everyTime(10000, function(i) {
		$.getJSON('<?=$base_href?>/updateTimers/?bid=<?=$bid?>&cid=<?=$cid?>', function (data) {
			$.each(data, function (x, item) {
				if ($(this).find('#parent-'+item[0])[0] == undefined) {
					var element = $('#'+item[0]+' span.timer');
					$(element).attr('id', '');
					$(element).text(item[1]);
					$('#'+item[0]+' span.recv_status').text(item[2] + ' ' + item[3]);
					$('#'+item[0]+' img.status').attr('src', item[4]);
					$('#'+item[0]+' img.current').attr('src', item[5]);
					if (item[6] == '1') {
						$('#' + item[0] + ' img.errors').show();
					} else {
						$('#' + item[0] + ' img.errors').hide();
					}
				}
			});
		});
	});
	$('#add-machine').click(function (e) {
		e.preventDefault();
		$.get('<?=$base_href?>/addMachine/?bid=<?=$bid?>&cid=<?=$cid?>', function (html) {
			$('<div id="add-dialog" />').html(html).dialog({
				buttons: {
					'Save': function () { $('form#add').submit(); },
					'Cancel': function () { $('#add-dialog').dialog('destroy'); }
				},
				modal: true,
				width: '400px',
				title: 'Add Machine'
			});
		});
	});
});
</script>
<div>
	<button style="margin: 5px; padding: 5px 10px;" class="ui-button ui-widget ui-corner-all ui-state-default ui-button-text-icon-primary" id="add-machine">
		<span class="ui-button-text">Add New Machine</span>
		<span class="ui-button-icon-primary ui-icon ui-icon-plusthick">
		</span>
	</button>
	<br /><br />
</div>
<table cellspacing="0" cellpadding="0" class="client ui-corner-all">
	<thead>
		<th width=10%>
			&nbsp;<strong>Program ID</strong>
		</th>
		<th width=30%>
			&nbsp;<strong>Client</strong>
		</th>
		<th width=20%>
			&nbsp;<strong>POS Type</strong>
		</th>
		<th width=20%>
			&nbsp;<strong>Last Action</strong>
		</th>
		<th width=10%>
			&nbsp;<strong>New Action</strong>
		</th>
		<th align=right width=10%>
			<strong>Status</strong>&nbsp;
		</th>
	</thead>
<?php
foreach ($clients as $client):
	$newfile = $client->client_programid;
	$newfile.=".old.txt";
?>
	<tr onMouseOver="this.bgColor='#ccc'" onMouseOut="this.bgColor='white'" id="<?php echo $client->client_programid; ?>">
		<td>&nbsp;<?php echo $client->client_programid; ?></td>
		<td><?php if ($client->hasChildren()) {
			echo '&nbsp;<img src="/assets/images/plus.png" class="expand click">';
		} ?>
			&nbsp;&nbsp;<?php if ($is_admin && !$client->hasChildren()): ?><a name="<?php echo $client->client_programid; ?>" href="<?=$base_href?>/manage/?bid=<?=$bid?>&cid=<?=$cid?>&pid=<?php echo $client->client_programid; ?>" style=""><?php endif; ?><font color=blue><?php echo $client->name; ?></font></a>
			<?php if ($is_admin && !$client->hasChildren() && !empty($client->client_ip)): ?><span style="font-size: 0.8em;">&nbsp;IP: <?php echo $client->client_ip; ?></span><?php endif; ?>
		</td>
		<td>&nbsp;<?php echo $client->posType()->name; ?></td>
		<td>&nbsp;<?php if (!$client->hasChildren()): echo $client->showSchedule(); ?> <span class="recv_status"><?php echo (empty($client->receiveStatus()->name))? 'None' : $client->receiveStatus()->name,' ',$client->receiveTime(); ?></span><?php endif; ?></td>
		<td>
			&nbsp;
			<?php if ($is_admin && !$client->hasChildren()): ?>
			<a target='_blank' href="<?=$base_href?>/data/<?php echo $client->client_programid; ?>"><img src="/assets/images/folder.gif" border="0" height="16" width="16" alt="View Data" title="View Data"></a>
			<a href="<?=$base_href?>/sendUpdates/?bid=<?=$bid?>&cid=<?=$cid?>&pid=<?php echo $client->client_programid; ?>"><img src="/assets/images/wrench.gif" border="0" height="17" width="16" alt="Send Updates" title="Send Updates"></a>
			<a href="<?=$base_href?>/requestData/?bid=<?=$bid?>&cid=<?=$cid?>&pid=<?php echo $client->client_programid; ?>&action=1"><img src="/assets/images/calendar.gif" border="0" height="16" width="16" alt="Request Data" title="Request Data"></a>
			<a href="<?=$base_href?>/cancelRequest/?bid=<?=$bid?>&cid=<?=$cid?>&pid=<?php echo $client->client_programid; ?>&action=2"><img src="/assets/images/clear.gif" border="0" height="16" width="16" alt="Cancel Request" title="Cancel Request"></a>
			<?php if ($client->isKiosk()): ?>
			<a href="<?=$base_href?>/restartKiosk/?bid=<?=$bid?>&cid=<?=$cid?>&pid=<?php echo $client->client_programid; ?>&action=3"><img src="/assets/images/restart.png" height="16" width="16" alt="Restart Kiosk" title="Restart Kiosk" /></a>
			<?php endif; ?>
			<img class="errors click" style="display: <?php echo ($client->hasErrors())? 'inline' : 'none'; ?>;" src="/assets/images/error.gif" border="0" height="16" width="16" alt="View Errors" title="View Errors" />
			<?php endif; ?>
		</td>
		<td align=right><?php if(!$client->hasChildren()): ?><span class="timer"><?php echo $client->timeDiff(); ?></span><img class="status" src="/assets/images/<?php echo $client->statusImg(); ?>" border="0" height="16" width="16"><img class="current" src="/assets/images/<?php echo $client->showSendStatus2(); ?>" border="0" height="16" width="16"><?php endif; ?>&nbsp;</td>
	</tr>
	<tr><td colspan="7" height="2" style="background-color: black;"></td></tr>
<?php endforeach; ?>
</table>