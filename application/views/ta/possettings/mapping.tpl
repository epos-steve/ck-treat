	<div id="client_mapping">
			<form method="post" action="<?=$base_href?>/possettings/saveMapping">
				<input type="hidden" name="bid" value="<?=$bid?>" />
				<input type="hidden" name="cid" value="<?=$cid?>" />
				<input type="hidden" name="pid" value="<?php echo $client->client_programid; ?>" />
				<div class="ui-widget ui-widget-header ui-corner-all">
					<h3 style="float: right;"><input type="submit" value="Save" /></h3>
					<h3>Mapping - <?php echo $client->name; ?> <a href="/receive/data/<?php echo $client->client_programid; ?>.old.txt" target="_blank"><img src="/assets/images/folder.gif" alt="View Data File" title="View Data File" /></a></h3>
				</div>
				<div class="tabs">
					<ul>
<?php
foreach($mappingTypes as $mapType):
?>
						<li><a href="#mapping_<?php echo $mapType->id; ?>"><?php echo $mapType->name; ?></a></li>
<?php
endforeach;
?>
					</ul>
<?php
foreach($mappingTypes as $mapType):
?>
					<div id="mapping_<?php echo $mapType->id; ?>">
						<table class="client ui-corner-all">
							<thead>
								<th width="45%">Name</th>
								<th>&nbsp;</th>
								<th width="25%">Credits</th>
								<th width="25%">Debits</th>
							</thead>
<?php
	foreach ($mapType->getMappingByClient($client) as $map):
?>
							<tr>
								<td width="45%">(<?php echo $map->posid.') '.$map->name; ?></td>
								<td>
									<a href="<?=$base_href?>/addMapping?<?= $query_vars ?>&pid=<?php echo $client->client_programid; ?>&id=<?php echo $map->id; ?>&do=1" title="Add Mapping"><img src="/assets/images/plus.gif" alt="Add Mapping" height="16" width="16" border="0" /></a>
									<a href="<?=$base_href?>/removeMapping?<?=$query_vars?>&pid=<?php echo $client->client_programid; ?>&id=<?php echo $map->id; ?>&do=2" title="Delete Mapping" onclick="javascript:return confirm('Are you sure?');"><img src="/assets/images/delete.gif" alt="Delete Mapping" height="16" width="16" border="0"></a>
<?php if($map->localid == 0): ?>
									<img src="/assets/images/error.gif" height="16" width="16">
<?php endif; ?>
								</td>
								<td width="25%">
									<select name="credit[<?php echo $map->id; ?>]">
										<option value="0">(None)</option>
<?php foreach ($credits as $credit): ?>
										<option value="<?php echo $credit['creditid']; if($map->localid == $credit['creditid'] && $map->tbl->id == 1) echo '" selected="selected'; ?>"><?php echo $credit['creditname']; ?> (<?php echo $credit['creditid']; ?>)</option>
<?php endforeach; ?>
								</td>
								<td width="25%">
									<select name="debit[<?php echo $map->id; ?>]">
										<option value="0">(None)</option>
<?php foreach ($debits as $debit): ?>
										<option value="<?php echo $debit['debitid']; if($map->localid == $debit['debitid'] && $map->tbl->id == 2) echo '" selected="selected'; ?>"><?php echo $debit['debitname']; ?> (<?php echo $debit['debitid']; ?>)</option>
<?php endforeach; ?>
									</select>
								</td>
							</tr>
<?php
	endforeach;
?>
						</table>
					</div>
<?php
endforeach;
?>
			</div>
		</form>
	</div>