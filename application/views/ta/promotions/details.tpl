<?php
		$promo_trigger = $promotion->promo_trigger->id;
		$bid = $promotion->businessid;

		if ($promotion->promo_trigger->target_id == Treat_Model_Promotion::TARGET_ITEM)
		{
			$db = Treat_Model_Promotion::db();
			$promo_id = $promotion->id;
			$items = $promotion->getItemsByCategory();
			
			// find out which items have been checked
			$items_checked = Array();
			$query = "SELECT id FROM promo_detail WHERE promo_id = $promo_id AND type = "
			. Treat_Model_Promotion::DETAIL_TYPE_ITEM;
			$result = $db->query($query);
			foreach ($result as $row) {
				$items_checked[$row['id']] = TRUE;
			}
			
			echo "<div class='settingsTable' id='promoItems'>\n";
			echo "<form id='promoItemForm' onsubmit='return false;'>\n";
			for ($c = 0; $c < count($items); $c++) {
				$column_num = ($c % 3) + 1;
				//echo "<div class='settingsRow settingsColumn'>\n";
				if ($column_num == 1) {
					echo "<div class='settingsRow'>\n";
				}
				echo "<div class='settingsThird'>\n";
				echo "\t<div class='settingsHeader ui-state-default'>";
				echo "<a href='javascript:' onclick='jQuery(\"#category" . $items[$c]['category_id'] . "\").toggle();'>" . $items[$c]['category_name'] . "</a>";
				echo "</div>\n";
				echo "\t<div class='settingsForm' id='category{$items[$c]['category_id']}'>\n";
				foreach ($items[$c]['category_items'] as $item) {
					$id = $item['id'];
					$name = $item['name'];
					$upc = $item['upc'];
					$is_checked = ($items_checked[$id]) ? "CHECKED" : NULL;
					echo "\t\t<div class='settingsItem'><input type=checkbox class='promoItem' name=mid$id id='mid$id' value='1' $is_checked /><label for='mid$id'>$name</label></div>\n";
				}
				echo "\t</div>\n</div>\n";
				if ($column_num == 3) {
					echo "</div>\n";
				}
			}
				echo "</form>\n";
				echo "</div>\n\n";
		} elseif( $promotion->promo_trigger->target_id  == Treat_Model_Promotion_Triggers::TARGET_SIMPLE_GROUP ) {
		
			$query = "SELECT * FROM menu_groups_new WHERE businessid = $bid ORDER BY name";
			$result = Treat_DB_ProxyOld::query( $query );

			//echo "<table width=100% style=\"border:2px solid black;\" cellspacing=0 cellpadding=0><tr><td>";
			//echo "<div style=\"background-color:#999999;width:100%;font-weight:bold;border-bottom:2px solid black;color:white;\">&nbsp;Menu Groups</div>";
			echo '<div class="settingsContainer"><div class="settingsHeader ui-state-default">Menu Groups</div><div class="settingsForm">';

			if( $result ) {
				while( $r = mysql_fetch_array( $result ) ) {
					$id = $r["pos_id"];
					$name = $r["name"];

					$query2 = "SELECT * FROM promo_detail WHERE promo_id = {$promotion->id} AND type = 2 AND id = $id";
					$result2 = Treat_DB_ProxyOld::query( $query2 );
					$num2 = Treat_DB_ProxyOld::mysql_numrows( $result2, 0 );

					if( $num2 == 1 ) {
						$val = "CHECKED";
					}
					else {
						$val = "";
					}

					echo "<div class='settingsItem settingsFloat'><input type=checkbox name=gid$id id='gid$id' value=1 onclick=\"promo_detail({$promotion->id},2,$id);\" $val /><label for='gid$id'>$name</label></div>";
				}
			}

			//echo "</td></tr></table>";
			echo '</div></div>';
		}
		elseif( $promotion->promo_trigger->target_id == Treat_Model_Promotion_Triggers::TARGET_ALL_PRODUCTS ) {
		
			echo '<div class="settingsContainer"><div class="settingsHeader ui-state-default">All Products Selected</div></div>';
			return false;
		}
		elseif( $promotion->promo_trigger->target_id == Treat_Model_Promotion_Triggers::TARGET_SIMPLE_COMBO ) {
		
			$query = "SELECT * FROM menu_groups_new WHERE businessid = $bid ORDER BY name";
			$result = Treat_DB_ProxyOld::query( $query );

			//echo "<table width=100% style=\"border:2px solid black;\" cellspacing=0 cellpadding=0><tr><td>";
			//echo "<div style=\"background-color:#999999;width:100%;font-weight:bold;border-bottom:2px solid black;color:white;\">&nbsp;Menu Groups</div>";
			echo '<div class="settingsContainer"><div class="settingsHeader ui-state-default">Menu Groups</div><div class="settingsForm">';

			$selectedOptions = array( );
			$query2 = "SELECT * FROM promo_detail WHERE promo_id = {$promotion->id} AND ( type = 31 OR type = 32 OR type = 33 )";
			$result2 = Treat_DB_ProxyOld::query( $query2 );
			while( $r = mysql_fetch_assoc( $result2 ) ) {
				$selectedOptions[$r['type']] = $r['id'];
			}

			$menuGroupOptions = '<option value="">(none)</option>';
			while( $r = mysql_fetch_array( $result ) ) {
				$id = $r["pos_id"];
				$name = $r["name"];

				$menuGroupOptions .= "<option value='$id'>$name</option>";
			}

			echo '<label for="promo_group_1">1:</label> <select class="promoOption" name="promo_group_1" id="promo_group_1" onchange="promoDetailClear( '
					. $promotion->id . ', 31, this );">' . $menuGroupOptions . '</select>';
			echo '<label for="promo_group_2">2:</label> <select class="promoOption" name="promo_group_2" id="promo_group_2" onchange="promoDetailClear( '
					. $promotion->id . ', 32, this );">' . $menuGroupOptions . '</select>';
			echo '<label for="promo_group_3">3:</label> <select class="promoOption" name="promo_group_3" id="promo_group_3" onchange="promoDetailClear( '
					. $promotion->id . ', 33, this );">' . $menuGroupOptions . '</select>';
			echo '<script type="text/javascript" language="javascript">';
			echo 'jQuery(function(){';
			if( !empty( $selectedOptions[31] ) )
				echo 'jQuery("#promo_group_1 option[value=' . $selectedOptions[31]
						. ']").attr("selected","selected");';
			if( !empty( $selectedOptions[32] ) )
				echo 'jQuery("#promo_group_2 option[value=' . $selectedOptions[32]
						. ']").attr("selected","selected");';
			if( !empty( $selectedOptions[33] ) )
				echo 'jQuery("#promo_group_3 option[value=' . $selectedOptions[33]
						. ']").attr("selected","selected");';
			echo '});';
			echo '</script>';
			//echo "</td></tr></table>";
			echo '</div></div>';
		}
		elseif( $promotion->promo_trigger->target_id == Treat_Model_Promotion_Triggers::TARGET_COMBO_GROUP ) {
		
                        if ($promotion->promo_trigger->id == Treat_Model_Promotion_Triggers::TRIGGER_SINGLE_COMBO_GROUP) {
                                $multi = FALSE;
                                $plural = '';
                        } else {
                                $multi = TRUE;
                                $plural = 's';
                        }
			echo '<div class="settingsContainer"><div class="settingsHeader ui-state-default">Combo Group' . $plural . '</div><div class="settingsForm">';

			$menuGroupOptions = '<option value="">(none)</option>';
			foreach ($groups as $group) {
				$id = $group->pos_id ?: $group->id;
				$name = $group->name;

				$menuGroupOptions .= "<option value='$id'>$name</option>";
			}
?>

<?php if ($promo_group_types): ?>
<form class='settingsDetailForm' id='comboDetailsForm' onsubmit='return false;' method='post'>
<table class="settingsHeaderTable">
    <tr>
        <th>Num#</th>
        <th>Group</th>
        <th>Discount</th>
        <th>Apply Discount</th>
        <th>Row/Column Size</th>
        <th>Color</th>
        <th>Remove</th>
        <th>Order</th>
    </tr>
<?php foreach ($promo_group_types as $key=>$pgt): ?>
    <tr id="mainPromoGroup_<?php echo $key+1 ?>" class="mainPromoGroup">
        <td><label for="combo_group_<?php echo $key+1 ?>"><?php echo $key+1 ?></label></td>
        <td>
			<select class="promoOption" name="combo_group_<?php echo $key+1 ?>" id="combo_group_<?php echo $key+1 ?>">
                <?php echo $menuGroupOptions; ?>
			</select>
		</td>
        <td><span class='comboGroupOption promo_amount_off'>
                <input class='comboOption' type='text' name='combo_item_discount_<?php echo $key+1 ?>'
                       id='combo_item_discount_<?php echo $key+1 ?>' value='' size=6 />
        </span></td>
        <td><span class='comboGroupOption promo_fixed_value'>
                <input class='comboOptionApplyDiscount' type='checkbox'
                       name='combo_item_apply_discount_<?php echo $key+1 ?>'
                       id='combo_item_apply_discount_<?php echo $key+1 ?>' value='1' />
        </span></td>
        <td><input type="text" name="rows_cols_<?php echo $key+1 ?>" id="rows_cols_<?php echo $key+1 ?>"
               value="" size="2">
        <select name="rows_cols_type_<?php echo $key+1 ?>" id="rows_cols_type_<?php echo $key+1 ?>">
            <option value="2">Columns</option>
            <option value="1">Rows</option>
        </select></td>
        <td><select name='combo_pos_color_<?php echo $key+1 ?>' id='combo_pos_color_<?php echo $key+1 ?>'
                alt='Button Color' class='picker validate-type-color picker-prefix-product-button-color-'>
            <?php for( $i = 0; $i < 12; $i++ ) { ?>
                <option value="<?=$i?>"><?=$i?></option>
            <?php } ?>
        </select></td>
        <td><span class='RemoveComboGroupButton'>Remove Combo Group</span></td>
        <td>
			<span class='ui-state-default ui-button ui-corner-all ui-state-default ui-button-icon-primary '>
				<span class="ui-icon ui-icon-transferthick-e-w" id="btnOrderGroup" value='<?php echo $key+1 ?>'></span>
			</span>
		</td>
    </tr>
<?php endforeach ?>





<div style="padding-top: 5px;">
    <span id='AddComboGroupButton'>Add Combo Group</span>
    <span id='RefreshComboGroupButton'>Refresh/Undo Combo Group Selection</span>
</div>
</form>



<script type="text/javascript" language="javascript">



	jQuery(function(){


	jQuery( "#table1" ).html('' );
	jQuery( "#tblOrderGroup" ).hide();
	

		
<?php foreach ($promo_group_types as $key=>$pgt): ?>
        jQuery( "#combo_pos_color_<?=$key+1?>" ).picker( );

<?php if( !empty( $selectedOptions[$pgt] ) ): ?>
		jQuery("#combo_group_<?=$key+1?>").val("<?php echo $selectedOptions[$pgt]; ?>");

    <?php if($selected_options_combos[$pgt]['adis'] == 1): ?>
        jQuery( "#combo_item_apply_discount_<?=$key+1?>" ).attr('checked', true);
    <?php endif; ?>
    <?php if($selected_options_combos[$pgt]['idis']): ?>
        jQuery( "#combo_item_discount_<?=$key+1?>" ).val("<?php echo $selected_options_combos[$pgt]['idis'] ?>");
    <?php endif; ?>

        jQuery( "#rows_cols_<?=$key+1?>" ).val("<?php echo $selected_options_combos[$pgt]['rows_cols'] ?>");
        jQuery( "#rows_cols_type_<?=$key+1?>" ).val("<?php echo $selected_options_combos[$pgt]['rows_cols_type'] ?>");
        jQuery( "#combo_pos_color_<?=$key+1?>" ).val("<?php echo $selected_options_combos[$pgt]['pos_color'] ?>").change();
<?php else: ?>
        <?php if ($key > 1): ?>
        jQuery( "#mainPromoGroup_<?=$key+1?>" ).hide();
        <?php endif; ?>
<?php endif; ?>
<?php endforeach ?>
	});

</script>

<?php else: ?>
<label for="promo_group_1">1:</label> <select class="promoOption" name="promo_group_1" id="promo_group_1" onchange="promoDetailClear( <?php echo $promotion->id; ?>, 51, this );"><?php echo $menuGroupOptions; ?></select>
<?php if ($multi) { ?>
<label for="promo_group_2">2:</label> <select class="promoOption" name="promo_group_2" id="promo_group_2" onchange="promoDetailClear( <?php echo $promotion->id; ?>, 52, this );"><?php echo $menuGroupOptions; ?></select>
<label for="promo_group_3">3:</label> <select class="promoOption" name="promo_group_3" id="promo_group_3" onchange="promoDetailClear( <?php echo $promotion->id; ?>, 53, this );"><?php echo $menuGroupOptions; ?></select>
<?php } ?>
<script type="text/javascript" language="javascript">
	jQuery(function(){
<?php if( !empty( $selectedOptions[51] ) ): ?>
		jQuery("#promo_group_1 option[value=<?php echo $selectedOptions[51]; ?>]").attr("selected","selected");
<?php endif;
if( !empty( $selectedOptions[52] ) && $multi ): ?>
		jQuery("#promo_group_2 option[value=<?php echo $selectedOptions[52]; ?>]").attr("selected","selected");
<?php endif;
if( !empty( $selectedOptions[53] ) && $multi ): ?>
		jQuery("#promo_group_3 option[value=<?php echo $selectedOptions[53]; ?>]").attr("selected","selected");
<?php endif; ?>
	});
</script>
</div>
</div>
<?php endif ?>
<?php 	} ?>
