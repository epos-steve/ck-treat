	<script language="javascript" type="text/javascript">
		jQuery(function(){
			jQuery('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' });

			jQuery('#schedule_form').submit(function(){
				var formData = jQuery(this).serialize( );
				jQuery.ajax({
					type: "POST",
					dataType: 'json',
					url: "/ta/promotions/updateSchedule/"+jQuery('input[name=promo_id]').val()+'/'+jQuery('input[name=businessid]').val(),
					data: formData,
					success: function(data){
						if( data.error ) {
							alert( data.error );
						}
					}
				});

				return false;
			});
		});
	</script>
	<?php

		//echo "<table width=100% style=\"border:1px solid black;\" cellspacing=0 cellpadding=0>";
		//echo "<tr><td style=\"border:1px solid black;\"><div style=\"background-color:#999999;width:100%;font-weight:bold;color:white;\">&nbsp;Schedule</div></td></tr>";
?>
	<div class="settingsContainer">
		<div class="settingsHeader ui-state-default">Schedule</div>
<?php
		//echo "<tr><td style=\"border:1px solid black;\">
		//			&nbsp;<form id='schedule_form' onsubmit='return false;' style=\"margin:0;padding:0;display:inline;\">
?>
		<form class="settingsForm" onsubmit="return false;" id="schedule_form">

		<input style='float: right;' type='submit' value='Update Schedule' id='promo_schedule_submit' />

		<input type='hidden' name='promo_id' value='<?php echo $promo_id; ?>' />
		<input type='hidden' name='businessid' value='<?php echo $businessid; ?>' />
		Start Date: <input type=text name='start_date' id='start_date' value="<?php echo $start_date; ?>" size=10 class="datepicker" />
		Start Time: <select class='settingsNospace' name="start_hour" id='start_hour'>
<?php
		foreach ( $hours_list as $hour ) :
			$_startHour = $start_time[0] ?: 12;
			if( $_startHour > 12 ) {
				$_startHour -= 12;
			}
			$sel = '';
			if( $_startHour == (int) $hour ) {
				$sel = ' selected';
			}
			?>
		<option value="<?php echo $hour; ?>"<?php echo $sel; ?>><?php echo $hour; ?></option>
<?php endforeach; ?>

		</select>:<select class="settingsNospace" name="start_min" id='start_min'>

<?php
		foreach ( $minutes_list as $_minute ) :
			$min = $start_time[1] ?: '00';
			$sel = '';
			if( (int)$_minute == $min ) {
				$sel = ' selected';
			}
			?>
			<option value="<?php echo $_minute; ?>"<?php echo $sel; ?>><?php echo $_minute; ?></option>
<?php endforeach; ?>
		</select>
		
		<select name=start_ampm id='start_ampm'>
			<option value="am">AM</option>
			<option value="pm"<?php if( $start_time[0] > 11 ) : ?> selected<?php endif; ?>>PM</option>
		</select>
		
		<br />
		End Date: <input type="text" name="end_date" id="end_date" value="<?php echo $end_date; ?>" size="10" class="datepicker" />
		End Time: <select class="settingsNospace" name="end_hour" id="end_hour">
<?php
		foreach ( $hours_list as $hour ) :
			$_endHour = $end_time[0] ?: 12;
			if( $_endHour > 12 ) {
				$_endHour -= 12;
			}
			$sel = '';
			if( $_endHour == (int) $hour ) {
				$sel = ' selected';
			}
			?>
		<option value="<?php echo $hour; ?>"<?php echo $sel; ?>><?php echo $hour; ?></option>
<?php endforeach; ?>

		</select>:<select class="settingsNospace" name="end_min" id="end_min">

<?php
		foreach ( $minutes_list as $_minute ) :
			$min = $end_time[1] ?: '00';
			$sel = '';
			if( (int)$_minute == $min ) {
				$sel = ' selected';
			}
			?>
			<option value="<?php echo $_minute; ?>"<?php echo $sel; ?>><?php echo $_minute; ?></option>
<?php endforeach; ?>
		</select>
		
		<select name=end_ampm id='end_ampm'>
			<option value="am">AM</option>
			<option value="pm"<?php if( $end_time[0] > 11 ) : ?> selected<?php endif; ?>>PM</option>
		</select>
		
		<br>

		<span>Available Days:</span>

<?php	foreach( $daysArray as $sday => $day ) : ?>
		<div class='settingsItem'>
		<input type="checkbox" name="<?php echo $day['name']; ?>" id="<?php echo $sday; ?>" value="1"<?php echo $day['show']; ?> />
		<label for='<?php echo $sday; ?>'><?php echo ucfirst( $day['name'] ); ?></label></div>
<?php	endforeach; ?>

	</form>
</div>
