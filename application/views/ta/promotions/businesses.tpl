<script type="text/javascript">
// Handle jQuery tree posting
jQuery(function(){
	// Initialize the tree in the onload event
	jQuery("#businessMappingTree").dynatree({
		checkbox:       true,
		selectMode:     3,
		activeVisible:  true,
		onSelect: function(select, node) {
			var selKeys = jQuery.map(node.tree.getSelectedNodes(), function(node) {
				return node.data.key;
			});

			// Query the tree for enabled nodes and form a proper POST request
			var promo_id = jQuery('#promo_id').val();
			var sdata = 'promo_id=' + promo_id + '&xhr=1';
			jQuery.each(selKeys, function(key, value) {
					var barpos = value.indexOf("_");
					var type = value.substr(0,barpos);
					var id = value.substr(barpos+1);
					sdata += '&' + type + '[]=' + id;
			});

			jQuery.post(
				'/ta/promotions/updateBusinesses/' + promo_id + '/',
				encodeURI(sdata),
				function(response, textStatus, xhr){
				}
			);
			return false;
		}
	});
	
	jQuery(".js-update-businesses").live('click', function(event) {
		event.preventDefault();
		var promo_id = jQuery('#promo_id').val();
		var businessid = jQuery( '#promoForm input[name=bid]' ).val();
		$.get($(this).attr('href') + promo_id, function() {
			jQuery( '#promo_businesses' ).html('<img src="/assets/images/ajax-loader-redmond-big.gif">');
			jQuery( '#promo_businesses' ).load( '/ta/promotions/businesses/' + promo_id + '/' + businessid);
		});
	});
});

</script>
<div id="promo_businesses">
	<div class="settingsContainer">
		<div class="settingsHeader ui-state-default">
			<div style="float:right;">
				<a href="/ta/promotions/checkUtilizedStatus/" class="js-update-businesses">
					Check Utilized Status
				</a>
			</div>
			Businesses
		</div>
		<div class="settingsForm">
			<div id="businessMappingTree" name="selBusinesses">
				<ul>
<?php
foreach ($companies as $company_id => $company) :
?>

					<li id="company_<?= $company_id; ?>" class="folder">
						<?= strip_tags($company['name']); ?>

						<ul>
<?php
	foreach ($company['districts'] as $district_id => $district) :
?>

							<li id="district_<?= $district_id; ?>" class="folder">
								<?= strip_tags($district['name']); ?>

								<ul>
<?php
		foreach ($district['businesses'] as $business_id => $business) :
			if ($business->categoryid != Treat_Model_Business::CATEGORY_KIOSK) {
				continue; // skip if not kiosk
			}
			$selected = "";
			if (in_array($business_id, $enabled)) {
					$selected = " selected";
			}
?>

									<li id='business_<?= $business_id; ?>' data-businessid="<?= $business_id; ?>" class='<?= $selected; ?>'>
										<?= strip_tags($business->name); ?>
<?php
			if (in_array($business_id, $utilized)) :
?>

										<img src="/assets/images/info.png" alt="This promotion has been utilized" title="This promotion has been utilized" />
<?php
			endif;
?>

									</li>
<?php
		endforeach;
?>

								</ul>
							</li>
<?php
	endforeach;
?>

						</ul>
					</li>
<?php
endforeach;
?>

				</ul>
			</div>
		</div>
	</div>
</div>
