<h3>Business Groups</h3>

<div id="businessgroups" class="divTable">
	<div class="divTableRow">
		<div class="divTableCell" style="width: 200px;">
			<select class="groupSelectList" style="width: 100%;" size="16" id="groupsList"></select>
		</div>
		<div class="divTableCell">
			<div class="settingsContainer groupSettings">
				<div class="settingsHeader ui-state-default">Group Details</div>
				<form action="updateGroupDetails" class="settingsForm" id="groupDetailsForm" onsubmit="return false;" method="post">
					<input type="hidden" name="group_id" id="group_id" value="0" />
					
					<div>
						<label for="group_name">Name:</label>
						<input class="groupOption" type="text" size="32" name="group_name" id="group_name" value="" />
					</div>
					
					<div>
						<button style="float: right;" class="groupOption" id="deleteGroup">Delete</button>
						<input type="submit" class="groupOption" id="group_submit" value="Save" />
					</div>
				</form>
				
				<div id="groupMembersContainer" style="position: relative; min-height: 200px; display: none;" class="groupSettings">
					<div id="groupMembers" class="itemListColumn itemListLeft">
						<div class="itemListHeader">
							<input type="text" id="groupMembers_Filter" class="itemListFilter" value="" />
							<span id="groupMembers_FilterReset" class="itemListFilterReset">Reset</span>
						</div>
						
						<select id="groupMembers_Select" class="itemListSelect" multiple="multiple"></select>
					</div>
					
					<img class="bigSpinner bigSpinnerLeft" id="groupMembers_SelectSpinner" style="display: none;" src="/assets/images/ajax-loader-redmond-big.gif" />
					
					<div class="itemListGutter">
						<div>
							<span id="groupMembers_Submit" style="visibility: hidden;">Save</span>
						</div>
						
						<br />
						
						<div class="fieldset">
							<div><span id="groupBusiness_MoveButton">&lt;&lt;</span></div>
							<div><span id="groupMembers_MoveButton">&gt;&gt;</span></div>
						</div>
					</div>
					
					<div id="groupBusiness" class="itemListColumn itemListRight">
						<div class="itemListHeader">
							<input type="text" id="groupBusiness_Filter" class="itemListFilter" value="" />
							<span id="groupBusiness_FilterReset" class="itemListFilterReset">Reset</span>
						</div>
						
						<select id="groupBusiness_Select" class="itemListSelect" multiple="multiple"></select>
					</div>
					
					<img class="bigSpinner bigSpinnerRight" id="groupBusiness_SelectSpinner" style="display: none;" src="/assets/images/ajax-loader-redmond-big.gif" />
				</div>
			</div>
		</div>
	</div>
</div>
