<div class='navPanelTitle'>Menu Groups</div>

<ul id='group_list'>

<?php
	foreach( $groupList as $groupInfo ) :
?>
	<li id='group<?=$groupInfo->id?>'
		onmouseover="this.style.cursor='pointer';"
		onclick="update_group(<?=$groupInfo->id?>,'<?=$groupInfo->name?>');">
		<?=$groupInfo->name?>
	</li>
<?php
	endforeach;
?>
</ul>
<ul>
	<li>
		<form action="#" method='post' style="margin:0;padding:0;display:inline;">
		<input type='hidden' name='bid' id='grpbid' value='<?=$businessid?>' />
		<input type='hidden' name='group_id' id='group_id' value='0' />
		<input type='text' size='15' name='group_name' id='group_name' value='' />
		<input type='submit' value='Submit' id='group_submit' onclick="submit_group(); return false;" />
		</form>
		<input type='button' value='Clear' onclick="update_group(0,''); return false;" />
	</li>
</ul>

<p></p>

<div id='group_tax' style="display:none;border:2px solid #CCCCCC;width:400px;">
<div style="background: #CCCCCC; text-align: center; font-weight: bold;">
	Update Item Tax
</div>&nbsp;
<form action="#" method='post'>
<input type='hidden' name='group_tax_id' id='group_tax_id' value='0' />

<?php
	foreach( $grTaxList as $grTaxInfo ) :
?>
&nbsp;&nbsp;
<a href="#" onclick="update_group_taxes(1,<?=$grTaxInfo->id?>); return false;" title='Add tax to all items in this group'>
	<img src="/assets/images/plus.gif" border='0'>
</a>
<a href="#" onclick="update_group_taxes(2,<?=$grTaxInfo->id?>); return false;" title='Remove tax from items in this group'>
	<img src='/assets/images/nutrition/delete.gif' border='0'>
</a>
<?=$grTaxInfo->name?> (<?=$grTaxInfo->percent?>%)
<br />
<?php
	endforeach;
?>

</form>
</div>

	<div id="promo_groups">
		<div class="title">Promotion/Printing Groups</div>
		<ul id="promo_list">
<?php foreach ($promoGroupsList as $promoGroup): ?>
			<li><a href="#" class="promo_group" id="<?php echo $promoGroup->id; ?>"><?php echo $promoGroup->name; ?></a></li>
<?php endforeach; ?>
		</ul>
		<form id="frm_promo_groups" action="#" method="post">
			<input type="hidden" name="bid" id="promo_group_bid" value="<?php echo $businessid; ?>" />
			<input type="hidden" name="promo_group_id" id="promo_group_id" value="" />
			<input type="hidden" name="todo" value="19" />
			<div>
				<label for="promo_group_name">Group Name</label>
				<input type="text" size="20" maxlength="128" name="promo_group_name" id="promo_group_name" value="" />
			</div>
			<div>
				<label for="promo_group_visible">Visible?</label>
				<input type="checkbox" name="promo_group_visible" id="promo_group_visible" value="1" />
			</div>
			<div>
				<label for="promo_group_routing">Print Routing Group</label>
				<input type="text" size="5" maxlength="1" name="promo_group_routing" id="promo_group_routing" value="<?php echo $promoGroup->routing; ?>" />
				(Enter zero to disable.)
			</div>
			<div>
				<label for="promo_group_order">Display Order</label>
				<input type="text" size="5" maxlength="5" name="promo_group_order" id="promo_group_order" value="99999" />
			</div>
			<input type="submit" value="Save" /><input type="reset" value="Clear" />
		</form>
	</div>
	<div id="promo_group_products"></div>