<div class='navPanelTitle'>Card Numbers</div>
<ul id='card_list'>

<?php
	foreach( $cardList as $cardInfo ) :
?>
	<li id='card<?=$cardInfo->id?>'
		onmouseover="this.style.cursor='pointer';"
		onclick="update_cards(<?=$cardInfo->id?>,
								'<?=$cardInfo->scan_start?>',
								'<?=$cardInfo->scan_end?>',
								'<?=number_format( $cardInfo->value, 2 )?>');">
		<?=$cardInfo->scan_start?> - <?=$cardInfo->scan_end?> [$<?=number_format( $cardInfo->value, 2 )?>]
	</li>
<?php
	endforeach;
?>
</ul>
<ul>
	<li>
		<form action="#" method='post' style="margin:0;padding:0;display:inline;">
		<input type='hidden' name='scan_bid' id='scan_bid' value='<?=$businessid?>' />
		<input type='hidden' name='scan_id' id='scan_id' value='0' />
		<input type='text' size='11' name='scan_start' id='scan_start' value='' /> -
		<input type='text' size='11' name='scan_end' id='scan_end' value='' />
		$<input type='text' size='4' name='scan_value' id='scan_value' value='' />
		<input type='submit' value='Submit' id='scan_submit' onclick="submit_cards(); return false;">
		</form>
		<input type='button' value='Clear' onclick="update_cards(0,'','',''); return false;" />
	</li>
</ul>