<div class='navPanelTitle'>Combos</div>
<div class="divTable"><div class="divTableRow">
	<div class='divTableCell' style='width: 200px;'>
		<select style='width: 100%;' size='16' id='comboList'></select>
		<div>
			<div>
				<span id='moveComboUp' class='comboMove'>Move Up</span>
				<span id='moveComboDown' class='comboMove'>Move Down</span>
			</div>
			<input type='checkbox' id='showHideCombos' /><label id='showHideCombosLabel' for='showHideCombos'>Show Inactive</label>
		</div>
	</div>

	<div class='divTableCell'>
		<div class='settingsContainer'>
			<div class='settingsHeader ui-state-default'>Basic Settings</div>
			<form class='settingsForm' id='comboForm' onsubmit='return false;' method='post'>
				<input type='hidden' name='bid' id='combobid' value='<?=$businessid?>' />
				<input type='hidden' name='combo_id' id='combo_id' value='0' />

				<label for='combo_name'>Name:</label>
				<input class='comboOption' type='text' size='15' name='combo_name' id='combo_name' value='' />

				<br />

				<label for='combo_type'>Combo Type:</label>
				<select class='comboOption' name=combo_type id='combo_type'>
                    <option value=''></option>
                    <option value='2'>Amount Off</option>
                    <option value='3'>Fixed Value</option>
				</select>

				<span class='comboDiscount promo_fixed_value'>
					<label for='promo_value'>Total:</label>
					<input class='comboOption' type='text' name='combo_value' id='combo_value' value='' size=6 />
				</span>
				<br />
                <div class='settingsItem'>
                    <input class='comboOption' type="checkbox" name='disable_promotions' id='disable_promotions' value='1' />
                    <label for='disable_promotions'>Disable Promotions</label>
                </div>
			</form>
		
			</div>

			<div id='combo_detail'></div>

			<div class='settingsContainer' id='tblOrderGroup' style="display: none;">
			<div class='settingsHeader ui-state-default'>Groups</div>

			<table >
			<tr>
				<td>
				
					<div id="drag1">
						<table id="table1" style='background-color: #e0e0e0;	border-collapse: collapse;'>

						</table>

						<table >
							<button id='btnSaveOrderCombo1' value='table1'>Save Order</button>
						</table>

						
					</div>
				</td>
				<td valign='top'>
				
					<div> 
						
						<table class="ui-picker-dropdown ui-widget-content ui-corner-all" style="display: block; width: 90px;font-size: 11pt; ">
							<tr >
									<td class='mark' colspan=5>
										Button Color:
									</td>
								</td>
								
							</tr>

							<tr class="ui-picker-dropdown-row" id="picker-20399-row-0">
									<td id = 'color1' onclick="select_color(this)" class="product-button-color-0" style="width: 18px; height: 18px; position: static; float: none;  margin-left: 12px;cursor: pointer">
									</td>
									<td id = 'color2' onclick="select_color(this)" class="product-button-color-1" style="width: 18px; height: 18px; position: static; float: none;  margin-left: 12px;cursor: pointer">
									</td>
									<td id = 'color3' onclick="select_color(this)" class="product-button-color-2" style="width: 18px; height: 18px; position: static; float: none;  margin-left: 12px;cursor: pointer">
									</td>
									<td id = 'color4' onclick="select_color(this)" class="product-button-color-3" style="width: 18px; height: 18px; position: static; float: none;  margin-left: 12px;cursor: pointer">
									</td>
								</td>
								
							</tr>
							<tr class="ui-picker-dropdown-row" id="picker-20399-row-0">
									<td id = 'color4' onclick="select_color(this)" class="product-button-color-4" style="width: 18px; height: 18px; position: static; float: none;  margin-left: 12px;cursor: pointer">
									</td>
									<td id = 'color5' onclick="select_color(this)" class="product-button-color-5" style="width: 18px; height: 18px; position: static; float: none;  margin-left: -12px;cursor: pointer">
									</td>
									<td id = 'color6' onclick="select_color(this)" class="product-button-color-6" style="width: 18px; height: 18px; position: static; float: none;  margin-left: 12px;cursor: pointer">
									</td>
									<td id = 'color7' onclick="select_color(this)" class="product-button-color-7" style="width: 18px; height: 18px; position: static; float: none;  margin-left: 12px;cursor: pointer">
									</td>
								</td>
								
							</tr>
							<tr class="ui-picker-dropdown-row" id="picker-20399-row-0">
									<td  id = 'color8' onclick="select_color(this)" class="product-button-color-8" style="width: 18px; height: 18px; position: static; float: none;  margin-left: 12px;cursor: pointer">
									</td>
									<td id = 'color9' onclick="select_color(this)" class="product-button-color-9" style="width: 18px; height: 18px; position: static; float: none;  margin-left: 12px;cursor: pointer">
									</td>
									<td id = 'color10' onclick="select_color(this)" class="product-button-color-10" style="width: 18px; height: 18px; position: static; float: none;  margin-left: -12px;cursor: pointer">
									</td>
									<td id = 'color11' onclick="select_color(this)" class="product-button-color-11" style="width: 18px; height: 18px; position: static; float: none;  margin-left: 12px;cursor: pointer">
									</td>
								</td>
								
							</tr>
						</table>					
					</div>
				</td>
				
			</tr>
			</table>
			
			</div>
			</div>

			
			


									
			
            <div class='settingsContainer' style="border: 0px;">
                <button class='comboOption' type='submit' id='combo_submit' value='Save'>Save</button>
                <button class='comboOption' id='deleteCombo'>Deactivate</button>
            </div>
		</div>
	</div>
</div>