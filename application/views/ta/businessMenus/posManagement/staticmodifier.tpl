<div class='navPanelTitle'>Static Modifiers</div>
<div class="divTable"><div class="divTableRow">
	
	<form id='staticModifierForm' class='settingsForm' action='updateStaticModifierSettings' method='post' onsubmit='return false;'>
		<fieldset>
			<legend>Display</legend>
			<input type='text' size='1' name='mdock_rows' alt='Rows' class='slider-0-12 validate-type-integer' />
			<input type='text' size='1' name='mdock_cols' alt='Cols' class='slider-0-12 validate-type-integer' />
			<input type='checkbox' name='mdock_scrollbar' alt='Hide Scrollbar' />
		</fieldset>
		
		<fieldset>
			<input type='hidden' name='bid' id='mdockbid' value='<?=$businessid?>' />
			<input type='submit' value='Save' />
			<input type='reset' value='Cancel' />
		</fieldset>
	</form>
	
	<div id='mdockAddGroupDialog' style='display: none;'>
		<form action='addStaticModifierToDock' id='mdockAddGroupForm' onsubmit='return false;' method='post'>
			<input type='hidden' name='bid' value='<?=$businessid?>' />
			<div id='mdock_AddGroup'>
				<div class='itemListHeader' style='position: relative;'>
					<input type='text' id='mdock_AddGroupFilter' class='itemListFilter' value='' />
					<span id='mdock_AddGroupFilterReset' class='itemListFilterReset' style='float: right;'>Reset</span>
				</div>
				<select id='mdock_AddGroupSelect' style='width: 100%' multiple='multiple'></select>
			</div>
			<img class='bigSpinner bigSpinnerCenter' id='mdock_AddGroupSpinner' style='display: none;' src='/assets/images/ajax-loader-redmond-big.gif' />
		</form>
	</div>
	
	<div>
		<div id='mdockBar' class='ui-widget browserBar'>
			<a id='mdockAddGroup'>Add Group</a>
			<a id='mdockRemoveGroup' style='display: none;'>Remove Group</a>
		</div>
		<div id='mdockGroups' class='posItems aerispanel-display-normal'></div>
	</div>
	
</div></div>