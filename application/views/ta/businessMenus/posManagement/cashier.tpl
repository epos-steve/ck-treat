<div class='navPanelTitle'>Cashier Users</div>
<div class="divTable"><div class="divTableRow">
	<div class='divTableCell' style='width: 200px;'>
		<select style='width: 100%;' size='8' id='cashierList'></select>
		<select style='width: 100%;' size='8' id='nonCashierList'></select>
	</div>

	<div class='settingsContainer' id='cashierSettings'>
		<div class='settingsHeader ui-state-default'>User Info</div>
		<form action='updateCashier' class='settingsForm' id='cashierForm' onsubmit='return false;' method='post'>
			<input type='hidden' name='bid' id='cashierbid' value='<?=$businessid?>' />
			<input type='hidden' name='cashier_loginid' id='cashier_loginid' value='0' />
			<input style='float: right;' class='cashierOption' type='submit' id='cashier_submit' value='Save' />

			<label for='cashier_loginName'>Name:</label>
			<input disabled='disabled' type='text' size='32' name='cashier_name' id='cashier_name' value='' />
			<br />

			<button style='float: right; clear: both;' class='cashierOption cashierDelete' id='deleteCashier'>Delete</button>

			<label for='cashier_passcode'>Passcode:</label>
			<input class='cashierOption cashierWatermark' disabled='disabled' type='text' size='12' name='cashier_passcode' id='cashier_passcode' value='' />
			
			<label for='cashier_groupName'>Group:</label>
			<select class='cashierOption' name='cashier_groupName' id='cashier_groupName'>
				<option value='Cashier'>Cashier</option>
				<option value='Manager'>Manager</option>
			</select>
		</form>
	</div>
</div></div>