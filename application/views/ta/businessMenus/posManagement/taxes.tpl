<div class='navPanelTitle'>Taxes</div>

<ul id='tax_list'>
<?php
	foreach( $taxList as $taxInfo ) :
?>
	<li id='tax<?=$taxInfo->id?>'
		onmouseover="this.style.cursor='pointer';"
		onclick="update_tax(
					<?=$taxInfo->id?>,
					'<?=$taxInfo->name?>',
					'<?=$taxInfo->percent?>'); return false;">
		<?=$taxInfo->name?> (<?=$taxInfo->percent?>%)
	</li>
<?php
	endforeach;
?>
</ul>

<ul>
	<li>
		<form action="#" id='tax_form' method=post
			style="margin: 0; padding: 0; display: inline;">
			<input type='hidden' id='bid' name='businessid' value='<?=$businessid?>' />
			<input type='hidden' name='tax_id' id='tax_id' value='0' />
			<input type='text' size='15' name='tax_name' id='new_tax_name' value='' />
			<input type='text' size='5' name='percent' id='new_percent' value='' />%
			<input type='submit' value='Submit' id='tax_submit' onclick="submit_tax(); return false;">
		</form>
		<input type='button' value='Clear' onclick="update_tax(0,'',''); return false;" />
	</li>
</ul>