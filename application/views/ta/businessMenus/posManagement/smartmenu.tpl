<div class='navPanelTitle'>Smart Menu Boards</div>
<div class="divTable"><div class="divTableRow">
	<div class='divTableCell' style='width: 200px;'>
		<select style='width: 100%;' id='smenuSelect'>
			<option value='smenuLocation' selected='selected'>Overview</option>
			<option value='smenuBoard'>Board Setup</option>
			<option value='smenuClient'>Client Setup</option>
			<option value='smenuMenu'>Menu Setup</option>
		</select>
		<select class='smenuSelectList' style='width: 100%; display: none;' size='16' id='smenuBoardList'></select>
		<select class='smenuSelectList' style='width: 100%; display: none;' size='16' id='smenuClientList'></select>
		<select class='smenuSelectList' style='width: 100%; display: none; visibility: hidden;' size='16' id='smenuLocationList'></select>
		<select class='smenuSelectList' style='width: 100%; display: none;' size='16' id='smenuMenuList'></select>
	</div>

		<div class='settingsContainer' id='smenuBoardSettings' style='display: none;'>
			<div class='settingsHeader ui-state-default'>Board Settings</div>
			<form action='updateSmenuBoard' class='settingsForm' id='smenuBoardForm' onsubmit='return false;' method='post'>
				<input type='hidden' name='bid' id='smenubid' value='<?=$businessid?>' />
				<input type='hidden' name='smenuBoard_id' id='smenuBoard_id' value='0' />
				<input style='float: right;' class='smenuOption' type='submit' id='smenuBoard_submit' value='Save' />

				<label for='smenuBoard_boardName'>Name:</label>
				<input class='smenuOption' type='text' size='15' name='smenuBoard_boardName' id='smenuBoard_boardName' value='' />
				<br />

				<button style='float: right; clear: both;' class='smenuOption smenuDelete' id='deleteSmenuBoard'>Deactivate</button>

				<label for='smenuBoard_boardGuid'>GUID:</label>
				<input class='smenuGuid' disabled='disabled' type='text' size='32' name='smenuBoard_boardGuid' id='smenuBoard_boardGuid' value='' />
				<button class='smenuOption smenuGuid' id='generateSmenuBoardGuid'>Generate GUID</button>

				<button style='float: right; clear: both;' class='smenuOption' onclick='return false;' id='exportSmenuBoard'>Export</button>

				<br />

				<label for='smenuBoard_loginKey'>Login Key:</label>
				<input class='smenuKeys' disabled='disabled' type='text' size='30' name='smenuBoard_loginKey' id='smenuBoard_loginKey' value='' />
				<br />
				<label for='smenuBoard_loginSecret'>Login Secret:</label>
				<input class='smenuKeys' disabled='disabled' type='text' size='30' name='smenuBoard_loginSecret' id='smenuBoard_loginSecret' value='' />
				<button class='smenuOption smenuKeys' id='generateSmenuBoardKeys'>Generate Keys</button>

				<br />

				<label for='smenuBoard_packageFile'>Current Package:</label>
				<select class='smenuOption' name='smenuBoard_currentPackage_Select' id='smenuBoard_currentPackage_Select'></select>
				<input type='hidden' class='function' id='function_updatePackageList' name='updatePackageList' value='smenuBoard_currentPackage' />
				<input type='hidden' class='value' id='smenuBoard_currentPackage_List' name='smenuBoard_packageList' value='' />
				<input type='hidden' class='value' id='smenuBoard_currentPackage' name='smenuBoard_currentPackage' value='' />
			</form>
		</div>

		<div class='settingsContainer smenuBoardSettings smenuNotNew' style='display: none;'>
			<div class='settingsHeader ui-state-default'>Linked Packages</div>
			<form action='updateSmenuPackageLinks' class='settingsForm noAutoSubmit' id='smenuBoardLinksForm' onsubmit='return false;' method='post'>
				<div style='position: relative; min-height: 82px;'>
					<input type='hidden' class='callback' id='callback_getPackageLinks' name='packageLinksList' value='getSmenuPackageLinks' />
					<div>
						<input type='text' id='packageLinksSelect' />
						<div id='packageLinksList' class='divList'></div>
					</div>
					<img class='bigSpinner bigSpinnerCenter' id='packageLinksListSpinner' style='display: none;' src='/assets/images/ajax-loader-redmond-big.gif' />
				</div>
			</form>
		</div>

		<div class='settingsContainer smenuBoardSettings smenuNotNew' style='display: none;'>
			<div class='settingsHeader ui-state-default'>Control Queue</div>
			<form action='updateSmenuBoardControl' class='settingsForm noAutoSubmit' id='smenuBoardControlForm' onsubmit='return false;' method='post'>
				<div style='position: relative; min-height: 82px;'>
					<input type='hidden' class='callback' id='callback_getBoardControl' name='boardControlList' value='getSmenuBoardControl' />
					<div><div id='boardControlList'></div></div>
					<img class='bigSpinner bigSpinnerCenter' id='boardControlListSpinner' style='display: none;' src='/assets/images/ajax-loader-redmond-big.gif' />
				</div>
			</form>
		</div>

		<div class='settingsContainer' id='smenuClientSettings' style='display: none;'>
			<div class='settingsHeader ui-state-default'>Client Settings</div>
			<form action='updateSmenuClient' class='settingsForm' id='smenuClientForm' onsubmit='return false;' method='post'>
				<input type='hidden' name='bid' id='smenubid' value='<?=$businessid?>' />
				<input type='hidden' name='smenuClient_id' id='smenuClient_id' value='0' />
				<input style='float: right;' class='smenuOption' type='submit' id='smenuClient_submit' value='Save' />

				<label for='smenuClient_clientName'>Name:</label>
				<input class='smenuOption' type='text' size='15' name='smenuClient_clientName' id='smenuClient_clientName' value='' />

				<br />

				<button style='float: right; clear: both;' class='smenuOption smenuDelete' id='deleteSmenuClient'>Deactivate</button>

				<label for='smenuClient_loginKey'>Login Key:</label>
				<input class='smenuKeys' disabled='disabled' type='text' size='30' name='smenuClient_loginKey' id='smenuClient_loginKey' value='' />
				<br />
				<label for='smenuClient_loginSecret'>Login Secret:</label>
				<input class='smenuKeys' disabled='disabled' type='text' size='30' name='smenuClient_loginSecret' id='smenuClient_loginSecret' value='' />
				<button class='smenuOption smenuKeys' id='generateSmenuClientKeys'>Generate Keys</button>
			</form>
		</div>

		<div class='settingsContainer' id='smenuMenuSettings' style='display: none;'>
			<div class='settingsHeader ui-state-default'>Menu Settings</div>
			<form action='updateSmenuMenu' class='settingsForm' id='smenuMenuForm' onsubmit='return false;' method='post'>
				<input type='hidden' name='bid' id='smenuMenuBid' value='<?=$businessid?>' />
				<input type='hidden' name='smenuMenu_id' id='smenuMenu_id' value='0' />
				<input style='float: right;' class='smenuOption' type='submit' id='smenuMenu_submit' value='Save' />
				<button style='float: right;' class='smenuOption' onclick='return false;' id='smenuMenu_export'>Export</button>

				<label for='smenuMenu_menuName'>Name:</label>
				<input class='smenuOption' type='text' size='32' name='smenuMenu_menuName' id='smenuMenu_menuName' value='' />
			</form>
		</div>

		<div class='settingsContainer smenuMenuSettings smenuNotNew' style='display: none;'>
			<div class='settingsHeader ui-state-default'>Menu Items</div>
			<form action='updateSmenuMenuItems' class='settingsForm noAutoSubmit' id='smenuMenuItemsForm' onsubmit='return false;' method='post'>
				<div style='position: relative; min-height: 400px;'>
					<input type='hidden' class='callback' id='callback_getMenuItems' name='menuItems_Select' value='getSmenuMenuItems' />
					<input type='hidden' class='callback' id='callback_getItemList' name='itemList_Select' value='getSmenuMenuItemList' />
					<div id='smenuMenu_menuItems' class='itemListColumn itemListLeft'>
						<div class='itemListHeader'>
							<input type='text' id='menuItems_Filter' class='itemListFilter' value='' />
							<span id='menuItems_FilterReset' class='itemListFilterReset'>Reset</span>
						</div>
						<select id='menuItems_Select' class='itemListSelect' multiple='multiple'></select>
					</div>
					<img class='bigSpinner bigSpinnerLeft' id='menuItems_SelectSpinner' style='display: none;' src='/assets/images/ajax-loader-redmond-big.gif' />
					<div class='itemListGutter'>
						<!-- <div><input type='submit' id='smenuMenuItems_submit' style='visibility: hidden;' onclick='return false;' value='Save' /></div>  -->
						<div><span id='smenuMenuItems_submit' style='visibility: hidden;'>Save</span></div>
						<br />
						<div class='fieldset'>
							<span class='header'>Items</span>
							<div><span id='itemList_MoveButton'>&lt;&lt;</span></div>
							<div><span id='menuItems_MoveButton'>&gt;&gt;</span></div>
							<div><span id='menuItems_OrderUpButton'>^</span></div>
							<div><span id='menuItems_OrderDownButton'>v</span></div>
						</div>
						<br />
						<div class='fieldset'>
							<span class='header'>Groups</span>
							<div><span id='menuItems_GroupEditButton'>Edit</span></div>
							<div><span id='menuItems_GroupButton'>Group</span></div>
							<div><span id='menuItems_UngroupButton'>Ungroup</span></div>
						</div>
					</div>
					<div id='smenuMenu_itemList' class='itemListColumn itemListRight'>
						<div class='itemListHeader'>
							<input type='text' id='itemList_Filter' class='itemListFilter' value='' />
							<span id='itemList_FilterReset' class='itemListFilterReset'>Reset</span>
						</div>
						<select id='itemList_Select' class='itemListSelect' multiple='multiple'></select>
					</div>
					<img class='bigSpinner bigSpinnerRight' id='itemList_SelectSpinner' style='display: none;' src='/assets/images/ajax-loader-redmond-big.gif' />
				</div>
			</form>
			<div id='menuItems_GroupEditDialog'>
				<form action='editSmenuMenuGroup' class='settingsForm' id='menuItems_GroupEditForm' onsubmit='return false;' method='post'>
					<div class='fieldset'>
						<span class='legend'>
							<input type='checkbox' id='smenuMenuGroup_automaticName' name='automaticName' />
							<label for='automaticName'>Automatic Name</label>
						</span>
						<input type='hidden' name='groupId' id='smenuMenuGroup_groupId' value='' />
						<label for='groupName'>Group Name:</label>
						<input type='text' name='groupName' id='smenuMenuGroup_groupName' value='' />
					</div>
				</form>
			</div>
		</div>

		<div class='settingsContainer' id='smenuLocationSettings'>
			<div class='settingsHeader ui-state-default'>Location Info</div>
			<div class='settingsForm'>
				<label for='smenuLocation_name'>Name:</label>
				<input disabled='disabled' type='text' size='32' name='smenuLocation_name' id='smenuLocation_name' value='<?=$smenuLocation['name']?>' />

				<br />

				<label for='smenuLocation_locationGuid'>GUID:</label>
				<input disabled='disabled' type='text' size='32' name='smenuLocation_locationGuid' id='smenuLocation_locationGuid' value='<?=$smenuLocation['locationGuid']?>' />
			</div>
		</div>

		<div class='settingsContainer smenuLocationSettings'>
			<div class='settingsHeader ui-state-default'>Boards</div>
			<table class='infoTable settingsForm'>
				<thead>
					<tr>
						<td>Board</td>
						<td>GUID</td>
						<td>Package</td>
					</tr>
				</thead>
				<tbody id='smenuLocationBoardList'>
				</tbody>
			</table>
		</div>

		<div class='settingsContainer smenuLocationSettings'>
			<div class='settingsHeader ui-state-default'>Packages</div>
			<table class='infoTable settingsForm'>
				<thead>
					<tr>
						<td>Package</td>
						<td>Created</td>
						<td>Modified</td>
					</tr>
				</thead>
				<tbody id='smenuLocationPackageList'>
				</tbody>
			</table>
		</div>
	</div>
</div></div>