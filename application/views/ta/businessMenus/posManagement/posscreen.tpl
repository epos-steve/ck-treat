<div class='navPanelTitle'>POS Screens</div>
<div class="divTable"><div class="divTableRow">
	<input type='hidden' id='currentScreen' value='' />
	<input type='hidden' name='bid' id='screenbid' value='<?=$businessid?>' />
	
	<div id='screenEditGroupDialog' style='display: none;'>
		<form action='screenEditGroup' id='screenEditGroupForm' onsubmit='return false;' method='post'>
			<input type='hidden' name='bid' value='<?=$businessid?>' />
			<input type='hidden' name='screenGroup_id' id='screenGroup_id' value='0' />
			<div>
				<label for='screenGroup_name'>Group Name:</label>
				<input type='text' class='screenOption' name='screenGroup_name' id='screenGroup_name' size='25' value='New Group' />
			</div>
		</form>
	</div>
	
	<div id='screenAddGroupDialog' style='display: none;'>
		<form action='screenAddGroup' id='screenAddGroupForm' onsubmit='return false;' method='post'>
			<input type='hidden' name='bid' value='<?=$businessid?>' />
			<div id='screen_AddGroup'>
				<div class='itemListHeader' style='position: relative;'>
					<input type='text' id='screen_AddGroupFilter' class='itemListFilter' value='' />
					<span id='screen_AddGroupFilterReset' class='itemListFilterReset' style='float: right;'>Reset</span>
				</div>
				<select id='screen_AddGroupSelect' style='width: 100%' multiple='multiple'></select>
			</div>
			<img class='bigSpinner bigSpinnerCenter' id='screen_AddGroupSpinner' style='display: none;' src='/assets/images/ajax-loader-redmond-big.gif' />
		</form>
	</div>
	
	<div id='screenAddItemDialog' style='display: none;'>
		<form action='screenAddItem' id='screenAddItemForm' onsubmit='return false;' method='post'>
			<input type='hidden' name='bid' value='<?=$businessid?>' />
			<div id='screen_AddItem'>
				<div class='itemListHeader' style='position: relative;'>
					<input type='text' id='screen_AddItemFilter' class='itemListFilter' value='' />
					<span id='screen_AddItemFilterReset' class='itemListFilterReset' style='float: right;'>Reset</span>
				</div>
				<select id='screen_AddItemSelect' style='width: 100%' multiple='multiple'></select>
			</div>
			<img class='bigSpinner bigSpinnerCenter' id='screen_AddItemSpinner' style='display: none;' src='/assets/images/ajax-loader-redmond-big.gif' />
		</form>
	</div>
	
	<div id='screenPage' class='divTableCell'>
		
		<div id='screenContainer' class='cashier'>
			<div id='screenGroups' class='posItems'></div>
			<div id='screenItems' class='posItems'></div>
		</div>
		
		<div id='screenProperties' class='ui-widget'>
			<div class='ui-helper-clearfix screenToolbar'>
				<div class='ui-corner-left ui-widget-header' style="padding-bottom: 0.6em;">
					<label>Group:</label>
					<a id='screenNewGroup'>Create New Group</a>
					<a id='screenAddGroup'>Add Existing Group</a>
					<a id='screenRemoveGroup'>Remove Group</a>
				</div>
				<div class='ui-corner-middle ui-widget-header' style="padding-bottom: 0.6em;">
					<label>Item:</label>
					<a id='screenNewItem'>Create New Item</a>
					<a id='screenAddItem'>Add Existing Item</a>
<!--                    <a id='screenAddSpecial'>Scheduled Special</a>-->
					<a id='screenRemoveItem'>Remove Item</a>

				</div>
				<div class='ui-corner-right ui-widget-header'>
					<label>Item Option:</label>
					<select id="screenAddOptions" name="screenAddOptions">
						<option value=""></option>
						<option value="scheduled_special">Add Scheduled Special</option>
						<option value="combo">Add Combo</option>
					</select>
				</div>
			</div>
			
			<div id='screenEditContainer' class='ui-widget-content ui-corner-all ui-state-default'>
				<p class='editNone'>No item or group selected</p>
				
				<div class='editItem ui-helper-hidden-accessible'>
					<form class='ajaxeditForm' action='screenEditItem' method='post'>
						<fieldset>
							<legend>Menu Item</legend>
							<input type='text' name='mi.name' alt='Name' value='New Item' size='32' class='validate-notempty' />
							<select name='mi.group_id' alt='Group'>
								<option value='0'>(none)</option>
								<?php foreach( $departments as $dept ) { ?>
								<option value='<?=$dept->id?>'><?=$dept->name?></option>
								<?php } ?>
							</select>
							<input type='text' name='mi.item_code' alt='Item Code' size='16' class='validate-notempty' />
							<div class='group'>
								<input type='text' name='mi.upc' alt='UPC' size='24' />
								<button id='generateOpenUPC' onclick='return false;'>Generate Open Item UPC</button>
							</div>
						</fieldset>
						<fieldset>
							<legend>This Screen</legend>
							<div style="font-style: italic;">These options affect this POS screen only.</div>
							<select name='pgd.pos_color' alt='Button Color' id='product-pos-color'
									class='picker validate-type-color picker-prefix-product-button-color-'>
							<?php for( $i = 0; $i < 12; $i++ ) { ?>
								<option><?=$i?></option>
							<?php } ?>
							</select>
							<input type='text' name='pgd.display_x' alt='X' readonly='true' size='2' />
							<input type='text' name='pgd.display_y' alt='Y' readonly='true' size='2' />
						</fieldset>
						<fieldset>
							<legend>Ordering</legend>
							<input type='text' name='mi.max' alt='Max' size='3' class='validate-type-integer' />
							<input type='text' name='mi.reorder_point' alt='Reorder Point' size='3' class='validate-type-integer' />
							<input type='text' name='mi.reorder_amount' alt='Reorder Amount' size='3' class='validate-type-integer' />
							<!-- <select name='mi.manufacturer' alt='Manufacturer'>
								<option value='0'>(none)</option>
								<?php foreach( $manufacturers as $manuf ) { ?>
								<option value='<?=$manuf->id?>'><?=$manuf->name?></option>
								<?php } ?>
							</select> -->
							<input type='text' name='mip.cost' alt='Cost' size='10' class='validate-type-double' />
							<select name='mi.ordertype' alt='Order Type'>
								<option value='0'>(none)</option>
								<?php foreach( $ordertypes as $ot ) { ?>
								<option value='<?=$ot->id?>'><?=$ot->name?></option>
								<?php } ?>
							</select>
						</fieldset>
						<fieldset>
							<legend>Price &amp; Taxes</legend>
							<input type='text' name='mip.price' alt='Current Price' readonly='true' size='10' />
							<input type='text' name='mip.new_price' alt='New Price' size='10' class='validate-type-double' />
							<div class='group'>
								<label>Taxes:</label>
								<?php foreach( $taxes as $tax ) { ?>
								<input type='checkbox' name='taxes[]' value='<?=$tax->id?>' alt='<?=$tax->name?> (<?=$tax->percent?>%)' />
								<?php } ?>
							</div>
							<select name='mi.sale_unit' alt='Sale Unit'>
								<option value='unit'>Each</option>
								<option value='lb'>Pound (lb)</option>
								<option value='oz'>Ounce (oz)</option>
							</select>
							<input type='text' name='mi.tare' alt='Tare' size='10' class='validate-type-double' />
						</fieldset>
						<fieldset>
							<legend>Miscellaneous</legend>
							<div class='group'>
								<label>Condiments:</label>
								

								
								
								
								
			<!---- Begin Create new control for order miscellaneous -->
			<div id="drag3">
				<table id="table3" style='background-color: #e0e0e0;	border-collapse: collapse;'>



								<?php 
									$idnumberElements = count($modifierGroups);
				
									$lnuCount = 0;
									$obarrayElememts = array();
														
									foreach( $modifierGroups as $mod ) { 
				
										$obarrayElememts[$lnuCount][0] = $mod->id;
										$obarrayElememts[$lnuCount][1] = $mod->name;
										$obarrayElememts[$lnuCount][2] = '';
										
										$lnuCount++;
									
									} 
														
									$lnuCount = 0;
									$lnumRows = $idnumberElements / 5;
									
									if (is_float($lnumRows)) $lnumRows = intval($lnumRows)+1;
				


									for ($lnuforRow =0;$lnuforRow<$lnumRows;$lnuforRow++)
									{
									//echo $lnumRows;
										$CodeIncludeInControl .= '<tr style="background-color: rgb(232, 231, 231)">';
										
										for ($lnuforCol =0;$lnuforCol<5;$lnuforCol++)
										{
											if ($lnuCount ==  $idnumberElements) break;
											$CodeIncludeInControl .= '<td style="border: 1px solid white;text-align: center;padding: 2px;"><div id="'.$obarrayElememts[$lnuCount][0].'" style="border-radius: 5px; height: 30px; width: 135px;font-size: 8pt;" class="drag t2 product-button-color-'.$obarrayElememts[$lnuCount][2].'" > <input type="checkbox" id="modifiers[]" name="modifiers[]" value="'.$obarrayElememts[$lnuCount][0].'" alt="'.$obarrayElememts[$lnuCount][1].'" /></div>  </td>';
				
											
				
											$lnuCount++;
										}
										if ($lnuforCol<5)
										{
											for ($lnuforCol2 =0;$lnuforCol2<(5-$lnuforCol);$lnuforCol2++)  {
											$CodeIncludeInControl .= '<td class="mark" style="border: 1px solid white;text-align: center;padding: 2px; width:102px"></td>';
											}
										}
										$CodeIncludeInControl .= '</tr>';
									}
				
									echo $CodeIncludeInControl; 
										
								

				
								?>
				</table>
			</div>

			<!---- Begin Create new control for order miscellaneous -->
				

								
								




								
								
							</div>
							<input type='checkbox' name='mi.force_condiment' alt='Force?' />
							<input type='text' name='mi.shelf_life_days' alt='Shelf Life' size='3' class='validate-type-integer' />
						</fieldset>
						<fieldset>
							<legend>Images</legend>
							<table>
								<tr>
									<td valign="bottom"><input type='checkbox' name='pgd.use_product_image' alt='Use Product Image' /></td>
									<td valign="bottom"><input type='checkbox' name='pgd.hide_text' alt='Hide Text' /></td>
									<td valign="bottom" style="padding-bottom: 4px;">
										<select name='pgd.image_display_type' alt='Display Type'>
											<?php foreach( $imageDisplayTypes as $value ) { ?>
												<option value='<?=$value?>'><?=$value?></option>
											<?php } ?>
										</select></td>
									<td valign="bottom"><input type='text' name='pgd.icon_color' id="icon-color-picker-product" alt='Icon Color' size='8' /></td>
								</tr>
							</table>
						</fieldset>
						<fieldset>
							<input type='hidden' name='pgd.promo_group_id' value='0' />
							<input type='hidden' name='mi.id' value='0' />
							<input type='hidden' name='businessid' value='<?=$GLOBALS['businessid']?>' />
							<input type='hidden' name='idOrderMiscellaneous' id='idOrderMiscellaneous'  />
							<input type='submit' value='Save' id='saveAll'/>
							<input type='reset' value='Cancel' />
						</fieldset>
					</form>
					<div id="product-image-upload"></div>
				</div>

				<div class='editSpecial ui-helper-hidden-accessible'>
					<form class='ajaxeditForm' action='screenEditSpecial' method='post'>
						<fieldset>
							<legend>Scheduled Special</legend>
							<select name='ss.id' alt='Special'>
								<option value=''><span style='font-style: italic; color: #aaa;'>Please choose...</span></option>
								<?php foreach( $scheduledSpecials as $sp ) { ?>
								<option value='<?=$sp->stationid?>'><?=$sp->station_name?></option>
								<?php } ?>
							</select>
							<input type='text' name='mi.name' alt='Name' value='New Special' size='32' class='validate-notempty' />
							<input type='text' name='mi.upc' alt='UPC' value='' size='24' disabled='disabled' />
						</fieldset>
						<fieldset>
							<legend>This Screen</legend>
							<div style="font-style: italic;">These options affect this POS screen only.</div>
							<select name='pgd.pos_color' alt='Button Color' id='product-special-pos-color' class='picker validate-type-color picker-prefix-product-button-color-'>
							<?php for( $i = 0; $i < 12; $i++ ) { ?>
								<option><?=$i?></option>
							<?php } ?>
							</select>
							<input type='text' name='pgd.display_x' alt='X' readonly='true' size='2' />
							<input type='text' name='pgd.display_y' alt='Y' readonly='true' size='2' />
						</fieldset>
						<fieldset>
							<legend>Price &amp; Taxes</legend>
							<input type='text' name='mip.price' alt='Current Price' readonly='true' size='10' />
							<input type='text' name='mip.new_price' alt='New Price' size='10' class='validate-type-double' />
						</fieldset>
						<fieldset>
							<legend>Images</legend>
							<table>
								<tr>
									<td valign="bottom"><input type='checkbox' name='pgd.use_product_image' alt='Use Product Image' /></td>
									<td valign="bottom"><input type='checkbox' name='pgd.hide_text' alt='Hide Text' /></td>
									<td valign="bottom" style="padding-bottom: 4px;">
										<select name='pgd.image_display_type' alt='Display Type'>
											<?php foreach( $imageDisplayTypes as $value ) { ?>
												<option value='<?=$value?>'><?=$value?></option>
											<?php } ?>
										</select></td>
									<td valign="bottom"><input type='text' name='pgd.icon_color' id="icon-color-picker-product-special" alt='Icon Color' size='8' /></td>
								</tr>
							</table>
						</fieldset>
						<fieldset>
							<input type='hidden' name='pgd.promo_group_id' value='0' />
							<input type='hidden' name='mi.id' value='0' />
							<input type='hidden' name='businessid' value='<?=$GLOBALS['businessid']?>' />
							<input type='submit' value='Save' />
							<input type='reset' value='Cancel' />
						</fieldset>
					</form>
					<div id="product-special-image-upload"></div>
				</div>

				<div class='editCombo ui-helper-hidden-accessible'>
					<form class='ajaxeditForm' action='screenEditCombo' method='post'>
						<fieldset>
							<legend>Combo</legend>
							<input type='text' name='mi.name' alt='Name' value='New Combo' size='32' class='validate-notempty' />
							<input type='text' name='mi.upc' alt='UPC' value='' size='24' disabled='disabled' />
							</br></br>
							<select name="cc.id" alt="Combo" id="combo_promo_group_select">
								<option value=''></option>
								<?php foreach( $comboPromotions as $cp ) { ?>
									<?php if ($cp->activeFlag == 1) { ?>
									<option value="<?=$cp->id?>"><?=$cp->name?></option>
									<?php } ?>
								<?php } ?>
							</select>
							<span id="combo_item_select_holder"></span>
						</fieldset>
						<fieldset>
							<legend>This Screen</legend>
							<div style="font-style: italic;">These options affect this POS screen only.</div>
							<select name='pgd.pos_color' alt='Button Color' id='product-combo-pos-color' class='picker validate-type-color picker-prefix-product-button-color-'>
								<?php for( $i = 0; $i < 12; $i++ ) { ?>
									<option><?=$i?></option>
								<?php } ?>
							</select>
							<input type='text' name='pgd.display_x' alt='X' readonly='true' size='2' />
							<input type='text' name='pgd.display_y' alt='Y' readonly='true' size='2' />
						</fieldset>
						<fieldset>
							<legend>Images</legend>
							<table>
								<tr>
									<td valign="bottom"><input type='checkbox' name='pgd.use_product_image' alt='Use Product Image' /></td>
									<td valign="bottom"><input type='checkbox' name='pgd.hide_text' alt='Hide Text' /></td>
									<td valign="bottom" style="padding-bottom: 4px;">
										<select name='pgd.image_display_type' alt='Display Type'>
											<?php foreach( $imageDisplayTypes as $value ) { ?>
												<option value='<?=$value?>'><?=$value?></option>
											<?php } ?>
										</select></td>
									<td valign="bottom"><input type='text' name='pgd.icon_color' id="icon-color-picker-product-combo" alt='Icon Color' size='8' /></td>
								</tr>
							</table>
						</fieldset>
						<fieldset>
							<input type='hidden' name='pgd.promo_group_id' value='0' />
							<input type='hidden' name='mi.id' value='0' />
							<input type='hidden' name='cc.selected_product_id' id="combo_previous_product_id" value='0' />
							<input type='hidden' name='businessid' value='<?=$GLOBALS['businessid']?>' />
							<input type='submit' value='Save' />
							<input type='reset' value='Cancel' />
						</fieldset>
					</form>
					<div id="product-combo-image-upload"></div>
				</div>
				
				<div class='editGroup ui-helper-hidden-accessible'>
					<form class='ajaxeditForm' action='screenEditGroup' method='post'>
						<fieldset>
							<legend>Group</legend>
							<input type='text' name='name' alt='Name' value='New Group' size='32' class='validate-notempty' />
							<input type='checkbox' name='routing' alt='Routing' />
							<input type='text' name='group_rows' alt='Rows' value='' size='2' class='validate-type-integer' />
							<input type='text' name='group_cols' alt='Cols' value='' size='2' class='validate-type-integer' />
							<div class='group'>
								<label>Display on:</label>
								<?php foreach( $aerisModes as $mode ) { ?>
								<input type='checkbox' name='display_on[]' value='<?=$mode->id?>' alt='<?=$mode->name?>' />
								<?php } ?>
							</div>
						</fieldset>
						<fieldset>
							<legend>This Screen</legend>
							<div style="font-style: italic;">These options affect this POS screen only.</div>
							<select name='pos_color' alt='Button Color' id='display-group-pos-color'
									class='picker validate-type-color picker-prefix-product-button-color-'>
							<?php for( $i = 0; $i < 12; $i++ ) { ?>
								<option><?=$i?></option>
							<?php } ?>
							</select>
							<input type='text' name='display_x' alt='X' readonly='true' size='2' />
							<input type='text' name='display_y' alt='Y' readonly='true' size='2' />
						</fieldset>
						<fieldset>
							<legend>Images</legend>
							<table>
								<tr>
									<td valign="bottom"><input type='checkbox' name='use_product_image' alt='Use Product Image' /></td>
									<td valign="bottom"><input type='checkbox' name='hide_text' alt='Hide Text' /></td>
									<td valign="bottom" style="padding-bottom: 4px;">
										<select name='image_display_type' alt='Display Type'>
											<?php foreach( $imageDisplayTypes as $value ) { ?>
												<option value='<?=$value?>'><?=$value?></option>
											<?php } ?>
										</select></td>
									<td valign="bottom"><input type='text' name='icon_color' id="icon-color-picker-display-group" alt='Icon Color' size='8' /></td>
								</tr>
							</table>
						</fieldset>
						<fieldset>
							<input type='hidden' name='id' value='0' />
							<input type='hidden' name='businessid' value='<?=$GLOBALS['businessid']?>' />
							<input type='submit' value='Save' />
							<input type='reset' value='Cancel' />
						</fieldset>
					</form>
					<div id="display-group-image-upload"></div>
				</div>
			</div>
		</div>
		
		<!-- div class='screenSpacer'></div -->
		<!-- div class='settingsContainer buttonSettings' id='screenSettings'>
			<div class='settingsHeader ui-state-default'>Button Settings</div>
			
			<form action='updateScreenButton' class='settingsForm buttonSettingsForm' id='screenButtonForm' onsubmit='return false;' method='post'>
				<input type='hidden' name='bid' id='screenbuttonbid' value='<?=$businessid?>' />
				<input type='hidden' name='button_id' id='button_id' value='0' />
				<input type='hidden' name='button_type' id='button_type' value='' />
				<input type='hidden' name='button_display_order' id='button_display_order' value='' />
				<input type='hidden' name='button_promo_group_id' id='button_promo_group_id' value='' />
				
				<label for='button_name'>Name:</label>
				<input disabled='disabled' type='text' size='14' name='button_name' id='button_name' value='' />
				<br />
				
				<label for='button_pos_color_selector'>Color:</label>
				<input class='colorSelectorValue' type='hidden' name='button_pos_color' id='button_pos_color' value='default' />
				<a id='button_pos_color_selector' class='screenOption'><span class='colorSelector'></span></a>
				<br />
				
				<input type='hidden' name='callback' value='ScreenController.saveScreenButton' />
				
				<input style='float: right;' class='screenOption' type='submit' id='button_submit' value='Save' />
				
			</form>
		</div -->
		
		<!-- div id='colorSelectorPanel' class='colorSelectorPanel' style='display: none;'>
			<div class='product-button-color-default'></div>
			<div class='product-button-color-1'></div>
			<div class='product-button-color-2'></div>
			<div class='product-button-color-3'></div>
			
			<br />
			
			<div class='product-button-color-4'></div>
			<div class='product-button-color-5'></div>
			<div class='product-button-color-6'></div>
			<div class='product-button-color-7'></div>
			
			<br />
			
			<div class='product-button-color-8'></div>
			<div class='product-button-color-9'></div>
			<div class='product-button-color-10'></div>
			<div class='product-button-color-11'></div>
		</div -->
	</div>
	
</div></div>
