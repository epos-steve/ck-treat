<div class='navPanelTitle'>Status</div>
<div class="divTable"><div style='padding: 8px;'>
	
	<?php foreach( $aerisUnits as $unit ) : ?>
	<div class='ui-widget'>
		<div style='padding: 4px;' class='ui-corner-top ui-widget-header'>
			<span><?=$unit->name?> (<?=$unit->client_programid?>)</span>
		</div>
		
		
		<div style='font-size: 82%;' class='ui-widget-content ui-border ui-helper-clearfix ui-floatarea'>
			<div>
				<div style='padding: 4px;' class='ui-corner-top ui-widget-header ui-state-default'>
					<span>General</span>
				</div>
				<div class='ui-corner-bottom ui-border ui-ruled' style='padding: 4px;'>
					<div class='ui-datablock'>
						<label>Client IP:</label> <span><?=$unit->client_ip?></span>
					</div>
					
					<div class='ui-datablock'>
						<label>Send Time:</label> <span><?=$unit->send_time?></span>
					</div>
					
					<div class='ui-datablock'>
						<label>Receive Time:</label> <span><?=$unit->receive_time?></span>
					</div>
				</div>
			</div>
			
			<div>
				<div style='padding: 4px;' class='ui-corner-top ui-widget-header ui-state-default'>
					<span>Software Versions</span>
				</div>
				<div class='ui-corner-bottom ui-border' style='padding: 4px;'>
					<table class='components-table ui-table' style='font-size: inherit; color: inherit;'>
						<thead>
							<tr>
								<th>Component</th>
								<th>Version</th>
								<th>Reported On</th>
							</tr>
						</thead>
						<tbody>
							<?php if( isset( $unit->components ) && !empty( $unit->components ) ) foreach( $unit->components as $component ) :?>
							<tr <?=( $component->isImportant( ) ? 'class="ui-state-highlight"' : ( $component->isUnimportant( ) ? 'class="ui-state-disabled"' : '' ) )?>>
								<td><?=$component->component_name?></td>
								<td><?=$component->component_version?></td>
								<td><?=$component->reported_on?></td>
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
					<div class='ui-helper-clearfix' style='margin-top: 2px;'>
						<span class='components-showmore' style='float: right;'><span>Show more</span> components...</span>
					</div>
				</div>
			</div>
			
			<div>
				<div style='padding: 4px;' class='ui-corner-top ui-widget-header ui-state-default'>
					<span>Control Panel</span>
				</div>
				
				<div class='ui-corner-bottom ui-border ui-ruled' style='padding: 4px;'>
					<button class='rebootTerminal' value='<?=$unit->client_programid?>'>Reboot Terminal</button>
				</div>
			</div>
			
		</div>
	</div>
	
	<?php endforeach; ?>
	
</div></div>