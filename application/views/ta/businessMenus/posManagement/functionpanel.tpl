<div class='navPanelTitle'>Function Panel</div>
<div id="fpassign">
	<input type="hidden" id="fpbid" value="<?=$businessid?>" />
	<div class="divTable"><div class="divTableRow">
		<div class="divTableCell" style="text-align: right; position: relative;">
			<div style="width: 475px; display: inline-block;">
				<div class="stitle">Assigned Panels</div>
				<select id="fpanelAssigned" size="16" style="width: 100%;"></select>
			</div>
		</div>
		<div id="fpanelButtons" class="divTableCell" style="width: 132px;">
			<div class="stitle">&nbsp;</div>
			<div id="fpanelMoveUp" class="button">Move Up</div>
			<div id="fpanelMoveDown" class="button">Move Down</div>
			<div id="fpanelAddPage" class="button">Add Page</div>
			<div id="fpanelRemovePage" class="button">Remove Page</div>
		</div>
		<div class="divTableCell" style="text-align: left;">
			<div style="width: 475px; display: inline-block;">
				<div class="stitle">Available Panels</div>
				<select id="fpanelList" size="16" style="width: 100%;"></select>
			</div>
		</div>
	</div></div>
</div>
