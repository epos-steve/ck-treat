<div id="dialog-form" title="Edit Customer Balance" style="display: hidden;">
	<form>
		<div>
			<input type="hidden" id="cid" name="cid" />
			<input type="hidden" id="bid" name="bid" />
			<label for="cname">Customer: </label>
			<label name="cname" id="cname" />
		</div>
		<div>
			<label for="cbalance"> Balance: </label>
			<input class="dialog-state-color" type="text" name="cbalance" id="cbalance" class="text ui-widget-content ui-corner-all" />
		</div>
		<div class="dialog-state-color" id="message">
		</div>
	</form>
</div>

	<div class='navPanelTitle'>Customers</div>
	<table width='100%'>
		<tr valign='top'>
			<td width='25%'>
				<div id='customer_list'
					style="height:275px; width:100%; overflow:auto;">
<?php
	foreach( $customerList as $customerInfo ) :
?>
					<a 	href="#"
						id='cust<?=$customerInfo->customerid?>'
						onclick="set_customer(<?=$customerInfo->customerid?>,
												'<?=$customerInfo->last_name?>',
												'<?=$customerInfo->first_name?>',
												'<?=$customerInfo->username?>',
												'<?=$customerInfo->scancode?>',
												'<?=$customerInfo->balance?>'); return false;"
						style="text-decoration:none;">
						<font color='black'>[<?=substr( $customerInfo->scancode, 6, 7 )?>] <?=$customerInfo->last_name?>, <?=$customerInfo->first_name?></font>
					</a><br>
<?php
	endforeach;
?>
				</div>
			</td>
			<td>
				<form action="#" method='post'>
					<input type='hidden' name='businessid' value='<?=$businessid?>' id='cust_bid' />
					<input type='hidden' name='customerid' value='0' id='customerid' />
					<input type='hidden' name='old_scancode' value='' id='old_scancode' />
					<table width='95%' style='margin-left: auto; margin-right: auto;'>
						<tr>
							<td style='text-align: right;'>
								Email:
							</td>
							<td>
								<input type='text' size='40' id='email' value='' />
							</td>
							<td rowspan='5' width='15%' align='right'>
								<div
									style="text-align: center; border: 2px solid black; background:#FFFF99;">
										<label>Balance</label>
										<br />
										$<label style="font-weight: bold" id='balance'>0.00</label>
								</div>
<?php if ((int)$_SESSION['USER']['security_level'] >= 8): ?>
								<div style="text-align: center;">
									<a href='#' id='editBalanceLink'>Edit Balance</a>
								</div>
<?php endif; ?>
							</td>
						</tr>
						<tr>
							<td style='text-align: right;'>
								Password:
							</td>
							<td>
								<input type='text' size='40' id='user_pass' value='' />
								<font size=2>*Leave blank if not changing</font>
							</td>
						</tr>
						<tr>
							<td style='text-align: right;'>
								First/Last Name:
							</td>
							<td>
								<input type='text' size='20' id='first_name' value='' />
								<input type='text' size='20' id='last_name' value='' />
							</td>
						</tr>
						<tr>
							<td style='text-align: right;'>
								Card#:
							</td>
							<td>
								<input type='text' size='20' id='card_num' value='' />
							</td>
						</tr>
						<tr>
							<td></td>
							<td>
								<input type='button' value='Submit' id='cust_submit' onclick="submit_customer(); return false;" />
								<input type='button' value='Clear' onclick="set_customer(0,'','','',''); return false;" />
							</td>
						</tr>
					</table>
				</form>

				<hr width="95%">

				<div>
					<div style="display: inline-block; width: 49%; text-align: right; vertical-align: top;">
						<form id="userFunctionForm" action="addUserFunction" method="post" onsubmit="return false;">
							<input type="hidden" name="cust_id" value="0" />
							<label for="userFunction">Add User Function:</label> <select name="userFunction">
								<option>---</option>
								<?php
									$fns = \EE\Model\Aeris\UserFunction::getCannedFunctionList( );
									foreach( $fns as $fnname => $fnspec ) { ?>
										<option><?=$fnname?></option>
									<?php }
								?>
							</select>
							<input type="submit" value="Add Function" />
						</form>
					</div>
					<div id="userFunctionList" style="display: inline-block; text-align: left; vertical-align: top;">
					</div>
				</div>

				<hr width='95%'>

				<div style='text-align: center;'>
					<form action="#" method='post'>
						<input type='hidden' name='cust_id' id='cust_id' value='0' />
						Add value to account:
						$<input type='text' size='3' name='dol_value' id='dol_value' value='' />
						Reason:<select name='transtype' id='transtype'>
							<option value='3'>Refund</option>
							<option value='4'>Promo</option>
<?php if ((int)$_SESSION['USER']['security_level'] >= 8): ?>
							<option value='7'>Balance Transfer</option>
							<option value='8'>CK Adjust</option>
<?php endif; ?>
						</select>
						<input type='submit' value='GO' id='trans_submit' onclick="submit_trans(); return false;">
					</form>
				</div>
                                
                                <hr width='95%'>
<?php if ((int)$_SESSION['USER']['security_level'] > 8): ?>
                                <div style="width:95%;text-align: center;" >
                                    <form action="#" method='post'>
                                        <input type='hidden' name='cust_id2' id='cust_id2' value='0' />
                                        <input type="text" name="filterDate1" id='filterDate1' value="" size="12" class="datepicker">
                                        <input type="text" name="filterDate2" id='filterDate2' value="" size="12" class="datepicker">
                                        <input type="button" value='Filter' id='trans_filter' onclick="filter_trans();">
                                    </form>
                                </div>
<?php endif; ?>
				<div id='cust_trans' style="width:95%;text-align: center;"></div>

			</td>
		</tr>
	</table>