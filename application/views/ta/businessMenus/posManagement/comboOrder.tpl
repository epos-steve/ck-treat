

<div class='navPanelTitle' >Combos Groups</div>
	<div class="divTable">
	
		<div class="divTableRow">
		
			<div class='divTableCell' >
			
				<!-- tabla for Groups -->
				<div class='settingsContainer' >
						<table border=0>

							<tr>
								<td class='mark' style='text-align: center;' >Group </td>
								<td class='mark' style='text-align: center; width:620px' >Group Elements</td>
								<td class='' style='text-align: center;' ></td>
								
							</tr>
							<tr>
								<td>
									<select  size='16' id='comboListOrder' >
										<?php foreach ($promoGroupsList as $promoGroup): ?>
											<option value="<?php echo $promoGroup->id; ?>"><?php echo $promoGroup->name; ?></option>
										<?php endforeach; ?>		
									</select>
					
								</td>
								<td valign='top' style="width: 400px;">
									<!-- tabla for Group Elements -->
									<!-- tables inside this DIV could have draggable content -->


									<div id="drag" valign='top'>
										<table id="table">
											<colgroup><col width="100"/><col width="100"/><col width="100"/><col width="100"/><col width="100"/></colgroup>
										</table>
										<br>
										<table >
											<button id='btnSaveOrderCombo' style="display: none;" value='table'>Save Order</button>
										</table>


									</div>
									<div id='error'></div>

									
									
									
									
									
									
							
								</td>
								<td valign='top'>
								
											<div> 
												
												<table class="ui-picker-dropdown ui-widget-content ui-corner-all" style="display: block; width: 90px;font-size: 11pt; ">
													<tr >
															<td class='mark' colspan=5>
																Button Color:
															</td>
														</td>
														
													</tr>

													<tr class="ui-picker-dropdown-row" id="picker-20399-row-0">
															<td id = 'color1' onclick="select_color(this)" class="product-button-color-0" style="width: 18px; height: 18px; position: static; float: none;  margin-left: 12px;cursor: pointer">
															</td>
															<td id = 'color2' onclick="select_color(this)" class="product-button-color-1" style="width: 18px; height: 18px; position: static; float: none;  margin-left: 12px;cursor: pointer">
															</td>
															<td id = 'color3' onclick="select_color(this)" class="product-button-color-2" style="width: 18px; height: 18px; position: static; float: none;  margin-left: 12px;cursor: pointer">
															</td>
															<td id = 'color4' onclick="select_color(this)" class="product-button-color-3" style="width: 18px; height: 18px; position: static; float: none;  margin-left: 12px;cursor: pointer">
															</td>
														</td>
														
													</tr>
													<tr class="ui-picker-dropdown-row" id="picker-20399-row-0">
															<td id = 'color4' onclick="select_color(this)" class="product-button-color-4" style="width: 18px; height: 18px; position: static; float: none;  margin-left: 12px;cursor: pointer">
															</td>
															<td id = 'color5' onclick="select_color(this)" class="product-button-color-5" style="width: 18px; height: 18px; position: static; float: none;  margin-left: -12px;cursor: pointer">
															</td>
															<td id = 'color6' onclick="select_color(this)" class="product-button-color-6" style="width: 18px; height: 18px; position: static; float: none;  margin-left: 12px;cursor: pointer">
															</td>
															<td id = 'color7' onclick="select_color(this)" class="product-button-color-7" style="width: 18px; height: 18px; position: static; float: none;  margin-left: 12px;cursor: pointer">
															</td>
														</td>
														
													</tr>
													<tr class="ui-picker-dropdown-row" id="picker-20399-row-0">
															<td  id = 'color8' onclick="select_color(this)" class="product-button-color-8" style="width: 18px; height: 18px; position: static; float: none;  margin-left: 12px;cursor: pointer">
															</td>
															<td id = 'color9' onclick="select_color(this)" class="product-button-color-9" style="width: 18px; height: 18px; position: static; float: none;  margin-left: 12px;cursor: pointer">
															</td>
															<td id = 'color10' onclick="select_color(this)" class="product-button-color-10" style="width: 18px; height: 18px; position: static; float: none;  margin-left: -12px;cursor: pointer">
															</td>
															<td id = 'color11' onclick="select_color(this)" class="product-button-color-11" style="width: 18px; height: 18px; position: static; float: none;  margin-left: 12px;cursor: pointer">
															</td>
														</td>
														
													</tr>
												</table>					
											</div>
								</td>
							</tr>
						</table>



				</div>
		
			
			</div>
		
			

			
			

	
<style type="text/css"> 
/* add bottom margin between tables */
#table1,
#table2 {
	margin-bottom: 20px;
}

/* drag container */
#drag {
	margin: auto;
	width: 530px;
}

/* set border for images inside DRAG region - exclude image margin inheritance */
/* my WordPress theme had some funny margin settings */
#drag img {
	margin: 1px;
}

/* drag objects (DIV inside table cells) */
.drag {
	cursor: move;
	margin: auto;
	margin-bottom: 1px;
	margin-top: 1px;
	z-index: 10;
	background-color: white;
	text-align: center;
	font-size: 10pt; /* needed for cloned object */
	width: 105px;
	height: 40px;
	line-height: 20px;
	/* round corners */
	border-radius: 4px; /* Opera, Chrome */
	-moz-border-radius: 4px; /* FF */
}


/* drag objects border for the first table */
.t1 {
	border: 2px solid #499B33;
}
/* drag object border for the second table */
.t2 {
	border: 2px solid SteelBlue;
}
/* cloned objects - third table */
.t3 {
	border: 2px solid #FF8A58;
}
/* allow / deny access to cells marked with 'mark' class name */
.mark {
	color: white;
	background-color: #9B9EA2;
}
/* trash cell */
.trash {
	color: white;
	background-color: #2D4B7A;
}

/* tables */
div#drag table {
	background-color: #e0e0e0;
	border-collapse: collapse;
}


/* input elements in dragging container */
div#drag input {
	cursor: auto;
}
	/* height for input text in DIV element */
	div#drag #d13 input {
		height: 13px;
	}
	/* height for dropdown menu in DIV element */
	div#drag #d5 select {
		height: 20px;
	}

/* table cells */
div#drag td {
	height: 58px;
	border: 1px solid white;
	text-align: center;
	font-size: 11pt;
	padding: 2px;
}

/* "Click" button */
.button {
	margin-bottom: 2px;
	background-color: #6A93D4;
	color: white; 
	border-width: 1px;
	width: 44px;
	padding: 0px;
}


/* toggle checkboxes at the bottom */
.checkbox {
	margin-left: 13px;
	margin-right: 14px;
	width: 13px; /* needed for IE ?! */
}


/* message cell */
.message_line {
	padding-left: 10px;
	margin-bottom: 3px;
	font-size: 10pt;
	color: #888;
}


</style> 
	

		
</div>