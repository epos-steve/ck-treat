	<div class='navPanelTitle'>Cold Food Orders</div>
<?php
	if( $routeMachine && $routeMachine['machine_num'] != '' ) :
?>
		<div style="background-color: orange; border: 1px solid black;">
			&nbsp;Order for <?=$routeInfo['showday']?> (<?=$routeInfo['order_date']?>) for Route# <?=$routeMachine['route']?> [<?=$routeMachine['machine_num']?>]&nbsp;|&nbsp;
			<i>Next Order for <?=$routeInfo['shownextday']?> (<?=$routeInfo['next_order_date']?>) in <?=$routeInfo['date_count']?> Day(s)</i>&nbsp;
		</div>
	
<?php
		if( $routeInfo['scheduleid'] > 0 ) :
?>
		<form action='/ta/savekioskorder.php' method='post' style="margin:0;padding:0;display:inline;">
			<input type='hidden' name='businessid' value='<?=$GLOBALS['businessid']?>' />
			<input type='hidden' name='companyid' value='<?=$GLOBALS['companyid']?>' />
			<input type='hidden' name='order_date' value='<?=$routeInfo['order_date']?>' />
			<input type='hidden' name='order_bus' value='<?=$routeInfo['order_bus']?>' />
			<input type='hidden' name='login_routeid' value='<?=$routeMachine['login_routeid']?>' />
			<input type='hidden' name='menu_typeid' value='<?=$routeMachine['menu_typeid']?>' />
			<input type='hidden' name='machineid' value='<?=$routeMachine['machineid']?>' />
	
			<table width='100%'>
				<tr valign='top'>
					<td width='100%'>
<?php
			foreach( $menuItemList as $menuGroup ) :
?>
						<table cellspacing='0' style="border: 1px solid black; font-size: 12px; width: 100%;">
							<tr bgcolor='black'>
								<td colspan='10' style='color: white; font-weight: bold;'>
										&nbsp;<?=$menuGroup[0][0]['groupname']?>
								</td>
							</tr>
<?php
				foreach( $menuGroup as $menuPGroup ) :
?>
							<tr bgcolor='#FFFF99'>
								<td colspan='4'>
									<i>&nbsp;<?=$menuPGroup[0]['menu_pricegroupname']?></i>
								</td>
								<td title='Roll Over'>
									<i>RO</i>
								</td>
								<td title='2 Week Daily Average'>
									<i>AVG</i>
								</td>
								<td title='Current OnHand'>
									<i>OHD</i>
								</td>
								<td title='In Transit'>
									<i>TRN</i>
								</td>
								<td title='Suggested'>
									<i>SGT</i>
								</td>
								<td>
									<i>Order</i>
								</td>
							</tr>
<?php
					foreach( $menuPGroup as $menuItem ) :
?>
							<tr onmouseover="this.bgColor='#FFFF00'" onmouseout="this.bgColor='#FFFFFF'">
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td><?=$menuItem['item_code']?></td>
								<td><?=$menuItem['item_name']?></td>
								<td>
									<input type='text' name='rollover<?=$menuItem['myid']?>' size='5' value='<?=$menuItem['rollover']?>' tabindex="1" style="font-size: 10px;" <?=$menuItem['amt_disable']?> />
								</td>
								<td><?=$menuItem['avg_daily_sales']?></td>
								<td><?=$menuItem['onhand']?></td>
								<td><?=$menuItem['inTransit']?></td>
								<td title='<?=$menuItem['show_suggested']?>' style='color: <?=$menuItem['fcolor']?>;'><?=$menuItem['suggested']?></td>
								<td>
									<input type='text' name='amount<?=$menuItem['vend_itemid']?>' size='5' value='<?=$menuItem['amount']?>' style="font-size: 10px;" <?=$menuItem['amt_disable']?> />
								</td>
							</tr>
<?php
					endforeach;
				endforeach;
?>
						</table>
<?php
			endforeach;
?>
					</td>
				</tr>
				<tr>
					<td align='right' colspan='10'>
						<input type='submit' value='Save' />
					</td>
				</tr>
			</table>
		</form>
<?php
		else :
?>
		<p style='text-align: center; font-weight: bold; color: red;'>
			This date is not a scheduled stop.
		<p>
<?php
		endif;
	else :
?>
		No machine setup for Cold Food&nbsp;
<?php
	endif;
?>
