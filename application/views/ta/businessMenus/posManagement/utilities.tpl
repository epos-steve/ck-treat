<script src="/assets/js/jquery.form.3.18.js"></script>
<script>
function show_help_import(){
	jQuery('#import_dialog').dialog({width:750,height:485});
}

function show_help_export(){
	jQuery('#export_dialog').dialog({width:750,height:485});
}

function show_loading(){
	jQuery('#upload_btn').attr('disabled', 'disabled');
	jQuery('#loading_dialog').dialog({width:400,height:200,modal:true});
	jQuery('#output').html('');
}

function show_complete(responseText){
	jQuery('#output').html(responseText);
	jQuery('#upload_btn').removeAttr("disabled");
	jQuery('#loading_dialog').dialog('close');
}

function toggle_skipped(){
	jQuery('#skipped').toggle();
}

function toggle_errors(){
	jQuery('#errors').toggle();
}
	
jQuery(document).ready(function() {
	var options = {
		forceSync: true,
		beforeSubmit: show_loading,
		success: show_complete
	};
	
	jQuery('#utilImportForm').ajaxForm(options);
});
</script>

<div class='navPanelTitle'>Customer Utilities</div>
<div class="divTable">
	<div class="divTableRow">
		<div class='divTableCell' style='width: 40%;'>
			<div class='settingsHeader ui-state-default'>Upload</div>
			
			<form action="custUtilImport" class="settingsForm" id="utilImportForm" enctype="multipart/form-data" method="post">
				<label for="upload">Update/Create File</label>
				<input type="file" name="upload" id="upload" size="25">
				<input type="hidden" name="bid" value="<?=$businessid?>" />
				<br /><br />	
				<input id="upload_btn" type="submit" value="Upload" />
				<button onclick="show_help_import(); return false;">?</button>
			</form>
			
			<div id="output"></div>
		</div>

		<div class='divTableCell' style='width: 20%;'>
			&nbsp;
		</div>
		
		<div class='divTableCell' style='width: 40%;'>
			<div class='settingsHeader ui-state-default'>Download</div>
			
			<br />
			
			<form action="/ta/utilities/customerExport" method="post">
				<input type="hidden" name="bid" value="<?=$businessid?>" />
				<input type="submit" value="Export Customers" />
				<button onclick="show_help_export(); return false;">?</button>
			</form>
		</div>
	</div>
</div>

<div style="display:none;" id="import_dialog" title="Importing Customer Files">
	<div style="float:left;width:48%;">
		<h3>Customer Import Format</h3>
		
		<p>All customer files must be in CSV (comma-separated value) format.</p>
		<p>Customer files MUST include a header row at the top with the values of "fname", "lname", "email", "scancode", and "amount"
			or the import will fail.</p>
	</div>
	
	<div style="float:right;width:48%;">
		<h3>Required Fields</h3>
		
		<p>There are five required fields:</p>
		<ol type="A">
			<li>first name</li>
			<li>last name</li>
			<li>email address</li>
			<li>scancode
				<ol style="padding-left:0;margin-left:8px;">
					<li>Completed scancodes are 13 digits long and start with "780".</li>
					<li>Imported scancodes will have "780" and zeros appended to the beginning to fufill the 13 digits requirement.</li>
				</ol>
			</li>
			<li>promotion amount</li>
		</ol>
	</div>
	
	<div style="text-align:center;clear:both;">
		<img src="/assets/images/import_instructions.png" />
	</div>
	
	<div style="float:left;">
		<h3>Passwords</h3>
		
		<p>Temporary passwords for newly created customers will default to "essential1".</p>
		<p>Customers will need to create their passwords after logging in with their temporary passwords.</p>
	</div>
	
	<div style="float:left;">
		<h3>Customer Import Results</h3>
		
		<p>Results of the customer import are reported once the import is complete.</p>
		<p>Customer records can be added, updated, or skipped.</p>
		<ol type="A">
			<li><strong>Added</strong> - the customer was created and the promotion amount was saved to the system</li>
			<li><strong>Updated</strong> - the promotion amount was added to the current promotion amount in the system</li>
			<li><strong>Skipped</strong> - the promotion amount was not added to the system because of a problem (such as the promotion amount
				was zero). <strong>Skipped</strong> customers can be viewed by clicking on the "Show Skipped Customers" link.</li>
		</ol>
	</div>
	
	<div style="text-align:center;clear:both;">
		<img style="border:1px solid #000;" src="/assets/images/import_results.png" />
	</div>
</div>

<div style="display:none;" id="export_dialog" title="Exporting Customer Files">
	<div style="float:left;width:48%;">
		<h3>Customer File Export</h3>
	
		<p>Customer files are exported in CSV (comma-separated value) format.</p>
		<p>Customer files include a header row at the top with the values of "fname", "lname", "email", "scancode", and "amount".</p>
		<p>The promotion amounts are exported as zeroes.
	</div>
	
	<div style="float:right;width:48%;">
		<h3>Customer Export Files Used For Importing</h3>
	
		<p>The customer file export saves all of the customers to a CSV file with promotion amounts zeroed out.</p>
		<p>The format of the export exactly matches the required format of the customer import for ease of updating.</p>
		<p>After editing the promotion amounts on the export, import using the same file to add to the current customer promotion amounts.</p>
	</div>
	
	<div style="text-align:center;clear:both;">
		<img src="/assets/images/export_instructions2.png" />
	</div>
</div>

<div style="display:none;" id="loading_dialog" title="Processing">
	<!--<p>Customer data is uploading. Please wait...<img src="http://treat.dev.essential-elements.net/assets/images/ajax-loader-redmond.gif" /></p>-->
	<p>Customer data is uploading. Please wait...<img src="/assets/images/ajax-loader-redmond.gif" /></p>
</div>