<div class='navPanelTitle'>Modifiers</div>
<div class="divTable"><div class="divTableRow">
	<input type='hidden' name='currentModScreen' id='currentModScreen' value='__root__' />
	<input type='hidden' name='bid' id='modifierbid' value='<?=$businessid?>' />
	
	<div id='modifierEditGroupDialog' style='display: none;'>
		<form action='modifierEditGroup' id='modifierEditGroupForm' class='modifierForm' onsubmit='return false;' method='post'>
			<input type='hidden' name='bid' value='<?=$businessid?>' />
			<input type='hidden' name='modifierGroup_id' id='modifierGroup_id' value='0' />
			<fieldset class="ui-widget-content ui-helper-clearfix">
				<legend>Name</legend>
				<div>
					<label for='modifierGroup_name'>Group Name:</label>
					<input type='text' class='modifierOption' name='modifierGroup_name' id='modifierGroup_name' size='25' value='New Group' />
				</div>
			</fieldset>
			<fieldset class="ui-widget-content ui-helper-clearfix">
				<legend>Aeris 1</legend>
				<div>
					<label for='modifierGroup_type'>Selection Type:</label>
					<select class='modifierOption' name='modifierGroup_type' id='modifierGroup_type'>
						<option value='single'>Single</option>
						<option value='multiple'>Multiple</option>
					</select>
				</div>
				<div>
					<label for='modifierGroup_newLine'>Use new line?</label>
					<input type='checkbox' class='modifierOption' name='modifierGroup_newLine' id='modifierGroup_newLine' />
				</div>
			</fieldset>
			<fieldset class="ui-widget-content ui-helper-clearfix">
				<legend>Aeris 2</legend>
				<div>
					<label for='modifierGroup_type'>Min:</label>
					<span class='modifier_minus'></span>
					<input type='text' class='modifierOption' name='limit_min' size='3' value='0'
					       title='Minimum amount of modifiers that can be selected (0=Modifiers are Optional).'
					       style="position: relative; bottom: 2px" />
					<span class='modifier_plus'></span>
				</div>
				<div>
					<label for='modifierGroup_type'>Max:</label>
					<span class='modifier_minus'></span>
					<input type='text' class='modifierOption' name='limit_max' size='3' value='0'
					       title='Maximum amount of modifiers that can be selected (0=Unlimited).'
					       style="position: relative; bottom: 2px"/>
					<span class='modifier_plus'></span>
				</div>
				<div>
					<label for='modifierGroup_type'>Amount Free:</label>
					<span class='modifier_minus'></span>
					<input type='text' class='modifierOption' name='amount_free' size='3' value='0'
					       title='Amount of Modifiers to get Free.'
					       style="position: relative; bottom: 2px"/>
					<span class='modifier_plus'></span>
				</div>
			</fieldset>
		</form>
	</div>
	
	<div id='modifierEditItemDialog' style='display: none;'>
		<form action='modifierEditItem' id='modifierEditItemForm' class='modifierForm' onsubmit='return false;' method='post'>
			<input type='hidden' name='bid' value='<?=$businessid?>' />
			<input type='hidden' name='modifierItem_groupId' id='modifierItem_groupId' value='0' />
			<input type='hidden' name='modifierItem_id' id='modifierItem_id' value='0' />
			<fieldset class="ui-widget-content ui-helper-clearfix">
				<legend>Modifier</legend>
				<div>
					<label for='modifierItem_name'>Name:</label>
					<input type='text' class='modifierOption' name='modifierItem_name' id='modifierItem_name' size='25' value='New Modifier' />
				</div>
				<div>
					<label for='modifierItem_price'>Price Adjustment:</label>
					<input type='text' class='modifierOption' name='modifierItem_price' id='modifierItem_price' value='0.00' />
				</div>
				<div>
					<label for='modifierItem_preset'>Is Preset?</label>
					<input type='checkbox' class='modifierOption' name='modifierItem_preset' id='modifierItem_preset' />
				</div>
				<div>
					<label for='pos_color'>Button Color</label>
					<select name='pos_color' alt='Button Color' id='modifier-pos-color'
					        class='picker validate-type-color picker-prefix-product-button-color-'>
						<?php for( $i = 0; $i < 12; $i++ ) { ?>
							<option><?=$i?></option>
						<?php } ?>
					</select>
				</div>
			</fieldset>
			<fieldset class="ui-widget-content ui-helper-clearfix">
				<legend>Images</legend>
				<label for='use_product_image'>Use Product Image:</label>
				<input type='checkbox' name='use_product_image' /><br>
				<label for='hide_text'>Hide Text:</label>
				<input type='checkbox' name='hide_text' /><br>
				<label for='image_display_type'>Display Type:</label>
				<select name='image_display_type'>
						<?php foreach( $imageDisplayTypes as $value ) { ?>
							<option value='<?=$value?>'><?=$value?></option>
						<?php } ?>
					</select><br>
				<label for='icon_color'>Icon Color:</label>
				<input type='text' name='icon_color' id='icon-color-picker-modifier' size='8' />
			</fieldset>
			<span style="margin-top: 2px" id="modifierSaveButton">Save</span>
		</form>
        <div id="modifier-image-upload"></div>
	</div>
	
	<div class='divTableCell'>
		<div id='modifierBar' class='ui-widget browserBar'>
			<span>Viewing: <span class='title'></span></span>
<!--			<div id='modifierSettingsPlaceholder' class='buttonSettingsPlaceholder'>&nbsp;</div>-->
			<a id='modifierBrowserBack' style='display: none;'>Back</a>
			<a id='modifierDeleteGroup' style='display: none;'>Delete</a>
			<a id='modifierEditGroup' style='display: none;'>Edit</a>
			<a id='modifierNewGroup' style='display: none;'>New</a>
			<a id='modifierDeleteItem' style='display: none;'>Delete</a>
			<a id='modifierEditItem' style='display: none;'>Edit</a>
			<a id='modifierNewItem' style='display: none;'>New</a>
		</div>
		<div id='modifierItems' class='posItems'></div>
<!--		<div class='settingsContainer buttonSettings' id='modifierSettings'>-->
<!--			<div class='settingsHeader ui-state-default'>Button Settings</div>-->
<!--			-->
<!--			<form action='updateModifierButton' class='settingsForm buttonSettingsForm noRefresh' id='modifierButtonForm' onsubmit='return false;' method='post'>-->
<!--				<input type='hidden' name='bid' id='modifierbuttonbid' value='--><?//=$businessid?><!--' />-->
<!--				<input type='hidden' name='modbutton_id' id='modbutton_id' value='0' />-->
<!--				<input type='hidden' name='modbutton_type' id='modbutton_type' value='' />-->
<!--				<input type='hidden' name='modbutton_display_order' id='modbutton_display_order' value='' />-->
<!--				<input type='hidden' name='modbutton_promo_group_id' id='modbutton_promo_group_id' value='' />-->
<!--				-->
<!--				<label for='modbutton_name'>Name:</label>-->
<!--				<input disabled='disabled' type='text' size='14' name='modbutton_name' id='modbutton_name' value='' />-->
<!--				<br />-->
<!--				-->
<!--				<label for='modbutton_pos_color_selector'>Color:</label>-->
<!--				<input class='colorSelectorValue' type='hidden' name='modbutton_pos_color' id='modbutton_pos_color' value='default' />-->
<!--				<a id='modbutton_pos_color_selector' class='screenOption'><span class='colorSelector'></span></a>-->
<!--				<br />-->
<!--				-->
<!--				<input type='hidden' name='callback' value='ModifierController.saveModifierButton' />-->
<!--				-->
<!--				<input style='float: right;' class='screenOption' type='submit' id='modbutton_submit' value='Save' />-->
<!--				-->
<!--			</form>-->
<!--		</div>-->
		
		<div id='modColorSelectorPanel' class='colorSelectorPanel' style='display: none;'>
			<div class='product-button-color-default'></div>
			<div class='product-button-color-1'></div>
			<div class='product-button-color-2'></div>
			<div class='product-button-color-3'></div>
			
			<br />
			
			<div class='product-button-color-4'></div>
			<div class='product-button-color-5'></div>
			<div class='product-button-color-6'></div>
			<div class='product-button-color-7'></div>
			
			<br />
			
			<div class='product-button-color-8'></div>
			<div class='product-button-color-9'></div>
			<div class='product-button-color-10'></div>
			<div class='product-button-color-11'></div>
		</div>
	</div>
	
</div></div>