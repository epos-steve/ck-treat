<div class='navPanelTitle'>Promotions</div>
<div class="divTable"><div class="divTableRow">
	<div class='divTableCell' style='width: 200px;'>
		<select style='width: 100%;' size='16' id='promoList'></select>
		<div>
			<div>
				<span id='movePromoUp' class='promoMove'>Move Up</span>
				<span id='movePromoDown' class='promoMove'>Move Down</span>
			</div>
			<input type='checkbox' id='showHidePromos' /><label id='showHidePromosLabel' for='showHidePromos'>Show Inactive</label>
		</div>
	</div>

	<div class='divTableCell'>
		<div class='settingsContainer'>
			<div class='settingsHeader ui-state-default'>Basic Settings</div>
			<form class='settingsForm' id='promoForm' onsubmit='return false;' method='post'>
				<input type='hidden' name='bid' id='promobid' value='<?=$businessid?>' />
				<input type='hidden' name='promo_id' id='promo_id' value='0' />
				<input style='float: right;' class='promoOption' type='submit' id='promo_submit' value='Save' />

				<label for='promo_name'>Name:</label>
				<input class='promoOption' type='text' size='15' name='promo_name' id='promo_name' value='' />

				<label for='promo_tax'>Aeris1 Tax Rate:</label>
				<select class='promoOption' name='promo_tax' id='promo_tax'>
					<option value="">None</option>
<?php
	foreach( $taxList as $taxInfo ) :
?>
					<option value='<?=$taxInfo->id?>'><?=$taxInfo->name?></option>
<?php
	endforeach;
?>
				</select>

				<div class='settingsItem'>
					<input class='promoOption' type="checkbox" name='pre_tax' id='pre_tax' value='1' />
					<label for='pre_tax'>Aeris2 (Tax Post Discount Amount)</label>
				</div>
				<div class='settingsItem'>
					<input class='promoOption' type="checkbox" name='promo_reserve' id='promo_reserve' value='1' />
					<label for='promo_reserve'>Reserve Item</label>
				</div>
				<br />

				<button style='float: right; clear: both;' class='promoOption' id='deletePromo'>Deactivate</button>

				<label for='promo_trigger'>Trigger:</label>
				<select class='promoOption' name='promo_trigger' id='promo_trigger'>
<?php
	foreach( $promoTargetList as $promoTargetInfo ) :
?>
					<option value='<?=$promoTargetInfo->id?>'><?=$promoTargetInfo->name?></option>
<?php
	endforeach;
?>
				</select>

				<span class='promoAmount'>
					<select class='promoOption' name='promo_trigger_amount_limit' id='promo_trigger_amount_limit'>
						<option value='single'>Once per transaction</option>
						<option value='multiple'>Unlimited</option>
					</select>
				</span>

				<span class='promoAmount'>
					<br />
				</span>

				<span class='promoAmount'>
					<label for='promo_trigger_amount_type'>Trigger On:</label>
					<select class='promoOption' name='promo_trigger_amount_type' id='promo_trigger_amount_type'>
						<option value='by_qty'>Quantity</option>
						<option value='by_subtotal'>Subtotal</option>
					</select>
				</span>

				<span class='promoAmount'>
					<label for='promo_trigger_amount'>Value:</label>
					<input type='text' name='promo_trigger_amount' id='promo_trigger_amount' size='5' />
				</span>

				<span class='promoAmount2'>
					<label for='promo_trigger_amount_2'>Value 2:</label>
					<input type='text' name='promo_trigger_amount_2' id='promo_trigger_amount_2' size='5' />
					<label for='promo_trigger_amount_3'>Value 3:</label>
					<input type='text' name='promo_trigger_amount_3' id='promo_trigger_amount_3' size='5' />
				</span>

				<br />

				<label for='promo_type'>Promotion Type:</label>
				<select class='promoOption' name=promo_type id='promo_type'>
<?php
	foreach( $promoTypeList as $promoTypeList ) :
?>
					<option value='<?=$promoTypeList->id?>'><?=$promoTypeList->name?></option>
<?php
	endforeach;
?>
				</select>

				<span class='promoDiscount promo_amount_off promo_fixed_value promo_cheapest_one_free promo_discount_after_n promo_percentage_off'>
					<label for='promo_value'>Value:</label>
					<input class='promoOption' type='text' name='promo_value' id='promo_value' value='' size=6 />
				</span>

				<span class='promoDiscount promo_amount_off promo_fixed_value'>
					<label for='promo_discount'>Discount:</label>
					<select class='promoOption' id='promo_discount' name='promo_option_discount'>
						<option value='by_qty'>Each Item</option>
						<option value='by_subtotal'>Item Subtotal</option>
					</select>
				</span>

				<span class='promoDiscount promo_cheapest_one_free promo_discount_after_n'>
					<br />
					<label for='promo_discount_type'>Discount Type:</label>
					<select class='promoOption' id='promo_discount_type' name='promo_option_discount_type'>
						<option value='by_fixed_price'>Fixed Price</option>
						<option value='by_amount_off'>Amount Off</option>
						<option value='by_percentage_off'>Percent</option>
					</select>
				</span>

				<span class='promoDiscount promo_cheapest_one_free promo_discount_after_n'>
					<label for='promo_discount_limit'>Limit:</label>
					<input type='text' class='promoOption' id='promo_discount_limit' name='promo_option_discount_limit' size='5' />
				</span>

				<span class='promoDiscount promo_discount_after_n'>
					<label for='promo_discount_n'>Discount N:</label>
					<input type='text' class='promoOption' id='promo_discount_n' name='promo_option_discount_n' size='5' />
				</span>
				<br />
				<span class='subsidy'>
					<input type='checkbox' name='subsidy' id='subsidy' value=1 />Subsidy
				</span>
			</form>
			</div>

			<div id='promo_schedule'></div>
			<div id='promo_detail'></div>
		</div>
	</div>
</div>