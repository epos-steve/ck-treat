<div class='navPanelTitle'>Menu Manager</div>
<div class="divTable"><div class="divTableRow">
	<div id='menuPage' class='divTableCell'>
		<div class='ui-helper-clearfix ui-widget' style='margin: 4px;'>
			<div id='menuManagerBar' class='ui-corner-all ui-widget-header'>
				<a href='/ta/busordermenu10.php?bid=<?=$businessid?>&cid=<?=$companyid?>'>Edit Schedule</a>
			</div>
		</div>
		<div id='menuPanelGroup'>
			<div alt='Missing Specials' class='ui-helper-clearfix ui-panelgroup-open'>
				<form id='menuSpecialForm' action='mergeMenuSpecial' method='post' onsubmit='return false;'>
					<fieldset>
						<table id='menuMissingItems' style='width: 100%; border-collapse: collapse;'>
							<thead style='font-weight: bold;'>
								<tr>
									<td style='width: 48px; border-bottom: solid 1px black;'><input type="checkbox" id="selectMissingItems" title="Select All" /></td>
									<td style='border-bottom: solid 1px black;'>Item Name</td>
									<td style='width: 256px; border-bottom: solid 1px black;'>Item Code</td>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</fieldset>
					<fieldset>
						<input type='hidden' name='bid' id='missingbid' value='<?=$businessid?>' />
						<input type='submit' value='Copy Selected Items' />
						<input type='button' value='Refresh' id='refreshMissingSpecials' onclick='return false;' />
					</fieldset>
				</form>
			</div>
			<div alt='Merge Items' class='ui-helper-clearfix ui-panelgroup-open'>
				<form id='menuMergeForm' class='settingsForm' action='mergeMenuItems' method='post' onsubmit='return false;'>
					<fieldset>
						<table id='menuMergeItems' style='width: 100%; border-collapse: collapse;'>
							<thead style='font-weight: bold;'>
								<tr>
									<td style='border-bottom: solid 1px black;'>Catering Item</td>
									<td style='border-bottom: solid 1px black; width: 256px;'>Item Code</td>
									<td style='border-bottom: solid 1px black;'>Kiosk Items</td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td colspan='3'>Please click <i>Refresh</i> to retrieve the list of items.</td>
								</tr>
							</tbody>
						</table>
					</fieldset>
					<fieldset>
						<input type='hidden' name='bid' id='mergebid' value='<?=$businessid?>' />
						<input type='submit' value='Link Selected Items' />
						<input type='button' value='Refresh' id='refreshMergeItems' onclick='return false;' />
					</fieldset>
				</form>
			</div>
			</div>
	</div>

</div></div>
