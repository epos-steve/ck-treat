<div class='navPanelTitle'>Setup</div>
<div class="divTable"><div class="divTableRow">
	
	<?php if( 1 == 2 ) :?>
	<div id='setupDisplayFormUnavailable' class='ui-widget' style='margin-left: 20px; margin-right: 20px;'>
		<h3 class='ui-state-error ui-corner-all' style='height: 26px; padding: 2px 8px;'>
			<span class='ui-icon ui-icon-alert' style='display: inline-block; vertical-align: middle;'></span>
			<span class='ui-text' style='vertical-align: middle;'>Settings Update Required</span>
		</h3>
		
		<p>
			The settings available on EE have changed. In order to avoid overwriting existing settings on this terminal,
			the terminal must report its current settings back to EE.
		</p>
		
		<p>
			The terminal has not yet reported its settings.<br />
			<input type='hidden' id='setupbid' value='<?=$businessid?>' />
			<button id="setupRequestSettings" onclick="return false;">Request a settings report</button>
		</p>
		
		<p>
			Please check back in 15 minutes, in which time the terminal should report back. If after 15 minutes
			you still see this message here, there may be a problem with the terminal's internet connection; please
			contact EE Technical Support.
		</p>
	</div>
	<?php else: ?>
	<form id='setupDisplayForm' class='settingsForm' action='updateSetupDisplay' method='post' onsubmit='return false;'>
		<div id='setupPanelGroup'>
			<div alt='Display' class='ui-helper-clearfix ui-panelgroup-open'>
				<fieldset>
					<legend>Department Panel</legend>
					<input type='text' size='1' name='dept_rows' alt='Rows' class='slider-1-12 validate-type-integer' />
					<input type='text' size='1' name='dept_cols' alt='Cols' class='slider-1-12 validate-type-integer' />
					<input type='checkbox' name='dept_scrollbar' alt='Hide Scrollbar' />
				</fieldset>
				
				<fieldset>
					<legend>Product Panel</legend>
					<input type='text' size='1' name='plu_rows' alt='Rows' class='slider-1-12 validate-type-integer' />
					<input type='text' size='1' name='plu_cols' alt='Cols' class='slider-1-12 validate-type-integer' />
					<input type='checkbox' name='plu_scrollbar' alt='Hide Scrollbar' />
				</fieldset>
				
				<fieldset>
					<legend>Miscellaneous</legend>
					<input type='checkbox' name='misc_register' alt='Show register on right' />
				</fieldset>
			</div>
			
			<div alt='Features' class='ui-helper-clearfix'>
				<fieldset>
					<legend>Timeout</legend>
					<input type='checkbox' name='feat_timeout' alt='Use Timeout Feature' />
					<input type='text' size='3' name='feat_timeout_secs' alt='Timeout length (in seconds)' />
					<input type='text' size='3' name='feat_timeout_countdown' alt='Confirmation countdown (in seconds)' />
				</fieldset>
				
				<fieldset>
					<legend>Optional Settings</legend>
					<input type='checkbox' name='feat_multilingual' alt='Bilingual Button' />
					<input type='checkbox' name='feat_dinein' alt='Dine In/Carry Out' />
					<input type='checkbox' name='feat_fingerprint' alt='Fingerprint Reader' />
					<input type='checkbox' name='feat_voucher' alt='Payment Vouchers' />
					<input type='checkbox' name='feat_memberonly' alt='Member Only Promos' />
				</fieldset>
			</div>
			
			<div alt='Payment Processing' class='ui-helper-clearfix'>
				<fieldset>
					<legend>Credit Card Processor</legend>
					<div class='ui-helper-clearfix'>
						<select name='pay_ccprocessor' alt='Processor'>
							<option value='heartland'>Heartland</option>
							<option value='mercury'>Mercury</option>
							<option value='verifone'>Verifone</option>
						</select>
					</div>
					
					<hr />
					
					<div id='pay_ccprocessor_heartland' class='ui-ajaxedit-selectgroup'>
						<input type='text' name='pay_ccheartland_siteid' alt='Site ID' />
						<input type='text' name='pay_ccheartland_licenseid' alt='License ID' />
						<input type='text' name='pay_ccheartland_deviceid' alt='Device ID' />
						<input type='text' name='pay_ccheartland_username' alt='Username' />
						<input type='password' name='pay_ccheartland_password' alt='Password' />
					</div>
					
					<div id='pay_ccprocessor_mercury' class='ui-ajaxedit-selectgroup'>
						<input type='text' name='pay_ccmercury_merchantid' alt='Merchant ID' />
						<input type='text' name='pay_ccmercury_operatorid' alt='Operator ID' />
						<input type='password' name='pay_ccmercury_password' alt='Password' />
					</div>
					
					<div id='pay_ccprocessor_verifone' class='ui-ajaxedit-selectgroup'>
						<span>Verifone CC processing not currently supported</span>
					</div>
				</fieldset>
			</div>
			
			<div alt='Advanced' class='ui-helper-clearfix'>
				<fieldset>
					<legend>Branding</legend>
					<div class='group'>
						<input type='text' size='32' name='adv_logo' alt='Logo Path' />
						<input type='checkbox' name='adv_logo_disable' alt='Disable' />
					</div>
					
					<div class='group'>
						<input type='text' size='4' name='adv_branding' alt='Card Branding' />
						<input type='text' size='16' name='adv_branding_long' />
					</div>
				</fieldset>
				
				<fieldset>
					<legend>Currency</legend>
					<input type="text" size="32" name="adv_accepted_currencies" alt="Accepted Currencies" <?=($security < 9 ? 'disabled="disabled"' : '')?> />
					<input type="text" size="8" name="adv_default_currency" alt="Default Currency" <?=($security < 9 ? 'disabled="disabled"' : '')?> />
				</fieldset>
				
				<fieldset>
					<legend>Logging</legend>
					<input type='checkbox' name='adv_logging' alt='Enable Activity Logging' />
					<select name='adv_log_level' alt='Log Level'>
						<option value='ALL'>All</option>
						<option value='FUNCTION'>Functions</option>
						<option value='CASH'>Reloads</option>
						<option value='PURCHASE'>Purchases</option>
					</select>
				</fieldset>
			</div>
		</div>
		
		<fieldset>
			<input type='hidden' name='bid' id='displaybid' value='<?=$businessid?>' />
			<input type='hidden' id='setupTZOffset' value='<?=$businessTimezone?>' />
			<input type='hidden' id='setupRushStart' value='11' />
			<input type='hidden' id='setupRushEnd' value='14' />
			<input type='button' id='setupSyncAll' value='Sync All' style='float: right;' onclick="return false;" />
			<input type='button' id='setupResetAll' value='Reset All' style='float: right;' onclick="return false;" />
			<input type='checkbox' id='setupSyncFingerprint' name="fingerprint" class='syncExtras' alt='Sync Fingerprints' style='float: right;' />
			<input type='checkbox' id='setupSyncImages' name="images" class='syncExtras' alt='Sync Images' style='float: right;' />
			<input type='submit' value='Save' />
			<input type='reset' value='Cancel' />
		</fieldset>
	</form>
	<?php endif; ?>
	
	<!-- <div class='settingsContainer' id='advSettings'>
		<div class='settingsHeader ui-state-default'>Advanced Settings</div>
		<form action='updateSetupAdv' class='settingsForm' id='advForm' onsubmit='return false;' method='post'>
			<input type='hidden' name='bid' id='advbid' value='<?=$businessid?>' />
			<input class='setupOption' style='float: right;' type='submit' id='adv_submit' value='Save' />
			
			<label for='adv_logo'>Logo Path:</label>
			<input class='setupOption' type='text' size='32' name='adv_logo' id='adv_logo' value='' />
			
			<label for='adv_logo_disable_check'>Disable:</label>
			<input class='checkValue' type='hidden' name='adv_logo_disable' id='adv_logo_disable' value='' />
			<input class='setupOption' type='checkbox' name='adv_logo_disable_check' id='adv_logo_disable_check' />
			
			<br />
			
			<label for='adv_branding'>Card Branding:</label>
			<input class='setupOption' type='text' size='4' name='adv_branding' id='adv_branding' value='' />
			<input class='setupOption' type='text' size='16' name='adv_branding_long' id='adv_branding_long' value='' />
		</form>
	</div> -->
</div></div>
