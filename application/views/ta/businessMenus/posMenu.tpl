<?php
if($bmp_access['disable']){
	$disable = 'disabled="disabled"';
	//$disable = 'readonly="readonly"';
} else {
	$disable = '';
}

if($bmp_access['disable_reorder']){
	$disable_reorder = 'disabled="disabled"';
	//$disable_reorder = 'readonly="readonly"';
} else {
	$disable_reorder = '';
}

if($bmp_access['copy_all_like']){
	$disable_copy = '';
} else {
	$disable_copy = 'disabled="disabled"';
	//$disable_copy = 'readonly="readonly"';
}

if($bus_menu_pos == 0){
	print 'You don\'t have sufficient access to view this page';
} else{
?>

<style>
input[readonly], select[readonly], checkbox[readonly]{background-color:#F0F0F0 !important; color:#303030 !important;}
</style>

<table cellspacing='0' cellpadding='0' border='0' width='100%'>
	<tr bgcolor='#E8E7E7'>
		<td colspan='2' align='right'>
			<div id="import_dialog" class="ui-hidden"></div>
			
			<?php if( $bus_menu_pos == 4 ): ?>
				<a href="#" style="text-decoration:none;" onclick="import_menu(); return false;">Import</a> |
			<?php endif; ?>
			
			<a style="text-decoration:none;" href="/ta/export_menu.php?bid=<?php echo $GLOBALS['businessid'] ?>">
			<font color=blue>Export</font></a>&nbsp;
			
			<?php if( $bus_menu_pos >= 3 ): ?>
				<!--| <a style="text-decoration:none;" href="/ta/buscatermenu4-1.php?bid=<?php echo $GLOBALS['businessid'] ?>&cid=<?php echo $GLOBALS['companyid'] ?>">-->
				| <a style="text-decoration:none;" href="posMultiEdit?bid=<?php echo $GLOBALS['businessid'] ?>&cid=<?php echo $GLOBALS['companyid'] ?>">
				<font color=blue>Multi Item Edit</font></a>&nbsp;
			<?php endif; ?>
			
		</td>
	</tr>
	<tr style='height: 1px; background-color: black; vertical-align: top;'>
		<td colspan='2'>
			<img src='/assets/images/spacer.gif' alt='' />
		</td>
	</tr>
	<tr valign=top bgcolor=#E8E7E7>
		<td width=25% rowspan=3>
			<form action=posMenu method=post onSubmit='return disableForm(this);' style="margin: 0; padding: 0; display: inline;">
				<input type=hidden name=businessid value="<?php echo $GLOBALS['businessid']; ?>" />
				<input type=hidden name=companyid value="<?php echo $GLOBALS['companyid']; ?>" />
				
				<center>
					<select size=18 name=menu_items_new style="width: 255px;" onChange='changePage(this.form.menu_items_new)'>
						<?php print $menu_items_new; ?>
					</select>
							
					<?php print $showactive; ?>
					<?php print $showbuttons; ?>
				</center>
			</form>
		</td>

		<td>
			<!--<form action="savePosMenu" method="post" onSubmit="return disableForm(this);" style="margin:0;padding:0;display:inline;">-->
			<form action="savePosMenu" name="pos_form" id="pos_form" method="post" enctype="multipart/form-data" style="margin:0;padding:0;display:inline;">
				<input type="hidden" name="companyid" value="<?php echo $GLOBALS['companyid']; ?>" />
				<input type="hidden" name="businessid" value="<?php echo $GLOBALS['businessid']; ?>" />
				<input type="hidden" name="districtid" value="<?php echo $districtid; ?>" />
				<input type="hidden" name="editid" value="<?php echo $editid; ?>" />
				<input type="hidden" name="mip_id" value="<?php echo $mip_id; ?>" />
				<input type="hidden" name="new" value="<?php echo $new; ?>" />
				<input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
				
				<center>
					<table width="90%" cellspacing="0" cellpadding="0">
						<tr>
							<td valign="top">
								<table cellspacing="0" cellpadding="0">
									<tr>
										<td colspan="2">&nbsp;</td>
									</tr>
									<tr>
										<td align="right">Item Name:</td>
										<td><input type="text" name="item_name" size="25" value="<?php echo $item_name; ?>" <?php echo $disable; ?> /></td>
									</tr>
									<tr>
										<td align="right">Item Code:</td>
										<td><input type="text" name="item_code" value="<?php echo $item_code; ?>" size="25" <?php echo $disable; ?> /></td>
									</tr>
									<tr>
										<td align="right">UPC:</td>
										<td>
											<input type="text" name="upc" value="<?php echo $upc; ?>" size="15" <?php echo $disable; ?> />
											<button id="generateOpenUPC" <?php if( !$hasCashiers ) echo 'disabled="disabled"' ?> onclick="return false;">Open</button>
										</td>
									</tr>
									<tr>
										<td align="right">Current Price:</td>
										<!--<td><input type="text" name="kiosk_price" value="<?php echo $kiosk_price; ?>" size="25" <?php echo $disable; ?> /></td>-->
										<td><input type="text" name="kiosk_price" value="<?php echo $kiosk_price; ?>" size="25" disabled="disabled" /></td>
									</tr>
									<tr>
										<td align="right">Sale Unit:</td>
										<td>
											<select name="sale_unit" <?php echo $disable; ?>>
												<option value='unit' <?=$sale_unit['unit']?>>Each</option>
												<option value='lb' <?=$sale_unit['lb']?>>Pound (lb)</option>
											</select>
										</td>
									</tr>
									<tr>
										<td align="right">Group:</td>
										<td>
											<select name="group_id" <?php echo $disable; ?>>
												<?php print $group_id_select; ?>
											</select>
										</td>
									</tr>
									<tr>
										<td align="right">Taxes:</td>
										<td>
											<?php print $tax_checkboxes; ?>
										</td>
									</tr>
									<tr>
										<td align="right">EBT FS Allowed:</td>
										<td><input name="ebt_fs_allowed" type="checkbox" value="1" <?php echo $ebt_fs_allowed=='1' ? 'checked=checked' : ''; ?> /></td>
									</tr>
									<tr>
										<td colspan="2">&nbsp;</td>
									</tr>
									<tr>
										<td align="right">Add Image:</td>
										<td><input name="product_image" id="product_image" type="file" />
									</tr>
									<?php if ( $product_images ) { ?>
										<tr>
											<td align="right">Existing Images:</td>
											<td>
												<table cellspacing="0" cellpadding="0" style="border-spacing: 2px">
													<tr>
													<?php $i=0; ?>
													<?php foreach( $product_images as $image ) { ?>
													<?php if ($i % 2 == 0 && $i != 0) { ?>
													</tr><tr>
													<?php } ?>
														<td style="border: black solid 1px; padding: 2px; text-align: center"
															width="100" height="100" valign="bottom" align="right">
															<img src="<?php echo $image->url; ?>">
															<?php echo $image->image_size; ?>
															<input name="delete_image[]" type="checkbox" title="Delete Image?" value="<?php echo $image->number; ?>" />
														</td>
													<?php $i++; ?>
													<?php } ?>
													</tr>
												</table>
											</td>
										</tr>
									<?php } ?>
								</table>
							</td>
							<td valign="top">
								<table cellspacing="0" cellpadding="0">
									<tr>
										<td colspan="2">&nbsp;</td>
									</tr>
									<tr>
										<td align="right">Max:</td>
										<td><input type="text" name="max" value="<?php echo $max; ?>" size="25" <?php echo $disable_reorder; ?> /></td>
									</tr>
									<tr>
										<td align="right">Reorder Point:</td>
										<td><input type="text" name="reorder_point" value="<?php echo $reorder_point; ?>" size="25" <?php echo $disable_reorder; ?> /></td>
									</tr>
									<tr>
										<td align="right">Reorder Amount:</td>
										<td><input type="text" name="reorder_amount" value="<?php echo $reorder_amount; ?>" size="25" <?php echo $disable_reorder; ?> /></td>
									</tr>
									<tr>
										<td align="right">Cost:</td>
										<td><input type="text" name="cost" value="<?php echo $cost; ?>" size="25" <?php echo $disable; ?>></td>
									</tr>
									<tr>
										<td align="right">New Price:</td>
										<!--<td><input type="text" name="new_price" value="<?php echo $new_price; ?>" size="25" <?php echo $disable; ?> /></td>-->
										<td><input type="text" name="new_price" value="<?php echo $new_price; ?>" size="25" <?php echo $disable_reorder; ?> /></td>
									</tr>
									<tr>
										<td align="right">Tare:</td>
										<td>
											<input name='tare' type='text' value='<?=$tare?>' <?php echo $disable; ?> />
										</td>
									</tr>
									<tr>
										<td align="right">Order Type:</td>
										<td>
											<select name="ordertype" <?php echo $disable; ?> >
												<?php
												foreach($order_type_select as $row){
													if ($row->id == $ordertype){
														$row->sel = ' selected="selected"';
													} else {
														$row->sel = '';
													}
													echo '<option value="' . $row->id . '" ' . $row->sel . '>' . $row->name . '</option>';
												}
												?>
											</select>
										</td>
									</tr>
									<tr>
										<td align="right">Shelf Life:</td>
										<td>
											<input name='shelf_life_days' type='text' value='<?php echo $shelf_life_days; ?>' <?php echo $disable; ?> />
										</td>
									</tr>
									<tr>
										<td></td><td><input name="active" type="checkbox" value="1" <?php echo $show_active_check; ?> <?php echo $disable; ?> />Inactive &nbsp;</td>
									</tr>
									<tr>
										<td></td><td><input name="is_button" type="checkbox" value="1" <?php echo $show_button_check; ?> <?php echo $disable; ?> />Button &nbsp;</td>
									</tr>
									<tr>
										<td></td><td><input name="add_to_all" type="checkbox" value="1" <?php echo $disable_copy; ?> /><b>Copy to all like systems?</b>  &nbsp;</td>
									</tr>
									<tr>
										<td></td><td>&nbsp;</td>
									</tr>
									<tr>
										<td colspan="2">
											<?php if($bmp_access['save_access']): ?>
												<input type="submit" value="<?php echo $submit_button; ?>" size="25" />
												<button type="button" onclick="document.location.href='posMenu?cid=<?php echo $GLOBALS['companyid']; ?>&bid=<?php echo $GLOBALS['businessid']; ?>'; return false;">Cancel</button>
											<?php endif; ?>

											<?php if($editid > 0){
												echo "&nbsp;&nbsp;Items Sold: $items_sold";
											}
											?>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</center>
			</form>
		</td>
	</tr>	
	<tr bgcolor="#E8E7E7">
		<td colspan="1">
			<p><br>
			<form action="posMenu?bid=<?php print $GLOBALS['businessid']; ?>&cid=<?php print $GLOBALS['companyid']; ?>" method="post" style="margin:0;padding:0;display:inline;" onSubmit="return disableForm(this);">
				<input type="hidden" name="limit" value="<?php print $limit; ?>" />
				<input type="hidden" name="search" value="1" />
				<center>
					<div style="border:2px solid #CCCCCC; width:450px; background: #FFFFFF;">
						<div style="background:#CCCCCC;">
							<center>
								<b>Search <?php print $show_bad_search; ?></b>
							</center>
						</div>
						
						<select name="search_what">
							<option value="name" <?php print $sel1; ?>>Item Name</option>
							<option value="item_code" <?php print $sel2; ?>>Item Code</option>
							<option value="upc" <?php print $sel3; ?>>UPC</option>
						</select>
						
						<input type=text size="25" name="search_for" value="<?php print $search_for; ?>">
						
						<input type="submit" value="Search">
						
					</div>
				</center>
			</form>
		</td>
	</tr>	
	<tr valign="bottom" bgcolor="#E8E7E7">
		<td align="right">
			<form id="manage" action="/ta/update_pos.php" method="post" style="margin: 0; padding: 0; display: inline;">
				<div class="info"></div>
				<input type="hidden" id="businessid" name="businessid" value="<?php echo $GLOBALS['businessid']; ?>" />																								
			</form>
			&nbsp;
		</td>
	</tr>
</table>

<?php
}
?>
