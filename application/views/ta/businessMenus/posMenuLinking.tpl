<form action='/ta/posmenulink.php' method='post' onSubmit='return disableForm(this);' style='margin:0;padding:0;display:inline;'>
<input type='hidden' name='groupid' value=<?=$groupid?>' />
<input type='hidden' name='limit' value='<?=$limit?>' />
<input type='hidden' name='businessid' value='<?=$businessid?>' />
<input type='hidden' name='companyid' value='<?=$companyid?>'>
<table cellspacing='0' cellpadding='0' border='0' width='100%'>
	<tr bgcolor='#E8E7E7'>
		<td colspan='4'>&nbsp;</td>
	</tr>
	<tr height='1' bgcolor='black' valign='top'>
		<td colspan='4'>
			<img src='/assets/images/spacer.gif'>
		</td>
	</tr>
	<tr bgcolor='#FFFF99'>
		<td colspan='3'>&nbsp;
<?php 
$groupArray = array( );
foreach( $groupList as $group ) :
	ob_start( );
?>
			<a href='?bid=<?=$businessid?>&cid=<?=$companyid?>&groupid=<?=$group['pos_id']?>' style='<?=$style?>'>
				<span style='<?=$group['style']?>'><?=$group['group_name']?></span>
			</a>
<?php 
	$groupArray[] = ob_get_clean( );
endforeach;
echo implode( ' | ', $groupArray );
?>
		</td>
		<td align='right'>
<?php 
if( count( $groupArray ) > 0 ) :
?>
			<a href='?bid=<?=$businessid?>&cid=<?=$companyid?>&groupid=-1' style='<?=$style?>'>
				<span style='color: <?=$groupid == -1 ? 'red; font-weight: bold' : 'blue'?>;'>Unlinked Items</span>
			</a>&nbsp;
<?php 
endif;
?>
		</td>
	</tr>
	<tr bgcolor='#E8E7E7' height='1'>
		<td colspan='4'><img src='/assets/images/spacer.gif' /></td>
	</tr>
<?php 
if( count( $itemList ) == 0 ) :
?>
	<tr>
		<td colspan='4'>&nbsp;<i>Nothing to Display</i></td>
	</tr>
	<tr bgcolor='#E8E7E7' height='1'>
		<td colspan='4'>
			<img src='/assets/images/spacer.gif' />
		</td>
	</tr>
<?php
else:
	foreach( $itemList as $item ) :
		if( $item['tr'] ) :
?>
	<tr>
<?php 
		endif;
?>
		<td width='25%' bgcolor='<?=$item['color']?>' align='right' style='border-left:1px solid #CCCCCC;border-top:1px solid #CCCCCC;border-bottom:1px solid #CCCCCC;font-size:12px;'>
<?php 
		if( $item['menu_itemid'] == 0 ) :
?>
			<img src='/assets/images/error.gif' height='16' width='20' />
<?php 
		endif;
			?><?=$item['name']?>&nbsp;
		</td>
		<td bgcolor='<?=$item['color']?>' style='border-right:1px solid #CCCCCC;border-top:1px solid #CCCCCC;border-bottom:1px solid #CCCCCC;' width='25%'>
			<select name='item<?=$item['id']?>' style='font-size:12px;'>
				<option value='0'></option>
<?php 
		foreach( $item['options'] as $opt ) :
?>
				<option value='<?=$opt['menu_item_id']?>' <?=$opt['sel'] ? 'selected="selected"' : ''?>>
					<?=$opt['item_name']?>
				</option>
<?php 
		endforeach;
?>
			</select>
		</td>
<?php 
		if( $item['str'] ) :
?>
	</tr>
<?php 
		endif;
?>
	
<?php 
	endforeach;
endif;
if( $closeTr ) :
?>
		<td colspan='2'>&nbsp;</td>
	</tr>
<?php 
endif;
?>
	<tr bgcolor='#E8E7E7'>
		<td width='25%'>&nbsp; 
			<?=$showlimit?> - <?=$last?> of <?=$totalcount?>
		</td>
		<td colspan='2' width='50%' style='text-align: center;'>
<?php
if( $showPrev ) :
?>
			<a href='?bid=<?=$businessid?>&cid=<?=$companyid?>&groupid=<?=$groupid?>&limit=<?=$prevlimit?>' style='<?=$style?>'>
				<font color='blue'>Previous</font>
			</a>
<?php 
else:
?>
			Previous
<?php 
endif;
?> 
			| 
<?php 
if( $showNext ) :
?>
			<a href='?bid=<?=$businessid?>&cid=<?=$companyid?>&groupid=<?=$groupid?>&limit=<?=$nextlimit?>' style='<?=$style?>'>
				<font color='blue'>Next</font>
			</a>
<?php 
else:
?>
			Next
<?php 
endif;
?>
		</td>
		<td align='right' width='25%'>
			<input type='submit' value='Save' />
		</td>
	</tr>
</table>
</form>
