<h3>Import Menu Nutrition Format</h3>
<div>
	<p>All nutrition information must be in CSV (comma-separated value) format.</p>
	<p>Nutrition information files <strong>should</strong> include a header row at the top.</p>
</div>
<div>
	<p>There are 2 required fields and several optional fields:</p>
	<ol type="A">
		<li>Required
			<ol>
				<li>item_code</li>
				<li>name</li>
			</ol>
		</li>
		<li>Optional (any order)
			<ol>
				<li>calories</li>
				<li>cal_from_fat</li>
				<li>total_fat</li>
				<li>sat_fat</li>
				<li>cholesterol</li>
				<li>sodium</li>
				<li>potassium</li>
				<li>sugar</li>
				<li>fiber</li>
				<li>simple_carbs</li>
				<li>protein</li>
				<li>vit_C</li>
				<li>vit_A</li>
				<li>thiamine</li>
				<li>iron</li>
				<li>magnesium</li>
				<li>niacin</li>
				<li>phosphorus</li>
				<li>riboflavin</li>
				<li>unsat_fat</li>
				<li>calcium</li>
				<li>zinc</li>
			</ol>
		</li>
	</ol>
</div>
<div style="text-align: center; clear: both;">
	<img src="/assets/images/ta/import_menu_nutrition_help.png" alt="import menu nutrition csv image"/>
</div>
