<?php

$style = "text-decoration:none";

$showall = $_GET["showall"];

if( $showall == "" ) {
	$showall = isset( $_COOKIE["showall"] ) ? $_COOKIE["showall"] : '';
}
setcookie( 'showall', $showall );

extract( (array)$securityArray, EXTR_PREFIX_ALL, 'sec' );


?>
<?php if( $sec_bus_menu_pos_report > 1 ) : ?>
	<div id="dialog-upload" title="Upload File" style="display: none;">
		<center>
			<form id="dialog-form-upload" name="dialog-form-upload" action="/ta/buscatermenu5-1.php" method="post" enctype="multipart/form-data">
				<input type="hidden" id="step" name="step" value="U" />
				<input type="hidden" id="bid" name="bid" value="<?php echo @$businessid ?>" />
				<input type="file" name="file" />
				<div>Choose File to Upload</div>
			</form>
		</center>
		<table id="file_list" style="width: 100%;">
			<tr>
				<th colspan="3">
					<hr>
						<div>
							Upload Results <img class="trigger" src="/assets/images/help.png" width="20px"/>
						</div>
						<div class="bubbleInfo">
							<div class="popup">
							Files must be in CSV format and contain 6 columns in the following order:
							Item Code, Machine Number, Amount, Date, Time, Count/Fill/Waste.   Once the file is uploaded,
							each row will be inspected for errors and if any are present, processing will stop.  No data
							will be inserted into the database until after the rows are checked for accuracy.  If any errors
							are encountered, the error will be displayed and it must be fixed before the upload can be attempted again.
							Dates should be in the format MM/DD/YYYY and times should be in the format HH::MM AM/PM.
							</div>
						</div>
					<hr>
				</th>
			</tr>
		</table>
	</div>
	<div id="dialog-form" title="Add New Record" style="display: none;">
		<form id="dialog-form-data">
			<table>
				<tr>
					<td>Item</td>
					<td>
						<strong id="item-dialog-product"></strong>
						<input type="hidden" id="itemcode" name="itemcode" value="" />
						<input type="hidden" id="step" name="step" value="A" />
						<input type="hidden" id="bid" name="bid" value="<?php echo @$businessid ?>" />
					</td>
				</tr>
				<tr>
					<td>Date</td>
					<td>
						<select name="month" id="month" class="text ui-widget-content ui-corner-all" >
						<?php
						for( $x = 1; $x <= 12; $x++ )
							echo "<option " . ( date( "n" ) == $x ? "default=true selected" : "" ) . ">$x</option>";
						?>
						</select> /
						<select name="day" id="day" class="text ui-widget-content ui-corner-all" >
						<?php
						for( $x = 1; $x <= 31; $x++ )
							echo "<option " . ( date( "j" ) == $x ? "default=true selected" : "" ) . ">$x</option>";
						?>
						</select> /
						<select name="year" id="year" class="text ui-widget-content ui-corner-all" >
						<?php
						for( $x = date( "Y" ); $x > date( "Y" ) - 2; $x-- )
							echo "<option " . ( date( "y" ) == $x ? "default=true selected" : "" ) . ">$x</option>";
						?>
						</select>
					</td>
				</tr>
				<tr>
					<td>Time</td>
					<td>
						<select name="hour" id="hour" class="text ui-widget-content ui-corner-all" >
						<?php
						for( $x = 1; $x <= 12; $x++ )
							echo "<option " . ( date( "g" ) == $x ? "default=true selected" : "" ) . ">$x</option>";
						?>
						</select> :
						<select name="minute" id="minute" class="text ui-widget-content ui-corner-all" >
						<?php
						for( $x = 0; $x <= 59; $x++ )
							echo "<option>" . str_pad( $x, 2, "0", STR_PAD_LEFT ) . "</option>";
						?>
						</select>&nbsp;
						<select name="ampm" id="ampm" class="text ui-widget-content ui-corner-all" >
							<option <?php echo ( date( "A" ) == "AM" ? "default=true selected" : "" ); ?>>AM</option>
							<option <?php echo ( date( "A" ) == "PM" ? "default=true selected" : "" ); ?>>PM</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Type</td>
					<td>
						<select name="type" id="type" class="text ui-widget-content ui-corner-all" >
							<option>COUNT</option>
							<option>FILL</option>
							<option>WASTE</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Amount</td>
					<td><input type="text" name="amount" id="amount" value="" class="text ui-widget-content ui-corner-all" ></td>
				</tr>
				<tr>
					<td colspan="2" id="dialog-status"></td>
				</tr>
				<tr>
					<td colspan="2"><hr>Item History</td>
				</tr>
				<tr>
					<td colspan="2">
						<div id="item-dialog-history"></div>
					</td>
				</tr>
			</table>
		</form>
	</div>
	<div id="dialog-form-item" title="Change Item Amounts" style="display: none;">
		<form id="dialog-form-item-data">
			<input type="hidden" name="bid" value="<?php echo intval($_GET['bid']); ?>" id="dialog-form-item-data-bid" />
			<input type="hidden" name="id" value="" id="dialog-form-item-data-id" />
			<table>
				<tr>
					<td width="33%">Max</td>
					<td>
						<input type="number" name="max" value="0" id="dialog-form-item-data-max" />
					</td>
				</tr>
				<tr>
					<td>Reorder Amount</td>
					<td>
						<input type="number" name="reorder_amount" value="0" id="dialog-form-item-data-reorder-amount" />
					</td>
				</tr>
				<tr>
					<td>Reorder Point</td>
					<td>
						<input type="number" name="reorder_point" value="0" id="dialog-form-item-data-reorder-point" />
					</td>
				</tr>
				<tr>
					<td colspan="2" id="dialog-item-status"></td>
				</tr>
			</table>
		</form>
	</div>

	<input type='hidden' id='global-businessid' value='<?=$businessid ?>' />
	<input type='hidden' id='global-terminalno' value='<?=$_GET["terminal_no"]; ?>' />
<?php endif; ?>
<?php
echo "<center><table cellspacing=0 cellpadding=0 border=0 width=100%>";
echo "<tr bgcolor=#E8E7E7><td colspan=2>&nbsp;</td></tr>";
echo "<tr height=1 bgcolor=black valign=top><td colspan=2><img src='/assets/images/spacer.gif'></td></tr>";

/////multi kiosk????
$query = "SELECT kiosk_multi_inv FROM business WHERE businessid = $businessid";
$result = Treat_DB_ProxyOldProcessHost::query( $query );

$kiosk_multi_inv = mysql_result( $result, 0, "kiosk_multi_inv" );

$query = "SELECT useLose FROM business_setting WHERE business_id = $businessid";
$result = Treat_DB_ProxyOldProcessHost::query( $query );

$useLose = mysql_result( $result, 0, "useLose" );

$terminal_no = array( );
$terminal_name = array( );

$query = "USE mburris_manage";
$result = Treat_DB_ProxyOld::query_both( $query );

$query = "SELECT client_programid,name FROM client_programs WHERE businessid = $businessid AND db_name = '" . Treat_Config::singleton()->get('db', 'database') . "'";
$result = Treat_DB_ProxyOld::query( $query );

$counter = 0;
while( $r = mysql_fetch_array( $result ) ) {
	$terminal_no[$counter] = $r["client_programid"];
	$terminal_name[$counter] = $r["name"];
	$counter++;
}

$query = "USE ".Treat_Config::singleton()->get('db', 'database');
$result = Treat_DB_ProxyOld::query_both( $query );
/////end multi kiosk

/////choose report
$report = $_GET["report"];
$ordertype = $_GET["ordertype"];
$current_terminal = $_GET["terminal_no"];
$which = $_GET["which"];


if($report == ""){
	$report = $_COOKIE['report'];
}

if( $which == "" ) {
	$which = 0;
}

if( $ordertype == "" ) {
	$ordertype = $_COOKIE['ordertype'];
	if($ordertype == ""){$ordertype = 1;}
}

/////set cookies so filter stays
setcookie("report",$report);
setcookie("ordertype",$ordertype);
setcookie("current_terminal",$current_terminal);
setcookie("which",$which);

// set calendar
switch ($report) {
    case ($report==10) OR ($report==9) OR ($report==11) OR ($report==30) OR ($report==29) OR ($report==28) OR ($report==35) OR ($report==12) OR ($report==15) OR ($report==2) OR ($report==33) OR ($report==37) OR ($report==39) OR ($report==7) OR ($report==8) OR ($report==14) OR ($report==16) OR ($report==27) OR ($report==17)  :
		$maskdate='9999-19-39'; $formatdate='Y-m-d'; $timepicker='false';
        break;
    case ($report==1) OR ($report==31) OR ($report==5) OR ($report==20) OR ($report==34) OR ($report==25) OR ($report==26)  :
		$maskdate='9999-19-39 29:59:59'; $formatdate='Y-m-d H:i:s'; $timepicker='true';
        break;
    default:
		$maskdate='9999-19-39'; $formatdate='Y-m-d'; $timepicker='true';
        break;
}

if( $report == 1 ) {
	$sel1 = "SELECTED";
}
elseif( $report == 2 ) {
	$sel2 = "SELECTED";
}
elseif( $report == 3 ) {
	$sel3 = "SELECTED";
}
elseif( $report == 4 ) {
	$sel4 = "SELECTED";
}
elseif( $report == 5 ) {
	$sel5 = "SELECTED";
}
elseif( $report == 6 ) {
	$sel6 = "SELECTED";
}
elseif( $report == 7 ) {
	$sel7 = "SELECTED";
}
elseif( $report == 8 ) {
	$sel8 = "SELECTED";
}
elseif( $report == 9 ) {
	$sel9 = "SELECTED";
}
elseif( $report == 10 ) {
	$sel10 = "SELECTED";
}
elseif( $report == 11 ) {
	$sel11 = "SELECTED";
}
elseif( $report == 12 ) {
	$sel12 = "SELECTED";
}
elseif( $report == 13 ) {
	$sel13 = "SELECTED";
}
elseif( $report == 14 ) {
	$sel14 = "SELECTED";
}
elseif( $report == 15 ) {
	$sel15 = "SELECTED";
}
elseif( $report == 16 ) {
	$sel16 = "SELECTED";
}
elseif( $report == 17 ) {
	$sel17 = "SELECTED";
}
elseif( $report == 18 ) {
	$sel18 = "SELECTED";
}
elseif( $report == 19 ) {
	$sel19 = "SELECTED";
}
elseif( $report == 20 ) {
	$sel20 = "SELECTED";
}
elseif( $report == 21 ) {
	$sel21 = "SELECTED";
}
elseif( $report == 22 ) {
	$sel22 = "SELECTED";
}
elseif( $report == 23 ) {
	$sel22 = "SELECTED";
}
elseif( $report == 24 ) {
	$sel24 = "SELECTED";
}
elseif( $report == 25 ) {
	$sel25 = "SELECTED";
}
elseif( $report == 26 ) {
	$sel26 = "SELECTED";
}
elseif( $report == 27 ) {
	$sel27 = "SELECTED";
}
elseif( $report == 28 ) {
	$sel28 = "SELECTED";
}
elseif( $report == 29 ) {
	$sel29 = "SELECTED";
}
elseif( $report == 30 ) {
	$sel30 = "SELECTED";
}
elseif( $report == 31 ) {
	$sel31 = "SELECTED";
}
elseif( $report == 32 ) {
	$sel32 = "SELECTED";
}
elseif( $report == 33 ) {
	$sel33 = "SELECTED";
}
elseif( $report == 34 ) {
	$sel34 = "SELECTED";
}
elseif( $report == 35 ) {
	$sel35 = "SELECTED";
}
elseif( $report == 36 ) {
	$sel36 = "SELECTED";
}
elseif( $report == 37 ) {
	$sel37 = "SELECTED";
}
elseif( $report == 38 ) {
	$sel38 = "SELECTED";
}

////choose report
?>
<tr bgcolor='#E8E7E7'>
	<td colspan='2'>
		<form method='post' style='margin:0;padding:0;display:inline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Report:
			<select name='report' onChange='changePage(this.form.report)'>
				<option value='?bid=<?=$businessid ?>&cid=<?=$companyid ?>&report='>Please Choose...</option>
				<optgroup label="Accounting">
					<option value='?bid=<?=$businessid ?>&cid=<?=$companyid ?>&report=10&terminal_no=<?=$current_terminal ?>' <?=@$sel10 ?>>Accounting</option>
					<option value='?bid=<?=$businessid ?>&cid=<?=$companyid ?>&report=9&terminal_no=<?=$current_terminal ?>' <?=@$sel9 ?>>Item Taxes</option>
<?php
	if ( $useLose == 1 ) {
?>
					<option value='?bid=<?=$businessid ?>&cid=<?=$companyid ?>&report=19&terminal_no=<?=$current_terminal ?>' <?=@$sel19 ?>>Company Cash</option>
					<option value='?bid=<?=$businessid ?>&cid=<?=$companyid ?>&report=21&terminal_no=<?=$current_terminal ?>' <?=@$sel21 ?>>Company Cash Summary</option>
<?php
	}
?>
				</optgroup>
				<optgroup label="Inventory">
					<option value='?bid=<?=$businessid ?>&cid=<?=$companyid ?>&report=3&terminal_no=<?=$current_terminal ?>' <?=@$sel3 ?>>Variance</option>
					<option value='?bid=<?=$businessid ?>&cid=<?=$companyid ?>&report=4&terminal_no=<?=$current_terminal ?>' <?=@$sel4 ?>>Running Variance</option>
					<option value='?bid=<?=$businessid ?>&cid=<?=$companyid ?>&report=1&terminal_no=<?=$current_terminal ?>' <?=@$sel1 ?>>OnHand/ReOrder</option>
					<option value='?bid=<?=$businessid ?>&cid=<?=$companyid ?>&report=11&terminal_no=<?=$current_terminal ?>' <?=@$sel11 ?>>Fills/Waste</option>
					<option value='?bid=<?=$businessid ?>&cid=<?=$companyid ?>&report=30&terminal_no=<?=$current_terminal ?>' <?=@$sel30 ?>>Fills</option>
					<option value='?bid=<?=$businessid ?>&cid=<?=$companyid ?>&report=29&terminal_no=<?=$current_terminal ?>' <?=@$sel29 ?>>Waste</option>
					<option value='?bid=<?=$businessid ?>&cid=<?=$companyid ?>&report=28&terminal_no=<?=$current_terminal ?>' <?=@$sel28 ?>>Shrink</option>
					<option value='?bid=<?=$businessid ?>&cid=<?=$companyid ?>&report=35&terminal_no=<?=$current_terminal ?>' <?=@$sel35 ?>>COGS</option>
					<option value='?bid=<?=$businessid ?>&cid=<?=$companyid ?>&report=31&terminal_no=<?=$current_terminal ?>' <?=@$sel31 ?>>Inventory Stats</option>
					<option value='?bid=<?=$businessid ?>&cid=<?=$companyid ?>&report=12&terminal_no=<?=$current_terminal ?>' <?=@$sel12 ?>>Menu Profitability</option>
<?php
	if ( $report == 13 ) {
?>
					<option value='?bid=<?=$businessid ?>&cid=<?=$companyid ?>&report=13&terminal_no=<?=$current_terminal ?>' <?=@$sel13 ?>>Menu Profitability Detail</option>
<?php
	}
?>
					<option value='?bid=<?=$businessid ?>&cid=<?=$companyid ?>&report=15&terminal_no=<?=$current_terminal ?>' <?=@$sel15 ?>>Food Profitability</option>
				</optgroup>
				<optgroup label="Sales">
					<option value='?bid=<?=$businessid ?>&cid=<?=$companyid ?>&report=2&terminal_no=<?=$current_terminal ?>' <?=@$sel2 ?>>Daily Sales</option>
					<option value='?bid=<?=$businessid ?>&cid=<?=$companyid ?>&report=5&terminal_no=<?=$current_terminal ?>' <?=@$sel5 ?>>Transactions</option>
					<option value='?bid=<?=$businessid ?>&cid=<?=$companyid ?>&report=6&terminal_no=<?=$current_terminal ?>' <?=@$sel6 ?>>Statistics</option>
					<option value='?bid=<?=$businessid ?>&cid=<?=$companyid ?>&report=20&terminal_no=<?=$current_terminal ?>' <?=@$sel20 ?>>Time Period</option>
<?php
	if ( in_array( $user->username, array(
		'SteveM',
		'TraceyM',
		'budo',
		'JakeN',
		'JimM',
		'PeteB',
	) ) ) {
?>

					<option value='?bid=<?=$businessid ?>&cid=<?=$companyid ?>&report=24&terminal_no=<?=$current_terminal ?>' <?=$sel24 ?>>Category Trends</option>
					<option value='?bid=<?=$businessid ?>&cid=<?=$companyid ?>&report=32&terminal_no=<?=$current_terminal ?>' <?=$sel32 ?>>Realtime</option>
					<option value='?bid=<?=$businessid ?>&cid=<?=$companyid ?>&report=33&terminal_no=<?=$current_terminal ?>' <?=$sel33 ?>>Forecast</option>
					<option value='?bid=<?=$businessid ?>&cid=<?=$companyid ?>&report=40&terminal_no=<?=$current_terminal ?>' <?=$sel40 ?>>Forecast2</option>
					<option value='?bid=<?=$businessid ?>&cid=<?=$companyid ?>&report=36&terminal_no=<?=$current_terminal ?>' <?=$sel36 ?>>Inventory Onhand</option>
					<option value='?bid=<?=$businessid ?>&cid=<?=$companyid ?>&report=37&terminal_no=<?=$current_terminal ?>' <?=$sel37 ?>>Inventory Variance</option>
					<?php if($report == 38){ ?>
						<option value='?bid=<?=$businessid ?>&cid=<?=$companyid ?>&report=38&terminal_no=<?=$current_terminal ?>' <?=$sel38 ?>>Inventory Variance Detail</option>
					<?php } ?>

<?php
	}
?>
				</optgroup>
				<optgroup label="Promotions/Deposits/Subsidies">
					<option value='?bid=<?=$businessid ?>&cid=<?=$companyid ?>&report=7&terminal_no=<?=$current_terminal ?>' <?=@$sel7 ?>>Promotion Summary</option>
					<option value='?bid=<?=$businessid ?>&cid=<?=$companyid ?>&report=8&terminal_no=<?=$current_terminal ?>' <?=@$sel8 ?>>Item Promotion Detail</option>
<?php
	if ( in_array( $user->username, array(
		'SteveM',
		'TraceyM',
		'PeteB',
		'mistyd',
		'Angelas',
		'christinel',
		'jeffm',
		'wk',
		'jodir',
		'ritaz',
		'JakeN',
		'jone',
		'amyh',
		'shellyj'
	) ) ) {
?>
					<option value='?bid=<?=$businessid ?>&cid=<?=$companyid ?>&report=14&terminal_no=<?=$current_terminal ?>' <?=@$sel14 ?>>Item Subsidy Detail</option>
<?php
	}
	if ( $user->security_level >= 4 || $companyid == 1 ) {
?>
				</optgroup>
				<optgroup label="Customers">
					<option value='?bid=<?=$businessid ?>&cid=<?=$companyid ?>&report=16&terminal_no=<?=$current_terminal ?>' <?=@$sel16 ?>>Customer Detail</option>
					<option value='?bid=<?=$businessid ?>&cid=<?=$companyid ?>&report=18&terminal_no=<?=$current_terminal ?>' <?=@$sel18 ?>>Customer Balances</option>
					<option value='?bid=<?=$businessid ?>&cid=<?=$companyid ?>&report=27&terminal_no=<?=$current_terminal ?>' <?=@$sel27 ?>>Customer Purchases</option>
				</optgroup>
<?php
	}
?>
				</optgroup>
				<optgroup label="Orders">
					<option value='?bid=<?=$businessid ?>&cid=<?=$companyid ?>&report=17&terminal_no=<?=$current_terminal ?>' <?=@$sel17 ?>>Order Detail</option>
					<option value='?bid=<?=$businessid ?>&cid=<?=$companyid ?>&report=34&terminal_no=<?=$current_terminal ?>' <?=@$sel34 ?>>Speed of Service</option>
				</optgroup>
				<optgroup label="Error Reporting">
					<option value='?bid=<?=$businessid ?>&cid=<?=$companyid ?>&report=22&terminal_no=<?=$current_terminal ?>' <?=@$sel22 ?>>Data Problems</option>
				</optgroup>
				
<?php
	if ( in_array( $user->username, array(
		'SteveM',
		'JakeN',
		'TraceyM',
		'budo',
		'dbriggs',
	) ) ) {
				
?>

				<optgroup label="Customer Data">
					<option value='?bid=<?=$businessid ?>&cid=<?=$companyid ?>&report=25&terminal_no=<?=$current_terminal ?>' <?=@$sel25 ?>>Popular Amount/Items</option>
					<option value='?bid=<?=$businessid ?>&cid=<?=$companyid ?>&report=26&terminal_no=<?=$current_terminal ?>' <?=@$sel26 ?>>Popular Check Detail</option>
				</optgroup>
				
<?php
	}
?>


<?php

if( $user->security_level >= 4 ) :

endif;


?>
			</select>




			
		</form>
<?php
////if multi kiosks
if ( count( $terminal_no ) > 1 && $kiosk_multi_inv == 1 ) {
	//if( $current_terminal == 0 ) {
		//$current_terminal = $terminal_no[0];
	//}

	echo "<form method=post style=\"margin:0;padding:0;display:inline;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Terminal: <select name=terminal onChange='changePage(this.form.terminal)'>
		  <option value='?cid=$companyid&bid=$businessid&report=$report&ordertype=$ordertype&terminal_no=' $sel>All Terminals</option>";

	foreach ( $terminal_no AS $key => $value ) {
		if ( $value == $current_terminal ) {
			$sel = "SELECTED";
		}
		else {
			$sel = "";
		}

		echo "<option value='?cid=$companyid&bid=$businessid&report=$report&ordertype=$ordertype&terminal_no=$value' $sel>$terminal_name[$key]</option>";
	}

	echo "</select></form>";

	$forms = 5;
}
else {
	$current_terminal = 0;
	$forms = 4;
}

////profitability filter
if ( $report == 12 ) {

	if ( $ordertype < 2 ) {
		$ordertype = 2;
	}

	echo "<form method=post style=\"margin:0;padding:0;display:inline;\">&nbsp;&nbsp;&nbsp;Order Type: <select name=ordertype onChange='changePage(this.form.ordertype)'>";

	$query = "SELECT * FROM menu_items_ordertype WHERE id != 1 ORDER BY id";
	$result = Treat_DB_ProxyOldProcessHost::query( $query );

	while ( $r = mysql_fetch_array( $result ) ) {
		$ordertype_id = $r["id"];
		$ordertype_name = $r["name"];

		if ( $ordertype == $ordertype_id ) {
			$sel = "SELECTED";
		}
		else {
			$sel = "";
		}

		echo "<option value='?bid=$businessid&cid=$companyid&report=$report&ordertype=$ordertype_id&terminal_no=$current_terminal&which=$which' $sel>$ordertype_name</option>";
	}

	echo "</select></form>";
}
elseif ( $report == 15 ) {

	if ( $ordertype > 1 ) {
		$ordertype = 1;
	}

	echo "<form method=post style=\"margin:0;padding:0;display:inline;\">&nbsp;&nbsp;&nbsp;Order Type: <select name=ordertype onChange='changePage(this.form.ordertype)'>";

	$query = "SELECT * FROM menu_items_ordertype WHERE id = 1 ORDER BY id";
	$result = Treat_DB_ProxyOldProcessHost::query( $query );

	while ( $r = mysql_fetch_array( $result ) ) {
		$ordertype_id = $r["id"];
		$ordertype_name = $r["name"];

		if ( $ordertype == $ordertype_id ) {
			$sel = "SELECTED";
		}
		else {
			$sel = "";
		}

		echo "<option value='?bid=$businessid&cid=$companyid&report=$report&ordertype=$ordertype_id&terminal_no=$current_terminal&which=$which' $sel>$ordertype_name</option>";
	}

	echo "</select></form>";
}

////if onhand report - drill down by order type
if (
	( $report >= 1 && $report <= 4 )
	|| $report == 11
	|| $report == 17
	|| $report == 28
	|| $report == 29
	|| $report == 30
	|| $report == 31
	|| $report == 33
	|| $report == 35
) {
	echo "<form method=post style=\"margin:0;padding:0;display:inline;\">&nbsp;&nbsp;&nbsp;Order Type: <select name=ordertype onChange='changePage(this.form.ordertype)'><option value='?bid=$businessid&cid=$companyid&report=$report&ordertype=0'>All Types</option>";

	$business = EE\Model\Business\Singleton::getSingleton();
	$order_type_select = \EE\Model\Menu\Item\OrderType::buildMenuOrderTypes($business->companyid);

	foreach ($order_type_select as $row ) {
		if ( $row->id == $ordertype ) {
			$row->sel = ' selected="selected"';
		}
		else {
			$row->sel = '';
		}
		echo "<option value='?bid=$businessid&cid=$companyid&report=$report&ordertype=" . $row->id . "&terminal_no=$current_terminal&which=$which' " . $row->sel . ">" . $row->name . "</option>";
	}

	echo "</select></form>";
}

//////////////////////
///date-time for onhand
if ( $report == 1 || $report == 31 ) {
	$t_date1 = $_POST["t_date1"];
	if($t_date1 == ""){
		$t_date1 = $_COOKIE['oh_date1'];
		if($t_date1 == ""){$t_date1 = date( "Y-m-d H:i:s" );}
	}

	setcookie("oh_date1",$t_date1);

	echo "<form action='?bid=$businessid&cid=$companyid&report=$report&ordertype=$ordertype&terminal_no=$current_terminal' method=post style=\"margin:0;padding:0;display:inline;\">&nbsp;&nbsp;&nbsp; <input type=text id=t_date1 name=t_date1 value='$t_date1' size=16> <input type=hidden id=t_date2 name=t_date2 value='$t_date2' size=16> <input type=submit value='GO'></form>";
}

//////////////////////
////filter daily sales
if ( $report == 2 || $report == 11 || $report == 28 || $report == 29 || $report == 30 || $report == 35) {
	$t_date1 = $_POST["t_date1"];
	$t_date2 = $_POST["t_date2"];
	$t_datefield = isset($_POST['t_datefield']) ? intval($_POST['t_datefield']) : 1;

	if ($t_datefield < 1 && isset($_COOKIE['t_datefield'])) {
		$t_datefield = $_COOKIE['t_datefield'];
	}

	if( $t_date1 == "" ) {
		$t_date1 = $_COOKIE['t_date1'];
		if($t_date1 == ""){
			$t_date1 = date( "Y-m-d" );
			for( $counter = 1; $counter <= 6; $counter++ ) {
				$t_date1 = prevday( $t_date1 );
			}
		}
	}
	if( $t_date2 == "" ) {
		$t_date2 = $_COOKIE['t_date2'];
		if($t_date2 == ""){$t_date2 = date( "Y-m-d" );}
	}

	setcookie("t_date1",$t_date1);
	setcookie("t_date2",$t_date2);
	setcookie('t_datefield', $t_datefield);
?>
	<form action='?bid=<?php echo $businessid; ?>&cid=<?php echo $companyid; ?>&report=<?php echo $report; ?>&ordertype=<?php echo $ordertype; ?>&terminal_no=<?php echo $current_terminal; ?>' method="POST" style="margin: 0; padding: 0; display: inline;">
		&nbsp;&nbsp;&nbsp;From
		<input type="text" id=t_date1 name="t_date1" value="<?php echo $t_date1; ?>" size="10">

		to 
		<input type="text" id=t_date2 name="t_date2" value='<?php echo $t_date2; ?>' size="10">
		<?php
		if( $report == 2 ) :
		?>
		for
		<select name="t_datefield" size="1">
			<option value="1"<?php if($t_datefield == 1) : ?> selected="selected"<?php endif; ?>>Transaction Date</option>
			<option value="2"<?php if($t_datefield == 2) : ?> selected="selected"<?php endif; ?>>Posted Date</option>
		</select>
		<?php
		endif;
		?>
		<input type="submit" value="GO">
	</form>
<?php
}
//////////////////////
////filter profitability
if( $report == 12 || $report == 15 ) {
	$t_date1 = $_POST["t_date1"];

	if( $t_date1 == "" ) {
		$t_date1 = $_COOKIE['t_date1'];
		if($t_date1 == ""){
			$t_date1 = date( "Y-m-d" );
			for( $counter = 1; $counter <= 6; $counter++ ) {
				$t_date1 = prevday( $t_date1 );
			}
		}
	}
	$t_date2 = date( "Y-m-d" );

	setcookie("t_date1", $t_date1);
	setcookie("t_date2", $t_date2);
?>

	<form action='?bid=<?php echo $businessid; ?>&cid=<?php echo $companyid; ?>&report=<?php echo $report; ?>&ordertype=<?php echo $ordertype; ?>&terminal_no=<?php echo $current_terminal; ?>' method="POST" style="margin:0;padding:0;display:inline;">
		
		&nbsp;&nbsp;&nbsp;From 
		<input type=text id="t_date1" name="t_date1" class="datepicker" value='<?php echo $t_date1; ?>' size=10>
		<input id="t_date2" name="t_date2" class="datepicker" value='<?php echo $t_date2; ?>' size=10 type=hidden>		
		to 
		<?php echo $t_date2; ?>
		<input type="submit" value='GO'>
	</form>
<?php
}
if( $report == 13 ){
	$t_date1 = $_COOKIE['t_date1'];
	$t_date2 = $_COOKIE['t_date2'];
}
//////////////////////
////filter daily sales
if( $report == 7 || $report == 8 || $report == 9 || $report == 10 || $report == 14 || $report == 16 || $report == 19 || $report == 20 || $report == 21 || $report == 23 || $report == 27 || $report == 37) {
	$t_date1 = $_POST["t_date1"];
	$t_date2 = $_POST["t_date2"];
	$t_period = $_POST["period"];

	if( $t_date1 == "" ) {
		$t_date1 = $_COOKIE['t_date1'];
		if($t_date1 == ""){
			$t_date1 = date( "Y-m-d" );
			for( $counter = 1; $counter <= 6; $counter++ ) {
				$t_date1 = prevday( $t_date1 );
			}
		}
	}
	if( $t_date2 == "" ) {
		$t_date2 = $_COOKIE['t_date2'];
		if($t_date2 == ""){$t_date2 = date( "Y-m-d" );}
	}
	
	if($t_period == ""){
		$t_period = $_COOKIE['t_period'];
	}
	if ( $report == 7 || $report == 8 ) {
		$t_datefield = isset($_POST['t_datefield']) ? intval($_POST['t_datefield']) : 1;

		if ($t_datefield < 1 && isset($_COOKIE['t_datefield'])) {
			$t_datefield = $_COOKIE['t_datefield'];
		}

		setcookie('t_datefield', $t_datefield);
	}

	setcookie("t_date1",$t_date1);
	setcookie("t_date2",$t_date2);
	setcookie("t_period",$t_period);

	$forms--;
?>
	<form action='?bid=<?php echo $businessid; ?>&cid=<?php echo $companyid; ?>&report=<?php echo $report; ?>&ordertype=<?php echo $ordertype; ?>&terminal_no=<?php echo $current_terminal; ?>' method=post style="margin:0;padding:0;display:inline;">

		
		&nbsp;&nbsp;&nbsp;From 
		<input type=text id="t_date1" name="t_date1" class="datepicker" value='<?php echo $t_date1; ?>' size=16>
		to 
		<input type=text id="t_date2" name="t_date2" class="datepicker" value='<?php echo $t_date2; ?>' size=16>



		
	<?php
	if ( $report == 7 || $report == 8 ) :
	?>

		for
		<select name="t_datefield" size="1">
			<option value="1"<?php if($t_datefield == 1) : ?> selected="selected"<?php endif; ?>>Transaction Date</option>
			<option value="2"<?php if($t_datefield == 2) : ?> selected="selected"<?php endif; ?>>Posted Date</option>
		</select>
	<?php
	endif;
	?>
<?php
	///separate accounting report
	if($report == 10 || $report == 20)
	{
		if($t_period == 1){$sel01 = "SELECTED";}
		elseif($t_period == 2){$sel02 = "SELECTED";}
		elseif($t_period == 3){$sel03 = "SELECTED";}
		elseif($t_period == 4){$sel04 = "SELECTED";}
		
		echo " Group By:<select name=period>";
			echo "<option value=0>Date Range</option>";
			echo "<option value=1 $sel01>Day</option>";
			echo "<option value=2 $sel02>Week</option>";
			echo "<option value=3 $sel03>Month</option>";
			if($report == 20){
				echo "<option value=4 $sel04>Day of the Week</option>";
			}
		echo "</select> ";
	}
	
	if($report == 20){
		$t_period2 = $_POST["timePeriod"];
		
		if($t_period2 == ""){
			$t_period2 = $_COOKIE['t_period2'];
			if($t_period2 == ""){
				$t_period2 = 0;
			}
		}
		
		setcookie('t_period2', $t_period2);
	
		if($t_period2 == 0){$sel11 = "SELECTED";}
		elseif($t_period2 == 1){$sel12 = "SELECTED";}
		elseif($t_period2 == 2){$sel13 = "SELECTED";}
		
		echo " <select name=timePeriod>";
			echo "<option value=0 $sel11>1 hour</option>";
			echo "<option value=1 $sel12>30 min</option>";
			echo "<option value=2 $sel13>15 min</option>";
		echo "</select> ";
	}
	
	echo " <input type=submit value='GO'></form>";
}
////filter for transactions
elseif( $report == 5 || $report == 34) {
	$t_date1 = $_POST["t_date1"];
	$t_date2 = $_POST["t_date2"];
	$pnuvalCurrency = $_POST["reportcurrency"];
	list($pstrdesCurrency, $pnureportcurrency) = split('-', $pnuvalCurrency);
	if ($pstrdesCurrency==''){$pstrdesCurrency='USD'; $pnureportcurrency='1.00';}
	//echo "description : $pstrdesCurrency; value: $pnureportcurrency<br />\n";
	
	
	$t_datefield = isset($_POST['t_datefield']) ? intval($_POST['t_datefield']) : 1;

	if ($t_datefield < 1 && isset($_COOKIE['t_datefield'])) {
		$t_datefield = $_COOKIE['t_datefield'];
	}

	if( $t_date1 == "" ) {
		if ( isset($_COOKIE["trans_date1"]) ) {
			$t_date1 = $_COOKIE["trans_date1"];
		} else if ( isset($_COOKIE["t_date1"]) ) {
			$t_date1 = $_COOKIE["t_date1"];
		}
		
		if($t_date1 == ""){
			$t_date1 = date( "Y-m-d H:i:s",
			mktime( date( "H" ) - 3, date( "i" ), date( "s" ), date( "m" ), date( "d" ), date(
						"Y" ) ) );
			$t_date1 = date( "Y-m-d H:i:s", mktime( 0, 0, 0, date( "m" ), date( "d" ), date("Y" ) ) );
		}
	}
	if( $t_date2 == "" ) {
		if ( isset($_COOKIE["trans_date2"]) ) {
			$t_date2 = $_COOKIE["trans_date2"];
		} else if ( isset($_COOKIE["t_date2"]) ) {
			$t_date2 = $_COOKIE["t_date2"];
		}
		if($t_date2 == ""){
			$t_date2 = date( "Y-m-d H:i:s" );
			$t_date2 = date( "Y-m-d H:i:s", mktime( 23, 59, 59, date( "m" ), date( "d" ), date("Y" ) ) );
		}
	}

	setcookie("trans_date1", $t_date1);
	setcookie("trans_date2", $t_date2);
	setcookie("t_datefield", $t_datefield);
?>

	<form action="?bid=<?php echo $businessid; ?>&cid=<?php echo $companyid; ?>&report=<?php echo $report; ?>&ordertype=<?php echo $ordertype; ?>&terminal_no=<?php echo $current_terminal; ?>" method="POST" style="margin:0;padding:0;display:inline;">

			&nbsp;&nbsp;Currency:
			<select name='reportcurrency'>

			
<?PHP

	//JRLM
	$query = " SELECT DISTINCT  forexR.currency, (SELECT DISTINCT rate " 
	."	FROM forex_rates "
	."	WHERE currency =  forexR.currency  "
	."	ORDER BY timestamp DESC limit 1) as rate, aerisS.registryValue  "
	." FROM aeris_settings aerisS INNER JOIN forex_rates  forexR "
	." WHERE businessid =  ". $businessid
	." AND registryKey = 'aeris.currency.accepted' "
	." AND aerisS.registryValue  LIKE (concat('%',forexR.currency,'%')) "
	." ORDER BY forexR.timestamp  ";
	
	$obresult_Currency = Treat_DB_ProxyOldProcessHost::query( $query );
		$strOption = '';
	while( $r = mysql_fetch_array( $obresult_Currency ) ) {
		
		$strOption = "<option value=" . $r["currency"] . "-" . $r["rate"];

		if($r["currency"] ==$pstrdesCurrency) {
			$strOption = $strOption. " selected='selected'";
		}
		
		$strOption = $strOption. " > " . $r["currency"] . "</option>";
		echo $strOption;
	
	}

	
?>
			</select>


	
		&nbsp;&nbsp;&nbsp;From 
		<input type=text id="t_date1" name="t_date1" class="datepicker" value='<?php echo $t_date1; ?>' size=17>
		to 
		<input type=text id="t_date2" name="t_date2" class="datepicker" value='<?php echo $t_date2; ?>' size=17>

		
		Use
		<select name="t_datefield" size="1">
			<option value="1"<?php if($t_datefield == 1) : ?> selected="selected"<?php endif; ?>>Transaction Date</option>
			<option value="2"<?php if($t_datefield == 2) : ?> selected="selected"<?php endif; ?>>Posted Date</option>
		</select>


		<input type=submit value='GO'>


	</form>
<?php
} 
elseif( $report == 25 || $report == 26 ) {
	$report_value = $_REQUEST['report'];
	$t_date1 = $_POST["t_date1"];
	$t_date2 = $_POST["t_date2"];
	$t_datefield = isset($_POST['t_datefield']) ? intval($_POST['t_datefield']) : 1;

	if ($t_datefield < 1 && isset($_COOKIE['t_datefield'])) {
		$t_datefield = $_COOKIE['t_datefield'];
	}

	if( $t_date1 == "" ) {
		if ( isset($_COOKIE["trans_date1"]) ) {
			$t_date1 = $_COOKIE["trans_date1"];
		} else if ( isset($_COOKIE["t_date1"]) ) {
			$t_date1 = $_COOKIE["t_date1"];
		}
		
		if($t_date1 == ""){
			$t_date1 = date( "Y-m-d H:i:s",
			mktime( date( "H" ) - 3, date( "i" ), date( "s" ), date( "m" ), date( "d" ), date(
						"Y" ) ) );
		}
	}
	if( $t_date2 == "" ) {
		if ( isset($_COOKIE["trans_date2"]) ) {
			$t_date2 = $_COOKIE["trans_date2"];
		} else if ( isset($_COOKIE["t_date2"]) ) {
			$t_date2 = $_COOKIE["t_date2"];
		}
		if($t_date2 == ""){$t_date2 = date( "Y-m-d H:i:s" );}
	}

	setcookie("trans_date1", $t_date1);
	setcookie("trans_date2", $t_date2);
	setcookie("t_datefield", $t_datefield);
?>
	<form action="?bid=<?php echo $businessid; ?>&cid=<?php echo $companyid; ?>&report=<?php echo $report_value; ?>&ordertype=<?php echo $ordertype; ?>" method="POST" style="margin:0;padding:0;display:inline;">
		&nbsp;&nbsp;&nbsp;From 
		
		<input type=text id="t_date1" name="t_date1" class="datepicker" value='<?php echo $t_date1; ?>' size=17>
		to 
		<input type=text id="t_date2" name="t_date2" class="datepicker" value='<?php echo $t_date2; ?>' size=17>
		<input type=submit value='GO'>
	</form>
<?php
}
elseif( $report == 3 ) {
	$prevwhich = $which + 1;
	$nextwhich = $which - 1;

	echo "&nbsp;&nbsp;&nbsp;<a href='?bid=$businessid&cid=$companyid&report=$report&terminal_no=$current_terminal&which=$prevwhich&ordertype=$ordertype' style=$style><font size=2 color=blue>[PREV]</font></a>";

	if( $which == 0 ) {
		echo " |<font size=2> [LATEST]</font>";
	}
	else {
		echo " | <a href=?bid=$businessid&cid=$companyid&report=$report&terminal_no=$current_terminal&which=0&ordertype=$ordertype style=$style><font size=2 color=blue>[LATEST]</font></a>";
	}

	if( $nextwhich >= 0 ) {
		echo " | <a href=?bid=$businessid&cid=$companyid&report=$report&terminal_no=$current_terminal&which=$nextwhich&ordertype=$ordertype style=$style><font size=2 color=blue>[NEXT]</font></a>";
	}
	else {
		echo " |<font size=2> [NEXT]</font>";
	}
}
elseif( $report == 17 ){
	$o_date = $_POST["o_date"];  
	if($o_date == ""){
		$o_date = $_COOKIE['o_date'];
		if($o_date == ""){
			$o_date = date("Y-m-d");
		}
	}
	setcookie("o_date", $o_date);
	
	?>
	<form action='?bid=<?php echo $businessid; ?>&cid=<?php echo $companyid; ?>&report=<?php echo $report; ?>&ordertype=<?php echo $ordertype; ?>&terminal_no=<?php echo $current_terminal; ?>' method="POST" style="margin:0;padding:0;display:inline;">
		&nbsp;&nbsp;&nbsp;For:

		
		<input type="text" name="o_date" value='<?php echo $o_date; ?>' size="8">
		<input type=hidden id="t_date2" name="t_date2" class="datepicker" value='<?php echo $t_date2; ?>' size=10>
		
		<input type="submit" value='GO'>
	</form>
	<?php
	
	////Hide/Show for order detail
	if($_GET["success"] != 0 && $_GET["edit"] == 1){
		echo "&nbsp;&nbsp;<a href='?bid=$businessid&cid=$companyid&report=$report&ordertype=$ordertype&terminal_no=$current_terminal&edit=1' style=\"text-decoration: none;\"><font color=blue>All Items</font></a>";
	}
	elseif($_GET["edit"] == 1){
		echo "&nbsp;&nbsp;<a href='?bid=$businessid&cid=$companyid&report=$report&ordertype=$ordertype&terminal_no=$current_terminal&edit=1&success=-1' style=\"text-decoration: none;\"><font color=blue>Ordered Items</font></a>";
	}
	else {
		//echo "&nbsp;&nbsp;<a href='?bid=$businessid&cid=$companyid&report=$report&ordertype=$ordertype&terminal_no=$current_terminal&edit=1&success=-1' style=\"text-decoration: none;\"><font color=blue>Ordered Items</font></a>";
	}
	
}
elseif($report == 33){
	$t_date1 = $_POST["t_date1"];  
	if($t_date1 == ""){
		$t_date1 = $_COOKIE['t_date1'];
		if($t_date1 == ""){
			$t_date1 = date("Y-m-d");
		}
	}
	setcookie("t_date1", $t_date1);
	
	?>
	<form action='?bid=<?php echo $businessid; ?>&cid=<?php echo $companyid; ?>&report=<?php echo $report; ?>&ordertype=<?php echo $ordertype; ?>&terminal_no=<?php echo $current_terminal; ?>' method="POST" style="margin:0;padding:0;display:inline;">
		&nbsp;&nbsp;&nbsp;For:

	
		<input type=text id="t_date1" name="t_date1" class="datepicker" value='<?php echo $t_date1; ?>' size=10>
		<input type=hidden id="t_date2" name="t_date2" class="datepicker" value='<?php echo $t_date2; ?>' size=10>
	

	</form>
	<?php
}
elseif($report == 37){
	$o_date = $_POST["o_date"];  
	if($o_date == ""){
		$o_date = $_COOKIE['o_date'];
		if($o_date == ""){
			$o_date = date("Y-m-d");
		}
	}
	setcookie("o_date", $o_date);
	
	?>
	<form action='?bid=<?php echo $businessid; ?>&cid=<?php echo $companyid; ?>&report=<?php echo $report; ?>&ordertype=<?php echo $ordertype; ?>&terminal_no=<?php echo $current_terminal; ?>' method="POST" style="margin:0;padding:0;display:inline;">
		&nbsp;&nbsp;&nbsp;For:
		<script type="text/javascript" id='js18'>
		var cal18 = new CalendarPopup('testdiv1');
		cal18.setCssPrefix('TEST');
		</script>
		<input type="text" name="o_date" value='<?php echo $o_date; ?>' size="8">
		<a href="javascript:void();" onclick="cal18.select(document.forms[<?php echo $forms; ?>].o_date,'anchor18','yyyy-MM-dd'); return false;" title="cal18.select(document.forms[<?php echo $forms; ?>].o_date,'anchor1x','yyyy-MM-dd'); return false;" name="anchor18" id='anchor18'>
			<img src="/ta/calendar.gif" border="0" height="16" width="16" alt="Choose a Date"></a> 
		<input type="submit" value='GO'>
	</form>
	<?php
}
///realtime filter
elseif($report == 32){
	echo "&nbsp;&nbsp;&nbsp;Order Type: <select name=ordertype id='ordertype'><option value=0>All Types</option>";

	$business = EE\Model\Business\Singleton::getSingleton();
	$order_type_select = \EE\Model\Menu\Item\OrderType::buildMenuOrderTypes($business->companyid);

	foreach ($order_type_select as $row ) {
		if ( $row->id == $ordertype ) {
			$row->sel = ' selected="selected"';
		}
		else {
			$row->sel = '';
		}
		echo "<option value=" . $row->id . ">" . $row->name . "</option>";
	}

	echo "</select>";
	
	echo "&nbsp;&nbsp;<select name=level id='level'><option value=0>All Markets</option><option value=1>My Markets</option><option value=2>This Market</option></select>";
	
	echo "<input type=hidden name=businessid id='businessid' value=$businessid><input type=hidden name=companyid id='companyid' value=$companyid>";
}
echo "</td></tr>";

///get machine number tied to this business
if( $current_terminal > 0 ) {
	$add_query = "AND client_programid = $current_terminal";
}
else {
	$add_query = "";
}

$query = "SELECT machine_num FROM machine_bus_link WHERE businessid = $businessid $add_query";
$result = Treat_DB_ProxyOldProcessHost::query( $query );

$machine_num = array( );
$counter = 0;
while( $r = mysql_fetch_array( $result ) ) {
	$machine_num[$counter] = $r["machine_num"];
	$counter++;
}

//////////////////////////////////
///get list of active menu items//
//////////////////////////////////
if($report == 2){
	if($ordertype < 0){
		$ordertype = 0;
	}
	$items = menu_items::list_working_menu( $businessid, $ordertype, $t_date1, $t_date2 );
}
elseif( $ordertype > 0 && ( $report == 1 || $report == 3 || $report == 4 || $report == 11 || $report == 12 || $report == 15 || $report == 17) ) {
	$items = menu_items::list_menu( $businessid, $ordertype );
}
elseif( $report == 1 || $report == 3 || $report == 4 || $report == 11 ) {
	$items = menu_items::list_menu( $businessid );
}
elseif( $ordertype > 0 ) {
	$items = menu_items::list_active_menu( $businessid, $ordertype );
}
else {
	$items = menu_items::list_active_menu( $businessid, 0 );
}

////////////////////////
////DISPLAY REPORT//////
////////////////////////

echo "<tr bgcolor=#E8E7E7><td colspan=2><center>";

//onhand
if( $report == 1 ) {
	include 'posReporting/onhand.tpl';
}
//profitability
elseif( $report == 12 ) {
	include 'posReporting/profitability.tpl';
}
//food profitability
elseif( $report == 15 ) {
	include 'posReporting/food-profitability.tpl';
}
//profitability detail
elseif( $report == 13 ) {
	include 'posReporting/profitability-detail.tpl';
}
//sales
elseif( $report == 2 ) {
	include 'posReporting/sales.tpl';
}
//fills/waste
elseif( $report == 11 ) {
	include 'posReporting/fills-waste.tpl';
}
//variance
elseif( $report == 3 ) {
	include 'posReporting/variance.tpl';
}
//running variance
elseif( $report == 4 ) {
	include 'posReporting/running-variance.tpl';
}
////transactions
elseif( $report == 5 ) {
	include 'posReporting/transactions.tpl';
}
// Vending Graph a.k.a. Statistics
elseif( $report == 6 ) {
	include 'posReporting/statistics.tpl';
}
////promo summary
elseif( $report == 7 ) {
	include 'posReporting/promo-summary.tpl';
}
////item promotions
elseif( $report == 8 ) {
	include 'posReporting/item-promotions.tpl';
}
////item subsidies
elseif( $report == 14 ){
	include 'posReporting/item-subsidies.tpl';
}
//Subsidy Items - Aeris2 
elseif( $report == 23 ){
	include 'posReporting/subsidy-items.tpl';
}
////item taxes
elseif( $report == 9 ){
	include 'posReporting/item-taxes.tpl';
}
////accounting
elseif( $report == 10 ){
	include 'posReporting/accounting.tpl';
}
///customer detail
elseif($report == 16){
	include 'posReporting/customer-detail.tpl';
}
//order detail
elseif($report == 17){
	include 'posReporting/order-detail.tpl';
}
elseif($report == 18){
	include 'posReporting/customer-balances.tpl';
}
//Company Cash
elseif($report == 19){
	include 'posReporting/company-cash.tpl';
}
elseif( $report == 21 ){
	include 'posReporting/report-21.tpl';
}
//time period
elseif( $report == 20 ){
	include 'posReporting/time-period.tpl';
}
//data problems
elseif($report == 22){
	include 'posReporting/data-problems.tpl';
}
//Trending
elseif($report == 24){
	include 'posReporting/trending.tpl';
}
//Popular Amount Items
elseif($report == 25){ 
	include 'posReporting/popular-amount-items.tpl';
}
//Popular Check Detail
elseif($report == 26){ 
	include 'posReporting/popular-check-detail.tpl';
}
//Popular Check Detail
elseif($report == 27){ 
	include 'posReporting/customer-purchases.tpl';
}
elseif($report == 28){ 
	include 'posReporting/shrink.tpl';
}
elseif($report == 29){ 
	include 'posReporting/waste.tpl';
}
elseif($report == 30){ 
	include 'posReporting/fills.tpl';
}
elseif($report == 31){ 
	include 'posReporting/inv-stats.tpl';
}
elseif($report == 32){ 
	include 'posReporting/realtime.tpl';
}
elseif($report == 33){
	include 'posReporting/forecast.tpl';
}
//speed of service
elseif($report == 34){
	include 'posReporting/speedofservice.tpl';
}
//cogs
elseif($report == 35){
	include 'posReporting/cogs.tpl';
}
//inventory onhand
elseif($report == 36){
	include 'posReporting/inventory-onhand.tpl';
}
//inventory variance
elseif($report == 37){
	include 'posReporting/inventory-variance.tpl';
}
//inventory variance detail
elseif($report == 38){
	include 'posReporting/inventory-variance-detail.tpl';
}
//forecasting jake
elseif($report == 40){
	include 'posReporting/forecast2.tpl';
}
else {
	echo "Please Choose a Report...<p>";
	$download_disable = "DISABLED";
}

echo "</center></td></tr>";

$download_data = str_replace( "'", "`", $download_data );


?>
		<tr bgcolor=#E8E7E7>
			<td align=left>&nbsp;<form action='/ta/posreport.php' method='post' style="margin:0;padding:0;display:inline;">
				<input type=hidden name=download_data value='<?=$download_data?>' />
				<input type=submit value='Download' <?=$download_disable?> style="border: 1px solid #999999; font-size: 10px; background-color:#CCCCCC;" />
<?php
	if ( $report == 3 && $sec_bus_menu_pos_report > 1 ) {
		echo "<center><a href='#' id='upload_file_link'>Upload Data File</a></center>";
	}
?>
			</form></td>
		</tr>
	</table>
</center>
<script language='javascript'>

	$('.some_class').datetimepicker();
	var lboclosewindowDate = 0;

	if ('false'=='<?php echo $timepicker?>')
	{
			var lfecha1=$('#t_date1').val();
			var lfecha2=$('#t_date2').val();
			
			if (lfecha1) lfecha1=lfecha1.slice(0,10);
			if (lfecha2) lfecha2=lfecha2.slice(0,10);
			lboclosewindowDate = true;
	}

	$('#t_date1').datetimepicker({
		step:10, mask:'<?php echo $maskdate?>', format:'<?php echo $formatdate?>', timepicker:<?php echo $timepicker?>, value:lfecha1, closeOnDateSelect:lboclosewindowDate
	});
		

	$('#t_date2').datetimepicker({
		step:10, mask:'<?php echo $maskdate?>', format:'<?php echo $formatdate?>', timepicker:<?php echo $timepicker?>, value:lfecha2, closeOnDateSelect:lboclosewindowDate
	});


</script>


