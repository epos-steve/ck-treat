<div id='divbox'>&nbsp;</div>
<div id="reportFloater" style='display: none;'>
	<span id='editme' style="height:600px;"></span>
	<br />
	<input type='button' value='Close' onclick="$('reportFloater').hide();" />
</div>

<table cellspacing='0' cellpadding='0' border='0' width='100%'>
	<tr bgcolor='#E8E7E7'>
		<td height='1' colspan='4'>
			<a name='item'></a>
			<form action='/ta/placemenu.php?cid=<?=$companyid?>&bid=<?=$businessid?>' method='post'>
				<input type='hidden' name='direct' value='3' />
				<select name='curmenu'>
<?php

if($sec_bus_menu == 1){
    $viewonly = 'DISABLED';
}

$showordermenu = "";
foreach( $menuTypeList as $menuType ) {
	$menu_typename = $menuType->menu_typename;
	$menu_typeid = $menuType->menu_typeid;
	$menutype = $menuType->type;
	if ( $curmenu == $menu_typeid ) {
		$showmenu = ' selected="selected"';
		$curmenutype = $menutype;
		$menu_parent = $menuType->parent;
	}
	else {
		$showmenu = '';
	}
	if ( $curmenu == $menu_typeid && $menutype == 1 ) {
		$showordermenu = "<a href='/ta/busordermenu.php?bid=$businessid&cid=$companyid' style=$style><font color=blue>Menu Schedule Setup</font></a>";
	}
	elseif ( $curmenu == $menu_typeid && $menutype == 2 ) {
		$showordermenu = "<a href='/ta/busordermenu2.php?bid=$businessid&cid=$companyid' style=$style><font color=blue>Menu Schedule Setup</a>";
	}
	elseif ( $curmenu == $menu_typeid && $menutype == 0 ) {
		$showordermenu = "<a href='/ta/busordermenu10.php?bid=$businessid&cid=$companyid' style=$style><font color=blue>Menu Schedule Setup</a>";
	}
	echo "<option value=$menu_typeid $showmenu>$menu_typename</option>";
}
?>
				</select>
				<input type='submit' value='GO' />
				<?=$showordermenu?>
				<a href='/ta/printmenu.php?cid=<?=$companyid?>&bid=<?=$businessid?>&curmenu=<?=$curmenu?>' style='<?=$style?>' target='_blank'>
					<img src='/assets/images/print.gif' height='16' width='16' border='0' />
					<font color='blue'>Printable Menu</font>
				</a>&nbsp;
				<a href='/ta/buscaterexport.php?bid=<?=$businessid?>&cid=<?=$companyid?>' style='<?=$style?>'>
					<img src='/assets/images/export.gif' height='16' width='16' border='0' />
					<font color='blue'>Export Menu</font>
				</a>&nbsp;
				<a href='/ta/printnutrition.php?curmenu=<?=$curmenu?>&bid=<?=$businessid?>&cid=<?=$companyid?>' style='<?=$style?>' target='_blank'>
					<img src='/assets/images/detail.gif' height='16' width='16' border='0' />
					<font color='blue'>Print Nutrition</font>
				</a>
			</form>
		</td>
		<td></td>
		<td align='right'>
			<input type='text' id='itemSelectBox' <?php echo $viewonly; ?> size='25' value='' />
		</td>
	</tr>
	<tr bgcolor='#E8E7E7'>
		<td colspan='6'></td>
	</tr>
	<tr bgcolor='black'>
		<td height='1' colspan='6'></td>
	</tr>
</table>

<?php
if( $view1 == 'edit' ) :
?>
<form action='/ta/editcateritem.php' method='post' onSubmit='return disableForm(this);' style="margin:0;padding:0;display:inline;">
<table width='100%' bgcolor='#E8E7E7'>
	<tr>
		<td colspan='2'>
		</td>
	</tr>
	<tr bgcolor='#CCCCCC'>
		<td colspan='2'>
			<input type='hidden' name='companyid' value='<?=$companyid?>' />
			<input type='hidden' name='businessid' value='<?=$businessid?>' />
			<input type='hidden' name='editid' value='<?=$itemeditid?>' />
			<input type='hidden' name='edit' value='1' />
			<b><font color='red'>*</font><u>Edit Menu Item</u></b>
		</td>
	</tr>
	<tr>
		<td align='right' width='20%'>Item Name/Code:</td>
		<td>
			<input type='text' name='item_name' value='<?=$editname?>' size='30' />
			<input type='text' name='item_code' size='20' value='<?=$item_code?>' />
			<input type='checkbox' name='active' value='1' <?=$showactive?> />
			<label for='active'>Active <?=$showrecipe?> <?=$deleteitem?></label>
		</td>
	</tr>
<?php
	if ( $curmenutype == 2 ) :
?>
	<tr>
		<td align='right' width='20%'>Manufacturer UPC:</td>
		<td><input type='text' name='mfrupc' value='<?=$mfrupc?>' size='30'></td>
	</tr>
<?php
	endif;
?>
	<tr>
		<td align='right'>Units:</td>
		<td>
			<input type='text' name='unit' value='<?=$unit?>' size='10' />
			Recipe Serves:
			<input type='text' size='3' name='serving_size' value='<?=$serving_size?>' />
			Min. for Online Order:
			<input type='text' name='order_min' value='<?=$order_min?>' size='3' />
		</td>
	</tr>
<?php
	if ( $contract == 1 ) :
		if ( $label == 1 ) {
			$showcheck = ' checked="checked"';
		}
		else {
			$showcheck = '';
		}
		if ( $contract_option == 1 ) {
			$showcheck2 = ' checked="checked"';
		}
		else {
			$showcheck2 = '';
		}
?>
	<tr>
		<td align='right'>Production Units:</td>
		<td>
			<input type='text' name='su_in_ou' value='<?=$su_in_ou?>' size='5' />
			<select name='true_unit'>
				<?=$sizeList_unit?>
			</select>
			in Order Units:
			<select name='order_unit'>
				<?=$sizeList_order?>
			</select>
			<input type='checkbox' name='label' value='1' <?=$showcheck?> />
			Print Labels
			<input type='checkbox' name='contract_option' value='1' <?=$showcheck2?> />
			Allow Substitution
		</td>
	</tr>
<?php
	endif;
?>
	<tr>
		<td align='right'>Item Price:</td>
		<td>
			<input type='text' name='item_price' value='<?=$editprice?>' size='6' />
			<input type='hidden' name='oldprice' value='<?=$editprice?>' />
			Retail:
			<input type='text' name='retail' value='<?=$retail?>' size='6' />
		</td>
	</tr>
	<tr>
		<td align='right'>Parent:</td>
		<td>
			<select name='parent'>
				<?=$parentList?>
			</select>
		</td>
	</tr>
	<tr>
		<td align='right'>Group Type:</td>
		<td>
			<select name='grouptype'>
				<?=$curmenutype != 2 ? $groupList : $priceGroupList?>
			</select>
		</td>
	</tr>
<?php
	if( $curmenutype == 2 ) :
?>
	<tr>
		<td align='right'>Packaging:</td>
		<td>
			<select name='true_unit'>
				<?=$vendPckgList?>
			</select>
		</td>
	</tr>
	<tr>
		<td align='right'>Case Size:</td>
		<td>
			<input type='text' size='3' name='su_in_ou' value='<?=$su_in_ou?>' />
			<span style='color: red; font-weight: bold;'>THIS IS HOW DRIVERS ORDER (ie. COOKIES BY THE DOZEN)</span>
		</td>
	</tr>
<?php
	endif;
?>
	<tr>
		<td align='right'><i>Testing</i> G/L Sales:</td>
		<td>
			<select name='sales_acct'>
				<?=$creditList?>
			</select>
			Tax:
			<select name='tax_acct'>
				<?=$crTaxList?>
				<option value='0' <?=$tax_acct == 0 ? 'selected="selected"' : ''?>>Not Taxed</option>
			</select>
			Non-Taxed:
			<select name='nontax_acct'>
				<?=$crNoTaxList?>
			</select>
		</td>
	</tr>
	<tr>
		<td align='right'>Description:</td>
		<td>
			<textarea name='description' rows='3' cols='75'><?=$description?></textarea>
		</td>
	</tr>
	<tr>
		<td></td>
		<td>
			<input type='submit' value='Save' />
			<button onclick='window.location.href="menus?bid=<?=$businessid?>&cid=<?=$companyid?>"; return false;'>Return</button>
		</td>
	</tr>
</table>
</form>
<table width='100%' bgcolor='#E8E7E7'>
	<tr bgcolor='#CCCCCC'>
		<td colspan='2'>
			<b><font color='red'>*</font><u>Alternate Pricing</u></b>
		</td>
	</tr>
	<?=$altPriceList?>
	<tr>
		<td colspan='2'>
			<form action='/ta/alt_price.php' method='post' style="margin:0;padding:0;display:inline;">
			<ul style='margin: 0;'><li>
				<input type='hidden' name='itemeditid' value='<?=$itemeditid?>' />
				<input type='hidden' name='businessid' value='<?=$businessid?>' />
				<input type='hidden' name='companyid' value='<?=$companyid?>' />
				<b>$</b><input type='text' size='3' name='price' />
				<input type='text' size='25' name='comment' maxlength='40' />
				<input type='submit' value='Add' />
			</li></ul>
			</form>
		</td>
	</tr>
	<tr bgcolor='#CCCCCC'>
		<td colspan='2'>
			<b><font color='red'>*</font><u>Image</u></b>
		</td>
	</tr>
	<tr>
		<td colspan='2'>
			<form enctype='multipart/form-data' action='/ta/menuimage.php' method='post' target='_blank' style="margin:0;padding:0;display:inline;">
			<ul style='margin: 0;'><li>
				<input type='hidden' name='menu_itemid' value='<?=$itemeditid?>' />
				<input type='hidden' name='MAX_FILE_SIZE' value='10000000000' />
				<input type='file' name='uploadedfile' size='50' />
				<input type='submit' value='Save Picture' />
<?php
	if( !empty( $myimage ) ) :
?>
				<img src='/ta/menuimage/<?=$myimage?>' />
				<a href='/ta/menuimage.php?bid=<?=$businessid?>&cid=<?=$companyid?>&menu_itemid=<?=$itemeditid?>&del=1' target='_blank'>
					<img src='/assets/images/delete.gif' height='16' width='16' alt='Delete' border='0' />
				</a>
<?php
	endif;
?>
				<a href='/ta/usepic.php?bid=<?=$businessid?>&cid=<?=$companyid?>&mid=<?=$itemeditid?>' target='_blank' style=<?=$style?>>
					<font color='blue'>Use Existing Picture</font>
				</a>
			</li></ul>
			</form>
		</td>
	</tr>
	<tr bgcolor='#CCCCCC'>
		<td colspan='2'>
			<b><font color=red>*</font><u>Options for <?=$editname?></u></b>
		</td>
	</tr>
</table>
<?php
	foreach( $optGroupList as $optGroup ) :
?>
<form action='/ta/updateoptions.php' method='post' style='margin:0;padding:0;display:inline;'>
<table width='100%' bgcolor='#E8E7E7'>
	<tr>
		<td colspan='2'>
			<input type='hidden' name='optiongroupid' value='<?=$optGroup['optiongroupid']?>' />
			<input type='hidden' name='companyid' value='<?=$GLOBALS['companyid']?>' />
			<input type='hidden' name='businessid' value='<?=$business->businessid?>' />
			<input type='hidden' name='itemeditid' value='<?=$itemeditid?>' />
			<input type='text' size='3' name='grouporder' value='<?=$optGroup['orderid']?>' />
			<i>
				<u><input type='text' size='30' name='description' value='<?=$optGroup['description']?>' /></u>
				Multiples: <input type='checkbox' name='multiple' value='1' <?=( $type == 1 ) ? 'checked="checked"' : ''?> />
				<a href='/ta/editoptiongroup.php?cid=<?=$companyid?>&bid=<?=$businessid?>&edit=2&editid=<?=$optGroup['optiongroupid']?>&itemeditid=<?=$itemeditid?>' onclick='return delconfirm()'>
					<img border='0' src='/assets/images/delete.gif' height='16' width='16' alt='Delete' />
				</a>
			</i>
		</td>
	</tr>
<?php
		foreach( $optGroup['optionList'] as $opt ) :
?>
	<tr>
		<td colspan='2'>
			<ul style='margin: 0;'><li>
				<input type='text' name='order<?=$opt['num']?>' value='<?=$opt['orderid']?>' size='3' />
				<input type='text' size='30' name='desc<?=$opt['num']?>' value='<?=$opt['description']?>' />
				$<input type='text' size='3' name='price<?=$opt['num']?>' value='<?=money( $opt['price'] )?>' />
				Recipe:
				<select name='link<?=$opt['optionid']?>'>
					<option value='0'></option>
<?php
			foreach( $linkarray as $key => $value ) :
?>
					<option value='<?=$key?>' <?=( $key == $opt['menu_itemid'] ) ? 'selected="selected"' : ''?>>
						<?=$value?>
					</option>
<?php
			endforeach;
?>
				</select>
				<input type='checkbox' name='noprint<?=$opt['num']?>' value='1' <?=( $opt['noprint'] == 1 ) ? 'checked="checked"' : ''?> />
				<font size=1>DON'T PRINT</font>
				<a href='/ta/editoption.php?cid=<?=$companyid?>&bid=<?=$businessid?>&edit=3&editid=<?=$opt['optionid']?>&itemeditid=<?=$itemeditid?>' onclick='return delconfirm2()'>
					<img border='0' src='/assets/images/delete.gif' height='16' width='16' alt='Delete' />
				</a>
			</li></ul>
		</td>
	</tr>
<?php
		endforeach;
?>
	<tr>
		<td>
			<input type='submit' value='Save' />
		</td>
		<td></td>
	</tr>
</table>
</form>
<form action='/ta/editoption.php' method='post'>
<table width='100%' bgcolor='#E8E7E7'>
	<tr>
		<td colspan=2>
			<ul style='margin: 0;'><li>
				<input type='hidden' name='order' value='<?=$maxOrder?>' />
				<input type='hidden' name='optiongroupid' value='<?=$optGroup['optiongroupid']?>' />
				<input type='hidden' name='companyid' value='<?=$companyid?>' />
				<input type='hidden' name='businessid' value='<?=$businessid?>' />
				<input type='hidden' name='itemeditid' value='<?=$itemeditid?>'>
				New Option:
				<input type='text' name='optionname' size='20' />
				Price:
				<input type='text' name='optionprice' size='3' />
				Recipe:
				<select name='link'>
					<option value='0'></option>
<?php
			foreach( $linkarray as $key => $value ) :
?>
					<option value='<?=$key?>' <?=( $key == $opt['optionid'] ) ? 'selected="selected"' : ''?>>
						<?=$value?>
					</option>
<?php
			endforeach;
?>
				</select>
				<input type='submit' value='Add' />
			</li></ul>
		</td>
	</tr>
	<tr height='2' bgcolor='#CCCCCC'>
		<td colspan='2'></td>
	</tr>
</table>
</form>
<?php
	endforeach;
?>
<form action='/ta/editoptiongroup.php' method='post' style="margin:0;padding:0;display:inline;">
<table width='100%' bgcolor='#E8E7E7'>
	<tr>
		<td colspan='2'>
			<ul style='margin: 0;'><li>
				<input type='hidden' name='grouporder' value='<?=$maxGroupOrder?>' />
				<input type='hidden' name='companyid' value='<?=$companyid?>' />
				<input type='hidden' name='businessid' value='<?=$businessid?>' />
				<input type='hidden' name='itemeditid' value='<?=$itemeditid?>'>
				New Option Group:
				<input type='text' name='optiongroupname' size='20' />
				<input type='checkbox' name='multiple' />
				Multiple Options
				<input type='submit' value='Add' />
			</li></ul>
		</td>
	</tr>
</table>
</form>
<?php
endif;
if( $view1 == 'edit' && $curmenutype == 1 ) :
?>
<form action='/ta/copysizes.php' method='post'>
<table width='100%' bgcolor='#E8E7E7'>
	<tr bgcolor='#CCCCCC'>
		<td colspan='2'>
			<b><font color=red>*</font><u>Serving/Production Sizes for $editname</u></b>
			<a href='/ta/businventor4.php?cid=<?=$companyid?>&bid=<?=$businessid?>&editid=<?=$itemeditid?>'>
				<img border='0' src="/assets/images/recipe/recipe.gif" height='16' width='16' alt='EDIT RECIPE' />
			</a>
			<br />
			Copy Serving Sizes:
			<select name='servsize' onChange='changePage(this.form.servsize)'>
				<?=$srvItemList?>
			</select>
		</td>
	</tr>
</table>
</form>
<form action='/ta/editportion.php' method='post'>
<input type='hidden' name='optiongroupid' value='<?=$optionGroupId?>' />
<input type='hidden' name='companyid' value='<?=$companyid?>' />
<input type='hidden' name='businessid' value='<?=$businessid?>' />
<input type='hidden' name='editid' value='<?=$itemeditid?>' />
<table width='100%' bgcolor='#E8E7E7'>
<?php
	foreach( $srvTypeList as $srvType ) :
?>
	<tr>
		<td colspan='2'>
			<i><u><?=$srvType['menu_typename']?></u></i>
		</td>
	</tr>
	<tr>
		<td colspan='2'>
			<table border='1'>
				<tr>
					<td></td>
<?php
		foreach( $srvType['daypart'] as $dp ) :
?>
					<td align='right'>
						<font size='2'><?=$dp['menu_daypartname']?></font>
					</td>
<?php
		endforeach;
?>
				</tr>
<?php
		foreach( $srvType['portion'] as $por ) :
			foreach( $por['day'] as $day ) :
				if( $day['showname'] ) :
?>
				<tr>
					<td align='right'>
						<font size='2'><?=$por['menu_portionname']?></font>
					</td>
<?php
				endif;
?>
					<td width='<?=$day['colwidth']?>%' align='right'>
						<input type='text' size='3' name='<?=$day['var_name2']?>' value='<?=$day['srv_size']?>'
						/>/<input type='text' size='3' name='<?=$day['var_name']?>' value='<?=$day['portionsize']?>' onkeydown='return checkKey()' IndexId=<?=$day['curarrow']?> />
						<font size='2'><?=$day['units']?></font>
					</td>
<?php
				if( $day['showclose'] ) :
?>
				</tr>
<?php
				endif;
			endforeach;
		endforeach;
?>
			</table>
		</td>
	</tr>
<?php
	endforeach;
?>
	<tr bgcolor='#CCCCCC'>
		<td align='right'>
			<input type='submit' value='Save' />
		</td>
		<td></td>
	</tr>
</table>
</form>
<?php
endif;
?>
<table cellspacing='0' cellpadding='0' border='0' width='100%'>
	<tr bgcolor='#E8E7E7'>
		<td height='1' colspan='6'></td>
	</tr>
	<tr>
		<td colspan='6'>
<?php
if( $view1 != 'edit' ) :
?>
			<form action='/ta/editcateritem.php' method='post' onSubmit='return disableForm(this);' style='margin:0;padding:0;display:inline;'>
			<table width='100%' bgcolor='#E8E7E7'>
				<tr>
					<td colspan='2'>
					</td>
				</tr>
				<tr bgcolor='#CCCCCC'>
					<td colspan='2'>
						<input type='hidden' name='companyid' value='<?=$companyid?>' />
						<input type='hidden' name='businessid' value='<?=$businessid?>' />
						<b><u>Add Menu Item</u></b>
					</td>
				</tr>
				<tr>
					<td align='right' width='20%'>Item Name/Code:</td>
					<td>
						<input type='text' name='item_name' size='30' />
						<input type='text' name='item_code' size='20' />
					</td>
				</tr>
<?php
	if( $curmenutype == 2 ) :
?>
				<tr>
					<td align='right' width='20%'>Manufacturer UPC:</td>
					<td><input type='text' name='mfrupc' value='<?=$mfrupc?>' size='30' /></td>
				</tr>
<?php
	endif;
?>
				<tr>
					<td align='right'>Units:</td>
					<td>
						<input type='text' name='unit' value='each' size='10' />
						Recipe Serves:
						<input type='text' size='3' name='serving_size' value='1' />
						Min. for Online Order:
						<input type='text' name='order_min' value='<?=$order_min?>' size='3' />
					</td>
				</tr>
<?php
	if( $contract == 1 ) :
?>
				<tr>
					<td align='right'>Production Units:</td>
					<td>
						<input type='text' name='su_in_ou' size='5' />
						<select name='true_unit'>
							<?=$sizeList?>
						</select>
						in Order Units:
						<select name='order_unit'>
							<?=$sizeList_order?>
						</select>
						<input type='checkbox' name='label' value='1' checked='checked' />
						Print Labels
						<input type='checkbox' name='contract_option' value='1' <?=$showcheck2?> />
						Allow Substitution
					</td>
				</tr>
<?php
	endif;
?>
				<tr>
					<td align='right'>Item Price:</td>
					<td>
						<input type='text' name='item_price' size='6' />
						Retail:
						<input type='text' name='retail' size='6' />
					</td>
				</tr>
				<tr>
					<td align='right'>Parent:</td>
					<td>
						<select name='parent'>
							<?=$parentList?>
						</select>
					</td>
				</tr>
				<tr>
					<td align='right'>Group Type:</td>
					<td>
						<select name='grouptype'>
							<?=$curmenutype != 2 ? $groupList : $priceGroupList?>
						</select>
					</td>
				</tr>
<?php
	if( $curmenutype == 2 ) :
?>
				<tr>
					<td align='right'>Packaging:</td>
					<td>
						<select name='true_unit'>
							<?=$vendPckgList?>
						</select>
					</td>
				</tr>
				<tr>
					<td align='right'>Case Size:</td>
					<td>
						<input type='text' size='3' name='su_in_ou' value='1' />
					</td>
				</tr>
<?php
	endif;
?>
				<tr>
					<td align='right'><i>Testing</i> G/L Sales:</td>
					<td>
						<select name='sales_acct'>
							<?=$creditList?>
						</select>
						Tax:
						<select name='tax_acct'>
							<?=$crTaxList?>
							<option value='0'>Not Taxed</option>
						</select>
						Non-Taxed:
						<select name='nontax_acct'>
							<?=$crNoTaxList?>
						</select>
					</td>
				</tr>
				<tr>
					<td align='right'>Description:</td>
					<td>
						<textarea name='description' rows='3' cols='50'><?=$description?></textarea>
					</td>
				</tr>
				<tr>
					<td></td>
					<td><input type='submit' value='Add Item' <?php echo $viewonly; ?> /></td>
				</tr>
			</table>
			</form>
		</td>
	</tr>
</table>
<?php
endif;
?>
<form action='/ta/updatemenuprice.php' method='post' onSubmit='return disableForm(this);'>
<table cellspacing='0' cellpadding='0' border='0' width='100%'>
	<tr bgcolor='#CCCCCC'>
		<td height='1' colspan='6'>
			<input type='hidden' name='businessid' value='<?=$businessid?>' />
			<input type='hidden' name='companyid' value='<?=$companyid?>' />
			<input type='hidden' name='curmenu' value='<?=$curmenu?>' />
		</td>
	</tr>
	<tr bgcolor='#E8E7E7'>
		<td colspan='6'>
			<a href="#" onclick="hide_show_leads(0); return false;" style="text-decoration:none;">
				<font size='1' color='blue'>[SHOW INACTIVE ITEMS]</font>
			</a>
			<a href="#" onclick="hide_show_leads(1); return false;" style="text-decoration:none;">
				<font size='1' color='blue'>[HIDE INACTIVE ITEMS]</font>
			</a>
		</td>
	</tr>
<?php
foreach( $itemList as $item ) :
	if( $item['showtr'] ) :
?>
	<tr bgcolor='#FFFF99'>
		<td colspan='6'><b><i><?=$item['groupname']?></i></b></td>
	</tr>
	<tr bgcolor='#CCCCCC'>
		<td height='1' colspan='6'></td>
	</tr>
<?php
	endif;
?>
	<tr bgcolor='white'
		<?=$item['deleted'] == 0 ? '' : 'style="display: none;"'?>
		class='<?=$item['deleted'] == 0 ? 'active' : 'inactive'?>'
		onmouseover='this.bgColor="#CCCCCC"'
		onmouseout='this.bgColor="white"'
		id='row<?=$item['menu_counter']?>'>
		<td width='60%'>
			<input type='checkbox' name='edit<?=$item['menu_item_id']?>' value='1' id='<?=$item['menu_item_id']?>' />
<?php
	if ( $item->heart_healthy ) {
?>
			<img src='/assets/images/nutrition/hh1.gif' height='16' width='17' alt="Hearth Healthy" title="Hearth Healthy" />
<?php
	}
	if ( $item->healthy_living ) {
?>
			<img src='/assets/images/nutrition/healthy-living-icon-small.png' height='16' width='16' alt="Healthy Living" title="Healthy Living" />
<?php
	}
	if( $item['shownutr'] ) :
?>
			<a onclick='popup("/ta/nutrition.php?mid=<?=$item['menu_item_id']?>")'>
				<img src='/assets/images/heart.jpg' height='16' width='14' alt='Nutritional Information' border='0' />
			</a>
<?php
	endif;
?>
			<span class='item' id='imgname<?=$item['menu_item_id']?>' <?=$item['deleted'] == 0 ? '' : 'style="text-decoration:line-through;"'?>>
				<?=$item['item_name']?>
			</span>
			<?=$item['parent'] != 0 ? '<font size="2" color="gray">(LINKED)</font>' : ''?>
			<?=$item['recipe_station'] > 0 ? "<font size='2' color='blue'>({$item['station_name']})</font>" : ''?>
			<?=$item['message']?>
		</td>
		<td width='4%'>
			<a href="#" onclick="toggle_active('<?=$item['menu_item_id']?>'); return false;">
				<img border='0'
					id='img<?=$item['menu_item_id']?>'
					src='/assets/images/<?=$item['deleted'] == 0 ? 'on' : 'off' ?>.gif'
					height='15' width='15'
					alt='<?=$item['deleted'] == 0 ? 'ACTIVE' : 'INACTIVE' ?>' />
			</a>
		</td>
		<td width='4%'>
<?php
	if( $item['parent'] >= 0 && $viewonly == '') :
?>
			<a href="#" onclick="edit_menu_item(<?=$item['menu_item_id']?>,<?=$item['menu_counter']?>); return false;">
				<img border='0' src='/assets/images/edit.gif' height='16' width='16' alt='EDIT MENU ITEM' />
			</a>
<?php
	endif;
?>
		</td>
		<td width='4%'>
<?php
	if( $item['parent'] >= 0  && $viewonly == '') :
?>
			<a href="#" onclick="del_menu_item('<?=$item['menu_item_id']?>','<?=$item['menu_counter']?>'); return false;">
				<img border='0' src='/assets/images/delete.gif' height='16' width='16' alt='DELETE' id='del<?=$item['menu_item_id']?>' />
			</a>
<?php
	endif;
?>
		</td>
		<td>
			<a href='/ta/businventor4.php?cid=<?=$companyid?>&bid=<?=$businessid?>&editid=<?=$item['menu_item_id']?>'>
				<img border='0' src="/assets/images/recipe/recipe.gif" height='16' width='16' alt='EDIT RECIPE' />
			</a>
		</td>
		<td align='right'>
			$<input type='text' size='4'
				value='<?=$item['price']?>'
				name='<?=$item['menu_item_id']?>'
				tabindex='1' onfocus='this.select();'
				onchange="<?php echo 'row', $item['menu_counter']; ?>.style.backgroundColor='yellow'"
				style='font-size: 10px;'
				id='price<?=$item['menu_item_id']?>' />
		</td>
	</tr>
	<tr bgcolor='#E8E7E7' id='rowC<?=$item['menu_counter']?>' style="display:none;height:600px;">
		<td colspan='6'>
			<span id='editme<?=$item['menu_item_id']?>' style="height:600px;"></span>
		</td>
	</tr>
	<tr bgcolor='orange' id='rowD<?=$item['menu_counter']?>' style="display:none">
		<td colspan='6' align='right'>
			<input type='button' value='Close' onclick="close_menu_item(<?=$item['menu_item_id']?>,<?=$item['menu_counter']?>);" />
		</td>
	</tr>
	<tr bgcolor='#CCCCCC'
		<?=$item['deleted'] == 0 ? '' : 'style="display: none;"'?>
		id='rowB<?=$item['menu_counter']?>'
		class='<?=$item['deleted'] == 0 ? 'active' : 'inactive'?>' >
		<td height='1' colspan='6'></td>
	</tr>
<?php
endforeach;
?>
	<tr bgcolor='#E8E7E7'>
		<td colspan='4'>
			&nbsp;<img src='/assets/images/turnarrow.gif' height='16' width='16'>
			<select name='myupdate'>
				<option value='0'>Please Choose...</option>
				<option value='2'>Make Inactive</option>
				<option value='3'>Make Active</option>
				<optgroup label='OR Update Recipe Group to...'>
					<?=$serveryList?>
				</optgroup>
				<optgroup label='OR Update Menu Group to...'>
					<?=$uMenuList?>
				</optgroup>
				<optgroup label='Recipe Choices'>
					<option value='5:1'>Disable Internal Viewing of Recipe</option>
					<option value='5:0'>Enable Internal Viewing of Recipe</option>
					<option value='6:1'>Disable Customer Viewing of Recipe</option>
					<option value='6:0'>Enable Customer Viewing of Recipe</option>
				</optgroup>
			</select>
		</td>
		<td height='1' colspan='2' align='right'>
			<input type='submit' value='Save' <?php echo $viewonly; ?> onclick='return confirmsave()' />
		</td>
	</tr>
	<tr bgcolor='#E8E7E7' height='1'>
		<td colspan='6'></td>
	</tr>
</table>
</form>
<?php
if( $curmenutype == 1 ) :
?>
<p style='text-align: center;'><a name='matrix'></a></p>
<form action='/ta/saveordermatrix.php' method='post'>
<table width='100%' cellspacing='0' cellpadding='0'>
	<tr>
		<td colspan='<?=$mxColnum2?>' height='1' bgcolor='black'>
			<input type='hidden' name='businessid' value='<?=$businessid?>' />
			<input type='hidden' name='companyid' value='<?=$companyid?>' />
		</td>
	</tr>
	<tr>
		<td width='1' bgcolor='black'></td>
		<td colspan='<?=$mxColnum?>' style='filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr=white, startColorstr=#DCDCDC, gradientType=0);'>
			<b>Price Matrix</b>
		</td>
		<td width='1' bgcolor='black'></td>
	</tr>
<?php
	foreach( $mxDayList as $mxDay ) :
?>
	<tr bgcolor='black' height='1'>
		<td colspan='<?=$mxColnum2?>'>
			<input type='hidden' name='username' value='<?=$user?>'>
			<input type='hidden' name='password' value='<?=$pass?>'>
		</td>
	</tr>
	<tr>
		<td width='1' bgcolor='#E8E7E7'></td>
		<td bgcolor='white'><b><?=$mxDay['menu_daypartname']?></b></td>
<?php
		foreach( $mxDay['portion'] as $portionname ) :
?>
		<td align='right' bgcolor='white'><b><?=$portionname?></b></td>
<?php
		endforeach;
?>
		<td width='1' bgcolor='#E8E7E7'></td>
	</tr>
	<tr bgcolor='#E8E7E7' height='1'>
		<td colspan='<?=$mxColnum2?>'></td>
	</tr>
	<tr>
		<td width='1' bgcolor='#E8E7E7'></td>
		<td bgcolor='white'><i>Price:</i></td>
<?php
		foreach( $mxDay['portionList'] as $portion ) :
?>
		<td align='right' bgcolor='white'>
			<b>$</b><input type='text' size='4'
							name='<?=$portion['var_name']?>' value='<?=$portion['portionvalue']?>'
							onkeydown='return checkKey()' IndexId='<?=$portion['curarrow']?>' />
		</td>
<?php
		endforeach;
?>
		<td width='1' bgcolor='#E8E7E7'></td>
	</tr>
<?php
	endforeach;
?>
	<tr>
		<td colspan='<?=$mxColnum2?>' height='1' bgcolor='black'></td>
	</tr>
	<tr>
		<td width='1' bgcolor='black'></td>
		<td colspan='<?=$mxColnum?>' align='right'
			style='filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr=#DCDCDC, startColorstr=white, gradientType=0);'>
			<input type='submit' value='Save' <?php echo $viewonly; ?> onclick='return confirmsave()' />
		</td>
		<td width='1' bgcolor='black'></td>
	</tr>
	<tr>
		<td colspan='<?=$mxColnum2?>' height='1' bgcolor='black'></td>
	</tr>
</table>
</form>
<?php
endif;
?>
