<?php

$ordertypes = array();

$query = "SELECT * FROM menu_items_ordertype ORDER BY id";
$result = DB_Proxy::query($query);

while($r = mysql_fetch_array($result)){
	$id = $r["id"];
	$name = $r["name"];

	$ordertypes[$id] = $name;
}


	$style = "text-decoration:none;";
	
	?>

	<style type="text/css">
		.matched {background-color: #ccf; font-weight: bold;}
		.keyHover {background-color: #39f; color: white; border: 1px dotted red;}
		.mouseHover {background-color: #39f; border: 1px dotted red; color: white;}

		#autoBox{background: url('images/down_arrow.gif') no-repeat center right; background-color: white; border: 1px solid #1b1b1b;}
		#autoBox.hover{background: url('images/down_arrow_hover.gif') no-repeat center right; background-color: white; border: 1px solid #3c7fb1;}

		.dispVal{font-weight: bold; color: green; text-decoration: underline;}

		#loading{width: 200px; height: 48px; position: absolute; left: 50%; top: 50%; margin-top: -50px; margin-left: -100px; text-align: center;}
	</style>

	<?php
	
	// create groups array
	$groups = array();
	$query = "SELECT * FROM menu_groups_new WHERE businessid = ".$businessid;
	$result = DB_Proxy::query($query);					

	while($r = mysql_fetch_array($result)){
		$id = $r["id"];
		$name = $r["name"];

		$groups[$id] = $name;
	}

	// create ordertype array
	$ordertypes = array();

	$query = "SELECT * FROM menu_items_ordertype ORDER BY id";
	$result = DB_Proxy::query($query);

	while($r = mysql_fetch_array($result)){
		$id = $r["id"];
		$name = $r["name"];

		$ordertypes[$id] = $name;
	}

	// create isactive array
	$inactive_array = array(0 => 'active', 1 => 'inactive');

	// create taxes active array
	//$taxes_active = array(0 => 'inactive', 1 => 'active');
	$taxes_active = array(1 => 'active', 0 => 'inactive');	
	?>
	
	<script>
	$(document).ready(function(){
		var ajax_url = '/ta/businessMenus/posSaveAjax?bid=<?php print $businessid; ?>&cid=<?php print $companyid; ?>';	

		var edit_sel_group = window.setTimeout(function(){
			$('.edit_select_group').editable(ajax_url, { 
				data   : '<?php print json_encode($groups); ?>',
				type   : 'select',
				submit : 'Save',
				cancel : 'Cancel',
				indicator : 'Saving group...',
				tooltip   : 'Click to edit Group...'
			});
		},75);

		var edit_sel_order = window.setTimeout(function(){
			$('.edit_select_order').editable(ajax_url, { 
				data   : '<?php print json_encode($ordertypes); ?>',
				type   : 'select',
				submit : 'Save',
				cancel : 'Cancel',
				indicator : 'Saving order type...',
				tooltip   : 'Click to edit Order Type...'
			});
		},150);

		var edit_sel_active = window.setTimeout(function(){
			$('.edit_select_active').editable(ajax_url, { 
				data   : '<?php print json_encode($inactive_array); ?>',
				type   : 'select',
				submit : 'Save',
				cancel : 'Cancel',
				indicator : 'Saving active...',
				tooltip   : 'Click to edit Inactive...'
			});
		},225);

		var edit_sel_taxes = window.setTimeout(function(){
			$('.edit_select_taxes').editable(ajax_url, {
				data   : '<?php print json_encode($taxes_active); ?>',
				type   : 'select',
				submit : 'Save',
				cancel : 'Cancel',
				indicator : 'Saving tax...',
				tooltip   : 'Click to edit Tax...'
			});
		},300);

		var edit_item_code = window.setTimeout(function(){
			$('.edit_item_code').editable(ajax_url, {
				cancel    : 'Cancel',
				submit    : 'Save',
				tooltip   : 'Click to edit Item Code...',
				select : 'true'
			});
		},375);

		var edit_upc = window.setTimeout(function(){
			$('.edit_upc').editable(ajax_url, {
				cancel    : 'Cancel',
				submit    : 'Save',
				tooltip   : 'Click to edit UPC...',
				select : 'true'
			});
		},450);

		var edit_max = window.setTimeout(function(){
			$('.edit_max').editable(ajax_url, {
				cancel    : 'Cancel',
				submit    : 'Save',
				tooltip   : 'Click to edit Max...',
				select : 'true'
			});
		},525);

		var edit_reorder_point = window.setTimeout(function(){
			$('.edit_reorder_point').editable(ajax_url, {
				cancel    : 'Cancel',
				submit    : 'Save',
				tooltip   : 'Click to edit Reorder Point...',
				select : 'true'
			});
		},600);

		var edit_reorder_amount = window.setTimeout(function(){
			$('.edit_reorder_amount').editable(ajax_url, {
				cancel    : 'Cancel',
				submit    : 'Save',
				tooltip   : 'Click to edit Reorder Amount...',
				select : 'true'
			});
		},675);

		var edit_cost = window.setTimeout(function(){
			$('.edit_cost').editable(ajax_url, {
				cancel    : 'Cancel',
				submit    : 'Save',
				tooltip   : 'Click to edit Cost...',
				select : 'true'
			});
		},750);

		var edit_new_price = window.setTimeout(function(){
			$('.edit_new_price').editable(ajax_url, {
				cancel    : 'Cancel',
				submit    : 'Save',
				tooltip   : 'Click to edit New Price...',
				select : 'true'
			});
			$('#loading').hide();
		},825);
	});
	</script>
	
	<div id="loading">
		<div id="overlay"></div>
		<div id="message">
			Loading... Please Wait
			<br>
			<img width="126" height="22" src="/assets/images/preload/loading.gif">
		</div>
	</div>

	<table cellspacing=0 cellpadding=0 border=0 width=100%>
		<tr bgcolor=#E8E7E7>
			<td>
				<form method="get" action="posMultiEditNew" style="padding:5px;">
					<input type="hidden" name="bid" value="<?php print $businessid; ?>" />
					<input type="hidden" name="cid" value="<?php print $companyid; ?>" />

					Order Type: <select name="ordertype">
						<?php
						foreach($ordertypes as $key => $value){
							$sel_ot = ($ordertype_selected == $key) ? "selected='selected'" : "";
							print "<option $sel_ot value='$key'>$value</option>";
						}
						?>
					</select>
					
					<select name="active">
						<option<?php print ($active == 0) ? " selected='selected'" : ""; ?> value="0">Active Items</option>
						<option<?php print ($active == 1) ? " selected='selected'" : ''; ?> value="1">Inactive Items</option>						
					</select>

					<input type="submit" value="Submit" />
				</form>				
			</td>
			<td align="right">
				<div id="import_dialog" class="ui-hidden"></div>

				<?php if( $bus_menu_pos == 4 ): ?>
					<a href="#" style="text-decoration:none;" onclick="import_menu(); return false;">Import</a> |
				<?php endif; ?>

				<a style="text-decoration:none;" href="/ta/export_menu.php?bid=<?php echo $GLOBALS['businessid'] ?>">
				<font color=blue>Export</font></a>&nbsp;

				| <a style="text-decoration:none;" href="posMenu?bid=<?php echo $businessid ?>&cid=<?php echo $companyid ?>">
				<font color=blue>Single Item Edit</font></a>&nbsp;

			</td>
		</tr>
		<tr height=1 bgcolor=black valign=top>
			<td colspan=2>
				<img src="/assets/images/spacer.gif" alt="spacer.gif" />
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<form method="post" style="margin:0;padding:0;display:inline;">
					<input type="hidden" name="businessid" value="<?php print $businessid; ?>" />
					<input type="hidden" name="companyid" value="<?php print $companyid; ?>" />

					<table width="100%" cellspacing="0" cellpadding="0" style="border:5px solid #E8E7E7;font-size:12px;">
						<?php
						// get machine numbers tied to this business
						$query = "SELECT machine_num FROM machine_bus_link WHERE businessid = $businessid";
						$result = DB_Proxy::query($query);

						$machine_num = array();
						$counter=0;
						while($r=mysql_fetch_array($result)){
							$machine_num[$counter] = $r["machine_num"];
							$counter++;
						}										

						// taxes
						$query_taxes = "SELECT * FROM menu_tax WHERE businessid = $businessid ORDER BY pos_id";  //all of tax rates for business
						$result_taxes = Treat_DB_ProxyOld::query( $query_taxes );

						$tax_rates = array();
						$tx_ctr = 0;

						while( $r_taxes = mysql_fetch_array( $result_taxes ) ) {
							$tax_rates[$tx_ctr]['tax_id'] = $r_taxes["id"];
							$tax_rates[$tx_ctr]['tax_name'] = $r_taxes["name"];
							$tax_rates[$tx_ctr]['tax_percent'] = $r_taxes["percent"];
							$tx_ctr++;
						}
	
						if($active==1){							
							$query = "SELECT id,
										businessid,
										pos_id,
										group_id,
										name,
										ordertype,
										active,
										item_code,
										upc,
										reorder,
										max,
										reorder_point,
										reorder_amount
									FROM menu_items_new
									WHERE businessid = $businessid
									AND active = 1
									AND ordertype = $ordertype_selected
									ORDER BY name";
						} else {
							$query = "SELECT id,
										businessid,
										pos_id,
										group_id,
										name,
										ordertype,
										active,
										item_code,
										upc,
										reorder,
										max,
										reorder_point,
										reorder_amount
									FROM menu_items_new
									WHERE businessid = $businessid
									AND active = 0
									AND ordertype = $ordertype_selected
									ORDER BY name";
						}
						//print $query;
						
						$result = DB_Proxy::query($query);

						// list items
						while($r = mysql_fetch_array($result)){
							$id = $r["id"];
							$pos_id = $r["pos_id"];
							$group = $r["group_id"];
							$name = $r["name"];
							$ordertype = $r["ordertype"];
							$active = $r["active"];
							$item_code = $r["item_code"];
							$upc = $r["upc"];
							$max = $r["max"];
							$reorder_point = $r["reorder_point"];
							$reorder_amount = $r["reorder_amount"];
						
							$query2 = "SELECT price,new_price,cost FROM menu_items_price WHERE menu_item_id = $id";
							$result2 = DB_Proxy::query($query2);

							$price = mysql_result($result2,0,"price");
							$new_price = mysql_result($result2,0,"new_price");
							$cost = mysql_result($result2,0,"cost");

							$price = number_format($price,2);
							$new_price = number_format($new_price,2);
							if($new_price == 0){$new_price = "";}						

							if($ordertype == 1){$sel1="SELECTED";}
							elseif($ordertype == 2){$sel2="SELECTED";}
							elseif($ordertype == 3){$sel3="SELECTED";}							
						
							$bgcolor = ($active == 1) ? '#CCCCCC' : '#FFFFFF';
							?>
							<tr bgcolor="<?php print $bgcolor; ?>" onMouseOver="this.bgColor='yellow'" onMouseOut="this.bgColor='<?php print $bgcolor; ?>'" class="ot<?php print $ordertype; ?>" style="<?php print $showstyle; ?>">
								<td title="Item Code" style="border:1px solid #E8E7E7;">
									<div class="edit_item_code" id="item_code|<?php print $id; ?>"><?php print $item_code; ?></div>
								</td>
							
								<td title="Group" style="border:1px solid #E8E7E7;">								
									<?php
									foreach($groups AS $key => $value){
										if($group == $key){
											$sel_group_val = $value;
										}
									}
									?>								
									<div class="edit_select_group" id="group|<?php print $id; ?>"><?php print $sel_group_val; ?></div>
								</td>							
							
								<td title="Item Name" style="border:1px solid #E8E7E7;">&nbsp;<?php print $none_sold.$name; ?></td>

								<td title="Order Type" style="border:1px solid #E8E7E7;">								
									<?php
									foreach($ordertypes AS $ot_id => $ot_name){
										if($ordertype == $ot_id){
											$sel_ordertype_val = $ot_name;
										}
									}
									?>
									<div class="edit_select_order" id="ordertype|<?php print $id; ?>"><?php print $sel_ordertype_val; ?></div>
								</td>
							
								<td title="Inactive" style="border:1px solid #E8E7E7;" align=center>
									<div class="edit_select_active" id="active|<?php print $id; ?>"><?php print $active == 1 ? 'inactive' : 'active'; ?></div>
								</td>
								<td title="UPC" style="border:1px solid #E8E7E7;">
									<div class="edit_upc" id="upc|<?php print $id; ?>"><?php print $upc; ?></div>
								</td>
								<td title="Max" style="border:1px solid #E8E7E7;width:50px;" align="left">
									<div class="edit_max" id="max|<?php print $id; ?>"><?php print $max; ?></div>
								</td>
								<td title="ReOrder Point" style="border:1px solid #E8E7E7;width:50px;" align="left">
									<div class="edit_reorder_point" id="reorder_point|<?php print $id; ?>"><?php print $reorder_point; ?></div>
								</td>
								<td title="ReOrder Amount" style="border:1px solid #E8E7E7;width:50px;" align="left">
									<div class="edit_reorder_amount" id="reorder_amount|<?php print $id; ?>"><?php print $reorder_amount; ?></div>
								</td>						
							
								<td title="Taxes" style="border:1px solid #E8E7E7;" align="left">								
									<?php
									foreach($tax_rates as $tax_rate){
										$query_taxes2 = "SELECT * FROM menu_item_tax WHERE tax_id = ".$tax_rate['tax_id']." AND menu_itemid = $id";
										$result_taxes2 = Treat_DB_ProxyOld::query( $query_taxes2 );

										$has_tax = @mysql_result( $result_taxes2, 0, 'tax_id' );

										if( $has_tax > 0 ) {
											$tax_checked = '<strong>active</strong> - '.$tax_rate["tax_name"].' ('.$tax_rate["tax_percent"].'%)';
										} else {
											$tax_checked = '<strong>inactive</strong> - '.$tax_rate["tax_name"].' ('.$tax_rate["tax_percent"].'%)';
										}
										?>
										<div class="edit_select_taxes" style="margin-top:5px;" id="tax|<?php print $id; ?>|<?php print $tax_rate['tax_id']; ?>"><?php print $tax_checked; ?></div>
										<?php
									}
									?>								
								</td>						
							
								<td title="Cost" style="border:1px solid #E8E7E7;" align="right">
									<div class="edit_cost" id="cost|<?php print $id; ?>"><?php print $cost; ?></div>
								</td>
								<td title="Current Price" style="border:1px solid #E8E7E7;" align="right">
									<b><?php print $price; ?></b>
								</td>
								<td title="New Price" style="border:1px solid #E8E7E7;" align="right">
									<div class="edit_new_price" id="new_price|<?php print $id; ?>"><?php print (isset($new_price) ? $new_price : 'new price'); ?></div>
								</td>
							</tr>
							<?php
						}
						?>
					</table>
				</td>
			</tr>
		</table>