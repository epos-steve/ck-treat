<?php if ($messages) { ?>
    <div id="dialog-message" title="Results">
    <?php foreach( $messages as $message ) { ?>
    <p><?php echo $message; ?></p>
    <?php } ?>
    </div>
    <script type="text/javascript">

    function goBack( ) {
        <?php if ( $new == 1 ) { ?>
        window.location="<?php echo $messages_location ?>";
        <?php } ?>
    }

    jQuery( "#dialog-message" ).dialog(
        {
            width: 750,
            modal: true,
            close: function( ) {
                goBack();
            },
            buttons: {
                Ok: function( ) {
                    goBack();
                }
            }
        });
    </script>
<?php } ?>
<?php if ($new != 1 ) { ?>
    <script language="JavaScript" type="text/JavaScript">

        jQuery(document).ready(function() {

            jQuery('#checkme').click(function () {
                if(jQuery(this).is(':checked')){
                    jQuery('.checkall').attr('checked', 'checked');
                }
                else{
                    jQuery('.checkall').attr('checked', '');
                }
            });
        });

        jQuery(document).ready(function() {
            jQuery('#addme').click(function () {
                if(jQuery(this).is(':checked')){
                    jQuery('.additem').attr('checked', 'checked');
                }
                else{
                    jQuery('.additem').attr('checked', '');
                }
            });
        });

        function changename(){
            var name = jQuery('#newname').val();
            jQuery('.itemname').val(name);
        }
        function changeupc(){
            var upc = jQuery('#newupc').val();
            jQuery('.itemupc').val(upc);
        }
        function changecost(){
            var cost = jQuery('#newcost').val();
            jQuery('.itemcost').val(cost);
        }
        function changeprice(){
            var price = jQuery('#newprice').val();
            jQuery('.itemprice').val(price);
        }
        function changeordertype(){
            var ordertype = jQuery('#newordertype option:selected').text();
            //alert(ordertype);
            jQuery('.itemordertype').each(
                function() {
                    jQuery(this).children('option').each(
                        function(){
                            if(jQuery(this).text() == ordertype){
                                jQuery(this).attr('selected','selected');
                            }
                            else{
                                jQuery(this).attr('selected','');
                            }
                        }
                    );
                }
            );
        }
        function changegroup(){
            var group = jQuery('#newgroup option:selected').text();
            //alert(ordertype);
            jQuery('.itemgroup').each(
                function() {
                    jQuery(this).children('option').each(
                        function(){
                            if(jQuery(this).text() == group){
                                jQuery(this).attr('selected','selected');
                            }
                            else{
                                jQuery(this).attr('selected','');
                            }
                        }
                    );
                }
            );
        }
    </script>
<?php } ?>

<style>
    .savePosMenu table {
        border: 1px solid black;
        font-size: 11px;
        text-align: center;
    }

    .savePosMenu td {
        font-size:11px;
        border: 1px solid black;
    }

    .savePosMenu select {
        font-size:11px;
    }

    .savePosMenu input {
        font-size:11px;
    }
</style>

<div>
    <br>
    <br>
</div>
<form action="savePosItems" method="post">
<table class="savePosMenu" cellspacing="0" cellpadding="1" width="99%">
    <thead>
    <tr bgcolor="#FFFF99">
        <td>ADD</td>
        <td>Business</td>
        <td>Item Code</td>
        <td>Group</td>
        <td>Order Type</td>
        <td>Item Name</td>
        <td>Inactive</td>
        <td>UPC</td>
        <td>Cost</td>
        <td>Price</td>
        <td>Taxes</td>
    </tr>
    <tr bgcolor="red">
        <td><input type="checkbox" id="addme"></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>
            <select id="newgroup" tabindex=1 onchange="changegroup();"><option value="0"></option>
            <?php foreach ( $menu_groups as $menu_group ) { ?>
                <option value=<?php echo $menu_group->id; ?>><?php echo $menu_group->name; ?></option>
            <?php } ?>
                ?>
            </select>
        </td>
        <td style="border:1px solid black;"><select id="newordertype" tabindex=2 onchange="changeordertype();">
                <option value="0"></option>
                <?php foreach( $order_type_select as $row ) { ?>
                    <option value="<?php echo $row->id; ?> <?php echo $row->sel; ?>"><?php echo $row->name; ?></option>
                <?php } ?>
            </select>
        </td>
        <td style="border:1px solid black;"><input type=text size=25 id="newname" name="changeitemname" onkeyup="changename();" tabindex=3 value=''></td>
        <td><input type="checkbox" id="checkme" tabindex="4"></td>
        <td><input type=text size=20 id="newupc" name="changeitemupc" onkeyup="changeupc();" style="font-size:11px;" tabindex=5 value=''></td>
        <td><input type=text size=5 id="newcost" name="changeitemcost" onkeyup="changecost();" style="font-size:11px;" tabindex=6 value=''></td>
        <td><input type=text size=5 id="newprice" name="changeitemprice" onkeyup="changeprice();" style="font-size:11px;" tabindex=7 value=''></td>
        <td>&nbsp;</td>
    </tr>
    </thead>
    <tbody>
    <input type=hidden name=return value='<?php echo $http_referer; ?>'>
    <input type=hidden name=districtid value=<?php echo $district_id; ?>>
    <input type=hidden name=originalItemCode value='<?php echo $original_item_code; ?>'>

    <?php $counter = 1; ?>
    <?php foreach ( $menu_items as $menu_item ) { ?>
    <tr bgcolor='<?php echo $menu_item->mycolor; ?>'>
        <td>
            <?php if( empty($menu_item->id ) ) { ?>
                <?php $menu_item->id = "-$counter"; ?>
                <input type=checkbox class="additem" name='count<?php echo $counter; ?>' value=1>
            <?php } else { ?>
                &nbsp;
            <?php } ?>
        </td>
        <td><?php echo $menu_item->businessname; ?></td>
        <td><?php echo $menu_item->item_code; ?></td>
        <td>
            <select name='grp[<?php echo $menu_item->id; ?>]' class="itemgroup" tabindex=1>
            <?php foreach( $menu_item->new_menu_groups as $new_menu_group ) { ?>
                <option value='<?php echo $new_menu_group->id; ?>'
                    <?php echo ( $menu_item->group_id == $new_menu_group->id ? 'SELECTED' : ''); ?>><?php echo $new_menu_group->name; ?></option>
            <?php } ?>
            </select>
        </td>
        <td>
            <select class="itemordertype" name='ot[<?php echo $menu_item->id; ?>]' tabindex=2>
            <?php foreach( $order_type_select as $row ) { ?>
                <option value="<?php echo $row->id; ?>"
                    <?php echo ( $row->id == $menu_item->ordertype ? 'SELECTED' : ''); ?>><?php echo $row->name; ?></option>
            <?php } ?>
            </select>
        </td>
        <td>
            <input type=text size=25 class="itemname" name='name[<?php echo $menu_item->id; ?>]'
                   tabindex=3 value='<?php echo $menu_item->name; ?>'>
        </td>
        <td>
            <input type=checkbox name='active[<?php echo $menu_item->id; ?>]' class="checkall"
                   value=1 tabindex=4 <?php echo ( $menu_item->active == 1 ? 'CHECKED' : ''); ?>>
        </td>
        <td>
            <input type=text size=20 class="itemupc" name='upc[<?php echo $menu_item->id; ?>]'
                   tabindex=5 value='<?php echo $menu_item->upc; ?>'>
        </td>
        <td>
            <input type=text size=5 class="itemcost" name='cost[<?php echo $menu_item->id; ?>]'
                   tabindex=6 value='<?php echo $menu_item->formated_cost; ?>' title="Current Cost: <?php echo $menu_item->cost; ?>">
        </td>
        <td>
            <input type=text size=5 class="itemprice" name='price[<?php echo $menu_item->id; ?>]' style="font-size:11px;"
                   tabindex=7 value='<?php echo $menu_item->formated_price; ?>' title="Sync Price: <?php echo $menu_item->new_price; ?>">
        </td>
        <td>
            <?php foreach( $menu_item->new_menu_items_taxs as $tax ) { ?>
                <input type=checkbox name='tax<?php echo $menu_item->id; ?>-<?php echo $tax->id; ?>' tabindex=8 value=1
                       <?php echo ( $tax->menu_itemid > 0 ? 'CHECKED' : ''); ?>><?php echo $tax->name. '('. $tax->percent . '%)'; ?>
            <?php } ?>
        </td>
    </tr>
    <?php $counter++; ?>
    <?php } ?>
    <?php $counter--; ?>
    </tbody>
        <tfoot>
        <tr>
            <td colspan="11" bgcolor="#FFFF99" align="right">
                <?php echo $counter ?> location(s), <?php echo $missing ?> location(s) missing this product
                <input type="submit" value="Save">
            </td>
        </tr>
        </tfoot>
    </table>
</form>
<center>
    <form action="<?php echo $http_referer ?>" method="post">
        <input type="submit" value="Cancel">
    </form>
</center>
