<?php $style = "text-decoration:none;"; ?>

<style type="text/css">
	.matched {background-color: #ccf; font-weight: bold;}
	.keyHover {background-color: #39f; color: white; border: 1px dotted red;}
	.mouseHover {background-color: #39f; border: 1px dotted red; color: white;}

	#autoBox{background: url('images/down_arrow.gif') no-repeat center right; background-color: white; border: 1px solid #1b1b1b;}
	#autoBox.hover{background: url('images/down_arrow_hover.gif') no-repeat center right; background-color: white; border: 1px solid #3c7fb1;}

	.dispVal{font-weight: bold; color: green; text-decoration: underline;}
	
	#loading{width: 200px; height: 48px; position: absolute; left: 50%; top: 50%; margin-top: -50px; margin-left: -100px; text-align: center;}
	
	.active, .active th, .active td {
	}
	.inactive, .inactive th, .inactive td {
	}
	tr.active:hover, tr.inactive:hover {
		background-color: yellow;
	}
	.small {
		width: 47px;
	}
	td.small {
		width: 49px;
	}
	.medium {
		width: 102px;
	}
	td.medium {
		width: 104px;
	}
	.menu-items {
		border:5px solid #E8E7E7;
		font-size:12px;
		width: 100%;
	}
	.menu-items thead tr {
		background-color: #FFFF99;
	}
	.menu-items thead td, .item-header {
		cursor: pointer;
		border:1px solid #E8E7E7;
		font-size:14px;
	}
	.menu-items tbody td {
		border:1px solid #E8E7E7;
	}
	.menu-items tbody td input, .menu-items tbody td select {
		font-size: 12px;
	}
	td.right {
		text-align: right;
	}
	td.center {
		text-align: center;
	}
</style>

<table cellspacing=0 cellpadding=0 border=0 width=100%>
	<tr bgcolor=#E8E7E7>
		<td>&nbsp;<?
			if($showall == 1){
				echo "<a href=posMultiEdit?bid=$businessid&cid=$companyid&showall=2 style=\"text-decoration:none;\"><font size=1 color=blue>[ACTIVE ITEMS]</font></a>";
			} else {
				echo "<a href=posMultiEdit?bid=$businessid&cid=$companyid&showall=1 style=\"text-decoration:none;\"><font size=1 color=blue>[ALL ITEMS]</font></a>";
			}
			?>
		</td>
		<td align="right">
			<div id="import_dialog" class="ui-hidden"></div>
			
			<?php if( $bus_menu_pos == 4 ): ?>
				<a href="#" style="text-decoration:none;" onclick="import_menu(); return false;">Import</a> |
			<?php endif; ?>
			
			<a style="text-decoration:none;" href="/ta/export_menu.php?bid=<?php echo $GLOBALS['businessid'] ?>">
			<font color=blue>Export</font></a>&nbsp;
			
			| <a style="text-decoration:none;" href="posMenu?bid=<?php echo $businessid ?>&cid=<?php echo $companyid ?>">
			<font color=blue>Single Item Edit</font></a>&nbsp;
			
		</td>
	</tr>
	<tr height=1 bgcolor=black valign=top>
		<td colspan=2>
			<img src="/assets/images/spacer.gif" alt="spacer.gif" />
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<form action="posSaveMulti" method="post" id="pos-save-multi-form"
			      style="margin:0;padding:0;display:inline;"
			      data-business-id="<?php echo $businessid; ?>"
			      data-company-id="<?php echo $companyid; ?>"
			      data-showall="<?php echo $showall; ?>">

				<input type="hidden" name="businessid" value="<?php echo $businessid; ?>" />
				<input type="hidden" name="companyid" value="<?php echo $companyid; ?>" />

				<?php foreach($menu_items as $ordertype_id => $category_items) : ?>
				<table width="100%" cellspacing="0" cellpadding="0"
				       class="menu-items table-update"
				       data-ordertype-id="<?php echo $ordertype_id; ?>">
					<thead>
						<tr>
							<td colspan="15" class="item-header">
								&nbsp;
								<img src="/ta/exp.gif" height="16" width="16" class="menu-item-close"<?php if($ordertype_id > 1 || ($ordertype_id == 1  && $showall == 1)) : ?> style="display: none;"<?php endif; ?> border="0">
								<img src="/ta/exp2.gif" height="16" width="16" class="menu-item-open"<?php if(($ordertype_id == 1  && $showall != 1) || $ordertype_id == 0) : ?> style="display: none;"<?php endif; ?> border="0">

								<b><i><?php
								if($ordertype_id > 0) :
									echo $ordertypes[$ordertype_id];
								else : ?><img src="/assets/images/error.gif">Unassigned<?php
								endif;
								?> Items</i></b>
							</td>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($category_items as $info) : ?>
						<tr class="<?php if($info['item']->active == 0) : echo 'active'; else: echo 'inactive'; endif; ?>">
							<td title="Item Code" class="medium" style="width: 77px;">
								<input type="text" class="medium update-item" style="width: 75px;"
									   name="item_code<?php echo $info['item']->id; ?>" 
									   value="<?php echo $info['item']->item_code; ?>"
									   data-id="<?php echo $info['item']->id; ?>"
									   data-field-name="item_code"
										data-current-value="<?php echo $info['item']->item_code; ?>">
							</td>
							<?php /*
							<td title="Manufacturer" style="border:1px solid #E8E7E7;">
								<select name="manufacturer<?php echo $info['item']->id; ?>">
									<option value="0"></option>
								<?php foreach($manufacturers AS $key => $value) : ?>

									<option value="<?php echo $key; ?>"<?php if($info['item']->manufacturer == $key) : ?> selected="selected"<?php endif; ?>><?php echo $value; ?></option>
								<?php endforeach; ?>
								</select>
							</td>
							 */ ?>
							<td title="Group" style="width: 100px;">
								<select class="update-item"
								        name="group<?php echo $info['item']->id; ?>"
								        data-id="<?php echo $info['item']->id; ?>"
								        data-field-name="group_id"
								        data-current-value="<?php echo $info['item']->group_id; ?>">
									<option value="0"></option>
								<?php foreach($groups AS $key => $value) : ?>

									<option value="<?php echo $key; ?>"<?php if($info['item']->group_id == $key) : ?> selected="selected"<?php endif; ?>><?php echo $value; ?></option>
								<?php endforeach; ?>
								</select>
							</td>
							<td title="Item Name"><span class="item-name"><?php echo $none_sold.$info['item']->name; ?></span></td>
							<td title="Order Type" style="width: 150px;">
								<select class="update-item"
								        name="ordertype<?php echo $info['item']->id; ?>"
								        data-id="<?php echo $info['item']->id; ?>"
								        data-field-name="ordertype"
								        data-current-value="<?php echo $info['item']->ordertype; ?>">
									<option value="0">Unassigned</option>
								<?php foreach($ordertypes AS $ot_id => $ot_name) : ?>

									<option value="<?php echo $ot_id; ?>"<?php if($info['item']->ordertype == $ot_id) : ?> selected="selected"<?php endif; ?>><?php echo $ot_name; ?></option>
								<?php endforeach; ?>
								</select>
							</td>
							<td title="Inactive" class="center" style="width: 15px;">
								<input class="update-item"
								       type="checkbox"
								       value="1"
								       name="active<?php echo $info['item']->id; ?>"
								       data-id="<?php echo $info['item']->id; ?>"
								       data-field-name="active"
								       data-current-value="<?php echo $info['item']->active; ?>"
								<?php if($info['item']->active == 1) : echo ' checked="checked"'; endif; ?>>
							</td>
							<td title="UPC" class="medium">
								<input type="text" class="medium update-item"
									   name="upc<?php echo $info['item']->id; ?>"
									   value="<?php echo $info['item']->upc; ?>"
									   data-id="<?php echo $info['item']->id; ?>"
									   data-field-name="upc"
										data-current-value="<?php echo $info['item']->upc; ?>">
							</td>
							<td title="Max" class="right small">
								<input type="text" class="small update-item"
									   name="max<?php echo $info['item']->id; ?>"
									   value="<?php echo $info['item']->max; ?>"
									   data-id="<?php echo $info['item']->id; ?>"
									   data-field-name="max"
									   data-current-value="<?php echo $info['item']->max; ?>">
							</td>
							<td title="ReOrder Point" class="right small">
								<input type="text" class="small update-item"
									   name="reorder_point<?php echo $info['item']->id; ?>"
									   value="<?php echo $info['item']->reorder_point; ?>"
									   data-id="<?php echo $info['item']->id; ?>"
									   data-field-name="reorder_point"
									   data-current-value="<?php echo $info['item']->reorder_point; ?>">
							</td>
							<td title="ReOrder Amount" class="right small">
								<input type="text" class="small update-item"
									   name="reorder_amount<?php echo $info['item']->id; ?>"
									   value="<?php echo $info['item']->reorder_amount; ?>"
									   data-id="<?php echo $info['item']->id; ?>"
									   data-field-name="reorder_amount"
									   data-current-value="<?php echo $info['item']->reorder_amount; ?>">
							</td>

							<td title="Taxes" class="right" style="width:90px;">
							<?php foreach($tax_rates as $tax_rate) : ?>

								<label>
									<input class="update-item" type="checkbox" 
										   name="tax<?php echo $info['item']->id; ?>[]"
										   value="<?php echo $tax_rate['tax_id']; ?>"
										   data-id="<?php echo $info['item']->id; ?>"
										   data-field-name="tax_id"
										   data-current-value="<?php echo in_array($tax_rate['tax_id'], $info['taxes']) ? 1 : 0; ?>"
									<?php if(in_array($tax_rate['tax_id'], $info['taxes'])) : ?> checked="checked"<?php endif; ?>>
									<?php echo $tax_rate['tax_name']; ?> (<?php echo $tax_rate['tax_percent']; ?>%)
								</label>
							<?php endforeach; ?>
							</td>

							<td title="Cost" class="right small">
								<input style="text-align:right;" type="text" class="small update-item"
									   name="cost<?php echo $info['item']->id; ?>"
									   value="<?php echo number_format($info['item']->cost,2); ?>"
									   data-id="<?php echo $info['item']->id; ?>"
									   data-field-name="cost"
									   data-current-value="<?php echo $info['item']->cost; ?>">
							</td>
							<td title="Current Price" class="right" style="width: 35px;">
								<b><?php echo $info['item']->price; ?></b>
							</td>
							<td title="New Price" class="right small">
								<input style="text-align:right;" type="text" class="small update-item"
									   name="new_price<?php echo $info['item']->id; ?>"
									   value="<?php if($info['item']->new_price != 0) : echo  number_format($info['item']->new_price, 2); endif; ?>"
									   data-id="<?php echo $info['item']->id; ?>"
									   data-field-name="new_price"
									   data-current-value="<?php echo $info['item']->new_price; ?>">
							</td>
                                                        <td title="EBT FS Allowed" class="center" style="width: 15px;">
								<input class="update-item"
								       type="checkbox"
								       value="1"
								       name="ebt_fs_allowed<?php echo $info['item']->id; ?>"
								       data-id="<?php echo $info['item']->id; ?>"
								       data-field-name="ebt_fs_allowed"
								       data-current-value="<?php echo $info['item']->ebt_fs_allowed; ?>"
								<?php if($info['item']->ebt_fs_allowed == '1') : echo ' checked="checked"'; endif; ?>>
							</td>
							<td class="right" style="width: 35px;">
								&nbsp;
								<img src="/assets/images/ajax-loader-redmond.gif" class="update-img" style="display: none; width: 16px; height: 16px;" />
								<?php if ( defined('IN_PRODUCTION') && ! IN_PRODUCTION ) : ?>
								<span class="update-counter-num" style="display: none; font-size: 12px;">0</span>
								<?php endif; ?>
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
				<?php endforeach; ?>

				<?php
				/* Removed per change over to AJAX for saving.
				 * <div style="text-align: right;"><input type="submit" value="Save"></div>
				 */
				?>
			</form>
		</td>
	</tr>
</table>