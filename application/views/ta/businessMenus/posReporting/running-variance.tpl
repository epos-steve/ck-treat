<?php
	echo "<table class=\"sortable\" style=\"border:1px solid black;font-size:12px;background-color:white;\" cellspacing=0 cellpadding=1 width=98%>";
	echo "<thead><tr bgcolor=#FFFF99><td style=\"border:1px solid black;\" width=8%>Item Code</td><td style=\"border:1px solid black;\" width=20%>Item</td>";

	$download_data .= "Item Code,Item,Variance1,Variance2,Variance3,Variance4,Variance5,Total\r\n";

	for( $counter = 0; $counter <= 4; $counter++ ) {
		$mycount = $counter + 1;
		echo "<td style=\"border:1px solid black;\" width=8%>Variance$mycount</td>";
	}
	echo "<td style=\"border:1px solid black;\" width=8%>Total</td></tr></thead><tbody>";

	foreach( $items AS $key => $value ) {
		////get item_code
		$complete_query = "SELECT item_code FROM menu_items_new WHERE businessid = '$businessid' AND pos_id = '$key'";
		$result = Treat_DB_ProxyOldProcessHost::query( $complete_query );

		$item_code = mysql_result( $result, 0, "item_code" );

		echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor=''><td style=\"border:1px solid black;\">&nbsp;$item_code</td><td style=\"border:1px solid black;\">&nbsp;$value</td>";
		$download_data .= "$item_code,\"$value\",";

		$total = 0;
		for( $counter = 4; $counter >= 0; $counter-- ) {
			////get dates
			$dates = Kiosk::last_inv_dates( $machine_num, $item_code, $counter );
			$date1 = $dates[0];
			$date2 = $dates[1];

			////get onhand qty's
			$onhand1 = Kiosk::inv_onhand( $machine_num, $item_code, $date1 );
			$onhand2 = Kiosk::inv_onhand( $machine_num, $item_code, $date2, 1 );
			$fills = Kiosk::fills( $machine_num, $item_code, $date1, $date2 );

			////waste
			$waste = Kiosk::waste( $machine_num, $item_code, $date1, $date2 );

			////get check detail
			$items_sold = menu_items::items_sold( $key, $businessid, $date1, $date2,
					$current_terminal );

			$diff1 = $onhand1 + $fills - $waste - $onhand2;
			$diff2 = $items_sold - $diff1;

			$total += $diff2;

			if( $diff2 == 0 ) {
				$diff2 = "";
			}
			echo "<td style=\"border:1px solid black;\" width=8%>&nbsp;$diff2</td>";
			$download_data .= "$diff2,";
		}

		if( $total == 0 ) {
			$color = "#00FF00";
		}
		elseif( abs( $total ) > 0 && abs( $total ) <= 3 ) {
			$color = "yellow";
		}
		else {
			$color = "red";
		}
		echo "<td style=\"border:1px solid black;\" width=8% bgcolor=$color>&nbsp;$total</td></tr>";
		$download_data .= "$total\r\n";
	}
	echo "</tbody></table><br>";

?>