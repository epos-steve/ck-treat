<?php
	$date = $t_date1;
	$today = $t_date2;
	$date2 = date( "Y-m-d" );
	///sum of sales for period
	$query = "
		SELECT
			SUM(creditdetail.amount) AS totalsales
		FROM
			creditdetail,
			credits
		WHERE
			credits.businessid = $businessid
			AND credits.credittype = 1
			AND credits.creditid = creditdetail.creditid
			AND creditdetail.date BETWEEN '$date' AND '$today'
	";
	$result = Treat_DB_ProxyOldProcessHost::query( $query );

	$totalsales = mysql_result( $result, 0, "totalsales" );

	echo "<table class=\"sortable\" style=\"border:1px solid black;font-size:12px;background-color:white;\" cellspacing=0 cellpadding=1 width=98%>";
	echo "<thead><tr bgcolor=#FFFF99><td style=\"border:1px solid black;\" width=8%>Item Code</td><td style=\"border:1px solid black;\" width=20%>Item</td><td style=\"border:1px solid black;\">Cost</td><td style=\"border:1px solid black;\">Price</td>";

	$download_data .= "Item Code,Item,Cost,Price,";

	$date1 = $date;
	while( $date1 <= $today ) {
		$day = substr( $date1, 8, 2 );
		$month = substr( $date1, 5, 2 );
		echo "<td style=\"border:1px solid black;\">$month/$day</td>";
		$download_data .= "$month/$day,";
		$date1 = nextday( $date1 );
	}
	echo "<td style=\"border:1px solid black;\"><b>Total</b></td><td style=\"border:1px solid black;\"><b>Waste</b></td><td style=\"border:1px solid black;\"><b>Par</b></td><td style=\"border:1px solid black;\"><b>OnHand</b></td><td style=\"border:1px solid black;\" title='Percent of Sales'><center><b>%</b></center></td></tr></thead><tbody>";

	$download_data .= "Total,Waste,Par,OnHand,%\r\n";

	foreach( $items AS $key => $value ) {
		////get item_code
		$complete_query = "
			SELECT
				item_code,
				id,
				max
			FROM
				menu_items_new
			WHERE
				businessid = '$businessid'
				AND pos_id = '$key'
		";
		$result = Treat_DB_ProxyOldProcessHost::query( $complete_query );

		$item_code = mysql_result( $result, 0, "item_code" );
		$id = mysql_result( $result, 0, "id" );
		$max = mysql_result( $result, 0, "max" );

		$complete_query = "
			SELECT
				price,
				cost
			FROM
				menu_items_price
			WHERE
				menu_item_id = '$id'
		";
		$result = Treat_DB_ProxyOldProcessHost::query( $complete_query );

		$price = mysql_result( $result, 0, "price" );
		$price = number_format( $price, 2 );
		$cost = mysql_result( $result, 0, "cost" );
		$cost = number_format( $cost, 2 );

		echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor=''><td style=\"border:1px solid black;\">&nbsp;$item_code</td><td style=\"border:1px solid black;\">&nbsp;$value</td><td style=\"border:1px solid black;\">&nbsp;$$cost</td><td style=\"border:1px solid black;\">&nbsp;$$price</td>";

		$download_data .= "$item_code,\"$value\",$cost,$price,";

		////get check detail
		$date1 = $date;
		$total = 0;
		while( $date1 <= $today ) {
			$items_sold = menu_items::items_sold(
				$key,
				$businessid,
				$date1,
				$date1,
				$current_terminal,
				$t_datefield
			);
			$total += $items_sold;
			if( $items_sold == "" ) {
				$items_sold = "&nbsp;";
			}
			echo "<td style=\"border:1px solid black;\">&nbsp;$items_sold</td>";
			if($items_sold == 0){$items_sold = "";}
			$download_data .= "$items_sold,";
			$date1 = nextday( $date1 );
		}

		////get dates
		$inv_dates = Kiosk::last_inv_dates( $machine_num, $item_code );
		$inv_date = $inv_dates[1];
		if( $inv_date == "" ) {
			$inv_date = $inv_dates[0];
		}

		////last count
		$onhand2 = Kiosk::inv_onhand( $machine_num, $item_code, $inv_date );

		////fills
		$fills = Kiosk::fills( $machine_num, $item_code, $inv_date, $date2 );

		////waste
		$waste = Kiosk::waste( $machine_num, $item_code, $inv_date, $date2 );

		//// date range waste
		$dated_waste = Kiosk::waste( $machine_num, $item_code, "$date 00:00:00", "$today 23:59:59",
				1 );

		////get check detail
		$items_sold = menu_items::items_sold(
			$key,
			$businessid,
			$inv_date,
			$date2,
			$current_terminal,
			$t_datefield
		);

		$onhand = $onhand2 + $fills - $waste - $items_sold;

		$percent = $totalsales == 0 ? 0 : round( ( ( $total * $price ) / $totalsales ) * 100, 2 );
		echo "<td style=\"border:1px solid black;\" bgcolor=#E8E7E7>&nbsp;$total</td><td style=\"border:1px solid black;\">&nbsp;$dated_waste</td><td style=\"border:1px solid black;\">&nbsp;$max</td><td style=\"border:1px solid black;\">&nbsp;$onhand</td><td style=\"border:1px solid black;\" bgcolor=#E8E7E7><center><font color=blue size=1>&nbsp;$percent%</font></center></td></tr>";
		$download_data .= "$total,$dated_waste,$max,$onhand,$percent%\r\n";
	}
	echo "</tbody></table><br>";

?>