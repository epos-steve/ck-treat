<?php
        $weekend = "Sunday";
        
        $itemColor = array('#7BCAE1',
                        '#01F33E',
                        '#B6BA18',
                        '#FF62B0',
                        '#CBC5F5',
                        '#FF4848',
                        '#B5FFFC',
                        '#DFDF00',
                        '#FF800D',
                        '#D1A0A0',
                        '#C87C5B',
                        '#A8A8FF',
                        '#D1D17A',
                        '#FF2DFF',
                        '#D19C67',
                        '#FFE920',
                        '#80B584',
                        '#24E0FB',
                        '#FE67EB',
                        '#C4ABFE');
        $color_counter = 0;
        
        ///figure dates
        $temp_date = nextday($t_date1);
        $counter = 1;
        $dates[0] = prevday($t_date1);
        while($temp_date <= $t_date2){
            if(dayofweek($temp_date) == $weekend || $temp_date == $t_date2){
                $dates[$counter] = $temp_date;
                $counter++;
            }
            $temp_date = nextday($temp_date);
        }
        
        ///building population
        $query = "SELECT population FROM business WHERE businessid =$businessid";
        $result = Treat_DB_ProxyOldProcessHost::query( $query );

        $population = @mysql_result($result,0,"population");
        
        ///users
        $cust_visits = array();
        $customers = array();
        $singles = array();
        $returners = array();
        
        $temp_counter = 1;
        while($temp_counter <= $counter){
        
            $temp_date1 = $dates[$temp_counter-1];
            $temp_date2 = $dates[$temp_counter];
        
            ///get ck vs cc
            $query = "SELECT SUM( dd1.amount ) AS creditcard, SUM( dd2.amount ) AS ckcard
                        FROM debits d
                        LEFT JOIN debitdetail dd1 ON dd1.debitid = d.debitid
                        AND dd1.date > '$temp_date1'
                        AND dd1.date <= '$temp_date2'
                        AND d.category =1
                        LEFT JOIN debitdetail dd2 ON dd2.debitid = d.debitid
                        AND dd2.date > '$temp_date1'
                        AND dd2.date <= '$temp_date2'
                        AND d.category =2
                        WHERE d.businessid =$businessid";
            $result = Treat_DB_ProxyOldProcessHost::query( $query );

            $creditcard[$temp_counter] = @mysql_result($result,0,"creditcard");
            $ckcard[$temp_counter] = @mysql_result($result,0,"ckcard");
            
            ///get promos running
            $query = "SELECT p.name,p.pos_id
                        FROM promotions p
                        LEFT JOIN promo_mapping pm ON p.id = pm.PromotionId
                        AND pm.BusinessId =$businessid
                        WHERE (p.businessid =$businessid OR p.businessid IS NULL)
                        AND p.activeFlag = 1
                        AND p.subsidy = 0
                        AND (p.start_date <= '$temp_date2' AND p.end_date >= '$temp_date1')
                        AND p.value > 0";
            $result = Treat_DB_ProxyOldProcessHost::query( $query );
            
            while($r = mysql_fetch_array($result)){
                $promo_name = $r["name"];
                $promo_pos_id = $r["pos_id"];
                $promos[$temp_counter]++;
                if($promo_value[$temp_counter] == ""){$promo_value[$temp_counter] = $promo_name;}
                else{$promo_value[$temp_counter] .= " :: $promo_name";}
                
                ///promo quantity and sum
                $query2 = "SELECT COUNT(cd.id) AS promos, SUM(cd.amount) AS promoTotal
                            FROM checkdiscounts cd
                            JOIN checks c ON c.businessid = $businessid
                                AND c.check_number = cd.check_number
                                AND c.bill_datetime BETWEEN '$temp_date1 23:59:59' AND '$temp_date2 23:59:59'
                                WHERE cd.pos_id = '$promo_pos_id'";
                $result2 = Treat_DB_ProxyOldProcessHost::query( $query2 );
                
                $promo_count[$temp_counter] += @mysql_result($result2,0,"promos");
                $promo_sum[$temp_counter] += @mysql_result($result2,0,"promoTotal");
            }
            
            //credit cards
            $query = "SELECT SUM(pd.received) AS purchases, 
                            count(pd.id) AS visits,
                            DATE( c.bill_datetime ) AS p_date
                        FROM payment_detail pd
                        JOIN checks c ON c.businessid = $businessid
                                AND c.check_number = pd.check_number 
                                AND c.bill_datetime BETWEEN '$temp_date1 23:59:59' AND '$temp_date2 23:59:59'
                        WHERE pd.payment_type = 4";
            $result = Treat_DB_ProxyOldProcessHost::query( $query );

            $cc_purch[$temp_counter] = @mysql_result($result,0,"purchases");
            $cc_visits[$temp_counter] = @mysql_result($result,0,"visits");
            
            //ck cards
            $query = "SELECT SUM(pd.received) AS purchases, 
                            pd.scancode, 
                            count(pd.id) AS visits,
                            DATE( c.bill_datetime ) AS p_date
                        FROM payment_detail pd
                        JOIN checks c ON c.businessid = $businessid
                                AND c.check_number = pd.check_number 
                                AND c.bill_datetime BETWEEN '$temp_date1 23:59:59' AND '$temp_date2 23:59:59'
                        WHERE pd.payment_type = 14
                        group by pd.scancode, p_date
                        order by pd.scancode, p_date";
            $result = Treat_DB_ProxyOldProcessHost::query( $query );

            while($r = mysql_fetch_array($result)){
                    $purchases = $r["purchases"];
                    $scancode = $r["scancode"];
                    $visits = $r["visits"];
                    $p_date = $r["p_date"];

                    if($visits > 1){
                        $returners[$temp_counter]++;
                        $returner_purch[$temp_counter] += $purchases;
                        $returner_visit[$temp_counter] += $visits;
                    }
                    else{
                        $singles[$temp_counter]++;
                        $singles_purch[$temp_counter] += $purchases;
                        $singles_visit[$temp_counter] += $visits;
                    }
                    
                    if($lastscancode != $scancode){
                        $unique_visitors[$temp_counter]++;
                    }
                    
                    $ckvisits[$temp_counter] += $visits;
                    
                    $lastscancode = $scancode;
            }
            
            //get sales trend data
            $sixty_days = date("Y-m-d",mktime(0, 0, 0, substr($temp_date2,5,2) , substr($temp_date2,8,2) - 30, substr($temp_date2,0,4)));
            $query = "SELECT SUM(sales) AS sales 
                        FROM dashboard 
                        WHERE businessid = $businessid 
                            AND date BETWEEN '$sixty_days' AND '$temp_date2'";
            $result = Treat_DB_ProxyOldProcessHost::query( $query );
            
            $sales[$temp_counter] = @mysql_result($result,0,"sales");
            
            ///get top 10 sellers trend
            $query = "SELECT SUM( cd.quantity ) AS qty, min.name
                        FROM checkdetail cd
                        JOIN checks c ON c.businessid = $businessid
                            AND c.check_number = cd.bill_number
                            AND c.bill_datetime
                        BETWEEN  '$sixty_days 23:59:59' AND  '$temp_date2 23:59:59'
                        JOIN menu_items_new min ON min.businessid = $businessid
                            AND min.pos_id = cd.item_number
                        WHERE cd.businessid = $businessid
                        GROUP BY cd.item_number
                        ORDER BY qty DESC 
                        LIMIT 20";
            $result = Treat_DB_ProxyOldProcessHost::query( $query );
           
            while($r = mysql_fetch_array($result)){
                $itemTrendQty[$temp_counter][] = $r["qty"];
                $itemTrendName[$temp_counter][] = $r["name"];
                
                if($itemTrendColor[$r["name"]] == ""){
                    if($color_counter > 19){
                        $itemTrendColor[$r["name"]] = "white";
                    }
                    else{
                        $itemTrendColor[$r["name"]] = $itemColor[$color_counter];
                        $color_counter++;
                    }
                }
            }
            
            $temp_counter++;
        }
        
        ///start table
        echo "<table style=\"border:1px solid black;font-size:12px;background-color:white;\" cellspacing=0 cellpadding=1 width=98%>";
	echo "<thead><tr bgcolor=#FFFF99><td style=\"border:1px solid black;\">&nbsp;Category</td>";
        $temp_counter = 1;
        while($temp_counter < $counter){
            echo "<td style=\"border:1px solid black;\" align=right>&nbsp;{$dates[$temp_counter]}</td>";
            $temp_counter++;
        }
        echo "<td style=\"border:1px solid black;\" align=right>&nbsp;Averages</td></tr></thead><tbody>";
        
        ///population
        echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor=''><td bgcolor=#E8E7E7 style=\"border:1px solid black;\">&nbsp;Population</td>";
        $temp_counter = 1;
        while($temp_counter < $counter){
            echo "<td style=\"border:1px solid black;\" align=right>$population</td>";
            $temp_counter++;
        }
        echo "<td style=\"border:1px solid black;\" align=right bgcolor=#E8E7E7><b>$population</b></td></tr>";
        
        ///display cc
        echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor=''><td bgcolor=#E8E7E7 style=\"border:1px solid black;\">&nbsp;Credit Card</td>";
        $temp_counter = 1;
        while($temp_counter < $counter){
            $value = number_format($creditcard[$temp_counter], 2);
            echo "<td style=\"border:1px solid black;\" align=right>$$value</td>";
            $temp_counter++;
        }
        $average = number_format(array_sum($creditcard) / count($creditcard), 2);
        echo "<td style=\"border:1px solid black;\" align=right bgcolor=#E8E7E7><b>$$average</b></td></tr>";
        
        ///display ck
        echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor=''><td bgcolor=#E8E7E7 style=\"border:1px solid black;\">&nbsp;CK Card</td>";
        $temp_counter = 1;
        while($temp_counter < $counter){
            $value = number_format($ckcard[$temp_counter], 2);
            echo "<td style=\"border:1px solid black;\" align=right>$$value</td>";
            $temp_counter++;
        }
        $average = number_format(array_sum($ckcard) / count($ckcard), 2);
        echo "<td style=\"border:1px solid black;\" align=right bgcolor=#E8E7E7><b>$$average</b></td></tr>";
        
        ///display cc %
        echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor=''><td bgcolor=#E8E7E7 style=\"border:1px solid black;\">&nbsp;Credit Card %</td>";
        $temp_counter = 1;
        while($temp_counter < $counter){
			$value = 0;
			if (!(!$ckcard[$temp_counter] || !$creditcard[$temp_counter])){
				$value = round(($creditcard[$temp_counter] / ($ckcard[$temp_counter] + $creditcard[$temp_counter])) * 100, 1);
			}
            echo "<td style=\"border:1px solid black;\" align=right>$value%</td>";
            $temp_counter++;
        }
		$sum_ckcard_creditcard = array_sum($ckcard) + array_sum($creditcard);
		$average = 0;
		if ($sum_ckcard_creditcard){
			$average = round((array_sum($creditcard) / ($sum_ckcard_creditcard)) * 100, 1);
		}
        echo "<td style=\"border:1px solid black;\" align=right bgcolor=#E8E7E7><b>$average%</b></td></tr>";
        
        ///display ck %
        echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor=''><td bgcolor=#E8E7E7 style=\"border:1px solid black;\">&nbsp;CK Card %</td>";
        $temp_counter = 1;
        while($temp_counter < $counter){
			$sum_ckcard_creditcard = $ckcard[$temp_counter] + $creditcard[$temp_counter];
			$value = 0;
			if ($sum_ckcard_creditcard){
				$value = round(($ckcard[$temp_counter] / ($sum_ckcard_creditcard)) * 100, 1);
			}
            echo "<td style=\"border:1px solid black;\" align=right>$value%</td>";
            $temp_counter++;
        }
		$sum_ckcard_creditcard = array_sum($ckcard) + array_sum($creditcard);
		$average = 0; 
		if ($sum_ckcard_creditcard){
			$average = round((array_sum($ckcard) / ($sum_ckcard_creditcard)) * 100, 1);
		}
        echo "<td style=\"border:1px solid black;\" align=right bgcolor=#E8E7E7><b>$average%</b></td></tr>";
        
        //cc visits
        echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor=''><td bgcolor=#E8E7E7 style=\"border:1px solid black;\">&nbsp;Credit Card Transactions</td>";
        $temp_counter = 1;
        while($temp_counter < $counter){
            echo "<td style=\"border:1px solid black;\" align=right>$cc_visits[$temp_counter]</td>";
            $temp_counter++;
        }
        $average = round(array_sum($cc_visits) / ($counter - 1), 1);
        echo "<td style=\"border:1px solid black;\" align=right bgcolor=#E8E7E7><b>$average</b></td></tr>";
        
        //cc average purchase
        echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor=''><td bgcolor=#E8E7E7 style=\"border:1px solid black;\">&nbsp;CC Average Purchase</td>";
        $temp_counter = 1;
        while($temp_counter < $counter){
			$cc_visits_value = $cc_visits[$temp_counter];
			$value = 0;
			if ($cc_visits_value){
				$value = number_format($cc_purch[$temp_counter] / $cc_visits[$temp_counter], 2);
			}
            echo "<td style=\"border:1px solid black;\" align=right>$$value</td>";
            $temp_counter++;
        }
		$cc_visits_value = array_sum($cc_visits);
		$average = 0;
		if ($cc_visits_value){
			$average = number_format(array_sum($cc_purch) / array_sum($cc_visits), 2);
		}
        echo "<td style=\"border:1px solid black;\" align=right bgcolor=#E8E7E7><b>$$average</b></td></tr>";
        
        ///unique visitors
        echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor=''><td bgcolor=#E8E7E7 style=\"border:1px solid black;\">&nbsp;Unique CK Users</td>";
        $temp_counter = 1;
        while($temp_counter < $counter){
            echo "<td style=\"border:1px solid black;\" align=right>$unique_visitors[$temp_counter]</td>";
            $temp_counter++;
        }
		$average = 0;
		if (count($unique_visitors)){
			$average = round(array_sum($unique_visitors) / count($unique_visitors), 1);
		}
        echo "<td style=\"border:1px solid black;\" align=right bgcolor=#E8E7E7><b>$average</b></td></tr>";
        
        ///visits
        echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor=''><td bgcolor=#E8E7E7 style=\"border:1px solid black;\">&nbsp;CK Transactions</td>";
        $temp_counter = 1;
        while($temp_counter < $counter){
            echo "<td style=\"border:1px solid black;\" align=right>$ckvisits[$temp_counter]</td>";
            $temp_counter++;
        }
		$average = 0;
		if (count($ckvisits)){
			$average = round(array_sum($ckvisits) / count($ckvisits), 1);
		}
        echo "<td style=\"border:1px solid black;\" align=right bgcolor=#E8E7E7><b>$average</b></td></tr>";
        
        ///return visits
        echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor=''><td bgcolor=#E8E7E7 style=\"border:1px solid black;\">&nbsp;Multiple Daily Visits</td>";
        $temp_counter = 1;
		$sum_returners_singles = 0;
        while($temp_counter < $counter){
			$sum_returners_singles = $returners[$temp_counter] + $singles[$temp_counter];
			$value = 0;
			if ($sum_returners_singles){
				$value = round($returners[$temp_counter] / ($sum_returners_singles) * 100, 1);
			}
            echo "<td style=\"border:1px solid black;\" align=right>$value%</td>";
            $temp_counter++;
        }
		$average = 0;
		$sum_returners_singles = 0;
		$sum_returners_singles = array_sum($returners) + array_sum($singles);
		if ($sum_returners_singles){
			$average = round(array_sum($returners) / (array_sum($returners) + array_sum($singles)) * 100, 1);
		}
        echo "<td style=\"border:1px solid black;\" align=right bgcolor=#E8E7E7><b>$average%</b></td></tr>";
        
        ///return visits avg purchase
        echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor=''><td bgcolor=#E8E7E7 style=\"border:1px solid black;\">&nbsp;Average Purchase</td>";
        $temp_counter = 1;
		$count_returner_visit = 0;
        while($temp_counter < $counter){
			$count_returner_visit = $returner_visit[$temp_counter];
			$value = 0;
			if ($count_returner_visit){
				$value = number_format($returner_purch[$temp_counter] / $returner_visit[$temp_counter], 2);
			}
            echo "<td style=\"border:1px solid black;\" align=right>$$value</td>";
            $temp_counter++;
        }
		$average = 0;
		if (is_array($returner_visit)){
			$average = number_format(array_sum($returner_purch) / array_sum($returner_visit), 2);
		}
        echo "<td style=\"border:1px solid black;\" align=right bgcolor=#E8E7E7><b>$$average</b></td></tr>";
        
        ///single visits
        echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor=''><td bgcolor=#E8E7E7 style=\"border:1px solid black;\">&nbsp;Single Daily Visits</td>";
        $temp_counter = 1;
        while($temp_counter < $counter){
			$sum_returners_singles = 0;
			$sum_returners_singles = $returners[$temp_counter] + $singles[$temp_counter];
			$value = 0;
			if ($sum_returners_singles){
				$value = round($singles[$temp_counter] / ($sum_returners_singles) * 100, 1);
			}
            echo "<td style=\"border:1px solid black;\" align=right>$value%</td>";
            $temp_counter++;
        }
		$average = 0;
		$sum_returners_singles = 0;
		$sum_returners_singles = array_sum($returners) + array_sum($singles);
		if ($sum_returners_singles){
			$average = round(array_sum($singles) / ($sum_returners_singles) * 100, 1);
		}
        echo "<td style=\"border:1px solid black;\" align=right bgcolor=#E8E7E7><b>$average%</b></td></tr>";
        
        ///single visits avg purchase
        echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor=''><td bgcolor=#E8E7E7 style=\"border:1px solid black;\">&nbsp;Average Purchase</td>";
        $temp_counter = 1;
        while($temp_counter < $counter){
			$value = 0;
			if ($singles_visit[$temp_counter]){
				$value = number_format($singles_purch[$temp_counter] / $singles_visit[$temp_counter], 2);
			}
            echo "<td style=\"border:1px solid black;\" align=right>$$value</td>";
            $temp_counter++;
        }
		$average = 0;
		if (is_array($singles_visit)){
			$average = number_format(array_sum($singles_purch) / array_sum($singles_visit), 2);
		}
        echo "<td style=\"border:1px solid black;\" align=right bgcolor=#E8E7E7><b>$$average</b></td></tr>";
        
        ///display promos
        echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor=''><td bgcolor=#E8E7E7 style=\"border:1px solid black;\">&nbsp;Active Promotions</td>";
        $temp_counter = 1;
        while($temp_counter < $counter){
            echo "<td style=\"border:1px solid black;\" align=right title='{$promo_value[$temp_counter]}'>$promos[$temp_counter]</td>";
            $temp_counter++;
        }
        $average = round(array_sum($promos) / count($promos), 1);
        echo "<td style=\"border:1px solid black;\" align=right bgcolor=#E8E7E7><b>$average</b></td></tr>";
        
        ///display promo count
        echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor=''><td bgcolor=#E8E7E7 style=\"border:1px solid black;\">&nbsp;Promotions Used</td>";
        $temp_counter = 1;
        while($temp_counter < $counter){
            echo "<td style=\"border:1px solid black;\" align=right>$promo_count[$temp_counter]</td>";
            $temp_counter++;
        }
        $average = round(array_sum($promo_count) / count($promo_count), 1);
        echo "<td style=\"border:1px solid black;\" align=right bgcolor=#E8E7E7><b>$average</b></td></tr>";
        
        ///display promo value
        echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor=''><td bgcolor=#E8E7E7 style=\"border:1px solid black;\">&nbsp;Promotion Dollars</td>";
        $temp_counter = 1;
        while($temp_counter < $counter){
            $value = number_format($promo_sum[$temp_counter] * -1, 2);
            echo "<td style=\"border:1px solid black;\" align=right>$$value</td>";
            $temp_counter++;
        }
        $average = number_format(array_sum($promo_sum) / count($promo_sum) * -1, 2);
        echo "<td style=\"border:1px solid black;\" align=right bgcolor=#E8E7E7><b>$$average</b></td></tr>";
        
        ///sales change percent
        echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor=''><td bgcolor=#E8E7E7 style=\"border:1px solid black;\" title='Rolling 30 days'>&nbsp;Sales Trend</td>
                <td style=\"border:1px solid black;\" align=right>&nbsp;-</td>";
        $temp_counter = 2;
        while($temp_counter < $counter){
            $value = round($sales[$temp_counter] / $sales[$temp_counter - 1] * 100, 1);
            
            if($value > 100){
                $value = round($value - 100, 1);
                $value = "+$value";
                $color="green";
            }
            else{
                $value = round(100 - $value, 1);
                $value = "-$value";
                $color="red";
            }
            
            echo "<td style=\"border:1px solid black;\" align=right><font color=$color>$value%</font></td>";
            $temp_counter++;
        }
        
        $value = round($sales[$counter - 1] / $sales[1] * 100, 1);
        if($value > 100){
                $value = round($value - 100, 1);
                $value = "+$value";
                $color="green";
            }
        else{
                $value = round(100 - $value, 1);
                $value = "-$value";
                $color="red";
        }
        echo "<td style=\"border:1px solid black;\" align=right bgcolor=#E8E7E7><b><font color=$color>$value%</font></b></td></tr>";
        
        ///end table
        echo "</tbody><tfoot></tfoot></table><p>";
        
        ////////////////////////////
        ///top 20 seller trend table
        ///start table
        echo "<table style=\"border:1px solid black;font-size:12px;background-color:white;\" cellspacing=0 cellpadding=1 width=98%>";
	echo "<thead><tr bgcolor=#FFFF99>";
        $temp_counter = 1;
        
        while($temp_counter < $counter){
            echo "<td style=\"border:1px solid black;\" align=right colspan=2>&nbsp;{$dates[$temp_counter]}</td>";
            $temp_counter++;
        }
        echo "</tr></thead><tbody>";
             
        for($item_counter=0; $item_counter<20; $item_counter++){
            $temp_counter = 1;
            echo "<tr>";
            while($temp_counter < $counter){
                $item_name = substr($itemTrendName[$temp_counter][$item_counter],0,20);
                echo "<td style=\"border:1px solid black;\" align=right bgcolor=\"{$itemTrendColor[$itemTrendName[$temp_counter][$item_counter]]}\">&nbsp;{$itemTrendQty[$temp_counter][$item_counter]}</td>
                        <td style=\"border:1px solid black;\" align=right bgcolor=\"{$itemTrendColor[$itemTrendName[$temp_counter][$item_counter]]}\" title=\"{$itemTrendName[$temp_counter][$item_counter]}\">&nbsp;$item_name</td>";
                $temp_counter++;
            }
            echo "</tr>";
        }
        echo "</tbody></table>";
?>