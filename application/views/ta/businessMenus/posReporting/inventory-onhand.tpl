<?php
	require('lib/inc/functions-inventory.php');
	
	////main
	
	$inventoryItems = array();
	$inventoryItems = inventory_item_list($businessid);
	
	$lastCount = array();
	
	foreach($inventoryItems AS $id => $values){
		$lastCount[$id] = last_count($id);
	}
	
	$invQtys = array();
	$purQtys = array();
	
	foreach($inventoryItems AS $id => $values){
		$date = $lastCount[$id]['date'];
		$invQtys[$id] = what_has_sold($businessid, $id, $date);
		
		///batch items
		if($values['in_batch'] > 0){
			$temp = array();
			$temp = what_has_sold_batch($businessid, $id, $date);
			
			foreach($temp AS $key => $value){
				$invQtys[$id][$key] += $value;
			}
		}
		
		$purQtys[$id] = purchases($businessid, $id, $date);
	}
	
	///////////////
	/////print/////
	///////////////
	
	$lastGroup = "";
	
	echo "<table class=\"sortable\" style=\"border:1px solid black;font-size:12px;background-color:white;\" cellspacing=0 cellpadding=1 width=98%>";
	
	foreach($inventoryItems AS $id => $values){
		$used = "";
		$showSold = "";
		$onhand = @round($lastCount[$id]['amount'] / $values['pack_qty'], 2) . "&nbsp;&nbsp;{$values['pack_size']}";
		$onhandPieces = "{$lastCount[$id]['amount']}&nbsp;&nbsp;{$values['count_size']}";
		
		foreach($invQtys[$id] AS $key => $sold){
			if($key == $values['rec_sizeid1']){
				$used = @round($sold / $values['rec_num1'], 2) . "&nbsp;&nbsp;{$values['pack_size']}";
				$showSold = "$sold {$values['rec_sizename1']}";
				
				$onhand = @round(($lastCount[$id]['amount'] / $values['pack_qty']) + $purQtys[$id] - ($sold / $values['rec_num1']), 2) . "&nbsp;&nbsp;{$values['pack_size']}";
				$onhandPieces = @round((($lastCount[$id]['amount'] / $values['pack_qty']) + $purQtys[$id] - ($sold / $values['rec_num1'])) * $values['pack_qty'], 2) . "&nbsp;&nbsp;{$values['count_size']}";
			}
			elseif($key == $values['rec_sizeid2']){
				$used = @round($sold / $values['rec_num2'], 2) . "&nbsp;&nbsp;{$values['pack_size']}";
				$showSold = "$sold {$values['rec_sizename2']}";
				
				$onhand = @round(($lastCount[$id]['amount'] / $values['pack_qty']) + $purQtys[$id] - ($sold / $values['rec_num2']), 2) . "&nbsp;&nbsp;{$values['pack_size']}";
				$onhandPieces = @round((($lastCount[$id]['amount'] / $values['pack_qty']) + $purQtys[$id] - ($sold / $values['rec_num2'])) * $values['pack_qty'], 2) . "&nbsp;&nbsp;{$values['count_size']}";
			}
			else{
				$used = "";
				$showSold = "";
				$onhand = @round($lastCount[$id]['amount'] / $values['pack_qty'], 2) . "&nbsp;&nbsp;{$values['pack_size']}";
			}
		}
		
		if($purQtys[$id] != 0){
			$showPurch = "{$purQtys[$id]} {$values['pack_size']}";
		}
		else{
			$showPurch = "";
		}
		
		////group header
		if($lastGroup != $values['acct_name']){
			echo "<tr bgcolor=#FFFF99>
				<td style=\"border:1px solid black;\">{$values['acct_name']}</td>
				<td style=\"border:1px solid black;\">Last Count</td>
				<td style=\"border:1px solid black;\">Last Count Amt</td>
				<td style=\"border:1px solid black;\" colspan=2><center>Amt Used</center></td>	
				<td style=\"border:1px solid black;\">Purchases</td>
				<td style=\"border:1px solid black;\" colspan=2><center>Onhand</center></td>
			</tr>";
		}
	
		echo "<tr>
				<td style=\"border:1px solid black;\">{$values['item_name']}</td>
				<td style=\"border:1px solid black;\">{$lastCount[$id]['date']}</td>
				<td style=\"border:1px solid black;\">{$lastCount[$id]['amount']}&nbsp;&nbsp;{$values['count_size']}</td>
				<td style=\"border:1px solid black;\">$showSold</td>
				<td style=\"border:1px solid black;\">$used</td>
				<td style=\"border:1px solid black;\">$showPurch</td>
				<td style=\"border:1px solid black;\">$onhandPieces</td>
				<td style=\"border:1px solid black;\">$onhand</td>
			</tr>";
			
		$lastGroup = $values['acct_name'];
	}
	echo "</table>";
	
?>