<?php
	$download_disable = "DISABLED";

	$business = Treat_Model_Business_Singleton::getSingleton( );
	$districtid = $business->districtid;
	$company_type = $business->company_type;
	$divisionid = $business->divisionid;

	$dbWho = array(
		"company_type-$company_type",
		"companyid-$companyid",
		"divisionid-$divisionid",
		"districtid-$districtid",
		"businessid-$businessid"
	);
	$dashboardWho = implode( '+', $dbWho );
?>
<script type="text/javascript" language="javascript">
jQuery(function(){
	createFCExport( );

	var posInit = [ getGroupId, createDashboard ];
	dashboardEmbedded = dashboardDefault = true;
	dashboardWho = '<?=$dashboardWho?>';

	_runInit( posInit );
	//getGroupId( 'posReporting' );
	//createDashboard( );
});
</script>
<input type='hidden' id='dashboardGroup' value='posReporting' />
<input type='hidden' id='dashboardOwner' value='' />
<div id="saveChartFrame">&nbsp;</div>
<div id="dashboard_header"></div>
<div id="dashboard_charts_container"><div id="dashboard_charts">
	<div id="dashboard_message"></div>
	<div id="dashboard_left" class="connectedDashboard">&nbsp;</div>
	<div id="dashboard_right" class="connectedDashboard">&nbsp;</div>
	<div id="dashboard_spacer">&nbsp;</div>
</div></div>
