<?php
	$datetime_field = $t_datefield < 2 ? 'bill_datetime' : 'bill_posted';
	$query = "
		SELECT
			SUM(checkdiscounts.amount) AS promo_total,
			COUNT(checkdiscounts.id) AS total_promo,
			COUNT(DISTINCT checkdiscounts.check_number) AS transactions,
			promotions.name
		FROM
			checkdiscounts
		JOIN promotions ON
			promotions.pos_id = checkdiscounts.pos_id
			AND (
				promotions.businessid = $businessid
				OR promotions.businessid IS NULL
			)
		JOIN checks ON
			checks.check_number = checkdiscounts.check_number
			AND checks.businessid = $businessid
			AND checks.{$datetime_field} BETWEEN '$t_date1 00:00:00' AND '$t_date2 23:59:59'
		GROUP BY checkdiscounts.pos_id
	";

	$result = Treat_DB_ProxyOldProcessHost::query( $query );

	echo "<table class=\"sortable\" style=\"border:1px solid black;font-size:12px;background-color:white;\" cellspacing=0 cellpadding=1 width=98%>";
	echo "<thead><tr bgcolor=#FFFF99><td style=\"border:1px solid black;\">&nbsp;Promotions/Deposits</td><td style=\"border:1px solid black;\" align=right>Transactions</td><td style=\"border:1px solid black;\" align=right>Count</td><td style=\"border:1px solid black;\" align=right>Amount</td></tr></thead><tbody>";

	$download_data .= "Promotion/Deposit,Transactions,Count,Total\r\n";

	while($r = mysql_fetch_array($result)){
		$promo_total = $r["promo_total"];
		$total_promo = $r["total_promo"];
		$transactions = $r["transactions"];
		$promo_name = $r["name"];

		$promo_total = number_format($promo_total,2,'.','');
		$sheet_promo_total += $promo_total;
		$sheet_trans_total += $transactions;
		$sheet_total_promo += $total_promo;

		echo "<tr><td style=\"border:1px solid black;\">&nbsp;$promo_name</td><td style=\"border:1px solid black;\" align=right>$transactions&nbsp;</td><td style=\"border:1px solid black;\" align=right>$total_promo&nbsp;</td><td style=\"border:1px solid black;\" align=right>$promo_total&nbsp;</td></tr>";
		$download_data .= "$promo_name,$transactions,$total_promo,$promo_total\r\n";
	}

	////totals
	echo "</tbody><tfoot><tr bgcolor=#FFFF99><td style=\"border:1px solid black;\">&nbsp;</td><td style=\"border:1px solid black;\" align=right>$sheet_trans_total&nbsp;</td><td style=\"border:1px solid black;\" align=right>$sheet_total_promo&nbsp;</td><td style=\"border:1px solid black;\" align=right>$sheet_promo_total&nbsp;</td></tr></tfoot>";

	echo "</table><p>";

?>