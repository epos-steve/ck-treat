<?php
	///date range
		$date_array = array();
		$label_array = array();
		///daily
		if($t_period == 1){
			$temp_date = $t_date1;
			while($temp_date <= nextday($t_date2)){
				$date_array[] = $temp_date;
				$label_array[] = $temp_date;
				$temp_date = nextday($temp_date);	
			}
		}
		///weekly
		elseif($t_period == 2){
			$query2 = "SELECT week_end,week_start FROM company WHERE companyid = $companyid";
			$result2 = Treat_DB_ProxyOldProcessHost::query( $query2 );
			
			$week_end = @mysql_result($result2,0,"week_end");
			$week_start = @mysql_result($result2,0,"week_start");
			
			$temp_date = $t_date1;
			while($temp_date <= nextday($t_date2)){
				if($temp_date == $t_date1 || dayofweek($temp_date) == $week_start || $temp_date == nextday($t_date2)){
					$date_array[] = $temp_date;
				}
				if(dayofweek($temp_date) == $week_end || $temp_date == $t_date2){$label_array[] = $temp_date;}
				$temp_date = nextday($temp_date);
			}
		}
		///monthly
		elseif($t_period == 3){
			$temp_date = $t_date1;
			while($temp_date <= nextday($t_date2)){
				if($temp_date == $t_date1 || substr($temp_date,8,2) == "01" || $temp_date == nextday($t_date2)){
					$date_array[] = $temp_date;
				}
				if(substr(nextday($temp_date),8,2) == "01" || $temp_date == $t_date2){$label_array[] = $temp_date;}
				$temp_date = nextday($temp_date);
			}
		}
		///by date range
		else{
			$date_array[] = $t_date1;
			$date_array[] = nextday($t_date2);
		}
		
	$date_limit = count($date_array) - 2;
	
	echo "<table style=\"border:1px solid black;font-size:12px;background-color:white;\" cellspacing=0 cellpadding=1 width=98%>";

	////credits
	$query = "SELECT credits.creditname,
				credits.account,
				credits.creditid,
				SUM(creditdetail.amount) AS amount,
				credittype.credittypename
				FROM credits
				JOIN creditdetail ON creditdetail.creditid = credits.creditid AND creditdetail.date BETWEEN '$t_date1' AND '$t_date2'
				JOIN credittype ON credittype.credittypeid = credits.credittype
				WHERE credits.businessid = $businessid AND is_deleted = 0
				GROUP BY credits.creditid
				ORDER BY credits.credittype,credits.creditname";
	$result = Treat_DB_ProxyOldProcessHost::query( $query );

	echo "<tr bgcolor=#FFFF99><td style=\"border:1px solid black;\">&nbsp;Account</td>
				<td style=\"border:1px solid black;\">&nbsp;Account Name</td>";
		for($temp = 0; $temp <= $date_limit; $temp++ ){
			echo "<td style=\"border:1px solid black;text-align:right;\">$label_array[$temp]&nbsp;</td>";
		}
	echo "<td style=\"border:1px solid black;text-align:right;\">Total&nbsp;</td>
			</tr>";

	$download_data .= "Account,Account Name";
		for($temp = 0; $temp <= $date_limit; $temp++ ){
			$download_data .= ",$label_array[$temp]";
		}
	$download_data .= ",Total\r\n";

	while($r = mysql_fetch_array($result)){
		$creditname = $r["creditname"];
		$account = $r["account"];
		$creditid = $r["creditid"];
		$amount = $r["amount"];
		$credittypename = $r["credittypename"];

		///totals
		if($lastcredittype != $credittypename && $lastcredittype != ""){
			$credit_total += $type_total;
			$type_total = number_format($type_total,2);

			echo "<tr bgcolor=#E8E7E7 style=\"font-weight:bold;\"><td style=\"border:1px solid black;\">&nbsp;</td>
				<td style=\"border:1px solid black;\">&nbsp;$lastcredittype</td>";
			$download_data .= ",$lastcredittype";
				for($temp = 0; $temp <= $date_limit; $temp++ ){
					$detail_total_amount = number_format($detail_totals[$temp],2);
					echo "<td style=\"border:1px solid black;text-align:right;\">&nbsp;$$detail_total_amount</td>";
					$download_data .= ",\"$detail_total_amount\"";
				}
			echo "<td style=\"border:1px solid black;text-align:right;font-weight:bold;\">$$type_total&nbsp;</td>
			</tr>";
			$download_data .= ",\"$type_total\"" . "\r\n";
			
			$detail_totals = array();
			$type_total = 0;
		}

		$type_total += $amount;
		$amount = number_format($amount,2);

		echo "<tr><td style=\"border:1px solid black;\">&nbsp;$account</td>
				<td style=\"border:1px solid black;\">&nbsp;$creditname</td>";
		$download_data .= "$account,$creditname";
			///date range
			for($temp = 0; $temp <= $date_limit; $temp++ ){
				$temp2 = $temp + 1;
				$query2 = "SELECT SUM(creditdetail.amount) AS detail FROM creditdetail WHERE creditid = $creditid AND date >= '$date_array[$temp]' AND date < '$date_array[$temp2]'";
				$result2 = Treat_DB_ProxyOldProcessHost::query($query2);
				
				$detail = mysql_result($result2,0,"detail");
				$detail_totals[$temp] += $detail;
				$type_totals[$temp] += $detail;
				$detail = number_format($detail,2);
				
				echo "<td style=\"border:1px solid black;text-align:right;\">$$detail&nbsp;</td>";
				$download_data .= ",\"$detail\"";
			}
		echo "<td style=\"border:1px solid black;text-align:right;\">$$amount&nbsp;</td>
			</tr>";
		$download_data .= ",\"$amount\"" . "\r\n";

		//$download_data .= "$account,\"$creditname\",\"$amount\"\r\n";

		$lastcredittype = $credittypename;
	}

	///last type total
			$credit_total += $type_total;
			$type_total = number_format($type_total,2);

			echo "<tr bgcolor=#E8E7E7 style=\"font-weight:bold;\"><td style=\"border:1px solid black;\">&nbsp;</td>
				<td style=\"border:1px solid black;\">&nbsp;$lastcredittype</td>";
			$download_data .= ",$lastcredittype";
				for($temp = 0; $temp <= $date_limit; $temp++ ){
					$detail_total_tax = number_format($detail_totals[$temp], 2);
					echo "<td style=\"border:1px solid black;text-align:right;\">$$detail_total_tax&nbsp;</td>";
					$download_data .= ",\"$detail_total_tax\"";
				}
			echo "<td style=\"border:1px solid black;text-align:right;\">$$type_total&nbsp;</td>
			</tr>";
			$download_data .= ",\"$type_total\"" . "\r\n";

			$type_total = 0;
	///credit total;
			$credit_total = number_format($credit_total,2);

			echo "<tr bgcolor=#CCCCCC style=\"font-weight:bold;\"><td style=\"border:1px solid black;\">&nbsp;</td>
				<td style=\"border:1px solid black;\">&nbsp;Total</td>";
			$download_data .= ",Total";
				for($temp = 0; $temp <= $date_limit; $temp++ ){
					$type_totals_formatted = number_format($type_totals[$temp], 2);
					echo "<td style=\"border:1px solid black;text-align:right;\">$$type_totals_formatted&nbsp;</td>";
					$download_data .= ",\"$type_totals_formatted\"";
				}				
			echo "<td style=\"border:1px solid black;text-align:right;\">$$credit_total&nbsp;</td>
			</tr>";
			$download_data .= ",\"$credit_total\"" . "\r\n";

	///debits
	$type_totals = array();
			
	$query = "SELECT debits.debitname,
				debits.account,
				debits.debitid,
				SUM(debitdetail.amount) AS amount,
				debittype.debittypename
				FROM debits
				JOIN debitdetail ON debitdetail.debitid = debits.debitid AND debitdetail.date BETWEEN '$t_date1' AND '$t_date2'
				JOIN debittype ON debittype.debittypeid = debits.debittype
				WHERE debits.businessid = $businessid AND is_deleted = 0
				GROUP BY debits.debitid
				ORDER BY debits.debittype,debits.debitname";
	$result = Treat_DB_ProxyOldProcessHost::query( $query );

	echo "<tr bgcolor=#FFFF99><td style=\"border:1px solid black;\">&nbsp;Account</td>
				<td style=\"border:1px solid black;\">&nbsp;Account Name</td>";
		for($temp = 0; $temp <= $date_limit; $temp++ ){
			echo "<td style=\"border:1px solid black;text-align:right;\">$label_array[$temp]&nbsp;</td>";
		}
	echo "<td style=\"border:1px solid black;text-align:right;\">Total&nbsp;</td>
			</tr>";

	$download_data .= "Account,Account Name";
		for($temp = 0; $temp <= $date_limit; $temp++ ){
			$download_data .= ",$label_array[$temp]";
		}
	$download_data .= ",Total\r\n";

	$type_totals = array();
	$detail_totals = array();

	while($r = mysql_fetch_array($result)){
		$debitname = $r["debitname"];
		$account = $r["account"];
		$debitid = $r["debitid"];
		$amount = $r["amount"];
		$debittypename = $r["debittypename"];

		///totals
		if($lastdebittype != $debittypename && $lastdebittype != ""){
			$debit_total += $type_total;
			$type_total = number_format($type_total,2);

			echo "<tr bgcolor=#E8E7E7 style=\"font-weight:bold;\"><td style=\"border:1px solid black;\">&nbsp;</td>
				<td style=\"border:1px solid black;\">&nbsp;$lastdebittype</td>";
			$download_data .= ",\"$lastdebittype\"";
				for($temp = 0; $temp <= $date_limit; $temp++ ){
					$detail_total_debit = number_format($detail_totals[$temp], 2);
					echo "<td style=\"border:1px solid black;text-align:right;\">$$detail_total_debit&nbsp;</td>";
					$download_data .= ",\"$detail_total_debit\"";
				}
			echo "<td style=\"border:1px solid black;text-align:right;\">$$type_total&nbsp;</td>
			</tr>";
			$download_data .= ",\"$type_total\"" . "\r\n";

			$detail_totals = array();
			$type_total = 0;
		}

		$type_total += $amount;
		$amount = number_format($amount,2);

		echo "<tr><td style=\"border:1px solid black;\">&nbsp;$account</td>
				<td style=\"border:1px solid black;\">&nbsp;$debitname</td>";
		$download_data .= "\"$account\",\"$debitname\"";
			///date range
			for($temp = 0; $temp <= $date_limit; $temp++ ){
				$temp2 = $temp + 1;
				$query2 = "SELECT SUM(debitdetail.amount) AS detail FROM debitdetail WHERE debitid = $debitid AND date >= '$date_array[$temp]' AND date < '$date_array[$temp2]'";
				$result2 = Treat_DB_ProxyOldProcessHost::query($query2);
				
				$detail = mysql_result($result2,0,"detail");
				$detail_totals[$temp] += $detail;
				$type_totals[$temp] += $detail;
				$detail = number_format($detail,2);
				
				echo "<td style=\"border:1px solid black;text-align:right;\">$$detail&nbsp;</td>";
				$download_data .= ",\"$detail\"";
			}
		echo "<td style=\"border:1px solid black;text-align:right;font-weight:bold;\">$$amount&nbsp;</td>
			</tr>";
		$download_data .= ",\"$amount\"" . "\r\n";

		//$download_data .= "$account,\"$debitname\",\"$amount\"\r\n";

		$lastdebittype = $debittypename;
	}

	///last type total
			$debit_total += $type_total;
			$type_total = number_format($type_total,2);

			echo "<tr bgcolor=#E8E7E7 style=\"font-weight:bold;\"><td style=\"border:1px solid black;\">&nbsp;</td>
				<td style=\"border:1px solid black;\">&nbsp;$lastdebittype</td>";
			$download_data .= ",\"$lastdebittype\"";
				for($temp = 0; $temp <= $date_limit; $temp++ ){
					$detail_total_debit = number_format($detail_totals[$temp], 2);
					echo "<td style=\"border:1px solid black;text-align:right;\">$$detail_total_debit&nbsp;</td>";
					$download_data .= ",\"$detail_total_debit\"";
				}
			echo "<td style=\"border:1px solid black;text-align:right;\">$$type_total&nbsp;</td>
			</tr>";
			$download_data .= ",\"$type_total\"" . "\r\n";

			$type_total = 0;
	///debit total;
			$debit_total = number_format($debit_total,2);

			echo "<tr bgcolor=#CCCCCC style=\"font-weight:bold;\"><td style=\"border:1px solid black;\">&nbsp;</td>
				<td style=\"border:1px solid black;\">&nbsp;Total</td>";
			$download_data .= ",\"Total\"";
				for($temp = 0; $temp <= $date_limit; $temp++ ){
					$type_totals_formatted = number_format($type_totals[$temp], 2);
					echo "<td style=\"border:1px solid black;text-align:right;\">$$type_totals_formatted&nbsp;</td>";
					$download_data .= ",\"$type_totals_formatted\"";
				}
			echo "<td style=\"border:1px solid black;text-align:right;\">$$debit_total&nbsp;</td>
			</tr>";
			$download_data .= ",\"$debit_total\"" . "\r\n";

	////COGS
	echo "<tr bgcolor=#FFFF99><td style=\"border:1px solid black;\">&nbsp;Inventory</td>
				<td style=\"border:1px solid black;\">&nbsp;Account Name</td>";
		for($temp = 0; $temp <= $date_limit; $temp++ ){
			echo "<td style=\"border:1px solid black;text-align:right;\">$label_array[$temp]&nbsp;</td>";
		}
	echo "<td style=\"border:1px solid black;text-align:right;\">Total&nbsp;</td>
			</tr>";
	
	$download_data .= "Inventory,Account Name";
		for($temp = 0; $temp <= $date_limit; $temp++ ){
			$download_data .= ",$label_array[$temp]";
		}
	$download_data .= ",Total\r\n";

	$query = "SELECT SUM(dashboard.cos) AS cos, SUM(dashboard.waste) AS waste, SUM(dashboard.shrink) AS shrink FROM dashboard WHERE businessid = $businessid AND date BETWEEN '$t_date1' AND '$t_date2'";
	$result = Treat_DB_ProxyOldProcessHost::query( $query );

	$cos = mysql_result($result,0,"cos");
	$waste = mysql_result($result,0,"waste");
	$shrink = mysql_result($result,0,"shrink");

	$cos = number_format($cos,2);
	$waste = number_format($waste,2);
	$shrink = number_format($shrink,2);

	echo "<tr bgcolor=#E8E7E7 style=\"\"><td style=\"border:1px solid black;\">&nbsp;</td>
				<td style=\"border:1px solid black;\">&nbsp;COGS</td>";
	$download_data .= ",\"COGS\"";
			///date range
			for($temp = 0; $temp <= $date_limit; $temp++ ){
				$temp2 = $temp + 1;
				$query2 = "SELECT SUM(dashboard.cos) AS detail FROM dashboard WHERE businessid = $businessid AND date >= '$date_array[$temp]' AND date < '$date_array[$temp2]'";
				$result2 = Treat_DB_ProxyOldProcessHost::query( $query2 );
				
				$detail = @mysql_result($result2,0,"detail");
				$detail = number_format($detail,2);
				
				echo "<td style=\"border:1px solid black;text-align:right;\">$$detail&nbsp;</td>";
				$download_data .= ",\"$detail\"";
			}
			$download_data .= ",\"$cos\"" . "\r\n";
	echo "<td style=\"border:1px solid black;text-align:right;font-weight:bold;\">$$cos&nbsp;</td>
			</tr>
			<tr bgcolor=#E8E7E7 style=\"\"><td style=\"border:1px solid black;\">&nbsp;</td>
				<td style=\"border:1px solid black;\">&nbsp;Waste</td>";
	$download_data .= ",\"Waste\"";
			///date range
			for($temp = 0; $temp <= $date_limit; $temp++ ){
				$temp2 = $temp + 1;
				$query2 = "SELECT SUM(dashboard.waste) AS detail FROM dashboard WHERE businessid = $businessid AND date >= '$date_array[$temp]' AND date < '$date_array[$temp2]'";
				$result2 = Treat_DB_ProxyOldProcessHost::query( $query2 );
				
				$detail = @mysql_result($result2,0,"detail");
				$detail = number_format($detail,2);
				
				echo "<td style=\"border:1px solid black;text-align:right;\">$$detail&nbsp;</td>";
				$download_data .= ",\"$detail\"";
			}
			$download_data .= ",\"$waste\"" . "\r\n";
	echo "<td style=\"border:1px solid black;text-align:right;font-weight:bold;\">$$waste&nbsp;</td>
			</tr>
			<tr bgcolor=#E8E7E7 style=\"\"><td style=\"border:1px solid black;\">&nbsp;</td>
				<td style=\"border:1px solid black;\">&nbsp;Shrink</td>";
	$download_data .= ",\"Shrink\"";
			///date range
			for($temp = 0; $temp <= $date_limit; $temp++ ){
				$temp2 = $temp + 1;
				$query2 = "SELECT SUM(dashboard.shrink) AS detail FROM dashboard WHERE businessid = $businessid AND date >= '$date_array[$temp]' AND date < '$date_array[$temp2]'";
				$result2 = Treat_DB_ProxyOldProcessHost::query( $query2 );
				
				$detail = @mysql_result($result2,0,"detail");
				$detail = number_format($detail,2);
				
				echo "<td style=\"border:1px solid black;text-align:right;\">$$detail&nbsp;</td>";
				$download_data .= ",\"$detail\"";
			}
			$download_data .= ",\"$shrink\"" . "\r\n";
	echo "<td style=\"border:1px solid black;text-align:right;font-weight:bold;\">$$shrink&nbsp;</td>
			</tr>";

	//$download_data .= ",\"COGS\",\"$cos\"\r\n";
	//$download_data .= ",\"Waste\",\"$waste\"\r\n";
	//$download_data .= ",\"Shrink\",\"$shrink\"\r\n";

	echo "</table><p>";
?>