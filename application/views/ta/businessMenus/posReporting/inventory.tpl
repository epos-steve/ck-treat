<?php
	function inventory_item_list($businessid){
		$items = array();

		$query = "SELECT inv.inv_itemid,
					inv.item_name,
					inv.price,
					inv.pack_size,
					inv.pack_qty,
					inv.units,
					ap.acct_name,
					ap.acct_payableid,
					v.vendor_name,
					inv.rec_size,
					inv.rec_num,
					size1.sizename,
					inv.rec_size2,
					inv.rec_num2,
					size2.sizename AS sizename2,
					inv.batch,
					COUNT(r.recipeid) AS in_batch
				FROM inv_items inv 
				JOIN acct_payable ap ON ap.acct_payableid = inv.apaccountid
				JOIN vendor_master v ON v.vendor_master_id = inv.vendor
				LEFT JOIN size size1 ON size1.sizeid = inv.rec_size
				LEFT JOIN size size2 ON size2.sizeid = inv.rec_size2
				LEFT JOIN recipe r ON r.inv_itemid = inv.inv_itemid AND r.menu_itemid < 0
				WHERE inv.businessid = $businessid AND active = 0
				GROUP BY inv.inv_itemid";
		$result = Treat_DB_ProxyOldProcessHost::query( $query );
		
		while($r = mysql_fetch_array($result)){
			$id = $r["inv_itemid"];
			$items[$id]['item_name'] = $r["item_name"];
			$items[$id]['item_cost'] = $r["price"];
			$items[$id]['pack_size'] = $r["pack_size"];
			$items[$id]['pack_qty'] = $r["pack_qty"];
			$items[$id]['count_size'] = $r["units"];
			$items[$id]['acct_name'] = $r["acct_name"];
			$items[$id]['apid'] = $r["acct_payableid"];
			$items[$id]['vendor'] = $r["vendor_name"];
			$items[$id]['batch'] = $r["batch"];
			$items[$id]['in_batch'] = $r["in_batch"];
			
			$items[$id]['rec_sizeid1'] = $r["rec_size"];
			$items[$id]['rec_num1'] = $r["rec_num"];
			$items[$id]['rec_sizename1'] = $r["sizename"];
			
			$items[$id]['rec_sizeid2'] = $r["rec_size2"];
			$items[$id]['rec_num2'] = $r["rec_num2"];
			$items[$id]['rec_sizename2'] = $r["sizename2"];
		}
		
		return $items;
	}
	
	function last_count($item_id, $date = null){
		if($date == null){
			$date = date("Y-m-d H:i:s");
		}
	
		$lastCount = array();
	
		$query = "SELECT `date`,SUM(amount) AS amount,price FROM inv_count
			WHERE inv_itemid = $item_id 
				AND `date` = (SELECT MAX(`date`) FROM inv_count WHERE inv_itemid = $item_id AND date < '$date')";
		$result = Treat_DB_ProxyOldProcessHost::query( $query );
		
		$r = @mysql_fetch_array($result);
		$lastCount['date'] = $r["date"];
		$lastCount['amount'] = $r["amount"];
		$lastCount['price'] = $r["price"];
		
		return $lastCount;
	}
	
	function what_has_sold_batch($businessid, $item_id, $date, $date2 = null){
		if($date2 == null){
			$date2 = date("Y-m-d H:i:s");
		}
	
		$qtyList = array();
	
		$query = "SELECT min.id, 
					min.name, 
					SUM( cd.quantity ) AS qty, 
					ii2.rec_num, 
					r.rec_num AS rec_num1, 
					r.rec_size AS rec_size1, 
					s1.sizename AS size1, 
					r2.rec_num AS rec_num2, 
					r2.rec_size AS rec_size2, 
					s2.sizename AS size2
				FROM menu_items_new min
				JOIN inv_items ii ON ii.inv_itemid = $item_id
				JOIN recipe r ON r.inv_itemid = ii.inv_itemid AND r.menu_itemid < 0
				JOIN inv_items ii2 ON ii2.inv_itemid = r.menu_itemid * -1
				JOIN recipe r2 ON r2.inv_itemid = ii2.inv_itemid AND r2.menu_item_new_id = min.id
				JOIN checks c ON c.businessid = $businessid
					AND c.bill_datetime BETWEEN '$date' AND '$date2'
				JOIN checkdetail cd ON cd.businessid = c.businessid
					AND cd.bill_number = c.check_number
				JOIN size s1 ON s1.sizeid = r.rec_size
				JOIN size s2 ON s2.sizeid = r2.rec_size
				WHERE min.pos_id = cd.item_number
				GROUP BY min.id
			";
		$result = Treat_DB_ProxyOldProcessHost::query( $query );
	
		while($r = @mysql_fetch_array($result)){
			if($r["qty"] != 0){			
				$qtyList[$r["rec_size1"]] += $r["qty"] * ($r["rec_num1"] / $r["rec_num"] * $r["rec_num2"]);
			}
		}

		return $qtyList;
	}
	
	function what_has_sold($businessid, $item_id, $date, $date2 = null){
		if($date2 == null){
			$date2 = date("Y-m-d H:i:s");
		}
	
		$qtyList = array();
	
		$query = "SELECT min.id, 
						min.name, 
						SUM(cd.quantity) AS qty, 
						r.rec_num,
						r.rec_size,
						size.sizename
					FROM menu_items_new min
					JOIN recipe r ON r.menu_item_new_id = min.id AND r.inv_itemid = $item_id
					JOIN checks c ON c.businessid = $businessid AND c.bill_datetime BETWEEN '$date' AND '$date2'
					JOIN checkdetail cd ON cd.businessid = c.businessid AND cd.bill_number = c.check_number
					JOIN size ON size.sizeid = r.rec_size
					WHERE min.pos_id = cd.item_number
					GROUP BY min.id";
		$result = Treat_DB_ProxyOldProcessHost::query( $query );
	
		while($r = @mysql_fetch_array($result)){
			if($r["qty"] != 0){			
				$qtyList[$r["rec_size"]] += $r["qty"] * $r["rec_num"];
			}
		}

		return $qtyList;
	}
	
	function purchases($businessid, $item_id, $date, $date2 = null){
		if($date2 == null){
			$date2 = date("Y-m-d");
		}
	
		$query = "SELECT SUM(apd.qty) AS purchase FROM apinvoicedetail apd
			JOIN apinvoice ap ON ap.apinvoiceid = apd.apinvoiceid 
				AND ap.businessid = $businessid
				AND ap.date BETWEEN '$date' AND '$date2'
			WHERE apd.inv_item_id = $item_id";
		$result = Treat_DB_ProxyOld::query( $query );
	
		$r = @mysql_fetch_array($result);
		$purchases = $r["purchase"];
		
		return $purchases;
	}
	
	////main
	
	$inventoryItems = array();
	$inventoryItems = inventory_item_list($businessid);
	
	$lastCount = array();
	
	foreach($inventoryItems AS $id => $values){
		$lastCount[$id] = last_count($id);
	}
	
	$invQtys = array();
	$purQtys = array();
	
	foreach($inventoryItems AS $id => $values){
		$date = $lastCount[$id]['date'];
		$invQtys[$id] = what_has_sold($businessid, $id, $date);
		
		///batch items
		if($values['in_batch'] > 0){
			$temp = array();
			$temp = what_has_sold_batch($businessid, $id, $date);
			
			foreach($temp AS $key => $value){
				$invQtys[$id][$key] += $value;
			}
		}
		
		$purQtys[$id] = purchases($businessid, $id, $date);
	}
	
	///////////////
	/////print/////
	///////////////
	
	echo "<table class=\"sortable\" style=\"border:1px solid black;font-size:12px;background-color:white;\" cellspacing=0 cellpadding=1 width=98%>";
	
	echo "<tr bgcolor=#FFFF99>
				<td style=\"border:1px solid black;\">Item</td>
				<td style=\"border:1px solid black;\">Last Count</td>
				<td style=\"border:1px solid black;\">Last Count Amt</td>
				<td style=\"border:1px solid black;\">Amt Used</td>	
				<td style=\"border:1px solid black;\">Case</td>
				<td style=\"border:1px solid black;\">Purchases</td>
				<td style=\"border:1px solid black;\">Onhand</td>
			</tr>";
	
	foreach($inventoryItems AS $id => $values){
		$used = "";
		$showSold = "";
		$onhand = "";
		
		foreach($invQtys[$id] AS $key => $sold){
			if($key == $values['rec_sizeid1']){
				$used = @round($sold / $values['rec_num1'], 2) . "&nbsp;&nbsp;{$values['pack_size']}";
				$showSold = "$sold {$values['rec_sizename1']}";
				
				$onhand = @round(($lastCount[$id]['amount'] / $values['pack_qty']) + $purQtys[$id] - ($sold / $values['rec_num1']), 2) . "&nbsp;&nbsp;{$values['pack_size']}";
			}
			elseif($key == $values['rec_sizeid2']){
				$used = @round($sold / $values['rec_num2'], 2) . "&nbsp;&nbsp;{$values['pack_size']}";
				$showSold = "$sold {$values['rec_sizename2']}";
				
				$onhand = @round(($lastCount[$id]['amount'] / $values['pack_qty']) + $purQtys[$id] - ($sold / $values['rec_num2']), 2) . "&nbsp;&nbsp;{$values['pack_size']}";
			}
			else{
				$used = "";
				$showSold = "";
				$onhand = "";
			}
		}
		
		if($purQtys[$id] != 0){
			$showPurch = "{$purQtys[$id]} {$values['pack_size']}";
		}
		else{
			$showPurch = "";
		}
	
		echo "<tr>
				<td style=\"border:1px solid black;\">{$values['item_name']}</td>
				<td style=\"border:1px solid black;\">{$lastCount[$id]['date']}</td>
				<td style=\"border:1px solid black;\">{$lastCount[$id]['amount']}&nbsp;&nbsp;{$values['count_size']}</td>
				<td style=\"border:1px solid black;\">$showSold</td>
				<td style=\"border:1px solid black;\">$used</td>
				<td style=\"border:1px solid black;\">$showPurch</td>
				<td style=\"border:1px solid black;\">$onhand</td>
			</tr>";
				
	}
	echo "</table>";
	
?>