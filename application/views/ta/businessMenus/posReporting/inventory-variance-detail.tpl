<?php
	require('lib/inc/functions-inventory.php');

	$link_date = @$_GET["link_date"];
	$link_date = str_replace('|',' ',$link_date);
	
	$inventoryItems = array();
	$inventoryItems = inventory_item_list($businessid, $link_date);
	
	$count1 = array();
	$count2 = array();
	foreach($inventoryItems AS $id => $values){
		$count1[$id] = last_count($id, $link_date);
		$count2[$id] = last_count($id, $link_date, 1);
	}
	
	$invQtys = array();
	$purQtys = array();
	$salesAmt = array();
	
	foreach($inventoryItems AS $id => $values){
		$date1 = $count1[$id]['date'];
		$date2 = $count2[$id]['date'];
		$invQtys[$id] = what_has_sold($businessid, $id, $date1, $date2);
		
		///batch items
		if($values['in_batch'] > 0){
			$temp = array();
			$temp = what_has_sold_batch($businessid, $id, $date1, $date2);
			
			foreach($temp AS $key => $value){
				$invQtys[$id][$key] += $value;
			}
		}
		
		$purQtys[$id] = purchases($businessid, $id, $date1, $date2);
		
		$salesAmt[$id] = sales($businessid, $date1, $date2);
	}
	
	/////////
	//print//
	/////////
	
	echo "<table style=\"border:1px solid black;font-size:12px;background-color:white;\" cellspacing=0 cellpadding=1 width=98%>";
		
	$lastGroup = "";
	
	$groupfc = 0;
	$groupfc2 = 0;
	$groupVar = 0;
	$groupVarDollars = 0;
	
	$varTotal = 0;
	$totalfc = 0;
	$totalfc2 = 0;
	$totalVar = 0;
	
	$showWaste = "";
			
	foreach($inventoryItems AS $id => $values){
		$used = "";
		$actual = "";
		$showUsed = "";
		$showSold = "";
		
		foreach($invQtys[$id] AS $key => $sold){
			if($key == $values['rec_sizeid1']){
				$used = @round($sold / $values['rec_num1'] * $values['pack_qty'], 2);
				$showUsed = @round($sold / $values['rec_num1'], 2) . "&nbsp;&nbsp;{$values['pack_size']}";
				$showSold = "$sold {$values['rec_sizename1']}";
			}
			elseif($key == $values['rec_sizeid2']){
				$used = @round($sold / $values['rec_num2'] * $values['pack_qty'], 2);
				$showUsed = @round($sold / $values['rec_num2'], 2) . "&nbsp;&nbsp;{$values['pack_size']}";
				$showSold = "$sold {$values['rec_sizename2']}";
			}
			else{
				$used = "";
				$showUsed = "";
				$showSold = "";
			}
		}
		
		if($purQtys[$id] != 0){
			$purch = @round($purQtys[$id] * $values['pack_qty'], 2);
			$showPurch = "{$purQtys[$id]} {$values['pack_size']}";
		}
		else{
			$purch = "";
			$showPurch = "";
		}
		
		///variance
		$variance = $count1[$id]['amount'] - $used + $purch;
		$ideal = $variance . "&nbsp;&nbsp;{$values['count_size']}";
		if($variance != $count2[$id]['amount']){
			$variance = @round($count2[$id]['amount'] - $variance, 2) . "&nbsp;&nbsp;{$values['count_size']}";
			$varDollars = '$' . @number_format(($count2[$id]['amount'] - $variance) * $count2[$id]['price'], 2);
			$groupVarDollars += round(($count2[$id]['amount'] - $variance) * $count2[$id]['price'], 2);
			$varTotal += round(($count2[$id]['amount'] - $variance) * $count2[$id]['price'], 2);
		}
		else{
			$variance = "";
			$varDollars = "";
		}
		
		////actual food cost
		$actual = $count1[$id]['amount'] + $purch - $count2[$id]['amount'];
		$fc2 = round($actual * $count2[$id]['price'] / $salesAmt[$id] * 100, 2);
		
		////theortical food cost
		$fc = round($used * $count2[$id]['price'] / $salesAmt[$id] * 100, 2);
		
		///variance %
		$varPercent = $fc - $fc2;
		
		if($varPercent >= 2.5 || $varPercent <= -1.7){
			$color = "red";
		}
		else{
			$color = "black";
		}
		
		$groupfc += $fc;
		$groupfc2 += $fc2;
		$groupVar += $varPercent;
		
		$totalfc += $fc;
		$totalfc2 += $fc2;
		$totalVar += $varPercent;
		
		///group footer
		if($lastGroup != $values['acct_name'] && $lastGroup != ''){
			echo "<tr bgcolor=#CCCCCC><td style=\"border:1px solid black;\" colspan=7><b>$lastGroup Totals</b></td>
				<td style=\"border:1px solid black;\">&nbsp;</td>
				<td style=\"border:1px solid black;\" align=right><b>$$groupVarDollars</b></td>
				<td style=\"border:1px solid black;\" align=right><b>$groupfc%</b></td>
				<td style=\"border:1px solid black;\" align=right><b>$groupfc2%</b></td>
				<td style=\"border:1px solid black;\" align=right><b>$groupVar%</b></td>
				</tr>";
				
			$groupfc = 0;
			$groupfc2 = 0;
			$groupVar = 0;
			$groupVarDollars = 0;
		}
		
		///group header
		if($lastGroup != $values['acct_name']){
			echo "<tr bgcolor=#FFFF99>
				<td style=\"border:1px solid black;\">{$values['acct_name']}</td>
				<td style=\"border:1px solid black;\">Cost</td>
				<td style=\"border:1px solid black;\">Sold</td>
				<td style=\"border:1px solid black;\">Waste</td>
				<td style=\"border:1px solid black;\">Purchases</td>
				<td style=\"border:1px solid black;\">Count</td>
				<td style=\"border:1px solid black;\">Ideal</td>
				<td style=\"border:1px solid black;\">Variance</td>
				<td style=\"border:1px solid black;\">Variance$</td>
				<td style=\"border:1px solid black;\">Ideal%</td>
				<td style=\"border:1px solid black;\">Actual%</td>
				<td style=\"border:1px solid black;\">Variance%</td>
			</tr>";
		}
	
		echo "<tr>
				<td style=\"border:1px solid black;\">{$values['item_name']}</td>
				<td style=\"border:1px solid black;\" align=right>" . "$" . "{$count2[$id]['price']}</td>
				<td style=\"border:1px solid black;\">$showSold</td>
				<td style=\"border:1px solid black;\">$showWaste</td>
				<td style=\"border:1px solid black;\">$showPurch</td>
				<td style=\"border:1px solid black;\">{$count2[$id]['amount']}&nbsp;&nbsp;{$values['count_size']}</td>
				<td style=\"border:1px solid black;\">$ideal</td>
				<td style=\"border:1px solid black;\">$variance</td>
				<td style=\"border:1px solid black;\" align=right>$varDollars</td>
				<td style=\"border:1px solid black;\" align=right>$fc%</td>
				<td style=\"border:1px solid black;\" align=right>$fc2%</td>
				<td style=\"border:1px solid black;\" align=right><font color=$color>$varPercent%</font></td>
			</tr>";
			
		$lastGroup = $values['acct_name'];
	}
	
	///last group footer
	echo "<tr bgcolor=#CCCCCC><td style=\"border:1px solid black;\" colspan=7><b>$lastGroup Totals</b></td>
				<td style=\"border:1px solid black;\">&nbsp;</td>
				<td style=\"border:1px solid black;\" align=right><b>$$groupVarDollars</b></td>
				<td style=\"border:1px solid black;\" align=right><b>$groupfc%</b></td>
				<td style=\"border:1px solid black;\" align=right><b>$groupfc2%</b></td>
				<td style=\"border:1px solid black;\" align=right><b>$groupVar%</b></td>
				</tr>";
	
	$varTotal = number_format($varTotal, 2);
	echo "<tfoot>
			<tr bgcolor=#AAAAAA>
				<td style=\"border:1px solid black;\" colspan=7><b>Sheet Totals</b></td>
				<td style=\"border:1px solid black;\">&nbsp;</td>
				<td style=\"border:1px solid black;\" align=right><b>$$varTotal</b></td>
				<td style=\"border:1px solid black;\" align=right><b>$totalfc%</b></td>
				<td style=\"border:1px solid black;\" align=right><b>$totalfc2%</b></td>
				<td style=\"border:1px solid black;\" align=right><b>$totalVar%</b></td>
			</tr>
		</tfoot>";
	
	echo "</table><p>";
?>