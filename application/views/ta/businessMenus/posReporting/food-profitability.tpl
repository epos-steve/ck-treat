<?php
	$date2 = "$t_date1 00:00:00";
	$date3 = "$t_date2 23:59:59";

	////days serviced
	$query = "SELECT count(vms.id) AS service, lr.route FROM vend_machine_schedule vms
				JOIN vend_machine vm ON vm.machineid = vms.machineid
				JOIN machine_bus_link mbl ON mbl.machine_num = vm.machine_num 
					AND mbl.businessid = $businessid 
					AND mbl.ordertype = $ordertype
				JOIN login_route lr ON lr.login_routeid = vm.login_routeid";
	$result = Treat_DB_ProxyOldProcessHost::query( $query );

	$service = @mysql_result($result,0,"service");
	$route = @mysql_result($result,0,"route");

	////participates in a program?
	$query = "SELECT pp.name FROM participation_program pp
				JOIN setup_detail sd ON sd.businessid = $businessid 
					AND sd.setupid = 70
					AND sd.value = pp.id
					AND sd.value > 0";
	$result = Treat_DB_ProxyOldProcessHost::query( $query );

	$pp_name = @mysql_result($result,0,"name");

	if($pp_name != ""){$show_pp = "This Kiosk participates in $pp_name.";}

	echo "<table style=\"border:1px solid black;background-color:white;font-size:12px;\" cellspacing=0 cellpadding=1 width=50%>"; 
	echo "<tr bgcolor=#FFFF99><td style=\"border:1px solid black;\"><center>&nbsp; This Kiosk is serviced $service time(s) a week by route $route. $show_pp &nbsp;</center></td></tr>";
	echo "</table>";
	
	////days between
	$start = strtotime($date2);
	$end = strtotime($date3);

	$days_between = ceil(abs($end - $start) / 86400);

	///table header
	echo "<table class=\"sortable\" style=\"border:1px solid black;background-color:white;font-size:12px;\" cellspacing=0 cellpadding=1 width=98%>";
	echo "
		<thead>
			<tr bgcolor=#FFFF99>
				<td style=\"border:1px solid black;\" width=8%>Item Code</td>
				<td style=\"border:1px solid black;\" width=20%>Item</td>
				<td style=\"border:1px solid black;\">Current Cost</td>
				<td style=\"border:1px solid black;\">Current Price</td>
				<td style=\"border:1px solid black;\">Margin</td>
				<td style=\"border:1px solid black;\">Max/Reorder Point</td>
				<td style=\"border:1px solid black;\">Items Sold</td>
				<td style=\"border:1px solid black;\">Waste</td>
				<td style=\"border:1px solid black;\">Avg Daily Sales</td>
				<td style=\"border:1px solid black;\">OnHand</td>
				<td style=\"border:1px solid black;\">% of Sales</td>
				<td style=\"border:1px solid black;\" title='# Times Out of Stock'>OOS</td>
				<td style=\"border:1px solid black;\">Recommendation</td>
			</tr>
		</thead>
		<tbody>
	";

	$download_data = "Item Code,Name,Current Cost,Current Price,Margin,Max/Reorder Point,Items Sold,Waste,Avg. Daily Sales,OnHand,%ofSales,OOS,Recommendation\r\n";

	////machine sales for period
	foreach($machine_num AS $count => $machine){
		$query = "SELECT SUM(creditdetail.amount) AS sales 
					FROM creditdetail 
					JOIN machine_bus_link mbl ON mbl.machine_num = '$machine' 
					WHERE mbl.creditid = creditdetail.creditid
						AND creditdetail.date BETWEEN '$date2' AND '$date3'";
		$result = Treat_DB_ProxyOldProcessHost::query( $query );

		$mach_sales[$machine] = mysql_result($result, 0, "sales");
	}

	foreach( $items AS $key => $value ) {
		$sales = 0;
		$history = "";
		
		////get item_code
		$complete_query = "SELECT menu_items_new.id,
							menu_items_new.item_code,
							menu_items_new.max,
							menu_items_new.reorder_point,
							menu_items_new.reorder_amount,
							menu_items_new.active,
							menu_items_price.price,
							menu_items_price.cost
					FROM menu_items_new
					JOIN menu_items_price ON menu_items_price.menu_item_id = menu_items_new.id
					WHERE menu_items_new.businessid = $businessid AND menu_items_new.pos_id = $key";
		$result = Treat_DB_ProxyOldProcessHost::query( $complete_query );

		$item_id = mysql_result( $result, 0, "id" );
		$item_code = mysql_result( $result, 0, "item_code" );
		$max = mysql_result( $result, 0, "max" );
		$reorder_point = mysql_result( $result, 0, "reorder_point" );
		$reorder_amount = mysql_result( $result, 0, "reorder_amount" );
		$inactive = mysql_result( $result, 0, "active" );
		$price = mysql_result( $result, 0, "price" );
		$cost = mysql_result( $result, 0, "cost" );

		////get dates for current onhand
		$dates = Kiosk::last_inv_dates( $machine_num, $item_code, 0 , $date3 );
		$date = $dates[1];
		if( $date == "" ) {
			$date = $dates[0];
		}
		////last count
		$cur_onhand2 = Kiosk::inv_onhand( $machine_num, $item_code, $date );
		////fills
		$cur_fills = Kiosk::fills( $machine_num, $item_code, $date, $date3 );
		////waste
		$cur_waste = Kiosk::waste( $machine_num, $item_code, $date, $date3 );
		////get check detail
		$cur_items_sold = menu_items::items_sold( $key, $businessid, $date, $date3, $current_terminal );
		///current onhand
		$cur_onhand = $cur_onhand2 + $cur_fills - $cur_waste - $cur_items_sold;

		////get dates
		$dates = Kiosk::last_inv_dates( $machine_num, $item_code, 0 , $date2 );
		$date = $dates[1];
		if( $date == "" ) {
			$date = $dates[0];
		}
		
		////last count
		$onhand2 = Kiosk::inv_onhand( $machine_num, $item_code, $date );

		////fills
		$fills = Kiosk::fills( $machine_num, $item_code, $date, $date2 );

		////waste
		$waste = Kiosk::waste( $machine_num, $item_code, $date, $date2 );

		////get check detail
		$items_sold = menu_items::items_sold( $key, $businessid, $date, $date2, $current_terminal );

		////starting onhand amount
		$onhand = $onhand2 + $fills - $waste - $items_sold;
		
		$machines = implode(",", $machine_num);
		$machines = str_replace(",","','",$machines);
		
		///history
		$query = "SELECT checks.bill_datetime AS date_time,
			checkdetail.quantity AS amt, 
			'1' AS type,
			'' AS machine_num,
			checkdetail.price
		FROM checkdetail
		JOIN checks ON checks.check_number = checkdetail.bill_number 
			AND checks.businessid = $businessid
			AND checks.bill_datetime BETWEEN '$date2' AND '$date3'
		where checkdetail.businessid = $businessid 
			AND checkdetail.item_number = $key
		UNION
		SELECT inv_count_vend.date_time AS date_time,
			inv_count_vend.fills AS amt,
			'2' as type,
			inv_count_vend.machine_num,
			0 AS price
		FROM inv_count_vend 
		WHERE item_code = '$item_code' 
			AND machine_num IN ('$machines') 
			AND date_time BETWEEN '$date2' AND '$date3' 
			AND is_inv = 0 
			AND fills != 0
		UNION 
		SELECT inv_count_vend.date_time as date_time, 
			inv_count_vend.waste AS amt, 
			'3' as type,
			inv_count_vend.machine_num,
			0 AS price
		FROM inv_count_vend 
		WHERE item_code = '$item_code' 
			AND machine_num IN ('$machines') 
			AND date_time BETWEEN '$date2' AND '$date3' 
			AND is_inv = 0 
			AND waste != 0
		UNION 
		SELECT inv_count_vend.date_time as date_time, 
			inv_count_vend.onhand AS amt, 
			'4' as type,
			inv_count_vend.machine_num,
			0 AS price
		FROM inv_count_vend 
		WHERE item_code = '$item_code' 
			AND machine_num IN ('$machines') 
			AND date_time BETWEEN '$date2' AND '$date3' 
			AND is_inv = 1 
		ORDER BY date_time
		";
		$result = Treat_DB_ProxyOldProcessHost::query( $query );
	
		///format
		$cur_cost = number_format($cost, 2);
		$cur_price = number_format($price, 2);
		$margin = round(($cur_price - $cur_cost) / $cur_price * 100, 1);
		
		////go through data
		$current = $onhand;
		$min_current = $current;
		$recommend = null;
		$oos = 0;
		$fills = 0;
		$sold = 0;
		$waste = 0;
		$skew = 0;
		$counts = 0;
		while($r = mysql_fetch_array($result)){
			$time = $r["date_time"];
			$amount = $r["amt"];
			$type = $r["type"];
			if($r["machine_num"] != "") $cur_machine = $r["machine_num"];
			$price = $r["price"];
			
			if($type == 1){$sold += $amount; $sales += $amount * $price;}
			elseif($type == 2){$fills += $amount;}
			elseif($type == 3){$waste += $amount;}
			
			if($type == 4){$current = $amount; $hist_rec = "count"; $counts++;}
			elseif($type == 1){$current -= $amount; $hist_rec = "sale";}
			elseif($type == 3){$current -= $amount; $hist_rec = "waste";}
			elseif( $type == 2){$current += $amount; $hist_rec = "fill";}
			
			$history .= "$hist_rec: $amount / $current \n";
			
			if($current <= 0 && $last_current > 0) $oos++;
			
			$last_current = $current;
			if($current < $min_current) $min_current = $current;
		}

		$sales_prcnt = round($sales / $mach_sales[$cur_machine] * 100, 1);

		$avg_daily_sales = round($sold / $days_between, 1);

		////recommendations
		if( $sold > 0 && ($waste / $sold) >= .1 && $sold > $waste)
			$recommend = "<font color=red>Lower the max because of waste.</font> ";
		elseif($waste >= $sold && $waste != 0 && $sold != 0)
			$recommend = "<font color=red>Remove this product because of waste.</font> ";
		elseif($sold == 0)
			$recommend = "<font color=red>Remove this product because it's not selling.</font> ";
		elseif($sales_prcnt <= .2)
			$recommend = "Consider replacing this product. ";
		elseif($min_current > ($avg_daily_sales * 2) && $min_current > 6)
			$recommend = "Lower the max on this product. Onhand never under $min_current. ";
		elseif($oos > 2)
			$recommend = "Raise the max/reorder point of this product. ";
		elseif($oos > 1)
			$recommend = "Consider raising the max/reorder point of this product. ";

		if($cost == 0)
			$recommend .= "Add a cost.";

		if($counts == 0)
			$recommend .= " This product was not counted.";

		echo "
			<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor=''>
				<td style=\"border:1px solid black;\">&nbsp;$item_code</td>
				<td style=\"border:1px solid black;\">
					<a href='?bid=$businessid&cid=$companyid&report=13&id=$item_id&terminal_no=$current_terminal&which=$prevwhich&ordertype=$ordertype' style=$style>$value</a>
				</td>
				<td style=\"border:1px solid black;\">&nbsp;$$cur_cost</td>
				<td style=\"border:1px solid black;\">&nbsp;$$cur_price</td>
				<td style=\"border:1px solid black;\">&nbsp;$margin%</td>
				<td style=\"border:1px solid black; cursor: pointer;\" class=\"max-order-change\" data-item-id=\"$item_id\" data-max=\"$max\" data-reorder-point=\"{$reorder_point}\" data-reorder-amount=\"{$reorder_amount}\">&nbsp;$max/$reorder_point</td>
				<td style=\"border:1px solid black;\">&nbsp;$sold</td>
				<td style=\"border:1px solid black;\">&nbsp;$waste</td>
				<td style=\"border:1px solid black;\">&nbsp;$avg_daily_sales</td>
				<td style=\"border:1px solid black;\">&nbsp;$cur_onhand</td>
				<td style=\"border:1px solid black;\">&nbsp;$sales_prcnt%</td>
				<td style=\"border:1px solid black;\">&nbsp;$oos</td>
				<td style=\"border:1px solid black;\">&nbsp;$recommend</td>
			</tr>";

		$download_data .= "$item_code,\"$value\",$cur_cost,$cur_price,$margin,$max/$reorder_point,$sold,$waste,$avg_daily_sales,$cur_onhand,$sales_prcnt,$oos,$recommend\r\n";
	}

	echo "</tbody>";
	echo "</table><br>";
        
        ////suggested replacements
        $complete_query = "SELECT mim.UPC, mim.Name, m.name AS manufacturer, min.id, min.active
            FROM menu_items_master mim
            LEFT JOIN manufacturer m ON m.id = mim.Manufacturer
            LEFT JOIN menu_items_new min ON min.businessid = $businessid AND min.upc = mim.UPC
            WHERE mim.ordertype = $ordertype AND sold >= 100
            ORDER BY sold DESC";
	$result = Treat_DB_ProxyOldProcessHost::query( $complete_query );
        
        ///table
        echo "Recommended items are based off sales across all Company Kitchen units.";
        echo "<table style=\"border:1px solid black;background-color:white;font-size:12px;\" cellspacing=0 cellpadding=1 width=98%>";
        echo "<thead><tr bgcolor=#FFFF99><td style=\"border:1px solid black;\">Suggested Replacements</td><td style=\"border:1px solid black;\">UPC</td><td style=\"border:1px solid black;\">Manufacturer</td><td style=\"border:1px solid black;\">Note</td></tr></thead><tbody>";
        
        ///results
        $count = 0;
        while($r = mysql_fetch_array($result)){
            if($count == 25) break;
        
            $UPC = $r["UPC"];
            $Name = $r["Name"];
            $manufacturer = $r["manufacturer"];
            $local_id = $r["id"];
            $active = $r["active"];
            
            if($local_id > 0 && $active == 0){
                //nothing, item already exists and is active
                $note = "";
            }
            elseif($local_id > 0 && $active == 1){
                //item exists, but is inactive, recommend
                $note = "This item exists in the menu, but is currently inactive.";
                echo "<tr bgcolor=$bColor><td style=\"border:1px solid black;\">$Name</td><td style=\"border:1px solid black;\">$UPC</td><td style=\"border:1px solid black;\">$manufacturer</td><td style=\"border:1px solid black;\">$note</td></tr>";
                $count++;
            }
            else{
                //item does not exist, recommend
                $note = "This item does not exist in this kiosk.";
                echo "<tr bgcolor=$bColor><td style=\"border:1px solid black;\">$Name</td><td style=\"border:1px solid black;\">$UPC</td><td style=\"border:1px solid black;\">$manufacturer</td><td style=\"border:1px solid black;\">$note</td></tr>";
                $count++;
            }
        }
        
        ///end table
        echo "</tbody></table><br>";
        
        ///hourly/daily stats
        echo "<table width=98%><tr valign=top>";
            ///hourly
            echo "<td width=25%>";
                $hourly_sales = array();
            
                echo "<table class=\"sortable\" style=\"border:1px solid black;background-color:white;font-size:12px;\" cellspacing=0 cellpadding=1 width=100%>";
                echo "<thead><tr bgcolor=#FFFF99><td style=\"border:1px solid black;\">Hourly Sales</td><td style=\"border:1px solid black;\">Sales</td><td style=\"border:1px solid black;\">%</td></tr></thead><tbody>";
                
                    $complete_query = "SELECT SUM( checks.total ) AS total, SUBSTRING( bill_datetime, 12, 2 ) AS cur_hour 
                                        FROM checks
                                        WHERE businessid =$businessid
                                            AND bill_datetime
                                            BETWEEN  '$date2' AND '$date3'
                                            GROUP BY cur_hour 
                                            ORDER BY cur_hour ";
                    $result = Treat_DB_ProxyOldProcessHost::query( $complete_query );
                    
                    $sum = 0;
                    while($r = mysql_fetch_array($result)){
                        $hour = $r["cur_hour"];
                        $total = $r["total"];
                        
                        $hourly_sales[$hour] += $total;
                        $sum += $total;
                    }
                    
                    foreach($hourly_sales AS $key => $value){
                        $percent = round($value / $sum * 100, 1);
                        echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor=''><td style=\"border:1px solid black;\">$key</td><td style=\"border:1px solid black;\">$$value</td><td style=\"border:1px solid black;\">$percent%</td></tr>";
                    }
                
                echo "</tbody></table>";
            echo "</td>";
            ///daily
            echo "<td width=25%>";
                $daily_sales = array();
            
                echo "<table class=\"sortable\" style=\"border:1px solid black;background-color:white;font-size:12px;\" cellspacing=0 cellpadding=1 width=100%>";
                echo "<thead><tr bgcolor=#FFFF99><td style=\"border:1px solid black;\">Daily Sales</td><td style=\"border:1px solid black;\">Sales</td><td style=\"border:1px solid black;\">%</td></tr></thead><tbody>";
                
                    $complete_query = "SELECT SUM( checks.total ) AS total, DATE( bill_datetime ) AS cur_date
                                        FROM checks
                                        WHERE businessid =$businessid
                                        AND bill_datetime
                                        BETWEEN  '$date2' AND '$date3'
                                        GROUP BY cur_date";
                    $result = Treat_DB_ProxyOldProcessHost::query( $complete_query );
                    
                    $sum = 0;
                    while($r = mysql_fetch_array($result)){
                        $cur_date = $r["cur_date"];
                        $total = $r["total"];
                        
                        $daily_sales[dayofweek($cur_date)] += $total;
                        $sum += $total;
                    }
                    
                    foreach($daily_sales AS $key => $value){
                        $percent = round($value / $sum * 100, 1);
                        echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor=''><td style=\"border:1px solid black;\">$key</td><td style=\"border:1px solid black;\">$$value</td><td style=\"border:1px solid black;\">$percent%</td></tr>";
                    }
                
                echo "</tbody></table>";
            echo "</td>";
            echo "<td width=50%>&nbsp;</td>";
        echo "</tr></table>";
?>