<?php
//echo "T-Date1: " . $t_date1 . "<br>";
//echo "T-Date2: " . $t_date2 . "<br>";
//echo "BID: " . $businessid . "<br>";

$sql = "call mburris_report_tables.zz_load_checks_data(" . $businessid . ", 1, '" . $t_date1 . "', '" . $t_date2 . "')";
$result = Treat_DB_ProxyOldProcessHost::query( $sql );

$sql1 = "TRUNCATE mburris_report_tables.zz_check_detail_orig";
Treat_DB_ProxyOldProcessHost::query( $sql1 );

$sql1a = "INSERT INTO mburris_report_tables.zz_check_detail_orig SELECT * FROM mburris_report_tables.zz_check_detail";
Treat_DB_ProxyOldProcessHost::query( $sql1a );
//echo "SQL: " . $sql . "<br>";

$sql1 = "TRUNCATE mburris_report_tables.zz_payment_detail";
Treat_DB_ProxyOldProcessHost::query( $sql1 );

$sql1 = "insert ignore into mburris_report_tables.zz_payment_detail ( businessid, check_number, payment_type, received, scancode, customerid, purchase_bill_datetime, purchase_date, total_visits ) select pd.businessid, pd.check_number, pd.payment_type, pd.received, pd.scancode, pd.customerid, pc.bill_datetime, date(pc.bill_datetime) as purchase_date, count(*) total_visits from mburris_report_tables.posreports_payment_detail pd join mburris_report_tables.posreports_checks pc using (check_number) where pd.payment_type = 14 group by pd.customerid , purchase_date";
$result1 = Treat_DB_ProxyOldProcessHost::query( $sql1 );

//$sql2 = "SELECT payment_type, sum(received) as sum_received FROM mburris_report_tables.posreports_payment_detail group by payment_type";
$sql2 = "SELECT received, count(*) as received_count FROM mburris_report_tables.posreports_payment_detail WHERE payment_type = 14 group by received order by received_count desc limit 20";
$result2 = Treat_DB_ProxyOldProcessHost::query( $sql2 );

$t=1;
while ($r2 = mysql_fetch_array($result2)){
	
	if ($t==1){ $received = $r2['received']; }
	
	$received_amount = $r2['received'];
	$received_count = $r2['received_count'];
	
	//echo "<tr><td class=\"pdra-1\">$" . $received_amount . "</td><td class=\"pdra-2\">" . $received_count . "</td></tr>";
	
	$t++;

}

?>



<?php

	if (@$_REQUEST['received']){
		$received_value = $_GET['received'];
	} else {
		$received_value = $received;
	}
	$received_value = number_format($received_value, 2);

?>

<div id="popular-three-dollar-items">
<table class="sortable" style="border:1px solid black;font-size:12px;background-color:white;" cellspacing=0 cellpadding=1 width=98%>
<th colspan="9">Most Popular Items for the most popular $<?php echo $received_value; ?> checks</th>
<tr bgcolor=#FFFF99><td id="ptdi-1a-title">UPC</td><td id="ptdi-1c-title">Item</td><td id="ptdi-1d-title">Menu Group</td><td id="ptdi-1e-title">Sold</td><td id="ptdi-1f-title">Price</td><td id="ptdi-1g-title">Margin</td><!--td id="ptdi-1h-title">BID</td><td id="ptdi-1i-title">POS ID</td>--><td id="ptdi-1j-title"># > $<?php echo $received_value; ?></td></tr>
<?php
		
	$sql5 = "SELECT m1.upc as 'upc_value', m1.businessid as 'businessid', if(m1.businessid > 1380, 'Edgewater', 'Colorado Belle') as 'location', m1.name as 'item', m1.pos_id as 'menu_item_pos_id', m3.name as 'group_name', count(*) as 'total_sold', round((m2.price - m2.cost), 2) as 'margin', m2.price 'item_price' FROM mburris_report_tables.posreports_checkdetail p1 left join mburris_report_tables.posreports_menu_items_new m1 on p1.item_number = m1.pos_id left join mburris_report_tables.posreports_menu_items_price m2 on m1.id = m2.menu_item_id left join mburris_report_tables.menu_groups_new m3 on m1.group_id = m3.id where bill_number in ( SELECT check_number  FROM mburris_report_tables.zz_payment_detail WHERE received = $received_value ) group by item_number ORDER BY total_sold DESC limit 10";
	$result5 = Treat_DB_ProxyOldProcessHost::query( $sql5 );

	while ($r5 = mysql_fetch_array($result5)){
	
		$upc_value = $r5['upc_value'];
		$businessid = $r5['businessid'];
		$location = $r5['location'];
		$item = $r5['item'];
		$menu_item_pos_id = $r5['menu_item_pos_id'];
		$group_name = $r5['group_name'];
		$total_sold = $r5['total_sold'];
		$margin = $r5['margin'];
		$item_price = $r5['item_price'];
		
		$sql6 = "SELECT group_concat(distinct bill_number) as 'check_numbers' FROM mburris_report_tables.posreports_checkdetail WHERE businessid = $businessid AND item_number = $menu_item_pos_id group by businessid";
		$result6 = Treat_DB_ProxyOldProcessHost::query( $sql6 );
		$check_numbers = mysql_result($result6, 0, 'check_numbers');
		
		$sql7 = "SELECT count(*) total_checks_above_received_value, group_concat( total ) check_total_amounts FROM mburris_report_tables.posreports_checks WHERE check_number IN ( $check_numbers ) and total > $received_value GROUP BY businessid";
		$result7 = Treat_DB_ProxyOldProcessHost::query( $sql7 );
		
		if (mysql_num_rows($result7) > 0){
			$total_checks_above_received_value = mysql_result($result7, 0, 'total_checks_above_received_value');
			$check_total_amounts = mysql_result($result7, 0, 'check_total_amounts');
			$output = $total_checks_above_received_value . " check(s) for: (" . $check_total_amounts . ")";
		} else {
			$total_checks_above_received_value = 0;
			$check_total_amounts = 0;
			$output = "None";
		}
		
		echo "<tr><td class=\"ptdi-data\">" . $upc_value . "</td>" . "<td class=\"ptdi-data\">" . $item . "</td>" . "<td class=\"ptdi-data\">" . $group_name . "</td>" . "<td class=\"ptdi-data\">" . $total_sold . "</td>" . "<td class=\"ptdi-data\">$" . $item_price . "</td>" . "<td class=\"ptdi-data\">$" . $margin . "</td>" . "<td class=\"ptdi-2-data\">" . $output . "</td>" . "</tr>";

	}

?>
</table>
</div>

<div id="popular-promos">
<table class="sortable" style="border:1px solid black;font-size:12px;background-color:white;" cellspacing=0 cellpadding=1>
<!--tr><td id="pp-1a-title">POS ID</td><td id="pp-1b-title">Location</td><td id="pp-1c-title">Promotion</td><td id="pp-1d-title">Promo Count</td></tr-->
<tr bgcolor=#FFFF99><td id="pp-1c-title">Promotion</td><td id="pp-1d-title">Promo Count</td></tr>
<?php

//for($i=1; $i<11; $i++){

	$sql4 = "SELECT pos_id, COUNT(*) as promo_count, promo_name, businessid, if(businessid > 1380, 'Edgewater', 'Colorado Belle') location FROM mburris_report_tables.zz_promotions_by_locations GROUP BY pos_id ORDER BY COUNT(*) DESC";
	$result4 = Treat_DB_ProxyOldProcessHost::query( $sql4 );

	while ($r4 = mysql_fetch_array($result4)){
	
		$pos_id = $r4['pos_id'];
		$promo_count = $r4['promo_count'];
		$promo_name = $r4['promo_name'];
		$businessid = $r4['businessid'];
		$location = $r4['location'];
		
		//echo "<tr><td class=\"pp-1a\">" . $pos_id . "</td>" . "<td class=\"pp-1b\">" . $location . "</td>" . "<td class=\"pp-1c\">" . $promo_name . "</td>" . "<td class=\"pp-1d\">" . $promo_count . "</td>" . "</tr>";
		
		echo "<tr><td class=\"pp-1c\">" . $promo_name . "</td>" . "<td class=\"pp-1d\">" . $promo_count . "</td>" . "</tr>";

	}
//}
?>
</table>
</div>

<div id="pd-received-amount-1">
<table class="sortable" style="border:1px solid black;font-size:12px;background-color:white;" cellspacing=0 cellpadding=1>
<tr bgcolor=#FFFF99><td id="pdra-1-title">Amount</td><td id="pdra-2-title"># Checks</td></tr>
<?php

//$sql2 = "SELECT payment_type, sum(received) as sum_received FROM mburris_report_tables.posreports_payment_detail group by payment_type";
$sql2 = "SELECT received, count(*) as received_count FROM mburris_report_tables.posreports_payment_detail WHERE payment_type = 14 group by received order by received_count desc limit 5";
$result2 = Treat_DB_ProxyOldProcessHost::query( $sql2 );

$t=1;
while ($r2 = mysql_fetch_array($result2)){
	
	if ($t==1){ $received = $r2['received']; }
	
	$received_amount = $r2['received'];
	$received_count = $r2['received_count'];
	
	echo "<tr><td class=\"pdra-1\">$" . $received_amount . "</td><td class=\"pdra-2\">" . $received_count . "</td></tr>";
	
	$t++;

}

?>
</table>
</div>



<div id="pd-received-amount-2">
<table class="sortable" style="border:1px solid black;font-size:12px;background-color:white;" cellspacing=0 cellpadding=1>
<tr bgcolor=#FFFF99><td id="pdra-1-title">Total Visits</td><td id="pdra-2-title">Customers</td><td id="pdra-3-title">Totals</td><td id="pdra-4-title">Min Spent</td><td id="pdra-5-title">Max Spent</td><td id="pdra-6-title">Average</td></tr>
<?php

for($i=1; $i<11; $i++){

	//$sql3 = "SELECT " . $i . " as total_visits, COUNT(*) num_customers, SUM(sum_received) sum_received, ROUND(AVG(sum_received), 2) avg_sum_received, MIN(sum_received) min_sum_received, MAX(sum_received) max_sum_received FROM mburris_report_tables.zz_test WHERE total_visits = " . $i;
	
	$sql3 = "SELECT " . $i . " as total_visits, COUNT(*) num_customers, SUM(received) sum_received, ROUND(AVG(received), 2) avg_sum_received, MIN(received) min_sum_received, MAX(received) max_sum_received FROM mburris_report_tables.zz_payment_detail WHERE received > 0 AND total_visits = " . $i;
	$result3 = Treat_DB_ProxyOldProcessHost::query( $sql3 );

	while ($r3 = mysql_fetch_array($result3)){
	
		$total_visits = $r3['total_visits'];
		$num_customers = $r3['num_customers'];
		$sum_received = $r3['sum_received'];
		$avg_sum_received = $r3['avg_sum_received'];
		$min_sum_received = $r3['min_sum_received'];
		$max_sum_received = $r3['max_sum_received'];
		
		if ($num_customers > 0){
			echo "<tr><td class=\"pdra2-data-1\">" . $total_visits . "</td>" . "<td class=\"pdra2-data-2\">" . $num_customers . "</td>" . "<td class=\"pdra2-data-3\">$" . $sum_received . "</td>" . "<td class=\"pdra2-data-4\">$" . $min_sum_received . "</td>" . "<td class=\"pdra2-data-5\">$" . $max_sum_received . "</td>" . "<td class=\"pdra2-data-6\">$" . $avg_sum_received . "</td>" . "</tr>";
		}

	}
}
?>
</table>
</div>

