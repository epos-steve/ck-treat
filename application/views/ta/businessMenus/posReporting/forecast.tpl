<?php
	$daysBack = array(7,14,21,28);
	
	$tempdate = $o_date;
	$dates = array();
	
	foreach($daysBack AS $key => $value){
		$dates[] = date( "Y-m-d", mktime( 0, 0, 0, substr($o_date,5,2), substr($o_date,8,2) - $value, substr($o_date,0,4) ) );
	}
	
	$dates = implode("','",$dates);
	
	echo "<table class=\"sortable\" style=\"border:1px solid black;font-size:12px;background-color:white;\" cellspacing=0 cellpadding=1 width=98%>";
	echo "<thead><tr bgcolor=#FFFF99><td style=\"border:1px solid black;\" width=25%>Item Code</td><td style=\"border:1px solid black;\" width=25%>Item</td><td style=\"border:1px solid black;\" width=25%>Forecast</td><td style=\"border:1px solid black;\" width=25%>Actual</td></thead><tbody>";
	$download_data .= "Item Code,Item Name,Forecast,Actual\r\n";
	
	foreach ($items AS $id => $itemName){
		$query = "SELECT item_code FROM menu_items_new min WHERE businessid = $businessid AND pos_id = $id";
		$result = Treat_DB_ProxyOldProcessHost::query( $query );
		
		$r = mysql_fetch_array($result);
		$item_code = $r['item_code'];
	
		$query = "SELECT ROUND(sum(cd.quantity) / 4) AS forecast FROM checks c
			JOIN checkdetail cd ON cd.businessid = c.businessid 
				AND cd.bill_number = c.check_number
			JOIN menu_items_new min ON min.businessid = cd.businessid 
				AND min.pos_id = cd.item_number 
				AND min.pos_id = $id
			WHERE DATE(c.bill_datetime) IN ('$dates') 
				AND c.businessid = $businessid";
		$result = Treat_DB_ProxyOldProcessHost::query( $query );
	
		$r = mysql_fetch_array($result);
		$forecast = $r['forecast'];
		
		$query = "SELECT SUM(cd.quantity) AS actual FROM checks c
			JOIN checkdetail cd ON cd.businessid = c.businessid 
				AND cd.bill_number = c.check_number
			JOIN menu_items_new min ON min.businessid = cd.businessid 
				AND min.pos_id = cd.item_number 
				AND min.pos_id = $id
			WHERE DATE(c.bill_datetime) = '$o_date' 
				AND c.businessid = $businessid";
		$result = Treat_DB_ProxyOldProcessHost::query( $query );
		
		$r = mysql_fetch_array($result);
		$actual = $r['actual'];
		
		echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor=''><td style=\"border:1px solid black;\">&nbsp;$item_code</td><td style=\"border:1px solid black;\">&nbsp;$itemName</td><td style=\"border:1px solid black;\" align=right>$forecast&nbsp;</td><td style=\"border:1px solid black;\" align=right>$actual&nbsp;</td></tr>";
		$download_data .= "$item_code,\"$itemName\",$forecast,$actual\r\n";
	}
	echo "</table><p>";
?>