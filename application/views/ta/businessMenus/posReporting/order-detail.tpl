<?php
        if($ordertype > 0){
            $addquery = "AND mbl.ordertype = $ordertype";
        }
		
		$edit = $_GET["edit"];
		$success = $_GET["success"];
		$receive = $_GET["receive"];

        $query = "SELECT modl.menu_items_new_id, 
                    modl.item_code, 
                    modl.qty,
                    modl.new_item_code,
                    modl.new_qty,
					modl.new_cost,
					modl.new_price,
                    min.name,
					min.upc,
					mip.price,
                    mip.cost,
					mbl.machine_num,
					mbl.leadtime
                  FROM machine_orderdetail modl
                  JOIN menu_items_new min ON min.id = modl.menu_items_new_id
                  JOIN menu_items_price mip ON mip.menu_item_id = min.id
                  JOIN machine_bus_link mbl ON mbl.businessid = $businessid $addquery
                  JOIN vend_machine vm ON vm.machine_num = mbl.machine_num
                  JOIN machine_order mo ON mo.id = modl.orderid AND mo.machineid = vm.machineid AND mo.expected_delivery = '$o_date'
                  ORDER BY modl.item_code";
        $result = Treat_DB_ProxyOldProcessHost::query( $query );

        ?>
        <table style="border:1px solid black;background-color:white;font-size:12px;" cellspacing=0 cellpadding=1 width=98%>
		<thead>
            <tr bgcolor=#FFFF99>
                <td style="border:1px solid black;" width=8%>Item Code</td>
                <td style="border:1px solid black;" width=30%>Item</td>
                <td style="border:1px solid black;">Amount</td>
				<td style="border:1px solid black;">Cost</td>
				<td style="border:1px solid black;">Price</td>
				<td style="border:1px solid black;">Margin</td>
				<td style="border:1px solid black;">Received</td>
            </tr>
        </thead>
        <tbody>
        
        <?php
		if($edit == 1){
			while($r = mysql_fetch_array($result)){
				$cur_order[$r["menu_items_new_id"]] = $r["item_code"];
				$cur_order_qty[$r["menu_items_new_id"]] = $r["qty"];
				$cur_order_cost[$r["menu_items_new_id"]] = $r["cost"];
				$cur_order_price[$r["menu_items_new_id"]] = $r["price"];
				$cur_order_new_cost[$r["menu_items_new_id"]] = $r["new_cost"];
				$cur_order_new_price[$r["menu_items_new_id"]] = $r["new_price"];
			}
			
			////find order
			$query = "SELECT lr.route, lr.login_routeid, vm.machineid, mo.id
				FROM machine_bus_link mbl
				JOIN vend_machine vm ON vm.machine_num = mbl.machine_num
				JOIN login_route lr ON lr.login_routeid = vm.login_routeid
				LEFT JOIN machine_order mo ON mo.machineid = vm.machineid AND mo.expected_delivery = '$o_date'
				WHERE mbl.businessid = $businessid AND mbl.ordertype = $ordertype";
			$result = Treat_DB_ProxyOldProcessHost::query( $query );
			
			$route = @mysql_result( $result, 0 , "route");
			$login_routeid = @mysql_result( $result, 0 , "login_routeid");
			$machineid = @mysql_result( $result, 0 , "machineid");
			$orderid = @mysql_result( $result, 0 , "id");
			
			if($route == "" || $login_routeid == "" || $machineid == ""){
				echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor=''>
							<td style=\"border:1px solid black;\" colspan=7>&nbsp;<font color=red>Route or Machine not properly setup.</font></td>
						 </tr>";
			}
			else{
				//create order if not exist
				if($orderid < 1){
					$query = "INSERT INTO machine_order (route,expected_delivery,login_routeid,machineid) 
								VALUES ('$route','$o_date',$login_routeid,$machineid)";
					$result = Treat_DB_ProxyOld::query( $query );
					$orderid = mysql_insert_id();
				}
			
				///display
				foreach( $items AS $key => $value ) {
					$complete_query = "SELECT min.id,
								min.item_code,
								mip.price,
								mip.cost
							FROM menu_items_new min 
						JOIN menu_items_price mip ON mip.menu_item_id = min.id
						WHERE min.businessid = $businessid AND min.pos_id = '$key'";
					$result = Treat_DB_ProxyOldProcessHost::query( $complete_query );

					$item_id = mysql_result( $result, 0, "id" );
					$item_code = mysql_result( $result, 0, "item_code" );
					$price = mysql_result( $result, 0, "price" );
					$cost = mysql_result( $result, 0, "cost" );
					
					if($cur_order_new_cost[$item_id] != ''){
						$cost = $cur_order_new_cost[$item_id];
						$costUpdate = "<font color=red>*</font>";
					}
					else{
						$costUpdate = "";
					}
					
					if($cur_order_new_price[$item_id] != ''){
						$price = $cur_order_new_price[$item_id];
						$priceUpdate = "<font color=red>*</font>";
					}
					else{
						$priceUpdate = "";
					}
					
					$margin = round(($price - $cost) / $price * 100, 1);
					$cost = number_format($cost, 2);
					$price = number_format($price, 2);
					$new_cost = number_format($cur_order_new_cost[$item_id], 2);

					////skip if imported and empty
					if($success != 0 && $cur_order_qty[$item_id] == 0) continue;
					
					///display
					echo "<tr id=row$item_id>
							<td style=\"border:1px solid black;\">&nbsp;$item_code</td>
							<td style=\"border:1px solid black;\">&nbsp;$value</td>
							<td style=\"border:1px solid black;\">&nbsp;<input type=text id=\"qty$item_id\" name=\"qty$item_id\" size=4 onchange=\"update_order($orderid,$item_id,'$item_code','qty');\" value=$cur_order_qty[$item_id]></td>
							<td style=\"border:1px solid black;\">&nbsp;<input type=text id=\"cost$item_id\" name=\"cost$item_id\" size=4 onchange=\"update_order($orderid,$item_id,'$item_code','new_cost');\" value=$cost>$costUpdate</td>
							<td style=\"border:1px solid black;\">&nbsp;<input type=text id=\"price$item_id\" name=\"price$item_id\" size=4 onchange=\"update_order($orderid,$item_id,'$item_code','new_price');\" value=$price>$priceUpdate</td>
							<td style=\"border:1px solid black;\">&nbsp;<span id=\"margin$item_id\">$margin%</span></td>
							<td style=\"border:1px solid black;\">&nbsp;</td>
						 </tr>";
				}
			}
			
			if($success === '0'){
				$showImport = "Nothing Imported&nbsp;&nbsp;";
			}
			elseif($success > 0){
				$showImport = "$success Records Imported&nbsp;&nbsp;";
			}
			else{
				$showImport = "";
			}
			
			echo "<tfoot><tr bgcolor=#FFFF99>
						<td style=\"border:1px solid black;\" colspan=7 align=right>
							$showImport
							<form style=\"display:inline;border:0;\" id=\"uploadFile\" name=\"uploadFile\" action=\"/ta/utility/importOrder.php\" method=\"post\" enctype=\"multipart/form-data\">
							<input type=hidden name=businessid value=$businessid>
							<input type=hidden name=companyid value=$companyid>
							<input type=hidden name=orderid value=$orderid>
							<input type=hidden name=ordertype value=$ordertype>
							<input type=file id=\"fileinput\" name=\"fileinput\" size=50>
							<input type=submit value='Import'></form>&nbsp;&nbsp;
							<input type=button value='Save' onclick=\"window.open('?bid=$businessid&cid=$companyid&report=17&terminal_no=$current_terminal&which=$prevwhich&ordertype=$ordertype', '_self');\">&nbsp;
						</td>
					 </tr></tfoot></table>";
		}
		else{
			$counter = 0;
		
            while($r = mysql_fetch_array($result)){
				$item_id = $r["menu_items_new_id"];
                $item_code = $r["item_code"];
                $qty = $r["qty"];
                $new_item_code = $r["new_item_code"];
                $new_qty = $r["new_qty"];
                $item_name = $r["name"];
				$upc = $r["upc"];
				$price = $r["price"];
				$new_price = $r["new_price"];
                $cost = $r["cost"];
				$new_cost = $r["new_cost"];
				$machine_num = $r["machine_num"];
				$leadtime = $r["leadtime"];
                
                $item_sub = "";
                $qty_sub = "";
				
				$counter++;
                
                if($item_code != $new_item_code && $new_item_code != ""){
                    $item_code = $new_item_code;
                    
                    $query2 = "SELECT name FROM menu_items_new WHERE businessid = $businessid AND item_code = '$item_code'";
                    $result2 = Treat_DB_ProxyOldProcessHost::query( $query2 );
                    
                    $item_name = @mysql_result($result2,0,"name");
                    
                    $item_sub = "<font color=green>*</font>";
                }
                if($new_qty != $qty && $new_qty != ""){
                    $qty = $new_qty;
                    
                    $qty_sub = "<font color=blue>*</font>";
                }
				
				///fills
				$query2 = "SELECT SUM(fills) AS fills, date_time FROM inv_count_vend WHERE item_code = '$item_code' AND machine_num = '$machine_num' AND DATE(date_time) = '$o_date' GROUP BY item_code";
				$result2 = Treat_DB_ProxyOldProcessHost::query( $query2 );

				$fills = @mysql_result($result2,0,"fills");
				$date_time = @mysql_result($result2,0,"date_time");
					
				////receive items
				if($receive == 1 && $fills == 0){
					$query2 = "INSERT INTO inv_count_vend (machine_num,date_time,item_code,fills,is_inv)
						VALUES ('$machine_num',NOW(),'$item_code',$qty,0)
						ON DUPLICATE KEY UPDATE fills = $qty";
					$result2 = Treat_DB_ProxyOld::query( $query2 );
					
					$query2 = "UPDATE menu_items_new SET active = 0 WHERE id = $item_id";
					$result2 = Treat_DB_ProxyOld::query( $query2 );
					
					$fills = $qty;
					$date_time = date("Y-m-d H:i:s");
					
					if($new_cost != '' || $new_price != ''){
						if($new_cost != ''){
							$updateQuery = "cost = '$new_cost'";
						}
						if($new_price != ''){
							if($updateQuery != ""){
								$updateQuery .= ", new_price = '$new_price'";
							}
							else{
								$updateQuery = "new_price = '$new_price'";
							}
						}
						$query2 = "UPDATE menu_items_price SET $updateQuery WHERE menu_item_id = $item_id";
						$result2 = Treat_DB_ProxyOld::query( $query2 );
						
						Treat_Model_KioskSync::addSync($businessid, $item_id, Treat_Model_KioskSync::TYPE_ITEM);
						Treat_Model_KioskSync::addSync($businessid, $item_id, Treat_Model_KioskSync::TYPE_ITEM_TAX);
					}
				}
				
				if($qty != $fills){
					if($fills == ''){$fills = 0;}
					$show_diff = "<font color=red><b>($fills) ";
					$show_diff2 = "</b></font color=red>";
				}
				else{
					$show_diff = "";
					$show_diff2 = "";
				}
				
				if($date_time == ""){
					$notReceived = 1;
				}
				
				if($new_cost != ''){
					$cost = $new_cost;
					$costUpdate = "<font color=red>*</font>";
				}
				else{
					$costUpdate = "";
				}
				
				if($new_price != ''){
					$price = $new_price;
					$priceUpdate = "<font color=red>*</font>";
				}
				else{
					$priceUpdate = "";
				}
				
				$margin = round(($price - $cost) / $price * 100, 1);
				$cost = number_format($cost, 2);
				$price = number_format($price, 2);
                
                echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor=''>
                        <td style=\"border:1px solid black;\">&nbsp;$item_code</td>
                        <td style=\"border:1px solid black;\">&nbsp;$item_name$item_sub</td>
                        <td style=\"border:1px solid black;\">&nbsp;$qty$qty_sub</td>
						<td style=\"border:1px solid black;\">&nbsp;$$cost$costUpdate</td>
						<td style=\"border:1px solid black;\">&nbsp;$$price$priceUpdate</td>
						<td style=\"border:1px solid black;\">&nbsp;$margin%</td>
						<td style=\"border:1px solid black;\">&nbsp;$show_diff$date_time$show_diff2</td>
                     </tr>";
					 
				$download_data .= "\"$item_name\",\"$upc\",$price\r\n";
            }
        ?>
        
        </tbody>
        <tfoot>  
			<?php
			echo "<tr bgcolor=#FFFF99>
					<td style=\"border:1px solid black;\" colspan=7 align=right>";
						if($o_date >= date("Y-m-d",mktime(0, 0, 0, date("m") , date("d") + $leadtime, date("Y"))) && $ordertype > 0){
							if($counter > 0){
								$showedit = 'Edit Order';
							}
							else{
								$showedit = 'Create Order';
							}
							echo "<a href='?bid=$businessid&cid=$companyid&report=17&terminal_no=$current_terminal&which=$prevwhich&ordertype=$ordertype&edit=1' style=$style><font color=blue>$showedit</font></a>&nbsp;";
							$showme = 1;
						}
						if($o_date <= date("Y-m-d") && $notReceived == 1){
							if($showme == 1){echo " | &nbsp;";}
							echo "<a href='?bid=$businessid&cid=$companyid&report=17&terminal_no=$current_terminal&which=$prevwhich&ordertype=$ordertype&receive=1' style=$style onclick=\"return confirm('Are you sure you want to receive this order?');\"><font color=blue>Receive Order</font></a>&nbsp;";
						}
					echo "</td>
				 </tr>";
			?>
        </tfoot>
        </table>
        <font size=2><font color=red>*</font>Amount Change <font color=blue>*</font>Quantity Changed <font color=green>*</font>Item Substituted</font> 
        <?php
		}

?>