<?php
	$date2 = $t_date1;
	//$date2 = date( "Y-m-d H:i:s" );
	$forty_date = date( "Y-m-d", mktime( 0, 0, 0, date( "m" ), date( "d" ) - 40, date( "Y" ) ) );

	echo "<table class=\"sortable\" style=\"border:1px solid black;background-color:white;font-size:12px;\" cellspacing=0 cellpadding=1 width=98%>";
	echo "<thead><tr bgcolor=#FFFF99><td style=\"border:1px solid black;\" width=8%>Item Code</td><td style=\"border:1px solid black;\" width=20%>Item</td><td style=\"border:1px solid black;\" width=10%>Count Date</td><td style=\"border:1px solid black;\" width=5%>Last Count</td><td style=\"border:1px solid black;\" width=5%>Fills</td><td style=\"border:1px solid black;\" width=5%>Waste</td><td style=\"border:1px solid black;\" width=5%>Items Sold</td><td style=\"border:1px solid black;\" width=5%>OnHand</td><td style=\"border:1px solid black;\" width=5%>Cost</td><td style=\"border:1px solid black;\" width=5%>Total</td><td style=\"border:1px solid black;\" width=5%>Max</td><td style=\"border:1px solid black;\" width=5%>RO Pnt</td><td style=\"border:1px solid black;\" width=5%>RO Amt</td><td style=\"border:1px solid black;\" width=5%>ReOrder</td></tr></thead><tbody>";

	////download
	$download_data .= "Item Code,Item,Count Date,Last Count,Fills,Waste,Items Sold,OnHand,Cost,Total,Max,RO Pnt,RO Amt,ReOrder\r\n";

	foreach( $items AS $key => $value ) {
		////get item_code
		$complete_query = "SELECT id,item_code,max,reorder_point,reorder_amount,active FROM menu_items_new WHERE businessid = '$businessid' AND pos_id = '$key'";
		$result = Treat_DB_ProxyOldProcessHost::query( $complete_query );

		$item_id = mysql_result( $result, 0, "id" );
		$item_code = mysql_result( $result, 0, "item_code" );
		$max = mysql_result( $result, 0, "max" );
		$reorder_point = mysql_result( $result, 0, "reorder_point" );
		$reorder_amount = mysql_result( $result, 0, "reorder_amount" );
		$inactive = mysql_result( $result, 0, "active" );

		////get dates
		$dates = Kiosk::last_inv_dates( $machine_num, $item_code, 0 , $date2 );
		$date = $dates[1];
		if( $date == "" ) {
			$date = $dates[0];
		}

		/*
		///get mei name
		$query5 = "SELECT item_name FROM mei_product WHERE item_code = '$item_code'";
		$result5 = Treat_DB_ProxyOldProcessHost::query($query5);

		$mei_name = mysql_result($result5,0,"item_name");
		 * <td style=\"border:1px solid black;\">&nbsp;$mei_name</td>
		 */

		////cost
		$query5 = "SELECT cost FROM menu_items_price WHERE menu_item_id = $item_id";
		$result5 = Treat_DB_ProxyOldProcessHost::query( $query5 );

		$cost = @mysql_result( $result5, 0, "cost" );

		////last count
		$onhand2 = Kiosk::inv_onhand( $machine_num, $item_code, $date );

		////fills
		$fills = Kiosk::fills( $machine_num, $item_code, $date, $date2 );

		////waste
		$waste = Kiosk::waste( $machine_num, $item_code, $date, $date2 );

		////get check detail
		$items_sold = menu_items::items_sold( $key, $businessid, $date, $date2, $current_terminal );

		////sold in last 40 days
		$forty_sold = menu_items::items_sold( $key, $businessid, $forty_date, $date2,
				$current_terminal );

		$onhand = $onhand2 + $fills - $waste - $items_sold;

		/////REORDER
		if( $reorder_point == 0 && $max != 0 ) {
			$reorder = $max - $onhand;
		}
		elseif( $max != 0 ) {
			if( $onhand <= $reorder_point ) {
				if( $reorder_amount != 0 && $reorder_amount > ( $max - $onhand ) ) {
					$reorder = $reorder_amount;
				}
				else {
					$reorder = $max - $onhand;
				}
			}
			else {
				$reorder = "&nbsp;";
			}
		}
		else {
			$reorder = 0;
		}
		if( $reorder > $max ) {
			$reorder = $max;
		}
		if( $reorder <= 0 ) {
			$reorder = "&nbsp;";
		}

		if( $forty_sold == 0 ) {
			$none_sold = "<font color=red>*</font>";
			$none_sold2 = "*";
		}
		else {
			$none_sold = "";
			$none_sold2 = "";
		}
		
		if($inactive == 1){
			$none_sold .= "<font color=green>*</font>";
		}

		$report_total += $onhand * $cost;
		$total_cost = number_format( $onhand * $cost, 2 );
		$cost = number_format( $cost, 2 );

		echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor=''><td style=\"border:1px solid black;\">&nbsp;$item_code</td><td style=\"border:1px solid black;\"><a href=posMenu?bid=$businessid&cid=$companyid&id=$item_id&dir=1&report=$report&ordertype=$ordertype style=$style>$none_sold<font color = blue>$value</font></a></td><td style=\"border:1px solid black;\">&nbsp;$date</td><td style=\"border:1px solid black;\">&nbsp;$onhand2</td><td style=\"border:1px solid black;\">&nbsp;$fills</td><td style=\"border:1px solid black;\">&nbsp;$waste</td><td style=\"border:1px solid black;\">&nbsp;$items_sold</td><td style=\"border:1px solid black;\" bgcolor=#E8E7E7>&nbsp;$onhand</td><td style=\"border:1px solid black;\">&nbsp;$$cost</td><td style=\"border:1px solid black;\">&nbsp;$$total_cost</td><td style=\"border:1px solid black;\">&nbsp;$max</td><td style=\"border:1px solid black;\">&nbsp;$reorder_point</td><td style=\"border:1px solid black;\">&nbsp;$reorder_amount</td><td style=\"border:1px solid black;\">&nbsp;$reorder</td></tr>";

		///download
		$download_data .= "$item_code,\"$none_sold2$value\",$date,$onhand2,$fills,$waste,$items_sold,$onhand,$cost,$total_cost,$max,$reorder_point,$reorder_amount,$reorder\r\n";
	}
	echo "</tbody>";
	$report_total = number_format( $report_total, 2 );
	echo "<tfoot><tr bgcolor=#FFFF99><td colspan=9 style=\"border:1px solid black;\">&nbsp;</td><td colspan=1 style=\"border:1px solid black;\">&nbsp;$$report_total</td><td colspan=4 style=\"border:1px solid black;\">&nbsp;</td></tr></tfoot>";
	echo "</table>&nbsp;&nbsp;&nbsp;<font color=red>*</font>None Sold in last 40 Days.<font color=green>*</font>Inactive.<br>";

?>