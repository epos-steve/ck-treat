<?php
	require('lib/inc/functions-inventory.php');
	
	$t_date1 = "$t_date1 00:00:00";
	$t_date2 = "$t_date2 23:59:59";
	
	$inventoryItems = array();
	$inventoryItems = inventory_item_list($businessid);
	
	$salesAmt = sales($businessid, $t_date1, $t_date2);
	
	foreach($inventoryItems AS $id => $values){

		$invQtys[$id] = what_has_sold($businessid, $id, $t_date1, $t_date2);
		
		///batch items
		if($values['in_batch'] > 0){
			$temp = array();
			$temp = what_has_sold_batch($businessid, $id, $t_date1, $t_date2);
			
			foreach($temp AS $key => $value){
				$invQtys[$id][$key] += $value;
			}
		}
	}
	
	///////////
	///print///
	///////////
	
	echo "<table style=\"border:1px solid black;font-size:12px;background-color:white;\" cellspacing=0 cellpadding=1 width=98%>";
	
	$groupCogs = 0;
	$sheetCogs = 0;
	$lastGroup = "";
	
	foreach($inventoryItems AS $id => $values){
		$used = "";
		$showSold = "";
		$cogs = 0;
		
		foreach($invQtys[$id] AS $key => $sold){
			if($key == $values['rec_sizeid1']){
				$used = @round($sold / $values['rec_num1'] * $values['pack_qty'], 2);
				$showSold = "$sold {$values['rec_sizename1']}";
				$cogs = @round($values['item_cost'] / $values['rec_num1'] * $sold, 2);
			}
			elseif($key == $values['rec_sizeid2']){
				$used = @round($sold / $values['rec_num2'] * $values['pack_qty'], 2);
				$showSold = "$sold {$values['rec_sizename2']}";
				$cogs = @round($values['item_cost'] / $values['rec_num2'] * $sold, 2);
			}
			else{
				$used = "";
				$showSold = "";
				$cogs = 0;
			}
		}
		
		if($cogs != 0){
			$groupCogs += @round($cogs, 2);
			$fc = round($cogs / $salesAmt * 100, 2) . '%';
			$cogs = '$' . number_format($cogs, 2);
		}
		else{
			$fc = "";
			$cogs = "";
		}
		
		///display
		
		///group footer
		if($lastGroup != $values['acct_name'] && $lastGroup != ""){
			$groupCogsPercent = @round($groupCogs / $salesAmt * 100, 2);
			$groupCogs = number_format($groupCogs, 2);
			echo "<tr bgcolor=#CCCCCC>
					<td style=\"border:1px solid black;\" colspan=2><b>$lastGroup Totals</b></td>
					<td style=\"border:1px solid black;\" align=right><b>$$groupCogs</b></td>
					<td style=\"border:1px solid black;\" align=right><b>$groupCogsPercent%</b></td>
				</tr>";
				
			$sheetCogs += $groupCogs;
			$groupCogs = 0;
		}
		
		///group header
		if($lastGroup != $values['acct_name']){
			echo "<tr bgcolor=#FFFF99>
					<td style=\"border:1px solid black;\"><b>{$values['acct_name']}</b></td>
					<td style=\"border:1px solid black;\"><b>Sold</b></td>
					<td style=\"border:1px solid black;\"><b>COGS</b></td>
					<td style=\"border:1px solid black;\"><b>Food Cost</b></td>
				</tr>";
		}
		
		echo "<tr>
			<td style=\"border:1px solid black;\">{$values['item_name']}</td>
			<td style=\"border:1px solid black;\">$showSold</td>
			<td style=\"border:1px solid black;\" align=right>$cogs</td>
			<td style=\"border:1px solid black;\" align=right>$fc</td>
		</tr>";
		
		$lastGroup = $values['acct_name'];
	}
	
	///last group footer
	$groupCogsPercent = @round($groupCogs / $salesAmt * 100, 2);
	$groupCogs = number_format($groupCogs, 2);
	echo "<tr bgcolor=#CCCCCC>
			<td style=\"border:1px solid black;\" colspan=2><b>{$values['acct_name']} Totals</b></td>
			<td style=\"border:1px solid black;\" align=right><b>$$groupCogs</b></td>
			<td style=\"border:1px solid black;\" align=right><b>$groupCogsPercent%</b></td>
		</tr>";
		
	///sheet totals
	$sheetCogsPercent = @round($sheetCogs / $salesAmt * 100, 2);
	$sheetCogs = number_format($sheetCogs, 2);
	$salesAmt = number_format($salesAmt, 2);
	echo "<tr bgcolor=#AAAAAA>
			<td style=\"border:1px solid black;\" colspan=2><b>Sheet Totals (Sales: $$salesAmt)</b></td>
			<td style=\"border:1px solid black;\" align=right><b>$$sheetCogs</b></td>
			<td style=\"border:1px solid black;\" align=right><b>$sheetCogsPercent%</b></td>
		</tr>";
	
	echo "</table><p>";
?>