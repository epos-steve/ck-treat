<?php 
	echo "<table width=98%><tr valign=top><td width=25%>";
		echo "<table width=100%><tr bgcolor=#FFFF99><td style=\"border:1px solid black;\">Current Customers</td></tr>";
			echo "<tr bgcolor=white><td style=\"border:1px solid black;\">";
				echo "<table border=0 cellspacing=0 cellpadding=0 width=100%>";
					echo "<tr><td rowspan=2 style=\"font-size:70px;\" width=80%><center id=\"custCount\">0</center></td>";
					echo "<td style=\"font-size:18px;\"><center style=\"color:#B0B0B0;\" title='Last Week' id=\"cclw\">0</center></td></tr>";
					echo "<tr><td style=\"font-size:18px;\"><center style=\"color:#B0B0B0;\" title='Last Year' id=\"ccly\">0</center></td></tr>";
				echo "</table>";	
			echo "</td></tr></table>";
	echo "</td><td rowspan=2 width=75%>";

		echo "<table width=100%><tr bgcolor=#FFFF99><td width=10% style=\"border:1px solid black;\">Count</td><td width=60% style=\"border:1px solid black;\">Item</td><td width=15% style=\"border:1px solid black;\">Category Rank</td><td width=15% style=\"border:1px solid black;\">Overall Rank</td></tr>";

		for($counter = 1;$counter<=10;$counter++){
			echo "<tr id=\"$counter\" bgcolor=white>";
				echo "<td width=10% id=\"count$counter\" style=\"border:1px solid black;\"> - </td>
					<td width=60% id=\"item$counter\" style=\"border:1px solid black;\"> - </td>
					<td width=15% id=\"CR$counter\" style=\"border:1px solid black;\"> - </td>
					<td width=15% id=\"OR$counter\" style=\"border:1px solid black;\"> - </td>
				";
			echo "</tr>";
		}
		
		echo "</table>";
	
	echo "</td></tr>";
	
	echo "<tr valign=top><td>";
		echo "<table width=100%><tr bgcolor=#FFFF99><td style=\"border:1px solid black;\">Current Check Average</td></tr>";
		echo "<tr bgcolor=white><td style=\"border:1px solid black;font-size:70px;\">";
			echo "<table border=0 cellspacing=0 cellpadding=0 width=100%>";
				echo "<tr><td rowspan=2 style=\"font-size:70px;\" width=80%><center id=\"checkAvg\">$0.00</center></td>";
				echo "<td style=\"font-size:18px;\"><center style=\"color:#B0B0B0;\" title='Last Week' id=\"calw\">$0.00</center></td></tr>";
				echo "<tr><td style=\"font-size:18px;\"><center style=\"color:#B0B0B0;\" title='Last Year' id=\"caly\">$0.00</center></td></tr>";
			echo "</table>";
		echo "</td></tr></table>";
	echo "</td></tr>";
	
	echo "<tr><td colspan=2>";
		echo "<table width=100%><tr bgcolor=#FFFF99><td style=\"border:1px solid black;\" id=\"graphTitle\">History &nbsp;&nbsp;<div onclick=\"showTrendOptions();\" style=\"display:inline;float:right;\"><font color=blue size=2>&nbsp;&nbsp;TREND&nbsp;&nbsp;</font></div> <div onclick=\"searchItems();\" style=\"display:inline;float:right;\"><font color=blue size=2>&nbsp;&nbsp;SEARCH&nbsp;&nbsp;</font></div> &nbsp;&nbsp;</td></tr>";
		echo "<tr bgcolor=white><td style=\"border:1px solid black;\" height=300>";
			echo "<table id=\"graphMain\" cellspacing=0 cellpadding=0 height=300>";
			
				echo "<tr id=\"graph\" valign=bottom background='/assets/images/chartLine2.png'>";
				echo "</tr>";
				
				echo "<tr id=\"graphItem\" valign=bottom style=\"display:none;height:300px;\" background='/assets/images/chartLine.png'>";
				echo "</tr>";
				
				echo "<tr id=\"searchItem\" valign=bottom style=\"display:none;\">";
					echo "<td width=100% colspan=60>";
						echo "<div id=\"searchBox\" style=\"height:30px;width:100%;\">";
							echo "<input name=searchInput type=text size=50 id=\"searchInput\" onkeyup=\"filterList();\">&nbsp;<div style=\"display:inline;\" onclick=\"clearFilter();\"><font size=2 color=blue>CLEAR</font></div>";
						echo "</div>";
						echo "<div id=\"searchItems\" style=\"overflow:scroll;height:270px;\">";

						echo "</div>";
				echo "</td></tr>";
				
				echo "<tr id=\"trendData\" valign=bottom style=\"display:none;height:300px;\" background='/assets/images/chartLine.png'>";
				echo "</tr>";
				
				echo "<tr id=\"trendDetail\" valign=bottom style=\"display:none;\">";
					echo "<div style=\"width:100%;height:300px;overflow:scroll;display:none;\" id=\"trendDetailData\"></div>";
                                echo "</tr>";
				
				echo "<tr id=\"trendOptions\" valign=top style=\"display:none;height:300px;\">";
					echo "<td colspan=77 width=100%>";
						echo "<table><tr valign=top><td><div><i>Categories</i></div>";
							echo "<div class=\"trends\" style=\"width:100px;\" id=\"salesTrend\" onclick=\"setTrendOptions('currentTrend','sales');\">Sales</div>";
							echo "<div class=\"trends\" style=\"width:100px;\" id=\"cosTrend\" onclick=\"setTrendOptions('currentTrend','cos');\">Cost of Sales</div>";
							echo "<div class=\"trends\" style=\"width:100px;\" id=\"wasteTrend\" onclick=\"setTrendOptions('currentTrend','waste');\">Waste</div>";
							echo "<div class=\"trends\" style=\"width:100px;\" id=\"shrinkTrend\" onclick=\"setTrendOptions('currentTrend','shrink');\">Shrink</div>";
							echo "<div class=\"trends\" style=\"width:100px;\" id=\"customersTrend\" onclick=\"setTrendOptions('currentTrend','customers');\">Customers</div></td>";
						
						echo "<td>";
							echo "<div><i>Units</i></div>";
							echo "<div style=\"overflow:scroll;height:290px;\">";
							
							$query = "SELECT c.companyname, c.companyid, d.districtname, d.districtid, b.businessname, b.businessid
								FROM business b
								JOIN district d ON d.districtid = b.districtid
								JOIN company c ON c.companyid = b.companyid
								WHERE b.companyid =$companyid
								AND b.categoryid =4
								ORDER BY d.districtname, b.businessname";
							$result = Treat_DB_ProxyOldProcessHost::query( $query );
							
							$lastdistrictid = 0;
							$lastcompanyid = 0;
							
							while($r = mysql_fetch_array($result)){
								$unit_companyid = $r["companyid"];
								$unit_companyname = $r["companyname"];
								$unit_districtid = $r["districtid"];
								$unit_districtname = $r["districtname"];
								$unit_businessid = $r["businessid"];
								$unit_businessname = $r["businessname"];
								
								if($lastcompanyid != $unit_companyid){
									echo "<div class=\"units\" id=\"c-$unit_companyid\" onclick=\"setTrendOptions('currentTrendUnit','c-$unit_companyid');\">$unit_companyname</div>";
								}
								if($lastdistrictid != $unit_districtid){
									echo "<div class=\"units\" id=\"d-$unit_districtid\" onclick=\"setTrendOptions('currentTrendUnit','d-$unit_districtid');\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$unit_districtname</div>";
								}
								
								if($businessid == $unit_businessid){
									$background = "yellow";
								}
								else{
									$background = "white";
								}
								
								echo "<div class=\"units\" id=\"b-$unit_businessid\" onclick=\"setTrendOptions('currentTrendUnit','b-$unit_businessid');\" style=\"background:$background;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$unit_businessname</div>";
								$lastdistrictid = $unit_districtid;
								$lastcompanyid = $unit_companyid;
							}
							
							echo "</div>";
						echo "</td></tr></table>";
					echo "</td>";
				echo "</tr>";
				
			echo "</table>";			
		echo "</td></tr></table>";
	echo "</td></tr>";
	
	echo "<tr><td width=100% colspan=2>";
		echo "<table width=100%><tr bgcolor=#FFFF99><td style=\"border:1px solid black;\" width=10%>Rank</td><td style=\"border:1px solid black;\" width=45%>Item</td><td style=\"border:1px solid black;\" colspan=3 width=15%>Day Change</td><td style=\"border:1px solid black;\" colspan=3 width=15%>7 Day Change</td><td style=\"border:1px solid black;\" colspan=3 width=15%>30 Day Change</td></tr>";
	
		for($counter = 1;$counter<=10;$counter++){
			echo "<tr id=\"$counter\" bgcolor=white>";
				echo "<td id=\"mmRank$counter\" style=\"border:1px solid black;\" width=10%> - </td>
					<td id=\"mmItem$counter\" style=\"border:1px solid black;\" width=45%> - </td>
					<td id=\"mm1$counter\" style=\"border:1px solid black;\" width=5%> - </td>
					<td id=\"mm2$counter\" style=\"border:1px solid black;\" width=5%> - </td>
					<td id=\"mm2Percent$counter\" style=\"border:1px solid black;\" width=5%> - </td>
					<td id=\"mm3$counter\" style=\"border:1px solid black;\" width=5%> - </td>
					<td id=\"mm4$counter\" style=\"border:1px solid black;\" width=5%> - </td>
					<td id=\"mm4Percent$counter\" style=\"border:1px solid black;\" width=5%> - </td>
					<td id=\"mm5$counter\" style=\"border:1px solid black;\" width=5%> - </td>
					<td id=\"mm6$counter\" style=\"border:1px solid black;\" width=5%> - </td>
					<td id=\"mm6Percent$counter\" style=\"border:1px solid black;\" width=5%> - </td>
				";
			echo "</tr>";
		}
		echo "<tr bgcolor=\"white\"><td id=\"ticker\" style=\"border:1px solid black;\" colspan=11>&nbsp;</td></tr>";
		echo "</table>";
	echo "</td></tr>";
	
	echo "<tr valign=top><td height=50 colspan=2>";
		echo "<center id=\"startContent\"><input type=button name=start id='start' value='Start Tracking' onclick=\"realtimeUpdate();\"></center>";
	echo "</td></tr>";
	
	echo "</table>";
	
	echo "<input type=hidden name=currentGraph id=\"currentGraph\" value=0>";
	echo "<input type=hidden name=currentStock id=\"currentStock\" value=1>";
	echo "<input type=hidden name=graphStatus id=\"graphStatus\" value=1>";
	echo "<input type=hidden name=tickerData id=\"tickerData\" value=''>";
	echo "<input type=hidden name=currentTrend id=\"currentTrend\" value='sales'>";
	echo "<input type=hidden name=currentTrendUnit id=\"currentTrendUnit\" value='b-$businessid'>";

	$download_disable = "DISABLED";
	
?>
