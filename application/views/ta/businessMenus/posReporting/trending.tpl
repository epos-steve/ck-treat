<?php
	$date1 = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d")-113, date("Y")));
	while(dayofweek($date1) != "Sunday"){$date1 = prevday($date1);}
	$date1 .= " 00:00:00";
	
	$date2 = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d")-1, date("Y")));
	while(dayofweek($date2) != "Saturday"){$date2 = prevday($date2);}
	$date2 .= " 23:59:59";
	
	$query = "SELECT mio.name AS ordertype, mic.Id, mic.Name, SUM(cd.quantity) as total, CONCAT(YEAR(c.bill_datetime), '-', WEEK(c.bill_datetime)) AS kTime FROM checks c
		JOIN checkdetail cd ON cd.businessid = c.businessid AND cd.bill_number = c.check_number
		JOIN menu_items_new min ON min.pos_id = cd.item_number AND min.businessid = cd.businessid
		LEFT JOIN menu_items_master mim ON mim.UPC = min.upc
		LEFT JOIN menu_items_category mic ON mic.Id = mim.Category
		JOIN menu_items_ordertype mio ON mio.id = min.ordertype
		WHERE c.businessid = $businessid AND c.bill_datetime BETWEEN '$date1' AND '$date2'
		GROUP BY mic.Id, kTime
		order by min.ordertype, mic.Name, kTime";
    $result = Treat_DB_ProxyOldProcessHost::query( $query );
	
	while($r = mysql_fetch_array($result)){
		$ordertypes[$r["ordertype"]] += $r["total"];
		$category[$r["ordertype"]][$r["Id"]] = $r["Name"];
		if($r["Id"] == ''){
			$category[0][0] = "Unknown";
		}
		$amt[$r["Id"]][$r["kTime"]] = $r["total"];
		$kTime[$r["kTime"]] = $r["kTime"];
	}
	
	ksort($kTime);
	$kTimeSeq = array_values($kTime);
	
	///display
	foreach($ordertypes AS $ordertype => $active){
		?>
		<table class="sortable" style="border:1px solid black;background-color:white;font-size:12px;" cellspacing=0 cellpadding=1 width=98%>
		<thead>
			<tr bgcolor=#FFFF99>
				<td style="border:1px solid black;" width=15%><?php echo $ordertype; ?> Category</td>
				<td style="border:1px solid black;" width=10%>Category%</td>
				<td style="border:1px solid black;" width=10%>Overall%</td>
				<?php
					$counter = 0;
					foreach($kTime AS $key => $value){
						if($counter > 3){
							$key = explode("-", $key);
							echo "<td style=\"border:1px solid black;\">$key[0] Week $key[1]</td>";
						}
						$counter++;
					}
				?>
				<td style="border:1px solid black;">Total</td>
			</tr>
			</thead>
			<tbody>
		<?php
		$catTotal = array();
		foreach($category[$ordertype] AS $catId => $catName){
			
			////category percents
			$catPercent = round(array_sum($amt[$catId]) / $ordertypes[$ordertype] * 100, 1);
			$totalPercent = round(array_sum($amt[$catId]) / array_sum($ordertypes) * 100, 1);
			
			echo "<tr><td style=\"border:1px solid black;\">$catName</td>
				<td style=\"border:1px solid black;\">$catPercent%</td>
				<td style=\"border:1px solid black;\">$totalPercent%</td>";
			
			$counter = 0;
			$amount = 0;
			$startAmt = 0;
			foreach($kTime AS $key => $value){
				$remove = $kTimeSeq[$counter-4];
				$amount += $amt[$catId][$key] - $amt[$catId][$remove];
				$catTotal[$key] += $amount;
				
				if($counter > 3){	
					if($counter == 4){$startAmt = $amount;}
					
					$change = round(($amount - $lastTotal) / $lastTotal * 100, 1);
					
					if($change > 0){
						$showSign = "+";
					}
					else{
						$showSign = "";
					}

					////trend colors
					if($catPercent >= 3){
						if($change >= 20){
							$color = "green";
						}
						elseif($change <= -20){
							$color = "red";
						}
						else{
							$color = "black";
						}

						if($change >= 30 || $change <= -30){
							$bold = "<b>";
							$unbold = "</b>";
						}
						else{
							$bold = "";
							$unbold = "";
						}
					}
					else{
						$color = "black";
						$bold = "";
						$unbold = "";
					}

					echo "<td style=\"border:1px solid black;\">$bold<font color=$color>$showSign$change%</font>$unbold</td>";	
				}
				$lastTotal = $amount;
				$counter++;
			}
			///overall change
			$change = round(($amount - $startAmt) / $startAmt * 100, 1);
			if($change > 0){
				$showSign = "+";
			}
			else{
				$showSign = "";
			}
			echo "<td style=\"border:1px solid black;\" bgcolor=#FFFF99>$showSign$change%</td></tr>";	
		}
		echo "</tbody><tfoot><tr bgcolor=#FFFF99><td colspan=3 style=\"border:1px solid black;\">&nbsp;</td>";
		
		$counter = 0;
		foreach($kTime AS $key => $value){
			if($counter > 3){
				$change = round(($catTotal[$key] - $lastTotal) / $lastTotal * 100, 1);
				if($change > 0){
					$showSign = "+";
					$color = "green";
				}
				elseif($change == 0){
					$showSign = "";
					$color = "black";
				}
				else{
					$showSign = "";
					$color = "red";
				}
				echo "<td style=\"border:1px solid black;\"><font color=$color>$showSign$change%</font></td>";	
			}
			$lastTotal = $catTotal[$key];
			$counter++;
		}
		
		echo "<td style=\"border:1px solid black;\">&nbsp;</td></tr></tfoot></table><p>";
	}

?>