<?php
	$query = "SELECT cdisc.*, p.id AS promo_id, p.name AS promo_name, mt.percent
            FROM checkdiscounts cdisc
            JOIN checks c ON c.check_number = cdisc.check_number
            AND c.businessid = cdisc.businessid
            AND c.bill_datetime
            BETWEEN  '$t_date1 00:00:00'
            AND  '$t_date2 23:59:59'
            JOIN promotions p ON p.businessid = cdisc.businessid
            AND p.pos_id = cdisc.pos_id
            AND p.subsidy =1
            AND p.promo_trigger IN ( 1, 2, 3, 4, 5, 6 )
            LEFT JOIN menu_tax mt ON mt.id = p.tax_id
            WHERE cdisc.businessid =$businessid
            ORDER BY cdisc.check_number, promo_id, cdisc.amount ASC";
	$result = Treat_DB_ProxyOldProcessHost::query( $query );

	$detail_counter = 0;
	$subsidy_items = array();
	$promos = array();
	$lastchecknum = 0;
	$lastpromo_id = 0;

	echo "<table class=\"sortable\" style=\"border:1px solid black;font-size:12px;background-color:white;\" cellspacing=0 cellpadding=1 width=98%>";
	echo "<thead><tr bgcolor=#FFFF99><td style=\"border:1px solid black;\">&nbsp;Item Code</td>
				<td style=\"border:1px solid black;\">&nbsp;Item Name</td>
				<td align=right style=\"border:1px solid black;\">&nbsp;Quantity</td>
                                <td align=right style=\"border:1px solid black;\">&nbsp;Price</td>
                                <td align=right style=\"border:1px solid black;\">&nbsp;Subsidy Amount</td>
                                <td align=right style=\"border:1px solid black;\">&nbsp;Extended Subsidy Amount</td>
                                <td align=right style=\"border:1px solid black;\">&nbsp;Extended Tax Amount</td>
                                <td align=right style=\"border:1px solid black;\">&nbsp;Total</td>
			</tr></thead><tbody>";
        $download_data = "Item Code, Item Name, Quantity, Price, Subsidy Amount, Extended Subsidy Amount, Extended Tax Amount, Total\r\n";

	while($r = mysql_fetch_array($result)){
		$check_number = $r["check_number"];
                $pos_id = $r["pos_id"];
                $amount = $r["amount"] * -1;
                $promo_id = $r["promo_id"];
                $promo_name = $r["promo_name"];
                $promo_tax = $r["percent"];
                
                $promos[$promo_id] = $promo_name;

                if($lastchecknum == $check_number && $lastpromo_id == $promo_id){
                    $detail_counter++;
                }
                else{
                    $detail_counter = 0;
                }
                
                $query2 = "SELECT cd.item_number,
                                SUM(cd.quantity) AS quantity,
                                cd.cost,
                                cd.price,
                                min.name, 
                                min.item_code, 
                                min.id AS min_id,
                                IF(pd.promo_id > 0, 1, 0) AS in_promo,
                                mt.percent
                            FROM checkdetail cd
                            JOIN menu_items_new min ON min.pos_id = cd.item_number AND min.businessid = cd.businessid
                            LEFT JOIN promo_detail pd ON pd.id = cd.item_number AND pd.promo_id = $promo_id
                            LEFT JOIN menu_item_tax mit ON mit.menu_itemid = min.id
                            LEFT JOIN menu_tax mt ON mt.id = mit.tax_id
                            WHERE cd.bill_number = '$check_number' AND cd.businessid = $businessid
                            GROUP BY cd.bill_number, cd.item_number 
                            ORDER BY in_promo DESC, quantity DESC, cd.price DESC, cd.id
                            LIMIT $detail_counter,1";
                $result2 = Treat_DB_ProxyOldProcessHost::query( $query2 );

                $item_number = mysql_result($result2,0,"item_number");
                $quantity = mysql_result($result2,0,"quantity");
                $cost = mysql_result($result2,0,"cost");
                $price = mysql_result($result2,0,"price");
                $name = mysql_result($result2,0,"name");
                $min_id = mysql_result($result2,0,"min_id");
                $item_code = mysql_result($result2,0,"item_code");
                $item_tax = mysql_result($result2,0,"percent");
				$in_promo = mysql_result($result2,0,"in_promo");
                
				if ($in_promo > 0){
					$subsidy_items[$min_id]["name"] = $name;
					$subsidy_items[$min_id]["item_code"] = $item_code;
					$subsidy_items[$min_id]["qty"] += $quantity;
					$subsidy_items[$min_id]["cost"] += $cost * $quantity;
					$subsidy_items[$min_id]["price"] += $price * $quantity;
					$subsidy_items[$min_id]["amount"] += $amount;
					$subsidy_items[$min_id]["item_tax"] = $item_tax;
					$subsidy_items[$min_id]["promo_tax"] = $promo_tax;
				}

                $lastchecknum = $check_number;
                $lastpromo_id = $promo_id;
                
	}
        
        ///display results
        $sum = 0;
        foreach($subsidy_items AS $min_id => $value){
                     
                $item_code = $subsidy_items[$min_id]["item_code"];
                $item_name = $subsidy_items[$min_id]["name"];
                $qty = $subsidy_items[$min_id]["qty"];
                $cost = number_format($subsidy_items[$min_id]["cost"] / $subsidy_items[$min_id]["qty"], 2);
                $price = number_format($subsidy_items[$min_id]["price"] / $subsidy_items[$min_id]["qty"], 2);
                $amount = number_format($subsidy_items[$min_id]["amount"], 2);
                $item_tax = number_format($subsidy_items[$min_id]["item_tax"], 2);
                $promo_tax = number_format($subsidy_items[$min_id]["promo_tax"], 2);
                
                $sub_amount = number_format($subsidy_items[$min_id]["amount"] / $subsidy_items[$min_id]["qty"], 2);
                
                if($promo_tax > 0){
                    $taxes = ($sub_amount * $qty) * ($promo_tax / 100);
                }
                else{
                    $taxes = 0;
                }
                $taxes = number_format($taxes, 2);
                
                $row_total = number_format($taxes + $amount, 2);
                
                $sum += $amount;
                $tax_sum += $taxes;
                $qty_sum += $qty;
                $total += $row_total;
            
                echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor=''>
                        <td style=\"border:1px solid black;\">$item_code</td>
                        <td style=\"border:1px solid black;\">$item_name</td>
                        <td align=right style=\"border:1px solid black;\">$qty</td>
                        <td align=right style=\"border:1px solid black;\">$$price</td>
                        <td align=right style=\"border:1px solid black;\">$$sub_amount</td>
                        <td align=right style=\"border:1px solid black;\">$$amount</td>
                        <td align=right style=\"border:1px solid black;\">$$taxes</td>
                        <td align=right style=\"border:1px solid black;\">$$row_total</td></tr>";
                $download_data .= "$item_code,\"$item_name\",$qty,$price,$sub_amount,$amount,$taxes,$row_total\r\n";
        }
        $sum = number_format($sum, 2);
        $tax_sum = number_format($tax_sum, 2);
        $total = number_format($total, 2);
	echo "</tbody>
                <tfoot>
                <tr bgcolor=#FFFF99>
                <td colspan=2 align=right style=\"border:1px solid black;\">&nbsp;</td>
                <td align=right style=\"border:1px solid black;\">$qty_sum</td>
                <td colspan=2 align=right style=\"border:1px solid black;\">&nbsp;</td>
                <td align=right style=\"border:1px solid black;\">$$sum</td>
                <td align=right style=\"border:1px solid black;\">$$tax_sum</td>
                <td align=right style=\"border:1px solid black;\">$$total</td>
                </tr></tfoot></table><p>";

?>