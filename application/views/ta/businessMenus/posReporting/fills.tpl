<?php
	$date = $t_date1;
	$today = $t_date2;
	$date2 = date( "Y-m-d" );
	
	echo "<table class=\"sortable\" style=\"border:1px solid black;font-size:12px;background-color:white;\" cellspacing=0 cellpadding=1 width=98%>";
	echo "<thead><tr bgcolor=#FFFF99><td style=\"border:1px solid black;\" width=25%>Item Code</td><td style=\"border:1px solid black;\" width=25%>Item</td><td style=\"border:1px solid black;\" width=25%>Qty</td><td style=\"border:1px solid black;\" width=25%>Amount</td></thead><tbody>";

	$download_data .= "Item Code,Item,Qty,Cost\r\n";
	
	$totalAmt = 0;
	$totalQty = 0;
	
	if($ordertype > 0){
		$havingQry = "AND min.ordertype = $ordertype";
	}
	else{
		$havingQry = "";
	}
	
	$machines = implode("','",$machine_num);
	
	$query = "SELECT icv.item_code, min.id, min.name, min.ordertype, SUM( icv.fills * icv.unit_cost ) AS amount, SUM( icv.fills ) AS qty
			FROM inv_count_vend icv
			LEFT JOIN menu_items_new min ON min.item_code = icv.item_code
			AND min.businessid = $businessid
			WHERE icv.machine_num
			IN ('$machines') AND icv.date_time BETWEEN  '$date 00:00:00' AND  '$today 23:59:59'
			AND icv.is_inv = 0
			GROUP BY icv.item_code
			HAVING qty != 0 $havingQry
			ORDER BY icv.item_code";
	$result = Treat_DB_ProxyOldProcessHost::query( $query );
	
	while($r = mysql_fetch_array($result)){
		$item_code = $r["item_code"];
		$name = $r["name"];
		$mid = $r["id"];
		$qty = $r["qty"];
		$amount = $r["amount"];
		
		$qty = round($qty);
		
		////if amount is zero look up cost in menu_items_price
		if($amount == 0){
			$query2 = "SELECT cost FROM menu_items_price WHERE menu_item_id = $mid";
			$result2 = Treat_DB_ProxyOldProcessHost::query( $query2 );
			
			$cost = @mysql_result($result2,0,"cost");
			
			$amount = round($qty * $cost, 2);
		}
		
		$totalQty += $qty;
		$totalAmt += $amount;
		
		$amount = number_format($amount, 2);
		
		echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor=''><td style=\"border:1px solid black;\">&nbsp;$item_code</td><td style=\"border:1px solid black;\">&nbsp;$name</td><td style=\"border:1px solid black;\" align=right>$qty&nbsp;</td><td style=\"border:1px solid black;\" align=right>$$amount&nbsp;</td></tr>";
		$download_data .= "$item_code,\"$name\",$qty,$amount\r\n";
	}
	
	echo "</tbody><tfoot><tr bgcolor=#FFFF99><td style=\"border:1px solid black;\" colspan=2>&nbsp;Totals</td><td style=\"border:1px solid black;\" align=right>$totalQty&nbsp;</td><td style=\"border:1px solid black;\" align=right>$$totalAmt&nbsp;</td></tr></tfoot></table>";
?>