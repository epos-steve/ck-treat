<?php 

	$date_1 = $t_date1 . " 00:00:00";
	$date_2 = $t_date2 . " 23:59:59";
	
	$t_date1 = strtotime("$t_date1 00:00:00");
	$t_date2 = strtotime("$t_date2 23:59:59");
	
	$promo_tax = 0;

	// $query = "SELECT menu_items_new.* FROM menu_items_new WHERE businessid = $businessid";
	$query = "SELECT m1.pos_id, m1.item_code, m1.name, count(item_number) AS 'tot_qty', sum(c1.taxes) AS 'tot_tax', sum(price) AS 'tot_sales', c1.item_number 
				FROM menu_items_new m1 
				join checkdetail c1 on m1.pos_id = c1.item_number AND c1.businessid = m1.businessid 
				join checks c2 on c1.bill_number = c2.check_number AND c1.businessid = c2.businessid 
				WHERE m1.businessid = $businessid 
				and c2.bill_posted between '$date_1' and '$date_2' 
				group by m1.pos_id";

	$result = Treat_DB_ProxyOldProcessHost::query( $query );

	echo "<table class=\"sortable\" style=\"border:1px solid black;font-size:12px;background-color:white;\" cellspacing=0 cellpadding=1 width=98%>";
	echo "<thead><tr bgcolor=#FFFF99><td style=\"border:1px solid black;\" colspan=5>&nbsp;<b>Item Tax Report is only available on the New Software Platform</b></td></tr>
				<tr bgcolor=#FFFF99><td style=\"border:1px solid black;\">&nbsp;Item Code</td>
				<td style=\"border:1px solid black;\">&nbsp;Item Name</td>
				<td style=\"border:1px solid black;text-align:right;\">Items Sold&nbsp;</td>
				<td style=\"border:1px solid black;text-align:right;\">Total Tax&nbsp;</td>
				<td style=\"border:1px solid black;text-align:right;\">Gross Sales&nbsp;</td>
			</tr></thead><tbody>";

	$download_data .= "Item Code,Item Name, Items Sold, Total Tax, Gross Sales\r\n";

	/////terminal no
	// $query2 = "USE mburris_manage";
	// $result2 = Treat_DB_ProxyOld::query_both( $query2 );

	// $query2 = "SELECT client_programid FROM client_programs WHERE businessid = $businessid";
	// $result2 = Treat_DB_ProxyOld::query( $query2 );

	// $terminal_no = "";

	// while($r2 = mysql_fetch_array($result2)){
	// 	if($terminal_no == ""){$terminal_no = $r2["client_programid"];}
	// 	else{$terminal_no .= ", " . $r2["client_programid"];}
	// }

	// $query2 = "USE vivipos_order";
	// $result2 = Treat_DB_ProxyOldViviposOrder::query_both( $query2 );

	while($r = mysql_fetch_array($result)){
		$pos_id = $r["pos_id"];
		$item_name = $r["name"];
		$item_code = $r["item_code"];

		// $query2 = "SELECT SUM( ROUND(order_items.current_subtotal,2) ) AS tot_sales,
		// 				SUM( ROUND(order_items.current_tax,2) ) AS tot_tax,
		// 				SUM(order_items.current_qty) AS tot_qty
		// 				FROM order_items
		// 				JOIN orders ON orders.id = order_items.order_id AND orders.terminal_no IN ($terminal_no) AND order_items.modified BETWEEN '$t_date1' AND '$t_date2'
		// 				WHERE order_items.product_no = '$pos_id'
		// 				GROUP BY order_items.product_no
		// 				ORDER BY order_items.cate_name,order_items.product_name";
		// $result2 = Treat_DB_ProxyOldViviposOrder::query( $query2 );

		// $tot_sales = @mysql_result($result2,0,"tot_sales");
		// $tot_tax = @mysql_result($result2,0,"tot_tax");
		// $tot_qty = @mysql_result($result2,0,"tot_qty");
		
		$tot_sales = $r["tot_sales"];
		$tot_tax = $r["tot_tax"];
		$tot_qty = $r["tot_qty"];

		$total_sales += $tot_sales + $tot_tax;
		$total_taxes += $tot_tax;
		$total_qty += $tot_qty;

		$total = number_format($tot_sales + $tot_tax,2);

		$tot_tax = number_format($tot_tax,2);

		if($tot_sales > 0){
			echo "<tr><td style=\"border:1px solid black;\">&nbsp;$item_code</td>
				<td style=\"border:1px solid black;\">&nbsp;$item_name</td>
				<td style=\"border:1px solid black;text-align:right;\">$tot_qty&nbsp;</td>
				<td style=\"border:1px solid black;text-align:right;\">$$tot_tax&nbsp;</td>
				<td style=\"border:1px solid black;text-align:right;\">$$total&nbsp;</td>
			</tr>";

			$download_data .= "$item_code,\"$item_name\",$tot_qty,$tot_tax,$total\r\n";
		}
	}

	////promotion taxes
	// $query2 = "SELECT SUM(order_promotions.current_tax) * -1 AS promo_tax
	// 			FROM order_promotions
	// 			JOIN orders ON orders.id = order_promotions.order_id
	// 			WHERE orders.terminal_no IN ($terminal_no) AND orders.modified BETWEEN '$t_date1' AND '$t_date2'";
	// $result2 = Treat_DB_ProxyOldViviposOrder::query( $query2 );

	// $promo_tax = @mysql_result($result2,0,"promo_tax");

	echo "</tbody><tfoot>";
		if($promo_tax != 0){
			$promo_tax = number_format($promo_tax,2);
			echo "<tr><td style=\"border:1px solid black;\">&nbsp;</td>
				<td style=\"border:1px solid black;\">&nbsp;Promotion Tax Adjustment</td>
				<td style=\"border:1px solid black;text-align:right;\">&nbsp;</td>
				<td style=\"border:1px solid black;text-align:right;\">$$promo_tax&nbsp;</td>
				<td style=\"border:1px solid black;text-align:right;\">&nbsp;</td>
			</tr>";

			$download_data .= ",\"Promotion Tax Adjustment\",,\"$promo_tax\",\r\n";

			$total_taxes += $promo_tax;
			$total_sales += $promo_tax;
		}

	$total_taxes = number_format($total_taxes,2);
	$total_sales = number_format($total_sales,2);

	echo "<tr bgcolor=#FFFF99><td style=\"border:1px solid black;\">&nbsp;</td>
				<td style=\"border:1px solid black;\">&nbsp;</td>
				<td style=\"border:1px solid black;text-align:right;\">$total_qty&nbsp;</td>
				<td style=\"border:1px solid black;text-align:right;\">$$total_taxes&nbsp;</td>
				<td style=\"border:1px solid black;text-align:right;\">$$total_sales&nbsp;</td>
			</tr></tfoot>";

	echo "</table><p>";

	// $query2 = "USE ".Treat_Config::singleton()->get('db', 'database');
	// $result2 = Treat_DB_ProxyOld::query_both( $query2 );

?>