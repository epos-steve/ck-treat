<?php
	$totalcost = 0;
	$totalprice = 0;
	echo "<table class=\"sortable\" style=\"border:1px solid black;font-size:12px;background-color:white;\" cellspacing=0 cellpadding=1 width=98%>";
	echo "<thead><tr bgcolor=#FFFF99><td style=\"border:1px solid black;\" width=8%>Item Code</td><td style=\"border:1px solid black;\" width=20%>Item</td><td style=\"border:1px solid black;\" width=10%>Count1 Date</td><td style=\"border:1px solid black;\" width=6%>Count1</td><td style=\"border:1px solid black;\" width=6%>Fills</td><td style=\"border:1px solid black;\" width=6%>Waste</td><td style=\"border:1px solid black;\" width=10%>Count2 Date</td><td style=\"border:1px solid black;\" width=6%>Count2</td><td style=\"border:1px solid black;\" width=6%>Count +/-</td><td style=\"border:1px solid black;\" width=6%>Items Sold</td><td style=\"border:1px solid black;\" width=4%>+/-</td><td style=\"border:1px solid black;\" width=%>Cost</td><td style=\"border:1px solid black;\" width=%>Price</td></tr></thead><tbody>";
	$download_data .= "Item Code,Item,Count1 Date,Count1,Fills,Waste,Count2 Date,Count2,Count +/-,Items Sold,+/-,Cost,Price\r\n";
	foreach( $items AS $key => $value ) {
		////get item_code
		$complete_query = "SELECT item_code,id FROM menu_items_new WHERE businessid = '$businessid' AND pos_id = '$key'";
		$result = Treat_DB_ProxyOldProcessHost::query( $complete_query );

		$item_code = mysql_result( $result, 0, "item_code" );
		$id = mysql_result( $result, 0, "id" );

		///get item price
		$complete_query = "SELECT price FROM menu_items_price WHERE menu_item_id = '$id'";
		$result = Treat_DB_ProxyOldProcessHost::query( $complete_query );

		$price = mysql_result( $result, 0, "price" );

		////get dates
		$dates = Kiosk::last_inv_dates( $machine_num, $item_code, $which );
		$date1 = $dates[0];
		$date2 = $dates[1];

		////get onhand qty's
		if ($date1 != ''){
			$onhand1 = Kiosk::inv_onhand( $machine_num, $item_code, $date1 );
		} else {
			$onhand1 = '';
		}

		if ($date2 != ''){

		$onhand2 = Kiosk::inv_onhand( $machine_num, $item_code, $date2, 1 );
		$fills = Kiosk::fills( $machine_num, $item_code, $date1, $date2 );

		////waste
		$waste = Kiosk::waste( $machine_num, $item_code, $date1, $date2 );

		////get check detail
		$items_sold = menu_items::items_sold( $key, $businessid, $date1, $date2, $current_terminal );
		} else {
			$onhand2 = '';
			$fills = '';
			$waste = '';
			$items_sold = '';
		}
		///cost
		$cost = Kiosk::cost( $machine_num, $item_code );

		$diff1 = $onhand1 + $fills - $waste - $onhand2;
		$diff2 = $items_sold - $diff1;

		$totalcost += $cost * $diff2;
		$totalprice += $price * $diff2;
		$cost = number_format( $cost * $diff2, 2 );
		$price = number_format( $price * $diff2, 2 );

		if( $diff2 == 0 ) {
			$color = "#00FF00";
		}
		elseif( abs( $diff2 ) > 0 && abs( $diff2 ) <= 3 ) {
			$color = "yellow";
		}
		else {
			$color = "red";
		}

		$onclick = $sec_bus_menu_pos_report > 1 ? "onClick='open_dialog(\"$item_code\", \""
												. htmlentities( $value, ENT_QUOTES )
												. "\", \"$machine_num\")'" : "";
		echo "
			<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor='' $onclick>
				<td style=\"border:1px solid black;\">&nbsp;$item_code</td>
				<td style=\"border:1px solid black;\">&nbsp;$value</td>
				<td style=\"border:1px solid black;\">&nbsp;$date1</td>
				<td style=\"border:1px solid black;\">&nbsp;$onhand1</td>
				<td style=\"border:1px solid black;\">&nbsp;$fills</td>
				<td style=\"border:1px solid black;\">&nbsp;$waste</td>
				<td style=\"border:1px solid black;\">&nbsp;$date2</td>
				<td style=\"border:1px solid black;\">&nbsp;$onhand2</td>
				<td style=\"border:1px solid black;\">&nbsp;$diff1</td>
				<td style=\"border:1px solid black;\">&nbsp;$items_sold</td>
				<td style=\"border:1px solid black;\" bgcolor=$color>&nbsp;$diff2</td>
				<td style=\"border:1px solid black;\" bgcolor=$color>&nbsp;$cost</td>
				<td style=\"border:1px solid black;\" bgcolor=$color>&nbsp;$price</td>
			</tr>
		";
		$download_data .= "$item_code,\"$value\",$date1,$onhand1,$fills,$waste,$date2,$onhand2,$diff1,$items_sold,$diff2,$cost,$price\r\n";
	}
	$totalcost = number_format( $totalcost, 2 );
	$totalprice = number_format( $totalprice, 2 );
	echo "</tbody><tfoot><tr bgcolor=#FFFF99><td style=\"border:1px solid black;\" colspan=11>&nbsp;</td><td style=\"border:1px solid black;\" title='Cost Variance'><center><b>$$totalcost</b></center></td><td style=\"border:1px solid black;\" title='Price Variance'><center><b>$$totalprice</b></center></td></tr></tfoot></table><br>";

?>
