<?php
	///date range
	$time_array = array();
	$date_array = array();
	
	///daily
	if($t_period == 1){
		$temp_date = $t_date1;
		while($temp_date <= $t_date2){
			$date_array[] = $temp_date;
			$temp_date = nextday($temp_date);	
		}
	}
	///weekly
	elseif($t_period == 2){
		$query2 = "SELECT week_end,week_start FROM company WHERE companyid = $companyid";
		$result2 = Treat_DB_ProxyOldProcessHost::query( $query2 );

		$week_end = @mysql_result($result2,0,"week_end");
		$week_start = @mysql_result($result2,0,"week_start");

		$temp_date = $t_date1;
		while($temp_date <= $t_date2){
			if(dayofweek($temp_date) == $week_end || $temp_date == $t_date2){
				$date_array[] = $temp_date;
			}
			$temp_date = nextday($temp_date);
		}
	}
	///monthly
	elseif($t_period == 3){
		$temp_date = $t_date1;
		while($temp_date <= $t_date2){
			if((substr($temp_date,8,2) == "01" && $temp_date != $t_date1) || $temp_date == $t_date2){
				if(substr($temp_date,8,2) == "01"){
					$date_array[] = prevday($temp_date);
				}
				else{
					$date_array[] = $temp_date;
				}
			}
			$temp_date = nextday($temp_date);
		}
	}
	////day of the week
	elseif($t_period == 4){
		$date_array = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
	}
	///by date range
	else{
		$date_array[] = $t_date2;
	}

	///time periods
	$hCounter = 0;
	$mCounter = 0;
	
	if($t_period2 == 1){
		$mCount = 30;
	}
	elseif($t_period2 == 2){
		$mCount = 15;
	}
	else{
		$mCount = 60;
	}

	while($hCounter < 24){
		if(strlen($hCounter) < 2){
			$hCounter = "0$hCounter";
		}
		if(strlen($mCounter) < 2){
			$mCounter = "0$mCounter";
		}

		$time_array[] = "$hCounter:$mCounter";

		$mCounter += $mCount;
		
		if($mCounter >= 60){
			$mCounter = 0;
			$hCounter++;
		}
	}
	
	$date_array_len = count($date_array);
	$time_array_len = count($time_array);

	///build master array
	foreach($date_array AS $key => $value){
		foreach($time_array AS $key2 => $value2){
			$sales_array[$value][$value2] = 0;
			$cust_array[$value][$value2] = 0;
		}
	}
	
	if($current_terminal > 0){
		$add_query = "AND c.terminal_group = $current_terminal";
	}
	else{
		$add_query = "";
	}
	
	////get check data
	$query = "SELECT c.bill_datetime, SUM( cd.price * cd.quantity ) AS total, count(c.id) as people
				FROM checks c
				JOIN checkdetail cd ON cd.bill_number = c.check_number
				AND cd.businessid = c.businessid
				WHERE c.businessid = $businessid
				AND c.bill_datetime
				BETWEEN '$t_date1 00:00:00'
				AND '$t_date2 23:59:59'
				$add_query
				GROUP BY c.bill_datetime
				ORDER BY c.bill_datetime";
	$result = Treat_DB_ProxyOldProcessHost::query( $query );

	///fill array from result
	while($r = mysql_fetch_array($result)){
		$bill_datetime = $r["bill_datetime"];
		$total = $r["total"];
		$people = $r["people"];
		
		$cDate = substr($bill_datetime, 0, 10);
		$cTime = substr($bill_datetime, 11, 5);
		$cDay = dayofweek($cDate);
		
		$tempa = 0;
		while($tempa <= $date_array_len){
			if(($cDate <= $date_array[$tempa] && $t_period != 4) || ($cDay == $date_array[$tempa] && $t_period == 4)){
				$key1 = $date_array[$tempa];
				
				$tempb = $time_array_len - 1;
				while($tempb >= 0){
					if($cTime >= $time_array[$tempb]){
						$key2 = $time_array[$tempb];
						
						$sales_array[$key1][$key2] += $total;
						$people_array[$key1][$key2] += $people;
						
						continue 3;
					}
					$tempb--;
				}
			}
			$tempa++;
		}
	}
	
	///display data
	?>
	<table class="sortable" style="border:1px solid black;background-color:white;font-size:12px;" cellspacing=0 cellpadding=1 width=98%>
		<thead>
			<tr bgcolor=#FFFF99>
				<td style="border:1px solid black;">Period</td>
				<?php
				$download_data .= "Period";
				
				foreach($date_array AS $key => $value){
					echo "<td style=\"border:1px solid black;\">Sales $value</td>
						<td style=\"border:1px solid black;\">Cust $value</td>
						<td style=\"border:1px solid black;\">Avg$ $value</td>
						<td style=\"border:1px solid black;\">%</td>";
					$download_data .= ",Sales $value, Cust $value, Avg$, %";
				}
				$download_data .= "\r\n";
				?>
			</tr>
        </thead>
        <tbody>
			<?php
			$highestSales = 0;
			
			foreach($time_array AS $key => $value){
				echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor=''><td style=\"border:1px solid black;\" bgcolor=#FFFF99>$value</td>";
				$download_data .= "$value";
				foreach($date_array AS $key2 => $value2){
					if( $sales_array[$value2][$value] != 0 ){
						if($sales_array[$value2][$value] > $highestSales){
							$highestSales = $sales_array[$value2][$value];
						}
					
						$showsales = "$" . number_format( $sales_array[$value2][$value], 2 );
						$showcust = $people_array[$value2][$value];
						$avg = "$" . number_format($sales_array[$value2][$value] / $people_array[$value2][$value], 2);
						$percent = round($sales_array[$value2][$value] / array_sum($sales_array[$value2]) * 100, 1) . "%";
					}
					else{
						$showsales = "&nbsp;";
						$showcust = "&nbsp;";
						$avg = "&nbsp;";
						$percent = "&nbsp;";
					}
					
					$idValue = "$key-$key2";
				
					echo "<td style=\"border:1px solid black;\" align=right id=\"$idValue\">$showsales</td>
						<td style=\"border:1px solid black;\" align=right id=\"cust$idValue\">$showcust</td>
						<td style=\"border:1px solid black;\" align=right id=\"avg$idValue\">$avg</td>
						<td style=\"border:1px solid black;\" align=right id=\"pct$idValue\" bgcolor=#E8E7E7>$percent</td>";
					$download_data .= ",$$showsales,$showcust,$avg,$percent%";
				}
				echo "</tr>";
				$download_data .= "\r\n";
			}
			?>
		</tbody>
			<?php
			echo "<tr bgcolor=#FFFF99><td style=\"border:1px solid black;\"><b>Totals</b></td>";
			foreach($date_array AS $key => $value){
				$total = number_format(array_sum($sales_array[$value]), 2);
				$total_people = array_sum($people_array[$value]);
				$total_avg = number_format(array_sum($sales_array[$value]) / array_sum($people_array[$value]), 2);
				
				echo "<td style=\"border:1px solid black;\" align=right><b>$$total</b></td>
						<td style=\"border:1px solid black;\" align=right><b>$total_people</b></td>
						<td style=\"border:1px solid black;\" align=right><b>$$total_avg</b></td>
						<td style=\"border:1px solid black;\" align=right>&nbsp;</td>";
			}
			echo "</tr>";
			?>
		<tfoot>
		</tfoot>
	</table>
	<p>
	<?php
	echo "<input type=button value='Trend' onclick=\"showTrend($highestSales,$date_array_len,$time_array_len,$t_period2,$t_period);\">";
	echo "<p>";

?>