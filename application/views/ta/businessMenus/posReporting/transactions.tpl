<?php



	if( $current_terminal > 0 ) {
		$add_query = "AND terminal_group = $current_terminal";
	}
	else {
		$add_query = "";
	}
	
	$datetime_field = $t_datefield < 2 ? 'bill_datetime' : 'bill_posted';
	
	/////payroll deduct
	$query33 = "select businessid FROM business_group_detail 
                                    WHERE group_id = (SELECT group_id FROM business_group_detail WHERE businessid = $businessid)";
	$result33 = Treat_DB_ProxyOldProcessHost::query( $query33 );
	
	while($r33 = Treat_DB_ProxyOldProcessHost::mysql_fetch_array($result33)){
		if($businessIDs == ''){
			$businessIDs = $r33["businessid"];
		}
		else{
			$businessIDs .= ',' . $r33["businessid"];
		}
	}
	
	if($businessIDs == ''){
		$businessIDs = $businessid;
	}
	////end pd

	$query = "
		SELECT
			*
		FROM
			checks
		WHERE
			businessid = '$businessid'
			AND {$datetime_field} BETWEEN '$t_date1' AND '$t_date2'
			$add_query
		ORDER BY {$datetime_field}
	";
	$result = Treat_DB_ProxyOldProcessHost::query( $query );
?>

	<table class="sortable" style="border:1px solid black;font-size:12px;background-color:white;" cellspacing="0" cellpadding="1" width="98%">
		<thead>
			<tr bgcolor="#FFFF99">
				<td style="border:1px solid black;">Check#</td>
				<td style="border:1px solid black;">Time</td>
				<td style="border:1px solid black;">Item</td>
				<td style="border:1px solid black;">Qty</td>
				<td style="border:1px solid black;">Price</td>
				<td style="border:1px solid black;">Total</td>
				<!--td style="border:1px solid black;">Tendered</td-->
			</tr>
		</thead>
		<tbody>
<?php
	$report_totals = array();
	
	$download_data .= "Check#,Time,KIOSK Status,Item,Qty,Price,Total\r\n";

	$lascheck = 0;
	$report_total = array();
	while( $r = Treat_DB_ProxyOldProcessHost::mysql_fetch_array( $result ) ) {
		$check_number = $r["check_number"];
		$bill_datetime = $r[$datetime_field];
		$total = $r["total"];
		$taxes = $r["taxes"];
		$check_is_void = $r["is_void"];
		$offline = $r["offline"];
		
		if($offline == 1){
			$showOffline = "(Offline)";
		}
		else{
			$showOffline = "";
		}

		///check detail
		$query2 = "
			SELECT
				checkdetail.quantity,
				checkdetail.price,
				checkdetail.item_number,
				checkdetail.is_void,
				menu_items_new.name,
				menu_items_new.active
			FROM
				checkdetail
			LEFT JOIN menu_items_new ON
				checkdetail.item_number = menu_items_new.pos_id 
				AND menu_items_new.businessid = $businessid 
			WHERE
				checkdetail.bill_number = $check_number
				AND checkdetail.businessid = $businessid
		";
		$result2 = Treat_DB_ProxyOldProcessHost::query( $query2 );
		$num_detail = Treat_DB_ProxyOldProcessHost::mysql_numrows( $result2 );

		while( $r2 = Treat_DB_ProxyOldProcessHost::mysql_fetch_array( $result2 ) ) {
			$qty = $r2["quantity"];
			$price = number_format( ($r2["price"]*$pnureportcurrency), 2 ); 
			$name = $r2["name"];
			$is_void = $r2["is_void"];
			$active = $r2["active"];
			$item_number = $r2["item_number"];

			if($item_number == 2147483647){$name = "Reload"; $reload = 1;}
			else{$reload = 0;}

			if($name == ""){$name = "<font color=red>Missing Item</font>";}

			$total = number_format( ($qty * $price), 2 );
			$sales_total = $qty * $price;

			if( $check_is_void == 1 ) {
				$show_color = "red";
			}
			elseif($reload == 1) {
				$show_color = "#CCCC99";
			}
			else {
				$show_color = "white";
			}

			if( $is_void == 1 ) {
				$show_style = "text-decoration: line-through;";
			}
			else {
				$show_style = "text-decoration: none;";
			}
			
			if($active == 1){
				$showinactive = "<font color=blue>*</font>";
			}
			else{
				$showinactive = "";
			}

			if($reload == 0 && $is_void == 0 && $check_is_void == 0){
				$report_total['Sales'] += $sales_total;
			}
			elseif($reload != 1){
				$report_total['Voided Sales'] += $sales_total;
			}
			elseif($reload == 1 && ($is_void == 1 || $check_is_void == 1)){
				$report_total['Voided Reloads'] += $sales_total;
			}
			elseif($reload == 1){
				$report_total['Reloads'] += $sales_total;
			}
?>
			<!-- SALES --!>
			<tr bgcolor="<?php echo $show_color; ?>">
				<td style="border:1px solid black;<?php echo $show_style; ?>">&nbsp;<?php echo $check_number; ?></td>
				<td style="border:1px solid black;<?php echo $show_style; ?>">&nbsp;<?php echo $bill_datetime; ?> <?php echo $showOffline; ?></td>
				<td style="border:1px solid black;<?php echo $show_style; ?>">&nbsp;<?php echo $name; ?><?php echo $showinactive; ?></td>
				<td style="border:1px solid black;<?php echo $show_style; ?>" width="10%">&nbsp;<?php echo $qty; ?></td>
				<td style="border:1px solid black;<?php echo $show_style; ?>" width="10%">&nbsp;<?php echo $price; ?></td>
				<td style="border:1px solid black;<?php echo $show_style; ?>" width="10%">&nbsp;<?php echo $total; ?></td>
			</tr>
<?php
			$download_data .= "$check_number,$bill_datetime,$showOffline,\"$name\",$qty,$price,$total\r\n";
			
		}

		////discount detail
		$query2 = "
			SELECT
				checkdiscounts.amount,
				IF(p2.name IS NOT NULL, p2.name, p1.name) AS name
			FROM
				checkdiscounts
			LEFT JOIN promotions p1 ON
				p1.pos_id = checkdiscounts.pos_id
				AND p1.businessid = $businessid
			LEFT JOIN promotions p2 ON
				p2.pos_id = checkdiscounts.pos_id
				AND p1.businessid IS NULL
			WHERE
				checkdiscounts.check_number = '$check_number'
				AND checkdiscounts.businessid = $businessid
		";
		$result2 = Treat_DB_ProxyOldProcessHost::query( $query2 );

		while($r2 = Treat_DB_ProxyOldProcessHost::mysql_fetch_array($result2)){
			$promo_name = $r2["name"];
			$promo_amount = $r2["amount"];
			$promo_amount = number_format($promo_amount, 2);
			
			if($promo_name == ""){
				$promo_name = "Discount";
				$promo_amount *= -1;
			}
?>
			<!-- DISCOUNT --!>
			<tr bgcolor="#99FF99">
				<td style="border:1px solid black;">&nbsp;<?php echo $check_number; ?></td>
				<td style="border:1px solid black;">&nbsp;<?php echo $bill_datetime; ?> <?php echo $showOffline; ?></td>
				<td style="border:1px solid black;">&nbsp;<?php echo $promo_name; ?></td>
				<td style="border:1px solid black;">&nbsp;</td>
				<td style="border:1px solid black;">&nbsp;</td>
				<td style="border:1px solid black;">&nbsp;<?php echo $promo_amount; ?></td>
			</tr>
<?php
			$download_data .= "$check_number,$bill_datetime,$showOffline,$promo_name,,,$promo_amount\r\n";
			
			if($check_is_void == 0){
				$report_total['Discounts'] += $promo_amount;
			}
		}

		///tax
		if( $taxes != 0 ) {
?>

			<!-- TAXES --!>
			<tr bgcolor="#E8E7E7">
				<td style="border:1px solid black;">&nbsp;<?php echo $check_number; ?></td>
				<td style="border:1px solid black;">&nbsp;<?php echo $bill_datetime; ?> <?php echo $showOffline; ?></td>
				<td style="border:1px solid black;">&nbsp;TAX</td>
				<td style="border:1px solid black;">&nbsp;</td>
				<td style="border:1px solid black;">&nbsp;</td>
				<td style="border:1px solid black;">&nbsp;<?php echo $taxes; ?></td>
			</tr>
<?php
			$download_data .= "$check_number,$bill_datetime,$showOffline,TAX,,,$taxes\r\n";
			
			if($check_is_void == 0){
				$report_total['Taxes'] += $taxes;
			}
		}

		///payments
		$query2 = "SELECT pd.* "
				//. " , c1.currency_type, COALESCE(c1.tendered, 0) as tendered "
				. " FROM payment_detail pd "
				// . " LEFT JOIN checks_currency c1 ON pd.check_number = c1.check_number and pd.businessid = c1.businessid "
				// . " LEFT JOIN checks_currency.uuid_checks_currency = pd.uuid_payment_detail"
				. " WHERE pd.check_number = '$check_number' AND pd.businessid = '$businessid'";
		$result2 = Treat_DB_ProxyOldProcessHost::query( $query2 );

		while( $r2 = Treat_DB_ProxyOldProcessHost::mysql_fetch_array( $result2 ) ) {
			$payment_type = $r2["payment_type"];
			$received = ($r2["received"]*$pnureportcurrency);
			$base_amount = $r2["base_amount"];
			$scancode = $r2["scancode"];
			
			//$received = number_format($received - $base_amount, 2);
			
			
			if( $payment_type == 5 ) {
				// Hack to diplay Payroll Deduct instead of voucher.
				$payment_type = "Payroll Deduct";
			} else {
				$payment_type = \EE\Model\Payment\Type::getLabelById($payment_type);
				if (is_null($payment_type)) {
					$payment_type = "Cash";
				}
			}
			
			if($check_is_void == 0){
				$report_total[$payment_type] += $received;
				$report_amount[$payment_type]++;
				$show_style = "text-decoration: none;";
			}
			else{
				$show_style = "text-decoration: line-through;";
			}
			
			$received = number_format($received - $base_amount, 2);

			$show_name = "";
			if( $scancode != "" ) {
				if($payment_type == "Payroll Deduct"){
					$query3 = "SELECT first_name,last_name FROM customer WHERE scancode = '$scancode' AND businessid IN ($businessIDs)";
					$result3 = Treat_DB_ProxyOldProcessHost::query( $query3 );
					
					$first_name = @Treat_DB_ProxyOldProcessHost::mysql_result( $result3, 0, "first_name" );
					$last_name = @Treat_DB_ProxyOldProcessHost::mysql_result( $result3, 0, "last_name" );
				}
				elseif($scancode != 0){
					$query3 = "SELECT first_name,last_name FROM customer WHERE scancode = '$scancode'";
					$result3 = Treat_DB_ProxyOldProcessHost::query( $query3 );
					
					$first_name = @Treat_DB_ProxyOldProcessHost::mysql_result( $result3, 0, "first_name" );
					$last_name = @Treat_DB_ProxyOldProcessHost::mysql_result( $result3, 0, "last_name" );
				}
				else{
					$first_name = "";
					$last_name = "";
				}

				if( $fist_name == "" && $last_name == "" ) {
					$show_name = "";
				}
				else {
					$show_name = "($first_name $last_name - $scancode)";
				}
			}

			if( $num_detail == 0 ) {
				$show_color = "#CCCC99";
			}
			else {
				$show_color = "#BBBBBB";
			}
?>
			<!-- CASH --!>
			<tr bgcolor="<?php echo $show_color; ?>">
				<td style="border:1px solid black;<?php echo $show_style; ?>">&nbsp;<?php echo $check_number; ?></td>
				<td style="border:1px solid black;<?php echo $show_style; ?>">&nbsp;<?php echo $bill_datetime; ?> <?php echo $showOffline; ?></td>
				<td style="border:1px solid black;<?php echo $show_style; ?>">&nbsp;<?php echo $payment_type; ?> <?php echo $show_name; ?></td>
				<td style="border:1px solid black;<?php echo $show_style; ?>">&nbsp;</td>
				<td style="border:1px solid black;">&nbsp;</td>
				<td style="border:1px solid black;<?php echo $show_style; ?>">&nbsp;<?php echo $received; ?></td>
				<!--td style="border:1px solid black;<?php //echo $show_style; ?>">&nbsp;<?php //echo $r2["tendered"]; ?> <?php //echo $r2["currency_type"]; ?></td-->
			</tr>
<?php
			$received = $received * -1;
			$download_data .= "$check_number,$bill_datetime,$showOffline,\"$payment_type $show_name\",,,$received\r\n";
		}
	}
?>

		</tbody>
	</table>
</center>
<br>
<?php
	///online transactions
	if ( in_array( $user->username, array(
		"SteveM",
		"budo",
		"TraceyM",
		"JimM",
		"jeffm",
		"richs",
		"paulb",
		"mhutchings",
		"adaml",
		"jone",
		"pgrant",
		"FlintH",
		"toddm",
		"jhupp",
		"johnw",
		"christopherc",
		"joelf",
		"ritaz",
		"JakeN",
		"Jake",
		"mistyd",
		"PeteB",
		"sandyb",
	) ) ) {
		$query = "SELECT ct.id, 
						ct.trans_type_id,
						ct.date_created AS date_created,
						ctt.label, 
						c.scancode, 
						c.first_name, 
						c.last_name,
						ct.chargetotal
						FROM customer_transactions ct
					JOIN customer c ON c.customerid = ct.customer_id AND c.businessid = $businessid
					JOIN customer_transaction_types ctt ON ctt.id = ct.trans_type_id
					LEFT JOIN customer_transactions_business ctb ON ctb.ct_id = ct.id
					WHERE ct.date_created BETWEEN '$t_date1' AND '$t_date2' AND response = 'APPROVED' AND ct.trans_type_id != 8 AND ctb.businessid IS NULL
				UNION
					SELECT ct.id, 
						ct.trans_type_id,
						ct.date_created AS date_created,
						ctt.label, 
						c.scancode, 
						c.first_name,
						c.last_name,
						ct.chargetotal
						FROM customer_transactions ct
					JOIN customer c ON c.customerid = ct.customer_id
					JOIN customer_transaction_types ctt ON ctt.id = ct.trans_type_id
					JOIN customer_transactions_business ctb ON ctb.ct_id = ct.id AND ctb.businessid = $businessid
					WHERE ct.date_created BETWEEN '$t_date1' AND '$t_date2' AND response = 'APPROVED' AND ct.trans_type_id != 8                           
					ORDER BY date_created ASC";
		$result = Treat_DB_ProxyOldProcessHost::query( $query );

		///table header
?>

<center>
	<table class="sortable" style="border:1px solid black;font-size:12px;background-color:white;" cellspacing="0" cellpadding="1" width="98%">
		<thead>
			<tr bgcolor="#FFFF99">
				<td style="border:1px solid black;">Check#</td>
				<td style="border:1px solid black;">Time</td>
				<td style="border:1px solid black;">Item</td>
				<td style="border:1px solid black;">Qty</td>
				<td style="border:1px solid black;">Price</td>
				<td style="border:1px solid black;">Total</td>
			</tr>
		</thead>
		<tbody>
<?php
		while($r = Treat_DB_ProxyOldProcessHost::mysql_fetch_array($result)){
			$checknum = $r["id"];
			$trans_type = $r["trans_type_id"];
			$date_created = $r["date_created"];
			$type = $r["label"];
			$scancode = $r["scancode"];
			$first_name = $r["first_name"];
			$last_name = $r["last_name"];
			$amount = $r["chargetotal"];
			
			$report_total[$type] += $amount;
			$report_amount[$type]++;
			
			$amount = number_format($amount, 2);
			
			if($trans_type == 2 || $trans_type == 3){
				$showcolor = "#F17884";
			}
			elseif($trans_type == 1 || $trans_type == 4){
				$showcolor = "#CCCC99";
			}
			else{
				$showcolor = "white";
			}
?>

			<tr bgcolor="<?php echo $showcolor; ?>">
				<td style="border:1px solid black;">&nbsp;<?php echo $checknum; ?></td>
				<td style="border:1px solid black;">&nbsp;<?php echo $date_created; ?></td>
				<td style="border:1px solid black;">&nbsp;<?php echo $type; ?> (<?php echo $first_name; ?> <?php echo $last_name; ?> - <?php echo $scancode; ?>)</td>
				<td style="border:1px solid black;">&nbsp;1</td>
				<td style="border:1px solid black;">&nbsp;<?php echo $amount; ?></td>
				<td style="border:1px solid black;">&nbsp;<?php echo $amount; ?></td>
			</tr>
<?php
		}
?>

		</tbody>
	</table>
</center>
<br>
<?php
	}
	
	///totals
?>

<center>
	<table width="98%">
		<tr>
			<td>
				<table style="border:1px solid black;font-size:12px;background-color:white;" cellspacing="0" cellpadding="1" width="33%">
					<thead>
						<tr bgcolor="#FFFF99">
							<td style="border:1px solid black;">Category</td>
							<td style="border:1px solid black;" align="right">Currency</td>
							<td style="border:1px solid black;" align="right">Amount</td>
							<td style="border:1px solid black;" align="right">Qty</td>
						</tr>
					</thead>
					<tbody>
<?php
		foreach($report_total AS $key => $value){
			$value = number_format($value,2);
?>

						<tr>
							<td style="border:1px solid black;"><?php echo $key; ?></td>
							<td style="border:1px solid black;" align="right"><?php echo $pstrdesCurrency; ?></td>
							<td style="border:1px solid black;" align="right">$<?php echo $value; ?></td>
							<td style="border:1px solid black;" align="right"><?php echo $report_amount[$key]; ?></td>
						</tr>
<?php
		}
?>

					</tbody>
				</table>
			</td>
		</tr>
	</table>
</center>
<br>
