<?php
	$goalTimes = array("A" => "00:02:30", "B" => "00:03:00", "C" => "00:05:00", "D" => "00:06:00", "F" => "06:59:00");

	$query = "SELECT ckt.check_number,
		MIN(ckt.start_time) AS kds_start,
		MAX(ckt.end_time) AS kds_end,
		c.bill_datetime,
		ckt.destination,
		TIMEDIFF(MAX(ckt.end_time),MIN(ckt.start_time)) AS kds_time,
		TIMEDIFF(c.bill_datetime,MIN(ckt.start_time)) AS register_time,
		c.terminal_group
		FROM check_kds_time ckt
		JOIN checks c ON c.businessid = ckt.businessid 
			AND c.check_number = ckt.check_number 
			AND c.bill_datetime BETWEEN '$t_date1 00:00:00' AND '$t_date2 23:59:59'
		WHERE ckt.businessid = $businessid
		GROUP BY check_number
		ORDER BY terminal_group";
	$result = Treat_DB_ProxyOldProcessHost::query( $query );
	
	$last_terminal = 0;
	$orderTimes = array();
	$orders = array();
	
	while($r = mysql_fetch_array($result)){
		$check_number = $r["check_number"];
		$kds_time = $r["kds_time"];
		$register_time = $r["register_time"];
		$terminal = $r["terminal_group"];
		$bill_datetime = $r["bill_datetime"];
		
		$orders[$terminal]['kitchen'][$check_number] = $kds_time;
		$orderDates[$terminal]['kitchen'][$check_number] = $bill_datetime;
		$orders[$terminal]['register'][$check_number] = $register_time;
		
		$term_kds = 0;
		$term_reg = 0;
		foreach($goalTimes AS $key => $value){
			if($kds_time < $value && $term_kds == 0){
				$orderTimes[$terminal]["kitchen"][$key]++;
				$term_kds = 1;
			}
			if($register_time < $value && $term_reg == 0){
				$orderTimes[$terminal]["register"][$key]++;
				$term_reg = 1;
			}
			if($term_kds == 1 && $term_reg == 1){
				break;
			}
		}
	}
	
	///////////
	///print///
	///////////
	foreach($orderTimes AS $key => $values){
		echo "<table cellspacing=0 style=\"border:1px solid black;font-size:12px;\" width=95%><tr bgcolor=#FFFF99><td style=\"border:1px solid black;\" width=10%>Terminal $key</td><td style=\"border:1px solid black;\" width=10%>Orders</td>";
		
		///header
		foreach($goalTimes AS $grade => $times){
			echo "<td style=\"border:1px solid black;\" colspan=2 width=10%>$grade</td>";
		}
		echo "<td style=\"border:1px solid black;\" width=10%>Average</td><td style=\"border:1px solid black;\" width=20%>Longest Times</td>";
		echo "</tr>";
		
		///kds (total) times
		$total = array_sum($orderTimes[$key]['kitchen']);
		echo "<tr><td style=\"border:1px solid black;\">Total</td><td style=\"border:1px solid black;\">$total</td>";
		foreach($goalTimes AS $grade => $times){
			echo "<td width=5% style=\"border:1px solid black;\">{$orderTimes[$key]['kitchen'][$grade]}</td>";
			$percent = round($orderTimes[$key]['kitchen'][$grade] / $total * 100, 1);
			echo "<td width=5% style=\"border:1px solid black;\">$percent%</td>";
		}
		///averages
		$totaltime = 0;
		foreach($orders[$key]['kitchen'] AS $check_number => $time){
			$time = explode(":",$time);
			$hour = $time[0];
			$min = $time[1];
			$sec = $time[2];
			$min = $min * 60;
			$ttime = $min + $sec;
			$totaltime += $ttime;
		}
		$totaltime = round($totaltime / array_sum($orderTimes[$key]['kitchen']));
		$seconds = $totaltime % 60;
		if($seconds == 0){$seconds = "00";}
		elseif(strlen($seconds) < 2){$seconds = "0$seconds";}
		$minutes = round($totaltime / 60);
		if($minutes == 0){$minutes = "00";}
		elseif(strlen($minutes) < 2){$minutes = "0$minutes";}
		echo "<td style=\"border:1px solid black;\">$minutes:$seconds</td>";
		
		///longest times
		arsort($orders[$key]['kitchen']);
		echo "<td style=\"border:1px solid black;\" rowspan=2>";
		$limit = 0;
		foreach($orders[$key]['kitchen'] AS $check_number => $datetime){
			echo "{$orderDates[$key]['kitchen'][$check_number]} ($datetime)<br>";
			$limit++;
			if($limit >= 3){break;}
		}
		echo "</td>";
		
		echo "</tr>";
		
		///register times
		$total = array_sum($orderTimes[$key]['register']);
		echo "<tr><td style=\"border:1px solid black;\">Register</td><td style=\"border:1px solid black;\">$total</td>";
		foreach($goalTimes AS $grade => $times){
			echo "<td width=5% style=\"border:1px solid black;\">{$orderTimes[$key]['register'][$grade]}</td>";
			$percent = round($orderTimes[$key]['register'][$grade] / $total * 100, 1);
			echo "<td width=5% style=\"border:1px solid black;\">$percent%</td>";
		}
		///averages
		$totaltime = 0;
		foreach($orders[$key]['register'] AS $check_number => $time){
			$time = explode(":",$time);
			$hour = $time[0];
			$min = $time[1];
			$sec = $time[2];
			$min = $min * 60;
			$ttime = $min + $sec;
			$totaltime += $ttime;
		}
		$totaltime = round($totaltime / array_sum($orderTimes[$key]['register']));
		$seconds = $totaltime % 60;
		if($seconds == 0){$seconds = "00";}
		elseif(strlen($seconds) < 2){$seconds = "0$seconds";}
		$minutes = round($totaltime / 60);
		if($minutes == 0){$minutes = "00";}
		elseif(strlen($minutes) < 2){$minutes = "0$minutes";}
		echo "<td style=\"border:1px solid black;\">$minutes:$seconds</td>";
		echo "</tr>";
		echo "</table><p>";
	}
	
	///key
	echo "<table cellspacing=0 style=\"border:1px solid black;font-size:12px;\" width=95%><tr bgcolor=#CCCCCC><td colspan=5 style=\"border:1px solid black;\">Key</td></tr>";
	echo "<tr>";
	foreach($goalTimes AS $key => $value){
		if($key == "F"){
			echo "<td style=\"border:1px solid black;\" width=20%>F >= $lastvalue</td>";
		}
		else{
			echo "<td style=\"border:1px solid black;\" width=20%>$key < $value</td>";
		}
		$lastvalue = $value;
	}
	echo "</tr></table>";
?>