<?php
	$date2 = $t_date1;
	
	$query = "SELECT districtid FROM business WHERE businessid = $businessid";
	$result = Treat_DB_ProxyOldProcessHost::query( $query );
	
	$r = mysql_fetch_array($result);
	$districtid = $r["districtid"];
	
	$triggerName = array();
	$triggerValue = array();
	
	$query = "SELECT kot.id,kot.trigger,kotd.value FROM kiosk_order_trigger kot 
				LEFT JOIN kiosk_order_trigger_detail kotd ON kotd.triggerid = kot.id AND kotd.districtid = $districtid 
				ORDER BY kot.id";
	$result = Treat_DB_ProxyOldProcessHost::query( $query );
	
	while($r = mysql_fetch_array($result)){
		$triggerName[$r["id"]] = $r["trigger"];
		$triggerValue[$r["id"]] = $r["value"];
	}

	if($ordertype > 0){
		$o_query = "AND min.ordertype = $ordertype";
	}
	else{
		$o_query = "";
	}

	$query = "SELECT SUM( min.max ) AS storeTotal, ROUND(SUM(min.max * mip.cost), 2) AS storeCost, ROUND(SUM(min.max * mip.price), 2) AS storePrice
		FROM posreports_menu_items_new min
		LEFT JOIN posreports_menu_items_price mip ON mip.menu_item_id = min.id
		WHERE min.businessid =$businessid
		AND min.active =0
		$o_query";
	$result = Treat_DB_ProxyOldProcessHost::query( $query );
	
	$r = mysql_fetch_array($result);
	$storeAmt = $r["storeTotal"];
	$storeCost = $r["storeCost"];
	$storePrice = $r["storePrice"];
		
	//echo "$storeAmt, $storeCost, $storePrice<br>";
	
	$reorderAmt = 0;
	$reorderCost = 0;
	$reorderPrice = 0;
	$reorderOOS = 0;
	
	foreach( $items AS $key => $value ) {
		////get item_code
		$complete_query = "SELECT id,item_code,max,reorder_point,reorder_amount,active FROM menu_items_new WHERE businessid = '$businessid' AND pos_id = '$key'";
		$result = Treat_DB_ProxyOldProcessHost::query( $complete_query );

		$item_id = mysql_result( $result, 0, "id" );
		$item_code = mysql_result( $result, 0, "item_code" );
		$max = mysql_result( $result, 0, "max" );
		$reorder_point = mysql_result( $result, 0, "reorder_point" );
		$reorder_amount = mysql_result( $result, 0, "reorder_amount" );
		$inactive = mysql_result( $result, 0, "active" );

		////get dates
		$dates = Kiosk::last_inv_dates( $machine_num, $item_code, 0 , $date2 );
		$date = $dates[1];
		if( $date == "" ) {
			$date = $dates[0];
		}

		////cost
		$query5 = "SELECT price,cost FROM menu_items_price WHERE menu_item_id = $item_id";
		$result5 = Treat_DB_ProxyOldProcessHost::query( $query5 );

		$cost = @mysql_result( $result5, 0, "cost" );
		$price = @mysql_result( $result5, 0, "price" );

		////last count
		$onhand2 = Kiosk::inv_onhand( $machine_num, $item_code, $date );

		////fills
		$fills = Kiosk::fills( $machine_num, $item_code, $date, $date2 );

		////waste
		$waste = Kiosk::waste( $machine_num, $item_code, $date, $date2 );

		////get check detail
		$items_sold = menu_items::items_sold( $key, $businessid, $date, $date2, $current_terminal );

		$onhand = $onhand2 + $fills - $waste - $items_sold;
		
		if($onhand == 0){$storeOOS++;}

		/////REORDER
		if( $reorder_point == 0 && $max != 0 ) {
			$reorder = $max - $onhand;
		}
		elseif( $max != 0 ) {
			if( $onhand <= $reorder_point ) {
				if( $reorder_amount != 0 && $reorder_amount > ( $max - $onhand ) ) {
					$reorder = $reorder_amount;
				}
				else {
					$reorder = $max - $onhand;
				}
			}
			else {
				$reorder = 0;
			}
		}
		else {
			$reorder = 0;
		}
		if( $reorder > $max ) {
			$reorder = $max;
		}
		if( $reorder <= 0 ) {
			$reorder = 0;
		}
		
		///print
		if($reorder > 0){
			$reorderAmt += $reorder;
			$reorderCost += $cost * $reorder;
			$reorderPrice += $price * $reorder;
			//echo "$value,$reorder,$cost,$price<br>";
		}
	}
	//echo "$reorderAmt,$reorderCost,$reorderPrice<br>";
	
	$diffAmt = round($reorderAmt / $storeAmt * 100 ,2);
	$diffCost = round($reorderCost / $storeCost * 100, 2);
	$diffPrice = round($reorderPrice / $storePrice * 100, 2);
	
	//echo "$diffAmt%, $diffCost%, $diffPrice%, $storeOOS<br>";
	
	echo "<table style=\"border:1px solid black;background-color:white;font-size:12px;\" cellspacing=0 cellpadding=1 width=98%>";
	echo "<thead><tr bgcolor=#FFFF99><td style=\"border:1px solid black;\">&nbsp;</td><td style=\"border:1px solid black;\">Store Max</td><td style=\"border:1px solid black;\">ReOrder</td><td style=\"border:1px solid black;\">Value</td><td style=\"border:1px solid black;\">Trigger</td></tr></thead><tbody>";
	
	if($diffAmt >= $triggerValue[1] && $triggerValue[1] != ""){
		$bgColor = "green";
	}
	else{
		$bgColor = "white";
	}
	echo  "<tr bgcolor=$bgColor><td style=\"border:1px solid black;\" bgcolor=#FFFF99>Item Count</td><td style=\"border:1px solid black;\">$storeAmt</td><td style=\"border:1px solid black;\">$reorderAmt</td><td style=\"border:1px solid black;\">$diffAmt%</td><td style=\"border:1px solid black;\">$triggerValue[1]</td></tr>";
	
	if($diffCost >= $triggerValue[2] && $triggerValue[2] != ""){
		$bgColor = "green";
	}
	else{
		$bgColor = "white";
	}
	echo  "<tr bgcolor=$bgColor><td style=\"border:1px solid black;\" bgcolor=#FFFF99>Item Cost</td><td style=\"border:1px solid black;\">$$storeCost</td><td style=\"border:1px solid black;\">$$reorderCost</td><td style=\"border:1px solid black;\">$diffCost%</td><td style=\"border:1px solid black;\">$triggerValue[2]</td></tr>";
	
	if($diffPrice >= $triggerValue[3] && $triggerValue[3] != ""){
		$bgColor = "green";
	}
	else{
		$bgColor = "white";
	}
	echo  "<tr bgcolor=$bgColor><td style=\"border:1px solid black;\" bgcolor=#FFFF99>Item Price</td><td style=\"border:1px solid black;\">$$storePrice</td><td style=\"border:1px solid black;\">$$reorderPrice</td><td style=\"border:1px solid black;\">$diffPrice%</td><td style=\"border:1px solid black;\">$triggerValue[3]</td></tr>";
	
	if($storeOOS >= $triggerValue[4] && $triggerValue[4] != ""){
		$bgColor = "green";
	}
	else{
		$bgColor = "white";
	}
	echo  "<tr bgcolor=$bgColor><td style=\"border:1px solid black;\" bgcolor=#FFFF99>Out of Stock</td><td style=\"border:1px solid black;\">&nbsp;</td><td style=\"border:1px solid black;\">&nbsp;</td><td style=\"border:1px solid black;\">$storeOOS</td><td style=\"border:1px solid black;\">$triggerValue[4]</td></tr>";
	echo "</tbody></table>";
?>