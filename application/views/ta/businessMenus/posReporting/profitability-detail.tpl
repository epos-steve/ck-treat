<?php
	$date2 = "$t_date1 00:00:00";
	$date3 = "$t_date2 23:59:59";

	$item_id = $_GET["id"];
	
		////get item_code
		$complete_query = "SELECT menu_items_new.name,
							menu_items_new.item_code,
							menu_items_new.pos_id,
							menu_items_new.max,
							menu_items_new.reorder_point,
							menu_items_new.reorder_amount,
							menu_items_new.active,
							menu_items_price.price,
							menu_items_price.cost
					FROM menu_items_new
					JOIN menu_items_price ON menu_items_price.menu_item_id = menu_items_new.id
					WHERE menu_items_new.businessid = $businessid AND menu_items_new.id = $item_id";
		$result = Treat_DB_ProxyOldProcessHost::query( $complete_query );

		$item_name = mysql_result( $result, 0, "name" );
		$item_code = mysql_result( $result, 0, "item_code" );
		$key = mysql_result( $result, 0, "pos_id" );
		$max = mysql_result( $result, 0, "max" );
		$reorder_point = mysql_result( $result, 0, "reorder_point" );
		$reorder_amount = mysql_result( $result, 0, "reorder_amount" );
		$inactive = mysql_result( $result, 0, "active" );
		$price = mysql_result( $result, 0, "price" );
		$cost = mysql_result( $result, 0, "cost" );

		////get dates
		$dates = Kiosk::last_inv_dates( $machine_num, $item_code, 0 , $date2 );
		$date = $dates[1];
		if( $date == "" ) {
			$date = $dates[0];
		}
		
		////last count
		$onhand2 = Kiosk::inv_onhand( $machine_num, $item_code, $date );

		////fills
		$fills = Kiosk::fills( $machine_num, $item_code, $date, $date2 );

		////waste
		$waste = Kiosk::waste( $machine_num, $item_code, $date, $date2 );

		////get check detail
		$items_sold = menu_items::items_sold( $key, $businessid, $date, $date2, $current_terminal );

		////starting onhand amount
		$onhand = $onhand2 + $fills - $waste - $items_sold;
		
		$machines = implode(",", $machine_num);
		$machines = str_replace(",","','",$machines);
		
		///history
		$query = "SELECT checks.bill_datetime AS date_time,
			SUM(checkdetail.quantity) AS amt, 
			'1' AS type,
			'' AS machine_num,
			checkdetail.price
		FROM checkdetail
		JOIN checks ON checks.check_number = checkdetail.bill_number 
			AND checks.businessid = $businessid
			AND checks.bill_datetime BETWEEN '$date2' AND '$date3'
		where checkdetail.businessid = $businessid 
			AND checkdetail.item_number = $key
                        group by date_time
		UNION
		SELECT inv_count_vend.date_time AS date_time,
			inv_count_vend.fills AS amt,
			'2' as type,
			inv_count_vend.machine_num,
			0 AS price
		FROM inv_count_vend 
		WHERE item_code = '$item_code' 
			AND machine_num IN ('$machines') 
			AND date_time BETWEEN '$date2' AND '$date3' 
			AND is_inv = 0 
			AND fills != 0
		UNION 
		SELECT inv_count_vend.date_time as date_time, 
			inv_count_vend.waste AS amt, 
			'3' as type,
			inv_count_vend.machine_num,
			0 AS price
		FROM inv_count_vend 
		WHERE item_code = '$item_code' 
			AND machine_num IN ('$machines') 
			AND date_time BETWEEN '$date2' AND '$date3' 
			AND is_inv = 0 
			AND waste != 0
		UNION 
		SELECT inv_count_vend.date_time as date_time, 
			inv_count_vend.onhand AS amt, 
			'4' as type,
			inv_count_vend.machine_num,
			0 AS price
		FROM inv_count_vend 
		WHERE item_code = '$item_code' 
			AND machine_num IN ('$machines') 
			AND date_time BETWEEN '$date2' AND '$date3' 
			AND is_inv = 1 
		ORDER BY date_time
		";
		$result = Treat_DB_ProxyOldProcessHost::query( $query );
	
		///format
		$cur_cost = number_format($cost, 2);
		$cur_price = number_format($price, 2);
		$margin = number_format(($cur_price - $cur_cost) / $cur_price * 100, 2);

		///table header
		echo "<table style=\"border:1px solid black;background-color:white;font-size:12px;\" cellspacing=0 cellpadding=1 width=98%>";
		echo "<thead><tr bgcolor=#FFFF99><td style=\"border:1px solid black;\" width=20%>$item_code $item_name</td><td width=20% style=\"border:1px solid black;\">Type</td><td width=20% style=\"border:1px solid black;\">Amount</td><td width=20% style=\"border:1px solid black;\">OnHand</td><td width=20% style=\"border:1px solid black;\">Note</td></tr></thead><tbody>";

		$download_data = "\"$item_code $item_name\",Type,Amount,OnHand,Note\r\n";

		////go through data
		$current = $onhand;
		$min_current = $current;
		$recommend = null;
		$oos = 0;
		$fills = 0;
		$sold = 0;
		$waste = 0;
		$skew = 0;
		$counts = 0;
		while($r = mysql_fetch_array($result)){
			$time = $r["date_time"];
			$amount = $r["amt"];
			$type = $r["type"];
			if($r["machine_num"] != "") $cur_machine = $r["machine_num"];
			$price = $r["price"];
			
			if($type == 1){$sold += $amount; $sales += $amount * $price;}
			elseif($type == 2){$fills += $amount;}
			elseif($type == 3){$waste += $amount;}
			
			if($type == 4){$current = $amount; $hist_rec = "count"; $counts++; $bColor = "#E8E8E8";}
			elseif($type == 1){$current -= $amount; $hist_rec = "sale"; $bColor = "white";}
			elseif($type == 3){$current -= $amount; $hist_rec = "waste"; $bColor = "#FF6699";}
			elseif( $type == 2){$current += $amount; $hist_rec = "fill"; $bColor = "#66FF66";}
			
			//$history .= "$hist_rec: $amount / $current \n";
			
			if($current <= 0 && $last_current > 0){
				$oos++;
				$note = "<font color=red>Out of Stock.</font>";
			}
			elseif($type == 4 && $current != $last_current){
				$note = "Variance.";
			}
			else{
				$note = "";
			}

			$last_current = $current;
			if($current < $min_current) $min_current = $current;

			echo "<tr bgcolor=$bColor><td style=\"border:1px solid black;\">$time</td><td style=\"border:1px solid black;\">$hist_rec</td><td style=\"border:1px solid black;\">$amount</td><td style=\"border:1px solid black;\">$current</td><td style=\"border:1px solid black;\">$note</td></tr>";

			$download_data .= "$time,$hist_rec,$amount,$current,$note\r\n";
		}
	
	echo "</tbody><tfoot><tr bgcolor=#FFFF99><td colspan=5 style=\"border:1px solid black;\">&nbsp;<a href='?bid=$businessid&cid=$companyid&report=12&id=$item_id&terminal_no=$current_terminal&which=$prevwhich&ordertype=$ordertype' style=$style>Return</a></td></tr></tfoot>";
	echo "</table><br>";

?>