<?php
	require('lib/inc/functions-inventory.php');

	$date1_value = '$t_date1 00:00:00';
	$date2_value = '$t_date2 23:59:59';
	
	$query = "SELECT * FROM inv_count_date 
		WHERE businessid = $businessid 
			AND date BETWEEN '$t_date1 00:00:00' AND '$t_date2 23:59:59' ORDER BY date";
	$result = Treat_DB_ProxyOldProcessHost::query( $query );
	
	$query2 = "call kfc_inventory_count($businessid , $date1_value , $date2_value)";
	//$result2 = Treat_DB_ProxyOldProcessHost::query( $query2 );
	
	echo "<table class=\"sortable\" style=\"border:1px solid black;font-size:12px;background-color:white;\" cellspacing=0 cellpadding=1 width=98%>";
	echo "<tr bgcolor=#FFFF99>
				<td style=\"border:1px solid black;\">Count Time</td>
				<td style=\"border:1px solid black;\">Variance</td>
				<td style=\"border:1px solid black;\">Ideal%</td>
				<td style=\"border:1px solid black;\">Actual%</td>
				<td style=\"border:1px solid black;\">Variance%</td>
				<td style=\"border:1px solid black;\">Audit</td>
			</tr>";
			
	$sheetTotal = 0;
	
	while($r = mysql_fetch_array($result)){
		$inv_date = $r["date"];
		$audit_user = $r["audit_user"];
		$audit_time = $r["audit_time"];
		$link_date = str_replace(' ','|',$inv_date);
		
		/////////////////////////////////////////////////////////////
		$inventoryItems = array();
		$inventoryItems = inventory_item_list($businessid, $inv_date);
		
		$count1 = array();
		$count2 = array();
		foreach($inventoryItems AS $id => $values){
			$count1[$id] = last_count($id, $inv_date);
			$count2[$id] = last_count($id, $inv_date, 1);
		}
		
		$invQtys = array();
		$purQtys = array();
		$salesAmt = array();
		
		$totalfc = 0;
		$totalfc2 = 0;
		$totalVar = 0;
		
		foreach($inventoryItems AS $id => $values){
			$date1 = $count1[$id]['date'];
			$date2 = $count2[$id]['date'];
			
			$invQtys[$id] = inventory_quantity($businessid, $id, $date1, $date2);
			
			$purQtys[$id] = purchases($businessid, $id, $date1, $date2);
			
			$salesAmt[$id] = sales($businessid, $date1, $date2);
		}
			
		$varTotal = 0;
				
		foreach($inventoryItems AS $id => $values){
			$used = "";
			$showUsed = "";
			$showSold = "";
			
			foreach($invQtys[$id] AS $key => $sold){
				if($key == $values['rec_sizeid1']){
					$used = @round($sold / $values['rec_num1'] * $values['pack_qty'], 2);
				}
				elseif($key == $values['rec_sizeid2']){
					$used = @round($sold / $values['rec_num2'] * $values['pack_qty'], 2);
				}
				else{
					$used = "";
					$showUsed = "";
					$showSold = "";
				}
			}
			
			if($purQtys[$id] != 0){
				$purch = @round($purQtys[$id] * $values['pack_qty'], 2);
			}
			else{
				$purch = "";
			}
			
			///variance
			$variance = $count1[$id]['amount'] - $used + $purch;
			if($variance != $count2[$id]['amount']){
				$variance = $count2[$id]['amount'] - $variance . "&nbsp;&nbsp;{$values['count_size']}";
				$varDollars = '$' . @number_format(($count2[$id]['amount'] - $variance) * $count2[$id]['price'], 2);
				$varTotal += @round(($count2[$id]['amount'] - $variance) * $count2[$id]['price'], 2);
			}
			else{
				$variance = "";
				$varDollars = "";
			}
		
			////actual food cost
			$actual = $count1[$id]['amount'] + $purch - $count2[$id]['amount'];
			$fc2 = @round($actual * $count2[$id]['price'] / $salesAmt[$id] * 100, 2);
			
			////theortical food cost
			$fc = @round($used * $count2[$id]['price'] / $salesAmt[$id] * 100, 2);
			
			///variance %
			$varPercent = $fc - $fc2;
			
			$totalfc += $fc;
			$totalfc2 += $fc2;
			$totalVar += $varPercent;
		}	
		$sheetTotal += $varTotal;
		$sheetTotalfc += $totalfc;
		$sheetTotalfc2 += $totalfc2;
		$sheetTotalVar += $totalVar;
		$varTotal = number_format($varTotal, 2);

		/////////////////////////////////////////////////////////////
		
		echo "<tr>
				<td style=\"border:1px solid black;\">$inv_date</td>
				<td style=\"border:1px solid black;\" align=right><a href='?bid=$businessid&cid=$companyid&report=38&terminal_no=$current_terminal&link_date=$link_date' style=$style>$$varTotal</a></td>
				<td style=\"border:1px solid black;\" align=right>$totalfc%</td>
				<td style=\"border:1px solid black;\" align=right>$totalfc2%</td>
				<td style=\"border:1px solid black;\" align=right>$totalVar%</td>
				<td style=\"border:1px solid black;\">$audit_user $audit_time</td>
			</tr>";
	}
	
	$sheetTotal = number_format($sheetTotal, 2);
	
	echo "<tfoot><tr bgcolor=#BBBBBB>
			<td style=\"border:1px solid black;\"><b>Total</b></td>
			<td style=\"border:1px solid black;\" align=right><b>$$sheetTotal</b></td>
			<td style=\"border:1px solid black;\" align=right><b>$sheetTotalfc%</b></td>
			<td style=\"border:1px solid black;\" align=right><b>$sheetTotalfc2%</b></td>
			<td style=\"border:1px solid black;\" align=right><b>$sheetTotalVar%</b></td>
			<td style=\"border:1px solid black;\">&nbsp;</td>
		</tr></tfoot>";
	
	echo "</table><p>";
?>