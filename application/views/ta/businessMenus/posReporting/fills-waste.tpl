<?php
	$date = $t_date1;
	$today = $t_date2;
	$date2 = date( "Y-m-d" );

	echo "<table class=\"sortable\" style=\"border:1px solid black;font-size:12px;background-color:white;\" cellspacing=0 cellpadding=1 width=98%>";
	echo "<thead><tr bgcolor=#FFFF99><td style=\"border:1px solid black;\" width=8%>Item Code</td><td style=\"border:1px solid black;\" width=20%>Item</td><td style=\"border:1px solid black;\">Cost</td><td style=\"border:1px solid black;\">Price</td>";

	$download_data .= "Item Code,Item,Cost,Price,";

	$date1 = $date;
	while( $date1 <= $today ) {
		$day = substr( $date1, 8, 2 );
		$month = substr( $date1, 5, 2 );
		echo "<td style=\"border:1px solid black;\">$month/$day Fills</td><td style=\"border:1px solid black;\">$month/$day Waste</td>";
		$download_data .= "$month/$day Fills,$month/$day Waste,";
		$date1 = nextday( $date1 );
	}
	echo "<td style=\"border:1px solid black;\" width=8%>Items Filled</td><td style=\"border:1px solid black;\" width=8%>Fill Cost</td><td style=\"border:1px solid black;\" width=8%>Items Wasted</td><td style=\"border:1px solid black;\" width=8%>Waste Cost</td></tr></thead><tbody>";

	$download_data .= "Items Filled,Fill Cost,Items Wasted,Waste Cost\r\n";

	foreach( $items AS $key => $value ) {
		$row_fill_cost = 0;
		$row_fill_total = 0;
		$row_waste_cost = 0;
		$row_waste_total = 0;
		
		////get item_code
		$complete_query = "SELECT item_code,id FROM menu_items_new WHERE businessid = '$businessid' AND pos_id = '$key'";
		$result = Treat_DB_ProxyOldProcessHost::query( $complete_query );

		$item_code = mysql_result( $result, 0, "item_code" );
		$id = mysql_result( $result, 0, "id" );

		$complete_query = "SELECT price,cost FROM menu_items_price WHERE menu_item_id = '$id'";
		$result = Treat_DB_ProxyOldProcessHost::query( $complete_query );

		$price = mysql_result( $result, 0, "price" );
		$price = number_format( $price, 2 );
		$cost = mysql_result( $result, 0, "cost" );
		$cost = number_format( $cost, 2 );	

		echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor=''><td style=\"border:1px solid black;\">&nbsp;$item_code</td><td style=\"border:1px solid black;\">&nbsp;$value</td><td style=\"border:1px solid black;\">&nbsp;$$cost</td><td style=\"border:1px solid black;\">&nbsp;$$price</td>";

		$download_data .= "$item_code,\"$value\",$cost,$price,";

		$date1 = $date;
		$total = 0;
		while( $date1 <= $today ) {
			$fills = Kiosk::fills( $machine_num, $item_code, "$date1 00:00:00", "$date1 23:59:59" );
			$waste = Kiosk::waste( $machine_num, $item_code, "$date1 00:00:00", "$date1 23:59:59" );
			
			if($fills == 0)$fills = "";
			if($waste == 0)$waste = "";
			
			echo "<td style=\"border:1px solid black;\">&nbsp;$fills</td><td style=\"border:1px solid black;\" bgcolor=#E8E7E7>&nbsp;$waste</td>";
			$download_data .= "$fills,$waste,";
			
			////totals
			$row_fill_cost += $cost * $fills;
			$row_fill_total += $fills;
			
			$row_waste_cost += $cost * $waste;
			$row_waste_total += $waste;
			
			$col_fill_cost[$date1] += $cost * $fills;
			$col_fill_total[$date1] += $fills;
			
			$col_waste_cost[$date1] += $cost * $waste;
			$col_waste_total[$date1] += $waste;
			
			$date1 = nextday($date1);
		}
		
		$row_fill_cost = number_format($row_fill_cost, 2);
		$row_waste_cost = number_format($row_waste_cost, 2);

		echo "<td style=\"border:1px solid black;\">&nbsp;$row_fill_total</td><td style=\"border:1px solid black;\">&nbsp;$$row_fill_cost</td>";
		echo "<td style=\"border:1px solid black;\" bgcolor=#E8E7E7>&nbsp;$row_waste_total</td><td style=\"border:1px solid black;\" bgcolor=#E8E7E7>&nbsp;$$row_waste_cost</td>";
		echo "</tr>";
		$download_data .= "$row_fill_total,$$row_fill_cost,$row_waste_total,$$row_waste_cost\r\n";
	}
	echo "</tbody><tfoot>";
	echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor=''><td style=\"border:1px solid black;\" align=right colspan=4><b>Total Items</b>&nbsp;</td>";
	
	if (is_array($col_fill_total)){
		foreach($col_fill_total AS $col_date => $value){	
			echo "<td style=\"border:1px solid black;\">&nbsp;$value</td><td style=\"border:1px solid black;\" bgcolor=#E8E7E7>&nbsp;{$col_waste_total[$col_date]}</td>";
		}
	}
	
	echo "<td colspan=4 style=\"border:1px solid black;\">&nbsp;</td></tr><tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor=''><td style=\"border:1px solid black;\" align=right colspan=4><b>Total Cost</b>&nbsp;</td>";
	
	if (is_array($col_fill_cost)){
		foreach($col_fill_cost AS $col_date => $value){
			$value = number_format($value, 2);
			$value2 = number_format($col_waste_cost[$col_date] , 2);
			echo "<td style=\"border:1px solid black;\">&nbsp;$$value</td><td style=\"border:1px solid black;\" bgcolor=#E8E7E7>&nbsp;$$value2</td>";
		}
	}
	
	echo "<td colspan=4 style=\"border:1px solid black;\">&nbsp;</td></tr></tfoot></table><br>";

?>