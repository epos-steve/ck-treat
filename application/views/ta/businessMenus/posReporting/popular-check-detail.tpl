<?php

if (($businessid == 1380) || ($businessid == 1381)){
	$max_check_total = 3.25;
} else if (($businessid == 1558) || ($businessid == 0)){
	$max_check_total = 3.50;
} else {
	$max_check_total = 0;
}

$sql = "call mburris_report_tables.zz_load_checks_data(" . $businessid . ", 1, '" . $t_date1 . "', '" . $t_date2 . "')";
$result = Treat_DB_ProxyOldProcessHost::query( $sql );

$sql1 = "TRUNCATE mburris_report_tables.zz_check_detail_orig";
Treat_DB_ProxyOldProcessHost::query( $sql1 );

$sql1a = "INSERT INTO mburris_report_tables.zz_check_detail_orig SELECT * FROM mburris_report_tables.zz_check_detail";
Treat_DB_ProxyOldProcessHost::query( $sql1a );
//echo "SQL: " . $sql . "<br>";

$sql1 = "TRUNCATE mburris_report_tables.zz_payment_detail";
Treat_DB_ProxyOldProcessHost::query( $sql1 );

$sql1 = "insert ignore into mburris_report_tables.zz_payment_detail ( businessid, check_number, payment_type, received, scancode, customerid, purchase_bill_datetime, purchase_date, total_visits ) select pd.businessid, pd.check_number, pd.payment_type, pd.received, pd.scancode, pd.customerid, pc.bill_datetime, date(pc.bill_datetime) as purchase_date, count(*) total_visits from mburris_report_tables.posreports_payment_detail pd join mburris_report_tables.posreports_checks pc using (check_number) where pd.payment_type = 14 group by pd.customerid , purchase_date";
$result1 = Treat_DB_ProxyOldProcessHost::query( $sql1 );

$sql5 = "call mburris_report_tables.build_check_detail(" . $businessid . ", 1)";
//$result5 = Treat_DB_ProxyOldProcessHost::query( $sql5 );

$sql6 = "call mburris_report_tables.build_check_detail(" . $businessid . ", 2)";
//$result6 = Treat_DB_ProxyOldProcessHost::query( $sql6 );

$sql7 = "call mburris_report_tables.build_check_detail(" . $businessid . ", 3)";
//$result7 = Treat_DB_ProxyOldProcessHost::query( $sql7 );

$sql8 = "call mburris_report_tables.build_check_detail(" . $businessid . ", 4)";
//$result8 = Treat_DB_ProxyOldProcessHost::query( $sql8 );

$sql1 = "TRUNCATE mburris_report_tables.zz_check_detail_orig";
Treat_DB_ProxyOldProcessHost::query( $sql1 );

$sql1a = "INSERT INTO mburris_report_tables.zz_check_detail_orig SELECT * FROM mburris_report_tables.zz_check_detail";
Treat_DB_ProxyOldProcessHost::query( $sql1a );

?>

<div id="pd-received-amount-2">
<table class="sortable" style="border:1px solid black;font-size:12px;background-color:white;" cellspacing=0 cellpadding=1>
<!--th colspan="6">Customers who made 1 trip to kiosk</th-->
<tr bgcolor=#FFFF99><td id="pdra-1-title">Customer Count</td><td id="pdra-2-title">Item Count</td><td id="pdra-3-title">Total Item Purchases</td><td id="pdra-4-title">Min Spent</td><td id="pdra-5-title">Max Spent</td><td id="pdra-6-title">Average</td></tr>
<?php

	$sql3 = "SELECT total_items_purchased, COUNT(*) customer_count, SUM(sum_items_purchased) total_checks, MIN(sum_items_purchased) as 'min_spent', MAX(sum_items_purchased) as 'max_spent', ROUND(AVG(sum_items_purchased), 2) as 'average' FROM mburris_report_tables.zz_check_detail GROUP BY total_items_purchased order by customer_count desc";
	$result3 = Treat_DB_ProxyOldProcessHost::query( $sql3 );

	while ($r3 = mysql_fetch_array($result3)){
	
		$total_items_purchased = $r3['total_items_purchased'];
		$customer_count = $r3['customer_count'];
		$total_checks = $r3['total_checks'];
		$min_spent = $r3['min_spent'];
		$max_spent = $r3['max_spent'];
		$average = $r3['average'];
		
		echo "<tr><td class=\"pdra-3-a\">" . $customer_count . "</td>" . "<td class=\"pdra-3-a\">" . $total_items_purchased . "</td>" . "<td class=\"pdra-3-a\">$" . $total_checks . "</td>" . "<td class=\"pdra-3-a\">$" . $min_spent . "</td>" . "<td class=\"pdra-3-a\">$" . $max_spent . "</td>" . "<td class=\"pdra-3-a\">$" . $average . "</td>" . "</tr>";

	}

?>
</table>
</div>


<div id="pd-received-amount-3">
<table class="sortable" style="border:1px solid black;font-size:12px;background-color:white;" cellspacing=0 cellpadding=1>
<tr bgcolor=#FFFF99><td id="pdra-1-title">Item Count</td><td id="pdra-2-title">Subtotal</td><td id="pdra-2-title">Discounts</td><td id="pdra-2-title">Total</td></tr>
<?php

	$sql3 = "SELECT items_purchased_count, sum_items_purchased, sum_discounts_of_check, check_total FROM mburris_report_tables.zz_test_2 WHERE 1 ORDER BY zz_test_2.items_purchased_count ASC, check_total asc ";
	$result3 = Treat_DB_ProxyOldProcessHost::query( $sql3 );

	while ($r3 = mysql_fetch_array($result3)){
	
		$items_purchased_count = $r3['items_purchased_count'];
		$sum_items_purchased = $r3['sum_items_purchased'];
		$sum_discounts_of_check = $r3['sum_discounts_of_check'];
		$check_total = $r3['check_total'];
		
		echo "<tr><td class=\"pdra-3-a\">" . $items_purchased_count . "</td>" . "<td class=\"pdra-3-a\">" . $sum_items_purchased . "</td>" . "<td class=\"pdra-3\">$" . $sum_discounts_of_check . "</td>" . "<td class=\"pdra-3\">$" . $check_total . "</td>" . "</tr>";

	}

?>
</table>
</div>


<div id="checkdetail-common-items-purchased-2">
<table class="sortable" style="border:1px solid black;font-size:12px;background-color:white;" cellspacing=0 cellpadding=1>
<!--th colspan="5">Common Menu Item Cluster for 3 items per check</th-->
<tr bgcolor=#FFFF99><!--td id="ccip-0-title">Location</td--><td id="ccip-1-title">Most Common 3 Items Per Check</td><td id="ccip-2-title">Occurrences</td><td id="ccip-3-title">Item Total</td><td id="ccip-3a-title">Current Prices</td><td id="ccip-4-title">Total Received</td><td id="ccip-5-title">New Suggested Prices</td><td id="ccip-5a-title">New Item Total</td><td id="ccip-6-title">New Total Received</td><td id="ccip-7-title">Increased Sales</td></tr>
<?php
	
	$sql4a = "call mburris_report_tables.customer_check_detail(3)";
	Treat_DB_ProxyOldProcessHost::query( $sql4a );

	$sql4 = "SELECT businessid, item_number_group, occurrences, sum_items_purchased, total_received, cluster_amount FROM mburris_report_tables.zz_zz_final_data WHERE cluster_amount = 3 LIMIT 30";
	$result4 = mysql_query($sql4);
	$result4 = Treat_DB_ProxyOldProcessHost::query( $sql4 );
	
	$item_name_grouping = "";
	$new_suggested_pricing = "";
	$new_total_received = 0;

	while ($r4 = mysql_fetch_array($result4)){
	
		$businessid = $r4['businessid'];
		$checkdetail_item_number_group = $r4['item_number_group'];
		$occurrences = $r4['occurrences'];
		$sum_items_purchased = $r4['sum_items_purchased'];
		$total_received = $r4['total_received'];
				
		$item_name = explode(",", $checkdetail_item_number_group);
		$item0 = $item_name[0];
		$item1 = $item_name[1];
		$item2 = $item_name[2];
		
		$item_name_0_query = "SELECT m1.name AS 'item_name_0', m2.price AS 'item_price_0' FROM posreports_menu_items_new m1 join posreports_menu_items_price m2 on m1.id = m2.menu_item_id WHERE m1.pos_id = " . $item0 . " LIMIT 1";
		$item_name_1_query = "SELECT m1.name AS 'item_name_1', m2.price AS 'item_price_1' FROM posreports_menu_items_new m1 join posreports_menu_items_price m2 on m1.id = m2.menu_item_id WHERE m1.pos_id = " . $item1 . " LIMIT 1";
		$item_name_2_query = "SELECT m1.name AS 'item_name_2', m2.price AS 'item_price_2' FROM posreports_menu_items_new m1 join posreports_menu_items_price m2 on m1.id = m2.menu_item_id WHERE m1.pos_id = " . $item2 . " LIMIT 1";
		
		$item_name_0_result = Treat_DB_ProxyOldProcessHost::query( $item_name_0_query );
		$item_name_1_result = Treat_DB_ProxyOldProcessHost::query( $item_name_1_query );
		$item_name_2_result = Treat_DB_ProxyOldProcessHost::query( $item_name_2_query );
		
		$item_name_0_value = mysql_result($item_name_0_result, 0, "item_name_0");
		$item_price_0_value = mysql_result($item_name_0_result, 0, "item_price_0");
		
		
		$item_name_1_value = mysql_result($item_name_1_result, 0, "item_name_1");
		$item_price_1_value = mysql_result($item_name_1_result, 0, "item_price_1");
		
		$item_name_2_value = mysql_result($item_name_2_result, 0, "item_name_2");
		$item_price_2_value = mysql_result($item_name_2_result, 0, "item_price_2");
		
		$item_name_grouping = $item_name_0_value . ", " . $item_name_1_value . ", & " . $item_name_2_value;
		if ($total_received < $max_check_total){
			
			$new_rate_increase_query = "select get_price_increase($sum_items_purchased, $max_check_total, .1) as new_rate_increase_value";
			$new_rate_increase_result = Treat_DB_ProxyOldProcessHost::query( $new_rate_increase_query );
			$new_rate_increase_value = Treat_DB_ProxyOldProcessHost::mysql_result($new_rate_increase_result, 0, 'new_rate_increase_value');
			
			$current_pricing = "$" . $item_price_0_value . ", $" . $item_price_1_value . ", $" . $item_price_2_value;
			$item_price_0_value = round((($item_price_0_value * $new_rate_increase_value) - .01), 2);
			$item_price_1_value = round((($item_price_1_value * $new_rate_increase_value) - .01), 2);
			$item_price_2_value = round((($item_price_2_value * $new_rate_increase_value) - .01), 2);
			$new_item_total = $item_price_0_value + $item_price_1_value + $item_price_2_value;
			$new_suggested_pricing = "$" . $item_price_0_value . ", $" . $item_price_1_value . ", $" . $item_price_2_value;
			
			$new_total_received = round((($item_price_0_value + $item_price_1_value + $item_price_2_value) * .90), 2);
			$increased_sales = round((($new_total_received - $total_received) * $occurrences), 2);
			
		} else {
			$new_suggested_pricing = "No Price Changes";
			$new_total_received = $total_received;
			$new_item_total = $total_received;
			$increased_sales = 0;
		}
		
		//echo "<tr><td class=\"ccip-4-a\">" . $businessid . "</td>" . "<td class=\"ccip-4-a\">" . $checkdetail_item_number_group . "</td>" . "<td class=\"ccip-4-a\">" . $occurrences . "</td>" . "<td class=\"ccip-4-a\">$" . $sum_items_purchased . "</td>" . "<td class=\"ccip-4-a\">$" . $total_received . "</td>" . "</tr>";
		echo "<tr><td class=\"ccip-4-a\">" . $item_name_grouping . "</td>" . "<td class=\"ccip-4-a\">" . $occurrences . "</td>" . "<td class=\"ccip-4-a\">$" . number_format($sum_items_purchased, 2) . "</td>" . "<td class=\"ccip-4-a\">" . $current_pricing . "</td>" . "<td class=\"ccip-4-a\">$" . number_format($total_received, 2) . "</td>" . "<td class=\"ccip-4-a\">" . $new_suggested_pricing . "</td>" . "<td class=\"ccip-4-a\">$" . number_format($new_item_total, 2) . "</td>" . "<td class=\"ccip-4-a\">$" . number_format($new_total_received, 2) . "</td>" . "<td class=\"ccip-4-a\">$" . number_format($increased_sales, 2) . "</td>" . "</tr>";
		
		$item_name_grouping = "";
		$new_item_total = 0;

	}

?>
</table>
</div>

<div id="checkdetail-common-items-purchased-1">
<table class="sortable" style="border:1px solid black;font-size:12px;background-color:white;" cellspacing=0 cellpadding=1>
<!--th colspan="5">Common Menu Item Cluster for 2 items per check</th-->
<tr bgcolor=#FFFF99><!--td id="ccip-0-title">Location</td--><td id="ccip-1-title">Most Common 2 Items Per Check</td><td id="ccip-2-title">Occurrences</td><td id="ccip-3-title">Item Total</td><td id="ccip-3a-title">Current Prices</td><td id="ccip-4-title">Total Received</td><td id="ccip-5-title">New Suggested Prices</td><td id="ccip-5a-title">New Item Total</td><td id="ccip-6-title">New Total Received</td><td id="ccip-7-title">Increased Sales</td></tr>
<?php

	$sql4a = "call mburris_report_tables.customer_check_detail(2)";
	Treat_DB_ProxyOldProcessHost::query( $sql4a );

	$sql4 = "SELECT businessid, item_number_group, occurrences, sum_items_purchased, total_received, cluster_amount FROM mburris_report_tables.zz_zz_final_data WHERE cluster_amount = 2 LIMIT 30";
	$result4 = Treat_DB_ProxyOldProcessHost::query( $sql4 );
	
	$item_name_grouping = "";

	while ($r4 = mysql_fetch_array($result4)){
	
		$businessid = $r4['businessid'];
		$checkdetail_item_number_group = $r4['item_number_group'];
		$occurrences = $r4['occurrences'];
		$sum_items_purchased = $r4['sum_items_purchased'];
		$total_received = $r4['total_received'];
		
		$item_name = explode(",", $checkdetail_item_number_group);
		$item0 = $item_name[0];
		$item1 = $item_name[1];
		//$item2 = $item_name[2];
		
		$item_name_0_query = "SELECT m1.name AS 'item_name_0', m2.price AS 'item_price_0' FROM posreports_menu_items_new m1 join posreports_menu_items_price m2 on m1.id = m2.menu_item_id WHERE m1.pos_id = " . $item0 . " LIMIT 1";
		$item_name_1_query = "SELECT m1.name AS 'item_name_1', m2.price AS 'item_price_1' FROM posreports_menu_items_new m1 join posreports_menu_items_price m2 on m1.id = m2.menu_item_id WHERE m1.pos_id = " . $item1 . " LIMIT 1";
		//$item_name_2_query = "SELECT m1.name AS 'item_name_2', m2.price AS 'item_price_2' FROM posreports_menu_items_new m1 join posreports_menu_items_price m2 on m1.id = m2.menu_item_id WHERE m1.pos_id = " . $item2 . " LIMIT 1";
		
		$item_name_0_result = Treat_DB_ProxyOldProcessHost::query( $item_name_0_query );
		$item_name_1_result = Treat_DB_ProxyOldProcessHost::query( $item_name_1_query );
		//$item_name_2_result = Treat_DB_ProxyOldProcessHost::query( $item_name_2_query );
		
		$item_name_0_value = mysql_result($item_name_0_result, 0, "item_name_0");
		$item_price_0_value = mysql_result($item_name_0_result, 0, "item_price_0");
				
		$item_name_1_value = mysql_result($item_name_1_result, 0, "item_name_1");
		$item_price_1_value = mysql_result($item_name_1_result, 0, "item_price_1");
		
		//$item_name_2_value = mysql_result($item_name_2_result, 0, "item_name_2");
		//$item_price_2_value = mysql_result($item_name_2_result, 0, "item_price_2");
		
		$item_name_grouping = $item_name_0_value . " & " . $item_name_1_value;
		if ($total_received < $max_check_total){
			$new_rate_increase_query = "select get_price_increase($sum_items_purchased, $max_check_total, .1) as new_rate_increase_value";
			$new_rate_increase_result = Treat_DB_ProxyOldProcessHost::query( $new_rate_increase_query );
			$new_rate_increase_value = Treat_DB_ProxyOldProcessHost::mysql_result($new_rate_increase_result, 0, 'new_rate_increase_value');

			$current_pricing = "$" . $item_price_0_value . ", $" . $item_price_1_value;;
			$item_price_0_value = round((($item_price_0_value * $new_rate_increase_value) - .01), 2);
			$item_price_1_value = round((($item_price_1_value * $new_rate_increase_value) - .01), 2);
			//$item_price_2_value = round((($item_price_2_value * $new_rate_increase_query) - .01), 2);
			$new_item_total = $item_price_0_value + $item_price_1_value;
			$new_suggested_pricing = "$" . $item_price_0_value . ", $" . $item_price_1_value;
			
			$new_total_received = round((($item_price_0_value + $item_price_1_value) * .90), 2);
			$increased_sales = ($new_total_received - $total_received) * $occurrences;
			
		} else {
			$new_suggested_pricing = "No Price Changes";
			$new_total_received = $total_received;
			$new_item_total = $total_received;
			$increased_sales = 0;
		}
		
		//echo "<tr><td class=\"ccip-4-a\">" . $businessid . "</td>" . "<td class=\"ccip-4-a\">" . $checkdetail_item_number_group . "</td>" . "<td class=\"ccip-4-a\">" . $occurrences . "</td>" . "<td class=\"ccip-4-a\">$" . $sum_items_purchased . "</td>" . "<td class=\"ccip-4-a\">$" . $total_received . "</td>" . "</tr>";
		echo "<tr><td class=\"ccip-4-a\">" . $item_name_grouping . "</td>" . "<td class=\"ccip-4-a\">" . $occurrences . "</td>" . "<td class=\"ccip-4-a\">$" . number_format($sum_items_purchased, 2) . "</td>" . "<td class=\"ccip-4-a\">" . $current_pricing . "</td>" . "<td class=\"ccip-4-a\">$" . number_format($total_received, 2) . "</td>" . "<td class=\"ccip-4-a\">" . $new_suggested_pricing . "</td>" . "<td class=\"ccip-4-a\">$" . number_format($new_item_total, 2) . "</td>" . "<td class=\"ccip-4-a\">$" . number_format($new_total_received, 2) . "</td>" . "<td class=\"ccip-4-a\">$" . number_format($increased_sales, 2) . "</td>" . "</tr>";
		$new_item_total = 0;
		$item_name_grouping = "";

	}

?>
</table>
</div>
