<?php
		$datetime_field = $t_datefield < 2 ? 'bill_datetime' : 'bill_posted';

        $query = "SELECT c.scancode, c.first_name, c.last_name, SUM(pd.received) AS purchases
			FROM customer c
			JOIN checks ch ON ch.businessid = c.businessid AND ch.$datetime_field BETWEEN '$t_date1 00:00:00' AND '$t_date2 23:59:59'
			JOIN payment_detail pd ON pd.businessid = ch.businessid 
				AND pd.check_number = ch.check_number 
				AND pd.payment_type IN (5,14)
				AND pd.scancode = c.scancode
			WHERE c.businessid = $businessid
			GROUP BY c.customerid
			ORDER BY c.last_name, c.first_name";
        $result = Treat_DB_ProxyOldProcessHost::query( $query );

        ?>
        <table class="sortable" style="border:1px solid black;background-color:white;font-size:12px;" cellspacing=0 cellpadding=1 width=98%>
		<thead>
            <tr bgcolor=#FFFF99>
                <td style="border:1px solid black;">Account#</td>
                <td style="border:1px solid black;">Last Name</td>
				<td style="border:1px solid black;">First Name</td>
                <td style="border:1px solid black;">Purchases</td>
            </tr>
        </thead>
        <tbody>
        
        <?php
            while($r = mysql_fetch_array($result)){
                $scancode = $r["scancode"];
				$last_name = $r["last_name"];
				$first_name = $r["first_name"];
				$total += $r["purchases"];
				$purchases = number_format($r["purchases"], 2);
				
				$download_data .= "$scancode,\"$last_name\",\"$first_name\",\"$purchases\"\r\n";
                
                echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor=''>
                        <td style=\"border:1px solid black;\">&nbsp;$scancode</td>
                        <td style=\"border:1px solid black;\">&nbsp;$last_name</td>
                        <td style=\"border:1px solid black;\">&nbsp;$first_name</td>
						<td style=\"border:1px solid black;\" align=right>$$purchases &nbsp;</td>
                     </tr>";
            }
			
			$total = number_format($total, 2);
        ?>
        
        </tbody>
        <tfoot> 
			<tr bgcolor="#E8E7E7">
				<td style="border:1px solid black;" colspan=3>&nbsp;</td>
				<td style="border:1px solid black;" align=right><?php echo "$" . $total; ?>&nbsp;&nbsp;</td>	
			</tr>
        </tfoot>
        </table>
