<?php
	$fix = @$_GET["fix"];
	$taxProb2 = 0;

	?>
	<table class="sortable" style="border:1px solid black;background-color:white;font-size:12px;" cellspacing=0 cellpadding=1 width=98%>
		<thead>
			<tr bgcolor=#FFFF99>
				<td style="border:1px solid black;">Issue</td><td colspan=2 style="border:1px solid black;">Data</td>
			</tr>
			</thead>
			<tbody>
	<?php
		$problems = 0;
	
		////upc
        $query = "SELECT COUNT(id) AS total, 
					upc, 
					name 
					FROM menu_items_new 
					where businessid = $businessid group by upc HAVING total > 1;";
        $result = Treat_DB_ProxyOldProcessHost::query( $query );
		
		while($r = mysql_fetch_array($result)){
			$total = $r["total"];
			$upc = $r["upc"];
			$name = $r["name"];
			
			echo "<tr>
				<td style=\"border:1px solid black;\">
					Duplicate UPC ($total)
				</td>
				<td style=\"border:1px solid black;\">
					$upc
				</td>
				<td style=\"border:1px solid black;\">
					$name
				</td>
			</tr>";
			
			$problems++;
		}
		
		///item code
		$query = "SELECT COUNT(id) AS total, 
					item_code, 
					name 
					FROM menu_items_new 
					where businessid = $businessid group by item_code HAVING total > 1;";
        $result = Treat_DB_ProxyOldProcessHost::query( $query );
		
		while($r = mysql_fetch_array($result)){
			$total = $r["total"];
			$item_code = $r["item_code"];
			$name = $r["name"];
			
			echo "<tr>
				<td style=\"border:1px solid black;\">
					Duplicate Item Code ($total)
				</td>
				<td style=\"border:1px solid black;\">
					$item_code
				</td>
				<td style=\"border:1px solid black;\">
					$name
				</td>
			</tr>";
			
			$problems++;
		}
		
		///characters
		$query = "SELECT name,item_code,upc FROM menu_items_new 
			WHERE businessid = $businessid 
			AND 
			(NOT HEX(name) REGEXP '^([0-7][0-9A-F])*$' 
			OR name LIKE '%\t%'
			OR name LIKE '%\r%'
			OR name LIKE '%\n%');";
        $result = Treat_DB_ProxyOldProcessHost::query( $query );
		
		while($r = mysql_fetch_array($result)){
			$name = $r["name"];
			$item_code = $r["item_code"];
			$upc = $r["upc"];
			
			echo "<tr>
				<td style=\"border:1px solid black;\">
					Bad Character
				</td>
				<td style=\"border:1px solid black;\">
					$item_code
				</td>
				<td style=\"border:1px solid black;\">
					$name
				</td>
			</tr>";
			
			$problems++;
		}
		
		///bad tax
		$query = "SELECT mit . * , mt . * 
			FROM menu_item_tax mit
			JOIN menu_items_new min ON min.businessid = $businessid
			AND min.id = mit.menu_itemid
			JOIN menu_tax mt ON mt.id = mit.tax_id AND mt.businessid != $businessid";
        $result = Treat_DB_ProxyOldProcessHost::query( $query );
		
		while($r = mysql_fetch_array($result)){
			$menu_itemid = $r["menu_itemid"];
			$tax_id = $r["tax_id"];
			
			if($fix == 1){
				$query2 = "DELETE FROM menu_item_tax WHERE menu_itemid = $menu_itemid AND tax_id = $tax_id";
				$result2 = Treat_DB_ProxyOld::query( $query2 );
			}
			else{
				$problems++;
				$taxProb++;
			}
		}
		
		if($taxProb > 0){
			echo "<tr>
				<td style=\"border:1px solid black;\">
					Tax Issue
				</td>
				<td style=\"border:1px solid black;\">
					Mismatch ($taxProb)
				</td>
				<td style=\"border:1px solid black;\">
					<a href=?bid=$businessid&cid=$companyid&report=$report&terminal_no=$current_terminal&fix=1 style=$style><font size=2 color=blue>[FIX]</font></a>
				</td>
			</tr>";
		}
		
		if($problems < 1){
			echo "<tr>
				<td style=\"border:1px solid black;\" colspan=3>
					No problems found.
				</td>
			</tr>";
		}

		///bad tax
		$query1 = "select id from menu_tax where businessid = $businessid";
		$result1 = Treat_DB_ProxyOld::query( $query1 );
		$rows = Treat_DB_ProxyOld::mysql_num_rows($result1);
		
		if ($rows == 1){
			$query1 = "select id from menu_tax where businessid = $businessid";
			$result1 = Treat_DB_ProxyOld::query( $query1 );
			$tax_id_value = Treat_DB_ProxyOld::mysql_result( $result1, 0, "id");
		
		
		$query2 = "select mit.menu_itemid 
			from menu_item_tax mit 
			left join menu_items_new mitn on mit.menu_itemid = mitn.id 
			where mit.tax_id = $tax_id_value and mitn.id is null";
		
        $result2 = Treat_DB_ProxyOld::query( $query2 );
		$rows2 = Treat_DB_ProxyOld::mysql_num_rows($result2);
		$result3 = Treat_DB_ProxyOld::query( $query2 );
		
		
		while($r2 = mysql_fetch_array($result3)){
			$menu_itemid2 = $r2["menu_itemid"];
			
			if($fix == 1){
				$query4 = "DELETE FROM menu_item_tax WHERE menu_itemid = $menu_itemid2 AND tax_id = $tax_id_value LIMIT 1";
				$result4 = Treat_DB_ProxyOld::query( $query4 );
			}
			else{
				$problems2++;
				$taxProb2++;
			}
		}
		
		if (($taxProb2 > 0) && ($rows2 > 0)){
			echo "<tr>
				<td colspan=3 style=\"border:1px solid black;\">
					Sync All Data - There were menu item id values in the tax table that do not exist
				</td>
			</tr>";
			$sql = "call fix_menu_tax($businessid)";
			$fix_menu_tax_result4 = Treat_DB_ProxyOld::query( $sql , true);
		}

		
		}
		echo "</tbody></table><p>";
		$download_disable = "DISABLED";

?>