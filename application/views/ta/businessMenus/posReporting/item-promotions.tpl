<?php
	$datetime_field = $t_datefield < 2 ? 'bill_datetime' : 'bill_posted';
	$query = "
		SELECT
			mitn.item_code,
			mitn.name AS item_name,
			promotions.id AS promo_id_value, 
			promotions.name AS promo_name,
			mip.cost,
			avg(cd1.price) as price,
			promotions.value AS promo_amount,
			promo_type.name AS promo_type,
			SUM(cd1.quantity) AS total_sold,
			SUM(checkdiscounts.amount) AS amount,
			cd1.item_number
		FROM
			checkdiscounts
		JOIN posreports_checks c1 ON
			c1.check_number = checkdiscounts.check_number
			AND c1.businessid = checkdiscounts.businessid
		JOIN promotions ON
			promotions.pos_id = checkdiscounts.pos_id
			AND (
				promotions.businessid = checkdiscounts.businessid
				OR promotions.businessid IS NULL
			)
			AND promotions.promo_trigger = 1
		JOIN posreports_checkdetail cd1 ON
			cd1.bill_number = c1.check_number
			AND cd1.businessid = c1.businessid
		JOIN promo_detail ON
			promo_detail.id = cd1.item_number
			AND promo_detail.promo_id = promotions.id
		JOIN posreports_menu_items_new mitn use index (businessid) ON
			mitn.pos_id = cd1.item_number
			AND mitn.businessid = cd1.businessid
		JOIN posreports_menu_items_price mip ON
			mip.menu_item_id = mitn.id
		JOIN promo_type ON
			promo_type.id = promotions.type
		WHERE
			c1.businessid = $businessid
			AND c1.{$datetime_field} BETWEEN '$t_date1 00:00:00' AND '$t_date2 23:59:59'
		-- GROUP BY mitn.pos_id, promotions.id
		group by promotions.name, cd1.item_number
	";
	$result = Treat_DB_ProxyOldProcessHost::query( $query );

	echo "<table class=\"sortable\" style=\"border:1px solid black;font-size:12px;background-color:white;\" cellspacing=0 cellpadding=1 width=98%>";
	echo "<thead><tr bgcolor=#FFFF99><td style=\"border:1px solid black;\">&nbsp;Item Code</td>
				<td style=\"border:1px solid black;\">&nbsp;Item Name</td>
				<td style=\"border:1px solid black;\">&nbsp;Promo Name</td>
				<td style=\"border:1px solid black;text-align:right;\">Cost&nbsp;</td>
				<td style=\"border:1px solid black;text-align:right;\">Price&nbsp;</td>
				<td style=\"border:1px solid black;text-align:right;\">Promo Amount&nbsp;</td>
				<td style=\"border:1px solid black;\">&nbsp;Promo Type</td>
				<td style=\"border:1px solid black;text-align:right;\">Total Sold&nbsp;</td>
				<td style=\"border:1px solid black;text-align:right;\">Amount&nbsp;</td>
			</tr></thead><tbody>";

	$download_data .= "Item Code,Item Name, Promo Name, Cost, Avg. Price, Promo Amount, Promo Type, Total Sold, Amount\r\n";

	while($r = Treat_DB_ProxyOldProcessHost::mysql_fetch_array($result)){
		$item_code = $r["item_code"];
		$promo_id_value = $r["promo_id_value"];
		$item_name = $r["item_name"];
		$promo_name = $r["promo_name"];
		$cost = $r["cost"];
		$price = $r["price"];
		$promo_amount = $r["promo_amount"];
		$promo_type = $r["promo_type"];
		$total_sold = $r["total_sold"];
		$amount = $r["amount"];
		$item_number = $r['item_number'];

		$cost = number_format($cost,2);
		$price = number_format($price,2);
		$amount = number_format($amount,2);
		$promo_amount = number_format($promo_amount,2);
		
		$query_total_sold = "SELECT count(distinct cd1.id) AS total_sold FROM checkdiscounts cd2 JOIN posreports_checks c1 ON c1.check_number = cd2.check_number AND c1.businessid = cd2.businessid JOIN promotions p1 ON p1.pos_id = cd2.pos_id AND ( p1.businessid = cd2.businessid OR p1.businessid IS NULL ) AND p1.promo_trigger = 1 JOIN posreports_checkdetail cd1 ON cd1.bill_number = c1.check_number AND cd1.businessid = c1.businessid JOIN promo_detail ON promo_detail.id = cd1.item_number AND promo_detail.promo_id = p1.id JOIN posreports_menu_items_new mitn ON mitn.pos_id = cd1.item_number AND mitn.businessid = cd1.businessid JOIN posreports_menu_items_price mip ON mip.menu_item_id = mitn.id and mitn.businessid = mip.businessid JOIN promo_type ON promo_type.id = p1.type WHERE c1.businessid = $businessid AND c1.{$datetime_field} BETWEEN '$t_date1 00:00:00' AND '$t_date2 23:59:59' AND mitn.item_code = '$item_code' AND p1.id = '$promo_id_value' and cd1.item_number = '$item_number'";
		
		$result_total_sold = Treat_DB_ProxyOldProcessHost::query( $query_total_sold );
		$total_sold = Treat_DB_ProxyOldProcessHost::mysql_result($result_total_sold, 0, 'total_sold');
		
		$amount = number_format(($price * $total_sold),2);

		echo "<tr><td style=\"border:1px solid black;\">&nbsp;$item_code</td>
				<td style=\"border:1px solid black;\">&nbsp;$item_name</td>
				<td style=\"border:1px solid black;\">&nbsp;$promo_name</td>
				<td style=\"border:1px solid black;text-align:right;\">$$cost&nbsp;</td>
				<td style=\"border:1px solid black;text-align:right;\">$$price&nbsp;</td>
				<td style=\"border:1px solid black;text-align:right;\">$promo_amount&nbsp;</td>
				<td style=\"border:1px solid black;\">&nbsp;$promo_type</td>
				<td style=\"border:1px solid black;text-align:right;\">$total_sold&nbsp;</td>
				<td style=\"border:1px solid black;text-align:right;\">$$amount&nbsp;</td>
			</tr>
		";

		$download_data .= "$item_code,\"$item_name\",\"$promo_name\",$cost,$price,$promo_amount,\"$promo_type\",$total_sold,$amount\r\n";
	}
	echo "</tbody></table><br>";

?>