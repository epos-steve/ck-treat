<?php
	$date = $t_date1;
	$today = $t_date2;
	$date2 = date( "Y-m-d" );
	
	echo "<table class=\"sortable\" style=\"border:1px solid black;font-size:12px;background-color:white;\" cellspacing=0 cellpadding=1 width=98%>";
	echo "<thead><tr bgcolor=#FFFF99><td style=\"border:1px solid black;\" width=20%>Item Code</td><td style=\"border:1px solid black;\" width=20%>Item</td><td style=\"border:1px solid black;\" width=20%>Qty</td><td style=\"border:1px solid black;\" width=20%>Cost</td><td style=\"border:1px solid black;\" width=20%>Price</td></thead><tbody>";

	$download_data .= "Item Code,Item,Qty,Cost,Price\r\n";
	
	$totalAmt = 0;
	$totalQty = 0;
	
	if($ordertype > 0){
		$havingQry = "AND min.ordertype = $ordertype";
	}
	else{
		$havingQry = "";
	}
	
	$query = "SELECT min.item_code, min.name, SUM( ROUND( cd.quantity * cd.cost, 2 ) ) AS cost, SUM( ROUND( cd.quantity * cd.price, 2 ) ) AS price, SUM(cd.quantity) AS qty
		FROM checks c
		JOIN checkdetail cd ON cd.businessid = c.businessid
		AND cd.bill_number = c.check_number
		JOIN menu_items_new min ON min.pos_id = cd.item_number $havingQry
		AND min.businessid = cd.businessid
		WHERE c.bill_posted
		BETWEEN  '$date 00:00:00'
		AND  '$today 23:59:59'
		AND c.businessid =$businessid
		GROUP BY min.id
		ORDER BY min.name";
	$result = Treat_DB_ProxyOldProcessHost::query( $query );
	
	while($r = mysql_fetch_array($result)){
		$item_code = $r["item_code"];
		$name = $r["name"];
		$qty = $r["qty"];
		$cost = $r["cost"];
		$price = $r["price"];
		
		$qty = round($qty);
		
		$totalQty += $qty;
		$totalAmt += $cost;
		$totalPrice += $price;
		
		$cost = number_format($cost, 2);
		$price = number_format($price, 2);
		
		echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor=''><td style=\"border:1px solid black;\">&nbsp;$item_code</td><td style=\"border:1px solid black;\">&nbsp;$name</td><td style=\"border:1px solid black;\" align=right>$qty&nbsp;</td><td style=\"border:1px solid black;\" align=right>$$cost&nbsp;</td><td style=\"border:1px solid black;\" align=right>$$price&nbsp;</td></tr>";
		$download_data .= "$item_code,\"$name\",$qty,$cost,$price\r\n";
	}
	
	$totalAmt = number_format($totalAmt, 2);
	$totalPrice = number_format($totalPrice, 2);
	
	echo "</tbody><tfoot><tr bgcolor=#FFFF99><td style=\"border:1px solid black;\" colspan=2>&nbsp;Totals</td><td style=\"border:1px solid black;\" align=right>$totalQty&nbsp;</td><td style=\"border:1px solid black;\" align=right>$$totalAmt&nbsp;</td><td style=\"border:1px solid black;\" align=right>$$totalPrice&nbsp;</td></tr></tfoot></table>";
?>