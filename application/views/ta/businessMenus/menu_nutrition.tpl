<?php
$title = 'Menu Item Nutrition Settings';

$core_query_items = array(
	'cid' => $company_id,
	'bid' => $business_id,
);
?>

<?php
/*
* /
?>
<pre>$_GET = <?php var_dump( $_GET ); ?></pre>
<pre>$edit_item = <?php var_dump( $edit_item ); ?></pre>
<pre>$item_code = <?php var_dump( $item_code ); ?></pre>
<?php
/*
*/
?>

			<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr bgcolor="#E8E7E7">
					<td colspan="3" align="right">
						<div id="import_dialog" class="ui-hidden"></div>
						
<?php
					$http_qry = array_merge( $core_query_items, array(
						'showall' => $showall,
						'return_url' => urlencode( $return_url )
					) );
?>
						<a
							href="/ta/businessMenus/menu_nutrition_export?<?php
							echo http_build_query( $http_qry );
						?>"
							style="text-decoration:none;"
						>Export</a>
<?php
/*
* /
?>
						|
						<a
							href="/ta/businessMenus/menu_nutrition_import?<?php
							echo http_build_query( $http_qry );
						?>"
							style="text-decoration:none;"
							onclick="menu_nutrition_import(); return false;"
						>Import</a>
<?php
/*
* /
?>
						| <a
							style="text-decoration:none;"
							onclick="return confirm('Make sure you import this data into Excel! Excel will remove leading zero`s in the UPCs. When importing, set the UPC column format to text.');"
							href="/ta/export_menu.php?<?php echo http_build_query( $core_query_items, null, '&amp;' ); ?>"
						>
						<font color="blue">Export</font></a>
						| <a style="text-decoration:none;" href="/ta/buscatermenu4-1.php?<?php echo http_build_query( $core_query_items, null, '&amp;' ); ?>">
						<font color="blue">Multi Item Edit</font></a>&nbsp;
<?php
/*
*/
?>
					</td>
				</tr>
				<tr style="height: 1px; background-color: black; vertical-align: top;">
					<td colspan="3">
					</td>
				</tr>
				<tr valign="top" bgcolor="#E8E7E7">
					<td width="30%">
						<br />
						<form action="" method="get"
							onSubmit="disableForm(this);"
							style="margin: 0; padding: 0; display: inline;"
							id="menu_item_list"
						>
							<div style="margin: 0 auto; width: 255px;">
								<input type="hidden" name="cid" value="<?php echo $company_id; ?>" />
								<input type="hidden" name="bid" value="<?php echo $business_id; ?>" />
								<input type="hidden" name="showall" value="<?php echo $showall; ?>" />
<?php
///choose item
?>
								<select size="27" name="edit_item" style="background-color: #fff; width: 255px;"
									onChange="this.form.submit()"
<?php
/*
* /
?>
									onChange="changePage(this.form.menu_items_new)"
<?php
/*
*/
?>
								>
<?php
/*
* /
?>
									<option value="posMenu?<?php
										$qry = array_merge( $core_query_items, array(
											'search' => $search,
											'search_what' => $search_what,
											'search_for' => $search_for2,
											'id' => $id,
										) );
										echo http_build_query( $qry, null, '&amp;' );
									?>"<?php
										echo $sel;
									?>><?php echo $name; ?></option>
<?php
/*
*/
?>
<?php
	$total = 0;
	foreach ( $item_list as $group_name => $group_items ) {
		if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
			$total += count( $group_items );
		}
		if ( $group_name ) {
?>
									<optgroup label="<?php
										echo $group_name;
									?>" style="border-top: 1px dotted #888">
<?php
		}
		foreach ( $group_items as $item ) {
			$sel = '';
			if ( $item->id_key == $edit_item_v ) {
				$sel = ' selected="selected"';
			}
?>
										<option value="<?php
											echo $item->item_code, ';', $item->id_key;
										?>" style="<?php
											$styles = array(
												'border-top' => 'border-top: 1px dotted #888',
											);
											if ( $item->active ) {
												$styles['background-color'] = 'background-color: #e8e7e7';
												$styles['border-top']    = '';
											}
											if ( !( $item->nutrition_exists || $item->nutrition_exists_upc ) ) {
												$styles['background-color'] = 'background-color: #ff0';
											}
											ksort( $styles );
											echo implode( '; ', $styles );
										?>"<?php
											echo $sel;
										?>><?php echo $item->item_name; ?></option>
<?php
		}
		if ( $group_name ) {
?>
									</optgroup>
<?php
		}
	}
?>
								</select>
<?php
	$http_query = array(
		'cid' => $company_id,
		'bid' => $business_id,
		'showall' => null,
		'edit_item' => null,
	);
	if ( $showall ) {
		$show_link = '[ACTIVE ITEMS]';
	}
	else {
		$show_link = '[ALL ITEMS]';
		$http_query['showall'] = 1;
	}
	if ( $edit_object->item_code || $edit_object->id_key ) {
		$http_query['edit_item'] = $edit_object->item_code . ';' . $edit_item_v;
	}
?>
								<div style="text-align: center;">
									<a href="/ta/businessMenus/menu_nutrition?<?php
										echo http_build_query( $http_query, null, '&amp;' );
									?>" style="color: blue; font-size: 0.63em; text-decoration: none;">
										<?php
											echo $show_link, PHP_EOL;
										?>
									</a>
								</div>
<?php
	if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
		echo '<pre>';
		echo '$total = ';
		var_dump( $total );
		
		echo '$showall = ';
		var_dump( $showall );
		echo '</pre>';
	}
?>
								<div class="button">
									<input type="submit" value="Edit" id="edit_button" />
								</div>
							</div>
						</form>
					</td>
					<td width="40%">
						<form action="/ta/businessMenus/menu-nutrition-save" method="post" onSubmit="return disableForm(this);" style="margin: 0; padding: 0; display: inline;">
							<div style="margin: 0 5%; ">
								<input type="hidden" name="cid" value="<?php echo $company_id; ?>" />
								<input type="hidden" name="bid" value="<?php echo $business_id; ?>" />
								<input type="hidden" name="edit_item" value="<?php echo $edit_item; ?>" />
								<input type="hidden" name="mic_id" value="<?php echo $edit_object->mic_id; ?>" />
								<input type="hidden" name="min_id" value="<?php echo $edit_object->min_id; ?>" />
								<input type="hidden" name="menu_item_id" value="<?php echo $edit_object->menu_item_id; ?>" />
								<input type="hidden" name="showall" value="<?php echo intval( $showall ); ?>" />
								<table cellspacing="0" cellpadding="0" style="margin: 0;" >
									<tr><td colspan="3">&nbsp;</td></tr>
									<tr>
										<td align="right"> Item Name: </td>
										<td colspan="2">
											<input type="text" size="25" name="name" value="<?php
												echo $edit_object->item_name;
											?>"<?php
												echo $disable;
											?> />
										</td>
									</tr>
									<tr>
										<td align="right"> Item Code: </td>
										<td colspan="2">
											<input type="text" size="25" name="item_code" value="<?php
												echo $edit_object->item_code;
											?>"<?php
												echo $disable;
											?> />
										</td>
									</tr>
									<tr>
										<td colspan="3" style="text-align: center; font-weight: bold;">Nutrition</td>
									</tr>
<?php
		if ( $edit_object->item_code && ( is_array( $nutrition_types ) || $nutrition_types instanceof Traversable ) ) {
			unset($nutrition_types->complex_cabs);
			foreach ( $nutrition_types as $nutrition_type ) {
				if ( 'treat_score' != $nutrition_type->column_key ) {
?>
									<tr>
										<td> <?php
											echo $nutrition_type->label;
										?> </td>
										<td>
											<input type="text" size="20" name="nutrition[<?php
												echo $nutrition_type->id;
											?>]" value="<?php
												if ( is_array( $nutrition ) && array_key_exists( $nutrition_type->id, $nutrition ) ) {
													echo round( $nutrition[ $nutrition_type->id ]->value, 2 );
												}
											?>" <?php echo $disable; ?> />
										</td>
										<td> <?php
											echo $nutrition_type->units;
										?> </td>
									</tr>
<?php
				}
			}
		}
?>
									<tr>
										<td colspan="3">
                                                                                    <?php if($buttons_enabled): ?>
											<div class="button">
												<input type="submit" value="Save Item" size="25" <?php echo $disable; ?> />
												<button type="button"
													onclick="document.location.href='/ta/businessMenus/menu-nutrition?<?php
														echo http_build_query( $core_query_items );
													?>'; return false;"
												>Cancel</button>
											</div>
                                                                                    <?php else: ?>
                                                                                        <b>Nutrition already provided</b><br />
																						<div class="button">
												<input type="submit" value="Save Item" size="25" <?php echo $disable; ?> />
												<button type="button"
													onclick="document.location.href='/ta/businessMenus/menu-nutrition?<?php
														echo http_build_query( $core_query_items );
													?>'; return false;"
												>Cancel</button>
											</div>
                                                                                    <?php endif; ?>
										</td>
									</tr>
								</table>
							</div>
						</form>
					</td>
					<td width="30%" valign="top">
<?php
		if ( $edit_object->name_list ) {
?>
						<br />
						<div>All names: <?php
							$separator = ', ';
							if ( false !== strpos( $edit_object->name_list, ',' ) ) {
								$separator = '; ';
							}
							echo implode( $separator, explode( ':', $edit_object->name_list ) );
						?></div>
<?php
/*
* /
?>
						<ul>
<?php
			if ( $edit_object->mic_name_list ) {
				$name_list = explode( ':', $edit_object->mic_name_list );
				foreach ( $name_list as $name ) {
?>
							<li class="mic_name"><?php echo $name; ?></li>
<?php
				}
			}
?>
<?php
			if ( $edit_object->min_name_list ) {
				$name_list = explode( ':', $edit_object->min_name_list );
				foreach ( $name_list as $name ) {
?>
							<li class="min_name"><?php echo $name; ?></li>
<?php
				}
			}
?>
<?php
			if ( $edit_object->mi_name_list ) {
				$name_list = explode( ':', $edit_object->mi_name_list );
				foreach ( $name_list as $name ) {
?>
							<li class="mi_name"><?php echo $name; ?></li>
<?php
				}
			}
?>
						</ul>
<?php
/*
*/
?>
<?php
		}
?>
						
					</td>
				</tr>
				<tr valign="bottom" bgcolor="#E8E7E7">
					<td align="right" colspan="3">
						&nbsp;
					</td>
				</tr>
			</table>
