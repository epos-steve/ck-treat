
<style type="text/css">
	.field { width: 100%; float: left; margin: 0 0 20px; }
	.field input { margin: 0 0 0 20px; }
	.hidden-radio { position: absolute; top: -9999px; }

	td, th { vertical-align: middle; }

	/* Used for the Switch effect: */
	.cb-enable, .cb-disable, .cb-enable span, .cb-disable span { background: url(/assets/images/switch.gif) repeat-x; display: block; float: left; }
	.cb-enable span, .cb-disable span { line-height: 30px; display: block; background-repeat: no-repeat; font-weight: bold; }
	.cb-enable span { background-position: left -90px; padding: 0 10px; }
	.cb-disable span { background-position: right -180px; padding: 0 10px; }
	.cb-disable.selected { background-position: 0 -30px; }
	.cb-disable.selected span { background-position: right -210px; color: #fff; }
	.cb-enable.selected { background-position: 0 -60px; }
	.cb-enable.selected span { background-position: left -150px; color: #fff; }
	.switch label { cursor: pointer; }
</style>

<script type="text/javascript">
$(document).ready(function() {
	var serializedForm = [];

	var dialogHeight = $(window).height() - ($(window).height() * .1);
	if (dialogHeight < 400) {
		dialogHeight = 400;
	}

	var dialogWidth = $(window).width() - ($(window).width() * .05);

	function sendOfflineChange(rawData, onSuccess) {
		var formData = rawData || serializedForm;
		$.post(
			"/ta/kioskutilities/offlineChange",
			formData,
			function(data) {
				onSuccess(data);
			},
			"json"
		);
	}

	function setActiveValue(active) {
		for( key in serializedForm ) {
			if(serializedForm[key]["name"] === "active") {
				serializedForm[key]["value"] = active;
				break;
			}
		}
	}

	function closedDialogOnSuccess(element) {
		$( element ).dialog( "close" );
	}

	function openSetupManagerDialog(data, closed_status) {
		var useBusinessId = data.businessId || 0;
		if (closed_status === 'relocate' && useBusinessId > 0) {
			$('#setup_frame').attr('src', "/ta/setup.php?bid=" + useBusinessId + "&goto=1&setup_status=1");
			$("#dialog-business-setup").dialog("open");
		}
	}

	$('.cb-enable').click(function() {
		var parent = $(this).parents('.switch');
		$('.cb-disable',parent).removeClass('selected');
		$(this).addClass('selected');
		//$('.checkbox',parent).attr('checked', true);
		serializedForm = $(this).closest(".js-offline-change-form").serializeArray();
		setActiveValue(1);
		$("#dialog-activation-schedule").dialog("open");
	});

	$('.cb-disable').click(function() {
		var parent = $(this).parents('.switch');
		$('.cb-enable',parent).removeClass('selected');
		$(this).addClass('selected');
		//$('.checkbox',parent).attr('checked', false);
		serializedForm = $(this).closest(".js-offline-change-form").serializeArray();
		setActiveValue(0);
		$("#dialog-deactivation-schedule").dialog("open");
	});

	$( "#dialog-deactivation-schedule" ).dialog({
		autoOpen: false,
		resizable: false,
		height: 330,
		width: 450,
		modal: true,
		buttons: {
			"Schedule": function() {
				confirmation = confirm("This action is permanent and will prevent undo once it is completed.");
				if(!confirmation) {
					closedDialogOnSuccess(this);
					return;
				}

				var isValid = true;
				var date = $( "#deactivation-date" ).val();
				var reason = $( "#deactivation-reason" ).val();
				var closedStatus = $( ".js-closed-status:checked" ).val();
				var processedData = [];
				for(key in serializedForm) {
					processedData.push(serializedForm[key]);
				}

				if (date === "") {
					$( "#deactivation-date" ).addClass("ui-state-error");
					isValid = false;
				} else {
					$( "#deactivation-date" ).removeClass("ui-state-error");
					processedData.push({"name": "extra[date]", "value": date});
				}

				if (reason === "") {
					$( "#deactivation-reason" ).addClass("ui-state-error");
					isValid = false;
				} else {
					$( "#deactivation-reason" ).removeClass("ui-state-error");
					processedData.push({"name": "extra[reason]", "value": reason});
				}

				if (!closedStatus || closedStatus === "") {
					$( ".js-closed-status" ).addClass("ui-state-error");
					isValid = false;
				} else {
					$( ".js-closed-status" ).removeClass("ui-state-error");
					processedData.push({"name": "extra[closed_status]", "value": closedStatus});
				}

				if ( isValid ) {
					var element = this;
					var closeDialogOnSuccess = function(data) {
						var hasError = "error" in data;
						var hasDidChange = "didChange" in data;
						if (hasDidChange && data.didChange === false) {
							$('#dialog-deactivation-schedule').find('.error').eq(0).text("There is nothing to do.").show();
						}
						if (hasError && data.error !== false) {
							$('#dialog-deactivation-schedule').find('.error').eq(0).text(data.error).show();
						}
						if (hasDidChange && data.didChange === true && (!hasError || (hasError &&  data.error === false))) {
							closed_status = $('#dialog-deactivation-schedule')
								.find('.js-closed-status:checked')
								.val();
							closedDialogOnSuccess(element);
							openSetupManagerDialog(data, closed_status);
						}
					};
					sendOfflineChange(processedData, closeDialogOnSuccess);
				}
			},
			Cancel: function() {
				$( this ).dialog( "close" );
			}
		},
		close: function() {
			serializedForm = [];
			$('#dialog-deactivation-schedule .error').text("").hide();
			$( "#deactivation-date" ).val( "" ).removeClass( "ui-state-error" );
			$( "#deactivation-reason" ).val( "" ).removeClass( "ui-state-error" );
			$( ".js-closed-status:checked" ).removeAttr( "checked" ).removeClass( "ui-state-error" );
		}
	});

	$( "#dialog-activation-schedule" ).dialog({
		autoOpen: false,
		resizable: false,
		height: 215,
		width: 450,
		modal: true,
		buttons: {
			"Schedule": function() {
				var isValid = true;
				var date = $( "#activation-date" ).val();
				var processedData = [];
				for(key in serializedForm) {
					processedData.push(serializedForm[key]);
				}

				if (date === "") {
					$( "#activation-date" ).addClass("ui-state-error");
					isValid = false;
				} else {
					$( "#activation-date" ).removeClass("ui-state-error");
					processedData.push({"name": "extra[date]", "value": date});
				}

				if ( isValid ) {
					var element = this;
					var closeDialogOnSuccess = function(data) {
						var hasError = "error" in data;
						var hasDidChange = "didChange" in data;
						if (hasDidChange && data.didChange === false) {
							$('#dialog-activation-schedule').find('.error').eq(0).text("There is nothing to do.").show();
						}
						if (hasError && data.error !== false) {
							$('#dialog-activation-schedule').find('.error').eq(0).text(data.error).show();
						}
						if (hasDidChange && data.didChange === true && (!hasError || (hasError && data.error === false))) {
							closedDialogOnSuccess(element);
						}
					};
					sendOfflineChange(processedData, closeDialogOnSuccess);
				}
			},
			Cancel: function() {
				$( this ).dialog( "close" );
			}
		},
		close: function() {
			serializedForm = [];
			$('#dialog-activation-schedule .error').text("").hide();
			$( "#activation-date" ).val( "" ).removeClass( "ui-state-error" );
		}
	});

	$( "#dialog-status-log" ).dialog({
		autoOpen: false,
		resizable: true,
		height: dialogHeight,
		width: 600,
		modal: true,
		buttons: {
			"Close": function() {
				$( this ).dialog( "close" );
			}
		},
		close: function() {
			$("#status-log-contents").empty();
		}
	});

	$( "#dialog-business-setup" ).dialog({
		autoOpen: false,
		resizable: false,
		height: dialogHeight,
		width: dialogWidth,
		modal: true,
		buttons: {
			"Close": function() {
				$( this ).dialog( "close" );
			}
		},
		open: function(event, ui) {
			var obj = $('#dialog-business-setup');
			var paddingLeft = Math.ceil(parseFloat(obj.css('paddingLeft')));
			var paddingRight = Math.ceil(parseFloat(obj.css('paddingRight')));
			var paddingTop = Math.ceil(parseFloat(obj.css('paddingTop')));
			var paddingBottom = Math.ceil(parseFloat(obj.css('paddingBottom')));
			var innerWidth = Math.floor(obj.innerWidth()) - paddingLeft - paddingRight - 5;
			var innerHeight = Math.floor(obj.innerHeight()) - paddingTop - paddingBottom - 22;
			$('#setup_frame').width(innerWidth).height(innerHeight);
		},
		close: function() {
			// Close
		}
	});

	function buildRow(element, showDelete) {
		var row = $('#dialog-status-log tfoot').clone(false)
			.find('.js-status-log-date').text(element.date).end()
			.find('.js-status-log-scheduled').text(element.scheduled).end()
			.find('.js-status-log-name').text(element.name).end()
			.find('.js-status-log-status').text(element.status).end();
		if (showDelete) {
			$('.js-status-log-delete', row).attr('data-log-action-id', element.id).show();
		}
		return row;
	}

	function traverseStatusLogRows(ignored, element) {
		var showDelete = (element.scheduled !== '' && element.deleted === false);
		var row = buildRow(element, showDelete);
		if (element.deleted !== false) {
			$('tr', row).css('text-decoration', 'strikethrough');
			var deletedRow = buildRow(element.deleted, false, false);
			$('.js-status-log-date', deletedRow).css('text-align', 'right');
			$('#status-log-contents').append($(row).html());
			$('#status-log-contents').append($(deletedRow).html());
			return;
		}

		$('#status-log-contents').append($(row).html());
	}

	function updateStatusLog(link) {
		$("#status-log-contents").empty();
		$.getJSON(
			link,
			function(data) {
				$("#dialog-status-log").find(".dialog-error-row").remove();
				if (data.error && data.error !== '') {
					$('#status-log-contents').append('<tr class="dialog-error-row"><td colspan="5" style="color: red; font-weight: bold; text-align: center;">'+ data.error +'</td></tr>');
				}
				if (!$.isArray(data) || data.length < 1) {
					$('#status-log-contents').append('<tr class="dialog-error-row"><td colspan="5" style="text-align: center;">No changes were made.</td></tr>');
				} else {
					$.each(data, traverseStatusLogRows);
				}

				$("#dialog-status-log").dialog("open");
			},
			"json"
		);
	}

	$('.js-status-log-delete').live('click', function(event) {
		var element = this;
		$.post(
			'/ta/kioskutilities/removeOfflineChangeSchedule',
			{
				"log_action_id": $(this).attr('data-log-action-id')
			},
			function(data) {
				$("#dialog-status-log").find(".dialog-error-row").remove();
				if (data.error && data.error !== '') {
					$(element).closest("tr").after('<tr class="dialog-error-row"><td colspan="5" style="color: red; font-weight: bold; text-align: center;">'+ data.error +'</td></tr>');
				}
				if (data.didChange === true) {
					updateStatusLog(data.link);
				}
			},
			"json"
		);
	});

	$('.js-popup-inline').click(function(event) {
		event.preventDefault();
		updateStatusLog($(this).attr('href'));
	});
});
</script>

<div id="dialog-business-setup" title="Setup">
	<iframe name="setup_frame" id="setup_frame" src="/ta/setup.php?bid=0&goto=1" width="99%" height="525"></iframe>
</div>

<div id="dialog-activation-schedule" title="Schedule Activation">
	<p style="color: red; font-weight: bold; display: none;" class="error"></p>
	<form>
		<fieldset>
			<label for="deactivation-date">Schedule Date</label>
			<input type="date" name="date" id="activation-date" class="text ui-widget-content ui-corner-all datepicker">
		</fieldset>
	</form>
</div>

<div id="dialog-deactivation-schedule" title="Schedule Deactivation">
	<p>All form fields are required.</p>
	<p style="color: red; font-weight: bold; display: none;" class="error"></p>
	<form>
		<fieldset>
			<table style="width: 100%;">
				<tr>
					<td><label for="deactivation-date">Schedule Date</label></td>
					<td><input type="date" name="date" id="deactivation-date" class="text ui-widget-content ui-corner-all datepicker"></td>
				</tr>
				<tr>
					<td><label for="deactivation-reason">Reason</label></td>
					<td><input type="text" name="reason" id="deactivation-reason" class="text ui-widget-content ui-corner-all" style="width: 100%;"></td>
				</tr>
				<tr>
					<td>Closed Status</td>
					<td>
						<label>
							<input type="radio" name="closed_status" value="permanent" class="js-closed-status ui-widget-content">
							Warehouse
						</label>
						<label>
							<input type="radio" name="closed_status" value="relocate" class="js-closed-status ui-widget-content">
							Transfer
						</label>
					</td>
				</tr>
			</table>
		</fieldset>
	</form>
</div>

<div id="dialog-status-log" title="Status Log">
	<table style="width: 100%;">
		<thead>
			<tr>
				<th style="text-align: left;">Date</th>
				<th>Name</th>
				<th>Status</th>
				<th>Scheduled</th>
				<th></th>
			</tr>
		</thead>
		<tfoot style="display: none;">
			<tr>
				<td style="text-align: left;" class="js-status-log-date"></td>
				<td style="text-align: center;" class="js-status-log-name"></td>
				<td style="text-align: center;" class="js-status-log-status"></td>
				<td style="text-align: center;" class="js-status-log-scheduled"></td>
				<td style="text-align: center; width: 25px;">
					<img src="/assets/images/delete.gif" class="js-status-log-delete" data-log-action-id="0" style="display:none; cursor: pointer;">
				</td>
			</tr>
		</tfoot>
		<tbody id="status-log-contents"></tbody>
	</table>
</div>

<table cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td valign="top">
			<h2>Kiosk Management</h2>
<?php
$today = strtotime('today 11:59:59');
foreach($districts as $district_id => $businesses) :
?>

			<fieldset>
				<legend><?php echo $districtTitles[ $district_id ]; ?></legend>
				<table width="100%">
					<thead>
						<td><b>Business</b></td>
						<td><b>Terminal</b></td>
						<td><b>Status</b></td>
						<td><b>Condition</b></td>
						<td></td>
						<td title="Scheduled change for active or inactive"><b>Scheduled</b></td>
					</thead>
					<tbody>
<?php
	foreach($businesses as $kiosks) :
		foreach($kiosks as $num => $kiosk) :
?>

						<tr>
							<td>
<?php
			if ( $num < 1 ) :
				if($kiosk['setup'] == 1) :
?>

								<strong style="color: orange;">NEW</strong>
<?php
				endif;
				echo "\t\t\t\t\t\t\t\t\t\t\t".$kiosk['businessname'];
			endif;
?>

							</td>
							<td width="35%">
								<a href="/ta/kioskutilities/offlineReport?cpid=<?php echo $kiosk['client_programid']; ?>&cid=<?php echo $companyId; ?>">
									<?php echo $kiosk['name']; ?>

								</a>
							</td>
							<td width="210px" valign="middle">
<?php
			if ( $num < 1 ) :
?>

								<a href="/ta/kioskutilities/offlineChangeLog/<?php echo $kiosk['businessid']; ?>" class="js-popup-inline" style="float: right; width: 17px; margin: 0px; padding: 0px;">
									<img src="/assets/images/folder.gif" title="Status Log" alt="Log" style="width: 16px; height: 16px;" />
								</a>
								<form action="/ta/kioskutilities/offlineChange" class="js-offline-change-form" method="POST" style="border:0px;padding:0px;margin:0px;">
									<input type="hidden" name="businessid" class="js-kiosk-business-id" value="<?php echo $kiosk['businessid']; ?>">
									<input type="hidden" name="client_programid" value="<?php echo $kiosk['client_programid']; ?>">
									<p class="field switch" title="<?php echo $kiosk['active_message']; ?>" style="border: 0px; padding: 0px; margin: 0px; width: 180px;">
										<input type="radio"<?php if($kiosk['active'] == 1) : ?> checked="checked"<?php endif; ?>
											   id="active-<?php echo $kiosk['businessid']; ?>"
											   class="hidden-radio"
											   name="active"
											   value="1" />
										<input type="radio"<?php if($kiosk['active'] == 0) : ?> checked="checked"<?php endif; ?>
											   id="inactive-<?php echo $kiosk['businessid']; ?>"
											   class="hidden-radio"
											   name="active"
											   value="0" />
										<label for="active-<?php echo $kiosk['businessid']; ?>"
											   class="<?php if($kiosk['active'] == 1) : ?>cb-enable selected<?php else : ?>cb-enable<?php endif; ?>">
											<span>ACTIVE</span>
										</label>
										<label for="inactive-<?php echo $kiosk['businessid']; ?>"
											   class="<?php if($kiosk['active'] == 0) : ?>cb-disable selected<?php else : ?>cb-disable<?php endif; ?>">
											<span>INACTIVE</span>
										</label>
									</p>
								</form>
<?php
			endif;
?>

							</td>
							<td width="10%">
<?php
			if ($kiosk['online'] >= '00:15:00') :
?>

								<strong style="color: <?php if($kiosk['value'] == 1) : ?>red<?php else: ?>#404040<?php endif; ?>;">
									OFFLINE
								</strong>
<?php
			else :
?>

								<strong style="color: green;">ONLINE</strong>
<?php
			endif;
?>

							</td>
							<td width="10%">
								<?php
								if($kiosk['online'] >= '00:15:00') :
									echo $kiosk['online'];
								endif;
								?>

							</td>
							<td width="10%">
								<?php
								if(trim($kiosk['scheduled_date']) !== '' && strtotime($kiosk['scheduled_date']) > $today) :
									echo date('Y-m-d', strtotime($kiosk['scheduled_date']));
								endif;
								?>

							</td>
						</tr>

<?php
		endforeach;
	endforeach;
?>

					</tbody>
				</table>
			</fieldset>
<?php
endforeach;
?>
<?php ////total ?>

			<fieldset>
				<legend><b>Totals</b></legend>
				<table width="100%">
					<thead>
						<tr>
							<td><b>Active Kiosks</b></td>
							<td><b>New Kiosks</b></td>
							<td><b>Offline Kiosks</b></td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><?php echo $activeTotal; ?></td>
							<td><?php echo $newKiosks; ?></td>
							<td><?php echo $offlineTotal; ?></td>
						</tr>
					</tbody>
				</table>
			</fieldset>
		</td>
	</tr>
</table>