<script language="JavaScript" type="text/javascript">
	function changePage(newLoc)
	{
		nextPage = newLoc.options[newLoc.selectedIndex].value

		if (nextPage != "")
		{
			document.location.href = nextPage
		}
	}

	function delconfirm(){
		return confirm('ARE YOU SURE YOU WANT TO DELETE THIS ROUTE?');
	}
</script>

<style>
.hidden{display:none;}
</style>

<table cellspacing='0' cellpadding='0' border='0' width='100%'>
	<tr bgcolor='#F6F9FA'>
		<td colspan='2' align='right'>&nbsp;</td>
	</tr>
	<tr style='height: 1px; background-color: black; vertical-align: top;'>
		<td colspan='2'>
			<img src='/assets/images/spacer.gif' alt='' />
		</td>
	</tr>
	
	<tr valign=top bgcolor=#E8E7E7>
		<td width=25% rowspan=3>
			&nbsp;
		</td>
		<td width=75%>
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td valign="top">
						<h2>Route Management</h2>
						
						<table>
							<tr bgcolor="#e8e7e7">
								<td colspan="2">
									<form action="#" method="post">
										Route:
										<select name="user" onChange="changePage(this.form.user)">
											<?php print $user_select; ?>
										</select>
									</form>
								</td>
							</tr>
							
							<tr><td colspan="2">&nbsp;</td></tr>
							
						<?php if ( $edituser ) { ?>
							<tr bgcolor="#e8e7e7">
								<td colspan="2">
									<form action="<?php print $docroot; ?>edit_venduser.php" method="post">
										<input type="hidden" name="login_routeid" value="<?php print $login_routeid; ?>" />
										<input type="hidden" name="edit" value="1" />
										<input type="hidden" name="redirect" value="<?php print $redirect; ?>" />
										
										<table bgcolor="#ffff99" style="border:1px solid #000;">
											<tr>
												<td align="right">
													<b>Route#</b>:
												</td>
												<td>
													<input type="text" size="5" name="route" value="<?php print $route; ?>" />
													<font size="2">
														<i>
															*Should be at least 3 digits long (ie 001) for route 1.
														</i>
													</font>
												</td>
											</tr>
											<tr>
												<td align="right">
													Truck#:
												</td>
												<td>
													<input type="text" size="5" name="truck" value="<?php print $truck; ?>" />
												</td>
											</tr>
											<tr>
												<td align="right">
													Last Name:
												</td>
												<td>
													<input type="text" size="30" name="lastname" value="<?php print $lastname; ?>" />
												</td>
											</tr>
											<tr>
												<td align="right">
													First Name:
												</td>
												<td>
													<input type="text" size="30" name="firstname" value="<?php print $firstname; ?>" />
												</td>
											</tr>
											<tr>
												<td align="right">
													ID1:
												</td>
												<td>
													<input type="text" size="10" name="id1" value="<?php print $id1; ?>" />
												</td>
											</tr>
											<tr>
												<td align="right">
													ID2:
												</td>
												<td>
													<input type="text" size="10" name="id2" value="<?php print $id2; ?>" />
												</td>
											</tr>
											<tr>
												<td align="right">
													Location:
												</td>
												<td>
													<select name="newlocid">
														<?php print $newlocid_select; ?>
													</select>
												</td>
											</tr>
											
											<tr class="hidden">
												<td align="right">
													Supervisor:
												</td>
												<td>
													<select name="supervisor">
														<?php print $supervisor_select; ?>
													</select>
												</td>
											</tr>
											<tr class="hidden">
												<td align="right">
													Security:
												</td>
												<td>
													<select name="sec_level">
														<?php print $seclevel_select; ?>
													</select>
												</td>
											</tr>
											<tr class="hidden">
												<td align="right">
													Commission:
												</td>
												<td>
													<select name="newcommissionid">
														<?php print $newcommissionid_select; ?>
													</select>
												</td>
											</tr>
											<tr class="hidden">
												<td align="right">
													Commission Source:
												</td>
												<td>
													<select name="newsourceid">
														<?php print $newsourceid_select; ?>
													</select>
												</td>
											</tr>
											
											<tr>
												<td align="right">
													Order:
												</td>
												<td>
													<select name="order_by_machine">
														<?php print $orderbymachine_select; ?>
													</select>
												</td>
											</tr>
											<tr>
												<td align="right">
													Inactive:
												</td>
												<td>
													<input type="checkbox" name="active" value="1" <?php print $showactive; ?>  />
												</td>
											</tr>
											<tr>
												<td>&nbsp;</td>
												<td>
													<input type="submit" value="Save" />
												</td>
											</tr>
										</table>
									</form>
								</td>
							</tr>
						<?php } else { ?>
							<tr bgcolor="#e8e7e7">
								<td colspan="3">
									<form action="<?php print $docroot; ?>edit_venduser.php" method="post">
										<input type="hidden" name="redirect" value="<?php print $redirect; ?>" />
										
										<table bgcolor="#e8e7e7">
											<tr>
												<td align="right">
													<b>Route#</b>:
												</td>
												<td>
													<input type="text" size="5" name="route" />
													<font size="2">
														<i>
															*Should be at least 3 digits long (ie 001) for route 1.
														</i>
													</font>
												</td>
											</tr>
											<tr>
												<td align="right">
													Truck#:
												</td>
												<td>
													<input type="text" size="5" name="truck" />
												</td>
											</tr>
											<tr>
												<td align="right">
													Last Name:
												</td>
												<td>
													<input type="text" size="30" name="lastname" />
												</td>
											</tr>
											<tr>
												<td align="right">
													First Name:
												</td>
												<td>
													<input type="text" size="30" name="firstname" />
												</td>
											</tr>
											<tr>
												<td align="right">
													ID1:
												</td>
												<td>
													<input type="text" size="10" name="id1" />
												</td>
											</tr>
											<tr>
												<td align="right">
													ID2:
												</td>
												<td>
													<input type="text" size="10" name="id2" />
												</td>
											</tr>
											<tr>
												<td align="right">
													Location:
												</td>
												<td>
													<select name="newlocid">
														<?php print $newlocid_select; ?>
													</select>
												</td>
											</tr>
										
											<tr class="hidden">
												<td align="right">
													Supervisor:
												</td>
												<td>
													<select name="supervisor">
														<?php print $supervisor_select; ?>
													</select>
												</td>
											</tr>
											<tr class="hidden">
												<td align="right">
													Security:
												</td>
												<td>
													<select name="sec_level">
														<option value="1">1 - Driver</option>
														<option value="2">2 - Route Mgr</option>
														<option value="3">3 - Admin</option>
													</select>
												</td>
											</tr>
											<tr class="hidden">
												<td align="right">
													Commission:
												</td>
												<td>
													<select name="newcommissionid">
														<option value="0"></option>
														<?php print $newcommissionid_select; ?>
													</select>
												</td>
											</tr>
											<tr class="hidden">
												<td align="right">
													Commission Source:
												</td>
												<td>
													<select name="newsourceid">
														<option value="0"></option>
														<?php print $newsourceid_select; ?>
													</select>
												</td>
											</tr>
										
											<tr>
												<td align="right">
													Order:
												</td>
												<td>
													<select name="order_by_machine">
														<?php print $orderbymachine_select; ?>
													</select>
												</td>
											</tr>
											<tr>
												<td align="right">
													Inactive:
												</td>
												<td>
													<input type="checkbox" name="active" value="1" <?php
														echo $showactive;
													?> />
												</td>
											</tr>
											<tr>
												<td></td>
												<td>
													<input type="submit" value="Add" />
												</td>
											</tr>
										</table>
									</form>
								</td>
							</tr>
						<?php } ?>
						
						<?php if ( $edituser ) { ?>
							<tr bgcolor="#e8e7e7">
								<td colspan="3">
									<ul>
										<li>
											<form action="<?php print $docroot; ?>edit_venduser.php" method="post" onclick="return delconfirm()">
												<input type="hidden" name="deleteuser" value="<?php print $edituser; ?>" />
												<input type="hidden" name="redirect" value="<?php print $redirect; ?>" />
												<font color="red">
													<input type="submit" value="DELETE ROUTE" />
												</font>
											</form>
										</li>
									</ul>
								</td>
							</tr>
						<?php } ?>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

