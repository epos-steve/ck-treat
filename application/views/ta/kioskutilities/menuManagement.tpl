<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr bgcolor="#F6F9FA">
		<td colspan="2" align="right">&nbsp;</td>
	</tr>
	<tr style="height: 1px; background-color: black; vertical-align: top;">
		<td colspan="2">
			<img src="/assets/images/spacer.gif" alt="" />
		</td>
	</tr>

	<tr valign="top" bgcolor="#E8E7E7">
		<td width="25%" rowspan="3">&nbsp;</td>
		<td width="75%">
			<h2>Menu Management</h2>

			<form action="copyPosMenu" method="POST">
				<input type="hidden" name="companyid" value="<?php echo $companyId; ?>" />
				<table>
					<tr>
						<td><label for="copy_from">From:</label></td>
						<td>
							<select name="copy_from" id="copy_from">
								<?php echo $copy_from; ?>
							</select>
						</td>
					</tr>
					<tr>
						<td><label for="copy_to">To:</label></td>
						<td>
							<select name="copy_to" id="copy_to">
								<?php echo $copy_to; ?>
							</select>
						</td>
					</tr>
				</table>
				<br />

				<input type="submit" name="submit" value="Copy Details" />
			</form>
		</td>
	</tr>

	<tr valign="bottom" bgcolor="#E8E7E7">
		<td align="right">
			&nbsp;
		</td>
	</tr>
</table>

<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr bgcolor="#F6F9FA">
		<td colspan="2" align="right">&nbsp;</td>
	</tr>
	<tr style="height: 1px; background-color: black; vertical-align: top;">
		<td colspan="2">
			<img src="/assets/images/spacer.gif" alt="" />
		</td>
	</tr>

	<tr valign="top" bgcolor="#E8E7E7">
		<td width="25%" rowspan="3">&nbsp;</td>
		<td width="75%">
			<h2>Export / Import</h2>

			<div id="uploader-container">
				<form action="syncMenu" method="POST" enctype="multipart/form-data" target="import-file-target" style="display: inline;">
					<input type="hidden" name="companyid" value="<?php echo $companyId; ?>" />
					<div class="importer-exporter-selection" style="margin-bottom: 0px;">
						<table>
							<tr>
								<td><label for="copy_from">Menu Selection:</label></td>
								<td>
									<select name="limit" class="menu-selection">
										<option value="company">Company</option>
										<option value="district">District</option>
										<option value="kiosk">Kiosk</option>
									</select>
								</td>
							</tr>
							<tr>
								<td></td>
								<td>
									<select name="district" class="districts menu-selection-dependent">
										<option value="">Select District</option>
				<?php foreach ( $districts as $id => $name ) { ?>

										<option value="<?php echo $id; ?>"><?php echo $name; ?></option>
				<?php } ?>
									</select>

									<select name="kiosk" class="kiosks menu-selection-dependent">
										<option value="">Select Kiosk</option>
										<?php echo $kiosks; ?>
									</select>
								</td>
							</tr>
						</table>
						<br />

					</div>
					<span class="exporter-list" style="margin-right: 30px;">
						<input type="submit" name="export" value="Export Menu Items" id="exporter-file" />
					</span>
					<span class="importer-list">
						<input type="file" accept=".csv,text/csv" name="import_menu_items" id="importer-file" />
						<span class="html4">
							<input type="submit" name="import" value="Import Menu Items" class="import-file-button" />
							<img src="/assets/images/ajax-loader-redmond.gif" style="display: none;" class="upload-update-progress" />
							<iframe id="import-file-target" name="import-file-target" src="#" style="width:0;height:0;border:0px solid #fff;"></iframe>
						</span>

						<span class="html5">
							<button name="choose_file" class="importer-choose-file" type="button">Choose Import File</button>
							<progress max="100"></progress>
						</span>
					</span>
				</form>
			</div>

			<div id="import-uploader-error-display" style="display: none; border: 2px solid #990000; padding: 5px; background-color: #ddaaaa; font-weight: bold; margin: 0px 10px;">
				<span>Error:</span>
				<span class="error-message"></span>
			</div>

			<div id="upload-menu-item-review" style="display: none;">
				<button type="button" name="ignore_changes" class="ignore-changes">Ignore</button>
				<button type="button" name="finish_changes" class="save-changes">Save</button>

				<p class="skipped-message">Skipped <span class="skipped-num"></span> items.</p>

				<table id="importer-changes-display" style="width: 75%; display: none; border: 0px none;">
					<thead>
					<tr>
						<th></th>
						<th style="text-align: left">Name</th>
						<th style="text-align: left">UPC</th>
						<th style="width: 75px; text-align: right;">Cost</th>
						<th style="width: 75px; text-align: right;">Price</th>
					</tr>
					</thead>
					<tfoot>
					<tr>
						<th></th>
						<th style="text-align: left">Name</th>
						<th style="text-align: left">UPC</th>
						<th style="width: 90px; text-align: right;">Cost</th>
						<th style="width: 90px; text-align: right;">Price</th>
					</tr>
					</tfoot>
					<tbody>
					</tbody>
				</table>
			</div>
		</td>
	</tr>

	<tr valign="bottom" bgcolor="#E8E7E7">
		<td align="right">
			&nbsp;
		</td>
	</tr>
</table>

<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr bgcolor="#F6F9FA">
		<td colspan="2" align="right">&nbsp;</td>
	</tr>
	<tr style="height: 1px; background-color: black; vertical-align: top;">
		<td colspan="2">
			<img src="/assets/images/spacer.gif" alt="" />
		</td>
	</tr>

	<tr valign="top" bgcolor="#E8E7E7">
		<td width="25%" rowspan="3">&nbsp;</td>
		<td width="75%">
			<h2>Master Menu</h2>

			<form action="exportMasterMenu" method="POST">
				<input type="submit" name="submit" value="Export" /><br>
				*Cost and Price are averages from all terminals.
			</form>
		</td>
	</tr>

	<tr valign="bottom" bgcolor="#E8E7E7">
		<td align="right">
			&nbsp;
		</td>
	</tr>
</table>
