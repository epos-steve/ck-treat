<style>
.links{width:100px; float:left; padding:0; margin:0 0 10px 0;}
.selects{width:350px;}
h3{margin:0 0 10px 0;}
label{width:100px; float:left;}
.clear_form{margin-right:35px;}
</style>

<div id="loading" style="display:none;">
	<div id="overlay"></div>
	<div id="message">
		Loading... Please Wait<br/><img src="/assets/images/preload/loading.gif" height="22" width="126">
	</div>
</div>

<table cellspacing='0' cellpadding='0' border='0' width='100%'>
	<tr bgcolor='#F6F9FA'>
		<td colspan='2' align='right'>&nbsp;</td>
	</tr>
	<tr style='height: 1px; background-color: black; vertical-align: top;'>
		<td colspan='2'>
			<img src='/assets/images/spacer.gif' alt='' />
		</td>
	</tr>
	<tr bgcolor='#E8E7E7'>
		<td colspan='2'>
			<br />
			<table width='99%' cellspacing='0' cellpadding='0' style='margin-left: auto; margin-right: auto;'>
				<tr bgcolor='#E8E7E7' valign='top' id='content'>
					<td width='100%'>
						<h3>Categories</h3>
					</td>
				</tr>
				<tr>
					<td>
						<?php print $html; ?>
					</td>
				</tr>
			</table>
			<p></p>
		</td>
	</tr>
</table>