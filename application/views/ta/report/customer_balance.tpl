<?php
Treat_Controller_Abstract::addCss( '/assets/css/ta/customer_balance.css' );
?>

<?php if ( count($accounts_rebalanced) > 0 ) : ?>
<div id="accounts-rebalanced">
	<span id="customer-rebalanced-header">Accounts Re-balanced</span>
</div>
<div id="accounts-rebalanced-2">
<?php foreach ( $accounts_rebalanced as $customer ) : ?>

	<span class="customer-rebalanced-name"><?php echo $customer['data_line']; ?></span>
	<span class="customer-rebalanced-scancode"><?php echo $customer['scancode']; ?></span>
	<span class="customer-rebalanced-amount"><?php echo $customer['message']; ?></span>
<?php endforeach; ?>
</div>
<?php endif; ?>


<form action="" method="GET">
	<div>
		<label>
			<input type="radio" name="show_all" value="1" <?php if($show_all == 1) { echo "checked"; } ?> />
			Display All
		</label>
		<label>
			<input type="radio" name="show_all" value="0" <?php if($show_all == 0) { echo "checked"; } ?> />
			Display OOB only
		</label>
	</div>

	<div>
		<label>
			<input type="radio" name="show_detail" value="1" <?php if($show_detail == 1) { echo "checked"; } ?> />
			Show Detail
		</label>
		<label>
			<input type="radio" name="show_detail" value="0" <?php if($show_detail == 0) { echo "checked"; } ?> />
			No Detail
		</label>
	</div>

	<div>
		<label>
			From:
			<input type="date" name="date_from" value="<?php echo $date_from; ?>" />
		</label>
		<label>
			To:
			<input type="date" name="date_to" value="<?php echo $date_to; ?>" />
		</label>
	</div>

	<select name="bid">
		<option value="">Choose Business</option>
<?php foreach ( $business_options as $business ) : ?>
		<option value="<?php echo $business->businessid; ?>"<?php if ( intval($business->businessid) === $bid ) : ?> selected="selected"<?php endif; ?>>
			<?php echo $business->companyname; ?> :: <?php echo $business->businessname; ?>
		</option>
<?php endforeach; ?>
	</select>

	<input type="submit" value="GO">
	<br />
</form>
<br />

<?php
if (!isset($_GET['bid'])) :
?>
<h3>Select the business you would like to rebalance</h3>
<?php
endif;
?>

<div id="accounts-rebalanced">
	<span id="customer-rebalanced-header">Account Balance Summary</span>
</div>
	
<div id="accounts-rebalanced-2">
	<span class="account-balance-summary-label">Total Accounts</span><span class="account-balance-summary-amount"><?php echo $total_customers; ?></span><span class="account-balance-summary-text"></span>
	<span class="account-balance-summary-label">Accounts Balanced</span><span class="account-balance-summary-amount"><?php echo $accounts_balanced; ?></span><span class="account-balance-summary-text"></span>
	<span class="account-balance-summary-label">Accounts OO Balance</span><span class="account-balance-summary-amount"><?php echo $accounts_oob; ?></span><span class="account-balance-summary-text"><?php if ($accounts_oob > 0){ ?><a href="/ta/lib/test7.php?bid=<?php echo $bid; ?>&rebalance=all" target="_blank">Re-balance All</a><?php } ?></span>
	<span class="account-balance-summary-label">Accounts &plusmn;$1</span><span class="account-balance-summary-amount"><?php echo $accounts_oob_1; ?></span><span class="account-balance-summary-text"><?php if ($accounts_oob_1 > 0){ ?><a href="/ta/lib/test7.php?bid=<?php echo $bid; ?>&rebalance=1&data=<?php echo $oob_1; ?>" target="_blank">Re-balance</a><?php } ?></span>
	<span class="account-balance-summary-label">Accounts &plusmn;$2</span><span class="account-balance-summary-amount"><?php echo $accounts_oob_2; ?></span><span class="account-balance-summary-text"><?php if ($accounts_oob_2 > 0){ ?><a href="/ta/lib/test7.php?bid=<?php echo $bid; ?>&rebalance=2&data=<?php echo $oob_2; ?>" target="_blank">Re-balance</a><?php } ?></span>
	<span class="account-balance-summary-label">Accounts &plusmn;$5</span><span class="account-balance-summary-amount"><?php echo $accounts_oob_5; ?></span><span class="account-balance-summary-text"><?php if ($accounts_oob_5 > 0){ ?><a href="/ta/lib/test7.php?bid=<?php echo $bid; ?>&rebalance=5&data=<?php echo $oob_5; ?>" target="_blank">Re-balance</a><?php } ?></span>
	<span class="account-balance-summary-label">Accounts &plusmn;$10</span><span class="account-balance-summary-amount"><?php echo $accounts_oob_10; ?></span><span class="account-balance-summary-text"><?php if ($accounts_oob_10 > 0){ ?><a href="/ta/lib/test7.php?bid=<?php echo $bid; ?>&rebalance=10&data=<?php echo $oob_10; ?>" target="_blank">Re-balance</a><?php } ?></span>
	<span class="account-balance-summary-label">Accounts &plusmn;$20</span><span class="account-balance-summary-amount"><?php echo $accounts_oob_20; ?></span><span class="account-balance-summary-text"><?php if ($accounts_oob_20 > 0){ ?><a href="/ta/lib/test7.php?bid=<?php echo $bid; ?>&rebalance=20&data=<?php echo $oob_20; ?>" target="_blank">Re-balance</a><?php } ?></span>
	<span class="account-balance-summary-label">Accounts > &plusmn;$20</span><span class="account-balance-summary-amount"><?php echo $accounts_oob_20_plus; ?></span><span class="account-balance-summary-text"><?php if ($accounts_oob_20_plus > 0){ ?><a href="/ta/lib/test7.php?bid=<?php echo $bid; ?>&rebalance=21&data=<?php echo $oob_21; ?>" target="_blank">Re-balance</a><?php } ?></span>
</div>

<table width="925px">
<?php
foreach ($report_table as $row_info) :
?>
	<tr>
<?php
	foreach ($row_info as $css_class => $cells) :
		foreach ($cells as $cell) :
?>
		<td class="<?php echo $css_class; ?>"><?php echo $cell; ?></td>
<?php
		endforeach;
	endforeach;
?>
	</tr>
<?php
endforeach;
?>
</table>