<?php
Treat_Controller_Abstract::addCss( '/assets/css/jq_themes/redmond/jqueryui-current.css' );
Treat_Controller_Abstract::addCss( '/assets/css/jq_themes/redmond/jqueryui-custom.css' );
Treat_Controller_Abstract::addCss( '/assets/css/ta/report.css' );
Treat_Controller_Abstract::addJavascriptAtTop('/assets/js/jquery-latest.js');
Treat_Controller_Abstract::addJavascriptAtTop('/assets/js/jquery.tablesorter.js');
Treat_Controller_Abstract::addJavascriptAtTop('/assets/js/pos_reports_run.js');
$http_qry = array(
	'cid' => Treat_Controller_Abstract::getPageCompanyId(),
	'bid' => Treat_Controller_Abstract::getPageBusinessId(),
);


	ob_start();
?>

	<h2>POS Reports</h2>

<?php
	$header[] = ob_get_clean();
?>


	<div class="ui-tabs ui-widget-content ui-corner-all">
		<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
			<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active">
				<a href="/ta/report?<?php echo http_build_query( $http_qry ); ?>">Reports Run</a>
			</li>
			<li class="ui-state-default ui-corner-top">
				<a href="/ta/report/instant?<?php echo http_build_query( $http_qry ); ?>">Instant Reports</a>
			</li>
		</ul>


		<fieldset class="page-block collapsible ui-corner-all">
			<legend class=" ui-widget-header ui-corner-all">
				Reports Run
			</legend>
		
<?php
		if ( $user_report_list ) {
?>
			<table border="0" cellspacing="0" width="100%" class="current_reports" id="current_reports">
				<thead>
					<tr>
						<th class="delete_report">&nbsp;</th>
						<th class="report_name">Report Name</th>
						<th class="date_run">Date Run</th>
						<th class="report_how_often">How Often</th>
						<th class="report_dow">Day of Week</th>
						<th class="report_dom">Day of Month</th>
						<th class="report_when">When</th>
						<th class="report_next_run">Next Run</th>
					</tr>
				</thead>
				<tbody>
<?php
			$stripe = 0;
			foreach ( $user_report_list as $user_report ) {
				if ( $stripe % 2 ) {
					$class = 'even';
				}
				else {
					$class = 'odd';
				}
				$stripe++;
/*
					<tr>
						<td colspan="7"><pre><?php
							var_dump( $user_report_list );
#							var_dump( $user_report );
						?></pre></td>
					</tr>
*/
				?>
					<tr class="<?php
						echo $class;
					?>">
						
						<?php 
							$user = $_SESSION->getUser();
							$userid = $user->loginid;
							$security_level = $user->security_level;
							
							$login_id = $user_report->login_id;
							
							if (($security_level > 8) || ($userid == '4776')){
								?>
								<td class="delete_report">
									<a href="/ta/report/archive_report?posrc_filename=<?php echo $user_report->filename; ?>&rcid=<?php echo $user_report->report_cache_id; ?>&cid=<?php echo $user_report->company_id; ?>"><img src="/assets/images/delete.gif" border="0" height="16" width="16" /></a>
								</td>
								<?php
							} else if ($userid == $login_id) {
								?>
								<td class="delete_report">
									<a href="/ta/report/archive_report?rcid=<?php echo $user_report->report_cache_id; ?>&cid=<?php echo $user_report->company_id; ?>"><img src="/assets/images/delete.gif" border="0" height="16" width="16" /></a>
								</td>
								<?php
							} else {
								?><td class="delete_report">&nbsp;</td><?php
							}
							
						?>
						
						<td class="report_name">
							<a href="<?php
								echo $user_report->getFileDownloadPath();
							?>"><?php
								echo $user_report->label;
							?></a>
						</td>
						<td class="date_run">
							<a href="<?php
								echo $user_report->getFileDownloadPath();
							?>"><?php
								echo date( 'Y-m-d',
									Treat_Date::getValidTimeStamp( $user_report->date_run )
								);
							?></a>
						</td>
						<td class="report_how_often">
							<a href="<?php
								echo $user_report->getFileDownloadPath();
							?>"><?php
								echo $user_report->display_how_often;
							?></a>
						</td>
						<td class="report_dow">
							<a href="<?php
								echo $user_report->getFileDownloadPath();
							?>"><?php
								echo $user_report->display_day_of_week;
							?></a>
						</td>
						<td class="report_dom">
							<a href="<?php
								echo $user_report->getFileDownloadPath();
							?>"><?php
								echo $user_report->display_day_of_month;
							?></a>
						</td>
						<td class="report_when">
							<a href="<?php
								echo $user_report->getFileDownloadPath();
							?>"><?php
								echo $user_report->getHowOftenLongString();
							?></a>
						</td>
						<td class="report_next_run">
							<a href="<?php
								echo $user_report->getFileDownloadPath();
							?>"><?php
								echo $user_report->getNextRun();
							?></a>
						</td>
						<td class="delete_button">
						</td>
					</tr>
<?php
			}
?>
				</tbody>
			</table>
<?php
		}
?>
		
		</fieldset>

	</div>

