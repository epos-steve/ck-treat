<?php
Treat_Controller_Abstract::addCss( '/assets/css/jq_themes/redmond/jqueryui-current.css' );
Treat_Controller_Abstract::addCss( '/assets/css/jq_themes/redmond/jqueryui-custom.css' );
\EE\Controller\Base::addCss( '/assets/css/ta/report_instant.css' );
\EE\Controller\Base::addJavascriptAtTop('/assets/js/jquery-1.7.2.min.js');
\EE\Controller\Base::addJavascriptAtTop('/assets/js/jquery-ui-1.8.11.custom.min.js');
\EE\Controller\Base::addJavascriptAtTop('/assets/js/ta/report_instant.js');

$http_qry = array(
	'cid' => $cid,
#	'bid' => $bid,
);


	ob_start();
?>

	<h2><?php
		echo $company->companyname;
	?> POS Reports</h2>

<?php
	$header[] = ob_get_clean();
?>


	<div class="ui-tabs ui-widget-content ui-corner-all">
		<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
			<li class="ui-state-default ui-corner-top">
				<a href="/ta/report?<?php echo http_build_query( $http_qry ); ?>">Reports Run</a>
			</li>
			<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active">
				<a href="/ta/report/instant?<?php echo http_build_query( $http_qry ); ?>">Instant Reports</a>
			</li>
		</ul>


		<fieldset class="page-block collapsible ui-corner-all">
			<legend class=" ui-widget-header ui-corner-all">
				List of Reports You May Run Instantly
			</legend>
		
<?php
		if ( $report_list ) {
?>
			<table border="0" cellspacing="0" width="100%" class="current_reports">
				<thead>
					<tr>
						<th class="report_name">Report</th>
					</tr>
				</thead>
				<tbody>
<?php
			$stripe = 0;
			foreach ( $report_list as $user_report ) {
				if ( $stripe % 2 ) {
					$class = 'even';
				}
				else {
					$class = 'odd';
				}
				$stripe++;
?>
					<tr class="<?php
						echo $class;
					?>">
						<td class="report_name">
							<a href="/ta/report/do-instant-report?<?php
								echo http_build_query( array_merge(
									$http_qry
									,array(
										'rid' => $user_report->report_id,
										'slug' => $user_report->slug,
										'companyname' => preg_replace(
											array(
												'@[\s;\.\:\[\]\{\}\(\)\-\\/]+@'
											),
											array(
												''
											),
											$company->companyname
										),
									)
								) );
							?>">
								Run the <?php
									echo $company->companyname;
								?> <?php
									echo $user_report->label;
								?> Report
							</a>
						</td>
					</tr>
<?php
			}
?>
				</tbody>
			</table>
<?php
		}
?>
		
		</fieldset>

	</div>

<div style="display:none;" id="dates_dialog" title="Send Report Email"> 
	<form id="reportrun_form" name="reportrun_form" action="" method="post">
		<div class="field_container">
			<label for="start_date">Start Date:</label>
			<input type="text" id="start_date" name="start_date" />
		</div>
		
		<div class="field_container">
			<label for="end_date">End Date:</label>
			<input type="text" id="end_date" name="end_date" />
		</div>
		
		<div class="field_container">
			<label for="email">Email:</label>
			<input style="width:340px;" type="text" id="email" name="email" value="<?php echo $email; ?>" />
		</div>
		
		<input type="hidden" name="redirect_url" id="redirect_url" value="<?php echo $redirect_url; ?>" />
		
		<div class="field_container">
			<input name="date_submit" id="date_submit" type="submit" value="Go" />
		</div>
	</form>
</div>

