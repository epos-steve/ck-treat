<style>
#item_table ul{padding:0; margin:0;}
#item_table li{list-style:none;}
</style>

<table cellspacing='0' cellpadding='0' border='0' width='100%'>
	<tr bgcolor='#E8E7E7'>
		<td colspan='2' align='right'>&nbsp;</td>
	</tr>
	<tr style='height: 1px; background-color: black; vertical-align: top;'>
		<td colspan='2'>
			<img src='/assets/images/spacer.gif' alt='' />
		</td>
	</tr>
	<tr bgcolor='#E8E7E7'>
		<td colspan='2'>
			<br />
			<table width='99%' cellspacing='0' cellpadding='0' style='margin-left: auto; margin-right: auto;'>
				<tr bgcolor='#E8E7E7' valign='top' id='content'>
					<td width='20%'>						
						<p>LIST OF PROMO GROUPS HERE</p>
					</td>
					<td width=2%>&nbsp;</td>
					<td>
						<div id='groups' class='navPanel'>
							<div id="promo_groups">
								<div class='navPanelTitle'>Promotion/Printing Groups</div>
								
								<form id="frm_promo_groups" action="#" method="post">
									<input type="hidden" name="promo_group_id" id="promo_group_id" value="" />
									<div>
										<label for="promo_group_name">Group Name</label>
										<input type="text" size="20" maxlength="128" name="promo_group_name" id="promo_group_name" value="" />
									</div>
											
									<div>
										<label for="promo_group_visible">Visible?</label>
										<input type="checkbox" name="promo_group_visible" id="promo_group_visible" value="1" />
									</div>
											
									<div>
										<label for="promo_group_routing">Print Routing?</label>
										<input type="checkbox" name="promo_group_routing" id="promo_group_routing" value="1" />
									</div>
										
									<div>
										<label for="promo_group_order">Display Order</label>
										<input type="text" size="5" maxlength="5" name="promo_group_order" id="promo_group_order" value="99999" />
									</div>
										
									<div>
										<label for="item_ids">Item Ids</label>
										<input type="text" name="item_ids" id="item_ids" />
									</div>
										
									<input name="submit" id="submit" type="submit" value="Save" />
									<input name="clear" id="clear" type="reset" value="Clear" />
								</form>
							</div>
							<div id="promo_group_products">
								<?php print $items; ?>
							</div>
						</div>
					</div>
					<br />
				</td>
			</tr>
		</table>
		<p></p>
	</td>
	</tr>
</table>
