<style>
#item_table td{width:33%;}
li:nth-child(2n){background-color:#EEE8CD;}
#item_table ul{padding:0; margin:0;}
#item_table li{list-style:none; margin:0 5px;}
h3{margin:0 0 5px 5px;}
.cat{cursor:pointer;}
.highlight{background-color:yellow !important;}
h3 div{font-weight:normal; font-size:13px; color:blue; margin:0 10px; width:150px;}
</style>

<div id="loading" style="display:none;">
	<div id="overlay"></div>
	<div id="message">
		Loading... Please Wait<br/><img src="/assets/images/preload/loading.gif" height="22" width="126">
	</div>
</div>

<table cellspacing='0' cellpadding='0' border='0' width='100%'>
	<tr bgcolor='#F6F9FA'>
		<td colspan='2' align='right'>&nbsp;</td>
	</tr>
	<tr style='height: 1px; background-color: black; vertical-align: top;'>
		<td colspan='2'>
			<img src='/assets/images/spacer.gif' alt='' />
		</td>
	</tr>
	<tr bgcolor='#E8E7E7'>
		<td colspan='2'>
			<br />
			<table width='99%' cellspacing='0' cellpadding='0' style='margin-left: auto; margin-right: auto;'>
				<tr bgcolor='#E8E7E7' valign='top' id='content'>
					<td width='20%'>
						<form action="#" method="post" style="margin: 0; padding: 0; display: inline;">
							<center>
								<select size="18" name="promo_groups_list" id="promo_groups_list" style="width: 255px;"'>
									<?php print $promo_groups; ?>
								</select>
							</center>
						</form>
					</td>
					<td width=2%>&nbsp;</td>
					<td>
						<form id="frm_promo_groups" name="frm_promo_groups" action="#" method="post">
							<div id='groups' class='navPanel'>
								<div id="promo_groups">
									<div class='navPanelTitle'>Promotion/Printing Groups</div>
					
									<input type="hidden" name="promo_group_id" id="promo_group_id" value="-1" />
									
									<br />
									
									<div>
										<label for="promo_group_name">Group Name</label>
										<input type="text" size="55" maxlength="128" name="promo_group_name" id="promo_group_name" value="" />
									</div>
											
									<!--<div>
										<label for="promo_group_visible">Visible?</label>
										<input type="checkbox" name="promo_group_visible" id="promo_group_visible" value="1" />
									</div>
											
									<div>
										<label for="promo_group_routing">Print Routing?</label>
										<input type="checkbox" name="promo_group_routing" id="promo_group_routing" value="1" />
									</div>
										
									<div>
										<label for="promo_group_order">Display Order</label>
										<input type="text" size="5" maxlength="5" name="promo_group_order" id="promo_group_order" value="99999" />
									</div>-->
									
									<br />
									
									<div style="float:left;">
										<input name="clear" id="clear" style="margin-right:35px;" type="button" value="Clear Form" />
										<input name="submit" id="submit" type="submit" value="Save" />
										<input name="delete" id="delete" type="button" value="Delete" />
									</div>
									
									<div style="float:left; width:300px;">
										<a style="float:left;" id="hide" href="javascript:">Hide Categories</a>
										<a style="float:left;" id="show" href="javascript:">Show Categories</a>
									</div>
									
									<div id="res_message" style="font-weight:bold;"></div>
								</div>
								
								<br />
								
								<div id="promo_group_products">
									<?php print $items; ?>
								</div>
							</div>
						</div>
						<br />
					</form>
				</td>
			</tr>
		</table>
		<p></p>
	</td>
	</tr>
</table>