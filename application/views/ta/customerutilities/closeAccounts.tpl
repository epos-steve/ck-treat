<table cellspacing='0' cellpadding='0' border='0' width='100%'>
	<tr bgcolor='#F6F9FA'>
		<td colspan='2' align='right'>&nbsp;</td>
	</tr>
	<tr style='height: 1px; background-color: black; vertical-align: top;'>
		<td colspan='2'>
			<img src='/assets/images/spacer.gif' alt='' />
		</td>
	</tr>
	
	<tr valign=top bgcolor=#E8E7E7>
		<td width=10% rowspan=3>
			&nbsp;
		</td>
		<td width=90%>
			<table cellpadding="0" cellspacing="0" width="85%">
				<tr>
					<td valign="top">
						<h2>Close Accounts</h2>
						<table cellspacing=5 cellpadding=1 width="100%">
						<tr>
							<td>
								<?php echo $kView; ?>
								<p>
								<?php
									//echo $cardView;
									if (count($audit_record_table) > 0){
								?>
								<center>
								<table id="audit-record-table">
									<tr id="audit-record-header-row">
										<td colspan=7><font color=white>Audit Records</font></td>
									</tr>
									<tr>
										<td class="audit-record-header">DateTime</td>
										<td class="audit-record-header">Username</td>
										<td class="audit-record-header">Batch #</td>
										<td class="audit-record-header">Active # closed</td>
										<td class="audit-record-header">Active CK Adjust</td>
										<td class="audit-record-header">InActive # closed</td>
										<td class="audit-record-header">InActive CK Adjust</td>
									</tr>
									<?php
								foreach ($audit_record_table as $value){
								?>
									<tr>
										<td class="audit-record-row"><?php echo $value['date_time']; ?></td>
										<td class="audit-record-row"><?php echo $value['username']; ?></td>
										<td class="audit-record-row"><?php echo $value['batch_number']; ?></td>
										<td class="audit-record-row"><?php echo $value['closed_active_accounts']; ?></td>
										<td class="audit-record-row"><?php echo $value['ck_adjusted_active']; ?></td>
										<td class="audit-record-row"><?php echo $value['closed_inactive_accounts']; ?></td>
										<td class="audit-record-row"><?php echo $value['ck_adjusted_inactive']; ?></td>
									</tr>
										<?php
								}
									?>
								</table>
							<?php
								}
							?>
								<p>
								<?php echo $cView; ?>
								<p>
								<?php echo $dView; ?>
							</td>
						</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>	
	<tr bgcolor=#E8E7E7>
		<td>
			<p>
			<form action='/ta/posreport.php' method='post' style="margin:0;padding:0;display:inline;">
				<input type=hidden name=download_data value="<?php echo $download_data; ?>">
				<input type=submit value='Download' style="border: 1px solid #999999; font-size: 10px; background-color:#CCCCCC;" />
			</form>
		</td>
	</tr>
	<tr bgcolor=#E8E7E7>
		<td>
			<p>
			<form action='/ta/customerutilities/zeroCloseAccounts' method='post' style="margin:0;padding:0;display:inline;">
				<input type=hidden name=cid value="<?php echo $cid; ?>">
				<input type=submit value='Close Accounts' style="border: 1px solid #999999; font-size: 10px; background-color:#CCCCCC;" />
			</form>
		</td>
	</tr>	
	<tr valign="bottom" bgcolor="#E8E7E7">
		<td align="right">
			&nbsp;
		</td>
	</tr>
</table>