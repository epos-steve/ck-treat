<table cellspacing='0' cellpadding='0' border='0' width='100%'>
	<tr bgcolor='#F6F9FA'>
		<td colspan='2' align='right'>&nbsp;</td>
	</tr>
	<tr style='height: 1px; background-color: black; vertical-align: top;'>
		<td colspan='2'>
			<img src='/assets/images/spacer.gif' alt='' />
		</td>
	</tr>
	
	<tr valign=top bgcolor=#E8E7E7>
		<td width=25% rowspan=3>
			&nbsp;
		</td>
		<td width=75%>
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td valign="top">
						<h2>Customer Utilities</h2>
						<p><a href="/ta/customerutilities/customerSearch?cid=<?php echo $companyId ?>">Customers</a></p>
						<p><a href="/ta/customerutilities/adjustments?cid=<?php echo $companyId ?>">Adjustments</a></p>
						<p><a href="/ta/customerutilities/closeAccounts?cid=<?php echo $companyId ?>">Close Accounts</a></p>
						<p><a href="/ta/customerutilities/customerExport?cid=<?php echo $companyId ?>">Export</a></p>
						<p><a href="/ta/customerutilities/customerSettings?cid=<?php echo $companyId ?>">Settings</a></p>
						<p><a href="/ta/customerutilities/businessGroups?cid=<?php echo $companyId ?>">Business Groups</a></p>
					</td>
				</tr>
			</table>
		</td>
	</tr>	
	
	<tr valign="bottom" bgcolor="#E8E7E7">
		<td align="right">
			<form id="manage" action="/ta/update_pos.php" method="post" style="margin: 0; padding: 0; display: inline;">
				<div class="info"></div>
				<input type="hidden" id="businessid" name="businessid" value="<?php echo $GLOBALS['businessid']; ?>" />
			</form>
			&nbsp;
		</td>
	</tr>
</table>