<table cellspacing='0' cellpadding='0' border='0' width='100%'>
	<tr bgcolor='#F6F9FA'>
		<td colspan='2' align='right'>&nbsp;</td>
	</tr>
	<tr style='height: 1px; background-color: black; vertical-align: top;'>
		<td colspan='2'>
			<img src='/assets/images/spacer.gif' alt='' />
		</td>
	</tr>
	
	<tr valign=top bgcolor=#E8E7E7>
		<td width=10% rowspan=3>
			&nbsp;
		</td>
		<td width=90%>
			<table cellpadding="0" cellspacing="0" width="85%">
				<tr>
					<td valign="top">
						<h2>Accounts Out of Balance</h2>
						
							<center>
								<table id="audit-record-table">
									<tr id="audit-record-header-row">
										<td colspan=4><font color=white>Account Balance Summary</font></td>
									</tr>
									<tr>
										<td>Total Accounts</td>
										<td><?php echo $accountBalanceSummary['customer_count']; ?></td>
										<td></td>
									</tr>
									<tr>
										<td>Accounts Balanced</td>
										<td><?php echo $accountBalanceSummary['in_balance_customer_count']; ?></td>
										<td></td>
									</tr>
									<tr>
										<td>Accounts OO Balance</td>
										<td><?php echo $accountBalanceSummary['oob_customer_count']; ?></td>
										<td></td>
									</tr>
									<tr>
										<td>Accounts &plusmn;$1</td>
										<td><?php echo $accountBalanceSummary['accounts_oob_1']; ?></td>
										<td><a href="/ta/lib/test7.php?bid=<?php echo $accountBalanceSummary['bid_value']; ?>&rebalance=1" target="_blank">Re-balance</a>
										</td>
									</tr>
									<tr>
										<td>Accounts &plusmn;$2</td>
										<td><?php echo $accountBalanceSummary['accounts_oob_2']; ?></td>
										<td><a href="/ta/lib/test7.php?bid=<?php echo $accountBalanceSummary['bid_value']; ?>&rebalance=2" target="_blank">Re-balance</a>
										</td>
									</tr>
									<tr>
										<td>Accounts &plusmn;$5</td>
										<td><?php echo $accountBalanceSummary['accounts_oob_5']; ?></td>
										<td><a href="/ta/lib/test7.php?bid=<?php echo $accountBalanceSummary['bid_value']; ?>&rebalance=5" target="_blank">Re-balance</a>
										</td>
									</tr>
									<tr>
										<td>Accounts &plusmn;$10</td>
										<td><?php echo $accountBalanceSummary['accounts_oob_10']; ?></td>
										<td><a href="/ta/lib/test7.php?bid=<?php echo $accountBalanceSummary['bid_value']; ?>&rebalance=10" target="_blank">Re-balance</a>
										</td>
									</tr>
									<tr>
										<td>Accounts &plusmn;$20</td>
										<td><?php echo $accountBalanceSummary['accounts_oob_20']; ?></td>
										<td><a href="/ta/lib/test7.php?bid=<?php echo $accountBalanceSummary['bid_value']; ?>&rebalance=20" target="_blank">Re-balance</a>
										</td>
									</tr>
									<tr>
										<td>Accounts > &plusmn;$20</td>
										<td><?php echo $accountBalanceSummary['accounts_oob_20_plus']; ?></td>
										<td><a href="/ta/lib/test7.php?bid=<?php echo $accountBalanceSummary['bid_value']; ?>&rebalance=21" target="_blank">Re-balance</a>
										</td>
									</tr>
								</table>
							</center>
						
						<table cellspacing=5 cellpadding=1 width="100%">
						<tr>
							<td>
								<?php //echo $oobCustomers; var_dump($oobCustomers); ?>
								<p>
								<?php
									//echo $cardView;
									if (count($oobCustomers) > 0){
								?>
								<center>
								<table id="audit-record-table">
									<tr id="audit-record-header-row">
										<td colspan=7><font color=white>Customer Records</font></td>
									</tr>
									<tr>
										<td class="audit-record-header">Customerid</td>
										<td class="audit-record-header">First Name</td>
										<td class="audit-record-header">Last Name</td>
										<td class="audit-record-header">Scancode</td>
										<td class="audit-record-header">Stored Balance</td>
										<td class="audit-record-header">Calculated Balance</td>
										<td class="audit-record-header">Out of Balance</td>
									</tr>
									<?php
								foreach ($oobCustomers as $value){
								?>
									<tr>
										<td class="audit-record-row"><?php echo $value['customerid']; ?></td>
										<td class="audit-record-row"><?php echo $value['first_name']; ?></td>
										<td class="audit-record-row"><?php echo $value['last_name']; ?></td>
										<td class="audit-record-row"><?php echo $value['scancode']; ?></td>
										<td class="audit-record-row">$<?php echo $value['stored_balance']; ?></td>
										<td class="audit-record-row">$<?php echo $value['calculated_balance']; ?></td>
										<td class="audit-record-row">$<?php echo $value['oob_amount']; ?></td>
									</tr>
										<?php
								}
									?>
								</table>
							<?php
								}
							?>
							</td>
						</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>	

	<tr valign="bottom" bgcolor="#E8E7E7">
		<td align="right">
			&nbsp;
		</td>
	</tr>
</table>