<table class="audit-record-table">
	<tr>
		<td colspan='2'>
			<img src='/assets/images/spacer.gif' alt='' />
		</td>
	</tr>
</table>
<p></p>
<table id="zero-close-inactive">
	<tr id="zero-close-accounts-header-row">
		<td colspan=8><?php echo $activeCount; ?> Active Customers</td>
		<td>$<?php echo $activeTotalBalance; ?></td>
	</tr>
	<tr id="zero-close-account-header">
		<td class="zero-close-account-active-header">Scancode</td>
		<td class="zero-close-account-active-header">Last Name</td>
		<td class="zero-close-account-active-header">First Name</td>
		<td class="zero-close-account-active-header">Email</td>
		<td class="zero-close-account-active-header">Last Used</td>
		<td class="zero-close-account-active-header">Balance</td>
		<td class="zero-close-account-active-header">Status</td>
		<td class="zero-close-account-active-header">Calculated Balance</td>
		<td class="zero-close-account-active-header">Out of Balance</td>
	</tr>
	<?php
	foreach ($active_customer_list as $value){
	?>
	<tr>
		<td class="zero-close-account-active"><?php echo $value['scancode']; ?></td>
		<td class="zero-close-account-active"><?php echo $value['last_name']; ?></td>
		<td class="zero-close-account-active"><?php echo $value['first_name']; ?></td>
		<td class="zero-close-account-active"><?php echo $value['email']; ?></td>
		<td class="zero-close-account-active"><?php echo $value['last_used']; ?></td>
		<td class="zero-close-account-active">$<?php echo $value['balance']; ?></td>
		<td class="zero-close-account-active"><?php echo $value['customer_status']; ?></td>
		<td class="zero-close-account-active">$<?php echo $value['calculated_balance']; ?></td>
		<td class="zero-close-account-active"><?php echo $value['customer_oob']; ?></td>
	</tr>
	<?php
	}
	?>
</table>
<p></p>
<table id="zero-close-inactive">
	<tr id="zero-close-accounts-header-row">
		<td colspan=8><?php echo $inactiveCount; ?> InActive Customers</td>
		<td>$<?php echo $inactiveTotalBalance; ?></td>
	</tr>
	<tr id="zero-close-account-header">
		<td class="zero-close-account-active-header">Scancode</td>
		<td class="zero-close-account-active-header">Last Name</td>
		<td class="zero-close-account-active-header">First Name</td>
		<td class="zero-close-account-active-header">Email</td>
		<td class="zero-close-account-active-header">Last Used</td>
		<td class="zero-close-account-active-header">Balance</td>
		<td class="zero-close-account-active-header">Status</td>
		<td class="zero-close-account-active-header">Calculated Balance</td>
		<td class="zero-close-account-active-header">Out of Balance</td>
	</tr>
	<?php
	foreach ($inactive_customer_list as $value){
	?>
	<tr>
		<td class="zero-close-account-active"><?php echo $value['scancode']; ?></td>
		<td class="zero-close-account-active"><?php echo $value['last_name']; ?></td>
		<td class="zero-close-account-active"><?php echo $value['first_name']; ?></td>
		<td class="zero-close-account-active"><?php echo $value['email']; ?></td>
		<td class="zero-close-account-active"><?php echo $value['last_used']; ?></td>
		<td class="zero-close-account-active">$<?php echo $value['balance']; ?></td>
		<td class="zero-close-account-active"><?php echo $value['customer_status']; ?></td>
		<td class="zero-close-account-active">$<?php echo $value['calculated_balance']; ?></td>
		<td class="zero-close-account-active"><?php echo $value['customer_oob']; ?></td>
	</tr>
	<?php
	}
	?>
</table>
	<tr bgcolor=#E8E7E7>
		<td>
			<p>
			<form action='/ta/posreport.php' method='post' style="margin:0;padding:0;display:inline;">
				<input type=hidden name=download_data value="<?php echo $download_data; ?>">
				<input type=submit value='Download' style="border: 1px solid #999999; font-size: 10px; background-color:#CCCCCC;" />
			</form>
		</td>
	</tr>