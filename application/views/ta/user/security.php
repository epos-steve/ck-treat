<?php
	if ( !isset( $is_custom_security ) ) {
		$is_custom_security = null;
	}

	if ( !isset( $editsecurity ) ) {
		$editsecurity = Treat_Controller_Abstract::getPostOrGetVariable( 'editsecurity', null );
	}

	if ( !isset( $firstname ) ) {
		$firstname = null;
	}

	if ( !isset( $lastname ) ) {
		$lastname = null;
	}

	$list_security_id = null;
	if ( !isset( $security_id ) ) {
		if ( isset( $security ) ) {
			$security_id = $security;
			$list_security_id = $security;
			if ( !$is_custom_security ) {
				$is_custom_security = true;
			}
		}
		elseif ( isset( $editsecurity ) ) {
			$security_id = $editsecurity;
		}
		else {
			$security_id = null;
		}
	}
	
	//print 'SECURITY ID: '.$security_id;
	
	$security_levels = Treat_Model_User_Security::getSecurityList( $list_security_id );
	if ( ( $security_id = intval( $security_id ) ) ) {
		$edit_security_level = Treat_Model_User_Security::get( $security_id, true );
	}
	if ( !isset( $edit_security_level ) ) {
		$edit_security_level = new Treat_Null();
	}
	$page_business = Treat_Model_Business_Singleton::getSingleton();
	if ( !$page_business ) {
		$page_business = Treat_Model_Company_Singleton::getSingleton();
	}
	
	//print '<pre>'; print_r($edit_security_level); print '</pre>';
?>



			<table width="<?php
				if ( !$is_custom_security ) {
					echo '100%';
				}
				else {
					echo '75%';
				}
			?>">
<?php
	if ( !$is_custom_security ) {
?>
				<tr bgcolor="#e8e7e7">
					<td colspan="2">
						<form action="/ta/user_setup.php" method="post" style="margin: 0; padding: 0;">
							<ul style="margin-top: 0; margin-bottom: 14px; padding-top: 0; padding-bottom: 0;">
								<li>
									<select name="security" onChange="changePage(this.form.security)">
										<option value="/ta/user_setup.php?cid=<?php
											echo $companyid;
										?>">============Please Choose============</option>
<?php
		foreach ( $security_levels as $security_level ) {
?>
										<option value="/ta/user_setup.php?editsecurity=<?php
											echo $security_level->securityid;
										?>&cid=<?php
											echo $companyid;
										?>#security" <?php
											if ( $edit_security_level->securityid == $security_level->securityid ) {
												echo ' selected="selected"';
											}
										?>><?php
											echo $security_level->security_name;
										?> - (<?php
											echo $security_level->num_users;
										?>)</option>
<?php
		}
?>
									</select>
									<font size="2">*Number displayed is number of users with this security</font>
								</li>
							</ul>
						</form>
					</td>
				</tr>
<?php
	}
?>
<?php

			if ( $edit_security_level->securityid != '' ) {


?>
				<tr bgcolor="#e8e7e7">
					<td colspan="2">
						<form action="/ta/user/security_save" method="post">
							<input type="hidden" name="edit" value="1" />
							<input type="hidden" name="editsecurity" value="<?php echo $edit_security_level->securityid; ?>" />
							<input type="hidden" name="username" value="<?php echo $user; ?>" />
							<input type="hidden" name="password" value="<?php echo $pass; ?>" />
							<input type="hidden" name="is_custom_security" value="<?php echo intval( $is_custom_security ); ?>" />
							<input type="hidden" name="cid" value="<?php echo $page_business->getCompanyId(); ?>" />
							<input type="hidden" name="bid" value="<?php echo $page_business->getBusinessId(); ?>" />
							<table width="100%">
								<tr valign="top">
									<td width="70%">
										<table style="border:1px solid black;" bgcolor="#ffff99" width="100%">
											<tr>
												<td colspan="3">
<?php
	if ( !$is_custom_security ) {
?>
													Name:
													<input type="text" size="35" name="security_name" value="<?php
														echo $edit_security_level->security_name;
													?>" />
<?php
		if (
			$edit_security_level->securityid > 1
			&& 'Administrator' != $edit_security_level->security_name
		) {
?>
													<a href="/ta/savesecurity.php?cid=<?php
														echo $companyid;
													?>&amp;edit=2&amp;sid=<?php
														echo $edit_security_level->securityid;
													?>" onclick="return delconfirm()">
														<img src="/assets/images/delete.gif" height="16" width="16" border="0" alt="delete" />
													</a>
<?php
		}
	}
	else {
?>
													Security for <?php
														echo $firstname, ' ', $lastname;
													?> 
													<input type="hidden" name="security_name" value="<?php
														echo $firstname, ' ', $lastname;
													?>" />
<?php
	}
?> 
												</td>
											</tr>
											<tr>
												<td colspan="6">
													<b>
														Company:
													</b>
												</td>
											</tr>
											<tr>
												<td>
													<input type="checkbox" name="multi_company" value="1" <?php
														if ( $edit_security_level->multi_company ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													Change Companies
												</td>
												<td>
													<input type="checkbox" name="com_setup" value="1" <?php
														if ( $edit_security_level->com_setup ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													Company Setup
												</td>
												<td>
													<input type="checkbox" name="edituser" value="1" <?php
														if ( $edit_security_level->user ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													Users
												</td>
											</tr>
											<tr>
												<td>
													<input type="checkbox" name="reconcile" value="1" <?php
														if ( $edit_security_level->reconcile ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													Reconcile
												</td>
												<td>
													<input type="checkbox" name="securities" value="1" <?php
														if ( $edit_security_level->securities ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													Securities
												</td>
												<td>
													<input type="checkbox" name="routes" value="1" <?php
														if ( $edit_security_level->routes ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													Vending Setup
												</td>
											</tr>
											<tr>
												<td>
													<input type="checkbox" name="invoice" value="1" <?php
														if ( $edit_security_level->invoice ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													Company Invoicing
												</td>
												<td>
													<input type="checkbox" name="payable" value="1" <?php
														if ( $edit_security_level->payable ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													Company Payables
												</td>
												<td>
													<input type="checkbox" name="item_list" value="1" <?php
														if ( $edit_security_level->item_list ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													Menu Management
												</td>
											</tr>
											<tr>
												<td>
													<input type="checkbox" name="request" value="1" <?php
														if ( $edit_security_level->request ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													Requests
												</td>
												<td>
													<input type="checkbox" name="export" value="1" <?php
														if ( $edit_security_level->export ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													Exports
												</td>
												<td>
													<input type="checkbox" name="utility" value="1" <?php
														if ( $edit_security_level->utility ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													Utilities
												</td>
											</tr>
											<tr>
												<td>
													<input type="checkbox" name="kpi" value="1" <?php
														if ( $edit_security_level->kpi ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													Company KPI
												</td>
												<td>
													<input type="checkbox" name="kpi_graph" value="1" <?php
														if ( $edit_security_level->kpi_graph ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													Budget Report
												</td>
												<td>
													<input type="checkbox" name="labor" value="1" <?php
														if ( $edit_security_level->labor ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													Labor Comparison
												</td>
											</tr>
											<tr>
												<td>
													<input type="checkbox" name="transfer" value="1" <?php
														if ( $edit_security_level->transfer ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													Transfers/Purchase Cards
												</td>
												<td>
													<input type="checkbox" name="emp_labor" value="1" <?php
														if ( $edit_security_level->emp_labor ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													Employee Labor
												</td>
												<td>
													<input type="text" name="project_manager" value="<?php
														echo $edit_security_level->project_manager;
													?>" size="3" />
												</td>
												<td>
													BT Project Manager
												</td>
											</tr>
											<tr>
												<td>
													<input type="checkbox" name="budget_calc" value="1" <?php
														if ( $edit_security_level->budget_calc ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													Budget Calculator
												</td>
												<td>
													<input type="checkbox" name="sec_nutrition" value="1" <?php
														if ( $edit_security_level->nutrition ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													Nutrition
												</td>
												<td>
													<input type="checkbox" name="rebate" value="1" <?php
														if ( $edit_security_level->rebate ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													Rebates
												</td>
											</tr>
											<tr>
												<td>
													<input type="checkbox" name="reporting" value="1" <?php
														if ( $edit_security_level->reporting ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													Reporting
												</td>
												<td>
													<input type="checkbox" name="calendar" value="1" <?php
														if ( $edit_security_level->calendar ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													Calendar
												</td>
												<td>
													<input type="checkbox" name="faq" value="1" <?php
														if ( $edit_security_level->faq ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													FAQ's
												</td>
											</tr>
											<tr>
												<td>
													<input type="checkbox" name="comrecipe" value="1" <?php
														if ( $edit_security_level->comrecipe ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													Company Recipes
												</td>
												<td>
													<input type="checkbox" name="comdirectory" value="1" <?php
														if ( $edit_security_level->comdirectory ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													Company Directory
												</td>
												<td>
													<input type="checkbox" name="messages" value="1" <?php
														if ( $edit_security_level->messages ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													Messages
												</td>
											</tr>

<?php
			//echo "<tr><td><input type=checkbox name=dashboard value=1 $show55></td><td>Dashboard</td>";
?>
											<tr>
												<td>
													<select name="dashboard">
<?php
				$dbdd = array(
					0 => 'None',
					1 => 'Standard',
					2 => 'Custom',
					3 => 'Standard+Custom',
					4 => 'Admin',
				);
				foreach ( $dbdd as $key => $label ) {
?>
														<option value="<?php
															echo $key;
														?>"<?php
															if ( intval( $edit_security_level->dashboard ) == intval( $key ) ) {
																echo ' selected="selected"';
															}
														?>><?php
															echo $label;
														?></option>
<?php
				}

?>
													</select>
												</td>
												<td>
													Dashboard
												</td>
												<td>
													<input type="checkbox" name="setup_mgr" value="1" <?php
														if ( $edit_security_level->setup_mgr ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													Setup Manager
												</td>
												<td>
													<input type="checkbox" name="customer_utility" value="1" <?php
														if ( $edit_security_level->customer_utility ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													Customer Utilities
												</td>
											</tr>
											
											<tr>
												<td>
													<input type="checkbox" name="bus_promo_manager" value="1" <?php
														if ( $edit_security_level->bus_promo_manager ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													Global Promo Manager
												</td>
												
												
												<td>
													<input type="checkbox" name="bus_kiosk_route_mgmt" value="1" <?php
														if ( $edit_security_level->bus_kiosk_route_mgmt ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													Route Management
												</td>
<?php
/*
* /
?>
													<td>
														<input type="checkbox" name="bus_kiosk_menu_mgmt" value="1" <?php
															if ( $edit_security_level->bus_kiosk_menu_mgmt ) {
																echo ' checked="checked"';
															}
														?> />
														Menu Management
													</td>
<?php
/*
*/
?>
											</tr>
											
											<tr>
												<td colspan="6">
										<fieldset>
											<legend>Kiosk Utilities</legend>
											<table>
												<tbody>
													<tr>
														<td>
															<label>
																<input type="checkbox" name="kiosk_route_mgmt" value="1" <?php
																	if ( $edit_security_level->kiosk_route_mgmt ) {
																		echo ' checked="checked"';
																	}
																?> />
																Route Management
															</label>
														</td>
														<td>
															<label>
																<input type="checkbox" name="kiosk_menu_mgmt" value="1" <?php
																	if ( $edit_security_level->kiosk_menu_mgmt ) {
																		echo ' checked="checked"';
																	}
																?> />
																Menu Management
															</label>
														</td>
														<td>
															<label>
																<input type="checkbox" name="kiosk_kiosk_mgmt" value="1" <?php
																	if ( $edit_security_level->kiosk_kiosk_mgmt ) {
																		echo ' checked="checked"';
																	}
																?> />
																Kiosk Management
															</label>
														</td>
													</tr>
												</tbody>
											</table>
										</fieldset>
												</td>
											</tr>
											
											
											
											
											<tr>
												<td colspan="3">
													<b>
														Business:
													</b>
												</td>
											</tr>
											<tr>
												<td>
													<input type="text" name="bus_labor" value="<?php
														echo $edit_security_level->bus_labor;
													?>" size="3" />
												</td>
												<td>
													Store Labor<font color="red">*</font>
												</td>
												<td>
													<input type="checkbox" name="bus_setup" value="9" <?php
														if ( $edit_security_level->bus_setup == 9 ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													Store Setup
												</td>
												<td>
													<input type="text" name="bus_sales" value="<?php
														echo $edit_security_level->bus_sales;
													?>" size="3" />
												</td>
												<td>
													Store Sales
												</td>
											</tr>
											<tr>
												<td>
													<input type="text" name="bus_invoice" value="<?php
														echo $edit_security_level->bus_invoice;
													?>" size="3" />
												</td>
												<td>
													Store Invoicing
												</td>
												<td>
													<input type="text" name="bus_order" value="<?php
														echo $edit_security_level->bus_order;
													?>" size="3" />
												</td>
												<td>
													Store Orders<font color="blue">*</font>
												</td>
												<td>
													<input type="text" name="bus_payable" value="<?php
														echo $edit_security_level->bus_payable;
													?>" size="3" />
												</td>
												<td>
													Store Payables
												</td>
											</tr>
											<tr>
												<td>
													<input type="text" name="bus_payable2" value="<?php
														echo $edit_security_level->bus_payable2;
													?>" size="3" />
												</td>
												<td>
													Expense Reports
												</td>
												<td>
													<input type="text" name="bus_payable3" value="<?php
														echo $edit_security_level->bus_payable3;
													?>" size="3" />
												</td>
												<td>
													Purchase Orders
												</td>
												<td></td>
												<td></td>
											</tr>
											<tr>
												<td>
													<input type="text" name="bus_inventor1" value="<?php
														echo $edit_security_level->bus_inventor1;
													?>" size="3" />
												</td>
												<td>
													Inventory $'s
												</td>
												<td>
													<input type="checkbox" name="bus_inventor2" value="1" <?php
														if ( $edit_security_level->bus_inventor2 ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													Inventory Count
												</td>
												<td>
													<input type="checkbox" name="bus_inventor3" value="1" <?php
														if ( $edit_security_level->bus_inventor3 ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													Inventory Setup
												</td>
											</tr>
											<tr>
												<td>
													<select name="bus_inventor4"> 
														<option value=0
                                                                                                                        <?php
                                                                                                                        if ( $edit_security_level->bus_inventor4 < 1) {
                                                                                                                            echo ' selected="selected"';
                                                                                                                        }
                                                                                                                        ?>
                                                                                                                        >Disabled</option>
                                                                                                                <option value=1
                                                                                                                        <?php
                                                                                                                        if ( $edit_security_level->bus_inventor4 == 1) {
                                                                                                                            echo ' selected="selected"';
                                                                                                                        }
                                                                                                                        ?>
                                                                                                                        >View Only</option>
                                                                                                                <option value=2
                                                                                                                        <?php
                                                                                                                        if ( $edit_security_level->bus_inventor4 > 1) {
                                                                                                                            echo ' selected="selected"';
                                                                                                                        }
                                                                                                                        ?>
                                                                                                                        >Enabled</option>
                                                                                                        </select>
												</td>
												<td>
													Inventory Recipes
												</td>
												<td>
													<input type="checkbox" name="bus_inventor5" value="1" <?php
														if ( $edit_security_level->bus_inventor5 ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													Inventory Vendors
												</td>
												<td>
													<input type="checkbox" name="bus_control" value="1" <?php
														if ( $edit_security_level->bus_control ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													Control Sheet
												</td>
											</tr>
											<tr>
												<td>
                                                                                                        <select name="bus_menu"> 
														<option value=0
                                                                                                                        <?php
                                                                                                                        if ( $edit_security_level->bus_menu < 1) {
                                                                                                                            echo ' selected="selected"';
                                                                                                                        }
                                                                                                                        ?>
                                                                                                                        >Disabled</option>
                                                                                                                <option value=1
                                                                                                                        <?php
                                                                                                                        if ( $edit_security_level->bus_menu == 1) {
                                                                                                                            echo ' selected="selected"';
                                                                                                                        }
                                                                                                                        ?>
                                                                                                                        >View Only</option>
                                                                                                                <option value=2
                                                                                                                        <?php
                                                                                                                        if ( $edit_security_level->bus_menu > 1) {
                                                                                                                            echo ' selected="selected"';
                                                                                                                        }
                                                                                                                        ?>
                                                                                                                        >Enabled</option>
                                                                                                        </select>
												</td>
												<td>
													Menu
												</td>
												<td>
													<input type="checkbox" name="bus_menu_linking" value="1" <?php
														if ( $edit_security_level->bus_menu_linking ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													POS Menu Linking
												</td>
												<td>
													<select name="bus_menu_pos">
<?php
				$options = array(
					0 => 'Disabled',
					1 => 'View Only',
					2 => 'Inventory',
					3 => 'Standard',
					4 => 'Master',
				);
				foreach ( $options as $key => $label ) {
?>
														<option value="<?php echo $key; ?>"<?php
															if ( intval( $edit_security_level->bus_menu_pos ) == intval( $key ) ) {
																echo ' selected="selected"';
															}
														?>><?php echo $label; ?></option>
<?php
				}

?>
													</select>
												</td>
												<td>
													POS Menu
												</td>
											</tr>
											<tr>
												<td>
													<input type="text" name="bus_menu_pos_report" value="<?php
														echo $edit_security_level->bus_menu_pos_report;
													?>" size="3" />
												</td>
												<td>
													POS Reporting
												</td>
												<td>
													<input type="checkbox" name="menu_nutrition" value="1" <?php
														if ( $edit_security_level->menu_nutrition ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													Menu Nutrition
												</td>
												<td>
													<input type="checkbox" name="bus_order_setup" value="1" <?php
														if ( $edit_security_level->bus_order_setup ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													Order Setup
												</td>
											</tr>
											<tr>
												<td>
													<input type="checkbox" name="bus_request" value="1" <?php
														if ( $edit_security_level->bus_request ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													Requests
												</td>
<?php
/*
* /
?>
												<td>
													<select name="bus_menu_pos_mgmt">
<?php
				$options = array(
					0 => 'Disabled',
					1 => 'Cust/Groups/Taxes',
					2 => 'Cust/Groups/Taxes/Food',
					3 => 'All',
					4 => 'Cold Food',
				);
				foreach ( $options as $key => $label ) {
?>
														<option value="<?php echo $key; ?>"<?php
															if ( intval( $edit_security_level->bus_menu_pos_mgmt ) == intval( $key ) ) {
																echo ' selected="selected"';
															}
														?>><?php echo $label; ?></option>
<?php
				}

?>
													</select>
												</td>
												<td>
													POS Management
												</td>
<?php
/*
*/
?>
											</tr>
											
											<tr>
												<td colspan="6">
										<fieldset>
											<legend>POS Management</legend>
											<table>
												<tbody>
													<tr>
														<td>
															<label>
																<input type="checkbox" name="posmgmt_status" value="1" <?php
																	if ( $edit_security_level->posmgmt_status ) {
																		echo ' checked="checked"';
																	}
																?> />
																Status
															</label>
														</td>
														<td>
															<label>
																<input type="checkbox" name="posmgmt_setup" value="1" <?php
																	if ( $edit_security_level->posmgmt_setup ) {
																		echo ' checked="checked"';
																	}
																?> />
																Setup
															</label>
														</td>
														<td>
															<label>
																<input type="checkbox" name="posmgmt_cashiers" value="1" <?php
																	if ( $edit_security_level->posmgmt_cashiers ) {
																		echo ' checked="checked"';
																	}
																?> />
																Cashier Users
															</label>
														</td>
													</tr>
													<tr>
														<td>
															<label>
																<input type="checkbox" name="posmgmt_posscreens" value="1" <?php
																	if ( $edit_security_level->posmgmt_posscreens ) {
																		echo ' checked="checked"';
																	}
																?> />
																POS Screens
															</label>
														</td>
														<td>
															<label>
																<input type="checkbox" name="posmgmt_modifiers" value="1" <?php
																	if ( $edit_security_level->posmgmt_modifiers ) {
																		echo ' checked="checked"';
																	}
																?> />
																Modifiers
															</label>
														</td>
														<td>
															<label>
																<input type="checkbox" name="posmgmt_staticmods" value="1" <?php
																	if ( $edit_security_level->posmgmt_staticmods ) {
																		echo ' checked="checked"';
																	}
																?> />
																Static Modifiers
															</label>
														</td>
													</tr>
													<tr>
														<td>
															<label>
																<input type="checkbox" name="posmgmt_customers" value="1" <?php
																	if ( $edit_security_level->posmgmt_customers ) {
																		echo ' checked="checked"';
																	}
																?> />
																Customers
															</label>
														</td>
														<td>
															<label>
																<input type="checkbox" name="posmgmt_cardnums" value="1" <?php
																	if ( $edit_security_level->posmgmt_cardnums ) {
																		echo ' checked="checked"';
																	}
																?> />
																Card Numbers
															</label>
														</td>
														<td>
															<label>
																<input type="checkbox" name="posmgmt_menugroup" value="1" <?php
																	if ( $edit_security_level->posmgmt_menugroup ) {
																		echo ' checked="checked"';
																	}
																?> />
																Menu Groups
															</label>
														</td>
													</tr>
													<tr>
														<td>
															<label>
																<input type="checkbox" name="posmgmt_taxes" value="1" <?php
																	if ( $edit_security_level->posmgmt_taxes ) {
																		echo ' checked="checked"';
																	}
																?> />
																Taxes
															</label>
														</td>
														<td>
															<label>
																<input type="checkbox" name="posmgmt_promotions" value="1" <?php
																	if ( $edit_security_level->posmgmt_promotions ) {
																		echo ' checked="checked"';
																	}
																?> />
																Promotions
															</label>
														</td>
														<td>
															<label>
																<input type="checkbox" name="posmgmt_coldfood" value="1" <?php
																	if ( $edit_security_level->posmgmt_coldfood ) {
																		echo ' checked="checked"';
																	}
																?> />
																Cold Food Orders
															</label>
														</td>
													</tr>
													<tr>
														<td>
															<label>
																<input type="checkbox" name="posmgmt_customerutils" value="1" <?php
																	if ( $edit_security_level->posmgmt_customerutils ) {
																		echo ' checked="checked"';
																	}
																?> />
																Customer Utilities
															</label>
														</td>
														<td>
															<label>
																<input type="checkbox" name="posmgmt_vmachine" value="1" <?php
																	if ( $edit_security_level->posmgmt_vmachine ) {
																		echo ' checked="checked"';
																	}
																?> />
																Vending Machines
															</label>
														</td>
													</tr>
												</tbody>
											</table>
										</fieldset>
												</td>
											</tr>
											
											
											<tr>
												<td>
													<input type="text" name="route_collection" value="<?php
														echo $edit_security_level->route_collection;
													?>" size="3" />
												</td>
												<td>
													Route Collections
												</td>
												<td>
													<input type="checkbox" name="route_labor" value="1" <?php
														if ( $edit_security_level->route_labor ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													Payroll/Route Linking
												</td>
												<td>
													<input type="text" name="route_detail" value="<?php
														echo $edit_security_level->route_detail;
													?>" size="3" />
												</td>
												<td>
													Route Detail<font color="green"><b>*</b></font>
												</td>
											</tr>
											<tr>
												<td>
													<input type="text" name="route_machine" value="<?php
														echo $edit_security_level->route_machine;
													?>" size="3" />
												</td>
												<td>
													Rt. Machine Mgmt
												</td>
												<td>
													<input type="text" name="bus_timeclock" value="<?php
														echo $edit_security_level->bus_timeclock;
													?>" size="3" />
												</td>
												<td>
													Time Clock<font color="purple"><b>*</b></font>
												</td>
												<td>
													<input type="checkbox" name="bus_budget" value="1" <?php
														if ( $edit_security_level->bus_budget ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													Budget
												</td>
											</tr>

											<tr>
												<td>
													<input type="checkbox" name="pos_settings" value="1" <?php
														if ( $edit_security_level->pos_settings ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													POS Settings
												</td>
												<td  colspan="4"></td>
											</tr>

											<tr>
												<td colspan="6">
<?php
$payroll_depts = Treat_Model_User_Security_Department::getPayrollDepartmentSecurityList( $edit_security_level->securityid );
?>
													<b>
														Payroll Departments Allowed:
													</b>
												</td>
											</tr>
<?php
	# add enough blank items onto the end 
	if ( ( $count = count( $payroll_depts ) % 3 ) ) {
		for ( $i = 0, $j = 3 - $count; $i < $j; $i++ ) {
			$payroll_depts[] = null;
		}
	}
	
	foreach ( $payroll_depts as $key => $dept ) {
		if ( 0 == $key % 3 ) {
?>
											<tr>
<?php
		}
		if ( $dept ) {
?>
												<td>
													<input type="checkbox" name="dept[<?php
														# these should honestly have names like dept[$dept_id], not just a number
														echo $dept->dept_id;
													?>]" value="1" <?php
														if ( $dept->dept_checked ) {
															echo ' checked="checked"';
														}
													?> />
												</td>
												<td>
													<?php
														echo $dept->dept_code, ' ', $dept->dept_name;
													?> 
												</td>
<?php
		}
		if ( 2 == $key % 3 ) {
?>
											</tr>
<?php
		}
		
	}
?>
											<tr>
												<td colspan="6" align="right">
													<input type="submit" value="Save" />
												</td>
											</tr>
										</table>
									</td>
<?php
	if ( !$is_custom_security ) {
?>
									<td>
										<table cellspacing="0" cellpadding="0" style="border:1px solid black;" bgcolor="#ffff99" width="100%">
											<tr>
												<td>
													<b>
														Users Assigned this Security:
													</b>
												</td>
											</tr>
<?php
				$list = Treat_Model_User_Login::getUserListBySecurityId( $edit_security_level->securityid );
				foreach ( $list as $user_obj ) {
?>
											<tr>
												<td>
													<?php
														echo sprintf(
															'%s, %s (%s)'
															,$user_obj->lastname
															,$user_obj->firstname
															,$user_obj->username
														);
													?> 
												</td>
											</tr>
<?php
				}
?>
										</table>
									</td>
<?php
	}
?>
								</tr>
							</table>
						</form>
					</td>
				</tr>
<?php
			}
?>
<?php
	if ( !$is_custom_security ) {
?>
				<tr bgcolor="#e8e7e7">
					<td colspan="2">
						<form action="/ta/savesecurity.php" method="post">
							<input type="hidden" name="username" value="<?php echo $user; ?>" />
							<input type="hidden" name="password" value="<?php echo $pass; ?>" />
							<ul>
								<li>
									New Security:
									<input type="text" size="20" name="security_name" />
									<input type="submit" value="Add" />
								</li>
							</ul>
						</form>
					</td>
				</tr>
<?php
	}
?>
				<tr bgcolor="black">
					<td colspan="2" height="1"></td>
				</tr>
				<tr bgcolor="#e8e7e7">
					<td colspan="2">
						<font size="2">
							<font color=red>*</font>1: Hours Only for Assigned Units
							<br/>
							&nbsp;&nbsp;2: Hours for Assigned Units/Totals+Summary for Home Unit Only
							<br/>
							&nbsp;&nbsp;3-8: Hours+Totals+Summary for Assigned Units
							<br/>
							&nbsp;&nbsp;9: Admin
						</font>
					</td>
				</tr>
				<tr bgcolor="black"><td colspan="2" height="1"></td></tr>
				<tr bgcolor="#e8e7e7">
					<td colspan="2">
						<font size="2">
							<font color="blue">*</font>1: Shipping Only
							<br/>
							&nbsp;&nbsp;2: Receiving Only
							<br/>
							&nbsp;&nbsp;3: Orders Only
							<br/>
							&nbsp;&nbsp;4-9: Shipping+Receiving+Orders
							<br/>
							&nbsp;&nbsp;*This is for Vending Only.  For Contract or Catering Orders, 0 is disabled, 1-9 enabled.
						</font>
					</td>
				</tr>
				<tr bgcolor="black"><td colspan="2" height="1"></td></tr>
				<tr bgcolor="#e8e7e7">
					<td colspan="2">
						<font size="2">
							<font color="green">*</font>1: Route Manager
							<br/>
							&nbsp;&nbsp;2-9: Admin/Supervisor
						</font>
					</td>
				</tr>
				<tr bgcolor="black"><td colspan="2" height="1"></td></tr>
				<tr bgcolor="#e8e7e7">
					<td colspan="2">
						<font size="2">
							<font color="purple">*</font>1: Timeclock for Home Unit
							<br/>
							&nbsp;&nbsp;2-8: Timeclock for all Units assigned
							<br/>
							&nbsp;&nbsp;9: Override submitted days
						</font>
					</td>
				</tr>
<?php
		//echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";
?>
			</table>
