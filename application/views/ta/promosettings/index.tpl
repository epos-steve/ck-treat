<style>
.links{width:100px; float:left; padding:0; margin:0 0 10px 0;}
.selects{width:350px;}
h3{margin:0 0 10px 0;}
label{width:100px; float:left;}
.clear_form{margin-right:35px;}
</style>

<div id="loading" style="display:none;">
	<div id="overlay"></div>
	<div id="message">
		Loading... Please Wait<br/><img src="/assets/images/preload/loading.gif" height="22" width="126">
	</div>
</div>

<table cellspacing='0' cellpadding='0' border='0' width='100%'>
	<tr bgcolor='#F6F9FA'>
		<td colspan='2' align='right'>&nbsp;</td>
	</tr>
	<tr style='height: 1px; background-color: black; vertical-align: top;'>
		<td colspan='2'>
			<img src='/assets/images/spacer.gif' alt='' />
		</td>
	</tr>
	<tr bgcolor='#E8E7E7'>
		<td colspan='2'>
			<br />
			<table width='99%' cellspacing='0' cellpadding='0' style='margin-left: auto; margin-right: auto;'>
				<tr bgcolor='#E8E7E7' valign='top' id='content'>
					<td width='20%'>
						<h3>Setting Type</h3>
						<select id="mode" name="mode">
							<option value="">Select</option>
							<option value="manufacturer">Manufacturer</option>
							<option value="category">Category</option>
						</select>
					</td>
					<td width=2%>&nbsp;</td>
					<td>
						<div id="manufacturer_wrap" style="display:none;">
							<h3>Manufacturer</h3>
							
							<label for="manufacturer">Manufacturer: </label>
							<select style="display:none;" id="manufacturer" class="selects" name="manufacturer">
								<?php print $man_select; ?>
							</select>
							
							<form name="manufacturer_form" id="manufacturer_form">
								<label for="man_name">Name: </label> <input size="55" type="text" name="man_name" id="man_name" />
								
								<input type="hidden" name="man_id" id="man_id" value="-1" /><br />
								
								<input type="button" name="clear_man" class="clear_form" value="Clear Form" />
								<input type="submit" name="submit_man" id="submit_man" value="Save" />
								<input type="button" name="delete_man" id="delete_man" style="display:none;" value="Delete" />
							</form>
						</div>
						<div id="category_wrap" style="display:none;">
							<h3>Category</h3>
							
							<label for="category">Category: </label>
							<select style="display:none;" id="category" class="selects" name="category">
								<?php print $cat_select; ?>
							</select>
						
							<form name="category_form" id="category_form">
								<label for="cat_name">Name: </label> <input size="55" type="text" name="cat_name" id="cat_name" /><br />
								
								<input type="hidden" name="cat_id" id="cat_id" value="-1" />
								
								<input type="button" name="clear_cat" class="clear_form" value="Clear Form" />
								<input type="submit" name="submit_cat" id="submit_cat" value="Save" />
								<input type="button" name="delete_cat" id="delete_cat" style="display:none;" value="Delete" />
							</form>
						</div>
						
						<div id="res_message">&nbsp;</div>
					</td>
				</tr>
			</table>
			<p></p>
		</td>
	</tr>
</table>