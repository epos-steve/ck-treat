<?php
Treat_Controller_Abstract::addBodyClass( 'overwrite_account' );

$beginning_of_month = Treat_Date::getBeginningOfMonth( $viewmonth, true );
$end_of_month = Treat_Date::getEndOfMonth( $beginning_of_month, true );

?>


<div class="month_label">
	<?php echo date( 'F Y', $viewmonth ); ?>
</div>

<div class="account_name">
	<?php echo $account->name; ?>
</div>


<form method="post" action="">
	<div>
		<input type="hidden" name="business_id" value="<?php echo $business_id; ?>" />
		<input type="hidden" name="company_id" value="<?php echo $company_id; ?>" />
		<input type="hidden" name="account_id" value="<?php echo $account_id; ?>" />
		<input type="hidden" name="viewmonth" value="<?php echo $viewmonth; ?>" />
		<input type="hidden" name="update" value="true" />
<?php
	$counter = 1;
	foreach ( $data as $week => $week_data ) {
		if ( $week >= $beginning_of_month && $week <= $end_of_month ) {
?>
		<div class="week_info">
			<label for="week<?php echo $week; ?>amount">
				<?php echo date( 'Y-m-d ', $week ); ?>
				<strong>WEEK<?php echo $counter; ?></strong>
			</label>
			<input type="hidden" name="week[<?php echo $week; ?>][amount][orig]" value="<?php echo $week_data->amount; ?>" />
			<input type="text"   name="week[<?php echo $week; ?>][amount][new]" value="<?php echo $week_data->amount; ?>" id="week<?php echo $week; ?>amount" />
			<input type="hidden" name="week[<?php echo $week; ?>][percent][orig]" value="<?php echo $week_data->percent; ?>" />
			<input type="text"   name="week[<?php echo $week; ?>][percent][new]" value="<?php echo $week_data->percent; ?>" id="week<?php echo $week; ?>percent" />% of 
			<input type="hidden" name="week[<?php echo $week; ?>][sales][orig]" value="<?php echo $week_data->sales; ?>" />
			<input type="text"   name="week[<?php echo $week; ?>][sales][new]" value="<?php echo $week_data->sales; ?>" id="week<?php echo $week; ?>sales" disabled="disabled" size="10" /> sales
		</div>
<?php
			$counter++;
		}
	}
?>

		<div class="submit_button">
			<button type="submit">
				Update
			</button>
		</div>
	</div>
</form>

