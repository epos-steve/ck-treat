<?php
Treat_Controller_Abstract::addBodyClass( 'buscontrol2' );
#require_ta_bootstrap();

if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
	echo '<pre>';
	echo '$GLOBALS[\'user\'] = ';
	var_dump( $GLOBALS['user'] );
	echo '$GLOBALS[\'pass\'] = ';
	var_dump( $GLOBALS['pass'] );
	echo '$GLOBALS[\'num\'] = ';
	var_dump( $GLOBALS['num'] );
#	echo '$_SESSION = ';
#	var_dump( $_SESSION );
	echo '$passed_cookie_login = ';
	var_dump( $passed_cookie_login );
	echo '$GLOBALS[\'security_level\'] = ';
	var_dump( $GLOBALS['security_level'] );
	echo '$GLOBALS[\'bid\'] = ';
	var_dump( $GLOBALS['bid'] );
	echo '$GLOBALS[\'businessid\'] = ';
	var_dump( $GLOBALS['businessid'] );
	echo '$GLOBALS[\'cid\'] = ';
	var_dump( $GLOBALS['cid'] );
	echo '$GLOBALS[\'companyid\'] = ';
	var_dump( $GLOBALS['companyid'] );
	echo '</pre>';
}

	$prevperiod = getPostOrGetVariable( 'prevperiod' );
	$nextperiod = getPostOrGetVariable( 'nextperiod' );
	$viewmonth  = getPostOrGetVariable( 'viewmonth' );
	$viewmonth  = Treat_GeneralLedger_Date::getBeginningOfMonth( $viewmonth, true );
	$audit      = getPostOrGetVariable( 'audit' );
	$show_sales = getPostOrGetVariable( 'show_sales' );
	if ( 'false' == $show_sales ) {
		$show_sales = false;
	}

#	require_once( DOC_ROOT . '/ta/lib/class.credits.php' );
#	require_once( DOC_ROOT . '/ta/lib/class.labor.php' );

	////header
	if ( isset( $uri ) ) {
		$page = $uri->getPath();
	}
	else {
		$page = '/ta/buscontrol2.php';
	}
?>

	<table width="100%" cellspacing="0" cellpadding="0" class="month_header">
		<tr>
			<td class="prev">
				[<a href="<?php echo $page; ?>?bid=<?php
						echo $GLOBALS['businessid'];
					?>&amp;cid=<?php
						echo $GLOBALS['companyid'];
					?>&amp;viewmonth=<?php
						echo date( 'Y-m-01', Treat_GeneralLedger_Date::getPreviousMonthBeginning( $viewmonth ) );
				?>"><span>PREV</span></a>]
			</td>
			<td class="month_label">
				<?php echo date( 'F Y', $viewmonth ); ?>
			</td>
			<td class="next">
				[<a href="<?php echo $page; ?>?bid=<?php
						echo $GLOBALS['businessid'];
					?>&amp;cid=<?php
						echo $GLOBALS['companyid'];
					?>&amp;viewmonth=<?php
						echo date( 'Y-m-01', Treat_GeneralLedger_Date::getNextMonthBeginning( $viewmonth ) )
				?>"><span>NEXT</span></a>]
			</td>
		</tr>

	</table>
	<div>&nbsp;</div>



<?php
	
	/////////////
	/////data////
	/////////////
	
	$cs_week_end = Treat_GeneralLedger_Date::getMonthOfFridays( $viewmonth );
	$controlSheet = Treat_Model_GeneralLedger_ControlSheet::getSingleton(
		Treat_Model_GeneralLedger_AccountGroup::getAccountGroups(
			$GLOBALS['businessid']
			,$viewmonth
			,$cs_week_end
		)
	);
	$controlSheet->setBusinessId( $GLOBALS['businessid'] );
	$controlSheet->setViewMonth( $viewmonth );
	$controlSheet->setMonthOfFridays( $cs_week_end );
	if ( $show_sales ) {
		$controlSheet->setOverrideDisplayWeek( $show_sales );
	}
	$operating_days = $controlSheet->getOperatingDaysAll();
	$accountGroups = $controlSheet->process( $viewmonth, $cs_week_end );
?>
		<table cellspacing="0" cellpadding="0" class="control_sheet_data">
			<thead>
				<tr>
					<td>&nbsp;CONTROL SHEET</td>
<?php
	foreach ( $cs_week_end as $key => $value ) {
		$out = sprintf(
			'<span>%s <span>WEEK%s</span></span>'
			,date( 'm/d/Y', $value )
			,$key
		);
		if ( $GLOBALS['security_level'] > 8 ) {
			$out = sprintf(
				'<a href="utility.php?cid=%s&amp;import_bid=%s&amp;date=%s#bottom">%s</a>'
				,$GLOBALS['companyid']
				,$GLOBALS['businessid']
				,date( 'Y-m-d', $value )
				,$out
			);
		}
?>
					<td colspan="2" class="week_label"><?php echo $out; ?></td>
<?php
	}
?>
					<td colspan="6" class="month_label">TOTAL</td>
				</tr>
				
				
				
				
				<tr>
					<td>
<?php
	if ( 9 == $_SESSION->getUser()->security_level ) {
?>

						<form method="get" action="">
							<div>
								<input type="hidden" name="bid" value="<?php echo $GLOBALS['businessid']; ?>" />
								<input type="hidden" name="cid" value="<?php echo $GLOBALS['companyid']; ?>" />
<?php
		if ( Treat_Date::getBeginningOfMonth( null, true ) != $viewmonth ) {
?>

								<input type="hidden" name="viewmonth" value="<?php echo date( 'Y-m-d', $viewmonth ); ?>" />
<?php
		}
		if ( !$show_sales ) {
?>

								<input type="hidden" name="show_sales" value="true" />
								<button type="submit">Show Sales</button>
<?php
		}
		else {
?>

								<button type="submit">Undo Show Sales</button>
<?php
		}
?>

							</div>
						</form>
<?php
	}
	else {
#		echo '&nbsp;';
	}
?>

					</td>
<?php
	///legend
	foreach ( $cs_week_end AS $key => $value ) {
?>
					<th class="week_label">Actual</th>
					<th class="week_label">%</th>
<?php
	}
?>
					<th class="week_label">ACT</th>
					<th class="week_label">%</th>
					<th class="week_label">EST</th>
					<th class="week_label">%</th>
					<th class="week_label">BGT</th>
					<th class="week_label">+/-</th>
				</tr>
			</thead>
			
			<tfoot>
				<tr class="data_line">
					<th class="name">Operating Days</th>
<?php
	foreach ( $cs_week_end as $key => $week ) {
?>
					<td class="operating_days" colspan="2"><?php
						echo $controlSheet->getOperatingDaysByWeekToString( $week );
					?></td>
<?php
	}
?>
					<td class="total operating_days" colspan="6"><?php
						echo $controlSheet->getOperatingDaysForMonthToString();
					?></td>
				</tr>
				
<?php
	if ( 9 == $_SESSION->getUser()->security_level ) {
?>
				<tr class="data_line">
					<td>&nbsp;</td>
<?php
		foreach ( $cs_week_end as $key => $week ) {
?>
					<td class="delete_week" colspan="2">
						<form action="/ta/buscontrolc/delete_gl_items" method="post">
							<div>
								<input type="hidden" name="company_id" value="<?php echo $GLOBALS['companyid']; ?>" />
								<input type="hidden" name="business_id" value="<?php echo $GLOBALS['businessid']; ?>" />
								<input type="hidden" name="type" value="week" />
								<input type="hidden" name="date" value="<?php echo $week ?>" />
								<input type="submit" value="Delete" />
							</div>
						</form>
					</td>
<?php
		}
?>
					<td class="total delete_week" colspan="6">
						<form action="/ta/buscontrolc/delete_gl_items" method="post">
							<div>
								<input type="hidden" name="company_id" value="<?php echo $GLOBALS['companyid']; ?>" />
								<input type="hidden" name="business_id" value="<?php echo $GLOBALS['businessid']; ?>" />
								<input type="hidden" name="type" value="budget" />
								<input type="hidden" name="date" value="<?php echo $viewmonth ?>" />
								<input type="submit" value="Delete Budgets" />
							</div>
						</form>
					</td>
				</tr>
<?php
	}
?>
			</tfoot>
			
			<tbody>
<?php
	////sales
	$sales = array();

	////numbers
	
	// if you need to define a different format for 1 of these, just pull this
	// apart & define them separately
	Treat_Model_GeneralLedger_Object_Abstract::setMonthAmountFormat(
		Treat_Model_GeneralLedger_Object_Abstract::setWeekAmountFormat(
			function( $amount ) {
				return str_replace( '$', '', money_format( '%.0n', $amount ) );
			}
		)
	);
	
	// if you need to define a different format for 1 of these, just pull this
	// apart & define them separately
	Treat_Model_GeneralLedger_Object_Abstract::setMonthPercentageFormat(
		Treat_Model_GeneralLedger_Object_Abstract::setWeekPercentageFormat(
			function( $amount ) {
				return number_format( $amount * 100, 1 ) . '%';
			}
		)
	);
	
	$colspan = count( $cs_week_end ) * 2 + 5;
	foreach ( $accountGroups as $accountGroup ) {
		$accounts = $accountGroup->getAccounts();
		foreach ( $accounts as $account ) {
?>
				<tr class="data_line account_name" >
					<td class="name"><?php echo htmlentities( $account->name ); ?></td>
<?php
			foreach ( $cs_week_end as $week ) {
?>
					<td class="weekly amount"><?php
						echo $account->getWeekAmount( $week, true );
					?></td>
					<td class="weekly percentage"><?php
							echo $account->getWeekPercentage( $week, true );
					?></td>
<?php	
			}
			
			if ( $account instanceof Treat_Model_GeneralLedger_Account_ImportObject ) {
				$a_href = sprintf(
					'<a href="/ta/buscontrol2.popup.php?bid=%d&amp;cid=%d&amp;aid=%d&amp;viewmonth=%s">%%s</a>'
					,$GLOBALS['businessid']
					,$GLOBALS['companyid']
					,$account->id
					,date( 'Y-m-01', $viewmonth )
				);
			}
			else {
				$a_href = '%s';
			}
?>
					<td class="total actual amount"><?php
						echo sprintf( $a_href, $account->getMonthAmount( true ) );
					?></td>
					<td class="total actual percentage <?php
						if ( 'SALES' != $accountGroup->name ) {
							echo $account->getMonthDifferenceClass( 'positive', 'negative' );
						}
					?>"><?php
							echo sprintf( $a_href, $account->getMonthPercentage( true ) );
					?></td>
					
					<td class="total estimate amount"><?php
						echo sprintf( $a_href, $account->getMonthEstimate( true ) );
					?></td>
					<td class="total estimate percentage <?php
						if ( 'SALES' != $accountGroup->name ) {
							echo $account->getMonthDifferenceClass( 'positive', 'negative' );
						}
					?>"><?php
							echo sprintf( $a_href, $account->getMonthEstimatePercentage( true ) );
					?></td>
					
					<td class="total budget amount"><?php
						echo sprintf( $a_href, $account->getBudgetAmount( true ) );
					?></td>
					<td class="total budget percentage <?php
#						if ( 'SALES' != $accountGroup->name ) {
#							echo $account->getMonthDifferenceClass( 'positive', 'negative' );
#						}
					?>"><?php
							echo sprintf( $a_href, $account->getMonthBudgetPercentage( true ) );
					?></td>
				</tr>
				

<?php
		}
?>
				<tr class="data_line group_name" >
					<td class="name"><?php echo $accountGroup->name; ?></td>
<?php
			foreach ( $cs_week_end as $week ) {
?>
					<td class="weekly amount"><?php
						echo $accountGroup->getWeekAmount( $week, true );
					?></td>
					<td class="weekly percentage"><?php
							echo $accountGroup->getWeekPercentage( $week, true );
					?></td>
<?php
			}
?>
					<td class="total actual amount"><?php
						echo $accountGroup->getMonthAmount( true );
					?></td>
					<td class="total actual percentage <?php
						if ( 'SALES' != $accountGroup->name ) {
							echo $accountGroup->getMonthDifferenceClass( 'positive', 'negative' );
						}
					?>"><?php
							echo $accountGroup->getMonthPercentage( true );
					?></td>
					
					<td class="total estimate amount"><?php
						echo $accountGroup->getMonthEstimate( true );
					?></td>
					<td class="total estimate percentage <?php
						if ( 'SALES' != $accountGroup->name ) {
							echo $accountGroup->getMonthDifferenceClass( 'positive', 'negative' );
						}
					?>"><?php
							echo $accountGroup->getMonthEstimatePercentage( true );
					?></td>
					
					<td class="total budget amount"><?php
						echo $accountGroup->getBudgetAmount( true );
					?></td>
					<td class="total budget percentage <?php
#						if ( 'SALES' != $accountGroup->name ) {
#							echo $accountGroup->getMonthDifferenceClass( 'positive', 'negative' );
#						}
					?>"><?php
							echo $accountGroup->getMonthBudgetPercentage( true );
					?></td>
				</tr>
<?php
	}
?>
			</tbody>
		</table>

