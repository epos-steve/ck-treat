<?php
		ob_start();
?>
			<table class="buscontrol2_popup">
				<thead class="account_parent">
					<tr>
						<th colspan="3" class="name"><?php echo htmlentities( $accountParent->name ); ?>:</th>
					</tr>
				</thead>
<?php
		foreach ( $accounts as $account ) {
?>
				<tbody>
					<tr class="account_child">
						<td colspan="3" class="name"><?php
							echo htmlentities( $account->number ), ' ', htmlentities( $account->name );
						?></td>
					</tr>
<?php
			if ( defined( 'DEBUG' ) && DEBUG ) {
?>
					<tr>
						<td colspan="3"><?php var_dump( $account ); ?></td>
					</tr>
					<tr>
						<td colspan="3"><?php var_dump( $account['account_items'] ); ?></td>
					</tr>
<?php
			}
			foreach ( $account['account_items'] as $accountItem ) {
?>
					<tr class="account_items">
						<td class="label1"><?php echo htmlentities( $accountItem->label1 ); ?></td>
						<td class="label2"><?php echo htmlentities( $accountItem->label2 ); ?></td>
						<td class="amount <?php
							echo $accountItem->getHtmlClass(
								$accountItem->amount
								,'positive'
								,'negative'
								,$accountParent->getAccountGroupId()
							);
						?>"><?php echo money_format( '%.2n', $accountItem->amount ) ?></td>
					</tr>
<?php
			}
?>
					<tr class="account_child">
						<th colspan="2" class="total">Total <?php echo htmlentities( $account->name ); ?></th>
						<th class="amount <?php
							echo $account->getHtmlClass(
								$account->detail_amount
								,'positive'
								,'negative'
								,$accountParent->getAccountGroupId()
							);
						?>"><?php echo money_format( '%.2n', $account->detail_amount ) ?></th>
					</tr>
					<tr class="account_child">
						<td colspan="3">&nbsp;</td>
					</tr>
				</tbody>
<?php
		}
?>
				<tbody class="account_parent">
					<tr>
						<th colspan="2" class="total">Total <?php echo htmlentities( $accountParent->name ); ?></th>
						<th class="amount <?php
							echo $accountParent->getHtmlClass(
								$accountParent->detail_amount
								,'positive'
								,'negative'
								,$accountParent->getAccountGroupId()
							);
						?>"><?php echo money_format( '%.2n', $accountParent->detail_amount ); ?></th>
					</tr>
				</tbody>
			</table>

<?php
		$content = ob_get_flush();
?>
			<form action="<?php
				echo htmlentities( $location );
			?>" method="post">
				<div>
					<input type="hidden" name="bid" value="<?php echo $businessid; ?>" />
					<input type="hidden" name="cid" value="<?php echo $companyid; ?>" />
					<input type="hidden" name="aid" value="<?php echo $parent_account_id; ?>" />
					<input type="hidden" name="viewmonth" value="<?php echo date( 'Y-m-d', $viewmonth ); ?>" />
					<input type="hidden" name="content" value="<?php echo base64_encode( $content ); ?>" />
					<div class="comment">
						<label for="comment">Comment:</label>
						<textarea name="comment" id="comment" cols="60" rows="10"></textarea>
					</div>
					<div class="email_address">
						<label for="email_address">Email:</label>
						<input type="text" name="email_address" id="email_address" size="52" />
					</div>
					<div class="email">
						<label>
							<input type="checkbox" name="email[ed]" checked="checked" />
							email Ed
						</label>
					</div>
					<div class="email">
						<label>
							<input type="checkbox" name="email[renee]" checked="checked" />
							email Renee
						</label>
					</div>
					<div class="button">
						<button type="submit">email this</button>
					</div>
				</div>
			</form>
