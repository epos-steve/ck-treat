<style>
.hovered{background-color:yellow;}
</style>

<table cellspacing='0' cellpadding='0' border='0' width='100%'>
	<tr bgcolor='#F6F9FA'>
		<td colspan='2' align='right'>&nbsp;</td>
	</tr>
	<tr style='height: 1px; background-color: black; vertical-align: top;'>
		<td colspan='2'>
			<img src='/assets/images/spacer.gif' alt='' />
		</td>
	</tr>
	
	<tr valign=top bgcolor=#E8E7E7>
		<td align="center">
			<table id="item_table" style="margin-top:20px;background-color:#fff;" cellspacing="0" cellpadding="0" border="1" width="75%">
				<thead>
					<tr>
						<th class="ui-state-default">UPC</th>
						<th class="ui-state-default">Item Name</th>
						<th class="ui-state-default">Number Sold</th>
					</tr>
				</thead>
				<tbody>
					<?php print $items;	?>
					<tr>
						<td colspan="3" align="center">
							<div style="width:20%;margin-top:10px;">
								<?php
									$previous ? print '<a class="ui-state-active" style="float:left;border:none;text-decoration:none;" href="?'.$previous.'">&larr; Previous</a>' : '';
									$next ? print '<a class="ui-state-active" style="float:right;border:none;text-decoration:none;" href="?'.$next.'">Next &rarr;</a>' : '';
								?>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</td>
	</tr>	
	
	<tr valign="bottom" bgcolor="#E8E7E7">
		<td align="right">
			<form id="manage" action="/ta/update_pos.php" method="post" style="margin: 0; padding: 0; display: inline;">
				<div class="info"></div>
				<input type="hidden" id="businessid" name="businessid" value="<?php echo $GLOBALS['businessid']; ?>" />
			</form>
			&nbsp;
		</td>
	</tr>
</table>