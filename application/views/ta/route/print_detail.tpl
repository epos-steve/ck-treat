<?php
$title = 'printroute.php';

?>
<!DOCTYPE html
	PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title><?php
			if ( isset( $title ) && $title ) {
				echo $title, ' :: ', \EE\Config::singleton()->getValue( 'application', 'page_title' );
			}
			else {
				echo \EE\Config::singleton()->getValue( 'application', 'page_title' );
			}
		?></title>
		<meta name="verify-v1" content="PDmo8LnHK6P8AitDtvqWv2k4/4iF5RKXVji1YAEExSk=" />
<?php
	\EE\Controller\Base::outputArrayVariable( \EE\Controller\Base::getMetas() );
?>
		<link type="image/vnd.microsoft.icon" rel="shortcut icon" href="/favicon.ico" />
		<link type="image/vnd.microsoft.icon" rel="icon"          href="/favicon.ico" />
		<style type="text/css">
#loading {
	width: 200px;
	height: 48px;
	background-color: transparent;
	position: absolute;
	left: 50%;
	top: 50%;
	margin-top: -50px;
	margin-left: -100px;
	text-align: center;
}
		</style>
		<script type="text/javascript" src="preLoadingMessage.js"></script>
<?php
	\EE\Controller\Base::outputArrayVariable( \EE\Controller\Base::getCss() );
	\EE\Controller\Base::outputArrayVariable( \EE\Controller\Base::getJavascriptsAtTop() );
?>

		<style type="text/css">
			div.employee_detail {
				margin-bottom: 1em;
				page-break-before: always
			}
			
			div.employee_detail img {
				border: 0 none;
			}
			
			div.employee_detail div.table_wrapper {
				border: 1px solid black;
				padding: 2px;
			}
			
			table.employee_detail {
			}
			
			table.employee_detail tr.row th,
			table.employee_detail tr.row td
			{
				border-bottom: 2px solid black;
				text-align: right;
			}
			
			table.employee_detail tr.total th,
			table.employee_detail tr.total td
			{
				border-bottom: 0 none;
			}
			
			tr.employee_info,
			tr.employee_info th
			{
				background-color: #e8e7e7;
				text-align: left;
			}
			tr.headers th,
			tr.headers td
			{
				text-align: right;
			}
			
			
		</style>
	</head>
	<body onload="window.print();">
<?php
	foreach ( $route_detail_employees as $employee ) {
		if ( !( $employee instanceof \EE\Model\Labor\Route\Employee\Page ) ) {
			$job_weeks = $employee->getWeeksCalculated();
?>
		<div class="employee_detail">
			<img src="/assets/images/vendlogo.jpg" height="43" width="205" alt="logo" />
			<div class="table_wrapper">
				<table width="100%" border="0" class="employee_detail" cellspacing="0" cellpadding="2">
					<thead>
						<tr class="employee_info">
							<th colspan="4"> <?php
								echo $employee->getEmployeeNumber();
							?> <?php
								echo $employee->getLastName();
							?>, <?php
								echo $employee->getFirstName();
							?> </th>
						</tr>
						<tr class="headers">
							<td width="25%"></td>
<?php
					# Dates
					foreach ( $job_weeks as $week => $week_obj ) {
						if ( !( $week_obj instanceof \EE\Model\Labor\Route\Employee\WeekTotal ) ) {
?>
							<th width="25%"> <?php
								echo date( 'Y-m-d', $week_obj->getDate() );
							?> </th>
<?php
						}
					}
?>
							<th width="25%"> Totals </th>
						</tr>
					</thead>
					<tbody>
						<tr class="row">
							<th width="25%"> Route# </th>
<?php
					# Routes
					foreach ( $job_weeks as $week => $week_obj ) {
?>
							<td width="25%"> <?php
								$route_name = $week_obj->getRouteName();
								if ( $route_name ) {
									echo htmlentities( $route_name );
								}
								elseif ( $week_obj instanceof \EE\Model\Labor\Route\Employee\WeekTotal ) {
									?>&nbsp;<?php
								}
								else {
									?><img src="/assets/images/error.gif" height="16" width="20" alt="error" /><?php
								}
							?> </td>
<?php
					}
?>
						</tr>
						<tr class="row sales">
							<th width="25%"> Sales </th>
<?php
					# Sales
					foreach ( $job_weeks as $week => $week_obj ) {
?>
							<td width="25%"> <?php
								if ( $week_obj->{ \EE\Model\Labor\Route\Detail::FIELD_FIXED_AMOUNT } ) {
									if ( $week_obj instanceof \EE\Model\Labor\Route\Employee\WeekTotal ) {
										echo 'N/A';
									}
									else {
										echo 'Guaranteed Pay';
									}
								}
								else {
									echo number_format( $week_obj->{ \EE\Model\Labor\Route\Detail::FIELD_SHOW_SALES }, 2 );
								}
							?> </td>
<?php
					}
?>
						</tr>
						<tr class="row cashless">
							<th width="25%"> Cashless </th>
<?php
					# Cashless
					foreach ( $job_weeks as $week => $week_obj ) {
?>
							<td width="25%"> <?php
								if ( $week_obj->{ \EE\Model\Labor\Route\Detail::FIELD_FIXED_AMOUNT } ) {
									echo 'N/A';
								}
								else {
									echo number_format( $week_obj->{ \EE\Model\Labor\Route\Detail::FIELD_SHOW_NONCASH }, 2 );
								}
							?> </td>
<?php
					}
?>
						</tr>
						<tr class="row kiosk">
							<th width="25%"> Kiosk Sales </th>
<?php
					# Kiosk Sales
					foreach ( $job_weeks as $week => $week_obj ) {
?>
							<td width="25%"> <?php
								if ( $week_obj->{ \EE\Model\Labor\Route\Detail::FIELD_FIXED_AMOUNT } ) {
									echo 'N/A';
								}
								else {
									echo number_format( $week_obj->{ \EE\Model\Labor\Route\Detail::FIELD_SHOW_KIOSK }, 2 );
								}
							?> </td>
<?php
					}
?>
						</tr>
						<tr class="row freevend">
							<th width="25%"> Free Vends/Subsidies </th>
<?php
					# Free Vends/Subsidies
					foreach ( $job_weeks as $week => $week_obj ) {
?>
							<td width="25%"> <?php
								if ( $week_obj->{ \EE\Model\Labor\Route\Detail::FIELD_FIXED_AMOUNT } ) {
									echo 'N/A';
								}
								else {
									echo number_format( $week_obj->{ \EE\Model\Labor\Route\Detail::FIELD_SHOW_FREEVEND }, 2 );
								}
							?> </td>
<?php
					}
?>
						</tr>
						<tr class="row csv">
							<th width="25%"> CSV </th>
<?php
					# CSV
					foreach ( $job_weeks as $week => $week_obj ) {
?>
							<td width="25%"> <?php
								if ( $week_obj->{ \EE\Model\Labor\Route\Detail::FIELD_FIXED_AMOUNT } ) {
									echo 'N/A';
								}
								else {
									echo number_format( $week_obj->{ \EE\Model\Labor\Route\Detail::FIELD_SHOW_CSV }, 2 );
								}
							?> </td>
<?php
					}
?>
						</tr>
					</tbody>
					<tbody>
						<tr height="24">
							<td colspan="4"></td>
						</tr>
						<tr class="row holiday">
							<th width="25%"> Holiday Days </th>
<?php
					# Holiday Days
					foreach ( $job_weeks as $week => $week_obj ) {
?>
							<td width="25%"> <?php
								echo $week_obj->{ \EE\Model\Labor\Route\Detail::FIELD_SHOW_PAY_DAYS };
							?> </td>
<?php
					}
?>
						</tr>
						<tr class="row snp">
							<th width="25%"> Sick/Personal Days </th>
<?php
					# Sick/Personal Days
					foreach ( $job_weeks as $week => $week_obj ) {
?>
							<td width="25%"> <?php
								echo $week_obj->{ \EE\Model\Labor\Route\Detail::FIELD_SHOW_NOPAY_DAYS };
							?> </td>
<?php
					}
?>
						</tr>
						<tr class="row total_work_days">
							<th width="25%"> Pay Period Days </th>
<?php
					# Pay Period Days
					foreach ( $job_weeks as $week => $week_obj ) {
?>
							<td width="25%"> <?php
								echo $week_obj->{ \EE\Model\Labor\Route\Detail::FIELD_SHOW_TOTAL_WORK_DAYS };
							?> </td>
<?php
					}
?>
						</tr>
						<tr class="row pto">
							<th width="25%"> PTO Hours </th>
<?php
					# PTO Hours
					foreach ( $job_weeks as $week => $week_obj ) {
?>
							<td width="25%"> <?php
								echo $week_obj->{ \EE\Model\Labor\Route\Detail::FIELD_SHOW_PTO };
							?> </td>
<?php
					}
?>
						</tr>
						<tr class="row pto_5week_daily">
							<th width="25%"> Daily PTO </th>
<?php
					# Daily PTO
					foreach ( $job_weeks as $week => $week_obj ) {
?>
							<td width="25%"> <?php
								echo number_format( $week_obj->{ \EE\Model\Labor\Route\Detail::FIELD_SHOW_PTO_5WEEK_DAILY }, 2 );
							?> </td>
<?php
					}
?>
						</tr>
					</tbody>
					<tbody>
						<tr height="24">
							<td colspan="4"></td>
						</tr>
						<tr class="row base">
							<th width="25%"> Base </th>
<?php
					# Base
					foreach ( $job_weeks as $week => $week_obj ) {
?>
							<td width="25%"> <?php
								if (
									$week_obj->{ \EE\Model\Labor\Route\Detail::FIELD_BASE }
									|| $week_obj instanceof \EE\Model\Labor\Route\Employee\WeekTotal
								) {
									echo number_format( $week_obj->{ \EE\Model\Labor\Route\Detail::FIELD_BASE }, 2 );
								}
							?> </td>
<?php
					}
?>
						</tr>
						<tr class="row com">
							<th width="25%"> Commission </th>
<?php
					# Commission
					foreach ( $job_weeks as $week => $week_obj ) {
?>
							<td width="25%"> <?php
								if (
									$week_obj->{ \EE\Model\Labor\Route\Detail::FIELD_COMMISSION }
									|| $week_obj instanceof \EE\Model\Labor\Route\Employee\WeekTotal
								) {
									echo number_format( $week_obj->{ \EE\Model\Labor\Route\Detail::FIELD_COMMISSION }, 2 );
								}
							?> </td>
<?php
					}
?>
						</tr>
						<tr class="row weekend">
							<th width="25%"> Weekend Pay </th>
<?php
					# Weekend Pay
					foreach ( $job_weeks as $week => $week_obj ) {
?>
							<td width="25%"> <?php
								echo number_format( $week_obj->{ \EE\Model\Labor\Route\Detail::FIELD_SHOW_WEEKEND }, 2 );
							?> </td>
<?php
					}
?>
						</tr>
						<tr class="row pto_5week_total">
							<th width="25%"> PTO Total </th>
<?php
					# PTO Total
					foreach ( $job_weeks as $week => $week_obj ) {
?>
							<td width="25%"> <?php
								echo number_format( $week_obj->{ \EE\Model\Labor\Route\Detail::FIELD_SHOW_PTO_5WEEK_TOTAL }, 2 );
							?> </td>
<?php
					}
?>
						</tr>
						<tr class="row holiday_pay">
							<th width="25%"> Holiday Pay </th>
<?php
					# PTO Total
					foreach ( $job_weeks as $week => $week_obj ) {
?>
							<td width="25%"> <?php
								echo number_format( $week_obj->{ \EE\Model\Labor\Route\Detail::FIELD_SHOW_HOLIDAY_PAY }, 2 );
							?> </td>
<?php
					}
?>
						</tr>
						<tr class="row total">
							<th width="25%"> Total Pay </th>
<?php
					# Total Pay
					foreach ( $job_weeks as $week => $week_obj ) {
?>
							<td width="25%"> <?php
								$out = '%s';
								if ( $week_obj instanceof \EE\Model\Labor\Route\Employee\WeekTotal ) {
									$out = '<strong>%s</strong>';
								}
								echo sprintf( $out, number_format( $week_obj->{ \EE\Model\Labor\Route\Detail::FIELD_SHOW_TOTAL }, 2 ) );
							?> </td>
<?php
					}
?>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
<?php
		}
	}

	google_page_track();
?>
	</body>
</html>
<?php

