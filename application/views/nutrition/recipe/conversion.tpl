<?php
Treat_Controller_Abstract::addCss( '/assets/css/nutrition/conversion.css' );

if ( !function_exists( 'output_data' ) ) {
	function output_data( $val ) {
		$rv = $val;

		if ( true === $val ) {
			$rv = 'TRUE';
		}
		elseif ( false === $val ) {
			$rv = 'FALSE';
		}
		elseif ( null === $val ) {
			$rv = '&nbsp;&nbsp;&nbsp;';
		}
		elseif ( '' === $val ) {
			$rv = '&nbsp;&nbsp;&nbsp;';
		}
		elseif ( 0 === $val ) {
			$rv = '0';
		}
		elseif ( 0.0 === $val ) {
			$rv = '0.0';
		}

		return $rv;
	}
}

$page_business = Treat_Model_Business_Singleton::getSingleton();

?>

	<form method="post" action="">
		<div class="button">
			<p>
				<a href="/ta/businventor4.php?bid=<?php
					echo $page_business->getBusinessId();
				?>&amp;cid=<?php
					echo $page_business->getCompanyId();
				?>">Back to Inventory</a>
			</p>
		</div>
		<div class="button">
			<button type="submit">Make Changes</button>
			<button type="reset">Reset</button>
		</div>

<table cellspacing="0">
	<thead>
		<tr>
			<th></th>
			<th></th>
			<th>item_code</th>
			<th>item_name</th>

			<th>n_item_name</th>
			<th>r_recipeid</th>
			<th>menu_itemid</th>
			<th>menu_item_name</th>

		</tr>
	</thead>
	<tbody>
<?php
	$last_item_name = '';
	$last_n_item_name = '';
	$counter = 1;
	foreach ( $list as $item ) {
		if ( $item->isConverted() ) {
			continue;
		}
		$class = array();
		$cell_class = array();
		$item_info = '';
		$failed = false;
		if (
			$last_item_name == $item->item_name
			&& $last_n_item_name == $item->n_item_name
		) {
#			$class[] = 'duplicate';
		}
		elseif (
			$last_item_name == $item->item_name
		) {
#			$class[] = 'item_name';
		}
		elseif (
			$last_n_item_name == $item->n_item_name
		) {
#			$class[] = 'n_item_name';
		}
		
		if ( $item->isEasyConversion() ) {
			$class[] = 'convert_ok';
			$rowspan = 0;
			$failed = false;
			if ( $item->isConverted() ) {
				$cell_class[] = 'converted';
				$item_info = 'nothing to do';
			}
			else {
#				$cell_class[] = 'change_order';
#				$item_info = 'need to change order';
				if ( $item->saveConversion() ) {
					$item_info = 'converted automatically';
					$cell_class[] = 'change_order';
				}
				else {
					$item_info = 'failed to convert automatically';
					$cell_class[] = 'change_order_failed';
					$failed = true;
					$rowspan = 2;
				}
			}
		}
		else {
			$class[] = 'convert_not_ok';
			$cell_class[] = 'manual_convert';
			$item_info = 'can\'t convert automatically';
			$rowspan = 2;
		}
?>

		<tr class="<?php echo implode( ' ', $class ); ?>">
			<td<?php
					if ( $rowspan ) {
						echo sprintf( ' rowspan="%d"', $rowspan );
					}
			?> class="<?php
					echo 'counter';
				?>"><?php echo $counter++; ?></td>
			<td<?php
					if ( $rowspan ) {
						echo sprintf( ' rowspan="%d"', $rowspan );
					}
				?> class="<?php
					echo implode( ' ', $cell_class );
				?>"><?php
					echo $item_info;
			?></td>
			<td><?php echo output_data( $item->item_code ); ?></td>
			<td><a href="/ta/businventor3.php?bid=<?php
				echo $page_business->getBusinessId();
			?>&amp;cid=<?php
				echo $page_business->getCompanyId();
			?>&amp;view2=editinv&editid=<?php
				echo $item->inv_itemid;
			?>&amp;batch=#invitems" target=""><?php
				echo output_data( $item->item_name );
			?></a></td>

			<td><?php echo output_data( $item->n_item_name ); ?></td>
			<td><?php echo output_data( $item->r_recipeid ); ?></td>
			<td><?php echo output_data( $item->menu_itemid ); ?></td>
			<td><a href="/ta/businventor4.php?bid=<?php
				echo $page_business->getBusinessId();
			?>&amp;cid=<?php
				echo $page_business->getCompanyId();
			?>&amp;editid=<?php
				echo $item->menu_itemid;
			?>&amp;item_price=0" target="_blank"><?php
				echo output_data( $item->menu_item_name );
			?></a></td>
		</tr>

<?php
		if ( $item->isEasyConversion() && !$failed ) {
			if ( $item->isConverted() ) {
?>
<?php
			}
			else {
?>
<?php
			}
		}
		else {
?>

		<tr class="<?php echo implode( ' ', $class ); ?>">
<?php
			if ( $item->hasNutrition() ) {
?>

			<td colspan="3">
				<table>
					<tr>
						<th style="white-space: nowrap;">Old Available Size 1:</th>
						<td style="white-space: nowrap;"><?php
							echo $item->toStringInventorySizeLabel1();
						?></td>
						<th style="white-space: nowrap;">New Available Size 1:</th>
						<td style="white-space: nowrap;"><?php
							echo $item->toStringNutritionSizeLabel1();
						?></td>
					</tr>
					<tr>
						<th style="white-space: nowrap;">Old Available Size 2:</th>
						<td style="white-space: nowrap;"><?php
							echo $item->toStringInventorySizeLabel2();
						?></td>
						<th style="white-space: nowrap;">New Available Size 2:</th>
						<td style="white-space: nowrap;"><?php
							echo $item->toStringNutritionSizeLabel2();
						?></td>
					</tr>
					<tr>
						<th>Existing Size Set:</th>
						<td><?php
							echo $item->toStringRecipeSizeLabel();
						?></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<th>Change Size:</th>
						<td colspan="3">
							<input type="hidden" name="item[<?php
								echo $item->r_recipeid;
							?>][recipe_id]" value="<?php echo $item->r_recipeid; ?>" />
							<input type="hidden" name="item[<?php
								echo $item->r_recipeid;
							?>][inv_itemid]" value="<?php echo $item->inv_itemid; ?>" />
							<input type="hidden" name="item[<?php
								echo $item->r_recipeid;
							?>][item_code]" value="<?php echo $item->item_code; ?>" />
							
							<input type="text" name="item[<?php
								echo $item->r_recipeid;
							?>][n_rec_num]" value="" />
							<select name="item[<?php
								echo $item->r_recipeid;
							?>][n_rec_size]">
								<option value="">Select a size...</option>
<?php				if ( $item->showNutritionOption1() ) { ?>

								<option value="<?php
									echo $item->toStringNutritionOptionValue1();
								?>"><?php echo $item->toStringNutritionOptionLabel1(); ?></option>
<?php				} ?>
<?php				if ( $item->showNutritionOption2() ) { ?>

								<option value="<?php
									echo $item->toStringNutritionOptionValue2();
								?>"><?php echo $item->toStringNutritionOptionLabel2(); ?></option>
<?php				} ?>

							</select>
						</td>
					</tr>
				</table>
			</td>
			<td colspan="3">
				&nbsp;
			</td>
<?php
			}
			else {
?>

			<td colspan="6" style="text-align: center;">
<?php
				$errors = $item->getNutritionConversionErrors();
				if ( $errors ) {
?>
					<ul style="font-weight: bold; text-align: left;">
<?php				foreach ( $errors as $error ) { ?> 
						<li><?php echo $error; ?></li>
<?php				} ?>
					</ul>
<?php
				}
				else {
?>
					<strong>Nutrition information is not available for this item.</strong>
<?php
				}
?>
			</td>
<?php
			}
?>

		</tr>
<?php
		}
		$last_item_name = $item->item_name;
		$last_n_item_name = $item->n_item_name;
	}
?>

	</tbody>
</table>
		
		<div class="button">
			<button type="submit">Make Changes</button>
			<button type="reset">Reset</button>
		</div>
		<div class="button">
			<p>
				<a href="/ta/businventor4.php?bid=<?php
					echo $page_business->getBusinessId();
				?>&amp;cid=<?php
					echo $page_business->getCompanyId();
				?>">Back to Inventory</a>
			</p>
		</div>
	</form>


