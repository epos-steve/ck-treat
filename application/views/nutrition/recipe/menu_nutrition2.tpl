<?php
Treat_Controller_Abstract::addBodyClass( 'menu_nutrition2' );
Treat_Controller_Abstract::addCss( '/assets/css/nutrition/nutrition.css' );
Treat_Controller_Abstract::addJavascript( '/assets/js/nutrition/nutrition.js' );
Treat_Controller_Abstract::addCss( '/assets/css/nutrition/style.css' );
?>
<?php ob_start(); ?>
	<div class="menu_logo">
		<img src="/assets/images/nutrition/logo_205x43.jpg" width="205" height="43" alt="Treat America logo" />
	</div>
<?php $header[] = ob_get_clean(); ?>

	<div class="meal_details">
		<table cellspacing="0" cellpadding="0" class="meal_details" >
			<thead>
				<tr>
					<th colspan="3">
						Meal:
					</th>
				</tr>
			</thead>
			<tbody>
<?php
	$menu_items = $invoice_detail->getRecipeItems();
	$row_class = '';
	foreach ( $menu_items as $menu_item ) {
		if ( 'odd' == $row_class ) {
			$row_class = 'even';
		}
		else {
			$row_class = 'odd';
		}
		
?>
				<tr class="<?php echo $row_class; ?>">
					<td class="quantity" align="right" width="10%"><?php
						echo $menu_item->getQuantity();
					?></td>
					<td><?php
						echo $menu_item->getHearthHealthyImage(), ' ', $menu_item->item_name;
					?></td>
					<td class="units" align="right" width="20%"><?php
						echo $menu_item->unit;
					?></td>
				</tr>
<?php
	}
?>
			</tbody>
		</table>
	</div>
	<div>&nbsp;</div>

<?php
$item = $invoice_detail;
$nutrition_info_width = '';
$per_serving_col1 = '<strong>Amount Per Meal</strong>';
$show_printme_link = true;
include( dirname(__FILE__) . '/../../nutrition/nutrition_info.tpl' );
?>

	<div style="margin: 0 1%; text-align: center;">
		<div style="color: #736f6e; font-family: arial; font-size: 0.63em; text-align: left;">
				Nutritional information is approximate and for general 
				information purposes. Persons with restricted diets 
				should consult their physician, or registered dietician 
				for more specific nutritional guidelines. 
				(<?php echo date( 'Y-m-d' ); ?>)
		</div>
	</div>



<!--
- - >
<pre>
$invoice_detail = <?php var_dump( $invoice_detail ); ?>
</pre>
-->

