<?php
Treat_Controller_Abstract::addCss( '/assets/css/nutrition/conversion.css' );
?>


<table cellspacing="0">
	<thead>
		<tr>
			<th></th>
			<th></th>
			<th>inv_itemid</th>
			<th>item_code</th>
			<th>item_name</th>

			<th>n_item_name</th>
			<th>r_recipeid</th>
			<th>menu_item_name</th>

			<th>rec_num</th>
			<th>rec_size</th>
			<th>rec_num2</th>
			<th>rec_size2</th>
			<th>order_size</th>

			<th>n_rec_num</th>
			<th>n_rec_size</th>
			<th>n_rec_num2</th>
			<th>n_rec_size2</th>

			<th>r_rec_num</th>
			<th>r_rec_size</th>
			<th>r_rec_order</th>
		</tr>
	</thead>
	<tbody>
<?php
	$last_item_name = '';
	$last_n_item_name = '';
	$counter = 1;
	foreach ( $list as $item ) {
		$class = array();
		$cell_class = array();
		$item_info = '';
		if (
			$last_item_name == $item->item_name
			&& $last_n_item_name == $item->n_item_name
		) {
			$class[] = 'duplicate';
		}
		elseif (
			$last_item_name == $item->item_name
		) {
			$class[] = 'item_name';
		}
		elseif (
			$last_n_item_name == $item->n_item_name
		) {
			$class[] = 'n_item_name';
		}
		
		if ( $item->isEasyConversion() ) {
			if ( $item->isConverted() ) {
				$cell_class[] = 'converted';
				$item_info = 'nothing to do';
			}
			else {
				$cell_class[] = 'change_order';
				$item_info = 'need to change order';
			}
			$class[] = 'convert_ok';
		}
		else {
			$class[] = 'convert_not_ok';
			$cell_class[] = 'manual_convert';
			$item_info = 'can\'t convert automatically';
		}
?>

		<tr class="<?php echo implode( ' ', $class ); ?>">
			<td><?php echo $counter++; ?></td>
			<td class="<?php
					echo implode( ' ', $cell_class );
				?>"><?php
					echo $item_info;
			?></td>
			<td><?php echo $item->inv_itemid; ?></td>
			<td><?php echo $item->item_code; ?></td>
			<td><?php echo $item->item_name; ?></td>

			<td><?php echo $item->n_item_name; ?></td>
			<td><?php echo $item->r_recipeid; ?></td>
			<td><?php echo $item->menu_item_name; ?></td>

			<td><?php echo $item->i_rec_num; ?></td>
			<td><?php echo $item->i_rec_size_label; ?></td>
			<td><?php echo $item->i_rec_num2; ?></td>
			<td><?php echo $item->i_rec_size2_label; ?></td>
			<td><?php echo $item->order_size; ?></td>

			<td><?php echo $item->n_rec_num; ?></td>
			<td><?php echo $item->n_rec_size_label; ?></td>
			<td><?php echo $item->n_rec_num2; ?></td>
			<td><?php echo $item->n_rec_size2_label; ?></td>

			<td><?php echo $item->r_rec_num; ?></td>
			<td><?php echo $item->r_rec_size_label; ?></td>
			<td><?php echo $item->r_rec_order; ?></td>
		</tr>
<?php
		$last_item_name = $item->item_name;
		$last_n_item_name = $item->n_item_name;
	}
?>

	</tbody>
</table>


<pre><?php /* var_dump( $list ); */ ?></pre>
