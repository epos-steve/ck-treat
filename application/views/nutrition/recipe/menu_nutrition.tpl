<?php
Treat_Controller_Abstract::addBodyClass( 'menu_nutrition' );
Treat_Controller_Abstract::addCss( '/assets/css/nutrition/nutrition.css' );
Treat_Controller_Abstract::addJavascript( '/assets/js/nutrition/nutrition.js' );
Treat_Controller_Abstract::addCss( '/assets/css/nutrition/style.css' );
?>
<?php
$title = $menu_item->item_name;
if ( isset( $_GET['ck'] ) && $_GET['ck'] ) {
	Treat_Config::singleton()->set( 'application', 'page_title', 'Company Kitchen' );
}
else {
	ob_start();
?>
		<div class="menu_logo">
			<img src="/assets/images/nutrition/logo_205x43.jpg" width="205" height="43" alt="Treat America logo" />
		</div>
<?php
	$header[] = ob_get_clean();
}
?>
		<center>
			<table width="98%" class="product_name">
				<tr valign="top">
					<td>
						<?php echo $menu_item->getHearthHealthyImage(); ?> 
						<?php echo $menu_item->getHealthyLivingImage(); ?> 
						<b><?php echo $menu_item->item_name; ?></b>
					</td>
					<td align="right">
						<font color="gray"><?php
					if ( $show_recipe ) {
						echo 'Serving: ';
					}
					elseif ( $menu_item->price ) {
						echo money_format( '%.2n', $menu_item->getRetailPrice() ), '/';
					}
					echo $menu_item->unit;
						?></font>
					</td>
				</tr>
<?php	if ( $menu_item->description ) { ?>
				<tr>
					<td colspan="2">
						<font size="2"><?php echo $menu_item->description; ?></font>
					</td>
				</tr>
<?php	}?>
			</table>
			<div>&nbsp;</div>


<?php	if ( $show_recipe ) { ?>
			<table width="98%" style="border:2px solid black;" class="show_recipe">
				<tr>
					<td colspan=3>
						<i>
							<b>
								<font size="2" face="arial">
									Recipe serves <?php echo $menu_item->serving_size; ?> 
								</font>
							</b>
						</i>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<u>
							<font size="2" face="arial">
								Ingredients
							</font>
						</u>
					</td>
				</tr>
<?php
			$recipe_items = $menu_item->getRecipeItems();
			$nutrition = $menu_item->getNutritionValues();
			$nutrition = new EEF_ArrayObject( $nutrition );
			$counter = 1;
			foreach ( $recipe_items as $recipe_item ) {
?>
				<tr class="ingredient">
					<td>
						<?php echo $recipe_item->item_name; ?> 
						<?php
		if ( $recipe_item->hasChildren() ) {
			echo $recipe_item->getChildren( true );
		}
						?>
					</td>
					<td align="right" width="8%">
						<?php echo $recipe_item->getServingNumber(); ?>
					</td>
					<td width="25%">
						<?php echo $recipe_item->getServingSizeName(); ?>
					</td>
				</tr>
<?php
			}
?>
				<tr>
					<td colspan="3" class="instructions">
						<u>Instructions</u>
						<br>
						<?php echo str_replace( "\n", "<br/>\n", $menu_item->recipe ); ?>
					</td>
				</tr>
			</table>
			<div>&nbsp;</div>
<?php	} ?>

<?php
$item = $menu_item;
$nutrition_info_width = '98%';
$show_recipe_link = true;
if ( isset( $_GET['ck'] ) && $_GET['ck'] ) {
}
else {
	$per_serving_col3 = sprintf( '
			<div class="add_item_button">
				%s <a href="/menu/additem2.php?bid=%d&amp;item=%s&amp;goto=1"
					><img src="/assets/images/nutrition/add.gif" width="16" height="15" border="0" alt="Add Item"
				/></a>
			</div>
		'
		,$menu_item->unit
		,$business_id
		,$menu_item_id
	);
}

include( dirname(__FILE__) . '/../../nutrition/nutrition_info.tpl' );
?>

<?php
		if ( $menu_item->notes ) {
?>
			<table width="98%">
				<tr>
					<td>
						<font face="arial" size="2"><?php
				if ( isset( $shownotes ) && $shownotes ) {
					echo $shownotes, '<br/>*', $menu_item->notes;
				}
				else {
					echo sprintf( '
							<u>Note:</u>
							<br>
							<font color="red">*%s</font>
						'
						,$menu_item->notes
					);
				}
						?></font>
					</td>
				</tr>
			</table>
			<p>&nbsp;</p>
<?php
		}
		
		if ( isset( $incomplete ) && $incomplete > 0 ) {
?>
			<table width="98%">
				<tr>
					<td>
						<font size="2" face="arial">
							<?php
								echo $incomplete;
							?> ingredient(s) missing nutritional information<?php
								echo $showingr;
							?>
						</font>
					</td>
				</tr>
			</table>
			<div>&nbsp;</div>
<?php
		}
?>



			<table width="98%">
				<tr>
					<td>
						<font size="1" face="arial" color="gray">
<?php
		if ( isset( $_GET['ck'] ) && $_GET['ck'] ) {
?>
				Nutritional information is provided as a general guide only. 
				Variation in serving sizes, preparation techniques and 
				sources of supply, as well seasonal differences may affect 
				the nutrition values for each product. In addition, product 
				formulations change periodically. You should expect some 
				variation in the nutrient content of our products. 
				While we do our best to ensure accuracy, Company Kitchen, 
				its affiliates and its operators do not represent, guarantee 
				or warrant the accuracy and completeness of the nutritional 
				content provided within.  No information within shall be 
				construed as or understood to be medical advice or care. 
				If you are 
				on a restricted diet for the treatment of a disease or other 
				condition, you should consult your physician, or registered 
				dietician for more specific nutritional guidelines.
<?php
		}
		else {
?>
							Nutritional information is approximate and for general 
							information purposes. Persons with restricted diets 
							should consult their physician, or registered dietician 
							for more specific nutritional guidelines. 
							(<?php echo date( 'Y-m-d' ); ?>)
<?php
		}
?>
						</font>
					</td>
				</tr>
			</table>
		</center>

