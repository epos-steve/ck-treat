<?php
if ( !( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) ) {
	Treat_Controller_Abstract::addJavascript( '/assets/js/nutrition/print_window.js' );
}
?>

		<center>
			<table width=100%>
				<tr>
					<td colspan="5">
						<strong>
							<u><?php echo $menu_item->item_name; ?></u> 
							(<?php echo $menu_item->getNumberOfServings(); ?> Servings @ 
							<?php echo $menu_item->serving_size2; ?>/<?php echo $menu_item->unit; ?>)
						</strong>
						<p><br/></p>
					</td>
				</tr>
				<tr>
					<td colspan="5">
						<i><u>Ingredients</u>:</i>
					</td>
				</tr>
<?php
	$recipe_items = $menu_item->getRecipeItems();
	$nutrition = $menu_item->getNutritionValues();
	$nutrition = new Treat_ArrayObject( $nutrition );
	$counter = 1;
	foreach ( $recipe_items as $recipe_item ) {
?>
				<tr class="ingredient">
					<td width="5%"><?php echo $counter++; ?>.</td>
					<td width="20%"><?php echo $recipe_item->getShowNewSize(); ?></td>
					<td>
						<?php echo $recipe_item->item_name; ?> 
						<sup><font size="1"><?php
							echo $recipe_item->item_code, $recipe_item->getShowNutr();
						?></font></sup>
						<?php
		if ( $recipe_item->hasChildren() ) {
			echo $recipe_item->getChildren( true );
		}
						?>
					</td>
					<td></td>
					<td class="cost"><?php
		if ( $show_cost ) {
			echo money_format( '%.2n', $recipe_item->getCost() );
		}
					?></td>
				</tr>
<?php
	}
	
	if ( $show_cost ) {
?>
				<tr>
					<td colspan="5" align="right">Total: <?php echo money_format( '%.2n', $menu_item->getCost() ); ?></td>
				</tr>
<?php
	}
?>
				<tr>
					<td colspan="5">
						<p><br/></p>
						<i><u>Instructions</u>:</i>
					</td>
				</tr>
				
				<tr>
					<td colspan="5"><?php
						echo str_replace( "\n", "<br/>\n", $menu_item->recipe );
					?></td>
				</tr>
				<tr>
					<td colspan="5">
						<p><br/></p>
						<i><u>Nutritional Information per Serving</u>:</i>
					</td>
				</tr>
				
				<tr>
					<td colspan="5">
						<p></p>

<?php
$item = $menu_item;
include( dirname(__FILE__) . '/../../nutrition/nutrition_info.tpl' );
?>

					</td>
				</tr>
			</table>
		</center>


