<?php
// $item should be child of Treat_Model_Nutrition_RecipeItemContainer
Treat_Controller_Abstract::addCss( '/assets/css/nutrition/nutrition.css' );
Treat_Controller_Abstract::addJavascript( '/assets/js/nutrition/nutrition.js' );

if ( !isset( $nutrition_info_width ) ) {
	$nutrition_info_width = 'width: 50%; ';
}
else {
	if ( false === stripos( $nutrition_info_width, 'width:' ) ) {
		$nutrition_info_width = sprintf( 'width: %s; ', $nutrition_info_width );
	}
}

#if ( !isset( $nutrition_info_align ) ) {
#	$nutrition_info_align = 'left';
#}
if ( !isset( $nutrition_info_align ) ) {
	$nutrition_info_align = '';
}
switch ( $nutrition_info_align ) {
	case 'center':
		$nutrition_info_align = 'margin: 0 auto;';
		break;
	case 'left':
		$nutrition_info_align = 'margin: 0;';
		break;
	default:
		$nutrition_info_align = '';
		break;
}

if ( !isset( $per_serving_col1 ) ) {
	$per_serving_col1 = '<strong>Amount Per Serving</strong>';
}

if ( !isset( $show_recipe_link ) ) {
	$show_recipe_link = false;
}

if ( !isset( $show_recipe ) ) {
	$show_recipe = false;
}

$querystr = array();
if ( isset( $mic_id ) && $mic_id ) {
	$querystr['mic'] = $mic_id;
}
if ( isset( $min_id ) && $min_id ) {
	$querystr['min'] = $min_id;
}
if ( isset( $menu_item_id ) && $menu_item_id ) {
	$querystr['mid'] = $menu_item_id;
}
if ( isset( $_GET['ck'] ) && $_GET['ck'] ) {
	$querystr['ck'] = $_GET['ck'];
}
?>

<div style="<?php echo $nutrition_info_width, $nutrition_info_align; ?>" class="nutrition_info">
						<table cellspacing="0" cellpadding="0" width="100%" class="nutrition_info_table">
							<tr>
								<td><?php
										echo $per_serving_col1;
								?></td>
								<td><?php
									if ( isset( $per_serving_col2 ) ) {
										echo $per_serving_col2;
									}
								?></td>
								<td><?php
									if ( isset( $per_serving_col3 ) ) {
										echo $per_serving_col3;
									}
								?></td>
							</tr>
							<tr bgcolor="black" height="1">
								<td colspan=5></td>
							</tr>
							<tr>
								<td>
									<?php echo $item->getLowCaloriesImage(); ?>
									<strong>Calories: <?php
										echo round( $item->getNutritionValue( 'calories' ), 1 );
									?></strong>
								</td>
								<td colspan="2" align="right">
									Calories from Fat: <?php echo round( $item->getNutritionValue( 'cal_from_fat' ), 1 ); ?>
								</td>
							</tr>
							<tr bgcolor="black" height="5">
								<td colspan="5"></td>
							</tr>
							<tr>
								<td>
									<?php echo $item->getLowCaloriesFromFatImage(); ?>
									<strong>Total Fat:</strong>
								</td>
								<td align="right"><?php
									echo round( $item->getNutritionValue( 'total_fat' ), 1 );
								?> <?php
									echo strtoupper( Treat_Model_Nutrition_TypesObject::getUnitsByKey( 'total_fat' ) );
								?></td>
								<td></td>
							</tr>
							<tr>
								<td>&nbsp;&nbsp;&nbsp;Saturated Fat:</td>
								<td align="right"><?php
									echo round( $item->getNutritionValue( 'sat_fat' ), 1 );
								?> <?php
									echo strtoupper( Treat_Model_Nutrition_TypesObject::getUnitsByKey( 'sat_fat' ) );
								?></td>
								<td></td>
							</tr>
							<tr bgcolor="black" height="1">
								<td colspan="5"></td>
							</tr>
							<tr>
								<td>
									<?php echo $item->getLowCholesterolImage(); ?>
									<strong>Cholesterol:</strong>
								</td>
								<td align="right"><?php
									echo round( $item->getNutritionValue( 'cholesterol' ), 1 );
								?> <?php
									echo strtoupper( Treat_Model_Nutrition_TypesObject::getUnitsByKey( 'cholesterol' ) );
								?></td>
								<td></td>
							</tr>
							<tr bgcolor="black" height="1">
								<td colspan="5"></td>
							</tr>
							<tr>
								<td>
									<?php echo $item->getLowSodiumImage(); ?>
									<strong>Sodium:</strong>
								</td>
								<td align="right"><?php
									echo round( $item->getNutritionValue( 'sodium' ), 1 );
								?> <?php
									echo strtoupper( Treat_Model_Nutrition_TypesObject::getUnitsByKey( 'sodium' ) );
								?></td>
								<td width="30%"></td>
							</tr>
							<tr bgcolor="black" height="1">
								<td colspan="5"></td>
							</tr>
							<tr>
								<td><strong>Potassium:</strong></td>
								<td align="right"><?php
									echo round( $item->getNutritionValue( 'potassium' ), 1 );
								?> <?php
									echo strtoupper( Treat_Model_Nutrition_TypesObject::getUnitsByKey( 'potassium' ) );
								?></td>
								<td></td>
							</tr>
							<tr bgcolor="black" height="1">
								<td colspan="5"></td>
							</tr>
							<tr>
								<td>
									<?php echo $item->getLowTotalCarbsImage(); ?>
									<strong>Total Carbohydrates:</strong>
								</td>
								<td align="right"><?php
									echo round( $item->getNutritionValue( 'complex_cabs' ), 1 );
								?> <?php
									echo strtoupper( Treat_Model_Nutrition_TypesObject::getUnitsByKey( 'complex_cabs' ) );
								?></td>
								<td></td>
							</tr>
							<tr>
								<td>&nbsp;&nbsp;&nbsp;Dietary Fiber:</td>
								<td align="right"><?php
									echo round( $item->getNutritionValue( 'fiber' ), 1 );
								?> <?php
									echo strtoupper( Treat_Model_Nutrition_TypesObject::getUnitsByKey( 'fiber' ) );
								?></td>
								<td></td>
							</tr>
							<tr>
								<td>&nbsp;&nbsp;&nbsp;Sugars:</td>
								<td align="right"><?php
									echo round( $item->getNutritionValue( 'sugar' ), 1 );
								?> <?php
									echo strtoupper( Treat_Model_Nutrition_TypesObject::getUnitsByKey( 'sugar' ) );
								?></td>
								<td></td>
							</tr>
							<tr bgcolor="black" height="1">
								<td colspan="5"></td>
							</tr>
							<tr>
								<td>
									<?php echo $item->getHighProteinImage(); ?>
									<strong>Protein:</strong>
								</td>
								<td align="right"><?php
									echo round( $item->getNutritionValue( 'protein' ), 1 );
								?> <?php
									echo strtoupper( Treat_Model_Nutrition_TypesObject::getUnitsByKey( 'protein' ) );
								?></td>
								<td></td>
							</tr>
							<tr bgcolor="black" height="5">
								<td colspan="5"></td>
							</tr>
							<tr>
								<td colspan="3">
									<font size="1">
										*Percent Daily Values are based on a 2,000 calorie diet.  
										Your daily values may be higher or lower depending on your 
										calorie needs.
									</font>
								</td>
							</tr>
						</table>
						
						<div class="treat_score_wrapper">
		<table cellspacing="0" cellpadding="0" width="100%" class="treat_score_table">
			<tr>
				<td class="treat_score">
<?php
if ( isset( $_GET['ck'] ) && $_GET['ck'] ) {
?>
							CK Score: 
<?php
}
else {
?>
							<img src="/assets/images/nutrition/ww.jpg" height="16" width="16" alt="Treat Score logo" />
							Treat Score: 
<?php
}
?>
							<span class="treat_score_value">
								<?php echo round( $item->getTreatScore(), 0 ); ?>
							</span>
				</td>
				<td align="right" class="treat_score_extra">
<?php
		// must either set $show_printme_link = true
		// or must set $show_recipe_link = true & $show_recipe = true
		// to get the Print link to show up
		if (
			( isset( $show_printme_link ) && $show_printme_link )
			|| ( isset( $show_recipe_link ) && $show_recipe_link && !$item->recipe_active_cust && !( $min_id || $mic_id ) )
		) {
			if (
				( isset( $show_printme_link ) && $show_printme_link )
				|| ( isset( $show_recipe ) && $show_recipe )
			) {
?>
					<span class="printme">Print</span>
<?php
			}
			else {
?>
					<a href="nutrition.php?<?php
						$querystr['showrec'] = 1;
						echo http_build_query( $querystr );
					?>"
						class="show_recipe"
					>Show Recipe</a>
<?php
			}
		}
?>
				</td>
		</table>
						</div>
</div>


