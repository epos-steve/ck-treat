<?php
	
	$company = Treat_Model_Company_Singleton::getSingleton();
	
	$companyLogo = $company->logo;
	if ( !$companyLogo ) {
		$companyLogo = 'weblogo.gif';
	}
	
?>
<!DOCTYPE html
	PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title><?php
			if ( isset( $title ) && $title ) {
				echo $title, ' :: ', Treat_Config::singleton()->getValue( 'application', 'page_title' );
			}
			else {
				echo Treat_Config::singleton()->getValue( 'application', 'page_title' );
			}
		?></title>
		<meta name="verify-v1" content="PDmo8LnHK6P8AitDtvqWv2k4/4iF5RKXVji1YAEExSk=" />
<?php
	Treat_Controller_Abstract::outputArrayVariable( Treat_Controller_Abstract::getMetas() );
?>
		<link type="image/vnd.microsoft.icon" rel="shortcut icon" href="/favicon.ico" />
		<link type="image/vnd.microsoft.icon" rel="icon"          href="/favicon.ico" />
<?php
	Treat_Controller_Abstract::outputArrayVariable( Treat_Controller_Abstract::getCss() );
	Treat_Controller_Abstract::outputArrayVariable( Treat_Controller_Abstract::getJavascriptsAtTop() );
?>
	<script type="text/javascript">$(document).ready(function (){$('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' });});</script>
	</head>
	<body class="<?php echo Treat_Controller_Abstract::getBodyClass(); ?>">
		<div id="page_wrapper" class="page_wrapper company_view">
			<div id="header">
				<a href="/ta/businesstrack.php"
					><img src="/assets/images/logo.jpg" height="43" width="205" alt="logo"
				/></a>
			</div>

			<div id="header">
<?php Treat_Controller_Abstract::outputArrayVariable( $header, false, "\n\t\t\t\t", "\t\t\t\t", "\n" ); ?>
			</div>
<?php
if ( !IN_PRODUCTION ) {
	echo $_SESSION->getFlashDebug( "\t\t\t<pre class=\"flash_debug\">%s</pre>\n" );
}
echo $_SESSION->getFlashError(   "\t\t\t<div class=\"flash_error\">%s</div>\n"   );
echo $_SESSION->getFlashMessage( "\t\t\t<div class=\"flash_message\">%s</div>\n" );
?>

<div class="ui-tabs ui-widget-content ui-corner-all">
	<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
		<li class="ui-state-default ui-corner-top<?= $uri == 'customerSearch' ? ' ui-tabs-selected ui-state-active' : '' ?>">
			<a href="/ta/customerutilities/customerSearch?cid=<?php echo $company->getCompanyId() ?>">Customers</a>
		</li>
		<li class="ui-state-default ui-corner-top<?= $uri == 'adjustments' ? ' ui-tabs-selected ui-state-active' : '' ?>">
			<a href="/ta/customerutilities/adjustments?cid=<?php echo $company->getCompanyId() ?>">Adjustments</a>
		</li>
		<li class="ui-state-default ui-corner-top<?= $uri == 'closeAccounts' ? ' ui-tabs-selected ui-state-active' : '' ?>">
			<a href="/ta/customerutilities/closeAccounts?cid=<?php echo $company->getCompanyId() ?>">Close Accounts</a>
		</li>
		<li class="ui-state-default ui-corner-top<?= $uri == 'customerExport' ? ' ui-tabs-selected ui-state-active' : '' ?>">
			<a href="/ta/customerutilities/customerExport?cid=<?php echo $company->getCompanyId() ?>">Export</a>
		</li>
		<li class="ui-state-default ui-corner-top<?= $uri == 'customerSettings' ? ' ui-tabs-selected ui-state-active' : '' ?>">
			<a href="/ta/customerutilities/customerSettings?cid=<?php echo $company->getCompanyId() ?>">Settings</a>
		</li>
		<li class="ui-state-default ui-corner-top<?= $uri == 'businessGroups' ? ' ui-tabs-selected ui-state-active' : '' ?>">
			<a href="/ta/customerutilities/businessGroups?cid=<?php echo $company->getCompanyId() ?>">Business Groups</a>
		</li>
		<li class="ui-state-default ui-corner-top<?= $uri == 'businessGroups' ? ' ui-tabs-selected ui-state-active' : '' ?>">
			<a href="/ta/customerutilities/accountsOutOfBalance?cid=<?php echo $company->getCompanyId() ?>">Accounts Out of Balance</a>
		</li>
	</ul>

			<div id="content">
				<?php echo $content; ?>
			</div>	
			
</div>

			<div id="testdiv1" style="position: absolute; visibility: hidden; background-color: white;"></div>

			<form action="/ta/businesstrack.php" method="get">
				<p style="text-align: center;">
					<input type="submit" value="Return to Main Page" />
				</p>
			</form>
		</div>
<?php
if ( !isset( $enable_google_page_track ) || $enable_google_page_track ) {
	google_page_track();
}
?>
	</body>
</html>
