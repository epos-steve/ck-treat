<?php

	$company = Treat_Model_Company_Singleton::getSingleton( $companyId );

	$companyLogo = $company->logo;
	if( !$companyLogo )
		$companyLogo = 'weblogo.gif';

?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title><?php
			if ( isset( $title ) && $title ) {
				echo $title, ' :: ', Treat_Config::singleton()->getValue( 'application', 'page_title' );
			}
			else {
				echo Treat_Config::singleton()->getValue( 'application', 'page_title' );
			}
		?></title>
		<base href="<?=$base_href?>" />
		<meta name="verify-v1" content="PDmo8LnHK6P8AitDtvqWv2k4/4iF5RKXVji1YAEExSk=" />
<?php
	Treat_Controller_Abstract::outputArrayVariable( Treat_Controller_Abstract::getMetas() );
?>
		<link type="image/vnd.microsoft.icon" rel="shortcut icon" href="/favicon.ico" />
		<link type="image/vnd.microsoft.icon" rel="icon"          href="/favicon.ico" />
<?php
/*
		<link rel="stylesheet" type="text/css" href="/assets/js/preload/preLoadingMessage.css" />
		<link rel="stylesheet" type="text/css" href="/assets/css/ta/_dir/generic.css" />
		<script type="text/javascript" src="/assets/js/jquery-1.4.3.min.js"></script>
		<script type="text/javascript" src="/assets/js/preload/preLoadingMessage.js"></script>
		<script type="text/javascript" src="/assets/js/ta/buscontrol2-functions.js"></script>
		<script type="text/javascript" src="/assets/js/dynamicdrive-disableform.js"></script>
		<script type="text/javascript" src="/assets/js/calendarPopup.js"></script>
		<link rel="stylesheet" type="text/css" href="/assets/css/calendarPopup.css" />
*/
	Treat_Controller_Abstract::outputArrayVariable( Treat_Controller_Abstract::getCss() );
	Treat_Controller_Abstract::outputArrayVariable( Treat_Controller_Abstract::getJavascriptsAtTop() );
?>
	</head>
	<body class="<?php echo Treat_Controller_Abstract::getBodyClass(); ?>">
<div align="center">
	<table cellspacing="0" cellpadding="0" border="0" width="90%">
			<tr>
				<td colspan="2">
					<a href="/ta/businesstrack.php"
						><img src="/assets/images/logo.jpg" border="0" height="43" width="205" alt="logo"
					/></a>
				</td>
			</tr>
<?php
#	$company = Treat_Model_Company::getCompanyById( $company_id );
	$query = sprintf( "
			SELECT *
			FROM company
			WHERE companyid = '%s'
		"
		,$GLOBALS['companyid']
	);
	$result = Treat_DB_ProxyOld::query( $query );

	$companyname = @mysql_result( $result, 0, 'companyname' );
	$week_end = @mysql_result( $result, 0, 'week_end' );
	$week_start = @mysql_result( $result, 0, 'week_start' );

	$query = sprintf( "
			SELECT *
			FROM security
			WHERE securityid = '%s'
		"
		,$GLOBALS['mysecurity']
	);
	$result = Treat_DB_ProxyOld::query( $query );

	$sec_bus_sales = @mysql_result( $result, 0, 'bus_sales' );
	$sec_bus_invoice = @mysql_result( $result, 0, 'bus_invoice' );
	$sec_bus_order = @mysql_result( $result, 0, 'bus_order' );
	$sec_bus_payable = @mysql_result( $result, 0, 'bus_payable' );
	$sec_bus_inventor1 = @mysql_result( $result, 0, 'bus_inventor1' );
	$sec_bus_control = @mysql_result( $result, 0, 'bus_control' );
	$sec_bus_menu = @mysql_result( $result, 0, 'bus_menu' );
	$sec_bus_order_setup = @mysql_result( $result, 0, 'bus_order_setup' );
	$sec_bus_request = @mysql_result( $result, 0, 'bus_request' );
	$sec_route_collection = @mysql_result( $result, 0, 'route_collection' );
	$sec_route_labor = @mysql_result( $result, 0, 'route_labor' );
	$sec_route_detail = @mysql_result( $result, 0, 'route_detail' );
	$sec_bus_labor = @mysql_result( $result, 0, 'bus_labor' );
	$sec_bus_budget = @mysql_result( $result, 0, 'bus_budget' );


	if ( $GLOBALS['operate'] == 5 ) {
		$paydays = 10;
	}
	elseif ( $GLOBALS['operate'] == 6 ) {
		$paydays == 12;
	}
	elseif ( $GLOBALS['operate'] == 7 ) {
		$paydays == 14;
	}

?>


			<tr bgcolor="#ccccff" style="background-color:#ccccff">
				<td width="1%">
					<img src="/assets/images/weblogo.jpg" width="80" height="75" alt="Treat America logo" />
				</td>
				<td>
					<div style="font-size: 1.1em; font-weight: bold;"><?php echo $GLOBALS['businessname'], ' #', $GLOBALS['unit_num']; ?></div>
					<div><?php echo $GLOBALS['street']; ?></div>
					<div><?php echo $GLOBALS['city'], ', ', $GLOBALS['state'], ' ', $GLOBALS['zip']; ?></div>
					<div><?php echo $GLOBALS['phone']; ?></div>
				</td>
				<td align="right" valign="top">
					<br/>
					<form action="/ta/editbus.php" method="post">
						<div>
							<input type="hidden" name="username" value="<?php echo $GLOBALS['user']; ?>" />
							<input type="hidden" name="password" value="<?php echo $GLOBALS['pass']; ?>" />
							<input type="hidden" name="businessid" value="<?php echo $GLOBALS['businessid']; ?>" />
							<input type="hidden" name="companyid" value="<?php echo $GLOBALS['companyid']; ?>" />
							<input type="submit" value=" Store Setup " />
						</div>
					</form>
				</td>
			</tr>
			<tr>
				<td colspan="3" class="header_nav">
<?php
				echo $_SESSION->getUser()->getBusinessMenu( $GLOBALS['businessid'], $GLOBALS['companyid'], 'buscontrol' );
?>
				</td>
			</tr>
	</table>
		<!--<div id="page_wrapper" class="page_wrapper company_view">
			<div id="header">
				<a href="/ta/businesstrack.php"
					><img src="/assets/images/logo.jpg" height="43" width="205" alt="logo"
				/></a>
			</div>

			<div id="header">
<?php Treat_Controller_Abstract::outputArrayVariable( $header, false, "\n\t\t\t\t", "\t\t\t\t", "\n" ); ?>
			</div>
  -->
<?php
if ( !IN_PRODUCTION ) {
	echo $_SESSION->getFlashDebug( "\t\t\t<pre class=\"flash_debug\">%s</pre>\n" );
}
echo $_SESSION->getFlashError(   "\t\t\t<div class=\"flash_error\">%s</div>\n"   );
echo $_SESSION->getFlashMessage( "\t\t\t<div class=\"flash_message\">%s</div>\n" );
?>

	<div class="ui-tabs  ui-corner-all">

			<div id="content">
				<p />
				<?php echo $content; ?>
			</div>

	</div>

			<div id="testdiv1" style="position: absolute; visibility: hidden; background-color: white;"></div>

			<form action="/ta/businesstrack.php" method="get">
				<p style="text-align: center;">
					<input type="submit" value="Return to Main Page" />
				</p>
			</form>
		</div>
<?php
if ( !isset( $enable_google_page_track ) || $enable_google_page_track ) {
	google_page_track();
}
?>
	</div>
	</body>
</html>
