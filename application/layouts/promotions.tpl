<?php
	
	$company = \EE\Model\Company\Singleton::getSingleton( $companyId );
	
	$companyLogo = $company->logo;
	if( !$companyLogo )
		$companyLogo = 'weblogo.gif';
	
?>
<!DOCTYPE html
	PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title><?php
			if ( isset( $title ) && $title ) {
				echo $title, ' :: ', \EE\Config::singleton()->getValue( 'application', 'page_title' );
			}
			else {
				echo \EE\Config::singleton()->getValue( 'application', 'page_title' );
			}
		?></title>
		<meta name="verify-v1" content="PDmo8LnHK6P8AitDtvqWv2k4/4iF5RKXVji1YAEExSk=" />
<?php
	\EE\Controller\Base::outputArrayVariable( \EE\Controller\Base::getMetas() );
?>
		<link type="image/vnd.microsoft.icon" rel="shortcut icon" href="/favicon.ico" />
		<link type="image/vnd.microsoft.icon" rel="icon"          href="/favicon.ico" />
<?php
/*
		<link rel="stylesheet" type="text/css" href="/assets/js/preload/preLoadingMessage.css" />
		<link rel="stylesheet" type="text/css" href="/assets/css/ta/_dir/generic.css" />
		<script type="text/javascript" src="/assets/js/jquery-1.4.3.min.js"></script>
		<script type="text/javascript" src="/assets/js/preload/preLoadingMessage.js"></script>
		<script type="text/javascript" src="/assets/js/ta/buscontrol2-functions.js"></script>
		<script type="text/javascript" src="/assets/js/dynamicdrive-disableform.js"></script>
		<script type="text/javascript" src="/assets/js/calendarPopup.js"></script>
		<link rel="stylesheet" type="text/css" href="/assets/css/calendarPopup.css" />
*/
	\EE\Controller\Base::outputArrayVariable( \EE\Controller\Base::getCss() );
	\EE\Controller\Base::outputArrayVariable( \EE\Controller\Base::getJavascriptsAtTop() );
?>
	</head>
	<body class="<?php echo \EE\Controller\Base::getBodyClass(); ?>">
		<div id="page_wrapper" class="page_wrapper company_view">
			<div id="header">
				<a href="/ta/businesstrack.php"
					><img src="/assets/images/logo.jpg" height="43" width="205" alt="logo"
				/></a>
			</div>

			<div id="header">
<?php \EE\Controller\Base::outputArrayVariable( $header, false, "\n\t\t\t\t", "\t\t\t\t", "\n" ); ?>
			</div>
<?php
if ( !IN_PRODUCTION ) {
	echo $_SESSION->getFlashDebug( "\t\t\t<pre class=\"flash_debug\">%s</pre>\n" );
}
echo $_SESSION->getFlashError(   "\t\t\t<div class=\"flash_error\">%s</div>\n"   );
echo $_SESSION->getFlashMessage( "\t\t\t<div class=\"flash_message\">%s</div>\n" );
?>

<div class="ui-tabs ui-widget-content ui-corner-all">
	<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
		<li class="ui-state-default ui-corner-top<?= $uri == 'ta/mastermenu' ? ' ui-tabs-selected ui-state-active' : '' ?>">
			<a href="/ta/mastermenu">Master Menu</a>
		</li>
		<li class="ui-state-default ui-corner-top<?= $uri == 'ta/categories' ? ' ui-tabs-selected ui-state-active' : '' ?>">
			<a href="/ta/categories">Categories</a>
		</li>
		<li class="ui-state-default ui-corner-top<?= $uri == 'ta/promomaster' ? ' ui-tabs-selected ui-state-active' : '' ?>">
			<a href="/ta/promomaster">Combo Groups</a>
		</li>
		<li class="ui-state-default ui-corner-top<?= $uri == 'ta/promotions' ? ' ui-tabs-selected ui-state-active' : '' ?>">
			<a href="/ta/promotions">Promotions</a>
		</li>
		<li class="ui-state-default ui-corner-top<?= $uri == 'ta/promosettings' ? ' ui-tabs-selected ui-state-active' : '' ?>">
			<a href="/ta/promosettings">Settings</a>
		</li>
		<?php 
		/*
		<li class="ui-state-default ui-corner-top<?= $uri == 'ta/masterreports' ? ' ui-tabs-selected ui-state-active' : '' ?>">
			<a href="/ta/masterreports">Reports</a>
		</li>
		*/
		?>
		<li class="ui-state-default ui-corner-top<?= $uri == 'ta/functionPanels' ? ' ui-tabs-selected ui-state-active' : '' ?>">
			<a href="/ta/functionPanels">Function Panels</a>
		</li>
	</ul>

			<div id="content">
				<?php echo $content; ?>
			</div>	
			
</div>

			<div id="testdiv1" style="position: absolute; visibility: hidden; background-color: white;"></div>

			<form action="/ta/businesstrack.php" method="get">
				<p style="text-align: center;">
					<input type="submit" value="Return to Main Page" />
				</p>
			</form>
		</div>
<?php
if ( !isset( $enable_google_page_track ) || $enable_google_page_track ) {
	google_page_track();
}
?>
	</body>
</html>
