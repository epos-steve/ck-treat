<?php
	$viewmonth  = getPostOrGetVariable( 'viewmonth' );
	$viewmonth  = Treat_GeneralLedger_Date::getBeginningOfMonth( $viewmonth, true );
	$prevperiod = getPostOrGetVariable( 'prevperiod' );
	$nextperiod = getPostOrGetVariable( 'nextperiod' );
	$audit      = getPostOrGetVariable( 'audit' );

ob_start();
?>
		<table cellspacing="0" cellpadding="0" border="0">
			<tr>
				<td colspan="2">
					<a href="/ta/businesstrack.php"
						><img src="/assets/images/logo.jpg" border="0" height="43" width="205" alt="logo"
					/></a>
				</td>
			</tr>
<?php
#	$company = Treat_Model_Company::getCompanyById( $company_id );
	$query = sprintf( "
			SELECT *
			FROM company
			WHERE companyid = '%s'
		"
		,$GLOBALS['companyid']
	);
	$result = Treat_DB_ProxyOld::query( $query );

	$companyname = @mysql_result( $result, 0, 'companyname' );
	$week_end = @mysql_result( $result, 0, 'week_end' );
	$week_start = @mysql_result( $result, 0, 'week_start' );

	$query = sprintf( "
			SELECT *
			FROM security
			WHERE securityid = '%s'
		"
		,$GLOBALS['mysecurity']
	);
	$result = Treat_DB_ProxyOld::query( $query );

	$sec_bus_sales = @mysql_result( $result, 0, 'bus_sales' );
	$sec_bus_invoice = @mysql_result( $result, 0, 'bus_invoice' );
	$sec_bus_order = @mysql_result( $result, 0, 'bus_order' );
	$sec_bus_payable = @mysql_result( $result, 0, 'bus_payable' );
	$sec_bus_inventor1 = @mysql_result( $result, 0, 'bus_inventor1' );
	$sec_bus_control = @mysql_result( $result, 0, 'bus_control' );
	$sec_bus_menu = @mysql_result( $result, 0, 'bus_menu' );
	$sec_bus_order_setup = @mysql_result( $result, 0, 'bus_order_setup' );
	$sec_bus_request = @mysql_result( $result, 0, 'bus_request' );
	$sec_route_collection = @mysql_result( $result, 0, 'route_collection' );
	$sec_route_labor = @mysql_result( $result, 0, 'route_labor' );
	$sec_route_detail = @mysql_result( $result, 0, 'route_detail' );
	$sec_bus_labor = @mysql_result( $result, 0, 'bus_labor' );
	$sec_bus_budget = @mysql_result( $result, 0, 'bus_budget' );


	if ( $GLOBALS['operate'] == 5 ) {
		$paydays = 10;
	}
	elseif ( $GLOBALS['operate'] == 6 ) {
		$paydays == 12;
	}
	elseif ( $GLOBALS['operate'] == 7 ) {
		$paydays == 14;
	}

?>


			<tr bgcolor="#ccccff">
				<td width="1%">
					<img src="/assets/images/weblogo.jpg" width="80" height="75" alt="Treat America logo" />
				</td>
				<td>
					<div style="font-size: 1.1em; font-weight: bold;"><?php echo $GLOBALS['businessname'], ' #', $GLOBALS['unit_num']; ?></div>
					<div><?php echo $GLOBALS['street']; ?></div>
					<div><?php echo $GLOBALS['city'], ', ', $GLOBALS['state'], ' ', $GLOBALS['zip']; ?></div>
					<div><?php echo $GLOBALS['phone']; ?></div>
				</td>
				<td align="right" valign="top">
					<br/>
					<form action="/ta/editbus.php" method="post">
						<div>
							<input type="hidden" name="username" value="<?php echo $GLOBALS['user']; ?>" />
							<input type="hidden" name="password" value="<?php echo $GLOBALS['pass']; ?>" />
							<input type="hidden" name="businessid" value="<?php echo $GLOBALS['businessid']; ?>" />
							<input type="hidden" name="companyid" value="<?php echo $GLOBALS['companyid']; ?>" />
							<input type="submit" value=" Store Setup " />
						</div>
					</form>
				</td>
			</tr>
			<tr>
				<td colspan="3" class="header_nav">
<?php
				echo $_SESSION->getUser()->getBusinessMenu( $GLOBALS['businessid'], $GLOBALS['companyid'], 'buscontrol' );
?>
				</td>
			</tr>
		</table>
	<div>&nbsp;</div>


<?php
$header[] = ob_get_clean();

ob_start();
	if ( $GLOBALS['security_level'] > 0 ) {
		$showyear = date( 'Y' );
		$gomonth  = date( 'm', $viewmonth );
		$goyear   = date( 'Y', $viewmonth );
?>

	<table>
		<tr>
			<td>
				<form action="/ta/gocontrol.php" method="post" onSubmit="return disableForm(this);">
					<div>
						<input type="hidden" name="companyid" value="<?php echo $GLOBALS['companyid']; ?>" />
						<input type="hidden" name="viewmonth" value="<?php echo date( 'Y-m-01', $viewmonth ); ?>" />
						<input type="hidden" name="import_pl" value="<?php echo $GLOBALS['import_pl']; ?>" />
						<select name="gomonth">
<?php
		$range = array(
			'01' => 'Jan',
			'02' => 'Feb',
			'03' => 'Mar',
			'04' => 'Apr',
			'05' => 'May',
			'06' => 'Jun',
			'07' => 'Jul',
			'08' => 'Aug',
			'09' => 'Sep',
			'10' => 'Oct',
			'11' => 'Nov',
			'12' => 'Dec',
		);

		if ( $viewmonth ) {
			$date = date( 'm', $viewmonth );
		}
		else {
			$date = date( 'm' );
		}
		echo html::select_options( $range, $date );
		
?>
						</select>
						<select name="goyear">


<?php
		// 2037 is the maximum year a 32bit integer can create, if more is
		// needed we won't be able to calculate the dates on 32bit systems.
		$range = range( date( 'Y' ), date( 'Y', strtotime( '-4 years' ) ) );
		$range = array_combine( $range, $range );

		if ( $viewmonth ) {
			$date = date( 'Y', $viewmonth );
		}
		else {
			$date = date( 'Y' );
		}
		echo html::select_options($range, $date );
?>


						</select>
						<select name="gobus">


<?php
		if ( $GLOBALS['security_level'] > 6 ) {
			$query = sprintf( "
					SELECT *
					FROM business
					WHERE companyid = '%s'
					ORDER BY businessname DESC
				"
				,$GLOBALS['companyid']
			);
		}
		elseif ( $GLOBALS['security_level'] == 1 ) {
			$query = sprintf( "
					SELECT *
					FROM business
					WHERE companyid = '%s'
						AND businessid = '%s'
					ORDER BY businessname DESC
				"
				,$GLOBALS['companyid']
				,$GLOBALS['bid']
			);
		}
		elseif ( $GLOBALS['security_level'] == 2 ) {
			$query = sprintf( "
					SELECT *
					FROM business
					WHERE companyid = '%s'
						AND (
							businessid = '%s'
							OR businessid = '%s'
							OR businessid = '%s'
							OR businessid = '%s'
							OR businessid = '%s'
							OR businessid = '%s'
							OR businessid = '%s'
							OR businessid = '%s'
							OR businessid = '%s'
							OR businessid = '%s'
						)
					ORDER BY businessname DESC
				"
				,$GLOBALS['companyid']
				,$GLOBALS['bid']
				,$GLOBALS['bid2']
				,$GLOBALS['bid3']
				,$GLOBALS['bid4']
				,$GLOBALS['bid5']
				,$GLOBALS['bid6']
				,$GLOBALS['bid7']
				,$GLOBALS['bid8']
				,$GLOBALS['bid9']
				,$GLOBALS['bid10']
			);
		}
		elseif ( $GLOBALS['security_level'] > 2 && $GLOBALS['security_level'] < 7 ) {
			$districtquery = '';
#			if ( $GLOBALS['security_level'] > 2 && $GLOBALS['security_level'] < 7 ) {
				$query = sprintf( "
						SELECT *
						FROM login_district
						WHERE loginid = '%s'
					"
					,$GLOBALS['loginid']
				);
				$result = Treat_DB_ProxyOld::query( $query );
				$num = mysql_numrows( $result );

				$num--;
				while ( $num >= 0 ) {
					$districtid = @mysql_result( $result, $num, "districtid" );
					if ( $districtquery == '' ) {
						$districtquery = "districtid = '$districtid'";
					}
					else {
						$districtquery = "$districtquery OR districtid = '$districtid'";
					}
					$num--;
				}
#			}

			$query = sprintf( "
					SELECT *
					FROM business
					WHERE companyid = '%s'
						AND (%s)
					ORDER BY businessname DESC
				"
				,$GLOBALS['companyid']
				,$districtquery
			);
		}
		$result = Treat_DB_ProxyOld::query( $query );
		$num = mysql_numrows( $result );

		$num--;
		while ( $num >= 0 ) {
			$businessname = @mysql_result( $result, $num, "businessname" );
			$busid = @mysql_result( $result, $num, "businessid" );
			
			if ( false !== stripos( $businessname, '<font' ) ) {
				$color = preg_replace(
					'@<font color=(.+)>(.+)(</font>)?@'
					,' style="color: $1;"'
					,$businessname
				);
				$businessname = preg_replace(
					'@<font color=(.+)>(.+)(</font>)?@'
					,'$2'
					,$businessname
				);
			}
			else {
				$color = '';
			}
			
			if ( $busid == $GLOBALS['businessid'] ) {
				$show = 'selected="selected"';
			}
			else {
				$show = '';
			}

			echo '<option value="', $busid, '" ', $show, $color, '>', $businessname, '</option>';
			$num--;
		}

?>


						</select>
						<input type="submit" value="GO" />
					</div>
				</form>
			</td>
			<td></td>
		</tr>
	</table>
	<div>&nbsp;</div>


<?php
	}
	// ************* END HEADER

	//////START TABLE//////////////////////////////////////////////////////////////////////////////////

	if ( $viewmonth != '' ) {
		$startdate = date( 'Y-m-01', $viewmonth );
		$temp_date = $today;
	}
	else {
		$startdate = $today;
		$temp_date = $today;
	}

	//////////////////WEEK GROUPS
	$whichweek = 1;
	$cs_week_start[$whichweek] = $startdate;
	$cs_enddate_year = substr( $startdate, 0, 4 );
	$cs_enddate_month = substr( $startdate, 5, 2 );
	$cs_enddate_month++;
	if ( $cs_enddate_month == 13 ) {
		$cs_enddate_month = 1;$cs_enddate_year++;
	}
	if ( $cs_enddate_month < 10 ) {
		$cs_enddate_month = "0$cs_enddate_month";
	}
	$cs_enddate = "$cs_enddate_year-$cs_enddate_month-01";
	$cs_enddate = prevday( $cs_enddate );

	$tempdate = $startdate;
	$cs_week_end = array();
	while ( $tempdate <= $cs_enddate ) {
		if ( dayofweek( $tempdate ) == $week_end || $tempdate == $cs_enddate ) {
			$cs_week_end[$whichweek] = $tempdate;
			$whichweek++;
			$tempdate = nextday( $tempdate );
			if ( $tempdate <= $cs_enddate ) {
				$cs_week_start[$whichweek] = $tempdate;
			}
		}
		else {
			$tempdate = nextday( $tempdate );
		}
	}
	//////////////////END WEEK GROUPS

?>
<?php
$header[] = ob_get_clean();

ob_start();
?>
				<div>&nbsp;</div>
				<form action="/ta/businesstrack.php" method="post">
					<div style="text-align: center;">
						<input type="hidden" name="username" value="<?php echo $GLOBALS['user']; ?>" />
						<input type="hidden" name="password" value="<?php echo $GLOBALS['pass']; ?>" />
						<input type="submit" value="Return to Main Page" />
					</div>
				</form>
<?php
$footer[] = ob_get_clean();

include( dirname(__FILE__) . '/application.tpl' );
?>