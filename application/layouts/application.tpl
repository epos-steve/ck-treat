<?php
?>
<!DOCTYPE html
	PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title><?php
			if ( isset( $title ) && $title ) {
				echo $title, ' :: ', Treat_Config::singleton()->getValue( 'application', 'page_title' );
			}
			else {
				echo Treat_Config::singleton()->getValue( 'application', 'page_title' );
			}
		?></title>
		<meta name="verify-v1" content="PDmo8LnHK6P8AitDtvqWv2k4/4iF5RKXVji1YAEExSk=" />
<?php
	Treat_Controller_Abstract::outputArrayVariable( Treat_Controller_Abstract::getMetas() );
?>
		<link type="image/vnd.microsoft.icon" rel="shortcut icon" href="/favicon.ico" />
		<link type="image/vnd.microsoft.icon" rel="icon"          href="/favicon.ico" />
<?php
/*
		<link rel="stylesheet" type="text/css" href="/assets/js/preload/preLoadingMessage.css" />
		<link rel="stylesheet" type="text/css" href="/assets/css/ta/_dir/generic.css" />
		<script type="text/javascript" src="/assets/js/jquery-1.4.3.min.js"></script>
		<script type="text/javascript" src="/assets/js/preload/preLoadingMessage.js"></script>
		<script type="text/javascript" src="/assets/js/ta/buscontrol2-functions.js"></script>
		<script type="text/javascript" src="/assets/js/dynamicdrive-disableform.js"></script>
		<script type="text/javascript" src="/assets/js/calendarPopup.js"></script>
		<link rel="stylesheet" type="text/css" href="/assets/css/calendarPopup.css" />
*/
	Treat_Controller_Abstract::outputArrayVariable( Treat_Controller_Abstract::getCss() );
	Treat_Controller_Abstract::outputArrayVariable( Treat_Controller_Abstract::getJavascriptsAtTop() );
?>
	</head>
	<body class="<?php echo Treat_Controller_Abstract::getBodyClass(); ?>">
		<div id="page_wrapper" class="page_wrapper">
			<div id="header">
<?php Treat_Controller_Abstract::outputArrayVariable( $header, false, "\n\t\t\t\t", "\t\t\t\t", "\n" ); ?>
<?php /*
				<a href="businesstrack.php"
					><img src="/assets/images/logo.jpg" height="43" width="205" alt="logo"
				/></a>
*/ ?>
<?php /*
			<div class="header">
				<table class="vending_operations" cellspacing="0" cellpadding="0" border="0" width="100%">
					<tr bgcolor="#ccccff">
						<td width="1%">
							<img src="weblogo.jpg" width="80" height="75" />
						</td>
						<td>
							<div style="font-size: 1.1em; font-weight: bold;"><?php echo $GLOBALS['businessname'], ' #', $GLOBALS['unit_num']; ?></div>
							<div><?php echo $GLOBALS['street']; ?></div>
							<div><?php echo $GLOBALS['city'], ', ', $GLOBALS['state'], ' ', $GLOBALS['zip']; ?></div>
							<div><?php echo $GLOBALS['phone']; ?></div>
						</td>
						<td align="right" valign="top">
							<br/>
							<form action="editbus.php" method="post">
								<div>
									<input type="hidden" name="username" value="<?php echo $GLOBALS['user']; ?>" />
									<input type="hidden" name="password" value="<?php echo $GLOBALS['pass']; ?>" />
									<input type="hidden" name="businessid" value="<?php echo $GLOBALS['businessid']; ?>" />
									<input type="hidden" name="companyid" value="<?php echo $GLOBALS['companyid']; ?>" />
									<input type="submit" value=" Store Setup " />
								</div>
							</form>
						</td>
					</tr>
					<tr>
						<td colspan="3" class="header_nav">
						</td>
					</tr>
				</table>
			</div>
*/ ?>
			</div>

<?php
if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
	echo $_SESSION->getFlashDebug( "\t\t\t<pre class=\"flash_debug\">%s</pre>\n" );
}
echo $_SESSION->getFlashError(   "\t\t\t<div class=\"flash_error\">%s</div>\n"   );
echo $_SESSION->getFlashMessage( "\t\t\t<div class=\"flash_message\">%s</div>\n" );
?>


			<div id="content">
<?php echo $content; ?>
			</div>


			<div id="footer">
<?php Treat_Controller_Abstract::outputArrayVariable( $footer, false, "\n\t\t\t\t", "\t\t\t\t", "\n" ); ?>
			</div>

		</div>
<?php
	Treat_Controller_Abstract::outputArrayVariable( Treat_Controller_Abstract::getJavascripts() );

if ( !isset( $enable_google_page_track ) || $enable_google_page_track ) {
	google_page_track();
}
?>
	</body>
</html>