<?php
$curmenu = Treat_Controller_Abstract::getSessionCookieVariable( 'curmenu' );

$query = "SELECT * FROM menu_type WHERE menu_typeid = '$curmenu'";
$result = Treat_DB_ProxyOld::query( $query );

$menu_name = @mysql_result( $result, 0, 'menu_typename' );

$business_id = abs(intval(\EE\Controller\Base::getSessionCookieVariable( 'businessid' )));
$settings = \EE\Model\Business\Setting::get("business_id = ". abs(intval($businessid)), true);
$background_image_logo = $settings->background_image ?: '';
$site_logo = $settings->site_logo ?: '/menu/logo.jpg';

$style = "text-decoration:none";
?>
<!DOCTYPE html
	PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title>Treat America: <?php echo $businessname2; ?></title>
		<link rel="stylesheet" href="/assets/css/style.css" />
		<link rel="stylesheet" href="/assets/css/calendarPopup.css" />
		<link rel="stylesheet" href="/assets/css/nutrition/style.css" />
		<link rel="stylesheet" href="/assets/css/nutrition/history.css" />
<?php includeTimeOutCode(); ?>
		<script language="JavaScript" type="text/javascript" src="/assets/js/calendarPopup.js"></script>
	</head>
	<body <?php if(false !== stripos($background_image_logo, 'backdrop.jpg')) : ?>class="standard"<?php else : ?>style="background-image: url(<?php echo $background_image_logo; ?>)"<?php endif; ?>>
		<div class="page_wrapper">
			<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
					<td colspan="4">
						<img src="<?php echo $site_logo; ?>" width="205" height="43" alt="logo" />
						<p></p>
					</td>
				</tr>
				<tr bgcolor="#6699ff">
					<td colspan="2">
						<font color="white" size="3">
							<b>
								The Right Choice... for a Healthier You!
							</b>
						</font>
					</td>
					<td colspan="2" align="right" class="account_link">
						<a href="/menu/account_settings.php?goto=3">
							My Account
						</a>
					</td>
				</tr>
				<tr bgcolor="#669933">
					<td width="10%"
						><img src="/menu/rc-header2.jpg" height="80" width="108" alt="header"
					/></td>
					<td class="account_info">
						<div class="account_company_info">
							<?php echo $page_business->companyname; ?> - <?php echo $page_business->address; ?> 
						</div>
						<div>Account: <?php echo $account->name; ?></div>
						<div>Account #: <?php echo $account->accountnum; ?></div>
					</td>
					<td colspan="2" valign="top" class="date_block">
						<div><?php echo date( 'l, F j, Y' ); ?></div>
						<div class="account_logout">
							[<a href="/menu/logout.php?goto=1"
							>Sign Off</a>]
						</div>
						<p></p>
						<div><?php echo $showcart; ?></div>
					</td>
				</tr>
				<tr bgcolor="black">
					<td height="1" colspan="4"></td>
				</tr>
			</table>


			<div class="header_menu">
				<a href="/menu/menu.php?curmenu=<?php
					echo $curmenu;
				?>">
					<?php echo $menu_name; ?> 
				</a> 
				:: 
				<a href="/menu/history.php">
					Nutritional Analysis
				</a>
				:: 
				<a href="/menu/intro.php?bid=<?php
					echo $businessid;
				?>">
					Home
				</a>
			</div>

<?php
if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
	echo $_SESSION->getFlashDebug( "\t\t\t<pre class=\"flash_debug\">%s</pre>\n" );
}
else {
	$_SESSION->getFlashDebug( null, null, null, null );
}
echo $_SESSION->getFlashError(   "\t\t\t<div class=\"flash_error\">%s</div>\n"   );
echo $_SESSION->getFlashMessage( "\t\t\t<div class=\"flash_message\">%s</div>\n" );
?>



			<div id="content">
<?php echo $content; ?>
			</div>



		</div>


		<div id="testdiv1" style="position: absolute; visibility: hidden; background-color: white;"></div>
<?php
	google_page_track();
?>
	</body>
</html>
<?php

