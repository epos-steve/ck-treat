<?php
	$company = \EE\Model\Company::getById($companyid);
	list($companyname, $com_logo) = Array(
		$company->companyname,$company->logo,
	);
?>
<!DOCTYPE html
	PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title><?php
			if ( isset( $title ) && $title ) {
				echo $title, ' :: ', $company->companyname;
			}
			else {
				echo $company->companyname;
			}
		?></title>
		<meta name="verify-v1" content="PDmo8LnHK6P8AitDtvqWv2k4/4iF5RKXVji1YAEExSk=" />
<?php
	Treat_Controller_Abstract::outputArrayVariable( Treat_Controller_Abstract::getMetas() );
?>
		<link type="image/vnd.microsoft.icon" rel="shortcut icon" href="/favicon.ico" />
		<link type="image/vnd.microsoft.icon" rel="icon"          href="/favicon.ico" />
<?php
	//Treat_Controller_Abstract::outputArrayVariable( Treat_Controller_Abstract::getCss() );
	//Treat_Controller_Abstract::outputArrayVariable( Treat_Controller_Abstract::getJavascriptsAtTop() );
	Treat_Controller_Abstract::printCss( );
	Treat_Controller_Abstract::printJavascriptsAtTop( );
?>
	</head>
	<body class="<?php echo Treat_Controller_Abstract::getBodyClass(); ?>">
		<div id="page_wrapper" class="page_wrapper">
			<table cellspacing='0' cellpadding='0' border='0' width='100%'>
				<tr>
					<td colspan='3'>
						<div id='headerMessage'></div>
						<a href="/ta/businesstrack.php"
							><img src="/assets/images/logo.jpg" border="0" height="43" width="205" alt="logo"
						/></a>
					</td>
				</tr>
<?php
	$companyId = $_GET['cid'];
	$businessId = $_GET['bid'];
	
	$business = \EE\Model\Business\Singleton::getSingleton( $businessId, $companyId );
	
	$companyLogo = $business->logo;
	if( !$companyLogo )
		$companyLogo = 'weblogo.gif';
	
?>
				<tr bgcolor="#ccccff">
					<td width='1%'>
						<img width="80" height="75" alt="Company Logo" src='/assets/images/logo/<?php echo $companyLogo; ?>' />
					</td>
					<td>
						<div style="font-size: 1.1em; font-weight: bold;"><?php echo $business->businessname, ' #', $business->unit_num; ?></div>
						<div><?php echo $business->street; ?></div>
						<div><?php echo $business->city, ', ', $business->state, ' ', $business->zip; ?></div>
						<div><?php echo $business->phone; ?></div>
						<span id='managerToggle'
							style='color: blue; cursor: pointer; font-size: 70%;'
							onmouseover='this.style.color="#FF9900"'
							onmouseout='this.style.color="blue"'
							onclick="var d=document.getElementById('managerList');if(d.style.display=='none'){d.style.display='table-row';this.innerHTML='[HIDE MANAGERS]';}else{d.style.display='none';this.innerHTML='[SHOW MANAGERS]';}"
						>
							[SHOW MANAGERS]
						</span>
					</td>
					<td align='right' valign='top'>
						<br />
						<form action='/ta/editbus.php' method='post'>
							<div>
								<input type='hidden' name='username' value='<?php echo $GLOBALS['user']; ?>' />
								<input type='hidden' name='password' value='<?php echo $GLOBALS['pass']; ?>' />
								<input type='hidden' name='businessid' value='<?php echo $businessId; ?>' />
								<input type='hidden' name='companyid' value='<?php echo $companyId; ?>' />
								<input type='submit' value=' Store Setup ' />
							</div>
						</form>
					</td>
				</tr>
				
				<tr id='managerList' style='display: none; background-color: #CCCCFF;'>
					<td>&nbsp;</td>
					<td colspan='2'>
<?php
		foreach( $managerArray as $manager ) :
?>
						<div>
							<a href='mailto:<?php echo $manager['email']; ?>' style='<?php echo $style; ?>'>
								<span style='color: blue; <?php echo $style; ?>;' onmouseover='this.style.color="#FF9900"' onmouseout='this.style.color="blue"'>
									<?php echo $manager['firstname']; ?> <?php echo $manager['lastname']; ?>, <?php echo $manager['email']; ?>
								</span>
							</a>
						</div>
<?php
		endforeach;
?>
					</td>
				</tr>
				
				<tr>
					<td colspan='3'>
						<font color='#999999'>
							<?php echo $unitMenu; ?>
						</font>
					</td>
				</tr>
			</table>
			<p></p>
			<?php echo $changeBusiness; ?>
			<p></p>
<?php
if ( !IN_PRODUCTION ) {
	echo $_SESSION->getFlashDebug( "\t\t\t<pre class=\"flash_debug\">%s</pre>\n" );
}
echo $_SESSION->getFlashError(   "\t\t\t<div class=\"flash_error\">%s</div>\n"   );
echo $_SESSION->getFlashMessage( "\t\t\t<div class=\"flash_message\">%s</div>\n" );

if( !empty( $pageTabs ) ) :
?>
			<table cellspacing="0" cellpadding="0" width="100%" class="tabbed">
				<tbody>
					<tr valign="top">
						<?php echo $pageTabs; ?>
					</tr>
				</tbody>
			</table>
<?php
endif;
?>
			<?php echo $content; ?>
	
			<div id="testdiv1" style="position: absolute; visibility: hidden; background-color: white;"></div>
	
			<form action='/ta/businesstrack.php' method='post'>
				<p style='text-align: center;'>
					<input type='hidden' name='username' value='<?php echo $GLOBALS['user']; ?>' />
					<input type='hidden' name='password' value='<?php echo $GLOBALS['pass']; ?>' />
					<input type='submit' value='Return to Main Page' />
				</p>
			</form>
		</div>
<?php
if ( !isset( $enable_google_page_track ) || $enable_google_page_track ) {
	google_page_track();
}
?>
	</body>
</html>