<?php

define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once( 'lib/class.menu_items.php' );
require_once( 'lib/class.Kiosk.php' );

if ( require_once( realpath( DOC_ROOT . '/../lib/inc/cookie-login.php' ) ) ) {
    $query = "SELECT * FROM security WHERE securityid = '".$GLOBALS['mysecurity']."'";
    $result = DB_Proxy::query($query);
	
    $sec_bus_sales=mysql_result($result,0,"bus_sales");
    $sec_bus_invoice=mysql_result($result,0,"bus_invoice");
    $sec_bus_order=mysql_result($result,0,"bus_order");
    $sec_bus_payable=mysql_result($result,0,"bus_payable");
    $sec_bus_inventor1=mysql_result($result,0,"bus_inventor1");
    $sec_bus_control=mysql_result($result,0,"bus_control");
    $sec_bus_menu=mysql_result($result,0,"bus_menu");
    $sec_bus_order_setup=mysql_result($result,0,"bus_order_setup");
    $sec_bus_request=mysql_result($result,0,"bus_request");
    $sec_route_collection=mysql_result($result,0,"route_collection");
    $sec_route_labor=mysql_result($result,0,"route_labor");
    $sec_route_detail=mysql_result($result,0,"route_detail");
	$sec_route_machine=@mysql_result($result,0,"route_machine");
    $sec_bus_labor=mysql_result($result,0,"bus_labor");
    $sec_bus_budget=mysql_result($result,0,"bus_budget");
    $sec_bus_menu_pos=mysql_result($result,0,"bus_menu_pos");
	
    if($sec_bus_menu_pos<1){
        //$location="buscatermenu.php?bid=$businessid&cid=$companyid";
		$location="buscatermenu.php?bid=".$GLOBALS['businessid']."&cid=".$GLOBALS['companyid'];
        header('Location: ./' . $location);
    }
	?>

	<html>
		<head>
			<title><?php
			if ( isset( $title ) && $title ) {
				echo $title, ' :: ', Treat_Config::singleton()->getValue( 'application', 'page_title' );
			}
			else {
				echo Treat_Config::singleton()->getValue( 'application', 'page_title' );
			}
			?></title>
			
			<!--<script type="text/javascript" src="/assets/js/dynamicdrive-disableform.js"></script>-->
			<script type="text/javascript" src="/assets/js/jquery-1.4.2.min.js"></script>
			<script type="text/javascript" src="/assets/js/common.js"></script>
			<script type="text/javascript" src="/assets/js/ta/buscatermenu4-functions.js"></script>
			<script type="text/javascript" src="/assets/js/calendarPopup.js"></script>
			<script type="text/javascript">document.write(getCalendarStyles());</script>
			<link type="text/css" rel="stylesheet" href="/assets/css/calendarPopup.css" />
		
			<!--<script type="text/javascript" src="/assets/js/prototype.js"></script>
			<script type="text/javascript" src="/assets/js/prototype-eef.js"></script>
			<script type="text/javascript" src="/assets/js/scriptaculous.js"></script>
			<script type="text/javascript" src="/ta/preLoadingMessage2.js"></script>-->
			<?php
			Treat_Controller_Abstract::outputArrayVariable( Treat_Controller_Abstract::getCss() );
			Treat_Controller_Abstract::outputArrayVariable( Treat_Controller_Abstract::getJavascriptsAtTop() );
			?>
		</head>
		<body>
			<center>
				<table cellspacing=0 cellpadding=0 border=0 width=90%>
					<tr>
						<td colspan=2>
							<a href=businesstrack.php><img src="/assets/images/logo.jpg" border="0" height="43" width="205" alt="logo"/></a>
							<p>
						</td>
					</tr>
				
					<?php
					$query = "SELECT * FROM company WHERE companyid = '$companyid'";
					$result = DB_Proxy::query($query);

					$companyname=mysql_result($result,0,"companyname");
					$com_logo=@mysql_result($result,0,"logo");

					if($com_logo == ""){$com_logo = "talogo.gif";}
					?>
					<tr bgcolor=#CCCCFF>
						<td width=1%>
							<?php echo "<img src='/assets/images/logo/$com_logo'>"; ?>
						</td>
						<td>
							<font size=4>
								<b><?php echo $GLOBALS['businessname']; ?> #<?php echo $GLOBALS['bus_unit']; ?></b>
							</font>
							<br/>
							<?php echo $GLOBALS['street']; ?>
							<br/>
							<?php echo $GLOBALS['city'], ', ', $GLOBALS['state'], ' ', $GLOBALS['zip']; ?>
							<br/>
							<?php echo $GLOBALS['phone']; ?>
						</td>
						<td align=right valign=top>
							<br/>
							<FORM ACTION=editbus.php method=post>
								<input type=hidden name=username value="<?php echo $GLOBALS['user']; ?>" />
								<input type=hidden name=password value="<?php echo $GLOBALS['pass']; ?>" />
								<input type=hidden name=businessid value="<?php echo $GLOBALS['businessid']; ?>" />
								<input type=hidden name=companyid value="<?php echo $GLOBALS['companyid']; ?>" />
								<INPUT TYPE=submit VALUE=' Store Setup ' />
							</FORM>
						</td>
					</tr>
					<tr>
						<td colspan=3>
							<font color=#999999>
								<?php
									if ($sec_bus_sales>0){echo "<a href=busdetail.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Sales</font></a>";}
									if ($sec_bus_invoice>0){echo " | <a href=businvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Invoicing</font></a>";}
									if ($sec_bus_order>0){echo " | <a href=buscatertrack.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Orders</font></a>";}
									if ($sec_bus_payable>0){echo " | <a href=busapinvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Payables</font></a>";}
									if ($sec_bus_inventor1>0){echo " | <a href=businventor.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Inventory</font></a>";}
									if ($sec_bus_control>0){echo " | <a href=buscontrol.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Control Sheet</font></a>";}
									if ($sec_bus_menu>0){echo " | <a href=buscatermenu.php?bid=$businessid&cid=$companyid><font color=red onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='red' style='text-decoration:none'>Menus</font></a>";}
									if ($sec_bus_order_setup>0){echo " | <a href=buscaterroom.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Order Setup</font></a>";}
									if ($sec_bus_request>0){echo " | <a href=busrequest.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Requests</font></a>";}
									if ($sec_route_collection>0||$sec_route_labor>0||$sec_route_detail>0||$sec_route_machine>0){echo " | <a href=busroute_collect.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Route Collections</font></a>";}
									if ($sec_bus_labor>0){echo " | <a href=buslabor.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Labor</font></a>";}
									if ($sec_bus_budget>0||($businessid == $bid && $pr1 == 1)||($businessid == $bid2 && $pr2 == 1)||($businessid == $bid3 && $pr3 == 1)||($businessid == $bid4 && $pr4 == 1)||($businessid == $bid5 && $pr5 == 1)||($businessid == $bid6 && $pr6 == 1)||($businessid == $bid7 && $pr7 == 1)||($businessid == $bid8 && $pr8 == 1)||($businessid == $bid9 && $pr9 == 1)||($businessid == $bid10 && $pr10 == 1)){echo " | <a href=busbudget.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Budget</font></a>";}
								?>
							</font>
						</td>
					</tr>
				</table>
			</center>
			<p>
			<?php

			if ($GLOBALS['security_level']>0){
				?>
				<center>
					<table width=90%>
						<tr>
							<td width=50%>
								<form action=businvoice.php method=post>
									<select name=gobus onChange='changePage(this.form.gobus)'>
										<?php
										$districtquery="";
									
										if ($GLOBALS['security_level']>2&&$GLOBALS['security_level']<7){
											$query = "SELECT * FROM login_district WHERE loginid = '".$GLOBALS['loginid']."'";
											$result = DB_Proxy::query($query);
											$num=mysql_numrows($result);

											$num--;
											while($num>=0){
											  $districtid=mysql_result($result,$num,"districtid");
											  if($districtquery==""){$districtquery="districtid = '$districtid'";}
											  else{$districtquery="$districtquery OR districtid = '$districtid'";}
											  $num--;
											}
										}

										if($GLOBALS['security_level']>6){
											$query = "SELECT * FROM business WHERE companyid = '".$GLOBALS['companyid']."' ORDER BY businessname DESC";
										} elseif($GLOBALS['security_level']==2||$GLOBALS['security_level']==1){
											$query = "SELECT * FROM business WHERE companyid = '".$GLOBALS['companyid']."' AND (businessid = '".$GLOBALS['bid']."' OR businessid = '".$GLOBALS['bid2']."' OR businessid = '".$GLOBALS['bid3']."' OR businessid = '".$GLOBALS['bid4']."' OR businessid = '".$GLOBALS['bid5']."' OR businessid = '".$GLOBALS['bid6']."' OR businessid = '".$GLOBALS['bid7']."' OR businessid = '".$GLOBALS['bid8']."' OR businessid = '".$GLOBALS['bid9']."' OR businessid = '".$GLOBALS['bid10']."') ORDER BY businessname DESC";
										} elseif($GLOBALS['security_level']>2&&$GLOBALS['security_level']<7){
											$query = "SELECT * FROM business WHERE companyid = '".$GLOBALS['companyid']."' AND ($districtquery) ORDER BY businessname DESC";
										}
									
										$result = DB_Proxy::query($query);
										$num=mysql_numrows($result);

										$num--;
									
										while($num>=0){
											$businessname=mysql_result($result,$num,"businessname");
											$busid=mysql_result($result,$num,"businessid");

											if ($busid==$businessid){
												$show="SELECTED";
											} else {
												$show="";
											}
											?>
											
											<option value="buscatermenu4-1.php?bid=<?php
												echo $busid; ?>&amp;cid=<?php
												echo $companyid; ?>" <?php
												echo $show; ?>><?php
												echo $businessname;
											?></option>
											<?php
											$num--;
										} 
										?>
									</select>
								</form>
							</td>
							<td></td>
							<td width=50% align=right>&nbsp;&nbsp;</td>
						</tr>
					</table>
				</center>
		
				<?php
			}

			/////tabs
			?>
			<center>
				<table width=90% cellspacing=0 cellpadding=0>
					<tr bgcolor=#CCCCCC valign=top>
						<td width=1%>
							<img src="/assets/images/corners/leftcrn2.jpg" height="6" width="6" align="top" alt="" />
						</td>
						<td width=13%>
							<center>								
								<a href="menus?cid=<?php
									echo $companyid; ?>&amp;bid=<?php
									echo $businessid; ?>" style="<?php
									echo $style;
								?>">
									<font color=blue>Menus</font>
								</a>
							</center>
						</td>
						<td width=1% align=right>
							<img src="/assets/images/corners/rightcrn2.jpg" height="6" width="6" align="top" alt="" />
						</td>
						<td width=1% bgcolor=white></td>
						<td width=1% bgcolor=#CCCCCC>
							<img src="/assets/images/corners/leftcrn2.jpg" height="6" width="6" align="top" alt="" />
						</td>
						<td width=13% bgcolor=#CCCCCC>
							<center>							
								<a href="menu_nutrition?cid=<?php
									echo $companyid; ?>&amp;bid=<?php
									echo $businessid; ?>" style="<?php
									echo $style;
								?>">
									<font color=blue>Menu Nutrition</font>
								</a>
							</center>
						</td>
						<td width=1% align=right bgcolor=#CCCCCC>
							<img src="/assets/images/corners/rightcrn2.jpg" height=6 width=6 align=top alt="" />
						</td>
						<td width=1% bgcolor=white></td>
						<td width=1% bgcolor=#E8E7E7>
							<img src="/assets/images/corners/leftcrn.jpg" height=6 width=6 align=top alt="" />
						</td>
						<td width=13% bgcolor=#E8E7E7>
							<center>								
								<a href="posMenu?cid=<?php
									echo $companyid; ?>&amp;bid=<?php
									echo $businessid; ?>" style="<?php
									echo $style; ?>" style="<?php
									echo $style;
									?>">
									<font color=black>POS Menu</font>
								</a>
							</center>
						</td>
						<td width=1% align=right bgcolor=#E8E7E7>
							<img src="/assets/images/corners/rightcrn.jpg" height=6 width=6 align=top alt="" />
						</td>
						<td width=1% bgcolor=white></td>
						<td width=1% bgcolor=#CCCCCC>
							<img src="/assets/images/corners/leftcrn2.jpg" height=6 width=6 align=top alt="" />
						</td>
						<td width=13% bgcolor=#CCCCCC>
							<center>								
								<a href="posReporting?cid=<?php
									echo $companyid; ?>&amp;bid=<?php
									echo $businessid; ?>" style="<?php
									echo $style; ?>" style="<?php
									echo $style;
									?>">
									<font color=blue>POS Reporting</font>
								</a>
							</center>
						</td>
						<td width=1% align=right bgcolor=#CCCCCC>
							<img src="/assets/images/corners/rightcrn2.jpg" height=6 width=6 align=top alt="" />
						</td>
						<td width=1% bgcolor=white></td>
						<td width=1% bgcolor=#CCCCCC>
							<img src="/assets/images/corners/leftcrn2.jpg" height=6 width=6 align=top>
						</td>
						<td width=13% bgcolor=#CCCCCC>
							<center>
								<a href=posManagement?cid=<?php echo $companyid; ?>&bid=<?php echo $businessid; ?> style=<?echo $style; ?>><font color=blue>POS Mgmt</a>
							</center>
						</td>
						<td width=1% align=right bgcolor=#CCCCCC>
							<img src="/assets/images/corners/rightcrn2.jpg" height=6 width=6 align=top>
						</td>
						<td width=1% bgcolor=white></td>
						<td width=1% bgcolor=#CCCCCC>
							<img src="/assets/images/corners/leftcrn2.jpg" height=6 width=6 align=top>
						</td>
						<td width=13% bgcolor=#CCCCCC>
							<center>
								<a href=casManagement?cid=<?php echo $companyid; ?>&bid=<?php echo $businessid; ?> style=<?echo $style; ?>><font color=blue>Cashier Mgmt</a>
							</center>
						</td>
						<td width=1% align=right bgcolor=#CCCCCC>
							<img src="/assets/images/corners/rightcrn2.jpg" height=6 width=6 align=top>
						</td>						
					</tr>
					<tr height=2>
						<td colspan=4></td>
						<td colspan=4></td>
						<td colspan=3 bgcolor=#E8E7E7></td>
						<td></td>
						<td colspan=4></td>
						<td colspan=4></td>
						<td colspan=1></td>
					</tr>
				</table>
			
				<div id="content">
					<?php echo $content; ?>
				</div>
			
			</center>

			<div id="testdiv1" style="position: absolute; visibility: hidden; background-color: white; layer-background-color: white;"></div>

			<br/>
			<form action=businesstrack.php method=post>
				<center>
					<input type=hidden name=username value="<?php echo $user; ?>" />
					<input type=hidden name=password value="<?php echo $pass; ?>" />
					<input type=submit value=' Return to Main Page' />
				</center>
			</form>
			<?php
			google_page_track();
}
?>
</body>
</html>