<?php
error_reporting(E_ALL);
/**
 * @author Andrew Joseph <andrew@essential-elements.net>
 */
class ParticipationController extends Treat_Controller_Abstract
{

	/** @var string */
	const BASE_URI				= '/ta/participation/';

	/** @var int business id */
	private $bid;

	/** @var integer company id */
	private $cid;

	/** @var Treat_Model_User_Object_Login */
	private $user;

	/** @var array */
	protected $_noLoginRequired = array(
	);

	/** @var string */
	public $query_vars;

	/** @var string filename for layout template */
	protected $_layout;

	public function init()
	{
		mb_internal_encoding("UTF-8");
		mb_http_output("UTF-8");
		ob_start("mb_output_handler");
		$this->user = $_SESSION->getUser(Treat_Session::TYPE_USER);

		if (!($this->user instanceof Treat_Model_User_Object_Login)) {
			header('Location: /');
			return(false);
		}
		$this->bid = (int)$_SESSION->USER->getBusinessId();
		$this->cid = (int)$_SESSION->USER->getCompanyId();
		if (isset($_REQUEST['bid']) && (int)$_REQUEST['bid'] > 0) {
			$this->bid = (int)$_REQUEST['bid'];
			$this->query_vars = "bid=" . $this->bid;
		}
		if (isset($_REQUEST['cid']) && (int)$_REQUEST['cid'] > 0) {
			$this->cid = (int)$_REQUEST['cid'];
			$this->query_vars .= "&cid=" . $this->cid;
		}

		if (isset($_REQUEST['pid']) && (int)$_REQUEST['pid'] > 0) {
			$this->pid = (int)$_REQUEST['pid'];
		}

		if (substr($_SERVER['REDIRECT_URL'], -1) != '/') {
			$redirect_url = $_SERVER['REDIRECT_URL'] . '/?' . $_SERVER['QUERY_STRING'];
			header('Location: ' . $redirect_url);
			$this->_layout = FALSE;
			return false;
		}

		$this->_layout = 'promotions.tpl';
		if (!IN_PRODUCTION) {
			$this->addJavascriptAtTop('http://code.jquery.com/jquery-latest.js');
			$this->addJavascriptAtTop('http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.js');
		} else {
			$this->addJavascriptAtTop('/assets/js/jquery-current.min.js');
			$this->addJavascriptAtTop('/assets/js/jqueryui-current.min.js');
		}
		$this->addJavascriptAtTop('/assets/js/kioskPromotions.js');
		$this->addJavascriptAtTop('/assets/js/kioskPromotionsGlobal.js');
		$this->addJavascriptAtTop('/assets/js/jquery.cookie.js');
		$this->addJavascriptAtTop('/assets/js/jquery.dynatree.js');
		$this->addCss('/assets/css/jq_themes/redmond/jquery-ui-1.8.11.custom.css');
		$this->addCss('/assets/css/jquery.dynatree/skin/ui.dynatree.css');
		$this->addCss('/assets/css/calendarPopup.css');
		$this->addCss('/assets/css/customDashboard.css');
		$this->addCss('/assets/css/jquery.ui.tooltip.css');
		$this->addCss('/assets/css/ta/businessmenus.css');
		$this->addCss('/assets/css/settingsForm.css');

		if ($this->_isXHR) {
			$this->_layout = false;
		} elseif (!empty($_REQUEST['bid'])) {
			$this->_layout = 'unitview.tpl';
		}

		$base_href = 'http://' . $_SERVER['HTTP_HOST'] . self::BASE_URI;
		if (substr($base_href, -1) == '/') {
			$base_href = substr($base_href, 0, strlen($base_href) - 1);
		}
		$this->_view->assign('base_href',	$base_href);
		$this->_view->assign('query_vars',	$this->query_vars);
		$this->_view->assign('bid',			$this->bid);
		$this->_view->assign('cid',			$this->cid);
		$this->addCss('/assets/css/manage.css');
		parent::init();
	}

	public function index()
	{
		$this->_view->assign('uri',			$this->_uri->getController());
		//$this->_view->assign('programs',	Treat_Model_Participation::getByBusiness($this->bid));
		$this->_view->assign('programs',	Treat_Model_Participation::getProgramList());
		//$this->_view->assign('upcs',		Treat_Model_Participation::getWhitelistedUpcs($this->bid));
	}

	public function show()
	{
		if (!isset($_REQUEST['ppid']) || (int)$_REQUEST['ppid'] <= 0) {
			throw new Exception('bad program id');
		}
		$ppid           = (int)$_REQUEST['ppid'];
        $upcs           = Treat_Model_Participation::getWhitelistedUPCsById($ppid);
        $business_ids   = Treat_Model_Participation::getBusinesses($ppid);

        /** @var $program Treat_Model_Participation */
        $program        = Treat_Model_Participation::get($ppid, TRUE);

		$this->_view->assign('ppid',		$ppid);
		$this->_view->assign('uri',			$this->_uri->getController());
		$this->_view->assign('program',		$program);
		$this->_view->assign('enabled',     $business_ids);
        $this->_view->assign('businesses',  $program->getBusinessesIndexedArray($business_ids));
		$this->_view->assign('upcs',		$upcs);
        $this->_view->assign('companies',   Treat_Model_Company::get());
        $this->_view->assign('conflicts',   $program->findUPCsAlreadyInKiosks($business_ids));
        $this->_view->assign('names',       Treat_Model_Participation::getMasterItemNamesByUPC($upcs));
        $this->_view->assign('items',       Treat_Model_Participation::getMasterItemsByCategory());
    }

    /**
     * Receive and process Program items information
     */
    public function save() {
        $this->_layout = false;
        $debug = (!IN_PRODUCTION) ? TRUE : FALSE;
        $ppid = (int) $_POST['ppid'];
        header('Content-Type: application/json; charset=utf-8');
        if (!isset($_POST['ppid']) || $ppid < 1) {
            echo json_encode(Array(
                'success' => 0,
                'msg' => "ppid='$ppid'",
                'error' => "Valid Program was not specified",
            ));
            return;
        }
        try {
            /** @var $pprg Treat_Model_Participation */
            $pprg = Treat_Model_Participation::get($ppid, TRUE);
        } catch (Exception $e) {
            echo json_encode(Array(
                'success' => 0,
                'msg' => "ppid='$ppid'",
                'error' => "Participation program not found. $e",
            ));
            return;
        }

        $results = '';
        //business mapping
        if (isset($_REQUEST['business']) && is_array($_REQUEST['business'])) {
            $result = $pprg->setAppliesTo($_REQUEST['business'], $_SESSION->getUser());
        }
        if (isset($_REQUEST['UPC']) && is_array($_REQUEST['UPC'])) {
            $result = $pprg->setUPCs($_REQUEST['UPC']);
        }

        if (!$debug) {
            unset($results);
        }
        echo json_encode(Array(
            'success' => TRUE,
            'msg' => "Participation program $ppid updated: " . $results,
            'error' => NULL,
        ));
        return false;
    }


}

