<?php
class ErrorController
	extends \EE\Controller\Base
{
	protected $_noLoginRequired = array(
		array(
			'controller' => 'error',
			'action' => 'e404',
		),
		array(
			'controller' => 'error',
			'action' => 'e500',
		),
	);
	
	public function e404() {
		// this page has it's own layout
		$this->_layout = '';
		$this->_view->assign( 'title', 'Essential Elements' );
		$uri = static::getOriginalUri();
		$this->_view->assign( 'path', (string)$uri );
		$this->_view->assign( 'host', (string)$uri->getHost() );
		$this->_view->assign( 'port', (string)$uri->getPort() );
		header( 'HTTP/1.0 404 Not Found' );
		header( 'Status: 404 Not Found' );
	}
	
	
	public function e500() {
		// this page has it's own layout
		$this->_layout = '';
		$this->_view->assign( 'title', 'Essential Elements' );
		$uri = static::getOriginalUri();
		$this->_view->assign( 'path', (string)$uri );
		$this->_view->assign( 'host', (string)$uri->getHost() );
		$this->_view->assign( 'port', (string)$uri->getPort() );
		header( 'HTTP/1.0 500 Internal Server Error' );
		header( 'Status: 500 Internal Server Error' );
	}
	
	
}

