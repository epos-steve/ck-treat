<?php 

class Menu_MergerController extends Treat_Controller_Abstract {
	
	public function copy( ) {
		ini_set( 'display_errors', 1 );
		$id = $_REQUEST['id'];
		$itemcode = isset($_REQUEST['code']) ? $_REQUEST['code'] : null;
		
		try {
			$item = \EE\Menu\Merger::copyMenuItem( $id, $itemcode );
		} catch( Exception $e ) {
			$item = $e;
		}
		echo '<pre>';
		var_dump( $item );
		echo '</pre>';
	}
	
}
