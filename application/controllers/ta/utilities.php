<?php

class Ta_UtilitiesController extends \EE\Controller\UnitAbstract
{
	public function index(){
		$bid = static::getPostOrGetVariable('bid');
		$cid = static::getPostOrGetVariable('cid');
		
		$this->_view->assign('bid',$bid);
		$this->_view->assign('cid',$cid);
	}
	
	public function customerPost(){
		if($_FILES && isset($_FILES['upload']) ){
			$bid = static::getPostOrGetVariable('bid');
			$cid = static::getPostOrGetVariable('cid');
			
			$num_added = 0;
			$num_updated = 0;
			$num_skipped = 0;
			
			$container = \EE\File\Upload\Csv::processFile($_FILES['upload']);  //get file container of uploaded file
			
			$file = $container->getOne();  //save file object to "file" variable
			
			$file->setFile($file->tmp_name);  //set the file name
			
			//required columns. if any are missing, misspelled, or don't exist here, processing will fail
			$req_columns = array(
					'fname',
					'lname',
					'email',
					'scancode',
					'amount'
				);
			
			$file->setBusinessId($bid);
			
			if( $file->setRequiredColumns($req_columns) ){  //all required columns were found
				if( $file->process() ){						
					foreach($file->csv_data as $data){  //iterate over each row of the csv file
						
						if($data['amount'] <= 0){  //zero balance, skip record
							$num_skipped++;
							continue;
						}						
						
						$scancode = \EE\Model\User\Customer::validateScancode($data['scancode']);
						
						if(!$scancode){  //scancode didn't validate
							foreach( \EE\Model\User\Customer::getValidationErrors() as $error){
								print $error.'<br />';
							}
							$num_skipped++;
							continue;
						}
						
						$customers = \EE\Model\User\Customer::getUserByScancode($scancode);  //get customers info
						
						if( $customers ){  //customer exists
							if( count($customers) > 1 ){  //more than one customer with same scancode, skip
								echo 'There is more than one customer with the scancode of '.$data['scancode'].'. You will have to update this manually.';
								$num_skipped++;
								continue;
							}else{  //update existing customer
								$customer = current($customers);
							}
						}
						
						if( !isset($customer) ){  //creating new customer, see if email is a duplicate
							$email = \EE\Model\User\Customer::getUserByEmail($data['email']);

							if( $email ){  //can't reuse email address
								echo 'There is more than one customer with the email of '.$data['email'].'. You will have to update this manually.';
								$num_skipped++;
								continue;
							}
						}

						\EE\Model\User\Customer::db()->beginTransaction();
						
						if( !isset($customer) ){  //create new customer
							$customer = new \EE\Model\User\Customer;

							$customer->username = $data['email'];
							$customer->businessid = $bid;
							$customer->scancode = $scancode;
							$customer->first_name = $data['fname'];
							$customer->last_name = $data['lname'];
							
							$num_added++;
						} else {
							$num_updated++;
						}
						
						$customer->temp_password = 'essential1';
						$customer->temp_used = 0;
						
						$customer->save();

						//insert record into customer_transactions table, add to balance if one is there already
						$trans_res = \EE\Model\User\Customer\Transaction::processCustomerTransaction(
								$customer,
								\EE\Model\User\Customer\Transaction::TRANSACTION_TYPE_PROMO,
								$data['amount'],
								$_SESSION->getUser()->username,
								false
						);

						\EE\Model\User\Customer::db()->commit();
						
						$customer = null;
					}
					
					print 'Adding/updating the customer data is complete.';
				} else {
					$file->addError('There was an error processing the file.');
				}
			} else {  //not all required columns were found, processing failed
				$file->addError('Required columns are missing or are misspelled.');
			}
			
			$errors = $file->getErrors();
			foreach($errors as $error){
				print $error.'<br />';
			}
			
			print '<p><strong>Customers added:</strong> '.$num_added.
					'</p><p><strong>Customers updated:</strong> '.$num_updated.
					'</p><p><strong>Customers skipped:</strong> '.$num_skipped.'</p>';
		} else {
			print 'No file was uploaded.';
		}
		
		print '<a href="/ta/utilities?bid='.$bid.'&cid='.$cid.'">Go back</a>';
		
		return false;
	}
	
	public function customerExport(){
		$bid = static::getPostOrGetVariable('bid');
		
		$users = \EE\Model\User\Customer::getUsersByBusinessId($bid);  //get all users for current business id
		
		//mapping header columns to be user-friendly and match import
		$header_cols = array(
			'first_name' => 'fname',
			'last_name' => 'lname',
			'username' => 'email',
			'scancode' => 'scancode',
			'balance' => 'amount'
		);
		
		$container = new \EE\File\Export\Csv();  //instantiate CSV object		
		
		$container->setType('csv');			
		$container->setUseHeaderRow(true);
		
		if( $container->setHeaderCols($header_cols) ){  //if set col headers for export
			$container->setFile('cust-util-export.csv');  //file we will write to
			
			$counter = 0;
			$data = array();

			//set data for export, display zero balances
			foreach($users as $user){
				$data[$counter]['first_name'] = $user->first_name;
				$data[$counter]['last_name'] = $user->last_name;
				$data[$counter]['username'] = $user->username;
				$data[$counter]['scancode'] = $user->scancode;
				$data[$counter]['balance'] = 0;

				$counter++;
			}
			
			if( $container->setData($data) ){  //successfully added data for export
				$container->setHttpHeaders();  //send csv headers to browser
				$csv = $container->toCsv();
				print $csv;
			} else {
				print 'Error setting data for export';
			}
		} else {
			print 'Error setting column headers';
		}
		
		return false;
	}
}