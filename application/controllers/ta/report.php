<?php

	use EE\Config;
	use EE\Gearman\Job\CompanyReport as PosReportJob;
	use EE\Gearman\Client as GearmanClient;
	use EE\Gearman\Exception as GearmanException;
	use EE\Model\Gearman\Process as GearmanProcess;

class Ta_ReportController extends \EE\Controller\CompanyAbstract
{
	protected $_noLoginRequired = array(
		'ta/report' => array(
#			'pos_report' => true,
#			'do-cron-report' => true,
#			'do-instant-report' => true,
			'download_report' => true,
		),
	);


	public function archive_report() {
		$report_cache_id = static::getGetVariable("rcid", 0);
		$cid = static::getGetVariable("cid", 0);
		$filename = static::getGetVariable("posrc_filename", 0);

		$obj = Treat_Model_Report_Pos_Cache::get(intval($report_cache_id), true);

		if ($obj){
			$obj->archived = "Yes";
			$ret = $obj->delete();
			$this->delete_report_files($filename, $cid);
		}

		$location = '/ta/report?cid=' . $cid;
		header( 'Location: ' . $location );

		return false;
	}

	public function delete_report_files($filename, $cid) {

		$report_filepath = DIR_REPORT_FILES . "/";
		$pos_report_slug_inventory_values = array("inventory-detail","inventory-summary","inventory-sales","inventory-waste");

		if (strstr($filename,"inventory")){
			foreach ($pos_report_slug_inventory_values as $key=>$value){

				$file2 = preg_replace(
					'#^([0-9]{4}-[0-9]{2}-[0-9]{2}-[0-9]+-)inventory-(detail|summary|sales|waste)(-(week|month)ly)#',
					'\1' . $value . '\3',
					$filename
				);

				$csv_file = $report_filepath . $file2 . ".csv";
				$json_file = $report_filepath . $file2 . ".json";

				$obj = Treat_Model_Report_Pos_Cache::get("filename='$file2'", true);

				if (($obj) && ($file2 == $filename)) {
					unlink($csv_file);
					unlink($json_file);
				} else if ($obj) {

				} else {
					unlink($csv_file);
					unlink($json_file);
				}

			}
		} else {
			$csv_file = $report_filepath . $filename . ".csv";
			$json_file = $report_filepath . $filename . ".json";

			unlink($csv_file);
			unlink($json_file);
		}

		return false;

	}


	public function do_instant_report() {
		$company_id = static::getPostOrGetVariable( 'cid', null );
		$report_id = static::getPostOrGetVariable( 'rid', null );
		$companyname = static::getPostOrGetVariable( 'companyname', null );
		$start_date = static::getPostOrGetVariable( 'start_date', null );
		$end_date = static::getPostOrGetVariable( 'end_date', null );
		$email = static::getPostOrGetVariable( 'email', null );
		
		$redirect_url = static::getPostOrGetVariable( 'redirect_url', '/ta/report/instant?cid=' . $company_id );
		$this->_view->assign( 'redirect_url', $redirect_url );
		
		$start_date = Treat_Date::getValidTimeStamp( $start_date );
		$end_date = Treat_Date::getValidTimeStamp( $end_date );
		
		if (
			$company_id
			&& $report_id
			&& $start_date
			&& $end_date
			&& $email
		) {
#			if ( $start_date > $end_date ) {
#				$tmp = $end_date;
#				$end_date = $start_date;
#				$start_date = $tmp;
#			}
			$config = Config::singleton();
			$site = getenv( 'TREAT_CONFIG' );
			if ( !$site ) {
				$site = 'treat.ini';
			}
			
			$data = array(
				PosReportJob::PARAMETER_COMPANY_ID  => $company_id,
				PosReportJob::PARAMETER_REPORT_ID   => $report_id,
				PosReportJob::PARAMETER_DATE_START  => date( 'Y-m-d', $start_date ),
				PosReportJob::PARAMETER_DATE_END    => date( 'Y-m-d', $end_date ),
				PosReportJob::PARAMETER_EMAIL       => $email,
			);
			
			$gearmanClient = new GearmanClient();
			$processModel = new GearmanProcess();
			#$processModel->setNullIdOnAddJob( true );
			$gearmanClient->setProcessLogger( $processModel );
			$gearmanClient->setSiteConfigFile( $site );
			
			$servers = $config->get( 'gearmand', 'host' );
			$defaultPort = $config->get( 'gearmand', 'port' );
			if ( !$defaultPort ) {
				$defaultPort = 4730;
			}
			if ( !is_array( $servers ) ) {
				$servers = array( sprintf( '%s:%s', $servers, $defaultPort ) );
			}
			foreach ( $servers as $server ) {
				$tmp = explode( ':', $server );
				$host = array_shift( $tmp );
				$port = array_shift( $tmp );
				if ( !$port ) {
					$port = $defaultPort;
				}
				if ( $host && $port ) {
					$gearmanClient->addServer( $host, $port );
				}
			}
			
			try{
				$jobHandle = $gearmanClient->doBackground( PosReportJob::FUNCTION_NAME, json_encode( $data ) );
#				$_SESSION->addFlashMessage( 'Report successfully added to the queue.' );
				$_SESSION->addFlashMessage( sprintf( 'Report successfully added to the queue: %s', $jobHandle ) );
			}
			catch ( \EE\Gearman\Exception $e) {
				$_SESSION->addFlashError( 'Error adding report to the queue. Code 1' );
#				var_dump( $e );
			}
			catch ( \Exception $e) {
				$_SESSION->addFlashError( 'Error adding report to the queue. Code: 5' );
#				var_dump( $e );
			}
		}
		else {
			if ( !$company_id ) {
				$_SESSION->addFlashError( 'No Company identifier. Need to know what company to run the reports for.' );
			}
			if ( !$report_id ) {
				$_SESSION->addFlashError( 'No Report identifier. Need to know what report to run.' );
			}
			if ( !$start_date ) {
				$_SESSION->addFlashError( 'Either no starting date was given or the date given was invalid.' );
			}
			if ( !$end_date ) {
				$_SESSION->addFlashError( 'Either no ending date was given or the date given was invalid.' );
			}
			if ( !$email ) {
				$_SESSION->addFlashError( 'No Email address was given to send information about the completed report.' );
			}
		}
		
#		echo sprintf( '<div>Location: <a href="%s">[%s]</a></div>', $redirect_url, var_export( $redirect_url, true ) );
		header( 'Location: ' . $redirect_url );
		return false;
	}

	public function _handle_gearman_error($errno, $errstr){
		throw new \EE\Gearman\Exception($errstr, $errno);
		return true;
	}

	public function download_report(){
		$report_url = static::getPostOrGetVariable( 'r', null );
		if ( $report_url ) {
			$path = DIR_REPORT_FILES . '/' . $report_url;
		}
		
		if ( isset( $path ) && file_exists( $path ) && is_readable( $path ) ) {
			// get the file size and send the http headers
			$size = filesize( $path );
			header( 'Content-Type: application/octet-stream' );
			header( 'Content-Length: ' . $size );
			header( 'Content-Disposition: attachment; filename=' . $report_url );
			header( 'Content-Transfer-Encoding: binary' );
			// open the file in binary read-only mode
			// display the error messages if the file can´t be opened
			$file = @fopen( $path, 'rb' );
			if ( $file ) {
				// stream the file and exit the script when complete
				fpassthru( $file );
				return false;
			}
		}
	}
	
	
	/**
	 *	Lists various reports that have previously been run
	 */
	public function index() {
		$login_user = $_SESSION->getUser();
		if ( !$login_user ) {
			$login_user = new \EE\Null();
		}
		
		$user_report_list = \EE\Model\Report\Pos\Cache::getByUserId(
			$user_id = $login_user->getId()
			,$company_id = static::getPageCompanyId()
		);
		$this->_view->assign( 'user_report_list', $user_report_list );

		$this->_view->assign( 'user_id', $user_id );
	}
	
	
	/**
	 *	Displays a list of instant reports
	 */
	public function instant() {
		$cid = static::getPageCompanyId();
		$bid = static::getPageBusinessId();

		$company = \EE\Model\Company\Singleton::getSingleton( $cid );
		$login_user = $_SESSION->getUser( \EE\Session::TYPE_USER );
		
		$report_list = \EE\Model\Report\Pos::getList( \EE\Model\Report\Pos::GET_LIST_ON_DEMAND );
		$this->_view->assign( 'report_list', $report_list );
		
		$this->_view->assign( 'user_id', $login_user->getId() );
		
		$this->_view->assign( 'cid', $cid );
		$this->_view->assign( 'bid', $bid );
		$this->_view->assign( 'company', $company );
		$this->_view->assign( 'email', $login_user->getEmail() );
		
		$this->_view->assign( 'redirect_url', '/ta/report/instant?cid=' . $cid );
	}
	
	
	public function customer_balance() {
		set_time_limit(300);
		function adjust_customer_balance($id, $calculated_balance, $stored_balance) {
			$today = date('Y-m-d G:i:s');
			$authresponse = 'EE - ' . $today;

			$message = '';
			if ($calculated_balance > $stored_balance) {
				$sql = "
					UPDATE
						mburris_businesstrack.customer
					SET
						balance = $calculated_balance
					WHERE
						customerid = $id
					LIMIT 1
				";

				$balance_adjustment_amount = ($calculated_balance - $stored_balance);
				$result = Treat_DB_ProxyOld::query($sql);

				$sql2 = "
					INSERT INTO customer_transactions
					(
						oid,
						trans_type_id,
						customer_id,
						subtotal,
						chargetotal,
						response,
						authresponse
					)
					VALUES
					(
						'Balanced CK Card',
						9,
						$id,
						'0.00',
						'0.00',
						'APPROVED',
						'$authresponse'
					)
				";
				$result = Treat_DB_ProxyOld::query($sql2);

				$message = "Balance Adjustment: +" . $balance_adjustment_amount;
			} else if ($calculated_balance < $stored_balance) {
				$balance_adjustment_amount = ($stored_balance - $calculated_balance);
				$sql = "
					INSERT INTO customer_transactions
					(
						oid,
						trans_type_id,
						customer_id,
						subtotal,
						chargetotal,
						response,
						authresponse
					)
					VALUES
					(
						'CK Adjust',
						8,
						$id,
						'$balance_adjustment_amount',
						'$balance_adjustment_amount',
						'APPROVED',
						'$authresponse'
					)
				";
				$result = Treat_DB_ProxyOld::query($sql);
				$message = "CK Adjustment: +" . $balance_adjustment_amount;
			}

			return $message;
		}

		function get_amount_value($type_id, $amount) {
			if (($type_id == 1) || ($type_id == 4) || ($type_id == 6) || ($type_id == "Promotion") || ($type_id == "Credit Card Transaction")){
				$return_value = ($amount);
			} else {
				$return_value = $amount;
			}

			return $return_value;
		}

		function get_payment_type($transaction_type_id) {
			switch($transaction_type_id) {
				case 1:
					$transaction_type_label = "Kiosk Cash Reload";
				break;

				case 4:
					$transaction_type_label = "Kiosk CC Reload";
				break;

				case 6:
					$transaction_type_label = "Kiosk CC Reload";
				break;

				case 14:
					$transaction_type_label = "Purchase";
				break;

				case "Company Kitchen Refund":
					$transaction_type_label = "Company Kitchen Refund";
				break;

				case "Transfer":
					$transaction_type_label = "Transfer";
				break;

				case "CK Adjust":
					$transaction_type_label = "CK Adjust";
				break;

				case "Promotion":
					$transaction_type_label = "Promotion";
				break;

				case "Credit Card Transaction":
					$transaction_type_label = "Credit Card Transaction";
				break;

				case "Balanced CK Card":
					$transaction_type_label = "Balanced CK Card";
				break;

				default:
					$transaction_type_label = "N/A";
				break;
			}

			return $transaction_type_label;
		}

		$total_customers = $_SESSION["total_customers"];
		$accounts_balanced = $_SESSION["accounts_balanced"];
		$accounts_oob = $_SESSION["accounts_oob"];

		$accounts_oob_1 = 0;
		$accounts_oob_2 = 0;
		$accounts_oob_5 = 0;
		$accounts_oob_10 = 0;
		$accounts_oob_20 = 0;
		$accounts_oob_20_plus = 0;

		$accounts_rebalanced = array();
		if (is_array($_POST) && count($_POST) > 0) {
			$customer_data_value = $_SESSION['customer_data_value'];
			$data_values = unserialize($customer_data_value);
			if ( is_array($data_values) ) {
				foreach ($data_values as $key => $value) {
					foreach ($value as $key2 => $value2) {
						$customer_current_calculated_balance = $value2;
						$get_customer_data_line_sql = "
							SELECT
								CONCAT(c.first_name, ' ', c.last_name) AS customer_info,
								c.scancode AS scancode,
								c.balance AS stored_balance
							FROM
								mburris_businesstrack.customer c
							WHERE
								c.customerid = $key2
						";

						$get_customer_data_line_result = Treat_DB_ProxyOldProcessHost::query($get_customer_data_line_sql);
						$customer_data_line = mysql_result($get_customer_data_line_result, 0, "customer_info");
						$customer_scancode = mysql_result($get_customer_data_line_result, 0, "scancode");
						$customer_current_stored_balance = mysql_result($get_customer_data_line_result, 0, "stored_balance");

						$customer_message = adjust_customer_balance(
							$key2,
							$customer_current_calculated_balance,
							$customer_current_stored_balance
						);

						$accounts_rebalanced[] = array(
							'data_line' => $customer_data_line,
							'scancode' => $customer_scancode,
							'message' => $customer_message,
						);
					}
				}
			}
		}
		$this->_view->assign('accounts_rebalanced', $accounts_rebalanced);


		$query = "
			SELECT
				business.businessid,
				business.businessname,
				company.companyname
			FROM
				business
			JOIN company ON
				company.companyid = business.companyid
			WHERE
				(business.companyid != 2)
				AND (business.categoryid = 4)
			ORDER BY
				company.companyname,
				business.businessname
		";
		$result = Treat_DB_ProxyOldProcessHost::query($query);

		$businessOptions = array();
		while ($r = mysql_fetch_object($result)) {
			$businessOptions[] = $r;
		}
		$this->_view->assign('business_options', $businessOptions);

		$bid = 0;
		$show_all = 0;
		$show_detail = 0;
		if (isset($_GET['bid'])) {
			$bid = intval($_GET['bid']);
		}

		if (isset($_GET['show_all'])) {
			$show_all = intval($_GET['show_all']);
		}

		if (isset($_GET['show_detail'])) {
			$show_detail = intval($_GET['show_detail']);
		}

		$this->_view->assign('bid', $bid);
		$this->_view->assign('show_all', $show_all);
		$this->_view->assign('show_detail', $show_detail);


		$sortByTime = function($a, $b) {
			return strtotime($a['bill_datetime']) > strtotime($b['bill_datetime']);
		};

		$number_of_correct_card_balances = 0;
		$number_of_not_correct_balances = 0;

		$other_data = array();
		$data = array();
		$line_counter = 1;
		$query = "
			SELECT
				business.businessid,
				business.businessname,
				company.companyname
			FROM
				mburris_businesstrack.business
			JOIN mburris_businesstrack.company ON
				company.companyid = business.companyid
			WHERE
				(business.businessid = $bid)
			ORDER BY
				business.companyid,
				business.businessname
		";
		$result = Treat_DB_ProxyOldProcessHost::query($query);

		$date_range = mktime(0,0,0, 1,1,2000);
		$from_date = '2001-01-01 00:00:00';
		$to_date = date('Y-m-d 23:59:59');
		if ( isset($_GET['date_from']) && '' !== trim($_GET['date_from']) && strtotime($_GET['date_from']) > $date_range ) {
			$from_date = date('Y-m-d 00:00:00', strtotime($_GET['date_from']));
		}
		if ( isset($_GET['date_to']) && '' !== trim($_GET['date_to']) && strtotime($_GET['date_to']) > $date_range ) {
			$to_date = date('Y-m-d 00:00:00', strtotime($_GET['date_to']));
		}
		$this->_view->assign('date_from', date('Y-m-d', strtotime($from_date)));
		$this->_view->assign('date_to', date('Y-m-d', strtotime($to_date)));

		$customers = array();

		while ($r = mysql_fetch_array($result)) {
			$businessid = $r['businessid'];
			$businessname = $r['businessname'];
			$companyname = $r['companyname'];

			$conditionCustomer = '';
			if (intval($bid) !== 52)
			{
				$conditionCustomer = "AND scancode LIKE '780%'";
			}

			$query55 = "
				SELECT
					first_name,
					last_name,
					customerid,
					scancode,
					balance
				FROM
					mburris_businesstrack.customer
				WHERE
					businessid = $businessid
					$conditionCustomer
			";
			$result55 = Treat_DB_ProxyOldProcessHost::query($query55);
			$num55 = @mysql_numrows($result55);

			while ($r55 = @mysql_fetch_array($result55)) {
				$scancode = $r55["scancode"];
				$customerid = $r55["customerid"];
				$mybalance = $r55["balance"];
				$first_name = $r55["first_name"];
				$last_name = $r55["last_name"];

				$customer_name = $first_name . " " . $last_name;

				$sql = "
					SELECT
						balance
					FROM
						mburris_businesstrack.customer
					WHERE
						scancode LIKE '$scancode'
				";
				$result2 = Treat_DB_ProxyOldProcessHost::query($sql);
				$customer_balance = mysql_result($result2, 0, 'balance');

				$counter = 1;

				$sql2 = "
					SELECT
						customer.customerid,
						checks.check_number,
						checks.businessid,
						checks.bill_datetime,
						checks.total,
						checks.sale_type,
						payment_detail.received,
						payment_detail.payment_type,
						business.businessname
					FROM
						checks
					JOIN payment_detail ON
						payment_detail.check_number = checks.check_number
						AND payment_detail.businessid = checks.businessid
						AND payment_detail.scancode = '$scancode'
					JOIN customer USING (scancode)
					JOIN business ON
						business.businessid = checks.businessid
					WHERE
						checks.bill_datetime BETWEEN '$from_date' AND '$to_date'
					ORDER BY bill_datetime DESC
				";
				$result2 = Treat_DB_ProxyOldProcessHost::query($sql2);
				$oob_array = array();

				while ($rs = mysql_fetch_array($result2)) {
					$customerid = $rs['customerid'];
					$check_number = $rs['check_number'];
					$businessid2 = $rs['businessid'];
					$bill_datetime = $rs['bill_datetime'];
					$total = $rs['total'];
					$sale_type = $rs['sale_type'];
					$received = $rs['received'];
					$payment_type_value = $rs['payment_type'];
					$businessname = $rs['businessname'];

					$customer_transaction_type_label = get_payment_type($payment_type_value);

					$oob_array[] = array(
						'pay_value' => $payment_type,
						'payment_type' => $payment_type_value,
						'bill_datetime' => $bill_datetime,
						'total' => $total,
						'businessname' => $businessname,
					);
				}

				$additional_customer_transactions_sql = "
					SELECT
						ctt.label AS payment_type,
						ct.date_created AS bill_datetime,
						ct.chargetotal AS total,
						'Not Available' AS businessname
					FROM
						customer_transactions ct
					INNER JOIN customer_transaction_types ctt ON
						ct.trans_type_id = ctt.id
					WHERE
						(customer_id = $customerid)
						AND (ct.response LIKE 'APPROVED')
						AND  customer_transactions.bill_datetime BETWEEN '$from_date' AND '$to_date'
					ORDER BY ct.date_created DESC
				";
				$additional_customer_transactions_result = Treat_DB_ProxyOldProcessHost::query($additional_customer_transactions_sql);

				while ($rs = mysql_fetch_array($additional_customer_transactions_result)) {
					$payment_type_value = $rs['payment_type'];
					$bill_datetime = $rs['bill_datetime'];
					$total = $rs['total'];
					$businessname = $rs['businessname'];

					$oob_array[] = array(
						'payment_type' => $payment_type_value,
						'bill_datetime' => $bill_datetime,
						'total' => $total,
						'businessname' => $businessname,
					);
				}

				usort($oob_array, $sortByTime);

				$running_balance = 0;
				$running_balance_array = array();
				$value3 = 0;
				$value4 = 0;
				foreach ($oob_array as $key => $value) {
					$payment_type = $value['payment_type'];
					$bill_datetime = $value['bill_datetime'];
					$total = $value['total'];
					$businessname = $value['businessname'];

					if (($payment_type == 1) || ($payment_type == 4) || ($payment_type == 6) || ($payment_type == 'Promotion') || ($payment_type == 'Credit Card Transaction') || ($payment_type == "Company Kitchen Refund") || ($payment_type == "Transfer") || ($payment_type == "CK Adjust")) {
						$total = $total * -1;
						$value3 += $total;
					} else {
						$value4 += $total;
					}
					$running_balance += $total;

					$running_balance_array[] = array(
						'pay_value' => $payment_type,
						'payment_type' => $payment_type_value,
						'bill_datetime' => $bill_datetime,
						'total' => $total,
						'businessname' => $businessname,
						'running_balance' => $running_balance,
					);
				}

				$calculated_balance = $value3 + $value4;
				$calculated_balance = number_format($calculated_balance, 2);

				$calculated_balance = $calculated_balance * -1;
				$show_customer_line_items = 0;

				$oob_amount = $customer_balance - $calculated_balance;

				if ($customer_balance == $calculated_balance) {
					$number_of_correct_card_balances++;
					$header_row_css = "check-header-row";
					$check_detail_row_css = "check-detail-row";
					$customer_stored_balance_row = "customer-balance-headers";
				} else {
					$number_of_not_correct_balances++;
					$header_row_css = "check-header-row-oob";
					$check_detail_row_css = "check-detail-row-oob";
					$customer_stored_balance_row = "customer-balance-headers-oob";
					$customer_calculated_balance_row = "";
					$show_customer_line_items = 1;
					$fix_customers[] = array($customerid => $calculated_balance);

					if (($oob_amount > 20) || ($oob_amount < -20)) {
						$accounts_oob_20_plus++;
						$fix_customers_oob_20_plus[] = array($customerid => $calculated_balance);
					} else if (($oob_amount > 10) || ($oob_amount < -10)) {
						$accounts_oob_20++;
						$fix_customers_oob_20[] = array($customerid => $calculated_balance);
					} else if (($oob_amount > 5) || ($oob_amount < -5)) {
						$accounts_oob_10++;
						$fix_customers_oob_10[] = array($customerid => $calculated_balance);
					} else if (($oob_amount > 2) || ($oob_amount < -2)) {
						$accounts_oob_5++;
						$fix_customers_oob_5[] = array($customerid => $calculated_balance);
					} else if (($oob_amount > 1) || ($oob_amount < -1)) {
						$accounts_oob_2++;
						$fix_customers_oob_2[] = array($customerid => $calculated_balance);
					} else if (($oob_amount > 0) || ($oob_amount < 0)) {
						$accounts_oob_1++;
						$fix_customers_oob_1[] = array($customerid => $calculated_balance);
					}
				}

				if ($show_all == 1 || ($show_all == 0) && ($show_customer_line_items == 1)) {
					$customers[] = array(
						'customer_name' => $customer_name,
						'customer_balance' => $customer_balance,
						'calculated_balance' => $calculated_balance,
					);

					usort($running_balance_array, $sortByTime);
					$running_balance_array = array_reverse($running_balance_array);

					$kl = 0;
					foreach ($running_balance_array as $key => $value2) {
						$pay_value = $running_balance_array[$kl]['pay_value'];
						$payment_type = $running_balance_array[$kl]['payment_type'];
						$bill_datetime = $running_balance_array[$kl]['bill_datetime'];
						$total = $running_balance_array[$kl]['total'];
						$businessname = $running_balance_array[$kl]['businessname'];
						$running_balance_2 = $running_balance_array[$kl]['running_balance'];
						$running_balance_2 = number_format($running_balance_2, 2);

						$customer_transaction_type_label = get_payment_type($pay_value);
						$label = $customer_transaction_type_label;

						if (($customer_transaction_type_label == 'CK Adjust') || ($customer_transaction_type_label == 'Balanced CK Card')) {
							$sql3 = "SELECT authresponse FROM customer_transactions WHERE date_created = '$bill_datetime' LIMIT 1";
							$result3 = Treat_DB_ProxyOldProcessHost::query($sql3);
							$businessname = mysql_result($result3, 0, 'authresponse');
						}

						if (($label == "Company Kitchen Refund") || ($label == "CK Adjust")) {
							$total = ($total * -1);
						}

						if ($kl & 1) {
							$odd_even = "-odd";
						} else {
							$odd_even = "-even";
						}

						$kl++;

						$data_values_2 = array(
							'customer_transaction_type_label' => $customer_transaction_type_label,
							'bill_datetime' => $bill_datetime,
							'total' => $total,
							'running_balance_2' => $running_balance_2,
							'businessname' => $businessname,
						);

						$customers[] = $data_values_2;
					}
				}
			}
		}


		$data_value = serialize($fix_customers);

		$_SESSION["customer_data_value"] = $data_value;

		$_SESSION["total_customers"] = $number_of_correct_card_balances + $number_of_not_correct_balances;
		$_SESSION["accounts_balanced"] = $number_of_correct_card_balances;
		$_SESSION["accounts_oob"] = $number_of_not_correct_balances;

		$this->_view->assign('total_customers', ($number_of_correct_card_balances + $number_of_not_correct_balances));
		$this->_view->assign('accounts_balanced', $number_of_correct_card_balances);
		$this->_view->assign('accounts_oob', $number_of_not_correct_balances);

		$oob_1 = 0;
		$oob_2 = 0;
		$oob_5 = 0;
		$oob_10 = 0;
		$oob_20 = 0;
		$oob_21 = 0;

		$this->_view->assign('accounts_oob_1', $accounts_oob_1);
		$this->_view->assign('accounts_oob_2', $accounts_oob_2);
		$this->_view->assign('accounts_oob_5', $accounts_oob_5);
		$this->_view->assign('accounts_oob_10', $accounts_oob_10);
		$this->_view->assign('accounts_oob_20', $accounts_oob_20);
		$this->_view->assign('accounts_oob_20_plus', $accounts_oob_20_plus);


		$kl = 0;

		$report_table = array();
		foreach ($customers as $key => $value) {
			if ($value['customer_name']) {
				$k2 = 0;
				$customer_name_value = $value['customer_name'];
				$customer_balance = $value['customer_balance'];
				$calculated_balance = $value['calculated_balance'];

				if ($value['customer_balance'] == $value['calculated_balance']) {
					$number_of_correct_card_balances++;
					$header_row_css = "check-header-row";
					$check_detail_row_css = "check-detail-row";
					$customer_stored_balance_row = "customer-balance-headers-1";
				} else {
					$number_of_not_correct_balances++;
					$header_row_css = "check-header-row-oob";
					$check_detail_row_css = "check-detail-row-oob";
					$customer_stored_balance_row = "customer-balance-headers-oob-1";
					$customer_calculated_balance_row = "";
					$show_customer_line_items = 1;
				}

				$report_table[] = array(
					$customer_stored_balance_row => array(
						$value['customer_name'],
						'Stored Balance',
						'$'.$value['customer_balance'],
						'Calculated Balance',
						'$'.$value['calculated_balance'],
					),
				);
				if (($_GET['show_detail'] == 1)) {
					$report_table[] = array(
						$header_row_css => array(
							'Payment Type',
							'Date/Time',
							'Amount',
							'Balance',
							'Location',
						),
					);
				}
			}

			if (($_GET['show_detail'] == 1)) {
				if ($kl & 1) {
					$odd_even = "-odd";
				} else {
					$odd_even = "-even";
				}

				$kl++;

				$report_table[] = array(
					$check_detail_row_css . $odd_even => array(
						$value['customer_transaction_type_label'],
						$value['bill_datetime'],
						$value['total'],
						$value['running_balance_2'],
						$value['businessname'],
					),
				);
			}
		}

		$this->_view->assign('report_table', $report_table);
		@ob_end_flush();
	}



}

