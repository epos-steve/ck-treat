<?php

/* Scott 3-22-12 */
class Ta_PromosettingsController extends Treat_Controller_CompanyAbstract
{
	protected $_noLoginRequired = array(
		'ta/report' => array(
			'pos_report' => true,
			'do-cron-report' => true,
			'do-instant-report' => true,
		),
	);	
	
	protected $_layout = 'promotions.tpl';
	
	public function index() {
		error_reporting(E_ALL);
		ini_set('display_errors', '1');
		
		$this->_view->assign('uri',$this->_uri->getController());
		
		$this->addJavascriptAtTop('/assets/js/jquery-1.5.1.min.js');
		$this->addJavascriptAtTop('/assets/js/jquery-ui-1.8.11.custom.min.js');
		$this->addJavascriptAtTop('/assets/js/ta/promo_settings.js');
		$this->addCss( '/assets/css/preload/preLoadingMessage.css');		
		$this->addCss('/assets/css/jq_themes/redmond/jqueryui-current.css');
		$this->addCss('/assets/css/jq_themes/redmond/jqueryui-custom.css');
		
		/* build manufacturer dropdown */		
		$query = "SELECT * FROM manufacturer ORDER BY name";
		$result = $this->_db->query($query);

		$man_select = '<option value="">Select Manufacturer</option>';
		
		while( $r_man = $result->fetch() ) {			
			$man_select .= '<option value="'.$r_man["id"].'">'.$r_man["name"].'</option>';
		}
		
		/* build category dropdown */		
		$query = "SELECT * FROM menu_items_category ORDER BY name";
		$result = $this->_db->query($query);

		$cat_select = '<option value="">Select Category</option>';

		while( $r_cat = $result->fetch() ) {			
			$cat_select .= '<option value="'.$r_cat["Id"].'">'.$r_cat["Name"].'</option>';
		}
		
		$this->_view->assign('man_select',$man_select);
		$this->_view->assign('cat_select',$cat_select);
	}
	
	public function ajaxCreateManufacturer(){
		if($this->_isXHR){
			header('Content-type: application/json');
		
			/* missing required field(s) */
			if( empty($_POST['man_name']) || empty($_POST['man_id']) ){
				$return = array('passed' => 0, 'error' => 'req_missing');
				
				$return = json_encode($return);
				
				print $return;
				
				return false;
			}
			
			if($_POST['man_id'] == -1){  //create a new manufacturer
				$query = 'INSERT INTO manufacturer (name)
							VALUES("'.$_POST['man_name'].'")';
							
				$result = $this->_db->query($query);
				
				$insert_id = $this->_db->lastInsertId(); //last insert id of newly created manufacturer
			} else {  //update existing manufacturer
				$query = 'UPDATE manufacturer
							SET name = "'.$_POST['man_name'].'"
							WHERE id = '.$_POST['man_id'];
							
				$result = $this->_db->query($query);

				$insert_id = $_POST['man_id'];  //id of updated manufacturer
			}
			
			$return = array('man_id' => $insert_id, 'man_name' => $_POST['man_name'], 'passed' => 1);
			$return = json_encode($return);				
			print $return;
				
			return false;
			
		} else {
			print 'not ajax';
		}
	}
	
	public function ajaxCreateCategory(){
		if($this->_isXHR){
			header('Content-type: application/json');
		
			/* missing required field(s) */
			if( empty($_POST['cat_name']) || empty($_POST['cat_id']) ){
				$return = array('passed' => 0, 'error' => 'req_missing');
				
				$return = json_encode($return);
				
				print $return;
				
				return false;
			}
			
			if($_POST['cat_id'] == -1){  //create a new category
				$query = 'INSERT INTO menu_items_category (Name)
							VALUES("'.$_POST['cat_name'].'")';
							
				$result = $this->_db->query($query);
				
				$insert_id = $this->_db->lastInsertId(); //last insert id of newly created category
			} else {  //update existing category
				$query = 'UPDATE menu_items_category
							SET Name = "'.$_POST['cat_name'].'"
							WHERE Id = '.$_POST['cat_id'];
							
				$result = $this->_db->query($query);

				$insert_id = $_POST['cat_id'];  //id of updated category
			}
			
			$return = array('cat_id' => $insert_id, 'cat_name' => $_POST['cat_name'], 'passed' => 1);
			$return = json_encode($return);				
			print $return;
				
			return false;
			
		} else {
			print 'not ajax';
		}
	}
	
	public function ajaxAllManufacturers(){
		if($this->_isXHR){
			/* build manufacturer list */
			$query = "select id, name
						from manufacturer
						order by name";
						
			$result = $this->_db->query($query);
			
			$man = '<option value="">Select Manufacturer</option>';
			
			while( $r = $result->fetch() ) {
				$man .= '<option value="'.$r['id'].'">'.$r['name'].'</option>';
			}
			
			print $man;

			return false;
		} else {
			print 'not ajax';
		}
	}
	
	public function ajaxAllCategories(){
		if($this->_isXHR){
			/* build category list */
			$query = "select Id, Name
						from menu_items_category
						order by Name";
						
			$result = $this->_db->query($query);
			
			$cat = '<option value="">Select Category</option>';
			
			while( $r = $result->fetch() ) {
				$cat .= '<option value="'.$r['Id'].'">'.$r['Name'].'</option>';
			}
			
			print $cat;

			return false;
		} else {
			print 'not ajax';
		}
	}
	
	public function ajaxCheckDeleteMan(){
		if($this->_isXHR){
			header('Content-type: application/json');			
						
			$query = "select Id
						from menu_items_master
						where Manufacturer = ".$_POST['man_id'];
						
			$result = $this->_db->query($query);
			
			$num_rows = $result->rowCount();
			
			if($num_rows > 0){
				$return = array('allow_delete' => 0);
			} else {
				$return = array('allow_delete' => 1);
			}

			$return = json_encode($return);				
			print $return;
			
			return false;
		} else {
			print 'not ajax';
		}
	}
	
	public function ajaxCheckDeleteCat(){
		if($this->_isXHR){
			header('Content-type: application/json');			
						
			$query = "select Id
						from menu_items_master
						where Category = ".$_POST['cat_id'];
						
			$result = $this->_db->query($query);
			
			$num_rows = $result->rowCount();
			
			if($num_rows > 0){
				$return = array('allow_delete' => 0);
			} else {
				$return = array('allow_delete' => 1);
			}

			$return = json_encode($return);				
			print $return;
			
			return false;
		} else {
			print 'not ajax';
		}
	}
	
	public function ajaxDeleteMan(){
		if($this->_isXHR){
			header('Content-type: application/json');
			
			$query = "select Id
						from menu_items_master
						where Manufacturer = ".$_POST['man_id'];
						
			$result = $this->_db->query($query);
			
			$num_rows = $result->rowCount();
			
			if($num_rows == 0){  //no corresponding rows in menu_items_master, safe to delete
				$query = "delete from manufacturer
							where id = ".$_POST['man_id'];
				
				$result = $this->_db->exec($query);
				
				if($result !== false){  //successfully deleted row
					$return = array('pass' => 1, 'man_id' => $_POST['man_id']);
				} else {  //failed to delete
					$return = array('pass' => 0);
				}
			} else {
				$return = array('pass' => 0);
			}
			
			$return = json_encode($return);
			print $return;
			
			return false;
		} else {
			print 'not ajax';
		}
	}
	
	public function ajaxDeleteCat(){
		if($this->_isXHR){
			header('Content-type: application/json');
			
			$query = "select Id
						from menu_items_master
						where Category = ".$_POST['cat_id'];
						
			$result = $this->_db->query($query);
			
			$num_rows = $result->rowCount();
			
			if($num_rows == 0){  //no corresponding rows in menu_items_master, safe to delete
				$query = "delete from menu_items_category
							where Id = ".$_POST['cat_id'];
				
				$result = $this->_db->exec($query);
				
				if($result !== false){  //successfully deleted row
					$return = array('pass' => 1, 'cat_id' => $_POST['cat_id']);
				} else {  //failed to delete
					$return = array('pass' => 0);
				}
			} else {
				$return = array('pass' => 0);
			}
			
			$return = json_encode($return);
			print $return;
			
			return false;
		} else {
			print 'not ajax';
		}
	}
}