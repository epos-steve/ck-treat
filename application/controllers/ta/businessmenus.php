<?php

use Essential\Treat\Db\ProxyOld;
use Essential\Treat\Db\ProxyOld\ProcessHost;

class Ta_BusinessmenusController
	extends \EE\Controller\UnitAbstract
{
	static protected $_javascript_minify = true;
	static protected $_css_minify = true;
	
	static protected $_pageTabs = array(
		// -Page Url/Controller+Action-, -Page Name-, -Security Array( -Security Name- => -Security Level- )-
		array( array( 'menus' ), 'Menus' ),
		array( array( 'menu_nutrition' ), 'Menu Nutrition' ),
#		array( array( 'posMenuLinking' ), 'POS Menu Linking' ),
		array( array( 'posMenu' ), 'POS Menu' ),
		array( array( 'posReporting' ), 'POS Reporting' ),
		array( array( 'posManagement' ), 'POS Management' ),
	);

	protected $_delayJQuery = array(
		'menus',
	);

	protected $_methodSecurity = array(
		'menu_nutrition' =>			array( 'securityName' => 'menu_nutrition', 'securityLevel' => '1' ),
		'menu_nutrition_import' =>	array( 'securityName' => 'menu_nutrition', 'securityLevel' => '1' ),
		'menu_nutrition_save' =>	array( 'securityName' => 'menu_nutrition', 'securityLevel' => '1' ),
		'posMenuLinking' => 		array( 'securityName' => 'bus_menu_linking', 'securityLevel' => '1' ),
		'posMenu' => 				array( 'securityName' => 'bus_menu_pos', 'securityLevel' => '1' ),
		'posReporting' =>			array( 'securityName' => 'bus_menu_pos_report', 'securityLevel' => '1' ),
		'posManagement' => 			array( 'securityName' => 'bus_menu_pos_mgmt', 'securityLevel' => '1' ),
	);
	
	static protected $_metaControllers = array( 'Action', 'Menus', 'Nutrition', 'Posmanagement' );
	
	
	/*** BEGIN META CONTROLLER CODE ***/
	
	protected function loadMetaControllers() {
		$metaControllers = array();
		$cn = preg_replace( '/Controller$/', '', get_called_class() ) . 'meta';
		
		foreach( static::$_metaControllers as $meta ) {
			$mcn = $cn . '_' . $meta . 'Controller';
			$metaControllers[$meta] = new $mcn( $this );
		}
		
		static::$_metaControllers = $metaControllers;
	}
	
	public function __construct( $uri ) {
		$this->loadMetaControllers();
		
		parent::__construct( $uri );
	}
	
	public function __call( $name, $args ) {
		foreach( static::$_metaControllers as $meta ) {
			if( method_exists( $meta, $name ) ) {
				return call_user_func_array( array( $meta, $name ), $args );
			}
		}
		
		throw new SiTech_Exception( 'Method ' . $name . ' not found on ' . get_called_class() . ' or any of its meta controllers' );
	}
	
	public static function __callStatic( $name, $args ) {
		return $this->__call( $name, $args );
	}
	
	public function metaCall( $name, $args ) {
		return call_user_func_array( array( $this, $name ), $args );
	}
	
	public function metaGet( $name ) {
		return $this->{$name};
	}
	
	public function metaSet( $name, $value ) {
		$this->{$name} = $value;
	}
	
	public function metaStaticGet( $name ) {
		return static::${$name};
	}
	
	public function metaStaticSet( $name, $value ) {
		static::${$name} = $value;
	}
	
	public function metaIsset( $name ) {
		return isset( $this->{$name} );
	}
	
	public function metaUnset( $name ) {
		unset( $this->{$name} );
	}
	
	/*** END META CONTROLLER CODE ***/
	
	
	public static function getEditItemCode( $mic_id, $min_id, $menu_item_id ) {
		return implode( ':', array(
			$mic_id,
			$min_id,
			$menu_item_id,
		) );
	}

	public function init() {
		// redirect legacy urls
		if( $_SERVER['REQUEST_METHOD'] == 'GET'
			&& !stristr( $_SERVER['REQUEST_URI'], '/' . $this->_uri->getController() . '/' ) ) {
			$url = $this->_uri->getScheme()
				. '://'
				. $this->_uri->getHost()
				. '/'
				. $this->_uri->getController()
				. '/'
				. $this->_uri->getAction( true )
				. '?'
				. $this->_uri->getQueryString();
			header( 'Location: ' . $url );
			return false;
		}
		
		static::$_jQueryVersion = \EE\Controller\Base::JQUERY_1_7_2;
		AjaxWrapper::setRequestCallbackMethod( 'startAjax', $this );
		
		parent::init();
	}

	
	/*
	 * Utility functions
	 */
	protected function slidenavRender( $navLinks, $navSecurity = null, $securityLevel = null ) {
		$sec = null;
		$tplPath = $this->_uri->getController() . '/' . $this->_uri->getAction() . '/';

		foreach( $navLinks as &$link ) {
			$link['render'] = '';
			if ( $link['enabled'] ) {
				$tpl = $tplPath . $link['id'] . '.tpl';
				$link['render'] = $this->_view->render( $tpl );
			}
			if ( isset( $link['callback'] ) ) {
				$link['callback'] = str_replace( '\'', '"', $link['callback'] );
			}
			// security
			$link['disabled'] = !( (bool)$link['enabled'] );
		}
		$this->_view->assign( 'navLinks', $navLinks );
	}
	
	static public function slidenavLinks( $navLinks ) {
		foreach( $navLinks as $link ) {
			if( $link['disabled'] ) {
				echo '<span class="navLink disabled">' . $link['title'] . '</span>';
			} else {
				echo '<a class="navLink" href="#' . $link['id'] . '" onclick="' . @$link['callback'] . '">';
				echo $link['title'];
				echo '</a>';
			}
		}
	}
	
	static public function slidenavPanels( $navLinks ) {
		foreach( $navLinks as $link ) {
			if( $link['disabled'] )
				continue;
			
			echo '<div id="nav-' . $link['id'] . '" class="navPanel" style="display: none;">';
			echo $link['render'];
			echo '</div>';
		}
	}


	/*
	 * Begin ajax functions
	 */
	public function startAjax( $json = false ) {
		if( IN_PRODUCTION || !isset( $_REQUEST['debug'] ) ) {
			// referrer security check
			$refRequest = explode( '?', $_SERVER['HTTP_REFERER'] );
			$refParts = explode( '/', $refRequest[0] );
	
			$_server = 2;
			$_action = count( $refParts ) - 1;
			$controllerParts = array_slice( $refParts, 3, $_action - 3 );
			$controller = implode( '/', $controllerParts );
	
			if( !preg_match( '#.*/buscatermenu\d?\.php$#', $refRequest[0] )
				&& !preg_match( '#.*/setup.php$#', $refRequest[0] )
				&& ( !preg_match( '#^' . $_SERVER['SERVER_NAME'] . '(:.*)?$#', $refParts[$_server] )
					|| $controller != $this->_uri->getController()
					|| !is_callable( array( $this, $refParts[$_action] ) ) )
			) {
				// for legacy pages
				if( !preg_match( '#.*/buscatermenu\d?\.php$#', $refRequest[0] ) ) {
					static::error_exit( 'Access denied' );
				}
			}
		}
		
		if( $json ) {
			header( 'Content-Type: application/json; charset=utf-8' );
		}
		
		// ajax fix for ie
		mb_internal_encoding( "UTF-8" );
		mb_http_output( "UTF-8" );
		ob_start( "mb_output_handler" );
	}

	public function posAjax() {
		$rv = false;

		$todo = intval( $_REQUEST['todo'] );

		$todoList = array(
			1 => 'updateMenuTax',
			2 => 'updateMenuGroups',
			3 => 'updateCustomer',
			4 => 'updateCustomerScancodes',
			5 => 'processCustomerTransaction',
			6 => 'menuCheckDetail',
			7 => 'processMenuTax',
			8 => 'resetForSync',
			9 => 'resetBusiness',
			10 => 'updatePromotions',
			11 => 'getPromoTargets',
			12 => 'updatePromoTargets',
			13 => NULL,
			14 => 'getPromoSchedule',
			15 => 'updatePromoSchedule',
			16 => 'getPromotions',
			17 => 'togglePromoActive',
			18 => 'reorderPromo',
			19 => 'updatePromoGroup',
			20 => 'getPromoGroup',
			21 => 'getPromoGroupDetails',
			22 => 'updatePromoGroupDetails',
			23 => 'setVMachDetails',
			24 => 'addVMachSlot',
			25 => 'deleteVMachSlot',
			26 => 'updateVMach',
			27 => 'updateOrder',
			28 => 'realtimeUpdate',
			29 => 'realtimeCustCount',
			30 => 'realtimeStock',
			31 => 'itemHistory',
			32 => 'masterItemList',
			33 => 'trendingItems',
			34 => 'trendDashboard',
			35 => 'trendDetail',
		);
		
		try {
			if( ( $fn = $todoList[$todo] ) ) {
				$rv = $this->$fn();
			} else {
				throw new Exception( 'Bad function' );
			}
		} catch( \PDOException $e ) {
			static::error_exit( 'A database error has occurred' . ( IN_PRODUCTION ? '' : ': ' . $e->getMessage() ) );
		} catch( \Exception $e ) {
			static::error_exit( $e->getMessage() );
		}
		
		return $rv;
	}
	
	public function createDateRangeArray($strDateFrom,$strDateTo)
	{
		$aryRange=array();

		$iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2), substr($strDateFrom,8,2),substr($strDateFrom,0,4));
		$iDateTo=mktime(1,0,0,substr($strDateTo,5,2), substr($strDateTo,8,2),substr($strDateTo,0,4));

		if ($iDateTo>=$iDateFrom)
		{
			array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry
			while ($iDateFrom<$iDateTo)
			{
				$iDateFrom+=86400; // add 24 hours
				array_push($aryRange,date('Y-m-d',$iDateFrom));
			}
		}
		
		return $aryRange;
	}

	public function trendDetail(){
		ini_set('display_errors',false);
		$date = @$_POST["date"];
		$level = @$_POST["level"];
                $id = @$_POST["id"];
                $trend = @$_POST["trend"];

		$data = array();

		$startdate = date( "Y-m-d", mktime( 0, 0, 0, substr($date, 5, 2), substr($date, 8, 2) - 13, substr($date, 0, 4) ) );

		if($level == "c"){
			$query = "SELECT d.districtid AS id, d.districtname AS name, ROUND( SUM( db.$trend ) /14, 0 ) AS total
				FROM dashboard db
				JOIN business b ON b.businessid = db.businessid
						AND b.companyid = $id
						AND b.categoryid =4
				JOIN district d ON d.districtid = b.districtid
				WHERE db.date BETWEEN  '$startdate' AND '$date'
				GROUP BY d.districtid
				ORDER BY total DESC";
		}
		elseif($level == "d"){
			$query = "SELECT b.businessid AS id, b.businessname AS name, ROUND( SUM( db.$trend ) /14, 0 ) AS total
				FROM dashboard db
				JOIN business b ON b.businessid = db.businessid
					AND b.districtid = $id
					AND b.categoryid =4
				WHERE db.date BETWEEN  '$startdate' AND '$date'
				GROUP BY b.businessid
				ORDER BY total DESC";
		}
		else{
			$query = "SELECT 1";
		}

		$result = ProcessHost::query( $query );

		$count = 1;
		while($r = mysql_fetch_array($result)){
			$data[$count]["id"] = $r["id"]; 
			$data[$count]["name"] = $r["name"];
			$data[$count]["value"] = $r["total"]; 
			
			$count++;
		}

		$data = json_encode($data);
		$data = str_replace("<pre>","",$data);

		echo $data;

	}
	
	public function trendDashboard(){
		ini_set('display_errors',false);
		$level = @$_POST["level"];
		$id = @$_POST["id"];
		$trend = @$_POST["trend"];
		
		$date1 = date( "Y-m-d", mktime( 0, 0, 0, date( "m" ), date( "d" ) - 89, date( "Y" ) ) );
		$startdate = date( "Y-m-d", mktime( 0, 0, 0, date( "m" ), date( "d" ) - 76, date( "Y" ) ) );
		$date2 = date("Y-m-d");
		
		$dateArray = $this->createDateRangeArray($date1,$date2);
		
		$data = array();
		foreach($dateArray AS $key => $value){
			$data[$value] = 0;
			if($value > $date2){
				unset($data[$value]);
			}
		}
		
		if($level == "c"){
			$query = "SELECT db.date, SUM(db.$trend) AS $trend FROM dashboard db 
					JOIN business b ON b.businessid = db.businessid AND b.companyid = $id
					WHERE db.date BETWEEN '$date1' AND '$date2' 
					GROUP BY db.date 
					ORDER BY db.date";
		}
		elseif($level == "d"){
			$query = "SELECT db.date, SUM(db.$trend) AS $trend FROM dashboard db 
					JOIN business b ON b.businessid = db.businessid AND b.districtid = $id
					WHERE db.date BETWEEN '$date1' AND '$date2' 
					GROUP BY db.date 
					ORDER BY db.date";
		}
		else{
			$query = "SELECT date, $trend FROM dashboard WHERE businessid = $id AND date BETWEEN '$date1' AND '$date2' ORDER BY date";
		}
		$result = ProcessHost::query( $query );
		
		while($r = mysql_fetch_array($result)){
			$data[$r["date"]] = $r["$trend"];
		}
		
		$returnData = array();
		$tempDate1 = $date1;
		$tempDate2 = $startdate;
		while($tempDate2 <= $date2){
			$tempDate1restart = nextday($tempDate1);
			while($tempDate1 <= $tempDate2){
				$returnData[$tempDate2] += $data[$tempDate1];
				$tempDate1 = nextday($tempDate1);
			}
			$returnData[$tempDate2] = round($returnData[$tempDate2] / 14);
			$tempDate1 = $tempDate1restart;
			$tempDate2 = nextday($tempDate2);
		}
		
		$returnData = json_encode($returnData);
		
		echo $returnData;
	}
	
	public function trendingItems(){
		ini_set('display_errors',false);
		
		$data = array();
		$counter = 1;
		
		$query = "SELECT * FROM menu_item_master_trend ORDER BY RAND()";
		$result = ProcessHost::query( $query );
		
		while($r = mysql_fetch_array($result)){
			$category = $r["category"];
			$details = $r["details"];
			
			$data[$counter] = $details;
			$counter++;
		}
		
		$data = json_encode($data);
		$data = str_replace("<pre>","",$data);
		
		echo $data;
	}
	
	public function masterItemList(){
		ini_set('display_errors',false);
		
		$data = array();
		
		$query = "SELECT mim.Id, mim.Name, mim.UPC FROM menu_items_master mim ORDER BY Name";
		$result = ProcessHost::query( $query );
		
		while($r = mysql_fetch_array($result)){
			$id = $r["Id"];
			$name = $r["Name"];
			$upc = $r["UPC"];
			
			$data[$id] = $name;
		}
		
		$data = json_encode($data);
		$data = str_replace("<pre>","",$data);
		
		echo $data;
	}
	
	public function itemHistory(){
		ini_set('display_errors',false);
		$id = intval( @$_POST['itemid'] );
		
		$date1 = date( "Y-m-d", mktime( 0, 0, 0, date( "m" ), date( "d" ) - 59, date( "Y" ) ) );
		$date2 = date("Y-m-d");
		
		$dateArray = $this->createDateRangeArray($date1,$date2);
		
		$data = array();
		foreach($dateArray AS $key => $value){
			$data[$value] = 0;
			if($value > $date2){
				unset($data[$value]);
			}
		}
		
		$query = "SELECT date, sold FROM menu_item_master_hist WHERE Id = $id AND date BETWEEN '$date1' AND '$date2' ORDER BY date ASC";
		$result = ProcessHost::query( $query );
		
		while($r = mysql_fetch_array($result)){
			$data[$r["date"]] = $r["sold"];
		}
		
		$data = json_encode($data);
		$data = str_replace("<pre>","",$data);
		
		echo $data;
	}
	
	public function realtimeStock(){
		ini_set('display_errors',false);
		$ordertype = intval( @$_POST['ordertype'] );
		$stock = intval( @$_POST['stock'] );
		
		$dates = array();
		
		$dates[1][1] = date( "Y-m-d", mktime( 0, 0, 0, date( "m" ), date( "d" ) - 1, date( "Y" ) ) );
		$dates[1][2] = date( "Y-m-d", mktime( 0, 0, 0, date( "m" ), date( "d" ), date( "Y" ) ) );
		$dates[1][3] = date( "Y-m-d", mktime( 0, 0, 0, date( "m" ), date( "d" ) + 1, date( "Y" ) ) );
		
		$dates[2][1] = date( "Y-m-d", mktime( 0, 0, 0, date( "m" ), date( "d" ) - 13, date( "Y" ) ) );
		$dates[2][2] = date( "Y-m-d", mktime( 0, 0, 0, date( "m" ), date( "d" ) - 6, date( "Y" ) ) );
		$dates[2][3] = date( "Y-m-d", mktime( 0, 0, 0, date( "m" ), date( "d" ) + 1, date( "Y" ) ) );
		
		$dates[3][1] = date( "Y-m-d", mktime( 0, 0, 0, date( "m" ), date( "d" ) - 59, date( "Y" ) ) );
		$dates[3][2] = date( "Y-m-d", mktime( 0, 0, 0, date( "m" ), date( "d" ) - 29, date( "Y" ) ) );
		$dates[3][3] = date( "Y-m-d", mktime( 0, 0, 0, date( "m" ), date( "d" ) + 1, date( "Y" ) ) );
		
		if($ordertype > 0){
			$selectQry = "category_rank AS rank";
			$orderQry = "ORDER BY category_rank ASC";
			$whereQry = "WHERE ordertype = $ordertype AND category_rank > 0";
		}
		else{
			$selectQry = "rank AS rank";
			$orderQry = "ORDER BY rank ASC";
			$whereQry = "WHERE rank > 0";
		}
		
		$limit = $stock - 1;
		
		$query = "SELECT Id, Name, $selectQry FROM menu_items_master
					$whereQry
					$orderQry
					LIMIT $limit, 10
					";
		$result = ProcessHost::query( $query );
		
		$data = array();
		$counter = 1;
		
		while($r = mysql_fetch_array($result)){
			$id = $r["Id"];
			$name = $r["Name"];
			$rank = $r["rank"];
			$line_counter = 1;
			
			$data[$counter]['id'] = $id;
			$data[$counter]['rank'] = $rank;
			$data[$counter]['name'] = $name;
			
			for($count1=1;$count1<=3;$count1++){
				for($count2=1;$count2<=2;$count2++){
					$query2 = "SELECT IF(SUM(sold),SUM(sold),0) AS total FROM menu_item_master_hist WHERE Id = $id AND date >= '{$dates[$count1][$count2]}' AND date < '{$dates[$count1][$count2+1]}'";
					$result2 = ProcessHost::query( $query2 );
					
					$r2 = mysql_fetch_array($result2);
					$total = $r2["total"];
					
					$data[$counter][$line_counter] = $total;
					$line_counter++;
				}
			}
			$counter++;
		}
		
		$data = json_encode($data);
		$data = str_replace("<pre>","",$data);
		
		echo $data;
	}
	
	public function realtimeCustCount(){
		ini_set('display_errors',false);
		
		$level = intval( @$_POST['level'] );
		$businessid = intval( @$_POST['businessid'] );
		$companyid = intval( @$_POST['companyid'] );
		$interval = intval( @$_POST['interval'] );
		
		if($interval == 2){
			$range = "DATE_SUB(NOW() , INTERVAL 30 MINUTE) - INTERVAL 1 YEAR AND NOW() - INTERVAL 1 YEAR";
		}
		elseif($interval == 1){
			$range = "DATE_SUB(NOW() , INTERVAL 30 MINUTE) - INTERVAL 7 DAY AND NOW() - INTERVAL 7 DAY";
		}
		else{
			$range = "DATE_SUB(NOW() , INTERVAL 15 MINUTE) AND NOW()";
		}
		
		if($level == 1){
			$query = "SELECT COUNT(c.id) as total, 
					SUM(c.total) as amt 
				FROM checks c 
				JOIN payment_detail pd ON pd.check_number = c.check_number 
					and pd.businessid = c.businessid 
					AND pd.payment_type NOT IN (1,6) 
					JOIN business b ON b.businessid = c.businessid AND b.companyid = $companyid
				WHERE bill_posted BETWEEN $range";
		}
		elseif($level == 2){
			$query = "SELECT COUNT(c.id) as total, 
					SUM(c.total) as amt 
				FROM checks c 
				JOIN payment_detail pd ON pd.check_number = c.check_number 
					and pd.businessid = c.businessid 
					AND pd.payment_type NOT IN (1,6) 
					JOIN business b ON b.businessid = c.businessid AND b.businessid = $businessid
				WHERE bill_posted BETWEEN $range";
		}
		else{
			$query = "SELECT COUNT(c.id) as total, 
					SUM(c.total) as amt 
				FROM checks c 
				JOIN payment_detail pd ON pd.check_number = c.check_number 
					and pd.businessid = c.businessid 
					AND pd.payment_type NOT IN (1,6) 
				WHERE bill_posted BETWEEN $range";
		}
		$result = ProcessHost::query( $query );
		
		$r = mysql_fetch_array($result);
		$temp = $r["total"];
		$amt =  $r["amt"];
		
		echo $temp . ":" . $amt;
	}
	
	public function realtimeUpdate(){
		ini_set('display_errors',false);
	
		$ordertype = intval( @$_POST['ordertype'] );
		$level = intval( @$_POST['level'] );
		$businessid = intval( @$_POST['businessid'] );
		$companyid = intval( @$_POST['companyid'] );
		
		///filter ordertype
		if($ordertype > 0){
			$addQuery = "AND min.ordertype = $ordertype";
			$addQuery2 = "AND mim.ordertype = $ordertype";
		}
		else{
			$addQuery = "";
			$addQuery2 = "";
		}
		
		///filter level
		if($level == 1){
			$query = "SELECT SUM( cd.quantity ) AS total, mim.Name, mim.UPC, min.name AS localName, min.upc, mim.rank, mim.category_rank
				FROM checks c
				JOIN checkdetail cd ON c.check_number = cd.bill_number
				AND c.businessid = cd.businessid
				AND c.bill_posted
				BETWEEN DATE_SUB( NOW( ) , INTERVAL 15 
				MINUTE ) 
				AND NOW( ) 
				JOIN menu_items_new min ON min.businessid = cd.businessid
				AND min.pos_id = cd.item_number
				$addQuery
				LEFT JOIN menu_items_master mim ON mim.UPC = min.upc
				JOIN business b ON b.businessid = min.businessid AND b.companyid = $companyid
				AND b.categoryid =4
				GROUP BY min.upc
				ORDER BY total DESC 
				LIMIT 10";
		}
		elseif($level == 2){
			$query = "SELECT SUM( cd.quantity ) AS total, mim.Name, mim.UPC, min.name AS localName, min.upc, mim.rank, mim.category_rank
				FROM checks c
				JOIN checkdetail cd ON c.check_number = cd.bill_number
				AND c.businessid = cd.businessid
				AND c.bill_posted
				BETWEEN DATE_SUB( NOW( ) , INTERVAL 15 
				MINUTE ) 
				AND NOW( ) 
				JOIN menu_items_new min ON min.businessid = cd.businessid
				AND min.pos_id = cd.item_number
				$addQuery
				LEFT JOIN menu_items_master mim ON mim.UPC = min.upc
				JOIN business b ON b.businessid = min.businessid AND b.businessid = $businessid
				AND b.categoryid =4
				GROUP BY min.upc
				ORDER BY total DESC 
				LIMIT 10";
		}
		else{
			$query = "SELECT SUM( cd.quantity ) AS total, mim.Name, mim.UPC, min.name AS localName, min.upc, mim.rank, mim.category_rank
				FROM checks c
				JOIN checkdetail cd ON c.check_number = cd.bill_number
				AND c.businessid = cd.businessid
				AND c.bill_posted
				BETWEEN DATE_SUB(NOW() , INTERVAL 15 MINUTE) AND NOW()
				JOIN menu_items_new min ON min.businessid = cd.businessid
				AND min.pos_id = cd.item_number
				JOIN menu_items_master mim ON mim.UPC = min.upc $addQuery2
				JOIN business b ON b.businessid = min.businessid
				AND b.categoryid =4
				WHERE min.businessid >1
				GROUP BY mim.UPC
				ORDER BY total DESC 
				LIMIT 10";
		}
		
		$data = array();
		
		//$query = "SELECT name AS Name, item_code AS total FROM menu_items_new LIMIT 110,10";
		$result = ProcessHost::query( $query );
	
		$counter = 1;

		while($r = mysql_fetch_array($result)){
			$total = $r["total"];
			$name = $r["Name"];
			$localName = $r["localName"];
			$rank = $r["rank"];
			$category_rank = $r["category_rank"];
			
			if($name == ""){
				$name = $localName;
			}
			
			$name = str_replace("&","and",$name);
			
			$data[$counter]['name'] = $name;
			$data[$counter]['total'] = $total;
			$data[$counter]['rank'] = $rank;
			$data[$counter]['cat_rank'] = $category_rank;

			$counter++;
		}
		
		$data = json_encode($data);
		$data = str_replace("<pre>","",$data);
		
		echo $data;
	}
	/**
	 * @todo if you modify this method, move all of the `echo` output & put it in the template where it belongs
	 * @todo if you modify this method, move the queries into the models
	 * @todo if you modify this method, use either filter_input() function or \EE\Controller\Base::getPost(), etc methods - don't use @ on $_POST, bad practice
	 */
	public function setVMachDetails() {
		trigger_error( 'Check the @todo and fix this method.', E_USER_WARNING );
		$id = intval( @$_POST['id'] );
		$bid = intval( @$_POST['bid'] );
		
		///build menu list
		$query = "SELECT id,name FROM menu_items_new WHERE businessid = $bid ORDER BY ordertype DESC,name ASC";
		$result = ProxyOld::query( $query );
		
		$items[0] = "Please Choose...";
		while ( $r = mysql_fetch_array( $result ) ) {
			$items[ $r["id"] ] = $r["name"];
		}
		
		$query = "SELECT miv.id, 
					miv.menu_itemid, 
					miv.col_row,
					mip.price AS net,
					ROUND(mip.price + (mip.price * (mt.percent / 100)), 2) AS gross
				FROM menu_items_vmach miv
				JOIN menu_items_new min ON min.id = miv.menu_itemid
				JOIN menu_items_price mip ON mip.menu_item_id = miv.menu_itemid
				LEFT JOIN menu_item_tax mit ON mit.menu_itemid = miv.menu_itemid
				LEFT JOIN menu_tax mt ON mt.id = mit.tax_id
				WHERE miv.client_programid = $id ORDER BY miv.col_row ASC";
		$result = ProxyOld::query( $query );
		
		$counter = 0;
		
		echo "
		<table style=\"border:1px solid #999999;\" border=0 cellspacing=0 cellpadding=0 width=60%>
			<tr bgcolor=#999999>
				<td style=\"border:1px solid #999999;\">Existing Slots</td>
				<td style=\"border:1px solid #999999;\">Menu Item</td>
				<td style=\"border:1px solid #999999;\" align=right>Price</td>
				<td style=\"border:1px solid #999999;\" align=right>Price+Tax</td>
				<td style=\"border:1px solid #999999;\" align=right>&nbsp;</td>
			</tr>
		";
		
		while ( $r = mysql_fetch_array( $result ) ) {
			$vmach_id = $r["id"];
			$menu_itemid = $r["menu_itemid"];
			$col_row = $r["col_row"];
			$net = $r["net"];
			$gross = $r["gross"];
			
			if($gross == 0){
				$gross = $net;
			}
			
			$counter++;
                        
			if($counter < 10){$showspace = "&nbsp;";}
			else{$showspace = "";}
			
			echo "
			<tr id=row$vmach_id>
				<td style=\"border:1px solid #999999;\">$showspace$counter. <input type=text size=6 id=col$vmach_id value=$col_row onblur=\"vmachUpdate($id, $vmach_id, 1);\"></td>
				<td style=\"border:1px solid #999999;\"><select name=menu_item id=menu_item$vmach_id onchange=\"vmachUpdate($id, $vmach_id, 2);\">";
					foreach($items AS $key => $value){
						if($key == $menu_itemid){$selected = "SELECTED";}
						else{$selected = "";}
						
						echo "<option value=$key $selected>$value</option>";
					}
				echo "</select></td>
					<td style=\"border:1px solid #999999;\" align=right>
						$<span id=net$vmach_id>$net</span>&nbsp;
					</td>
					<td style=\"border:1px solid #999999;\" align=right>
						$<span id=gross$vmach_id>$gross</span>&nbsp;
					</td>
					<td style=\"border:1px solid #999999;\" width=1%>
						<input type=button value='X' onclick=\"deleteSlot($id, $bid, $vmach_id);\">
					</td>
			</tr>
			";
		}
		
		if ( $counter == 0 ) {
			echo "<tr><td colspan=2 style=\"border:1px solid #999999;\">No Data Found</td></tr>";
		}
		
		echo "</table><br>";
		
		$counter++;
		
		////add new slot
		echo "<table style=\"border:1px solid #999999;\" border=0 cellspacing=0 cellpadding=0 width=60%>
			<tr bgcolor=#999999>
				<td style=\"border:1px solid #999999;\" width=20%>New Slot</td>
				<td style=\"border:1px solid #999999;\" colspan=2>Menu Item</td>
			</tr>
		";
		
		echo "<tr>
				<td style=\"border:1px solid #999999;\">$counter. <input type=text size=6 id=newslot></td>
				<td style=\"border:1px solid #999999;\"><select name=menu_item id=newitem>";
					foreach ( $items as $key => $value ) {
						echo "<option value=$key>$value</option>";
					}
				echo "</select></td>
					<td style=\"border:1px solid #999999;\" width=10%>
						<input type=button id=addSlotButton value='Add' onclick=\"addSlot($id, $bid);\">
					</td>
		</tr></table>
		";
	}
	
	/**
	 * @todo if you modify this method, move the queries into the models
	 * @todo if you modify this method, use either filter_input() function or \EE\Controller\Base::getPost(), etc methods - don't use @ on $_POST, bad practice
	 */
	public function addVMachSlot() {
		trigger_error( 'Check the @todo and fix this method.', E_USER_WARNING );
		$id = intval( @$_POST['id'] );
		$bid = intval( @$_POST['bid'] );
		$newSlot =  @$_POST['newSlot'];
		$newItem = intval( @$_POST['newItem'] );
		
		$query = "
			INSERT INTO menu_items_vmach (client_programid, menu_itemid, col_row)
			VALUES ($id, $newItem, '$newSlot')
		";
		$result = ProxyOld::query( $query );
		
		sleep(1);
	}
	
	/**
	 * @todo if you modify this method, move the queries into the models
	 * @todo if you modify this method, use either filter_input() function or \EE\Controller\Base::getPost(), etc methods - don't use @ on $_POST, bad practice
	 */
	public function deleteVMachSlot() {
		trigger_error( 'Check the @todo and fix this method.', E_USER_WARNING );
		$id = intval( @$_POST['vmach_id'] );
		
		$query = "DELETE FROM menu_items_vmach WHERE id = $id";
		$result = ProxyOld::query( $query );
		
		sleep(1);
	}
	
	/**
	 * @todo if you modify this method, move the queries into the models
	 * @todo if you modify this method, use either filter_input() function or \EE\Controller\Base::getPost(), etc methods - don't use @ on $_POST, bad practice
	 */
	public function updateVMach() {
		trigger_error( 'Check the @todo and fix this method.', E_USER_WARNING );
		$vmach_id = intval( @$_POST['vmach_id'] );
		$which = intval( @$_POST['which'] );
		$newValue = @$_POST['newValue'];
		
		if ( $which == 1 ) {
			$field = "col_row";
		}
		elseif ( $which == 2 ) {
			$field = "menu_itemid";
		}
		
		$query = "UPDATE menu_items_vmach SET $field = '$newValue' WHERE id = $vmach_id";
		$result = ProxyOld::query( $query );
		
		if($which == 2){
			$query = "SELECT mip.price AS net, ROUND(mip.price + (mip.price * (mt.percent / 100)), 2) AS gross
						FROM menu_items_new min 
						JOIN menu_items_price mip ON mip.menu_item_id = min.id
						LEFT JOIN menu_item_tax mit ON mit.menu_itemid = min.id
						LEFT JOIN menu_tax mt ON mt.id = mit.tax_id
						WHERE min.id = $newValue";
			$result = ProxyOld::query( $query );
			
			$net = ProxyOld::mysql_result($result, 0, "net");
			$gross = ProxyOld::mysql_result($result, 0, "gross");
			
			if($gross == 0){
				$gross = $net;
			}
			
			echo $net . "," . $gross;
		}
	}
	
	public function updateOrder() {
		$orderid = intval( @$_POST['orderid'] );
		$item_id = intval( @$_POST['item_id'] );
		$item_code = @$_POST['item_code'];
		$field = @$_POST['field'];
		$amt = @$_POST['amt'];
		
		if($orderid > 0 && $item_id > 0 && $item_code != ''){
			if($amt != 0){
				$query = "INSERT INTO machine_orderdetail (orderid, menu_items_new_id, item_code, $field)
						VALUES ($orderid, $item_id, '$item_code', '$amt')
						ON DUPLICATE KEY UPDATE $field = '$amt'";
			}
			else{
				$query = "DELETE FROM machine_orderdetail WHERE orderid = $orderid AND item_code = '$item_code'";
			}
			$result = ProxyOld::query( $query );
		}
		else{
			alert('Failed');
		}
	}

	public function getPromoGroup()
	{
		$this->startAjax();

		if (!($promoGroup = \EE\Model\Promotion\Group::get((int)$_POST['id'], true))) {
			echo json_encode(array());
		} else {
			echo $promoGroup->toJson();
		}
		return(false);
	}

	public function getPromoGroupDetails() {
		$this->startAjax();
		$bid = intval( @$_POST['bid'] );
		$id = intval( @$_POST['id'] );
		
		if( !( $bid > 0 ) ) {
			echo '<p>Invalid business id</p>';
			return false;
		}
		
		if( !( $id > 0 ) ) {
			echo '<p>Invalid promo group id</p>';
			return false;
		}
		
		$groups = \EE\Model\Menu\ItemNew::getN1GroupByBusiness( $bid );
		
		echo '<div class="settingsTable"><div class="settingsRow">';
		$k = -1;
		foreach( $groups as $group => $items ) {
			if( ++$k % 3 == 0 ) {
				echo '</div><div class="settingsRow">';
			}
			
			echo '<div class="settingsThird"><div class="settingsHeader ui-state-default">' . $group . '</div><div class="settingsForm">';
			
			foreach( $items as $item ) {
				$selected = \EE\Model\Promotion\Group\Detail::isMenuItemSelected( $item['id'], $id );
				echo '<div><label><input type="checkbox" name="' . $item['id'] . '" value="1"' . ( ( $selected ) ? ' checked="checked"' : '' ) . ' />' . $item['name'] . '</label></div>';
			}
			
			echo '</div></div>';
		}
		
		echo '</div></div>';
		
		return false;
	}


	public function updatePromoGroupDetails()
	{
		$filter = new SiTech_Filter(SiTech_Filter::INPUT_POST);
		$bid = $filter->input( 'bid', SiTech_Filter::FILTER_SANITIZE_NUMBER_INT );
		$checked = $filter->input('checked', SiTech_Filter::FILTER_VALIDATE_BOOLEAN);
		$id = $filter->input('id', SiTech_Filter::FILTER_SANITIZE_NUMBER_INT);
		$item_id = $filter->input('item_id', SiTech_Filter::FILTER_SANITIZE_NUMBER_INT);

		if ($checked) {
			\EE\Model\Promotion\Group\Detail::addItemToGroup( $bid, $id, $item_id );
		} else {
			\EE\Model\Promotion\Group\Detail::removeItemFromGroup( $bid, $id, $item_id );
		}

		return(false);
	}

	public function updatePromoGroup()
	{
		$this->startAjax();

		$filter = new SiTech_Filter(SITECH_FILTER::INPUT_POST);
		$bid = $filter->input('bid', SiTech_Filter::FILTER_SANITIZE_NUMBER_INT);
		$id = $filter->input('promo_group_id', SiTech_Filter::FILTER_SANITIZE_NUMBER_INT);
		$name = $filter->input('promo_group_name', SiTech_Filter::FILTER_SANITIZE_STRING);
		$visible = (int)$filter->input('promo_group_visible', SiTech_Filter::FILTER_VALIDATE_BOOLEAN);
		$routing = (int)$filter->input('promo_group_routing', SiTech_Filter::FILTER_SANITIZE_NUMBER_INT);
		$order = $filter->input('promo_group_order', Sitech_Filter::FILTER_SANITIZE_NUMBER_INT);

		if (empty($id)) {
			// new record
			$uuid = $this->_genUuid();
			\EE\Model\Promotion\Group::create( $bid, $uuid, $name, $visible, $routing, $order );
		} else {
			// update existing
			$id = \EE\Model\Promotion\Group::update( $bid, $id, $name, $visible, $routing, $order );
		}

		echo $id;
		return(false);
	}

	public function updateMenuTax() {
		return AjaxWrapper::process( array(
			AjaxWrapperField::bid(),
			new AjaxWrapperField( 'tax_name', AjaxWrapperField::FIELD_STRING, AjaxWrapperField::OPTIONS_NAME, 'Invalid tax name' ),
			new AjaxWrapperField( 'tax_percent', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPT_GTEZERO, 'Invalid tax percent' ),
			new AjaxWrapperField( 'id', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_NEWID, 'Invalid tax id' )
		), function( array $inputs ) {
			if( $inputs['id'] > 0 ) {
				$tax = \EE\Model\Menu\Tax::getById( $inputs['id'] );
				if( !$tax ) {
					throw new Exception( 'Tax id does not exist' );
				}
			} else {
				$tax = new \EE\Model\Menu\Tax();
				$tax->businessid = $inputs['bid'];
				$tax->pos_id = \EE\Model\Menu\Tax::nextPosId( $inputs['bid'] );
			}
			
			$tax->name = $inputs['tax_name'];
			$tax->percent = $inputs['tax_percent'];
			$tax->pos_processed = 0;
			$tax->save();
			\EE\Model\KioskSync::syncPromotionCache($tax->businessid);
		} );
	}

	public function updateMenuGroups() {
		return AjaxWrapper::process( array(
			AjaxWrapperField::bid(),
			new AjaxWrapperField( 'group_name', AjaxWrapperField::FIELD_STRING, AjaxWrapperField::OPTIONS_NAME, 'Invalid group name' ),
			new AjaxWrapperField( 'id', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_NEWID, 'Invalid group id' )
		), function( array $inputs ) {
			if( $inputs['id'] > 0 ) {
				$group = \EE\Model\Menu\GroupsNew::getById( $inputs['id'] );
				if( !$group ) {
					throw new Exception( 'Group id does not exist' );
				}
			} else {
				$group = \EE\Model\Menu\GroupsNew::create( $inputs['bid'] );
			}
			
			$group->name = $inputs['group_name'];
			$group->pos_processed = 0;
			$group->save();
		} );
	}

	public function updateCustomer() {
		$this->startAjax();

		$bid = htmlspecialchars( trim( $_POST['bid'] ) );
		$customerid = htmlspecialchars( trim( $_POST['customerid'] ) );
		$email = htmlspecialchars( trim( $_POST['email'] ) );
		$user_pass = htmlspecialchars( trim( $_POST['user_pass'] ) );
		$last_name = htmlspecialchars( trim( $_POST['last_name'] ) );
		$first_name = htmlspecialchars( trim( $_POST['first_name'] ) );
		$scancode = htmlspecialchars( trim( $_POST['scancode'] ) );
		$old_scancode = htmlspecialchars( trim( $_POST['old_scancode'] ) );
		
		$customer = \EE\Model\User\Customer::get( intval( $customerid ), true );
		if ( !$customer ) {
			$customer = new \EE\Model\User\Customer();
		}
		
		if ( ($customerid) && ($old_scancode != $scancode) ) {
			$user = $_SESSION->getUser( \EE\Session::TYPE_USER );
			$customer->username = $customer->username . '-' . $customer->customerid;
			$customer->last_name = $customer->last_name . ' [old]';
			$customer->first_name = $customer->first_name . ' [old]';
			$balance = $customer->balance;
#			$customer->balance = 0;
			$customer->is_deleted = 1;
			
#			$customer->save();
			\EE\Model\User\Customer\Transaction::db()->beginTransaction();
			\EE\Model\User\Customer\Transaction::processCustomerTransaction(
				//$customer->customerid
				$customer
				, \EE\Model\User\Customer\Transaction::TRANSACTION_TYPE_BALANCE_TRANSFER
				, -$balance
				, $user->username
				,false
			);
			
			$customer->customerid = 0; // essentially a new customer
			//$customer->balance = $balance;
			$customer->is_deleted = 0;
		}
		
		
		$customer->businessid = $bid;
		$customer->username = $email;
		$customer->last_name = $last_name;
		$customer->first_name = $first_name;
		$customer->scancode = $scancode;
		
		$customer->save();
		if ( ($customerid) && ($old_scancode != $scancode) ) {
			\EE\Model\User\Customer\Transaction::processCustomerTransaction(
				//$customer->customerid
				$customer
				, \EE\Model\User\Customer\Transaction::TRANSACTION_TYPE_BALANCE_TRANSFER
				, $balance
				, $user->username
				,false
			);
			\EE\Model\User\Customer\Transaction::db()->commit();
		}
		
		if ( $user_pass ) {
			\EE\Model\User\Customer::changePasswordByAdmin(
				$customer->customerid
				,$user_pass
			);
		}

		
/*
		if( $customerid > 0 ) {

				$query = "UPDATE customer SET pos_processed = 0, username = '$email', last_name = '$last_name', first_name = '$first_name', scancode = '$scancode' WHERE customerid = $customerid";
				$result = ProxyOld::query( $query );

		}
		else {
			$query = "INSERT INTO customer (pos_processed,username,password,businessid,scancode,first_name,last_name) VALUES (0,'$email','password',$bid,'$scancode','$first_name','$last_name')";
			$result = ProxyOld::query( $query );
		}
*/
		return false;
	}

	public function updateCustomerScancodes() {
		return AjaxWrapper::process( array(
			AjaxWrapperField::bid(),
			new AjaxWrapperField( 'scan_start', AjaxWrapperField::FIELD_STRING, AjaxWrapperField::OPT_STRINGNUMERIC, 'Invalid scancode start' ),
			new AjaxWrapperField( 'scan_end', AjaxWrapperField::FIELD_STRING, AjaxWrapperField::OPT_STRINGNUMERIC, 'Invalid scancode end' ),
			new AjaxWrapperField( 'scan_value', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPT_GTEZERO, 'Invalid balance value' ),
			new AjaxWrapperField( 'scan_id', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_NEWID, 'Invalid scancode range id' )
		), function( array $inputs ) {
			if( $inputs['scan_id'] > 0 ) {
				$sc = \EE\Model\User\Customer\Scancodes::getById( $inputs['scan_id'] );
				if( !$sc ) {
					throw new Exception( 'Scancode range id does not exist' );
				}
			} else {
				$sc = new \EE\Model\User\Customer\Scancodes();
				$sc->businessid = $inputs['bid'];
			}
			
			$sc->scan_start = $inputs['scan_start'];
			$sc->scan_end = $inputs['scan_end'];
			$sc->value = $inputs['scan_value'];
			$sc->save();
		} );
	}

	public function processCustomerTransaction() {
 		return AjaxWrapper::process( array(
 			new AjaxWrapperField( 'cust_id', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID, 'Invalid customer id' ),
 			new AjaxWrapperField( 'dol_value', AjaxWrapperField::FIELD_DECIMAL ),
 			new AjaxWrapperField( 'transtype', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_NEWID, 'Invalid transaction type' )
 		), function( array $inputs ) {
 			$reasons = array(
 				3 => 'refund',
 				4 => 'promo',
 				7 => 'balance transfer',
 				8 => 'CK Adjust'
 			);
 			
 			$reason = @$reasons[$inputs['transtype']] ?: '';
 			$audit = @$_COOKIE['usercook'] ?: '';
 			
 			$customer = \EE\Model\User\Customer::getById( $inputs['cust_id'] );
 			if( !$customer ) {
 				throw new Exception( 'Invalid customer id' );
 			}
 			
 			$trans = new \EE\Model\User\Customer\Transaction( array(
 				'oid' => $reason,
 				'trans_type_id' => $inputs['transtype'],
 				'customer_id' => $inputs['cust_id'],
 				'subtotal' => $inputs['dol_value'],
 				'chargetotal' => $inputs['dol_value'],
 				'response' => 'APPROVED',
 				'authresponse' => $audit
 			) );
 			$trans->save();
 			
 			$customer->balance += $inputs['dol_value'];
 			$customer->save();
 		} );
	}

	public function updateCustomerBalance()
	{
		if ($_SESSION['USER']['security_level'] < 9) {
			echo 'ERROR';
			return(false);
		}

		$bid = intval( trim( $_POST['bid'] ) );
		$customerid = intval(trim($_POST['cid']));
		$balance = number_format( trim( $_POST['cbalance'] ), 2, ".", "" );

		if( $customerid > 0 ) {
			$customer = \EE\Model\User\Customer::get( $customerid, true );

			if( !$customer )
			{
				echo "ERROR";
			}
			else
			{
				$customer->balance = $balance;
				$customer->save();
				echo "SUCCESS";
			}
		}

		return false;
	}

	public function menuCheckDetail() {
		$this->startAjax();

		$customerid = static::getPostVariable( 'customerid', $default = null );
		$date1 = static::getPostVariable( 'date1', $default = null );
		$date2 = static::getPostVariable( 'date2', $default = null );

		$customerid = htmlspecialchars( trim( $customerid ) );
		$date1 = htmlspecialchars( trim( $date1 ) );
		$date2 = htmlspecialchars( trim( $date2 ) );

		menu_items::check_detail( $customerid, $date1, $date2 );

		return false;
	}

	public function processMenuTax() {
		return AjaxWrapper::process( array(
			new AjaxWrapperField( 'groupid', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID, 'Invalid group id' ),
			new AjaxWrapperField( 'taxid', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID, 'Invalid tax id' ),
			new AjaxWrapperField( 'which', AjaxWrapperField::FIELD_ENUM, array( 1, 2 ), 'Invalid operation' )
		), function( array $inputs ) {
			$items = \EE\Model\Menu\ItemNew::getListByGroup( $inputs['groupid'], false, array( 'id', 'businessid' ) );
			$action = $inputs['which'] == 1 ? 'add' : 'remove';
			$counter = 0;
			foreach( $items as $item ) {
				\EE\Model\Menu\Item\Tax::modifyTax( $action, $item['id'], $inputs['taxid'] );
				\EE\Model\KioskSync::addSync( $item['businessid'], $item['id'], \EE\Model\KioskSync::TYPE_ITEM );
				\EE\Model\KioskSync::addSync( $item['businessid'], $item['id'], \EE\Model\KioskSync::TYPE_ITEM_TAX );
				$counter++;
			}
			\EE\Model\Menu\ItemNew::setGroupProcessed( $inputs['groupid'], 0 );
			
			return "Updated $counter items";
		}, AjaxWrapper::DATATYPE_TEXT );
	}
	
	public function rebootTerminal() {
		return AjaxWrapper::process( array(
				AjaxWrapperField::bid( ),
				new AjaxWrapperField( 'pid', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID, 'Invalid Program id' ),
		), function( array $inputs ) {
							
			$program_id = $inputs['pid'];
				
			$name = sprintf( 'aeris2@aeris%05d' , $program_id );
			$url = $name . ".vpn.essential-elements.net";
			$script = DOC_ROOT . '/../scripts/aeris2/reboot.sh';
			$exec = "$script $url 2>&1";
			$message=shell_exec($exec);
			
			$pieces = explode("\n", $message);
			if (in_array('success', $pieces)) {
				return array();
			}
			return array('error' => true, 'message' => $message);
		} );
	}

	
}


