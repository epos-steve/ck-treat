<?php

/*
 * Action Methods
 */

#error_reporting( error_reporting( ) ^ E_USER_WARNING );

use Essential\Treat\Db\ProxyOld;

# @TODO replace the Treat_ class with the EE class
use EE\Config as Treat_Config;
use EE\Model\Aeris\Modifier\Group as Treat_Model_Aeris_Modifier_Group;
use EE\Model\ClientPrograms as Treat_Model_ClientPrograms;
use Essential\Treat\Db\ProxyOld as DB_Proxy;
use Essential\Treat\Db\ProxyOld as Treat_DB_ProxyOld;
use Essential\Treat\Db\ProxyOld\ProcessHost as Treat_DB_ProxyOldProcessHost;

class Ta_Businessmenusmeta_ActionController
	extends \EE\Controller\Meta\Base
{

	public function index() {
		$this->menus();
	}




	public function posMenuLinking() {
		$business = \EE\Model\Business\Singleton::getSingleton();
		
		$loginuser = $_SESSION->getUser();
		$toAssign['user'] = $loginuser->username;
		$toAssign['pass'] = $loginuser->password;
		
		$toAssign['view1'] = $_POST["view1"];
		$toAssign['date1'] = $_POST["date1"];
		$toAssign['date2'] = $_POST["date2"];
		$toAssign['findinv'] = $_POST["findinv"];
		
		$toAssign['businessid'] = $businessid = $business->businessid;
		$toAssign['companyid'] = $companyid = $GLOBALS['companyid'];
		
		$toAssign['style'] = 'text-decoration: none;';
		
		$toAssign['groupid'] = $groupid = $_GET["groupid"];
		$toAssign['limit'] = $limit = $_GET["limit"] ? : 0;
		
		$query = "SELECT * FROM menu_groups_new WHERE businessid = '$businessid' AND parent_id IS NULL ORDER BY pos_id";
		$result = Treat_DB_ProxyOld::query( $query );
		$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );
		
		$groupList = array();
		
		$counter = 1;
		while( $r = mysql_fetch_array( $result ) ) {
			$pos_id = $r["id"];
			$group_name = $r["name"];
			
			if( $groupid == "" && $counter == 1 ) {
				$groupid = $pos_id;
			}
			
			$groupList[] = array(
				'pos_id' => $pos_id,
				'group_name' => $group_name,
				'style' => 'color: ' . ( $groupid == $pos_id ? 'red; font-weight: bold' : 'blue' ) . ';',
			);
			
			$counter = 2;
		}
		$toAssign['groupList'] = $groupList;
		
		$query2 = "SELECT menu_item_id,item_name FROM menu_items WHERE businessid = '$businessid' AND active = 0 ORDER BY item_name";
		$result2 = Treat_DB_ProxyOld::query( $query2 );
		$num2 = Treat_DB_ProxyOld::mysql_numrows( $result2, 0 );
		
		////total count
		$query = "SELECT COUNT(DISTINCT menu_items_new.id) AS totalcount FROM menu_items_new,menu_groups_new WHERE (menu_items_new.businessid = '$businessid' AND menu_items_new.group_id = '$groupid' AND menu_items_new.active = 0) OR (menu_items_new.businessid = '$businessid' AND menu_items_new.active = 0 AND menu_items_new.group_id = menu_groups_new.id AND menu_groups_new.parent_id = '$groupid')";
		$result = Treat_DB_ProxyOld::query( $query );
		
		$toAssign['totalcount'] = $totalcount = Treat_DB_ProxyOld::mysql_result( $result, 0, "totalcount", '' );
		
		/////list items
		if( $groupid == -1 ) {
			$query = "SELECT * FROM menu_items_new WHERE menu_items_new.businessid = '$businessid' AND menu_items_new.active = 0 AND menu_items_new.menu_itemid = '0' ORDER BY menu_items_new.name";
		}
		else {
			$query = "SELECT DISTINCT menu_items_new.id, menu_items_new.name,menu_items_new.menu_itemid FROM menu_items_new,menu_groups_new WHERE (menu_items_new.businessid = '$businessid' AND menu_items_new.group_id = '$groupid' AND menu_items_new.active = 0) OR (menu_items_new.businessid = '$businessid' AND menu_items_new.active = 0 AND menu_items_new.group_id = menu_groups_new.id AND menu_groups_new.parent_id = '$groupid') ORDER BY menu_items_new.name LIMIT $limit,20";
		}
		$result = Treat_DB_ProxyOld::query( $query );
		$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );
		
		$itemList = array();
		
		$counter = 1;
		$counter2 = 1;
		while( $r = mysql_fetch_array( $result ) ) {
			$id = $r["id"];
			$item_name = $r["name"];
			$menu_itemid = $r["menu_itemid"];
			
			$item = $r;
			
			if( $counter == 1 )
				$item['tr'] = true;
			
			$item['color'] = ( $counter2 == 1 || $counter2 == 4 ) ? '#E8E7E7' : 'white';
			
			$item['options'] = array();
			while( $r2 = mysql_fetch_array( $result2 ) ) {
				$opt = $r2;
				
				if( $menu_itemid == $menu_item_id ) {
					$opt['sel'] = true;
				}
				
				$item['options'][] = $opt;
			}
			
			mysql_data_seek( $result2, 0 );
			
			$counter++;
			$counter2++;
			if( $counter == 3 ) {
				$item['str'] = true;
				$counter = 1;
			}
			if( $counter2 == 5 ) {
				$counter2 = 1;
			}
			
			$itemList[] = $item;
		}
		if( $counter == 2 ) {
			$toAssign['closeTr'] = true;
		}
		$toAssign['itemList'] = $itemList;
		
		$toAssign['showlimit'] = $showlimit = $limit + 1;
		$toAssign['last'] = $last = $limit + $num;
		
		if( $limit > 0 ) {
			$toAssign['prevlimit'] = $prevlimit = $limit - 20;
			$toAssign['showPrev'] = true;
		}
		
		if( $num == 20 ) {
			$toAssign['nextlimit'] = $nextlimit = $limit + 20;
			$toAssign['showNext'] = true;
		}
		
		foreach( $toAssign as $var => $val ) {
			$this->_view->assign( $var, $val );
		}
	}
	
	public function posMultiEditnew() {
		$this->_view->assign( 'title', 'POS Menu' );
		
		$this->addJavascriptAtTop( '/assets/js/ta/posMultiEdit.js' );
		$this->addJavascriptAtTop( '/assets/js/jquery.editable.js' );
		$this->addCss( '/assets/css/preload/preLoadingMessage.css' );
		
		$user = $_SESSION->getUser();
		$bus_menu_pos = $user['bus_menu_pos'];
		$this->_view->assign( 'bus_menu_pos', $bus_menu_pos );
		
		$active = $_GET["active"];
		
		$ordertype = static::getPostOrGetVariable( 'ordertype', '' );
		
		$this->_view->assign( 'ordertype_selected', $ordertype );
		$this->_view->assign( 'active', $active );
	}
	
	public function posSaveAjax() {
		$bid = static::getPostOrGetVariable( 'bid', '' );
		$value = static::getPostOrGetVariable( 'value', '' );
		
		$id_parts = array();
		$id_parts = explode( '|', static::getPostOrGetVariable( 'id', '' ) );
		$id = $id_parts[1];
		
		switch( $id_parts[0] ) {
			case 'item_code':
				$query = "UPDATE menu_items_new SET item_code = '$value' WHERE id = $id";
				$ret_value = $value;
				break;
			case 'group':
				$query = "UPDATE menu_items_new SET group_id = '$value' WHERE id = $id";
				
				$query_group = "SELECT name FROM menu_groups_new WHERE businessid = $bid and id = $value";
				$result_group = DB_Proxy::query( $query_group );
				
				$r = mysql_fetch_array( $result_group );
				$ret_value = $r['name'];
				break;
			case 'ordertype':
				$query = "UPDATE menu_items_new SET ordertype = '$value' WHERE id = $id";
				
				$query_order = "SELECT name FROM menu_items_ordertype where id = $value";
				$result_order = DB_Proxy::query( $query_order );
				
				$r = mysql_fetch_array( $result_order );
				$ret_value = $r['name'];
				break;
			case 'active':
				$query = "UPDATE menu_items_new SET active = '$value' WHERE id = $id";
				$ret_value = $value;
				break;
			case 'upc':
				$query = "UPDATE menu_items_new SET upc = '$value' WHERE id = $id";
				$ret_value = $value;
				break;
			case 'max':
				$query = "UPDATE menu_items_new SET max = '$value' WHERE id = $id";
				$ret_value = $value;
				break;
			case 'reorder_point':
				$query = "UPDATE menu_items_new SET reorder_point = '$value' WHERE id = $id";
				$ret_value = $value;
				break;
			case 'reorder_amount':
				$query = "UPDATE menu_items_new SET reorder_amount = '$value' WHERE id = $id";
				$ret_value = $value;
				break;
			case 'tax':
				$tax_id = $id_parts[2];
				
				$query = "SELECT menu_itemid,tax_id FROM menu_item_tax WHERE menu_itemid = $id AND tax_id = $tax_id";
				
				$query_tax_name = "SELECT name,percent FROM menu_tax WHERE businessid = " . $_GET['bid'] . " and id = $tax_id";
				$result_tax_name = Treat_DB_ProxyOld::query( $query_tax_name );
				
				$res = mysql_fetch_array( $result_tax_name );
				
				$result = DB_Proxy::query( $query );
				
				if( mysql_num_rows( $result ) > 0 ) { //tax record exists
					if( $value == 0 ) { //delete tax record
						$query_taxes = "DELETE FROM menu_item_tax WHERE menu_itemid = $id and tax_id = $tax_id";
						$result_taxes = Treat_DB_ProxyOld::query( $query_taxes );
						
						if( mysql_affected_rows() > 0 ) {
							$ret_value = '<strong>inactive</strong> - ' . $res['name'] . ' (' . $res['percent'] . '%)';
						}
						else {
							$ret_value = 'Error deleting';
						}
					}
					else {
						$ret_value = '<strong>active</strong> - ' . $res['name'] . ' (' . $res['percent'] . '%)';
					}
				}
				else { //tax record doesnt exist
					if( $value == 1 ) { //insert tax record
						$uuid = \EE\Model\Menu\Item::genUuid();
						$query_taxes = "INSERT INTO menu_item_tax (menu_itemid,tax_id, uuid_menu_item_tax) VALUES ($id,$tax_id, '$uuid')";
						$result_taxes = Treat_DB_ProxyOld::query( $query_taxes );
						
						if( mysql_affected_rows() > 0 ) {
							$ret_value = '<strong>active</strong> - ' . $res['name'] . ' (' . $res['percent'] . '%)';
						}
						else {
							$ret_value = 'Error inserting';
						}
					}
					else {
						$ret_value = '<strong>inactive</strong> - ' . $res['name'] . ' (' . $res['percent'] . '%)';
					}
				}
				break;
			case 'cost':
				$query = "UPDATE menu_items_price SET cost = '$value' WHERE menu_item_id = $id";
				$ret_value = $value;
				break;
			case 'new_price':
				$query = "UPDATE menu_items_price SET new_price = '$value' WHERE menu_item_id = $id";
				$ret_value = $value;
				break;
		}
		
		if( $id_parts[0] != 'tax' ) {
			$result = DB_Proxy::query( $query );
		}
		
		Treat_Model_KioskSync::addSync( $bid, $id, Treat_Model_KioskSync::TYPE_ITEM );
		Treat_Model_KioskSync::addSync( $bid, $id, Treat_Model_KioskSync::TYPE_ITEM_TAX );
		
		
		print $ret_value;
		
		return false;
	}

	public function posMultiEdit() {
		$this->_view->assign( 'title', 'POS Menu' );

		$this->addJavascriptAtTop( '/assets/js/ta/posMultiEdit.js' );
		$this->addJavascriptAtTop( '/assets/js/ta/menu_items.js' );
		$this->addJavascriptAtTop( '/assets/js/ta/posMenu.js' );
		$this->addJavascriptAtTop( '/assets/js/jquery.form.js' );

		$user = $_SESSION->getUser();
		$bus_menu_pos = $user['bus_menu_pos'];
		$this->_view->assign( 'bus_menu_pos', $bus_menu_pos );

		$showall = $_GET["showall"];

		if( $showall > 0 ) {
			if( $showall != 1 ) {
				$showall = 0;
			}
			setcookie( "showall", "$showall" );
		}
		else {
			$showall = isset( $_COOKIE["showall"] ) ? $_COOKIE["showall"] : '';
		}
		$this->_view->assign( 'showall', $showall );

		$businessid = intval($GLOBALS['businessid']);

		///get machine numbers tied to this business
		$query = "SELECT machine_num FROM machine_bus_link WHERE businessid = $businessid";
		$result = Treat_DB_ProxyOld::query($query);

		$machine_num = array();
		$counter=0;
		while( ($r = Treat_DB_ProxyOld::mysql_fetch_object($result)) ) 
		{
			$machine_num[$counter] = $r->machine_num;
			$counter++;
		}
		$this->_view->assign('counter', $counter);
		$this->_view->assign('machine_num', $machine_num);


		/////Create Manufacturer Array
		/*
		$manufacturers = array();

		$query = "SELECT * FROM manufacturer ORDER BY name";
		$result = Treat_DB_ProxyOld::query($query);

		while($r = Treat_DB_ProxyOld::mysql_fetch_array($result)){
			$id = $r["id"];
			$name = $r["name"];

			$manufacturers[$id] = $name;
		}
		 */

		/////create groups array
		$groups = array();

		$menu_groups_new = \EE\Model\Menu\GroupsNew::getListByBusinessid($businessid);
		foreach ( $menu_groups_new as $menu_group )
		{
			$groups[ $menu_group['id'] ] = $menu_group['name'];
		}
		$this->_view->assign('groups', $groups);

		///taxes new
		$tax_rates = array();
		$menu_taxes = \EE\Model\Menu\Tax::getListByBusinessid($businessid);
		foreach ( $menu_taxes as $menu_tax )
		{
			$tax_rates[] = array(
				'tax_id' => $menu_tax->id,
				'tax_name' => $menu_tax->name,
				'tax_percent' => $menu_tax->percent,
			);
		}
		$this->_view->assign('tax_rates', $tax_rates);

		/////create ordertype array
		$ordertypes = array();

		$menu_items_ordertypes = \EE\Model\Menu\Item\OrderType::getAll('id', false);
		foreach ( $menu_items_ordertypes as $order_type)
		{
			$ordertypes[ $order_type->id ] = $order_type->name;
		}
		$this->_view->assign('ordertypes', $ordertypes);


		$menu_items = array();
		$menu_items_new = Treat_Model_Menu_Item_New::getActiveItemListForBusiness($businessid, 'ordertype, name', ( $showall == 1 ));
		foreach ( $menu_items_new as $menu_item )
		{
			$menu_items[ $menu_item->ordertype ][] = array(
				'item' => $menu_item,
				'taxes' => Treat_Model_Menu_Item_Tax::getColumn('tax_id', "menu_itemid = {$menu_item->id}"),
			);
		}
		$this->_view->assign('menu_items', $menu_items);
	}
	
	public function posItem()
	{
		$this->_layout = '';
		$businessid = static::getPostOrGetVariable( 'bid', false );

		if ( !$businessid )
		{
			header('content-type: application/json');
			echo json_encode(array('error' => true, 'content' => 'Business id parameter does not exist.'));
			return false;
		}

		if ( strcasecmp($_SERVER['REQUEST_METHOD'], 'post') === 0 )
		{
			$changes = static::getPostOrGetVariable('changes', false);
			if ( ! $changes )
			{
				$id = intval(static::getPostOrGetVariable('id', 0));
				$fieldName = static::getPostOrGetVariable('field_name', '');
				$value = static::getPostOrGetVariable('value', '');

				$changes = array(
					$id => array(
						$fieldName => array(
							'value' => $value,
						)
					)
				);
			}

			$updates = array();
			foreach ($changes as $id => $fields)
			{
				$id = intval($id);

				$updates[$id] = array(
					'updates' => array(
						'queued' => count($fields),
						'completed' => 0,
					),
					'errors' => array(),
					'fields' => array(),
				);

				$doKioskSync = false;

				$item = Treat_Model_Menu_Item_New::get("`id` = {$id}", true);

				if ( !$item || !($item instanceof Treat_Model_Menu_Item_New) )
				{
					// This should never ever be sent as each menu item should
					// already exist. However, errors happen and need to
					$updates[$id]['errors'][] = 'Item does not exist or could not be found.';
					continue;
				}

				// Get Price
				$updatePrice = false;
				$price = Treat_Model_Menu_Item_Price::get("`menu_item_id` = {$id}", true);
				if ( ! $price || !($price instanceof Treat_Model_Menu_Item_Price ) )
				{
					$price = new Treat_Model_Menu_Item_Price;
					$price->menu_item_id = $id;
					$price->businessid = $businessid;
					$price->pos_price_id = 1;
				}

				$item_fields = array();

				foreach ($fields as $field => $data)
				{
					$value = $data['value'];
					switch($field)
					{
						case 'new_price':
							$doKioskSync = true;
						case 'cost':
							$oldValue = $price->{$field};
							$price->{$field} = doubleval($value);
							$updates[$id]['fields'][$field] = array(
								'didSave' => false,
								'value' => array(
									'old' => $oldValue,
									'new' => $price->{$field},
								),
							);
							$updatePrice = true;
							break;


						case 'tax_id':
							$tax_id = intval($value);
							$checked = !!$data['checked'];
							$tax = Treat_Model_Menu_Item_Tax::get("`menu_itemid` = {$id} AND `tax_id` = {$tax_id}", true);

							$oldValue = '0';
							if ($tax instanceof Treat_Model_Menu_Item_Tax)
							{
								$oldValue = '1';
							}

							$didSave = false;
							if ($checked && !$tax)
							{
								$new_tax = new Treat_Model_Menu_Item_Tax;
								$new_tax->menu_itemid = $id;
								$new_tax->tax_id = $tax_id;
								$didSave = $new_tax->save();
								$doKioskSync = true;
							}
							else if ( !$checked && $tax instanceof Treat_Model_Menu_Item_Tax )
							{
								$didSave = $tax->delete();
								$doKioskSync = true;
							}

							$updates[$id]['fields']['tax_id'] = array(
								'didSave' => (bool) $didSave,
								'value' => array(
									'old' => $oldValue,
									'new' => (int) !!!$oldValue,
								),
							);

							if ( $didSave )
							{
								$updates[$id]['updates']['completed']++;
							}
							break;


						case 'active':
							// Active is a checkbox and needs additional code to get it
							// to work correctly when inactive.
							$checked = !!$data['checked'];
							$value = $checked ? 1 : 0;
							$doKioskSync = true;
						case 'group_id': // Fallthrough
						case 'ordertype': // Fallthrough
						case 'max': // Fallthrough
						case 'reorder_point': // Fallthrough
						case 'reorder_amount':
							$item_fields[] = $field;
							// Everything above are integers, sanitize.
							$value = intval($value);
							$oldValue = $item->{$field};
							$item->{$field} = $value;
							$updates[$id]['fields'][$field] = array(
								'didSave' => false,
								'value' => array(
									'old' => $oldValue,
									'new' => $item->{$field},
								),
							);
							break;
						case 'item_code': // Fallthrough : string
						case 'upc': // Fallthrough : string
							$item_fields[] = $field;
							$oldValue = $item->{$field};

							$checkUnique = Treat_Model_Menu_Item_New::get(
								sprintf(
									"`%s` = '%s' AND `businessid` = %d", $field,
									mysql_real_escape_string($value),
									$businessid
								),
								true
							);

							$updates[$id]['fields'][$field] = array(
								'didSave' => false,
								'value' => array(
									'old' => $oldValue,
									'new' => null,
								),
							);

							if( $checkUnique && !empty($checkUnique->id) && intval($checkUnique->id) !== intval($id))
							{
								$updates[$id]['errors'][] = sprintf('%s is not unique. Reverted change.', ucfirst($field));
							}
							else
							{
								$item->{$field} = $value;
								$updates[$id]['fields'][$field]['value']['new'] = $item->{$field};
								$doKioskSync = true;
							}
							break;
						case 'ebt_fs_allowed': // Fallthrough : string
								$item_fields[] = $field;
								$oldValue = $item->{$field};
								$checked = !!$data['checked'];
							$value = $checked ? 1 : 0;
							$item->{$field} = $value;
							$doKioskSync = true;
							
							$updates[$id]['fields'][$field] = array(
								'didSave' => false,
								'value' => array(
									'old' => $oldValue,
									'new' => $value,
								),
							);
							
							break;
						default:
							$updates[$id]['errors'][] = "Couldn't save, missing information.";
					}
				}

				if ( $updatePrice )
				{
					try {
						$didSave = (bool) $price->save();
						
						$loginuser = $_SESSION->getUser();
						$audit_event_message = '';
						$audit_event_message .= $loginuser->username . ' - Multi Item Edit - ' . ' (application/controllers/ta/businessmenusmeta/action.php - posItem - Line 587)';
						
						$query22 = "insert into menu_items_price_audit (businessid, menu_item_id, pos_price_id, price, new_price, cost, audit_event, audit_datetime) values ($price->businessid, $price->menu_item_id, $price->pos_price_id, $price->price, $price->new_price, $price->cost, '" . $audit_event_message . "', now())";
						Treat_DB_ProxyOld::query( $query22 );
						
						foreach (array('cost', 'new_price') as $fieldName)
						{
							if (isset($updates[$id]['fields'][$fieldName]))
							{
								$updates[$id]['fields'][$fieldName]['didSave'] = $didSave;
							}
							if ($didSave && isset($updates[$id]['fields'][$fieldName]))
							{
								$updates[$id]['updates']['completed']++;
							}
						}
					} catch(\PDOException $e) {
						$error = "Couldn't automatically save, click save at bottom.";
						if (!IN_PRODUCTION) {
							$error = $e->getMessage();
						}
						$updates[$id]['errors'][] = $error;
					}
				}
				try {
					$didSave = $item->save();
					foreach ($item_fields as $fieldName)
					{
						if (isset($updates[$id]['fields'][$fieldName]))
						{
							$updates[$id]['fields'][$fieldName]['didSave'] = $didSave;
							if ($didSave)
							{
								$updates[$id]['updates']['completed']++;
							}
						}
					}
				} catch(\PDOException $e) {
					$error = 'Couldn\'t automatically save, please refresh and try again.';
					if (!IN_PRODUCTION) {
						$error = $e->getMessage();
					}
					$updates[$id]['errors'][] = $error;
				}

				if ($doKioskSync)
				{
					Treat_Model_KioskSync::addSync( $businessid, $id, Treat_Model_KioskSync::TYPE_ITEM );
					Treat_Model_KioskSync::addSync( $businessid, $id, Treat_Model_KioskSync::TYPE_ITEM_TAX );
				}
			}

			header('content-type: application/json');
			echo json_encode($updates);

			return false;
		}

		// Default is GET method.
		$showall = static::getPostOrGetVariable( 'showall', false );
		$ordertypeid = static::getPostOrGetVariable('ordertype_id', false);
		if ($ordertypeid === '')
		{
			$ordertypeid = null;
		}
		else if ( !!$ordertypeid )
		{
			$ordertypeid = intval($ordertypeid);
		}
		$menu_item_id = intval(static::getPostOrGetVariable('menu_item_id', 0));

		if($menu_item_id > 0)
		{
			$menu_items_new = Treat_Model_Menu_Item_New::getById($menu_item_id);
			$menu_items_price = Treat_Model_Menu_Item_Price::get("menu_item_id = {$menu_item_id}", true);

			$menu_items_new['menu_items_price_id'] = $menu_items_price->id;
			$menu_items_new['cost'] = $menu_items_price->cost;
			$menu_items_new['new_price'] = $menu_items_price->new_price;
			$menu_items_new['price'] = $menu_items_price->price;

			$taxes = Treat_Model_Menu_Item_Tax::getColumn('tax_id', "menu_itemid = {$menu_item_id}");

			header('content-type: application/json');
			echo json_encode(array(
				'error' => false,
				'content' => array(
					'item' => $menu_items_new,
					'taxes' => $taxes
				)
			));
		}
		else if( $ordertypeid > 0 )
		{
			$menu_items = array();
			$menu_items_new = Treat_Model_Menu_Item_New::getOrderTypeItemListForBusiness(
				$businessid,
				$ordertypeid,
				'ordertype, name',
				( $showall == 1 )
			);
			foreach ( $menu_items_new as $menu_item )
			{
				$menu_items[ $menu_item->ordertype ][] = array(
					'item' => $menu_item,
					'taxes' => Treat_Model_Menu_Item_Tax::getColumn('tax_id', "menu_itemid = {$menu_item->id}"),
				);
			}

			header('content-type: application/json');
			echo json_encode(array('error' => false, 'content' => $menu_items));
		}
		else
		{
			$menu_items = array();
			$menu_items_new = Treat_Model_Menu_Item_New::getActiveItemListForBusiness(
				$businessid,
				'ordertype, name',
				( $showall == 1 )
			);
			foreach ( $menu_items_new as $menu_item )
			{
				$menu_items[ $menu_item->ordertype ][] = array(
					'item' => $menu_item,
					'taxes' => Treat_Model_Menu_Item_Tax::getColumn('tax_id', "menu_itemid = {$menu_item->id}"),
				);
			}

			header('content-type: application/json');
			echo json_encode(array('error' => false, 'content' => $menu_items));
		}
		return false;
	}
	
	public function posSaveMulti() {
		$this->_layout = '';
		
		$businessid = $_POST["businessid"];
		$companyid = $_POST["companyid"];
		$showall = isset( $_COOKIE["showall"] ) ? $_COOKIE["showall"] : '';
		
		if( $showall == 1 ) {
			$query = "SELECT * FROM menu_items_new WHERE businessid = $businessid ORDER BY ordertype,name";
		}
		else {
			$query = "SELECT * FROM menu_items_new WHERE businessid = $businessid AND active = 0 ORDER BY ordertype,name";
		}
		
		$result = DB_Proxy::query( $query );
		
		while( $r = mysql_fetch_array( $result ) ) {
			$id = $r["id"];
			
			$ordertype = $_POST["ordertype$id"];
			$group = $_POST["group$id"];
			$item_code = $_POST["item_code$id"];
			$active = $_POST["active$id"];
			$upc = $_POST["upc$id"];
			$max = $_POST["max$id"];
			$reorder_point = $_POST["reorder_point$id"];
			$reorder_amount = $_POST["reorder_amount$id"];
			$new_price = $_POST["new_price$id"];
			$cost = $_POST["cost$id"];
			
			$active = ( empty( $active ) ) ? 0 : 1;
			
			$query2 = "UPDATE menu_items_new SET
ordertype = $ordertype,
group_id = $group,
pos_processed = 0,
item_code = '$item_code',
active = $active,
upc = '$upc',
max = '$max',
reorder_point = '$reorder_point',
reorder_amount = '$reorder_amount'
WHERE id = $id";
			
			$result2 = DB_Proxy::query( $query2 );
			if( mysql_affected_rows( $result2 ) > 0 ) {
				Treat_Model_KioskSync::addSync( $businessid, $id, Treat_Model_KioskSync::TYPE_ITEM );
				Treat_Model_KioskSync::addSync( $businessid, $id, Treat_Model_KioskSync::TYPE_ITEM_TAX );
			}
			
			if( $new_price != "" ) {
				$query2 = "UPDATE menu_items_price SET
new_price = '$new_price'
WHERE menu_item_id = $id";
				$result2 = DB_Proxy::query( $query2 );
				Treat_Model_KioskSync::addSync( $businessid, $id, Treat_Model_KioskSync::TYPE_ITEM );
				Treat_Model_KioskSync::addSync( $businessid, $id, Treat_Model_KioskSync::TYPE_ITEM_TAX );
			}
			
			///cost
			$query2 = "UPDATE menu_items_price SET
	cost = '$cost'
	WHERE menu_item_id = $id";
			$result2 = DB_Proxy::query( $query2 );
			
			
			///taxes
			/* Scott 5-31-12 -- delete all tax info for id */
			$query_taxes = "DELETE FROM menu_item_tax WHERE menu_itemid = $id";
			$result_taxes = Treat_DB_ProxyOld::query( $query_taxes );
			/* Scott 5-31-12 */
			
			/* Scott 3-30-12 */
			$query_tax1 = "SELECT * FROM menu_tax WHERE businessid = $businessid ORDER BY pos_id";
			$result_tax1 = Treat_DB_ProxyOld::query( $query_tax1 );
			
			$tax_index = 0;
			
			while( $r_tax1 = mysql_fetch_array( $result_tax1 ) ) {
				$tax_id = $r_tax1["id"];
				
				if( !isset( $_POST["tax$id"][$tax_index] ) ) {
					$has_tax = 0;
				}
				else {
					$has_tax = $_POST["tax$id"][$tax_index];
				}
				
				/*if($has_tax > 0){
				$query_tax2 = "INSERT INTO menu_item_tax (menu_itemid,tax_id) VALUES ($id,$tax_id)";
				$result_tax2 = Treat_DB_ProxyOld::query( $query_tax2 );
				// Add to the sync table for the item tax
				Treat_Model_KioskSync::addSync($businessid, $id, Treat_Model_KioskSync::TYPE_ITEM);
				Treat_Model_KioskSync::addSync($businessid, $id, Treat_Model_KioskSync::TYPE_ITEM_TAX);
				}
				else{
				$query_tax2 = "DELETE FROM menu_item_tax WHERE menu_itemid = $id and tax_id = $tax_id";
				$result_tax2 = Treat_DB_ProxyOld::query( $query_tax2 );
				Treat_Model_KioskSync::addSync($businessid, $id, Treat_Model_KioskSync::TYPE_ITEM_TAX);
				}*/
				
				if( $has_tax > 0 && $tax_id == $_POST["tax$id"][$tax_index] ) {
					$uuid = \EE\Model\Menu\Item::genUuid();
					$query_tax2 = "INSERT INTO menu_item_tax (menu_itemid,tax_id, uuid_menu_item_tax) VALUES ($id,$tax_id, '$uuid')";
					$result_tax2 = Treat_DB_ProxyOld::query( $query_tax2 );
					// Add to the sync table for the item tax
					Treat_Model_KioskSync::addSync( $businessid, $id, Treat_Model_KioskSync::TYPE_ITEM );
					Treat_Model_KioskSync::addSync( $businessid, $id, Treat_Model_KioskSync::TYPE_ITEM_TAX );
					$tax_index++;
				}
			}
			/* Scott 3-30-12 */
		}
		
		header( 'Location: ' . $_SERVER['HTTP_REFERER'] );
		return false;
	}
	
	public function posMenu() {
		$user = $_SESSION->getUser();
		$bus_menu_pos = $user['bus_menu_pos'];
		$this->_view->assign( 'bus_menu_pos', $bus_menu_pos );
		
		$redirect = $this->_uri->getPath() . '?' . $_SERVER['QUERY_STRING'];
		$this->_view->assign( 'redirect', $redirect );
		
		// set access levels for view
		switch( $bus_menu_pos ) {
			case 1:
				$bmp_access = array(
					'save_access' => 0, 'disable_reorder' => 1, 'disable' => 1, 'copy_all_like' => 0, 'master_menu_access' => 0
				);
				$disabled = 1;
				break;
			case 2:
				$bmp_access = array(
					'save_access' => 1, 'disable_reorder' => 0, 'disable' => 1, 'copy_all_like' => 0, 'master_menu_access' => 0
				);
				$disabled = 1;
				break;
			case 3:
				$bmp_access = array(
					'save_access' => 1, 'disable_reorder' => 0, 'disable' => 0, 'copy_all_like' => 0, 'master_menu_access' => 0
				);
				$disabled = 0;
				break;
			case 4:
				$bmp_access = array(
					'save_access' => 1, 'disable_reorder' => 0, 'disable' => 0, 'copy_all_like' => 1, 'master_menu_access' => 1
				);
				$disabled = 0;
				break;
			default:
				$bmp_access = array(
					'save_access' => 0, 'disable_reorder' => 1, 'disable' => 1, 'copy_all_like' => 0, 'master_menu_access' => 0
				);
				$disabled = 1;
				break;
		}
		
		$this->_view->assign( 'bmp_access', $bmp_access );
		
		
		$business = \EE\Model\Business\Singleton::getSingleton();
		
		$this->addJavascriptAtTop( '/assets/js/jquery.form.js' );
		$this->addJavascriptAtTop( '/assets/js/ta/posMenu.js' );
		$this->addCss( '/assets/css/ta/buscatermenu4.css' );
		
		///search
		$menu_items_new = '';
		$search_for = null;
		$search_what = null;
		$show_bad_search = null;
		$ordertype = null;
		$cond_group = null;
		$force_condiment = null;
		$shelf_life_days = null;
		$is_button = null;
		$mip_id = null;
		$tare = null;
		$items_sold = null;
		$show_active_check = null;
		$show_button_check = null;
		$sel1 = null;
		$sel2 = null;
		$sel3 = null;
		$skip_limit = null;

		$editid = $GLOBALS['editid'];
		
		$search = @$_POST["search"];
		if( $search != 1 ) {
			$search = @$_GET["search"];
			$skip_limit = 1;
			$limit = 0;
		}
		
		if( $search == 1 ) {
			$search_what = static::getPostVariable( 'search_what', '' );
			if( $search_what == "" ) {
				$search_what = $_GET["search_what"];
			}
			
			$search_for = static::getPostVariable( 'search_for', '' );
			if( $search_for == "" ) {
				$search_for = $_GET["search_for"];
				$search_for = str_replace( "%20", "", $search_for );
			}
			
			if( $skip_limit != 1 ) {
				$limit = $_POST["limit"];
				$query = "SELECT id FROM menu_items_new WHERE businessid = {$GLOBALS['businessid']} AND $search_what LIKE '%$search_for%' ORDER BY group_id,name LIMIT $limit,1";
				$result = Treat_DB_ProxyOld::query( $query );
				
				$search_num = mysql_num_rows( $result );
				
				$editid = @mysql_result( $result, 0, 'id' );
				$limit++;
				
				if( $search_num == 0 && $limit > 1 ) {
					$show_bad_search = "<font color=red> - End of List</font>";
					$limit = 0;
				}
				elseif( $search_num == 0 ) {
					$show_bad_search = "<font color=red> - No Results</font>";
					$limit = 0;
				}
			}
		}
		else {
			$limit = 0;
		}
		
		$showall = @$_GET['showall'];
		
		if( $showall == 1 ) {
			$subquery = "";
		}
		elseif( $showall == 2 ) {
			$subquery = "AND is_button = 1";
		}
		else {
			$subquery = "AND active = 0";
		}
		
		if( $search == 1 ) {
			$query2 = "SELECT * FROM menu_items_new WHERE businessid = {$GLOBALS['businessid']} AND $search_what LIKE '%$search_for%' ORDER BY group_id,name";
		}
		else {
			$query2 = "SELECT * FROM menu_items_new WHERE businessid = '{$GLOBALS['businessid']}' $subquery ORDER BY group_id,name";
		}
		$result2 = Treat_DB_ProxyOld::query( $query2 );
		
		$lastgroup = 0;
		while( $r = mysql_fetch_array( $result2 ) ) {
			$id = $r["id"];
			$name = $r["name"];
			$item_code = $r["item_code"];
			$upc = $r["upc"];
			$group_id = $r["group_id"];
			$active = $r["active"];
			
			if( $active == 1 ) {
				$color = "#E8E7E7";
			}
			elseif( $item_code == "" || $upc == "" || $group_id == 0 ) {
				$color = "yellow";
			}
			else {
				$color = "white";
			}
			
			if( $editid == $id ) {
				$sel = 'selected="selected"';
			}
			else {
				$sel = '';
			}
			
			if( $lastgroup != $group_id ) {
				if( $lastgroup != 0 ) {
					$menu_items_new .= '</optgroup>';
				}
				
				$query3 = "SELECT name FROM menu_groups_new WHERE id = '$group_id'";
				$result3 = Treat_DB_ProxyOld::query( $query3 );
				
				$group_name = @mysql_result( $result3, 0, 'name' );
				
				$menu_items_new .= '<optgroup label="' . $group_name . '">';
			}
			
			//////search update
			$search_for2 = str_replace( " ", "%20", $search_for );
			
			$menu_items_new .= '<option value="posMenu?search=' . $search . '&amp;search_what=' . $search_what . '&amp;search_for='
				. $search_for2 . '&amp;bid=' . $GLOBALS['businessid'] . '&amp;cid=' . $GLOBALS['companyid'] . '&amp;id=' . $id
				. '" ' . $sel . ' style="background-color:' . $color . '">' . $name . '</option>';
			
			$lastgroup = $group_id;
		}
		
		$this->_view->assign( 'limit', $limit );
		$this->_view->assign( 'show_bad_search', $show_bad_search );
		$this->_view->assign( 'search_for', $search_for );
		
		$menu_items_new .= '</optgroup>';
		
		$this->_view->assign( 'menu_items_new', $menu_items_new );
		
		////active / all items
		$style = 'text-decoration: none;';
		
		$showactive = sprintf( 
				'
		<br><div style="display:inline;">
		<a href="posMenu?bid=%d&cid=%d&showall=%%d" style="%s">
		<font size=1 color=blue>%%s</font>
		</a>
		</div>
		', $GLOBALS['businessid'], $GLOBALS['companyid'], $style );
		
		if( $showall == 1 || $showall == 2 ) {
			$showactive = sprintf( $showactive, 0, '[ACTIVE ITEMS]' );
		}
		else {
			$showactive = sprintf( $showactive, 1, '[ALL ITEMS]' );
		}
		
		$this->_view->assign( 'showactive', $showactive );
		
		////show buttons
		$showbuttons = " <a href=posMenu?bid={$GLOBALS['businessid']}&cid={$GLOBALS['companyid']}&showall=2 style='$style'><font color=blue size=1>[BUTTONS]</font></a>";
		$this->_view->assign( 'showbuttons', $showbuttons );
		
		
		
		///manage items
		if( $search != 1 ) {
			$editid = @$_GET["id"];
		}
		$new = null;
		if( $editid > 0 ) {
			$query3 = sprintf( 
					'
SELECT
mni.pos_id
,mni.manufacturer
,mni.name
,mni.group_id
,mni.ordertype
,mni.cond_group
,mni.force_condiment
,mni.active
,mni.item_code
,mni.upc
,mni.max
,mni.reorder_point
,mni.reorder_amount
,mni.shelf_life_days
,mni.is_button
,mbl.machine_num
,mip.id AS mip_id
,mip.price AS kiosk_price
,mip.cost
,mip.new_price AS new_price
,mni.sale_unit
,mni.tare
,mni.ebt_fs_allowed
FROM menu_items_new mni
LEFT JOIN machine_bus_link mbl
ON mni.businessid = mbl.businessid
LEFT JOIN menu_items_price mip
ON mni.id = mip.menu_item_id
WHERE mni.id = \'%d\'
', intval( $editid ) );
			$result3 = Treat_DB_ProxyOld::query( $query3 );
			
			$machine_num = Treat_DB_ProxyOld::mysql_result( $result3, 0, 'machine_num' );
			$pos_id = Treat_DB_ProxyOld::mysql_result( $result3, 0, 'pos_id' );
			//$manufacturer = mysql_result( $result3, 0, 'manufacturer' );
			$item_name = Treat_DB_ProxyOld::mysql_result( $result3, 0, 'name' );
			$group_id = Treat_DB_ProxyOld::mysql_result( $result3, 0, 'group_id' );
			$ordertype = Treat_DB_ProxyOld::mysql_result( $result3, 0, 'ordertype' );
			$cond_group = Treat_DB_ProxyOld::mysql_result( $result3, 0, 'cond_group' );
			$force_condiment = Treat_DB_ProxyOld::mysql_result( $result3, 0, 'force_condiment' );
			$active = Treat_DB_ProxyOld::mysql_result( $result3, 0, 'active' );
			$item_code = Treat_DB_ProxyOld::mysql_result( $result3, 0, 'item_code' );
			$upc = Treat_DB_ProxyOld::mysql_result( $result3, 0, 'upc' );
			$max = Treat_DB_ProxyOld::mysql_result( $result3, 0, 'max' );
			$reorder_point = Treat_DB_ProxyOld::mysql_result( $result3, 0, 'reorder_point' );
			$reorder_amount = Treat_DB_ProxyOld::mysql_result( $result3, 0, 'reorder_amount' );
			$shelf_life_days = Treat_DB_ProxyOld::mysql_result( $result3, 0, 'shelf_life_days' );
			$is_button = Treat_DB_ProxyOld::mysql_result( $result3, 0, 'is_button' );
			$mip_id = Treat_DB_ProxyOld::mysql_result( $result3, 0, 'mip_id' );
			$kiosk_price = Treat_DB_ProxyOld::mysql_result( $result3, 0, 'kiosk_price' );
			$new_price = Treat_DB_ProxyOld::mysql_result( $result3, 0, 'new_price' );
			$cost = Treat_DB_ProxyOld::mysql_result( $result3, 0, 'cost' );
			$sale_unit = array(
				Treat_DB_ProxyOld::mysql_result( $result3, 0, 'sale_unit' ) => 'selected="selected"'
			);
			$tare = Treat_DB_ProxyOld::mysql_result( $result3, 0, 'tare' );
						$ebt_fs_allowed = Treat_DB_ProxyOld::mysql_result( $result3, 0, 'ebt_fs_allowed');
			$submit_button = "Save";
			$copy_all_disabled = true;
			
			if( $active == 1 ) {
				$show_active_check = 'checked="checked"';
			}
			if( $is_button == 1 ) {
				$show_button_check = 'checked="checked"';
			}
			
			$date2 = date( 'Y-m-d H:i:s' );
			$date = date( "Y-m-d H:i:s", mktime( 0, 0, 0, date( "m" ), date( "d" ) - 100, date( "Y" ) ) );
			$items_sold = menu_items::items_sold( $pos_id, $GLOBALS['businessid'], $date, $date2 );
		}
		else {
			//$disable = 'disabled="disabled"';
			$new = 1;
			$submit_button = "Add";
			$group_id = 0;
			$machine_num = "";
			$pos_id = "";
			$item_name = "";
			$item_code = "";
			$upc = "";
			$max = 0;
			$reorder_point = 0;
			$reorder_amount = 0;
			$kiosk_price = "0.00";
			$cost = "";
			$new_price = "";
			$active = "";
				//$manufacturer = 0;
			$copy_all_disabled = false;
			$sale_unit = array(
				'unit' => 'selected="selected"'
			);
			$ebt_fs_allowed = 0;
		}
		
		$businessid = $GLOBALS['businessid'];
		$districtid_sql = "SELECT districtid FROM business WHERE businessid = $businessid";
		$districtid_result = Treat_DB_ProxyOld::query( $districtid_sql );
		
		$districtid = Treat_DB_ProxyOld::mysql_result( $districtid_result, 0, 'districtid' );

		$this->_view->assign( 'districtid', $districtid );
		$this->_view->assign( 'editid', $editid );
		$this->_view->assign( 'new', $new );
		$this->_view->assign( 'machine_num', $machine_num );
		$this->_view->assign( 'pos_id', $pos_id );
		//$this->_view->assign( 'manufacturer', $manufacturer );
		$this->_view->assign( 'item_name', $item_name );
		$this->_view->assign( 'group_id', $group_id );
		$this->_view->assign( 'ordertype', $ordertype );
		$this->_view->assign( 'cond_group', $cond_group );
		$this->_view->assign( 'force_condiment', $force_condiment );
		$this->_view->assign( 'active', $active );
		$this->_view->assign( 'item_code', $item_code );
		$this->_view->assign( 'upc', $upc );
		$this->_view->assign( 'max', $max );
		$this->_view->assign( 'reorder_point', $reorder_point );
		$this->_view->assign( 'reorder_amount', $reorder_amount );
		$this->_view->assign( 'shelf_life_days', $shelf_life_days );
		$this->_view->assign( 'is_button', $is_button );
		$this->_view->assign( 'mip_id', $mip_id );
		$this->_view->assign( 'kiosk_price', $kiosk_price );
		$this->_view->assign( 'new_price', $new_price );
		$this->_view->assign( 'cost', $cost );
		$this->_view->assign( 'sale_unit', $sale_unit );
		$this->_view->assign( 'tare', $tare );
		$this->_view->assign( 'ebt_fs_allowed', $ebt_fs_allowed );
		$this->_view->assign( 'submit_button', $submit_button );
		$this->_view->assign( 'copy_all_disabled', $copy_all_disabled );
		$this->_view->assign( 'items_sold', $items_sold );
		
		/*$query5 = "SELECT * FROM manufacturer ORDER BY name";
		$result5 = Treat_DB_ProxyOld::query( $query5 );
		
		$manufacturer_select = '';
		
		while( $r5 = mysql_fetch_array( $result5 ) ) {
			$mft_id = $r5["id"];
			$mft_name = $r5["name"];
			
			if( $manufacturer == $mft_id ) {
			$mft_sel = "SELECTED";
		}
		else {
		$mft_sel = "";
		}
		
		$manufacturer_select .= '<option value="'.$mft_id.'" '.$mft_sel.'>'.$mft_name.'</option>';
		}
		
		$this->_view->assign( 'manufacturer_select', $manufacturer_select );*/
		
		/////group types
		$query3 = "SELECT * FROM menu_groups_new WHERE businessid = {$GLOBALS['businessid']} ORDER BY name";
		$result3 = Treat_DB_ProxyOld::query( $query3 );
		
		$group_id_select = '';
		
		while( $r3 = mysql_fetch_array( $result3 ) ) {
			$grp_id = $r3["id"];
			$grp_name = $r3["name"];
			
			if( $group_id == $grp_id ) {
				$sel = "SELECTED";
			}
			else {
				$sel = "";
			}
			
			$group_id_select .= '<option value="' . $grp_id . '" ' . $sel . '>' . $grp_name . '</option>';
		}
		
		$this->_view->assign( 'group_id_select', $group_id_select );
		//////order types

		$business = EE\Model\Business\Singleton::getSingleton();
		$order_type_select = \EE\Model\Menu\Item\OrderType::buildMenuOrderTypes($business->companyid);
		$this->_view->assign( 'order_type_select', $order_type_select );
		$this->_view->assign( 'ordertype', $ordertype );
		
		$condGroups = Treat_Model_Aeris_Modifier_Group::getByBusinessid( $GLOBALS['businessid'] );
		
		$cond_groups_select = '';
		
		foreach( $condGroups as $condGroup ) {
			/* @var $condGroup Treat_Model_Aeris_Modifier_Group */
			$cond_groups_select .= '<option value="' . $condGroup->id . '"'
					. ( $condGroup->id == $cond_group ? ' selected="selected"' : '' ) . '>' . $condGroup->name . '</option>';
		}
		
		$this->_view->assign( 'cond_groups_select', $cond_groups_select );
		
		///taxes
		$query3 = "SELECT * FROM menu_tax WHERE businessid = {$GLOBALS['businessid']} ORDER BY pos_id";
		$result3 = Treat_DB_ProxyOld::query( $query3 );
		
		$tax_checkboxes = '';
		
		if( $disabled ) {
			$show_disabled = ' disabled="disabled"';
		}
		else {
			$show_disabled = '';
		}
		
		$tx_ctr = 0;
		while( $r3 = mysql_fetch_array( $result3 ) ) {
			if( $tx_ctr > 0 ) {
				$tax_checkboxes .= '<br />';
			}
			$tax_id = $r3["id"];
			$tax_name = $r3["name"];
			$tax_percent = $r3["percent"];
			
			$query4 = "SELECT * FROM menu_item_tax WHERE tax_id = $tax_id AND menu_itemid = $editid";
			$result4 = Treat_DB_ProxyOld::query( $query4 );
			
			$has_tax = @mysql_result( $result4, 0, 'tax_id' );
			
			if( $has_tax > 0 ) {
				$is_checked = "CHECKED";
			}
			else {
				$is_checked = "";
			}
			
			//$tax_checkboxes .= '<input type="checkbox" name="tax'.$tax_id.'" '.$is_checked.' value="'.$tax_id.'">'.$tax_name.' ('.$tax_percent.'%)';
			$tax_checkboxes .= '<input' . $show_disabled . ' type="checkbox" name="tax' . $tax_id . '" ' . $is_checked . ' value="'
					. $tax_id . '">' . $tax_name . ' (' . $tax_percent . '%)';
			$tx_ctr++;
		}
		
		$this->_view->assign( 'tax_checkboxes', $tax_checkboxes );
		
		$this->_view->assign( 'show_active_check', $show_active_check );
		$this->_view->assign( 'show_button_check', $show_button_check );
		
		
		/////search
		if( $search_what == "name" ) {
			$sel1 = "selected=SELECTED";
		}
		elseif( $search_what == "item_code" ) {
			$sel2 = "selected=SELECTED";
		}
		elseif( $search_what == "upc" ) {
			$sel3 = "selected=SELECTED";
		}
		
		$this->_view->assign( 'sel1', $sel1 );
		$this->_view->assign( 'sel2', $sel2 );
		$this->_view->assign( 'sel3', $sel3 );
		
		
		$showall = @$_GET["showall"];
		if( $showall == "" ) {
			$showall = isset( $_COOKIE["showall"] ) ? $_COOKIE["showall"] : '';
		}
		
		setcookie( 'showall', $showall );
		$this->_view->assign( 'showall', $showall );
		
		$hasCashiers = Treat_Model_ClientPrograms::getStationsForBusiness( $business->getBusinessId(),
			Treat_Model_ClientPrograms::MODE_CASHIER, true
		);
		$this->_view->assign( 'hasCashiers', (bool)$hasCashiers );

		if ( $editid > 0 ) {
			$images = \EE\Model\Image::getImages( $editid, \EE\Model\Image::$CATEGORY_MENU_ITEMS_NEW );
		} else {
			$images = null;
		}
		if ( $images) {
			foreach ( $images as $image) {
				$image->url = "/ta/businessMenus/getImage?id=" . $editid . "&category=" .
					\EE\Model\Image::$CATEGORY_MENU_ITEMS_NEW . "&number=". $image->number . "&thumbnail=1";
			}

			$this-> _view->assign( 'product_images', $images );
		} else {
			$this->_view->assign( 'product_images', array() );
		}
	}

    public function getImage() {
		$filter = new SiTech_Filter(SiTech_Filter::INPUT_GET);

		$id = $filter->input('id');
		$category = $filter->input('category');
		$number = $filter->input('number');
		$thumbnail = $filter->input('thumbnail');

		$image = \EE\Model\Image::getImage( $id, $category, $number );

		if ( $image ) {
			header("Content-type: image/" . $image->file_extension);
			if ( $thumbnail == '1' && $image->thumbnail ) {
				echo $image->thumbnail;
			} else {
				echo $image->image;
			}
			return false;
		} else {
			return false;
		}
    }

	public function posReporting() {

		$this->addJavascriptAtTop( '/assets/js/calendarPopup.js' );
		$this->addJavascriptAtTop( '', 'document.write(getCalendarStyles());' );
		$this->addJavascriptAtTop( '/assets/js/sorttable-jQuery.js' );
		$this->addJavascriptAtTop( '/assets/js/preload/preLoadingMessage2-styled.js' );
		$this->addJavascriptAtTop( '/assets/js/jquery.fileupload.js' );
		$this->addJavascriptAtTop( '/assets/js/jquery.fileupload-ui.js' );
		$this->addJavascriptAtTop( '/assets/js/ta/buscatermenu5.js' );
		
		$this->addJavascriptAtTop( '/assets/js/jquery.fusioncharts.debug.js' );
		$this->addJavascriptAtTop( '/assets/js/defaults.fusioncharts.js' );
		$this->addJavascriptAtTop( '/assets/js/FusionCharts.js' );
		$this->addJavascriptAtTop( '/assets/js/FusionChartsExportComponent.js' );
		$this->addJavascriptAtTop( '/assets/js/jquery.ui.tooltip.js' );
		$this->addJavascriptAtTop( '/assets/js/customDashboard.js' );

		

		$this->addCss( '/assets/css/jquery.fileupload-ui.css' );
		$this->addCss( '/assets/css/calendarPopup.css' );
		$this->addCss( '/assets/css/customDashboard.css' );
		$this->addCss( '/assets/css/jquery.ui.tooltip.css' );

		//Elements for calendar datetimepiker
//		$this->addJavascriptAtTop( '/assets/js/jquery.js' );
		$this->addJavascriptAtTop( '/assets/js/jquery.datetime.js' );
		$this->addJavascriptAtTop( "", "$('.some_class').datetimepicker();" );
		$this->addCss( '/assets/css/datetime.css' );
		//-----------------------------------
		
		$this->_view->assign( 'securityArray', $this->_securityArray );
		$this->_view->assign( 'user', $_SESSION->getUser( ) );
	}
	
	
}
