<?php

class Ta_Businessmenusmeta_NutritionController
	extends \EE\Controller\Meta\Base
{
	
	public function menu_nutrition() {
		static::addGetToJs();
		static::addCss( '/assets/css/ta/menu_nutrition.css' );
		static::addJavascriptAtTop( '/assets/js/ta/menu_nutrition.js' );
		
		$cid = static::getPostGetSessionOrCookieVariable( 'cid', $default = 0 );
		$bid = static::getPostGetSessionOrCookieVariable( 'bid', $default = 0 );
		$business = \EE\Model\Business\Singleton::getSingleton( $bid );
		$edit_item = static::getPostOrGetVariable( 'edit_item', $default = '' );
		$item_code = $edit_item_v =
		$mic_id = $min_id = $menu_item_id = null;
		if ( $edit_item ) {
			list( $item_code, $edit_item_v ) = explode( ';', $edit_item, 2 );
			if ( $edit_item_v ) {
				list( $mic_id, $min_id, $menu_item_id ) = explode( ':', $edit_item_v );
			}
		}
		
		$showall = static::getPostOrGetVariable( 'showall', $default = 0 );
		$showall = (bool)$showall;
		
		$customJS = sprintf( 
			'
				function menu_nutrition_import(){
					jQuery("<div />")
					.load("/ta/businessMenus/menu_nutrition_import?cid="+$GET(\'cid\')+"&bid="+$GET(\'bid\')+"&return_url="+encodeURIComponent(document.location.href))
					.dialog({
						modal: true,
						title: "Import Menu Nutrition"
					});
					return false;
				}
			'
#			,$business->getCompanyId()
#			,$business->getBusinessId()
		);
		static::addJavascriptAtTop( '', $customJS );
		
		/*
		 */
		$item_list = \EE\Model\Menu\Item::getMenuNutritionList( $bid, $cid, $showall );
		
		$edit_object = null;
		$nutrition = null;
		foreach ( $item_list as $group_name => $group_items ) {
			$edit_object = $this->_menu_nutrition__edit_object( $mic_id, $group_items, $item_code );
			if ( $edit_object ) {
				break;
			}
		}
		if ( !$edit_object ) {
			$edit_object = new \EE\ArrayObject();
#			echo '<pre>$edit_object from \EE\ArrayObject</pre>';
		}
		
		
		if ( $edit_object->mic_id ) {
			$nutrition = \EE\Model\Nutrition\Xref\Company::getByMenuItemCompanyId( $edit_object->id, $class = '\EE\ArrayObject' );
			
			if ( !$edit_object->min_id ) {
				$edit_object->min_id = $min_id;
			}
			if ( !$edit_object->menu_item_id ) {
				$edit_object->menu_item_id = $menu_item_id;
			}
		}
		
		$buttons_enabled = true;
		
		if ( !$nutrition ) { //couldn't get nutrition from company, try master level
		/* Scott 5-1-12 */
			$nutrition = array();
			$nut_item = \EE\Model\Menu\Item::getNutritionByMinId( $edit_object->min_id );
			
			if ( $nut_item && !empty( $nut_item->nutritionid ) ) { //got nutrition from master level
				$nut_types = explode( ':', $nut_item->nutrition_type_ids );
				$nut_keys = explode( ':', $nut_item->column_keys );
				$nut_values = explode( ':', $nut_item->values );
				
				foreach ( $nut_keys as $key => $value ) {
					if ( isset( $nut_values[ $key ] ) && isset( $nut_types[ $key ] ) ) {
						if ( !isset( $nutrition[ $nut_types[ $key ] ] ) ) {
							$nutrition[ $nut_types[ $key ] ] = new \EE\ArrayObject();
						}
						$nutrition[ $nut_types[ $key ] ]->value = $nut_values[ $key ];
					}
				}
				
				$buttons_enabled = false;
			}
			else {
				$nutrition = array();
				$buttons_enabled = true;
			}
			/* Scott 5-1-12 */
		}
		
		$return_url = (string)$this->_uri;
		if ( false === strpos( $return_url, '?' ) ) {
#			echo '<pre>false === strpos( $url, \'?\' )</pre>';
			$return_url = $return_url . '?' . http_build_query( $_GET );
		}
		
#		echo '<pre>';
#		echo '(string)$this->_uri = ';
#		var_dump( (string)$this->_uri );
#		echo '$_GET = ';
#		var_dump( $_GET );
#		echo '$return_url = ';
#		var_dump( $return_url );
#		echo '</pre>';
		
		$this->_view->assign( 'company_id', $cid );
		$this->_view->assign( 'business_id', $bid );
		$this->_view->assign( 'business_page', $business );
		$this->_view->assign( 'edit_item', $edit_item );
		$this->_view->assign( 'edit_item', $edit_item_v );
		$this->_view->assign( 'edit_item_v', $edit_item_v );
		$this->_view->assign( 'item_code', $item_code );
		$this->_view->assign( 'item_list', $item_list );
		$this->_view->assign( 'edit_object', $edit_object );
		$this->_view->assign( 'nutrition', $nutrition );
		$this->_view->assign( 'nutrition_types', \EE\Model\Nutrition\TypesObject::getTypes() );
		$this->_view->assign( 'return_url', $return_url );
		$this->_view->assign( 'showall', $showall );
		$this->_view->assign( 'buttons_enabled', $buttons_enabled );
	}
	
	
	protected function _menu_nutrition__edit_object( $mic_id, $group_items, $item_code ) {
		$edit_object = null;
		
		if ( $mic_id ) {
			$edit_object = \EE\Model\Menu\Item\Company::get( (int)$mic_id, true );
			if ( $edit_object ) {
				$edit_object->mic_id = $edit_object->id;
				
				$id_key = array(
					$edit_object->id, 0, 0,
				);
				$edit_object->id_key = implode( ':', $id_key );
			}
#			echo '<pre>$edit_object from $mic_id</pre>';
		}
		# there is the possibility that the above will not return an object
		if ( $group_items && isset( $group_items[$item_code] ) ) {
			if ( !$edit_object ) {
				$edit_object = $group_items[$item_code];
				$edit_object->id = $edit_object->mic_id;
#				echo '<pre>$edit_object from $item_list</pre>';
			}
			else {
				$tmp = $group_items[$item_code];
				$edit_object->min_id = $tmp->min_id;
				$edit_object->menu_item_id = $tmp->menu_item_id;
				$edit_object->id_key = $tmp->id_key;
				$edit_object->business_id = $tmp->business_id;
				$edit_object->name_list = $tmp->name_list;
				$edit_object->mic_name_list = $tmp->mic_name_list;
				$edit_object->min_name_list = $tmp->min_name_list;
				$edit_object->mi_name_list = $tmp->mi_name_list;
				if ( !$edit_object->item_name ) {
					if ( $edit_object->name ) {
						$edit_object->item_name = $edit_object->name;
					}
					else {
						$edit_object->item_name = $tmp->item_name;
					}
				}
			}
		}
		
		return $edit_object;
	}
	
	
	public function menu_nutrition_export() {
#		if ( $this->_isXHR ) {
		$this->_layout = '';
#		}
		
#		$cid = static::getPostGetSessionOrCookieVariable( 'cid', $default = 0 );
		$bid = static::getPostGetSessionOrCookieVariable( 'bid', $default = 0 );
		$type = static::getPostGetSessionOrCookieVariable( 'type', $default = 'csv' );
		$showall = static::getPostGetSessionOrCookieVariable( 'showall', $default = 0 );
		$showall = (bool)$showall;
		
#		$export = new \EE\Nutrition\Export( $cid, $type );
		$export = new \EE\Nutrition\Export\Business( $bid, $type, $showall);
		$export->setUseHeaderRow( $use_header = true );
		$export->setHttpHeaders();
		$output = $export->process();
		echo $output;
		
		return false;
	}
	
	
	public function menu_nutrition_import() {
		if ( $this->_isXHR ) {
			$this->_layout = '';
		}
		
		$cid = static::getPostGetSessionOrCookieVariable( 'cid', $default = 0 );
		$bid = static::getPostGetSessionOrCookieVariable( 'bid', $default = 0 );
		$business = \EE\Model\Business\Singleton::getSingleton( $bid );
		$import_id = microtime( true );
		
		$edit_item = static::getPostGetSessionOrCookieVariable( 'edit_item', $default = '' );
		$return_url = static::getPostGetSessionOrCookieVariable( 'return_url', $default = '' );
		$return_url = urldecode( $return_url );
		
		if ( $_POST && $_FILES ) {
			$this->_layout = '';
#			echo '<pre>';
#			echo '$_POST = ';
#			var_dump( $_POST );
#			echo '$_FILES = ';
#			var_dump( $_FILES );
#			echo '</pre>';
			
			$container = \EE\Nutrition\Import\Csv::loadFiles( $files = null, $key = 'menu_data' );
#			$processed = $container->process();
#			echo '<pre>';
#			echo '$container = ';
#			var_dump( $container );
#			echo '</pre>';
			
			$file = $container->getOne();
			$processed = $file->process();
#			echo '<pre>';
#			echo '$file = ';
#			var_dump( $file );
#			echo '</pre>';
			
#			$_POST = new \EE\ArrayObject( $_POST );
#			$import_id = $_POST->import_id;
			
			if ( $processed ) {
				$_SESSION->addFlashMessage( 'File import successful' );
			}
			else {
#				$errors = $container->getErrors();
				$errors = $file->getErrors();
				foreach ( $errors as $error ) {
					$_SESSION->addFlashError( $error );
				}
#				echo '<pre>';
#				echo '$errors = ';
#				var_dump( $errors );
#				echo '</pre>';
			}
			$final_numbers = $file->getFinalNumbers();
			if (
				isset( $final_numbers[ \EE\Nutrition\Import\Csv::COUNT_SUCCESS_MESSAGE_KEY ] )
				&& $final_numbers[ \EE\Nutrition\Import\Csv::COUNT_SUCCESS_MESSAGE_KEY ]
			) {
				$_SESSION->addFlashMessage( $final_numbers[ \EE\Nutrition\Import\Csv::COUNT_SUCCESS_MESSAGE_KEY ] );
			}
			if (
				isset( $final_numbers[ \EE\Nutrition\Import\Csv::COUNT_ERROR_MESSAGE_KEY ] )
				&& $final_numbers[ \EE\Nutrition\Import\Csv::COUNT_ERROR_MESSAGE_KEY ]
			) {
				$_SESSION->addFlashError( $final_numbers[ \EE\Nutrition\Import\Csv::COUNT_ERROR_MESSAGE_KEY ] );
			}
			
			if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION && defined( 'DEBUG_STOP_REDIRECT' ) && DEBUG_STOP_REDIRECT ) {
				echo sprintf( '<pre><a href="%1$s">%1$s</a><pre>', $return_url );
			}
			else {
				header( 'Location: ' . $return_url );
			}
			return false;
		}
		
		$this->_view->assign( 'company_id', $cid );
		$this->_view->assign( 'business_id', $bid );
		$this->_view->assign( 'business_page', $business );
		$this->_view->assign( 'edit_item', $edit_item );
		$this->_view->assign( 'return_url', $return_url );
		$this->_view->assign( 'import_id', $import_id );
	}
	
	
	public function menu_nutrition_help() {
		if ( $this->_isXHR ) {
			$this->_layout = '';
		}
	}
	
	
	public function menu_nutrition_save() {
#		if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
			\EE\Model\Menu\Item\Company::db()->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
#		}
		$cid = static::getPostGetSessionOrCookieVariable( 'cid', $default = 0 );
		$bid = static::getPostGetSessionOrCookieVariable( 'bid', $default = 0 );
		$business = \EE\Model\Business\Singleton::getSingleton( $bid, $cid );
		$edit_item = static::getPostGetSessionOrCookieVariable( 'edit_item', $default = '' );
		$mic_id = static::getPostGetSessionOrCookieVariable( 'mic_id', $default = 0 );
		$min_id = static::getPostGetSessionOrCookieVariable( 'min_id', $default = 0 );
		$menu_item_id = static::getPostGetSessionOrCookieVariable( 'menu_item_id', $default = 0 );
		$name = static::getPostGetSessionOrCookieVariable( 'name', $default = 0 );
		$item_code = static::getPostGetSessionOrCookieVariable( 'item_code', $default = 0 );
		$nutrition = static::getPostGetSessionOrCookieVariable( 'nutrition', $default = 0 );
		$showall = static::getPostGetSessionOrCookieVariable( 'showall', $default = 0 );
		
		$test_key = static::getEditItemCode( $mic_id, $min_id, $menu_item_id );
		
		$item = null;
		$http_qry = array(
			'cid' => $cid,
			'bid' => $bid,
			'showall' => (bool)$showall,
			'edit_item' => $item_code . ';' . $test_key,
		);
		
#		echo '<pre>';
#		echo '$_POST = ';
#		var_dump( $_POST );
#		echo '</pre>';
		if ( $test_key == $edit_item ) {
			$msg = '';
			if ( $mic_id ) {
#				echo '<pre>Attempting to retrieve $item</pre>';
				$item = \EE\Model\Menu\Item\Company::get( (int)$mic_id, true );
				$msg = 'Modified existing entry for "%s" : "%s"';
			}
			if ( !$item ) {
#				echo '<pre>Attempting to retrieve $item by companyid and item_code</pre>';
				$item = \EE\Model\Menu\Item\Company::getByCompanyIdAndItemCode( $cid, $item_code );
				$msg = 'Modified existing entry for "%s" : "%s"';
			}
#			echo '<pre>';
#			echo '$item = ';
#			var_dump( $item );
#			echo '</pre>';
			if ( !$item ) {
#				echo '<pre>$item retrieval failed, creating new $item</pre>';
				$item = new \EE\Model\Menu\Item\Company();
				$msg = 'Creating new entry for "%s" : "%s"';
			}
			
			if ( $item->company_id != $cid || $item->item_code != $item_code || $item->name != $name ) {
				$item->company_id = $cid;
				$item->item_code = $item_code;
				$item->name = $name;
#				echo '<pre>$item->save()</pre>';
#				echo '<pre>';
#				echo '$item = ';
#				var_dump( $item );
#				echo '</pre>';
				$item->save();
			}
			else {
				$msg = 'No initial differences detected in entry for "%s" : "%s"';
#				echo '<pre>No initial differences detected in entry</pre>';
			}
			$_SESSION->addFlashMessage( sprintf( $msg, $item_code, $name ) );
#			echo '<pre>$item->save() successful?</pre>';
		}
		
		if ( $item && $item->id && ( is_array( $nutrition ) || $nutrition instanceof Traversable ) ) {
#			echo '<pre>retrieving existing nutrition</pre>';
			$existing_nutrition = \EE\Model\Nutrition\Xref\Company::getByMenuItemCompanyId(
				$item->id
				,$class = '\EE\Model\Nutrition\Xref\Company'
			);
#			echo '<pre>looping valid nutrition types</pre>';
			$nutrition_types = \EE\Model\Nutrition\TypesObject::getTypes();
			if ( !$existing_nutrition ) {
				$existing_nutrition = array();
			}
			elseif ( !is_array( $existing_nutrition ) ) {
				$existing_nutrition = (array)$existing_nutrition;
			}
			
#			echo '<pre>';
#			echo '$nutrition_types = ';
#			var_dump( $nutrition_types );
#			echo '$existing_nutrition = ';
#			var_dump( $existing_nutrition );
#			echo '</pre>';
			
			foreach ( $nutrition_types as $nutrition_type ) {
				$value = null;
				if ( array_key_exists( $nutrition_type->id, $nutrition ) ) {
					$value = round( floatval( $nutrition[ $nutrition_type->id ] ), 4 );
				}
#				echo '<pre>';
#				echo sprintf( '$nutrition[ "%s" ]', $nutrition_type->id );
#				var_dump( $value );
#				echo '</pre>';
				
				if ( array_key_exists( $nutrition_type->id, $existing_nutrition ) ) {
#					echo '<pre>nutrition_type->id exists in $existing_nutrition</pre>';
					$tmp = $existing_nutrition[$nutrition_type->id];
					if ( $value ) {
#						echo '<pre>$value is a non empty amount</pre>';
						$tmp->value = $value;
						$tmp->save();
#						echo sprintf(
#							'<pre>Modified existing entry for "%s" with a value of "%s" %s</pre>'
#							,$nutrition_type->label
#							,$value
#							,$nutrition_type->units
#						);
						$_SESSION->addFlashMessage( sprintf(
							'Modified "%s" to have a value of "%s" %s'
							,$nutrition_type->label
							,$value
							,$nutrition_type->units
						) );
					}
					else {
#						echo '<pre>$value is an empty amount - attempting to delete</pre>';
						$tmp->delete();
#						echo sprintf(
#							'<pre>Deleted value for "%s"</pre>'
#							,$nutrition_type->label
#						);
						$_SESSION->addFlashMessage( sprintf( 'Deleted value for "%s"', $nutrition_type->label ) );
					}
				}
				else {
#					echo '<pre>nutrition_type->id *does not* exist in $existing_nutrition</pre>';
					if ( $value ) {
#						echo '<pre>$value is a non empty amount</pre>';
						$tmp = new \EE\Model\Nutrition\Xref\Company();
						$tmp->mic_id = $item->id;
						$tmp->nutrition_type_id = $nutrition_type->id;
						$tmp->value = $value;
						$tmp->save();
#						echo sprintf(
#							'<pre>Created a new entry for "%s" with a value of "%s" %s</pre>'
#							,$nutrition_type->label
#							,$value
#							,$nutrition_type->units
#						);
						$_SESSION->addFlashMessage( sprintf(
							'Saved "%s" with a value of "%s" %s'
							,$nutrition_type->label
							,$value
							,$nutrition_type->units
						) );
					}
					else {
#						echo '<pre>$value is an empty amount</pre>';
#						echo sprintf(
#							'<pre>Nothing to do for "%s" with a value of "%s" %s</pre>'
#							,$nutrition_type->label
#							,$value
#							,$nutrition_type->units
#						);
					}
				}
			}
			$http_qry['edit_item'] = $item_code . ';' . static::getEditItemCode( $item->id, $min_id, $menu_item_id );
		}
		
		if ( $item && $item->id && $item->company_id && $item->item_code ) {
			\EE\Model\Menu\ItemNew::updateMicId( $item->company_id, $item->item_code );
		}
		
		$location = '/ta/businessMenus/menu_nutrition?' . http_build_query( $http_qry );
#		echo '<pre>';
#		echo '$location = ';
#		var_dump( $location );
#		echo '</pre>';
#		echo sprintf( '<a href="%1$s">%1$s</a>', $location );
		header( 'Location: ' . $location );
		return false;
	}
	
	
	
}
