<?php

use Essential\Treat\Db\ProxyOld as Treat_DB_ProxyOld;
use Essential\Treat\Db\ProxyOld\ProcessHost as Treat_DB_ProxyOldProcessHost;

class Ta_Businessmenusmeta_MenusController
	extends \EE\Controller\Meta\Base
{
	
	public function menus() {
		$curmenutype = null; # Notice: Undefined variable
		
		// turn off minify - breaks pages with both jQuery and prototype
		//static::$_javascript_minify = false;
		$this->staticSet( '_javascript_minify', false );
		
		$business = \EE\Model\Business\Singleton::getSingleton();
		
		$this->addJavascriptAtTop( '/assets/js/prototype-custom.js' );
		$this->addJavascriptAtTop( '/assets/js/scriptaculous.js' );
		$this->addJavascriptAtTop( '/assets/js/ta/public_smo_scripts.js' );
		$this->addCss( '/assets/css/ta/buscatermenu4.css' );
		
		$curmenu = \EE\Controller\Base::getSessionCookieVariable( 'curmenu' );
		if( $curmenu == '' ) {
			$curmenu = $business->cafe_menu;
			setcookie( 'curmenu', $curmenu );
		}
		
		$customJS = "
			window.onload = function(){
				var auto = new Form.AutoComplete('autoBox', {
				dbTable: 'menu_items',
				dbDisp: 'item_name',
				dbVal: 'menu_item_id',
				dbWhere: \"businessid = '{$business->businessid}' AND menu_typeid = '$curmenu' ORDER BY item_name\",
				watermark: '(Select an Item)',
				showOnEmpty: true});
				auto.onchange=function(){auto.input.form.submit();}
			}
		";
		
		//$this->addJavascriptAtTop( '', $customJS );
		
		//$this->_addJQuery();
		$this->addJavascriptAtTop( '/assets/js/ta/buscatermenu.js' );
		
		$this->_addJQuery();
		$this->addJavascriptAtTop( '', 'jQuery.noConflict();' );
		$this->addJavascriptAtTop( '/assets/js/jquery.combo.js' );
		$this->addJavascriptAtTop( '/assets/js/ta/menus.js' );
		
		
		$menuTypeList = Treat_Model_Menu_Type::get( 
				"companyid = '{$GLOBALS['companyid']}' AND (businessid = '{$business->businessid}' OR businessid = '0') ORDER BY menu_typeid" );
		$this->_view->assign( 'menuTypeList', $menuTypeList );
		
		$toAssign = new \EE\ArrayObject();
		$toAssign['viewonly'] = null; # Notice: Undefined variable in template
		$toAssign['order_min'] = null; # Notice: Undefined variable in template
		$toAssign['description'] = null; # Notice: Undefined variable in template
		$toAssign['curmenu'] = $curmenu;
		$toAssign['style'] = 'text-decoration:none';
		$toAssign['day'] = date( 'd' );
		$toAssign['year'] = date( 'Y' );
		$toAssign['month'] = date( 'm' );
		$toAssign['today'] = "{$toAssign['year']}-{$toAssign['month']}-{$toAssign['day']}";
		$toAssign['todayname'] = date( 'l' );
		$toAssign['creditamount'] = 'creditamount';
		$toAssign['creditidnty'] = 'creditidnty';
		$toAssign['creditdate'] = 'creditdate';
		$toAssign['debitamount'] = 'debitamount';
		$toAssign['debitidnty'] = 'debitidnty';
		$toAssign['debitdate'] = 'debitdate';
		$toAssign['quatertotal'] = 0;
		$toAssign['monthtotal'] = 0;
		$toAssign['total'] = 0;
		$toAssign['counter'] = 0;
		$toAssign['findme'] = '.';
		$toAssign['double'] = '.00';
		$toAssign['single'] = '0';
		$toAssign['double2'] = '00';
		
		$toAssign['view'] = \EE\Controller\Base::getSessionCookieVariable( 'viewcook' );
		$toAssign['date1'] = \EE\Controller\Base::getSessionCookieVariable( 'date1cook' );
		$toAssign['date2'] = \EE\Controller\Base::getSessionCookieVariable( 'date2cook' );
		$toAssign['businessid'] = \EE\Controller\Base::getGetVariable( 'bid' );
		setcookie( 'caterbus', $toAssign['businessid'], 0, '/ta/' );
		$toAssign['companyid'] = \EE\Controller\Base::getGetVariable( 'cid' );
		$toAssign['view1'] = \EE\Controller\Base::getGetVariable( 'view1' );
		$toAssign['itemeditid'] = \EE\Controller\Base::getGetVariable( 'mid' );
		$toAssign['copyitemid2'] = \EE\Controller\Base::getGetVariable( 'copyitemid' );
		$toAssign['finditem'] = \EE\Controller\Base::getPostVariable( 'finditem' );
		$toAssign['findnext'] = \EE\Controller\Base::getPostVariable( 'findnext' );
		$toAssign['lastfind'] = \EE\Controller\Base::getPostVariable( 'lastfind' );
		if( $toAssign['findnext'] == '' ) {
			$toAssign['findnext'] = 0;
		}
		
		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		
		if( $toAssign['finditem'] != '' ) {
			
			if( $toAssign['lastfind'] != $toAssign['finditem'] ) {
				$toAssign['findnext'] = 0;
			}
			if( $toAssign['lastfind'] == $toAssign['finditem'] ) {
				$toAssign['findnext']++;
			}
			
			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query = "SELECT menu_item_id FROM menu_items WHERE menu_item_id = '{$toAssign['finditem']}' AND businessid = '{$toAssign['businessid']}'";
			$result = Treat_DB_ProxyOld::query( $query );
			$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );
			//mysql_close();
			
			$toAssign['itemeditid'] = Treat_DB_ProxyOld::mysql_result( $result, $toAssign['findnext'], 'menu_item_id', '' );
			
			if( $toAssign['itemeditid'] > 0 ) {
				$toAssign['view1'] = "edit";
			}
			else {
				$toAssign['itemeditid'] = '';
			}
			
			//echo "<font size=1>$finditem,$lastfind,$findnext</font>";
		}
		
		if( $toAssign['businessid'] == '' && $toAssign['companyid'] == '' ) {
			$toAssign['businessid'] = \EE\Controller\Base::getPostVariable( 'businessid' );
			$toAssign['companyid'] = \EE\Controller\Base::getPostVariable( 'companyid' );
			$toAssign['view1'] = \EE\Controller\Base::getPostVariable( 'view1' );
			$toAssign['date1'] = \EE\Controller\Base::getPostVariable( 'date1' );
			$toAssign['date2'] = \EE\Controller\Base::getPostVariable( 'date2' );
			$toAssign['findinv'] = \EE\Controller\Base::getPostVariable( 'findinv' );
		}
		
		if( $toAssign['date1'] == '' && $toAssign['date2'] == '' ) {
			$toAssign['previous'] = $toAssign['today'];
			for( $count = 1; $count <= 31; $count++ ) {
				$toAssign['previous'] = prevday( $toAssign['previous'] );
			}
			$toAssign['date1'] = $toAssign['previous'];
			$toAssign['date2'] = $toAssign['today'];
			$toAssign['view'] = 'All';
		}
		
		
		$user = $_SESSION->getUser();
		
		$toAssign['user'] = $user->username;
		$toAssign['pass'] = $user->password;
		$toAssign['security_level'] = $user->security_level;
		$toAssign['mysecurity'] = $user->security;
		$toAssign['bid'] = $user->businessid;
		$toAssign['cid'] = $user->companyid;
		$toAssign['bid2'] = $user->busid2;
		$toAssign['bid3'] = $user->busid3;
		$toAssign['bid4'] = $user->busid4;
		$toAssign['bid5'] = $user->busid5;
		$toAssign['bid6'] = $user->busid6;
		$toAssign['bid7'] = $user->busid7;
		$toAssign['bid8'] = $user->busid8;
		$toAssign['bid9'] = $user->busid9;
		$toAssign['bid10'] = $user->busid10;
		$toAssign['loginid'] = $user->loginid;
		
		$toAssign['pr1'] = $user->payroll;
		$toAssign['pr2'] = $user->pr2;
		$toAssign['pr3'] = $user->pr3;
		$toAssign['pr4'] = $user->pr4;
		$toAssign['pr5'] = $user->pr5;
		$toAssign['pr6'] = $user->pr6;
		$toAssign['pr7'] = $user->pr7;
		$toAssign['pr8'] = $user->pr8;
		$toAssign['pr9'] = $user->pr9;
		$toAssign['pr10'] = $user->pr10;
		
		$business = \EE\Model\Business\Singleton::getSingleton();
		
		$toAssign['businessname'] = $business->businessname;
		$toAssign['businessid'] = $business->businessid;
		$toAssign['street'] = $business->street;
		$toAssign['city'] = $business->city;
		$toAssign['state'] = $business->state;
		$toAssign['zip'] = $business->zip;
		$toAssign['phone'] = $business->phone;
		$toAssign['contract'] = $business->contract;
		$toAssign['bus_unit'] = $business->unit;
		
		$query2 = "SELECT type FROM menu_type WHERE menu_typeid = '{$toAssign['curmenu']}'";
		$result2 = Treat_DB_ProxyOld::query( $query2 );
		//mysql_close();
		
		$toAssign['curmenutype'] = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'type', '' );
				
				$query2 = "SELECT bus_menu FROM security WHERE securityid = $user->security";
		$result2 = Treat_DB_ProxyOld::query( $query2 );
		//mysql_close();
		
		$toAssign['sec_bus_menu'] = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'bus_menu', '' );
		
		if( $toAssign['view1'] == 'edit' ) {
			$query2 = "SELECT * FROM menu_items WHERE menu_item_id = '{$toAssign['itemeditid']}'";
			$result2 = Treat_DB_ProxyOld::query( $query2 );
			//mysql_close();
			
			$toAssign['editname'] = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'item_name', '' );
			$toAssign['mfrupc'] = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'upc', '' );
			$toAssign['unit'] = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'unit', '' );
			$toAssign['editprice'] = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'price', '' );
			$toAssign['retail'] = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'retail', '' );
			$toAssign['parent'] = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'parent', '' );
			$toAssign['description'] = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'description', '' );
			$toAssign['grouptypeid'] = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'groupid', '' );
			$toAssign['true_unit'] = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'true_unit', '' );
			$toAssign['order_unit'] = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'order_unit', '' );
			$toAssign['su_in_ou'] = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'su_in_ou', '' );
			$toAssign['serving_size'] = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'serving_size', '' );
			$toAssign['label'] = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'label', '' );
			$toAssign['order_min'] = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'order_min', '' );
			$toAssign['item_code'] = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'item_code', '' );
			$toAssign['sales_acct'] = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'sales_acct', '' );
			$toAssign['tax_acct'] = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'tax_acct', '' );
			$toAssign['contract_option'] = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'contract_option', '' );
			$toAssign['myimage'] = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'image', '' );
			$toAssign['active'] = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'deleted', '' );
			
			$toAssign['deleteitem'] = "<a href='/ta/editcateritem.php?cid={$GLOBALS['companyid']}&bid={$business->businessid}&edit=3&editid={$toAssign['itemeditid']}' onclick='return delconfirm2()'><img border='0' src='/assets/images/delete.gif' height='16' width='16' alt='DELETE' /></a>";
			$toAssign['showrecipe'] = "<a href='/ta/businventor4.php?cid={$GLOBALS['companyid']}&bid={$business->businessid}&editid={$toAssign['itemeditid']}'><img border='0' src='/assets/images/recipe/recipe.gif' height='16' width='16' alt='EDIT RECIPE' /></a>";
			
			if( $toAssign['active'] == 1 ) {
				$toAssign['showactive'] = '';
			}
			else {
				$toAssign['showactive'] = ' checked="checked"';
			}
		}
		
		$query2 = "SELECT * FROM size ORDER BY sizename";
		$result2 = Treat_DB_ProxyOld::query( $query2 );
		$num2 = Treat_DB_ProxyOld::mysql_numrows( $result2, 0 );
		
		$toAssign['sizeList_true'] = '';
		$toAssign['sizeList_order'] = '';
		
		while( $num2 >= 0 ) {
			$sizeid = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'sizeid', '' );
			$sizename = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'sizename', '' );
			$check1 = $check2 = '';
			if( $sizeid == $toAssign['true_unit'] ) {
				$check1 = ' selected="selected"';
				$toAssign['units'] = $sizename;
			}
			elseif( $sizeid == $toAssign['order_unit'] ) {
				$check2 = ' selected="selected"';
			}
			$toAssign['sizeList_true'] .= "<option value='$sizeid' $check1>$sizename</option>";
			$toAssign['sizeList_order'] .= "<option value='$sizeid' $check2>$sizename</option>";
			$num2--;
		}
		
		$query2 = "SELECT * FROM menu_items WHERE menu_typeid = '{$toAssign['menu_parent']}' ORDER BY item_name DESC";
		$result2 = Treat_DB_ProxyOld::query( $query2 );
		$num2 = Treat_DB_ProxyOld::mysql_numrows( $result2, 0 );
		
		$toAssign['parentList'] = '';
		
		while( $num2 >= 0 ) {
			$parent_id = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'menu_item_id', '' );
			$parentname = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'item_name', '' );
			if( $toAssign['parent'] == $parent_id ) {
				$check = ' selected="selected"';
			}
			else {
				$check = '';
			}
			$toAssign['parentList'] .= "<option value='$parent_id' $check>$parentname</option>";
			$num2--;
		}
		
		$query = "SELECT * FROM menu_groups WHERE companyid = '{$GLOBALS['companyid']}' AND menu_typeid = '{$toAssign['curmenu']}' ORDER BY groupname DESC";
		$result = Treat_DB_ProxyOld::query( $query );
		$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );
		
		$toAssign['groupList'] = '';
		
		$num--;
		while( $num >= 0 ) {
			$groupname = Treat_DB_ProxyOld::mysql_result( $result, $num, 'groupname', '' );
			$groupid = Treat_DB_ProxyOld::mysql_result( $result, $num, 'menu_group_id', '' );
			if( $groupid == $toAssign['grouptypeid'] ) {
				$sel = ' selected="selected"';
			}
			else {
				$sel = '';
			}
			$toAssign['groupList'] .= "<option value='$groupid' $sel>$groupname</option>";
			$num--;
		}
		
		$query = "SELECT * FROM menu_pricegroup WHERE menu_typeid = '{$toAssign['curmenu']}' ORDER BY menu_groupid,orderid DESC";
		$result = Treat_DB_ProxyOld::query( $query );
		$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );
		
		$toAssign['priceGroupList'] = '';
		
		$num--;
		while( $num >= 0 ) {
			$menu_groupname = Treat_DB_ProxyOld::mysql_result( $result, $num, 'menu_pricegroupname', '' );
			$groupid = Treat_DB_ProxyOld::mysql_result( $result, $num, 'menu_pricegroupid', '' );
			$menu_groupid = Treat_DB_ProxyOld::mysql_result( $result, $num, 'menu_groupid', '' );
			if( $groupid == $toAssign['grouptypeid'] ) {
				$sel = ' selected="selected"';
			}
			else {
				$sel = '';
			}
			
			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query5 = "SELECT groupname FROM menu_groups WHERE menu_group_id = '$menu_groupid'";
			$result5 = Treat_DB_ProxyOld::query( $query5 );
			//mysql_close();
			
			$groupname = Treat_DB_ProxyOld::mysql_result( $result5, 0, 'groupname', '' );
			
			$toAssign['priceGroupList'] .= "<option value='$groupid' $sel>$groupname - $menu_groupname</option>";
			$num--;
		}
		
		$query = "SELECT * FROM vend_pckg ORDER BY pckg_name DESC";
		$result = Treat_DB_ProxyOld::query( $query );
		$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );
		
		$toAssign['vendPckgList'] = '';
		
		$num--;
		while( $num >= 0 ) {
			$pckg_name = Treat_DB_ProxyOld::mysql_result( $result, $num, 'pckg_name', '' );
			$vend_pckgid = Treat_DB_ProxyOld::mysql_result( $result, $num, 'vend_pckgid', '' );
			if( $toAssign['true_unit'] == $vend_pckgid ) {
				$sel = ' selected="selected"';
			}
			else {
				$sel = '';
			}
			
			$toAssign['vendPckgList'] .= "<option value='$vend_pckgid' $sel>$pckg_name</option>";
			$num--;
		}
		
		$query = "SELECT * FROM credits WHERE credittype = '1' AND businessid = '{$GLOBALS['businessid']}' AND invoicesales = '1' ORDER BY creditname DESC";
		$result = Treat_DB_ProxyOld::query( $query );
		$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );
		
		$toAssign['creditList'] = '';
		
		$num--;
		while( $num >= 0 ) {
			$creditname = Treat_DB_ProxyOld::mysql_result( $result, $num, 'creditname', '' );
			$creditid = Treat_DB_ProxyOld::mysql_result( $result, $num, 'creditid', '' );
			if( $toAssign['sales_acct'] == $creditid ) {
				$sel = ' selected="selected"';
			}
			else {
				$sel = '';
			}
			
			$toAssign['creditList'] .= "<option value='$creditid' $sel>$creditname</option>";
			$num--;
		}
		
		$query = "SELECT * FROM credits WHERE credittype = '2' AND businessid = '{$GLOBALS['businessid']}' AND invoicetax ='1' ORDER BY creditname DESC";
		$result = Treat_DB_ProxyOld::query( $query );
		$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );
		
		$toAssign['crTaxList'] = '';
		
		$num--;
		while( $num >= 0 ) {
			$creditname = Treat_DB_ProxyOld::mysql_result( $result, $num, 'creditname', '' );
			$creditid = Treat_DB_ProxyOld::mysql_result( $result, $num, 'creditid', '' );
			if( $toAssign['tax_acct'] == $creditid ) {
				$sel = ' selected="selected"';
			}
			else {
				$sel = '';
			}
			
			$toAssign['crTaxList'] .= "<option value='$creditid' $sel>$creditname</option>";
			$num--;
		}
		
		$query = "SELECT * FROM credits WHERE credittype = '1' AND businessid = '{$GLOBALS['businessid']}' AND invoicesales_notax ='1' ORDER BY creditname DESC";
		$result = Treat_DB_ProxyOld::query( $query );
		$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );
		
		$toAssign['crNoTaxList'] = '';
		
		$num--;
		while( $num >= 0 ) {
			$creditname = Treat_DB_ProxyOld::mysql_result( $result, $num, 'creditname', '' );
			$creditid = Treat_DB_ProxyOld::mysql_result( $result, $num, 'creditid', '' );
			if( $toAssign['tax_acct'] == $creditid ) {
				$sel = ' selected="selected"';
			}
			else {
				$sel = '';
			}
			
			$toAssign['crNoTaxList'] .= "<option value='$creditid' $sel>$creditname</option>";
			$num--;
		}
		
		$query2 = "SELECT * FROM menu_alt_price WHERE menu_itemid = '{$toAssign['itemeditid']}' ORDER BY priceid DESC";
		$result2 = Treat_DB_ProxyOld::query( $query2 );
		$num2 = Treat_DB_ProxyOld::mysql_numrows( $result2, 0 );
		
		$toAssign['altPriceList'] = '';
		
		$num2--;
		while( $num2 >= 0 ) {
			$priceid = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'priceid', '' );
			$price = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'price', '' );
			$comment = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'comment', '' );
			
			$showdelete = "<a href='/ta/alt_price.php?cid={$GLOBALS['companyid']}&bid={$business->businessid}&edit=2&editid=$priceid&itemeditid={$toAssign['itemeditid']}' onclick='return delconfirm()'><img border='0' src='/assets/images/delete.gif' height='16' width='16' alt='Delete' /></a>";
			
			$price = money( $price );
			$toAssign['altPriceList'] .= "<tr><td colspan='2'><ul><li>$price/$comment $showdelete</li></ul></td></tr>";
			
			$num2--;
		}
		
		$query4 = "SELECT * FROM menu_items WHERE businessid = '{$business->businessid}' AND menu_typeid = '$curmenu' ORDER BY item_name";
		$result4 = Treat_DB_ProxyOld::query( $query4 );
		
		$toAssign['linkarray'] = array();
		
		while( $r = mysql_fetch_array( $result4 ) ) {
			$linkid = $r['menu_item_id'];
			$linkname = $r['item_name'];
			$linkarray[$linkid] = $linkname;
		}
		
		$query2 = "SELECT * FROM optiongroup WHERE menu_item_id = '{$toAssign['itemeditid']}' ORDER BY orderid";
		$result2 = Treat_DB_ProxyOld::query( $query2 );
		
		$toAssign['optGroupList'] = array();
		
		while( $r = mysql_fetch_assoc( $result2 ) ) {
			$optGroup = $r;
			$toAssign['maxGroupOrder'] = $r['orderid'];
			$toAssign['optionGroupId'] = $r['optiongroupid'];
			
			$query = "SELECT * FROM options WHERE optiongroupid = '{$r['optiongroupid']}' ORDER BY orderid DESC";
			$result = Treat_DB_ProxyOld::query( $query );
			
			while( $r2 = mysql_fetch_assoc( $result ) ) {
				$optGroup['optionList'] = $r2;
				$toAssign['maxOrder'] = $r2['orderid'];
			}
			$toAssign['maxOrder'] += 1;
			$toAssign['optGroupList'][] = $optGroup;
		}
		$toAssign['maxGroupOrder'] += 1;
		
		$query2 = "SELECT * FROM menu_items WHERE businessid = '{$business->businessid}' ORDER BY item_name DESC";
		$result2 = Treat_DB_ProxyOld::query( $query2 );
		$num2 = Treat_DB_ProxyOld::mysql_numrows( $result2, 0 );
		
		$toAssign['srvItemList'] = '';
		
		while( $num2 >= 0 ) {
			$copyitemid = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'menu_item_id', '' );
			$copyitemname = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'item_name', '' );
			$toAssign['srvItemList'] .= "<option value='/ta/buscatermenu.php?cid={$GLOBALS['companyid']}&bid={$business->businessid}&view1=edit&mid={$toAssign['itemeditid']}&copyitemid=$copyitemid#item'>$copyitemname</option>";
			$num2--;
		}
		
		$query2 = "SELECT * FROM menu_type WHERE companyid = '{$GLOBALS['companyid']}' AND type = '1' AND businessid = '{$business
				->businessid}' ORDER BY menu_typename DESC";
		$result2 = Treat_DB_ProxyOld::query( $query2 );
		$num2 = Treat_DB_ProxyOld::mysql_numrows( $result2, 0 );
		
		$counter = 1;
		$curarrow = 1;
		
		$toAssign['srvTypeList'] = array();
		
		$num2--;
		while( $num2 >= 0 ) {
			$menu_typeid = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'menu_typeid', '' );
			$menu_typename = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'menu_typename', '' );
			
			$srvType = array(
				'menu_typeid' => $menu_typeid, 'menu_typename' => $menu_typename
			);
			
			$query12 = "SELECT * FROM menu_daypart WHERE menu_typeid = '$menu_typeid' ORDER BY orderid DESC";
			$result12 = Treat_DB_ProxyOld::query( $query12 );
			$num12 = Treat_DB_ProxyOld::mysql_numrows( $result12, 0 );
			$num14 = $num12;
			
			$query = "SELECT * FROM menu_portion WHERE menu_typeid = '$menu_typeid' ORDER BY orderid DESC";
			$result = Treat_DB_ProxyOld::query( $query );
			$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );
			
			$num14--;
			while( $num14 >= 0 ) {
				$menu_daypartid = Treat_DB_ProxyOld::mysql_result( $result12, $num14, 'menu_daypartid', '' );
				$menu_daypartname = Treat_DB_ProxyOld::mysql_result( $result12, $num14, 'menu_daypartname', '' );
				$srvType['daypart'][] = array(
					'menu_daypartid' => $menu_daypartid, 'menu_daypartname' => $menu_daypartname
				);
				$num14--;
			}
			
			$num--;
			while( $num >= 0 ) {
				$menu_portionid = Treat_DB_ProxyOld::mysql_result( $result, $num, 'menu_portionid', '' );
				$menu_portionname = Treat_DB_ProxyOld::mysql_result( $result, $num, 'menu_portionname', '' );
				$srvTypePortion = array(
					'menu_portionid' => $menu_portionid, 'menu_portionname' => $menu_portionname
				);
				
				$num14 = $num12 - 1;
				while( $num14 >= 0 ) {
					$menu_daypartid = Treat_DB_ProxyOld::mysql_result( $result12, $num14, 'menu_daypartid', '' );
					$menu_daypartname = Treat_DB_ProxyOld::mysql_result( $result12, $num14, 'menu_daypartname', '' );
					
					$srvTypePortionDay = array();
					
					if( $copyitemid2 != '' ) {
						$query5 = "SELECT * FROM menu_portiondetail WHERE menu_itemid = '{$toAssign['copyitemid2']}' AND menu_typeid = '$menu_typeid' AND menu_portionid = '$menu_portionid' AND menu_daypartid = '$menu_daypartid'";
					}
					else {
						$query5 = "SELECT * FROM menu_portiondetail WHERE menu_itemid = '{$toAssign['itemeditid']}' AND menu_typeid = '$menu_typeid' AND menu_portionid = '$menu_portionid' AND menu_daypartid = '$menu_daypartid'";
					}
					$result5 = Treat_DB_ProxyOld::query( $query5 );
					
					$portionsize = Treat_DB_ProxyOld::mysql_result( $result5, 0, 'portion', '' );
					$srv_size = Treat_DB_ProxyOld::mysql_result( $result5, 0, 'srv_size', '' );
					
					$var_name = "portion$counter";
					$var_name2 = "srv_size$counter";
					
					$srvTypePortionDay['showname'] = ( $num14 == $num12 - 1 );
					$colwidth = 100 / ( $num12 + 1 );
					
					$srvTypePortionDay['colwidth'] = $colwidth;
					$srvTypePortionDay['var_name'] = $var_name;
					$srvTypePortionDay['var_name2'] = $var_name2;
					$srvTypePortionDay['srv_size'] = $srv_size;
					$srvTypePortionDay['portionsize'] = $portionsize;
					$srvTypePortionDay['units'] = $units;
					$srvTypePortionDay['curarrow'] = $curarrow;
					
					$curarrow = $curarrow + 1000;
					$srvTypePortionDay['showclose'] = ( $num14 == 0 );
					
					$num14--;
					$counter++;
					
					$srvTypePortion['day'][] = $srvTypePortionDay;
				}
				$curarrow = $curarrow - ( $num12 * 1000 );
				$curarrow++;
				$num14 = $num12;
				$num--;
				
				$srvType['portion'][] = $srvTypePortion;
			}
			$num2--;
			
			$toAssign['srvTypeList'][] = $srvType;
		}
		
		$query2 = "SELECT * FROM size ORDER BY sizename";
		$result2 = Treat_DB_ProxyOld::query( $query2 );
		$num2 = Treat_DB_ProxyOld::mysql_numrows( $result2, 0 );
		
		$toAssign['sizeList'] = '';
		
		while( $num2 >= 0 ) {
			$sizeid = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'sizeid', '' );
			$sizename = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'sizename', '' );
			$toAssign['sizeList'] .= "<option value=$sizeid>$sizename</option>";
			$num2--;
		}
		
		$itemList = \EE\Model\Menu\Item::getByCurrentMenuId(
			$business->businessid
			,$toAssign['curmenu']
			,'\EE\ArrayObject'
		);
		
		if (!$itemList){
			$itemList = array();
		}

		$toAssign['itemList'] = new \EE\ArrayObject();
		
		$menu_counter = 1;
		$lastgroup = '';
		$num--;
		foreach ( $itemList as $r ) {
			$menu_item_id = $r['menu_item_id'];
			$item_name = $r['item_name'];
			$price = $r['price'];
			$parent = $r['parent'];
			$groupid = $r['groupid'];
			$deleted = $r['deleted'];
			$recipe_station = $r['recipe_station'];
			$heart_healthy = $r['heart_healthy'];
			$upc = $r['upc'];
			$item_code = $r['item_code'];
			
			$item = $r;
			$item['showtr'] = null; # Notice: Undefined index
			
			////////////HAS RECIPE/NUTRITION
			$query61 = "SELECT COUNT(recipeid) AS totalrec FROM recipe WHERE menu_itemid = '$menu_item_id'";
			$result61 = Treat_DB_ProxyOld::query( $query61 );
			$num61 = Treat_DB_ProxyOld::mysql_result( $result61, 0, 'totalrec', '' );
			
			$item['shownutr'] = ( $num61 > 0 );
			
			/////////////////////////////////
			
			if( $recipe_station > 0 ) {
				$query7 = "SELECT station_name FROM servery_station WHERE stationid = '$recipe_station'";
				$result7 = Treat_DB_ProxyOld::query( $query7 );
				
				$item['station_name'] = Treat_DB_ProxyOld::mysql_result( $result7, 0, 'station_name', '' );
			}

			$price = number_format( $price, 2, '.', '' );
			$item['price'] = $price;

			if( $lastgroup != $groupid ) {
				if( $toAssign['curmenutype'] == 2 ) {
					$query7 = "SELECT menu_pricegroupname,menu_groupid FROM menu_pricegroup WHERE menu_pricegroupid = '$groupid'";
				}
				else {
					$query7 = "SELECT * FROM menu_groups WHERE menu_group_id = '$groupid'";
				}
				$result7 = Treat_DB_ProxyOld::query( $query7 );
				
				if( $toAssign['curmenutype'] == 2 ) {
					$groupname = Treat_DB_ProxyOld::mysql_result( $result7, 0, 'menu_pricegroupname', '' );
					$menu_groupid = Treat_DB_ProxyOld::mysql_result( $result7, 0, 'menu_groupid', '' );
					
					$query7 = "SELECT groupname FROM menu_groups WHERE menu_group_id = '$menu_groupid'";
					$result7 = Treat_DB_ProxyOld::query( $query7 );
					
					$groupname2 = Treat_DB_ProxyOld::mysql_result( $result7, 0, 'groupname', '' );
					
					$item['groupname'] = "$groupname2 - $groupname";
				}
				else {
					$item['groupname'] = Treat_DB_ProxyOld::mysql_result( $result7, 0, 'groupname', '' );
				}
				
				$item['showtr'] = true;
				
				$lastgroup = $groupid;
			}
			
			/////if commissary menu show if has upc and item code
			if( ( $upc == '' || $item_code == '' ) && $curmenutype == 2 ) {
				$message = "<sup><font color=red>";
				if( $upc == '' ) {
					$message .= 'No UPC';
					if( $item_code == '' ) {
						$message .= ', No Item Code';
					}
				}
				elseif( $item_code == '' ) {
					$message .= 'No Item Code';
				}
				$message .= "</font></sup>";
			}
			else {
				$message = '';
			}
			
			$item['message'] = $message;
			
			$item['menu_counter'] = $menu_counter;
			$toAssign['itemList'][] = $item;
			
			$menu_counter++;
			$num--;
		}
		
		$query11 = "SELECT * FROM servery_station ORDER BY station_name DESC";
		$result11 = Treat_DB_ProxyOld::query( $query11 );
		$num11 = Treat_DB_ProxyOld::mysql_numrows( $result11, 0 );
		
		$toAssign['serveryList'] = '';
		
		$num11--;
		while( $num11 >= 0 ) {
			$stationid = Treat_DB_ProxyOld::mysql_result( $result11, $num11, 'stationid', '' );
			$station_name = Treat_DB_ProxyOld::mysql_result( $result11, $num11, 'station_name', '' );
			
			$toAssign['serveryList'] .= "<option value='1:$stationid'>$station_name</option>";
			
			$num11--;
		}
		
		$toAssign['uMenuList'] = '';
		
		if( $toAssign['curmenutype'] == 2 ) {
			$query11 = "SELECT * FROM menu_groups WHERE menu_typeid = '{$toAssign['curmenu']}' ORDER BY groupname DESC";
			$result11 = Treat_DB_ProxyOld::query( $query11 );
			$num11 = Treat_DB_ProxyOld::mysql_numrows( $result11, 0 );
			
			$num11--;
			while( $num11 >= 0 ) {
				$menu_group_id = Treat_DB_ProxyOld::mysql_result( $result11, $num11, 'menu_group_id', '' );
				$groupname = Treat_DB_ProxyOld::mysql_result( $result11, $num11, 'groupname', '' );
				
				$query12 = "SELECT * FROM menu_pricegroup WHERE menu_groupid = '$menu_group_id' ORDER BY menu_pricegroupname DESC";
				$result12 = Treat_DB_ProxyOld::query( $query12 );
				$num12 = Treat_DB_ProxyOld::mysql_numrows( $result12, 0 );
				
				$num12--;
				while( $num12 >= 0 ) {
					$menu_pricegroupid = Treat_DB_ProxyOld::mysql_result( $result12, $num12, 'menu_pricegroupid', '' );
					$menu_pricegroupname = Treat_DB_ProxyOld::mysql_result( $result12, $num12, 'menu_pricegroupname', '' );
					
					$toAssign['uMenuList'] .= "<option value='4:$menu_pricegroupid'>$groupname - $menu_pricegroupname</option>";
					
					$num12--;
				}
				
				$num11--;
			}
		}
		else {
			$query11 = "SELECT * FROM menu_groups WHERE menu_typeid = '{$toAssign['curmenu']}' ORDER BY groupname DESC";
			$result11 = Treat_DB_ProxyOld::query( $query11 );
			$num11 = Treat_DB_ProxyOld::mysql_numrows( $result11, 0 );
			
			$num11--;
			while( $num11 >= 0 ) {
				$menu_group_id = Treat_DB_ProxyOld::mysql_result( $result11, $num11, 'menu_group_id', '' );
				$groupname = Treat_DB_ProxyOld::mysql_result( $result11, $num11, 'groupname', '' );
				
				$toAssign['uMenuList'] .= "<option value='4:$menu_group_id'>$groupname</option>";
				
				$num11--;
			}
		}
		
		if( $toAssign['curmenutype'] == 1 ) {
			$query11 = "SELECT * FROM menu_portion WHERE menu_typeid = '{$toAssign['curmenu']}' ORDER BY orderid DESC";
			$result11 = Treat_DB_ProxyOld::query( $query11 );
			$num11 = Treat_DB_ProxyOld::mysql_numrows( $result11, 0 );
			
			$colnum = $num11 + 1;
			$colnum2 = $colnum + 2;
			$curarrow = 1;
			
			$toAssign['mxColnum'] = $colnum;
			$toAssign['mxColnum2'] = $colnum2;
			
			$query = "SELECT * FROM menu_daypart WHERE menu_typeid = '{$toAssign['curmenu']}' ORDER BY orderid DESC";
			$result = Treat_DB_ProxyOld::query( $query );
			$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );
			
			$toAssign['mxDayList'] = array();
			
			$num--;
			while( $num >= 0 ) {
				$menu_daypartid = Treat_DB_ProxyOld::mysql_result( $result, $num, 'menu_daypartid', '' );
				$menu_daypartname = Treat_DB_ProxyOld::mysql_result( $result, $num, 'menu_daypartname', '' );
				
				$mxDay = array(
					'menu_daypartname' => $menu_daypartname
				);
				$mxDay['portion'] = array();
				
				$counter = $num11;
				$counter--;
				while( $counter >= 0 ) {
					$portionname = Treat_DB_ProxyOld::mysql_result( $result11, $counter, 'menu_portionname', '' );
					$portionid = Treat_DB_ProxyOld::mysql_result( $result11, $counter, 'menu_portionid', '' );
					$mxDay['portion'][] = $portionname;
					$counter--;
				}
				
				$mxDay['portionList'] = array();
				
				$counter = $num11;
				$counter--;
				$countme = 1;
				while( $counter >= 0 ) {
					$portionname = Treat_DB_ProxyOld::mysql_result( $result11, $counter, 'menu_portionname', '' );
					$portionid = Treat_DB_ProxyOld::mysql_result( $result11, $counter, 'menu_portionid', '' );
					
					$query12 = "SELECT * FROM order_matrix WHERE daypartid = '$menu_daypartid' AND portionid = '$portionid' AND menu_typeid = '{$toAssign['curmenu']}' AND businessid = '{$business
							->businessid}'";
					$result12 = Treat_DB_ProxyOld::query( $query12 );
					$portionvalue = Treat_DB_ProxyOld::mysql_result( $result12, 0, 'price', '' );
					
					$var_name = "portion$num$counter";
					$mxDay['portionList'][] = array(
						'var_name' => $var_name, 'portionvalue' => $portionvalue, 'curarrow' => $curarrow,
					);
					$counter--;
					$curarrow = $curarrow + 1000;
					$countme++;
				}
				if( $countme != 1 ) {
					$curarrow = $curarrow - ( ( $countme - 1 ) * 1000 ) + 1;
				}
				
				$num--;
			}
		}
		
		foreach( $toAssign as $var => $val ) {
			$this->_view->assign( $var, $val );
		}
		if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
			$this->_view->setAttribute( \SiTech_Template::ATTR_STRICT, true );
		}
	}
	
	public function savePosItems() {
		$return = filter_input( INPUT_POST, 'return' );
		$districtid = filter_input( INPUT_POST, 'districtid' );
		$originalItemCode = filter_input( INPUT_POST, 'originalItemCode' );
		
		$query = "SELECT menu_items_new . *
		, menu_items_price.id AS mip_id
		, menu_items_price.price
		, menu_items_price.new_price
		, menu_items_price.cost
		, business.businessname
		, business.businessid AS newbid
		FROM business
		LEFT JOIN menu_items_new ON menu_items_new.businessid =  business.businessid AND menu_items_new.item_code = '$originalItemCode'
		LEFT JOIN menu_items_price ON menu_items_price.menu_item_id = menu_items_new.id
		WHERE business.districtid = $districtid
		ORDER BY business.businessname";
		$result = Treat_DB_ProxyOld::query( $query );
		
		$counter = 1;

		while( $r = mysql_fetch_array( $result ) ) {
			$id = $r["id"];
			$mip_id = $r["mip_id"];
			$iBid = $r["newbid"];

			$group_id = @$_POST["grp"][$id];
			$ordertype = @$_POST["ot"][$id];
			$active = @$_POST["active"][$id];
			$name = @$_POST["name"][$id];
			$upc = @$_POST["upc"][$id];
			$cost = @$_POST["cost"][$id];
			$price = @$_POST["price"][$id];
			
			if( empty( $id ) ) {
				$add = @$_POST["count$counter"];
				$group_id = @$_POST["grp"][-$counter];
				$ordertype = @$_POST["ot"][-$counter];
				$active = @$_POST["active"][-$counter];
				$name = @$_POST["name"][-$counter];
				$upc = @$_POST["upc"][-$counter];
				$cost = @$_POST["cost"][-$counter];
				$price = @$_POST["price"][-$counter];
				
				if( $add == 1 && $group_id > 0 ) {
					$id = 0;
					///get new pos_id
					$query2 = "SELECT MAX(pos_id) AS pos_id FROM menu_items_new WHERE businessid = $iBid";
					$result2 = Treat_DB_ProxyOld::query( $query2 );
					$pos_id = Treat_DB_ProxyOld::mysql_result( $result2, 0, "pos_id", '' );
					$pos_id++;
					
					$uuid = \EE\Model\Menu\ItemNew::genUuid();
					///add item
					$query2 = "INSERT INTO menu_items_new
		(businessid
		, pos_id
		, group_id
		, name
		, ordertype
		, active
		, item_code
		, upc
		, sale_unit
		, tare
		, uuid_menu_items_new)
		VALUES
		($iBid
		, $pos_id
		, $group_id
		, '$name'
		, $ordertype
		, '$active'
		, '$originalItemCode'
		, '$upc'
		, 'unit'
		, '0'
		, '$uuid'
		)";
					$result2 = Treat_DB_ProxyOld::query( $query2 );
					$id = mysql_insert_id();
					
					if( $id < 1 ) {
						continue;
					}
					
					///add cost and price
					$query2 = "INSERT INTO menu_items_price (businessid,menu_item_id,pos_price_id,price,new_price,cost)
						VALUES ($iBid, $id, '1', '$price', '$price', '$cost')";
					$result2 = Treat_DB_ProxyOld::query( $query2 );
					
					///add taxes
					$query2 = "SELECT menu_tax.id
						,menu_item_tax.menu_itemid
						FROM menu_tax
						LEFT JOIN menu_item_tax ON menu_item_tax.menu_itemid = $id AND menu_item_tax.tax_id = menu_tax.id
						WHERE menu_tax.businessid = $iBid";
					$result2 = Treat_DB_ProxyOld::query( $query2 );
					
					while( $r2 = mysql_fetch_array( $result2 ) ) {
						$tax_id = $r2["id"];
						$tax_mid = $r2["menu_itemid"];
						
						$has_tax = $_POST["tax$tax_mid-$tax_id"];
						
						if( $has_tax == 1 ) {
							$uuid = \EE\Model\Menu\Item::genUuid();
							$query3 = "INSERT INTO menu_item_tax (menu_itemid,tax_id, uuid_menu_item_tax) VALUES ($id,$tax_id, '$uuid')";
							$result3 = Treat_DB_ProxyOld::query( $query3 );
						}
						else {
							$query3 = "DELETE FROM menu_item_tax WHERE menu_itemid = $id AND tax_id = $tax_id";
							$result3 = Treat_DB_ProxyOld::query( $query3 );
						}
					}
					
					//set to sync
					Treat_Model_KioskSync::addSync( $iBid, $id, Treat_Model_KioskSync::TYPE_ITEM );
					Treat_Model_KioskSync::addSync( $iBid, $id, Treat_Model_KioskSync::TYPE_ITEM_TAX );
				}
			}
			else {
				//update item
				$query2 = "UPDATE menu_items_new SET group_id = '$group_id'
			,ordertype = $ordertype
			,active = '$active'
			,name = '$name'
			,upc = '$upc'
			WHERE id = $id";

				$result2 = Treat_DB_ProxyOld::query( $query2 );
				
				//update cost and price
				$query2 = "UPDATE menu_items_price SET cost = '$cost', new_price = '$price' WHERE id = $mip_id";
				$result2 = Treat_DB_ProxyOld::query( $query2 );
				
				//audit
				$user = isset( $_COOKIE["usercook"] ) ? $_COOKIE["usercook"] : '';
				$audit_event_message = '';
				$audit_event_message .= $user . ' - Unknown - ' . ' (application/controllers/ta/businessmenusmeta/menus.php - savePosItems - Line 960)';
				
				$query2 = "INSERT INTO menu_items_price_audit (businessid, menu_item_id, new_price, cost, audit_event, audit_datetime)
							VALUES ($iBid,$id, '$price', '$cost', '$audit_event_message', NOW())";
				$result2 = Treat_DB_ProxyOld::query( $query2 );
				 
				//update taxes
				$query2 = "SELECT menu_tax.id
					,menu_item_tax.menu_itemid
					FROM menu_tax
					LEFT JOIN menu_item_tax ON menu_item_tax.menu_itemid = $id AND menu_item_tax.tax_id = menu_tax.id
					WHERE menu_tax.businessid = $iBid";
				$result2 = Treat_DB_ProxyOld::query( $query2 );
				//var_dump($_POST["tax"][$id]);
				while( $r2 = mysql_fetch_array( $result2 ) ) {
					$tax_id = $r2["id"];
					$tax_mid = $r2["menu_itemid"];
					
					$has_tax = @$_POST["tax$id-$tax_id"];


					if( $has_tax == 1 ) {
						$uuid = \EE\Model\Menu\Item::genUuid();
						$query3 = "INSERT INTO menu_item_tax (menu_itemid,tax_id,uuid_menu_item_tax) VALUES ($id,$tax_id,'$uuid')";
						$result3 = Treat_DB_ProxyOld::query( $query3 );
					}
					else {
						$query3 = "DELETE FROM menu_item_tax WHERE menu_itemid = $id AND tax_id = $tax_id";
						$result3 = Treat_DB_ProxyOld::query( $query3 );
					}
				}
				
				//set to sync
				Treat_Model_KioskSync::addSync( $iBid, $id, Treat_Model_KioskSync::TYPE_ITEM );
				Treat_Model_KioskSync::addSync( $iBid, $id, Treat_Model_KioskSync::TYPE_ITEM_TAX );
			}
			$counter++;
		}
		
		header( 'Location: ' . $return );
		return ( false );
	}
	
	public function savePosMenu() {

		$this->_layout = 'application.tpl';
		$this->addJavascriptAtTop( '/assets/js/jquery-1.4.2.min.js' );
		$this->addJavascriptAtTop( '/assets/js/jquery-ui-1.7.2.custom.min.js' );
		$this->addCss( '/assets/css/jq_themes/to-theme/jquery-ui-1.7.2.custom.css' );

		$errorMsg = '';
		$errorMsgImage = '';
		$messages = array();
		$businessIds = array();
		$editIds = array();
		$originalItemCode = "";

		$new = filter_input( INPUT_POST, 'new' );
		$businessid = filter_input( INPUT_POST, 'businessid' );

		$redirect = filter_input( INPUT_POST, 'redirect' );
		
		$user = $_SESSION->getUser();
		$bus_menu_pos = $user['bus_menu_pos'];
		//$bus_menu_pos = 4;

		// only level 4 access can see form, redirect if not level 4
		// so do not display to screen anything before redirect
		if( $bus_menu_pos == 4 ) {
			$display = true;
		}
		else {
			$display = false;
		}
		
		$businessIds = array();

		if(isset($_FILES['product_image']) && is_uploaded_file($_FILES['product_image']['tmp_name'])) {
			$product_image = $_FILES['product_image'];
		}
		else {
			$product_image = null;
		}

		$this->_view->assign( 'display', $display );
		$this->_view->assign( 'new', $new );

		$districtid = filter_input( INPUT_POST, "districtid" );
		$item_name = filter_input( INPUT_POST, "item_name" );
		$group_id = filter_input( INPUT_POST, "group_id" );
		//$manufacturer = filter_input( INPUT_POST, "manufacturer");
		$item_code = filter_input( INPUT_POST, "item_code" );
		$upc = filter_input( INPUT_POST, "upc" );
		$active = filter_input( INPUT_POST, "active" ) == '1' ? 1 : 0;
		$is_button = filter_input( INPUT_POST, "is_button" ) == '1' ? 1 : 0;
		$max = filter_input( INPUT_POST, "max" );
		$reorder_point = filter_input( INPUT_POST, "reorder_point" );
		$reorder_amount = filter_input( INPUT_POST, "reorder_amount" );
		$shelf_life_days = filter_input( INPUT_POST, "shelf_life_days" );
		$new_price = filter_input( INPUT_POST, "new_price" );
		$cost = filter_input( INPUT_POST, "cost" );
		$ordertype = filter_input( INPUT_POST, "ordertype" );
		$force_condiment = isset( $_POST['force_condiment'] ) ? 1 : 0;
		$add_to_all = intval( @$_POST["add_to_all"] );
		$sale_unit = filter_input( INPUT_POST, 'sale_unit' );
		$tare = filter_input( INPUT_POST, 'tare' );
		$ebt_fs_allowed = filter_input( INPUT_POST, 'ebt_fs_allowed' );

		// New Menu Item
		if( $new == 1 ) {

			$editid = 0;

			$add_to_businesses = array();
			if( $add_to_all == 1 ) {
				$business_lookup = \EE\Model\Business::getByDistrict( $districtid );
				foreach ($business_lookup as $each_business) {
					$add_to_businesses[$each_business->businessid] = strip_tags( $each_business->businessname );
				}
			}
			else {
				$bussiness_lookup = \EE\Model\Business::getById( $businessid );
				$add_to_businesses[$bussiness_lookup->businessid] = strip_tags( $bussiness_lookup->businessname );
			}
			
			$addme = "";
			foreach( $add_to_businesses as $bid => $bname ) {

				$existing_item = \EE\Model\Menu\ItemNew::get( 'businessid = ' . $bid . ' AND upc = \'' . $upc . '\'' );
				
				if( $existing_item ) {
					$messages[] = "Attempting to add item to $bname: <font color='red'>ALREADY EXISTS</font>";
					continue;
				}
				
				$gid = ( $bid == $businessid ? $group_id : "NULL" );
				$gid = $gid > 0 ? $gid : NULL;
				$uuid = \EE\Model\Menu\ItemNew::genUuid();

				$item = new \EE\Model\Menu\ItemNew();
				$item->businessid = $bid;
				$item->group_id = $gid;
				$item->name = $item_name;
				$item->ordertype = $ordertype;
				$item->force_condiment = $force_condiment;
				$item->active = $active;
				$item->item_code = $item_code;
				$item->upc = $upc;
				$item->max = $max;
				$item->reorder_point = $reorder_point;
				$item->reorder_amount = $reorder_amount;
				$item->shelf_life_days = $shelf_life_days;
				$item->is_button = $is_button;
				$item->sale_unit = $sale_unit;
				$item->tare = $tare;
				$item->uuid_menu_items_new = $uuid;
				$item->ebt_fs_allowed = $ebt_fs_allowed;

				$result = $item->save();

				$editid = 0;
				$new_mip_id = 0;

				if( $result ) {
					$editid = $item->id;
					
					if( $addme == "" )
						$addme = "&id=$editid";

					$itemPrice = new \EE\Model\Menu\Item\Price;
					$itemPrice->businessid = $bid;
					$itemPrice->menu_item_id = $item->id;
					$itemPrice->pos_price_id = 1;
					//$itemPrice->price = $new_price;
					$itemPrice->new_price = $new_price;
					$itemPrice->cost = $cost;

					$result = $itemPrice->save();

					if( $result ) {
						$new_mip_id = $itemPrice->id;
						$mipIds[$editid] = $new_mip_id;
					}
					else {
						$errorMsg = 'failure while updating menu prices: ';
					}

					// Insert Product image if it exists
					if ( $product_image ) {
						$image = new \EE\Model\Image();
						$errorMsgImage = $image->newImage($product_image, $editid,
							\EE\Model\Image::$CATEGORY_MENU_ITEMS_NEW
						);
						if ( $errorMsgImage == "" ) {
							\EE\Model\KioskSync::addSync( $bid, $image->id,
								\EE\Model\KioskSync::TYPE_MENU_ITEM_IMAGE
							);
						}
					}
				}
				else {
					$errorMsg = 'failure while updating menu items: ';
				}
				
				if( $new_mip_id == 0 ) {
					$mysqlError = $errorMsg;
					$addlError = $mysqlError ? "<br /><span style='font-style: italic; color: grey;'>$mysqlError</span>" : '';
					$messages[] = "Attempting to add item to $bname: <font color='red'>FAILURE</font>$addlError";
				}
				else {
					$messages[] = "Attempting to add item to $bname: <font color='green'>SUCCESS</font>";
					$businessIds[] = $bid;
					$editIds[$bid] = $editid;
				}

				if ( $errorMsgImage != '' ) {
					$messages[] = $errorMsgImage;
				}
			}

			$this->_view->assign( 'messages_location', $_SERVER['HTTP_REFERER'] . $addme );
		}
		// Existing Item
		else {
			
			$editid = filter_input( INPUT_POST, 'editid' );
			$delete_image = filter_input(INPUT_POST, 'delete_image', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

			$existing_item = \EE\Model\Menu\ItemNew::getById( $editid );
			$originalItemCode = $existing_item->item_code;
			
			$gid = $group_id;
			$gid = $gid > 0 ? $gid : "NULL";

			$existing_item->group_id = $gid;
			$existing_item->name = $item_name;
			$existing_item->pos_processed = 0;
			$existing_item->ordertype = $ordertype;
			$existing_item->force_condiment = $force_condiment;
			$existing_item->active = $active;
			$existing_item->item_code = $item_code;
			$existing_item->upc = $upc;
			$existing_item->max = $max;
			$existing_item->reorder_point = $reorder_point;
			$existing_item->reorder_amount = $reorder_amount;
			$existing_item->shelf_life_days = $shelf_life_days;
			$existing_item->is_button = $is_button;
			$existing_item->sale_unit = $sale_unit;
			$existing_item->tare = $tare;
			$existing_item->ebt_fs_allowed = $ebt_fs_allowed;

			$result = $existing_item->save();

			$editIds[$businessid] = $editid;

			// Update Product image if it exists
			if ( $product_image ) {
				$image = new \EE\Model\Image();
				$errorMsgImage = $image->newImage($product_image, $editid,
					\EE\Model\Image::$CATEGORY_MENU_ITEMS_NEW
				);

				if ( $errorMsgImage == "" ) {
					\EE\Model\KioskSync::addSync( $businessid, $image->id, \EE\Model\KioskSync::TYPE_MENU_ITEM_IMAGE );
				}
			}

			if ( !empty( $delete_image ) ) {
				$image_deleted = false;
				foreach( $delete_image as $image_number ) {
					$image = \EE\Model\Image::getMenuItemNewImage( $editid, $image_number );
					if ( $image ) {
						$image_deleted = true;
						$image->delete();
					}
				}
				if ( $image_deleted ) {
					\EE\Model\Image::reorderImages( $editid, \EE\Model\Image::$CATEGORY_MENU_ITEMS_NEW );
				}
			}

			if( !$result ) {
				$messages[] = "<font color='red'>Error</font> Updating item";
			}

			if ( $errorMsgImage != '' ) {
				$messages[] = $errorMsgImage;
			}
		}
		
		foreach( $editIds as $bId => $sync_editId ) {
			\EE\Model\KioskSync::addSync( $bId, $sync_editId, \EE\Model\KioskSync::TYPE_ITEM );
		}
		
		if( isset( $_POST['new_price'] ) || isset( $_POST['cost'] ) ) {
			// if they set the price & then changed their mind they need
			// to be able to set the value to null
			$new_price = trim( $_POST['new_price'] ) == '' ? 'NULL' : sprintf( '%f', filter_input( INPUT_POST, 'new_price' ) );
			$cost = trim( $_POST['cost'] ) == '' ? '0.00' : sprintf( '%f', filter_input( INPUT_POST, 'cost' ) );
			
			foreach( $editIds as $mId ) {
				if( $mId == $editid ) {
					$mip_id = filter_input( INPUT_POST, 'mip_id' );
				}
				else {
					$itemPrice = \EE\Model\Menu\Item\Price::get( 'menu_item_id = ' . $mId, true );
					if ( $itemPrice ) {
						$mip_id = $itemPrice->id;
					} else {
						$mip_id = '';
					}
				}
				
				if( $mip_id < 1 ) {
					$mip_id = $mipIds[$mId];
				}
				// if $mip_id isn't a number or it's 0 there's no sense in hitting the database to do nothing.
				if( (string)intval( $mip_id ) == (string)$mip_id && $mip_id !== 0 ) {

					$itemPrice = \EE\Model\Menu\Item\Price::get( 'id = ' . $mip_id, true );
					if (!($itemPrice instanceof \EE\Model\Menu\Item\Price)){
						$itemPrice = new \EE\Model\Menu\Item\Price();
					}

					$itemPrice->cost = $cost;
					if ( $mId == $editid ) {
						//$itemPrice->new_price = $new_price;
						if ($new_price == 'NULL' || $new_price == ''){
							$itemPrice->new_price = NULL;
						} else {
							$itemPrice->new_price = $new_price;
						}
					}
					$result = $itemPrice->save();

					if( !$result ) {
						$messages[] = "<font color='red'>Error</font> Updating Price ($mip_id)";
					}

					$current_price_value = $itemPrice->price;

					$user = isset( $_COOKIE["usercook"] ) ? $_COOKIE["usercook"] : '';
					$audit_event_message = '';
					$audit_event_message .= $user . ' - Single Item Edit' . ' (application/controllers/ta/businessmenusmeta/menus.php - savePosMenu)';

					$item_price_audit_log = new \EE\Model\Menu\ItemPriceAudit();
					$item_price_audit_log->create( $mip_id, $bId, $mId, $current_price_value, $new_price, $cost,
						$audit_event_message
					);
				}
			}
		}
		
		////taxes
		$businessid = filter_input( INPUT_POST, "businessid" );
		if( $editid < 1 ) {
			$editid = filter_input( INPUT_POST, "editid" );
		}

		$menu_taxes = \EE\Model\Menu\Tax::get( 'businessid = '. $businessid . ' ORDER BY pos_id' );

		if ( is_numeric($editid) ) {
			foreach ($menu_taxes as $menu_tax) {
				$tax_id = $menu_tax->id;

				$has_tax = filter_input(INPUT_POST, "tax" . $tax_id);

				if ($has_tax > 0) {
					$menu_item_tax = new \EE\Model\Menu\Item\Tax();
					$menu_item_tax->menu_itemid = $editid;
					$menu_item_tax->tax_id = $tax_id;
					$result = $menu_item_tax->save();

					if (!$result) {
						$messages[] = "<font color='red'>Error</font> Updating Tax ($tax_id)";
					}

					// Add to the sync table for the item tax
					Treat_Model_KioskSync::addSync($businessid, $editid, Treat_Model_KioskSync::TYPE_ITEM);
					Treat_Model_KioskSync::addSync($businessid, $editid, Treat_Model_KioskSync::TYPE_ITEM_TAX);
				} else {
					$menu_item_tax = \EE\Model\Menu\Item\Tax::get('menu_itemid = ' . $editid . ' AND tax_id = ' . $tax_id);
					if ($menu_item_tax) {
						//$menu_item_tax->delete();
						$query2 = "DELETE FROM menu_item_tax WHERE menu_itemid = $editid AND tax_id = $tax_id";
						$result2 = Treat_DB_ProxyOld::query( $query2 );

						Treat_Model_KioskSync::addSync($businessid, $editid, Treat_Model_KioskSync::TYPE_ITEM_TAX);
					}
				}
			}
		}

		if( !$display ) { //user doesn't have access to form, redirect
			$pieces = explode( '?', $redirect );
			parse_str( $pieces[1] ); //parse query string into variables
			
			if( !isset( $id ) ) { //if parsing query string did not create a variable called id, append editid to redirect url so item reloads
				$redirect .= '&id=' . $editid;
			}
			header( 'Location: ' . $redirect );
			return false;
		}
		
		////new master menu
//		$user = isset( $_COOKIE["usercook"] ) ? $_COOKIE["usercook"] : '';

		$business = \EE\Model\Business::getById( $businessid );
		$districtid = $business->districtid;

		$menu_items = \EE\Model\Menu\ItemNew::getListByDistrictAndUPC( $districtid, $originalItemCode );

		$menu_groups = \EE\Model\Menu\GroupsNew::getListByBusinessid( $businessid );

		$this->_view->assign( 'menu_groups', $menu_groups );

		$business = EE\Model\Business\Singleton::getSingleton();
		$order_type_select = \EE\Model\Menu\Item\OrderType::buildMenuOrderTypes( $business->companyid );
		$this->_view->assign( 'order_type_select', $order_type_select );

		$this->_view->assign( 'http_referer', $_SERVER['HTTP_REFERER'] );
		$this->_view->assign( 'district_id', $districtid );
		$this->_view->assign( 'original_item_code', $originalItemCode );

//		$counter = 1;
		$missing = 0;

		foreach( $menu_items as $menu_item ) {
			if( empty($menu_item->id )) {
				$menu_item->mycolor = "#FFFF00";
				$menu_item->item_code = "Not Found";
				$missing++;
			}
			elseif( $active == 1 ) {
				$menu_item->mycolor = "#CCCCCC";
			}
			else {
				$menu_item->mycolor = "#FFFFFF";
			}

			$new_menu_groups = \EE\Model\Menu\GroupsNew::getListByBusinessid( $menu_item->newbid );
			$menu_item->new_menu_groups = $new_menu_groups;

			$new_menu_items_tax = \EE\Model\Menu\Item\Tax::getListByMenuItemAndBusiness( $menu_item->id, $menu_item->newbid );
			$menu_item->new_menu_items_taxs = $new_menu_items_tax;

			$price = number_format( $menu_item->price, 2 );
			if( empty( $menu_item->new_price ) ) {
				$price = number_format( $price, 2 );
			}
			else {
				$price = number_format( $menu_item->new_price, 2 );
			}
			$formatted_cost = number_format( (double)$cost, 2 );

			$menu_item->formated_cost = $formatted_cost;
			$menu_item->formated_price = $price;

		}

		$this->_view->assign( 'missing', $missing );
		$this->_view->assign( 'menu_items', $menu_items );
		$this->_view->assign( 'messages', $messages );
	}
	
}
