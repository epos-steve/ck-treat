<?php
use Essential\Treat\Db\ProxyOld as Treat_DB_ProxyOld;
use Essential\Treat\Db\ProxyOld\ProcessHost as Treat_DB_ProxyOldProcessHost;

class Ta_Businessmenusmeta_PosmanagementController
	extends \EE\Controller\Meta\Base
{
	
	public function posManagement() {
		$this->addJavascriptAtTop( '/assets/js/commonjq.js' );

		$this->staticSet( '_jQueryVersion', '1.7.2+1.9.0' );


		$this->addFontFace( 'dejavusans' );
		$this->addCss( '/assets/css/ta/businessmenus.css' );
		$this->addCss( '/assets/css/casManagement.css' );
		$this->addCss( '/assets/css/product-button-colors.css' );
		
		$this->addPlugin( 'jquery.ui.aerispanel' );
		$this->addPlugin( 'jquery.ba-hashchange', false );
		$this->addPlugin( 'jquery.ui.slidenav' );
		$this->addPlugin( 'jquery.waffle' );
		$this->addPlugin( 'jquery.appear-1.1.1.min', false );
		$this->addPlugin( 'jquery.ba-resize', false );
		$this->addPlugin( 'jquery.cookie', false );
		$this->addPlugin( 'jquery.ui.picker' );
		$this->addPlugin( 'jquery.ui.ajaxedit' );
		$this->addPlugin( 'jquery.ui.panelgroup' );
		$this->addPlugin( 'colorpicker' );

		$this->addJavascriptAtTop( '/assets/js/casManagement.js' );
		$this->addJavascriptAtTop( '/assets/js/ta/businessmenus.js' );
		
		$this->addJavascriptAtTop( '/assets/js/kioskPromotions.js' );
		$this->addJavascriptAtTop( '/assets/js/kioskCombos.js' );
		$this->addJavascriptAtTop( '/assets/js/kioskPromotionsCommon.js' );
		$this->addJavascriptAtTop( '/assets/js/kioskSmartMenus.js' );
		$this->addJavascriptAtTop( '/assets/js/jquery.combo.js' );
		$this->addJavascriptAtTop( '/assets/js/ta/posManagement.js' );
		$this->addJavascriptAtTop( '/assets/js/ta/businessmenus/staticModifiers.js' );
		$this->addJavascriptAtTop( '/assets/js/ta/businessmenus/setupLocation.js' );
		$this->addJavascriptAtTop( '/assets/js/ta/businessmenus/menuManager.js' );

		$this->addJavascriptAtTop( '/assets/js/jquery.fileupload.js' );
		$this->addJavascriptAtTop( '/assets/js/jquery.fileupload-ui.js' );
		$this->addCss( '/assets/css/jquery.fileupload-ui.css' );

		//new comboorder JRLM
		$this->addJavascriptAtTop( '/assets/js/ta/comboOrder.js' );
		
		
		
		########################################################################
		# WTF people? stop randomly creating variables in only certain places
		#
		# Patrick - This was copied from legacy code. If you have a problem with
		#		   it, talk to Steve. Or just fix it and quit bitching.
		########################################################################
		$showday = null; # Notice: Undefined variable
		$order_date = null; # Notice: Undefined variable
		$shownextday = null; # Notice: Undefined variable
		$next_order_date = null; # Notice: Undefined variable
		$date_count = null; # Notice: Undefined variable
		$temp_days = null; # Notice: Undefined variable
		$order_bus = null; # Notice: Undefined variable
		$scheduleid = null; # Notice: Undefined variable
		$menuItemList = null; # Notice: Undefined variable
		$operating_days = null; # Notice: Undefined variable
		$current_terminal = null; # Notice: Undefined variable
		$mach_schedule = null; # Notice: Undefined variable
		
		$business = \EE\Model\Business\Singleton::getSingleton();
		
		$this->_view->assign( 'businessTimezone', $business->timezone );
		
		/* Scott 4-3-12 */
		$user = $_SESSION->getUser();
		$security_lvl = $user['bus_menu_pos_mgmt'];
		$this->_view->assign( 'security_lvl', $security_lvl );
		/* Scott 4-3-12 */
		
		$this->_view->assign( 'setupUnavailable', !\EE\Model\Aeris\Setting::checkSettingsVersion( $GLOBALS['businessid'] ) );
		
		// Customers
		$customerList = Treat_Model_User_Customer::get( 
				"businessid = {$GLOBALS['businessid']} AND is_deleted = '0' ORDER BY last_name,first_name" );
		$this->_view->assign( 'customerList', $customerList );
		
		// Card Numbers
		$cardList = Treat_Model_User_Customer_Scancodes::get( "businessid = {$GLOBALS['businessid']} ORDER BY scan_start" );
		$this->_view->assign( 'cardList', $cardList );
		
		// Menu Groups
		$groupList = Treat_Model_Menu_Group::get( "businessid = {$GLOBALS['businessid']} ORDER BY pos_id" );
		$this->_view->assign( 'groupList', $groupList );
		
		// Promotion Groups
		$this->_view->assign( 'promoGroupsList', Treat_Model_Promotion_Group::getByBusiness( $GLOBALS['businessid'] ) );
		
		// Taxes
		$taxList = Treat_Model_Menu_Tax::get( "businessid = {$GLOBALS['businessid']} ORDER BY pos_id" );
		$this->_view->assign( 'taxList', $taxList );
		
		// Menu Group Taxes
		$grTaxList = array_slice( $taxList, 0, 3 );
		$this->_view->assign( 'grTaxList', $grTaxList );
		
		// Promotion Targets
		$promoTargetList = Treat_Model_Menu_Promo_Trigger::get();
		$this->_view->assign( 'promoTargetList', $promoTargetList );
		// Promotion Types
		$promoTypeList = Treat_Model_Menu_Promo_Type::get();
		$this->_view->assign( 'promoTypeList', $promoTypeList );
		
		// Smart Menu Location
		$smenuLocationObj = Treat_Model_SmartMenu_Location::getByBusinessid( $business->businessid );
		$smenuLocation = $smenuLocationObj->toArray();
		$smenuLocation['name'] = $business->businessname;
		$this->_view->assign( 'smenuLocation', $smenuLocation );
		
		// Removing more code from the view
		$goto = \EE\Controller\Base::getGetVariable( 'goto' );
		$this->_view->assign( 'showcust', $goto == 1 ? 'none' : 'block' );
		$this->_view->assign( 'showcfo', $goto == 1 ? 'block' : 'none' );
		
		$this->_view->assign( 'functionPanelFunctions', \EE\Model\Aeris\FunctionPanelFunction::getFunctionList() );

		// Nav security
		$navSecurity = array(
			0 => false,
			1 => array(
				'customer', 'groups', 'taxes', 'posscreen'
			),
			2 => array(
				'customer', 'groups', 'taxes', 'commissary', 'posscreen'
			),
			3 => true,
			4 => array(
				'commissary'
			),
		);
		
		// Navigation Links
		$navLinks = array(
			array(
				'id' => 'posstatus',
				'title' => 'Status',
				'enabled' => (bool)(
					$user->bus_menu_pos_mgmt
					|| $user->posmgmt_status
					|| $user->posmgmt_setup
					|| $user->posmgmt_cashiers
					|| $user->posmgmt_posscreens
					|| $user->posmgmt_modifiers
					|| $user->posmgmt_staticmods
					|| $user->posmgmt_customers
					|| $user->posmgmt_cardnums
					|| $user->posmgmt_menugroup
					|| $user->posmgmt_taxes
					|| $user->posmgmt_promotions
					|| $user->posmgmt_coldfood
					|| $user->posmgmt_customerutils
				),
			),
			array(
				'id' => 'possetup',
				'title' => 'Setup',
				'enabled' => $user->posmgmt_setup,
			),
			array(
				'id' => 'cashier',
				'title' => 'Cashier Users',
				'enabled' => $user->posmgmt_cashiers,
			),
			array(
				'id' => 'posscreen',
				'title' => 'POS Screens',
				'callback' => 'ScreenController.refresh()',
				'enabled' => $user->posmgmt_posscreens,
			),
			array(
				'id' => 'functionpanel', 
				'title' => 'Function Panel', 
				'callback' => 'FPAssignController.refresh()',
				'enabled' => $user->posmgmt_posscreens,
			),
			array(
				'id' => 'menumanager',
				'title' => 'Menu Manager',
				'callback' => 'MenuManagerController.refresh()',
				'enabled' => $user->posmgmt_posscreens,
			),
			array(
				'id' => 'posmodifier',
				'title' => 'Modifiers',
				'callback' => 'ModifierController.resizeScreen()',
				'enabled' => $user->posmgmt_modifiers,
			),
			array(
				'id' => 'staticmodifier',
				'title' => 'Static Modifiers',
				'enabled' => $user->posmgmt_staticmods,
			),
			array(
				'id' => 'customer',
				'title' => 'Customers',
				'enabled' => $user->posmgmt_customers,
			),
			array(
				'id' => 'cards',
				'title' => 'Card Numbers',
				'enabled' => $user->posmgmt_cardnums,
			),
			array(
				'id' => 'groups',
				'title' => 'Menu Groups',
				'enabled' => $user->posmgmt_menugroup,
			),
			array(
				'id' => 'taxes',
				'title' => 'Taxes',
				'enabled' => $user->posmgmt_taxes,
			),
			array(
				'id' => 'promos',
				'title' => 'Promotions',
				'enabled' => $user->posmgmt_promotions,
			),
			array(
				'id' => 'combos',
				'title' => 'Combos',
				'enabled' => $user->posmgmt_promotions,
			),
			array(
				'id' => 'commissary',
				'title' => 'Cold Food Orders',
				'enabled' => $user->posmgmt_coldfood,
			),
			array(
				'id' => 'utilities',
				'title' => 'Customer Utilities',
				'enabled' => $user->posmgmt_customerutils,
			),
			array(
				'id' => 'vmachine',
				'title' => 'Vending Machines',
				'enabled' => $user->posmgmt_vmachine,
			),
			array(
				'id' => 'comboOrder',
				'title' => 'Combo Group Orders',
				'enabled' => $user->posmgmt_vmachine,
			),
		);
		
		if( !IN_PRODUCTION ) {
			$navLinks[] = array(
				'id' => 'smartmenu',
				'title' => 'Smart Menu Boards',
				'enabled' => true,
			);
		}
		
		// Vend Machines
		$machineList = \EE\Model\VendMachine::getListByBusinessid( $GLOBALS['businessid'] );
		$this->_view->assign( 'machineList', $machineList );
		
		$orderTypes = array(
			1 => "Commissary", 2 => "Pick to Light", 3 => "Email"
		);
		$this->_view->assign( 'orderTypes', $orderTypes );
		
		// Route Machines
		$routeMachines = \EE\Model\VendMachine::getRouteMachineListByBusinessid( $GLOBALS['businessid'] );
		$routeMachine = array();

		if( count( $routeMachines ) && $routeMachines[0]['machine_num'] != '' ) {
			$routeMachine = $routeMachines[0];
			
			$date3 = date( "Y-m-d" );
			$date2 = date( "Y-m-d" ); 
			$today = date( "Y-m-d" );
			$yesterday = date( "Y-m-d", mktime( 0, 0, 0, date( "m" ), date( "d" ) - 1, date( "Y" ) ) );
			
			$hour = date( "G" );

			if( $hour < 10 && dayofweek( $date2 ) == "Monday" ) {
				$date2 = prevday( $date2 );
				$date2 = prevday( $date2 );
				$date2 = prevday( $date2 );
				
				$yesterday = prevday( $yesterday );
				$yesterday = prevday( $yesterday );
				$yesterday = prevday( $yesterday );
			}
			elseif( $hour < 10 ) {
				$date2 = prevday( $date2 );
				$yesterday = prevday( $yesterday );
			}
			
			$ship_dates = Kiosk::comm_inTransit_dates( $routeMachine['machine_num'], $date2 );
			
			/////get ordering businessid
			$menuType = Treat_Model_Menu_Type::getById( $routeMachine['menu_typeid'] );
			$order_bus = null;
			if ( $menuType ) {
				$order_bus = $menuType->businessid;
			}
			
			////correct order date
			$hour = date( "H" );
			$order_date = $date2;
			
			////new closed commissary
			$open = 0;
			while($open < 3){
				$temp = nextday($order_date);
				$query = "SELECT count(catercloseid) AS closed_days FROM caterclose WHERE businessid = 93 AND date = '$order_date' AND date >= '$today'";
				$result = Treat_DB_ProxyOld::query($query);
				
				$closed_days = @mysql_result($result, 0, 'closed_days');
						
				if($closed_days > 0 || dayofweek($order_date) == 'Saturday' || dayofweek($order_date) == 'Sunday'){
					$order_date = nextday($order_date);
				}
				else{
					$order_date = nextday($order_date);
					$open++;
				}	
			}
			
			///quick fix number 2
			$query = "SELECT count(catercloseid) AS closed_days FROM caterclose WHERE businessid = 93 AND date = '$order_date' AND date >= '$today'";
			$result = Treat_DB_ProxyOld::query($query);
			
			$closed_days = @mysql_result($result, 0, 'closed_days');
			
			if($closed_days > 0){
				$order_date = nextday($order_date);
			}
			
			//quick fix
			if(dayofweek($order_date) == "Saturday"){
				$order_date = nextday($order_date);
				$order_date = nextday($order_date);
			}
			
			//Hard code the date for a holiday if needed
			//For example - we set this to 2015-05-26 because Monday, 2015-05-25 was Memorial Day Holiday
			//$order_date = '2015-05-26';
			
			/*
			/////move to Monday if weekend
			if( dayofweek( $order_date ) == "Saturday" ) {
				$order_date = nextday( $order_date );
				$order_date = nextday( $order_date );
			}
			if( dayofweek( $order_date ) == "Sunday" ) {
				$order_date = nextday( $order_date );
			}
			
			///3 days
			$counter = 0;
			while( $counter < 3 ) {
				$order_date = nextday( $order_date );
				if( dayofweek( $order_date ) == "Saturday" ) {
					$order_date = nextday( $order_date );
					$order_date = nextday( $order_date );
				}
				elseif( dayofweek( $order_date ) == "Sunday" ) {
					$order_date = nextday( $order_date );
				}
				$counter++;
			}
			
			if( dayofweek( $order_date ) == "Saturday" ) {
				$order_date = nextday( $order_date );
				$order_date = nextday( $order_date );
			}
			*/
			
			////closed commissary
			/*
			$query = "SELECT count(catercloseid) AS closed_days FROM caterclose WHERE businessid = 93 AND date = '$order_date'";
			$result = Treat_DB_ProxyOld::query($query);
			
			$closed_days = @mysql_result($result, 0, 'closed_days');
					
			if($closed_days > 0){
				$order_date = nextday($order_date);
			}
			*/
			
			/*
			////new closed commissary
			$open = 0;
			while($open != 1){
				$query = "SELECT count(catercloseid) AS closed_days FROM caterclose WHERE businessid = 93 AND date = '$order_date'";
				$result = Treat_DB_ProxyOld::query($query);
				
				$closed_days = @mysql_result($result, 0, 'closed_days');
						
				if($closed_days > 0){
					$order_date = nextday($order_date);
				}
				elseif(dayofweek($order_date) == 'Saturday'){
					$order_date = nextday($order_date);
					$order_date = nextday($order_date);
				}
				else{
					$open = 1;
					break;
				}	
			}
			*/
			
			////scheduled?
			if( dayofweek( $order_date ) == "Monday" ) {
				$day = 1;
			}
			elseif( dayofweek( $order_date ) == "Tuesday" ) {
				$day = 2;
			}
			elseif( dayofweek( $order_date ) == "Wednesday" ) {
				$day = 3;
			}
			elseif( dayofweek( $order_date ) == "Thursday" ) {
				$day = 4;
			}
			elseif( dayofweek( $order_date ) == "Friday" ) {
				$day = 5;
			}
			
			
			$scheduleid = null;
			$vendMachineSchedule = Treat_Model_VendMachine_Schedule::getByMachineForDay( $routeMachine['machineid'], $day );
			if ( $vendMachineSchedule ) {
				$scheduleid = $vendMachineSchedule->id;
			}
			
			///next order
			$vmScheduleList = Treat_Model_VendMachine_Schedule::getByMachine( $routeMachine['machineid'] );
			
			foreach( $vmScheduleList as $vm ) {
				$mach_schedule[$vm->day] = $vm->id;
			}
			
			$date_tmp = date( "Y-m-d" );
			//if($hour >= 10){$date_tmp = nextday($date_tmp);}
			
			$date_count2 = 1;
			while( $date_tmp <= $order_date ) {
				$date_count2++;
				$date_tmp = nextday( $date_tmp );
			}
			
			$next_order_date = nextday( $order_date );
			for( $date_count = 1; $date_count <= 7; $date_count++ ) {
				////scheduled?
				if( dayofweek( $next_order_date ) == "Monday" ) {
					$next_order_day = 1;
				}
				elseif( dayofweek( $next_order_date ) == "Tuesday" ) {
					$next_order_day = 2;
				}
				elseif( dayofweek( $next_order_date ) == "Wednesday" ) {
					$next_order_day = 3;
				}
				elseif( dayofweek( $next_order_date ) == "Thursday" ) {
					$next_order_day = 4;
				}
				elseif( dayofweek( $next_order_date ) == "Friday" ) {
					$next_order_day = 5;
				}
				elseif( dayofweek( $next_order_date ) == "Saturday" ) {
					$next_order_day = 6;
				}
				elseif( dayofweek( $next_order_date ) == "Sunday" ) {
					$next_order_day = 7;
				}
				
				////if schedule exists
				if( is_array( $mach_schedule ) && array_key_exists( $next_order_day, $mach_schedule ) ) {
					break;
				}
				
				$next_order_date = nextday( $next_order_date );
			}
			
			$date_count = 0;
			$dc_date = $date2;
			while( $dc_date < $next_order_date ) {
				$date_count++;
				$dc_date = nextday( $dc_date );
			}
			
			///remove weekends if not 24 operation
			$temp_date = $order_date;
			$temp_days = $date_count;
			while( $temp_date <= $next_order_date ) {
				if( $operating_days <= 6 && dayofweek( $temp_date ) == "Sunday" ) {
					$temp_days--;
				}
				elseif( $operating_days <= 5 && dayofweek( $temp_date ) == "Saturday" ) {
					$temp_days--;
				}
				$temp_date = nextday( $temp_date );
			}
			
			///operating days until order arrives
			$temp_date2 = nextday( $date2 );
			$temp_days2 = 0;
			while( $temp_date2 <= $order_date ) {
				$temp_days2++;
				if( $operating_days < 7 && dayofweek( $temp_date2 ) == "Sunday" ) {
					$temp_days2--;
				}
				elseif( $operating_days < 6 && dayofweek( $temp_date2 ) == "Saturday" ) {
					$temp_days2--;
				}
				$temp_date2 = nextday( $temp_date2 );
			}
			
			/*
			///commissary closed days
			$this_date = date("Y-m-d");
			
			$query = "SELECT count(catercloseid) AS closed_days FROM caterclose WHERE businessid = 93 AND date BETWEEN '$this_date' AND '$order_date'";
			$result = Treat_DB_ProxyOld::query($query);
			
			$closed_days = mysql_result($result,0,"closed_days");
			$temp_closed = $closed_days;
			
			while( $temp_closed > 0 ) {
				$order_date = nextday( $order_date );
				if( dayofweek( $order_date ) == "Sunday" ) {
					$order_date = nextday( $order_date );
				}
				elseif( $dayofweek( $order_date ) == "Saturday" ) {
				$order_date = nextday( $order_date );
					$order_date = nextday( $order_date );
				}
				$temp_closed--;
			}
			 * 
			 */
			
			///display order
			$showday = dayofweek( $order_date );
			$shownextday = dayofweek( $next_order_date );
			
			////////////////
			
			if( $scheduleid > 0 ) {
				$business = \EE\Model\Business\Singleton::getSingleton();
				$operating_days = $business->operate;
				
				$fourteen_days = date( "Y-m-d", mktime( 0, 0, 0, date( "m" ), date( "d" ) - 14, date( "Y" ) ) );
				if( $hour < 10 ) {
					$fourteen_days = prevday( $fourteen_days );
				}
				
				$routeItemList = \EE\Model\VendMachine::getItemListForRouteMachine( $GLOBALS['businessid'], $routeMachine['machineid'], 
						$routeMachine['login_routeid'], $routeMachine['menu_typeid'], $order_date, $order_bus );
				
				$mGroupList = array();
				$mpGroupList = array();
				
				$menuItemList = array();
				while( $r = next( $routeItemList ) ) {
					$group_name = $r['groupname'];
					$pgroup_name = $r['menu_pricegroupname'];
					
					if( isset( $mGroupList[$group_name] ) ) {
						$groupIndex = $mGroupList[$group_name];
					}
					else {
						$tmp = count( $menuItemList );
						$groupIndex = $tmp;
						$mGroupList[$group_name] = $tmp;
					}
					
					if( isset( $mpGroupList[$pgroup_name] ) ) {
						$pgroupIndex = $mpGroupList[$pgroup_name];
					}
					else {
						if ( isset( $menuItemList[$groupIndex] ) ) {
							$tmp = count( $menuItemList[$groupIndex] );
						}
						else {
							$tmp = 0;
						}
						$pgroupIndex = $tmp;
						$mpGroupList[$pgroup_name] = $tmp;
					}
					
					$menu_itemid = $r["menu_item_id"];
					$item_name = $r["item_name"];
					$item_code = $r["item_code"];
					$myid = $r["myid"];
					$rollover = $r["rollover"];
					$pos_id = $r["pos_id"];
					$vend_itemid = $r["vend_itemid"];
					$amount = $r["amount"];
					
					/////////////////////
					////onhand amounts///
					/////////////////////
					////get dates
					$dates = Kiosk::last_inv_dates( $routeMachine['machine_num'], $item_code );
					$date = $dates[1];
					if( $date == "" ) {
						$date = $dates[0];
					}
					
					////last count
					$onhand2 = Kiosk::inv_onhand( $routeMachine['machine_num'], $item_code, $date );
					
					////fills
					$fills = Kiosk::fills( $routeMachine['machine_num'], $item_code, $date, $date3 );
					
					////waste
					$waste = Kiosk::waste( $routeMachine['machine_num'], $item_code, $date, $date3 );
					
					////get check detail
					$items_sold = menu_items::items_sold( $pos_id, $GLOBALS['businessid'], $date, $date3, $current_terminal );
					
					////in transit
					if( count( $ship_dates ) > 0 ) {
						$inTransit = Kiosk::comm_inTransit_amt( $ship_dates, $item_code, $routeMachine['menu_typeid'], $order_bus,
								$routeMachine['machineid'] );
					}
					else {
						$inTransit = 0;
					}
					
					if( $inTransit == 0 ) {
						$inTransit = 0;
					}
					
					////average daily sales for last 2 weeks
					$items_sold14 = menu_items::items_sold( $pos_id, $GLOBALS['businessid'], $fourteen_days, $yesterday,
							$current_terminal );
					
					$avg_daily_sales = round( $items_sold14 / ( $operating_days * 2 ), 1 );
					
					////onhand amount
					$onhand = $onhand2 + $fills - $waste - $items_sold;
					
					////suggested
					if( $onhand < 0 ) {
						$newonhand = 0;
					}
					else {
						$newonhand = $onhand;
					}
					
					if( dayofweek( $order_date ) == "Friday" ) {
						$reduce = 1;
					}
					else {
						$reduce = 1;
					}
					
					$suggested = round( ( round( $rollover * $avg_daily_sales * $date_count ) - ( $newonhand + $inTransit ) ) * $reduce );
					$show_suggested = "round( (round( $rollover * $avg_daily_sales * $date_count ) - ($newonhand + $inTransit )) * $reduce )";
					
					if( $suggested == 1 ) {
						$suggested = 2;
					}
					
					if( $myid < 1 ) {
						$item_code = "<strike>$item_code</strike>";
						$item_name = "<strike>$item_name</strike>";
						$amt_disable = "DISABLED";
					}
					else {
						$amt_disable = "";
					}
					
					if( $suggested <= 0 ) {
						$fcolor = "#999999";
					}
					else {
						$fcolor = "black";
					}
					
					////////////////////////
					
					$menuItemList[$groupIndex][$pgroupIndex][] = array(
						'menu_itemid' => $menu_itemid,
						'item_name' => $item_name,
						'item_code' => $item_code,
						'myid' => $myid,
						'rollover' => $rollover,
						'pos_id' => $pos_id,
						'vend_itemid' => $vend_itemid,
						'amount' => $amount,
						'amt_disable' => $amt_disable,
						'avg_daily_sales' => $avg_daily_sales,
						'onhand' => $onhand,
						'inTransit' => $inTransit,
						'suggested' => $suggested,
						'show_suggested' => $show_suggested,
						'groupname' => $group_name,
						'menu_pricegroupname' => $pgroup_name,
						'fcolor' => $fcolor,
					);
				}
			}
			////////////////
		}
		else {
			$routeMachine = array();
		}
		
		$this->_view->assign( 'routeMachine', $routeMachine );
		$this->_view
			->assign( 'routeInfo', array( 
				'showday' => $showday,
				'order_date' => $order_date,
				'shownextday' => $shownextday,
				'next_order_date' => $next_order_date,
				'date_count' => $date_count,
				'temp_days' => $temp_days,
				'order_bus' => $order_bus,
				'scheduleid' => $scheduleid,
			)
		);
		$this->_view->assign( 'menuItemList', $menuItemList );
		
		// manufacturers
		$manufacturers = \EE\Model\Manufacturer::getAll( 'name' );
		$this->_view->assign( 'manufacturers', $manufacturers );
		
		// departments
		$departments = \EE\Model\Menu\GroupsNew::getListByBusinessid( $GLOBALS['businessid'] );
		$this->_view->assign( 'departments', $departments );
		
		// order types
		$business = EE\Model\Business\Singleton::getSingleton();
		$ordertypes = \EE\Model\Menu\Item\OrderType::buildMenuOrderTypes($business->companyid);
		$this->_view->assign( 'ordertypes', $ordertypes );
		
		// modifier groups
		$modifierGroups = Treat_Model_Aeris_Modifier_Group::getByBusinessid( $GLOBALS['businessid'] ) ? : array();
		$this->_view->assign( 'modifierGroups', $modifierGroups );
		
		// taxes
		$taxes = \EE\Model\Menu\Tax::getListByBusinessid( $GLOBALS['businessid'] );
		$this->_view->assign( 'taxes', $taxes );
		
		// aeris modes
		$aerisModes = Treat_Model_Aeris_OperatingMode::getModes();
		$this->_view->assign( 'aerisModes', $aerisModes );
		
		// aeris units
		$aerisUnits = Treat_Model_ClientPrograms::getKiosksByBusiness( $GLOBALS['businessid'] );
		foreach( $aerisUnits as $unit ) {
			$components = $unit->components;
			$tttt = usort( $components, function( $u1, $u2 ) {
				$u1i = $u1->isImportant( );
				$u2i = $u2->isImportant( );
				
				if( $u1i && !$u2i ) {
					return -1;
				}
				
				if( $u2i && !$u1i ) {
					return 1;
				}
				
				$u1u = $u1->isUnimportant( );
				$u2u = $u2->isUnimportant( );
				
				if( $u1u && !$u2u ) {
					return 1;
				}
				
				if( $u2u && !$u1u ) {
					return -1;
				}
				
				$u1n = strtolower( $u1->component_name );
				$u2n = strtolower( $u2->component_name );
				
				if( $u1n == $u2n ) {
					return 0;
				}
				
				return ( $u1n > $u2n ) ? 1 : -1;
			} );
			$unit->components = $components;
		}
		$this->_view->assign( 'aerisUnits', $aerisUnits );
		
		// scheduled specials
		$scheduledSpecials = \EE\Model\Menu\ServeryStation::getServeryGroups( $GLOBALS['businessid'] );
		$this->_view->assign( 'scheduledSpecials', $scheduledSpecials );

		$promos = \EE\Model\Promotion::getList($GLOBALS['businessid'], 2);
		$this->_view->assign( 'comboPromotions', $promos );

		$image_display_types = \EE\Model\Image::imageDisplayTypes();
		$this->_view->assign( 'imageDisplayTypes', $image_display_types );

		$this->slidenavRender( $navLinks, $navSecurity, $security_lvl );
	}
	
	public function getCashiers() {
		$this->startAjax();
		
		$businessid = intval( $_REQUEST['bid'] );
		
		$cashierList = Treat_Model_User_Cashier::getCashierList( $businessid );
		$nonCashierList = Treat_Model_User_Cashier::getNonCashierList( $businessid );
		
		echo json_encode( array( 'cashiers' => $cashierList, 'nonCashiers' => $nonCashierList ) );
		
		return false;
	}
	
	public function updateCashier() {
		return AjaxWrapper::process( array(
			AjaxWrapperField::bid( ),
			new AjaxWrapperField( 'cashier_loginid', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID, 'Invalid login id' ),
			new AjaxWrapperField( 'cashier_passcode', AjaxWrapperField::FIELD_STRING, AjaxWrapperField::OPTIONS_NAME | AjaxWrapperField::OPT_STRINGNUMERIC, 'Invalid passcode' ),
			new AjaxWrapperField( 'cashier_groupName', AjaxWrapperField::FIELD_STRING, AjaxWrapperField::OPTIONS_NAME, 'Invalid security group' )
		), function( array $inputs ) {
			$rv = array( );
			
			$cashier = Treat_Model_User_Cashier::getById( $inputs['cashier_loginid'] );
			if( !$cashier ) {
				$cashier = Treat_Model_User_Cashier::create( $inputs['cashier_loginid'], $inputs['bid'], $inputs['cashier_passcode'], $inputs['cashier_groupName'] );
				$rv['message'] = 'Cashier created successfully.';
			} else {
				$cashier->update( $inputs['bid'], $inputs['cashier_passcode'], $inputs['cashier_groupName'] );
				$rv['message'] = 'Cashier updated successfully.';
			}
			$rv['id'] = $cashier->loginid;
			
			return $rv;
		} );
	}
	
	public function deleteCashier() {
		$this->startAjax();
		
		$loginid = intval( $_REQUEST['id'] );
		$businessid = intval( $_REQUEST['bid'] );
		
		$rv = array();
		
		try {
			$link = Treat_Model_User_Cashier_Link::getById( array( 'loginid' => $loginid, 'businessid' => $businessid ) );
			$link->delete();
			
			$remainingLinks = Treat_Model_User_Cashier_Link::getBusinessList( $loginid );
			if( empty( $remainingLinks ) ) {
				$cashier = Treat_Model_User_Cashier::getById( $loginid );
				$cashier->delete();
			}
			
			$rv['id'] = $loginid;
			$rv['message'] = 'Cashier deleted successfully.';
		} catch( Exception $e ) {
			$rv['error'] = $e->getMessage();
		}
		
		echo json_encode( $rv );
		
		return false;
	}

	public function getMachineInfo() {
		return AjaxWrapper::process( array(
			AjaxWrapperField::bid()
		), function( array $inputs ) {
			$businessid = $inputs['bid'];
			
			$tokens = \EE\Model\Aeris\Setting::getMapTokens();
			$rv = \EE\Model\Aeris\Setting::getMapSettings( $businessid, $tokens );
			
			return $rv;
		} );
	}
	
	public function updateSetupDisplay() {
		return AjaxWrapper::process( array(
			AjaxWrapperField::bid(),
			
			// display
			new AjaxWrapperField( 'dept_rows', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID, 'Invalid department rows' ),
			new AjaxWrapperField( 'dept_cols', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID, 'Invalid department cols' ),
			new AjaxWrapperField( 'dept_scrollbar', AjaxWrapperField::FIELD_BOOLEAN ),
			new AjaxWrapperField( 'plu_rows', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID, 'Invalid product rows' ),
			new AjaxWrapperField( 'plu_cols', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID, 'Invalid product cols' ),
			new AjaxWrapperField( 'plu_scrollbar', AjaxWrapperField::FIELD_BOOLEAN ),
			new AjaxWrapperField( 'misc_register', AjaxWrapperField::FIELD_BOOLEAN ),
			
			// features
			new AjaxWrapperField( 'feat_timeout', AjaxWrapperField::FIELD_BOOLEAN ),
			new AjaxWrapperField( 'feat_timeout_secs', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPT_INTEGER | AjaxWrapperField::OPT_GTZERO, 'Invalid timeout length' ),
			new AjaxWrapperField( 'feat_timeout_countdown', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPT_INTEGER | AjaxWrapperField::OPT_GTZERO, 'Invalid timeout countdown length' ),
			new AjaxWrapperField( 'feat_multilingual', AjaxWrapperField::FIELD_BOOLEAN ),
			new AjaxWrapperField( 'feat_dinein', AjaxWrapperField::FIELD_BOOLEAN ),
			new AjaxWrapperField( 'feat_fingerprint', AjaxWrapperField::FIELD_BOOLEAN ),
			new AjaxWrapperField( 'feat_voucher', AjaxWrapperField::FIELD_BOOLEAN ),
			new AjaxWrapperField( 'feat_memberonly', AjaxWrapperField::FIELD_BOOLEAN ),
			
			// payment
			new AjaxWrapperField( 'pay_ccprocessor', AjaxWrapperField::FIELD_ENUM, array( 'heartland', 'mercury', 'verifone' ), 'Invalid credit card processor' ),
			new AjaxWrapperField( 'pay_ccheartland_siteid', AjaxWrapperField::FIELD_STRING ),
			new AjaxWrapperField( 'pay_ccheartland_licenseid', AjaxWrapperField::FIELD_STRING ),
			new AjaxWrapperField( 'pay_ccheartland_deviceid', AjaxWrapperField::FIELD_STRING ),
			new AjaxWrapperField( 'pay_ccheartland_username', AjaxWrapperField::FIELD_STRING ),
			new AjaxWrapperField( 'pay_ccheartland_password', AjaxWrapperField::FIELD_STRING ),
			new AjaxWrapperField( 'pay_ccmercury_merchantid', AjaxWrapperField::FIELD_STRING ),
			new AjaxWrapperField( 'pay_ccmercury_operatorid', AjaxWrapperField::FIELD_STRING ),
			new AjaxWrapperField( 'pay_ccmercury_password', AjaxWrapperField::FIELD_STRING ),
			
			// advanced
			new AjaxWrapperField( 'adv_logo', AjaxWrapperField::FIELD_STRING ),
			new AjaxWrapperField( 'adv_logo_disable', AjaxWrapperField::FIELD_BOOLEAN ),
			new AjaxWrapperField( 'adv_branding', AjaxWrapperField::FIELD_STRING ),
			new AjaxWrapperField( 'adv_branding_long', AjaxWrapperField::FIELD_STRING ),
			new AjaxWrapperField( 'adv_logging', AjaxWrapperField::FIELD_BOOLEAN ),
			new AjaxWrapperField( 'adv_log_level', AjaxWrapperField::FIELD_ENUM, array( 'ALL', 'FUNCTION', 'CASH', 'PURCHASE' ) )
		), function( array $inputs ) {
			$businessid = $inputs['bid'];
			unset( $inputs['bid'] );
			\EE\Model\Aeris\Setting::updateMapSettings( $businessid, $inputs );
			return array( 'id' => $businessid, 'message' => 'Successfully updated display settings' );
		} );
	}
	
	public function setupRequestSettings() {
		return AjaxWrapper::process( array(
			AjaxWrapperField::bid()
		), function( array $inputs ) {
			\EE\Model\Aeris\Setting::requestSettings( $inputs['bid'] );
			return array( 'id' => $inputs['bid'], 'message' => 'Settings will be requested from the terminal' );
		} );
	}
	
	public function updateSetupDept() {
		$this->startAjax();
		
		$businessid = intval( $_REQUEST['bid'] );
		$key['vivipos.fec.settings.DepartmentRows'] = intval( $_REQUEST['dept_rows'] );
		$key['vivipos.fec.settings.DepartmentCols'] = intval( $_REQUEST['dept_cols'] );
		$key['vivipos.fec.settings.layout.HideDeptScrollbar'] = intval( $_REQUEST['dept_scrollbar'] );
		
		\EE\Model\Aeris\Setting::updateSettings( $businessid, $key );
		
		echo json_encode( array( 'id' => $businessid, 'message' => 'Successfully updated department panel settings' ) );
		
		return false;
	}
	
	public function updateSetupPlu() {
		$this->startAjax();
		
		$businessid = intval( $_REQUEST['bid'] );
		$key['vivipos.fec.settings.PluRows'] = intval( $_REQUEST['plu_rows'] );
		$key['vivipos.fec.settings.PluCols'] = intval( $_REQUEST['plu_cols'] );
		$key['vivipos.fec.settings.layout.HidePLUScrollbar'] = intval( $_REQUEST['plu_scrollbar'] );
		
		\EE\Model\Aeris\Setting::updateSettings( $businessid, $key );
		
		echo json_encode( array( 'id' => $businessid, 'message' => 'Successfully updated product panel settings' ) );
		
		return false;
	}
	
	public function updateSetupMisc() {
		$this->startAjax();
		
		$businessid = intval( $_REQUEST['bid'] );
		$key['vivipos.fec.settings.layout.RegisterAtLeft'] = intval( $_REQUEST['misc_register'] );
		
		\EE\Model\Aeris\Setting::updateSettings( $businessid, $key );
		
		echo json_encode( array( 'id' => $businessid, 'message' => 'Successfully updated miscellaneous settings' ) );
		
		return false;
	}
	
	public function updateSetupAdv() {
		$this->startAjax();
		
		$businessid = intval( $_REQUEST['bid'] );
		$key['vivipos.fec.settings.layout.aerispos.receiptHeaderImage'] = $_REQUEST['adv_logo'];
		$key['vivipos.fec.settings.layout.aerispos.noReceiptHeader'] = $_REQUEST['adv_logo_disable'];
		$key['vivipos.fec.settings.layout.aerispos.EPayBranding'] = $_REQUEST['adv_branding'];
		$key['vivipos.fec.settings.layout.aerispos.EPayBrandingLong'] = $_REQUEST['adv_branding_long'];
		
		\EE\Model\Aeris\Setting::updateSettings( $businessid, $key );
		
		echo json_encode( array( 'id' => $businessid, 'message' => 'Successfully updated advanced settings' ) );
		
		return false;
	}
	
	public function getScreens() {
		$this->startAjax(true);
		
		$businessid = intval( $_REQUEST['bid'] );
		
		$rv = array();
		$dept_rows = \EE\Model\Aeris\Setting::getValue( $businessid, 'vivipos.fec.settings.DepartmentRows' );
		$dept_cols = \EE\Model\Aeris\Setting::getValue( $businessid, 'vivipos.fec.settings.DepartmentCols' );
		$plu_rows = \EE\Model\Aeris\Setting::getValue( $businessid, 'vivipos.fec.settings.PluRows' );
		$plu_cols = \EE\Model\Aeris\Setting::getValue( $businessid, 'vivipos.fec.settings.PluCols' );
		
		//$groupList = Treat_Model_Menu_Group::getListByBusinessid( $businessid );
		$promoGroupsList = Treat_Model_Promotion_Group::getListByBusinessid( $businessid, 'display_order', true );
		
		$rv['screens'] = array();
		
		$arrayfn = function (&$value, $key, $data) {
			$value['type'] = $data;
		};
		
		//array_walk( $groupList, $arrayfn, 'D' );
		array_walk( $promoGroupsList, $arrayfn, 'G' );
		
		//$depts = array_merge( $groupList, $promoGroupsList );
		$depts = $promoGroupsList;
		/*usort( $depts, function( $a, $b ) {
			$aa = $a['display_order'];
			$bb = $b['display_order'];
			return ( $aa == $bb ) ? 0 : ( ( $aa < $bb ) ? -1 : 1 );
		} );*/
		
		$rv['screens']['__root__'] = array(
			'items' => $depts, 'rows' => $dept_rows, 'cols' => $dept_cols
		);
		
		foreach( $depts as $id => $dept ) {
			if( $dept['type'] == 'D' ) {
				$deptItems = Treat_Model_Menu_Item_New::getListByGroup( $businessid, $dept['id'], true );
			}
			elseif( $dept['type'] == 'G' ) {
				$deptItems = Treat_Model_Promotion_Group_Detail::getItemListByGroup( $dept['id'], true );
				foreach( $deptItems as &$item ) {
					$item['is_special'] = \EE\Menu\Merger::isMergeItem( $item['upc'] );
					$item['is_combo'] = \EE\Menu\MergerCombo::isMergeItem( $item['upc'] );
				}
			}
			
			$rv['screens'][$id] = array(
				'items' => $deptItems, 'rows' => $dept['group_rows'] ? : $plu_rows, 'cols' => $dept['group_cols'] ? : $plu_cols
			);
		}
		
		echo json_encode( $rv );
		
		return false;
	}

	public function updateScreenOrder() {
		$this->startAjax();
		
		$businessid = intval( $_POST['bid'] );
		$screenId = intval( $_POST['screenId'] );
		$screenType = preg_filter( '/^([DGI]).*$/', '$1', $_POST['screenType'] );
		$order = $_POST['order'];
		
		$error = array();
		
		if( $screenId > 0 )
			Treat_Model_Promotion_Group_Detail::resetProcessed( $screenId );

		$ids = array();

		foreach( $order as $item ) {
			switch( $item['type'] ) {
				case 'G':
					try {
						$group = Treat_Model_Promotion_Group::getById( intval( $item['id'] ) );
						
						if( !$group ) {
							throw new SiTech_Exception( 'Failed to update group ' . $item['id']);
						}
						
						Treat_Model_Promotion_Group::update( $businessid, intval( $item['id'] ), null, null, null, null, null,
								intval( $item['y'] ), intval( $item['x'] ) );
					} catch( Exception $e ) {
						$error[] = $e->getMessage();
					}
					break;
				
				case 'I':
					try {
						Treat_Model_Promotion_Group_Detail::update( $item['id'], $screenId,
								array( 'x' => $item['x'], 'y' => $item['y'] ), null, true );
						$ids[] = intval( $item['id'] );
					} catch( Exception $e ) {
						$error[] = $e->getMessage();
					}
					break;
			}
		}

		if ( $ids ) {
			Treat_Model_KioskSync::bulkSync( $businessid, $ids, Treat_Model_KioskSync::TYPE_ITEM );
			Treat_Model_KioskSync::bulkSync( $businessid, $ids, Treat_Model_KioskSync::TYPE_ITEM_TAX );
		}
		
		$rv = array();
		if( !empty( $error ) ) {
			$rv['error'] = 'One or more errors occurred:' . "\n" . implode( "\n", $error );
		}
		else {
			if( $screenId > 0 )
				Treat_Model_Promotion_Group_Detail::cleanUnprocessed( $screenId );
			
			$rv['message'] = 'Order updated successfully';
		}
		echo json_encode( $rv );
		
		return false;
	}
	
	public function updateScreenButton() {
		$this->startAjax();
		
		$syncTypes = array(
			'D' => Treat_Model_KioskSync::TYPE_GROUP,
			'G' => Treat_Model_KioskSync::TYPE_PROMO_GROUP,
			'I' => Treat_Model_KioskSync::TYPE_ITEM,
		);
		
		$businessid = intval( $_POST['bid'] );
		$itemId = intval( $_POST['button_id'] );
		$itemType = preg_filter( '/^([DGI]).*$/', '$1', $_POST['button_type'] );
		$itemColor = intval( $_POST['button_pos_color'] );
		$groupId = $itemType == 'I' ? intval( $_POST['button_promo_group_id'] ) : 0;
		
		$rv = array();
		
		try {
			switch( $itemType ) {
				case 'D':
					$item = Treat_Model_Menu_Group::getById( $itemId );
					break;
				
				case 'G':
					$item = Treat_Model_Promotion_Group::getById( $itemId );
					break;
				
				case 'I': /*$item = Treat_Model_Menu_ScreenOrder::getById( array( 'businessid' => $businessid, 'itemId' => $itemId ) );
					if( !$item ) {
						$item = new Treat_Model_Menu_ScreenOrder;
						$item->businessid = $businessid;
						$item->itemId = $itemId;
					}*/
					Treat_Model_Promotion_Group_Detail::update( $itemId, $groupId, NULL, $itemColor );
					$item = true;
					break;
			}
			
			if( !$item )
				throw new SiTech_Exception( 'Couldn\'t find item ' . $itemId);
			
			if( is_object( $item ) ) {
				$item->pos_color = $itemColor;
				$item->save();
			}
			
			Treat_Model_KioskSync::addSync( $businessid, $itemId, $syncTypes[$itemType] );
			
			$rv['id'] = $itemId;
			$rv['message'] = 'Item updated successfully';
		} catch( Exception $e ) {
			$rv['error'] = $e->getMessage();
		}
		
		echo json_encode( $rv );
		
		return false;
	}
	
	public function generateUPC() {
		$this->startAjax();
		
		$rv = array();
		
		$businessid = intval( $_REQUEST['bid'] );
		$prefix = $_REQUEST['prefix'];
		
		if( !( $businessid > 0 ) ) {
			$rv['error'] = "Invalid business id";
		}
		elseif( strlen( $prefix ) > 0 && !preg_match( '/^\d*$/', $prefix ) ) {
			$rv['error'] = "Invalid prefix";
		}
		else {
			do {
				$upc = static::randomUPC( $prefix );
			} while( Treat_Model_Menu_Item_New::getCount( 'businessid = ' . $businessid . ' AND upc = "' . $upc . '"' ) > 0 );
			
			$rv['upc'] = $upc;
		}
		
		echo json_encode( $rv );
		
		return false;
	}
	
	private static function randomUPC( $prefix ) {
		$upc = $prefix ? : '';
		
		while( strlen( $upc ) < 12 ) {
			$upc .= mt_rand( 0, 9 );
		}
		
		$upcNums = str_split( $upc );
		
		$sum = 0;
		$odd = true;
		for( $i = count( $upcNums ) - 1; $i >= 0; $i-- ) {
			if( $odd )
				$sum += 3 * intval( $upcNums[$i] );
			else $sum += intval( $upcNums[$i] );
			$odd = !$odd;
		}
		
		$check = 10 - ( $sum % 10 );
		if( $check == 10 )
			$check = 0;
		
		$upc .= $check;
		
		return $upc;
	}
	
	public function screenGetItemList() {
		$this->startAjax();
		
		$rv = array();
		
		$businessid = intval( $_REQUEST['bid'] );
		$screenId = intval( $_REQUEST['screenId'] );
		$screenType = $_REQUEST['screenType'] == 'D' ? 'D' : 'G';
		
		$group = null;
		
		switch( $screenType ) {
			case 'G':
				$group = Treat_Model_Promotion_Group::getById( $screenId );
				break;
		}
		
		if( !( $businessid > 0 ) ) {
			$rv['error'] = 'Invalid business id';
		}
		elseif( !$group ) {
			$rv['error'] = 'Invalid screen id';
		}
		else {
			try {
				$rv['data'] = Treat_Model_Menu_Item_New::getListNotInPromoGroup( $businessid, $screenId );
			} catch( PDOException $e ) {
				$rv['error'] = 'A database error has occurred.';
			} catch( Exception $e ) {
				$rv['error'] = $e->getMessage();
			}
		}
		
		echo json_encode( $rv );
		
		return false;
	}
	
	public function screenGetGroupList() {
		$this->startAjax();
		
		$rv = array();
		
		$businessid = intval( $_REQUEST['bid'] );
		
		if( !( $businessid > 0 ) ) {
			$rv['error'] = 'Invalid business id';
		}
		else {
			try {
				$rv['data'] = Treat_Model_Promotion_Group::getListByBusinessid( $businessid, null, true, 0 );
			} catch( PDOException $e ) {
				$rv['error'] = 'A database error has occurred.';
			} catch( Exception $e ) {
				$rv['error'] = $e->getMessage();
			}
		}
		
		echo json_encode( $rv );
		
		return false;
	}
	
	public function screenAddItem() {
		$this->startAjax();
		
		$rv = array();
		
		$businessid = intval( $_REQUEST['bid'] );
		$screenId = intval( $_REQUEST['screenId'] );
		$screenType = $_REQUEST['screenType'] == 'D' ? 'D' : 'G';
		
		$group = null;
		
		switch( $screenType ) {
			case 'G':
				$group = Treat_Model_Promotion_Group::getById( $screenId );
				break;
		}
		
		$items = $_REQUEST['items'];
		array_walk( $items, intval );
		
		if( !( $businessid > 0 ) ) {
			$rv['error'] = 'Invalid business id';
		}
		elseif( !$group ) {
			$rv['error'] = 'Invalid screen id';
		}
		else {
			try {
				foreach( $items as $item ) {
					if( $item > 0 ) {
						Treat_Model_Promotion_Group_Detail::addItemToGroup( $businessid, $screenId, $item );
					}
				}
				
				$rv['success'] = true;
			} catch( PDOException $e ) {
				$rv['error'] = 'A database error has occurred.';
			} catch( Exception $e ) {
				$rv['error'] = $e->getMessage();
			}
		}
		
		echo json_encode( $rv );
		
		return false;
	}
	
	public function screenRemoveItem() {
		$this->startAjax();
		
		$rv = array();
		
		$businessid = intval( $_REQUEST['bid'] );
		$screenId = intval( $_REQUEST['screenId'] );
		$screenType = $_REQUEST['screenType'] == 'D' ? 'D' : 'G';
		
		$group = null;
		
		switch( $screenType ) {
			case 'G':
				$group = Treat_Model_Promotion_Group::getById( $screenId );
				break;
		}
		
		$item = intval( $_REQUEST['itemId'] );
		
		if( !( $businessid > 0 ) ) {
			$rv['error'] = 'Invalid business id';
		}
		elseif( !$group ) {
			$rv['error'] = 'Invalid screen id';
		}
		else {
			try {
				if( $item > 0 ) {
					Treat_Model_Promotion_Group_Detail::removeItemFromGroup( $businessid, $screenId, $item );
					\EE\Menu\Merger::cleanSpecialItem( $item );
					\EE\Menu\MergerCombo::cleanSpecialItem( $item );
				}
				
				$rv['success'] = true;
			} catch( PDOException $e ) {
				$rv['error'] = 'A database error has occurred.';
			} catch( Exception $e ) {
				$rv['error'] = $e->getMessage();
			}
		}
		
		echo json_encode( $rv );
		
		return false;
	}
	
	public function screenUpdateGroup() {
		$this->startAjax();
		
		$rv = array();
		
		$businessid = intval( $_REQUEST['bid'] );
		$groupId = intval( $_REQUEST['screenGroup_id'] );
		$groupName = $_REQUEST['screenGroup_name'];
		
		if( !( $businessid > 0 ) ) {
			$rv['error'] = 'Invalid business id';
		}
		elseif( !( strlen( $groupName ) > 0 ) ) {
			$rv['error'] = 'Invalid group name';
		}
		else {
			try {
				if( $groupId > 0 ) {
					Treat_Model_Promotion_Group::update( $businessid, $groupId, $groupName );
				}
				else {
					Treat_Model_Promotion_Group::create( $businessid, $this->_genUuid(), $groupName );
				}
				
				$rv['success'] = true;
			} catch( PDOException $e ) {
				$rv['error'] = 'A database error has occurred.';
			} catch( Exception $e ) {
				$rv['error'] = $e->getMessage();
			}
		}
		
		echo json_encode( $rv );
		
		return false;
	}
	
	public function screenAddGroup() {
		return $this->screenAddOrRemoveGroup( true );
	}
	
	public function screenRemoveGroup() {
		return $this->screenAddOrRemoveGroup( false );
	}
	
	protected function screenAddOrRemoveGroup( $add ) {
		$this->startAjax();
		
		$rv = array();
		
		$businessid = intval( $_REQUEST['bid'] );
		$groupId = intval( $_REQUEST['groupId'] );
		
		if( $groupId > 0 )
			$items = array(
				$groupId
			);
		else $items = $_REQUEST['items'];
		
		if( !( $businessid > 0 ) ) {
			$rv['error'] = 'Invalid business id';
		}
		else {
			try {
				foreach( $items as $item ) {
					if( !( $item > 0 ) )
						throw new Exception( 'Invalid group id');
					Treat_Model_Promotion_Group::update( $businessid, $item, null, $add );
					if( $add )
						Treat_Model_Promotion_Group::resetGrid( $businessid, $item );
				}
				$rv['success'] = true;
			} catch( PDOException $e ) {
				$rv['error'] = 'A database error has occurred.';
			} catch( Exception $e ) {
				$rv['error'] = $e->getMessage();
			}
		}
		
		echo json_encode( $rv );
		
		return false;
	}
	
	public function getModifiers() {
		return AjaxWrapper::process( 
				array( 
					new AjaxWrapperField( 'bid', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID, 'Invalid business id'),
				),
				function (array $inputs) {
					$rv = array();
					
					$groups = Treat_Model_Aeris_Modifier_Group::getByBusinessid( $inputs['bid'] );
					
					$rows = 10;
					$cols = 5;
					
					$rv['modifiers'] = array();
					$rv['modifiers']['__root__'] = array(
						'items' => array(), 'rows' => $rows, 'cols' => $cols
					);
					
					foreach( $groups as $group ) {
						/* @var $group Treat_Model_Aeris_Modifier_Group */
						$rv['modifiers']['__root__']['items'][] = $group->toArray();
						$modifiers = $group->getModifiers();
						Treat_Model_Aeris_Modifier_Group::modelToArray( $modifiers );
						$rv['modifiers'][] = array(
							'items' => $modifiers, 'rows' => $rows, 'cols' => $cols
						);
					}
					
					return $rv;
				} );
	}
	
	public function modifierUpdateGroup() {
		return AjaxWrapper::process( 
				array( 
					new AjaxWrapperField( 'bid', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID, 'Invalid business id'),
					new AjaxWrapperField( 'modifierGroup_id', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_NEWID,
							'Invalid modifier group id'),
					new AjaxWrapperField( 'modifierGroup_name', AjaxWrapperField::FIELD_STRING, AjaxWrapperField::OPTIONS_NAME,
							'Invalid group name'),
					new AjaxWrapperField( 'modifierGroup_type', AjaxWrapperField::FIELD_ENUM, array( 'single', 'multiple' ),
							'Invalid selection type'),
					new AjaxWrapperField( 'modifierGroup_newLine', AjaxWrapperField::FIELD_BOOLEAN),
					new AjaxWrapperField( 'limit_min', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_NEWID,
						'Invalid modifier Minimum'),
					new AjaxWrapperField( 'limit_max', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_NEWID,
						'Invalid modifier Maximum'),
					new AjaxWrapperField( 'amount_free', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_NEWID,
						'Invalid modifier Amount Free'),
				),
				function (array $inputs) {
					if( $inputs['modifierGroup_id'] > 0 )
						$group = Treat_Model_Aeris_Modifier_Group::getById( $inputs['modifierGroup_id'] );
					else $group = new Treat_Model_Aeris_Modifier_Group();
					
					$group->businessid = $inputs['bid'];
					$group->name = $inputs['modifierGroup_name'];
					$group->type = $inputs['modifierGroup_type'];
					$group->newLine = $inputs['modifierGroup_newLine'];
					$group->limit_max = $inputs['limit_max'];
					$group->limit_min = $inputs['limit_min'];
					$group->amount_free = $inputs['amount_free'];
					
					$group->save();
				} );
	}
	
	public function modifierDeleteGroup() {
		return AjaxWrapper::process( 
				array( 
					new AjaxWrapperField( 'bid', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID, 'Invalid business id'),
					new AjaxWrapperField( 'groupId', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID,
							'Invalid modifier group id'),
				),
				function (array $inputs) {
					$group = Treat_Model_Aeris_Modifier_Group::getById( $inputs['groupId'] );
					$group->delete();
				} );
	}
	
	public function modifierUpdateItem() {
		return AjaxWrapper::process( 
				array( 
					new AjaxWrapperField( 'modifierItem_id', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_NEWID,
						'Invalid modifier id'),
					new AjaxWrapperField( 'modifierItem_groupId', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID,
						'Invalid modifier group id'),
					new AjaxWrapperField( 'modifierItem_name', AjaxWrapperField::FIELD_STRING, AjaxWrapperField::OPTIONS_NAME,
						'Invalid modifier name'),
					new AjaxWrapperField( 'modifierItem_price', AjaxWrapperField::FIELD_DECIMAL, null, 'Invalid modifier price'),
					new AjaxWrapperField( 'modifierItem_preset', AjaxWrapperField::FIELD_BOOLEAN),
					new AjaxWrapperPostField( 'pos_color', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_NUMERAL,
						'Invalid button color'),
					new AjaxWrapperPostField( 'use_product_image', AjaxWrapperField::FIELD_BOOLEAN),
					new AjaxWrapperPostField( 'hide_text', AjaxWrapperField::FIELD_BOOLEAN),
					new AjaxWrapperPostField( 'image_display_type', AjaxWrapperField::FIELD_STRING,
						AjaxWrapperField::OPT_NOTNULL & AjaxWrapperField::OPT_NOTEMPTY, 'Invalid Image Display Type'),
					new AjaxWrapperPostField( 'icon_color', AjaxWrapperField::FIELD_STRING,  'Invalid Image Display Type'),
				),
				function (array $inputs) {
					if( $inputs['modifierItem_id'] > 0 ) {
						$modifier = Treat_Model_Aeris_Modifier::getById( $inputs['modifierItem_id'] );
						$new = false;
					} else {
						$modifier = new Treat_Model_Aeris_Modifier();
						$new = true;
					}
					
					$modifier->modifierGroupId = $inputs['modifierItem_groupId'];
					$modifier->name = $inputs['modifierItem_name'];
					$modifier->price = $inputs['modifierItem_price'];
					$modifier->preset = $inputs['modifierItem_preset'];
					$modifier->pos_color = $inputs['pos_color'];
					$modifier->use_product_image = $inputs['use_product_image'];
					$modifier->hide_text = $inputs['hide_text'];
					$modifier->image_display_type = $inputs['image_display_type'];
					$modifier->icon_color = $inputs['icon_color'];

					if ( $new ) {
						$modifier->sort_key = $modifier->getNextSortKey();
					}
					
					$modifier->save();
				} );
	}
	
	public function modifierDeleteItem() {
		return AjaxWrapper::process( 
				array( 
					new AjaxWrapperField( 'bid', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID, 'Invalid business id'),
					new AjaxWrapperField( 'id', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID, 'Invalid modifier id'),
				),
				function (array $inputs) {
					$modifier = Treat_Model_Aeris_Modifier::getById( $inputs['id'] );
					$modifier->delete();

					$images = \EE\Model\Image::getImages( $inputs['id'], \EE\Model\Image::$CATEGORY_MODIFIER );
					if ( $images ) {
						foreach ( $images as $image ) {
							$image->delete();
						}
					}
				} );
	}
	
	public function updateModifierButton() {
		return AjaxWrapper::process( 
				array(
					new AjaxWrapperField( 'bid', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID, 'Invalid business id'),
					new AjaxWrapperField( 'modbutton_id', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID,
							'Invalid modifier id'),
					new AjaxWrapperField( 'modbutton_pos_color', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID,
							'Invalid button color'),
				),
				function (array $inputs) {
					$modifier = Treat_Model_Aeris_Modifier::getById( $inputs['modbutton_id'] );
					$modifier->pos_color = $inputs['modifier_pos_color'];
					$modifier->save();
				} );
	}

	public function moveModifierButton() {
		return AjaxWrapper::process(
			array(
				new AjaxWrapperField( 'mod_to_move_id', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID,
					'Invalid modifier id'),
				new AjaxWrapperField( 'existing_mod_id', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID,
					'Invalid existing modifier id'),
			),
			function (array $inputs) {
				$mod_to_move = Treat_Model_Aeris_Modifier::getById( $inputs['mod_to_move_id'] );
				$existing_mod = Treat_Model_Aeris_Modifier::getById( $inputs['existing_mod_id'] );

				if ( $mod_to_move && $existing_mod ) {
					if ( $mod_to_move->modifierGroupId == $existing_mod->modifierGroupId ) {
						$all_mods = \EE\Model\Aeris\Modifier::getByGroupId( $mod_to_move->modifierGroupId );

						$i = 1;
						$sort_first = true;

						$ids = array();
						$bid = $mod_to_move->modifierGroup()->businessid;

						foreach( $all_mods as $mod ) {
							if ( $mod->id == $existing_mod->id ) {
								if ( $sort_first ) {
									$mod_to_move->updateSortKey( $i );
									$ids[] = $mod_to_move->id;
									$i++;
								} else {
									$mod->updateSortKey( $i );
									$ids[] = $mod->id;
									$i++;
									$mod_to_move->updateSortKey( $i );
									$ids[] = $mod_to_move->id;
									$i++;
									continue;
								}
							} elseif ( $mod->id == $mod_to_move->id ) {
								$sort_first = false;
								continue;
							}
							$mod->updateSortKey( $i );
							$ids[] = $mod->id;
							$i++;
						}

						if ( $ids ) {
							\EE\Model\KioskSync::bulkSync( $bid, $ids, \EE\Model\KioskSync::TYPE_MODIFIER );
						}

					} else {
						throw new Exception( 'Invalid Modifier Group ID');
					}
				} else {
					throw new Exception( 'Invalid Modifier ID');
				}
			} );
	}
	
	public function getMenuItem() {
		return AjaxWrapper::process(
				array( 
					new AjaxWrapperField( 'id', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID, 'Invalid menu item id'),
					new AjaxWrapperField( 'groupId', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID, 'Invalid group id'),
				),
				function (array $inputs) {
					if( !( $menuItem = Treat_Model_Menu_Item_New::getAllById( $inputs['id'], $inputs['groupId'] ) ) )
						throw new Exception( 'Menu item "' . $inputs['id'] . '" not found');
					$menuItem['menu_type'] = 'product';
					return $menuItem;
				} );
	}

	public function getImages() {
		return AjaxWrapper::process(
			array(
				new AjaxWrapperField( 'id', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID, 'Invalid image id'),
				new AjaxWrapperField( 'category_id', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID, 'Invalid category id'),
			),
			function (array $inputs) {

				$image_paths = array();
				$images = \EE\Model\Image::getImages( $inputs['id'], $inputs['category_id'] );

				foreach( $images as $image ) {
					$image_paths[] = array('url' => "/ta/businessMenus/getImage?id=" . $inputs['id'] . "&category=" .
													$image->category . "&number=" . $image->number . "&thumbnail=1",
										   'number' => $image->number,
										   'image_size' => $image->image_size);
				}

				return array('images' => $image_paths );
			} );
	}

	public function getMenuSpecial() {
		return AjaxWrapper::process(
				array(
					new AjaxWrapperField( 'id', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID, 'Invalid special item id' ),
					new AjaxWrapperField( 'groupId', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID, 'Invalid group id' ),
				),
				function ( array $inputs ) {
					if( !( $menuItem = Treat_Model_Menu_Item_New::getAllById( $inputs['id'], $inputs['groupId'] ) ) )
						throw new Exception( 'Menu item "' . $inputs['id'] . '" not found');
					$menuItem['ss.id'] = \EE\Menu\Merger::getSpecialId( $menuItem['mi.upc'] );
					$menuItem['menu_type'] = 'product-special';
					return $menuItem;
				} );
	}

	public function getMenuCombo() {
		return AjaxWrapper::process(
			array(
				new AjaxWrapperField( 'id', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID, 'Invalid combo item id' ),
				new AjaxWrapperField( 'groupId', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID, 'Invalid group id' ),
			),
			function ( array $inputs ) {
				if( !( $menuItem = Treat_Model_Menu_Item_New::getAllById( $inputs['id'], $inputs['groupId'] ) ) )
					throw new Exception( 'Menu item "' . $inputs['id'] . '" not found');
				$menuItem['cc.id'] = \EE\Menu\MergerCombo::getComboId( $menuItem['mi.upc'] );
				$menuItem['cc.selected_product_id'] = \EE\Menu\MergerCombo::getProductId( $menuItem['mi.upc'] );
				$menuItem['menu_type'] = 'product-combo';
				return $menuItem;
			} );
	}
	
	public function screenEditItem() {

		
		return AjaxWrapper::process( 
				array( 
					new AjaxWrapperPostField( 'businessid', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID,'Invalid business id'),
					new AjaxWrapperPostField( 'mi.id', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_NEWID,'Invalid menu item id'),
					new AjaxWrapperPostField( 'mi.cond_group', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_NUMERAL,'Invalid condiment group'),
					new AjaxWrapperPostField( 'mi.force_condiment', AjaxWrapperField::FIELD_BOOLEAN),
					new AjaxWrapperPostField( 'mi.group_id', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID,'Invalid group id'),
					new AjaxWrapperPostField( 'mi.item_code', AjaxWrapperField::FIELD_STRING,AjaxWrapperField::OPT_NOTNULL & AjaxWrapperField::OPT_NOTEMPTY, 'Invalid item code'),
					/*new AjaxWrapperPostField( 'mi.manufacturer',	AjaxWrapperField::FIELD_DECIMAL,	AjaxWrapperField::OPTIONS_NUMERAL,	'Invalid manufacturer id' ),*/
					new AjaxWrapperPostField( 'mi.max', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_NUMERAL,'Invalid max'),
					new AjaxWrapperPostField( 'mi.name', AjaxWrapperField::FIELD_STRING, AjaxWrapperField::OPTIONS_NAME, 'Invalid name'),
					new AjaxWrapperPostField( 'mi.ordertype', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_NUMERAL,'Invalid order type'),
					new AjaxWrapperPostField( 'mi.reorder_amount', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_NUMERAL,'Invalid reorder amount'),
					new AjaxWrapperPostField( 'mi.reorder_point', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_NUMERAL,
							'Invalid reorder point'),
					new AjaxWrapperPostField( 'mi.sale_unit', AjaxWrapperField::FIELD_ENUM, array( 'unit', 'lb', 'oz' ), 'Invalid sale unit'),
					new AjaxWrapperPostField( 'mi.shelf_life_days', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_NUMERAL,
							'Invalid shelf life'),
					new AjaxWrapperPostField( 'mi.tare', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPT_GTEZERO,
							'Invalid tare weight'),
					new AjaxWrapperPostField( 'mi.upc', AjaxWrapperField::FIELD_STRING),
					new AjaxWrapperPostField( 'mip.cost', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPT_GTEZERO,
							'Invalid cost'),
					new AjaxWrapperPostField( 'mip.new_price', AjaxWrapperField::FIELD_DECIMAL, 0, 'Invalid price'),
					new AjaxWrapperPostField( 'pgd.pos_color', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_NUMERAL,
							'Invalid button color'),
					new AjaxWrapperPostField( 'pgd.promo_group_id', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID,
							'Invalid promo group id'),
					new AjaxWrapperPostField( 'taxes', AjaxWrapperField::FIELD_ARRAY),
					new AjaxWrapperPostField( 'modifiers', AjaxWrapperField::FIELD_ARRAY),
					new AjaxWrapperPostField( 'pgd.use_product_image', AjaxWrapperField::FIELD_BOOLEAN),
					new AjaxWrapperPostField( 'pgd.hide_text', AjaxWrapperField::FIELD_BOOLEAN),
					new AjaxWrapperPostField( 'pgd.image_display_type', AjaxWrapperField::FIELD_STRING,
						AjaxWrapperField::OPT_NOTNULL & AjaxWrapperField::OPT_NOTEMPTY, 'Invalid Image Display Type'),
					new AjaxWrapperPostField( 'pgd.icon_color', AjaxWrapperField::FIELD_STRING,  'Invalid Image Display Type'),
					new AjaxWrapperPostField( 'idOrderMiscellaneous', AjaxWrapperField::FIELD_ARRAY),
					
				),
				function (array $inputs) {
					if( empty( $inputs['mip.new_price'] ) )
						$inputs['mip.new_price'] = null;
					if( !( $inputs['mi.group_id'] > 0 ) )
						$inputs['mi.group_id'] = null;
					if( !( $inputs['mi.cond_group'] > 0 ) )
						$inputs['mi.cond_group'] = null;
					
					$inputs['mi.is_button'] = 1;
					Treat_Model_Menu_Item_New::updateAll( $inputs );
					return;
				} );
				
	}

	public function uploadImage() {

		$this->startAjax();
		$error = false;
		$errorMsgImage = '';

		$bid = filter_input( INPUT_POST, 'businessid');
		$id = filter_input( INPUT_POST, 'id');
		$category_id = filter_input( INPUT_POST, 'category_id');

		if ( !is_numeric( $bid ) || !is_numeric( $id ) || $id == 0 || !is_numeric( $category_id ) || $category_id == 0 ) {
			echo json_encode( array( 'error' => true, 'message' => 'Invalid Item ID' ));
			return false;
		}

		if(isset($_FILES['file']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
			$product_image = $_FILES['file'];

			// Insert Product image if it exists
			if ( $product_image ) {
				$image = new \EE\Model\Image();
				$errorMsgImage = $image->newImage($product_image, $id, $category_id);
				if ( $errorMsgImage == "" ) {
					\EE\Model\Image::addSync( $bid, $id, $category_id );
				} else {
					$error = true;
				}
			} else {
				$error = true;
				$errorMsgImage = 'Image was not uploaded';
			}
		}
		else {
			$product_image = null;
		}

		echo json_encode( array( 'error' => $error, 'message' => $errorMsgImage ));

		return false;
	}

	public function deleteImages() {

		return AjaxWrapper::process(
			array(
				new AjaxWrapperPostField( 'businessid', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID,
					'Invalid business id'),
				new AjaxWrapperPostField( 'id', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_NEWID,
					'Invalid id'),
				new AjaxWrapperPostField( 'category_id', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_NEWID,
					'Invalid Image Category id'),
				new AjaxWrapperPostField( 'delete_image', AjaxWrapperField::FIELD_ARRAY,  'Invalid Delete Image Values'),
			),
			function (array $inputs) {

				//throw new Exception( 'Error' );
				$delete_image = $inputs['delete_image'];
				if ( !empty( $delete_image ) ) {
					$image_deleted = false;
					foreach( $delete_image as $image_number ) {
						$image = \EE\Model\Image::getImage( $inputs['id'], $inputs['category_id'], $image_number );
						if ( $image ) {
							$image_deleted = true;
							$image->delete();
						}
					}
					if ( $image_deleted ) {
						\EE\Model\Image::reorderImages( $inputs['id'], $inputs['category_id'] );
						\EE\Model\Image::addSync( $inputs['businessid'], $inputs['id'], $inputs['category_id'] );
					}
				}

				return;
			} );

	}

	public function screenEditSpecial() {
		return AjaxWrapper::process(
				array(
					AjaxWrapperPostField::bid( 'businessid' ),
					new AjaxWrapperPostField( 'mi.id', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_NEWID, 'Invalid menu item id' ),
					new AjaxWrapperPostField( 'ss.id', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID, 'Invalid special id' ),
					new AjaxWrapperPostField( 'mi.name', AjaxWrapperField::FIELD_STRING, AjaxWrapperField::OPTIONS_NAME, 'Invalid name' ),
					new AjaxWrapperPostField( 'pgd.pos_color', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_NUMERAL, 'Invalid button color' ),
					new AjaxWrapperPostField( 'mip.new_price', AjaxWrapperField::FIELD_DECIMAL, 0, 'Invalid price' ),
					new AjaxWrapperPostField( 'pgd.promo_group_id', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID, 'Invalid promo group id' ),
					new AjaxWrapperPostField( 'pgd.use_product_image', AjaxWrapperField::FIELD_BOOLEAN),
					new AjaxWrapperPostField( 'pgd.hide_text', AjaxWrapperField::FIELD_BOOLEAN),
					new AjaxWrapperPostField( 'pgd.image_display_type', AjaxWrapperField::FIELD_STRING,
						AjaxWrapperField::OPT_NOTNULL & AjaxWrapperField::OPT_NOTEMPTY, 'Invalid Image Display Type'),
					new AjaxWrapperPostField( 'pgd.icon_color', AjaxWrapperField::FIELD_STRING,  'Invalid Image Display Type'),
				),
				function (array $inputs) {
					if( empty( $inputs['mip.new_price'] ) )
						$inputs['mip.new_price'] = null;
					\EE\Menu\Merger::mergeSpecialItem( $inputs );
					return;
				} );
	}

	public function screenEditCombo() {
		return AjaxWrapper::process(
			array(
				AjaxWrapperPostField::bid( 'businessid' ),
				new AjaxWrapperPostField( 'mi.id', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_NEWID, 'Invalid menu item id' ),
				new AjaxWrapperPostField( 'cc.id', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID, 'Invalid combo id' ),
				new AjaxWrapperPostField( 'cc.product_id', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID, 'Invalid combo product id' ),
				new AjaxWrapperPostField( 'mi.name', AjaxWrapperField::FIELD_STRING, AjaxWrapperField::OPTIONS_NAME, 'Invalid name' ),
				new AjaxWrapperPostField( 'pgd.pos_color', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_NUMERAL, 'Invalid button color' ),
				new AjaxWrapperPostField( 'mip.new_price', AjaxWrapperField::FIELD_DECIMAL, 0, 'Invalid price' ),
				new AjaxWrapperPostField( 'pgd.promo_group_id', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID, 'Invalid promo group id' ),
				new AjaxWrapperPostField( 'pgd.use_product_image', AjaxWrapperField::FIELD_BOOLEAN),
				new AjaxWrapperPostField( 'pgd.hide_text', AjaxWrapperField::FIELD_BOOLEAN),
				new AjaxWrapperPostField( 'pgd.image_display_type', AjaxWrapperField::FIELD_STRING,
					AjaxWrapperField::OPT_NOTNULL & AjaxWrapperField::OPT_NOTEMPTY, 'Invalid Image Display Type'),
				new AjaxWrapperPostField( 'pgd.icon_color', AjaxWrapperField::FIELD_STRING,  'Invalid Image Display Type'),
			),
			function (array $inputs) {

				\EE\Menu\MergerCombo::mergeComboitem( $inputs );
				return;
			} );
	}
	
	public function screenEditGroup() {
		return AjaxWrapper::process( 
				array( 
					new AjaxWrapperField( 'id', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_NEWID, 'Invalid group id'),
					new AjaxWrapperField( 'businessid', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID,
							'Invalid business id'),
					new AjaxWrapperField( 'name', AjaxWrapperField::FIELD_STRING, AjaxWrapperField::OPTIONS_NAME, 'Invalid name'),
					new AjaxWrapperField( 'routing', AjaxWrapperField::FIELD_BOOLEAN),
					new AjaxWrapperField( 'group_rows', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_NUMERAL,
							'Invalid number of rows'),
					new AjaxWrapperField( 'group_cols', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_NUMERAL,
							'Invalid number of columns'),
					new AjaxWrapperField( 'pos_color', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_NUMERAL,
							'Invalid button color'),
					new AjaxWrapperField( 'display_on', AjaxWrapperField::FIELD_ARRAY),
					new AjaxWrapperPostField( 'use_product_image', AjaxWrapperField::FIELD_BOOLEAN),
					new AjaxWrapperPostField( 'hide_text', AjaxWrapperField::FIELD_BOOLEAN),
					new AjaxWrapperPostField( 'image_display_type', AjaxWrapperField::FIELD_STRING,
						AjaxWrapperField::OPT_NOTNULL & AjaxWrapperField::OPT_NOTEMPTY, 'Invalid Image Display Type'),
					new AjaxWrapperPostField( 'icon_color', AjaxWrapperField::FIELD_STRING,  'Invalid Image Display Type'),
				),
				function (array $inputs) {
					if( $inputs['id'] > 0 ) {
						$group = Treat_Model_Promotion_Group::getById( $inputs['id'] );
						if( !$group->businessid = $inputs['businessid'] )
							throw new Exception( 'Group and business do not match');
					}
					else {
						$group = new Treat_Model_Promotion_Group;
						$group->businessid = $inputs['businessid'];
					}
					
					if( !( $inputs['group_rows'] > 0 ) )
						$inputs['group_rows'] = null;
					if( !( $inputs['group_cols'] > 0 ) )
						$inputs['group_cols'] = null;
					
					$group->name = $inputs['name'];
					$group->routing = $inputs['routing'];
					$group->group_rows = $inputs['group_rows'];
					$group->group_cols = $inputs['group_cols'];
					$group->pos_color = $inputs['pos_color'];
					$group->display_on = empty( $inputs['display_on'] ) ? '' : implode( ',', $inputs['display_on'] );
					$group->use_product_image = $inputs['use_product_image'];
					$group->hide_text = $inputs['hide_text'];
					$group->image_display_type = $inputs['image_display_type'];
					$group->icon_color = $inputs['icon_color'];
					$group->save();
				} );
	}
	
	public function getMissingSpecials() {
		return AjaxWrapper::process(
				array(
					AjaxWrapperField::bid( 'bid' ),
				),
				function( array $inputs ) {
					$items = \EE\Model\Menu\ServeryStation::getMissingItems( $inputs['bid'] );
					return array( 'items' => $items );
				} );
	}
	
	public function getMergeItems( ) {
		return AjaxWrapper::process(
				array(
					AjaxWrapperField::bid( 'bid' ),
				),
				function( array $inputs ) {
					$items = \EE\Model\Menu\ServeryStation::getMergeItems( $inputs['bid'] );
					return array( 'items' => $items );
				} );
	}
	
	public function mergeMenuSpecial( ) {
		return AjaxWrapper::process(
				array(
					AjaxWrapperField::bid( 'bid' ),
					new AjaxWrapperField( 'merge', AjaxWrapperField::FIELD_ARRAY, 0, 'Invalid merge array' ),
					new AjaxWrapperField( 'item_code', AjaxWrapperField::FIELD_ARRAY, 0, 'Invalid item code array' ),
				),
				function( array $inputs ) {
					$bid = $inputs['bid'];
					foreach( $inputs['merge'] as $id => $flag ) {
						try {
							\EE\Menu\Merger::copyMenuItem( $id, $inputs['item_code'][$id] );
						} catch( \Exception $e ) {
							if( !IN_PRODUCTION ) {
								throw $e;
							} else {
								throw new \Exception( 'Failed to copy menu item ' . $id );
							}
						}
					}
				} );
	}

	public function mergeMenuItems( ) {
		return AjaxWrapper::process(
			array(
				AjaxWrapperField::bid( ),
				new AjaxWrapperField( 'link', AjaxWrapperField::FIELD_ARRAY, 0, 'Invalid link array' ),
				new AjaxWrapperField( 'item_code', AjaxWrapperField::FIELD_ARRAY, 0, 'Invalid item code array' ),
			),
			function( array $inputs ) {
				$bid = $inputs['bid'];
				foreach( $inputs['link'] as $id => $value ) {
					if( $value ) {
						try {
							\EE\Menu\Merger::linkMenuItem( $id, $value, $inputs['item_code'][$id] );
						} catch( \Exception $e ) {
							if( !IN_PRODUCTION ) {
								throw $e;
							} else {
								throw new \Exception( 'Failed to link menu item ' . $id );
							}
						}
					}
				}
			}
		);
	}
	
	public function resetForSync( $reset = false ) {
		return AjaxWrapper::process( array(
			AjaxWrapperField::bid( ),
			new AjaxWrapperField( 'extras', AjaxWrapperField::FIELD_ARRAY ),
		), function( array $inputs ) use( $reset ) {
			if( $reset )
				Treat_Model_KioskSync::addType( $inputs['bid'], Treat_Model_KioskSync::TYPE_RESET_ALL );
			Treat_Model_KioskSync::syncAll( $inputs['bid'], $inputs['extras'] );
		} );
	}

	public function resetAllData( ) {
		return $this->resetForSync( true );
	}
	
	public function resetBusiness() {
		$this->startAjax();
		
		$bid = htmlspecialchars( trim( $_POST['bid'] ) );
		$setup_status = htmlspecialchars( trim( $_POST['setup_status'] ) );
		
		if( $setup_status == 2 ) {
			$new_setup_status = 0;
		}
		else {
			$new_setup_status = 2;
		}
		
		$query = "UPDATE business SET setup = $new_setup_status WHERE businessid = $bid";
		$result = Treat_DB_ProxyOld::query( $query );

		$user = \EE\Controller\Base::getSessionCookieVariable('usercook', '');
		$sql2 = "INSERT IGNORE INTO audit_business ( user_account , businessid , date_time ) VALUES ( '$user' , '$bid' , now())";
		Treat_DB_ProxyOld::query( $sql2 );
		
		return false;
	}
	
	public function updatePromotions() {
		$this->startAjax();
		
		$bid = htmlspecialchars( trim( $_POST['bid'] ) );
		$promo_name = htmlspecialchars( trim( $_POST['promo_name'] ) );
		$promo_type = htmlspecialchars( trim( $_POST['promo_type'] ) );
		$promo_target = htmlspecialchars( trim( $_POST['promo_target'] ) );
		$promo_value = htmlspecialchars( trim( $_POST['promo_value'] ) );
		$promo_trigger = htmlspecialchars( trim( $_POST['promo_trigger'] ) );
		$promo_tax = intval( $_POST['promo_tax'] );
		$promo_tax = $promo_tax ? : 'NULL';
		$promo_reserve = isset( $_POST['promo_reserve'] ) ? 1 : 0;
		$promo_discount = htmlspecialchars( trim( $_POST['promo_option_discount'] ) );
		$promo_discount_type = htmlspecialchars( trim( $_POST['promo_option_discount_type'] ) );
		$promo_discount_limit = intval( $_POST['promo_option_discount_limit'] );
		$promo_discount_n = intval( $_POST['promo_option_discount_n'] );
		$promo_trigger_amount_limit = htmlspecialchars( trim( $_POST['promo_trigger_amount_limit'] ) );
		$promo_trigger_amount_type = htmlspecialchars( trim( $_POST['promo_trigger_amount_type'] ) );
		$promo_trigger_amount = intval( $_POST['promo_trigger_amount'] );
		$promo_trigger_amount_2 = intval( $_POST['promo_trigger_amount_2'] );
		$promo_trigger_amount_3 = intval( $_POST['promo_trigger_amount_3'] );
		$id = intval( $_POST['promo_id'] );
		if( htmlspecialchars( trim( $_POST['subsidy_check'] ) ) == '1' ) {
			$subsidy = 1;
		}
		else {
			$subsidy = 0;
		}
		
		if( $id > 0 ) {
			$query = "UPDATE promotions SET
			name = '$promo_name',
			type = '$promo_type',
			target = '$promo_target',
			value = '$promo_value',
			promo_trigger = '$promo_trigger',
			tax_id = $promo_tax,
			promo_reserve = $promo_reserve,
			promo_discount = '$promo_discount',
			promo_discount_type = '$promo_discount_type',
			promo_discount_limit = '$promo_discount_limit',
			promo_discount_n = '$promo_discount_n',
			promo_trigger_amount_limit = '$promo_trigger_amount_limit',
			promo_trigger_amount_type = '$promo_trigger_amount_type',
			promo_trigger_amount = '$promo_trigger_amount',
			promo_trigger_amount_2 = '$promo_trigger_amount_2',
			promo_trigger_amount_3 = '$promo_trigger_amount_3',
			subsidy = '$subsidy'
			WHERE id = $id";
			$result = Treat_DB_ProxyOld::query( $query );
			
			$message = "Record Updated";
		}
		else {
			$pos_id = 0;
			
			$query = "SELECT COALESCE( MAX(pos_id) + 1, 1 ) AS pos_id, COALESCE( MAX(rule_order) + 1, 1 ) AS rule_order FROM promotions WHERE businessid = $bid";
			$result = Treat_DB_ProxyOld::query( $query );
			
			$v = @mysql_fetch_assoc( $result );
			
			$uuid = \EE\Model\Promotion::genUuid();
			$query = "
			INSERT INTO promotions (
			businessid,
			pos_id,
			name,
			type,
			target,
			value,
			promo_trigger,
			rule_order,
			tax_id,
			promo_reserve,
			promo_discount,
			promo_discount_type,
			promo_discount_limit,
			promo_discount_n,
			promo_trigger_amount_limit,
			promo_trigger_amount_type,
			promo_trigger_amount,
			promo_trigger_amount_2,
			promo_trigger_amount_3,
			subsidy,
			uuid_promotions
			) VALUES (
			$bid,
			{$v['pos_id']},
			'$promo_name',
			'$promo_type',
			'$promo_target',
			'$promo_value',
			'$promo_trigger',
			{$v['rule_order']},
			'$promo_tax',
			$promo_reserve,
			'$promo_discount',
			'$promo_discount_type',
			'$promo_discount_limit',
			'$promo_discount_n',
			'$promo_trigger_amount_limit',
			'$promo_trigger_amount_type',
			'$promo_trigger_amount',
			'$promo_trigger_amount_2',
			'$promo_trigger_amount_3',
			'$subsidy',
			'$uuid'
			)
			";
			$result = Treat_DB_ProxyOld::query( $query );
			$id = mysql_insert_id();
			
			$message = "Record Added";
		}
		
		Treat_Model_KioskSync::addSync( $bid, $id, Treat_Model_KioskSync::TYPE_PROMO );
		
		echo json_encode( array( 'promo_id' => $id, 'message' => $message ) );
		
		return false;
	}
	
	public function getPromoTargets() {
		$this->startAjax();
		
		$promo_id = htmlspecialchars( trim( $_REQUEST['promoid'] ) );
		
		$query = "SELECT
			promotions.businessid,
			promo_trigger.target_id,
			promotions.promo_trigger
			FROM promotions
			JOIN promo_trigger ON promotions.promo_trigger = promo_trigger.id
			WHERE promotions.id = $promo_id";
		$result = Treat_DB_ProxyOld::query( $query );
		
		$promo_trigger = Treat_DB_ProxyOld::mysql_result( $result, 0, 'promo_trigger', '' );
		
		$bid = Treat_DB_ProxyOld::mysql_result( $result, 0, "businessid", '' );
		$target = Treat_DB_ProxyOld::mysql_result( $result, 0, "target_id", '' );
		
		if( $target == 1 ) {
			$query = "SELECT
		menu_items_new.*
		,menu_groups_new.name AS group_name
		FROM menu_items_new
		JOIN menu_groups_new ON menu_groups_new.id = menu_items_new.group_id
		WHERE menu_items_new.businessid = $bid AND menu_items_new.active = 0 ORDER BY menu_items_new.group_id,menu_items_new.name";
			$result = Treat_DB_ProxyOld::query( $query );
			
			//echo "<table width=100% style=\"border:1px solid black;\" cellspacing=0 cellpadding=0><tr valign=top>";
			echo "<div class='settingsTable'><div class='settingsRow'><div class='settingsThird'>";
			
			$lastgroup = false;
			$counter = 1;
			while( $r = mysql_fetch_array( $result ) ) {
				$id = $r["pos_id"];
				$name = $r["name"];
				$group_name = $r["group_name"];
				
				if( $lastgroup != $group_name ) {
					//if($lastgroup != 0){echo "</td>";}
					//if($counter == 4){echo "</tr><tr valign=top>";$counter = 1;}
					//echo "<td style=\"border:1px solid black;font-size:11px;\"><div style=\"background-color:#999999;width:100%;font-weight:bold;border-bottom:2px solid black;color:white;\">&nbsp;$group_name</div>";
					if( $lastgroup !== false ) {
						echo "</div></div>";
						if( $counter == 4 ) {
							echo "</div><div class='settingsRow'>";
							$counter = 1;
						}
						echo "<div class='settingsThird'>";
					}
					echo "<div class='settingsHeader ui-state-default'>$group_name</div><div class='settingsForm'>";
					$counter++;
				}
				
				$query2 = "SELECT * FROM promo_detail WHERE promo_id = $promo_id AND type = 1 AND id = $id";
				$result2 = Treat_DB_ProxyOld::query( $query2 );
				$num2 = Treat_DB_ProxyOld::mysql_numrows( $result2, 0 );
				
				if( $num2 == 1 ) {
					$val = "CHECKED";
				}
				else {
					$val = "";
				}
				
				echo "<div class='settingsItem'><input type=checkbox name=mid$id id='mid$id' value=1 onchange=\"promo_detail($promo_id,1,$id);\" $val /><label for='mid$id'>$name</label></div>";
				
				$lastgroup = $group_name;
			}
			
			echo "</div></div>";
		}
		elseif( $target == 2 ) {
			$query = "SELECT * FROM menu_groups_new WHERE businessid = $bid ORDER BY name";
			$result = Treat_DB_ProxyOld::query( $query );
			
			//echo "<table width=100% style=\"border:2px solid black;\" cellspacing=0 cellpadding=0><tr><td>";
			//echo "<div style=\"background-color:#999999;width:100%;font-weight:bold;border-bottom:2px solid black;color:white;\">&nbsp;Menu Groups</div>";
			echo '<div class="settingsContainer"><div class="settingsHeader ui-state-default">Menu Groups</div><div class="settingsForm">';
			
			while( $r = mysql_fetch_array( $result ) ) {
				$id = $r["pos_id"];
				$name = $r["name"];
				
				$query2 = "SELECT * FROM promo_detail WHERE promo_id = $promo_id AND type = 2 AND id = $id";
				$result2 = Treat_DB_ProxyOld::query( $query2 );
				$num2 = Treat_DB_ProxyOld::mysql_numrows( $result2, 0 );
				
				if( $num2 == 1 ) {
					$val = "CHECKED";
				}
				else {
					$val = "";
				}
				
				echo "<div class='settingsItem settingsFloat'><input type=checkbox name=gid$id id='gid$id' value=1 onclick=\"promo_detail($promo_id,2,$id);\" $val /><label for='gid$id'>$name</label></div>";
			}
			
			//echo "</td></tr></table>";
			echo '</div></div>';
		}
		elseif( $target == 3 ) {
			echo '<div class="settingsContainer"><div class="settingsHeader ui-state-default">All Products Selected</div></div>';
			return false;
		}
		elseif( $target == 4 ) {
			$query = "SELECT * FROM menu_groups_new WHERE businessid = $bid ORDER BY name";
			$result = Treat_DB_ProxyOld::query( $query );
			
			//echo "<table width=100% style=\"border:2px solid black;\" cellspacing=0 cellpadding=0><tr><td>";
			//echo "<div style=\"background-color:#999999;width:100%;font-weight:bold;border-bottom:2px solid black;color:white;\">&nbsp;Menu Groups</div>";
			echo '<div class="settingsContainer"><div class="settingsHeader ui-state-default">Menu Groups</div><div class="settingsForm">';
			
			$selectedOptions = array();
			$query2 = "SELECT * FROM promo_detail WHERE promo_id = $promo_id AND ( type = 31 OR type = 32 OR type = 33 )";
			$result2 = Treat_DB_ProxyOld::query( $query2 );
			while( $r = mysql_fetch_assoc( $result2 ) ) {
				$selectedOptions[$r['type']] = $r['id'];
			}
			
			$menuGroupOptions = '<option value="">(none)</option>';
			while( $r = mysql_fetch_array( $result ) ) {
				$id = $r["pos_id"];
				$name = $r["name"];
				
				$menuGroupOptions .= "<option value='$id'>$name</option>";
			}
			
			echo '<label for="promo_group_1">1:</label> <select class="promoOption" name="promo_group_1" id="promo_group_1" onchange="promoDetailClear( '
					. $promo_id . ', 31, this );">' . $menuGroupOptions . '</select>';
			echo '<label for="promo_group_2">2:</label> <select class="promoOption" name="promo_group_2" id="promo_group_2" onchange="promoDetailClear( '
					. $promo_id . ', 32, this );">' . $menuGroupOptions . '</select>';
			echo '<label for="promo_group_3">3:</label> <select class="promoOption" name="promo_group_3" id="promo_group_3" onchange="promoDetailClear( '
					. $promo_id . ', 33, this );">' . $menuGroupOptions . '</select>';
			echo '<script type="text/javascript" language="javascript">';
			echo 'jQuery(function(){';
			if( !empty( $selectedOptions[31] ) )
				echo 'jQuery("#promo_group_1 option[value=' . $selectedOptions[31] . ']").attr("selected","selected");';
			if( !empty( $selectedOptions[32] ) )
				echo 'jQuery("#promo_group_2 option[value=' . $selectedOptions[32] . ']").attr("selected","selected");';
			if( !empty( $selectedOptions[33] ) )
				echo 'jQuery("#promo_group_3 option[value=' . $selectedOptions[33] . ']").attr("selected","selected");';
			echo '});';
			echo '</script>';
			//echo "</td></tr></table>";
			echo '</div></div>';
		}
		elseif( $target == 5 ) {
			$query = "SELECT * FROM promo_groups WHERE businessid = $bid ORDER BY name";
			$result = Treat_DB_ProxyOld::query( $query );
			
			echo '<div class="settingsContainer"><div class="settingsHeader ui-state-default">Combo Groups</div><div class="settingsForm">';
			
			$selectedOptions = array();
			$query2 = "SELECT * FROM promo_detail WHERE promo_id = $promo_id AND ( type = 51 OR type = 52 OR type = 53 )";
			$result2 = Treat_DB_ProxyOld::query( $query2 );
			while( $r = mysql_fetch_assoc( $result2 ) ) {
				$selectedOptions[$r['type']] = $r['id'];
			}
			
			$menuGroupOptions = '<option value="">(none)</option>';
			while( $r = mysql_fetch_array( $result ) ) {
				$id = $r["pos_id"] ? : $r['id'];
				$name = $r["name"];
				
				$menuGroupOptions .= "<option value='$id'>$name</option>";
			}
			
			echo '<label for="promo_group_1">1:</label> <select class="promoOption" name="promo_group_1" id="promo_group_1" onchange="promoDetailClear( '
					. $promo_id . ', 51, this );">' . $menuGroupOptions . '</select>';
			echo '<label for="promo_group_2">2:</label> <select class="promoOption" name="promo_group_2" id="promo_group_2" onchange="promoDetailClear( '
					. $promo_id . ', 52, this );">' . $menuGroupOptions . '</select>';
			echo '<label for="promo_group_3">3:</label> <select class="promoOption" name="promo_group_3" id="promo_group_3" onchange="promoDetailClear( '
					. $promo_id . ', 53, this );">' . $menuGroupOptions . '</select>';
			echo '<script type="text/javascript" language="javascript">';
			echo 'jQuery(function(){';
			if( !empty( $selectedOptions[51] ) )
				echo 'jQuery("#promo_group_1 option[value=' . $selectedOptions[51] . ']").attr("selected","selected");';
			if( !empty( $selectedOptions[52] ) )
				echo 'jQuery("#promo_group_2 option[value=' . $selectedOptions[52] . ']").attr("selected","selected");';
			if( !empty( $selectedOptions[53] ) )
				echo 'jQuery("#promo_group_3 option[value=' . $selectedOptions[53] . ']").attr("selected","selected");';
			echo '});';
			echo '</script>';
			echo '</div></div>';
		}
		
		return false;
	}
	
	public function updatePromoTargets() {
		$this->startAjax();
		
		$businessid = intval( $_POST['businessid'] );
		$promo_id = htmlspecialchars( trim( $_POST['promo_id'] ) );
		$id = htmlspecialchars( trim( $_POST['id'] ) );
		$type = htmlspecialchars( trim( $_POST['type'] ) );
		$promo_value = strtolower( htmlspecialchars( trim( $_POST['promo_value'] ) ) );
		$clear = $_POST['clear'];

//		if( $clear == 'true' || $promo_value == 'checked' || $promo_value == 1 ) {
//			$query = "DELETE FROM promo_detail WHERE promo_id = $promo_id AND type = $type";
//			Treat_DB_ProxyOld::query( $query );
//		}

		if( $promo_value == "true" || $promo_value == 'checked' || $promo_value == 1 ) {
			$query = "INSERT INTO promo_detail (promo_id,type,id) VALUES ($promo_id,$type,$id)";
			$result = Treat_DB_ProxyOld::query( $query );
		}
		else {
			$query = "DELETE FROM promo_detail WHERE promo_id = $promo_id AND type = $type AND id = $id";
			$result = Treat_DB_ProxyOld::query( $query );
		}

		Treat_Model_KioskSync::addSync( $businessid, $promo_id, Treat_Model_KioskSync::TYPE_PROMO );

		echo $promo_value;

		return false;
	}
	
	public function getPromoSchedule() {
		$this->startAjax();
		
?>
		<script language="javascript" type="text/javascript">
			jQuery(function(){
				jQuery('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' });
	
				jQuery('#schedule_form').submit(function(){
					var formData = jQuery(this).serialize();
					jQuery.ajax({
						type: "POST",
						dataType: 'json',
						url: "posAjax",
						data: "todo=15&" + formData,
						success: function(data){
							if( data.error ) {
								alert( data.error );
							}
						}
					});
	
					return false;
				});
			});
		</script>
		<?php
		for( $counter = 1; $counter <= 12; $counter++ ) {
			$hours .= "<option value=$counter>$counter</option>";
		}
		
		for( $counter = 0; $counter <= 55; $counter += 5 ) {
			if( strlen( $counter ) < 2 ) {
				$counter = "0$counter";
			}
			$minutes .= "<option value=$counter>$counter</option>";
		}
		
		$ampm = "<option value=am>AM</option><option value=pm>PM</option>";
		
		$businessid = intval( $_REQUEST['bid'] );
		$promo_id = htmlspecialchars( trim( $_REQUEST['promoid'] ) );
		
		$query = "SELECT * FROM promotions WHERE id = $promo_id";
		$result = Treat_DB_ProxyOld::query( $query );
		
		$start_date = Treat_DB_ProxyOld::mysql_result( $result, 0, "start_date", '' );
		$_start_time = Treat_DB_ProxyOld::mysql_result( $result, 0, "start_time", '' );
		$start_time = explode( ':', $_start_time );
		$end_date = Treat_DB_ProxyOld::mysql_result( $result, 0, "end_date", '' );
		$_end_time = Treat_DB_ProxyOld::mysql_result( $result, 0, "end_time", '' );
		$end_time = explode( ':', $_end_time );
		
		$monday = Treat_DB_ProxyOld::mysql_result( $result, 0, "monday", '' );
		if( $monday == 1 ) {
			$show_mon = "CHECKED";
		}
		$tuesday = Treat_DB_ProxyOld::mysql_result( $result, 0, "tuesday", '' );
		if( $tuesday == 1 ) {
			$show_tue = "CHECKED";
		}
		$wednesday = Treat_DB_ProxyOld::mysql_result( $result, 0, "wednesday", '' );
		if( $wednesday == 1 ) {
			$show_wed = "CHECKED";
		}
		$thursday = Treat_DB_ProxyOld::mysql_result( $result, 0, "thursday", '' );
		if( $thursday == 1 ) {
			$show_thu = "CHECKED";
		}
		$friday = Treat_DB_ProxyOld::mysql_result( $result, 0, "friday", '' );
		if( $friday == 1 ) {
			$show_fri = "CHECKED";
		}
		$saturday = Treat_DB_ProxyOld::mysql_result( $result, 0, "saturday", '' );
		if( $saturday == 1 ) {
			$show_sat = "CHECKED";
		}
		$sunday = Treat_DB_ProxyOld::mysql_result( $result, 0, "sunday", '' );
		if( $sunday == 1 ) {
			$show_sun = "CHECKED";
		}
		
		//echo "<table width=100% style=\"border:1px solid black;\" cellspacing=0 cellpadding=0>";
		//echo "<tr><td style=\"border:1px solid black;\"><div style=\"background-color:#999999;width:100%;font-weight:bold;color:white;\">&nbsp;Schedule</div></td></tr>";
		
		echo '<div class="settingsContainer"><div class="settingsHeader ui-state-default">Schedule</div>';
		
		//echo "<tr><td style=\"border:1px solid black;\">
		//			&nbsp;<form id='schedule_form' onsubmit='return false;' style=\"margin:0;padding:0;display:inline;\">
		
		echo '<form class="settingsForm" onsubmit="return false;" id="schedule_form">';
		
		echo "<input style='float: right;' type='submit' value='Update Schedule' />";
		
		echo "<input type='hidden' name='promo_id' value='$promo_id' />";
		echo "<input type='hidden' name='businessid' value='$businessid' />";
		echo "Start Date: <input type=text name='start_date' id='start_date' value='$start_date' size=10 class=\"datepicker\" />";
		echo "Start Time: <select class='settingsNospace' name=start_hour id='start_hour'>";
		
		for( $counter = 1; $counter <= 12; $counter++ ) {
			$hour = $start_time[0] ? : 12;
			if( $hour > 12 ) {
				$hour = $hour - 12;
			}
			if( $hour == $counter ) {
				$sel = 'SELECTED';
			}
			else {
				$sel = '';
			}
			echo "<option value=$counter $sel>$counter</option>";
		}
		
		echo "</select>:<select class='settingsNospace' name=start_min id='start_min'>";
		
		for( $counter = 0; $counter <= 60; $counter += 5 ) {
			if( $counter == 60 )
				$counter = 59;
			elseif( strlen( $counter ) < 2 ) {
				$counter = "0$counter";
			}
			$min = $start_time[1] ? : '00';
			if( $min == $counter ) {
				$sel = 'SELECTED';
			}
			else {
				$sel = '';
			}
			echo "<option value=$counter $sel>$counter</option>";
		}
		
		echo "</select><select name=start_ampm id='start_ampm'>";
		
		if( $start_time[0] > 11 ) {
			$sel = 'SELECTED';
		}
		else {
			$sel = '';
		}
		echo "<option value=am>AM</option><option value=pm $sel>PM</option>";
		
		echo "</select><br />";
		
		echo "End Date: <input type=text name='end_date' id='end_date' value='$end_date' size=10 class=\"datepicker\" />";
		echo "End Time: <select class='settingsNospace' name=end_hour id='end_hour'>";
		
		for( $counter = 1; $counter <= 12; $counter++ ) {
			$hour = $end_time[0] ? : '11';
			if( $hour > 12 ) {
				$hour = $hour - 12;
			}
			if( $hour == $counter ) {
				$sel = 'SELECTED';
			}
			else {
				$sel = '';
			}
			echo "<option value=$counter $sel>$counter</option>";
		}
		
		echo "</select>:<select class='settingsNospace' name=end_min id='end_min'>";
		
		for( $counter = 0; $counter <= 60; $counter += 5 ) {
			if( $counter == 60 )
				$counter = 59;
			elseif( strlen( $counter ) < 2 ) {
				$counter = "0$counter";
			}
			$min = $end_time[1] ? : '59';
			if( $min == $counter ) {
				$sel = 'SELECTED';
			}
			else {
				$sel = '';
			}
			echo "<option value=$counter $sel>$counter</option>";
		}
		
		echo "</select><select name=end_ampm id='end_ampm'>";
		
		$meri = $end_time[0] ? $end_time[0] > 11 : true;
		if( $meri ) {
			$sel = 'SELECTED';
		}
		else {
			$sel = '';
		}
		echo "<option value=am>AM</option><option value=pm $sel>PM</option>";
		
		echo "</select><br>";
		
		echo "<span>Available Days:</span> ";
		
		$daysArray = array(
			'mon' => array(
				'name' => 'monday', 'show' => $show_mon
			),
			'tue' => array(
				'name' => 'tuesday', 'show' => $show_tue
			),
			'wed' => array(
				'name' => 'wednesday', 'show' => $show_wed
			),
			'thu' => array(
				'name' => 'thursday', 'show' => $show_thu
			),
			'fri' => array(
				'name' => 'friday', 'show' => $show_fri
			),
			'sat' => array(
				'name' => 'saturday', 'show' => $show_sat
			),
			'sun' => array(
				'name' => 'sunday', 'show' => $show_sun
			),
		);
		
		foreach( $daysArray as $sday => $day ) {
			echo "<div class='settingsItem'>";
			echo "<input type='checkbox' name='{$day['name']}' id='$sday' value='1' {$day['show']} />";
			echo "<label for='$sday'>" . ucfirst( $day['name'] ) . "</label></div>";
		}
		
		echo "</form></div>";
		
		return false;
	}
	
	public function updatePromoSchedule() {
		$this->startAjax();
		
		// run query to open db - needed for mysql_real_escape_string
		Treat_DB_ProxyOld::query( 'SELECT 1' );
		
		$businessid = intval( $_POST['businessid'] );
		$promo_id = intval( $_POST['promo_id'] );
		$start_date = mysql_real_escape_string( $_POST['start_date'] );
		$_start_hour = mysql_real_escape_string( $_POST['start_hour'] );
		$_start_min = mysql_real_escape_string( $_POST['start_min'] );
		$_start_ampm = $_POST['start_ampm'] == 'am' ? 0 : 12;
		if( $_start_hour == 12 )
			$_start_hour = 0;
		$start_time = ( $_start_hour + $_start_ampm ) . ':' . $_start_min;
		$end_date = mysql_real_escape_string( $_POST['end_date'] );
		$_end_hour = mysql_real_escape_string( $_POST['end_hour'] );
		$_end_min = mysql_real_escape_string( $_POST['end_min'] );
		$_end_ampm = $_POST['end_ampm'] == 'am' ? 0 : 12;
		if( $_end_hour == 12 )
			$_end_hour = 0;
		$end_time = ( $_end_hour + $_end_ampm ) . ':' . $_end_min;
		$monday = isset( $_POST['monday'] ) ? 1 : 0;
		$tuesday = isset( $_POST['tuesday'] ) ? 1 : 0;
		$wednesday = isset( $_POST['wednesday'] ) ? 1 : 0;
		$thursday = isset( $_POST['thursday'] ) ? 1 : 0;
		$friday = isset( $_POST['friday'] ) ? 1 : 0;
		$saturday = isset( $_POST['saturday'] ) ? 1 : 0;
		$sunday = isset( $_POST['sunday'] ) ? 1 : 0;
		
		$returnArray = array(
			'promo_id' => $promo_id,
		);
		
		$query = "
				UPDATE promotions SET
					start_date = '$start_date',
					start_time = '$start_time',
					end_date = '$end_date',
					end_time = '$end_time',
					monday = $monday,
					tuesday = $tuesday,
					wednesday = $wednesday,
					thursday = $thursday,
					friday = $friday,
					saturday = $saturday,
					sunday = $sunday
				WHERE id = $promo_id
				AND businessid = $businessid
			";
		Treat_DB_ProxyOld::query( $query );
		
		if( mysql_errno() ) {
			$error = "An error occurred: " . mysql_error();
			$returnArray = array(
				'error' => $error,
			);
		}
		
		$returnArray['query'] = $query;
		
		Treat_Model_KioskSync::addSync( $businessid, $promo_id, Treat_Model_KioskSync::TYPE_PROMO );
		
		echo json_encode( $returnArray );
		
		return false;
	}
	
	public function getCombos() {
		return $this->getPromotions();
	}

	public function getPromotions() {
		$this->startAjax();
		
		$businessid = intval( $_GET['bid'] );
		$query = "SELECT * FROM promotions WHERE businessid=$businessid ORDER BY rule_order";
		$result = Treat_DB_ProxyOld::query( $query );
		
		$data = array();
		while( $r = mysql_fetch_assoc( $result ) ) {
			$d = array();
			$d['promo_id'] = $r['id'];
			$d['promo_name'] = $r['name'];
			$d['promo_type'] = $r['type'];
			$d['promo_value'] = $r['value'];
			$d['promo_trigger'] = $r['promo_trigger'];
			$d['promo_target'] = $r['target'];
			$d['promo_active'] = $r['activeFlag'];
			$d['promo_order'] = $r['rule_order'];
			$d['promo_tax'] = $r['tax_id'];
			$d['promo_reserve'] = $r['promo_reserve'];
			$d['promo_discount'] = $r['promo_discount'];
			$d['promo_discount_type'] = $r['promo_discount_type'];
			$d['promo_discount_n'] = $r['promo_discount_n'];
			$d['promo_discount_limit'] = $r['promo_discount_limit'];
			$d['promo_trigger_amount_limit'] = $r['promo_trigger_amount_limit'];
			$d['promo_trigger_amount_type'] = $r['promo_trigger_amount_type'];
			$d['promo_trigger_amount'] = $r['promo_trigger_amount'];
			$d['promo_trigger_amount_2'] = $r['promo_trigger_amount_2'];
			$d['promo_trigger_amount_3'] = $r['promo_trigger_amount_3'];
			$d['subsidy'] = $r['subsidy'];
			$data[] = $d;
		}
		
		$query = "SELECT id,vivipos_name FROM promo_type";
		$result = Treat_DB_ProxyOld::query( $query );
		
		$viviposTypes = array();
		while( $r = mysql_fetch_assoc( $result ) ) {
			$viviposTypes[$r['id']] = $r['vivipos_name'];
		}
		
		$query = "SELECT id,vivipos_name FROM promo_trigger";
		$result = Treat_DB_ProxyOld::query( $query );
		
		$viviposTriggers = array();
		while( $r = mysql_fetch_assoc( $result ) ) {
			$viviposTriggers[$r['id']] = $r['vivipos_name'];
		}
		
		echo json_encode( 
				array( 'promos' => $data, 'viviposTypes' => $viviposTypes, 'viviposTriggers' => $viviposTriggers, ) );
		
		return false;
	}
	
	public function togglePromoActive() {
		$this->startAjax();
		
		$businessid = intval( $_REQUEST['bid'] );
		$promo_id = intval( $_REQUEST['promo_id'] );
		$query = "UPDATE promotions SET activeFlag=ABS( activeFlag - 1 ) WHERE id=$promo_id AND businessid=$businessid";
		Treat_DB_ProxyOld::query( $query );
		
		if( mysql_affected_rows() != 1 ) {
			echo json_encode( array( 'error' => mysql_error() ) );
		}
		else {
			Treat_Model_KioskSync::addSync( $businessid, $promo_id, Treat_Model_KioskSync::TYPE_PROMO );
			echo json_encode( array( 'promo_id' => $promo_id ) );
		}
		
		return false;
	}
	
	public function reorderPromo() {
		$this->startAjax();
		
		$businessid = intval( $_REQUEST['bid'] );
		$promo_id = intval( $_REQUEST['promo_id'] );
		$dir = intval( $_REQUEST['dir'] );
		
		$compArray = array(
			1 => '<', 2 => '>'
		);
		$orderArray = array(
			1 => 'DESC', 2 => 'ASC'
		);
		
		$comp = $compArray[$dir];
		$order = $orderArray[$dir];
		
		$query = "
				SELECT
					p.id,
					p.rule_order AS rule_order,
					q.rule_order AS this_order
				FROM promotions p
				JOIN promotions q
					ON q.id=$promo_id
					AND q.businessid=p.businessid
				WHERE p.rule_order $comp q.rule_order
				AND p.businessid=$businessid
				ORDER BY rule_order $order
				LIMIT 1
			";
		$result = Treat_DB_ProxyOld::query( $query );
		
		$swapWith = mysql_fetch_assoc( $result );
		
		if( $swapWith && $swapWith['id'] > 0 ) {
			$query = "
					UPDATE promotions SET
						rule_order = %s
					WHERE id = %s
				";
			
			$ruleOrder = $swapWith['this_order'];
			
			$query1 = str_replace( array( "\t", "\n", "\r" ), ' ', sprintf( $query, $ruleOrder, $swapWith['id'] ) );
			$query2 = str_replace( array( "\t", "\n", "\r" ), ' ', sprintf( $query, $swapWith['rule_order'], $promo_id ) );
			Treat_DB_ProxyOld::query( $query1 );
			Treat_DB_ProxyOld::query( $query2 );
			
			Treat_Model_KioskSync::addSync( $businessid, $promo_id, Treat_Model_KioskSync::TYPE_PROMO );
			Treat_Model_KioskSync::addSync( $businessid, $swapWith['id'], Treat_Model_KioskSync::TYPE_PROMO );
		}
		
		return false;
	}
	
	public function getSmenus() {
		$this->startAjax();
		
		$businessid = $_GET['bid'];
		$smenus = Treat_Model_SmartMenu_Object::getListByBusinessid( $businessid );
		$smenuBoards = Treat_Model_SmartMenu_Board::getListByBusinessid( $businessid, true );
		$smenuClients = Treat_Model_SmartMenu_Client::getListByBusinessid( $businessid );
		$smenuMenus = Treat_Model_SmartMenu_Menu::getListByBusinessid( $businessid );
		
		$business = \EE\Model\Business\Singleton::getSingleton();
		$smenuLocationObj = Treat_Model_SmartMenu_Location::getByBusinessid( $businessid );
		$smenuLocation = $smenuLocationObj->toArray();
		$smenuLocation['name'] = $business->businessname;
		$smenuLocation['packageList'] = Treat_Model_SmartMenu_Package::getListByCompanyid( $business->companyid );
		$smenuLocation['boardList'] = Treat_Model_SmartMenu_Board::getListByBusinessid( $business->companyid );
		
		$smenuPackages = Treat_Model_SmartMenu_Package::getListByCompanyid( $business->companyid );
		
		echo json_encode( array(
			'smenus' => $smenus ? : array(),
			'smenuBoards' => $smenuBoards ? : array(),
			'smenuClients' => $smenuClients ? : array(),
			'smenuLocation' => $smenuLocation ? : array(),
			'smenuMenus' => $smenuMenus ? : array(),
			'smenuPackages' => $smenuPackages ? : array(),
		) );
		
		return false;
	}
	
	public function updateSmenu() {
		$this->startAjax();
		
		$bid = htmlspecialchars( trim( $_POST['bid'] ) );
		$smenu_name = htmlspecialchars( trim( $_POST['smenu_name'] ) );
		$smenu_type = htmlspecialchars( trim( $_POST['smenu_type'] ) );
		$id = intval( $_POST['smenu_id'] );
		
		if( $id > 0 ) {
			$query = "UPDATE smartMenus SET
							name = '$smenu_name',
							type = '$smenu_type'
						WHERE id = $id";
			$result = Treat_DB_ProxyOld::query( $query );
			
			$message = "Record Updated";
		}
		else {
			$query = "INSERT INTO smartMenus (businessid,name,type) VALUES ($bid,'$smenu_name','$smenu_type')";
			$result = Treat_DB_ProxyOld::query( $query );
			$id = mysql_insert_id();
			
			$message = "Record Added";
		}
		
		echo json_encode( array( 'smenu_id' => $id, 'message' => $message ) );
		
		return false;
	}
	
	public function updateSmenuBoard() {
		$this->startAjax();
		
		if( empty( $_REQUEST['smenuBoard_loginKey'] ) )
			$_REQUEST['smenuBoard_loginKey'] = Treat_Model_SmartMenu_Auth::generateKey();
		if( empty( $_REQUEST['smenuBoard_loginSecret'] ) )
			$_REQUEST['smenuBoard_loginSecret'] = Treat_Model_SmartMenu_Auth::generateKey();
		if( empty( $_REQUEST['smenuBoard_boardGuid'] ) )
			$_REQUEST['smenuBoard_boardGuid'] = Treat_Model_SmartMenu_Object::createGuid();
		
		if( strlen( $_REQUEST['smenuBoard_loginKey'] ) != 30 || strlen( $_REQUEST['smenuBoard_loginSecret'] ) != 30 ) {
			echo json_encode( array( 'error' => 'Login Keys and Secrets must be exactly 30 characters long' ) );
			return false;
		}
		
		$smenuBoard_id = intval( $_REQUEST['smenuBoard_id'] );
		
		if( !empty( $smenuBoard_id ) )
			$smenuBoard = Treat_Model_SmartMenu_Board::getById( $smenuBoard_id );
		else $smenuBoard = new Treat_Model_SmartMenu_Board;
		
		//$pLink = Treat_Model_SmartMenu_PackageLink::getByBoardGuid( $smenuBoard->boardGuid );
		
		$smenuBoard->boardName = $_REQUEST['smenuBoard_boardName'];
		$smenuBoard->currentPackage = $_REQUEST['smenuBoard_currentPackage'];
		
		if( $smenuBoard->boardGuid != $_REQUEST['smenuBoard_boardGuid'] ) {
			/*if( $pLink )
				$pLink->boardGuid = $_REQUEST['smenuBoard_boardGuid'];*/
			$smenuBoard->boardGuid = $_REQUEST['smenuBoard_boardGuid'];
		}
		
		$location = Treat_Model_SmartMenu_Location::getByBusinessid( $_REQUEST['bid'] );
		$smenuBoard->locationGuid = $location->locationGuid;
		
		$auth = Treat_Model_SmartMenu_Auth::update( $smenuBoard->authId, $_REQUEST['smenuBoard_loginKey'],
				$_REQUEST['smenuBoard_loginSecret'] );
		
		$smenuBoard->authId = $auth->id;
		
		/*if( $pLink ) {
			if( $_REQUEST['smenuBoard_packageFile'] == 0 ) {
				$pLink->delete();
			} else {
				$pLink->packageFile = $_REQUEST['smenuBoard_packageFile'];
				$pLink->save();
			}
		} else {
			Treat_Model_SmartMenu_PackageLink::link( $smenuBoard->boardGuid, $_REQUEST['smenuBoard_packageFile'] );
		}*/
		
		$smenuBoard->save();
		
		echo json_encode( array( 'message' => 'Board updated successfully', 'id' => $smenuBoard->id ) );
		
		return false;
	}
	
	public function exportSmenuBoard() {
		$this->startAjax();
		
		$boardId = intval( $_REQUEST['sbid'] );
		$board = Treat_Model_SmartMenu_Board::getById( $boardId );
		
		header( 'Content-type: text/xml' );
		header( 'Content-disposition: attachment; filename="' . $board->boardName . '.setup.xml"' );
		
		echo $board->toXml();
		
		return false;
	}
	
	public function updateSmenuClient() {
		$this->startAjax();
		
		if( empty( $_REQUEST['smenuClient_loginKey'] ) )
			$_REQUEST['smenuClient_loginKey'] = Treat_Model_SmartMenu_Auth::generateKey();
		if( empty( $_REQUEST['smenuClient_loginSecret'] ) )
			$_REQUEST['smenuClient_loginSecret'] = Treat_Model_SmartMenu_Auth::generateKey();
		
		if( strlen( $_REQUEST['smenuClient_loginKey'] ) != 30 || strlen( $_REQUEST['smenuClient_loginSecret'] ) != 30 ) {
			echo json_encode( array( 'error' => 'Login Keys and Secrets must be exactly 30 characters long' ) );
			return false;
		}
		
		$smenuClient_id = intval( $_REQUEST['smenuClient_id'] );
		
		if( !empty( $smenuClient_id ) )
			$smenuClient = Treat_Model_SmartMenu_Client::getById( $smenuClient_id );
		else $smenuClient = new Treat_Model_SmartMenu_Client;
		
		$smenuClient->businessid = intval( $_REQUEST['bid'] );
		$smenuClient->clientName = $_REQUEST['smenuClient_clientName'];
		
		$auth = Treat_Model_SmartMenu_Auth::update( $smenuClient->authId, $_REQUEST['smenuClient_loginKey'],
				$_REQUEST['smenuClient_loginSecret'] );
		
		$smenuClient->authId = $auth->id;
		$smenuClient->save();
		
		echo json_encode( array( 'message' => 'Board updated successfully', 'id' => $smenuBoard->id ) );
		
		return false;
	}
	
	public function updateSmenuMenu() {
		$this->startAjax();
		
		$smenuMenu_id = intval( $_REQUEST['smenuMenu_id'] );
		
		if( !empty( $smenuMenu_id ) )
			$smenuMenu = Treat_Model_SmartMenu_Menu::getById( $smenuMenu_id );
		else $smenuMenu = new Treat_Model_SmartMenu_Menu;
		
		$smenuMenu->businessid = intval( $_REQUEST['bid'] );
		$smenuMenu->menuName = $_REQUEST['smenuMenu_menuName'];
		$smenuMenu->save();
		
		echo json_encode( array( 'message' => 'Menu updated successfully', 'id' => $smenuMenu->id ) );
		
		return false;
	}

	public function toggleSmenuActive() {
		$this->startAjax();
		
		$businessid = intval( $_REQUEST['bid'] );
		$smenu_id = intval( $_REQUEST['id'] );
		$query = "UPDATE smartMenus SET activeFlag=ABS( activeFlag - 1 ) WHERE id=$smenu_id AND businessid=$businessid";
		Treat_DB_ProxyOld::query( $query );
		
		if( mysql_affected_rows() != 1 ) {
			echo json_encode( array( 'error' => mysql_error() ) );
		}
		else {
			echo json_encode( array( 'smenu_id' => $smenu_id ) );
		}
		
		return false;
	}
	
	protected function _toggleActive( $class ) {
		try {
			$obj = $class::getById( $_REQUEST['id'] );
			$obj->activeFlag = !$obj->activeFlag;
			$obj->save();
		} catch( Exception $e ) {
			return json_encode( array( 'error' => mysql_error() ) );
		}
		
		return json_encode( array( 'id' => $obj->id ) );
	}
	
	public function toggleSmenuBoardActive() {
		$this->startAjax();
		echo $this->_toggleActive( 'Treat_Model_SmartMenu_Board' );
		return false;
	}
	
	public function toggleSmenuClientActive() {
		$this->startAjax();
		echo $this->_toggleActive( 'Treat_Model_SmartMenu_Client' );
		return false;
	}
	
	protected function _smenuOutputList( $itemListArray, $format = 'html' ) {
		$itemList = '';
		$returnArray = array();
		$itemScript = 'setupItemList( "#%%" );';
		
		if( $format == 'html' ) {
			if( is_array( $itemListArray ) ) {
				foreach( $itemListArray as $item ) {
					$itemList .= '<option name="' . $item['id'] . '">' . trim( $item['name'] ) . '</option>';
				}
			}
			elseif( is_a( $itemListArray, 'Treat_Model_SmartMenu_MenuItem_Collection' ) ) {
				$itemList = $itemListArray->toHtml();
			}
			$returnArray['content'] = $itemList;
		}
		elseif( $format == 'json' ) {
			if( is_array( $itemListArray ) ) {
				$returnArray['data'] = $itemListArray;
			}
			elseif( is_a( $itemListArray, 'Treat_Model_SmartMenu_MenuItem_Collection' ) ) {
				$returnArray['data'] = $itemListArray->toJson();
			}
			$itemScript = 'createItemList( "#%%" ); ' . $itemScript;
		}
		
		$returnArray['script'] = $itemScript;
		
		echo json_encode( $returnArray );
	}
	
	public function exportSmenuMenu() {
		$this->startAjax();
		
		$menuId = intval( $_REQUEST['smid'] );
		$menu = Treat_Model_SmartMenu_Menu::getById( $menuId );
		$collection = Treat_Model_SmartMenu_MenuItem::getCollectionByMenuId( $menuId,
				Treat_Model_SmartMenu_MenuItem_Collection::SIMPLE_MENU );
		$xml = $collection->toXml( $menu->menuName );
		
		header( 'Content-type: text/xml' );
		header( 'Content-disposition: attachment; filename="' . $menu->menuName . '.xml"' );
		
		echo $xml;
		
		return false;
	}
	
	public function getSmenuMenuItems() {
		$this->startAjax();
		
		$itemCollection = Treat_Model_SmartMenu_MenuItem::getCollectionByMenuId( $_REQUEST['id'] );
		
		$this->_smenuOutputList( $itemCollection, 'json' );
		
		return false;
	}
	
	public function getSmenuMenuItemList() {
		$this->startAjax();
		
		$itemListArray = Treat_Model_SmartMenu_MenuItem::getFilteredListByBusinessid( $_REQUEST['bid'], $_REQUEST['id'] );
		
		$this->_smenuOutputList( $itemListArray );
		
		return false;
	}
	
	public function updateSmenuMenuItems() {
		$this->startAjax();
		$smenuMenu_id = intval( $_REQUEST['smenuMenu_id'] );
		//$menuItems = array_map( 'intval', $_REQUEST['menuItems'] );
		
		$menuItems = $_REQUEST['menuItems'];
		array_walk_recursive( $menuItems,
				function (&$item, $key) {
					if( strpos( $item, 'roup' ) )
						return;
					
					$item = intval( $item );
				} );
		
		$count = Treat_Model_SmartMenu_MenuItem::updateMenuItems( $smenuMenu_id, $menuItems );
		
		echo json_encode( array( 'count' => $count ) );
		
		return false;
	}
	
	public function editSmenuMenuGroup() {
		$this->startAjax();
		$groupId = intval( $_REQUEST['groupId'] );
		$groupName = $_REQUEST['groupName'];
		$automaticName = intval( $_REQUEST['automaticName'] );
		
		$group = Treat_Model_SmartMenu_MenuGroup::getById( $groupId );
		$group->automaticName = $automaticName;
		if( !$automaticName )
			$group->groupName = $groupName;
		$group->save();
		
		return false;
	}
	
	public function getSmenuPackageLinks() {
		$this->startAjax();
		
		$businessid = intval( $_REQUEST['bid'] );
		$boardId = intval( $_REQUEST['id'] );
		
		$board = Treat_Model_SmartMenu_Board::getById( $boardId );
		$packageLinkArray = Treat_Model_SmartMenu_PackageLink::getByBoardGuid( $board->boardGuid );
		
		$packageLinks = '';
		foreach( $packageLinkArray as $pLink ) {
			$packageLinks .= '<div><span>' . $pLink->packageFile . '</span><span class="remove" id="pLink' . $pLink->id
					. '">remove</span></div>';
		}
		
		echo json_encode( array( 'content' => $packageLinks, 'script' => 'setupPLinkList( "#%%" )' ) );
		
		return false;
	}
	
	public function getSmenuPackageList() {
		$this->startAjax();
		
		$boardId = intval( $_REQUEST['boardId'] );
		
		$packageArray = Treat_Model_SmartMenu_Package::getListForBoard( $boardId );
		
		$packageList = array();
		foreach( $packageArray as $package ) {
			$packageList[$package['id']] = $package['packageFile'];
		}
		
		echo json_encode( array( 'packages' => $packageList ) );
		
		return false;
	}
	
	public function updateSmenuPackageLink() {
		$this->startAjax();
		
		$pLinkId = intval( $_REQUEST['pLinkId'] );
		$boardId = intval( $_REQUEST['boardId'] );
		$packageId = intval( $_REQUEST['packageId'] );
		
		if( $pLinkId > 0 ) {
			$pLink = Treat_Model_SmartMenu_PackageLink::getById( $pLinkId );
			$board = Treat_Model_SmartMenu_Board::getByGuid( $pLink->boardGuid );
			$package = Treat_Model_SmartMenu_Package::getByPackageFile( $pLink->packageFile );
			if( $board->currentPackage == $package->id ) {
				$board->currentPackage = null;
				$board->save();
			}
			$pLink->delete();
		}
		else {
			$board = Treat_Model_SmartMenu_Board::getById( $boardId );
			$package = Treat_Model_SmartMenu_Package::getById( $packageId );
			$pLink = Treat_Model_SmartMenu_PackageLink::link( $board->boardGuid, $package->packageFile );
		}
		
		echo json_encode( array( 'success' => true ) );
		
		return false;
	}
	
	public function getSmenuBoardControl() {
		$this->startAjax();
		
		$businessid = intval( $_REQUEST['bid'] );
		$boardId = intval( $_REQUEST['id'] );
		
		$board = Treat_Model_SmartMenu_Board::getById( $boardId );
		$controls = Treat_Model_SmartMenu_Control::getCollectionByBoardGuid( $board->boardGuid );
		
		if( $controls->count() == 0 )
			$output = '<div style="text-align: center;">There are no commands to display at this time.</div>';
		else $output = $controls->toHtml();
		
		echo json_encode( array( 'content' => $output ) );
		
		return false;
	}
	
	public function custUtilImport(){
		if($_FILES && isset($_FILES['upload']) ){
			$bid = static::getPostOrGetVariable('bid');
			
			$num_added = 0;
			$num_updated = 0;
			$num_skipped = 0;
			$skipped_users = array();
			$message = '';
			$err_message = '';
			$file_errors = '';
			
			$container = \EE\File\Upload\Csv::processFile($_FILES['upload']);  //get file container of uploaded file
			
			$file = $container->getOne();  //save file object to "file" variable
			
			$file->setFile($file->tmp_name);  //set the file name
			
			//required columns. if any are missing, misspelled, or don't exist here, processing will fail
			$req_columns = array(
					'fname',
					'lname',
					'email',
					'scancode',
					'amount'
				);
			
			$file->setBusinessId($bid);
			
			if( $file->setRequiredColumns($req_columns) ){  //all required columns were found
				if( $file->process() ){						
					foreach($file->csv_data as $data){  //iterate over each row of the csv file
						//replace apostrophe in fname, lname and email
						$data['fname'] = str_replace("'", "`", $data['fname']);
						$data['lname'] = str_replace("'", "`", $data['lname']);
						$data['email'] = str_replace("'", "`", $data['email']);
						
						//strip dollar sign in balance
						$data['amount'] = str_replace('$', '', $data['amount']);
						
						//balance must be a number
						if( !is_numeric($data['amount']) ){
							$err_message .= 'Invalid balance for '.$data['fname'].' '.$data['lname'].'<br />';
							$num_skipped++;
							$skipped_users[] = $data['fname'].' '.$data['lname'];
							continue;
						}
						
						$scancode = \EE\Model\User\Customer::validateScancode($data['scancode']);
						
						if(!$scancode){  //scancode didn't validate
							$err_message .= 'Scancode '.$data['scancode'].' for '.$data['fname'].' '.$data['lname'].' did not validate: ';
							foreach( \EE\Model\User\Customer::getValidationErrors() as $error){
								$err_message .= $error;
							}
							$err_message .= '<br />';
							$num_skipped++;
							$skipped_users[] = $data['fname'].' '.$data['lname'];
							continue;
						}
						
						$customers = \EE\Model\User\Customer::getUserByScancode($scancode);  //get customers info						
						$customer = null;
						
						if( count($customers) > 0 ){  //customer exists
							if( count($customers) > 1 ){  //more than one customer with same scancode, skip
								$err_message .= 'There is more than one customer with the scancode of '.$data['scancode'].'. You will have to update this manually.<br />';
								$num_skipped++;
								$skipped_users[] = $data['fname'].' '.$data['lname'];
								continue;
							}else{  //update existing customer
								$customer = current($customers);
							}
						}
						
						if( !isset($customer) ){  //creating new customer, see if email is a duplicate
							$email = \EE\Model\User\Customer::getUserByEmail($data['email']);

							if( $email ){  //can't reuse email address
								$err_message .= 'There is more than one customer with the email of '.$data['email'].'. You will have to update this manually.<br />';
								$num_skipped++;
								$skipped_users[] = $data['fname'].' '.$data['lname'];
								continue;
							}
							
							if($data['amount'] < 0){  //amount must be greater or than equal to zero for new customers
								$err_message .= $data['fname'].' '.$data['lname'].' has an invalid amount (less than zero).<br />';
								$num_skipped++;
								$skipped_users[] = $data['fname'].' '.$data['lname'];
								continue;
							}
						} else {  //existing customer
							//data in csv doesn't match data in DB for supplied scancode -- wrong person?
							if( $customer->username != $data['email'] || $customer->scancode != $scancode ){
								$err_message .= 'Email address and/or scancode do not match saved data for '.$data['fname'].' '.$data['lname'].'<br />';
								$num_skipped++;
								$skipped_users[] = $data['fname'].' '.$data['lname'];
								continue;
							}
							
							if($data['amount'] <= 0){  //amount must be greater than zero for existing customers
								$err_message .= 'Existing customer '.$data['fname'].' '.$data['lname'].' has an invalid amount (not greater than zero).<br />';
								$num_skipped++;
								$skipped_users[] = $data['fname'].' '.$data['lname'];
								continue;
							}
						}

						\EE\Model\User\Customer::db()->beginTransaction();
						
						if( !isset($customer) ){  //create new customer
							$customer = new \EE\Model\User\Customer;

							$customer->username = $data['email'];
							$customer->businessid = $bid;
							$customer->scancode = $scancode;
							$customer->first_name = $data['fname'];
							$customer->last_name = $data['lname'];
							
							$num_added++;
						} else {
							$num_updated++;
						}
						
						$customer->temp_password = 'essential1';
						$customer->temp_used = 0;
						
						$customer->save();

						if($data['amount'] > 0){  //don't insert into customer transaction for zero balance
							//insert record into customer_transactions table, add to balance if one is there already
							$trans_res = \EE\Model\User\Customer\Transaction::processCustomerTransaction(
									$customer,
									\EE\Model\User\Customer\Transaction::TRANSACTION_TYPE_PROMO,
									$data['amount'],
									$_SESSION->getUser()->username,
									false
							);
						}

						\EE\Model\User\Customer::db()->commit();
						
						$customer = null;
					}
					
					$message .= '<p><strong>Adding/updating the customer data is complete.</strong></p>';
				} else {
					$file->addError('There was an error processing the file.');
				}
			} else {  //not all required columns were found, processing failed
				$file->addError('Required columns are missing or are misspelled.');
			}
			
			$errors = $file->getErrors();
			foreach($errors as $error){
				$file_errors .= $error.'<br />';
			}
			
			if( !empty($file_errors) ){
				print $file_errors;
			}
			
			print $message;
			
			if( !empty($err_message) ){
				print '<p><a href="#" onclick="toggle_errors(); return false;" id="show_errors">Show Errors</a></p>'.
						'<div style="display:none;" id="errors">'.$err_message.'</div>';
			}
			
			print '<p><strong>Customers added:</strong> '.$num_added.
					'</p><p><strong>Customers updated:</strong> '.$num_updated.
					'</p><p><strong>Customers skipped:</strong> '.$num_skipped.'</p>';
			
			if( count($skipped_users) > 0 ){
				print '<p><a href="#" onclick="toggle_skipped(); return false;" id="show_skipped">Show Skipped Customers</a></p>'.
						'<div style="display:none;" id="skipped">';
				
				foreach($skipped_users as $su){
					print $su.'<br />';
				}
				
				print '</div>';
			}
		} else {
			print 'No file was uploaded.';
		}
		
		return false;
	}
	
	public function custUtilExport(){
		$bid = static::getPostOrGetVariable('bid');
		
		$users = \EE\Model\User\Customer::getUsersByBusinessId($bid);  //get all users for current business id
		
		//mapping header columns to be user-friendly and match import
		$header_cols = array(
			'first_name' => 'fname',
			'last_name' => 'lname',
			'username' => 'email',
			'scancode' => 'scancode',
			'balance' => 'amount'
		);
		
		$container = new \EE\File\Export\Csv();  //instantiate CSV object		
		
		$container->setType('csv');			
		$container->setUseHeaderRow(true);
		
		if( $container->setHeaderCols($header_cols) ){  //if set col headers for export
			$container->setFile('cust-util-export.csv');  //file we will write to
			
			$counter = 0;
			$data = array();

			//set data for export, display zero balances
			foreach($users as $user){
				$data[$counter]['first_name'] = $user->first_name;
				$data[$counter]['last_name'] = $user->last_name;
				$data[$counter]['username'] = $user->username;
				$data[$counter]['scancode'] = $user->scancode;
				$data[$counter]['balance'] = 0;

				$counter++;
			}
			
			if( $container->setData($data) ){  //successfully added data for export
				$container->setHttpHeaders();  //send csv headers to browser
				$csv = $container->toCsv();
				print $csv;
			} else {
				print 'Error setting data for export';
			}
		} else {
			print 'Error setting column headers';
		}
		
		return false;
	}
	
	public function getStaticModifierSettings( ) {
		return AjaxWrapper::process( array(
			AjaxWrapperField::bid( )
		), function( array $inputs ) {
			$businessid = $inputs['bid'];
				
			$tokens = Treat_Model_Aeris_Setting::getMapTokens( (array)'mdock' );
			$rv = Treat_Model_Aeris_Setting::getMapSettings( $businessid, $tokens );
			
			return $rv;
		} );
	}
	
	public function getStaticModifierDock( ) {
		return AjaxWrapper::process( array(
			AjaxWrapperField::bid( )
		), function( array $inputs ) {
			return array( 'groups' => \EE\Model\Aeris\Modifier\StaticDock::getGroupsByBusinessid( $inputs['bid'], true ) );
		} );
	}
	
	public function updateStaticModifierSettings( ) {
		return AjaxWrapper::process( array(
			AjaxWrapperField::bid( ),
			new AjaxWrapperField( 'mdock_rows', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_NEWID, 'Invalid rows' ),
			new AjaxWrapperField( 'mdock_cols', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_NEWID, 'Invalid cols' ),
			new AjaxWrapperField( 'mdock_scrollbar', AjaxWrapperField::FIELD_BOOLEAN ),
		), function( array $inputs ) {
			$businessid = $inputs['bid'];
			unset( $inputs['bid'] );
			Treat_Model_Aeris_Setting::updateMapSettings( $businessid, $inputs );
			return array( 'id' => $businessid, 'message' => 'Successfully updated static modifier settings' );
		} );
	}
	
	public function getStaticModifierGroups( ) {
		return AjaxWrapper::process( array(
			AjaxWrapperField::bid( )
		), function( array $inputs ) {
			return array( 'data' => \EE\Model\Aeris\Modifier\StaticDock::getGroupsForDock( $inputs['bid'], true ) );
		} );
	}
	
	public function addStaticModifierToDock( ) {
		return AjaxWrapper::process( array(
			AjaxWrapperField::bid( ),
			new AjaxWrapperField( 'groups', AjaxWrapperField::FIELD_ARRAY, AjaxWrapperField::FIELD_DECIMAL, 'Invalid groups' )
		), function( array $inputs ) {
			foreach( $inputs['groups'] as $groupId ) {
				$dockGroup = new \EE\Model\Aeris\Modifier\StaticDock( array(
					'businessid' => $inputs['bid'],
					'modifierGroupId' => $groupId
				) );
				if( !$dockGroup->save( ) )
					throw new Exception( 'Failed to add modifier group ' . $groupId );
			}
			
			return true;
		} );
	}
	
	public function removeStaticModifierFromDock( ) {
		return AjaxWrapper::process( array(
			AjaxWrapperField::bid( ),
			new AjaxWrapperField( 'id', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID, 'Invalid dock group id' )
		), function( array $inputs ) {
			$dockGroup = \EE\Model\Aeris\Modifier\StaticDock::getById( $inputs['id'] );
			if( !$dockGroup )
				throw new Exception( 'Invalid dock group: ' . $inputs['id'] );
			if( !$dockGroup->delete( ) )
				throw new Exception( 'Failed to remove modifier group ' . $inputs['id'] );
			
			return true;
		} );
	}
	
	public function updateStaticModifierDockOrder( ) {
		return AjaxWrapper::process( array(
			AjaxWrapperField::bid( ),
			new AjaxWrapperField( 'order', AjaxWrapperField::FIELD_ARRAY )
		), function( array $inputs ) {
			\EE\Model\Aeris\Modifier\StaticDock::resetProcessed( $inputs['bid'] );
			
			$ex = null;
			try {
				foreach( $inputs['order'] as $group ) {
					$dockGroup = \EE\Model\Aeris\Modifier\StaticDock::getById( $group['id'] );
					if( !$dockGroup )
						throw new Exception( 'Invalid modifier group: ' . $group['id'] );
					$dockGroup->display_x = $group['x'];
					$dockGroup->display_y = $group['y'];
					$dockGroup->processed = true;
					if( !$dockGroup->save( ) )
						throw new Exception( 'Failed to reorder modifier group ' . $group['id'] );
				}
			} catch( Exception $e ) {
				$ex = $e;
			}
			
			\EE\Model\Aeris\Modifier\StaticDock::cleanUnprocessed( $inputs['bid'] );
			
			if( $ex )
				throw $ex;
			
			return 'Successfully updated modifier dock order';
		} );
	}

	public function getUserFunctions( ) {
		return AjaxWrapper::process( array(
			new AjaxWrapperField( 'customerid', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID, 'Invalid customer id'),
		), function( array &$inputs ) {
			return array(
				'function' => \EE\Model\Aeris\UserFunction::getFunctionsForCustomer( $inputs['customerid'] )
			);
		} );
	}

	public function addCannedUserFunction( ) {
		return AjaxWrapper::process( array(
			new AjaxWrapperField( 'customerid', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID, 'Invalid customer id' ),
			new AjaxWrapperField( 'fnname', AjaxWrapperField::FIELD_STRING, AjaxWrapperField::OPTIONS_NAME, 'Invalid function name' ),
		), function( array &$inputs ) {
			$fns = \EE\Model\Aeris\UserFunction::getCannedFunctionList( );
			$fnspec = $fns[$inputs['fnname']];
			$fn = new \EE\Model\Aeris\UserFunction( array(
				'customerid' => $inputs['customerid'],
				'name' => $inputs['fnname'],
				'controller' => $fnspec['controller'],
				'method' => $fnspec['method'],
				'args' => $fnspec['args']
			) );
			$fn->save( );
		} );
	}

	public function removeUserFunction( ) {
		return AjaxWrapper::process( array(
			new AjaxWrapperField( 'customerid', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID, 'Invalid customer id' ),
			new AjaxWrapperField( 'fnid', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID, 'Invalid function id' ),
		), function( array &$inputs ) {
			$fn = \EE\Model\Aeris\UserFunction::getById( $inputs['fnid'] );
			$fn->delete( );
		} );
	}

	public function getFPAssignment( ) {
		return AjaxWrapper::process( array(
			AjaxWrapperField::bid( )
		), function( array &$inputs ) {
			$businessid = intval( $inputs['bid'] );
			$available = \EE\Model\Aeris\FunctionPanel::getListByBusinessId( $businessid );
			$assigned = \EE\Model\Aeris\FPSync::getPanelListByBusinessId( $businessid );
			$assignedIds = array();

			array_walk( $assigned, function( $a ) use( &$assignedIds ) {
				$assignedIds[] = intval( $a['fpanel_id'] );
			} );

			$available = array_filter( $available, function( $a ) use( $assignedIds ) {
				return !in_array( intval( $a['id'] ), $assignedIds );
			} );

			return array( 'available' => $available, 'assigned' => $assigned );
		} );
	}

	public function addFPPage( ) {
		return AjaxWrapper::process( array(
			AjaxWrapperField::bid( ),
			new AjaxWrapperField( 'fpid', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID, 'Invalid function panel id' ),
		), function( array &$inputs ) {
			if( \EE\Model\Aeris\FPSync::checkExists( $inputs['bid'], $inputs['fpid'] ) ) {
				throw new Exception( 'Panel already linked' );
			}

			$sync = new \EE\Model\Aeris\FPSync( array( 'fpanel_id' => $inputs['fpid'], 'businessid' => $inputs['bid'] ) );
			$sync->save( );
		} );
	}

	public function removeFPPage( ) {
		return AjaxWrapper::process( array(
			AjaxWrapperField::bid( ),
			new AjaxWrapperField( 'spid', AjaxWrapperField::FIELD_DECIMAL, AjaxWrapperField::OPTIONS_ID, 'Invalid link id' ),
		), function( array &$inputs ) {
			$sync = \EE\Model\Aeris\FPSync::getById( $inputs['spid'] );
			if( !$sync || $sync->businessid != $inputs['bid'] ) {
				throw new Exception( 'Panel not linked' );
			}

			$sync->delete( );
		} );
	}

	public function updateFPOrder( ) {
		return AjaxWrapper::process( array(
			AjaxWrapperField::bid( ),
			new AjaxWrapperField( 'pages', AjaxWrapperField::FIELD_ARRAY ),
		), function( array &$inputs ) {
			$order = 0;
			$pages = array_filter( $inputs['pages'], function( $page ) {
				return intval( $page ) > 0;
			} );

			array_walk( $pages, function( &$page ) use( &$order ) {
				$pageid = intval( $page );
				$page = \EE\Model\Aeris\FPSync::getById( $pageid );
				if( !$page ) {
					throw new Exception( sprintf( 'Invalid page id %d', $pageid ) );
				}
				$page->order = $order++;
			} );

			array_walk( $pages, function( $page ) {
				$page->save( );
			} );
		} );
	}

}
