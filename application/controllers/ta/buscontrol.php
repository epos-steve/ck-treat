<?php

class Ta_BuscontrolController extends Treat_Controller_Abstract
{
#	protected $_layout = 'ta_buscontrol.tpl';
	protected $_passed_security_check;
	protected $_popup2_email_targets = array(
		'ed' => 'peteb@treatamerica.com',
		'renee' => 'renees@treatamerica.com',
	);
	protected $_popup2_url = '/ta/buscontrol2.popup.php?bid=%d&cid=%d&aid=%d&viewmonth=%s';
	
	
	public function buscontrol2() {
		
	}
	
	
	public function control_sheet() {
		
	}
	
	
	public function control_sheet2() {
#		$prevperiod = $this->getPostOrGetVariable( 'prevperiod' );
#		$nextperiod = $this->getPostOrGetVariable( 'nextperiod' );
#		$viewmonth  = $this->getPostOrGetVariable( 'viewmonth' );
#		$viewmonth  = Treat_Date::getBeginningOfMonth( $viewmonth, true );
#		$audit      = $this->getPostOrGetVariable( 'audit' );
		
		$this->_control_sheet_setup();
		
		$this->_view->assign( 'uri', $this->_uri );
		
#		$this->_view->assign( 'prevperiod', $prevperiod );
#		$this->_view->assign( 'nextperiod', $nextperiod );
#		$this->_view->assign( 'viewmonth', $viewmonth );
#		$this->_view->assign( 'audit', $audit );
	}
	
	
	protected function _control_sheet_setup() {
		static::$_css = array();
		$this->_layout = 'ta_buscontrol.tpl';
		static::addCss( '/assets/css/preload/preLoadingMessage.css' );
		static::addCss( '/assets/css/ta/generic.css' );
		static::addCss( '/assets/css/ta/buscontrol.css' );
		static::addJavascript( '/assets/js/jquery-1.4.3.min.js' );
		static::addJavascriptAtTop( '/assets/js/preload/preLoadingMessage.js' );
		static::addJavascript( '/assets/js/ta/buscontrol2-functions.js' );
		static::addJavascript( '/assets/js/dynamicdrive-disableform.js' );
	}
	
	
	public function csv_import_budget() {
		$company_id  = $this->getPostOrGetVariable( array( 'companyid',  'cid' ) );
		$business_id = $this->getPostOrGetVariable( array( 'businessid', 'bid' ) );
		$date        = $this->getPostOrGetVariable( 'date1' );
#		$import_number = $this->getPostOrGetVariable( 'import_number', 1 );
		
		if ( $_POST && 9 == $_SESSION->getUser()->security_level ) {
			if (
				defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION
				&& defined( 'DEBUG_STOP_REDIRECT' ) && DEBUG_STOP_REDIRECT
			) {
				echo '<pre>';
				echo '$_FILES = ';
				var_dump( $_FILES );
				
				echo '$_POST = ';
				var_dump( $_POST );
				echo '</pre>';
			}
			
			try {
				$csv_import = new Treat_GeneralLedger_Csv_Import_Budget( DIR_UPLOADS, $business_id );
				$csv_import->setFileUpload( $_FILES, 'uploadedfile' );
				$newdate = $csv_import->setDate( $date );
				$process = $csv_import->process();
				$import  = $csv_import->import();
				
				if (
					defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION
					&& defined( 'DEBUG_STOP_REDIRECT' ) && DEBUG_STOP_REDIRECT
				) {
					echo '<pre>';
					foreach ( $import as $k => $v ) {
						echo 'array item ', $k, ': ';
						var_dump( $v );
					}
					echo '</pre>';
				}
			}
			catch ( Treat_GeneralLedger_Csv_Import_Exception $e ) {
				var_dump( $e );
			}
		}
		
		if ( !isset( $newdate ) ) {
			$newdate = Treat_Date::getBeginningOfMonth( $date, true );
		}
		
		$location = $this->_getBuscontrol2Location( $business_id, $company_id, $newdate );
		if (
			defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION
			&& defined( 'DEBUG_STOP_REDIRECT' ) && DEBUG_STOP_REDIRECT
		) {
			echo sprintf(
				'<pre>$location = "<a href="%s">%s</a>"</pre>'
				,$location
				,$location
			), PHP_EOL;
		}
		else {
			header( 'Location: ' . $location );
		}
		
		return false;
	}
	
	
	public function csv_import_final() {
		$company_id  = $this->getPostOrGetVariable( array( 'companyid',  'cid' ) );
		$business_id = $this->getPostOrGetVariable( array( 'businessid', 'bid' ) );
		$date        = $this->getPostOrGetVariable( 'date1' );
		
		if ( $_POST && 9 == $_SESSION->getUser()->security_level ) {
			if (
				defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION
				&& defined( 'DEBUG_STOP_REDIRECT' ) && DEBUG_STOP_REDIRECT
			) {
				echo '<pre>';
				echo '$_FILES = ';
				var_dump( $_FILES );
				
				echo '$_POST = ';
				var_dump( $_POST );
				echo '</pre>';
			}
			
			try {
				$csv_import = new Treat_GeneralLedger_Csv_Import_Final( DIR_UPLOADS, $business_id );
				$csv_import->setFileUpload( $_FILES, 'uploadedfile' );
				$newdate = $csv_import->setDate( $date );
				$process = $csv_import->process();
				$import  = $csv_import->import();
				
				if (
					defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION
					&& defined( 'DEBUG_STOP_REDIRECT' ) && DEBUG_STOP_REDIRECT
				) {
					echo '<pre>';
					foreach ( $import as $k => $v ) {
						echo 'array item ', $k, ': ';
						var_dump( $v );
					}
					echo '</pre>';
				}
				$viewmonth = Treat_Date::getBeginningOfMonth( $date, true );
				$cs_week_end = Treat_Date::getMonthOfFridays( $viewmonth );
				$controlSheet = Treat_Model_GeneralLedger_ControlSheet::getSingleton(
					Treat_Model_GeneralLedger_AccountGroup::getAccountGroups(
						$business_id
						,$viewmonth
						,$cs_week_end
					)
				);
				$controlSheet->setBusinessId( $business_id );
				$controlSheet->setViewMonth( $viewmonth );
				$controlSheet->setMonthOfFridays( $cs_week_end );
				$operating_days = $controlSheet->getOperatingDaysAll();
				$accountGroups = $controlSheet->process( $viewmonth, $cs_week_end );
				$cs_final = $controlSheet->isFinal();
				if ( $cs_final ) {
					foreach ( $accountGroups as $accountGroup ) {
						$accounts = $accountGroup->getAccounts();
						foreach ( $accounts as $account ) {
							foreach ( $cs_week_end as $week ) {
								Treat_Model_GeneralLedger_AccountDetail::addAccountDetail(
									$account->id
									,$week
									,$account->getWeekAmount( $week )
									,0
								);
							}
						}
					}
				}
			}
			catch ( Treat_GeneralLedger_Csv_Import_Exception $e ) {
				var_dump( $e );
			}
		}
		
		if ( !isset( $newdate ) ) {
			$newdate = Treat_Date::getBeginningOfMonth( $date, true );
		}
		
		$location = $this->_getBuscontrol2Location( $business_id, $company_id, $newdate );
		if (
			defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION
			&& defined( 'DEBUG_STOP_REDIRECT' ) && DEBUG_STOP_REDIRECT
		) {
			echo sprintf(
				'<pre>$location = "<a href="%s">%s</a>"</pre>'
				,$location
				,$location
			), PHP_EOL;
		}
		else {
			header( 'Location: ' . $location );
		}
		
		return false;
	}
	
	
	public function csv_import_gl() {
		require_ta_bootstrap();
		
		$company_id  = $this->getPostOrGetVariable( array( 'companyid',  'cid' ) );
		$business_id = $this->getPostOrGetVariable( array( 'businessid', 'bid' ) );
		$date        = $this->getPostOrGetVariable( 'date1' );
		$import_number = $this->getPostOrGetVariable( 'import_number', 1 );
		
		if ( $_POST && 9 == $_SESSION->getUser()->security_level ) {
			if ( defined( 'DEBUG_OUTPUT' ) && DEBUG_OUTPUT ) {
				echo '<pre>';
				echo '$_FILES = ';
				var_dump( $_FILES );
				
				echo '$_POST = ';
				var_dump( $_POST );
				echo '</pre>';
			}
			
			
			try {
				$csv_import = new Treat_GeneralLedger_Csv_Import( DIR_UPLOADS, $business_id );
				$newdate = $csv_import->setDate( $date );
				$csv_import->setImportNumber( $import_number );
				$csv_import->setFileUpload( $_FILES, 'uploadedfile' );
				$process = $csv_import->process();
				$import  = $csv_import->import();
				
				if ( defined( 'DEBUG_OUTPUT' ) && DEBUG_OUTPUT ) {
					echo '<pre style="outline: 1px solid orange;">';
#					var_dump( $process );
					foreach ( $process as $k => $v ) {
						echo 'array item ', $k, ': ';
						var_dump( $v );
					}
					echo '</pre>';
				}
			}
			catch ( Treat_GeneralLedger_Csv_Import_Exception $e ) {
				var_dump( $e );
			}
		}
		
		if ( !isset( $newdate ) ) {
			$newdate = Treat_Date::getBeginningOfMonth( $date, true );
		}
		
		$location = $this->_getBuscontrol2Location( $business_id, $company_id, $newdate );
		if (
			defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION
			&& defined( 'DEBUG_STOP_REDIRECT' ) && DEBUG_STOP_REDIRECT
		) {
			echo sprintf(
				'<pre>$location = "<a href="%s">%s</a>"</pre>'
				,$location
				,$location
			), PHP_EOL;
		}
		else {
			header( 'Location: ' . $location );
		}
		
		return false;
	}
	
	
	public function delete_gl_items() {
		if ( $_POST && 9 == $_SESSION->getUser()->security_level ) {
			$business_id = $this->getPostVariable( 'business_id' );
			$company_id  = $this->getPostVariable( 'company_id' );
			$date        = $this->getPostVariable( 'date' );
			$type        = $this->getPostVariable( 'type' );
			if (
				$business_id
#				&& $company_id
				&& $date
				&& $type
			) {
				if ( 'week' == $type ) {
					Treat_Model_GeneralLedger_AccountDetail::deleteByWeek( $date, $business_id );
					Treat_Model_GeneralLedger_AccountItem::purgeByWeek( $date, $business_id );
				}
				elseif ( 'budget' == $type ) {
					Treat_Model_GeneralLedger_AccountBudget::deleteByMonth( $date, $business_id );
				}
			}
		}
		
		$location = $this->_getBuscontrol2Location( $business_id, $company_id, $date );
		if (
			defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION
			&& defined( 'DEBUG_STOP_REDIRECT' ) && DEBUG_STOP_REDIRECT
		) {
			echo sprintf(
				'<pre>$location = "<a href="%s">%s</a>"</pre>'
				,$location
				,$location
			), PHP_EOL;
		}
		else {
			header( 'Location: ' . $location );
		}
		
		return false;
	}
	
	
	protected function _getBuscontrol2Location( $business_id, $company_id, $newdate ) {
		$newdate = Treat_Date::getValidDateStamp( $newdate );
		if ( date( 'Y-m' ) != date( 'Y-m', $newdate ) ) {
			$date = sprintf(
				'&viewmonth=%s'
				,urlencode( date( 'Y-m-01', $newdate ) )
			);
		}
		else {
			$date = '';
		}
		
		$location = sprintf(
			'/ta/buscontrol2.php?bid=%d&cid=%d%s'
			,intval( $business_id )
			,intval( $company_id )
			,$date
		);
		
		return $location;
	}
	
	
	public function init() {
		// always need to call _passedPageSecurityCheck() in case we
		// need to load the cookie-login for the page.
		$this->_passedPageSecurityCheck();
		parent::init();
	}
	
	
	public function overwrite_account() {
		if ( 9 == $_SESSION->getUser()->security_level ) {
			$inputs = array();
			if ( $_POST && isset( $_POST['update'] ) ) {
				$inputs = new Treat_ArrayObject( $_POST );
#				echo '<pre>';
#				echo '$_POST = ';
#				var_dump( $_POST );
#				var_dump( $inputs );
#				echo '</pre>';
				
				$business_id = $this->getPostVariable( array( 'business_id', 'bid', 'businessid' ) );
				$company_id  = $this->getPostVariable( array( 'company_id',  'cid', 'companyid' ) );
				$account_id  = $this->getPostVariable( array( 'account_id',  'aid', 'accountid' ) );
				$account = Treat_Model_GeneralLedger_Account::getParentAccountById( $account_id, null );
				
				foreach ( $inputs->week as $week => $data ) {
					if (
						$data->amount
						&& $data->amount->orig != $data->amount->new
					) {
#						echo '<pre>';
#						echo '$data->amount->orig != $data->amount->new', PHP_EOL;
#						var_dump( $data->amount->orig );
#						echo ' != ';
#						var_dump( $data->amount->new );
						Treat_Model_GeneralLedger_AccountDetail::addAccountDetail(
							$account_id
							,$week
							,$data->amount->new
							,0
						);
#						echo '</pre>';
						$_SESSION->addFlashMessage( sprintf(
							'Updated <span class="account_name">%s</span> week of %s with an amount of %s'
							,$account->name
							,date( 'Y-m-d', $week )
							,money_format( '%.2n', $data->amount->new )
						) );
					}
					elseif (
						$data->percent
						&& $data->sales
						&& $data->sales->orig
						&& $data->percent->orig != $data->percent->new
					) {
						$amount = $data->sales->orig * $data->percent->new;
#						echo '<pre>';
#						echo '$data->percent->orig != $data->percent->new', PHP_EOL;
#						var_dump( $data->percent->orig );
#						echo ' != ';
#						var_dump( $data->percent->new );
#						echo '$amount = $data->sales->orig * $data->percent->new', PHP_EOL;
#						echo sprintf(
#							'"%s" = "%s" * "%s"'
#							,$amount
#							,$data->sales->orig
#							,$data->percent->new
#						), PHP_EOL;
#						var_dump( $amount );
						Treat_Model_GeneralLedger_AccountDetail::addAccountDetail(
							$account_id
							,$week
							,$amount
							,0
						);
#						echo '</pre>';
						$_SESSION->addFlashMessage( sprintf(
							'Updated <span class="account_name">%s</span> week of %s with an amount of %s'
							,$account->name
							,date( 'Y-m-d', $week )
							,money_format( '%.2n', $amount )
						) );
					}
				}
				
				
				$location = $this->_getBuscontrol2Location( $business_id, $company_id, $inputs->viewmonth );
				if (
					defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION
					&& defined( 'DEBUG_STOP_REDIRECT' ) && DEBUG_STOP_REDIRECT
				) {
					echo sprintf(
						'<pre>$location = "<a href="%s">%s</a>"</pre>'
						,$location
						,$location
					), PHP_EOL;
				}
				else {
					header( 'Location: ' . $location );
				}
				
				return false;
			}
			else {
				static::addCss( '/assets/css/ta/generic.css' );
				static::addCss( '/assets/css/ta/buscontrol.css' );
				if ( $_POST ) {
					$inputs = new Treat_ArrayObject( $_POST );
				}
				else {
					$inputs = new Treat_ArrayObject( $_GET );
				}
				$data = new Treat_ArrayObject( json_decode( $inputs->data, true ) );
				
				$account = Treat_Model_GeneralLedger_Account::getParentAccountById( $inputs->aid, null );
				
				$this->_view->assign( 'data', $data );
				$this->_view->assign( 'business_id', $inputs->bid );
				$this->_view->assign( 'company_id', $inputs->cid );
				$this->_view->assign( 'account_id', $inputs->aid );
				$this->_view->assign( 'viewmonth', Treat_Date::getValidDateStamp( $inputs->viewmonth ) );
				$this->_view->assign( 'account', $account );
			}
		}
	}
	
	
	protected function _passedPageSecurityCheck() {
		if ( !$this->_passed_security_check ) {
			switch ( $this->_uri->getAction( true ) ) {
				case 'control_sheet2':
					$this->_passed_security_check = false;
					$passed_cookie_login = $this->_getCookieLogin();
					$login_user = Treat_Model_User_Login::login( $GLOBALS['user'], $GLOBALS['pass'] );
					if (
	#					$passed_cookie_login
						$login_user
						&& (
							(
								$GLOBALS['security_level'] == 1
								&& $GLOBALS['bid'] == $GLOBALS['businessid']
								&& $GLOBALS['cid'] == $GLOBALS['companyid']
							)
							|| (
								$GLOBALS['security_level'] > 1
								&& $GLOBALS['cid'] == $GLOBALS['companyid']
							)
						)
					) {
	#					echo '<pre>Ta_BuscontrolController->_passedPageSecurityCheck()</pre>';
						$this->_passed_security_check = true;
					}
					break;
				default:
					$this->_passed_security_check = true;
					break;
			}
		}
		return $this->_passed_security_check;
	}
	
	
	public function popup2() {
		$this->_layout = 'application.tpl';
#		define( 'DEBUG', true );
		$company_id  = $this->getPostOrGetVariable( array( 'companyid',  'cid' ) );
		$business_id = $this->getPostOrGetVariable( array( 'businessid', 'bid' ) );
		$viewmonth   = $this->getPostOrGetVariable( 'viewmonth' );
		$viewmonth   = Treat_Date::getBeginningOfMonth( $viewmonth, true );
		$parent_account_id = $this->getPostOrGetVariable( array( 'aid', 'account_id' ) );
		
		$location = sprintf(
			$this->_popup2_url
			,$business_id
			,$company_id
			,$parent_account_id
			,date( 'Y-m-d', $viewmonth )
		);
		
		if ( $_POST ) {
			$_POST = new Treat_ArrayObject( $_POST, ArrayObject::ARRAY_AS_PROPS );
			if ( $_POST->email ) {
				$_POST->email = new Treat_ArrayObject( $_POST->email, ArrayObject::ARRAY_AS_PROPS );
			}
			else {
				$_POST->email = new Treat_ArrayObject();
			}
			$user = $_SESSION->getUser();
			
			$content = '';
			if ( $_POST['content'] ) {
				$content = base64_decode( $_POST['content'] );
				$content = html_entity_decode( $content );
			}
			if ( $_POST['comment'] ) {
				if ( $content ) {
					$content = ''
						. $_POST['comment']
						. "\r\n"
						. '<br><hr>'
						. "\r\n"
						. $content
					;
				}
				else {
					$content = $_POST['comment'];
				}
			}
			
			$email_to = 0;
			$email = new Treat_Mail_GeneralLedgerPopup( $content );
			$name = $user->firstname . ' ' . $user->lastname;
			$email->setFrom( $user->email, $name );
			if ( $_POST->email_address ) {
				$email->addTo( $_POST->email_address );
				$email_to++;
			}
			if ( 'on' == $_POST->email->renee ) {
				$email->addTo( $this->_popup2_email_targets['renee'], 'Renee' );
				$email_to++;
			}
			if ( 'on' == $_POST->email->ed ) {
				if ( $email_to ) {
					$email->addCc( $this->_popup2_email_targets['ed'], 'Ed' );
				}
				else {
					$email->addTo( $this->_popup2_email_targets['ed'], 'Ed' );
				}
			}

			try {
				$email->send();
				$_SESSION->setFlashMessage( 'Email successfully sent.' );
			}
			catch ( Zend_Mail_Transport_Exception $e ) {
				$_SESSION->setFlashError( 'Email was unable to be sent.' );
			}
			header( 'Location: ' . $location );
			return false;
		}
		Treat_Controller_Abstract::addBodyClass( 'buscontrol2_popup' );
		$this->_view->assign( 'businessid', $business_id );
		$this->_view->assign( 'companyid', $company_id );
		$this->_view->assign( 'parent_account_id', $parent_account_id );
		$this->_view->assign( 'viewmonth', $viewmonth );
		$this->_view->assign( 'location', $location );
		$accountParent = Treat_Model_GeneralLedger_Account::getParentAccountById(
			$parent_account_id
			,$viewmonth
		);
		$this->_view->assign( 'accountParent', $accountParent );
		
		static::addCss( '/assets/css/ta/generic.css' );
		static::addCss( '/assets/css/ta/buscontrol.css' );
		static::addJavascriptAtTop( '/assets/js/preload/preLoadingMessage.js' );
#		static::addJavascript( '/assets/js/jquery-1.4.3.min.js' );
#		static::addJavascript( '/assets/js/ta/buscontrol2-functions.js' );
#		static::addJavascript( '/assets/js/dynamicdrive-disableform.js' );
		
		$accounts = Treat_Model_GeneralLedger_Account::getChildAccountsByParentId(
			$parent_account_id
			,$viewmonth
		);
		foreach ( $accounts as &$account ) {
			$account['account_items'] = Treat_Model_GeneralLedger_AccountItem::getAccountItemsByAccountId(
				$account->id
				,$viewmonth
			);
			if ( !$account['account_items'] ) {
				$account['account_items'] = array();
			}
		}
		$this->_view->assign( 'accounts', $accounts );
		
		
	}
	
	
	
}