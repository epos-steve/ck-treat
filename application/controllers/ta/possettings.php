<?php
error_reporting(E_ALL);
/**
 * POS settings controller
 *
 * @author Andrew Joseph <andrew@essential-elements.net>
 * @todo mapping.tpl line 6 - receive controller doesn't exist, what is this I don't even?
 * @todo security checks
 */
class Ta_POSSettingsController extends Treat_Controller_UnitAbstract
{

	/** @var string */
	const BASE_URI				= '/ta/possettings/';

	/** @var int business id */
	private $bid;

	/** @var integer company id */
	private $cid;

	/** @var integer client_program id */
	private $pid;

	/** @var Treat_Model_User_Object_Login */
	private $user;

	/** @var array */
	protected $_noLoginRequired = array(
	);

	/** @var string */
	public $query_vars;

	/** @var string filename for layout template */
	protected $_layout;

	public function init()
	{
		mb_internal_encoding("UTF-8");
		mb_http_output("UTF-8");
		ob_start("mb_output_handler");
		$this->user = $_SESSION->getUser(Treat_Session::TYPE_USER);

		if (!($this->user instanceof Treat_Model_User_Object_Login)) {
			header('Location: /');
			return(false);
		}
		$this->bid = (int)$_SESSION->USER->getBusinessId();
		$this->cid = (int)$_SESSION->USER->getCompanyId();
		if (isset($_REQUEST['bid']) && (int)$_REQUEST['bid'] > 0) {
			$this->bid = (int)$_REQUEST['bid'];
			$this->query_vars = "bid=" . $this->bid;
		}
		if (isset($_REQUEST['cid']) && (int)$_REQUEST['cid'] > 0) {
			$this->cid = (int)$_REQUEST['cid'];
			$this->query_vars .= "&cid=" . $this->cid;
		}

		if (isset($_REQUEST['pid']) && (int)$_REQUEST['pid'] > 0) {
			$this->pid = (int)$_REQUEST['pid'];
		}

		// check if request is looking for an image in the wrong place
		$filename_ext = substr($_SERVER['REDIRECT_URL'], -3);
		if ($filename_ext == 'gif' || $filename_ext == 'jpg' || $filename_ext == 'png') {
			$redirect_url = '/assets/images/' .  substr($_SERVER['REDIRECT_URL'], strrpos($_SERVER['REDIRECT_URL'], '/'));
			header('Location: ' . $redirect_url);
			$this->_layout = FALSE;
			return false;
		}
		if (substr($_SERVER['REDIRECT_URL'], -1) != '/') {
			$redirect_url = $_SERVER['REDIRECT_URL'] . '/?' . $_SERVER['QUERY_STRING'];
			header('Location: ' . $redirect_url);
			$this->_layout = FALSE;
			return false;
		}
		$this->_layout = 'unitview.tpl';
		if ($this->_isXHR) {
			$this->_layout = false;
		} elseif (!empty($_REQUEST['bid'])) {
			$this->_layout = 'unitview.tpl';
		}

		$base_href = 'http://' . $_SERVER['HTTP_HOST'] . self::BASE_URI;
		if (substr($base_href, -1) == '/') {
			$base_href = substr($base_href, 0, strlen($base_href) - 1);
		}
		$this->_view->assign('base_href',	$base_href);
		$this->_view->assign('query_vars',	$this->query_vars);
		$this->_view->assign('bid',			$this->bid);
		$this->_view->assign('cid',			$this->cid);
		$this->addCss('/assets/css/manage.css');
		parent::init();
	}

	public function index()
	{
		$this->addJavascriptAtTop('/assets/js/jquery.timers-1.2.js');
		$clients = Treat_Model_ClientPrograms::getByBusinessId($this->bid);

		$this->_view->assign('uri',			$this->_uri->getController());
		$this->_view->assign('user',		$this->user);
		$this->_view->assign('bid',			$this->bid);
		$this->_view->assign('cid',			$this->cid);
		$this->_view->assign('clients',		$clients);
		$this->_view->assign('is_admin',	(int)$this->user->pos_settings);
	}

	public function manage()
	{
		$this->addJavascriptAtTop('/assets/js/jquery.timers-1.2.js');

		if ($this->pid == NULL) {
			throw new Treat_Exception("Client program not found");
		}
		$clients = Treat_Model_ClientPrograms::getByBusinessId($this->bid);
		$client = Treat_Model_ClientPrograms::get("client_programid = " . $this->pid, TRUE);
		$posTypes = new \Treat_Model_Manage_PosType();
		$this->_view->assign('client',		$client);

		try {
			$this->_view->assign('clients', $clients);
			$this->_view->assign('posTypes', $posTypes->getAll());
			$db = Treat_DB::singleton();
			if ($db instanceof SiTech_DB) {
				$this->_view->assign('schedules',		$client->getSchedules());
				$this->_view->assign('mappingTypes',	Treat_Model_Manage_MappingType::get());
				$this->_view->assign('mapping',			Treat_Model_Manage_Mapping::getByClient($client));
				$this->_view->assign('transtypes',		Treat_Model_Manage_Transtypes::get());
				$this->_view->assign('transmapping',	Treat_Model_Manage_TransMapping::getByClient($client));
				$this->_view->assign('credits',			$client->getCredits());
				$this->_view->assign('debits',			$client->getDebits());
				$this->_view->assign('errors',			$client->getErrors());
				$this->_view->assign('jobTypes',		Treat_Model_Manage_Mapping::getByMappingType(12, $client));
				$this->_view->assign('btJobTypes',		$db->query('SELECT * FROM jobtype WHERE companyid = ?', array($client->getCompanyId()))->fetchAll(PDO::FETCH_OBJ));
				$this->_view->assign('pages',			ceil((Treat_Model_Manage_ClientErrors::getCount('client_id = '.$client->client_programid)) / 10));
				$this->_view->assign( 'posModes',		Treat_Model_Aeris_OperatingMode::getModes( ) );
			}
		} catch (PDOException $e) {
			if (!IN_PRODUCTION) {
				throw $e;
			}
		}

	}

	public function addMachine()
	{
		$this->_layout = FALSE;
		if (SiTech_Filter::isPost()) {
			$filter = new SiTech_Filter(SiTech_Filter::INPUT_POST);
			$name = $filter->input('name');
			$parent = NULL;
			if ($filter->input('parent') !== '0') {
				$parent = $filter->input('parent');
			}
			try {
				$client = Treat_Model_ClientPrograms::createClient($name, (int)$this->bid, $parent);
				$pid = $client->client_programid;
				$client = Treat_Model_ClientPrograms::get('client_programid = ' . (int)$pid, TRUE);
				$location = self::BASE_URI . 'manage/?pid=' .$client->client_programid . "&" . $this->query_vars .  "#client_schedule";
				header('Location: ' . $location);
				return FALSE;
			} catch (Treat_Exception $e) {
				$this->_view->assign('errors', $client->errors);
				return FALSE;
			}
		}
		$this->_view->assign('machines', Treat_Model_ClientPrograms::getByBusinessId($this->bid));
	}


	/**
	 *
	 * @return type
	 * @throws SiTech_Exception
	 * @todo see if this is set up on BT at all
	 */
	public function data()
	{
		if (!file_exists(SITECH_APP_PATH.'/files/'.$this->pid.'.old.txt')) throw new SiTech_Exception('Failed to load client file', 404);
		header('Cache-Control: no-cache, must-revalidate'); // HTTP/1.1
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Content-Type: text/plain');
		readfile(SITECH_APP_PATH.'/files/'.$this->pid.'.old.txt');
		return(false);
	}

	public function test()
	{
		if (!defined('IN_PRODUCTION') || IN_PRODUCTION == TRUE) {
			die("Not Authorized");
			return false;
		}
		$this->index();
		$this->_view->assign('session_object', $_SESSION);
	}

	private function isAuthorized($require_auth = FALSE)
	{
		if ($this->user->possettings != FALSE) {
			return true;
		}
		if ($require_auth) {
			die("Not Authorized");
		}
		return false;
	}

	public function errors()
	{
		$this->_layout = FALSE;
		header('Content-Type: application/json; charset=utf-8');
		//SiTech_Loader::loadModel('ClientErrors');
		json_encode(Treat_Model_Manage_ClientErrors::get('client_id = '.$this->pid.' ORDER BY error_time DESC LIMIT 10 OFFSET '.(($this->_args['page'] - 1) * 10)));
		return(false);
	}

	public function getChildren()
	{
		$this->_layout = FALSE;
		header('Content-Type: application/json; charset=utf-8');
		$clients = Treat_Model_ClientPrograms::getClients($this->pid, $this->isAuthorized());
		$arrClients = array();

		foreach ($clients as $client) {
			$pos_type = (string)$client->posType();
			if ($pos_type === FALSE) {
				$pos_type = "";
			}
			$arrClients[] = array(
				$client->client_programid,
				$client->name,
				$pos_type,
				(string)$client->showSchedule(),
				(string)$client->receiveStatus()->name,
				$client->receiveTime(),
				(string)$client->timeDiff('send'),
				$client->statusImg(),
				$client->showSendStatus(),
				(bool)$client->hasChildren(),
				(bool)$client->hasErrors(),
				(bool)$client->isKiosk()
			);
		}

		echo json_encode($arrClients);
		return(false);
	}

	public function getErrors()
	{
		$this->_layout = FALSE;
		header('Content-Type: application/json; charset=utf-8');
		if (!isset($this->pid)) {
			echo json_encode(Array('no pid specified'));
			return(false);
		}

		$errors = Treat_Model_Manage_ClientErrors::get('client_id = '.$this->pid.' AND isRead = 0');
		foreach ($errors as &$error) {
			$error->markRead();
			$error = array(
				$error->error_time,
				$error->message(),
				$error->trace()
			);
		}

		echo json_encode($errors);
		return(false);
	}

	public function getSchedule()
	{
		$this->_layout = FALSE;

		try {
			$schedule = Treat_Model_Manage_Schedule::getById($id);
		} catch (Exception $e) {
			echo "<pre>derp!</pre>\n";
			throw new Exception($e->getMessage());
		}

		header('Content-Type: application/json; charset=utf-8');
		if (!isset($_REQUEST['id'])) {
			echo -1;
			return false;
		}
		$id = (int)$_REQUEST['id'];
		$time = explode(':', $schedule->time);
		$start = explode(':', $schedule->starttime);
		$end = explode(':', $schedule->endtime);

		echo json_encode(array(
			'id' => $schedule->id,
			'isDefault' => $schedule->default,
			'day' => $schedule->day,
			'time_hour' => (int)$time[0],
			'time_min' => $time[1],
			'start_hour' => (int)$start[0],
			'start_min' => $start[1],
			'end_hour' => (int)$end[0],
			'end_min' => $end[1],
			'days' => $schedule->num_days,
			'days_back' => $schedule->days_back
		));
		return(false);
	}

	public function sendUpdates()
	{
		$this->_layout = FALSE;
		Treat_Model_ClientPrograms::sendUpdates($this->pid);
		header('Location: ' . self::BASE_URI . '?' . $this->query_vars);
		return(false);
	}

	public function requestData()
	{
		$this->_layout = FALSE;
		$get_inventory_count = (bool)(isset($_REQUEST['inv']) && (int)$_REQUEST['inv'] == 1);
		Treat_Model_ClientPrograms::requestData($this->pid, $get_inventory_count);
		header('Location: ' . self::BASE_URI . '?' . $this->query_vars);
		return(false);
	}

	public function updateTimers()
	{
		$this->_layout = FALSE;
		header('Content-Type: application/json; charset=utf-8');
		Treat_Model_ClientPrograms::getTimerUpdates();
		header('Location: ' . self::BASE_URI . '?' . $this->query_vars);
		return(false);
	}

	public function cancelRequest()
	{
		$this->_layout = FALSE;
		header('Content-Type: application/json; charset=utf-8');
		Treat_Model_ClientPrograms::cancelRequest($this->pid);
		header('Location: ' . self::BASE_URI . '?' . $this->query_vars);
		return(false);
	}

	public function restartKiosk()
	{
		$this->_layout = FALSE;
		header('Content-Type: application/json; charset=utf-8');
		Treat_Model_ClientPrograms::restartKiosk($this->pid);
		header('Location: ' . self::BASE_URI . '?' . $this->query_vars);
		return(false);
	}

	public function addMapping()
	{
		$this->_layout = FALSE;
		if (isset($_REQUEST['id'])) {
			Treat_Model_Manage_Mapping::add((int)$_REQUEST['id'], $this->pid);
		}
		header('Location: ' . self::BASE_URI . 'manage?' . $this->query_vars . '&pid=' . $this->pid . '#client_mapping');
		return false;
	}

	public function removeMapping()
	{
		$this->_layout = FALSE;
		if (isset($_REQUEST['id'])) {
			Treat_Model_Manage_Mapping::remove((int)$_REQUEST['id']);
		}
		header('Location: ' . self::BASE_URI . 'manage?' . $this->query_vars . '&pid=' . $this->pid . '#client_mapping');
		return false;
	}

	/** save mapping
	 *
	 * @todo implement credit/debit/whatever saving
	 */
	public function saveMapping()
	{
		$this->_layout = FALSE;
		var_dump($_REQUEST);
		return false;
		die;
		foreach ($_POST['credit'] as $id => $credit) {
			if (empty($credit)) continue;
			$map = Treat_Model_Manage_Mapping::get((int)$id, true);
			$map->tbl = 1;
			$map->localid = $credit;
			$map->save();
		}

		foreach ($_POST['debit'] as $id => $debit) {
			if (empty($debit)) continue;
			$map = Treat_Model_Manage_Mapping::get((int)$id, true);
			$map->tbl = 2;
			$map->localid = $debit;
			$map->save();
		}

		header('Location: ' . self::BASE_URI . 'manage?' . $this->query_vars . '&pid=' . $this->pid . '#client_mapping');
		return false;
	}

	/** save 'Transmapping'
	 *
	 * @todo implement whatever this is : done, test?
	 */
	public function saveTransmapping()
	{
		$this->_layout = FALSE;
		if (empty($_POST)) {
			header('Location: ' . self::BASE_URI . '?' . $this->query_vars);
			exit;
		}

		$client = Treat_Model_ClientPrograms::get("client_programid = " . $this->pid, TRUE);
		foreach ($_POST['credit'] as $k => $creditid) {
			if (!($transmap = \Treat_Model_Manage_TransMapping::get('client_programid = '.$client->client_programid.' AND type = '.$k, true))) {
				$transmap = new \Treat_Model_Manage_TransMapping();
				$transmap->type = $k;
				$transmap->client_programid = $client->client_programid;
			}

			$transmap->creditid = $creditid;
			$transmap->debitid = $_POST['debit'][$k];
			$transmap->save();
		}

		header('Location: ' . self::BASE_URI . 'manage?' . $this->query_vars . '&pid=' . $this->pid . '#client_mapping');
		return FALSE;
	}

	/** save client
	 *
	 * @todo test saveclient
	 */
	public function saveClient()
	{
		if (empty($_POST)) {
			header('Location: ' . self::BASE_URI . '?' . $this->query_vars);
			exit;
		}
		$db = Treat_DB::singleton('manage');
		$filter = new SiTech_Filter(SiTech_Filter::INPUT_REQUEST);

		$client_programid	= $filter->input('pid');
		$name				= $filter->input('name');
		$businessid			= $filter->input("businessid");
		$pos_type			= $filter->input("pos_type");
		$parent				= $filter->input("parent");
		$day_start			= $filter->input("day_start");
		$db_ip				= $filter->input("db_ip");
		$db_name			= $filter->input("db_name");
		$db_user			= $filter->input("db_user");
		$db_pass			= $filter->input("db_pass");
		$skip_client=(empty($_POST['skip_client']))? '0' : $_POST['skip_client'];
		$pos_mode = intval( $_POST['pos_mode'] );
		$pos_mode = $pos_mode > 0 ? '\'' . $pos_mode . '\'' : 'NULL';
		$name=str_replace("'","`",$name);
		
		$query = "UPDATE client_programs SET name = '$name', businessid = '$businessid', parent = '$parent', pos_type = '$pos_type', day_start = '$day_start', db_ip = '$db_ip', db_name = '$db_name', db_user = '$db_user', db_pass = '$db_pass', skip_client = '$skip_client', pos_mode = $pos_mode WHERE client_programid = '$client_programid'";
		$db->exec($query);

		header('Location: ' . self::BASE_URI . 'manage?' . $this->query_vars . '&pid=' . $this->pid . '#client_details');
		return false;
	}

	/** save job types
	 *
	 * @todo test savejobtypes
	 */
	public function saveJobTypes()
	{
		if (empty($_POST)) {
			header('Location: ' . self::BASE_URI . '?' . $this->query_vars);
			exit;
		}
		foreach ($_POST['jobtype'] as $jobtype => $localid) {
			$mapping = Treat_Manage_Model_Mapping::get((int)$jobtype, true);
			$mapping->localid = $localid;
			$mapping->save();
		}
		header('Location: ' . self::BASE_URI . 'manage?' . $this->query_vars . '&pid=' . $this->pid . '#job_types');
		return false;
	}

	/**
	 * save schedule
	 *
	 * @todo make this not suck
	 */
	public function saveSchedule()
	{
		$config = Treat_Config::singleton()->getSection( 'manage' );
		mysql_connect($config['host'],$config['username'],$config['password']);
		@mysql_select_db($config['database']) or die( mysql_error());
		$filter = new SiTech_Filter(SiTech_Filter::INPUT_REQUEST);

		$pid = $filter->input('pid');
		$id = $filter->input('id');
		$default = $filter->input('default');
		$day = $filter->input('day');
		$time_hour = $filter->input('time_hour');
		$time_min = $filter->input('time_min');
		$start_hour = $filter->input('start_hour');
		$start_min = $filter->input('start_min');
		$end_hour = $filter->input('end_hour');
		$end_min = $filter->input('end_min');
		$num_days = $filter->input('num_days');
		$days_back = $filter->input('days_back');

		$del = (isset($_GET['del']))? $_GET["del"] : null;

			if($del==1){
				$id=$_GET["id"];
				$pid=$_GET["pid"];

				$query = "DELETE FROM client_schedule WHERE id = '$id'";
				$result = mysql_query($query);
			}
			elseif($id>0){
				$time="$time_hour:$time_min:00";
				$starttime="$start_hour:$start_min:00";
				$endtime="$end_hour:$end_min:00";

				$query = "UPDATE client_schedule SET day = '$day', time = '$time', starttime = '$starttime', endtime = '$endtime', num_days = '$num_days', days_back = '$days_back' WHERE id = '$id'";
				$result = mysql_query($query);
			}
			else{
				$time="$time_hour:$time_min:00";
				$starttime="$start_hour:$start_min:00";
				$endtime="$end_hour:$end_min:00";

				$query = "INSERT INTO client_schedule (client_programid,day,time,starttime,endtime,num_days,days_back) VALUES ('$pid','$day','$time','$starttime','$endtime','$num_days','$days_back')";
				$result = mysql_query($query);
				$id = mysql_insert_id();
			}

			//////only one default schedule
			if($default==1){
				$query = "UPDATE client_schedule SET `default` = '1' WHERE id = '$id'";
				$result = mysql_query($query);

				$query = "UPDATE client_schedule SET `default` = '0' WHERE client_programid = '$pid' AND id != '$id'";
				$result = mysql_query($query);
			}
			else{
				$query = "SELECT * FROM client_schedule WHERE client_programid = '$pid'";
				$result = mysql_query($query);
				$num = mysql_numrows($result);

				if($num==1){
					$query = "UPDATE client_schedule SET `default` = '1' WHERE client_programid = '$pid'";
					$result = mysql_query($query);
				}
				elseif($num>1){
					$query = "SELECT * FROM client_schedule WHERE client_programid = '$pid' AND `default` = '1' AND id != '$id'";
					$result = mysql_query($query);
					$num = mysql_numrows($result);

					if($num==1){}
					else{
						$query = "UPDATE client_schedule SET `default` = '0' WHERE id = '$id'";
						$result = mysql_query($query);

						$query = "SELECT id FROM client_schedule WHERE client_programid = '$pid' AND id != '$id' LIMIT 1";
						$result = mysql_query($query);

						$newid = mysql_result($result,0,"id");

						$query = "UPDATE client_schedule SET `default` = '1' WHERE id = '$newid'";
						$result = mysql_query($query);
					}
				}
			}

			$location = self::BASE_URI . 'manage?pid=' .$pid . "&" . $this->query_vars .  "#client_schedule";
			header('Location: ' . $location);
			return false;
	}

	public function removeSchedule()
	{
		if (empty($_POST) || !isset($_POST['id']) || !is_numeric($_POST['id'])) {
			header('Location: ' . self::BASE_URI . '?' . $this->query_vars);
			exit;
		}
		$db = Treat_DB::singleton('manage');
		$id = (int)$_POST['id'];
		$query = "DELETE FROM client_schedule WHERE id = $id";
		$db->query($query);

		$location = self::BASE_URI . 'manage?pid=' .$pid . "&" . $this->query_vars .  "#client_schedule";
		header('Location: ' . $location);
	}


}

