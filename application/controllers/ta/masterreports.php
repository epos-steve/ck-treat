<?php

class Ta_MasterreportsController extends Treat_Controller_CompanyAbstract
{
	protected $_layout = 'promotions.tpl';
	
	public function index() {
		$this->_view->assign('uri',$this->_uri->getController());
		
		$this->addJavascriptAtTop('/assets/js/jquery-1.5.1.min.js');
		$this->addJavascriptAtTop('/assets/js/jquery-ui-1.8.11.custom.min.js');
		$this->addJavascriptAtTop('/assets/js/ta/masterreports.js');
		$this->addCss('/assets/css/jq_themes/redmond/jqueryui-current.css');
		$this->addCss('/assets/css/jq_themes/redmond/jqueryui-custom.css');
		
		$limit_default = 100;
		
		$offset = static::getPostOrGetVariable('offset',0);
		$limit = static::getPostOrGetVariable('limit',$limit_default);
		
		$menu_items = Treat_Model_MasterReports_Reports::itemsSold($offset,$limit);
		
		if($offset == 0){
			$previous = false;
		} else {
			$previous = 'offset='.($offset - $limit).'&limit='.$limit;
		}
		
		if( count($menu_items) < $limit ){
			$next = false;
		} else {
			$next = 'offset='.($offset + $limit).'&limit='.$limit;
		}
		
		$items = '';
		
		foreach($menu_items as $item){
			$items .= '<tr><td>'.$item['upc'].'&nbsp;</td><td>'.$item['name'].'</td><td>'.$item['sold'].'</td></tr>';
		}
		
		$this->_view->assign('items',$items);
		$this->_view->assign('previous',$previous);
		$this->_view->assign('next',$next);
	}
}
