<?php

class Ta_InventoryController extends \EE\Controller\UnitAbstract
{
	public function saveInvCountAjax(){
		$this->user = $_SESSION->getUser(Treat_Session::TYPE_USER);
		
		//if not logged in, return false
		if (
			!($this->user instanceof Treat_Model_User_Object_Login)
			&& !($this->user instanceof \EE\Model\User\Object\Login)
		) {
			print json_encode( array('error' => 'Does not have permission to edit.', 'status' => false) );
			return false;
		}
		
		$is_safe = static::getPostVariable('safe');
		$price = static::getPostVariable('price');
		$amount = static::getPostVariable('val');
		$field_num = static::getPostVariable('fieldnum');
		$area_id = static::getPostVariable('area_id');
		$item_id = static::getPostVariable('item_id');
		$business_id = static::getPostVariable('bus_id');
		$date = static::getPostVariable('date');
		
		//get detail info on passed in item id
		$detail = \EE\Model\Inventory\AreaDetail::getDetailByAreaItemBus($area_id, $item_id, $business_id, 'stdClass' );
		
		$area_detail_id = $detail[0]->inv_areadetailid;
		$order_id =  $detail[0]->orderid;
		
		//$id_array = \EE\Model\Inventory\AreaDetail::getIdByAreaIdArray($area_id);		
		//$key = array_search($item_id, $id_array);
		
		//if( \EE\Model\Inventory\AreaDetail::updateOrderIdById($key, $area_detail_id) ){
			//print '<br />update passed<br />';
		//} else {
			//print '<br />update failed<br />';
		//}
		
		//get item details
		$item = \EE\Model\Inventory\Item::getByItemId($item_id, 'stdClass');
		
		$apaccountid = $item[0]->apaccountid;
		$units = $item[0]->units;
		$oldprice = $item[0]->price;
		$safeguard = $field_num.'.'.$area_detail_id;
		
		if($safeguard != $is_safe){
			print json_encode( array('error' => 'Safe guarded', 'status' => false) );
		}
		
		$count = new \EE\Model\Inventory\Count;
		$res = $count->getByDetailIdAndDate($area_detail_id, $date);
		
		$linetot = '$'.number_format($price * $amount,2);
		
		if( $res && $safeguard == $is_safe ){  //update			
			$count->amount = $amount;
			$count->price = $price;
			$count->apaccountid = $apaccountid;
			$count->inv_countid = $res[0]->inv_countid;
			
			if( $count->save() ){
				$total = $count->getCountTotalByBus($business_id, $date);
				print json_encode( array('status' => true, 'linetot' => $linetot, 'field_num' => $field_num, 'tot' => '$'.$total[0]->tot) );
			} else {
				print json_encode( array('error' => 'Was not saved.', 'status' => false) );
			}
		} elseif( !empty($amount) && $safeguard == $is_safe ){  //insert
			$count->inv_itemid = $item_id;
			$count->amount = $amount;
			$count->price = $price;
			$count->date = $date;
			$count->apaccountid = $apaccountid;
			$count->inv_areadetailid = $area_detail_id;
			
			if( $count->save() ){
				$total = $count->getCountTotalByBus($business_id, $date);
				print json_encode( array('status' => true, 'linetot' => $linetot, 'field_num' => $field_num, 'tot' => '$'.$total[0]->tot) );
			} else {
				print json_encode( array('error' => 'Was not saved. Empty amount.', 'status' => false) );
			}
		}
		
		return false;
	}
}