<?php

/* Scott 4-24-12 */
class Ta_KioskutilitiesController extends \EE\Controller\CompanyAbstract
{
	protected $_layout = 'kiosk_utilities.tpl';

	protected $_noLoginRequired = array(
		'kioskutilities' => array(
			'updateChanges' => true,
		),
	);

	public function copyPosMenu(){
		$user = $_SESSION->getUser();
		$companyid = $user->getCompanyId();

		# if user is not authorized to see page, redirect
		if ( !$user || !$user->kiosk_menu_mgmt ) {
			header( 'Location: /ta/businesstrack.php' );
			return false;
		}

		define( 'DOC_ROOT', realpath(dirname(__FILE__).'/../') );
		$this->_view->assign('docroot',$DOC_ROOT.'/ta/');

		$this->_view->assign('uri',$this->_uri->getAction());

		$this->addJavascriptAtTop('/assets/js/jquery-1.5.1.min.js');
		$this->addJavascriptAtTop('/assets/js/jquery-ui-1.8.11.custom.min.js');
		$this->addCss('/assets/css/jq_themes/redmond/jqueryui-current.css');
		$this->addCss('/assets/css/jq_themes/redmond/jqueryui-custom.css');

		$copy_to = static::getPostOrGetVariable( 'copy_to', $default = 0 );
		$copy_from = static::getPostOrGetVariable( 'copy_from', $default = 0 );

		$to_name = Treat_Model_Kiosk_Posmenu::getBusinessNameById($copy_to);
		$from_name = Treat_Model_Kiosk_Posmenu::getBusinessNameById($copy_from);

		$item_count = Treat_Model_Kiosk_Posmenu::getItemCount($copy_to);

		if ( $item_count >= 50 ) {
			$dis_message = "<span style='color:red;'>There are $item_count items in this location already. You can not copy over a menu!</span>";
			$disable_copy = "disabled='disabled'";
		} elseif ( $item_count > 0 ) {
			$dis_message = "<span style='color:orange;'>There are $item_count items that will be removed from this location if you copy!</span>";
			$disable_copy = "";
		} else {
			$dis_message = "";
			$disable_copy = "";
		}

		$this->_view->assign('companyid',$companyid);
		$this->_view->assign('copy_from',$copy_from);
		$this->_view->assign('copy_to',$copy_to);
		$this->_view->assign('from_name',$from_name);
		$this->_view->assign('to_name',$to_name);
		$this->_view->assign('message',$dis_message);
		$this->_view->assign('disable',$disable_copy);
	}
	
	public function index() {
		$user = $_SESSION->getUser();
		$this->_view->assign( 'title', 'Route Management' );
		$this->_view->assign( 'companyId', $user->getCompanyId() );
		$this->_view->assign('uri',$this->_uri->getAction());
		
		$this->addJavascriptAtTop('/assets/js/jquery-1.5.1.min.js');
		$this->addJavascriptAtTop('/assets/js/jquery-ui-1.8.11.custom.min.js');
		$this->addCss('/assets/css/jq_themes/redmond/jqueryui-current.css');
		$this->addCss('/assets/css/jq_themes/redmond/jqueryui-custom.css');
	}
	
	public function menuManagement() {
		$user = $_SESSION->getUser();
		$companyid = $user->getCompanyId();
		
		# if user is not authorized to see page, redirect
		if ( !$user || !$user->kiosk_menu_mgmt ) {
			header( 'Location: /ta/businesstrack.php' );
			return false;
		}

		$this->_view->assign('uri', $this->_uri->getAction());

		$this->addJavascriptAtTop('/assets/js/jquery-1.5.1.min.js');
		$this->addJavascriptAtTop('/assets/js/jquery-ui-1.8.11.custom.min.js');
		$this->addJavascriptAtTop('/assets/js/ta/menuManagement_sync.js');
		$this->addCss('/assets/css/jq_themes/redmond/jqueryui-current.css');
		$this->addCss('/assets/css/jq_themes/redmond/jqueryui-custom.css');

		$copy_from = Treat_Model_Kiosk_Posmenu::getBusinessByCompany($companyid);

		$copy_from_select = '';
		foreach($copy_from as $cf)
		{
			$copy_from_select .= '<option value="'.$cf['businessid'].'">'.$cf['businessname'].'</option>';
		}

		$this->_view->assign( 'companyId', $companyid );
		$this->_view->assign('copy_from', $copy_from_select);
		$this->_view->assign('copy_to', $copy_from_select);
		$this->_view->assign('kiosks', $copy_from_select);

		$districtsList = Treat_Model_District::getByCompany($companyid);
		$districts = array();

		foreach($districtsList as $district)
		{
			$districts[ $district->districtid ] = $district->districtname;
		}
		natcasesort($districts);

		$this->_view->assign('districts', $districts);
	}

	public function updateChanges() {
		\EE\Model\ScheduledChange::processModelScheduledToday();
		\EE\Model\ScheduledChange::processSqlScheduledToday();
		\EE\Model\ScheduledChange::processSyncedToday();
		return false;
	}

	public function offlineChangeLog() {
		$user = $_SESSION->getUser();

		if ( $user['bus_kiosk_menu_mgmt'] == 0 ) {
//			echo json_encode(array(
//				'did_change' => false,
//				'error' => 'Does not have permissions.'
//			));
//			return false;
		}

		if (!is_array($this->_args) || count($this->_args) < 1) {
			header("Content-type: application/json");
			echo json_encode(
				array(
					'didChange' => false,
					'error' => 'Nothing to do.',
				)
			);
			return false;
		}

		$businessid = intval($this->_args[0]);
		$page = intval(($this->_args[1] ?: 0));
		$limit = intval(($_GET['limit'] ?: 10));

		if ($businessid < 1) {
			header("Content-type: application/json");
			echo json_encode(
				array(
					'didChange' => false,
					'error' => 'Missing business id.',
				)
			);
			return false;
		}

		$logLocation = \EE\Model\Log\Location::page('/ta/kioskutilities/offlineChange');
		$logLocationDeleted = \EE\Model\Log\Location::page('/ta/kioskutilities/removeOfflineChangeSchedule');
		$logType = \EE\Model\Log\Type::getNamespaced('form', 'treat.kioskutilities.offlinechange');
		$logTypeId = 0;
		if (is_object($logType)) {
			$logTypeId = $logType->id;
		}

		$pagedActions = \EE\Model\Log\Action::getPage($page, $limit);
		$results = $pagedActions($logLocation->id, $logTypeId, $businessid);

		$actions = array();
		$tomorrow = strtotime('tomorrow');
		if (is_array($results)) {
			foreach ($results as $result) {
				$user = \EE\Model\User\Login::getUserById($result->user_id);
				$data = json_decode($result->changes);
				$scheduledTimestamp = strtotime($data->scheduled);
				$scheduled = '';

				if ($scheduledTimestamp >= $tomorrow) {
					$scheduled = date('Y-m-d', $scheduledTimestamp);
				}

				$output = array(
					'id' => $result->id,
					'date' => date('Y-m-d g:i A', strtotime($result->at)),
					'name' => '',
					'status' => ($data->active == 0 ? 'Inactive' : 'Active'),
					'scheduled' => $scheduled,
					'deleted' => false,
				);

				if (is_object($user)) {
					$output['name'] = $user->firstname.' '.$user->lastname;
				}

				if (isset($data->deleted)) {
					$deleteAction = \EE\Model\Log\Action::get(
						sprintf(
							'`log_type_id` = %d AND `log_location_id` = %d AND `business_id` = %d',
							$logTypeId,
							$logLocationDeleted->id,
							$result->business_id
						),
						true
					);
					if (is_object($deleteAction)) {
						$user = \EE\Model\User\Login::getUserById($deleteAction->user_id);
						$output['deleted'] = array(
							'id' => $deleteAction->id,
							'date' => date('Y-m-d g:i A', strtotime($result->at)),
							'name' => '',
							'status' => 'Removed',
							'scheduled' => '',
						);

						if (is_object($user)) {
							$output['deleted']['name'] = $user->firstname.' '.$user->lastname;
						}
					}
				}
				$actions[] = $output;
			}
		}

		header("Content-type: application/json");
		echo json_encode($actions);
		return false;
	}

	public function removeOfflineChangeSchedule() {
		$user = $_SESSION->getUser();

		$jsonData = array(
			'didChange' => false,
			'error' => '',
			'link' => '/ta/kioskutilities/offlineChangeLog/0',
		);

		if ( $user['bus_kiosk_menu_mgmt'] == 0 ) {
//			$jsonData['error'] = 'Does not have permissions.';
//			echo json_encode($jsonData);
//			return false;
		}

		if (!is_array($_POST) && !isset($_POST['log_action_id'])) {
			header("Content-type: application/json");
			$jsonData['error'] = 'Nothing to do.';
			echo json_encode($jsonData);
			return false;
		}

		$didDelete = false;

		$action = \EE\Model\Log\Action::get(sprintf('`id` = %s', intval($_POST['log_action_id'])), true);

		if (!is_object($action)) {
			header("Content-type: application/json");
			$jsonData['error'] = 'Invalid request.';
			echo json_encode($jsonData);
			return false;
		}

		$business_id = $action->business_id;
		$jsonData['link'] = str_replace('0', $business_id, $jsonData['link']);

		$data = json_decode($action->changes);
		$scheduledIds = $data->scheduledChangesId;

		foreach($scheduledIds as $id) {
			$scheduled = \EE\Model\ScheduledChange::get(sprintf('`id` = %s', intval($this->_args[0])), true);
			if (is_object($scheduled)) {
				$jsonData['didChange'] = true;
				$scheduled->delete();
			}
		}

		if ($didDelete) {
			$data->deleted = true;
			$logLocation = \EE\Model\Log\Location::page('/ta/kioskutilities/removeOfflineChangeSchedule');
			$logType = \EE\Model\Log\Type::getNamespaced('form', 'treat.kioskutilities.offlinechange');
			$logAction = new \EE\Model\Log\Action;
			$logAction->at = date('Y-m-d H:i:s');
			$logAction->log_type_id = $logType->id;
			$logAction->log_location_id = $logLocation->id;
			$logAction->business_id = $action->business_id;
			$logAction->user_id = $user->getId();
			$logAction->changes = json_encode($data);
			$logAction->save();

			$data->deleted = $logAction->id;
			$logAction->changes = json_encode($data);
			$logAction->save();

			try {
				$email = new \EE\Mail\KioskActivationChange;
				$email->changedByName($user->firstname." ".$user->lastname);
				$email->setAdditionalEmailsByBusinessId($business_id);
				$email->setScheduledDeleted();
				$email->send();
			} catch(\Exception $e) {
				header("Content-type: application/json");
				$jsonData['error'] = $e->getMessage();
				echo json_encode($jsonData);
				return false;
			}
		}

		header("Content-type: application/json");
		echo json_encode($jsonData);
		return false;
	}

	public function offlineChange() {
		$user = $_SESSION->getUser();
		$companyid = $user->getCompanyId();

		$json = array(
			'didChange' => false,
			'error' => false,
			'businessId' => 0,
		);

		if ( $user['bus_kiosk_menu_mgmt'] == 0 ) {
//			$json['error'] = 'Does not have permissions.';
//			header("Content-type: application/json");
//			echo json_encode($json);
//			return false;
		}

		if (!is_array($_POST) || count($_POST) < 1) {
			$json['error'] = 'Nothing to do.';
			header("Content-type: application/json");
			echo json_encode($json);
			return false;
		}

		$businessid = intval($_POST['businessid']);
		$active = intval($_POST['active']);

		if (!isset($_POST['extra']) || !isset($_POST['extra']['date'])) {
			$json['error'] = 'Missing Schedule date.';
			header("Content-type: application/json");
			echo json_encode($json);
			return false;
		}

		$scheduleTimestamp = strtotime($_POST['extra']['date']);
		$midnight = strtotime('midnight');

		if ($scheduleTimestamp < $midnight) {
			$json['error'] = 'Can not schedule something before today.';
			header("Content-type: application/json");
			echo json_encode($json);
			return false;
		}

		$tomorrow = strtotime("tomorrow");

		$didChange = false;
		$scheduleChangeIds = array();
		$scheduleDate = '';

		if ($scheduleTimestamp < $tomorrow) {
			// Today.
			$setupDetail = \EE\Model\SetupDetail::get("businessid = {$businessid} AND setupid = 66", true);
			if (!$setupDetail || intval($setupDetail->value) !== $active) {
				$json['didChange'] = $didChange = true;
			}

			// Scheduled for today, do right now.
			\EE\Model\SetupDetail::setChangeValue($active, $businessid, 66);
			\EE\Model\SetupDetail::setChangeValue(0, $businessid, 70);

			$settings = \EE\Model\Aeris\Setting::getKey($businessid, 'aerispos.Active');
			if($active !== intval($settings->registryValue)) {
				$json['didChange'] = $didChange = true;
				$settings->registryValue = $active;
				$settings->save();
			}

			$business = \EE\Model\Business::get("businessid = {$businessid}", true);
			$business->setup = 3;
			$business->save();

			// Remove scheduled changes
			// It is possible that a change was scheduled prior, we need to
			// remove the scheduled change to prevent it from doing anything.
			\EE\Model\ScheduledChange::removeReferencedChanges(
				\EE\Model\ScheduledChange::TYPE_MODEL,
				'treat.kioskutilities.offlinechange::business',
				$businessid
			);

			\EE\Model\ScheduledChange::removeReferencedChanges(
				\EE\Model\ScheduledChange::TYPE_MODEL,
				'treat.kioskutilities.offlinechange::setup.detail',
				$businessid
			);

			\EE\Model\ScheduledChange::removeReferencedChanges(
				\EE\Model\ScheduledChange::TYPE_MODEL,
				'treat.kioskutilities.offlinechange::setup.detail.production',
				$businessid
			);

			\EE\Model\ScheduledChange::removeReferencedChanges(
				\EE\Model\ScheduledChange::TYPE_MODEL,
				'treat.kioskutilities.offlinechange::aeris.setting',
				$businessid
			);
		} else {
			// Tomorrow, Tomorrow, everything happens, tomorrow
			$scheduleDate = date('Y-m-d', $scheduleTimestamp);

			$namespace = \EE\Model\ScheduledChange::scheduleModelChange(
				$scheduleDate,
				'\\EE\\Model\\SetupDetail',
				array(
					'value' => $active,
					'editby' => $user->username,
				),
				array(
					'businessid' => $businessid,
					'setupid' => 66,
				),
				"businessid = $businessid AND setupid = 66"
			);
			$scheduleChangeIds[] = $namespace('treat.kioskutilities.offlinechange::setup.detail', $businessid)->id;

			$namespace = \EE\Model\ScheduledChange::scheduleModelChange(
				$scheduleDate,
				'\\EE\\Model\\SetupDetail',
				array(
					'value' => 0,
					'editby' => $user->username,
				),
				array(
					'businessid' => $businessid,
					'setupid' => 70,
				),
				"businessid = $businessid AND setupid = 70"
			);
			$scheduleChangeIds[] = $namespace('treat.kioskutilities.offlinechange::setup.detail.production', $businessid)->id;

			$namespace = \EE\Model\ScheduledChange::scheduleModelChange(
				$scheduleDate,
				'\\EE\\Model\\Aeris\\Setting',
				array(
					'registryValue' => "$active",
				),
				array(
					'businessid' => $businessid,
					'registryKey' => 'aerispos.Active',
				),
				"businessid = $businessid AND registryKey = 'aerispos.Active'"
			);
			$scheduleChangeIds[] = $namespace('treat.kioskutilities.offlinechange::aeris.setting', $businessid)->id;

			$namespace = \EE\Model\ScheduledChange::scheduleModelChange(
				$scheduleDate,
				'\\EE\\Model\\Business',
				array(
					'setup' => 3,
				),
				array(),
				"businessid = $businessid"
			);
			$scheduleChangeIds[] = $namespace('treat.kioskutilities.offlinechange::business', $businessid)->id;
			$json['didChange'] = $didChange = true;
		}

		if ($active == 0) {
			$checkBusiness = \EE\Model\Kiosk\Deactivation::get('`old_business_id` = ' . intval($businessid), true);
			switch (trim(strtolower($_POST['extra']['closed_status']))) {
				case 'permanent':
					if (!is_object($checkBusiness) || $checkBusiness->id < 1) {
						\EE\Model\Kiosk\Deactivation::permanently(strip_tags($_POST['extra']['reason']), $businessid, date('Y-m-d', $scheduleTimestamp));
					} else {
						$checkBusiness->scheduled_date = date('Y-m-d', $scheduleTimestamp);
						$checkBusiness->reason = strip_tags($_POST['extra']['reason']);
						$checkBusiness->new_business_id = 0;
						$checkBusiness->closed_status = 'permanent';
						$checkBusiness->save();
					}
					break;
				case 'relocate':
					$oldBusiness = \EE\Model\Business::get('`businessid` = '.intval($businessid), true);
					$setCompanyId = function($kiosk) use ($companyid, $oldBusiness)
					{
						if (is_object($oldBusiness)) {
							$kiosk->companyid = $oldBusiness->companyid;
						} else {
							$kiosk->companyid = intval($companyid);
						}
					};
					if (!is_object($checkBusiness) || $checkBusiness->id < 1) {
						$newBusiness = \EE\Model\Business::setupKiosk($setCompanyId);
						$json['businessId'] = $newBusiness->businessid;
						\EE\Model\Kiosk\Deactivation::relocate(strip_tags($_POST['extra']['reason']), $businessid, intval($newBusiness->businessid), date('Y-m-d', $scheduleTimestamp));
					} else {
						$checkBusiness->scheduled_date = date('Y-m-d', $scheduleTimestamp);
						$checkBusiness->reason = strip_tags($_POST['extra']['reason']);
						$checkBusiness->closed_status = 'relocate';
						if ( $checkBusiness->new_business_id < 1 ) {
							$newBusiness = \EE\Model\Business::setupKiosk($setCompanyId);
							$json['businessId'] = $newBusiness->businessid;
							$checkBusiness->new_business_id = $newBusiness->businessid;
						} else {
							$json['businessId'] = $checkBusiness->new_business_id;
						}
						$checkBusiness->save();
					}
					break;
			}
		}

		if ($didChange) {
			if ($scheduleDate === '') {
				$scheduleDate = false;
			}
			$logLocation = \EE\Model\Log\Location::page('/ta/kioskutilities/offlineChange');
			$logType = \EE\Model\Log\Type::getNamespaced('form', 'treat.kioskutilities.offlinechange');
			$logAction = new \EE\Model\Log\Action;
			$logAction->at = date('Y-m-d H:i:s');
			$logAction->log_type_id = $logType->id;
			$logAction->log_location_id = $logLocation->id;
			$logAction->business_id = $businessid;
			$logAction->user_id = $user->getId();
			$logAction->changes = json_encode(array(
				'active' => $active,
				'edited_by' => $user->username,
				'scheduled' => $scheduleDate,
				'scheduledChangesId' => $scheduleChangeIds,
			));
			$logAction->save();

			try {
				$email = new \EE\Mail\KioskActivationChange;
				$email->changedByName($user->firstname." ".$user->lastname);
				if ($scheduleTimestamp >= $tomorrow) {
					$email->scheduled($scheduleTimestamp);
				}
				$email->setAdditionalEmailsByBusinessId($businessid);
				$email->send();
			} catch(\Exception $e) {
//				$json['didChange'] = $didChange;
//				$json['error'] = $e->getMessage();
//				header("Content-type: application/json");
//				echo json_encode($json);
//				return false;
			}
		}

		header("Content-type: application/json");
		echo json_encode($json);

		return false;
	}

	public function offlineManagement() {
		$user = $_SESSION->getUser();
		$companyId = $user->getCompanyId();

		if ( $user->kiosk_kiosk_mgmt == 0 ) {
			header('Location: /ta/businesstrack.php');
			exit;
		}

		$this->_view->assign('uri',$this->_uri->getAction());

		$this->addJavascriptAtTop('/assets/js/jquery-1.5.1.min.js');
		$this->addJavascriptAtTop('/assets/js/jquery-ui-1.8.11.custom.min.js');
		$this->addCss('/assets/css/jq_themes/redmond/jqueryui-current.css');
		$this->addCss('/assets/css/jq_themes/redmond/jqueryui-custom.css');

		$kiosks = \EE\Model\Business::kioskManagementList($companyId);

		$offlineTotal = 0;
		$activeTotal = 0;
		$newKiosks = 0;
		$data = array();
		$districtTitles = array();
		foreach ($kiosks as $kiosk) {
			$kiosk['active'] = $kiosk["value"];
			$kiosk['active_message'] = "";
			$kiosk['active_color'] = "";
			if ( $kiosk['active'] == 1 ) {
				$kiosk['active_message'] = "Active since {$kiosk[edittime]} [{$kiosk[editby]}]";
				$kiosk['active_color'] = "green";
			} else {
				$kiosk['active_color'] = "red";
				if ( trim($kiosk['editby']) !== "" ) {
					$kiosk['active_message'] = "Inactive since {$kiosk[edittime]} [{$kiosk[editby]}]";
				}
				else {
					$kiosk['active_message'] = "Never Active, Ship Date: {$kiosk[shipDate]}, Install Date: {$kiosk[installDate]}";
				}
			}
			$data[$kiosk["districtid"]][$kiosk["businessid"]][] = $kiosk;
			$districtTitles[$kiosk["districtid"]] = $kiosk["districtname"];

			if ($kiosk['setup'] == 1) {
				$newKiosks++;
			}

			if ($kiosk['online'] >= '00:15:00' && $kiosk['active'] == 1) {
				$offlineTotal++;
			}

			if ( $kiosk['active'] == 1 ) {
				$activeTotal++;
			}
		}

		$this->_view->assign('companyId', $companyId);
		$this->_view->assign('districts', $data);
		$this->_view->assign('districtTitles', $districtTitles);
		$this->_view->assign('activeTotal', $activeTotal);
		$this->_view->assign('newKiosks', $newKiosks);
		$this->_view->assign('offlineTotal', $offlineTotal);
		if (ob_get_level() > 1) {
			ob_flush();
		}
	}

	public function offlineReport() {
		$user = $_SESSION->getUser();
		$companyId = $user->getCompanyId();
		$cpid = static::getPostOrGetVariable( 'cpid' );
		
		if ( $user->kiosk_kiosk_mgmt == 0 ) {
			header('Location: /ta/businesstrack.php');
			exit;
		}
		
		$this->_view->assign('uri',$this->_uri->getAction());
		
		$this->addJavascriptAtTop('/assets/js/jquery-1.5.1.min.js');
		$this->addJavascriptAtTop('/assets/js/jquery-ui-1.8.11.custom.min.js');
		$this->addCss('/assets/css/jq_themes/redmond/jqueryui-current.css');
		$this->addCss('/assets/css/jq_themes/redmond/jqueryui-custom.css');
		
		$kView = "<thead><td><b>Terminal</b></td><td><b>Status</b></td><td><b>Date/Time</b></td><td><b>Down Time</b></td></thead>";
		
		$query = "
			SELECT *
			FROM mburris_manage.offline_logging
			WHERE client_programid = $cpid
			ORDER BY datetime_value DESC
			LIMIT 50
		";
		$result = Treat_DB_ProxyOld::query( $query );
		
		while ( $r = mysql_fetch_array($result) ) {
			$name = $r["name"];
			$new_status = $r["new_status"];
			$datetime_value = $r["datetime_value"];
			
			if ( $new_status == "OFFLINE" ) {
				$query3 = "SELECT TIMEDIFF('$lasttime', '$datetime_value') AS amount";
				$result3 = Treat_DB_ProxyOld::query($query3);
				
				$amount = mysql_result($result3, 0, "amount");
				
				//turn into minutes
				$pieces = explode(':', $amount);
				$pieces[0] *= 60;
				$pieces[1] += $pieces[0];
				$hours = floor($pieces[1] / 60);
				$minutes = fmod($pieces[1], 60);
				
				if ( $hours > 0 ) {
					$display_time = "$hours hrs";
					if ( $minutes > 0 ) {
						$display_time .= ", $minutes mins";
					}
				}
				else {
					$display_time = "$minutes mins";
				}
			}
			else {
				$display_time = "";
			}
			
			$kView .= "<tr><td>$name</td><td>$new_status</td><td>$datetime_value</td><td>$display_time</td></tr>";
			
			$lasttime = $datetime_value;
		}
		
		$this->_view->assign( 'companyId', $companyId );
		$this->_view->assign('kView',$kView);
	}
	
	public function routeManagement() {
		$user = $_SESSION->getUser();
		# if user is not authorized to see page, redirect
		if ( !$user || !$user->kiosk_route_mgmt ) {
			header( 'Location: /ta/businesstrack.php' );
			return false;
		}
		$DOC_ROOT = null; # Notice: Undefined variable
		$supervisor = null; # Notice: Undefined variable
		$sourceid = null; # Notice: Undefined variable
		$truck = null; # Notice: Undefined variable
		$seclevel_select = null; # Notice: Undefined variable
		$showactive = null; # Notice: Undefined variable
		$sec1 = null; # Notice: Undefined variable
		$sec2 = null; # Notice: Undefined variable
		$sec3 = null; # Notice: Undefined variable
		
		$companyid = \EE\Controller\Base::getPostGetSessionOrCookieVariable( array( 'cid' ) );
		if ( !$companyid ) {
			$companyid = $user->getCompanyId();
		}
		$company = \EE\Model\Company\Singleton::getSingleton( $companyid );
		
		$username = $_SESSION['usercook'];
		$password = $_SESSION['passcook'];
		
		$compcook = Treat_Controller_Abstract::getSessionCookieVariable( 'compcook' );
		$edituser = Treat_Controller_Abstract::getGetVariable( 'edituser', '' );
		$editsecurity = Treat_Controller_Abstract::getGetVariable( 'editsecurity', '' );
		$editlocation = Treat_Controller_Abstract::getGetVariable( 'editlocation', '' );
		$editcommission = Treat_Controller_Abstract::getGetVariable( 'editcommission', '' );
		
		# get login/security info for user
#		$query = "SELECT * FROM login WHERE username = '$username' AND password = '$password'";
#		$result = Treat_DB_ProxyOld::query( $query );
		
#		$mysecurity = @mysql_result( $result, 0, 'security' );
		$mysecurity = $user->security;
		
		
#		define( 'DOC_ROOT', realpath(dirname(__FILE__).'/../'));
		$this->_view->assign( 'docroot', $DOC_ROOT . '/ta/' );
		
		$this->_view->assign('uri',$this->_uri->getAction());
		$this->_view->assign('redirect','/ta/kioskutilities/routeManagement?cid='.$companyid);
		
		$this->addJavascriptAtTop('/assets/js/jquery-1.5.1.min.js');
		$this->addJavascriptAtTop('/assets/js/jquery-ui-1.8.11.custom.min.js');
		$this->addCss('/assets/css/jq_themes/redmond/jqueryui-current.css');
		$this->addCss('/assets/css/jq_themes/redmond/jqueryui-custom.css');
		
		$companyname = $company->companyname;
		
		$query = "SELECT * FROM security WHERE securityid = '$mysecurity'";
		$result = Treat_DB_ProxyOld::query( $query );
		
		$sec_routes = @mysql_result( $result, 0, 'routes' );
		$sec_securities = @mysql_result( $result, 0, 'securities' );
		
		$query = "SELECT * FROM login_route WHERE companyid = '$companyid' AND is_deleted = '0' ORDER BY businessid,locationid,route DESC";
		$result = Treat_DB_ProxyOld::query( $query );
		$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );
		
		$user_select = '<option value="routeManagement?cid='.$companyid.'">============Please Choose============</option>';
		
		$lastlocationid = -1;
		$lastuserbus = -1;
		$num--;
		while ( $num >= 0 ) {
			$login_routeid = @mysql_result( $result, $num, 'login_routeid' );
			$lastname = @mysql_result( $result, $num, 'lastname' );
			$firstname = @mysql_result( $result, $num, 'firstname' );
			$id1 = @mysql_result( $result, $num, 'username' );
			$id2 = @mysql_result( $result, $num, 'password' );
			$route = @mysql_result( $result, $num, 'route' );
			$userbus = @mysql_result( $result, $num, 'businessid' );
			$locationid = @mysql_result( $result, $num, 'locationid' );
			$isactive = @mysql_result( $result, $num, 'active' );
			$order_by_machine = @mysql_result( $result, $num, 'order_by_machine' );

			if ( $login_routeid == $edituser ) {
				$showsel = 'SELECTED';
			}
			else {
				$showsel = '';
			}


			if ( $isactive == 1 ) {
				$showinactive = "(Inactive)";
			}
			else {
				$showinactive = '';
			}

			if ( $lastuserbus != $userbus ) {
				$query5 = "SELECT businessname FROM business WHERE businessid = '$userbus'";
				$result5 = Treat_DB_ProxyOld::query( $query5 );
				$busname = @mysql_result( $result5, 0, 'businessname' );
				
				$user_select .= '<option value="routeManagement?cid='.$companyid.'">~~~~~~~~~~~~~~~~~~~~'.$busname.'</option>';
			}
			if ( $lastlocationid != $locationid ) {
				$query5 = "SELECT location_name FROM vend_locations WHERE locationid = '$locationid'";
				$result5 = Treat_DB_ProxyOld::query( $query5 );
				$locname = @mysql_result( $result5, 0, 'location_name' );
				
				$user_select .= '<option value="routeManagement?cid='.$companyid.'">===================='.$locname.'</option>';
			}
			
			$user_select .= '<option value="routeManagement?edituser='.$login_routeid.'&amp;cid='.$companyid.'" '.$showsel.'>Route# '.$route.', '.$firstname.' '.$lastname.' '.$showinactive.'</option>';

			$lastuserbus = $userbus;
			$lastlocationid = $locationid;
			$num--;
		}
		
		if ( $edituser != '' ) {
			$query = "SELECT * FROM login_route WHERE login_routeid = '$edituser'";
			$result = Treat_DB_ProxyOld::query( $query );

			$login_routeid = @mysql_result( $result, 0, 'login_routeid' );
			$lastname = @mysql_result( $result, 0, 'lastname' );
			$firstname = @mysql_result( $result, 0, 'firstname' );
			$id1 = @mysql_result( $result, 0, 'username' );
			$id2 = @mysql_result( $result, 0, 'password' );
			$route = @mysql_result( $result, 0, 'route' );
			$truck = @mysql_result( $result, 0, 'truck' );
			$userbus = @mysql_result( $result, 0, 'businessid' );
			$locationid = @mysql_result( $result, 0, 'locationid' );
			$supervisor = @mysql_result( $result, 0, 'supervisor' );
			$sec_level = @mysql_result( $result, 0, 'sec_level' );
			$active = @mysql_result( $result, 0, 'active' );
			$commissionid = @mysql_result( $result, 0, 'commissionid' );
			$sourceid = @mysql_result( $result, 0, 'sourceid' );
			$order_by_machine = @mysql_result( $result, 0, 'order_by_machine' );

			if ( $active == 1 ) {
				$showactive = 'CHECKED';
			}
			else {
				$showactive = '';
			}
			
			$newlocid_select = '';

			//$query = "SELECT * FROM vend_locations ORDER BY businessid, location_name DESC";
			$query = "SELECT * FROM vend_locations WHERE companyid = $companyid ORDER BY businessid, location_name DESC";
			$result = Treat_DB_ProxyOld::query( $query );
			$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );

			$num--;
			while ( $num >= 0 ) {
				$newlocid = @mysql_result( $result, $num, 'locationid' );
				$newlocname = @mysql_result( $result, $num, 'location_name' );
				$busid = @mysql_result( $result, $num, 'businessid' );
				if ( $newlocid == $locationid ) {
					$showsel2 = 'SELECTED';
				}
				else {
					$showsel2 = '';
				}

				$query4 = "SELECT businessname FROM business WHERE businessid = '$busid'";
				$result4 = Treat_DB_ProxyOld::query( $query4 );

				$busname = @mysql_result( $result4, 0, 'businessname' );
				
				$newlocid_select .= '<option value="'.$newlocid.'" '.$showsel2.'>'.$busname.' - '.$newlocname.'</option>';

				$num--;
			}

			//SUPERVISOR
			$query = "SELECT vend_supervisor.supervisorid,vend_supervisor.supervisor_name FROM vend_supervisor,business WHERE vend_supervisor.businessid = business.businessid AND business.companyid = '$companyid' ORDER BY supervisor_name";
			$result = Treat_DB_ProxyOld::query( $query );
			
			$supervisor_select = '<option value="0"></option>';
			
			while ( $r = mysql_fetch_array( $result ) ) {
				$supervisorid = $r['supervisorid'];
				$supervisor_name = $r['supervisor_name'];
				if ( $supervisor == $supervisorid ) {
					$showsel2 = 'SELECTED';
				}
				else {
					$showsel2 = '';
				}
				
				$supervisor_select .= '<option value="'.$supervisorid.'" '.$showsel2.'>'.$supervisor_name.'</option>';
				$num--;
			}
			
			if ( $sec_level == 1 ) {
				$sec1 = "SELECTED";
			}
			elseif ( $sec_level == 2 ) {
				$sec2 = "SELECTED";
			}
			elseif ( $sec_level == 3 ) {
				$sec3 = "SELECTED";
			}
			
			$seclevel_select = '<option value="1" '.$sec1.'>1 - Driver</option>';
			$seclevel_select .= '<option value="2" '.$sec2.'>2 - Route Mgr</option>';
			$seclevel_select .= '<option value="3" '.$sec3.'>3 - Admin</option>';
			
			$newcommissionid_select = '<option value="0"></option>';
			
			$query = "SELECT business.companyid FROM vend_locations,business WHERE vend_locations.locationid = '$locationid' AND vend_locations.unitid = business.businessid";
			$result = Treat_DB_ProxyOld::query( $query );

			$mycomp = @mysql_result( $result, 0, 'business.companyid' );

			$query = "SELECT * FROM vend_commission WHERE companyid = '$mycomp'";
			$result = Treat_DB_ProxyOld::query( $query );
			$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );

			$num--;
			while ( $num >= 0 ) {
				$newcommissionid = @mysql_result( $result, $num, 'commissionid' );
				$commission_name = @mysql_result( $result, $num, 'name' );
				if ( $commissionid == $newcommissionid ) {
					$showsel2 = 'SELECTED';
				}
				else {
					$showsel2 = '';
				}
				
				$newcommissionid_select .= '<option value="'.$newcommissionid.'" '.$showsel2.'>'.$commission_name.'</option>';
				$num--;
			}
			
			$newsourceid_select = '<option value="0"></option>';
			
			//////commission source
			$query = "SELECT * FROM vend_commission_source ORDER BY source_name DESC";
			$result = Treat_DB_ProxyOld::query( $query );
			$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );

			$num--;
			while ( $num >= 0 ) {
				$newsourceid = @mysql_result( $result, $num, 'sourceid' );
				$source_name = @mysql_result( $result, $num, 'source_name' );
				if ( $sourceid == $newsourceid ) {
					$showsel2 = 'SELECTED';
				}
				else {
					$showsel2 = '';
				}
				
				$newsourceid_select .= '<option value="'.$newsourceid.'" '.$showsel2.'>'.$source_name.'</option>';
				$num--;
			}
			
			//////////////ORDER BY ROUTE/MACHINE
			if ( $order_by_machine == 1 ) {
				$showsel2 = 'SELECTED';
			}
			else {
				$showsel2 = '';
			}
			
			$orderbymachine_select = '<option value="0">By Route</option>';
			$orderbymachine_select .= '<option value="1" '.$showsel2.'>By Machine</option>';
			
			
			$this->_view->assign('user_select',$user_select);
			$this->_view->assign('username',$username);
			$this->_view->assign('password',$password);
			$this->_view->assign('route',$route);
			$this->_view->assign('truck',$truck);
			$this->_view->assign('lastname',$lastname);
			$this->_view->assign('firstname',$firstname);
			$this->_view->assign('id1',$id1);
			$this->_view->assign('id2',$id2);
			$this->_view->assign('newlocid_select',$newlocid_select);
			$this->_view->assign('supervisor_select',$supervisor_select);
			$this->_view->assign('seclevel_select',$seclevel_select);
			$this->_view->assign('newcommissionid_select',$newcommissionid_select);
			$this->_view->assign('newsourceid_select',$newsourceid_select);
			$this->_view->assign('orderbymachine_select',$orderbymachine_select);
			$this->_view->assign('showactive',$showactive);
			$this->_view->assign('login_routeid',$login_routeid);
			$this->_view->assign('edituser',$edituser);
		} else {
			//$query = "SELECT * FROM vend_locations ORDER BY businessid, location_name DESC";
			$query = "SELECT * FROM vend_locations WHERE companyid = $companyid ORDER BY businessid, location_name DESC";
			$result = Treat_DB_ProxyOld::query( $query );
			$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );

			$num--;
			
			$newlocid_select = '';
			
			while ( $num >= 0 ) {
				$newlocid = @mysql_result( $result, $num, 'locationid' );
				$newlocname = @mysql_result( $result, $num, 'location_name' );
				$busid = @mysql_result( $result, $num, 'businessid' );

				$query4 = "SELECT businessname FROM business WHERE businessid = '$busid'";
				$result4 = Treat_DB_ProxyOld::query( $query4 );

				$busname = @mysql_result( $result4, 0, 'businessname' );
				
				$newlocid_select .= '<option value="'.$newlocid.'">'.$busname.' - '.$newlocname.'</option>';
				$num--;
			}
			
			//SUPERVISOR
			$query = "SELECT vend_supervisor.supervisorid,vend_supervisor.supervisor_name FROM vend_supervisor,business WHERE vend_supervisor.businessid = business.businessid AND business.companyid = '$companyid' ORDER BY supervisor_name";
			$result = Treat_DB_ProxyOld::query( $query );
			
			$supervisor_select = '<option value="0"></option>';
			
			while ( $r = mysql_fetch_array( $result ) ) {
				$supervisorid = $r["supervisorid"];
				$supervisor_name = $r["supervisor_name"];
				if ( $supervisor == $supervisorid ) {
					$showsel2 = 'SELECTED';
				}
				else {
					$showsel2 = '';
				}
				
				$supervisor_select .= '<option value="'.$supervisorid.'" '.$showsel2.'>'.$supervisor_name.'</option>';
				
				$num--;
			}
			
			$newcommissionid_select = '';
			
			$query = "SELECT business.companyid FROM vend_locations,business WHERE vend_locations.locationid = '$locationid' AND vend_locations.unitid = business.businessid";
			$result = Treat_DB_ProxyOld::query( $query );

			$mycomp = @mysql_result( $result, 0, 'business.companyid' );

			$query = "SELECT * FROM vend_commission WHERE companyid = '$mycomp'";
			$result = Treat_DB_ProxyOld::query( $query );
			$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );

			$num--;
			while ( $num >= 0 ) {
				$newcommissionid = @mysql_result( $result, $num, 'commissionid' );
				$commission_name = @mysql_result( $result, $num, 'name' );
				
				$newcommissionid_select .= '<option value="'.$newcommissionid.'" '.$showsel2.'>'.$commission_name.'</option>';
				
				$num--;
			}
			
			$newsourceid_select = '';
			
			$query = "SELECT * FROM vend_commission_source ORDER BY source_name DESC";
			$result = Treat_DB_ProxyOld::query( $query );
			$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );

			$num--;
			while ( $num >= 0 ) {
				$newsourceid = @mysql_result( $result, $num, 'sourceid' );
				$source_name = @mysql_result( $result, $num, 'source_name' );
				if ( $sourceid == $newsourceid ) {
					$showsel2 = 'SELECTED';
				}
				else {
					$showsel2 = '';
				}
				
				$newsourceid_select .= '<option value="'.$newsourceid.'" '.$showsel2.'>'.$source_name.'</option>';
				
				$num--;
			}
			
			$orderbymachine_select = '<option value="0">By Route</option>';
			$orderbymachine_select .= '<option value="1" '.$showsel2.'>By Machine</option>';
			
			$this->_view->assign('user_select',$user_select);
			$this->_view->assign('username',$username);
			$this->_view->assign('password',$password);
			$this->_view->assign('route',$route);
			$this->_view->assign('truck',$truck);
			$this->_view->assign('lastname',$lastname);
			$this->_view->assign('firstname',$firstname);
			$this->_view->assign('id1',$id1);
			$this->_view->assign('id2',$id2);
			$this->_view->assign('newlocid_select',$newlocid_select);
			$this->_view->assign('supervisor_select',$supervisor_select);
			$this->_view->assign('seclevel_select',$seclevel_select);
			$this->_view->assign('newcommissionid_select',$newcommissionid_select);
			$this->_view->assign('newsourceid_select',$newsourceid_select);
			$this->_view->assign('orderbymachine_select',$orderbymachine_select);
			$this->_view->assign('showactive',$showactive);
		}
		$this->_view->assign( 'title', 'Route Management' );
	}

	public function syncMenu() {
		$user = $_SESSION->getUser();
		$companyid = (int) $user->getCompanyId();

		if ( ! $user )
		{
			header('content-type: application/json');
			echo json_encode(array('error' => true, 'message' => 'User not available. Sign in needed.'));
			return false;
		}

		if ( ! is_array($_POST) )
		{
			header('content-type: application/json');
			echo json_encode(array('error' => true, 'message' => 'POST content is required.'));
			return false;
		}

		$businessIds = array();
		$menuItems = array();
		$filename = '';

		$debug = array();

		$error = array(
			'error' => false,
			'message' => '',
		);

		if ( isset($_POST['limit']) )
		{
			try {
				list($businessIds, $filename) = \EE\Model\Business::getBusinessIdsByFilter(array(
						\EE\Model\Business::COMPANY_FILTER => $companyid,
						\EE\Model\Business::DISTRICT_FILTER => $_POST['district'],
						\EE\Model\Business::KIOSK_FILTER => $_POST['kiosk']
					),
					$_POST['limit']
				);
				if ( count($businessIds) < 1 )
				{
					$error['error'] = true;
					$error['message'] = 'Company does not have any businesses for exporting.';
				}
			} catch(\PDOException $e) {
				$error['error'] = true;
				$error['message'] = $e->getMessage();
			}

			if ( ! isset($_POST['check']))
			{
				try {
					$menuItems = \EE\Model\Menu\ItemNew::uniqueUPC(
						\EE\Model\Menu\ItemNew::distinctItems(
							$businessIds,
							array(
								'upc',
								'name',
							),
							array(
								'cost',
								'price',
							)
						),
						array('upc', 'name')
					);
					if ( ! $menuItems || count($menuItems) < 1 )
					{
						$error['error'] = true;
						$error['message'] = 'Could not find any menu items.';
					}
				} catch(\PDOException $e) {
					$error['error'] = true;
					$error['message'] = $e->getMessage();
				}
			}

			if ( $error['error'] === true )
			{
				if ( isset($_POST['export']) && !isset($_POST['output']) || $_POST['output'] == 'file' )
				{
					echo '<script language="javascript" type="text/javascript">' . "\n";
					echo 'if ( window.parent && window.parent.importUploadError ) {' . "\n";
					echo 'window.parent.importUploadError("' . $error['message'] . '");' . "\n";
					echo '} else if ( parent && parent.importUploadError ) {' . "\n";
					echo 'parent.importUploadError("' . $error['message'] . '");' . "\n";
					echo '} else { alert("' . $error['message'] . '"); }' . "\n";
					echo '</script>';
					return false;
				}
				else
				{
					header('content-type: application/json');
					echo json_encode($error);
					return false;
				}
			}
		}

		$skippedItems = 0;
		$filterMenuItems = function($item) use ($menuItems, &$skippedItems) {
			$index = $item->upc;
			if ('' === $index)
			{
				$index = $item->name;
			}
			if ( ! isset($menuItems[$index]) )
			{
				$skippedItems++;
				return false;
			}
			if (
				doubleval($item->cost) === doubleval($menuItems[$index]->cost)
				&& doubleval($item->price) === doubleval($menuItems[$index]->price)
			)
			{
				$skippedItems++;
				return false;
			}
			return true;
		};

		if ( isset($_POST['export']) )
		{
			if( !isset($_POST['output']) || $_POST['output'] == 'file' )
			{
				$fileContents = array(
					'Name,UPC,Cost,Price'
				);
				foreach ( $menuItems as $item )
				{
					$fileContents[] = implode(',', array(
						'"' . str_replace('"', '\"', $item->name) . '"',
						'"' . str_replace('"', '\"', $item->upc) . '"',
						(double) $item->cost,
						(double) $item->price,
					));
				}

				$csv = implode("\n", $fileContents);
				header('Content-Description: Menu Items CSV');
				header('Content-Disposition: attachment; filename=menuitems_' . $filename . '.csv');
				header('Content-Type: text/csv; charset=utf-8');
				header('Content-Length: ' . strlen($csv));
				echo $csv;
			}
			else if (isset($_POST['callback']) && (strcasecmp($_POST['output'], 'json') === 0 || strcasecmp($_POST['output'], 'jsonp') === 0))
			{
				$fileContents = array();
				foreach ( $menuItems as $item )
				{
					$item->distinct__count = intval($item->distinct__count);
					$item->cost = doubleval($item->cost);
					$item->price = doubleval($item->price);

					$fileContents[] = $item;
				}
				header('content-type: application/javascript');
				echo $_POST['callback'] . '(' . json_encode($fileContents) . ');';
				return false;
			}
			else if (strcasecmp($_POST['output'], 'json') === 0)
			{
				$fileContents = array();
				foreach ( $menuItems as $item )
				{
					$item->distinct__count = intval($item->distinct__count);
					$item->cost = doubleval($item->cost);
					$item->price = doubleval($item->price);

					$fileContents[] = $item;
				}
				header('content-type: application/json');
				echo json_encode($fileContents);
				return false;
			}
		}
		else if (
			isset($_POST['import']) && is_array($_FILES) &&
			isset($_FILES['import_menu_items']) && is_readable($_FILES['import_menu_items']['tmp_name'])
		)
		{
			$upload = fopen($_FILES['import_menu_items']['tmp_name'], 'r');
			if ( ! $upload )
			{
				echo '<script language="javascript" type="text/javascript">';
				echo 'window.parent.importUploadError("Could not read uploaded file.");';
				echo '</script>';
				return false;
			}

			$readFile = false;
			$csv = array();
			while ( ($contents = fgetcsv($upload)) )
			{
				$readFile = true;
				if ( $contents && is_array($contents) )
				{
					if (
						strtolower($contents[0]) === 'name'
						&& strtolower($contents[1]) === 'upc'
						&& strtolower($contents[2]) === 'cost'
						&& strtolower($contents[3]) === 'price'
					)
					{
						continue;
					}

					$index = $contents[1];
					if ('' === trim($index))
					{
						$index = $contents[0];
					}
					if ( '' === trim($index) )
					{
						continue;
					}
					$csv[ $index ] = (object) array_combine(
						array('name', 'upc', 'cost', 'price'),
						array($contents[0], $contents[1], doubleval(str_replace('$', '', $contents[2])), doubleval(str_replace('$', '', $contents[3])))
					);
				}
			}

			fclose($upload);

			if ( is_writable($_FILES['import_menu_items']['tmp_name']) )
			{
				unlink($_FILES['import_menu_items']['tmp_name']);
			}

			if ( ! $readFile )
			{
				echo '<script language="javascript" type="text/javascript">';
				echo 'window.parent.importUploadError("File does not appear to be a csv.");';
				echo '</script>';
				return false;
			}

			$changes = array_values(
				array_filter($csv, $filterMenuItems)
			);

			echo '<script language="javascript" type="text/javascript">' . "\n";
			echo 'if ( window.parent && window.parent.importUpload ) {' . "\n";
			echo 'window.parent.importUpload(' . json_encode($changes) . ', ' . $skippedItems . ');' . "\n";
			echo '} else if ( parent && parent.importUpload ) {' . "\n";
			echo 'parent.importUpload(' . json_encode($changes) . ', ' . $skippedItems  . ');' . "\n";
			echo '} else { alert("Parent iframe access not supported."); }' . "\n";
			echo '</script>';
			return false;
		}
		else if ( isset($_POST['convertToCSV']) && isset($_POST['rawCSV']) )
		{
			if ( '' === trim($_POST['rawCSV']) )
			{
				header('Content-Type: application/json');
				echo json_encode(array('error' => true, 'message' => 'Must submit CSV for conversion.'));
				return false;
			}

			$csv = array();
			$fileContents = explode( "\n", str_replace(array("\r\n", "\r"), "\n", $_POST['rawCSV']) );
			foreach ($fileContents as $line)
			{
				$contents = str_getcsv($line);
				if ( $contents && is_array($contents) )
				{
					if (
						strtolower($contents[0]) === 'name'
						&& strtolower($contents[1]) === 'upc'
						&& strtolower($contents[2]) === 'cost'
						&& strtolower($contents[3]) === 'price'
					)
					{
						continue;
					}

					$index = $contents[1];
					if ('' === trim($index))
					{
						$index = $contents[0];
					}
					if ( '' === trim($index) )
					{
						continue;
					}
					$csv[ $index ] = (object) array_combine(
						array('name', 'upc', 'cost', 'price'),
						array($contents[0], $contents[1], doubleval(str_replace('$', '', $contents[2])), doubleval(str_replace('$', '', $contents[3])))
					);
				}
			}

			$changes = array_values(
				array_filter($csv, $filterMenuItems)
			);

			header('Content-Type: application/json');
			echo json_encode(array(
				'skipped' => $skippedItems,
				'changes' => $changes
			));
			return false;
		}
		else if ( isset($_POST['changes']) )
		{
			$upc = $_POST['upc'];
			$name = $_POST['name'];
			if ( ('' === trim($upc) && '' === trim($name)) || (! is_array($businessIds) || count($businessIds) < 1) )
			{
				header('Content-Type: application/json');
				echo json_encode( array(
					'error' => true,
					'message' => 'Nothing to update.',
					'status' => array(),
				));
				return false;
			}

			if ( ! is_array($businessIds) || count($businessIds) < 1 )
			{
				header('Content-Type: application/json');
				echo json_encode( array(
					'error' => true,
					'message' => 'Nothing to update.',
					'status' => array(),
				));
				return false;
			}

			$where = array('`businessid` IN (' . implode(',', $businessIds) . ')');

			if ( '' !== trim($upc) )
			{
				$where[] = "`upc` = " . \EE\Model\Menu\ItemNew::db()->quote($upc);
			}
			else if ( '' !== trim($name) )
			{
				$where[] = "`name` = " . \EE\Model\Menu\ItemNew::db()->quote($name);
			}

			$cost = doubleval($_POST['cost']);
			$newPrice = doubleval($_POST['price']);
			$status = array();

			$menuItems = \EE\Model\Menu\ItemNew::get(implode(' AND ', $where));
			foreach ($menuItems as $item)
			{
				$price = \EE\Model\Menu\Item\Price::get('menu_item_id = '. intval($item->id), true);
				$info = array(
					'item' => array(
						'business_id' => $item->businessid,
						'menu_item_id' => $item->id,
						'name' => $item->name,
						'upc' => $item->upc,
						'cost' => $price->cost,
						'price' => $price->price,
					),
					'cost' => array(
						'updated' => false,
						'amount' => false,
					),
					'price' => array(
						'updated' => false,
						'amount' => false,
					),
					'saved' => false
				);
				if ( doubleval($price->cost) !== $cost )
				{
					$price->cost = $cost;
					$info['cost']['updated'] = true;
					$info['cost']['amount'] = $cost;
				}
				if ( doubleval($price->price) !== $newPrice )
				{
					$price->new_price = $newPrice;
					Treat_Model_KioskSync::addSync( $item->businessid, $item->id, Treat_Model_KioskSync::TYPE_ITEM );
					Treat_Model_KioskSync::addSync( $item->businessid, $item->id, Treat_Model_KioskSync::TYPE_ITEM_TAX );
					$info['price']['updated'] = true;
					$info['price']['amount'] = $newPrice;
				}
				$info['saved'] = $price->save();
				$status[] = $info;
			}

			header('Content-Type: application/json');
			echo json_encode( array(
				'debug' => $debug,
				'error' => false,
				'message' => '',
				'status' => $status,
			));
			return false;
		}

		return false;
	}
	
	public function exportMasterMenu() {
		$user = $_SESSION->getUser();
		$companyid = (int) $user->getCompanyId();
		
		$query = "SELECT min.upc, 
				min.item_code, 
				min.name, 
				ROUND( AVG( mip.price ) , 2 ) AS price, 
				ROUND( AVG( mip.cost ) , 2 ) AS cost
			FROM menu_items_new min
			JOIN menu_items_price mip ON mip.menu_item_id = min.id
			JOIN business b ON b.businessid = min.businessid
			AND b.companyid = $companyid
			GROUP BY min.UPC, min.item_code
			ORDER BY min.item_code
		";
		$result = Treat_DB_ProxyOld::query( $query );
		
		$data = "Item Code, UPC, Name, Cost, Price\r\n";
		
		while($r = mysql_fetch_array($result)){
			$upc = $r["upc"];
			$item_code = $r["item_code"];
			$name = $r["name"];
			$name = str_replace('"', '', $name);
			$price = $r["price"];
			$cost = $r["cost"];
			$data .= "\"$item_code\",\"$upc\",\"$name\",\"$cost\",\"$price\"\r\n";
		}
		
		$file="MasterMenu.csv";
		header("Content-type: application/x-msdownload");
		header("Content-Disposition: attachment; filename=$file");
		header("Pragma: no-cache");
		header("Expires: 100");
		echo $data;
	}

	public function picksheet() {
		$user = $_SESSION->getUser();
		$companyId = $user->getCompanyId();
		$district = static::getPostOrGetVariable( 'districtid' );
		$pickdate = static::getPostOrGetVariable( 'pickdate' );
		$ordertype = static::getPostOrGetVariable( 'ordertype' );
		
		if($pickdate == ""){$pickdate = date("Y-m-d");}
		
		if ( $user->kiosk_kiosk_mgmt == 0 ) {
			header('Location: /ta/businesstrack.php');
			exit;
		}
		
		$this->_view->assign('uri',$this->_uri->getAction());
		
		$this->addJavascriptAtTop('/assets/js/jquery-1.5.1.min.js');
		$this->addJavascriptAtTop('/assets/js/jquery-ui-1.8.11.custom.min.js');
		$this->addCss('/assets/css/jq_themes/redmond/jqueryui-current.css');
		$this->addCss('/assets/css/jq_themes/redmond/jqueryui-custom.css');
		
		$filter = "<select name=districtid><option value=0>Please Choose...</option>";
		
		$query = "SELECT districtid,districtname FROM district WHERE companyid = $companyId";
		$result = Treat_DB_ProxyOldProcessHost::query( $query );
		
		while($r = mysql_fetch_array($result)){
			$districtid = $r["districtid"];
			$districtname = $r["districtname"];
			
			if($districtid == $district){$sel = "SELECTED";}
			else{$sel = "";}
			
			$filter .= "<option value=$districtid $sel>$districtname</option>";
		}
		$filter .= "</select> <select name=ordertype>";
		
		$query = "SELECT * FROM kiosk_order_type ORDER BY id";
		$result = Treat_DB_ProxyOldProcessHost::query( $query );
		
		while($r = mysql_fetch_array($result)){
			$ordertypeid = $r["id"];
			$ordertypename = $r["ordertype"];
			
			if($ordertypeid == $ordertype){$sel = "SELECTED";}
			else{$sel = "";}
			
			$filter .= "<option value=$ordertypeid $sel>$ordertypename</option>";
		}
		$filter .= "</select>";
		
		$this->_view->assign('filter',$filter);
		$this->_view->assign('pickdate',$pickdate);
		$this->_view->assign('companyid',$companyId);
		
		if($district > 0 && $ordertype > 0 AND $pickdate != ""){
			$items = array();
			$qty = array();
			$label = array();
			
			$query = "select mo.id,
					mo.expected_delivery,
					mo.route,
					min.name,
					modl.item_code,
					modl.qty,
					modl.new_item_code,
					modl.new_qty,
					vm.name AS mach_name
				FROM business b
				JOIN machine_bus_link mbl ON mbl.businessid = b.businessid AND mbl.ordertype = $ordertype
				JOIN vend_machine vm ON vm.machine_num = mbl.machine_num
				JOIN machine_order mo ON mo.machineid = vm.machineid AND mo.expected_delivery = '$pickdate'
				JOIN machine_orderdetail modl ON modl.orderid = mo.id
				LEFT JOIN menu_items_new min ON min.id = modl.menu_items_new_id
				WHERE b.districtid = $district
				ORDER BY mo.route,mach_name";
			$result = Treat_DB_ProxyOldProcessHost::query( $query );
			
			while($r = mysql_fetch_array($result)){
				$item_code = $r["item_code"];
				$item_name = $r["name"];
				$item_qty = $r["qty"];
				$mach_name = $r["mach_name"];
				$route = $r["route"];
				
				$label[$route][$mach_name] = $route;
				$items[$item_code] = $item_name;
				$qty[$item_code][$route][$mach_name] = $item_qty;
			}
			
			ksort($items);
			
			///display
			$table = "<table cellspacing=0 cellpadding=0 width=100% style=\"border:1px solid black;background-color:white;font-size:12px;\">
						<thead>
							<tr bgcolor=#FFFF99 valign=top>
								<td style=\"border:1px solid black;\">Item Code</td>
								<td style=\"border:1px solid black;\">Name</td>";
			$download_data = "Item Code,Name,";
			
			foreach($label AS $route){
				foreach($route AS $mach_name => $value){
					$table .= "<td style=\"border:1px solid black;\"><b>$value</b><br>$mach_name</td>";
					$mach_name = str_replace('"', '', $mach_name);
					$mach_name = str_replace(',', '', $mach_name);
					$download_data .= "$value: $mach_name,";
				}
			}
			
			$table .= "<td style=\"border:1px solid black;\"><b>Totals</b></td></tr>";
			$download_data .= "Totals\r\n";
			
			////display items
			foreach($items AS $key => $value){
				$lineTotal = 0;
				$table .= "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor='' valign=top>
								<td style=\"border:1px solid black;\">$key</td>
								<td style=\"border:1px solid black;\">$value</td>";
				$value = str_replace('"', '', $value);
				$download_data .= "$key,$value,";
				
				foreach($label AS $route){
					foreach($route AS $mach_name => $value2){
						$amount = $qty[$key][$value2][$mach_name];
						$lineTotal += $amount;
						$download_data .= "$amount,";
						if($amount == 0){$amount = "&nbsp;";}
						$table .= "<td style=\"border:1px solid black;\" align=right>$amount&nbsp;</td>";
					}
				}
				$table .= "<td style=\"border:1px solid black;\" align=right><b>$lineTotal</b>&nbsp;</td></tr>";
				$download_data .= "$lineTotal\r\n";
			}
			
			$table .= "</table>";
			
			$this->_view->assign('displayItems',$table);
			$this->_view->assign('download_data',$download_data);
		}
	}
}

