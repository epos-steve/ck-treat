<?php

/* Scott 3-19-12 */
class Ta_PromomasterController extends Treat_Controller_CompanyAbstract
{
	protected $_noLoginRequired = array(
		'ta/report' => array(
			'pos_report' => true,
			'do-cron-report' => true,
			'do-instant-report' => true,
		),
	);	
	
	protected $_layout = 'promotions.tpl';
	
	public function index() {
		$this->_view->assign('uri',$this->_uri->getController());
		
		$this->addJavascriptAtTop('/assets/js/jquery-1.5.1.min.js');
		$this->addJavascriptAtTop('/assets/js/jquery-ui-1.8.11.custom.min.js');
		$this->addJavascriptAtTop('/assets/js/ta/promo_master.js');
		$this->addCss( '/assets/css/preload/preLoadingMessage.css');		
		$this->addCss('/assets/css/jq_themes/redmond/jqueryui-current.css');
		$this->addCss('/assets/css/jq_themes/redmond/jqueryui-custom.css');
		
		/* build promo group list */
		$query = "select id, name
					from promo_groups
					where businessid is null";
					
		$result = $this->_db->query($query);
		
		$promo_groups = '';
		
		while( $r = $result->fetch() ) {
			$promo_groups .= '<option value="'.$r['id'].'">'.$r['name'].'</option>';
		}
		
		/* build items by category lists */					
		/*$query = "select m.Name as 'item_name', c.Name as 'category', m.Id as 'item_id', m.Category as 'cat_id'
					from menu_items_master m
					inner join menu_items_category c on c.Id = m.Category
					order by c.Name, m.Name";*/
					
		$query = "select m.Name as 'item_name',
		case when c.Name = '' then '[Unassigned]' else c.Name end as 'category',
		m.Id as 'item_id', m.Category as 'cat_id'
					from menu_items_master m
					inner join menu_items_category c on c.Id = m.Category
					order by c.Name, m.Name";
					
		$result = $this->_db->query($query);

		$current_cat = '';
		$items = '<table width="99%" id="item_table" cellpadding="0" cellspacing="0" border="1"><tr>';
		$first_time = 1;		
		$col =  0;
		
		while( $r = $result->fetch() ) {
			if($current_cat != $r["category"]){
				$current_cat = $r["category"];
				$col++;
				
				if($col > 3){
					$col = 1;
					$items .= '</td></tr><tr>';
				} elseif($first_time != 1) {
					$items .= '</td>';
				} else {
					$first_time = 1;
				}
				
				$items .= '<td valign="top"><h3 id="catid_'.$r["cat_id"].'" class="cat">'.$r["category"].' + -<br />'
					.'<div><span>0</span> selected</div></h3><ul style="display:none;" class="category">'
					.'<li><a style="float:left;" class="select_all" href="javascript:">Select All</a>'
					.' <a style="float:left;" class="deselect_all" href="javascript:">Deselect All</a><br /><br /></li>';
			}			
			
			$items .= '<li><input type="checkbox" id="item_'.$r["item_id"].'" name="'.$r["item_id"].'" />'.$r["item_name"].'</li>';
		}
		
		switch($col){
			case 1:
				$items .= '</td><td>&nbsp;</td><td>&nbsp;</td>';
				break;
			case 2: $items .= '</td><td>&nbsp;</td>';
				break;
			case 3:
				$items .= '</td>';
				break;			
		}
		
		$this->_view->assign('items',$items);
		$this->_view->assign('promo_groups',$promo_groups);
	}
	
	public function ajaxCreatePromoGroup(){
		if($this->_isXHR){
			header('Content-type: application/json');

			/* missing required field(s) */
			if( empty($_POST['promo_group_name']) || empty($_POST['promo_group_id']) ){
				$return = array('passed' => 0, 'error' => 'req_missing');
				
				$return = json_encode($return);
				
				print $return;
				
				return false;
			}
	
			if($_POST['promo_group_id'] == -1){  //create a new promo group
				$uuid = $this->_genUuid( );
				
				/* create promo group */
				$query = 'INSERT INTO promo_groups
							(uuid, uuid_promo_groups, name,visible,routing)
							VALUES("'.$uuid.'", "'.$uuid.'", "'.$_POST['promo_group_name'].'",0,0)';
							
				$result = $this->_db->query($query);
				$insert_id = $this->_db->lastInsertId(); //last insert id of newly created promo group
			} else {  //update existing promo group
				$query = 'UPDATE promo_groups
							SET name = "'.$_POST['promo_group_name'].'"
							WHERE id = '.$_POST['promo_group_id'];
							
				$result = $this->_db->query($query);

				$insert_id = $_POST['promo_group_id'];  //id of updated promo group
			}
			
			/* delete all rows from promo_groups_detail for selected promo group */
			$query = 'DELETE FROM promo_groups_detail
						WHERE promo_group_id = '.$insert_id;

			$result = $this->_db->query($query);

			/* loop over post variables and insert rows in promo_groups_detail table */
			foreach($_POST as $key=>$value){
				if($key != 'promo_group_name' && $key != 'promo_group_visible' && $key != 'promo_group_routing' && $key != 'promo_group_order' && $key != 'promo_group_id'){
					/* insert row */
					/*$query = 'INSERT INTO promo_groups_detail
								(product_id,promo_group_id)
								VALUES('.$key.', '.$insert_id.')';*/

					//Scott 4-2-12
					$query = 'INSERT INTO promo_groups_detail
								(promo_group_id,master_product_id)
								VALUES('.$insert_id.', '.$key.')';

					$result = $this->_db->query($query);
				}
			}

			$return = array('passed' => 1);
			$return = json_encode($return);
			print $return;

			return false;
		} else {
			print 'not ajax';
		}
	}

	public function ajaxLoadPromoGroup(){
		if($this->_isXHR){
			header('Content-type: application/json');
			
			$query = "select g.name, d.master_product_id, i.category
						from promo_groups g
						inner join promo_groups_detail d on d.promo_group_id = g.id
						inner join menu_items_master i on i.id = d.master_product_id
						where g.id = ".$_POST['promo_group_id'];

			$result = $this->_db->query($query);
			
			$item_ids = array();
			$items_in_cat = array();
			
			while( $r = $result->fetch() ) {
				//$item_ids[] = $r['product_id'];
				$item_ids[] = $r['master_product_id'];
				
				if( isset($items_in_cat[ $r['category'] ]) ){
					$items_in_cat[ $r['category'] ]++;
				} else {
					$items_in_cat[ $r['category'] ] = 1;
				}
				
				if( !isset($group_name) ){
					$group_name = $r['name'];
				}
			}
			
			$return = array('group_name' => $group_name, 'group_id' => $_POST['promo_group_id'], 'item_ids' => $item_ids, 'items_in_cat' => $items_in_cat, 'pass' => 1);
			
			$return = json_encode($return);				
			print $return;
				
			return false;
		} else {
			print 'not ajax';
		}
	}
	
	public function ajaxAllPromoGroups(){
		if($this->_isXHR){
			/* build promo group list */
			$query = "select id, name
						from promo_groups
						where businessid is null";
						
			$result = $this->_db->query($query);
			
			$promo_groups = '';
			
			while( $r = $result->fetch() ) {
				$promo_groups .= '<option value="'.$r['id'].'">'.$r['name'].'</option>';
			}
			
			print $promo_groups;

			return false;
		} else {
			print 'not ajax';
		}
	}	

	public function ajaxDeletePromoGroup(){
		if($this->_isXHR){
			header('Content-type: application/json');
			
			/* missing required field(s) */
			if( empty($_POST['promo_group_name']) || empty($_POST['promo_group_id']) ){
				$return = array('passed' => 0, 'error' => 'req_missing');
				
				$return = json_encode($return);
				
				print $return;
				
				return false;
			}
			
			$query = "SELECT id
						FROM promo_groups
						WHERE id = ".$_POST['promo_group_id'];

			$result = $this->_db->query($query);
			$num_rows = $result->rowCount();
			
			if($num_rows == 1){  //only 1 result found
				$this->_db->beginTransaction();  //begin transaction
			
				$query = 'DELETE FROM promo_groups
							WHERE id = '.$_POST['promo_group_id'];
				
				$result = $this->_db->exec($query);
				
				if($result !== false){  //successfully deleted row				
					$query = 'DELETE FROM promo_groups_detail
								WHERE promo_group_id = '.$_POST['promo_group_id'];
					
					$result = $this->_db->query($query);
					
					$this->_db->commit();  //commit deletes to database
					
					$return = array('promo_group_id' => $_POST['promo_group_id'], 'promo_group_name' => $_POST['promo_group_name'], 'passed' => 1);
				} else {  //deletion failed, rollback
					$this->_db->rollBack();
					
					$return = array('passed' => 0);
				}
				
				$return = json_encode($return);
				
				print $return;
			} else {  //multiple results, error out
				$return = array('passed' => 0);
				
				$return = json_encode($return);
				
				print $return;
			}
			
			return false;
		} else {
			print 'not ajax';
		}
	}
}