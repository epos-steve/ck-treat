<?php
/**
 * Created by PhpStorm.
 * User: ryan
 * Date: 2/4/14
 * Time: 11:14 AM
 */

/**
 * Class Ta_FunctionpanelsController
 *
 * %securityLevel 7
 */
class Ta_FunctionpanelsController extends \EE\Controller\Modern {
	protected $_layout = 'promotions.tpl';

	public function init( ) {
		$uri = $this->_uri->getController();
		$this->_view->assign( 'uri', $uri );
		parent::init( );
	}

	public function index( ) {
		$this->addFontFace( 'dejavusans' );
		$this->addCss( '/assets/css/ta/businessmenus.css' );
		$this->addCss( '/assets/css/casManagement.css' );
		$this->addCss( '/assets/css/product-button-colors.css' );
		$this->addCss( '/assets/css/jquery.ui.tooltip.css' );
		$this->addCss( '/assets/css/ta/functionPanels.css' );

		$this->addPlugin( 'jquery.ui.aerispanel' );
		$this->addPlugin( 'jquery.ba-hashchange', false );
		$this->addPlugin( 'jquery.waffle' );
		$this->addPlugin( 'jquery.appear-1.1.1.min', false );
		$this->addPlugin( 'jquery.ba-resize', false );
		$this->addPlugin( 'jquery.ui.ajaxedit' );
		$this->addPlugin( 'jquery.ui.picker' );

		$this->addJavascriptAtTop( '/assets/js/jquery.ui.tooltip.js' );
		$this->addJavascriptAtTop( '/assets/js/ta/functionPanels.js' );
	}

	/**
	 * Generate an array of available function panels
	 *
	 * %ajax
	 * %param cid decimal id 'Invalid cid'
	 */
	public function getFunctionPanels( $cid ) {
		$functionPanels = \EE\Model\Aeris\FunctionPanel::getListByCompanyId( $cid );
		if( !is_array( $functionPanels ) ) {
			$functionPanels = array( );
		}
		return array( 'functionPanels' => $functionPanels );
	}

	/**
	 * Generate an array of buttons for a function panel
	 *
	 * %ajax
	 * %param fpid decimal id 'Invalid function panel id'
	 * %param functionPanelPage decimal newid 'Invalid function page id'
	 */
	public function getFunctionPanel( $fpid, $functionPanelPage = 0 ) {
		if( $functionPanelPage > 1 ) {
			$buttons = \EE\Model\Aeris\FunctionPanelButton::getListByFpanelId( $fpid, $functionPanelPage );
		} else {
			$buttons = \EE\Model\Aeris\FunctionPanelButton::getListByFpanelId( $fpid );
		}
		return array( 'functionPanelButtons' => $buttons );
	}

	/**
	 * @param array $inputs
	 *
	 * %ajax useArray
	 * %param companyid decimal id 'Invalid company id'
	 * %param id decimal newid 'Invalid function panel id'
	 * %param name string name 'Invalid name'
	 * %param rows decimal numeral 'Invalid rows'
	 * %param cols decimal numeral 'Invalid cols'
	 * %param pages decimal numeral 'Invalid pages'
	 */
	public function functionPanelEditItem( array &$inputs ) {
		\EE\Model\Aeris\FunctionPanel::updatePanel( $inputs );
	}

	/**
	 * @param $fpid
	 *
	 * @return array
	 *
	 * %ajax
	 * %param fpid decimal id 'Invalid function panel id'
	 */
	public function duplicateFunctionPanel( $fpid ) {
		$fp = \EE\Model\Aeris\FunctionPanel::getById( $fpid );
		$functions = $fp->getButtons( );
		$fp->id = null;
		$fp->name = $fp->selectDuplicateName( );
		$fp->save( );

		foreach( $functions as $function ) {
			$function->id = null;
			$function->fpanel_id = $fp->id;
			$function->save( );
		}

		return array( 'id' => $fp->id );
	}

	/**
	 * @param array $inputs
	 *
	 * %ajax useArray
	 * %param companyid decimal id 'Invalid company id'
	 * %param id decimal newid 'Invalid function panel id'
	 * %param pos_color decimal numeral 'Invalid color'
	 * %param page decimal numeral 'Invalid page'
	 * %param name string name 'Invalid name'
	 * %param args string
	 * %param fpanel_id decimal numeral 'Invalid function panel id'
	 * %param function_id decimal numeral 'Invalid function id'
	 */
	public function functionPanelEditButton( array &$inputs ) {
		$id = $inputs['id'];
		if( $id < 1 ) {
			$button = new \EE\Model\Aeris\FunctionPanelButton();
		} else {
			$button = \EE\Model\Aeris\FunctionPanelButton::getById( $id );
		}

		$button->updateValues( $inputs );
		$button->save( );
	}

	/**
	 * @return array
	 *
	 * %ajax
	 */
	public function getFunctionPanelFunctions( ) {
		return array( 'functionPanelFunctions' => \EE\Model\Aeris\FunctionPanelFunction::getFunctionList() );
	}

	/**
	 * @param $cid
	 * @param $functionPanelId
	 * @param $buttons
	 * @return array
	 *
	 * %ajax
	 * %param cid decimal id 'Invalid company id'
	 * %param functionPanelId decimal id 'Invalid function panel id'
	 * %param buttons array
	 */
	public function updateFunctionPanelOrder( $cid, $functionPanelId, $buttons ) {
		$error = array();
		$buttonOut = array();

		foreach( $buttons as $buttonUpdated ) {
			$button = \EE\Model\Aeris\FunctionPanelButton::getById( intval( $buttonUpdated['id'] ) );
			$buttonOut[] = $button->updateValues( $buttonUpdated );
			$button->save( );
		}

		return array( 'message' => $buttonOut );
	}

	private function deleteFunctionPanel( $fpid ) {
		$fp = \EE\Model\Aeris\FunctionPanel::getById( $fpid );
		$fp->delete( );

		return array( 'id' => $fpid, 'message' => 'Function panel deleted successfully' );
	}

	/**
	 * @param $id
	 *
	 * @return array
	 *
	 * %ajax
	 * %param id decimal id 'Invalid function panel button id'
	 */
	public function deleteFunctionPanelButton( $id ) {
		$fpb = \EE\Model\Aeris\FunctionPanelButton::getById( $id );
		$fpb->delete( );

		return array( 'id' => $id, 'message' => 'Function Panel button deleted successfully.' );
	}

	/**
	 * @param $fpid
	 *
	 * @return array
	 *
	 * %ajax
	 * %param fpid decimal id 'Invalid function panel id'
	 */
	public function toggleFunctionPanelActive( $fpid ) {
		$success = \EE\Model\Aeris\FunctionPanel::toggleActive( $fpid );
		return array( 'id' => $fpid, 'message' => $success ? 'Function Panel toggled' : 'Function Panel not found' );
	}

}
