<?php

 /**
 * this file execute query ans is call for comboOrder
 * @author: Juan Ramon Lepe
 * @version: 01/08/2015
 * @company: essential elements 
 */
 
class Ta_ComboOrderController 
{

	
	private $lstrIDElement;
	private $lstrArrayElements;
	private $lstrArrayElementsColor;
	private $lstrNumberElements;

	private $numberElements;

	 public function __construct(){

		$this->lstrIDElement='';
		$this->lstrArrayElements='';
		$this->lstrArrayElementsColor='';
		$this->lstrNumberElements='';

		$this->numberElements =0;
		
	 }

	 public function __set($name, $value)
    {


		// check that the property exist
		if (property_exists('Ta_ComboOrderController',$name))
		 {
			$this->$name = $value;
		 }
		 else
		 {
			$this->$name = "";
		 }
 
	}
	
	public function __get($name)
    {

		// check that the property exist
 
		if (property_exists('Ta_ComboOrderController', $name))
		{
			return $this->$name;
		}
 
		// Return nulo if doesn't exist 
		return NULL;
	}


	public function dragCombo() {	

		try {
 	
			// for drop	
			$query = " SELECT pgd.product_id , pgd.promo_group_id , mitn.id, mitn.name, pgd.icon_color "
			. " FROM promo_groups_detail pgd "
			. " LEFT JOIN menu_items_new mitn ON pgd.product_id = mitn.id"
			. " WHERE pgd.promo_group_id = ".$this->lstrIDElement	
			. " ORDER BY pgd.display_order  ";	


			$result = Treat_DB_ProxyOld::query( $query );
			


			$idnumberElements = mysql_num_rows($result);


			$lnuCount = 0;
			$obarrayElememts = array();
			while( $r = mysql_fetch_array( $result ) ) {
				$obarrayElememts[$lnuCount][0] = $r["id"];
				$obarrayElememts[$lnuCount][1] = $r["name"];
				$obarrayElememts[$lnuCount][2] = $r["icon_color"];
				
				$lnuCount++;
			}			
			$lnuCount = 0;
			$lnumRows = $idnumberElements / 5;
			
			if (is_float($lnumRows)) $lnumRows = intval($lnumRows)+1;




			for ($lnuforRow =0;$lnuforRow<$lnumRows;$lnuforRow++)
			{
			//echo $lnumRows;
				$CodeIncludeInControl .= '<tr style="background-color: rgb(232, 231, 231)">';
				
				for ($lnuforCol =0;$lnuforCol<5;$lnuforCol++)
				{
					if ($lnuCount ==  $idnumberElements) break;
					$CodeIncludeInControl .= '<td style="border: 1px solid white;text-align: center;padding: 2px;"><div id="'.$obarrayElememts[$lnuCount][0].'" style="border-radius: 5px; height: 40px; width: 98px;" class="drag t2 product-button-color-'.$obarrayElememts[$lnuCount][2].'" >'.$obarrayElememts[$lnuCount][1].'</div></td>';

					$lnuCount++;
				}
				if ($lnuforCol<5)
				{
					for ($lnuforCol2 =0;$lnuforCol2<(5-$lnuforCol);$lnuforCol2++)  {
					$CodeIncludeInControl .= '<td class="mark" style="border: 1px solid white;text-align: center;padding: 2px; width:102px"></td>';
					}
				}
				$CodeIncludeInControl .= '</tr>';
			}
			
								
			echo $CodeIncludeInControl; 
			
			
			// end for drop
		} catch (Exception $e) {
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}	

			
			
	}
	

	public function saveOrder(){
	
		
		for ($numOrder=0;$numOrder<$this->lstrNumberElements;$numOrder++)
		{
			$query=" UPDATE promo_groups_detail "
			." SET display_order =  ". $numOrder
			." , icon_color =  '". $this->lstrArrayElementsColor[$numOrder]. "'" 
			." WHERE product_id = ".$this->lstrArrayElements[$numOrder] . "" 
			." AND promo_group_id = ".$this->lstrIDElement." limit 1 ";
			
			$result = Treat_DB_ProxyOld::query( $query );
			
		}
	}	
	


}

?>
