<?php

mb_internal_encoding("UTF-8");
mb_http_output("UTF-8");
ob_start("mb_output_handler");

/* Scott 4-5-12 */
class Ta_MastermenuController
	extends \EE\Controller\CompanyAbstract
{
	protected $_noLoginRequired = array(
		'ta/report' => array(
			'pos_report' => true,
			'do-cron-report' => true,
			'do-instant-report' => true,
		),
	);
	
	protected $_layout = 'promotions.tpl';
	
	public function index()
	{
		$this->_view->assign('uri',$this->_uri->getController());
		
		$this->addJavascriptAtTop('/assets/js/jquery-1.5.1.min.js');
		$this->addJavascriptAtTop('/assets/js/jquery-ui-1.8.11.custom.min.js');
		$this->addJavascriptAtTop('/assets/js/ta/master_menu.js');
		$this->addCss( '/assets/css/preload/preLoadingMessage.css');
		$this->addCss('/assets/css/jq_themes/redmond/jqueryui-current.css');
		$this->addCss('/assets/css/jq_themes/redmond/jqueryui-custom.css');
		
		/* build manufacturer dropdown */
		$query = "SELECT * FROM manufacturer ORDER BY name";
		$result = $this->_db->query($query);

		$man_select = '<option value="">Select</option>';
		
		while( $r_man = $result->fetch() ) {
			$man_select .= '<option value="'.$r_man["id"].'">'.$r_man["name"].'</option>';
		}
		
		/* build category dropdown */
		$query = "SELECT * FROM menu_items_category ORDER BY name";
		$result = $this->_db->query($query);

		$cat_select = '<option value="">Select</option>';

		while( $r_cat = $result->fetch() ) {
			$cat_select .= '<option value="'.$r_cat["Id"].'">'.$r_cat["Name"].'</option>';
		}
		
		/* build menu item dropdown */					
		$query = "select A.*, coalesce(B.mim_id,0) as 'mim'
					from
						(SELECT m.upc as 'upc', m.name as 'item_name', m.Manufacturer as 'man_id', m.Category as 'cat_id', m.Id as 'master_id'
						FROM menu_items_master m
						LEFT JOIN menu_items_category c ON c.id = m.category
						LEFT JOIN manufacturer a ON a.id = m.manufacturer
						WHERE m.is_deleted = 0
						order by m.name) as A
					left outer join
						(select distinct mim_id
						from nutrition_xref_master) as B
					on A.master_id = B.mim_id";

		$result = $this->_db->query($query);
		
		$item_select = '';
		
		$total_count = 0;
		$count = 0;
		
		while( $r_item = $result->fetch() ) {
			if($r_item["man_id"] == 263|| $r_item["man_id"] < 1 || $r_item["cat_id"] == 81 || $r_item["cat_id"] < 1){
				$highlight = ' class="highlight" ';
			} else {
				$highlight = '';
			}
						
			if($r_item["mim"] == 0){
				$has_mim = '*';
			} else {
				$has_mim = '';
				$count++;
			}
			
			$total_count++;
			
			$item_select .= '<option value="'.$r_item["upc"].'" '.$highlight.'>'.$r_item["item_name"].$has_mim.'</option>';
			
		}
		
		$this->_view->assign('nutrition_per', round( (($count / $total_count)*100),1) );
		$this->_view->assign('nutrition_num', '('.$count.' / '.$total_count.')' );
		
		$nutrition_types = \EE\Model\Nutrition\TypesObject::getTypes();
		
		$this->_view->assign('man_select',$man_select);
		$this->_view->assign('cat_select',$cat_select);
		$this->_view->assign('item_select',$item_select);
		$this->_view->assign( 'nutrition_types', $nutrition_types );
		
		////new categories
		$html = "";
		$html = Ta_CategoriesController::listCategories(0, 1);
		$this->_view->assign('html', $html);
	}
	
	public function ajaxAllMenuItems(){
		if($this->_isXHR){
			/* build menu item dropdown */
			$query = "select A.*, coalesce(B.mim_id,0) as 'mim'
					from
						(SELECT m.upc as 'upc', m.name as 'item_name', m.Manufacturer as 'man_id', m.Category as 'cat_id', m.Id as 'master_id'
						FROM menu_items_master m
						LEFT JOIN menu_items_category c ON c.id = m.category
						LEFT JOIN manufacturer a ON a.id = m.manufacturer
						WHERE m.is_deleted = 0
						order by m.name) as A
					left outer join
						(select distinct mim_id
						from nutrition_xref_master) as B
					on A.master_id = B.mim_id";

			$result = $this->_db->query($query);
			
			$item_select = '';
			
			while( $r_item = $result->fetch() ) {
				if($r_item["man_id"] == 263|| $r_item["man_id"] < 1 || $r_item["cat_id"] == 81 || $r_item["cat_id"] < 1){
					$highlight = ' class="highlight" ';
				} else {
					$highlight = '';
				}
				
				if($r_item["mim"] == 0){
					$has_mim = '*';
				} else {
					$has_mim = '';
				}

				$item_select .= '<option value="'.$r_item["upc"].'" '.$highlight.'>'.$r_item["item_name"].$has_mim.'</option>';
			}
			
			print $item_select;

			return false;
		} else{
			print 'not ajax';
		}
	}
	
	public function ajaxLoadMenuItem() {
		if($this->_isXHR){
			header('Content-type: application/json');
			
			$upc = static::getPostVariable( array( 'upc', 'UPC' ) );
			
			\EE\Model\Menu\Item\Master::db()->beginTransaction();
			$menu_item = \EE\Model\Menu\Item\Master::getItemByUpcOrId( null, $upc, $class = null );
			\EE\Model\Menu\Item\Master::db()->commit();
			
			if ( $menu_item ) {
				$return = array(
					'item_id' => $menu_item->item_id,
					'upc' => $menu_item->upc,
					'item_name' => $menu_item->item_name,
					'cat_id' => $menu_item->cat_id,
					'man_id' => $menu_item->man_id,
					'pass' => 1,
					'nutrition' => array(),
					'categories' => array(),
				);
				$nutrition_types = explode( ':', $menu_item->nutrition_type_ids );
				$nutrition_keys = explode( ':', $menu_item->column_keys );
				$nutrition_values = explode( ':', $menu_item->values );
				
				foreach ( $nutrition_keys as $key => $value ) {
					if ( isset( $nutrition_values[ $key ] ) && isset( $nutrition_types[ $key ] ) ) {
						$return[ 'nutrition' ][] = array(
							'key' => $nutrition_keys[ $key ],
							'value' => $nutrition_values[ $key ],
						);
					}
				}
				
				$return['pass'] = 1;
				
				///find categories
				$query = "SELECT categoryid FROM category_detail WHERE master_id = :master_id";
				$params = array(
					'master_id' => $menu_item->item_id,
				);
				$db = \EE\Model\AbstractModel\Treat::db();
				$stmt = $db->prepare( $query );
				if ( $stmt->execute( $params ) ) {
					while( $r = $stmt->fetch() ) {
						$return['categories'][] = array( 'value' => $r["categoryid"] );
					}
				}
				
				$return = json_encode( $return );
			}
			else {
				$return = array( 'pass' => 0 );
			}
			
			print $return;
			
			return false;
		} else{
			print 'not ajax';
		}
	}
		
	public function ajaxSearch(){
		if($this->_isXHR){
			header('Content-type: application/json');
		
			if( empty($_POST['search_upc']) && empty($_POST['search_name']) && empty($_POST['search_man']) && empty($_POST['search_cat']) ){				
				$return = array( 'status' => 'no_criteria' );
				
				$return = json_encode( $return );
				
				return false;
			}			
			
			$where = '';
			$num_criteria = 0;
			
			/* where clause for upc */
			if(!empty($_POST['search_upc']) ){
				if($num_criteria == 0){
					$where .= 'm.upc LIKE "%'.$_POST['search_upc'].'%"';
					$num_criteria = 1;
				} else {
					$where .= ' AND m.upc LIKE "%'.$_POST['search_upc'].'%"';
				}
			}
			
			/* where clause for item name */
			if( !empty($_POST['search_name']) ){
				if($num_criteria == 0){
					$where .= 'm.Name LIKE "%'.$_POST['search_name'].'%"';
					$num_criteria = 1;
				} else {
					$where .= ' AND m.Name LIKE "%'.$_POST['search_name'].'%"';
				}
			}
			
			/* where clause for manufacturer */
			if( !empty($_POST['search_man']) ){
				if($num_criteria == 0){
					$where .= 'm.Manufacturer = '.$_POST['search_man'];
					$num_criteria = 1;
				} else {
					$where .= ' AND m.Manufacturer = '.$_POST['search_man'];
				}
			}
			
			/* where clause for category */
			if( !empty($_POST['search_cat']) ){
				if($num_criteria == 0){
					$where .= 'm.Category = '.$_POST['search_cat'];
					$num_criteria = 1;
				} else {
					$where .= ' AND m.Category = '.$_POST['search_cat'];
				}
			}
					
			$query = "select A.*, coalesce(B.mim_id,0) as 'mim'
						from
							(SELECT m.upc as 'upc', m.name as 'item_name', m.Manufacturer as 'man_id', m.Category as 'cat_id', m.Id as 'master_id'
							FROM menu_items_master m
							INNER JOIN menu_items_category c ON c.id = m.category
							INNER JOIN manufacturer a ON a.id = m.manufacturer
							WHERE ".$where." AND m.is_deleted = 0
							order by m.name) as A
						left outer join
							(select distinct mim_id
							from nutrition_xref_master) as B
						on A.master_id = B.mim_id";
						
			$result = $this->_db->query($query);
			
			$index = 0;
			$option = '';
			
			/* build ajax response */
			while( $r = $result->fetch() ) {
				if($r["man_id"] == 263 || $r["cat_id"] == 81){
					$highlight = ' class="highlight" ';
				} else {
					$highlight = '';
				}				
				
				if($r["mim"] == 0){
					$has_mim = '*';
				} else {
					$has_mim = '';
					$count++;
				}
				
				$option .= '<option value="'.$r["upc"].'" '.$highlight.'>'.$r["item_name"].$has_mim.'</option>';
				
				$index++;
			}
			
			if($index == 0){
				$return = array( 'status' => 'failed' );
			} else {
				$return = array( 'status' => 'passed', 'option' => $option, 'num_results' => $index);
			}
			
			$return = json_encode( $return );
			
			print $return;
			
			return false;
		} else{
			print 'not ajax';
		}
	}
	
	public function ajaxSaveMenuItem()
	{
		if($this->_isXHR){
			header('Content-type: application/json');
			$_POST = new \EE\ArrayObject( $_POST );
			
			// missing required field(s)
			if( empty($_POST['upc']) || empty($_POST['item_name']) ){
				$return = array('passed' => 0, 'error' => 'req_missing');
				
				$return = json_encode($return);
				
				print $return;
				
				return false;
			}
			
			$mim_item = \EE\Model\Menu\Item\Master::getByUpc( $_POST['upc'], $class = null );
			if ( !$mim_item ) {
				$mim_item = new \EE\Model\Menu\Item\Master();
				$mim_item->UPC = $_POST['upc'];
			}
			if ( !( is_array( $_POST['cat'] ) || $_POST['cat'] instanceof \Traversable ) ) {
				$_POST['cat'] = array();
			}
			
			if ( $mim_item->Id ) {
				$db_update = 1;
			}
			else {
				$db_update = 0;
			}
			
			$mim_item->Name = $_POST['item_name'];
			$mim_item->Manufacturer = $_POST['manufacturer'];
			$mim_item->Category = $_POST['category'];
#			$mim_item->isCore = $_POST['isCore'];
#			$mim_item->is_deleted = $_POST['is_deleted'];
			try {
				$mim_item->save();
			}
			catch ( PDOException $e ) {
				$return = array( 'passed' => 0 );
				echo json_encode( $return );
				return false;
			}

			// delete occurs on writer, but the way the ->save() method works
			// for a multi-key table is it does a select on the readers where
			// the delete has not yet propogated, making the ->save() attempt
			// an update instead of an insert. The way around this is to do
			// everything in a transaction.
			\EE\Model\Nutrition\Xref\Master::db()->beginTransaction();
			// insert/update nutrition_xref_master rows
			$nutrition_item = new \EE\Model\Nutrition\Xref\Master();
			$nutrition_item->mim_id = $mim_item->Id;
			
			if( $db_update ) {
				$nutrition_item->delete();  //delete all existing rows
			}
			
			//insert new rows
			foreach ( $_POST['nutrition'] as $key => $value ) {
				if ( empty( $value ) && $value !== '0' ) {  //don't insert blank/non-zero value records
					continue;
				}
				$nutrition_item = new \EE\Model\Nutrition\Xref\Master();
				$nutrition_item->mim_id = $mim_item->Id;
				$nutrition_item->nutrition_type_id = $key;  //key of nutrition item
				$nutrition_item->value = $value;
				
				$nutrition_item->save();
			}
			\EE\Model\Nutrition\Xref\Master::db()->commit();

			$db = \EE\Model\AbstractModel\Treat::db();
			///update categories
			$query = "DELETE FROM category_detail WHERE master_id = :master_id";
			$params = array(
				'master_id' => $mim_item->Id,
			);
			$stmt = $db->prepare( $query );
			$stmt->execute( $params );
			
			$query = "INSERT INTO category_detail ( categoryid, master_id ) VALUES ( :categoryid, :master_id )";
			$stmt = $db->prepare( $query );
			foreach ( $_POST['cat'] as $key => $value ) {
				if($value == 1){
					$params = array(
						'categoryid' => $key,
						'master_id' => $mim_item->Id,
					);
					$stmt->execute( $params );
				}
			}
			
			$return = array(
				'upc' => $mim_item->UPC,
				'item_name' => $mim_item->Name,
				'update' => $db_update,
				'mim' => $mim_item->Id,
				'passed' => 1
			);
			
			echo json_encode( $return );
			
			return false;
		}
		else {
			echo 'not ajax';
		}
	}
	
	public function ajaxDeleteMenuItem(){
		if($this->_isXHR){
			header('Content-type: application/json');
			
			/* missing required field(s) */
			if( empty($_POST['upc']) ){
				$return = array('passed' => 0, 'error' => 'req_missing');
				
				$return = json_encode($return);
				
				print $return;
				
				return false;
			}
			
			$query = "SELECT upc
						FROM menu_items_master
						WHERE upc = ".$_POST['upc'];

			$result = $this->_db->query($query);
			$num_rows = $result->rowCount();
			
			if($num_rows == 1){  //only 1 result found			
				$query = 'UPDATE menu_items_master
							SET is_deleted = 1
							WHERE UPC = "'.$_POST['upc'].'"';
				
				$result = $this->_db->query($query);
				
				$return = array('upc' => $_POST['upc'], 'item_name' => $_POST['item_name'], 'passed' => 1);
				
				$return = json_encode($return);
				
				print $return;
			} else {
				$return = array('passed' => 0);
				
				$return = json_encode($return);
				
				print $return;
			}
			
			return false;
		} else{
			print 'not ajax';
		}
	}	
}
