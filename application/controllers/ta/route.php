<?php

#class Ta_RouteController extends Treat_Controller_Abstract
class Ta_RouteController extends Treat_Controller_UnitAbstract
{
	const BUSROUTE_DETAIL = '/ta/busroute_detail.php';
	
#	protected $_layout = 'ta_buscontrol.tpl';
	
	
	# for softlink to htdocs/ta/busroute_detail.php ...
	# when the Treat_Controller_UnitAbstract is modified to allow iframes then
	# we can move htdocs/ta/busroute_detail.php to application/views/ta/route/detail.tpl
	public function detail() {
		$this->_layout = '';
	}
	
	
	public function index() {
#		http://treat/ta/busroute_collect.php?cid=3&bid=97
		$get = '';
		if ( $_GET ) {
			$get = '?' . http_build_query( $_GET );
		}
		header( 'Location: /ta/busroute_collect.php' . $get );
		exit();
		return false;
	}
	
	
#	public function labor_route_link() {
	public function labor_link() {
		$qry = '';
		if ( $_POST ) {
			if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
				echo '<pre>';
				var_dump( $_POST );
				echo '</pre>';
			}
			$_POST = new \EE\ArrayObject( $_POST );
			# do select based on ( loginid [employee record] || login_routeid [route record] ) && date
			# if the route record exists with (freevend|noncash|kiosk|csv) as non-zero use route record
			# always use the route record if it exists
			# if route record doesn't exist, use the loginid record
			# otherwise insert new record
			if ( $_POST->e && ( is_array( $_POST->e ) || $_POST->e instanceof Traversable ) ) {
				$options = array(
					\EE\Model\Route\LaborLink::SAVE_OPTION_IS_WK1_DISABLED    => $_POST->is_disabled1,	# $_POST->is_disabled2
					\EE\Model\Route\LaborLink::SAVE_OPTION_IS_WK2_DISABLED    => $_POST->is_disabled2,	# $_POST->is_disabled1
					\EE\Model\Route\LaborLink::SAVE_OPTION_LABOR_GROUP        => $_POST->lbrgrp,
					\EE\Model\Route\LaborLink::SAVE_OPTION_UPDATE_PAYROLL     => $_POST->update_payroll,
					\EE\Model\Route\LaborLink::SAVE_OPTION_WK1_DATE           => $_POST->prevperiod,	# $_POST->date1
					\EE\Model\Route\LaborLink::SAVE_OPTION_WK2_DATE           => $_POST->date1,	# $_POST->prevperiod
				);
				try {
					$success = \EE\Model\Route\LaborLink::saveRouteDetailEmployees( $_POST->e, $options );
					if ( $success ) {
						$_SESSION->addFlashMessage( 'Route details successfully saved' );
					}
					else {
						$_SESSION->addFlashError( 'There was a problem saving the route details' );
					}
				}
				catch( \Exception $e ) {
					$_SESSION->addFlashError(
						'There was a problem saving the route details. Exception caught.'
						. PHP_EOL
						. '<ul><li>Exception: '
						. (string)$e->getCode()
						. ': '
						. (string)$e->getMessage()
						. '</li></ul>'
					);
					if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
						$_SESSION->addFlashError( 'Exception:' . PHP_EOL . '<pre>' . var_export( $e, true ) . '</pre>' );
					}
				}
			}
			$post = new \EE\ArrayObject();
			$post->cid = $_POST->cid;
			$post->bid = $_POST->bid;
			$post->mei = $_POST->mei;
			$post->date1 = $_POST->date1;
			$post->lbrgrp = $_POST->lbrgrp;
			if ( $_POST->businessid && !$post->bid ) {
				$post->bid = $_POST->businessid;
			}
			if ( $_POST->companyid && !$post->cid ) {
				$post->cid = $_POST->companyid;
			}
			$qry = '?' . http_build_query( (array)$post );
		}
		elseif ( $_GET ) {
			$qry = '?' . http_build_query( (array)$_GET );
		}
		$location = Ta_RouteController::BUSROUTE_DETAIL . $qry;
		if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
			echo sprintf(
				'<a href="%1$s">%1$s</a>'
				,$location
			);
		}
		else {
			header( 'Location: ' . $location );
		}
		exit();
		return false;
	}
	
	
	public function print_detail() {
		$this->_layout = '';
		
		$business_id   = \EE\Controller\Base::getPostGetSessionOrCookieVariable( array( 'business_id', 'businessid' ), 0 );
		$company_id    = \EE\Controller\Base::getPostGetSessionOrCookieVariable( array( 'company_id', 'companyid' ), 0 );
		$com_calc_type = \EE\Controller\Base::getPostGetSessionOrCookieVariable( 'com_calc_type', 0 );
		$mei           = \EE\Controller\Base::getPostGetSessionOrCookieVariable( 'mei', 0 );
		$date1         = \EE\Controller\Base::getPostOrGetVariable( 'date1', '' );
		$lbrgrp        = \EE\Controller\Base::getPostOrGetVariable( 'lbrgrp', 0 );
		
		$page_business = \EE\Model\Business\Singleton::getSingleton( $business_id, $company_id );
		
		$route_detail = new \EE\Model\Labor\Route\Detail();
		$route_detail->setUserId( $_SESSION->getUser()->loginid );
		$route_detail->setUserRouteDetailSecurityLevel( $_SESSION->getUser()->route_detail );
		$route_detail->setBusiness( $page_business );
		$route_detail->setDate( $date1 );
		$route_detail->setComCalcType( $com_calc_type );
		$route_detail->setMei( $mei );
		$route_detail->setLaborGroup( $lbrgrp );
		
		$route_detail->init();
		$routes = $route_detail->getRoutes();
		$vend_commissions = $route_detail->getVendingCommissions();
		$route_detail_employees = $route_detail->getEmployees();
		
		$this->_view->assign( 'page_company', $page_business );
		$this->_view->assign( 'route_detail', $route_detail );
		$this->_view->assign( 'routes', $routes );
		$this->_view->assign( 'vend_commissions', $vend_commissions );
		$this->_view->assign( 'route_detail_employees', $route_detail_employees );
	}
	
	
	public function save_commission_settings() {
		$user = static::getSessionCookieVariable( 'usercook' );
		$pass = static::getSessionCookieVariable( 'passcook' );
		
		$edit = static::getPostVariable( 'edit' );
		$commission_id = static::getPostVariable( 'commissionid' );
		
		$company_id = static::getPostGetSessionOrCookieVariable( array( 'company_id', 'cid', 'companyid', 'compcook' ) );
		$name = static::getPostVariable( 'commission_name' );
		
		$commission_settings = null;
		if ( ( $commission_id = intval( $commission_id ) ) ) {
			$commission_settings = \EE\Model\Vending\Commission::get( $commission_id, true );
			if ( $commission_settings ) {
				$base = static::getPostVariable( 'base' );
				$holiday_pay = static::getPostVariable( 'holiday_pay' );
				$base_commission = static::getPostVariable( 'base_commission' );
				$fixed_amount = static::getPostVariable( 'fixed_amount' );
				$route_emp = static::getPostVariable( 'route_emp' );
				
				$commission_settings->base = $base;
				$commission_settings->holiday_pay = $holiday_pay;
				$commission_settings->base_commission = $base_commission;
				$commission_settings->fixed_amount = $fixed_amount;
				$commission_settings->route_emp = $route_emp;
				
				$link_list = \EE\Model\Payroll\Code\Commission::getLinkList();
				foreach ( $link_list as $link_item ) {
					$tmp = static::getPostVariable( $link_item->form_name, null );
					if ( null !== $tmp ) {
						$commission_settings->{$link_item->form_name} = intval( $tmp );
					}
				}
				
			}
		}
		if ( !$commission_settings ) {
			$commission_settings = new \EE\Model\Vending\Commission();
		}
		
		$commission_settings->companyid = $company_id;
		$commission_settings->name = $name;
		
		$commission_settings->save();
		
		$query_args = array(
			'cid' => $company_id,
			'editcommission' => $commission_settings->commissionid,
		);
		$location = '/ta/user_routes.php?%s#vendcom';
		if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
			$location = sprintf(
				$location
				,http_build_query( $query_args, null, '&amp;' )
			);
			echo sprintf(
				'<a href="%1$s">%1$s</a>'
				,$location
			);
		}
		else {
			$location = sprintf(
				$location
				,http_build_query( $query_args )
			);
			header( 'Location: ' . $location );
		}
		
		return false;
	}
	
	
}