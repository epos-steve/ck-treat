<?php

/**
 * promotions controller
 *
 * @author Eric Gach <eric@essential-elements.net>
 */
class Ta_PromotionsController extends Treat_Controller_Abstract {

	protected $_noLoginRequired = array(
	);
	protected $_argMap = array(
			'details' => array('promo_id', 'bid'),
			'delete' => array('bid','promo_id'),
			'getPromotions' => array('bid'),
			'schedule' => array('promo_id', 'bid'),
			'index' => array('bid'),
			'businesses' => array('promo_id'),
			'updatePromo' => array('promo_id'),
			'updateSchedule'	=> array('promo_id'),
	);
	protected $_layout = 'promotions.tpl';

	public function init() {
		mb_internal_encoding("UTF-8");
		mb_http_output("UTF-8");
		ob_start("mb_output_handler");
		if ($this->_isXHR) {
			//ini_set('display_errors', 'true');
			$this->_layout = false;
		} elseif (!empty($this->_args['bid'])) {
			$this->_layout = 'unitview.tpl';
		}
		//parent::init();
	}

	public function index() {
		/* Scott 4-19-12, redirect if bus_promo_manger == 0 */
		$user = $_SESSION->getUser();

		if($user['bus_promo_manager'] == 0){
			header('Location: /ta/businesstrack.php');
			exit;
		}
		/* Scott 4-19-12 */

		/* TODO: standardize how jQuery is included */
		if (!IN_PRODUCTION) {
			$this->addJavascriptAtTop('/assets/js/jquery-1.7.2.js');
			$this->addJavascriptAtTop('/assets/js/jqueryui-current.js');
		} else {
			$this->addJavascriptAtTop('/assets/js/jquery-current.min.js');
			$this->addJavascriptAtTop('/assets/js/jqueryui-current.min.js');
		}
		$this->addJavascriptAtTop('/assets/js/kioskPromotions.js');
        $this->addJavascriptAtTop('/assets/js/kioskCombos.js');
        $this->addJavascriptAtTop('/assets/js/kioskPromotionsCommon.js');
		$this->addJavascriptAtTop('/assets/js/kioskPromotionsGlobal.js');
		$this->addJavascriptAtTop('/assets/js/jquery.cookie.js');
		$this->addJavascriptAtTop('/assets/js/jquery.dynatree.js');
        $this->addPlugin( 'jquery.ui.picker' );
		$this->addCss('/assets/css/jq_themes/redmond/jquery-ui-1.8.11.custom.css');
		$this->addCss('/assets/css/jquery.dynatree/skin/ui.dynatree.css');
		$this->addCss('/assets/css/calendarPopup.css');
		$this->addCss('/assets/css/customDashboard.css');
		$this->addCss('/assets/css/jquery.ui.tooltip.css');
		$this->addCss('/assets/css/ta/businessmenus.css');
		$this->addCss('/assets/css/settingsForm.css');

		if (!empty($this->_args['bid'])) {
			$promotions = \EE\Model\Promotion::getList($this->_args['bid']);
			$this->_view->assign('taxList', Treat_Model_Menu_Tax::get("businessid {$this->_args['bid']} ORDER BY pos_id"));
		} else {
			$promotions = \EE\Model\Promotion::getList();
		}
		$this->_view->assign('promotions', $promotions);
		$this->_view->assign('promoReportingCategories', \EE\Model\Promotion::getReportingCategories());
		$this->_view->assign('promoTargetList', Treat_Model_Menu_Promo_Trigger::get());
		$this->_view->assign('promoTypeList', Treat_Model_Menu_Promo_Type::get());
		$this->_view->assign('uri', $this->_uri->getController());
	}

	public function details() {
		$promo_id = (int) $this->_args['promo_id'];
		try {
			$promotion = \EE\Model\Promotion::get($promo_id, TRUE);
		} catch (Exception $e) {
			echo "Promotion not found. (Error: ".$e->getMessage().")";
			return false;
		}

		// This is used when we save the details.
		if (SiTech_Filter::isPost()) {
			$filter = new SiTech_Filter(SiTech_Filter::INPUT_POST);
			if (!$filter->input('type', SiTech_Filter::FILTER_VALIDATE_INT)) {
				echo 'Invalid promo type';
				return false;
			}

			if ($filter->hasVar('clear') && $filter->input('clear') == 'true') {
				foreach ($promotion->promo_details as $detail) {
					if ($detail->type == $filter->input('type')) {
						$detail->delete();
					}
				}
			}

			if ($filter->hasVar('promo_value') && $filter->input('promo_value') == 'true') {
				$detail = new Treat_Model_Promotion_Details();
				$detail->promo_id = $promo_id;
				$detail->type = $filter->input('type');
				$detail->id = $filter->input('id');
				$detail->save();
			} else {
				$instance = Treat_Model_Promotion_Details::get('promo_id = ' . $promo_id . ' AND type = ' . $filter->input('type') . ' AND id = ' . $filter->input('id'), true);
				if (is_object($instance)) {
					$instance->delete();
				}
			}
			$promotion = \EE\Model\Promotion::get($promo_id, TRUE); // reload with changes
			$promotion->kioskSyncAll();

			// If its an ajax call, output 1 and \EE\Model\Promotion::DETAIL_TYPE_return
			if ($this->_isXHR) {
				echo 1;
				return false;
			}
		}

		$target_id = $promotion->promo_trigger->target_id;
		switch ((int) $target_id) {
			case Treat_Model_Promotion_Triggers::TARGET_COMBO_GROUP:
				$selectedOptions = array();
                $selectedOptionsCombos = array();
				foreach ($promotion->promo_details as $detail) {
					if (in_array($detail->type, $promotion->getComboGroupTypes())) {
						$selectedOptions[$detail->type] = $detail->id;
                        $selectedOptionsCombos[$detail->type] = array('idis' => $detail->combo_item_discount,
                                                                      'adis' => $detail->combo_item_apply_discount,
                                                                      'rows_cols' => $detail->combo_item_rows_cols,
                                                                      'rows_cols_type' => $detail->combo_item_rows_cols_type,
                                                                      'pos_color' => $detail->combo_item_pos_color);
					}
				}

				if (!empty($this->_args['bid'])) {
					$where = '= ' . (int)$this->_args['bid'];
				} else {
					$where = 'IS NULL';
				}

				$this->_view->assign('groups', Treat_Model_Promotion_Group::get('businessid ' . $where));
				$this->_view->assign('selectedOptions', $selectedOptions);
                                
                if ($promotion->category == \EE\Model\Promotion::CATEGORY_COMBO) {
                    $this->_view->assign('promo_group_types', $promotion->getComboGroupTypes());
                    $this->_view->assign('selected_options_combos', $selectedOptionsCombos);
                } else {
                    $this->_view->assign('promo_group_types', false);
                    $this->_view->assign('selected_options_combos', false);
                }
                                
				break;

			case Treat_Model_Promotion_Triggers::TARGET_ITEM:
			case Treat_Model_Promotion_Triggers::TARGET_ALL_PRODUCTS:
			case Treat_Model_Promotion_Triggers::TARGET_SIMPLE_COMBO:
			case Treat_Model_Promotion_Triggers::TARGET_SIMPLE_GROUP:
				break;

			default:
				throw new Treat_Exception('Promotion does not have a valid promotion trigger (' . $target_id . ')');
		}

		$this->_view->assign('promotion', $promotion);
	}

	/**
	 * This is called by the 'Activate/Deactivate' button, which sends an
	 * AJAX POST request.
	 *
	 * @todo Why is it called activate but labeled delete?
	 * @todo Is this supposed to actually delete anything?
	 * @todo Should it just be renamed to activate?
	 */
	public function activate() {
		$this->_layout = false;
		if (!SiTech_Filter::isPost()) {
			return false;
		}
		$promo_id = (int) $_REQUEST['promo_id'];
		header('Content-Type: application/json; charset=utf-8');
		if (!isset($_REQUEST['promo_id']) || $promo_id < 1) {
			echo json_encode(Array(
					'success' => 0,
					'msg'	=> print_r($_REQUEST, TRUE),	//"promo_id='$promo_id'",
					'error' => "Valid promotion was not specified",
			));
			return;
		}
		try {
			$promotion = \EE\Model\Promotion::get($promo_id, TRUE);
		} catch (Exception $e) {
			echo json_encode(Array(
					'success' => 0,
					'msg' => "promo_id='$promo_id'",
					'error' => "Promotion not found. $e",
			));
			return;
		}
		$is_active = $_REQUEST['active'];
		if ($is_active == 1) {
			$promotion->activeFlag = true;
		} else {
			$promotion->activeFlag = false;
		}
		$promotion->save();
		Treat_Model_KioskSync::syncPromotionCache();
		$promotion->KioskSyncPromotions();

		if (!isset($results)) {
			$results = '';
		}
		
		echo json_encode(Array(
				'success' => TRUE,
				'msg' => "Promotion $promo_id updated: " . $results,
				'error' => NULL,
		));
		return;
	}

	public function getMasterItems() {
		$this->_layout = false;
		$sitech_filter = new SiTech_Filter(SiTech_Filter::INPUT_GET);
		$term = $sitech_filter->input('term');
		$db = \EE\Model\Promotion::db();
		$query = "SELECT name FROM menu_items_master";
		$result = $db->query($query);
		$result->setFetchMode(PDO::FETCH_NAMED);
		$results = $result->fetchAll();
		$items = Array();
		foreach ($results as $item) {
			$items[] = $item['name'];
		}
		$filter = function($mitem) use ($term)
		{
			if(stristr($mitem,$term))
				return true;
			return false;
		};
		$found = array_filter($items,$filter);
		//var_dump($found);die;
		header('Content-Type: application/json; charset=utf-8');
		//echo json_encode($found);
		echo '["' . implode("\",\"", $found) . '"]';
		return;
	}

	/**
	 * Generate a list of promotions, output in JSON
	 */
	public function getPromotions() {
		$this->_layout = false;
		if (isset($_REQUEST['bid']) && $_REQUEST['bid'] != NULL) {
			$this->_args['bid'] = $_REQUEST['bid'];
		}
		if ($this->_args['bid'] == NULL) {
			$businessid = NULL;
		} else {
			$businessid = (int)$this->_args['bid'];
		}
                if (isset($_REQUEST['category']) && $_REQUEST['category'] != NULL) {
			$category = $_REQUEST['category'];
		} else {
                    $category = 1;
                }
                
		$promos = \EE\Model\Promotion::getList($businessid, $category);

		$types = array();
		$triggers = array();

		foreach ($promos as $k => $promo) {
			// le-sigh... SiTech 2.0 would handle this so much better
			$promos[$k] = json_decode($promo->toJson());
		}

		foreach (Treat_Model_Promotion_Types::get() as $type) {
			$types[$type->id] = $type->vivipos_name;
		}

		foreach (Treat_Model_Promotion_Triggers::get() as $trigger) {
			$triggers[$trigger->id] = $trigger->vivipos_name;
		}

		header('Content-Type: application/json; charset=utf-8');
		echo json_encode(array('promos' => $promos, 'viviposTypes' => $types, 'viviposTriggers' => $triggers));
		return false; // Prevent display for ajax.
	}

    public function getMainComboItems() {
        $this->_layout = false;
        $filter = new SiTech_Filter(SiTech_Filter::INPUT_GET);
        $promo_id = $filter->input('promo_id');
        if ($promo_id != NULL) {

            $promotion = \EE\Model\Promotion::get($promo_id, TRUE);

            $detail = \EE\Model\Promotion\Details::get('promo_id = ' . $promo_id . ' AND type = '
                . $promotion::DETAIL_TYPE_COMBO_GROUP_1, TRUE);

            $promo_group_id = $detail->id;
            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($promotion->getItemsByPromoGroup($promo_group_id));
        }
        return false; // Prevent display for ajax.
    }

	/**
	 * This is just a way to save the promo. This isn't used to display anything,
	 * just to save a promo and let the user know of any errors.
	 */
	public function save() {
		$this->_layout = false;
		try {
			if (SiTech_Filter::isPost()) {
				if ( isset($_REQUEST['bid']) && (int)$_REQUEST['bid'] > 0) {
					$businessid = (int)$_REQUEST['bid'];
				}
				$filter = new SiTech_Filter(SiTech_Filter::INPUT_POST);
                                
                // Combo Promotion
                $combo_id = $filter->input('combo_id');
                if ( is_numeric($combo_id) ) {
                    $promotion = ($combo_id == 0) ? new \EE\Model\Promotion(null, true, $businessid) : \EE\Model\Promotion::get((int) $combo_id, true);
                    $promotion->businessid = $businessid;
                    $promotion->category = \EE\Model\Promotion::CATEGORY_COMBO;
                    $promotion->name = $filter->input('combo_name');
                    $promotion->type = $filter->input('combo_type');
                    $promotion->target = 0;
                    $promotion->value = $filter->input('combo_value');
                    $promotion->promo_trigger = \EE\Model\Promotion::TRIGGER_COMBO_GROUP;
                    $promotion->promo_reserve = ($filter->input('disable_promotions') == '1') ? 1 : 0;

                // Regular promotion
                } else {
                    $promo_id = $filter->input('promo_id');
                    $promotion = ($promo_id == 0) ? new \EE\Model\Promotion(null, true, $businessid) : \EE\Model\Promotion::get((int) $promo_id, true);
                    $promotion->businessid = $businessid;
                    $promotion->category = \EE\Model\Promotion::CATEGORY_PROMO;
                    $promotion->name = $filter->input('promo_name');
                    $promotion->type = $filter->input('promo_type');
                    $promotion->target = ($filter->input('promo_target'))? : '0';
                    $promotion->value = $filter->input('promo_value');
                    $promotion->promo_trigger = Treat_Model_Promotion_Triggers::get((int) $filter->input('promo_trigger'), true);
                    $promotion->tax_id = $filter->input('promo_tax', SiTech_Filter::FILTER_DEFAULT, array('flags' => array(SiTech_Filter::FLAG_EMPTY_STRING_NULL)));
                    $promotion->pre_tax = ($filter->input('pre_tax') == 1) ? 1 : 0;
                    $promotion->promo_reserve = $filter->input('promo_reserve');
                    $promotion->promo_discount = $filter->input('promo_option_discount');
                    $promotion->promo_discount_type = $filter->input('promo_option_discount_type');
                    $promotion->promo_discount_limit = $filter->input('promo_option_discount_limit');
                    $promotion->promo_discount_n = $filter->input('promo_option_discount_n');
                    $promotion->promo_trigger_amount_limit = $filter->input('promo_trigger_amount_limit');
                    $promotion->promo_trigger_amount_type = $filter->input('promo_trigger_amount_type');
                    $promotion->promo_trigger_amount = $filter->input('promo_trigger_amount');
                    $promotion->promo_trigger_amount_2 = $filter->input('promo_trigger_amount_2');
                    $promotion->promo_trigger_amount_3 = $filter->input('promo_trigger_amount_3');
                    $promotion->promo_reserve = ($filter->hasVar('promo_reserve') && $filter->input('promo_reserve') == '1') ? 1 : 0;
                    $promotion->subsidy = ($filter->hasVar('subsidy') && $filter->input('subsidy') == '1') ? 1 : 0;
                    $promotion->reporting_category_id = $filter->input('promo_reporting_category');
                }

                $success = $promotion->save();
                Treat_Model_KioskSync::syncPromotionCache($businessid);
                $promotion->kioskSyncAll();
                if (!$success) {
                        header('Content-Type: application/json; charset=utf-8');
                        echo json_encode($promotion->errors);
                        return false;
                }

                // Update/Create Promotion Details for Combo Promotions.
                if ($promotion->category == \EE\Model\Promotion::CATEGORY_COMBO) {
                    foreach($promotion->getComboGroupTypes() as $key=>$type) {
                        $key_id = $key + 1;
                        $promo_group_id = $filter->input('combo_group_' . $key_id);
                        $instance = Treat_Model_Promotion_Details::get('promo_id = ' . $promotion->id . ' AND type = ' . $type, true);
                        if (is_object($instance)) {
                            $instance->delete();
                        }

                        if ($promo_group_id) {
                            $detail = new Treat_Model_Promotion_Details();
                            $detail->promo_id = $promotion->id;
                            $detail->type = $type;
                            $detail->id = $promo_group_id;
                            $detail->combo_item_rows_cols = $filter->input('rows_cols_' . $key_id);
                            $detail->combo_item_rows_cols_type = $filter->input('rows_cols_type_' . $key_id);
                            $detail->combo_item_pos_color = $filter->input('combo_pos_color_' . $key_id);

                            if ($promotion->type == \EE\Model\Promotion::TYPE_AMOUNT_OFF) {
                                $combo_item_discount = $filter->input('combo_item_discount_' . $key_id);
                                if (!$combo_item_discount) {
                                    $combo_item_discount = 0.00;
                                }
                                $detail->combo_item_discount = $combo_item_discount;

                            } else { // Assume \EE\Model\Promotion::TYPE_FIXED_VALUE
                                $raw_input = $filter->input('combo_item_apply_discount_' . $key_id);
                                $combo_item_apply_discount = ($raw_input == 1) ? 1 : 0;
                                $detail->combo_item_apply_discount = $combo_item_apply_discount;
                            }

                            $detail->save();
                        }
                    }
                }
                                    
			}
		} catch (Exception $e) {
			header('Content-Type: application/json; charset=utf-8');
			echo json_encode( array('message' => $e->getMessage()) );
		}
		return false;
	}

	public function schedule() {
		$promo_id = (int) $this->_args['promo_id'];
		if (isset($this->_args['bid'])) {
			$businessid = $this->_args['bid'];
			$this->_view->assign('businessid', $businessid);
		}
		$promotion = \EE\Model\Promotion::get($promo_id, TRUE);
		$addPadding = function($item) {
			return sprintf("%02d", $item);
		};
		$minutes_list = array_map($addPadding, range(0, 59, 5));
		$minutes_list[] = '59';
		$this->_view->assign('hours_list', array_map($addPadding, range(1, 12)));
		$this->_view->assign('minutes_list', $minutes_list);
		$this->_view->assign('promo_id', $promo_id);
		$this->_view->assign('promotion', $promotion);
		$this->_view->assign('start_date', $promotion->start_date);
		$this->_view->assign('start_time', explode( ':', $promotion->start_time ));
		$this->_view->assign('end_date', $promotion->end_date);
		$this->_view->assign('end_time', explode( ':', $promotion->end_time ));
		
		$days = array(
			'monday',
			'tuesday',
			'wednesday',
			'thursday',
			'friday',
			'saturday',
			'sunday',
		);
		
		$daysArray = array();

		foreach ( $days as $day_name )
		{
			$short_day = substr($day_name, 0, 3);
			$show_day = '';
			if ( $promotion->{$day_name} == 1)
			{
				$show_day = ' checked';
			}
			$daysArray[$short_day] = array(
				'name' => $day_name,
				'show' => $show_day,
			);
		}
		$this->_view->assign("daysArray", $daysArray);
	}

	public function updateSchedule() {
		//$this->_layout = false;
		$promo_id = (int) $this->_args['promo_id'];
		if (!SiTech_Filter::isPost()) {
			return false;
		}
		$filter = new SiTech_Filter();
		$businessid = $filter->input('businessid');
		//$promo_id = intval($_POST['promo_id']);
		$start_date = $filter->input('start_date');
		$_start_hour = $filter->input('start_hour');
		$_start_min = $filter->input('start_min');
		$_start_ampm = $filter->input('start_ampm') == 'am' ? 0 : 12;

		// wtf?!
		if ($_start_hour == 12)
			$_start_hour = 0;

		//$promotion->start_time = ( $_start_hour + $_start_ampm ) . ':' . $_start_min;
		$start_time = ( $_start_hour + $_start_ampm ) . ':' . $_start_min;

		$end_date = $filter->input('end_date');
		//$_end_hour = mysql_real_escape_string($_POST['end_hour']);
		//$_end_min = mysql_real_escape_string($_POST['end_min']);
		$_end_hour = $filter->input('end_hour');
		$_end_min = $filter->input('end_min');
		//$_end_ampm = $_POST['end_ampm'] == 'am' ? 0 : 12;
		$_end_ampm = $filter->input('end_ampm') == 'am' ? 0 : 12;
		if ($_end_hour == 12)
			$_end_hour = 0;
		$end_time = ( $_end_hour + $_end_ampm ) . ':' . $_end_min;
		$monday = isset($_POST['monday']) ? 1 : 0;
		$tuesday = isset($_POST['tuesday']) ? 1 : 0;
		$wednesday = isset($_POST['wednesday']) ? 1 : 0;
		$thursday = isset($_POST['thursday']) ? 1 : 0;
		$friday = isset($_POST['friday']) ? 1 : 0;
		$saturday = isset($_POST['saturday']) ? 1 : 0;
		$sunday = isset($_POST['sunday']) ? 1 : 0;
		$query = "
			UPDATE promotions SET
				start_date = '$start_date',
				start_time = '$start_time',
				end_date = '$end_date',
				end_time = '$end_time',
				monday = $monday,
				tuesday = $tuesday,
				wednesday = $wednesday,
				thursday = $thursday,
				friday = $friday,
				saturday = $saturday,
				sunday = $sunday
			WHERE id = $promo_id";
		Treat_DB_ProxyOld::query($query);

		if (mysql_errno()) {
			$error = "An error occurred: " . mysql_error();
			echo $error;
			return;
		}

		Treat_Model_KioskSync::syncPromotionCache($businessid);
		Treat_Model_KioskSync::addSync($businessid, $promo_id, Treat_Model_KioskSync::TYPE_PROMO);
		echo 1;
		return;
	}

	public function checkUtilizedStatus() {
		$promo_id = (int) $this->_args[0];
		if (!isset($this->_args[0]) || $promo_id <= 0) {
			throw new Treat_Exception("Promotion not specified.");
		}
		echo Treat_Model_Promotion_Businesses::updateUtilizedStatus($promo_id);
	}

	/**
	 * Display the view of the business mapping form
	 * @throws Treat_Exception
	 */
	public function businesses() {
		$promo_id = (int) $this->_args['promo_id'];
		if (!isset($this->_args['promo_id']) || $promo_id <= 0) {
			throw new Treat_Exception("Promotion not specified.");
		}
		try {
			$promotion = \EE\Model\Promotion::get($promo_id, TRUE);
		} catch (Exception $e) {
			throw new Treat_Exception("Promotion not found. (Error: $e)");
		}

		if ($promotion->businessid != NULL) {
			throw new Treat_Exception("Not a global promotion");
		}

		$this->_view->assign('promotion', $promotion);
		$this->_view->assign('enabled', Treat_Model_Promotion_Businesses::getColumn("BusinessId", "PromotionId={$promotion->id} AND `active` <> 0"));
		$this->_view->assign('utilized', Treat_Model_Promotion_Businesses::getColumn("BusinessId", "PromotionId={$promotion->id} AND Utilized <> 0"));
		$this->_view->assign('companies', \EE\Model\Company::getCompanyDistrictsList(true));
	}

	public function reorderPromo( ) {

		$businessid = intval( $_REQUEST['bid'] );
		if ($businessid == NULL) {
			$business_id_compare_sql = "IS NULL";
		} else {
			$business_id_compare_sql = '=' . $businessid;
		}
		$promo_id = intval( $_REQUEST['promo_id'] );
		$dir = intval( $_REQUEST['dir'] );

		$compArray = array(
			1 => '<', 2 => '>'
		);
		$orderArray = array(
			1 => 'DESC', 2 => 'ASC'
		);

		$comp = $compArray[$dir];
		$order = $orderArray[$dir];

		$query = "
			SELECT
				p.id,
				p.rule_order AS rule_order,
				q.rule_order AS this_order
			FROM promotions p
			JOIN promotions q
				ON q.id=$promo_id
				AND p.category=q.category
				AND q.businessid $business_id_compare_sql
			WHERE p.rule_order $comp q.rule_order
			AND p.businessid $business_id_compare_sql
			ORDER BY rule_order $order
			LIMIT 1
		";

		$db = \EE\Model\Promotion::db();
		$result = $db->query($query);
		$swapWith = $result->fetch();

		if( $swapWith && $swapWith['id'] > 0 ) {
			$query = "
				UPDATE promotions SET
					rule_order = %s
				WHERE id = %s
			";

			$ruleOrder = $swapWith['this_order'];

			$query1 = str_replace( array( "\t", "\n", "\r" ), ' ',
					sprintf( $query, $ruleOrder, $swapWith['id'] ) );
			$query2 = str_replace( array( "\t", "\n", "\r" ), ' ',
					sprintf( $query, $swapWith['rule_order'], $promo_id ) );
			$db->query($query1);
			$db->query($query2);

			Treat_Model_KioskSync::syncPromotionCache();
			$promotion = \EE\Model\Promotion::get($promo_id, TRUE);
			$promotion->kioskSyncPromotions();
			$swapwith_promo_id = (int)$swapWith['id'];
			$promotion = \EE\Model\Promotion::get($swapwith_promo_id, TRUE);
			$promotion->kioskSyncPromotions();
		}
		echo 1;
		return;
	}

	/**
	 * Receive and process global promotion business mapping information
	 */
	public function updateBusinesses() {
		$this->_layout = false;
		$promo_id = (int) $_POST['promo_id'];
		header('Content-Type: application/json; charset=utf-8');
		if (!isset($_POST['promo_id']) || $promo_id < 1) {
			echo json_encode(Array(
					'success' => 0,
					'msg' => "promo_id='$promo_id'",
					'error' => "Valid promotion was not specified",
			));
			return;
		}
		try {
			$promotion = \EE\Model\Promotion::get($promo_id, TRUE);
		} catch (Exception $e) {
			echo json_encode(Array(
					'success' => 0,
					'msg' => "promo_id='$promo_id'",
					'error' => "Promotion not found. $e",
			));
			return;
		}

		// don't display for non-global promotions
		if ($promotion->businessid != NULL) {
			echo json_encode(Array(
					'success' => 0,
					'msg' => '',
					'error' => "Invalid promotion. Please select a global promotion $e",
			));
			return;
		}

		// save to the database
		if (is_array($_REQUEST['business'])) {
			$businesses_array = $_REQUEST['business'];
		} else {
			$businesses_array = Array($_REQUEST['business']);
		}
		$result = $promotion->setAppliesTo($businesses_array);
		$status = print_r($result, TRUE);
		echo json_encode(Array(
				'success' => TRUE,
				'msg' => "Promotion $promo_id updated: " . $status,
				'error' => NULL,
		));
		return false;
	}

	/**
	 * Receive and process promotion items information
	 */
	public function updatePromo() {
		$this->_layout = false;
		$debug = (!IN_PRODUCTION) ? TRUE : FALSE;
		$promo_id = (int) $_POST['promo_id'];
		header('Content-Type: application/json; charset=utf-8');
		if (!isset($_POST['promo_id']) || $promo_id < 1) {
			echo json_encode(Array(
					'success' => 0,
					'msg' => "promo_id='$promo_id'",
					'error' => "Valid promotion was not specified",
			));
			return;
		}
		try {
			$promotion = \EE\Model\Promotion::get($promo_id, TRUE);
		} catch (Exception $e) {
			echo json_encode(Array(
					'success' => 0,
					'msg' => "promo_id='$promo_id'",
					'error' => "Promotion not found. $e",
			));
			return;
		}

		$results = '';
		//business mapping
		if ($promotion->isGlobal() && isset($_REQUEST['business']) && is_array($_REQUEST['business'])) {
			$result = $promotion->setAppliesTo($_REQUEST['business']);
			$results .= print_r($result, TRUE);
		}
		// promo items
		if (isset($_REQUEST['item']) && is_array($_REQUEST['item'])) {
			$result = $promotion->setItems($_REQUEST['item']);
			$results .= print_r($result, TRUE);
		}

		if (isset($_REQUEST['promo_group_1'])) {
			$promo_group_1 = htmlspecialchars(trim($_POST['promo_group_1']));
			if (isset($_REQUEST['promo_group_2']) && isset($_REQUEST['promo_group_3'])) {
				$promo_group_2 = htmlspecialchars(trim($_POST['promo_group_2']));
				$promo_group_3 = htmlspecialchars(trim($_POST['promo_group_3']));
			}
			$where = "promo_id = $promo_id"
					. ' AND (type = ' . \EE\Model\Promotion::DETAIL_TYPE_COMBO_GROUP_1
					. ' OR type = ' . \EE\Model\Promotion::DETAIL_TYPE_COMBO_GROUP_2
					. ' OR type = ' . \EE\Model\Promotion::DETAIL_TYPE_COMBO_GROUP_3
					. ')';
		}

		Treat_Model_KioskSync::syncPromotionCache();
		$promotion->kioskSyncAll();
		if (!$debug) {
			unset($results);
		}
		echo json_encode(Array(
				'success' => TRUE,
				'msg' => "Promotion $promo_id updated: " . $results,
				'error' => NULL,
		));
		return;
	}

}
