<?php

class Ta_NutritionController extends Treat_Controller_Abstract
{
	protected $_noLoginRequired = array(
		'nutrition/recipe' => array(
			'ajaxautocom_php' => true,
			'eip' => true,
			'menu_nutrition' => true,
			'menu_nutrition2' => true,
			'printer_friendly' => true,
		),
		'ta/nutrition' => array(
			'company_level' => true,
			'do_company_level_cron' => true,
		),
	);
	
	
	public function company_level() {
		$this->_layout = '';
		
		$businessid = static::getPostOrGetVariable( array( 'bid', 'businessid', 'business_id' ) );
		$districtid = static::getPostOrGetVariable( array( 'did', 'districtid', 'district_id' ) );
		$companyid = static::getPostOrGetVariable( array( 'cid', 'companyid', 'company_id' ) );
		$date = static::getPostOrGetVariable( 'date' );
		$date_start = static::getPostOrGetVariable( 'date_start' );
		$date_end = static::getPostOrGetVariable( 'date_end' );
		if ( false !== ( $date = Treat_Date::getValidTimeStamp( $date ) ) ) {
			$date[ Treat_Model_Nutrition_BusinessLevel::KEY_DATE ] = $date;
		}
		if ( false !== ( $date_start = Treat_Date::getValidTimeStamp( $date_start ) ) ) {
			$date[ Treat_Model_Nutrition_BusinessLevel::KEY_DATE_START ] = $date_start;
		}
		if ( false !== ( $date_end = Treat_Date::getValidTimeStamp( $date_end ) ) ) {
			$date[ Treat_Model_Nutrition_BusinessLevel::KEY_DATE_END ] = $date_end;
		}
		if ( !$date ) {
			$date = time();
		}
		
		$nutrition = Treat_Model_Nutrition_BusinessLevel::getDisplayData( $date, $companyid, $districtid, $businessid
			,Treat_Model_Catapult_TenderType::TENDER_TYPE_GIFT_CARD
		);
		
/*
* /
		if ( is_array( $nutrition ) ) {
			$item = reset( $nutrition );
			echo '<pre>';
			echo '$nutrition is an array(). listing 1st $item = ';
			var_dump( $item );
#			echo '$nutrition = ';
#			var_dump( $nutrition );
			echo '</pre>';
		}
		else {
			echo '<pre>';
			echo '$nutrition = ';
			var_dump( $nutrition );
			echo '</pre>';
		}
/*
*/
		
		$wrapper = null;
		if ( $nutrition ) {
			Treat_Model_Nutrition_BusinessLevel_Abstract::setFunctionality(
				Treat_Model_Nutrition_BusinessLevel_Abstract::FUNCTIONALITY_DISPLAY_DATA
			);
			Treat_Model_Nutrition_BusinessLevel_WeekContainer::setDates(
				$date_start
				,$date_end
			);
			$wrapper = Treat_Model_Nutrition_BusinessLevel_Wrapper::load(
				$data = $nutrition
				,$class = 'Treat_Model_Nutrition_BusinessLevel_WeekContainer'
			);
#			$wrapper = new Treat_Model_Nutrition_BusinessLevel_DateStamp( $nutrition );
			$wrapper->process();
			
			
		}
		
/*
* /
		echo '<pre>';
		echo '$wrapper = ';
		var_dump( $wrapper );
		echo '</pre>';
/*
*/
		
		$this->_view->assign( 'wrapper', $wrapper );
	}
	
	
	public function do_company_level_cron() {
		$this->_layout = '';
		ini_set( 'display_errors', true );
		ini_set( 'error_log', '' );
#		ini_set( 'error_reporting', E_ALL | E_STRICT );
		ini_set( 'error_reporting', E_ALL & ~E_NOTICE & ~E_DEPRECATED );
		ini_set( 'html_errors', false );
		ini_set( 'implicit_flush', true );
		ini_set( 'max_execution_time', 0 );
		ini_set( 'output_buffering', false );
		ini_set( 'track_errors', true );
#		ini_set( 'xdebug.default_enable', 'off' ); # needs to be disabled at the server level, probably with apache's <Location> directive
		
		$businessid = static::getPostOrGetVariable( array( 'bid', 'businessid', 'business_id' ) );
		$districtid = static::getPostOrGetVariable( array( 'did', 'districtid', 'district_id' ) );
		$companyid = static::getPostOrGetVariable( array( 'cid', 'companyid', 'company_id' ) );
		$date = static::getPostOrGetVariable( 'date', strtotime( 'Yesterday' ) );
		
		$nutrition = Treat_Model_PaymentDetail::getBusinessLevelNutrition( $date, $businessid, $districtid, $companyid );
		
		if ( $nutrition ) {
			$types = Treat_Model_Nutrition_TypesObject::getTypes();
			$blah = Treat_Model_Nutrition_TypesObject::getTypeKeyByLabel( 'Potassium' );
			
/*
*/
			if ( is_array( $nutrition ) ) {
				$item = reset( $nutrition );
				echo '<pre>';
				echo '$nutrition is an array(). listing 1st $item = ';
				var_dump( $item );
				echo '</pre>';
			}
			else {
				echo '<pre>';
				echo '$nutrition = ';
				var_dump( $nutrition );
				echo '</pre>';
			}
/*
*/
			
			$wrapper = new Treat_Model_Nutrition_BusinessLevel_Company( $nutrition );
			$wrapper->process();
			
			Treat_Model_Nutrition_BusinessLevel_Wrapper::saveData( $wrapper, $key = null );
			
#			echo '<pre>';
#			echo '$company = ';
#			var_dump( $company );
#			echo '</pre>';
			
			$this->_view->assign( 'wrapper', $wrapper );
		}
	}
	
	
	
}

