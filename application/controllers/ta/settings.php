<?php

class Ta_SettingsController extends Treat_Controller_CompanyAbstract {
	protected $_useJQuery = true;
	
	protected $_methodSecurity = array( );
	
	public function init( ) {
		// redirect legacy urls
		if( $_SERVER['REQUEST_METHOD'] == 'GET' && !stristr( $_SERVER['REQUEST_URI'], '/' . $this->_uri->getController( ) . '/' ) ) {
			$url = $this->_uri->getScheme( ) . '://' . $this->_uri->getHost( ) . '/' . $this->_uri->getController( ) . '/'
					. $this->_uri->getAction( true ) . '?' . $this->_uri->getQueryString( );
			header( 'Location: ' . $url );
			return false;
		}
		
		parent::init( );
	}
	
	
	public function company( ) {
		header( 'Location: ' . $this->_uri->getScheme( ) . '://' . $this->_uri->getHost( ) . '/ta/comsetup.php?bid=' . static::getPageBusinessId( ) . '&cid=' . static::getPageCompanyId( ) );
	}
	
	public function users( ) {
		header( 'Location: ' . $this->_uri->getScheme( ) . '://' . $this->_uri->getHost( ) . '/ta/user_setup.php?bid=' . static::getPageBusinessId( ) . '&cid=' . static::getPageCompanyId( ) );
	}
	
	public function vending( ) {
		header( 'Location: ' . $this->_uri->getScheme( ) . '://' . $this->_uri->getHost( ) . '/ta/user_routes.php?bid=' . static::getPageBusinessId( ) . '&cid=' . static::getPageCompanyId( ) );
	}
	
	public function menus( ) {
		header( 'Location: ' . $this->_uri->getScheme( ) . '://' . $this->_uri->getHost( ) . '/ta/catermenu.php?cid=' . static::getPageCompanyId( ) );
	}
	
	public function groups( ) {
		$user = $_SESSION->getUser( );
		
		if( !( $user->securityid == 1 && preg_match( '/@essential(pos.com|-elements.net)/', $user->email ) ) )
			static::error_exit( 'Access denied' );
		
		$this->addJavascriptAtTop( '/assets/js/ta/settings_groups.js' );
	}
	
	
	/*
	 * Begin ajax functions
	 */
	
	protected function _startAjax( ) {
		// referrer security check
		$refRequest = explode( '?', $_SERVER['HTTP_REFERER'] );
		$refParts = explode( '/', $refRequest[0] );
		
		$_server = 2;
		$_action = count( $refParts ) - 1;
		$controllerParts = array_slice( $refParts, 3, $_action - 3 );
		$controller = implode( '/', $controllerParts );
		
		if( ( $refParts[$_server] != $_SERVER['SERVER_NAME'] || $controller != $this->_uri->getController( )
						|| !method_exists( $this, $refParts[$_action] ) ) ) {
			static::error_exit( 'Access denied' );
		}
		
		// ajax fix for ie
		mb_internal_encoding( "UTF-8" );
		mb_http_output( "UTF-8" );
		ob_start( "mb_output_handler" );
		
		// initialize db connection so mysql_real_escape_string can be used
		$query = 'SELECT 1';
		Treat_DB_ProxyOld::query( $query );
	}
	
	public function getGroups( ) {
		$this->_startAjax( );
		
		$rv = array( );
		
		$groupList = Treat_Model_BusinessGroup::getList( );
		$rv['groups'] = $groupList;
		
		echo json_encode( $rv );
		
		return false;
	}
	
	public function updateGroupDetails( ) {
		$this->_startAjax( );
		
		$group_id = $_REQUEST['group_id'] == 'new' ? 'new' : intval( $_REQUEST['group_id'] );
		$group_name = mysql_real_escape_string( $_REQUEST['group_name'] );
		
		$rv = array( );
		
		try {
			if( $group_id == 'new' ) {
				$group = new Treat_Model_BusinessGroup( );
			} else {
				if( !( $group = Treat_Model_BusinessGroup::getById( $group_id ) ) ) {
					throw new Exception( "Invalid group id" );
				}
			}
			
			$group->group_name = $group_name;
			$group->save( );
			
			$rv['id'] = $group->group_id;
			$rv['message'] = 'Group added successfully';
		} catch( PDOException $e ) {
			$rv['error'] = 'A database error has occurred';
		} catch( Exception $e ) {
			$rv['error'] = $e->getMessage( );
		}
		
		echo json_encode( $rv );
		
		return false;
	}
	
	public function deleteGroup( ) {
		$this->_startAjax( );
		
		$group_id = intval( $_REQUEST['id'] );
		
		$rv = array( );
		
		try {
			if( !( $group = Treat_Model_BusinessGroup::getById( $group_id ) ) ) {
				throw new Exception( "Invalid group id" );
			}
			
			$group->delete( );
			
			$rv['id'] = true;
			$rv['message'] = 'Group deleted successfully';
		} catch( PDOException $e ) {
			$rv['error'] = 'A database error has occurred';
		} catch( Exception $e ) {
			$rv['error'] = $e->getMessage( );
		}
		
		echo json_encode( $rv );
		
		return false;
	}
	
	public function getMembersList( ) {
		$this->_startAjax( );
		
		$group_id = intval( $_REQUEST['id'] );
		
		$rv = array( );
		
		if( !( $group_id > 0 ) ) {
			$rv['error'] = "Invalid group id";
		} else {
			try {
				$busList = Treat_Model_BusinessGroup_Detail::getListByGroupId( $group_id );
				$rv['list'] = $busList;
			} catch( PDOException $e ) {
				$rv['error'] = 'A database error has occurred.';
			} catch( Exception $e ) {
				$rv['error'] = $e->getMessage( );
			}
		}
		
		echo json_encode( $rv );
		
		return false;
	}
	
	public function getBusinessList( ) {
		$this->_startAjax( );
		
		$group_id = intval( $_REQUEST['id'] );
		
		$rv = array( );
		
		if( !( $group_id > 0 ) ) {
			$rv['error'] = "Invalid group id";
		} else {
			try {
				$busList = Treat_Model_BusinessGroup_Detail::getListNotInGroup( $group_id );
				$rv['list'] = $busList;
			} catch( PDOException $e ) {
				$rv['error'] = 'A database error has occurred.';
			} catch( Exception $e ) {
				$rv['error'] = $e->getMessage( );
			}
		}
		
		echo json_encode( $rv );
		
		return false;
	}
	
	public function saveGroupMembers( ) {
		$this->_startAjax( );
		
		$group_id = intval( $_REQUEST['group_id'] );
		$members = $_REQUEST['members'];
		
		$rv = array( );
		
		if( !( $group_id > 0 ) ) {
			$rv['error'] = 'Invalid group id';
		} else {
			try {
				Treat_Model_BusinessGroup_Detail::clearGroup( $group_id );
				foreach( $members as $member ) {
					$detail = new Treat_Model_BusinessGroup_Detail( );
					$detail->group_id = $group_id;
					$detail->businessid = $member;
					$detail->save( );
				}
				
				$rv['success'] = true;
			} catch( PDOException $e ) {
				$rv['error'] = 'A database error has occurred.';
			} catch( Exception $e ) {
				$rv['error'] = $e->getMessage( );
			}
		}
		
		echo json_encode( $rv );
		
		return false;
	}
	
}
