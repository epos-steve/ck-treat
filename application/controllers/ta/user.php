<?php
class Ta_UserController extends Treat_Controller_Abstract
{
	const LOGIN_PAGE          = '/';
	const LOGIN_REDIRECT_PAGE = '/ta/businesstrack.php';
	const LOGIN_SUBMIT_PAGE   = '/ta/user/login';

	protected $_forgot_password_text = 'Forgot Your Password?';
	protected $_forgot_password_link = '/sendpass.htm';


	public function index() {
	}


	public function login( $remote = false, $username = false, $password = false ) {
#		if ( \EE\Model\User\Login::isLoggedIn() ) {
#			// if we're pulling this in via another page we don't want to redirect
#			if ( !$remote ) {
#				\EE\Loader::redirect( static::LOGIN_REDIRECT_PAGE );
#			}
#			return false;
#		}

		$this->_view->assign( 'form_action', \EE\Loader::createURI( Ta_UserController::LOGIN_SUBMIT_PAGE ) );

		if ( $_POST || ( $username && $password ) ) {
			$page = '';
			if ( !( $username && $password ) ) {
				$username = static::getPostVariable( array(
					\EE\Model\User\Login::FORM_USERNAME,
					'username',
					'user',
					'usercook',
				) );
				$password = static::getPostVariable( array(
					\EE\Model\User\Login::FORM_PASSWORD,
					'password',
					'pass',
					'passcook',
				) );
				$page = static::getPostOrGetVariable( 'page' );
				if ( $page ) {
					$uri = new \EE\Uri( $page );
					$page = $uri->getPath() . '?' . $uri->getQueryString();
				}
			}
			if ( \EE\Model\User\Login::login( $username, $password, true ) ) {
/*
				if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
					echo '<pre>';
					echo '$username = "', $username, '"', PHP_EOL;
					echo '$password = "', $password, '"', PHP_EOL;
##					echo '$_COOKIE = ';
##					var_dump( $_COOKIE );
					echo '$_SESSION = ';
					var_dump( $_SESSION );
					echo '<pre>', PHP_EOL;
				}
*/
				if ( $page ) {
					$location = $page;
				}
				else {
					$location = static::LOGIN_REDIRECT_PAGE;
				}
			}
			else {
				if ( isset( $_SESSION['flashmsg'] ) ) {
					unset( $_SESSION['flashmsg'] );
				}
				$_SESSION['flasherr'] = 'Login Failed. Please try again. '
					. html::link_to( $this->_forgot_password_text, $this->_forgot_password_link )
				;
				if ( $page ) {
					$location = static::LOGIN_PAGE . '?page=' . \urlencode( $page );
				}
				else {
					$location = static::LOGIN_PAGE;
				}
			}
#			if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
				printf( '<a href="%1$s">%1$s</a>', $location );
#			}
			\EE\Loader::redirect( $location );
			return false;
		}


	}


	public function logout() {
		\EE\Model\User\Login::logout( $kill_session = true );
	}


	public function security() {

	}


	public function security_save() {
		$logged_in_user = $_SESSION->getUser();
		if ( $logged_in_user->security_level < 8 ) {
			$location = '/ta/businesstrack.php';
			header( 'Location: ' . $location );
			exit();
			return false;
		}
		$cid = static::getPostOrGetVariable( 'cid', 0 );
		$bid = static::getPostOrGetVariable( 'bid', null );
		$is_custom_security = static::getPostOrGetVariable( 'is_custom_security' );

#		echo '<pre>';
#		var_dump( $_POST );
#		echo '</pre>';
		if ( $_POST ) {
			# 1 is if savesecurity.php should edit an existing record
			# 2 is if savesecurity.php should delete an existing record
			# otherwise it should insert a new record
			$edit = static::getPostOrGetVariable( 'edit' );
			# security_id
			$editsecurity = static::getPostOrGetVariable( 'editsecurity' );
			$security_name = static::getPostOrGetVariable( 'security_name' );

			if ( $editsecurity ) {
				$sec = Treat_Model_User_Security::get( intval( $editsecurity ), true );
				if ( !$sec ) {
					$sec = new Treat_Model_User_Security();
				}
#				echo '<pre>';
#				var_dump( $sec );
#				echo '</pre>';
				$security_items = Treat_Model_User_Security::getSecurityFormMap();
				unset( $security_items['user'] ); # using edituser instead
				unset( $security_items['nutrition'] ); # using sec_nutrition instead

				$sec->security_name = $security_name;
				foreach ( $security_items as $form_key => $column_name ) {
					if ( array_key_exists( $form_key, $_POST ) ) {
						if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
							echo sprintf(
								'<div>$form_key {%s} does exist with value of {%s}, setting {%s} = {%2$s}</div>'
								,$form_key
								,$_POST[ $form_key ]
								,$column_name
							);
						}
						$sec->{$column_name} = $_POST[ $form_key ];
					}
					else {
						if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
							echo sprintf(
								'<div>$form_key {%s} does *not* exist</div>'
								,$form_key
							);
						}
						$sec->{$column_name} = 0;
					}
				}
				$sec->save();

				if ( array_key_exists( 'dept', $_POST ) ) {
					$depts = $_POST['dept'];
				}
				else {
					$depts = array();
				}
				Treat_Model_User_Security_Department::updateSecurityGroupDepartments( $editsecurity, $depts );

			}

		}

		$qry = array(
			'cid' => $cid,
			'bid' => $bid,
			'editsecurity' => null,
		);
		if ( !$is_custom_security ) {
			if ( $editsecurity ) {
				$qry['editsecurity'] = $editsecurity;
			}
		}
		$location = '/ta/user_setup.php?';
		if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
			$location .= http_build_query( $qry, '', '&amp;' );
			echo sprintf(
				'<a href="%1$s">%1$s</a>'
				,$location
			);
		}
		else {
			$location .= http_build_query( $qry, '', '&' );
			header( 'Location: ' . $location );
		}

		return false;
	}



}
