<?php

use Essential\Treat\Db\ProxyOld\ProcessHost;

class Ta_OrderitemsdailyController
	extends \EE\Controller\Base
{

	public function index( $groupId = null, $date = null )
	{
		$this->_layout = '';
		$this->_action = 'index';
		
		$date = \EE\Date::getValidTimeStamp( $date, true );
		$date = new \EE\Date\Time( $date );
		$query = '
				SELECT min.item_code
					, min.name
					, SUM( cd.quantity ) AS sold
					, AVG(cd.price) AS price
					, SUM( cd.quantity * cd.price ) AS sales
				FROM posreports_checkdetail cd
					JOIN posreports_checks c
						ON c.businessid = cd.businessid
						AND c.check_number = cd.bill_number
						AND DATE( c.bill_datetime ) =  :date
					JOIN posreports_menu_items_new min
						ON min.pos_id = cd.item_number
						AND min.businessid = cd.businessid
						AND min.group_id = :groupId
				GROUP BY min.id
				ORDER BY min.name
		';
		$params = array(
			'date' => $date->format( 'Y-m-d' ),
			'groupId' => intval( $groupId ),
		);
		$db = \EE\Model\AbstractModel\ProcessHost::db();
		$stmt = $db->prepare( $query );
		
		$items = array();
		if ( $stmt->execute( $params ) ) {
			while ( $r = $stmt->fetch() ) {
				$items[] = $r;
			}
		}
		$this->_view->assign( 'date', $date );
		$this->_view->assign( 'items', $items );
	}

	public function accent()
	{
		$this->index( 12701, static::getPostOrGetVariable( 'date' ) );
	}

	public function atlanta_att_glenridge()
	{
		$this->index( 14884, static::getPostOrGetVariable( 'date' ) );
	}

	public function atlanta_att_lenox()
	{
		$this->index( 14885, static::getPostOrGetVariable( 'date' ) );
	}

	public function jj_juniper()
	{
		$this->index( 14969, static::getPostOrGetVariable( 'date' ) );
	}

	public function jj_palo_alto()
	{
		$this->index( 15073, static::getPostOrGetVariable( 'date' ) );
	}

	public function jj_sandisk_1()
	{
		$this->index( 14970, static::getPostOrGetVariable( 'date' ) );
	}

	public function jj_sandisk_2()
	{
		$this->index( 14971, static::getPostOrGetVariable( 'date' ) );
	}

	public function jj_sandisk_4()
	{
		$this->index( 14972, static::getPostOrGetVariable( 'date' ) );
	}

	public function jj_sandisk_5()
	{
		$this->index( 14973, static::getPostOrGetVariable( 'date' ) );
	}

	public function lam_labs()
	{
		$this->index( 15728, static::getPostOrGetVariable( 'date' ) );
	}

	public function tv_sandisk_1()
	{
		$this->index( 15729, static::getPostOrGetVariable( 'date' ) );
	}

	public function tv_sandisk_2()
	{
		$this->index( 15730, static::getPostOrGetVariable( 'date' ) );
	}

	public function tv_sandisk_4()
	{
		$this->index( 15731, static::getPostOrGetVariable( 'date' ) );
	}

	public function tv_sandisk_5()
	{
		$this->index( 15732, static::getPostOrGetVariable( 'date' ) );
	}

	public function blizzard_bldg5()
	{
		$this->index( 15769, static::getPostOrGetVariable( 'date' ) );
	}

}
