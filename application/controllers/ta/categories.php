<?php

class Ta_CategoriesController
	extends \EE\Controller\CompanyAbstract
{
	protected $_layout = 'promotions.tpl';
	
	public function index() {
		error_reporting(E_ALL);
		ini_set('display_errors', false);
		
		$this->_view->assign('uri',$this->_uri->getController());
		
		$this->addJavascriptAtTop('/assets/js/jquery-1.5.1.min.js');
		$this->addJavascriptAtTop('/assets/js/jquery-ui-1.8.11.custom.min.js');
		$this->addJavascriptAtTop('/assets/js/ta/promo_settings.js');
		$this->addCss( '/assets/css/preload/preLoadingMessage.css');		
		$this->addCss('/assets/css/jq_themes/redmond/jqueryui-current.css');
		$this->addCss('/assets/css/jq_themes/redmond/jqueryui-custom.css');
		
		$html = static::listCategories(0, 0);
		$this->_view->assign('html', $html);
	}
	
	public static function listCategories($parent, $edit)
	{
		$query = "SELECT * FROM category WHERE parent = :parent ORDER BY category";
		$params = array(
			'parent' => $parent,
		);
		$db = \EE\Model\AbstractModel\Treat::db();
		$stmt = $db->prepare( $query );
		
		$html = '';
		if ( $stmt->execute( $params ) ) {
			$query2  = "SELECT COUNT(id) AS children FROM category WHERE parent = :parent";
			$countChildrenStmt = $db->prepare( $query2 );
			while ( $category = $stmt->fetch() ) {
			
				////has children?
				$params2 = array(
					'parent' => $category[ "id" ],
				);
				$children = 0;
				if ( $countChildrenStmt->execute( $params2 ) ) {
					$childrenRow = $countChildrenStmt->fetchObject();
					if ( $childrenRow ) {
						$children = $childrenRow->children;
					}
				}
			
				if ( $children == 0 && $edit > 0 ) {
					$html .= sprintf(
						'<ul><input type="checkbox" id="cat%1$d" name="cat[%1$d]" value="1">%2$s</ul>'
						,$category[ "id" ]
						,$category[ "category" ]
					);
				}
				else {
					$html .= '<ul><li>' . $category[ "category" ];
					$html .= static::listCategories( $category[ "id" ], $edit );
					$html .= '</ul>';
				}
			}
		}
		
		return $html;
	}
}
