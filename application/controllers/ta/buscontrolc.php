<?php
require_once( dirname( __FILE__ ) . '/buscontrol.php' );

// DO NOT EXTEND THIS CLASS! If you need to do anything with this class, use
// the parent class instead... This is just a wrapper until we get buscontrol.php
// moved out of the /ta/ directory - otherwise apache rewrites won't work.
class Ta_BuscontrolcController extends Ta_BuscontrolController
{
	
	
	public function __construct( SiTech_Uri $uri ) {
		$uri->setController( 'ta/buscontrol' );
		parent::__construct( $uri );
	}
	
	
}
