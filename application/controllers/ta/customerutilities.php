<?php

/* Scott 4-24-12 */
class Ta_CustomerutilitiesController extends \EE\Controller\CompanyAbstract
{
	protected $_layout = 'customer_utilities.tpl';
	
	protected $_noLoginRequired = array(
		'ta/customerutilities' => array(
			'index' => true,
			'customerSettingsCron' => true,
			'customerSettingsValidateCron' => true,
		),
	);
	
	public function index() {
		$user = $_SESSION->getUser();
		$this->_view->assign( 'title', 'Customer Utilities' );
		//$this->_view->assign( 'companyId', $user->getCompanyId() );
		$this->_view->assign('uri',$this->_uri->getAction());
		
		$this->addJavascriptAtTop('/assets/js/jquery-1.5.1.min.js');
		$this->addJavascriptAtTop('/assets/js/jquery-ui-1.8.11.custom.min.js');
		$this->addCss('/assets/css/jq_themes/redmond/jqueryui-current.css');
		$this->addCss('/assets/css/jq_themes/redmond/jqueryui-custom.css');
	}

	public function accountsOutOfBalance(){

		$download_data = "";
		$accounts_oob_1 = 0;
		$accounts_oob_2 = 0;
		$accounts_oob_5 = 0;
		$accounts_oob_10 = 0;
		$accounts_oob_20 = 0;
		$accounts_oob_20_plus = 0;
		
		$get_oob_customers = array();
	
		$user = $_SESSION->getUser();
		$companyId = $user->getCompanyId();
		$currentBid = @$_GET["bid"];
		//$currentBid = 422;
	
		$this->addJavascriptAtTop('/assets/js/jquery-1.5.1.min.js');
		$this->addJavascriptAtTop('/assets/js/jquery-ui-1.8.11.custom.min.js');
		$this->addCss('/assets/css/jq_themes/redmond/jqueryui-current.css');
		//$this->addCss('/assets/css/jq_themes/redmond/jqueryui-custom.css');
		$this->addCss('/assets/css/ta/customer-utilities.css');
		
		$get_oob_customer_records = \EE\Model\User\Customer\OutOfBalance::getOobCustomersByBusinessid($currentBid);
		$this->_view->assign('oobCustomers',$get_oob_customer_records);
		
		foreach ($get_oob_customer_records as $value){
		
			$oob_amount = $value['oob_amount'];
			
			if (($oob_amount > 20) || ($oob_amount < -20)) {
				$accounts_oob_20_plus++;
				//$fix_customers_oob_20_plus[] = array($customerid => $calculated_balance);
			} else if (($oob_amount > 10) || ($oob_amount < -10)) {
				$accounts_oob_20++;
				//$fix_customers_oob_20[] = array($customerid => $calculated_balance);
			} else if (($oob_amount > 5) || ($oob_amount < -5)) {
				$accounts_oob_10++;
				//$fix_customers_oob_10[] = array($customerid => $calculated_balance);
			} else if (($oob_amount > 2) || ($oob_amount < -2)) {
				$accounts_oob_5++;
				//$fix_customers_oob_5[] = array($customerid => $calculated_balance);
			} else if (($oob_amount > 1) || ($oob_amount < -1)) {
				$accounts_oob_2++;
				//$fix_customers_oob_2[] = array($customerid => $calculated_balance);
			} else if (($oob_amount > 0) || ($oob_amount < 0)) {
				$accounts_oob_1++;
				//$fix_customers_oob_1[] = array($customerid => $calculated_balance);
			}

		}
		
		$customer_count = \EE\Controller\Base::getGetVariable('customers');
		$oob_customer_count = \EE\Controller\Base::getGetVariable('oob');
		$in_balance_customer_count = \EE\Controller\Base::getGetVariable('inbalance');
		
		$account_balance_summary_data = array(
											"bid_value" => $currentBid, 
											"customer_count" => $customer_count, 
											"oob_customer_count" => $oob_customer_count, 
											"in_balance_customer_count" => $in_balance_customer_count, 
											"accounts_oob_1" => $accounts_oob_1, 
											"accounts_oob_2" => $accounts_oob_2, 
											"accounts_oob_5" => $accounts_oob_5, 
											"accounts_oob_10" => $accounts_oob_10,
											"accounts_oob_20" => $accounts_oob_20,
											"accounts_oob_20_plus" => $accounts_oob_20_plus,
										);
										
		$this->_view->assign('accountBalanceSummary',$account_balance_summary_data);
		
	}
	
	public function zeroCloseAccounts(){

		$this->addJavascriptAtTop('/assets/js/jquery-1.5.1.min.js');
		$this->addJavascriptAtTop('/assets/js/jquery-ui-1.8.11.custom.min.js');
		$this->addCss('/assets/css/jq_themes/redmond/jqueryui-current.css');
		//$this->addCss('/assets/css/jq_themes/redmond/jqueryui-custom.css');
		$this->addCss('/assets/css/ta/customer-utilities.css');
	
		$activeCount = "";
		$inactiveCount = "";
		$totalBalance = "";
		$last_used = "";
		$adjust_balance = "";
		
		$download_data = "";
		$download_data .= "Active List\r\n";
		$download_data .= "customerid,balance,scancode,first_name,last_name,email,status,last_used,calculated_balance,customer_oob\r\n";
		
		$companyid = 0;
		$businessid = 0;
		$date_time = "";
		$user_id = 0;
		$batch_number = "";
		$closed_active_accounts = 0;
		$ck_adjusted_active = 0;
		$closed_inactive_accounts = 0;
		$ck_adjusted_inactive = 0;
		
		$active_customer_list = array();
		$inactive_customer_list = array();
		$closed_account_data = array();

		$companyid = "";
		$businessid = "";
		$userid = "";
		$username = "";		
		
		if (isset($_SESSION['active_customer_list'])){ 
			$active_customer_list = $_SESSION['active_customer_list'];
			unset($_SESSION['active_customer_list']);
		}
		
		if (isset($_SESSION['inactive_customer_list'])){ 
			$inactive_customer_list = $_SESSION['inactive_customer_list'];
			unset($_SESSION['inactive_customer_list']);
		}
		
		if (isset($_SESSION['closed_account_data'])){ 
			$closed_account_data = $_SESSION['closed_account_data'];
			$companyid = $closed_account_data['companyid'];
			$businessid = $closed_account_data['businessid'];
			$userid = $closed_account_data['userid'];
			$username = $closed_account_data['username'];
			unset($_SESSION['closed_account_data']);
		}

		$batch_number = date("ymdGis") . $businessid;
		
		$this->_view->assign('activeCount',$activeCount);
		$this->_view->assign('inactiveCount',$inactiveCount);
		//$this->_view->assign('activeTotalBalance',$ck_adjusted_active);
		
		$this->_view->assign('active_customer_list',$active_customer_list);
		foreach ($active_customer_list as $value){

			$closed_active_accounts++;

			$customerid = $value['customerid'];
			$balance = $value['balance'];
			$scancode = $value['scancode'];
			$first_name = $value['first_name'];
			$last_name = $value['last_name'];
			$email = $value['email'];
			$customer_status = $value['customer_status'];
			$last_used = $value['last_used'];
			$calculated_balance = $value['calculated_balance'];
			$customer_oob = $value['customer_oob'];

			$download_data .= "$customerid,$balance,$scancode,$first_name,$last_name,$email,$customer_status,$last_used,$calculated_balance,$customer_oob\r\n";

			$update_active_customer = new \EE\Model\User\Customer();
			$update_active_customer->customerid = $customerid;
			$update_active_customer->businessid = $businessid;
			$update_active_customer->inactive = '1';
			$update_active_customer->single_use = '1';
			$update_active_customer->is_deleted = '1';
			$update_active_customer->customid = $batch_number;
			$update_active_customer->balance = '0';
			$update_active_customer->username = $email . "-closed";
			$update_active_customer->first_name = $first_name;
			$update_active_customer->last_name = $last_name;
			$update_active_customer->save();			

			if ($balance != 0){
				$ck_adjusted_active += $balance;
				$adjust_balance = $balance * -1;
				
				$ckadjust_inactive_customer = new \EE\Model\User\Customer\Transaction();
				$ckadjust_inactive_customer->oid = 'CK Adjust';
				$ckadjust_inactive_customer->trans_type_id = \EE\Model\User\Customer\Transaction::TRANSACTION_TYPE_CK_ADJUST;
				$ckadjust_inactive_customer->customer_id = $customerid;
				$ckadjust_inactive_customer->subtotal = $adjust_balance;
				$ckadjust_inactive_customer->chargetotal = $adjust_balance;
				$ckadjust_inactive_customer->date_created = date('Y-m-d H:i:s');
				$ckadjust_inactive_customer->response = 'APPROVED';
				$ckadjust_inactive_customer->authresponse = $username;
				
				$ckadjust_inactive_customer->save();				

			}
		}

		$this->_view->assign('activeTotalBalance',$ck_adjusted_active);

		$download_data .= "\r\n";
		$download_data .= "Inactive List\r\n";
		$download_data .= "customerid,balance,scancode,first_name,last_name,email,status,last_used,calculated_balance,customer_oob\r\n";

		$this->_view->assign('inactive_customer_list',$inactive_customer_list);
		foreach ($inactive_customer_list as $value){
			
			$closed_inactive_accounts++;

			$customerid = $value['customerid'];
			$balance = $value['balance'];
			$scancode = $value['scancode'];
			$first_name = $value['first_name'];
			$last_name = $value['last_name'];
			$email = $value['email'];
			$customer_status = $value['customer_status'];
			$last_used = $value['last_used'];
			$calculated_balance = $value['calculated_balance'];
			$customer_oob = $value['customer_oob'];
			
			$download_data .= "$customerid,$balance,$scancode,$first_name,$last_name,$email,$customer_status,$last_used,$calculated_balance,$customer_oob\r\n";

			$update_inactive_customer = new \EE\Model\User\Customer();
			$update_inactive_customer->customerid = $customerid;
			$update_inactive_customer->businessid = $businessid;
			$update_inactive_customer->inactive = '1';
			$update_inactive_customer->single_use = '1';
			$update_inactive_customer->is_deleted = '1';
			$update_inactive_customer->customid = $batch_number;
			$update_inactive_customer->balance = '0';
			$update_inactive_customer->username = $email . "-closed";
			$update_inactive_customer->first_name = $first_name;
			$update_inactive_customer->last_name = $last_name;
			$update_inactive_customer->save();
			
			if ($balance != 0){
				$ck_adjusted_inactive += $balance;
				$adjust_balance = $balance*-1;
				$ckadjust_inactive_customer = new \EE\Model\User\Customer\Transaction();
				$ckadjust_inactive_customer->oid = 'CK Adjust';
				$ckadjust_inactive_customer->trans_type_id = \EE\Model\User\Customer\Transaction::TRANSACTION_TYPE_CK_ADJUST;
				$ckadjust_inactive_customer->customer_id = $customerid;
				$ckadjust_inactive_customer->subtotal = $adjust_balance;
				$ckadjust_inactive_customer->chargetotal = $adjust_balance;
				$ckadjust_inactive_customer->date_created = date('Y-m-d H:i:s');
				$ckadjust_inactive_customer->response = 'APPROVED';
				$ckadjust_inactive_customer->authresponse = $username;
				
				$ckadjust_inactive_customer->save();
			}
		}
		
		$this->_view->assign('inactiveTotalBalance',$ck_adjusted_inactive);
		
		$audit_close_accounts = new \EE\Model\Audit\Customer\Close();
		$audit_close_accounts->companyid = $companyid;
		$audit_close_accounts->businessid = $businessid;
		$audit_close_accounts->date_time = date('Y-m-d H:i:s');
		$audit_close_accounts->userid = $userid;
		$audit_close_accounts->username = $username;
		$audit_close_accounts->batch_number = $batch_number;
		$audit_close_accounts->closed_active_accounts = $closed_active_accounts;
		$audit_close_accounts->ck_adjusted_active = $ck_adjusted_active;
		$audit_close_accounts->closed_inactive_accounts = $closed_inactive_accounts;
		$audit_close_accounts->ck_adjusted_inactive = $ck_adjusted_inactive;
		$audit_close_accounts->save();
		
		$audit_close_scancodes = \EE\Model\User\Customer\Scancode::closeScancodes($businessid);
		
		$this->_view->assign('download_data',$download_data);

	}
	
	public function closeAccounts(){

		$download_data = "";
	
		$user = $_SESSION->getUser();
		$companyId = $user->getCompanyId();
		$currentBid = @$_POST["businessid"];

		$closed_account_data = array("companyid" => $companyId, "businessid" => $currentBid, "userid" => $user->loginid, "username" => $user->username);
		
		$show = 0;

		if ( $user->customer_utility == 0 ) {
			header('Location: /ta/businesstrack.php');
			exit;
		}
		
		$this->_view->assign('uri',$this->_uri->getAction());
		$this->_view->assign('cid',$companyId);
		$this->addJavascriptAtTop('/assets/js/jquery-1.5.1.min.js');
		$this->addJavascriptAtTop('/assets/js/jquery-ui-1.8.11.custom.min.js');
		$this->addCss('/assets/css/jq_themes/redmond/jqueryui-current.css');
		$this->addCss('/assets/css/jq_themes/redmond/jqueryui-custom.css');
		$this->addCss('/assets/css/ta/customer-utilities.css');
		
		$lastcid = 0;
		$myform = "<center><table width=95% cellspacing=0 cellpadding=0 style=\"border:2px solid #999999;\" bgcolor=#999999><tr><td>";
		$myform .= "<form action=closeAccounts?cid=$companyId method=post style=\"margin:0;padding:0;display:inline;\"><select name=businessid><option value=0>Please Choose...</option>";
		
		$query = "SELECT business.businessid,business.businessname,company.companyid,company.companyname
				FROM business
				JOIN company ON company.companyid = business.companyid
				WHERE business.categoryid = 4
				ORDER BY company.companyname,business.businessname";
		$result = Treat_DB_ProxyOld::query($query);
		
		while($r = mysql_fetch_array($result)){
			$businessid = $r["businessid"];
			$businessname = $r["businessname"];
			$companyid = $r["companyid"];
			$companyname = $r["companyname"];
			
			if($currentBid == $businessid){
				$sel= "SELECTED";
			}
			else{
				$sel = "";
			}
			
			if($companyid != $lastcid){
				if($lastcid != 0){
					$myform .= "</optgroup>";
				}
				$myform .= "<optgroup label=\"$companyname\">";
			}
			
			$myform .= "<option value=$businessid $sel>$businessname</option>";
			
			$lastcid = $companyid;
		}
		$myform .= "</optgroup>";
		$myform .= "</select><input type=submit value='GO'></form></td></tr></table><p></center>";
		
		$this->_view->assign('kView',$myform);
		
		////display report
		if($currentBid > 0){
			///card ranges

			$audit_record_table = \EE\Model\Audit\Customer\Close::getByBusinessId($currentBid);
			$this->_view->assign('audit_record_table',$audit_record_table);
			$cardTable = "";

			$cardTable .= "<center><table width=95% cellspacing=0 cellpadding=0 style=\"border:1px solid #999999;font-size:12px;\" bgcolor=white>";
			$cardTable .= "<tr bgcolor=#999999><td colspan=5><font color=white>Card Ranges</font></td><tr>";
			$download_data .= "Card Ranges\r\n";
		
			$query = "SELECT * FROM customer_scancodes WHERE businessid IN ($currentBid, -$currentBid)";
			$result = Treat_DB_ProxyOld::query($query);
			
			
			while($r = mysql_fetch_array($result)){
				$id = $r["id"];
				$businessid = $r["businessid"];
				$scan_start = $r["scan_start"];
				$scan_end = $r["scan_end"];
				$value = $r["value"];
				
				$value = number_format($value, 2);
				
				if($businessid > 0){$active = "<font color=green>ACTIVE</font>"; $active_status = "ACTIVE";}
				else{$active = "<font color=red>INACTIVE</font>"; $active_status = "INACTIVE";}
				
				$cardTable .= "<tr>
								<td style=\"border:1px solid #999999;\">$id</td>
								<td style=\"border:1px solid #999999;\">$scan_start</td>
								<td style=\"border:1px solid #999999;\">$scan_end</td>
								<td style=\"border:1px solid #999999;\">$$value</td>
								<td style=\"border:1px solid #999999;\">$active</td>
							</tr>";
				$download_data .= "$id,$scan_start,$scan_end,$$value,$active_status\r\n";
			}
			$download_data .= "\r\n";
			$cardTable .= "</table></center>";
			
			$this->_view->assign('cardView',$cardTable);
		
			///active customers
			$activeTable = "<center><table width=95% cellspacing=0 cellpadding=0 style=\"border:1px solid #999999;font-size:12px;\" bgcolor=white><tr bgcolor=#999999><td colspan=9><font color=white>Active Customers</font></td><tr>";
			$activeCount = 0;
			$download_data .= "Active Customers\r\n";
			$query = "SELECT * FROM customer WHERE businessid = $currentBid AND inactive = 0 AND is_deleted = '0' ORDER BY scancode";
			$result = Treat_DB_ProxyOld::query($query);
			$active_customer_list = array();
			$totalBalance = 0;
			while($r = mysql_fetch_array($result)){
				$customerid = $r["customerid"];				
				$scancode = $r["scancode"];
				$first_name = $r["first_name"];
				$last_name = $r["last_name"];
				$balance = $r["balance"];
				$email = str_replace('-closed', '', $r["username"]);
				$get_customer_calculated_balance_query = "select get_customer_calculated_balance($customerid, '$scancode', now()) AS calculated_balance";
				$calculated_balance_result = Treat_DB_ProxyOldProcessHost::query( $get_customer_calculated_balance_query );
				$calculated_balance = mysql_result($calculated_balance_result,0,"calculated_balance");
				
				if($balance != $calculated_balance){
					$customer_oob = "Out of balance";
				} else {
					$customer_oob = "";
				}
				
				$last_used = "";
				
				
				if(substr($scancode, 0, 3) == '780' || substr($scancode, 0, 3) == '770' || substr($scancode, 0, 3) == '790'){

					$query2 = "SELECT max(bill_posted) bill_posted from checks where account_number = $scancode";
					$result2 = Treat_DB_ProxyOldProcessHost::query( $query2 );
					$last_used = @mysql_result($result2,0,"bill_posted");
					
					$totalBalance += $balance;
				}
				else{
					$last_used = '';
				}
				
				$active_customer_list[] = array("customerid" => $customerid, "balance" => $balance, "scancode" => $scancode, "first_name" => $first_name, "last_name" => $last_name, "email" =>$email, "customer_status" => "Active", "last_used" => $last_used, "calculated_balance" => $calculated_balance, "customer_oob" => $customer_oob );
				$balance = number_format($balance,2);
				
				$activeTable .= "<tr>
							<td style=\"border:1px solid #999999;\">$scancode</td>
							<td style=\"border:1px solid #999999;\">$last_name</td>
							<td style=\"border:1px solid #999999;\">$first_name</td>
							<td style=\"border:1px solid #999999;\">$email</td>
							<td style=\"border:1px solid #999999;\"><font color=green>Active</font></td>
							<td style=\"border:1px solid #999999;\">$last_used</td>
							<td style=\"border:1px solid #999999;\" align=right>$$balance</td>
							<td style=\"border:1px solid #999999;\" align=right>$$calculated_balance</td>
							<td style=\"border:1px solid #999999;\" align=center>$customer_oob</td>
						</tr>";
				$download_data .= "$scancode,$last_name,$first_name,$email,Active,$last_used,$balance,$calculated_balance,$customer_oob\r\n";
				$activeCount++;
			}
			$download_data .= "\r\n";
			$totalBalance = number_format($totalBalance, 2);
			$activeTable .= "<tr bgcolor=#999999>
							<td style=\"border:1px solid #999999;\" colspan=8><font color=white>$activeCount Active Customers</font></td>
							<td style=\"border:1px solid #999999;\" align=right><font color=white>$$totalBalance</font></td>
						</tr></table>";
		
			$this->_view->assign('cView',$activeTable);
			
			
			///inactive customers
			$inactiveTable = "<center><table width=95% cellspacing=0 cellpadding=0 style=\"border:1px solid #999999;font-size:12px;\" bgcolor=white><tr bgcolor=#999999><td colspan=9><font color=white>Inactive Customers</font></td><tr>";
			$inactiveCount = 0;
			$download_data .= "Inactive Customers\r\n";
			$query = "SELECT * FROM customer WHERE businessid = $currentBid AND (inactive = 1 OR is_deleted = '1') ORDER BY scancode";
			$result = Treat_DB_ProxyOld::query($query);
			$totalBalance = 0;
			$calculated_balance = 0;
			$inactive_customer_list = array();
			while($r = mysql_fetch_array($result)){
				$customerid = $r["customerid"];
				$scancode = $r["scancode"];
				$first_name = $r["first_name"];
				$last_name = $r["last_name"];
				$balance = $r["balance"];
				$email = str_replace('-closed', '', $r["username"]);
				$get_customer_calculated_balance_query = "select get_customer_calculated_balance($customerid, '$scancode', now()) AS calculated_balance";

				$calculated_balance_result = Treat_DB_ProxyOldProcessHost::query( $get_customer_calculated_balance_query );
				$calculated_balance = mysql_result($calculated_balance_result,0,"calculated_balance");
				
				if($balance != $calculated_balance){
					$customer_oob = "Out of balance";
				} else {
					$customer_oob = "";
				}

				$last_used = "";
				
				if(substr($scancode, 0, 3) == '780' || substr($scancode, 0, 3) == '770' || substr($scancode, 0, 3) == '790'){

					$query2 = "SELECT max(bill_posted) bill_posted from checks where account_number = $scancode";
					$result2 = Treat_DB_ProxyOldProcessHost::query( $query2 );
					$last_used = @mysql_result($result2,0,"bill_posted");
					
					$totalBalance += $balance;
				}
				else{
					$last_used = '';
				}
				$inactive_customer_list[] = array("customerid" => $customerid, "balance" => $balance, "scancode" => $scancode, "first_name" => $first_name, "last_name" => $last_name, "email" =>$email, "customer_status" => "Inactive", "last_used" => $last_used, "calculated_balance" => $calculated_balance, "customer_oob" => $customer_oob );
				$balance = number_format($balance,2);
				
				$inactiveTable .= "<tr>
							<td style=\"border:1px solid #999999;\">$scancode</td>
							<td style=\"border:1px solid #999999;\">$last_name</td>
							<td style=\"border:1px solid #999999;\">$first_name</td>
							<td style=\"border:1px solid #999999;\">$email</td>
							<td style=\"border:1px solid #999999;\"><font color=red>Inactive</font></td>
							<td style=\"border:1px solid #999999;\">$last_used</td>
							<td style=\"border:1px solid #999999;\" align=right>$$balance</td>
							<td style=\"border:1px solid #999999;\">$$calculated_balance</td>
							<td style=\"border:1px solid #999999;\">$customer_oob</td>
						</tr>";
				$download_data .= "$scancode,$last_name,$first_name,$email,Inactive,$last_used,$balance,$calculated_balance,$customer_oob\r\n";
				$inactiveCount++;
			}
			$download_data .= "\r\n";
			$totalBalance = number_format($totalBalance, 2);
			$inactiveTable .= "<tr bgcolor=#999999>
							<td style=\"border:1px solid #999999;\" colspan=8><font color=white>$inactiveCount Inactive Customers</font></td>
							<td style=\"border:1px solid #999999;\" align=right><font color=white>$$totalBalance</font></td>
						</tr></table>";
						
			$download_data .= "\r\n";

			$this->_view->assign('dView',$inactiveTable);
			$this->_view->assign('download_data',$download_data);

			$_SESSION['inactive_customer_list'] = $inactive_customer_list;
			$_SESSION['active_customer_list'] = $active_customer_list;
			$_SESSION['closed_account_data'] = $closed_account_data;
		}
	}
	
	public function adjustments() {
		ini_set('display_errors', false);
		
		$user = $_SESSION->getUser();
		$companyId = $user->getCompanyId();
		
		$show = 0;

		if ( $user->customer_utility == 0 ) {
			header('Location: /ta/businesstrack.php');
			exit;
		}

		$this->_view->assign('uri',$this->_uri->getAction());

		$this->addCss('/assets/css/jq_themes/redmond/jqueryui-current.css');
		$this->addCss('/assets/css/jq_themes/redmond/jqueryui-custom.css');
		$this->addJavascriptAtTop('/assets/js/jquery-1.5.1.min.js');
		$this->addJavascriptAtTop('/assets/js/jquery-ui-1.8.11.custom.min.js');
		
		$date1 = static::getPostOrGetVariable( 'date1' );
		$date2 = static::getPostOrGetVariable( 'date2' );
		$currentCompany = static::getPostOrGetVariable( 'currentCompany' );
		$show = static::getPostOrGetVariable( 'show' );
		$type = static::getPostOrGetVariable( 'type' );
		
		////filter
		if($date1 == "" || $date2 == ""){
			$date1 = date("Y-m-d");
			$date2 = $date1;
		}
		if($currentCompany < 1){
			$currentCompany = 0;
		}
		if($type < 1){
			$type = 1;
		}
		
		$this->_view->assign('companyid',$companyId);
		$this->_view->assign('date1',$date1);
		$this->_view->assign('date2',$date2);
		
		$filter = "<select name=currentCompany><option value=0>All Companies</option>";
		
		$query = "SELECT companyid,companyname FROM company WHERE companyid != 2 ORDER BY companyname";
		$result = Treat_DB_ProxyOld::query($query);
		
		while($r = mysql_fetch_array($result)){
			$companyid = $r["companyid"];
			$companyname = $r["companyname"];
			
			if($currentCompany == $companyid){
				$sel = "SELECTED";
			}
			else{
				$sel = "";
			}
			
			$filter .= "<option value=$companyid $sel>$companyname</option>";
		}
		$filter .= "</select>";
		
		if($type == 2){
			$sel2 = "SELECTED";
		}
		
		$filter .= "<select name=type><option value=1>Summary</option><option value=2 $sel2>Details</option></select>";
		
		$this->_view->assign('filter',$filter);
		
		///show report
		if($show == 1){
			$report = "<table width=100% style=\"border:1px solid black;font-size:12px;\" border=0 cellspacing=0 cellpadding=1 bgcolor=white>
						<tr bgcolor=#FFFF99>
							<td style=\"border:1px solid black;\" width=16.66%>Company</td>
							<td style=\"border:1px solid black;\" width=16.66%>Business</td>
							<td style=\"border:1px solid black;\" width=16.66%>Audit</td>
							<td style=\"border:1px solid black;\" width=16.66%>Customer</td>
							<td style=\"border:1px solid black;\" width=16.66%>Transaction Date</td>
							<td style=\"border:1px solid black;\" width=16.66%>Total</td>
						</tr>";
						
			$download_data = "Company,Business,Audit,Customer,Transaction Date,Total\r\n";
		
			if($currentCompany > 0){
				$coFilter = " AND co.companyid = $currentCompany";
			}
			else{
				$coFilter = "";
			}
		
			$query = "SELECT l.username, 
							a.id,
							a.note,
							c.first_name, 
							c.last_name, 
							b.businessname, 
							co.companyname, 
							a.date_time,
							ROUND(SUM(ad.amount) / 2, 2) AS total
						FROM adjustment a
						JOIN login l ON l.loginid = a.loginid
						JOIN customer c ON c.customerid = a.customerid
						JOIN business b ON b.businessid = a.businessid
						JOIN company co ON co.companyid = b.companyid $coFilter
						JOIN adjustment_detail ad ON ad.adj_id = a.id
						WHERE a.date_time BETWEEN '$date1 00:00:00' AND '$date2 23:59:59'
						GROUP BY a.id
						ORDER BY a.date_time";
			$result = Treat_DB_ProxyOld::query($query);
			
			$reportTotal = 0;
			
			while($r = mysql_fetch_array($result)){
				$adj_id = $r["id"];
				$notes  = $r["note"];
				$audit = $r["username"];
				$custFirst = $r["first_name"];
				$custLast = $r["last_name"];
				$businessname = $r["businessname"];
				$companyname = $r["companyname"];
				$date_time = $r["date_time"];
				$reportTotal += $r["total"];
				$total = number_format($r["total"], 2);
				
				$report .= "<tr><td style=\"border:1px solid black;\">$companyname</td>
							<td style=\"border:1px solid black;\">$businessname</td>
							<td style=\"border:1px solid black;\">$audit</td>
							<td style=\"border:1px solid black;\">$custFirst $custLast</td>
							<td style=\"border:1px solid black;\">$date_time</td>
							<td style=\"border:1px solid black;\" align=right>$$total</td></tr>";
							
				$download_data .= "$companyname,$businessname,$audit,$custFirst $custLast,$date_time,$total\r\n";
							
				if($type == 2){
					$query2 = "SELECT * FROM adjustment_detail WHERE adj_id = $adj_id";
					$result2 = Treat_DB_ProxyOld::query($query2);
					
					while($r2 = mysql_fetch_array($result2)){
						$adj_type = $r2["type"];
						$account = $r2["account"];
						$amount = $r2["amount"];
						
						if($adj_type == 1){
							$query3 = "SELECT cr.creditname,ma.accountnum FROM credits cr
										JOIN mirror_accounts ma ON ma.accountid = cr.creditid AND ma.type = 1
										WHERE cr.creditid = $account AND cr.is_deleted = 0";
							$result3 = Treat_DB_ProxyOld::query($query3);
							
							$r3 = mysql_fetch_array($result3);
							
							$acctName = $r3["creditname"];
							$acctNum = $r3["accountnum"];
							$amt1 = "$" . number_format($amount, 2);
							$amt2 = "&nbsp;";
						}
						else{
							$query3 = "SELECT db.debitname,ma.accountnum FROM debits db
										JOIN mirror_accounts ma ON ma.accountid = db.debitid AND ma.type = 2
										WHERE db.debitid = $account AND db.is_deleted = 0";
							$result3 = Treat_DB_ProxyOld::query($query3);
							
							$r3 = mysql_fetch_array($result3);
							
							$acctName = $r3["debitname"];
							$acctNum = $r3["accountnum"];
							$amt2 = "$" . number_format($amount, 2);
							$amt1 = "&nbsp;";
						}
						
						$report .= "<tr bgcolor=#E9E9E9><td style=\"border:1px solid black;\">&nbsp;</td>
							<td style=\"border:1px solid black;\">$acctNum</td>
							<td style=\"border:1px solid black;\">$acctName</td>
							<td style=\"border:1px solid black;\">$amt1</td>
							<td style=\"border:1px solid black;\">$amt2</td>
							<td style=\"border:1px solid black;\" align=right>&nbsp;</td></tr>";
							
						$amt1 = str_replace("&nbsp;","",$amt1);	
						$amt2 = str_replace("&nbsp;","",$amt2);	
						
						$download_data .= ",$acctNum,$acctName,$amt1,$amt2,,\r\n";
					}
					
					///notes
					$report .= "<tr bgcolor=#E9E9E9><td style=\"border:1px solid black;\" colspan=6><i>$notes</i></td></tr>";
					$download_data .= "$notes\r\n";
				}
			}
			$reportTotal = number_format($reportTotal, 2);
			$report .= "<tr bgcolor=#FFFF99><td style=\"border:1px solid black;\" colspan=6 align=right><b>$$reportTotal</b></td></tr>";
			
			$report .= "</table>";
			
			$this->_view->assign('report',$report);
			$this->_view->assign('download_data',$download_data);
		}	
	}
	
	public function customerSettingsValidateCron() {
		ini_set('display_errors', false);

		$date = date("Y-m-d H:i:s");
		$date = date("Y-m-d H:i:s", mktime(substr($date,11,2) - 1, substr($date,14,2), substr($date,17,2)-1, substr($date,5,2) , substr($date,8,2), substr($date,0,4)));
		$date6 = date("Y-m-d H:i:s", mktime(substr($date,11,2) - 5, substr($date,14,2), substr($date,17,2)-1, substr($date,5,2) , substr($date,8,2), substr($date,0,4)));
		
		$query = "SELECT * FROM useLoseHistory
					WHERE (verified = 0 AND date2 <= '$date')
						OR
					(verified = 1 AND date2 <= '$date6')";
		$result = Treat_DB_ProxyOld::query($query);
		
		while($r = mysql_fetch_array($result)){
			$ULHid = $r["id"];
			$businessid = $r["businessid"];
			$date1 = $r["date1"];
			$date2 = $r["date2"];
			$verified = $r["verified"];
			$amountLoaded = $r["amountLoaded"];
			
			$businessIDs = "";
			
			////check reload totals (averages)
			if($verified == 1){
				$query2 = "SELECT AVG(amountLoaded) AS amountLoaded FROM useLoseHistory WHERE businessid = $businessid";
				$result2 = Treat_DB_ProxyOldProcessHost::query( $query2 );
				
				$average = @mysql_result($result2, 0, 'amountLoaded');
				
				if($amountLoaded <= ($average * .1 * 9) || $amountLoaded == 0){
					$emailData = "$businessid had a reload problem ($amountLoaded)";
					$emailData = str_replace("\n", "\r\n", str_replace(array("\r\n", "\r"), "\n", $emailData));

					$todayis = date("l, F j, Y, g:i a") ;
					$headers  = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					$headers .= "From: noreply@companykitchen.com\r\n";
					$subject = "Company Cash Reload Problem";

					mail("smartin@essentialpos.com", $subject, $emailData, $headers);
				}
			}
			
			///business_groups
			$query2 = "SELECT businessid FROM business_group_detail 
					WHERE group_id = (SELECT group_id FROM business_group_detail 
						WHERE businessid = $businessid)";
			$result2 = Treat_DB_ProxyOldProcessHost::query( $query2 );
			
			while($r2 = mysql_fetch_array($result2)){
				$newBid = $r2["businessid"];
				if($businessIDs == ""){
					$businessIDs = $newBid;
				}
				else{
					$businessIDs .= "," . $newBid; 
				}
			}
			
			if($businessIDs == ""){
				$businessIDs = $businessid;
			}
			
			$query2 = "SELECT c.customerid,
								c.first_name,
								c.last_name,
								c.scancode
							FROM customer c
							JOIN business b ON b.businessid = c.businessid
							WHERE c.businessid = $businessid
							GROUP BY c.customerid";
			$result2 = Treat_DB_ProxyOldProcessHost::query( $query2 );
			
			$totalPurchases = 0;
			
			while($r2 = mysql_fetch_array($result2)){
				$customerid = $r2["customerid"];
				$first_name = $r2["first_name"];
				$last_name = $r2["last_name"];
				$scancode = $r2["scancode"];
				
				///company cash positive
				$query3 = "SELECT SUM(chargetotal) AS coCash, count(id) AS charges FROM customer_transactions 
					WHERE customer_id = $customerid 
						AND date_created BETWEEN '$date1' AND '$date2' 
						AND trans_type_id = 9
						AND chargetotal > 0
					GROUP BY customer_id";
				$result3 = Treat_DB_ProxyOldProcessHost::query($query3);

				$coCashPos = @mysql_result($result3, 0, 'coCash');
				$idCount = @mysql_result($result3, 0, 'charges');
				
				if($coCashPos > 0){
					///company cash negative
					$query3 = "SELECT SUM(chargetotal) AS coCash, count(id) AS charges FROM customer_transactions 
						WHERE customer_id = $customerid 
							AND date_created BETWEEN '$date1' AND '$date2' 
							AND trans_type_id = 9
							AND chargetotal < 0
						GROUP BY customer_id";
					$result3 = Treat_DB_ProxyOldProcessHost::query($query3);

					$coCashNeg = @mysql_result($result3, 0, 'coCash');
					$idCount += @mysql_result($result3, 0, 'charges');

					///purchases
					$query3 = "SELECT SUM(pd.received) AS purchases FROM payment_detail pd 
						JOIN checks c ON c.businessid = pd.businessid 
							AND c.check_number = pd.check_number
							AND c.businessid IN ($businessIDs)
							AND c.bill_posted BETWEEN '$date1' AND '$date2'
						WHERE pd.scancode = '$scancode' AND pd.payment_type IN (5,14)";
					$result3 = Treat_DB_ProxyOldProcessHost::query($query3);

					$purchases = @mysql_result($result3, 0, 'purchases');

					if($idCount > 0){	
						$coCashPos = number_format($coCashPos, 2);
						$coCashNeg = number_format($coCashNeg, 2);

						if($purchases > $coCashPos){
							$used = number_format($coCashPos, 2);
						}
						else{
							$used = number_format($purchases, 2);
						}
						
						///totals
						$totalPurchases += $used;

						///Houston, we have a problem
						if(round($used, 2) != round($coCashPos + $coCashNeg, 2)){
							$adjust = round($used - ($coCashPos + $coCashNeg), 2);
							$adjust_date = date("Y-m-d H:i:s", mktime(substr($date2,11,2), substr($date2,14,2), substr($date2,17,2)-1, substr($date2,5,2) , substr($date2,8,2), substr($date2,0,4)));
							
							///add transactions
							$query3 = "INSERT INTO customer_transactions (oid, trans_type_id, customer_id, subtotal, chargetotal, date_created, response, authresponse)
									VALUES ('company cash', 9, $customerid, '$adjust', '$adjust', '$adjust_date', 'APPROVED', 'EE')
									ON DUPLICATE KEY UPDATE subtotal = subtotal + '$adjust', chargetotal = chargetotal + '$adjust'";
							$result3 = Treat_DB_ProxyOld::query($query3);

							///update balance
							$query3 = "UPDATE customer SET balance = balance + '$adjust' WHERE customerid = $customerid";
							$result3 = Treat_DB_ProxyOld::query($query3);
							
							$emailData .= "$customerid: $first_name $last_name, $date1 - $date2, $$used, $$adjust<br>";
						}
					}
				}
			}
			$query2 = "UPDATE useLoseHistory SET verified = verified + 1, amountSpent = '$totalPurchases' WHERE id = $ULHid";
			$result2 = Treat_DB_ProxyOld::query( $query2 );
		}
		
		if($emailData != ""){
			$emailData = str_replace("\n", "\r\n", str_replace(array("\r\n", "\r"), "\n", $emailData));

			$todayis = date("l, F j, Y, g:i a") ;
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= "From: noreply@companykitchen.com\r\n";
			$subject = "Company Cash Reset Fixes";

			mail("smartin@essentialpos.com", $subject, $emailData, $headers);
			mail("jake@essential-elements.net", $subject, $emailData, $headers);
		}
	}
	
	public function customerSettingsCron() {
		ini_set('display_errors', true);
		
		$filePath = "/srv/home/espos/www/TreatAmerica/useLose/";
		
		$now = date("H:i");
		$today = date("l");
		$date = date("Y-m-d");
		
		$query = "SELECT b.businessid, 
					b.businessname, 
					bs.useLosePeriod, 
					bs.useLoseDateTime,
					bs.useLoseEmail,
					bs.useLoseDataSourceType,
					bs.useLoseDataSource,
					bs.useLoseLastRun,
					bs.useLoseCardRange
					FROM business b
					JOIN business_setting bs ON bs.business_id = b.businessid
					WHERE bs.useLose = 1 AND 
						(
						(bs.useLosePeriod = 1 AND (
							(bs.useLoseDateTime = '$now') 
								OR 
							(bs.useLoseDateTime < '$now' AND DATE(bs.useLoseLastRun) < '$date')
							)
						)
						OR
						(bs.useLosePeriod = 2 AND (
							(bs.useLoseDateTime = '$now' AND bs.useLoseDay = '$today') 
								OR 
							(bs.useLoseDateTime < '$now' AND bs.useLoseDay = '$today' AND DATE(bs.useLoseLastRun) < '$date')
							)
						)
						OR
						(bs.useLosePeriod = 3 AND (
							(bs.useLoseDateTime = '$now' AND SUBSTRING(CURDATE(),9,2) = '01') 
								OR 
							(bs.useLoseDateTime < '$now' AND SUBSTRING(CURDATE(),9,2) = '01' AND DATE(bs.useLoseLastRun) < '$date')
							)
						)
						)";
		$result = Treat_DB_ProxyOld::query($query);
		
		while($r = mysql_fetch_array($result)){
			$businessid = $r["businessid"];
			$businessname = $r["businessname"];
			$useLosePeriod = $r["useLosePeriod"]; 
			$useLoseDateTime = $r ["useLoseDateTime"];
			$useLoseEmail = $r["useLoseEmail"];
			$useLoseDataSourceType = $r["useLoseDataSourceType"];
			$useLoseDataSource = $r["useLoseDataSource"];
			$useLoseLastRun = $r["useLoseLastRun"];
			$useLoseCardRange = $r["useLoseCardRange"];
			
			$businessIDs = "";
			
			///business_groups
			$query2 = "SELECT businessid FROM business_group_detail 
					WHERE group_id = (SELECT group_id FROM business_group_detail 
						WHERE businessid = $businessid)";
			$result2 = Treat_DB_ProxyOldProcessHost::query( $query2 );
			
			while($r2 = mysql_fetch_array($result2)){
				$newBid = $r2["businessid"];
				if($businessIDs == ""){
					$businessIDs = $newBid;
				}
				else{
					$businessIDs .= "," . $newBid; 
				}
			}
			
			if($businessIDs == ""){
				$businessIDs = $businessid;
			}
			
			$emailData = "<table border=1><thead><tr bgcolor=#E8E7E7><td>Scancode</td><td>Name</td><td>Value Added</td><td>Value Used</td><td>Total Purchases</td><td>Value Removed</td></tr></thead>\r\n";
			$totalstart = 0;
			$totalused = 0;
			$totalremoved = 0;
			
			///figure date range
			//////daily
			if($useLosePeriod == 1){
				///date range
				$date2 = "$date $now:00";
				$date1 = $useLoseLastRun;
				///remove date
				$date4 = date("Y-m-d H:i:s", mktime(substr($date2,11,2), substr($date2,14,2), substr($date2,17,2)-1, substr($date2,5,2) , substr($date2,8,2), substr($date2,0,4)));
				///add date
				$date3 = "$date $now:01";
			}
			//////weekly
			elseif($useLosePeriod == 2){
				///date range
				$date2 = "$date $now:00";
				$date1 = $useLoseLastRun;
				///remove date
				$date4 = date("Y-m-d H:i:s", mktime(substr($date2,11,2), substr($date2,14,2), substr($date2,17,2)-1, substr($date2,5,2) , substr($date2,8,2), substr($date2,0,4)));
				///add date
				$date3 = "$date $now:01";
			}
			//////monthly
			elseif($useLosePeriod == 3){
				///date range
				$date2 = "$date $now:00";
				$date1 = $useLoseLastRun;
				///remove date
				$date4 = date("Y-m-d H:i:s", mktime(substr($date2,11,2), substr($date2,14,2), substr($date2,17,2)-1, substr($date2,5,2) , substr($date2,8,2), substr($date2,0,4)));
				///add date
				$date3 = "$date $now:01";
			}
			else{
				
			}
			
			echo "$businessid, $businessname, $date1 to $date2<br>";
			
			///remove unused balances
			$cardRanges = explode(",", $useLoseCardRange);
			
			foreach($cardRanges AS $key => $cardRange){
				$cardRange = explode(":", $cardRange);
				$amount = $cardRange[1];
				///get customers
				if($cardRange[0] == "All"){
					$whereQuery = "c.businessid = $businessid";
				}
				else{
					$cards = explode("-", $cardRange[0]);
					
					$whereQuery =  "c.businessid = $businessid AND c.scancode BETWEEN '$cards[0]' AND '$cards[1]'";
				}
				
				///sum of company cash and purchases by customer
				$query2 = "SELECT c.customerid,
								c.first_name,
								c.last_name,
								c.scancode
							FROM customer c
							JOIN business b ON b.businessid = c.businessid
							WHERE $whereQuery
							GROUP BY c.customerid";
				$result2 = Treat_DB_ProxyOld::query($query2);
				
				while($r2 = mysql_fetch_array($result2)){
					$customerid = $r2["customerid"];
					$first_name = $r2["first_name"];
					$last_name = $r2["last_name"];
					$scancode = $r2["scancode"];
					
					///company cash
					$query3 = "SELECT SUM(chargetotal) AS coCash FROM customer_transactions 
						WHERE customer_id = $customerid AND date_created BETWEEN '$date1' AND '$date2' AND trans_type_id = 9";
					$result3 = Treat_DB_ProxyOld::query($query3);
				
					$coCash = @mysql_result($result3, 0, 'coCash');
					
					///purchases
					$query3 = "SELECT SUM(pd.received) AS purchases FROM payment_detail pd 
						JOIN checks c ON c.businessid = pd.businessid 
							AND c.check_number = pd.check_number 
							AND c.businessid IN ($businessIDs) 
							AND c.bill_posted BETWEEN '$date1' AND '$date2'
						WHERE pd.scancode = '$scancode' AND pd.payment_type IN (5,14)";
					$result3 = Treat_DB_ProxyOldProcessHost::query($query3);
				
					$purchases = @mysql_result($result3, 0, 'purchases');
					
					///remove if not used
					if($purchases < $coCash && $coCash > 0){
						$remove = round(($coCash - $purchases) * -1, 2);
						$totalremoved += $remove;
						$totalused += $purchases;
						$totalstart += $coCash;
						
						///add transactions
						$query3 = "INSERT INTO customer_transactions (oid, trans_type_id, customer_id, subtotal, chargetotal, date_created, response, authresponse)
								VALUES ('company cash', 9, $customerid, '$remove', '$remove', '$date4', 'APPROVED', 'EE')";
						$result3 = Treat_DB_ProxyOld::query($query3);
						
						///update balance
						$query3 = "UPDATE customer SET balance = ROUND(balance + $remove, 2) WHERE customerid = $customerid";
						$result3 = Treat_DB_ProxyOld::query($query3);
						
						$emailData .= "<tr><td>$scancode</td><td>$first_name $last_name</td><td>$$coCash</td><td>$$purchases</td><td>$$purchases</td><td>$$remove</td></tr>\r\n";
					}
					elseif($coCash < $purchases && $coCash > 0){
						$totalused += $coCash;	
						$totalstart += $coCash;
						$emailData .= "<tr><td>$scancode</td><td>$first_name $last_name</td><td>$$coCash</td><td>$$coCash</td><td>$$purchases</td><td>&nbsp;</td></tr>\r\n";
					}
				}
			}			
			$emailData .= "</table>";
			
			$totalstart = number_format($totalstart, 2);
			$totalused = number_format($totalused, 2);
			$totalremoved = number_format($totalremoved, 2);
			
			$emailData = "
				<html>
					<head></head>
					<body>
						Period: $date1 to $date2<br>
						Total Added: $$totalstart<br>
						Total Used: $$totalused<br>
						Total Removed: $$totalremoved<br>
						{$emailData}
					</body>
				</html>";
			
			///send email
			$emailData = str_replace("\n", "\r\n", str_replace(array("\r\n", "\r"), "\n", $emailData));
			
			$todayis = date("l, F j, Y, g:i a") ;
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= "From: noreply@companykitchen.com\r\n";
			$subject = "$businessname Company Cash Reset";

			mail($useLoseEmail, $subject, $emailData, $headers);
			
			///update last run time
			$query2 = "UPDATE business_setting SET useLoseLastRun = '$date2' WHERE business_id = $businessid";
			$result2 = Treat_DB_ProxyOld::query($query2);
			
			$query2 = "INSERT INTO useLoseHistory (businessid,date1,date2) VALUES ($businessid,'$date1','$date2')";
			$result2 = Treat_DB_ProxyOld::query($query2);
			$useLoseId = Treat_DB_ProxyOld::mysql_insert_id();
			
			///////////////
			///add value///
			///////////////
			
			///select customers
			$customers = array();
			
			if($useLoseDataSourceType == 1){
				///card range is ignored here
				$cardRanges = explode(",", $useLoseCardRange);
				$cardRange = explode(":", $cardRanges);
				$amount = $cardRange[1];
				
				///data source and columns
				$useLoseDataSource = explode(":", $useLoseDataSource);
				
				if($businessid == 1380 || $businessid == 143 || $businessid == 2330){
					$fileData = file_get_contents($filePath . $useLoseDataSource[0]);
					$fileData = str_replace(chr(13), "\r\n", $fileData);
					file_put_contents(DIR_VENDOR_IMPORTS . "/" . $useLoseDataSource[0], $fileData);
					
					$handle = fopen(DIR_VENDOR_IMPORTS . "/" . $useLoseDataSource[0], 'r') or $errorMessage = "Could Not Find File $useLoseDataSource[0]";
				}
				else{
					$handle = fopen($filePath . $useLoseDataSource[0], 'r') or $errorMessage = "Could Not Find File $useLoseDataSource[0]";
				}
				
				echo $errormessage;
				
				while ($pieces = fgetcsv($handle,NULL,",")){
					$scancode = $pieces[$useLoseDataSource[1]];
					$fileAmt = trim($pieces[$useLoseDataSource[2]]);
					$first_name = trim($pieces[$useLoseDataSource[3]]);
					$last_name = trim($pieces[$useLoseDataSource[4]]);
					
					$first_name = str_replace("'","`",$first_name);
					$last_name = str_replace("'","`",$last_name);
					
					$scancode = trim($scancode);
					$len = strlen($scancode);
					
					while($len < 13){
						if($len == 11){
							$scancode = "9$scancode";
						}
						elseif($len == 12){
							$scancode = "7$scancode";
						}
						else{
							$scancode = "0$scancode";
						}
						
						$len++;
					}
					
					$query2 = "SELECT customerid FROM customer WHERE businessid = $businessid AND scancode = '$scancode'";
					$result2 = Treat_DB_ProxyOldProcessHost::query($query2);
				
					$customerid = @mysql_result($result2, 0, "customerid");
					
					if($fileAmt > 0 && $customerid > 0){
						$customers[$customerid] = $fileAmt;
					}
					elseif($customerid > 0){
						$customers[$customerid] = $amount;
					}
					else{
						///add customer
						if($first_name == "" || $last_name == ""){
							$first_name = "Valued";
							$last_name = "Customer";
						}
						
						$query2 = "INSERT INTO customer (username,password,businessid,scancode,first_name,last_name)
									VALUES ('$scancode-$businessid@ck.com','essential1',$businessid,'$scancode','$first_name','$last_name')";
						$result2 = Treat_DB_ProxyOld::query($query2);
				
						$customerid = Treat_DB_ProxyOld::mysql_insert_id();
						
						if($customerid > 0){
							if($fileAmt > 0){
								$customers[$customerid] = $fileAmt;
							}
							elseif($amount > 0){
								$customers[$customerid] = $amount;
							}
						}
					}
					
					///colorado belle custom field
					if($businessid == 1380){
						$customfield = trim($pieces[4]);
						
						if($customfield != ""){
							$query2 = "INSERT INTO pd_portal.CustomFieldValue (CustomFieldId,CustomerId,Value)
								VALUES (56,$customerid,'$customfield')
								ON DUPLICATE KEY UPDATE Value = '$customfield'";
							$result2 = Treat_DB_ProxyOld::query($query2);
						}
					}
				}
			}
			else{
				$cardRanges = explode(",", $useLoseCardRange);

				foreach($cardRanges AS $key => $cardRange){
					$cardRange = explode(":", $cardRange);
					$amount = $cardRange[1];
					///get customers
					if($cardRange[0] == "All"){
						$whereQuery = "c.businessid = $businessid";
					}
					else{
						$cards = explode("-", $cardRange[0]);

						$whereQuery =  "c.businessid = $businessid AND c.scancode BETWEEN '$cards[0]' AND '$cards[1]'";
					}

					///sum of company cash and purchases by customer
					$query2 = "SELECT c.customerid
								FROM customer c
								JOIN business b ON b.businessid = c.businessid
								WHERE $whereQuery
								GROUP BY c.customerid";
					$result2 = Treat_DB_ProxyOld::query($query2);

					while($r2 = mysql_fetch_array($result2)){
						$customers[$r2["customerid"]] = $amount;
					}
				}
			}
			
			///add transaction and update balance
			$totalLoaded = 0;
			$balances = array();
			foreach($customers AS $customerid => $amount){
				if($amount != 0){
				///add transactions
					$query3 = "INSERT INTO customer_transactions (oid, trans_type_id, customer_id, subtotal, chargetotal, date_created, response, authresponse)
							VALUES ('company cash', 9, $customerid, '$amount', '$amount', '$date3', 'APPROVED', 'EE')";
					$result3 = Treat_DB_ProxyOld::query($query3);

					///update balance
					$query3 = "UPDATE customer SET balance = balance + '$amount' WHERE customerid = $customerid";
					$result3 = Treat_DB_ProxyOld::query($query3);
					
					///totals
					$totalLoaded += $amount;
					
					///double check balances
					if($balances["$amount"] == ""){
						$balances["$amount"] = $customerid;
					}
					else{
						$balances["$amount"] .= ",$customerid";
					}
				}
			}
			$query3 = "UPDATE useLoseHistory SET amountLoaded = '$totalLoaded' WHERE id = $useLoseId";
			$result3 = Treat_DB_ProxyOld::query($query3);
			
			///find low balances and send email
			$fixCust = "";
			
			foreach($balances AS $amount => $customerIds){
				$query3 = "SELECT customerid,
								scancode,
								first_name,
								last_name,
								balance 
							FROM customer 
							WHERE customerid IN ($customerIds) 
								AND balance < '$amount'";
				$result3 = Treat_DB_ProxyOld::query($query3, true);
				
				while($r3 = mysql_fetch_array($result3)){
					$bCust = $r3["customerid"];
					$bScan = $r3["scancode"];
					$bFirst = $r3["first_name"];
					$bLast = $r3["last_name"];
					$bBalance = $r3["balance"];
					
					$fixCust .= "<tr><td>$bCust</td><td>$bScan</td><td>$bFirst</td><td>$bLast</td><td>$bBalance</td></tr>\r\n";
				}
			}
			
			///send bad account email
			if($fixCust != "" || true){
				$fixCust = "<html><head></head><body><table border=1>$fixCust</table></body></html>";

				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				$headers .= "From: noreply@companykitchen.com\r\n";
				$subject = "$businessname Company Cash Problem Accounts";

				mail("smartin@essentialpos.com", $subject, $fixCust, $headers);
				mail("jake@essential-elements.net", $subject, $fixCust, $headers);
			}
		}
	}	
	
	public function customerSettings() {
		ini_set('display_errors', false);
		$companyId = $_GET["cid"];
		$businessid = $_POST["businessid"];
		$edit = $_POST["edit"];
		
		$this->_view->assign('uri',$this->_uri->getAction());
		
		$this->addJavascriptAtTop('/assets/js/jquery-1.5.1.min.js');
		$this->addJavascriptAtTop('/assets/js/jquery-ui-1.8.11.custom.min.js');
		$this->addCss('/assets/css/jq_themes/redmond/jqueryui-current.css');
		$this->addCss('/assets/css/jq_themes/redmond/jqueryui-custom.css');
		
		$query = "SELECT businessid,businessname FROM business WHERE companyid = $companyId ORDER BY businessname";
		$result = Treat_DB_ProxyOld::query($query);
		
		$myform = "<center><table width=95% cellspacing=0 cellpadding=0 style=\"border:2px solid #999999;\" bgcolor=#999999><tr><td>";
		$myform .= "<form action=customerSettings?cid=$companyId method=post style=\"margin:0;padding:0;display:inline;\"><select name=businessid><option value=0>Please Choose...</option>";
		
		while($r = mysql_fetch_array($result)){
			if($businessid == $r['businessid']){
				$sel = "SELECTED";
			}
			else{
				$sel = "";
			}
			$myform .= "<option value=" . $r['businessid'] . " $sel>" . $r['businessname'] . "</option>";
		}
		
		$myform .= "</select> <input type=submit value='GO' style=\"border:1px solid #777777;background-color:#E8E7E7;font-size:12px;\"></form></td></tr></table></center>";
		
		$this->_view->assign('myform',$myform);
		
		if($businessid > 0){
			if($edit == 1){
				$useLose = @$_POST["useLose"];
				$useLosePeriod = @$_POST["useLosePeriod"];
				$useLoseDateTime = @$_POST["useLoseDateTime"];
				$useLoseDay = @$_POST["useLoseDay"];
				$useLoseEmail = @$_POST["useLoseEmail"];
				$useLoseDataSourceType = @$_POST["useLoseDataSourceType"];
				$useLoseDataSource = @$_POST["useLoseDataSource"];
				$useLoseLastRun = @$_POST["useLoseLastRun"];
				$useLoseCardRange = @$_POST["useLoseCardRange"];
				
				$query = "INSERT INTO business_setting (business_id, useLose, useLosePeriod, useLoseDateTime, useLoseDay, useLoseEmail, useLoseDataSourceType, useLoseDataSource, useLoseCardRange)
							VALUES ($businessid, '$useLose', '$useLosePeriod', '$useLoseDateTime', '$useLoseDay', '$useLoseEmail', '$useLoseDataSourceType', '$useLoseDataSource', '$useLoseCardRange')
							ON DUPLICATE KEY UPDATE useLose = '$useLose', 
								useLosePeriod = '$useLosePeriod', 
								useLoseDateTime = '$useLoseDateTime', 
								useLoseDay = '$useLoseDay',
								useLoseEmail = '$useLoseEmail', 
								useLoseDataSourceType = '$useLoseDataSourceType', 
								useLoseDataSource = '$useLoseDataSource', 
								useLoseCardRange = '$useLoseCardRange'";
				$result = Treat_DB_ProxyOld::query($query);
				
				sleep(1);
				
				$showsave = "<font color=#00FF00> - UPDATED</font>";
			}
			
			$query = "SELECT * FROM business_setting WHERE business_id = $businessid";
			$result = Treat_DB_ProxyOld::query($query);
			
			$useLose = @mysql_result($result, 0 , "useLose");
			$useLosePeriod = @mysql_result($result, 0 , "useLosePeriod");
			$useLoseDateTime = @mysql_result($result, 0 , "useLoseDateTime");
			$useLoseDay = @mysql_result($result, 0 , "useLoseDay");
			$useLoseEmail = @mysql_result($result, 0 , "useLoseEmail");
			$useLoseDataSourceType = @mysql_result($result, 0 , "useLoseDataSourceType");
			$useLoseDataSource = @mysql_result($result, 0 , "useLoseDataSource");
			$useLoseLastRun = @mysql_result($result, 0 , "useLoseLastRun");
			$useLoseCardRange = @mysql_result($result, 0 , "useLoseCardRange");
			
			if($useLose == 1){
				$check = "CHECKED";
			}
			
			if($useLosePeriod == 1){
				$sel1 = "SELECTED";
			}
			elseif($useLosePeriod == 2){
				$sel2 = "SELECTED";
			}
			elseif($useLosePeriod == 3){
				$sel3 = "SELECTED";
			}
			
			if($useLoseDataSourceType == 1){
				$sel1a = "SELECTED";
			}
			
			$days = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
			
			$busform = "<center><form action=customerSettings?cid=$companyId method=post><input type=hidden name=businessid value=$businessid><input type=hidden name=edit value=1><table width=95% cellspacing=0 cellpadding=0 style=\"border:2px solid #999999;\" bgcolor=white>";
			$busform .= "<tr bgcolor=#999999 style=\"color:white;\"><td colspan=2>Use It or Lose It Settings $showsave</td></tr>";
			$busform .= "<tr><td align=right>Active:</td><td><input name=useLose type=checkbox value=1 $check></td></tr>";
			$busform .= "<tr><td align=right>Period:</td><td><select name=useLosePeriod><option value=1 $sel1>Daily</option><option value=2 $sel2>Weekly</option><option value=3 $sel3>Monthly</option></select> (Monthly resets occur on the 1st)</td></tr>";
			$busform .= "<tr><td align=right>Time:</td><td><input type=text size=30 name=useLoseDateTime value=$useLoseDateTime> (format 23:00)</td></tr>";
			$busform .= "<tr><td align=right>Day of the Week:</td><td><select name=useLoseDay>";
				foreach($days AS $key => $day){
					if($useLoseDay == $day){
						$sel1b = "SELECTED";
					}
					else{
						$sel1b = "";
					}
					$busform .= "<option value=$day $sel1b>$day</option>";
				}
			$busform .= "</select> (Only applies to weekly period)</td></tr>";
			$busform .= "<tr><td align=right>Email:</td><td><input type=text size=30 name=useLoseEmail value='$useLoseEmail'></td></tr>";
			$busform .= "<tr><td align=right>Data Source Type:</td><td><select name=useLoseDataSourceType><option value=0>None</option><option value=1 $sel1a>File</option></select></td></tr>";
			$busform .= "<tr><td align=right>Data Source:</td><td><input type=text size=30 name=useLoseDataSource value='$useLoseDataSource'> (filename:column of scancode:column of amount:fname:lname)</td></tr>";
			$busform .= "<tr><td align=right>Card Range/Value:</td><td><input type=text size=30 name=useLoseCardRange value='$useLoseCardRange'> (Default is All:1.00, example 7800000000001-7800000000002:3.25)</td></tr>";
			$busform .= "<tr><td colspan=2 align=right><input type=submit value='Save' style=\"border:1px solid #999999;background-color:#E8E7E7;font-size:12px;\">";
			$busform .= "</table></form></center>";
			
			$this->_view->assign('busform',$busform);
		}
	}
	
	public function customerTrans() {
		ini_set('display_errors', false);
		
		$customerid = $_POST["customerid"];
		$transtype = $_POST["transtype"];
		$amount = $_POST["amount"];
		$cid = $_GET["cid"];
		
		$amount = round($amount, 2);
		
		if($customerid > 0 && $amount != 0){
			$reasons = array(
 				3 => 'refund',
 				4 => 'promo',
 				7 => 'balance transfer',
 				8 => 'CK Adjust'
 			);
			
			$audit = @$_COOKIE['usercook'] ?: '';
			
			$query = "INSERT INTO customer_transactions (oid, trans_type_id, customer_id, subtotal, chargetotal, response, authresponse)
						VALUES ('$reasons[$transtype]', $transtype, $customerid, '$amount', '$amount', 'APPROVED', '$audit')";
			$result = Treat_DB_ProxyOld::query($query);
			
			$query = "UPDATE customer SET balance = balance + $amount WHERE customerid = $customerid";
			$result = Treat_DB_ProxyOld::query($query);
		}
		
		sleep(1);
		
		header("Location: customerSearch?cid=$cid&cust=$customerid");
	}
	
	public function customerSearch() {
		ini_set('display_errors', false);
		include_once( 'lib/class.menu_items.php' );
		
		$user = $_SESSION->getUser();
		$companyId = $user->getCompanyId();

		if ( $user->customer_utility == 0 ) {
			header('Location: /ta/businesstrack.php');
			exit;
		}

		$this->_view->assign('uri',$this->_uri->getAction());

		$this->addJavascriptAtTop('/assets/js/jquery-1.5.1.min.js');
		$this->addJavascriptAtTop('/assets/js/jquery-ui-1.8.11.custom.min.js');
		$this->addCss('/assets/css/jq_themes/redmond/jqueryui-current.css');
		$this->addCss('/assets/css/jq_themes/redmond/jqueryui-custom.css');
		
		$search_what = $_POST["search_what"];
		$search_for = $_POST["search_for"];
		$customerid = $_POST["customerid"];
		$showtrans = $_POST["showtrans"];
		if($customerid < 1){
			$customerid = $_GET["cust"];
			$showtrans = 1;
		}
		$edit = $_POST["edit"];
		
		$this->_view->assign('companyId',$companyId);
		
		if($search_what != ''){
			if($search_what == "scancode"){$sel1 = "SELECTED";}
			elseif($search_what == "first_name"){$sel2 = "SELECTED";}
			elseif($search_what == "last_name"){$sel3 = "SELECTED";}
			
			$this->_view->assign('sel1',$sel1);
			$this->_view->assign('sel2',$sel2);
			$this->_view->assign('sel3',$sel3);
			
			$this->_view->assign('search_for',$search_for);
		}
		
		///delete account
		if($edit == 2){
			$query = "DELETE FROM customer WHERE customerid = $customerid";
			$result = Treat_DB_ProxyOld::query($query);
			
			$customerid = 0;
		}
		
		////list search results
		if($search_for != '' && $customerid < 1){
			
			$query = "SELECT *,business.businessname FROM customer JOIN business ON business.businessid = customer.businessid WHERE $search_what LIKE '%" . $search_for . "%'";
			$result = Treat_DB_ProxyOld::query($query);
			
			while($r = mysql_fetch_array($result)){
				$cust_data .= "<tr onMouseOver=this.bgColor='yellow' onMouseOut=this.bgColor='white' style=\"font-size:12px;\"><td style=\"border: 1px solid #999999;\">{$r['scancode']}</td><td style=\"border: 1px solid #999999;\">{$r['username']}</td><td style=\"border: 1px solid #999999;\">{$r['first_name']}</td><td style=\"border: 1px solid #999999;\">{$r['last_name']}</td><td style=\"border: 1px solid #999999;\">{$r['businessname']}</td><td style=\"border: 1px solid #999999;\"><form action=customerSearch?cid=$companyId method=post style=\"margin:0;padding:0;display:inline;\"><input type=hidden name=customerid value=" . $r["customerid"] . ">&nbsp;<input type=submit value='Edit' style=\"border:1px solid #999999;background-color:#E8E7E7;font-size:10px;\"><input type=hidden name=search_what value='$search_what'><input type=hidden name=search_for value='$search_for'>&nbsp;</form>";
				$cust_data .= "<form action=customerSearch?cid=$companyId method=post style=\"margin:0;padding:0;display:inline;\"><input type=hidden name=customerid value=" . $r["customerid"] . "><input type=submit value='Delete' onclick=\"return confirm('ARE YOU SURE you want to DELETE this Account?');\" style=\"border:1px solid #999999;background-color:#E8E7E7;font-size:10px;\"><input type=hidden name=search_what value='$search_what'><input type=hidden name=search_for value='$search_for'><input type=hidden name=edit value=2></form></td></tr>";
			}
			
			if($cust_data == ""){$cust_data .= "<tr><td colspan=6 style=\"border:1px solid #999999;\">No Data to Display</td></tr>";}
			
			$myform = "<center><table width=95% cellspacing=0 cellpadding=0 style=\"border:1px solid #999999;\" bgcolor=white>";
			$myform .= "<tr bgcolor=#999999 style=\"color:white;\"><td style=\"border:1px solid #999999;\">Scancode</td><td style=\"border:1px solid #999999;\">Email</td><td style=\"border:1px solid #999999;\">First Name</td><td style=\"border:1px solid #999999;\">Last Name</td><td style=\"border:1px solid #999999;\">Business</td><td style=\"border:1px solid #999999;\">&nbsp;</td></tr>";
			$myform .= $cust_data;
			$myform .= "</table></center>";
			
			$this->_view->assign('myform',$myform);
		}	
		elseif($customerid > 0){
			
			////update database
			if($edit == 1){
				$email = $_POST["username"];
				$pass = $_POST["password"];
				$scancode = $_POST["scancode"];
				$first_name = $_POST["first_name"];
				$last_name = $_POST["last_name"];
				$cust_bus = $_POST["cust_bus"];
				$balance = $_POST["balance"];
				$updateBalance = $_POST["updateBalance"];
				
				if($updateBalance == 1){
					$audit = @$_COOKIE['usercook'] ?: '';

					$balanceQuery = "balance = " . $balance . ",";
			
					$query = "INSERT INTO audit_customer (customerid,user,new_balance) VALUES ($customerid,'$audit','$balance')";
					$result = Treat_DB_ProxyOld::query($query);
				}
				else{
					$balanceQuery = "";
				}
				
				if($pass != ""){
					$query = "UPDATE customer 
								SET username = '$email',
								password = '$pass',
								scancode = '$scancode',
								first_name = '$first_name',
								last_name = '$last_name',
								$balanceQuery
								businessid = $cust_bus
								WHERE customerid = $customerid";
					$result = Treat_DB_ProxyOld::query($query);
				}
				else{
					$query = "UPDATE customer 
								SET username = '$email',
								scancode = '$scancode',
								first_name = '$first_name',
								last_name = '$last_name',
								$balanceQuery
								businessid = $cust_bus
								WHERE customerid = $customerid";
					$result = Treat_DB_ProxyOld::query($query);
				}
				
				$cust_updated = 1;
			}
			
			///update customer
			if($cust_updated == 1){$cust_message = "<font color=#00FF00> - UPDATED</font>";}
			
			$query = "SELECT * FROM customer WHERE customerid = $customerid";
			$result = Treat_DB_ProxyOldProcessHost::query($query, true);
			
			$r = mysql_fetch_array($result);
			
			$homeBus = $r["businessid"];
			$staticBus = $homeBus;
			$scancode = $r["scancode"];
			
			$myform = "<center><form action=customerSearch?cid=$companyId method=post><input type=hidden name=customerid value=$customerid><input type=hidden name=edit value=1><table width=95% cellspacing=0 cellpadding=0 style=\"border:2px solid #999999;\" bgcolor=white>";
			$myform .= "<tr bgcolor=#999999 style=\"color:white;\"><td colspan=2>Edit Customer $customerid $cust_message</td></tr>";
			$myform .= "<tr><td align=right>Email:</td><td><input type=text name=username size=35 value=" . $r["username"] . "></td></tr>";
			$myform .= "<tr><td align=right>Password:</td><td><input type=text name=password size=35 value=''> <font size=2>(Leave blank if not changing)</font></td></tr>";
			$myform .= "<tr><td align=right>Scancode:</td><td><input type=text name=scancode size=35 value='" . $r["scancode"] . "'></td></tr>";
			$myform .= "<tr><td align=right>First Name:</td><td><input type=text name=first_name size=35 value=" . $r["first_name"] . "></td></tr>";
			$myform .= "<tr><td align=right>Last Name:</td><td><input type=text name=last_name size=35 value=" . $r["last_name"] . "></td></tr>";
			$myform .= "<tr><td align=right>Balance:</td><td><input type=text name=balance size=35 value=" . $r["balance"] . "> <input type=checkbox name=updateBalance value=1><font size=2>(Check this box to update the balance)</font></td></tr>";
			$myform .= "<tr><td align=right>Business:</td><td><select name=cust_bus>";
			
			$query2 = "SELECT * FROM business WHERE companyid != 2 ORDER BY businessname";
			$result2 = Treat_DB_ProxyOldProcessHost::query($query2);

			while( $r2 = mysql_fetch_array($result2) ){
				$businessid = $r2["businessid"];
				$businessname = $r2["businessname"];

				if($r2["businessid"] == $r["businessid"]){$sel = "SELECTED";}
				else{$sel = "";}

				$myform .= "<option value=$businessid $sel>$businessname</option>";
			}
			
			$myform .= "</select></td></tr>";
			$myform .= "<tr><td colspan=2 align=right><input type=submit onclick=\"\" value='Save' style=\"border:1px solid #999999;background-color:#E8E7E7;font-size:12px;\">";
			$myform .= "</table></form></center><p>";
			
			$this->_view->assign('myform',$myform);
			
			////refunds, promos, etc
			$adjForm = "<center><form action=customerTrans?cid=$companyId method=post><input type=hidden name=customerid value=$customerid><input type=hidden name=edit value=1><table width=95% cellspacing=0 cellpadding=0 style=\"border:2px solid #999999;\" bgcolor=white>";
			$adjForm .= "<tr bgcolor=#999999 style=\"color:white;\"><td>Online Transaction</td><td align=right><td align=right><a href=# onclick=\"jQuery('#adJs').fadeOut();\" style=\"text-decoration:none;\"><font color=white>Hide</font></a></td></tr>";
			$adjForm .= "<tr><td colspan=2>";
			$adjForm .= "&nbsp;Reason: <select name=transtype>
							<option value='3'>Refund</option>
							<option value='4'>Promo</option>
							<option value='7'>Balance Transfer</option>
							<option value='8'>CK Adjust</option>
							</select> 
							Amount: $<input type=text name=amount size=5> 
							<input type=submit id=trans value='GO' style=\"border:1px solid #999999;background-color:#E8E7E7;font-size:12px;\" onclick=\"return confirm('Are you sure?');\">";
			$adjForm .= "</td></tr>";
			$adjForm .= "</table></form></center><p>";
			
			$this->_view->assign('adjForm',$adjForm);
			
			/////adjustments (Steve - 8/5/14)
			$sync_version = 1;
			$adjBus = @$_POST["adjBus"];
			$confirm = @$_POST["confirm"];
			$notes = @$_POST["notes"];
			if($adjBus > 0){$homeBus = $adjBus;}
			
			$adjustmentForm = "<center><form action=customerSearch?cid=$companyId#bottom method=post><input type=hidden name=customerid value=$customerid><table width=95% cellspacing=0 cellpadding=0 style=\"border:2px solid #999999;\" bgcolor=white>";
			$adjustmentForm .= "<tr bgcolor=#999999 style=\"color:white;\"><td colspan=2><a name=bottom><font color=white>Adjustment for Customer $customerid</font></a></td></tr>";
			$adjustmentForm .= "<tr><td align=right width=25%>Business:</td><td>&nbsp;<select name=adjBus>";
			
			$query2 = "SELECT * FROM business WHERE companyid != 2 ORDER BY businessname";
			$result2 = Treat_DB_ProxyOldProcessHost::query($query2);

			while( $r2 = mysql_fetch_array($result2) ){
				$businessid = $r2["businessid"];
				$businessname = $r2["businessname"];

				if($r2["businessid"] == $homeBus){
					$sel = "SELECTED";
					$query3 = "SELECT cp.sync_version FROM mburris_manage.client_programs cp 
						WHERE cp.businessid = $homeBus 
						AND cp.db_name = '".\EE\Config::singleton()->get('db', 'database')."'";
					$result3 = Treat_DB_ProxyOld::query($query3);
					
					$r3 = mysql_fetch_array($result3);
			
					$sync_version = $r3["sync_version"];
				}
				else{
					$sel = "";
				}

				$adjustmentForm .= "<option value=$businessid $sel>$businessname</option>";
			}
			
			$adjustmentForm .= "</select> <input type=submit value='Change' style=\"border:1px solid #999999;background-color:#E8E7E7;font-size:12px;\"></form>";
			
			if($sync_version < 2){
				$adjustmentForm .= " <font color=red>Kiosk Needs to be Version 2 or greater</font>";
			}
			
			$adjustmentForm .= "</td></tr>";
			
			////adjustment details
			if($sync_version > 1){
				$creditTotal = 0;
				$debitTotal = 0;
				$tranactionArray = array();
				$validate = array();
				
				///insert adjustment record
				if($confirm == 2){
					$today = date("Y-m-d");
				
					$query2 = "SELECT companyid FROM business WHERE businessid = $homeBus";
					$result2 = Treat_DB_ProxyOld::query($query2);
					
					$r2 = mysql_fetch_array($result2);
					$homeComp = $r2["companyid"];
				
					$query2 = "INSERT INTO adjustment (loginid,customerid,businessid,date_time,note) VALUES ($user->loginid,$customerid,$homeBus,NOW(),'$notes')";
					$result2 = Treat_DB_ProxyOld::query($query2);
					$adj_id = mysql_insert_id();
				}
				
				$adjustmentForm .= "<tr><td colspan=2><form action=customerSearch?cid=$companyId#bottom method=post><input type=hidden name=customerid value=$customerid><input type=hidden name=adjBus value=$homeBus><hr width=95%></td></tr>";
			
				///credits
				$query2 = "SELECT cr.creditid,cr.creditname,rc.category,rc.id FROM credits cr 
					JOIN mirror_accounts ma ON ma.accountid = cr.creditid AND ma.type = 1
					JOIN reporting_category rc ON rc.id = cr.category
					WHERE cr.businessid = $homeBus AND cr.is_deleted = 0
					ORDER BY cr.credittype";
				$result2 = Treat_DB_ProxyOld::query($query2);	
				
				while($r2 = mysql_fetch_array($result2)){
					$creditid = $r2["creditid"];
					$creditname = $r2["creditname"];
					$category = $r2["category"];
					$id = $r2["id"];
					
					if($confirm > 0){
						$value = number_format(@$_POST["amt$creditid"],2,'.','');
						$creditTotal += $value;
						$disable = "readonly=\"readonly\"";
						if($value != null){
							$validate['credit'][$id] = $value;
						}
						
						if($confirm == 2 && $value != null){
							$query3 = "INSERT INTO adjustment_detail (adj_id,type,account,amount) VALUES ($adj_id,1,$creditid,'$value')";
							$result3 = Treat_DB_ProxyOld::query($query3);
							
							$query3 = "INSERT INTO creditdetail (companyid,businessid,amount,date,creditid) 
											VALUES ($homeComp,$homeBus,$value,'$today',$creditid)
											ON DUPLICATE KEY UPDATE amount = amount + '$value'";
							$result3 = Treat_DB_ProxyOld::query($query3);
							
							$transactionArray[$id] += $value;
						}
					}
					else{
						$value = '';
						$disable = '';
					}
					
					$adjustmentForm .= "<td align=right>$creditname: </td><td>&nbsp;$<input type=text name=amt$creditid value='$value' size=5 $disable></td></tr>";
				}
				
				///total
				$creditTotal = number_format($creditTotal,2);
				$adjustmentForm .= "<tr><td></td><td><b>&nbsp;$$creditTotal</b></td></tr>";
				$adjustmentForm .= "<tr><td colspan=2><hr width=95%></td></tr>";
				
				///debits
				$query2 = "SELECT db.debitid,db.debitname,rc.category,rc.id FROM debits db 
					JOIN mirror_accounts ma ON ma.accountid = db.debitid AND ma.type = 2
					JOIN reporting_category rc ON rc.id = db.category
					WHERE db.businessid = $homeBus AND db.is_deleted = 0
					ORDER BY db.debittype";
				$result2 = Treat_DB_ProxyOld::query($query2);	
				
				while($r2 = mysql_fetch_array($result2)){
					$debitid = $r2["debitid"];
					$debitname = $r2["debitname"];
					$category = $r2["category"];
					$id = $r2["id"];
					
					if($confirm > 0){
						$value = number_format(@$_POST["amt$debitid"],2,'.','');
						$debitTotal += $value;
						$disable = "readonly=\"readonly\"";
						if($value != null){
							$validate['debit'][$id] = $value;
						}
						
						if($confirm == 2 && $value != null){
							$query3 = "INSERT INTO adjustment_detail (adj_id,type,account,amount) VALUES ($adj_id,2,$debitid,'$value')";
							$result3 = Treat_DB_ProxyOld::query($query3);
							
							$query3 = "INSERT INTO debitdetail (companyid,businessid,amount,date,debitid) 
											VALUES ($homeComp,$homeBus,$value,'$today',$debitid)
											ON DUPLICATE KEY UPDATE amount = amount + '$value'";
							$result3 = Treat_DB_ProxyOld::query($query3);
							
							$transactionArray[$id] += $value;
						}
					}
					else{
						$value = '';
						$disable = '';
					}
					
					$adjustmentForm .= "<td align=right>$debitname: </td><td>&nbsp;$<input type=text name=amt$debitid value='$value' size=5 $disable></td></tr>";
				}
				
				///total
				$debitTotal = number_format($debitTotal,2);
				$adjustmentForm .= "<tr><td></td><td><b>&nbsp;$$debitTotal</b></td></tr>";
				$adjustmentForm .= "<tr><td colspan=2><hr width=95%></td></tr>";
				
				////validate amounts
				$validated = 0;
				$possible = array(26 => 1,20 => 1,19 => 1,17 => 1,15 => 1,8 => 1,7 => 1,5=>1);
				foreach($validate['debit'] AS $cat_id => $value){
					///purchase
					if($cat_id == 1 || $cat_id == 2 || $cat_id == 3 || $cat_id == 6 || $cat_id == 9 || $cat_id == 13 || $cat_id == 28 || $cat_id == 29){
						$temp_validate = 0;
						foreach($possible AS $key => $na){
							if(!array_key_exists($key,$validate['credit'])){
								$temp_validate++;
							}
						}
						if($temp_validate == count($possible)){
							$validated = 1;
						}
					}
					///refund
					elseif($cat_id == 4){
						if(!array_key_exists(24,$validate['credit']) || count($validate['debit']) > 1){
							$validated = 1;
						}
					}
					///cash reload
					elseif($cat_id == 10){
						if(!array_key_exists(25,$validate['credit']) || count($validate['debit']) > 1){
							$validated = 1;
						}
					}
					///kiosk cc reload
					elseif($cat_id == 11){
						if(!array_key_exists(22,$validate['credit']) || count($validate['debit']) > 1){
							$validated = 1;
						}
					}
					///web cc reload
					elseif($cat_id == 12){
						if(!array_key_exists(21,$validate['credit']) || count($validate['debit']) > 1){
							$validated = 1;
						}
					}
					///online ck promo
					elseif($cat_id == 14){
						if(!array_key_exists(23,$validate['credit']) || count($validate['debit']) > 1){
							$validated = 1;
						}
					}
				}
				
				if($confirm == 1 && round($creditTotal,2) == round($debitTotal,2) && $validated == 0){
					$adjustmentForm .= "<tr><td>Notes:<textarea name=notes rows=2 cols=50>$notes</textarea></td><td align=right><font color=blue>Add a Note and Press Submit if Everything is Correct.</font> <input type=hidden name=confirm value=2><input type=submit value='SUBMIT' style=\"border:1px solid #999999;background-color:#E8E7E7;font-size:12px;\"></form></td></tr>";
				}
				elseif($confirm == 1 && $validated != 0){
					$adjustmentForm .= "<tr><td colspan=2 align=right><font color=red>Invalid Entry</font> <input type=hidden name=confirm value=0><input type=submit value='RESET' style=\"border:1px solid #999999;background-color:#E8E7E7;font-size:12px;\"></form></td></tr>";
				}
				elseif($confirm == 1 && round($creditTotal,2) != round($debitTotal,2)){
					$adjustmentForm .= "<tr><td colspan=2 align=right><font color=red>1 Invalid Transaction ($creditTotal/$debitTotal)</font> <input type=hidden name=confirm value=0><input type=submit value='RESET' style=\"border:1px solid #999999;background-color:#E8E7E7;font-size:12px;\"></form></td></tr>";
				}
				/*
				elseif($confirm == 1 && round($creditTotal, 2) == round($debitTotal, 2) ){
					$adjustmentForm .= "<tr><td colspan=2 align=right><font color=red>2 Invalid Transaction ($creditTotal/$debitTotal)</font> <input type=hidden name=confirm value=0><input type=submit value='RESET' style=\"border:1px solid #999999;background-color:#E8E7E7;font-size:12px;\"></form></td></tr>";
				}
				*/
				elseif($confirm == 2){
					$adjustmentForm .= "<tr><td colspan=2 align=right><font color=green>Transaction Completed Successfully </font><input type=hidden name=confirm value=0><input type=submit value='FINISH' style=\"border:1px solid #999999;background-color:#E8E7E7;font-size:12px;\"></form></td></tr>";
				}
				else{
					$adjustmentForm .= "<tr><td colspan=2 align=right><input type=hidden name=confirm value=1><input type=submit value='CONFIRM' style=\"border:1px solid #999999;background-color:#E8E7E7;font-size:12px;\"></form></td></tr>";
				}
				
				///create transactions and update balance
				if($confirm == 2){
					///check number
					$y = date("y");
					$m = date("m");
					$d = date("d");
					$h = date("H");
					$m = date("i");
					$s = date("s");
					$check_number = "8$y$m$d$h$m$s";
					
					$checkTotal = 0;
					$taxTotal = 0;
					$createCheck = 0;
					
					foreach($transactionArray AS $id => $value){
						///sale
						if($id == 8 || $id == 20 || $id == 26 || $id == 19 || $id == 17){
							///generic item
							$query2 = "SELECT pos_id FROM menu_items_new WHERE businessid = $homeBus  AND item_code = 'adjustmentItem'";
							$result2 = Treat_DB_ProxyOld::query($query2);
							
							$r2 = mysql_fetch_array($result2);
							$pos_id = $r2["pos_id"];
							
							///create item
							if($pos_id < 1){
								$query2 = "SELECT MIN(id) AS id FROM menu_groups_new WHERE businessid = $homeBus";
								$result2 = Treat_DB_ProxyOld::query($query2);
								
								$r2 = mysql_fetch_array($result2);
								$group_id = $r2["id"];
								
								///uuid
								$query2 = "SELECT UUID() AS uuid";
								$result2 = Treat_DB_ProxyOld::query($query2);
								
								$r2 = mysql_fetch_array($result2);
								$uuid = $r2["uuid"];
								
								$query2 = "INSERT INTO menu_items_new (businessid,group_id,name,ordertype,active,item_code,upc,uuid_menu_items_new) VALUES
											($homeBus,$group_id,'Adjustment Item',1,1,'adjustmentItem','adjustmentItem','$uuid')";
								$result2 = Treat_DB_ProxyOld::query($query2);
								$min_id = mysql_insert_id();
					
								$query2 = "INSERT INTO menu_items_price (businessid,menu_item_id,pos_price_id,price) VALUES ($homeBus,$min_id,1,1)";
								$result2 = Treat_DB_ProxyOld::query($query2);
								
								$query2 = "SELECT pos_id FROM menu_items_new WHERE id = $min_id";
								$result2 = Treat_DB_ProxyOld::query($query2, true);
								
								$r2 = mysql_fetch_array($result2);
								$pos_id = $r2["pos_id"];
							}
							
							///check detail record
							$query2 = "INSERT INTO checkdetail (businessid,bill_number,item_number,quantity,price) VALUES ($homeBus,$check_number,$pos_id,1,$value)";
							$result2 = Treat_DB_ProxyOld::query($query2);
							
							$checkTotal += $value;
							$createCheck = 1;
							
						}
						///bottle deposit
						elseif($id == 5){
							$query2 = "INSERT INTO checkdiscounts (check_number,businessid,pos_id,amount) VALUES ($check_number,$homeBus,2147483647,'-$value')";
							$result2 = Treat_DB_ProxyOld::query($query2);
							
							$checkTotal += $value;
							$createCheck = 1;
						}
						///tax
						elseif($id == 7){
							$taxTotal += $value;
							$checkTotal += $value;
							$createCheck = 1;
						}
						///promotion/discount
						elseif($id == 3 || $id == 9){
							$query2 = "INSERT INTO checkdiscounts (check_number,businessid,pos_id,amount) VALUES ($check_number,$homeBus,2147483647,$value)";
							$result2 = Treat_DB_ProxyOld::query($query2);
							
							$checkTotal -= $value;
							$createCheck = 1;
						}
						//cc payment
						elseif($id == 1){
							$query2 = "INSERT INTO payment_detail (businessid,check_number,payment_type,received,scancode,customerid) 
										VALUES ($homeBus,$check_number,4,$value,'',0)";
							$result2 = Treat_DB_ProxyOld::query($query2);
							
							$createCheck = 1;
						}
						//cash payment
						elseif($id == 28){
							$query2 = "INSERT INTO payment_detail (businessid,check_number,payment_type,received,scancode,customerid) 
										VALUES ($homeBus,$check_number,1,$value,'',0)";
							$result2 = Treat_DB_ProxyOld::query($query2);
							
							$createCheck = 1;
						}
						//ck payment
						elseif($id == 2){
							$query2 = "INSERT INTO payment_detail (businessid,check_number,payment_type,received,scancode,customerid) 
										VALUES ($homeBus,$check_number,14,$value,'$scancode',$customerid)";
							$result2 = Treat_DB_ProxyOld::query($query2);
							
							$query2 = "UPDATE customer SET balance = balance - $value WHERE customerid = $customerid";
							$result2 = Treat_DB_ProxyOld::query($query2);
						
							$createCheck = 1;
						}
						//pd/voucher
						elseif($id == 6 || $id == 29){
							$query2 = "INSERT INTO payment_detail (businessid,check_number,payment_type,received,scancode,customerid) 
										VALUES ($homeBus,$check_number,5,$value,'$scancode',$customerid)";
							$result2 = Treat_DB_ProxyOld::query($query2);
							
							$query2 = "UPDATE customer SET balance = balance - $value WHERE customerid = $customerid";
							$result2 = Treat_DB_ProxyOld::query($query2);
							
							$createCheck = 1;
						}
						//kiosk cc reload
						elseif($id == 22){
							$query2 = "INSERT INTO checkdetail (businessid,bill_number,item_number,quantity,price) VALUES ($homeBus,$check_number,2147483647,1,$value)";
							$result2 = Treat_DB_ProxyOld::query($query2);
						
							$query2 = "INSERT INTO payment_detail (businessid,check_number,payment_type,received,scancode,customerid) 
										VALUES ($homeBus,$check_number,6,$value,'$scancode',$customerid)";
							$result2 = Treat_DB_ProxyOld::query($query2);
							
							$query2 = "UPDATE customer SET balance = balance + $value WHERE customerid = $customerid";
							$result2 = Treat_DB_ProxyOld::query($query2);
							
							$checkTotal += $value;
							$createCheck = 1;
						}
						//kiosk cash reload
						elseif($id == 25){
							$query2 = "INSERT INTO checkdetail (businessid,bill_number,item_number,quantity,price) VALUES ($homeBus,$check_number,2147483647,1,$value)";
							$result2 = Treat_DB_ProxyOld::query($query2);
						
							$query2 = "INSERT INTO payment_detail (businessid,check_number,payment_type,received,scancode,customerid) 
										VALUES ($homeBus,$check_number,1,$value,'$scancode',$customerid)";
							$result2 = Treat_DB_ProxyOld::query($query2);
							
							$query2 = "UPDATE customer SET balance = balance + $value WHERE customerid = $customerid";
							$result2 = Treat_DB_ProxyOld::query($query2);
							
							$checkTotal += $value;
							$createCheck = 1;
						}
						///online cc
						elseif($id == 21){
							$query2 = "INSERT INTO customer_transactions (oid,trans_type_id,customer_id,subtotal,chargetotal,date_created,response,authresponse)
										VALUES ('adjustment',1,$customerid,$value,$value,NOW(),'APPROVED','$user->username')";
							$result2 = Treat_DB_ProxyOld::query($query2);
							$id = mysql_insert_id();
							
							if($homeBus != $staticBus){
								$query2 = "INSERT INTO customer_transactions_business (ct_id,businessid) VALUES ($id,$homeBus)";
								$result2 = Treat_DB_ProxyOld::query($query2);
							}
							
							$query2 = "UPDATE customer SET balance = balance + $value WHERE customerid = $customerid";
							$result2 = Treat_DB_ProxyOld::query($query2);
						}
						/// online refund
						elseif($id == 24){
							$query2 = "INSERT INTO customer_transactions (oid,trans_type_id,customer_id,subtotal,chargetotal,date_created,response,authresponse)
										VALUES ('adjustment',3,$customerid,$value,$value,NOW(),'APPROVED','$user->username')";
							$result2 = Treat_DB_ProxyOld::query($query2);
							$id = mysql_insert_id();
							
							if($homeBus != $staticBus){
								$query2 = "INSERT INTO customer_transactions_business (ct_id,businessid) VALUES ($id,$homeBus)";
								$result2 = Treat_DB_ProxyOld::query($query2);
							}
							
							$query2 = "UPDATE customer SET balance = balance + $value WHERE customerid = $customerid";
							$result2 = Treat_DB_ProxyOld::query($query2);
						}
						///online promo
						elseif($id == 23){
							$query2 = "INSERT INTO customer_transactions (oid,trans_type_id,customer_id,subtotal,chargetotal,date_created,response,authresponse)
										VALUES ('adjustment',4,$customerid,$value,$value,NOW(),'APPROVED','$user->username')";
							$result2 = Treat_DB_ProxyOld::query($query2);
							$id = mysql_insert_id();
							
							if($homeBus != $staticBus){
								$query2 = "INSERT INTO customer_transactions_business (ct_id,businessid) VALUES ($id,$homeBus)";
								$result2 = Treat_DB_ProxyOld::query($query2);
							}
							
							$query2 = "UPDATE customer SET balance = balance + $value WHERE customerid = $customerid";
							$result2 = Treat_DB_ProxyOld::query($query2);
						}
						///unknown
						else{
						
						}
					}
					///create check record
					if($createCheck == 1){
						$query2 = "INSERT INTO checks (businessid,check_number,emp_no,bill_datetime,bill_posted,total,taxes,received,account_number) 
									VALUES ($homeBus,$check_number,$user->loginid,NOW(),NOW(),$checkTotal,$taxTotal,$checkTotal,'$scancode')";
						$result2 = Treat_DB_ProxyOld::query($query2);
					}
				}
			}
			
			$adjustmentForm .= "</table></center><p>";
			
			$this->_view->assign('adjustmentForm',$adjustmentForm);
			
			///list transactions
			if($showtrans == 1){
				$transList = "<center><a name=trans><div style=\"width:97%;\">";
				$transList .= menu_items::check_detail( $customerid, null, null, true );
				$transList .= "</div></a></center>";
			}
			else{
				$transList .= "<center><div style=\"width:95%;\">";
				$transList .= "<form action=customerSearch?cid=$companyId#trans method=post>";
					$transList .= "<input type=hidden name=customerid value=$customerid>";
					$transList .= "<input type=hidden name=showtrans value=1><input type=hidden name=search_what value='$search_what'><input type=hidden name=search_for value='$search_for'>";
					$transList .= "<input type=submit value='Show Transactions' style=\"border:1px solid #999999;background-color:#E8E7E7;font-size:12px;\">";
				$transList .= "</form>";
				$transList .= "</div></center>";
			}
			
			$this->_view->assign('transList',$transList);
		}
	}
	
	public function customerExport() {
		ini_set('display_errors', false);
		
		$user = $_SESSION->getUser();
		$companyId = $user->getCompanyId();

		if ( $user->customer_utility == 0 ) {
			header('Location: /ta/businesstrack.php');
			exit;
		}

		$this->_view->assign('uri',$this->_uri->getAction());

		$this->addJavascriptAtTop('/assets/js/jquery-1.5.1.min.js');
		$this->addJavascriptAtTop('/assets/js/jquery-ui-1.8.11.custom.min.js');
		$this->addCss('/assets/css/jq_themes/redmond/jqueryui-current.css');
		$this->addCss('/assets/css/jq_themes/redmond/jqueryui-custom.css');

		$query = "SELECT company.companyname,
					company.companyid,
					district.districtname,
					district.districtid,
					business.businessname,
					business.businessid
					FROM business
					JOIN district ON district.districtid = business.districtid
					JOIN company ON company.companyid = business.companyid
					WHERE business.categoryid = 4
					ORDER BY company.companyname,district.districtname,business.businessname";
		$result = Treat_DB_ProxyOldProcessHost::query( $query );
	
		$kView = "<tr bgcolor=#E8E7E7 valign=top><td><form action=customerExportResults method=post><select name=export_what[] multiple='multiple' size=20>";
	
		while($r = mysql_fetch_array($result)){
			$businessid = $r["businessid"];
			$businessname = $r["businessname"];
			$districtid = $r["districtid"];
			$districtname = $r["districtname"];
			$companyid = $r["companyid"];
			$companyname = $r["companyname"];
		
			if($companyid != $lastcompanyid){
				if(is_array($export_what) && in_array("3:$companyid",$export_what)){$sel="SELECTED";}
				else{$sel = "";}
				$kView .= "<option value=3:$companyid $sel>$companyname</option>";
			}
			if($districtid != $lastdistrictid){
				if(is_array($export_what) && in_array("2:$districtid",$export_what)){$sel="SELECTED";}
				else{$sel = "";}
				$kView .= "<option value=2:$districtid $sel>&nbsp;&nbsp;&nbsp;&nbsp;$districtname</option>";
			}
		
			if(is_array($export_what) && in_array("1:$businessid",$export_what)){$sel="SELECTED";}
			else{$sel = "";}
			$kView .= "<option value=1:$businessid $sel>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$businessname</option>";
		
			$lastdistrictid = $districtid;
			$lastcompanyid = $companyid;
		}
		$kView .= "</select><br><input type=submit value='Export Customer List' style=\"border:1px solid #999999;background-color:#E8E7E7;font-size:12px;\"></form></td></tr>";
		
		$this->_view->assign('kView',$kView);
	}
	
	public function customerExportResults() {
		ini_set('display_errors', false);
		
		$user = $_SESSION->getUser();
		$companyId = $user->getCompanyId();

		if ( $user->customer_utility == 0 ) {
			header('Location: /ta/businesstrack.php');
			exit;
		}

		$this->_view->assign('uri',$this->_uri->getAction());
		
		$export_what = $_POST["export_what"];
		
		foreach($export_what AS $key => $value){
			$get_cust = explode(":",$value);

			if($get_cust[0] == 1){
				$query = "SELECT customer.username,customer.first_name,customer.last_name,customer.scancode FROM customer JOIN business ON business.businessid = customer.businessid AND business.categoryid = 4 WHERE customer.businessid = $get_cust[1] AND customer.notify_via_email != 0";
				$result = Treat_DB_ProxyOldProcessHost::query($query);

				while($r = mysql_fetch_array($result)){
					$email = $r["username"];
					$first_name = $r["first_name"];
					$last_name = $r["last_name"];
					$scancode = $r["scancode"];
					$data .= "\"$email\",\"$first_name\",\"$last_name\",\"$scancode\"\r\n";
				}
			}
			elseif($get_cust[0] == 2){
				$query = "SELECT customer.username,customer.first_name,customer.last_name,customer.scancode FROM customer
							JOIN business ON business.businessid = customer.businessid AND business.districtid = $get_cust[1] AND business.categoryid = 4
							WHERE customer.notify_via_email != 0";
				$result = Treat_DB_ProxyOldProcessHost::query($query);

				while($r = mysql_fetch_array($result)){
					$email = $r["username"];
					$first_name = $r["first_name"];
					$last_name = $r["last_name"];
					$scancode = $r["scancode"];
					$data .= "\"$email\",\"$first_name\",\"$last_name\",\"$scancode\"\r\n";
				}
			}
			elseif($get_cust[0] == 3){
				$query = "SELECT customer.username,customer.first_name,customer.last_name,customer.scancode FROM customer
							JOIN business ON business.businessid = customer.businessid AND business.companyid = $get_cust[1] AND business.categoryid = 4
							WHERE customer.notify_via_email != 0";
				$result = Treat_DB_ProxyOldProcessHost::query($query);

				while($r = mysql_fetch_array($result)){
					$email = $r["username"];
					$first_name = $r["first_name"];
					$last_name = $r["last_name"];
					$scancode = $r["scancode"];
					$data .= "\"$email\",\"$first_name\",\"$last_name\",\"$scancode\"\r\n";
				}
			}
		}
	
		$file="customers.csv";
		header("Content-type: application/x-msdownload");
		header("Content-Disposition: attachment; filename=$file");
		header("Pragma: no-cache");
		header("Expires: 100");
		echo $data;
	}
	
	public function businessGroups() {
		ini_set('display_errors', false);
		
		$user = $_SESSION->getUser();
		$companyId = $user->getCompanyId();

		if ( $user->customer_utility == 0 ) {
			header('Location: /ta/businesstrack.php');
			exit;
		}

		$this->_view->assign('uri',$this->_uri->getAction());
		
		$currentGroup = $_POST["group_id"];
		if($currentGroup == ""){
			$currentGroup = $_GET["cGrp"];
		}

		$this->addJavascriptAtTop('/assets/js/jquery-1.5.1.min.js');
		$this->addJavascriptAtTop('/assets/js/jquery-ui-1.8.11.custom.min.js');
		$this->addCss('/assets/css/jq_themes/redmond/jqueryui-current.css');
		$this->addCss('/assets/css/jq_themes/redmond/jqueryui-custom.css');
		
		$query = "SELECT * FROM business_group ORDER BY group_name";
		$result = Treat_DB_ProxyOld::query( $query );
		
		if($currentGroup == -1){
			$newSel = "SELECTED";
		}
		else{
			$newSel = "";
		}
		
		$groupList = "<option value=0>Please Choose....</option><option value=-1 $newSel>Add New Group</option>";
		
		while($r = mysql_fetch_array($result)){
			$group_id = $r["group_id"];
			$group_name = $r["group_name"];
			
			if($currentGroup == $group_id){
				$sel = "SELECTED";
				$currentGroupName = $group_name;
			}
			else{
				$sel = "";
			}
			
			$groupList .= "<option value=$group_id $sel>$group_name</option>";
		}
		
		$this->_view->assign('groupList',$groupList);
		
		if($currentGroup != 0){
			$groupTable = "<center><table width=95% cellspacing=0 cellpadding=0 style=\"border:2px solid #999999;\" bgcolor=white>
							<tr valign=top bgcolor=#999999>
								<td>
									<font color=white>Details</font>
								</td>
							</tr>
							<tr valign=top>
								<td>
									<form action=businessGroupsUpdate?cid=$companyId method=POST>
										<input type=hidden name=todo value=1> 
										<input type=hidden name=group_id value=$currentGroup>
										<input type=text name=groupName value='$currentGroupName' size=50>
										<input type=submit value='SAVE' style=\"border:1px solid #999999;background-color:#E8E7E7;font-size:12px;\">
									</form>
								</td>
							</tr>";
							
			if($currentGroup > 0){
				$groupTable .= "<tr valign=top bgcolor=#999999>
								<td>
									<font color=white>Members</font>
								</td>
							</tr>";
							
				$query = "SELECT b.businessid, b.businessname, bgh.name FROM business b
							JOIN business_group_detail bgd ON bgd.businessid = b.businessid
							LEFT JOIN business_group_hierarchy bgh ON bgh.id = bgd.node
							WHERE bgd.group_id = $currentGroup
							ORDER BY bgh.name DESC, b.businessname DESC";
				$result = Treat_DB_ProxyOld::query( $query );
				
				while($r = mysql_fetch_array($result)){
					$businessid = $r["businessid"];
					$businessname = $r["businessname"];
					$nodeName = $r["name"];
					
					if($nodeName == ""){
						$nodeName = "<font color=red>[Unassigned]</font>";
					}
					else{
						$nodeName = "<font color=blue>[$nodeName]</font>";
					}
					
					$groupTable .= "<tr><td>
										<form action=businessGroupsUpdate?cid=$companyId method=post style=\"margin:0;padding:0;display:inline;\">
											<input type=hidden name=todo value=3>
											<input type=hidden name=group_id value=$currentGroup>
											<input type=hidden name=businessid value=$businessid>
											<input type=submit value='X' style=\"border:1px solid #999999;background-color:#E8E7E7;font-size:12px;\">
											</form>
									&nbsp;$businessname $nodeName</td></tr>";
				}
				
				////add members
				$groupTable .= "<tr><td><form action=businessGroupsUpdate?cid=$companyId method=POST>
								<input type=hidden name=todo value=2>
								<input type=hidden name=group_id value=$currentGroup>
								<select name=businessid>";
								
				$query = "SELECT businessid,businessname FROM business WHERE companyid != 2 ORDER BY businessname";
				$result = Treat_DB_ProxyOld::query( $query );
				
				while($r = mysql_fetch_array($result)){
					$businessid = $r["businessid"];
					$businessname = $r["businessname"];
					
					$groupTable .= "<option value=$businessid>$businessname</option>";
				}
				
				$groupTable .= "</select><select name=sub><option value=0>Please Choose...</option>";
				
				////node of
				$groupTable .= static::businessHierarchy($currentGroup, 0, 1, 1);
				
				$groupTable .= "</select><input type=submit value='ADD' style=\"border:1px solid #999999;background-color:#E8E7E7;font-size:12px;\">
							</form></td></tr>";
							
				///hierarchy
				$groupTable .= "<tr valign=top bgcolor=#999999>
								<td>
									<font color=white>Hierarchy</font>
								</td>
							</tr>";
							
				$groupTable .= "<tr><td>";
				
				$groupTable .= "<ul><li>$currentGroupName";
				
				///detailed  hierarchy
				$groupTable .= static::businessHierarchy($currentGroup, 0, 0, 0);
				
				$groupTable .= "</ul>";
				
				$groupTable .= "</td></tr>";
				
				///add node
				$groupTable .= "<tr><td><form action=businessGroupsUpdate?cid=$companyId method=POST>
								<input type=hidden name=todo value=4>
								<input type=hidden name=group_id value=$currentGroup>
								<input type=text size=50 name=nodeName> 
								<select name=sub><option value=-1>Node of....</option>
									<option value=0>$currentGroupName</option>";
				
				$groupTable .= static::businessHierarchy($currentGroup, 0, 1, 0);
				
				$groupTable .= "</select>
								<input type=submit value='ADD' style=\"border:1px solid #999999;background-color:#E8E7E7;font-size:12px;\">
								</form>
								</td></tr>";
				
				///remove node				
				$groupTable .= "<tr><td><form action=businessGroupsUpdate?cid=$companyId method=POST>
								<input type=hidden name=todo value=5>
								<input type=hidden name=group_id value=$currentGroup>
								<select name=sub><option value=0>Please Choose....</option>";
				
				$query = "SELECT bgh.id,bgh.name,COUNT(bgd.businessid) AS total,COUNT(bgh2.id) AS children FROM business_group_hierarchy bgh 
						LEFT JOIN business_group_detail bgd ON bgd.node = bgh.id
						LEFT JOIN business_group_hierarchy bgh2 ON bgh2.parent = bgh.id
						WHERE bgh.group_id = $currentGroup
						GROUP BY bgh.id
						HAVING total = 0 AND children = 0";
				$result = Treat_DB_ProxyOld::query( $query );
				
				while($r = mysql_fetch_array($result)){
					$nodeId = $r["id"];
					$nodeName = $r["name"];
					
					$groupTable .= "<option value=$nodeId>$nodeName</option>";
				}
				
				$groupTable .= "</select>
								<input type=submit value='REMOVE' style=\"border:1px solid #999999;background-color:#E8E7E7;font-size:12px;\">
								</form></td></tr>";
			}
						
			$groupTable .= "</table></center>";
						
			$this->_view->assign('groupTable',$groupTable);
		}
	}
	
	public function businessGroupsUpdate() {
		ini_set('display_errors', false);
		
		$user = $_SESSION->getUser();
		$companyId = $user->getCompanyId();

		if ( $user->customer_utility == 0 ) {
			header('Location: /ta/businesstrack.php');
			exit;
		}
		
		$currentGroup = $_POST["group_id"];
		$todo = $_POST["todo"];
		$groupName = $_POST["groupName"];
		$businessid = $_POST["businessid"];
		$nodeName = $_POST["nodeName"];
		$sub = $_POST["sub"];
		
		if($todo == 1){
			if($currentGroup < 1 && $groupName != ""){
				$query = "INSERT INTO business_group (group_name) VALUES ('$groupName')";
				$result = Treat_DB_ProxyOld::query( $query );
				
				$currentGroup = Treat_DB_ProxyOld::mysql_insert_id();
			}
			elseif($currentGroup > 0 && $groupName != ""){
				$query = "UPDATE business_group SET group_name = '$groupName' WHERE group_id = $currentGroup";
				$result = Treat_DB_ProxyOld::query( $query );
			}
		}
		elseif($todo == 2){
			$query = "INSERT INTO business_group_detail (group_id, businessid, node) VALUES ($currentGroup, $businessid, $sub)";
			$result = Treat_DB_ProxyOld::query( $query );
		}
		elseif($todo == 3){
			$query = "DELETE FROM business_group_detail WHERE group_id = $currentGroup AND businessid = $businessid";
			$result = Treat_DB_ProxyOld::query( $query );
		}
		elseif($todo == 4 && $sub != -1 && $nodeName != ""){
			$query = "INSERT INTO business_group_hierarchy (group_id, name, parent) VALUES ($currentGroup, '$nodeName', $sub)";
			$result = Treat_DB_ProxyOld::query( $query );
		}
		elseif($todo == 5 && $sub > 0){
			$query = "DELETE FROM business_group_hierarchy WHERE id = $sub";
			$result = Treat_DB_ProxyOld::query( $query );
		}
		
		sleep(1);
		
		header("Location: businessGroups?cid=$companyId&cGrp=$currentGroup");
	}
	
	public static function businessHierarchy($group_id, $parent, $option, $nodeOnly) {
		if($parent == 0){$html = "";}
	
		$query = "SELECT * FROM business_group_hierarchy WHERE group_id = $group_id AND parent = $parent ORDER BY name";
		$result = Treat_DB_ProxyOld::query($query);
		
		while($r = mysql_fetch_array($result)){
			$nodeId = $r["id"];
			$nodeName = $r["name"];
			
			////has children?
			$query2 = "SELECT COUNT(id) AS children FROM business_group_hierarchy WHERE parent = $nodeId";
			$result2 = Treat_DB_ProxyOld::query($query2);

			$children = @mysql_result($result2,0,"children");
			
			$query2 = "SELECT COUNT(businessid) AS businesses FROM business_group_detail WHERE node = $nodeId";
			$result2 = Treat_DB_ProxyOld::query($query2);

			$businesses = @mysql_result($result2,0,"businesses");
			
			if($option > 0){
				$display = "";
				for($counter = 1; $counter <= $option; $counter++){
					$display .= "&nbsp;";
				}
				
				if($children > 0 && $nodeOnly == 1){
					$disabled = "DISABLED";
				}
				elseif($businesses > 0 && $nodeOnly != 1){
					$disabled = "DISABLED";
				}
				else{
					$disabled = "";
				}
				
				$html .= "<option value=$nodeId $disabled>$display$nodeName</option>";
				$html .= static::businessHierarchy($group_id, $nodeId, $option+4, $nodeOnly);
			}
			else{
				$html .= "<ul><li>$nodeName";
				if($children == 0){
					$query2 = "SELECT b.businessname FROM business b
							JOIN business_group_detail bgd ON bgd.businessid = b.businessid 
							AND bgd.group_id = $group_id 
							AND node = $nodeId
							ORDER BY b.businessname";
					$result2 = Treat_DB_ProxyOld::query($query2);
					
					$html .= "<ul>";
					while($r2 = mysql_fetch_array($result2)){
						$businessname = $r2["businessname"];
						
						$html .= "<li>$businessname";
					}
					$html .= "</ul>";
				}
				else{
					$html .= static::businessHierarchy($group_id, $nodeId, $option);
				}
				$html .= "</ul>";
			}
		}
		
		return $html;
	}
}

