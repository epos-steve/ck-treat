<?php

/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of history
 *
 * @author Eric Gach <eric@php-oop.net>
 */
class Ajax_Hierarchy_HistoryController extends Treat_Controller_Abstract
{
	protected $_argMap = array(
		'update' => array('id', 'type', 'isShown')
	);

	public function update()
	{
		if (!isset($_SESSION['hierarchy'])) {
			$_SESSION['hierarchy'] = array('t' => array(), 'c' => array(), 'd' => array());
		}

		$_SESSION->hierarchy[$this->_args['type']][$this->_args['id']] = (bool)$this->_args['isShown'];
		return(false);
	}
}
