<?php
require_once( DOC_ROOT . '/../lib/inc/SoapDiscovery.class.php' );
ini_set( 'soap.wsdl_cache_enabled', 0 );

class Smartmenu_PackageController extends Treat_Controller_Abstract {

	/**
	 * @var SoapServer
	 */
	protected $soapServer;

	protected $serviceName = 'smartMenu_Package';
	protected $serviceNamespace = 'http://www.essential-elements.net/package';
	
	public function init( ) {
		$this->_passedPageSecurityCheck = false;
	}

	public function service( ) {
		$class = get_called_class( );

		try {
			$this->soapServer = @new SoapServer( $this->_uri->getScheme( )
												. '://'
												. $this->_uri->getHost( )
												. '/'
												. $this->_uri->getController( )
												. '/'
												. 'wsdl' );
		} catch( Exception $e ) {
			throw new SiTech_Exception( $e->getMessage( ) );
		}
		
		$serverObject = new Smartmenu_PackageController_SOAP;
		$serverObject->serverURI = $this->_uri;
		$this->soapServer->setObject( $serverObject );
		
		//$this->soapServer->setClass( $class . '_SOAP' );

		$this->soapServer->handle( );
		return false;
	}

	public function wsdl( ) {
		$class = get_called_class( );

		$disco = new SoapDiscovery( $class . '_SOAP', $this->serviceName, $this->serviceNamespace );

		header( 'Content-type: text/xml' );
		echo $disco->getWSDL( $this->_uri->getHost( )
							. '/'
							. $this->_uri->getController( )
							. '/'
							. 'service' );

		return false;
	}
	
	public function retrieve( ) {
		$id = intval( $_REQUEST['id'] );
		$request = Treat_Model_SmartMenu_PackageRequest::getById( $id );
		
		if( !$request ) {
			throw new SiTech_Exception( 'Access denied' );
		}
		
		try {
			$package = Treat_Model_SmartMenu_Package::getByPackageFile( $request->packageFile );
			$packageData = $package->retrieve( );
			
			header( 'Content-type: application/octet-stream' );
			header( 'Content-Disposition: attachment; filename="' . $request->packageFile . '.eepx"' );
			header( 'Pragma: no-cache' );
			header( 'Cache-Control: no-cache, must-revalidate' );
			header( 'Expires: -1' );
			echo $packageData;
			
			$request->delete( );
		} catch( Exception $e ) {
			throw new SiTech_Exception( $e->getMessage( ) );
		}

		return false;
	}

	public function upload()
	{
		$id = $this->_args[0];
		$package = Treat_Model_SmartMenu_Upload::get('MD5(CONCAT(createdTime, packageFile)) = '.$id);
		$packageData = file_get_contents('php://input');
		
		try {
			Treat_Model_SmartMenu_Package::create($package->createdBy, $package->packageFile, $packageData);
			$package->delete();
		} catch( PDOException $e ) {
			$errorArray = Treat_Model_SmartMenu_Package::$db->errorInfo();
			print_r($errorArray);
			return;
		} catch(Exception $e) {
			throw new SoapFault($e->getMessage(), 500);
		}

		return(false);
	}
}

class Smartmenu_PackageController_SOAP {
	static protected $client;
	
	public $serverURI;
	
	/**
	 *
	 * smart menu upload function
	 *
	 * @param string $packageFile
	 * @throws SoapFault
	 * @return string
	 */
	public function upload( $packageFile ) {
		if( !static::$client )
			throw new SoapFault( 'Access denied', 403 );
		extract( (array)$packageFile, EXTR_OVERWRITE );
		
		try {
			if (is_object($packageFile)) $packageFile = $packageFile->packageFile;
			$upload = Treat_Model_SmartMenu_Upload::create($packageFile, static::$client);
			$url = $this->serverURI->getScheme()
							. '://'
							. $this->serverURI->getHost()
							. '/'
							. $this->serverURI->getController()
							. '/upload/'.md5($upload->createdTime.$packageFile);
		} catch (PDOException $e) {
			print_r(Treat_Model_SmartMenu_Upload::db()->errorInfo());
			return;
		} catch (Exception $e) {
			throw new SoapFault($e->getMessage(), 500);
		}

		return array('return' => $url);
	}

	/**
	 *
	 * smart menu soap authentication
	 * @param mixed $usernameOrObj
	 * @param string $password
	 * @throws SoapFault
	 * @return boolean
	 */
	static public function authenticate( $usernameOrObj, $password ) {
		if( is_object( $usernameOrObj ) ) {
			$loginKey = $usernameOrObj->username;
			$loginSecret = $usernameOrObj->password;
		} else {
			$loginKey = $usernameOrObj;
			$loginSecret = $password;
		}

		$auth = Treat_Model_SmartMenu_Auth::connect( $loginKey, $loginSecret );

		if( !$auth ) {
			throw new SoapFault( 'Access denied', 401 );
		}
                
            
		static::$client = $auth->getClient( );

		return true;
	}

	/**
	 *
	 * smart menu retrieve function
	 * @param string $packageFile
	 * @throws SoapFault
	 * @return string
	 * @SOAPreturn base64Binary
	 */
	public function retrieve( $packageFile ) {
		if( !static::$client )
			throw new SoapFault( 'Access denied', 403 );
		extract( (array)$packageFile, EXTR_OVERWRITE );
		
		try {
			$package = Treat_Model_SmartMenu_Package::getByPackageFile( $packageFile );
			$packageData = $package->retrieve( );
			
			$soapResponse = new SoapVar( $packageData, XSD_BASE64BINARY );
		} catch( Exception $e ) {
			throw new SoapFault( $e->getMessage( ), 500 );
		}

		return array( 'return' => $soapResponse );
	}

	/**
	 *
	 * smart menu destroy function
	 * @param string $packageFile
	 * @throws SoapFault
	 * @return string
	 */
	public function destroy( $packageFile ) {
		if( !static::$client )
			throw new SoapFault( 'Access denied', 403 );
		extract( (array)$packageFile, EXTR_OVERWRITE );
		
		try {
			Treat_Model_SmartMenu_Package::destroy( static::$client, $packageFile );
		} catch( Exception $e ) {
			throw new SoapFault( $e->getMessage( ), 500 );
		}

		return array( 'return' => 'success' );
	}

	/**
	 *
	 * smart menu link function
	 * @param string $boardGuid
	 * @param string $packageFile
	 * @param string $locationGuid
	 * @throws SoapFault
	 * @return string
	 */
	public function link( $boardGuid, $packageFile, $locationGuid = NULL ) {
		if( !static::$client )
			throw new SoapFault( 'Access denied', 403 );
		extract( (array)$boardGuid, EXTR_OVERWRITE );
		
		try {
			Treat_Model_SmartMenu_PackageLink::link( $boardGuid, $packageFile, $locationGuid );
		} catch( Exception $e ) {
			throw new SoapFault( $e->getMessage( ), 500 );
		}

		return array( 'return' => 'success' );
	}

	/**
	 *
	 * smart menu unlink function
	 * @param string $boardGuid
	 * @param string $packageFile
	 * @throws SoapFault
	 * @return string
	 */
	public function unlink( $boardGuid, $packageFile ) {
		if( !static::$client )
			throw new SoapFault( 'Access denied', 403 );
		extract( (array)$boardGuid, EXTR_OVERWRITE );
		
		try {
			$package = Treat_Model_SmartMenu_PackageLink::getByBoardAndPackage( $boardGuid, $packageFile );
			$package->delete( );
		} catch( Exception $e ) {
			throw new SoapFault( $e->getMessage( ), 500 );
		}

		return array( 'return' => 'success' );
	}

	/**
	 *
	 * smart menu request function
	 * @param string $boardGuid
	 * @throws SoapFault
	 * @return SoapVar
	 * @xSOAPreturn SOAP-ENC:Struct
	 * @SOAPreturn PackageDetails[]
	 */
	public function request( $boardGuid ) {
		if( !static::$client )
			throw new SoapFault( 'Access denied', 403 );
		extract( (array)$boardGuid, EXTR_OVERWRITE );
		
		try {
			$packageLinks = Treat_Model_SmartMenu_PackageLink::getByBoardGuid( $boardGuid );
			$baseURI = $this->serverURI->getScheme( )
							. '://'
							. $this->serverURI->getHost( )
							. '/'
							. $this->serverURI->getController( )
							. '/retrieve?id=';
			$soapResponse = array( );
			
			foreach( $packageLinks as $packageLink ) {
				$package = $packageLink->getPackage( );
				
				$request = Treat_Model_SmartMenu_PackageRequest::create( $package->packageFile );
				$requestURI = $baseURI . $request->id;
				
				//$response = new Smartmenu_PackageController_Response;
				//$response->set( 'packageFile', $package->packageFile );
				//$response->set( 'modifiedTime', $package->modifiedTime, XSD_DATETIME );
				//$response->set( 'requestURI', $requestURI, XSD_ANYURI );
				
				$response = new PackageDetails( $package->packageFile,
												 date( 'c', strtotime( $package->modifiedTime ) ),
												 $requestURI );
				//$response->packageFile = $package->packageFile;
				//$response->modifiedTime = new SoapVar( date( 'c', strtotime( $package->modifiedTime ) ), XSD_DATETIME );
				//$response->requestURI = new SoapVar( $requestURI, XSD_ANYURI );
				
				//$soapResponseArray[] = new SoapVar( $response, SOAP_ENC_OBJECT, 'tns:PackageDetails' );
				$soapResponse[] = $response;
			}
			
			//$soapResponse = new SoapVar( $soapResponseArray, SOAP_ENC_ARRAY, 'tns:ArrayOfPackageDetails' );
		} catch( Exception $e ) {
			throw new SoapFault( $e->getMessage( ), 500 );
		}
		
		return array( 'return' => $soapResponse );
	}
	
	/**
	 *
	 * STUPID .NET!!!!!! smart menu request function
	 * @param string $boardGuid
	 * @throws SoapFault
	 * @return string
	 */
	public function requestDumbString( $boardGuid ) {
		if( !static::$client )
			throw new SoapFault( 'Access denied', 403 );
		extract( (array)$boardGuid, EXTR_OVERWRITE );
		
		try {
			$packageLinks = Treat_Model_SmartMenu_PackageLink::getByBoardGuid( $boardGuid );
			$baseURI = $this->serverURI->getScheme( )
							. '://'
							. $this->serverURI->getHost( )
							. '/'
							. $this->serverURI->getController( )
							. '/retrieve?id=';
			$soapResponseArray = array( );
			
			foreach( $packageLinks as $packageLink ) {
				$package = $packageLink->getPackage( );
				
				$request = Treat_Model_SmartMenu_PackageRequest::create( $package->packageFile );
				$requestURI = $baseURI . $request->id;
				
				$soapResponseArray[] = 'packageFile=' . $package->packageFile
									. '|modifiedTime=' . date( 'c', strtotime( $package->modifiedTime ) )
									. '|requestUri=' . $requestURI;
			}
			
			$soapResponse = implode( '#', $soapResponseArray );
		} catch( Exception $e ) {
			throw new SoapFault( $e->getMessage( ), 500 );
		}
		
		return array( 'return' => $soapResponse );
	}
	
	/**
	 *
	 * smart menu control function
	 * @param string $boardGuid
	 * @throws SoapFault
	 * @return string
	 */
	public function control( $boardGuid ) {
		if( !static::$client )
			throw new SoapFault( 'Access denied', 403 );
		extract( (array)$boardGuid, EXTR_OVERWRITE );
		
		try {
			$collection = Treat_Model_SmartMenu_Control::getCollectionByBoardGuid( $boardGuid );
			$xml = $collection->toXml( );
		} catch( Exception $e ) {
			throw new SoapFault( $e->getMessage( ), 500 );
		}
		
		return array( 'return' => $xml );
	}
	
	/**
	 *
	 * smart menu simpleMenu function
	 * @param int $menuId
	 * @throws SoapFault
	 * @return string
	 */
	public function simpleMenu( $menuId ) {
		if( !static::$client )
			throw new SoapFault( 'Access denied', 403 );
		extract( (array)$menuId, EXTR_OVERWRITE );
		
		try {
			$menu = Treat_Model_SmartMenu_Menu::getById( $menuId );
			$collection = Treat_Model_SmartMenu_MenuItem::getCollectionByMenuId( $menuId, Treat_Model_SmartMenu_MenuItem_Collection::SIMPLE_MENU );
			$xml = $collection->toXml( $menu->menuName );
		} catch( Exception $e ) {
			throw new SoapFault( $e->getMessage( ), 500 );
		}
		
		return array( 'return' => $xml );
	}
	
	/**
	 * @throws SoapFault
	 * @return string[]
	 */
	public function locations( ) {
		if( !static::$client )
			throw new SoapFault( 'Access denied', 403 );
		
		try {
			$locations = Treat_Model_SmartMenu_Location::getByCompanyid( static::$client->getCompanyId( ) );
			Treat_Model_Abstract::collapse( $locations, 'locationGuid' );
		} catch( Exception $e ) {
			throw new SoapFault( $e->getMessage( ), 500 );
		}
		
		return array( 'return' => $locations );
	}
}

class PackageDetails {
	/**
	 * @var string
	 */
	var $packageFile;
	
	/**
	 * @var string
	 * @SOAPvar dateTime
	 */
	var $modifiedTime;
	
	/**
	 * @var string
	 * @SOAPvar anyURI
	 */
	var $requestUri;
	
	function __construct( $packageFile, $modifiedTime, $requestUri ) {
		$this->packageFile = $packageFile;
		$this->modifiedTime = $modifiedTime;
		$this->requestUri = $requestUri;
	}
}
