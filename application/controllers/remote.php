<?php
/**
 * Remote controller
 * 
 * Controller for handling remote access requests
 * 
 * @author		Ryan Pessa <ryan@essential-elements.net>
 * @copyright	Copyright (c) 2013 Essential Elements, LLC (http://www.essential-elements.net)
 * @license		EE-Proprietary
 */

if( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname( __FILE__ ) . '/../../htdocs/' ) );
}

class RemoteController extends Treat_Controller_Abstract {
	protected $_client;
	
	protected $_debug = false;
	
	public function init( ) {
		if( isset( $_GET['debug'] ) ) {
			$this->_debug = true;
		} else {
			ini_set( 'display_errors', 'false' );
		}
		
		$this->_user = $_SESSION->getUser( );
		$this->_client = Treat_Model_ClientPrograms::get( (int)$this->_args[0], true );
	}
	
	protected function _connect( $protocol, array $extraParams = array() ) {
		if( !$this->_allowAccess( ) || $this->_client == null ) {
			echo 'Access Denied';
			return;
		}
		
		$name = sprintf( 'aeris%05d' , $this->_client->client_programid );
		$connId = $name . '-' . $protocol;
		$hostname = $name . '.vpn';
		
		if( $this->_debug )
			print '<a href="' . \EE\Guacamole::generateUrl( $connId, $protocol, $hostname, $extraParams ) . '">' . $name . '</a>';
		else
			header( 'Location: ' . \EE\Guacamole::generateUrl( $connId, $protocol, $hostname, $extraParams ) );
		return;
	}
	
	public function vnc( ) {
		$this->_connect( 'vnc' );
	}
	
	public function ssh( ) {
		$this->_connect( 'ssh', array( 'guac.username' => 'aeris2' ) );
	}
	
	protected function _passedPageSecurityCheck( ) {
		return $this->_allowAccess( );
	}
	
	protected function _allowAccess( ) {
		if( !$this->_isLoggedIn( ) )
			return false;
		
		return static::allowAccess( $this->_user, $this->_client->client_programid );
	}
	
	public static function allowAccess( $user, $cpid ) {
		if( !$user )
			return false;
		
		
		$security = \EE\Model\User\Security::getSecurityList( $user->security, 'stdClass' );
		if( !$security )
			return false;
		
		if( $user->security_level >= 9 )
			return true;
		
		if( !$security[0]->posmgmt_setup )
			return false;
		
		// TODO: check that $user has access to machine $cpid
		
		return false;
	}
}

