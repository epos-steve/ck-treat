<?php
class DefaultController extends Treat_Controller_Abstract {
	
	public function index() {
		// this page has it's own layout
#		$this->_layout = '';
		require_ta_bootstrap();
		
		$this->_view->assign( 'title', 'Essential Elements' );
		
		require_once( SITECH_APP_PATH . '/controllers/ta/user.php');
#		$this->_view->assign( 'form_action', Treat_Loader::createURI( Ta_UserController::LOGIN_SUBMIT_PAGE ) );
		$this->_view->assign( 'form_action', Treat_Loader::createURI( 'ta/businesstrack.php' ) );
	}
	
}

