<?php

use EE\Model\Inventory\Item as InventoryItem;
use EE\Model\Nutrition\RecipeItem;

class Nutrition_RecipeController
	extends \EE\Controller\Base
{
	protected $_noLoginRequired = array(
		'nutrition/recipe' => array(
			'ajaxautocom_php' => true,
			'eip' => true,
			'menu_nutrition' => true,
			'menu_nutrition2' => true,
			'printer_friendly' => true,
		),
	);
	
	
	public function ajaxautocom_php() {
		header( 'Content-type: text/javascript' );
		$this->_layout = '';
#		return false;
	}
	
	
	public function business_inventory4() {
		$this->_layout = '';
	}
	
	
	public function conversion() {
		$business_id = $this->getPostOrGetVariable( array(
			'business_id',
			'businessid',
			'bid',
		) );
		if ( $_POST && $_POST['item'] ) {
			
			foreach ( $_POST['item'] as $item ) {
				$obj = new Treat_Model_Nutrition_RecipeConvertObject();
				$good_data = $obj->setFormData( $item );
				if ( defined( 'IN_PRODUCTIN' ) && !IN_PRODUCTION ) {
					if ( $obj->rec_num && $obj->rec_size && $obj->rec_order ) {
						echo '<pre>';
						var_dump( $obj );
						echo '</pre>';
					}
				}
				if ( $good_data ) {
					$obj->save();
				}
			}
			
			$location = sprintf(
				'/ta/nutrition/recipe/conversion?bid=%d'
				,$business_id
			);
			header( 'Location: ' . $location );
			
			return false;
		}
		else {
			$list = Treat_Model_Nutrition_RecipeItem::getListForNewNutritionByBusinessId( $business_id );
			$this->_view->assign( 'list', $list );
		}
	}
	
	
	public function conversion_list() {
		$business_id = $this->getPostOrGetVariable( array(
			'business_id',
			'businessid',
			'bid',
		) );
		$list = Treat_Model_Nutrition_RecipeItem::getListForNewNutritionByBusinessId( $business_id );
		$this->_view->assign( 'list', $list );
		
#		echo '<pre>';
#		var_dump( $list );
#		echo '</pre>';
		
#		return false;
	}
	
	
	public function delete() {
		if ( $_POST ) {
			$user = getSessionCookieVariable( 'usercook' );
			$pass = getSessionCookieVariable( 'passcook' );
			
			$businessid = $this->getPostOrGetVariable( array( 'business_id', 'businessid', 'bid' ) );
			$companyid  = $this->getPostOrGetVariable( array( 'company_id', 'companyid', 'cid' ) );
			$editid     = $this->getPostOrGetVariable( array( 'edit_id', 'editid' ) );
			$minId = static::getPostOrGetVariable( 'minid' );
			$recipe_id  = $this->getPostOrGetVariable( array( 'recipe_id', 'recipeid' ) );
			$edit       = $this->getPostOrGetVariable( 'edit' );
			$item_price = $this->getPostOrGetVariable( 'item_price' );
			$batch      = $this->getPostOrGetVariable( 'batch' );
			$showvendor = $this->getPostOrGetVariable( array( 'show_vendor', 'showvendor' ) );
			
			$rec_num    = $this->getPostVariable( 'rec_num' );
			$inv_itemid = $this->getPostVariable( array( 'inv_item_id', 'inv_itemid' ) );
			$srv_num    = $this->getPostVariable( 'srv_num' );
			$rec_units  = $this->getPostVariable( 'rec_units' );
			$rec_order  = $this->getPostVariable( 'rec_order' );
			
			# I'm just replacing what's here with better code, but I think this login
			# & security check are already being done by the controller.
			$user = \EE\Model\User\Login::login( $user, $pass, true );
			if ( !$user || !$user || !$pass ) {
				echo "<center><h3>Failed</h3></center>";
				$_SESSION->addFlashError( 'Invalid login or security level.' );
			}
			else {
				$result = false;
				$item_name = '';
				$recipeItem = RecipeItem::getById( intval( $recipe_id ) );
				if ( $recipeItem ) {
					$item = InventoryItem::getById( $recipeItem->inv_itemid );
					$result = $recipeItem->delete();
				}
				
				if ( $result ) {
					$_SESSION->addFlashMessage( sprintf(
						'Deleted <strong>"%s"</strong> from the recipe.'
						,$item->item_name
					) );
				}
				else {
					$_SESSION->addFlashError( sprintf(
						'Encountered an error while attempting to deleted <strong>"%s"</strong> from the recipe.'
						,$item->item_name
					) );
				}
				
				$queryString = array(
					'cid' => $companyid,
					'bid' => $businessid,
					'editid' => $editid,
					'minid' => $minId,
					'item_price' => $item_price,
					'showvendor' => $showvendor,
					'batch' => $batch,
				);
				$location = '/ta/businventor4.php?' . http_build_query( $queryString );
				header( 'Location: ' . $location );
			}
		}
		return false;
	}
	
	
	public function edit() {
		
	}
	
	
	public function eip() {
		if ( $_POST ) {
			$p = new Treat_ArrayObject( $_POST );
			if (
				'recipe' == $p->table
				&& 'recipeid' == $p->primary_key
				&& (
					'rec_size' == $p->column
					|| 'rec_num' == $p->column
				)
			) {
				if ( 'rec_num' == $p->column ) {
					if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
						echo '/*', PHP_EOL;
						echo 'rec_num', PHP_EOL;
						echo sprintf(
							'Treat_Model_Nutrition_RecipeItem::updateFromEip( "%s", "%s" )'
							,$p->id
							,$p->value
						), PHP_EOL;
					}
					$rv = Treat_Model_Nutrition_RecipeItem::updateFromEip(
						$p->id
						,$p->value
					);
					if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
						var_dump( $rv );
						echo Treat_Model_Nutrition_RecipeItem::$SQL_DEBUG, PHP_EOL;
						echo '*/', PHP_EOL;
					}
				}
				elseif( 'rec_size' == $p->column ) {
					if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
						echo '/*', PHP_EOL;
						echo 'rec_size', PHP_EOL;
						echo sprintf(
							'Treat_Model_Nutrition_RecipeItem::updateFromEip( "%s", null, "%s" )'
							,$p->id
							,$p->value
						), PHP_EOL;
					}
					$rv = Treat_Model_Nutrition_RecipeItem::updateFromEip(
						$p->id
						,null
						,$p->value
					);
					if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
						var_dump( $rv );
						echo Treat_Model_Nutrition_RecipeItem::$SQL_DEBUG, PHP_EOL;
						echo '*/', PHP_EOL;
					}
				}
			}
			if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
				echo '/*', PHP_EOL;
				var_dump( $_POST );
				echo '*/', PHP_EOL;
			}
		}
		return false;
	}
	
	
	public function menu_nutrition() {
		$company_id         = $this->getPostOrGetVariable( array( 'companyid',  'cid' ) );
		$business_id        = $this->getPostOrGetVariable( array( 'businessid', 'bid' ) );
		$show_cost          = $this->getPostOrGetVariable( 'show_cost' );
		$show_recipe        = $this->getPostOrGetVariable( array( 'showrec', 'show_rec' ) );
		$rollup             = $this->getPostOrGetVariable( 'rollup' );
		
		$this->_nutrition_info();
		
		$this->_view->assign( 'company_id', $company_id );
		$this->_view->assign( 'business_id', $business_id );
		$this->_view->assign( 'show_cost', $show_cost );
		$this->_view->assign( 'show_recipe', $show_recipe );
		$this->_view->assign( 'rollup', $rollup );
	}
	
	
	public function menu_nutrition2() {
		$reserve_id = $this->getPostOrGetVariable( array( 'reserve_id', 'reserveid' ) );
		if ( !$reserve_id ) {
			$reserve_id = $this->getSessionCookieVariable( array( 'reserve_id', 'reserveid' ) );
		}
		
		if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
			echo '<pre>';
		}
		$invoice_detail = Treat_Model_Nutrition_InvoiceDetail::getByReserveId( $reserve_id );
		if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
			echo '</pre>';
		}
		
		$this->_view->assign( 'reserve_id', $reserve_id );
		$this->_view->assign( 'invoice_detail', $invoice_detail );
	}
	
	
	protected function _nutrition_info() {
		$menu_item_id       = $this->getPostOrGetVariable( array( 'editid', 'mid' ) );
		$mic_id             = $this->getPostOrGetVariable( 'mic' );
		$min_id             = $this->getPostOrGetVariable( 'min' );
		$mim_id             = $this->getPostOrGetVariable( array( 'mim', 'upc_id' ) );
		$upc                = $this->getPostOrGetVariable( 'upc' );
		$number_of_servings = $this->getPostOrGetVariable( 'numserv', 1 ); // default of 1 serving
		
		Treat_Model_Nutrition_Loader::setAllowNull( false );
		$menu_item = Treat_Model_Nutrition_Loader::getMenuItemObject( array(
			'min_id' => $min_id,
			'mic_id' => $mic_id,
			'mim_id' => $mim_id,
			'upc' => $upc,
			'menu_item_id' => $menu_item_id
		) );
		$menu_item->setNumberOfServings( $number_of_servings );
		
		$recipe_items = $menu_item->getRecipeItems();
		$nutrition = $menu_item->getNutritionValues();
		
		$this->_view->assign( 'menu_item_id', $menu_item_id );
		$this->_view->assign( 'min_id', $min_id );
		$this->_view->assign( 'mic_id', $mic_id );
		$this->_view->assign( 'mim_id', $mim_id );
		$this->_view->assign( 'upc', $upc );
		$this->_view->assign( 'number_of_servings', $number_of_servings );
		$this->_view->assign( 'menu_item', $menu_item );
	}
	
	
	protected function _passedPageSecurityCheck() {
		if ( !$this->_passed_security_check ) {
			switch ( $this->_uri->getAction( true ) ) {
				case 'business_inventory4':
				case 'conversion':
				case 'conversion_list':
					$page_business = Treat_Model_Business_Singleton::getSingleton();
					$this->_passed_security_check = false;
					$passed_cookie_login = $this->_getCookieLogin();
					$login_user = Treat_Model_User_Login::login( $GLOBALS['user'], $GLOBALS['pass'] );
					if (
						(
							$passed_cookie_login
							&& (
								(
									$GLOBALS['security_level'] == 1
									&& $GLOBALS['bid'] == $page_business->getBusinessId()
									&& $GLOBALS['cid'] == $page_business->getCompanyId()
								)
								|| (
									$GLOBALS['security_level'] > 1
									&& $GLOBALS['cid'] == $page_business->getCompanyId()
								)
							)
						)
						|| (
							$login_user
							&& (
								(
									1 == $login_user->security_level
									&& $login_user->businessid == $page_business->getBusinessId()
									&& $login_user->companyid == $page_business->getCompanyId()
								)
								|| (
									$login_user->security_level > 1
									&& $login_user->companyid == $page_business->getCompanyId()
								)
							)
						)
					) {
						$this->_passed_security_check = true;
					}
					break;
				default:
					$this->_passed_security_check = true;
					break;
			}
		}
		return $this->_passed_security_check;
	}
	
	
	public function printer_friendly() {
		$company_id         = $this->getPostOrGetVariable( array( 'companyid',  'cid' ) );
		$business_id        = $this->getPostOrGetVariable( array( 'businessid', 'bid' ) );
		$show_cost          = $this->getPostOrGetVariable( 'show_cost' );
		$rollup             = $this->getPostOrGetVariable( 'rollup' );
		
		$this->_nutrition_info();
		
		if ( !isset( $newdate ) ) {
			$newdate = Treat_GeneralLedger_Date::getBeginningOfMonth( $date, true );
		}
		
		$this->_view->assign( 'company_id', $company_id );
		$this->_view->assign( 'business_id', $business_id );
		$this->_view->assign( 'show_cost', $show_cost );
		$this->_view->assign( 'rollup', $rollup );
	}
	
	
	public function save_notes() {
		$price = null; # Notice: Undefined variable
		$pageBusiness = \EE\Model\Business\Singleton::getSingleton();
		if ( !$pageBusiness ) {
			$pageBusiness = \EE\Model\Company\Singleton::getSingleton();
			if ( !$pageBusiness ) {
				$pageBusiness = new \EE\Null();
			}
		}
		$pageUser = $_SESSION->getUser();
		if ( !$pageUser ) {
			$pageUser = new \EE\Null();
		}
		
#		$user = static::getSessionCookieVariable( 'usercook' );
#		$pass = static::getSessionCookieVariable( 'passcook' );
		$businessid = static::getPostOrGetVariable( 'businessid' );
		$companyid = static::getPostOrGetVariable( 'companyid' );
		$editid = static::getPostOrGetVariable( 'editid' );
		$notes = static::getPostOrGetVariable( 'recipenotes' );
		
		$recipe_active = static::getPostOrGetVariable( 'recipe_active' );
		$recipe_active_cust = static::getPostOrGetVariable( 'recipe_active_cust' );
		$serving_size = static::getPostOrGetVariable( 'serving_size' );
		$serving_size2 = static::getPostOrGetVariable( 'serving_size2' );
		$recipe_station = static::getPostOrGetVariable( 'recipe_station' );
		$unit = static::getPostOrGetVariable( 'unit' );
		$heart_healthy = static::getPostOrGetVariable( 'heart_healthy' );
		$healthy_living = static::getPostOrGetVariable( 'healthy_living' );
		$keeper = static::getPostOrGetVariable( 'keeper' );
		
		if (
			(
				$pageUser->security_level == 1
				&& $pageUser->hasBusinessId( $pageBusiness->getBusinessId() )
				&& $pageUser->hasCompanyId( $pageBusiness->getCompanyId() )
			)
			|| (
				$pageUser->security_level > 1
				&& $pageUser->hasCompanyId( $pageBusiness->getCompanyId() )
			)
		) {
			$menuItem = \EE\Model\Menu\Item::get( intval( $editid ), true );
			if ( !$menuItem ) {
				$menuItem = new \EE\Model\Menu\Item();
#				$menuItem->menu_item_id = intval( $editid );
			}
			$menuItem->recipe = $notes;
			$menuItem->recipe_active = $recipe_active;
			$menuItem->recipe_active_cust = $recipe_active_cust;
			$menuItem->serving_size = $serving_size;
			$menuItem->serving_size2 = $serving_size2;
			$menuItem->unit = $unit;
			$menuItem->recipe_station = $recipe_station;
			$menuItem->heart_healthy = $heart_healthy;
			$menuItem->healthy_living = $healthy_living;
			$menuItem->keeper = $keeper;
			try {
				$menuItem->save();
				$_SESSION->addFlashMessage( 'Recipe notes successfully saved' );
			}
			catch ( Exception $e ) {
				if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
					echo '<pre>';
					echo 'Exception caught: ';
					var_dump( $e );
					echo '</pre>';
				}
				else {
					$_SESSION->addFlashError( $e->getMessage() );
				}
			}
		}
		else {
			$_SESSION->addFlashError( 'User does not have the security permissions to modify this item' );
		}
		
		$http_qry = array(
			'cid' => $companyid,
			'bid' => $businessid,
			'editid' => $editid,
			'item_price' => $price,
		);
		$location = '/ta/businventor4.php?' . http_build_query( $http_qry );
		
		if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
			echo '<pre>';
			echo sprintf( '<a href="%1$s">%1$s</a>', $location );
			echo '</pre>';
		}
		else {
			header( 'Location: ' . $location );
		}
		return false;
	}
	
	
	
}