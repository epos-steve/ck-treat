#!/usr/bin/env php
<?php
/**
 * Created by PhpStorm.
 * User: ryan
 * Date: 12/11/14
 * Time: 3:41 PM
 */
define( 'APP_PATH', dirname( realpath( $_SERVER['PHP_SELF'] ) ) );
require_once( APP_PATH . '/../application/bootstrap.php' );

class Utility_UpdateForex extends CLIUtility {
	public function run( ) {
		$rates = \EE\Currency\Forex::getRates( );
		echo 'Found ' . $rates->numRates() . ' rates with timestamp ' . $rates->getTimestamp() . PHP_EOL;
		if( $rates->isCached() ) {
			echo 'Rates are old; not syncing' . PHP_EOL;
			return;
		}
		
		$businesses = \EE\Model\Business::getAllBusinessids();
		foreach( $businesses as $bid ) {
			\EE\Model\KioskSync::addType( $bid, \EE\Model\KioskSync::TYPE_FOREX );
		}
		
		echo 'Synced rates for ' . count( $businesses ) . ' businesses' . PHP_EOL;
	}
}

Utility_UpdateForex::start( );
