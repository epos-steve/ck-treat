#!/bin/bash -e

SELF=$(realpath "$0")
CRONDIR=$(dirname "$SELF")
SCRIPTDIR=$(realpath "$CRONDIR/..")
ROOTDIR=$(realpath "$CRONDIR/../..")
CONFDIR="$ROOTDIR/application/confs"

for conf in $(ls "$CONFDIR"/*.ini); do
	export TREAT_CONFIG="$(basename $conf)"
	output=$("$SCRIPTDIR"/sync-special-schedules.php 2>&1 ||:)
	if [ ! -z "$output" ]; then
		echo "$TREAT_CONFIG: $output"
	fi
done
