#!/usr/bin/env php
<?php
/**
 * Utility script to create and update models
 */
define( 'APP_PATH', dirname( realpath( $_SERVER['PHP_SELF'] ) ) );
require_once( APP_PATH . '/../application/bootstrap.php' );

class Utility_RequestPOSSettings extends CLIUtility {
	public function run( ) {
		$terminals = Treat_Model_ClientPrograms::get( );
		$businesses = array( );
		$settingsVersion = Treat_Model_Aeris_Setting::getSettingsVersion( );
		foreach( $terminals as $terminal ) {
			if( !in_array( $terminal->businessid, $businesses ) && $terminal->businessid > 0 ) {
				$businesses[] = $terminal->businessid;
			}
		}
		sort( $businesses );
		foreach( $businesses as $business ) {
			if( Treat_Model_Aeris_Setting::getSettingsVersion( $business ) < $settingsVersion ) {
				echo "Requesting settings for bid " . $business . "...\n";
				Treat_Model_Aeris_Setting::requestSettings( $business );
			}
		}
		echo "Done!\n";
	}
}

Utility_RequestPOSSettings::start( );







