<?php
/**
 * Replace duplicate order sequence numbers in the database. This will take a
 * duplicate sequence number and add 100000 to it so its unique again.
 */

//$terminal_no = 243;
require_once(dirname(dirname(__FILE__)).'/application/bootstrap.php');
$terminal_no = $_SERVER['argv'][1];
if (empty($terminal_no)) die('No terminal number specified'.PHP_EOL);

$db = Treat_DB::singleton('vivipos_order');
$stmnt = $db->query('SELECT sequence, COUNT(sequence) as totals FROM orders WHERE terminal_no = ? GROUP BY sequence ORDER BY sequence ASC', array($terminal_no));

$sequence_values_duplicate = array();
$counter = 0;
$total = 0;

foreach ($stmnt->fetchAll(PDO::FETCH_ASSOC) as $r) {

	$sequence = $r['sequence'];
	$totals = $r['totals'];

	if ($totals > 1){
		$sequence_values_duplicate[] = $sequence;
		$counter++;
	}
}

echo 'Total duplicate rows: '.$counter.PHP_EOL;

foreach ($sequence_values_duplicate as $key=>$value){

	$stmnt = $db->query('SELECT id FROM orders WHERE sequence = ? AND terminal_no = ? ORDER BY transaction_created ASC LIMIT 1', array($value, $terminal_no));
	$orders_id_value = $stmnt->fetchColumn();

	$new_sequence_value = $value - 100000;

	//$total += $db->exec('UPDATE orders SET sequence = ? WHERE id = ? AND terminal_no = ? LIMIT 1', array($new_sequence_value, $orders_id_value, $terminal_no));
}

echo 'Total rows corrected: '.$total.PHP_EOL;
?>
