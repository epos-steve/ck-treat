#!/bin/bash

classes=$( \
	find -type f \
		| grep -vE '/\.git/|/lib/PHPExcel|/report/|/scripts/test/|/test/|/vendors/' \
		| /bin/grep -E '\.(inc|php.?|tpl)$' \
		| sort \
		| xargs -d '\n' grep -HnP '^\s*class\s+([^\s\r\n\{]+)' \
		| sed -r 's@\s*class\s+([^ \t\{]+).*@\1@' \
		| awk -F: '{printf "%s:%s:%s\n", $3, $1, $2}' \
);

for i in $classes; do
	echo '******************************************************************************';
	echo $i;
	class=$( echo $i | awk -F: '{print $1}' );
	file=$( echo $i | awk -F: '{print $2}' );
	
	echo "	class: ${class}";
	echo "	file:  ${file}";
	
#		| grep -vEi '\.(bmp|class|crt|cs[sv]|db|dll|docx?|eot|gif|gz|htaccess|html?|ico|jar|java|jpe?g|js|key|msi|ogg|pdf|pl|png|properties|sh|sql|svg|swf|ttf|txt|wav|woff|xap|xlsx?|xa?[ms][dl])$' \
#		| xargs -d '\n' grep --color=always -HnP "(new ${class}\b|\b${class}::)|[\"']${class}[\"']" \
	find -type f \
		| grep -vE '/\.git/|/lib/PHPExcel|/report/|/scripts/test/|/test/|/vendors/' \
		| grep -v "$file" \
		| /bin/grep -E '\.(inc|php.?|tpl)$' \
		| sort \
		| xargs -d '\n' grep -HnP "(new ${class}\b|\b${class}::)|[\"']${class}[\"']" \
	;
	
	echo '******************************************************************************';
done;

