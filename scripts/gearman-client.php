#!/usr/bin/env php
<?php
putenv( 'TREAT_CONFIG=treat.ini' );
require_once( dirname( __FILE__ ) . '/../application/bootstrap.php' );

#$email = 'patrick@essential-elements.net';
$email = 'jake@essential-elements.net';

$client= new GearmanClient();

$client->addServer(
	\EE\Config::singleton()->getValue( 'gearmand', 'host' )
	,\EE\Config::singleton()->getValue( 'gearmand', 'port' )
);

$client_data = array(
	array(
		'cid' => 12, 
		'report_id' => 3, 
		'date1' => '2012-10-01', 
		'date2' => '2012-11-01', 
		'email' => $email,
	),
	array(
		'cid' => 1, 
		'report_id' => 3, 
		'date1' => '2012-01-01', 
		'date2' => '2012-02-01', 
		'email' => $email,
	),
	array(
		'cid' => 12, 
		'report_id' => 6, 
		'date1' => '2012-02-01', 
		'date2' => '2012-03-01', 
		'email' => $email,
	),
	array(
		'cid' => 36, 
		'report_id' => 3, 
		'date1' => '2012-03-01', 
		'date2' => '2012-04-01', 
		'email' => $email,
	),
	array(
		'cid' => 333, 
		'report_id' => 33, 
		'date1' => '2012-03-01', 
		'date2' => '2012-04-01', 
		'email' => $email,
	),
);

foreach ( $client_data as $data ) {
	$job_name = \EE\Gearman\Worker::REPORT_NAME;
	$rv = $client->doBackground( $job_name, json_encode( $data ) );
#	echo $job_name, ' = ', var_export( $rv, true ), PHP_EOL;
#	$rv = $client->doNormal( $job_name, json_encode( $data ) );
	echo $job_name, ' = ', $rv, PHP_EOL;
	break;
}




