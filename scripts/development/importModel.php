#!/usr/bin/env php
<?php
/**
 * Utility script to create and update models
 */
define( 'APP_PATH', dirname( realpath( $_SERVER['PHP_SELF'] ) ) );
require_once( APP_PATH . '/../../application/bootstrap.php' );

class Utility_ImportModel extends CLIUtility {
	static protected $required = array( 'model', 'table' );
	
	static protected $parameters = array(
		'm' => 'model',
		't' => 'table',
		'l' => 'library',
		'd' => 'database',
		'a' => 'author',
		'p' => 'package',
		's' => 'subpackage',
		'i' => 'description',
		'b' => 'abstract',
		'filedescription'
	);
	
	static protected $flags = array(
		'save'
	);
	
	static protected $defaults = array(
		'library' => 'vendors/EE/library',
		'database' => 'mburris_businesstrack',
		'author' => 'git',
		'package' => 'libEE',
		'subpackage' => 'Model',
		'filedescription' => 'EE library: %package$s',
		'description' => 'Model for table %table$s',
		'abstract' => '\EE\Model\Base'
	);
	
	public function run( ) {
		$args = $this->arguments;
		
		$docFileTemplate = implode( "\n", array(
			'/**',
			'%filedescription$s',
			' * ',
			' * @author %author$s',
			' * @copyright %year$d Essential Elements, LLC',
			' * @link http://www.essential-elements.net/',
			' * @license EE-Proprietary',
			' * @package %package$s',
			' */'
		) );
		
		$namespaceTemplate = implode( "\n", array(
			'/**',
			' * namespace: %namespace$s',
			' */',
			'namespace %namespace$s;'
		) );
		
		$docClassTemplate = implode( "\n", array( 
			'/**',
			'%classdescription$s',
			' * ',
			' * @package %package$s',
			' * @subpackage %subpackage$s',
			'%properties$s',
			' */'
		) );
		
		$startClassTemplate = 'class %modelClass$s extends %abstract$s {';
		$memberTableTemplate = "\t" . 'protected static $_table = \'%table$s\';';
		$memberPkTemplate = "\t" . 'protected static $_pk = %pk$s;';
		$memberFieldsTemplate = implode( "\n", array(
			"\t" . 'protected static $_valid_fields = array(',
			'%validfields$s',
			"\t" . ');'
		) );
		
		$modelParts = explode( '_', $args['model'] );
		$args['modelClass'] = array_pop( $modelParts );
		$args['namespace'] = implode( '\\', $modelParts );
		
		$db = Treat_Model_Abstract::db( );
		$db->exec( 'USE `' . $args['database'] . '`' );
		
		$stmt = $db->prepare( 'SHOW FULL COLUMNS FROM `' . $args['table'] . '`' );
		if( $stmt->execute( ) ) {
			$columns = $stmt->fetchAll( PDO::FETCH_ASSOC );
		}
		
		if( empty( $columns ) ) {
			throw new Exception( 'Couldn\'t fetch metadata for table `' . $args['table'] . '`!' );
		}
		
		// map sql types
		$types = array(
			'int' => array( 'tinyint', 'smallint', 'mediumint', 'int', 'bigint' ),
			'float' => array( 'float', 'double', 'decimal' )
		);
		
		$pk = array( );
		$properties = array( );
		$fields = array( );
		
		foreach( $columns as $col ) {
			if( $col['Key'] == 'PRI' ) {
				$pk[] = $col['Field'];
			}
			
			if( $col['Type'] == 'tinyint(1)' ) {
				$col['phpType'] = 'bool';
			} else {
				$colType = strtolower( preg_replace( '/^(\w*)\b.*$/', '$1', $col['Type'] ) );
				foreach( $types as $phpType => $sqlType ) {
					if( in_array( $colType, $sqlType ) ) {
						$col['phpType'] = $phpType;
						break;
					}
				}
				
				if( !isset( $col['phpType'] ) ) {
					$col['phpType'] = 'string';
				}
			}
			
			$desc = array( );
			if( $col['Default'] !== null ) {
				$desc[] = 'default: ' . $col['Default'];
			}
			if( $col['Null'] == 'NO' ) {
				$desc[] = 'not null';
			}
			if( stristr( $col['Extra'], 'auto_increment' ) ) {
				$desc[] = 'auto-increment';
			}
			if( !empty( $col['Comment'] ) ) {
				$desc[] = $col['Comment'];
			}
			
			$properties[] = $col['phpType'] . ' $' . $col['Field'] . ' ' . implode( '; ', $desc );
			$fields[] = $col['Field'];
		}
		
		$args['properties'] = ' * @property ' . implode( "\n" . ' * @property ', $properties );
		$args['validfields'] = "\t\t'" . implode( "',\n\t\t'", $fields ) . '\'';
		
		// set pk string
		if( count( $pk ) == 0 ) {
			$pk = null;
		} elseif( count( $pk ) == 1 ) {
			$pk = '\'' . $pk[0] . '\'';
		} else {
			$pk = 'array( \'' . implode( '\', \'', $pk ) . '\' )';
		}
		
		$args['pk'] = $pk;
		
		// auto template fields
		$args['year'] = date( 'Y' );
		if( $args['author'] == 'git' ) {
			$gitName = trim( shell_exec( 'git config --get user.name' ) );
			$gitEmail = trim( shell_exec( 'git config --get user.email' ) );
			$args['author'] = $gitName . ' <' . $gitEmail . '>';
		}
		
		// format descriptions
		$docFileDescription = ' * ' . wordwrap( $args['filedescription'], 77, "\n * " );
		$docClassDescription = ' * ' . wordwrap( $args['description'], 77, "\n * " );
		$args['filedescription'] = $docFileDescription;
		$args['classdescription'] = $docClassDescription;
		
		$output = array( );
		// start output
		$output[] = '<?php';
		
		// output: file-level docblock
		$docFile = static::template( $docFileTemplate, $args );
		$output[] = $docFile;
		
		// output: namespace
		$docNS = static::template( $namespaceTemplate, $args );
		$output[] = $docNS;
		
		// output: class-level docblock
		$docClass = static::template( $docClassTemplate, $args );
		$output[] = $docClass;
		
		// output: start class
		$startClass = static::template( $startClassTemplate, $args );
		$output[] = $startClass;
		
		// output: class table
		$classTable = static::template( $memberTableTemplate, $args );
		$output[] = $classTable;
		
		// output: primary key
		if( $pk ) {
			$classPk = static::template( $memberPkTemplate, $args );
			$output[] = $classPk;
		}
		
		// output: blank line
		$output[] = "\t";
		
		// output: valid fields
		$validFields = static::template( $memberFieldsTemplate, $args );
		$output[] = $validFields;
		
		// output: close class
		$output[] = '}';
		
		$output = implode( "\n", $output ) . "\n";
		echo $output . "\n";
		
		if( $args['save'] ) {
			$filename = APP_PATH . '/../../' . $args['library'] . '/';
			$filename .= str_replace( '_', '/', $args['model'] ) . '.php';
			echo 'Saving to ' . $filename . '...' . PHP_EOL;
			$dir = dirname( $filename );
			if( !file_exists( $dir ) ) {
				mkdir( $dir, 0777, true );
			}
			file_put_contents( $filename, $output );
		}
	}
}

Utility_ImportModel::start( );







