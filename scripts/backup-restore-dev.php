#!/usr/bin/env php
<?php
putenv( 'TREAT_CONFIG=treat.ini' );
require_once( dirname( __FILE__ ) . '/../application/bootstrap.php' );
#Treat_DB_Backup::setDebug( true );

# needs to be a timestamp!
$GLOBALS['date'] = strtotime( date( 'Y-m-d 23:30' ) );
Treat_DB_Backup::setDate( $GLOBALS['date'] );
# needs to be a timestamp!
$GLOBALS['date_import'] = strtotime( '-1 Day' );
Treat_DB_Backup::setImportDate( $GLOBALS['date_import'] );



$GLOBALS['databases'] = array(
	'mburris_businesstrack' => array(
		'options' => array(
		),
		'table_options' => array(
		),
		'tables' => array(
			'manufacturer',
			'menu_items_category',
			'menu_items_master',
			'promotions',
			'promotion_mapping',
			'promo_detail',
			'promo_groups',
			'promo_groups_detail',
			'promo_groups_mapping',
			'promo_target',
			'promo_trigger',
			'promo_type',
			'smartMenuAuth',
			'smartMenuBoards',
			'smartMenuBoardsLibraries',
			'smartMenuBoardsScreens',
			'smartMenuClients',
			'smartMenuControl',
			'smartMenuFiles',
			'smartMenuFonts',
			'smartMenuLocations',
			'smartMenuMenuGroups',
			'smartMenuMenuItems',
			'smartMenuMenus',
			'smartMenuPackageLinks',
			'smartMenuPackageRequests',
			'smartMenuPackages',
			'smartMenus',
			'smartMenuTypes',
			'smartMenuUpload',
		),
	),
);

Treat_DB_Backup::setHost( Treat_Config::singleton()->getValue( 'db', 'host' ) );
Treat_DB_Backup::setUsername( Treat_Config::singleton()->getValue( 'db', 'username' ) );
Treat_DB_Backup::setPassword( Treat_Config::singleton()->getValue( 'db', 'password' ) );

# /home/patrick/workspace/projects/company/ee/mburris_businesstrack_2012-03-12_23h30m.Monday.sql.gz
Treat_DB_Backup::setImportFilePattern( '/home/patrick/workspace/projects/company/ee/%s_%s.sql.gz' );

$GLOBALS['pwd'] = trim( shell_exec( 'pwd 2>/dev/null' ) );
$GLOBALS['backup_dir'] = $pwd . '/tmp-backup';
shell_exec( sprintf(
	'mkdir -p %s 2>/dev/null'
	,escapeshellarg( $backup_dir )
) );
Treat_DB_Backup::setBackupDirectory( $backup_dir );

function get_options( $default_options, $set_options ) {
	$tmp_options = array_merge( $default_options, $set_options );
	$options = array();
	foreach ( $tmp_options as $option => $bool ) {
		if ( $bool ) {
			$options[] = $option;
		}
	}
	return $options;
}

function backup_database( $options, $database_name ) {
	$backup_filename = sprintf(
		'%s/%s_%s.aaa-database.sql'
		,$GLOBALS['backup_dir']
		,$database_name
		,date( 'Y-m-d', $GLOBALS['date'] )
	);
	$cmd = sprintf(
		$GLOBALS['backup_table']
		,$options
		,$GLOBALS['host']
		,$GLOBALS['user']
		,$GLOBALS['pass']
		,$database_name
		,$backup_filename
	);
	
	return $backup_filename;
}

foreach ( $databases as $database => $items ) {
	
	$tmp = new Treat_DB_Backup( $database );
	
	echo $tmp->getImportFilename(), PHP_EOL;
	if ( $tmp->doesImportFileExist() ) {
		if ( !array_key_exists( 'options', $items ) ) {
			$items['options'] = array();
		}
		$tmp->setOptions( (array)$items['options'] );
		
		echo '+-----------------------------------------------------------------------------------------------+', PHP_EOL;
		
		var_dump( $tmp->doBackupDatabase() );
		
		if ( !array_key_exists( 'table_options', $items ) ) {
			$items['table_options'] = array();
		}
		$tmp->setOptionsForTables( (array)$items['table_options'] );
		
		if ( !array_key_exists( 'tables', $items ) ) {
			$items['tables'] = array();
		}
		$tmp->setTables( (array)$items['tables'] );
		
		echo '+-----------------------------------------------------------------------------------------------+', PHP_EOL;
		
		$tmp->doBackupDatabaseTables();
#		var_dump( $tmp->doBackupDatabaseTables() );
		
		echo '+-----------------------------------------------------------------------------------------------+', PHP_EOL;
		
		$tmp->doDumpTablesCommand();
		
		echo '+-----------------------------------------------------------------------------------------------+', PHP_EOL;
		
		$tmp->doImportProductionDatabase();
		
		echo '+-----------------------------------------------------------------------------------------------+', PHP_EOL;
		
		$tmp->doRestoreDatabaseTables();
		
		
		echo '+-----------------------------------------------------------------------------------------------+', PHP_EOL;
	}
	
	
	$ls = shell_exec( 'll /blah 2>/dev/null' );
	var_dump( $ls );
}




