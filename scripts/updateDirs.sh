#!/bin/sh

mkdir -p htdocs/uploads/20budget10/
mkdir -p htdocs/uploads/comkpi/
mkdir -p htdocs/uploads/customerFiles/
mkdir -p htdocs/uploads/dashboardfiles/
mkdir -p htdocs/uploads/exports/
mkdir -p htdocs/uploads/labor/
mkdir -p htdocs/uploads/payroll/
mkdir -p htdocs/uploads/import/
mkdir -p htdocs/uploads/utility/

mv -v htdocs/ta/20budget10/* htdocs/uploads/20budget10/
mv -v htdocs/ta/comkpi*htm htdocs/uploads/comkpi/
mv -v htdocs/ta/comkpi*xls htdocs/uploads/comkpi/
mv -v htdocs/ta/customerFiles/* htdocs/uploads/customerFiles/
mv -v htdocs/ta/dashboardfiles/* htdocs/uploads/dashboardfiles/
mv -v htdocs/ta/exports/* htdocs/uploads/exports/
mv -v htdocs/ta/labor/* htdocs/uploads/labor/
mv -v htdocs/ta/payroll/* htdocs/uploads/payroll/
mv -v htdocs/ta/import/* htdocs/uploads/import/

cp -v htdocs/ta/utility/*csv htdocs/uploads/utility/
cp -v htdocs/ta/utility/*xml htdocs/uploads/utility/
cp -v htdocs/ta/utility/*html htdocs/uploads/utility/

