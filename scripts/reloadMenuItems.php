#!/usr/bin/env php
<?php
putenv( 'TREAT_CONFIG=treat.ini' ); # production does not have database info stored in default.ini
require_once( dirname( __FILE__ ) . '/../application/bootstrap.php' );
if ( !ini_get( 'date.timezone' ) ) {
	date_default_timezone_set( 'America/Chicago' );
}

use EE\Date\Time as DateTime;
use EE\Logger\Formatter\Shell as ShellFormatter;
use EE\Logger\Handler\Stream;
use EE\Logger\Handler\StreamUpperLimit;
use EE\Logger\Logger;
use EE\Model\AbstractModel\ProcessHost;
use EE\Model\Base as ModelBase;
use EE\Model\ProcessHost\Company;
use EE\Model\ProcessHost\LoadMenuItems;
use EE\Model\ProcessHost\Procedure;
use \DateTimeZone;

$_loggerDefaultStream = fopen( 'php://stdout', 'w' );
$sections = array(
	'datetime'  => '%(color:light_cyan)s%(datetime)s%(color:end)s',
	'levelname' => '%(color:yellow)s%(levelname)s%(color:end)s',
#	'prefix'    => '%(color:brown)s%(prefix)s%(color:end)s',
);
$messagePart = '] %(message)s';
################################################################
$logger = Logger::getLogger( Logger::ROOT, Logger::NOTSET );
################################################################
$handler = new StreamUpperLimit( $_loggerDefaultStream, Logger::NOTSET, Logger::WARNING );
$out = '[' . implode( '][', $sections ) . $messagePart;
$formatter = new ShellFormatter( $out, 'Y-m-d H:i:s' );
$handler->setFormatter( $formatter );
$logger->addHandler( $handler );
################################################################
$handler = new Stream( $_loggerDefaultStream, Logger::WARNING );
#$sections['prefix'] = '%(color:white)s%(prefix)s%(color:end)s';
$out = '%(bg:red)s[' . implode( '%(bg:red)s][', $sections ) . '%(bg:red)s'. $messagePart . '%(color:end)s';
$formatter = new ShellFormatter( $out, 'Y-m-d H:i:s' );
$handler->setFormatter( $formatter );
$logger->addHandler( $handler );
################################################################
ModelBase::setLoggerStatic( $logger );

$logger->log( Logger::DEBUG, 'Procedure::callCreateMemoryTables()' );
Procedure::callCreateMemoryTables();

$date = new DateTime(
	Treat_Date::getPreviousMonthBeginning( time() )
	,new DateTimeZone( date_default_timezone_get() )
);
$logger->log( Logger::DEBUG, sprintf(
	'set DateTime to [%s]'
	,$date->format( 'Y-m-d H:i:s' )
) );

$logger->log( Logger::DEBUG, 'Procedure::callLoadIcvTemp()' );
Procedure::callLoadIcvTemp( $date );

$logger->log( Logger::DEBUG, 'Procedure::callLoadChecksTemp()' );
Procedure::callLoadChecksTemp( $date );

$logger->log( Logger::DEBUG, 'LoadMenuItems::truncateTable()' );
LoadMenuItems::truncateTable();

$logger->log( Logger::DEBUG, 'Company::getAll()' );
$companies = Company::getAll();
if ( !$companies ) {
	$companies = array();
}
$logger->log( Logger::DEBUG, sprintf(
	'Company::getAll() = [%s]'
	,var_export( count( $companies ), true )
) );

$stmt = Procedure::callLoadMenuItemsPrepare();
$logger->log( Logger::DEBUG, 'looping Procedure::callLoadMenuItems( $x )' );
foreach ( $companies as $company ) {
#	$logger->log( Logger::DEBUG, sprintf(
#		'Procedure::callLoadMenuItemsPrepare() = [%s]'
#		,var_export( $company, true )
#	) );
	Procedure::callLoadMenuItems(
		$stmt
		,$company->companyid
	);
}


