#!/usr/bin/env php
<?php

function show_help( ) {
	echo 'Usage: ' . basename( __FILE__ ) . ' <businessid>' . "\r\n";
	echo "\r\n";
}

$item = null;

function on_error( ) {
	global $item;
	
	echo "Found bad item {$item['id']}:\r\n";
	echo json_last_error( ) . "\r\n";
	var_dump( $item );
}

if( !isset( $argv[1] ) ) {
	show_help( );
	exit( -1 );
}

if( !( ( $businessid = intval( $argv[1] ) ) > 0 ) ) {
	echo 'Error: invalid business id' . "\r\n";
	show_help( );
	exit( 1 );
}

putenv( 'TREAT_CONFIG=treat.ini' );
require_once( dirname( __FILE__ ) . '/../application/bootstrap.php' );

echo "Selecting menu items for business $businessid...\r\n";
$result = Treat_DB_ProxyOld::query( 'SELECT * FROM menu_items_new WHERE businessid=' . $businessid );
echo 'Found ' . mysql_num_rows( $result ) . ' menu items.' . "\r\n";

set_error_handler( 'on_error' );

require_once( dirname( __FILE__ )  . '/../lib/inc/utils.php' );

while( $item = mysql_fetch_assoc( $result ) ) {
	$json = utf8_json_encode( $item );
}

echo $json . "\r\n";

echo 'All done!' . "\r\n\r\n";
