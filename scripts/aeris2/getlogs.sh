#!/bin/bash
#
# Get Log from a remote Aeris2 client.
#

SSH='/usr/bin/ssh'

client_url=$1
cwd=$(dirname $0)

temp_key=$(mktemp)

cp $cwd/id_aeris2_dsa $temp_key
chmod 600 $temp_key

$SSH -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" -i $temp_key vpncontrol@vpn.essential-elements.net "~/bin/get_logs $client_url"

rm -f $temp_key
