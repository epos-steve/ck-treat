#!/bin/bash
#
# Script to remove old KioskSync pull files that were not cleaned up.
#

# Path to the KioskSync Pull directory.
SYNC_PULL_PATH="/var/sync-processor/working/pull"

if [ "$(id -u)" == "0" ]; then
    echo "This script cannot be run as root" 1>&2
    echo "Try running this as the apache user:" 1>&2
    echo "    00 00 * * * www-data clean_sync_file.sh" 1>&2
    exit 1
fi

find ${SYNC_PULL_PATH} -type f -mtime +3 -exec rm {} \;