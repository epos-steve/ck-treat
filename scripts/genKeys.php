#!/usr/bin/php
<?php

$outDir = dirname( $_SERVER['PHP_SELF'] ) . '/../data/keys/';
$outCert = $outDir . $argv[1] . '.public.crt';
$outKey = $outDir . $argv[1] . '.private.key';

$dn = array(
	'countryName' => 'US',
	'stateOrProvinceName' => 'MO',
	'localityName' => 'NKC',
	'organizationName' => 'Essential Elements',
	'organizationalUnitName' => 'POS Development',
	'commonName' => 'Essential Elements'
);

$config = array(
	'private_key_bits' => 1024,
	'encrypt_key' => false
);

$pkey = openssl_pkey_new( $config );
$csr = openssl_csr_new( $dn, $pkey, $config );
$cert = openssl_csr_sign( $csr, null, $pkey, 3600, $config );

openssl_x509_export_to_file( $cert, $outCert );
openssl_pkey_export_to_file( $pkey, $outKey );
