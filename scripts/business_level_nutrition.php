#!/usr/bin/env php
<?php
putenv( 'TREAT_CONFIG=treat.ini' );
echo dirname( __FILE__ ) . '/../application/bootstrap.php', PHP_EOL;
require_once( dirname( __FILE__ ) . '/../application/bootstrap.php' );

#$date = strtotime( '2012-03-13' );
$date = time();
$date = strtotime( '-1 Day', $date );

$base_query = array(
	'bid' => null,
	'date' => date( 'Y-m-d', $date ),
);

$base_url = "http://treatamerica.essentialpos.com/ta/nutrition/do_company_level_cron?";

$output_file = "/var/www/essentialpos.com/subdomains/treatamerica/htdocs/uploads/do_company_level_cron.html";

$base_url = Treat_Config::singleton()->getValue( 'application', 'business_nutrition_url' );
$output_file = Treat_Config::singleton()->getValue( 'application', 'business_nutrition_output' );

echo 'Getting list of business ids.', PHP_EOL;
$bids = Treat_Model_Business_GatewayData::getListForBusinessLevelNutritionCron();
$list = array();
foreach ( $bids as $bid ) {
	$list[] = $bid->businessid;
}
echo '	business ids: ', implode( ', ', $list ), PHP_EOL;

$ch = curl_init();
curl_setopt( $ch, CURLOPT_HEADER, false );
curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

file_put_contents( $output_file, '' );
foreach ( $bids as $bid ) {
	
	echo '*********************************************************', PHP_EOL;
	echo 'Proceeding with business id: ', $bid->businessid, PHP_EOL;
	$query = array_merge( $base_query, array(
		'bid' => $bid->businessid,
	) );
	$url = $base_url . http_build_query( $query );
	echo '	hitting page: ', $url, PHP_EOL;
	
	// set URL and other appropriate options
	curl_setopt( $ch, CURLOPT_URL, $url );
	
	// grab URL and pass it to the browser
	$out = curl_exec( $ch ) . PHP_EOL;
	file_put_contents( $output_file, $out, FILE_APPEND );
}

// close cURL resource, and free up system resources
curl_close( $ch );

echo '*********************************************************', PHP_EOL;

