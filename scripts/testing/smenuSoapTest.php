#!/usr/bin/php
<?php
ini_set( 'soap.wsdl_cache_enabled', 0 );
ini_set( 'default_socket_timeout', 600 );

//$host = 'manage';
$host = 'manage.dev.essential-elements.net';
$wsdl = 'http://' . $host . '/smartmenu/package/wsdl';

echo 'Server: ' . $wsdl . "\r\n\r\n";

try {
	$client = @new SoapClient( $wsdl, array( 'trace' => true ) );
	$client->__setCookie( 'XDEBUG_SESSION', 'ECLIPSE_DBGP_ryanp' );
} catch( SoapFault $e ) {
	echo $e->faultstring;
	exit( 1 );
}

echo "Functions:\r\n";
print_r( $client->__getFunctions( ) );
echo "\r\n";

class Auth {
	public $username;
	public $password;

	public function __construct( $u, $p ) {
		$this->username = $u;
		$this->password = $p;
	}
}

//$auth = new Auth( '5BY)HE{^`Va=Jj5hB9\>s2}!/`3e]u', '\'N\')D/6AgKh`3[J|1c3sBu")Tj=&?2' );
//$auth = new Auth( 'test', 'test' );
$auth = new Auth( 'eesupport', 'essential' );
$header = new SoapHeader( $wsdl, 'authenticate', $auth, false );

$client->__setSoapHeaders( $header );

//$packageId = 'testPackageFile';
$packageId = 'd93290f1b88f42efaa96be3cf022f054';
//$boardGuid = '0E93C3F9667C4AA2A6A2419E92A0488D';
$boardGuid = '54DFDACF22D84F838EC6FB557FC434F0';

//// test menu list
echo "Menus Result:\r\n";
$request = new stdClass;
$request->boardGuid = $boardGuid;
$mresult = $client->menus( $request );
var_dump( $mresult );
unset( $request );
echo "\r\n";

//// test simple menu
echo "Simple Menu Result:\r\n";

foreach( $mresult->return->MenuDetails as $menu ) {
	var_dump( $menu );
	echo 'Retrieving menu ' . $menu->menuName . ' (' . $menu->menuId . ')' . "\r\n";
	$request = new stdClass;
	$request->menuId = $menu->menuId;
	$result = $client->simpleMenu( $request );
	var_dump( $result );
	unset( $request );
	unset( $result );
	echo "\r\n";
}

unset( $mresult );

exit( );






$dataLocalFile = 'Comic_Sans_MS.ttf';
$fileData = file_get_contents( $dataLocalFile );
$dataFile = md5( $fileData );

echo "Get Data Upload Token Result:\r\n";
$request = new stdClass;
$request->dataFile = $dataFile;
$request->packageId = $packageId;
$request->fileName = 'testing.ttf';
$result = $client->GetDataUploadUrl( $request );
var_dump( $result );
unset( $request );


echo "Upload Data Result:\r\n";
$ch = curl_init( );

curl_setopt( $ch, CURLOPT_URL, $result->return );
curl_setopt( $ch, CURLOPT_POST, true );
curl_setopt( $ch, CURLOPT_POSTFIELDS, $fileData );

curl_exec( $ch );
curl_close( $ch );

echo "\r\n";

unset( $ch );
unset( $result );


echo "Request Data Result:\r\n";
$request = new stdClass;
$request->dataFile = $dataFile;
$result = $client->requestDataFile( $request );
var_dump( $result );
unset( $request );


echo "Retrieve Data Result:\r\n";
$ch = curl_init( );

curl_setopt( $ch, CURLOPT_URL, $result->return->requestUri );
curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

$dataResult = curl_exec( $ch );
curl_close( $ch );

if( $dataResult == $fileData )
echo "Files match\r\n";
else echo "Files do not match\r\n";

file_put_contents( $dataLocalFile . '-downloaded', $dataResult );

unset( $ch );
unset( $result );


// test data files

echo "Get all data files:\r\n";
$request = new stdClass;
$request->packageId = $packageId;
$result = $client->requestDataFiles( $request );
var_dump( $result );
if( isset( $result ) && isset( $result->return ) && isset( $result->return->PackageDetails ) )
	foreach( $result->return->PackageDetails as $pd ) {
		echo "Package: ";
		var_dump( $pd );
	}
unset( $request );
unset( $result );

exit( );


// test reserved areas

echo "Get reserved areas:\r\n";
$request = new stdClass;
$request->boardGuid = 'abcd';
$result = $client->GetReservedAreas( $request );
var_dump( $result );
unset( $request );
unset( $result );



// test fonts

$fontLocalFile = 'Verdana.ttf';
$fontData = file_get_contents( $fontLocalFile );
$fontFile = md5( $fontData );

echo "Get Font Upload Token Result:\r\n";
$request = new stdClass;
$request->fontFile = $fontFile;
$result = $client->getFontUploadToken( $request );
var_dump( $result );
unset( $request );


echo "Upload Font Result:\r\n";
$ch = curl_init( );

curl_setopt( $ch, CURLOPT_URL, $result->return );
curl_setopt( $ch, CURLOPT_POST, true );
curl_setopt( $ch, CURLOPT_POSTFIELDS, $fontData );

curl_exec( $ch );
curl_close( $ch );

echo "\r\n";

unset( $ch );
unset( $result );


echo "Request Font Result:\r\n";
$request = new stdClass;
$request->fontFile = $fontFile;
$result = $client->requestFont( $request );
var_dump( $result );
unset( $request );


echo "Retrieve Font Result:\r\n";
$ch = curl_init( );

curl_setopt( $ch, CURLOPT_URL, $result->return->requestUri );
curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

$fontResult = curl_exec( $ch );
curl_close( $ch );

if( $fontResult == $fontData )
	echo "Fonts match\r\n";
else echo "Fonts do not match\r\n";

file_put_contents( $fontLocalFile . '-downloaded', $fontResult );

unset( $ch );
unset( $result );


exit( );




$packageFile = 'prettyTest';
$packageData = file_get_contents( '/home/ryan/Documents/testpackage.zip' );
$boardGuid = '629BD06875E54A46BC191508C78F6FC2';

/* test values */
$packageFile = 'testPackageFile';
$packageData = 'testtesttest test test testest';
$packageFile2 = 'anotherTestPackageFile';
//$packageData2 = file_get_contents( '/home/ryan/Downloads/soapdiscovery-2009-05-20.zip' );
$packageData2 = file_get_contents( 'test.zip' );
$boardGuid = '0E93C3F9667C4AA2A6A2419E92A0488D';

//// test upload
echo "Upload Result:\r\n";
$request = new stdClass;
$request->packageFile = $packageFile;
//$request->packageData = base64_encode( $packageData );
$request->packageData = $packageData;
$result = $client->upload( $request );
var_dump( $result );
unset( $request );
unset( $result );
echo "\r\n";

exit( );

//// test userlogin
echo "User Login:\r\n";
$result = $client->userlogin( );
var_dump( $result );
unset( $result );
echo "\r\n";

exit( );


//// test location list
echo "Location Result:\r\n";
$result = $client->locations( );
var_dump( $result );

echo "  Boards:\r\n";
foreach( $result->return->string as $location ) {
	echo '  {' . $location . '}' . "\r\n";
	$request = new stdClass;
	$request->locationGuid = $location;
	$lresult = $client->boards( $request );
	var_dump( $lresult );
	unset( $request );
	unset( $lresult );
	echo "\r\n";
}

unset( $result );
echo "\r\n";


exit( );

//// test control
echo "Control Result:\r\n";
$request = new stdClass;
$request->boardGuid = $boardGuid;
$result = $client->control( $request );
var_dump( $result );
unset( $request );
unset( $result );
echo "\r\n";


//// test simple menu
echo "Simple Menu Result:\r\n";
$request = new stdClass;
$request->menuId = 1;
$result = $client->simpleMenu( $request );
var_dump( $result );
unset( $request );
unset( $result );
echo "\r\n";

/*exit( );

echo "Upload package:\r\n";
$request = new stdClass;
$request->packageFile = $packageFile;
$request->packageData = $packageData;
$result = $client->upload( $request );
var_dump( $result );
unset( $request );
unset( $result );

/*echo "Link package:\r\n";
$request = new stdClass;
$request->boardGuid = $boardGuid;
$request->packageFile = $packageFile;
$result = $client->link( $request );
var_dump( $result );
unset( $request );
unset( $result );

exit( );*/



echo "Upload2 Result:\r\n";
$request = new stdClass;
$request->packageFile = $packageFile2;
//$request->packageData = base64_encode( $packageData2 );
$request->packageData = $packageData2;
var_dump( $request );
try {
	$result = $client->upload( $request );
} catch( Exception $e ) {
	echo $client->__getLastRequest( );
}
var_dump( $result );
unset( $request );
unset( $result );
echo "\r\n";


//// test retrieve
echo "Retrieve Result:\r\n";
$request = new stdClass;
$request->packageFile = $packageFile;
$result = $client->retrieve( $request );
var_dump( $result );
echo 'decoded: ' . base64_decode( $result->return ) . "\r\n";
unset( $request );
unset( $result );
echo "\r\n";


//// test link
echo "Link Result:\r\n";
$request = new stdClass;
$request->boardGuid = $boardGuid;
$request->packageFile = $packageFile;
$result = $client->link( $request );
var_dump( $result );
unset( $request );
unset( $result );
echo "\r\n";

echo "Link2 Result:\r\n";
$request = new stdClass;
$request->boardGuid = $boardGuid;
$request->packageFile = $packageFile2;
$result = $client->link( $request );
var_dump( $result );
unset( $request );
unset( $result );
echo "\r\n";


//// test request
echo "Request Result:\r\n";
$request = new stdClass;
$request->boardGuid = $boardGuid;
$result = $client->request( $request );
var_dump( $result );
unset( $request );
unset( $result );
echo "\r\n";


//// test dumb string request
echo "Request Result:\r\n";
$request = new stdClass;
$request->boardGuid = $boardGuid;
$result = $client->requestDumbString( $request );
var_dump( $result );
unset( $request );
unset( $result );
echo "\r\n";


//// test unlink
echo "Unlink Result:\r\n";
$request = new stdClass;
$request->boardGuid = $boardGuid;
$request->packageFile = $packageFile;
$result = $client->unlink( $request );
var_dump( $result );
unset( $request );
unset( $result );
echo "\r\n";

echo "Unlink2 Result: \r\n";
$request = new stdClass;
$request->boardGuid = $boardGuid;
$request->packageFile = $packageFile2;
$result = $client->unlink( $request );
var_dump( $result );
unset( $request );
unset( $result );
echo "\r\n";


//// test destroy
echo "Destroy Result:\r\n";
$request = new stdClass;
$request->packageFile = $packageFile;
$result = $client->destroy( $request );
var_dump( $result );
unset( $request );
unset( $result );
echo "\r\n";
