#!/usr/bin/env php
<?php
putenv( 'TREAT_CONFIG=treat.ini' );
require_once( dirname( __FILE__ ) . '/../application/bootstrap.php' );

$worker = new GearmanWorker();

$worker->addServer(
	\EE\Config::singleton()->getValue( 'gearmand', 'host' )
	,\EE\Config::singleton()->getValue( 'gearmand', 'port' )
);


$worker->addFunction( \EE\Gearman\Worker::REPORT_NAME, 'my_reverse_function' );

#while ( $worker->work() );
$worker->work();
$worker->work();

function my_reverse_function( $job )
{
	$a = (array)json_decode( $job->workload() );
	
	var_dump( $a );
	
	return var_export( $a, true );
	
}



