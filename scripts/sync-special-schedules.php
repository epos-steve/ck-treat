#!/usr/bin/env php
<?php
if ( !( $config = getenv( 'TREAT_CONFIG' ) ) ) {
	putenv( 'TREAT_CONFIG=treat.ini' );
}
require_once( dirname( __FILE__ ) . '/../application/bootstrap.php' );

if( !Treat_Model_KioskSync::syncSpecialItems() ) {
	echo 'failed!';
}
