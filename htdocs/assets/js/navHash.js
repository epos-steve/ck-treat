jQuery( document ).ready( function( ) {
	var firstPanel = null;
	
	function replacePanel( ) {
		var loc = location.href.replace( location.hash, '' );
		var visiblePanel = jQuery( '.navPanel:visible' );
		
		var gotoPanel = visiblePanel.length ? visiblePanel : firstPanel;
		
		if( gotoPanel && gotoPanel.length ) {
			var hash = '#' + gotoPanel.attr( 'id' );
			location.replace( loc + hash );
			switchPanel( hash );
		}
	}
	
	function switchPanel( panelId ) {
		var panel = jQuery( panelId );
		
		if( panel.length ) {
			jQuery( '.navPanel' ).hide( );
			panel.show( );
			
			var callback = panel.data( 'callback' );
			if( callback != undefined ) {
				try {
					if( typeof callback == 'function' )
						callback( );
					else if( typeof callback == 'string' )
						eval( callback );
				} catch( e ) {}
			}
		} else {
			replacePanel( );
		}
	}
	
	jQuery( window ).hashchange( function( ) {
		var hash = location.hash;
		
		switchPanel( hash );
	} );
	
	jQuery( '.navLink' ).each( function( ) {
		var panelId = jQuery( this ).attr( 'href' );
		var panel = jQuery( panelId );
		
		if( panel.length ) {
			firstPanel = firstPanel || panel;
			var callback = jQuery( this ).attr( 'onclick' );
			jQuery( this ).removeAttr( 'onclick' );
			panel.data( 'callback', callback );
			
			jQuery( this ).click( function( event ) {
				window.location.hash = panelId;
				event.preventDefault( );
				return false;
			} );
		}
	} );
	
	if( location.hash == undefined || jQuery( location.hash ).length == 0 ) {
		replacePanel( );
	} else {
		switchPanel( location.hash );
	}
} );