/* 
 * These are the functions that were included in the head section of
 * /ta/buscatermenu4.php & may be duplicated elsewhere
 * 
 * /ta/buscatermenu5.php
 */

function changePage(newLoc)
{
	nextPage = newLoc.options[newLoc.selectedIndex].value;

	if (nextPage != "")
	{
		document.location.href = nextPage;
	}
}


function popup_generic( url, title, pheight, pwidth ) {
	top.popwin = window.open(
		url
		,title
		,"location=no,scrollbars=yes,menubar=no,titlebar=no,resizeable=no,height="
			+ pheight
			+ ",width="
			+ pwidth
			+ ""
	);
	return top.popwin;
}


function popup_tutorial( url ) {
	return popup_generic( url, "Tutorial", 50, 500 );
}


function popup_labor( url ) {
	return popup_generic( url, "Labor", 600, 600 );
}



