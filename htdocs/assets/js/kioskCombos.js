
function getComboDetails( promo_id ) {
    fix_tooltips();

	var businessid = jQuery( '#comboForm input[name=bid]' ).val( );

	jQuery( '#combo_detail' ).html('<img src="/assets/images/ajax-loader-redmond-big.gif">')
        .load( '/ta/promotions/details/' + promo_id + '/' + businessid, function() {
            post_load_detail();
        });
        
}

function post_load_detail() {

    var get_visble = function() {
        return jQuery( ".mainPromoGroup" ).filter(function( index ) {
            return jQuery( this ).is(":visible")
        });
    };

    var get_id = function(name) {
        return name.split('_')[1];
    };

    jQuery( '#comboForm select[name=combo_type]' ).change( );
    jQuery( '.settingsHeader' ).removeClass( 'ui-state-highlight' )
        .addClass( 'ui-state-default' );

    jQuery( '#AddComboGroupButton' ).button( {
            text: false,
            icons: {
                primary: 'ui-icon-plusthick'
            }
        } ).click(function() {
            // Add a Combo group selection

            var index = get_id(get_visble().last()[0].id);

            index++;
            var last_element = jQuery( "#mainPromoGroup_" + index );

            if (last_element.length == 0) {
                jQuery.waffle.error( "Cannot add more Combo Groups" );
            } else {
                last_element.show();
            }
        });

    jQuery( '.RemoveComboGroupButton' ).button( {
            text: false,
            icons: {
                primary: 'ui-icon-closethick'
            }
        }).click(function() {
            // Remove Combo Group Selection
            // If a group is removed from the middle, move the ones above it
            // down and hide the last one.

            var parent_div = jQuery( this ).closest( 'tr' ).attr( 'id' );
            var index = get_id(parent_div);
            var next_index = parseInt(index) + 1;

            var next_element;

            while (true) {

                var total_groups = get_visble().length;
                if (total_groups < 3) {
                    jQuery.waffle.error( 'Cannot have less than two combo groups.' );
                    break
                }

                next_element = jQuery( "#mainPromoGroup_" + next_index );
                // Next Group is not hidden
                if (next_element.length != 0 && !next_element.is(':hidden')) {

                    var next_promo_group_val = jQuery( "#combo_group_" + next_index + ' option:selected').val();
                    var next_item_discount_val = jQuery( "#combo_item_discount_" + next_index ).val();
                    var next_apply_discount_checked = jQuery( "#combo_item_apply_discount_" + next_index ).is(':checked');
                    var next_rows_cols_val = jQuery( "#rows_cols_" + next_index ).val();
                    var next_rows_cols_type_val = jQuery( "#rows_cols_type_" + next_index ).val();
                    var next_combo_pos_color_val = jQuery( "#combo_pos_color_" + next_index ).val();

                    jQuery( "#combo_group_" + index ).val(next_promo_group_val);
                    jQuery( "#combo_item_discount_" + index ).val(next_item_discount_val);
                    jQuery( "#combo_item_apply_discount_" + index ).attr('checked', next_apply_discount_checked);
                    jQuery( "#rows_cols_" + index ).val(next_rows_cols_val);
                    jQuery( "#rows_cols_type_" + index ).val(next_rows_cols_type_val);
                    jQuery( "#combo_pos_color_" + index ).val(next_combo_pos_color_val).change();

                    index++;
                    next_index++;

                // Next group is Hidden or it doesn't exist, Hide current and stop loop.
                } else {
                    jQuery( "#combo_group_" + index + " option:selected" ).removeAttr("selected");
                    jQuery( "#combo_item_discount_" + index ).val('');
                    jQuery( "#combo_item_apply_discount_" + index ).attr('checked', false);
                    jQuery( "#rows_cols_" + index ).val('');
                    jQuery( "#rows_cols_type_" + index + " option:selected" ).removeAttr("selected");
                    jQuery( "#combo_pos_color_" + index ).val('').change();
                    jQuery( "#mainPromoGroup_" + index ).hide();
                    break;
                }
            }
//            var index = get_index();
//
//            if (index > 2) {
//                jQuery( "#promo_group_" + index + " option:selected" ).removeAttr("selected");
//                jQuery( "#combo_item_discount_" + index ).val('');
//                jQuery( "#combo_item_apply_discount_" + index ).attr('checked', false);
//                jQuery( "#mainPromoGroup_" + index ).hide();
//            } else {
//                alert("Cannot Remove Combo group 1 and 2");
//            }
        });

    jQuery( '.comboOptionApplyDiscount' ).click(function() {

        var is_checked = jQuery( this ).is(':checked');
        if (is_checked) {
            jQuery( ".comboOptionApplyDiscount" ).attr('checked', false);
            jQuery( this).attr('checked', true)
        }

    });

    jQuery( '#RefreshComboGroupButton' ).button( {
        text: false,
        icons: {
            primary: 'ui-icon-refresh'
        }
    } ).click(function() {
        // Refresh Combo Group Selection

        getComboDetails( jQuery( '#combo_id' ).val( ) )
    });

}

function fix_tooltips() {
    jQuery( '#RefreshComboGroupButton' ).mouseleave();
    jQuery( '.RemoveComboGroupButton' ).mouseleave();
    jQuery( '#AddComboGroupButton' ).mouseleave();
}

function initializeCombos( ) {
	getPromos( 'combo' );

	var t = function( n ) {
		return '#comboForm input[name=' + n + ']';
	};
	var s = function( n ) {
		return '#comboForm select[name=' + n + ']';
	};

	jQuery( '#comboList' )
		.change( function( ) {
			var d = jQuery( this ).data( 'promos' );
			var index = jQuery( 'option:selected', this ).val( );
            if (!index) {
                return;
            }
			jQuery( '.comboOption' ).attr( 'disabled', false );
			jQuery( '.comboMove' ).button( 'option', 'disabled', true );
			resetPromoForm('combo')
			if( index == 'new' ) {
				jQuery( '#deleteCombo' ).attr( 'disabled', true );
                jQuery( '#combo_id' ).val('0');
                jQuery( t( 'combo_type' ) ).change();
			} else {
				jQuery( t( 'combo_name' ) ).val( d[index].name );
				jQuery( t( 'combo_value' ) ).val( d[index].value );

				jQuery( s( 'combo_type' ) + ' option:selected' ).removeAttr( 'selected' );
				jQuery( s( 'combo_type' ) + ' option[value=' + d[index].type + ']' )
					.attr( 'selected', 'selected' );
				jQuery( t( 'combo_id' ) ).val( d[index].id );

				jQuery( s( 'combo_option_discount' ) + ' option:selected' ).removeAttr( 'selected' );
				jQuery( s( 'combo_option_discount' ) + ' option[value=' + d[index].promo_discount
						+ ']' ).attr( 'selected', 'selected' );
				jQuery( s( 'combo_option_discount_type' ) + ' option:selected' )
					.removeAttr( 'selected' );
				jQuery( s( 'combo_option_discount_type' ) + ' option[value='
						+ d[index].promo_discount_type + ']' ).attr( 'selected', 'selected' );

                if( d[index].promo_reserve == 1 )
                    jQuery( t( 'disable_promotions' ) ).attr( 'checked', 'checked' );
                else jQuery( t( 'disable_promotions' ) ).removeAttr( 'checked' );

				if( d[index].activeFlag == 1 )
					jQuery( '#deleteCombo span' ).text( 'Deactivate' );
				else
					jQuery( '#deleteCombo span' ).text( 'Activate' );

				jQuery( '.comboMove' ).button( 'option', 'disabled', false );
				if( index == 0 ) jQuery( '#moveComboUp' ).button( 'option', 'disabled', true );
				if( index == jQuery( this ).data( 'maxCombo' ) )
					jQuery( '#moveComboDown' ).button( 'option', 'disabled', true );

				getComboDetails( d[index].id );
			}
		} );

    initializeCommon( 'combo' );

    jQuery( '#combo_submit' ).button( {
        icons: {
            primary: 'ui-icon-circle-check'
        }
    } ).click(function() {
        jQuery( '#comboForm' ).submit();
    });

    jQuery( '#deleteCombo' ).button( {
    } );
}

jQuery( function( ) {
	initializeCombos( );
} );
