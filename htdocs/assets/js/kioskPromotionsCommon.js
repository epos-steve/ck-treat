/**
 *
 * @param type promo or combo
 */
function submit_promo( type ) {

    var data;

    jQuery( '#' + type + '_submit' ).attr( 'disabled', 'disabled' );

    if (type == 'promo') {
        data = jQuery( '#promoForm' ).serialize( );
    } else {
        data = jQuery( '#comboForm, .settingsDetailForm' ).serialize( );
    }

    jQuery.ajax( {
        type: "POST",
        url: '/ta/promotions/save',
        dataType: 'json',
        data: data,
        success: function( r ) {
            getPromos( type );

            if (r != null) {
                alert( r.message );
            } else {
                selectPromo( jQuery( '#' + type + '_id' ).val( ), type );
            }
        },
//        error: function( r ) {
//          if (r && r.responseText) {
//              jQuery.waffle.error(r.responseText);
//          }
//        },
        complete: function( xhr, status ) {
            $('#' + type + '_submit').attr('disabled', false)
        }
    } );

    if (type == 'promo') {
        promo_update();
    }
}

function selectPromo( promo_id, type ) {
    var p = jQuery( '#' + type + 'List' );
    jQuery( 'option:selected', p ).removeAttr( 'selected' );
    jQuery( '#' + type + promo_id, p ).attr( 'selected', 'selected' );
    p.change( );
}

function refreshPromos( type ) {
    resetPromoForm( type );
    jQuery( '.' + type + 'Option' ).attr( 'disabled', true );
    jQuery( '#' + type + 'List option:selected' ).removeAttr( 'selected' );
    getPromos( type );
}

function resetPromoForm( type ) {
    jQuery( '#' + type + 'Form input[type=text]' ).val( '' );
    jQuery( '#' + type + 'Form option:selected' ).removeAttr( 'selected');
    jQuery( '#' + type + 'Form input[name=promo_id]' ).val( 0 );
    if (type == 'promo') {
        jQuery( '#promo_schedule, #promo_detail' ).empty( );
    } else {
        jQuery( '#combo_detail' ).empty( );
    }
}

function getPromos( type ) {

    var selector, category;
    if (type == 'promo') {
        selector = '#promoList';
        category = 1;
    } else {
        selector = '#comboList';
        category = 2;
    }

    var selectedPromo;
    if ( jQuery( selector + ' option:selected' ).is(':not(:empty)') ) {
        selectedPromo = jQuery( selector + ' option:selected' ).attr( 'id' );
    }

    var getPromotionsUrl = '/ta/promotions/getPromotions?bid=' + jQuery( '#promobid' ).val( ) + '&category=' + category;

    jQuery.ajax( {
        url: getPromotionsUrl,
        dataType: 'json',
        success: function( data ) {
            if( !data || data == null) {
                alert('There was an error processing your request.');
                return false;
            }
            if( !data.promos || data.error ) {
                alert(data.error || 'There was an error processing your request.');
                return false;
            }

            jQuery( selector ).empty( );
            jQuery( selector ).data( 'promos', data.promos );
            var maxPromo = 0;
            var show;
            for( var i in data.promos ) {
                var promoName = data.promos[i].activeFlag == 0
                    ? '[' + data.promos[i].name + ']'
                    : data.promos[i].name;
                promoName = data.promos[i].rule_order + '. ' + promoName;
                jQuery( selector ).append( '<option id="promo' + data.promos[i].id
                    + '" value="' + i + '">' + promoName + '</option>' );
                if( data.promos[i].activeFlag == 0 ) {
                    if (category == 1) {
                        show = jQuery( '#showHidePromos' ).is( ':checked' );
                    } else {
                        show = jQuery( '#showHideCombos' ).is( ':checked' );
                    }
                    jQuery( '#promo' + data.promos[i].id ).addClass( 'promoInactive' ).toggleOption( show );
                }
                maxPromo = i;
            }
            jQuery( selector ).data( 'maxPromo', maxPromo );
            jQuery( selector ).data( 'viviposTypes', data.viviposTypes );
            jQuery( selector ).data( 'viviposTriggers', data.viviposTriggers );

            var add_name = 'promotion';
            if (category == 2) {
                add_name = 'combo';
            }

            jQuery( selector ).append( '<option id="promo_add_new" value="new">(add new '+ add_name + ')</option>' );

            if( typeof ( selectedPromo ) != 'undefined' ) {
                jQuery( selector + ' option#' + selectedPromo ).attr( 'selected', 'selected' );
                jQuery( selector ).change( );
            }
        }
    } );
}

function initializeCommon( type ) {

    var t = function( n ) {
        return '#' + type + 'Form input[name=' + n + ']';
    };
    var s = function( n ) {
        return '#' + type + 'Form select[name=' + n + ']';
    };

    var cap_type =  type.charAt(0).toUpperCase() + type.substring(1);

    jQuery( '#delete' + cap_type ).click( function( ) {
        var current;
        if (type == 'promo') {
            current = document.getElementById('delete' + cap_type).innerHTML;
        } else {
            current = jQuery( '#deleteCombo span' ).text();
        }

        var is_active, button_text;

        if (current == 'Activate') {
            is_active = 1;
            button_text = 'Activate';
        } else {
            is_active = 0;
            button_text = 'Deactivate';
        }

        jQuery( '#' + type + 'List option:selected' ).removeAttr( 'selected' );

        if (type == 'promo') {
            document.getElementById('deletePromo').innerHTML = button_text;
        } else {
            jQuery( '#deleteCombo span' ).text( button_text );
        }

        jQuery.ajax( {
            url: '/ta/promotions/activate/' + jQuery( t( 'bid' ) ).val() + '?promo_id=' + jQuery( t( type + '_id' ) ).val( ) + '&active=' + is_active,
            type: 'post',
            dataType: 'json',
            success: function( data ) {
                if( data && data.error ) {
                    return alert( data.error );
                }
                getPromos( type );
            }
        } );
        return false;
    } );

    jQuery( '#move'  + cap_type + 'Up' ).data( 'dir', 1 ).button( {
        text: false,
        icons: {
            primary: 'ui-icon-triangle-1-n'
        }
    } );
    jQuery( '#move'  + cap_type + 'Down' ).data( 'dir', 2 ).button( {
        text: false,
        icons: {
            primary: 'ui-icon-triangle-1-s'
        }
    } );

    jQuery( '.' + type + 'Move' ).addClass( 'ui-button-thin' ).click( function( ) {
        if( jQuery( this ).button( 'option', 'disabled' ) )
            return;

        var selectedPromo = jQuery( '#' + type + 'List option:selected' ).attr( 'value' );
        var d = jQuery( '#' + type + 'List' ).data( 'promos' );
        var selected_promo_id = jQuery( '#' + type + 'List option:selected' )
            .attr( 'id' ).substr(5);
        jQuery.ajax( {
            url: '/ta/promotions/reorderPromo'
                + '?promo_id=' + selected_promo_id
                + '&bid=' + jQuery( t( 'bid' ) ).val( )
                + '&dir=' + jQuery( this ).data( 'dir' ),
            type: 'post',
            dataType: 'json',
            success: function( data ) {
                if( data && data.error ) {
                    return alert( data.error );
                }
                getPromos( type );
            }
        } );
    } ).parent( ).css( {
        display: 'inline-block',
        margin: 1
    } ).buttonset( );

    jQuery( '#showHide' + cap_type + 's' ).button( ).change( function( ) {

        refreshPromos( type );
        jQuery( '#' + type + 'List option.promoInactive' ).toggleOption( jQuery( this ).is( ':checked' ) );
        var current = jQuery('#showHide' + cap_type + 'sLabel span').text();
        if (current == 'Show Inactive') {
            jQuery('#showHide' + cap_type + 'sLabel span').text('Hide Inactive');
        } else {
            jQuery('#showHide' + cap_type + 'sLabel span').text('Show Inactive');
        }
    } );

    jQuery( '#showHide' + cap_type + 'sLabel' ).addClass( 'ui-button-thin' ).css( {
        fontSize: '80%',
        float: 'right'
    } ).parent( ).css( {
        padding: 2
    } );

    jQuery( 'form .' + type + 'Option' ).live( {
        change: function( ) {
            jQuery( this ).closest( '.settingsContainer' ).find( '.settingsHeader' )
                .removeClass( 'ui-state-default' ).addClass( 'ui-state-highlight' );
        }
    });

    jQuery( '.' + type + 'Move' ).button( {'disabled': true } );
    jQuery( '.' + type + 'Discount' ).hide( );

    jQuery( s( type + '_type' ) )
        .change( function( ) {
            jQuery( '.' + type + 'Discount' ).hide( );
            jQuery( '.' + type + 'GroupOption' ).hide( );
            var viviposType = ( jQuery( '#' + type + 'List' ).data( 'viviposTypes' ) )[jQuery( s( type + '_type' ) )
                .val( )];
            jQuery( '.promo_' + viviposType ).show( );
        } );

    if (type == 'promo') {
        jQuery( s( 'promo_trigger' ) )
            .change( function( ) {
                var promoTrigger = jQuery( '#promoList' ).data( 'viviposTriggers' )[jQuery( this ).val( )];
                jQuery( '.promoAmount' ).toggle( promoTrigger != 'bypass' );
                jQuery( '.promoAmount2' ).toggle( promoTrigger == 'multi_dept' || promoTrigger == 'multi_group' );
            } );
    }

    var submit = function() {
        submit_promo(type);
    };

    jQuery( '#' + type + 'Form' ).submit( submit );

    resetPromoForm( type );
}
