var ajaxUrl = 'posAjax';

jQuery.ajaxSetup( {
	cache: false,
	complete: function( jqXHR, textStatus ) {
		if( textStatus != 'success' ) {
			alert( 'There was an error processing your request [' + textStatus + ']' );
		}
	}
} );

String.prototype.capitalize = function( ) {
	return this.charAt( 0 ).toUpperCase( ) + this.slice( 1 );
};

function getTitle( d ) {
	for( var i in d ) {
		if( i.substr( -4 ) == 'Name' ) return d[i];
	}
}

function setSmenuList( name, longName, data, onlyData ) {
	var selector = '#' + name + 'List';
	
	jQuery( selector ).empty( );
	jQuery( selector ).data( name + 's', data );
	
	if( onlyData == true ) return;
	
	var max = 0;
	for( var i in data ) {
		var title = getTitle( data[i] );
		title = data[i].activeFlag == 0 ? '[' + title + ']' : title;
		jQuery( selector ).append( '<option id="' + name + data[i].id + '" value="' + i + '">'
									+ title + '</option>' );
		max = i;
	}
	jQuery( selector ).data( 'max', max );
	jQuery( selector ).data( 'name', name );
	
	jQuery( selector ).append( '<option value="new">(add new ' + longName + ')</option>' );
	
	jQuery( '#' + name + 'Form' ).data( 'name', name );
}

function setSmenuForm( ) {
	var name = jQuery( this ).data( 'name' );
	var d = jQuery( this ).data( name + 's' );
	var index = jQuery( this ).val( );
	
	jQuery( '.smenuOption' ).attr( 'disabled', false );
	resetSmenuForm( name );
	
	var t = function( n ) {
		return '#' + name + 'Form input[name=' + n + ']';
	};
	var s = function( n ) {
		return '#' + name + 'Form select[name=' + n + ']';
	};
	
	if( index == 'new' ) {
		jQuery( '#delete' + name.capitalize( ) ).attr( 'disabled', true );
		jQuery( '.smenuGuid, .smenuKeys' ).attr( 'disabled', true );
		
		jQuery( '.smenuNotNew' ).hide( );
	} else {
		for( var i in d[index] ) {
			var elem = jQuery( t( name + '_' + i ) );
			if( elem.length ) {
				if( typeof( d[index][i] ) == 'object' )
					elem.data( 'object', d[index][i] );
				else elem.val( d[index][i] );
			}
			
			elem = jQuery( s( name + '_' + i ) );
			if( elem.length ) {
				elem.find( 'option:selected' ).removeAttr( 'selected' );
				elem.find( 'option[value=' + d[index][i] + ']' ).attr( 'selected', 'selected' );
			}
		}
		
		if( d[index].activeFlag == 1 )
			jQuery( '#delete' + name.capitalize( ) ).text( 'Deactivate' );
		else
			jQuery( '#delete' + name.capitalize( ) ).text( 'Activate' );
		
		jQuery( '.' + name + 'Settings.smenuNotNew' ).show( );
	}
	jQuery( '.settingsHeader' ).removeClass( 'ui-state-highlight' ).addClass( 'ui-state-default' );
	
	jQuery( 'input.callback', '#' + name + 'Settings, .' + name + 'Settings' ).data( 'name', name ).each( getItemList );
	jQuery( 'input.function', '#' + name + 'Settings, .' + name + 'Settings' ).data( 'name', name ).each( runFn );
}

function getItemList( ) {
	var name = jQuery( this ).data( 'name' );
	var elemName = jQuery( this ).attr( 'name' );
	var elem = jQuery( '#' + elemName );
	
	if( !elem.is( ':visible' ) ) return;
	
	jQuery( '#' + elemName + 'Spinner' ).show( );
	elem.empty( ).parent( ).addClass( 'far-left' );
	
	jQuery.ajax( {
		url: jQuery( this ).attr( 'value' ) + '?bid=' + jQuery( '#smenubid' ).val( ) + '&id='
				+ jQuery( '#' + name + '_id' ).val( ),
		dataType: 'json',
		success: function( data ) {
			if( !data 
				|| ( typeof( data.content ) == 'undefined' && typeof( data.script ) == 'undefined' && typeof( data.data ) == 'undefined' ) 
				|| data.error 
			) {
				alert( data.error || 'There was an error retrieving data.' );
				return false;
			}
			
			if( typeof( data.content ) != 'undefined' )
				elem.html( data.content );
			
			if( typeof( data.data ) != 'undefined' )
				elem.data( 'data', data.data );
			
			if( data.script ) {
				var script = data.script.replace( /%%/g, elem.attr( 'id' ) );
				eval( script );
			} else {
				elem.parent( ).removeClass( 'far-left' );
				jQuery( '#' + elemName + 'Spinner' ).hide( );
			}
		}
	} );
}

function runFn( ) {
	var name = jQuery( this ).data( 'name' );
	var elemName = jQuery( this ).val( );
	var elem = jQuery( '#' + elemName );
	var fn = jQuery( this ).attr( 'name' );
	
	if( !elem.is( ':visible' ) && !( elem.is( '[type=hidden]' ) && elem.parent( ).is( ':visible' ) ) ) return;
	
	var cmd = fn + '( elem )';
	eval( cmd );
}

function updatePackageList( elem ) {
	var data = jQuery( '#smenuBoard_currentPackage_List' ).data( 'object' ) || [ ];
	var list = jQuery( '#' + elem.attr( 'id' ) + '_Select' );
	
	list.empty( ).append( '<option value="0">(none)</option>' );
	
	for( var i in data ) {
		list.append( '<option value="' + data[i].id + '">' + data[i].packageFile + '</option>' );
	}
	
	jQuery( 'option[value="' + elem.val( ) + '"]', list ).attr( 'selected', 'selected' );
}

function setupPLinkList( selector ) {
	var elem = jQuery( selector );
	
	jQuery.ajax( {
		url: 'getSmenuPackageList?boardId=' + jQuery( '#smenuBoard_id' ).val( ),
		dataType: 'json',
		success: function( data ) {
			if( !data || typeof( data.packages ) == 'undefined' || data.error ) {
				alert( data.error || 'There was an error retrieving data.' );
				return false;
			}
			
			elem.parent( ).find( 'input.ui-autocomplete-input' ).data( 'packages', data.packages ).combo( 'source', data.packages );
		}
	} );
	
	jQuery( 'span.remove', elem ).button( {
		text: false,
		icons: {
			primary: 'ui-icon-closethick'
		}
	} ).click( function( ) {
		jQuery( '#packageLinksListSpinner' ).show( );
		
		jQuery.ajax( {
			url: 'updateSmenuPackageLink?pLinkId=' + jQuery( this ).attr( 'id' ).substr( 5 ),
			dataType: 'json',
			success: function( data ) {
				if( !data || data.error ) {
					alert( data.error || 'There was an error processing your request' );
					return false;
				}
				
				refreshCurrentSmenus( );
			}
		} );
	} );
	
	elem.parent( ).removeClass( 'far-left' );
	jQuery( selector + 'Spinner' ).hide( );
}

function refreshMenuItems( ) {
	jQuery( '#callback_getMenuItems' ).each( getItemList );
}

var itemList_iconLeft = {
	primary: 'ui-icon-triangle-1-w'
};
var itemList_iconRight = {
	secondary: 'ui-icon-triangle-1-e'
};

function itemListUI( selector, direction ) {
	var elem = jQuery( selector );
	var icon = direction == 'left' ? itemList_iconLeft : itemList_iconRight;
	
	// elem.find( 'li' ).addClass( 'ui-widget ui-state-default' );
	elem.find( 'li' ).button( {
		text: true,
		icons: icon
	} ).addClass( 'itemListButton-' + direction );
	
	sortList( selector );
}

function moveItem( ) {
	var btn = jQuery( this );
	var fromSelector = btn.data( 'from' );
	var toSelector = btn.data( 'to' );
	
	var selected = jQuery( '#' + fromSelector + ' option:selected' );
	
	if( selected.length == 0 ) return;
	
	selected.each( function( idx, itm ) {
		var ji = jQuery( itm );
		// var destList = jQuery( '#menuItems_Select option.sort' + ji.val(
		// ).substr( 0, 1 ).toUpperCase( ) );
		var destList = jQuery( '#' + toSelector ).children( );
		var dest = null;
		
		destList.each( function( idx, itm ) {
			if( dest ) return;
			if( _compare( ji, itm ) == -1 ) dest = itm;
		} );
		
		if( dest )
			jQuery( dest ).before( ji );
		else
			jQuery( '#' + toSelector ).append( ji );
	} );
	
	menuListModified( );
}

function menuListModified( ) {
	jQuery( '#smenuMenuItems_submit' ).css( 'visibility', 'visible' )
		.closest( '.settingsContainer' ).find( '.settingsHeader' )
		.removeClass( 'ui-state-default' ).addClass( 'ui-state-highlight' );
}

var inputTimeout = null;
var inputThis = null;

function delayedInputChange( ) {
	if( inputTimeout )
		clearTimeout( inputTimeout );
	
	var parent = jQuery( self ).closest( '.itemListColumn' );
	var header = jQuery( '.itemListHeader', parent );
	
	jQuery( '.itemListFilterReset', header )
		.button( 'option', 'disabled', jQuery( self ).val( ).length == 0 );
	
	inputThis = this;
	inputTimeout = setTimeout( startInputChange, 150 );
}

jQuery.expr[':'].iContains = function( a, i, m ) {
	return ( a.textContent || a.innerText || "" ).toUpperCase( ).indexOf( m[3].toUpperCase( ) ) >= 0;
};

function startInputChange( ) {
	jQuery( '.spinner', jQuery( inputThis ).parent( ) ).show( );
	setTimeout( 'inputChange( inputThis );', 50 );
}

function inputChange( self ) {
	var list = jQuery( '#' + jQuery( self ).attr( 'id' ).replace( 'Filter', 'Select' ) );
	var filterString = jQuery( self ).val( );
	
	list.children( ).removeClass( 'ui-filtered' ).show( );
	if( filterString.length > 0 ) {
		var filterTokens = filterString.toUpperCase( ).split( ' ' );
		
		for( var i in filterTokens ) {
			list.find( ':not(:iContains(' + filterTokens[i] + '))' ).addClass( 'ui-filtered' );
		}
		
		list.find( '.ui-filtered' ).hide( );
	}
	
	jQuery( '.spinner', jQuery( self ).parent( ) ).hide( );
}

function oldinputChange( self ) {
	var list = jQuery( '#' + jQuery( self ).attr( 'id' ).replace( 'Filter', 'Select' ) );
	var filterString = jQuery( self ).val( );
	
	list.children( ).removeClass( 'ui-filtered' ).show( );
	
	if( filterString.length > 0 ) {
		var filterTokens = filterString.toUpperCase( ).split( ' ' );
		
		list.children( ).each( function( idx, itm ) {
			var item = jQuery( itm );
			
			for( var i in filterTokens ) {
				if( item.text( ).toUpperCase( ).indexOf( filterTokens[i] ) == -1 ) {
					item.addClass( 'ui-filtered' );
					break;
				}
			}
		} );
		
		list.find( '.ui-filtered' ).hide( 0 );
	}
};

function createItemList( selector ) {
	var list = jQuery( selector );
	var items = list.data( 'data' );
	
	for( var i in items ) {
		if( typeof( items[i].title ) == 'string' ) {
			var optgroup = jQuery( '<optgroup id="group' + items[i].id + '" label="' + items[i].title + '" />' );
			for( var oi in items[i].items ) {
				createItemListOption( items[i].items[oi], optgroup, list );
			}
			optgroup.data( 'order', items[i].items[0].menuOrder ).appendTo( list );
		} else {
			createItemListOption( items[i], list );
		}
	}
}

function createItemListOption( item, list, head ) {
	if( typeof( head ) == 'undefined' )
		head = list;
	var option = jQuery( '<option name="' + item.id + '">' /*+ item.menuOrder + ': '*/ + item.name + '</option>' );
	item.menuOrder = parseInt( item.menuOrder );
	option.data( 'order', item.menuOrder ).appendTo( list );
	var min = head.data( 'min' ), max = head.data( 'max' );
	if( item.menuOrder < min || typeof( min ) == 'undefined' )
		head.data( 'min', item.menuOrder );
	if( item.menuOrder > max || typeof( max ) == 'undefined' )
		head.data( 'max', item.menuOrder );
}

function setupItemList( selector ) {
	var list = jQuery( selector );
	var parent = list.parent( );
	var header = jQuery( '.itemListHeader', parent );
	
	list.width( parent.width( ) ).height( parent.height( ) - header.height( ) )
		.addClass( 'ui-widget-content ui-state-default ui-corner-all' );
	
	jQuery( '.itemListFilterReset', header ).button( {
		disabled: true,
		text: false,
		icons: {
			primary: 'ui-icon-cancel'
		}
	} ).click( function( ) {
		jQuery( '.itemListFilter', header ).val( '' ).keyup( ).focusout( );
	} ).removeClass( 'ui-corner-all' ).addClass( 'ui-corner-right' );
	
	jQuery( '.itemListFilter', header ).addClass( 'ui-widget-content ui-corner-left' )
		.width( parent.width( ) - jQuery( '.itemListFilterReset', parent ).width( ) - 9 )
		.after( '<img class="spinner" style="display: none;" src="/assets/images/ajax-loader-redmond.gif" />' )
		.hover( function( ) {
			jQuery( this ).addClass( 'ui-state-hover' );
		}, function( ) {
			jQuery( this ).removeClass( 'ui-state-hover' );
		} ).bind( 'keydown, keypress', delayedInputChange ).focusin( function( ) {
			if( jQuery( this ).val( ) == jQuery( this ).data( 'watermark' ) )
				jQuery( this ).removeClass( 'inputWatermark' ).val( '' );
			jQuery( this ).addClass( 'ui-state-focus' );
		} ).focusout( function( ) {
			if( jQuery( this ).val( ) == '' )
				jQuery( this ).addClass( 'inputWatermark' )
					.val( jQuery( this ).data( 'watermark' ) );
			jQuery( this ).removeClass( 'ui-state-focus' );
		} ).data( 'watermark', 'filter items' ).focusout( );
	
	jQuery( '.spinner', header ).show( ).position( {
		my: 'right',
		at: 'right',
		of: header,
		offset: '-22 -3'
	} ).hide( );
	
	jQuery( 'optgroup', list ).each( function( ) {
		if( jQuery( this ).attr( 'label' ).match( /^.*:\d+$/ ) ) {
			var label = jQuery( this ).attr( 'label' );
			var auto = label;
			auto = auto.replace( /^.*:(\d+)$/, '$1' );
			jQuery( this ).data( 'automaticName', auto );
			label = label.replace( /:(\d+)$/, '' );
			jQuery( this ).attr( 'label', label );
		}
	} );
	
	sortList( selector );
	
	list.change( );
	
	parent.removeClass( 'far-left' );
	jQuery( selector + 'Spinner' ).hide( );
}

function getSmenus( ) {
	var selectedListVal = jQuery( '#smenuSelect' ).val( );
	var selectedList = '#' + selectedListVal + 'List';
	var selectedSmenu = jQuery( selectedList + ' option:selected' ).attr( 'id' );
	var bid = jQuery('#smenubid').val();
	var urlQueryResults = window.location.search.match(/bid=(\d+)/);
	if ( bid < 1 || typeof bid === 'undefined' ) {
		bid = urlQueryResults[1];
	}
	if ( bid < 1 || typeof bid === 'undefined' ) {
		var bidInputs = jQuery('[name=bid]');
		if ( bidInputs.length > 0 ) {
			bid = bidInputs.eq(0).val();
		}
	}
	if ( bid < 1 || typeof bid === 'undefined' ) {
		bid = 0;
	}

	return;
	jQuery.ajax( {
		url: 'getSmenus?bid=' + bid,
		dataType: 'json',
		success: function( data ) {
			if( !data || !data.smenus || data.error ) {
				alert( data.error || 'There was an error processing your request.' );
				return false;
			}
			
			// smenus
			setSmenuList( 'smenu', 'link', data.smenus );
			
			// smenu boards
			setSmenuList( 'smenuBoard', 'menu board', data.smenuBoards );
			
			// smenu clients
			setSmenuList( 'smenuClient', 'client', data.smenuClients );
			
			// smenu menus
			setSmenuList( 'smenuMenu', 'menu', data.smenuMenus );
			
			// smenu location
			// jQuery( '#smenuLocation' ).data( 'smenuLocation',
			// data.smenuLocation );
			// setSmenuList( 'smenuLocation', 'location', data.smenuLocation );
			
			jQuery( '#smenuLocation_name' ).val( data.smenuLocation.name );
			jQuery( '#smenuLocation_locationGuid' ).val( data.smenuLocation.locationGuid );
			var packageList = jQuery( '#smenuLocationPackageList' ).empty( );
			var boardList = jQuery( '#smenuLocationBoardList' ).empty( );
			var dP = data.smenuLocation.packageList;
			var dB = data.smenuLocation.boardList;
			
			for( var i in dP ) {
				packageList.append( '<tr><td><a name="package/' + dP[i].packageName + '">'
									+ dP[i].packageName + '</a></td>' + '<td>' + dP[i].createdTime
									+ '<br />' + '<span>by <a href="#client/' + dP[i].createdByName
									+ '" class="smenuLinkClient" onclick="return false;">'
									+ dP[i].createdByName + '</a></span></td>' + '<td>'
									+ dP[i].modifiedTime + '<br />' + '<span>by <a href="#client/'
									+ dP[i].modifiedByName
									+ '" class="smenuLinkClient" onclick="return false;">'
									+ dP[i].modifiedByName + '</a></span></td></tr>' );
			}
			
			for( var i in dB ) {
				var packageLink = dB[i].packageFile ? '<a href="#package/' + dB[i].packageFile
														+ '">' + dB[i].packageFile + '</a>'
					: '(none)';
				boardList.append( '<tr><td><a href="#board/' + dB[i].boardName
									+ '" class="smenuLinkBoard" onclick="return false;">'
									+ dB[i].boardName + '</a></td>' + '<td>' + dB[i].boardGuid
									+ '</td>' + '<td>' + packageLink + '</td></tr>' );
			}
			
			jQuery( '.smenuLinkBoard', boardList )
				.click( function( ) {
					var boardName = jQuery( this ).text( );
					var boardName2 = '[' + boardName + ']';
					
					jQuery( '#smenuSelect option:selected' ).removeAttr( 'selected' );
					jQuery( '#smenuSelect option[value=\'smenuBoard\']' )
						.attr( 'selected', 'selected' );
					jQuery( '#smenuSelect' ).change( );
					
					jQuery( '#smenuBoardList option:selected' ).removeAttr( 'selected' );
					jQuery( '#smenuBoardList option' ).each( function( ) {
						if( jQuery( this ).text( ) == boardName
							|| jQuery( this ).text( ) == boardName2 )
							jQuery( this ).attr( 'selected', 'selected' );
					} );
					jQuery( '#smenuBoardList' ).change( );
				} );
			
			jQuery( '.smenuLinkClient', packageList ).click( function( ) {
				var clientName = jQuery( this ).text( );
				var clientName2 = '[' + clientName + ']';
				
				jQuery( '#smenuSelect option:selected' ).removeAttr( 'selected' );
				jQuery( '#smenuSelect option[value=\'smenuClient\']' )
					.attr( 'selected', 'selected' );
				jQuery( '#smenuSelect' ).change( );
				
				jQuery( '#smenuClientList option:selected' ).removeAttr( 'selected' );
				jQuery( '#smenuClientList option' ).each( function( ) {
					if( jQuery( this ).text( ) == clientName
						|| jQuery( this ).text( ) == clientName2 )
						jQuery( this ).attr( 'selected', 'selected' );
				} );
				jQuery( '#smenuClientList' ).change( );
			} );
			
			if( typeof ( selectedSmenu ) != 'undefined' ) {
				jQuery( selectedList + ' option#' + selectedSmenu ).attr( 'selected', 'selected' );
				jQuery( selectedList ).change( );
			}
		}
	} );
}

function selectSmenu( id ) {
	var name = jQuery( '#smenuSelect' ).val( );
	var p = jQuery( '#' + name + 'List' );
	jQuery( 'option:selected', p ).removeAttr( 'selected' );
	jQuery( '#' + name + id, p ).attr( 'selected', 'selected' );
	p.change( );
}

function refreshCurrentSmenus( ) {
	getSmenus( );
}

function refreshSmenus( ) {
	resetSmenuForm( );
	jQuery( '.smenuOption' ).attr( 'disabled', true );
	jQuery( '.smenuSelectList option:selected' ).removeAttr( 'selected' );
	getSmenus( );
}

function resetSmenuForm( name ) {
	jQuery( '#' + name + 'Form input[type=text]' ).val( '' );
	jQuery( '#' + name + 'Form option:selected' ).removeAttr( 'selected' );
	jQuery( '#' + name + 'Form input[name=' + name + '_id]' ).val( 0 );
	jQuery( '#' + name + 'Form input.value' ).val( '' ).removeData( );
}

function submitAjaxForm( ) {
	if( jQuery( this ).hasClass( 'noAutoSubmit' ) )
		return;
	
	var submitBtn = jQuery( this ).find( 'input[type=submit]' );
	submitBtn.attr( 'disabled', 'disabled' );
	
	var disabledFields = jQuery( this ).find( 'input:disabled' );
	disabledFields.removeAttr( 'disabled' );
	var formData = jQuery( this ).serialize( );
	disabledFields.attr( 'disabled', 'disabled' );
	
	var fn;
	if( (fn = jQuery( this ).data( 'serialize' )) )
		formData += eval( fn + '( this )' );
	
	jQuery.ajax( {
		type: "POST",
		url: jQuery( this ).attr( 'action' ),
		dataType: 'json',
		data: formData,
		success: function( r ) {
			if( r.error ) {
				alert( r.error );
			} else {
				getSmenus( );
				alert( r.message );
				selectSmenu( r.id );
			}
		}
	} );
	
	setTimeout( "jQuery('#" + submitBtn.attr( 'id' ) + "').attr('disabled', '')", 1000 );
}

var tmp = false;
function _compareVal( o ) {
	if( tmp ) console.log( jQuery( o ).data( 'order' ) );
	if( jQuery( o ).is( 'optgroup' ) )
		o = jQuery( 'option:first-child', o );
	return jQuery( o ).data( 'order' ) ? jQuery( o ).data( 'order' )
		: jQuery( o ).attr( 'label' ) ? jQuery( o ).attr( 'label' )
		: jQuery( o ).text( );
}

function _compare( a, b ) {
	if( tmp ) console.log( 'compare ' + a.toString( ) + ' to ' + b.toString( ) );
	var compA = _compareVal( a ).toString( ).toUpperCase( );
	var compB = _compareVal( b ).toString( ).toUpperCase( );
	var numA, numB;
	var regex = /^\d+(?=\. |$)/;
	if( ( numA = compA.match( regex ) ) && ( numB = compB.match( regex ) ) ) {
		numA = parseInt( numA[0] );
		numB = parseInt( numB[0] );
		return ( numA < numB ) ? -1 : ( numA > numB ) ? 1 : 0;
	} else {
		return ( compA < compB ) ? -1 : ( compA > compB ) ? 1 : 0;
	}
}

function sortList( selector ) {
	_sortList( selector );
	jQuery( selector ).children( 'optgroup' ).each( function( ) {
		_sortList( this );
	} );
}

function _sortList( selector ) {
	var elem = jQuery( selector );
	var listItems = elem.children( ).get( );
	listItems.sort( _compare );
	jQuery.each( listItems, function( idx, itm ) {
		var ji = jQuery( itm );
		ji.addClass( 'sort' + ji.val( ).substr( 0, 1 ).toUpperCase( ) );
		elem.append( itm );
	} );
}

function initializeSmenus( ) {
	jQuery( '#smenuSelect option:selected' ).removeAttr( 'selected' );
	jQuery( '#smenuSelect option:first-child' ).attr( 'selected', 'selected' );
	
	getSmenus( '#smenuList' );
	
	jQuery( '#smenuSelect' ).change( function( ) {
		var index = jQuery( this ).val( );
		jQuery( '.smenuSelectList' ).hide( );
		jQuery( '#' + index + 'List' ).show( ).children( ).removeAttr( 'selected' );
		
		jQuery( this ).closest( '.navPanel' ).find( '.settingsContainer' ).hide( );
		jQuery( '#' + index + 'Settings, .' + index + 'Settings' ).not( '.smenuNotNew' ).show( );
		
		jQuery( '.smenuOption' ).attr( 'disabled', true );
	} );
	
	jQuery( '#smenuList' ).change( setSmenuForm );
	jQuery( '#smenuBoardList' ).change( setSmenuForm );
	jQuery( '#smenuClientList' ).change( setSmenuForm );
	jQuery( '#smenuMenuList' ).change( setSmenuForm );
	
	jQuery( '#exportSmenuBoard' ).click( smenuBoardExport );
	jQuery( '#smenuMenu_export' ).click( smenuMenuExport );
	
	jQuery( '.smenuDelete' ).click( function( ) {
		var name = jQuery( this ).closest( '.settingsForm' ).data( 'name' );
		jQuery.ajax( {
			url: 'toggle' + name + 'Active?id='
					+ jQuery( '#' + name + 'Form input[name=' + name + '_id]' ).val( ) + '&bid='
					+ jQuery( '#' + name + 'Form input[name=bid]' ).val( ),
			type: 'post',
			dataType: 'json',
			success: function( data ) {
				if( data && data.error ) {
					return alert( data.error );
				}
				getSmenus( );
			}
		} );
		return false;
	} );
	
	jQuery( '.smenuGuid' )
		.click( function( ) {
			if( !confirm( 'Changing GUIDs can have unintended consequences. It is not recommended that you do so unless you are sure you know what you are doing. Do you want to continue?' ) )
				return false;
			
			var fm = jQuery( this ).closest( '.settingsForm' );
			jQuery( 'input.smenuGuid', fm ).val( '' );
			// fm.submit( );
		} );
	
	jQuery( '.smenuKeys' )
		.click( function( ) {
			if( !confirm( 'Changing authorization keys can have unintended consequences. It is not recommended that you do so unless you are sure you know what you are doing. Do you want to continue?' ) )
				return false;
			
			var fm = jQuery( this ).closest( '.settingsForm' );
			jQuery( 'input.smenuKeys', fm ).val( '' );
			// fm.submit( );
		} );
	
	jQuery( 'form .smenuOption' ).live( {
		change: function( ) {
			jQuery( this ).closest( '.settingsContainer' ).find( '.settingsHeader' )
				.removeClass( 'ui-state-default' ).addClass( 'ui-state-highlight' );
		}
	} );
	

	jQuery( 'li.itemListButton-left' ).live( {
		click: function( ) {
			jQuery( this ).appendTo( '#smenuMenu_menuItems' )
				.button( 'option', 'icons', itemList_iconRight )
				.removeClass( 'itemListButton-left' ).addClass( 'itemListButton-right' );
			
			sortList( '#smenuMenu_menuItems' );
		}
	} );
	
	jQuery( 'li.itemListButton-right' ).live( {
		click: function( ) {
			jQuery( this ).appendTo( '#smenuMenu_itemList' )
				.button( 'option', 'icons', itemList_iconLeft )
				.removeClass( 'itemListButton-right' ).addClass( 'itemListButton-left' );
			
			sortList( '#smenuMenu_itemList' );
		}
	} );
	
	jQuery( '.smenuOption' ).attr( 'disabled', true );
	
	jQuery( '#smartmenu' ).find( '.settingsForm' ).not( '.noAutoSubmit' ).submit( submitAjaxForm );
	
	jQuery( '#smenuMenuItems_submit' ).button( {
		text: false,
		icons: {
			primary: 'ui-icon-disk'
		}
	} ).click( function( ) {
		var selectedMenuItems = [];
		jQuery( '#menuItems_Select' ).children( ).each( function( ) {
			if( jQuery( this ).is( 'optgroup' ) ) {
				var group = [ jQuery( this ).attr( 'id' ) + ':' + jQuery( this ).data( 'automaticName' ) + ':' + jQuery( this ).attr( 'label' ) ];
				jQuery( this ).children( ).each( function( ) {
					group.push( jQuery( this ).attr( 'name' ) );
				} );
				selectedMenuItems.push( group );
			} else {
				selectedMenuItems.push( jQuery( this ).attr( 'name' ) );
			}
		} );
		
		var data = { smenuMenu_id: jQuery( '#smenuMenu_id' ).val( ), menuItems: selectedMenuItems };
		var self = jQuery( this );
		
		jQuery.ajax( {
			url: 'updateSmenuMenuItems?bid=' + jQuery( '#smenubid' ).val( ),
			dataType: 'json',
			data: data,
			success: function( data ) {
				if( !data || typeof( data.count ) == 'undefined' || data.error ) {
					alert( data.error || 'There was an error processing your request' );
					return false;
				}
				
				self.css( 'visibility', 'hidden' );
				self.closest( '.settingsContainer' ).find( '.settingsHeader' )
					.removeClass( 'ui-state-highlight' ).addClass( 'ui-state-default' );
				
				refreshMenuItems( );
				
				return true;
			}
		} );
		
		return false;
	} ); //.addClass( 'ui-button-thin' );
	
	jQuery( '#itemList_MoveButton' ).button( {
		text: false,
		icons: {
			primary: 'ui-icon-arrowthick-1-w'
		}
	} ).click( moveItem ).data( 'from', 'itemList_Select' ).data( 'to', 'menuItems_Select' );
	
	jQuery( '#menuItems_MoveButton' ).button( {
		text: false,
		icons: {
			primary: 'ui-icon-arrowthick-1-e'
		}
	} ).click( moveItem ).data( 'from', 'menuItems_Select' ).data( 'to', 'itemList_Select' );
	
	jQuery( '#menuItems_OrderUpButton' ).button( {
		text: false,
		icons: {
			primary: 'ui-icon-arrowthick-1-n'
		}
	} ).click( orderItem ).data( 'direction', 'up' );
	
	jQuery( '#menuItems_OrderDownButton' ).button( {
		text: false,
		icons: {
			primary: 'ui-icon-arrowthick-1-s'
		}
	} ).click( orderItem ).data( 'direction', 'down' );
	
	jQuery( 'div.fieldset' ).addClass( 'ui-widget ui-widget-content' );
	jQuery( 'div.fieldset span.legend, div.fieldset span.header' ).addClass( 'ui-state-default' );
	
	jQuery( '#menuItems_GroupEditButton' ).button( {
		text: false,
		icons: {
			primary: 'ui-icon-pencil'
		}
	} ).click( function( ) {
		var group = jQuery( '#menuItems_Select option:selected' ).parent( 'optgroup' );
		if( group.length == 0 )
			return false;
		
		var dialog = jQuery( '#menuItems_GroupEditDialog' );
		jQuery( '#smenuMenuGroup_groupId', dialog ).val( group.attr( 'id' ).replace( /^[^\d]*/, '' ) );
		jQuery( '#smenuMenuGroup_groupName', dialog ).val( group.attr( 'label' ) );
		
		if( group.data( 'automaticName' ) == 1 )
			jQuery( '#smenuMenuGroup_automaticName' ).attr( 'checked', 'checked' ).change( );
		else jQuery( '#smenuMenuGroup_automaticName' ).removeAttr( 'checked' ).change( );
		
		dialog.data( 'group', group );
		dialog.dialog( 'open' );
	} ).hide( );
	
	jQuery( '#menuItems_GroupEditDialog' ).dialog( {
		autoOpen: false,
		hide: 'slide',
		show: 'slide',
		modal: true,
		resizable: false,
		title: 'Edit Group',
		buttons: {
			'Save': function( ) {
				var group = jQuery( this ).data( 'group' );
				group.data( 'automaticName', jQuery( '#smenuMenuGroup_automaticName' ).is( ':checked' ) ? 1 : 0 );
				if( !jQuery( '#smenuMenuGroup_automaticName' ).is( ':checked' ) )
					group.attr( 'label', jQuery( '#smenuMenuGroup_groupName' ).val( ) );
				jQuery( this ).dialog( 'close' );
				
				menuListModified( );
			}
		}
	} );
	
	jQuery( '#smenuMenuGroup_automaticName' ).change( function( ) {
		if( jQuery( this ).is( ':checked' ) )
			jQuery( this ).parent( ).siblings( 'input' ).attr( 'disabled', 'disabled' );
		else jQuery( this ).parent( ).siblings( 'input' ).removeAttr( 'disabled' );
	} );
	
	jQuery( '#menuItems_GroupButton' ).button( {
		text: false,
		icons: {
			primary: 'ui-icon-circle-plus'
		}
	} ).click( groupItems ).hide( );
	jQuery( '#menuItems_UngroupButton' ).button( {
		text: false,
		icons: {
			primary: 'ui-icon-circle-minus'
		}
	} ).click( ungroupItems ).hide( );
	
	jQuery( '#menuItems_Select optgroup' ).live( 'click', function( e ) {
		if( jQuery( e.target ).is( 'option' ) )
			return;
		
		if( !e.metaKey )
			jQuery( '#menuItems_Select option:selected' ).removeAttr( 'selected' );
		jQuery( this ).children( ).attr( 'selected', 'selected' );
		jQuery( '#menuItems_Select' ).change( );
	} );
	
	jQuery( '#menuItems_Select' ).bind( 'change focus', function( ) {
		var items = jQuery( '#menuItems_Select option:selected' );
		var OrderMin = jQuery( this ).data( 'min' ), OrderMax = jQuery( this ).data( 'max' );
		
		if( items.length == 0 ) {
			jQuery( '#menuItems_GroupEditButton, '
				+ '#menuItems_GroupButton, #menuItems_UngroupButton, '
				+ '#menuItems_OrderUpButton, #menuItems_OrderDownButton' ).hide( );
		} else if( items.length == 1 ) {
			jQuery( '#menuItems_GroupButton' ).hide( );
			jQuery( '#menuItems_GroupEditButton, #menuItems_UngroupButton' ).toggle( items.parent( 'optgroup' ).length > 0 );
			jQuery( '#menuItems_OrderUpButton' ).toggle( jQuery( items[0] ).data( 'order' ) > OrderMin );
			jQuery( '#menuItems_OrderDownButton' ).toggle( jQuery( items[0] ).data( 'order' ) < OrderMax );
		} else {
			var GroupButton = true, UngroupButton = true, EditButton = true, UpButton = true, DownButton = true;
			var GroupParent = null;
			items.each( function( ) {
				var g = null;
				if( (g = jQuery( this ).parent( )).is( 'optgroup' ) ) {
					GroupButton = false;
					if( GroupParent != null && g[0] != GroupParent[0] ) {
						UngroupButton = false;
						EditButton = false;
						GroupParent = false;
					} else {
						GroupParent = g;
					}
				} else {
					UngroupButton = false;
					EditButton = false;
				}
				
				var order = jQuery( this ).data( 'order' );
				if( order <= OrderMin )
					UpButton = false;
				if( order >= OrderMax )
					DownButton = false;
			} );
			if( GroupParent === null )
				UngroupButton = EditButton = false;
			
			if( GroupParent && !GroupButton && !UngroupButton ) {
				GroupButton = true;
			}
			
			if( GroupParent && GroupParent.attr( 'id' ) == /^newGroup\d+/ )
				EditButton = false;
			
			jQuery( '#menuItems_GroupEditButton' ).toggle( EditButton );
			jQuery( '#menuItems_GroupButton' ).toggle( GroupButton );
			jQuery( '#menuItems_UngroupButton' ).toggle( UngroupButton );
			jQuery( '#menuItems_OrderUpButton' ).toggle( UpButton );
			jQuery( '#menuItems_OrderDownButton' ).toggle( DownButton );
		}
	} );
	
	jQuery( '#itemList_Select' ).dblclick( moveItem ).data( 'from', 'itemList_Select' )
		.data( 'to', 'menuItems_Select' );
	jQuery( '#menuItems_Select' ).dblclick( moveItem ).data( 'from', 'menuItems_Select' )
		.data( 'to', 'itemList_Select' );
	
	jQuery( '#packageLinksSelect' ).combo( {
		watermark: 'link package...',
		alwaysEmpty: true,
		sourceWithIds: true,
		select: function( event, ui, id ) {
			jQuery( '#packageLinksListSpinner' ).show( );
			
			jQuery.ajax( {
				url: 'updateSmenuPackageLink?boardId=' + jQuery( '#smenuBoard_id' ).val( ) + '&packageId=' + id,
				dataType: 'json',
				success: function( data ) {
					if( !data || data.error ) {
						alert( data.error || 'There was an error processing your request' );
						return false;
					}
					
					jQuery( '#packageLinksSelect' ).blur( );
					
					refreshCurrentSmenus( );
				}
			} );
		}
	} );
	
	jQuery( '#smenuBoard_currentPackage_Select' ).change( function( ) {
		jQuery( '#smenuBoard_currentPackage' ).val( jQuery( this ).val( ) );
	} );
	
	resetSmenuForm( );
}

function smenuBoardExport( ) {
	document.location.href = 'exportSmenuBoard?sbid=' + jQuery( '#smenuBoard_id' ).val( );
}

function smenuMenuExport( ) {
	document.location.href = 'exportSmenuMenu?smid=' + jQuery( '#smenuMenu_id' ).val( );
}

jQuery.fn.reverse = [].reverse;

function _swapItemWithGroup( item, group, dir ) {
	var newSwap = jQuery( group ).find( dir == 1 ? 'option:last-child' : 'option:first-child' );
	var newOrder = newSwap.data( 'order' );
	jQuery( item ).data( 'order', newOrder ).addClass( 'menuItemMoved' );
	//jQuery( item ).text( jQuery( item ).data( 'order' ) + '-' + jQuery( item ).text( ) );
	jQuery( group ).find( 'option' ).each( function( ) {
		_moveItem( this, dir );
	} );
}

function _moveItem( item, dir ) {
	if( jQuery( item ).is( '.menuItemMoved' ) )
		return;
	jQuery( item ).data( 'order', jQuery( item ).data( 'order' ) - dir ).addClass( 'menuItemMoved' );
	//jQuery( item ).text( jQuery( item ).data( 'order' ) + '-' + jQuery( item ).text( ) );
}

function orderItem( ) {
	var list = jQuery( '#menuItems_Select' );
	var dir = jQuery( this ).data( 'direction' ) == 'up' ? -1 : 1;
	
	var items = jQuery( 'option:selected', list );
	
	if( items.length > 1 ) {
		items.sort( _compare );
		if( dir == 1  )
			items.reverse( );
	}
	
	items.each( function( ) {
		var order = parseInt( jQuery( this ).data( 'order' ) );
		var swap = findItem( order + dir );
		if( jQuery( this ).parent( ).is( 'optgroup' ) && jQuery( swap ).parent( ).is( ':not(optgroup)' ) ) {
			_swapItemWithGroup( swap, jQuery( this ).parent( ), dir * -1 );
		} else if( jQuery( swap ).parent( ).is( 'optgroup' ) && jQuery( this ).parent( ).is( ':not(optgroup)' ) ) {
			_swapItemWithGroup( this, jQuery( swap ).parent( ), dir );
		} else if( jQuery( this ).parent( ).is( 'optgroup' ) && jQuery( swap ).parent( ).is( 'optgroup' ) 
					&& jQuery( this ).parent( )[0] != jQuery( swap ).parent( )[0] 
		) {
			var group1 = jQuery( this ).parent( );
			var group2 = jQuery( swap ).parent( );
			var size1 = group1.find( ':last-child' ).data( 'order' ) - group1.find( ':first-child' ).data( 'order' ) + 1;
			var size2 = group2.find( ':last-child' ).data( 'order' ) - group2.find( ':first-child' ).data( 'order' ) + 1;
			group1.find( 'option' ).each( function( ) {
				_moveItem( this, size2 * dir * -1 );
			} );
			group2.find( 'option' ).each( function( ) {
				_moveItem( this, size1 * dir );
			} );
		} else {
			if( jQuery( this ).is( ':not(.menuItemMoved)' ) )
				jQuery( this ).data( 'order', order + dir ).addClass( 'menuItemMoved' ); //.text( ( order + dir ) + '-' + jQuery( this ).text( ) );
			if( jQuery( swap ).is( ':not(.menuItemMoved)' ) )
				jQuery( swap ).data( 'order', order ).addClass( 'menuItemMoved' ); //.text( order + '-' + jQuery( swap ).text( ) );
		}
	} );
	
	jQuery( '.menuItemMoved' ).removeClass( 'menuItemMoved' );
	
	sortList( list );
	list.change( );
	menuListModified( );
}

function findItem( order ) {
	var items = jQuery( '#menuItems_Select option' );
	var found = null;
	items.each( function( ) {
		if( jQuery( this ).data( 'order' ) == order ) {
			found = this;
			return false;
		}
	} );
	
	return found;
}

var newGroup = 1;

function groupItems( ) {
	var selected = jQuery( '#menuItems_Select option:selected' );
	var GroupParent = null;
	selected.each( function( ) {
		if( (g = jQuery( this ).parent( )).is( 'optgroup' ) )
			GroupParent = g;
	} );
	
	if( !GroupParent ) {
		GroupParent = jQuery( '<optgroup id="newGroup' + newGroup++ + '" label="New Group" />').appendTo( '#menuItems_Select' );
		GroupParent.data( 'automaticName', 1 );
	}
	
	selected.appendTo( GroupParent );
	
	jQuery( '#menuItems_Select' ).change( );
	
	menuListModified( );
}

function ungroupItems( ) {
	var selected = jQuery( '#menuItems_Select option:selected' );
	var GroupParent = selected.parent( 'optgroup' );
	
	selected.appendTo( '#menuItems_Select' );
	if( GroupParent.children( ).length == 0 )
		GroupParent.remove( );
	
	sortList( '#menuItems_Select' );
	
	jQuery( '#menuItems_Select' ).change( );
	
	menuListModified( );
}

//jQuery( function( ) {
//	initializeSmenus( );
//} );
