jQuery.fn.toggleOption = function( show ) {
	jQuery( this ).toggle( show );
	if( show ) {
		if( jQuery( this ).parent( 'span.toggleOption' ).length )
			jQuery( this ).unwrap( );
	} else {
		jQuery( this ).wrap( '<span class="toggleOption" style="display: none;" />' );
	}
};

function getPromoDetails( promo_id ) {
	var businessid = jQuery( '#promoForm input[name=bid]' ).val( );

	jQuery( '#promo_schedule' ).html('<img src="/assets/images/ajax-loader-redmond-big.gif">');
	jQuery( '#promo_detail' ).html('<img src="/assets/images/ajax-loader-redmond-big.gif">');
	jQuery( '#promo_businesses' ).html('<img src="/assets/images/ajax-loader-redmond-big.gif">');

	jQuery( '#promo_businesses' ).load( '/ta/promotions/businesses/' + promo_id + '/' + businessid);
	jQuery( '#promo_schedule' ).load( '/ta/promotions/schedule/' + promo_id + '/' + businessid);
	jQuery( '#promo_detail' ).load( '/ta/promotions/details/' + promo_id + '/' + businessid);
}

function promo_update() {
	var promo_id = jQuery('#promo_id').val();
	var checkedItems = jQuery('#promoItemForm input:checked'); // input:checked');
	var pid_data = 'promo_id=' + promo_id;
	var data = "";

	// promo items
	jQuery.each(checkedItems, function(key, value) {
		itemId = value.id.substr(3);
		data += "&item[]=" + itemId;
	});
	var treediv = jQuery("#businessMappingTree");
	// promo businesses mapping
	if (treediv.length != 0) {
		
		var tree = treediv.dynatree("getTree");
		var selKeys = jQuery.map(tree.getSelectedNodes(), function(node) {
			return node.data.key;
		});
		jQuery.each(selKeys, function(key, value) {
			var barpos = value.indexOf("_");
			var type = value.substr(0,barpos);
			var id = value.substr(barpos+1);
			data += '&' + type + '[]=' + id;
		});
	}

	if (data != "") {
		jQuery.post(
			'/ta/promotions/updatePromo/' + promo_id + '/',
			encodeURI(pid_data + data),
			function(response, textStatus, xhr){
			}
		);
	}
}


function initializePromos( ) {
	getPromos( 'promo' );

	var t = function( n ) {
		return '#promoForm input[name=' + n + ']';
	};
	var s = function( n ) {
		return '#promoForm select[name=' + n + ']';
	};

	jQuery( '#promoList' )
		.change( function( ) {
			var d = jQuery( this ).data( 'promos' );
			var index = jQuery( 'option:selected', this ).val( );
			jQuery( '.promoOption' ).attr( 'disabled', false );
			jQuery( '.promoMove' ).button( 'option', 'disabled', true );
			resetPromoForm( 'promo' );
			if( index == 'new' ) {
				jQuery( '#deletePromo' ).attr( 'disabled', true );
			} else {
				jQuery( t( 'promo_name' ) ).val( d[index].name );
				jQuery( t( 'promo_value' ) ).val( d[index].value );

				jQuery( s( 'promo_type' ) + ' option:selected' ).removeAttr( 'selected' );
				jQuery( s( 'promo_type' ) + ' option[value=' + d[index].type + ']' )
					.attr( 'selected', 'selected' );
				jQuery( s( 'promo_trigger' ) + ' option:selected' ).removeAttr( 'selected' );
				jQuery( s( 'promo_trigger' ) + ' option[value=' + d[index].promo_trigger + ']' )
					.attr( 'selected', 'selected' );
				if (jQuery(s('promo_tax'))) {
					jQuery( s( 'promo_tax' ) + ' option:selected' ).removeAttr( 'selected' );
					jQuery( s( 'promo_tax' ) + ' option[value=' + d[index].tax_id + ']' )
						.attr( 'selected', 'selected' );
				}
				jQuery( s( 'promo_reporting_category') + ' option:selected' ).removeAttr( 'selected' );
				jQuery( s( 'promo_reporting_category' ) + ' option[value=' + d[index].reporting_category_id + ']' )
					.attr( 'selected', 'selected' );
				
				if (jQuery(s('promo_reporting_category_id'))) {
					jQuery( s( 'promo_reporting_category_id' ) + ' option:selected' ).removeAttr( 'selected' );
					jQuery( s( 'promo_tax' ) + ' option[value=' + d[index].tax_id + ']' )
						.attr( 'selected', 'selected' );
				}
				jQuery( t( 'promo_id' ) ).val( d[index].id );

				if( d[index].pre_tax == 1 )
					jQuery( t( 'pre_tax' ) ).attr( 'checked', 'checked' );
				else jQuery( t( 'pre_tax' ) ).removeAttr( 'checked' );

				if( d[index].promo_reserve == 1 )
					jQuery( t( 'promo_reserve' ) ).attr( 'checked', 'checked' );
				else jQuery( t( 'promo_reserve' ) ).removeAttr( 'checked' );
				
				if( d[index].subsidy == 1 )
					jQuery( t( 'subsidy' ) ).attr( 'checked', 'checked' );
				else jQuery( t( 'subsidy' ) ).removeAttr( 'checked' );

				jQuery( s( 'promo_option_discount' ) + ' option:selected' ).removeAttr( 'selected' );
				jQuery( s( 'promo_option_discount' ) + ' option[value=' + d[index].promo_discount
						+ ']' ).attr( 'selected', 'selected' );
				jQuery( s( 'promo_option_discount_type' ) + ' option:selected' )
					.removeAttr( 'selected' );
				jQuery( s( 'promo_option_discount_type' ) + ' option[value='
						+ d[index].promo_discount_type + ']' ).attr( 'selected', 'selected' );
				jQuery( t( 'promo_option_discount_n' ) ).val( d[index].promo_discount_n );
				jQuery( t( 'promo_option_discount_limit' ) ).val( d[index].promo_discount_limit );

				jQuery( s( 'promo_trigger_amount_type' ) + ' option:selected' )
					.removeAttr( 'selected' );
				jQuery( s( 'promo_trigger_amount_type' ) + ' option[value='
						+ d[index].promo_trigger_amount_type + ']' ).attr( 'selected', 'selected' );
				jQuery( s( 'promo_trigger_amount_limit' ) + ' option:selected' )
					.removeAttr( 'selected' );
				jQuery( s( 'promo_trigger_amount_limit' ) + ' option[value='
						+ d[index].promo_trigger_amount_limit + ']' ).attr( 'selected', 'selected' );
				jQuery( t( 'promo_trigger_amount' ) ).val( d[index].promo_trigger_amount );
				jQuery( t( 'promo_trigger_amount_2' ) ).val( d[index].promo_trigger_amount_2 );
				jQuery( t( 'promo_trigger_amount_3' ) ).val( d[index].promo_trigger_amount_3 );

				if( d[index].activeFlag == 1 )
					jQuery( '#deletePromo' ).text( 'Deactivate' );
				else
					jQuery( '#deletePromo' ).text( 'Activate' );

				jQuery( '.promoMove' ).button( 'option', 'disabled', false );
				if( index == 0 ) jQuery( '#movePromoUp' ).button( 'option', 'disabled', true );
				if( index == jQuery( this ).data( 'maxPromo' ) )
					jQuery( '#movePromoDown' ).button( 'option', 'disabled', true );

				getPromoDetails( d[index].id );
			}
			jQuery( s( 'promo_type' ) ).change( );
			jQuery( s( 'promo_trigger' ) ).change( );
			jQuery( '.settingsHeader' ).removeClass( 'ui-state-highlight' )
				.addClass( 'ui-state-default' );
		} );

//	jQuery( '#showHidePromos' ).button( ).change( function( ) {
//		//refreshPromos( );
//		jQuery( '#promoList option.promoInactive' ).toggleOption( jQuery( this ).is( ':checked' ) );
//		var current = jQuery('#showHidePromosLabel span').text();
//		if (current == 'Show Inactive') {
//			jQuery('#showHidePromosLabel span').text('Hide Inactive');
//		} else {
//			jQuery('#showHidePromosLabel span').text('Show Inactive');
//		}
//	} );

	initializeCommon( 'promo' );

}

function promo_detail( promo_id, which, id ) {
	if( which == 1 ) {
		var promo_value = $( '#mid' + id ).attr( 'checked' );
	} else {
		var promo_value = $( '#gid' + id ).attr( 'checked' );
	}

	var businessid = jQuery( '#promoForm input[name=bid]' ).val( );

	$.ajax( {
		type: "POST",
		url: ajaxUrl,
		data: "type=" + which + "& id=" + id + "& promo_id=" + promo_id + "& todo=12& promo_value="
				+ promo_value + "&businessid=" + businessid
	} );
}

function promo_businesses( promo_id, businessid, checkbox) {
        //alert('enabled: ' + enabled.checked);
        var enabled = 0;
        if (checkbox.checked == true) {
                enabled = 1;
        }
	$.ajax( {
		type: "POST",
		//url: ajaxUrl,
                url: '/ta/promotions/updateGlobalBusinesses/' + promo_id + '/' + businessid,
                //url: '/ta/promotions/businesses/' + promo_id + '/' + businessid,
		data: "promo_id=" + promo_id + "&todo=13"
                        + "&businessid=" + businessid
                        + "&enabled=" + enabled
	} );
}

function promoDetailClear( promoId, promoType, selectElem ) {
	var businessid = jQuery( '#promoForm input[name=bid]' ).val( );
	var groupId = jQuery( selectElem ).val( );

	jQuery.ajax( {
		type: "POST",
		url: '/ta/promotions/details/'+promoId+'/'+businessid,
		data: {
			promo_id: promoId,
			type: promoType,
			id: groupId,
			promo_value: true,
			clear: true,
			businessid: businessid
		}
	} );
}

jQuery( function( ) {
	initializePromos( );
} );
