jQuery.widget( 'ui.menuBoard', {
	options: {

	},
	
	intervals: [],
	intervalSpecs: [],
	
	_create: function( ) {
		jQuery( this.element ).addClass( 'ui-menuboard' );
		
		jQuery( '.pageContainer' ).show( ).height( jQuery( 'body' ).height( ) )
			.width( jQuery( 'body' ).width( ) ).addClass( 'far-left' );
		
		jQuery( '.box' ).box( );
	},
	
	start: function( ) {
		jQuery( '.pageContainer' ).removeClass( 'far-left' );
		
		this._startIntervals( );
	},
	
	addInterval: function( o ) {
		var i = this.intervalSpecs.length;
		this.intervalSpecs[i] = {
			cmd: o.cmd,
			msec: o.msec
		};
		this.intervals[i] = 0;
		var self = this;
		return function( ) {
			return self.intervals[i];
		};
	},
	
	_startIntervals: function( ) {
		for( var i in this.intervalSpecs ) {
			this.intervals[i] = setInterval( this.intervalSpecs[i].cmd, this.intervalSpecs[i].msec );
		}
	}
} );

jQuery.widget( 'ui.imageScroller', {
	options: {
		images: [],
		scrollTime: 6000,
		scrollSpeed: 250
	},
	imageDivs: null,
	imageCount: 0,
	imageCurrent: null,
	scrollInterval: null,
	scrollWidth: 0,
	scroller: null,
	
	_create: function( ) {
		this.scroller = jQuery( '<div class="ui-imagescroller" />' ).appendTo( this.element );
		for( var i in this.options.images ) {
			var container = jQuery( '<div class="ui-imagecontainer"></div>' )
				.width( this.element.width( ) ).height( this.element.height( ) )
				.appendTo( this.scroller );
			var image = jQuery( '<img class="ui-image" src="' + this.options.images[i] + '" />' )
				.appendTo( container );
			image.width( image.width( ) * ( container.height( ) / image.height( ) ) )
				.height( container.height( ) );
		}
		
		this.imageDivs = this.scroller.find( '.ui-imagecontainer' );
		this.imageCount = this.imageDivs.length;
		
		jQuery( '.ui-imagecontainer:first-child', this.element ).clone( )
			.appendTo( this.scroller );
		
		this.imageDivs.height( this.element.height( ) ).width( this.element.width( ) );
		this.scrollWidth = jQuery( this.imageDivs[0] ).outerWidth( );
		
		if( this.imageCount > 1 ) {
			this.imageCurrent = 0;
			this.scrollInterval = jQuery( 'body' ).menuBoard( 'addInterval', {
				cmd: 'updateImageScroller( "' + this.element.attr( 'id' ) + '" );',
				msec: this.options.scrollTime
			} );
		}
	},
	
	scrollImage: function( ) {
		var self = this;
		self.scroller.animate( {
			marginLeft: parseInt( self.scroller.css( 'marginLeft' ) ) - self.scrollWidth
		}, self.scrollSpeed, function( ) {
			self.imageCurrent++;
			if( self.imageCurrent == self.imageCount ) {
				self.imageCurrent = 0;
				self.scroller.css( 'marginLeft', 0 );
			}
		} );
	}
} );

function updateImageScroller( id ) {
	jQuery( '#' + id ).imageScroller( 'scrollImage' );
}

jQuery.widget( 'ui.imageFader', {
	options: {
		images: [],
		fadeTime: 5000,
		fadeSpeed: 400
	},
	imageDivs: null,
	imageCount: 0,
	imageCurrent: null,
	fadeInterval: null,
	fader: null,
	
	_create: function( ) {
		this.fader = this.element.addClass( 'ui-imagefader' );
		for( var i in this.options.images ) {
			var container = jQuery( '<div class="ui-imagecontainer" style="opacity: 0;"></div>' )
				.width( this.element.width( ) ).height( this.element.height( ) )
				.appendTo( this.fader );
			var image = jQuery( '<img class="ui-image" src="' + this.options.images[i] + '" />' )
				.appendTo( container );
			image.width( image.width( ) * ( container.height( ) / image.height( ) ) )
				.height( container.height( ) );
		}
		
		this.imageDivs = this.fader.find( '.ui-imagecontainer' );
		this.imageCount = this.imageDivs.length;
		
		this.imageDivs.height( this.element.height( ) ).width( this.element.width( ) );
		
		jQuery( this.imageDivs[0] ).css( 'opacity', 1 );
		
		if( this.imageCount > 1 ) {
			this.imageCurrent = 0;
			this.fadeInterval = jQuery( 'body' ).menuBoard( 'addInterval', {
				cmd: 'updateImageFader( "' + this.element.attr( 'id' ) + '" );',
				msec: this.options.fadeTime
			} );
		}
	},
	
	fadeImage: function( ) {
		var self = this;
		
		var curImage = jQuery( self.imageDivs[self.imageCurrent++] );
		self.imageCurrent = self.imageCurrent == self.imageCount ? 0 : self.imageCurrent;
		var nextImage = jQuery( self.imageDivs[self.imageCurrent] );
		
		curImage.animate( {
			opacity: 0
		}, {
			duration: self.fadeSpeed,
			step: function( now, fx ) {
				nextImage.css( 'opacity', 1 - now );
			}
		} );
	}
} );

function updateImageFader( id ) {
	jQuery( '#' + id ).imageFader( 'fadeImage' );
}

jQuery
	.widget( 'ui.menuFeature', {
		options: {
			detail: '',
			featureTime: 5000,
			fadeSpeed: 400,
			padding: 6,
			items: []
		},
		menuList: null,
		menuDetail: null,
		menuHighlight: null,
		items: null,
		itemCount: 0,
		itemCurrent: null,
		fadeInterval: null,
		
		_create: function( ) {
			this.menuList = jQuery( '<ul />' ).appendTo( this.element ).addClass( 'ui-menulist' );
			if( this.options.detail != '' ) this.menuDetail = jQuery( this.options.detail );
			if( !this.menuDetail || this.menuDetail.count < 1 )
				this.menuDetail = null;
			else
				this.menuDetail.addClass( 'ui-menudetail' );
			
			for( var i in this.options.items ) {
				jQuery( '<li>' + this.options.items[i].name + '<span>'
						+ this.options.items[i].price + '</span></li>' ).appendTo( this.menuList );
				if( this.menuDetail ) {
					jQuery( '<div id="' + this.element.attr( 'id' ) + '-detail' + i + '">'
							+ this.options.items[i].details + '</div>' ).appendTo( this.menuDetail );
				}
			}
			
			this.items = this.menuList.find( 'li' );
			this.itemCount = this.items.length;
			
			this.menuHighlight = jQuery( '<div class="ui-menuhighlight ui-state-default ui-corner-all" id="'
											+ this.element.attr( 'id' ) + 'Highlight">&nbsp;</div>' )
				.appendTo( this.element );
			this.menuHighlight.width( jQuery( this.items[0] ).width( ) )
				.height( jQuery( this.items[0] ).height( ) ).position( {
					my: 'center',
					at: 'center',
					of: this.items[0]
				} );
			
			if( this.itemCount > 1 ) {
				this.itemCurrent = -1;
				
				this.menuDetail.children( ).css( 'opacity', 0 );
				
				var fadeSpeed = this.options.fadeSpeed;
				this.options.fadeSpeed = 1;
				this.featureItem( );
				this.options.fadeSpeed = fadeSpeed;
				
				this.fadeInterval = jQuery( 'body' ).menuBoard( 'addInterval', {
					cmd: 'updateMenuFeature( "' + this.element.attr( 'id' ) + '" );',
					msec: this.options.featureTime
				} );
			}
		},
		
		featureItem: function( ) {
			var self = this;
			
			var curDetail = jQuery( '#' + self.element.attr( 'id' ) + '-detail' + self.itemCurrent );
			var curItem = jQuery( self.items[self.itemCurrent++] );
			self.itemCurrent = self.itemCurrent == self.itemCount ? 0 : self.itemCurrent;
			var nextDetail = jQuery( '#' + self.element.attr( 'id' ) + '-detail' + self.itemCurrent );
			var nextItem = jQuery( self.items[self.itemCurrent] );
			
			var newPosition = nextItem.position( ), 
				mlPosition = self.menuList.position( ), 
				newTop = newPosition.top + mlPosition.top, 
				newLeft = newPosition.left + mlPosition.left - self.options.padding, 
				newWidth = nextItem.width( ) + self.options.padding * 2, 
				newHeight = nextItem.height( );
			
			var _stepFunction = self.menuDetail ? function( now, fx ) {
				if( fx.prop == 'top' ) {
					var r = ( fx.now - fx.start ) / ( fx.end - fx.start );
					curDetail.css( 'opacity', 1 - r );
					nextDetail.css( 'opacity', r );
				}
			} : null;
			
			self.menuHighlight.animate( {
				top: newTop,
				left: newLeft,
				width: newWidth,
				height: newHeight
			}, {
				duration: self.options.fadeSpeed,
				step: _stepFunction
			} );
		}
	} );

function updateMenuFeature( id ) {
	jQuery( '#' + id ).menuFeature( 'featureItem' );
}

jQuery.widget( 'ui.box', {
	options: {
		margin: null,
		marginTop: null,
		marginLeft: null
	},
	
	marginTop: 6,
	marginLeft: 6,
	
	_create: function( ) {
		var e = jQuery( this.element );
		var w = e.outerWidth( true ), h = e.outerHeight( true );
		
		if( this.options.margin != null ) this.marginTop = this.marginLeft = this.options.margin;
		if( this.options.marginTop != null ) this.marginTop = this.options.marginTop;
		if( this.options.marginLeft != null ) this.marginLeft = this.options.marginLeft;
		
		e.addClass( 'ui-widget ui-widget-content ui-corner-all' );
		e.find( 'span.boxHeader' ).addClass( 'ui-widget-header ui-corner-top ui-helper-clearfix' );
		
		e.width( w - e.outerWidth( true ) + e.width( ) - this.marginLeft * 2 );
		e.height( h - e.outerHeight( true ) + e.height( ) - this.marginTop * 2 );
		
		var parentPos = e.parent( ).css( 'position' );
		if( parentPos != 'absolute' && parentPos != 'relative' )
			e.parent( ).css( 'position', 'relative' );
		e.css( 'position', 'absolute' );
		e.css( 'top', this.marginTop );
		e.css( 'left', this.marginLeft );
	}
} );

var menuList = [{
	name: 'Menu Item 1',
	price: '$2.99',
	details: 'Detail 1<br />Such a good choice!'
}, {
	name: 'Menu Item 2',
	price: '$4.99',
	details: 'Detail 2<br />Delicious!'
}, {
	name: 'Menu Item 3',
	price: '$0.79',
	details: 'Detail 3<br />Yum yum yummm!!!'
}];

var imageList = ['/assets/images/import_menu_help.png', '/assets/images/login.jpg',
		'/assets/images/weblogo.jpg'];

jQuery( function( ) {
	jQuery( 'body' ).menuBoard( );
	
	jQuery( '.imageScroller' ).imageScroller( {
		images: imageList
	} );
	jQuery( '.imageFader' ).imageFader( {
		images: imageList
	} );
	jQuery( '.menuList' ).menuFeature( {
		items: menuList,
		detail: '#myMenuDetail'
	} );
	
	jQuery( 'body' ).menuBoard( 'start' );
} );
