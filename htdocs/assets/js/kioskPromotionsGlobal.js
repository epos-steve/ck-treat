
function promo_global_update_item( promo_id, which, id ) {
	return;
	if( which == 1 ) {
		var promo_value = $( '#mid' + id ).attr( 'checked' );
	} else {
		var promo_value = $( '#gid' + id ).attr( 'checked' );
	}

	var businessid = jQuery( '#promoForm input[name=bid]' ).val( );

	$.ajax( {
		type: "POST",
		url: "/ta/promotions/updatePromoItems",
		data: "type=" + which + "&id=" + id + "&promo_id=" + promo_id + "&promo_value="	+ promo_value
	} );
}

function promo_global_update_items() {
	var checkedItems = jQuery('#promoGlobalItemForm input:checked');
	var promo_id = jQuery('#promo_id').val();
	var sdata = 'promo_id=' + promo_id;
	jQuery.each(checkedItems, function(key, value) {
		itemId = value.id.substr(3);
		sdata += "&item[]=" + itemId;
		/*
		var barpos = value.indexOf("_");
		var type = value.substr(0,barpos);
		var id = value.substr(barpos+1);
		sdata += '&' + type + '[]=' + id;
		*/
	});
	alert(sdata);
	return;
	jQuery.post(
		'/ta/promotions/updateBusinesses/' + promo_id + '/',
		encodeURI(sdata),
		function(response, textStatus, xhr){
		}
	);
}

// save and sync businesses to kiosks
function promo_global_update_businesses() {
	var tree = jQuery("#businessMappingTree").dynatree("getTree");
	var selKeys = jQuery.map(tree.getSelectedNodes(), function(node) {
			return node.data.key;
	});

	// Query the tree for enabled nodes and form a proper POST request
	var promo_id = jQuery('#promo_id').val();
	var sdata = 'promo_id=' + promo_id;
	jQuery.each(selKeys, function(key, value) {
		var barpos = value.indexOf("_");
		var type = value.substr(0,barpos);
		var id = value.substr(barpos+1);
		sdata += '&' + type + '[]=' + id;
	});
	jQuery.post(
		'/ta/promotions/updateBusinesses/' + promo_id + '/',
		encodeURI(sdata),
		function(response, textStatus, xhr){
		}
	);

}