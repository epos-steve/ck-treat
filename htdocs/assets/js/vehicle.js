var createConfirm = function(){
	var dialog = $('<div></div>').attr('id', 'confirm').attr('title', 'Confirm')
	$(document.body).append( dialog );
	dialog.dialog({
		autoOpen: false,
		resizable: false,
		buttons: {
			'Yes': function(){},
			'No': function(){$(this).dialog('close');}
		}
	});
	$('#confirm + .ui-dialog-buttonpane button:contains(Yes)').attr('id', 'confirm_button');
};

var jConfirm = function( msg, func ){
	$('#confirm').html( msg ).dialog( 'open' );
	$('#confirm_button').unbind().click( function(){ $('#confirm').dialog('close'); func.call(this); } );
};

$(function () {
	createConfirm();
	$('.clickable').css('cursor', 'pointer');
	$('#vehicles').tablesorter();

	$('img.delete').click(function () {
		var img = this;
		$(this).parent().parent().css('background-color', '#FFCCCC');
		$('#confirm').bind('dialogclose', function() {$(img).parent().parent().css('background-color', '#E8E7E7');});
		jConfirm('Are you sure you want to delete this vehicle?', function () {
			$(img).parent().parent().fadeOut().fadeIn().fadeOut();
		});
	});

	$('img.edit').click(function () {
		alert('edit');
	});
});
