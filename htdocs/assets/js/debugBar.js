if( typeof jQuery != 'undefined' ) {
	setTimeout( function( ) {
		jQuery( 'body' ).css( {
			//position: 'relative',
			//top: '30px'
			marginTop: '+=30px'
		} ).resize( );
		
		var debugBarStyle = 'position: fixed; '
						  + 'font-size: 12px; '
						  + 'top: 0; left: 0; '
						  + 'width: 100%; height: 24px; '
						  + 'border-bottom: solid 1px rgb(64,64,200); '
						  + 'background: rgb(64,64,200); '
						  + 'background: linear-gradient(top,rgb(64,64,200),rgb(200,200,255)); '
						  + 'background: -moz-linear-gradient(top,rgb(64,64,200),rgb(200,200,255)); '
						  + 'background: -webkit-linear-gradient(top,rgb(64,64,200),rgb(200,200,255)); '
						  + 'background: -o-linear-gradient(top,rgb(64,64,200),rgb(200,200,255)); '
						  + 'filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#4040ff\', endColorstr=\'#c8c8ff\'); '
						  ;
		var debugBar = jQuery( '<div style="' + debugBarStyle + '" />' ).appendTo( 'body' );
		
		var debugButtonStyle = 'float: right; '
							 + 'margin: 2px 4px; '
							 ;
		var debugButtonHTML = '<a style="' + debugButtonStyle + '" />';
		
		var getCookie = function( cookie ) {
			var cookies = document.cookie.split( '; ' );
			for( var i = 0, pair; pair = cookies[i] && cookies[i].split( '=' ); i++ )
				if( pair[0] == cookie ) return pair[1];
			
			return null;
		};
		
		var setCookie = function( cookie, value ) {
			document.cookie = cookie + '=' + value + '; expires=Wednesday, 01-Aug-2040 08:00:00 GMT';
			return value;
		};
		
		var makeButton = function( text, cookie, defValue, reload, click ) {
			var btn = jQuery( debugButtonHTML ).html( text ).appendTo( debugBar );
			var value = getCookie( cookie ) || setCookie( cookie, defValue );
			
			if( jQuery.ui != undefined ) {
				btn.button( {
					text: true,
					icons: { primary: 'ui-icon-circle-close' }
				} );
			}
			
			var clickButton = function( ) {
				var checked = jQuery( this ).data( 'checked' );
				if( !checked ) {
					jQuery( this ).data( 'checked', true ).addClass( 'ui-state-highlight' )
						.find( '.ui-icon' )
							.removeClass( 'ui-icon-circle-close' )
							.addClass( 'ui-icon-circle-check' );
				} else {
					jQuery( this ).data( 'checked', false ).removeClass( 'ui-state-highlight' )
						.find( '.ui-icon' )
							.removeClass( 'ui-icon-circle-check' )
							.addClass( 'ui-icon-circle-close' );
				}
				
				setCookie( cookie, !checked );
				
				if( typeof click == 'function' )
					click.call( this );
			};
			
			btn.click( function( ) {
				clickButton.call( this );
				
				if( reload )
					location.reload( );
			} ).data( 'checked', false );
			btn.find( '.ui-button-text' ).css( 'padding', '0 0 0 1.3em' );
			btn.find( '.ui-icon' ).css( 'left', '0' );
			
			if( value === 'true' || value === true )
				clickButton.call( btn );
			
			return btn;
		};
		
		var findComments = function( baseNode, callback ) {
			var node = baseNode.firstChild;
			
			while( node ) {
				if( node.nodeType === 8 ) {
					callback( node );
				} else if( node.nodeType === 1 ) {
					findComments( node, callback );
				}
				
				node = node.nextSibling;
			}
		};
		
		
		
		
		
		/**********************
		 * BUTTONS START HERE *
		 **********************/
		
		// dbontop
		var dbontop = makeButton( 'dB on top', 'dbontop', true, false, function( ) {
			debugBar.css( 'z-index', jQuery( this ).data( 'checked' ) ? 99999 : 1 );
		} );
		
		// nomin
		var nomin = makeButton( 'nomin', 'nomin', false, true );
		
		// debug comments
		var dbgComments = makeButton( 'debug comments', 'debugcomm', false, false, function( ) {
			if( jQuery( this ).data( 'checked' ) ) {
				findComments( jQuery( 'body' ).get( 0 ), function( node ) {
					if( node.nodeValue.match( /^\s*<debug>.*<\/debug>\s*$/ ) )
						jQuery( node ).after( '<div class="debugcomm-div">' + node.nodeValue.replace( /<[\/]?debug>/g, '' ) );
				} );
			} else {
				jQuery( '.debugcomm-div' ).remove( );
			}
		} );
		
		
		
		
		/********************
		 * BUTTONS END HERE *
		 ********************/
		
		
		
		
		// make the bar label
		var labelStyle = 'display: inline-block; '
					   + 'float: left; '
					   + 'font-size: 16px; '
					   + 'font-weight: bold; '
					   + 'color: white; '
					   + 'text-shadow: rgb(64,64,200) 2px 2px 2px; '
					   + 'margin: 2px 8px; '
					   + 'padding-right: 6px; '
					   + 'border-right: solid 2px rgb(64,64,200); '
					   + '-moz-border-right-colors: rgb(64,64,200) rgb(200,200,255);';
		jQuery( '<span style="' + labelStyle + '">debugBar</span>' ).appendTo( debugBar );
		
	}, 1000 );
}
