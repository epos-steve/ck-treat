jQuery.ajaxSetup( {
	cache: false,
	complete: function( jqXHR, textStatus ) {
		if( textStatus != 'success' ) {
			alert( 'There was an error processing your request [' + textStatus + ']' );
		}
	}
} );

if( typeof( String.prototype.capitalize ) == "undefined" ) {
	String.prototype.capitalize = function( ) {
		return this.charAt( 0 ).toUpperCase( )  + this.slice( 1 );
	};
}

if( typeof( getTitle ) == "undefined" ) {
	function getTitle( d ) {
		for( var i in d ) {
			if( i.substr( -4 ) == 'Name' ) return d[i];
		}
	}
}

if( typeof( scrollbarWidth ) == "undefined" ) {
	function scrollbarWidth( ) {
		var div = jQuery( '<div style="width:50px;height:50px;overflow:hidden;position:absolute;top:-200px;left:-200px;"><div style="height:100px;" /></div>' );
		jQuery( 'body' ).append( div );
		var w1 = jQuery( 'div', div ).innerWidth( );
		div.css( 'overflow-y', 'scroll' );
		var w2 = jQuery( 'div', div ).innerWidth( );
		div.remove( );
		return w1 - w2;
	}
}

if( typeof( jQuery.expr[':'].iContains ) == "undefined" ) {
	jQuery.expr[':'].iContains = function( a, i, m ) {
		return ( a.textContent || a.innerText || "" ).toUpperCase( ).indexOf( m[3].toUpperCase( ) ) >= 0;
	};
}
