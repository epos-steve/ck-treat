jQuery.widget( 'ui.combo', {
	options: {
		source: [],
		sourceWithIds: false,
		select: function( ) {},
		watermark: '',
		alwaysEmpty: false,
		alignment: 'left'
	},
	
	box: null,
	btn: null,
	container: null,
	acSource: null,
	
	_create: function( ) {
		var self = this;
		
		var box = jQuery( this.element ).css( { 
			margin: 0,
			display: 'inline-block',
			verticalAlign: 'middle'
		} );
		this.box = box;
		var container = box.wrap( '<div id="' + box.attr( 'id' ) + 'Container" />' ).parent( ).css( {
			whiteSpace: 'nowrap',
			position: 'relative'
		} );
		this.container = container;
		var btn = jQuery( '<span id="' + box.attr( 'id' ) + 'Btn"></span>' );
		this.btn = btn;
		
		box.after( btn );
		box.data( 'ui.combo', this );
		
		btn.button( {
			text: false,
			icons: { primary: 'ui-icon-triangle-1-s' }
		} ).hover( function( ) {
			btn.add( box ).addClass( 'ui-state-hover' );
		}, function( ) {
			btn.add( box ).removeClass( 'ui-state-hover' );
		}).click( function( ) {
			box.focus( );
		} ).removeClass( 'ui-corner-all' ).addClass( 'ui-corner-right' ).css( {
			display: 'inline-block',
			verticalAlign: 'middle',
			position: 'relative',
			left: -1
		} );
		
		this._updateSource( );
		
		box.autocomplete( {
			delay: 0,
			minLength: 0,
			source: self.acSource,
			position: {
				my: this.options.alignment + ' top',
				at: this.options.alignment + ' bottom',
				offset: ( this.options.alignment == 'right' ? ( btn.outerWidth( true ) - 1 ) : '0' ) + ' 0'
			}
		} ).hover( function( ) {
			box.add( btn ).addClass( 'ui-state-hover' );
		}, function( ) {
			box.add( btn ).removeClass( 'ui-state-hover' );
		} ).focus( function( ) {
			box.css( 'width', box.width( ) );
			if( self.options.watermark && box.val( ) == self.options.watermark )
				box.val( '' ).removeClass( 'ui-combo-watermark' );
			box.autocomplete( 'search' ).addClass( 'ui-state-focus' );
		} ).blur( function( ) {
			if( self.options.alwaysEmpty || !box.val( ) )
				box.val( self.options.watermark );
			if( self.options.watermark && box.val( ) == self.options.watermark )
				box.addClass( 'ui-combo-watermark' );
			
			box.removeClass( 'ui-state-focus' );
		} ).bind( 'autocompleteselect', self._select ).addClass( 'ui-state-default ui-corner-left' );
		
		if( self.options.watermark )
			box.val( self.options.watermark ).addClass( 'ui-combo-watermark' );
		
		//var cmd ='jQuery( "#' + this.container.attr( 'id' ) + '" ).height( ' + box.outerHeight( true ) + ' ).width( ' + box.outerWidth( true ) + ' + ' + btn.outerWidth( true ) + ' - 1 );'; 
		//eval( cmd );
	},
	
	_select: function( event, ui ) {
		var self = jQuery( this ).data( 'ui.combo' );
		
		if( self.options.sourceWithIds ) {
			var id = null;
			for( var i in self.options.source ) {
				if( self.options.source[i] == ui.item.value ) {
					id = i;
					break;
				}
			}
			self.options.select( event, ui, id );
		} else {
			self.options.select( event, ui );
		}
	},
	
	source: function( sourceArray ) {
		this.options.source = sourceArray;
		this._updateSource( );
	},
	
	_updateSource: function( ) {
		this.acSource = [];
		for( var i in this.options.source ) {
			var val;
			if( this.options.selectWithIds )
				val = { label: this.options.source[i], value: i };
			else val = this.options.source[i];
			this.acSource.push( val );
		}
		this.box.autocomplete( 'option', 'source', this.acSource );
	}
} );