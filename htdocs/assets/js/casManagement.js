// report old browsers
jQuery.waffle.compat( );

jQuery.ajaxSetup( {
	cache: false,
	complete: function( jqXHR, textStatus ) {
		if( textStatus != 'success' ) {
			jQuery.waffle.error( 'There was an error processing your request [' + textStatus + ']' );
		}
	}
} );

if( typeof( String.prototype.capitalize ) == "undefined" ) {
	String.prototype.capitalize = function( ) {
		return this.charAt( 0 ).toUpperCase( ) + this.slice( 1 );
	};
}

if (!Array.prototype.filter)
{
	Array.prototype.filter = function( fun, thisp )
	{
		var len = this.length;
		if (typeof fun != "function") {
			throw new TypeError();
		}

		var res = new Array();
		for (var i = 0; i < len; i++)
		{
			if (i in this)
			{
				var val = this[i]; // in case fun mutates this
				if (fun.call(thisp || window, val, i, this)) {
					res.push(val);
				}
			}
		}

		return res;
	};
}

if( typeof ( getTitle ) == "undefined" ) {
	function getTitle( d ) {
		for( var i in d ) {
			if( i.substr( -4 ) == 'Name' ) return d[i];
		}
	}
}

if( typeof( scrollbarWidth ) == "undefined" ) {
	function scrollbarWidth( ) {
		var div = jQuery( '<div style="width:50px;height:50px;overflow:hidden;position:absolute;top:-200px;left:-200px;"><div style="height:100px;"></div>' );
		// Append our div, do our calculation and then remove it
		jQuery( 'body' ).append( div );
		var w1 = jQuery( 'div', div ).innerWidth( );
		div.css( 'overflow-y', 'scroll' );
		var w2 = jQuery( 'div', div ).innerWidth( );
		div.remove( );
		return ( w1 - w2 );
	}
}

if( typeof( jQuery.expr[':'].iContains ) == "undefined" ) {
	jQuery.expr[':'].iContains = function( a, i, m ) {
		return ( a.textContent || a.innerText || "" ).toUpperCase( ).indexOf( m[3].toUpperCase( ) ) >= 0;
	};
}

jQuery( function( ) {
	PageController.initializePage( );
	CashierController.initializeCashiers( );
	ImageController.initialize();
	ScreenController.initializeScreens( );
	FPAssignController.initialize( );
	ModifierController.initializeModifiers( );
} );
$(function() {
	$( document ).tooltip({
		tooltipClass: "functionPanelTooltip"
	});
});

var PageController = {
	/** @memberOf PageController */
	initializePage: function( ) {
		this.statusComponentList( );
		this.initializeButtons( );
	},
	
	statusComponentList: function( ) {
		var self = this;
		
		jQuery( '.components-table' ).each( function( ) {
			jQuery( '.components-showmore', jQuery( this ).parent( ) ).button( {
				icons: {
					primary: 'ui-icon-minus'
				}
			} ).data( {
				status: true,
				table: jQuery( this )
			} ).click( function( ) {
				self.statusComponentShowmore( jQuery( this ) );
			} ).click( );
		} );
	},
	
	initializeButtons: function( ) {
		
		jQuery( '.rebootTerminal' ).button( {
			label: 'Reboot'
		} ).click( function( event ) {
			event.preventDefault( );
			
			var pid = jQuery(this).val( );
			
			if( confirm( 'Are you sure you want to reboot terminal "' + pid + '"?' ) ) {
				jQuery.ajax( {
					type: 'POST',
					url: 'rebootTerminal',
					dataType: 'json',
					data: { pid: pid },
					success: function( data ) {
						if( !data || !data.success || data.error ) {
							//jQuery.waffle.error( data && data.error && !data.message || 'There was an error processing your request. Error: ' + data.message );
							jQuery.waffle.error('There was an error processing your request. Error: ' + data.message );
							return false;
						}
						
						jQuery.waffle.pop( 'Terminal "' + pid + '" rebooted successfully.' );
					}
				} );
			}
		
	} );
	},
	
	statusComponentShowmore: function( el ) {
		var shown = el.data( 'status' );
		var table = el.data( 'table' );
		
		if( !shown ) {
			table.find( 'tr' ).show( );
			el.button( 'option', 'icons', { primary: 'ui-icon-minus' } ).find( 'span' ).text( 'Hide extra' );
			el.data( 'status', true );
		} else {
			table.find( 'tr:not(.ui-state-highlight):not(:first-child)' ).hide( );
			el.button( 'option', 'icons', { primary: 'ui-icon-plus' } ).find( 'span' ).text( 'Show more' );
			el.data( 'status', false );
		}
	}
};

var CashierController = {
	/** @memberOf CashierController */
	isInitialized: false,
	
	initializeCashiers: function( ) {
		CashierController.isInitialized = true;
		CashierController.getCashiers( );
		
		jQuery( '#cashierList' ).change( CashierController.setCashierForm );
		jQuery( '#nonCashierList' ).change( CashierController.setCashierForm );
		
		jQuery( '.cashierDelete' ).click( CashierController.deleteCashier );
		
		jQuery( 'form .cashierOption' ).live( {
			change: function( ) {
				jQuery( this ).closest( '.settingsContainer' ).find( '.settingsHeader' )
					.removeClass( 'ui-state-default' ).addClass( 'ui-state-highlight' );
			}
		} );
		
		jQuery( '.cashierOption' ).attr( 'disabled', true );
		
		jQuery( '#cashierSettings' ).find( '.settingsForm' ).not( '.noAutoSubmit' ).submit( CashierController.submitAjaxForm );
		
		CashierController.resetCashierForm( );
	},
	
	getCashiers: function( ) {
		var selectedCashier = jQuery( '#cashierList option:selected' ).attr( 'id' );
		var selectedNonCashier = jQuery( '#nonCashierList option:selected' ).attr( 'id' );
		
		jQuery.ajax( {
			url: 'getCashiers?bid=' + jQuery( '#cashierbid' ).val( ),
			dataType: 'json',
			success: function( data ) {
				if( !data || typeof( data.cashiers ) == undefined || data.error ) {
					jQuery.waffle.error( data && data.error || 'There was an error processing your request.' );
					return false;
				}
				
				// cashiers
				CashierController.setCashierList( 'cashier', data.cashiers );
				
				// non cashiers
				CashierController.setCashierList( 'nonCashier', data.nonCashiers );
				
				jQuery( '#cashierList option#' + selectedCashier ).attr( 'selected', 'selected' );
				jQuery( '#cashierList' ).change( );
				
				jQuery( '#nonCashierList option#' + selectedNonCashier ).attr( 'selected', 'selected' );
				jQuery( '#nonCashierList' ).change( );
			}
		} );
	},
	
	setCashierList: function( name, data, onlyData ) {
		var selector = '#' + name + 'List';
		
		jQuery( selector ).empty( );
		jQuery( selector ).data( name + 's', data );
		
		if( onlyData == true ) return;
		
		var max = 0;
		for( var i in data ) {
			jQuery( selector ).append( '<option id="cashier' + data[i].loginid + '" value="' + i + '">'
										+ data[i].name + '</option>' );
			max = i;
		}
		jQuery( selector ).data( 'max', max );
		jQuery( selector ).data( 'name', name );
	},
	
	resetCashierForm: function( ) {
		jQuery( '#cashierForm input[type=text]' ).val( '' );
		jQuery( '#cashierForm option:selected' ).removeAttr( 'selected' );
		jQuery( '#cashierForm input[name=cashier_loginid]' ).val( 0 );
		jQuery( '#cashierForm input.value' ).val( '' ).removeData( );
	},
	
	setCashierForm: function( ) {
		var otherList = jQuery( '#cashierList, #nonCashierList' ).not( this );
		jQuery( 'option:selected', otherList ).removeAttr( 'selected' );
		
		var name = jQuery( this ).data( 'name' );
		var d = jQuery( this ).data( name + 's' );
		var index = jQuery( this ).val( );
		
		jQuery( '.cashierOption' ).attr( 'disabled', false );
		CashierController.resetCashierForm( );
		
		var t = function( n ) {
			return '#cashierForm input[name=' + n + ']';
		};
		var s = function( n ) {
			return '#cashierForm select[name=' + n + ']';
		};
		
		for( var i in d[index] ) {
			var elem = jQuery( t( 'cashier_' + i ) );
			if( elem.length ) {
				if( typeof( d[index][i] ) == 'object' )
					elem.data( 'object', d[index][i] );
				else elem.val( d[index][i] );
			}
			
			elem = jQuery( s( 'cashier_' + i ) );
			if( elem.length ) {
				elem.find( 'option:selected' ).removeAttr( 'selected' );
				elem.find( 'option[value=' + d[index][i] + ']' ).attr( 'selected', 'selected' );
			}
		}
		
		if( name == 'nonCashier' ) {
			jQuery( '#deleteCashier' ).attr( 'disabled', true );
		}
		
		jQuery( '.settingsHeader' ).removeClass( 'ui-state-highlight' ).addClass( 'ui-state-default' );
	},
	
	submitAjaxForm: function( ) {
		if( jQuery( this ).hasClass( 'noAutoSubmit' ) )
			return;
		
		var submitBtn = jQuery( this ).find( 'input[type=submit]' );
		submitBtn.attr( 'disabled', 'disabled' );
		
		var disabledFields = jQuery( this ).find( 'input:disabled' );
		disabledFields.removeAttr( 'disabled' );
		var formData = jQuery( this ).serialize( );
		disabledFields.attr( 'disabled', 'disabled' );
		
		var fn;
		if( (fn = jQuery( this ).data( 'serialize' )) )
			formData += eval( fn + '( this )' );
		
		jQuery.ajax( {
			type: "POST",
			url: jQuery( this ).attr( 'action' ),
			dataType: 'json',
			data: formData,
			success: function( r ) {
				if( !r || !r.id || r.error ) {
					jQuery.waffle.error( r && r.error || 'An error has occurred' );
				} else {
					CashierController.getCashiers( );
					jQuery.waffle.pop( r.message );
					CashierController.selectCashier( r.id );
				}
			}
		} );
		
		setTimeout( "jQuery('#" + submitBtn.attr( 'id' ) + "').attr('disabled', '')", 1000 );
	},
	
	selectCashier: function( id ) {
		var p = jQuery( '#cashierList' );
		jQuery( 'option:selected', p ).removeAttr( 'selected' );
		jQuery( 'option[value=' + id + ']', p ).attr( 'selected', 'selected' );
		p.change( );
	},
	
	selectNonCashier: function( id ) {
		var p = jQuery( '#nonCashierList' );
		jQuery( 'option:selected', p ).removeAttr( 'selected' );
		jQuery( 'option[value=' + id + ']', p ).attr( 'selected', 'selected' );
		p.change( );
	},
	
	deleteCashier: function( id, bid ) {
		id = parseInt( id ) == id ? id : jQuery( '#cashierForm input[name=cashier_loginid]' ).val( );
		bid = parseInt( bid ) == bid ? bid : jQuery( '#cashierForm input[name=bid]' ).val( );
		
		jQuery.ajax( {
			url: 'deleteCashier?id=' + parseInt( id ) + '&bid=' + parseInt( bid ),
			type: 'post',
			dataType: 'json',
			success: function( data ) {
				if( !data || !data.id || data.error ) {
					return jQuery.waffle.error( data && data.error || 'An error has occurred' );
				}
				CashierController.getCashiers( );
				jQuery.waffle.pop( data.message );
				CashierController.selectNonCashier( data.id );
			}
		} );
		return false;
	},
	
	
	__end_CashierController: function( ){}
};

/*
	Creates Image Add and Delete Forms.
 */
var ImageController = {

	isInitialized: false,
	upload_images_waffle: null,

	initialize: function( ) {
		if( ScreenController.isInitialized ) {
			return;
		}

		ImageController.isInitialized = true;

		var bid = jQuery( '#screenbid').val( );

		ImageController.genImageUploadForm( 'product-image-upload', 'product', 1, bid );
		ImageController.genImageUploadForm( 'product-combo-image-upload', 'product-combo', 1, bid );
		ImageController.genImageUploadForm( 'product-special-image-upload', 'product-special', 1, bid );
		ImageController.genImageUploadForm( 'display-group-image-upload', 'display-group', 2, bid );
		ImageController.genImageUploadForm( 'modifier-image-upload', 'modifier', 3, bid );

	},

	burn_image_waffle: function ( ) {
		if ( ImageController.upload_images_waffle ) {
			jQuery.waffle.burn( ImageController.upload_images_waffle );
			ImageController.upload_images_waffle = null;
		}
	},

	/*
	Used to Generate the Image upload and delete form

	Example:
		<div id="product-image-upload></div>
		<script>
			ImageController.genImageUploadForm( 'product-image-upload', 'product', 1, bid );
		</script>
	@param: tag ID placeholder tag
	@param: name Unique name
	@param: category Image Category
	@param: businessid
	 */
	genImageUploadForm: function ( tag, name, category, businessid ) {

		var template;
		template = '<fieldset style="margin-top: 10px" class="ui-widget-content ui-helper-clearfix">';
		template += '<form id="dialog-form-upload-' + name + '-images" name="dialog-form-upload"';
		template += 'action="uploadImage" method="post" enctype="multipart/form-data">';
		template += '<input type="hidden" name="id" value="0" />';
		template += '<input type="hidden" name="category_id" value="' + category + '" />';
		template += '<input type="hidden" name="businessid" value="' + businessid + '" />';
		template += '<input type="file" name="file" />';
		template += '<div>Add Image</div></form>';
		template += '<form id="form-delete-' + name + '-images">';
		template += '<div id="' + name + '-images"></div>';
		template += '<span id="' + name + '-images-delete" class="file_upload">Delete Selected</span>';
		template += '</form>';
		template += '</fieldset>';

		var holder = jQuery( '#' + tag );
		holder.html( template );

		jQuery('#dialog-form-upload-' + name + '-images').fileUploadUI({
			dragDropSupport: false,
			buildUploadRow: function (files, index) {
				ImageController.upload_images_waffle = jQuery.waffle.pop({
					autoClose: false,
					clickClose: false,
					spinner: true,
					content: 'Please Wait while the image is uploading...',
					weight: -10
				});
			},
			onLoad: function (event, files, index, xhr, handler) {
				try {
					var data = jQuery.parseJSON(typeof xhr.responseText !== 'undefined' ? xhr.responseText
						: xhr.contents().text());
					var response = jQuery.extend({}.dataResponse, data);
					ImageController.burn_image_waffle();
					if (response.error == true) {
						jQuery.waffle.error(response.message || 'There was an error processing your request.');
					} else {
						ImageController.getImages( name );
					}
				} catch (err) {
					ImageController.burn_image_waffle();
					jQuery.waffle.error('Error uploading Image.');
				}

			},
			onError: function (event, files, index, xhr, handler) {
				ImageController.burn_image_waffle();
				jQuery.waffle.error('Error uploading Image.');
			}
		});

		ImageController.genDeleteButton( name );
	},

	genDeleteButton: function ( name ) {

		jQuery('#' + name + '-images-delete').button({


		}).click(function (event) {
			event.preventDefault();

			var id = ImageController.getFormID( name );
			var category_id = ImageController.getFormCategoryID( name );
			var business_id = jQuery( '#screenbid' ).val();

			var delete_numbers = jQuery( '#form-delete-' + name + '-images' ).serialize();

			if (delete_numbers == "") {
				jQuery.waffle.error( 'Please select an Image to delete' );
				return
			}

			jQuery.ajax({
				type: 'POST',
				url: 'deleteImages',
				dataType: 'json',
				data: delete_numbers + '&id=' + id + '&category_id=' + category_id +
					'&businessid=' + business_id,
				success: function (data) {
					if (!data || !data.success || data.error) {
						jQuery.waffle.error(data && data.error || 'There was an error processing your request.');
						return false;
					}

					ImageController.getImages( name );
				}
			});
		});
	},

	/*
	Get html to show Images

	Note: To get background color, you need to add an id tag toe pos-color
		dropdown. "Unique Name"-pos-color
	@param: name Unique name
	 */
	getImages: function( name ) {
		var images = jQuery( '#' + name + '-images' );

		var id = ImageController.getFormID( name );
		var category_id = ImageController.getFormCategoryID( name );
		var pos_color_id = jQuery( '#' + name + '-pos-color').val( );

		if ( isNaN( id ) || id == '0' ) {
			return
		}

		if ( !pos_color_id || pos_color_id == '0' ) {
			pos_color_id = 'default';
		}

		jQuery.ajax( {
			url: 'getImages?id=' + id + '&category_id=' + category_id,
			dataType: 'json',
			success: function( data ) {
				if (!data || typeof( data.screens ) == undefined || data.error) {
					jQuery.waffle.error(data && data.error || 'There was an error processing your request.');
					return false;
				}
				var imageArray = data['images'];
				if (imageArray.length > 0) {
					var pi_html = '';
					var input_html = '';
					var cols = 3;
					var spacer = '<tr><td colspan="' + cols + '" style="height: 0.2em; border: 0;"></td></tr>';
					pi_html += '<table class="screenImages" cellpadding="0" cellspacing="1"><tr>';
					pi_html += spacer;
					input_html += '<tr>';
					var i;
					// Add the latest datetime to prevent firefox from caching the images.
					var date_cache = new Date().getTime();
					for (i = 0; i < imageArray.length; ++i) {
						// Break the row every 3
						if (i % cols == 0 && i != 0) {
							pi_html += '</tr><tr>';
							input_html += '</tr>';
							pi_html += input_html;
							input_html = '';
							// Add spacer
							pi_html += spacer + '<tr>';
						}
						pi_html += '<td class="product-button-color-' + pos_color_id + '">';
						pi_html += '<img src="' + imageArray[i]['url'] + '&date_cache=' + date_cache + '"></td>';
						input_html += '<th>' + imageArray[i]['image_size'];
						input_html += '<input name="delete_image[]" type="checkbox" ' + 'value="' +
							imageArray[i]['number'] + '" /></th>';
					}
					pi_html += '</tr>';
					input_html += '</tr>' + spacer + '</table>';
					images.html( pi_html + input_html );
				} else {
					ImageController.resetImageHtml( name );
				}
			}
		} );
	},

	getForm: function ( name ) {
		return jQuery('#dialog-form-upload-' + name + '-images');
	},

	setFormID: function ( name, id ) {
		var form = ImageController.getForm( name );
		form.find('input:hidden[name="id"]').val( id );
	},

	getFormValue: function ( name, val ) {
		var form = ImageController.getForm( name );
		return form.find('input:hidden[name="' + val + '"]').val( );
	},

	getFormID: function ( name ) {
		return ImageController.getFormValue( name, 'id' );
	},

	getFormCategoryID: function ( name ) {
		return ImageController.getFormValue( name, 'category_id' );
	},

	setFormDisabled: function ( name, disabled ) {
		var formInput = jQuery('#dialog-form-upload-' + name + '-images :input');
		formInput.prop( 'disabled', disabled );

		// Would like to disable image delete?
	},

	resetImageForms: function( name ) {
		ImageController.setFormID( name, 0 );
		ImageController.resetImageHtml( name );
	},

	resetImageHtml: function( name ) {
		var default_html = '<table class="screenImages"><tr><td style="text-align: center">' +
			'No Images</td></tr></table>';
		jQuery( '#' + name + '-images').html( default_html );
	}

};

var ScreenController = {
	/** @memberOf ScreenController */
	isInitialized: false,
	
	gridSize: 0,
	selectedIndex: null,
	needSave: false,
	
	updatingOrder: false,
	needUpdateOrder: false,
	panelGroups: null,
	panelItems: null,

	
	initializeScreens: function( ) {
		if( ScreenController.isInitialized )
			return;
		
		ScreenController.isInitialized = true;
		
		jQuery.expr[':'].iContains = function( a, i, m ) {
			return ( a.textContent || a.innerText || "" ).toUpperCase( ).indexOf( m[3].toUpperCase( ) ) >= 0;
		};
		
		ScreenController.initializePanels( );
		
		ScreenController.initializeDialogs( );
		
		ScreenController.initializeButtons( );
		
		ScreenController.getScreens( );
		
		jQuery( window ).resize( function( ) {
			ScreenController.resizeScreen( );
		} );
		
		jQuery( '#screenEditContainer' ).on( { 
			ajaxeditcancel: function( ) {
				ScreenController.editNone( );
			},
			ajaxeditaftersubmit: function( ) {
				ScreenController.getScreens( );
			},
            ajaxeditdatachange: function( val, data ) {

                var menu_type = data['menu_type'];
                if ( menu_type ) {
                    ImageController.setFormDisabled( menu_type, false );
                    ImageController.getImages( menu_type );
                }
            }
		} );
	},
	
	initializePanels: function( ) {
		ScreenController.panelGroups = jQuery( '#screenGroups' ).aerispanel( {
			reorder: ScreenController.updateGroupsOrder,
			change: function( event, ui ) {
				ScreenController.updateGroupButtons( ui && ui.index || -1 );
				if( ui && ui.index != undefined )
					ScreenController.showScreen( ui.index );
			},
			click: function( ) {
				ScreenController.editGroup( jQuery( this ).data( 'index' ) );
			}
		} );
		ScreenController.panelItems = jQuery( '#screenItems' ).aerispanel( {
			reorder: ScreenController.updateItemsOrder,
			change: function( event, ui ) {
				ScreenController.updateItemButtons( ui && ui.index || -1 );
			},
			click: function( ) {
				ScreenController.editItem( jQuery( '#currentScreen' ).val( ), jQuery( this ).data( 'index' ) );
			}
		} );
	},
	
	initializeDialogs: function( ) {
		jQuery( '#screenAddItemDialog' ).dialog( {
			autoOpen: false,
			hide: 'slide',
			show: 'slide',
			modal: true,
			resizable: false,
			title: 'Add Item',
			height: 350,
			buttons: {
				'Add': function( ) {
					var dialog = jQuery( this );
					
					var bid = jQuery( '#screenbid' ).val( );
					var screens = jQuery( '#screenItems' ).data( 'screens' );
					var group = jQuery( this ).data( 'group' );
					var screenId = screens['__root__'].items[group].id;
					var screenType = screens['__root__'].items[group].type;

					var items = [];
					jQuery( '#screen_AddItemSelect :selected' ).each( function( ) {
						items.push( jQuery( this ).attr( 'id' ) );
					} );
					
					jQuery.ajax( {
						type: 'POST',
						url: 'screenAddItem',
						dataType: 'json',
						data: { bid: bid, screenId: screenId, screenType: screenType, items: items },
						success: function( data ) {
							if( !data || !data.success || data.error ) {
								alert( data && data.error || 'There was an error processing your request.' );
								return false;
							}
							
							dialog.dialog( 'close' );
							ScreenController.needSave = true;
							ScreenController.getScreens( );
						}
					} );
				}
			}
		} );
		
		jQuery( '#screenAddGroupDialog' ).dialog( {
			autoOpen: false,
			hide: 'slide',
			show: 'slide',
			modal: true,
			resizable: false,
			title: 'Add Group',
			height: 350,
			buttons: {
				'Add': function( ) {
					var dialog = jQuery( this );
					
					var bid = jQuery( '#screenbid' ).val( );
					var items = [];
					jQuery( '#screen_AddGroupSelect :selected' ).each( function( ) {
						items.push( jQuery( this ).attr( 'id' ) );
					} );
					
					jQuery.ajax( {
						type: 'POST',
						url: 'screenAddGroup',
						dataType: 'json',
						data: { bid: bid, items: items },
						success: function( data ) {
							if( !data || !data.success || data.error ) {
								alert( data && data.error || 'There was an error processing your request.' );
								return false;
							}
							
							dialog.dialog( 'close' );
							ScreenController.getScreens( );
						}
					} );
				}
			}
		} );
	},
	
	initializeButtons: function( ) {
		// group buttons
		jQuery( '#screenNewGroup' ).button( {
			text: false,
			icons: { primary: 'ui-icon-document' }
		} ).click( function( event ) {
			event.preventDefault( );
			ScreenController.editGroup( );
		} );
		
		jQuery( '#screenAddGroup' ).button( {
			text: false,
			icons: { primary: 'ui-icon-plusthick' }
		} ).click( function( event ) {
			event.preventDefault( );
			jQuery( '#screenAddGroupDialog' ).dialog( 'open' );
			ScreenController.getGroupList( );
		} );
		
		jQuery( '#screenRemoveGroup' ).button( {
			text: false,
			icons: { primary: 'ui-icon-closethick' }
		} ).click( function( event ) {
			event.preventDefault( );
			
			var bid = jQuery( '#screenbid' ).val( );
			var selectedGroup = jQuery( '#screenGroups' ).aerispanel( 'selected' );
			
			if( !( selectedGroup >= 0 ) )
				return;
			
			var group = jQuery( '#screenItems' ).data( 'screens' )['__root__'].items[selectedGroup];
			
			if( confirm( 'Are you sure you want to remove "' + group.name + '"?' ) ) {
				jQuery.ajax( {
					type: 'POST',
					url: 'screenRemoveGroup',
					dataType: 'json',
					data: { bid: bid, groupId: group.id },
					success: function( data ) {
						if( !data || !data.success || data.error ) {
							jQuery.waffle.error( data && data.error || 'There was an error processing your request.' );
							return false;
						}
						
						jQuery.waffle.pop( 'Group "' + group.name + '" removed successfully.' );
						ScreenController.getScreens( );
					}
				} );
			}
		} );
		
		// item buttons
		jQuery( '#screenNewItem' ).button( {
			text: false,
			icons: { primary: 'ui-icon-document' }
		} ).click( function( event ) {
			event.preventDefault( );
			var group = jQuery( '#screenItems' ).data( 'screens' )['__root__'].items[jQuery( '#currentScreen' ).val( )];
			ScreenController.editItem( group.id );
		} );
		
		jQuery( '#screenAddItem' ).button( {
			text: false,
			icons: { primary: 'ui-icon-plusthick' }
		} ).click( function( event ) {
			event.preventDefault( );
			jQuery( '#screenAddItemDialog' ).data( 'group', jQuery( '#currentScreen' ).val( ) ).dialog( 'open' );
			ScreenController.getItemList( );
		} );

		jQuery( "#screenAddOptions" ).change(function() {
			var selected_val = jQuery( this ).val();
			var group = jQuery( '#screenItems' ).data( 'screens' )['__root__'].items[jQuery( '#currentScreen' ).val( )];

			switch (selected_val) {
				case "scheduled_special":
					ScreenController.editSpecial( group.id );
					break;
				case "combo":
					ScreenController.editCombo( group.id );
					break;
			}

			jQuery( this).val("");
		});

		var selected_combo = 0;
		jQuery( "#combo_promo_group_select" ).on('change', function (e) {
			var selected_val = jQuery( this ).val();
			if (selected_combo != selected_val && selected_val != "") {
				selected_combo = selected_val;
				//jQuery.waffle.error('Selected Val: ' + selected_val);
				ScreenController.populate_combo_items_dropdown(selected_val);
			} else {
				ScreenController.select_combo_items_dropdown();
			}
		});

		jQuery( '#screenRemoveItem' ).button( {
			text: false,
			icons: { primary: 'ui-icon-closethick' }
		} ).click( function( event ) {
			event.preventDefault( );
			
			var bid = jQuery( '#screenbid' ).val( );
			var screenIndex = jQuery( '#currentScreen' ).val( );
			var screens = jQuery( '#screenItems' ).data( 'screens' );
			var screenId = screens['__root__'].items[screenIndex].id;
			var screenType = screens['__root__'].items[screenIndex].type;
			var selectedItem = jQuery( '#screenItems' ).aerispanel( 'selected' );
			
			if( !( selectedItem ) )
				return;
			
			var item = screens[screenIndex].items[selectedItem];
			
			if( confirm( 'Are you sure you want to remove "' + item.name + '" from this screen?' ) ) {
				jQuery.ajax( {
					type: 'POST',
					url: 'screenRemoveItem',
					dataType: 'json',
					data: { bid: bid, screenId: screenId, screenType: screenType, itemId: item.id },
					success: function( data ) {
						if( !data || !data.success || data.error ) {
							jQuery.waffle.error( data && data.error || 'There was an error processing your request.' );
							return false;
						}
						
						jQuery.waffle.pop( 'Item "' + item.name + '" removed successfully.' );
						ScreenController.getScreens( );
					}
				} );
			}
		} );
		
		// edit buttons
		jQuery( '#generateOpenUPC' ).click( function( ) {
			var context = jQuery( this ).parents( 'form' );
			if( confirm( "This will generate an Open Item UPC, replacing any current UPC.\nOpen Items have no price and will pop up a dialog for price entry on Cashier stations." ) ) {
				jQuery.ajax( {
					url: 'generateUPC?prefix=292929&bid=' + jQuery( 'input[name=businessid]', context ).val( ),
					dataType: 'json',
					success: function( data ) {
						if( !data || typeof( data.upc ) == undefined || data.error ) {
							jQuery.waffle.error( data && data.error || 'There was an error processing your request.' );
							return false;
						}
						
						jQuery( 'input[name="mi.upc"]', context ).val( data.upc );
					}
				} );
			}
		} );
		
		// special interaction
		jQuery( 'select[name="ss.id"]' ).change( function( ) {
			if( jQuery( ':selected', this ).val( ) ) {
				jQuery( 'form:not(.ajaxedit-loading) input[name="mi.name"]' ).val( jQuery( ':selected', this ).text() );
			}
		} );

		jQuery('#icon-color-picker-product, #icon-color-picker-product-combo, #icon-color-picker-display-group,' +
               '#icon-color-picker-product-special').ColorPicker({
			onSubmit: function(hsb, hex, rgb, el) {
				$(el).val(hex);
				$(el).ColorPickerHide();
			},
			onBeforeShow: function () {
				$(this).ColorPickerSetColor(this.value);
			}
		});
	},

	populate_combo_items_dropdown: function( promo_id ) {

		jQuery.ajax( {
			url: '/ta/promotions/getMainComboItems?promo_id=' + promo_id,
			dataType: 'json',
			success: function( data ) {
				if( !data || typeof( data.upc ) == undefined || data.error ) {
					jQuery.waffle.error( data && data.error || 'There was an error processing your request.' );
					return false;
				}

				var combo_items_holder = jQuery( "#combo_item_select_holder");
				combo_items_holder.empty();
				combo_items_holder.append('<span><select name="cc.product_id" id="combo_item_select">');
				combo_items_holder.append('</select></span>');

				var combo_items = jQuery( "#combo_item_select" );
				combo_items.append(jQuery("<option />").val("").text(""));
				jQuery.each(data, function() {
					combo_items.append(jQuery("<option />").val(this.id).text(this.name));
				});
				ScreenController.select_combo_items_dropdown();
				//jQuery( '.ajaxedit' ).ajaxedit( 'styleInputs', [combo_items] );
			}
		} );
	},

	select_combo_items_dropdown: function() {
		var selected_product_id = jQuery('#combo_previous_product_id').val()
		if ( selected_product_id != 0) {
			jQuery( "#combo_item_select" ).val(selected_product_id);
		} else {
			jQuery( "#combo_item_select" ).val("");
		}
	},

	updateGroupButtons: function( index ) {
		ScreenController.updateButtons( 'Group', index );
	},
	
	updateItemButtons: function( index ) {
		ScreenController.updateButtons( 'Item', index );
	},
	
	updateButtons: function( type, index ) {
		jQuery( '#screenEdit' + type + ', #screenRemove' + type ).toggle( index > -1 );
	},
	
	getScreens: function( refresh, async ) {
		var selectedScreen = jQuery( '#currentScreen' ).val( );
		var selectedItem = '#' + jQuery( '#screenItems .selected' ).attr( 'id' );
		refresh = refresh == undefined ? true : refresh;
		async = async == undefined ? true : async;
		
		jQuery.ajax( {
			url: 'getScreens?bid=' + jQuery( '#screenbid' ).val( ),
			dataType: 'json',
			async: async,
			success: function( data ) {
				if( !data || typeof( data.screens ) == undefined || data.error ) {
					jQuery.waffle.error( data && data.error || 'There was an error processing your request.' );
					return false;
				}
				
				ScreenController.setScreens( data );
				
				if( refresh ) {
					ScreenController.showScreen( selectedScreen );
					ScreenController.selectItem( selectedItem );
				}
			}
		} );
	},
	
	setScreens: function( data ) {
		var screenItems = ScreenController.panelItems;
		
		for( var i in data ) {
			screenItems.data( i, data[i] );
		}
		
		if( data['screens']['__root__'] ) {
			var d = data['screens']['__root__'];
			ScreenController.panelGroups.aerispanel( 'show', d );
		}
	},
	
	showScreen: function( screen ) {
		var screenData = ScreenController.panelItems.data( 'screens' );
		if( !( screen in screenData ) ) {
			for( var i in screenData ) {
				if( i == '__root__' )
					continue;
				
				screen = i;
				break;
			}
		}
		
		var data = screenData[screen];
		jQuery( '#currentScreen' ).val( screen );
		
		ScreenController.panelItems.aerispanel( 'show', data );
		
		var groupButtons = ScreenController.panelGroups.find( '.aerispanel-item' );
		groupButtons.removeClass( 'selected' );
		for( var i = 0; i < groupButtons.length; i++ ) {
			if( jQuery( groupButtons[i] ).data( 'index' ) == screen ) {
				jQuery( groupButtons[i] ).addClass( 'selected' );
				break;
			}
		}
		
		/*if( screen == '__root__' ) {
			jQuery( '#screenBrowser .title' ).text( 'Departments' );
			jQuery( '#screenBrowserBack, #screenAddItem, #screenRemoveItem' ).hide( );
			jQuery( '#screenNewGroup, #screenAddGroup' ).show( );
		} else {
			jQuery( '#screenBrowser .title' ).text( screenData['__root__'].items[screen].name );
			jQuery( '#screenBrowserBack, #screenAddItem' ).show( );
			jQuery( '#screenNewGroup, #screenAddGroup, #screenRenameGroup, #screenRemoveGroup' ).hide( );
		}*/
		
		ScreenController.resizeScreen( );
		ScreenController.resetScreenForm( );
	},
	
	refresh: function( ) {
		ScreenController.resizeScreen( true );
	},
	
	resizeScreen: function( forceResize ) {
		if( forceResize === true ) {
			ScreenController.panelGroups.aerispanel( 'resize' );
			ScreenController.panelItems.aerispanel( 'resize' );
		}
		
		var sp = jQuery( '#screenProperties' );
		var spP = sp.outerWidth( true ) - sp.width( );
		sp.width( 1 );
		var spX = sp.offset( ).left - sp.parent( ).offset( ).left;
		var parentW = sp.parent( ).width( );
		var spW = parentW - spX - spP - 1;
		sp.width( spW );
		
		sp.find( '.screenToolbar' ).each( function( ) {
			var b = jQuery( this ).children( ).last( );
			var bP = b.outerWidth( true ) - b.width( );
			b.width( 1 );
			var bX = b.offset( ).left - jQuery( this ).offset( ).left;
			var parentW = jQuery( this ).width( );
			var bW = parentW - bX - bP;
			b.width( bW );
		} );
	},
	
	selectItem: function( target ) {
		if( jQuery( target ).is( '.selected' ) )
			return;
		
		var screenItems = jQuery( '#screenItems' );
		
		screenItems.find( '.selected' )/*.width( ScreenController.gridSize )*/.removeClass( 'selected' );
		
		screenItems.find( '.changed' ).each( function( ) {
			var color = jQuery( this ).data( 'color' );
			jQuery( this ).attr( 'class', 'screenItem product-button-color-' + ( color > 0 ? color : 'default' ) );
		} );
		
		ScreenController.selectedIndex = null;
		
		if( jQuery( target ).length ) {
			jQuery( target ).addClass( 'selected' )/*.width( ScreenController.gridSize - 8 )*/;
			ScreenController.selectedIndex = jQuery( target ).data( 'index' );
			/*ScreenController.showSettings( ScreenController.selectedIndex );
			var isRoot = jQuery( '#currentScreen' ).val( ) == '__root__';
			
			jQuery( '#screenRemoveItem' ).toggle( !isRoot );
			jQuery( '#screenRemoveGroup' ).toggle( isRoot );
			jQuery( '#screenRenameGroup' ).toggle( isRoot );
		} else {
			jQuery( '#screenRemoveItem, #screenRemoveGroup, #screenRenameGroup' ).hide( );*/
		}
	},
	
	editGroup: function( groupId ) {
		var groupData;
        ImageController.resetImageForms( 'display-group' );
		if( groupId == undefined ) {
			groupData = { __isNew: true };
			ImageController.setFormDisabled( 'display-group', true );
		} else {
			groupData = jQuery( '#screenItems' ).data( 'screens' )['__root__'].items[groupId];
			ImageController.setFormID( 'display-group', groupData.id );
			groupData['menu_type'] = 'display-group'
		}
		
		jQuery( '#screenEditContainer' ).children( ).addClass( 'ui-helper-hidden-accessible' );
		jQuery( '#screenEditContainer .editGroup' ).removeClass( 'ui-helper-hidden-accessible' )
			.children( '.ajaxedit' ).ajaxedit( 'edit', groupData );
	},
	
	editItem: function( groupId, itemId ) {
		var itemData;
		ImageController.resetImageForms( 'product' );
		if( itemId == undefined ) {
			itemData = { __isNew: true, 'pgd.promo_group_id': groupId };
			ImageController.setFormDisabled( 'product', true );
		} else {
			var item = jQuery( '#screenItems' ).data( 'screens' )[groupId].items[itemId];
			if( item.is_special ) {
				return ScreenController.editSpecial( groupId, itemId );
			} else if ( item.is_combo ) {
				return ScreenController.editCombo( groupId, itemId);
			}
			var group = jQuery( '#screenItems' ).data( 'screens' )['__root__'].items[groupId];
			itemData = { __get: { id: item.id, groupId: group.id }, __url: 'getMenuItem' };

			ImageController.setFormID( 'product', item.id );
		}

		jQuery( '#screenEditContainer' ).children( ).addClass( 'ui-helper-hidden-accessible' );
		jQuery( '#screenEditContainer .editItem' ).removeClass( 'ui-helper-hidden-accessible' )
			.children( '.ajaxedit' ).ajaxedit( 'edit', itemData );
	},

	editSpecial: function( groupId, specialId ) {
		var itemData;
		ImageController.resetImageForms( 'product-special' );
		if( specialId == undefined ) {
			itemData = { __isNew: true, 'pgd.promo_group_id': groupId };
			ImageController.setFormDisabled( 'product-special', true );
		} else {
			var special = jQuery( '#screenItems' ).data( 'screens' )[groupId].items[specialId];
			var group = jQuery( '#screenItems' ).data( 'screens' )['__root__'].items[groupId];
			itemData = { __get: { id: special.id, groupId: group.id }, __url: 'getMenuSpecial' };

			ImageController.setFormID( 'product-special', special.id );
		}
		
		jQuery( '#screenEditContainer' ).children( ).addClass( 'ui-helper-hidden-accessible' );
		jQuery( '#screenEditContainer .editSpecial' ).removeClass( 'ui-helper-hidden-accessible' )
			.children( '.ajaxedit' ).ajaxedit( 'edit', itemData );
	},

	editCombo: function( groupId, itemId ) {
		var itemData;
		ImageController.resetImageForms( 'product-combo' );
		if( itemId == undefined ) {
			itemData = { __isNew: true, 'pgd.promo_group_id': groupId };
			ImageController.setFormDisabled( 'product-combo', true );
		} else {
			var special = jQuery( '#screenItems' ).data( 'screens' )[groupId].items[itemId];
			var group = jQuery( '#screenItems' ).data( 'screens' )['__root__'].items[groupId];
			itemData = { __get: { id: special.id, groupId: group.id }, __url: 'getMenuCombo' };

			ImageController.setFormID( 'product-combo', special.id );
		}

		jQuery( '#screenEditContainer' ).children( ).addClass( 'ui-helper-hidden-accessible' );
		jQuery( '#screenEditContainer .editCombo' ).removeClass( 'ui-helper-hidden-accessible' )
			.children( '.ajaxedit' ).ajaxedit( 'edit', itemData );
	},
	
	editNone: function( ) {
		jQuery( '#screenEditContainer' ).children( ).addClass( 'ui-helper-hidden-accessible' );
		jQuery( '#screenEditContainer .editNone' ).removeClass( 'ui-helper-hidden-accessible' );
	},
	
	resetScreenForm: function( ) {
		jQuery( '#screenButtonForm input[type=text]' ).val( '' );
		jQuery( '#screenButtonForm option:selected' ).removeAttr( 'selected' );
		jQuery( '#screenButtonForm input[name=button_id]' ).val( 0 );
		jQuery( '#screenButtonForm input[name=button_type]' ).val( 0 );
		jQuery( '#screenButtonForm input[name=button_pos_color]' ).val( 0 ).change( );
		jQuery( '#screenButtonForm input.value' ).val( '' ).removeData( );
		jQuery( '#screenButtonForm .screenOption' ).attr( 'disabled', 'disabled' );
	},

	showSettings: function( index ) {
		var name = 'button';
		var screen = jQuery( '#currentScreen' ).val( );
		var data = jQuery( '#screenItems' ).data( 'screens' );
		var d = data[screen].items;
		
		ScreenController.resetScreenForm( );
		jQuery( '#posscreen .screenOption' ).attr( 'disabled', false );
		
		var t = function( n ) {
			return '#screenButtonForm input[name=' + n + ']';
		};
		var s = function( n ) {
			return '#screenButtonForm select[name=' + n + ']';
		};
		
		jQuery( '#posscreen .settingsHeader' ).removeClass( 'ui-state-highlight' ).addClass( 'ui-state-default' );
		
		if( !d[index] )
			return;
		
		if( d[index].type == undefined )
			d[index].type = 'I';
		
		for( var i in d[index] ) {
			var elem = jQuery( t( name + '_' + i ) );
			if( elem.length ) {
				if( typeof( d[index][i] ) == 'object' )
					elem.data( 'object', d[index][i] );
				else elem.val( d[index][i] );
				if( elem.is( '.colorSelectorValue' ) )
					elem.change( );
			}
			
			elem = jQuery( s( name + '_' + i ) );
			if( elem.length ) {
				elem.find( 'option:selected' ).removeAttr( 'selected' );
				elem.find( 'option[value=' + d[index][i] + ']' ).attr( 'selected', 'selected' );
			}
		}
	},
	
	submitAjaxForm: function( ) {
		if( jQuery( this ).hasClass( 'noAutoSubmit' ) )
			return;
		
		ScreenController.getScreens( false, true );
		
		var refresh = jQuery( this ).hasClass( 'noRefresh' ) ? false : true;
		
		var submitBtn = jQuery( this ).find( 'input[type=submit]' );
		submitBtn.attr( 'disabled', 'disabled' );
		
		var disabledFields = jQuery( this ).find( 'input:disabled' );
		disabledFields.removeAttr( 'disabled' );
		var formData = jQuery( this ).serialize( );
		disabledFields.attr( 'disabled', 'disabled' );
		
		var fn;
		if( (fn = jQuery( this ).data( 'serialize' )) )
			formData += eval( fn + '( this )' );
		
		var callback = jQuery( 'input[name=callback]', this ).val( );
		
		jQuery.ajax( {
			type: "POST",
			url: jQuery( this ).attr( 'action' ),
			dataType: 'json',
			data: formData,
			success: function( r ) {
				if( !r || !( r.id || r.success ) || r.error ) {
					jQuery.waffle.error( r && r.error || 'An error has occurred' );
				} else {
					ScreenController.getScreens( refresh );
					
					if( callback != undefined )
						eval( callback + '();' );
					//alert( r.message );
				}
			}
		} );
		
		setTimeout( "jQuery('#" + submitBtn.attr( 'id' ) + "').attr('disabled', '')", 1000 );
	},
	
	saveScreenButton: function( ) {
		jQuery( '#screenItems .changed' ).removeClass( 'changed' );
	},
	
	updateGroupsOrder: function( ) {
		return ScreenController._updateScreenOrder( '__root__' );
	},
	
	updateItemsOrder: function( ) {
		return ScreenController._updateScreenOrder( jQuery( '#currentScreen' ).val( ) );
	},
	
	_updateScreenOrder: function( screen ) {
		if( ScreenController.updatingOrder ) {
			ScreenController.needUpdateOrder = true;
			return;
		}
		
		ScreenController.updatingOrder = true;
		ScreenController.needUpdateOrder = false;
		
		var data = jQuery( '#screenItems' ).data( 'screens' );
		var screenData = data[screen];
		var items = screenData.items;
		var order = [];
		
		for( var i in items ) {
			var type = screen == '__root__' ? items[i].type : 'I';
			order[order.length] = { id: items[i].id, type: type, y: items[i].display_y, x: items[i].display_x };
		}
		
		var bid = jQuery( '#screenbid' ).val( );
		var screenId, screenType;
		
		if( screen != '__root__' ) {
			screenId = data['__root__'].items[screen].id;
			screenType = data['__root__'].items[screen].type;
		} else {
			screenId = 0;
			screenType = 0;
		}
		
		return jQuery.ajax( {
			type: 'POST',
			url: 'updateScreenOrder',
			dataType: 'json',
			data: { bid: bid, screenId: screenId, screenType: screenType, order: order },
			success: function( adata ) {
				if( !adata || !adata.message || adata.error ) {
					jQuery.waffle.error( adata && adata.error || 'There was an error processing your request' );
					return;
				}
				
				ScreenController.updatingOrder = false;
				
				if( ScreenController.needUpdateOrder ) {
					ScreenController._updateScreenOrder( screen );
				}
			}
		} );
	},
	
	inputTimeout: null,
	inputThis: null,

	delayedInputChange: function( self ) {
		if( ScreenController.inputTimeout )
			clearTimeout( ScreenController.inputTimeout );
		
		var parent = jQuery( self ).closest( '.itemListColumn' );
		var header = jQuery( '.itemListHeader', parent );
		
		jQuery( '.itemListFilterReset', header )
			.button( 'option', 'disabled', jQuery( self ).val( ).length == 0 );
		
		ScreenController.inputThis = this;
		ScreenController.inputTimeout = setTimeout( ScreenController.startInputChange, 150 );
	},

	startInputChange: function( ) {
		jQuery( '.spinner', jQuery( ScreenController.inputThis ).parent( ) ).show( );
		setTimeout( 'ScreenController.inputChange( ScreenController.inputThis );', 50 );
	},

	inputChange: function( self ) {
		var list = jQuery( '#' + jQuery( self ).attr( 'id' ).replace( 'Filter', 'Select' ) );
		var filterString = jQuery( self ).val( );
		
		list.children( ).removeClass( 'ui-filtered' ).show( );
		if( filterString.length > 0 ) {
			var filterTokens = filterString.toUpperCase( ).split( ' ' );
			
			for( var i in filterTokens ) {
				list.find( ':not(:iContains(' + filterTokens[i] + '))' ).addClass( 'ui-filtered' );
			}
			
			list.find( '.ui-filtered' ).hide( );
		}
		
		jQuery( '.spinner', jQuery( self ).parent( ) ).hide( );
	},
	
	sortList: function( list ) {
		function _compareVal( o ) {
			if( jQuery( o ).is( 'optgroup' ) )
				o = jQuery( 'option:first-child', o );
			return jQuery( o ).data( 'order' ) ? jQuery( o ).data( 'order' )
				: jQuery( o ).attr( 'label' ) ? jQuery( o ).attr( 'label' )
				: jQuery( o ).text( );
		}

		function _compare( a, b ) {
			var compA = _compareVal( a ).toString( ).toUpperCase( );
			var compB = _compareVal( b ).toString( ).toUpperCase( );
			var numA, numB;
			var regex = /^\d+(?=\. |$)/;
			if( ( numA = compA.match( regex ) ) && ( numB = compB.match( regex ) ) ) {
				numA = parseInt( numA[0] );
				numB = parseInt( numB[0] );
				return ( numA < numB ) ? -1 : ( numA > numB ) ? 1 : 0;
			} else {
				return ( compA < compB ) ? -1 : ( compA > compB ) ? 1 : 0;
			}
		}

		function _sortList( selector ) {
			var elem = jQuery( selector );
			var listItems = elem.children( ).get( );
			listItems.sort( _compare );
			jQuery.each( listItems, function( idx, itm ) {
				var ji = jQuery( itm );
				ji.addClass( 'sort' + ji.val( ).substr( 0, 1 ).toUpperCase( ) );
				elem.append( itm );
			} );
		}
		
		_sortList( list );
	},
	
	getItemList: function( ) {
		var elemName = 'screen_AddItem';
		var elem = jQuery( '#screen_AddItemSelect' );
		
		if( !elem.is( ':visible' ) ) return;
		
		jQuery( '#' + elemName + 'Spinner' ).show( );
		elem.empty( ).parent( ).addClass( 'far-left' );
		
		var screens = jQuery( '#screenItems' ).data( 'screens' );
		var screen = jQuery( '#currentScreen' ).val( );
		var screenId = screens['__root__'].items[screen].id;
		var screenType = screens['__root__'].items[screen].type;
		
		jQuery.ajax( {
			url: 'screenGetItemList?bid=' + jQuery( '#screenbid' ).val( ) + '&screenId=' + screenId + '&screenType=' + screenType,
			dataType: 'json',
			success: function( data ) {
				if( !data 
					|| ( typeof( data.content ) == 'undefined' && typeof( data.script ) == 'undefined' && typeof( data.data ) == 'undefined' ) 
					|| data.error 
				) {
					jQuery.waffle.error( data && data.error || 'There was an error retrieving data.' );
					return false;
				}
				
				if( typeof( data.data ) != 'undefined' ) {
					elem.data( 'data', data.data );
					var d = data.data;
					
					for( var i in d ) {
						jQuery( '<option id="' + d[i].id + '">' + d[i].name + '</option>' ).appendTo( elem );
					}
					
					ScreenController.sortList( elem );
					
					elem.parent( ).removeClass( 'far-left' );
					jQuery( '#' + elemName + 'Spinner' ).hide( );
				}
				
				ScreenController.setupItemList( '#screen_AddItemSelect' );
			}
		} );
	},
	
	getGroupList: function( ) {
		var elemName = 'screen_AddGroup';
		var elem = jQuery( '#screen_AddGroupSelect' );
		
		if( !elem.is( ':visible' ) ) return;
		
		jQuery( '#' + elemName + 'Spinner' ).show( );
		elem.empty( ).parent( ).addClass( 'far-left' );
		
		jQuery.ajax( {
			url: 'screenGetGroupList?bid=' + jQuery( '#screenbid' ).val( ),
			dataType: 'json',
			success: function( data ) {
				if( !data 
					|| ( typeof( data.content ) == 'undefined' && typeof( data.script ) == 'undefined' && typeof( data.data ) == 'undefined' ) 
					|| data.error 
				) {
					jQuery.waffle.error( data && data.error || 'There was an error retrieving data.' );
					return false;
				}
				
				if( typeof( data.data ) != 'undefined' ) {
					elem.data( 'data', data.data );
					var d = data.data;
					
					for( var i in d ) {
						jQuery( '<option id="' + d[i].id + '">' + d[i].name + '</option>' ).appendTo( elem );
					}
					
					ScreenController.sortList( elem );
					
					elem.parent( ).removeClass( 'far-left' );
					jQuery( '#' + elemName + 'Spinner' ).hide( );
				}
				
				ScreenController.setupItemList( '#screen_AddGroupSelect' );
			}
		} );
	},
	
	setupItemList: function( selector ) {
		var list = jQuery( selector );
		var parent = list.parent( );
		var header = jQuery( '.itemListHeader', parent );
		
		list.width( parent.width( ) ).height( parent.height( ) - header.height( ) )
			.addClass( 'ui-widget-content ui-state-default ui-corner-all' );
		
		jQuery( '.itemListFilterReset', header ).button( {
			disabled: true,
			text: false,
			icons: {
				primary: 'ui-icon-cancel'
			}
		} ).click( function( ) {
			var filter = jQuery( '.itemListFilter', header );
			filter.val( '' );
			ScreenController.inputChange( filter );
			filter.keyup( ).focusout( );
		} ).removeClass( 'ui-corner-all' ).addClass( 'ui-corner-right' );
		
		jQuery( '.itemListFilter', header ).addClass( 'ui-widget-content ui-corner-left' )
			.width( parent.width( ) - jQuery( '.itemListFilterReset', parent ).width( ) - 9 )
			.after( '<img class="spinner" style="display: none;" src="/assets/images/ajax-loader-redmond.gif" />' )
			.hover( function( ) {
				jQuery( this ).addClass( 'ui-state-hover' );
			}, function( ) {
				jQuery( this ).removeClass( 'ui-state-hover' );
			} ).bind( 'keydown, keypress', ScreenController.delayedInputChange ).focusin( function( ) {
				if( jQuery( this ).val( ) == jQuery( this ).data( 'watermark' ) )
					jQuery( this ).removeClass( 'inputWatermark' ).val( '' );
				jQuery( this ).addClass( 'ui-state-focus' );
			} ).focusout( function( ) {
				if( jQuery( this ).val( ) == '' )
					jQuery( this ).addClass( 'inputWatermark' )
						.val( jQuery( this ).data( 'watermark' ) );
				jQuery( this ).removeClass( 'ui-state-focus' );
			} ).data( 'watermark', 'filter items' ).focusout( );
		
		jQuery( '.spinner', header ).show( ).position( {
			my: 'right',
			at: 'right',
			of: header,
			offset: '-22 -3'
		} ).hide( );
		
		list.change( );
		
		parent.removeClass( 'far-left' );
		jQuery( selector + 'Spinner' ).hide( );
	},
	
	__end_ScreenController: function( ){}
};

var ModifierController = {
	/** @memberOf ModifierController */
	isInitialized: false,
	
	gridSize: 0,
	selectedIndex: null,
	
	initializeModifiers: function( ) {
		if( ModifierController.isInitialized )
			return;
		
		ModifierController.isInitialized = true;
		
		jQuery( '#modifierItems.posItems div' ).live( {
			click: function( event, ui ) {
				ModifierController.selectItem( this );
			},
			dblclick: function( event, ui ) {
				if( jQuery( '#currentModScreen' ).val( ) == '__root__' ) {
					ModifierController.showScreen( jQuery( this ).data( 'index' ) );
				} else {
					ModifierController.showEditDialog( );
				}
			}
		} );
		
		jQuery( '#modifierBrowserBack' ).button( {
			text: true,
			icons: { primary: 'ui-icon-circle-triangle-w' }
		} ).click( function( event ) {
			event.preventDefault( );
			ModifierController.showScreen( '__root__' );
		} );
		
		jQuery( '#modifierEditGroupDialog' ).dialog( {
			autoOpen: false,
			hide: 'slide',
			show: 'slide',
			modal: true,
			resizable: false,
			title: 'Edit Group',
			height: 420,
			width: 450,
			buttons: {
				'Save': function( ) {
					var dialog = jQuery( this );
					var data = jQuery( 'form', this ).serialize( );

					jQuery.ajax( {
						type: 'POST',
						url: 'modifierUpdateGroup',
						dataType: 'json',
						data: data,
						success: function( data ) {
							if( !data || !data.success || data.error ) {
								jQuery.waffle.error( data && data.error || 'There was an error processing your request.' );
								return false;
							}
							
							dialog.dialog( 'close' );
							ModifierController.getModifiers( );
						}
					} );
				}
			}
		} );
		
		jQuery( '#modifierEditItemDialog' ).dialog( {
			autoOpen: false,
			hide: 'slide',
			show: 'slide',
			modal: true,
			resizable: false,
			title: 'Edit Item',
			height: 650,
			width: 550,
			buttons: {
				'Close': function( ) {
					var dialog = jQuery( this );
					dialog.dialog( 'close' );
				}
			}
		} );

		jQuery( '#modifierSaveButton' ).button( {
		} ).click( function( event ) {
			event.preventDefault( );
			var dialog = jQuery( '#modifierEditItemDialog' );

			jQuery.ajax( {
				type: 'POST',
				url: 'modifierUpdateItem',
				dataType: 'json',
				data: jQuery( '#modifierEditItemForm' ).serialize( ),
				success: function( data ) {
					if( !data || !data.success || data.error ) {
						jQuery.waffle.error( data && data.error || 'There was an error processing your request.' );
						return false;
					}

					dialog.dialog( 'close' );
					ModifierController.getModifiers( );
				}
			} );
		} );

		jQuery( '#modifierNewGroup' ).button( {
			text: true,
			icons: { primary: 'ui-icon-plusthick' }
		} ).click( function( event ) {
			event.preventDefault( );
			var dialog = jQuery( '#modifierEditGroupDialog' );
			
			jQuery( '#modifierGroup_id', dialog ).val( 0 );
			jQuery( '#modifierGroup_name', dialog ).val( 'New Group' );
			jQuery( '#modifierGroup_type', dialog ).val( 'multiple' );
			jQuery( '#modifierGroup_newLine', dialog ).removeAttr( 'checked' );

			var modifier_form = jQuery( '#modifierEditGroupForm' );
			modifier_form.find('input[name="limit_min"]').val( 0 );
			modifier_form.find('input[name="limit_max"]').val( 0 );
			modifier_form.find('input[name="amount_free"]').val( 0 );

			dialog.dialog( 'open' );
		} );
		
		jQuery( '#modifierEditGroup' ).button( {
			text: true,
			icons: { primary: 'ui-icon-pencil' }
		} ).click( function( event ) {
			event.preventDefault( );
			var dialog = jQuery( '#modifierEditGroupDialog' );
			var data = jQuery( '#modifierItems' ).data( 'modifiers' )['__root__'].items[ModifierController.selectedIndex];
			
			jQuery( '#modifierGroup_id', dialog ).val( data.id );
			jQuery( '#modifierGroup_name', dialog ).val( data.name );
			jQuery( '#modifierGroup_type', dialog ).val( data.type );
			if( data.newLine == 1 )
				jQuery( '#modifierGroup_newLine' ).attr( 'checked', 'checked' );
			else jQuery( '#modifierGroup_newLine' ).removeAttr( 'checked' );

			var modifier_form = jQuery( '#modifierEditGroupForm' );
			modifier_form.find('input[name="limit_min"]').val( data.limit_min );
			modifier_form.find('input[name="limit_max"]').val( data.limit_max );
			modifier_form.find('input[name="amount_free"]').val( data.amount_free );

			dialog.dialog( 'open' );
		} );
		
		jQuery( '#modifierDeleteGroup' ).button( {
			text: true,
			icons: { primary: 'ui-icon-minusthick' }
		} ).click( function( event ) {
			event.preventDefault( );
			
			var bid = jQuery( '#modifierbid' ).val( );
			var item = jQuery( '#modifierItems' ).data( 'modifiers' )['__root__'].items[ModifierController.selectedIndex];
			
			if( confirm( 'Are you sure you want to delete "' + item.name + '" and all associated modifiers?' ) ) {
				jQuery.ajax( {
					type: 'POST',
					url: 'modifierDeleteGroup',
					dataType: 'json',
					data: { bid: bid, groupId: item.id },
					success: function( data ) {
						if( !data || !data.success || data.error ) {
							jQuery.waffle.error( data && data.error || 'There was an error processing your request.' );
							return false;
						}
						
						ModifierController.getModifiers( );
					}
				} );
			}
		} );
		
		jQuery( '#modifierNewItem' ).button( {
			text: true,
			icons: { primary: 'ui-icon-plusthick' }
		} ).click( function( event ) {
			event.preventDefault( );
			var dialog = jQuery( '#modifierEditItemDialog' );
			var modifierData = jQuery( '#modifierItems' ).data( 'modifiers' );

			jQuery( '#modifierItem_groupId', dialog ).val( modifierData['__root__']['items'][jQuery( '#currentModScreen' ).val( )].id );
			jQuery( '#modifierItem_id', dialog ).val( 0 );
			jQuery( '#modifierItem_name', dialog ).val( 'New Modifier' );
			jQuery( '#modifierItem_price', dialog ).val( '0.00' );
			jQuery( '#modifierItem_preset', dialog ).removeAttr( 'checked' );
			jQuery( '#modifier-pos-color', dialog ).val( 0 ).change();

			ImageController.resetImageForms( 'modifier' );
			ImageController.setFormDisabled( 'modifier', true );

			var modifier_form = jQuery( '#modifierEditItemForm' );
			modifier_form.find('input[name="use_product_image"]').prop( 'checked', false );
			modifier_form.find('input[name="hide_text"]').prop( 'checked', false );
			modifier_form.find('input[name="image_display_type"]').val( 0 );
			modifier_form.find('input[name="icon_color"]').val( '' );

			dialog.dialog( 'open' );
		} );
		
		jQuery( '#modifierEditItem' ).button( {
			text: true,
			icons: { primary: 'ui-icon-pencil' }
		} ).click( function( event ) {
			event.preventDefault( );
			ModifierController.showEditDialog( );
		} );

		jQuery( '#modifierDeleteItem' ).button( {
			text: true,
			icons: { primary: 'ui-icon-minusthick' }
		} ).click( function( event ) {
			event.preventDefault( );
			
			var bid = jQuery( '#modifierbid' ).val( );
			var item = jQuery( '#modifierItems' ).data( 'modifiers' )[jQuery( '#currentModScreen' ).val( )].items[ModifierController.selectedIndex];
			
			if( confirm( 'Are you sure you want to delete "' + item.name + '"?' ) ) {
				jQuery.ajax( {
					type: 'POST',
					url: 'modifierDeleteItem',
					dataType: 'json',
					data: { bid: bid, id: item.id },
					success: function( data ) {
						if( !data || !data.success || data.error ) {
							jQuery.waffle.error( data && data.error || 'There was an error processing your request.' );
							return false;
						}
						
						ModifierController.getModifiers( );
					}
				} );
			}
		} );
		
//		jQuery( '#modifierSettings' ).hover( function( ) {
//			if( jQuery( '#currentModScreen' ).val( ) == '__root__' )
//				return;
//
//			clearTimeout( jQuery( this ).data( 'anim' ) );
//
//			jQuery( this ).stop( ).animate( {
//				height: jQuery( this ).css( 'maxHeight' )
//			}, 600 );
//		}, function( ) {
//			if( jQuery( this ).data( 'nohide' ) == true )
//				return;
//
//			var self = this;
//			jQuery( this ).data( 'anim', setTimeout( function( ) {
//				jQuery( self ).stop( ).animate( {
//					height: 0
//				}, 600 );
//			}, 600 ) );
//		} );
		
		jQuery( '#modbutton_pos_color_selector' ).button( {
			text: true,
			icons: { secondary: 'ui-icon-triangle-1-s' }
		} ).click( function( event ) {
			event.preventDefault( );
			event.stopPropagation( );
			
			var bodyHandler;
			var hidePanel = function( nohide ) {
				jQuery( '#modColorSelectorPanel' ).hide( );
				jQuery( 'body' ).unbind( 'click', bodyHandler );
				jQuery( '#modifierSettings' ).data( 'nohide', false );
				if( nohide == true )
					return;
				jQuery( '#modifierSettings' ).triggerHandler( 'mouseleave' );
			};
			
			var bodyHandler = function( bevent ) {
				if( bevent.target ) {
					var t = jQuery( bevent.target );
					if( !( t.is( '#modColorSelectorPanel' ) || t.parent( '#modColorSelectorPanel' ).length ) ) {
						//bevent.preventDefault( );
						hidePanel( t.is( '#modifierSettings' ) || t.parent( '#modifierSettings' ).length );
					}
				}
			};
			
			if( jQuery( '#modColorSelectorPanel' ).is( ':visible' ) ) {
				hidePanel( true );
				return;
			}
			
			jQuery( '#modColorSelectorPanel' ).show( ).position( {
				my: 'right top',
				at: 'right bottom',
				of: '#modbutton_pos_color_selector'
			} );
			
			jQuery( '#modifierSettings' ).data( 'nohide', true );
			
			jQuery( 'body' ).click( bodyHandler );
		} );
		
		jQuery( '#modColorSelectorPanel' ).addClass( 'ui-widget ui-widget-content ui-state-default' );
		
		jQuery( '#modifierSettings .colorSelectorValue' ).each( function( ) {
			var btn = jQuery( '#' + jQuery( this ).attr( 'id' ) + '_selector' );
			var span = btn.find( '.colorSelector' );
			var value = jQuery( this );
			
			value.change( function( ) {
				var color = value.val( ) == 0 ? 'default' : value.val( );
				span.attr( 'class', 'colorSelector product-button-color-' + color );
			} );
		} );
		
		jQuery( '#modColorSelectorPanel>div' ).click( function( ) {
			var value = jQuery( '#modbutton_pos_color' );
			var color = jQuery( this ).attr( 'class' ).replace( /^.*-(\d*)$/, '$1' );
			var oldColor = value.val( );
			value.val( color ).change( );
			
			jQuery( '#modifierItems .selected' ).removeClass( 'product-button-color-' + ( oldColor > 0 ? oldColor : 'default' ) )
				.addClass( 'changed product-button-color-' + ( color > 0 ? color : 'default' ) );
		} );
		
		jQuery( '#posmodifier .screenOption' ).live( {
			change: function( ) {
				jQuery( this ).closest( '.settingsContainer' ).find( '.settingsHeader' )
					.removeClass( 'ui-state-default' ).addClass( 'ui-state-highlight' );
			}
		} );
		
		jQuery( '#posmodifier' ).find( '.settingsForm' ).not( '.noAutoSubmit' ).submit( ModifierController.submitAjaxForm );
		
		ModifierController.getModifiers( );
		
		ModifierController.resetModifierForm( );

		jQuery('#icon-color-picker-modifier').ColorPicker({
			onSubmit: function(hsb, hex, rgb, el) {
				$(el).val(hex);
				$(el).ColorPickerHide();
			},
			onBeforeShow: function () {
				$(this).ColorPickerSetColor(this.value);
			}
		});

		jQuery( '#modifier-pos-color').picker();


		function add_modifier_setting( elem, add ) {
			var parent_div = jQuery( elem ).closest( 'div' )
			var input = parent_div.find( 'input' );
			var num = parseInt( input.val() ) || 0;
			var result = num + add;
			if ( result < 0 ) {
				result = 0;
			}
			input.val( result );
		}

		jQuery( '.modifier_plus' ).button( {
			text: false,
			icons: { primary: 'ui-icon-plus' }
		} ).click( function( event ) {
			event.preventDefault( );
			add_modifier_setting( this, 1 );
		} );

		jQuery( '.modifier_minus' ).button( {
			text: false,
			icons: { primary: 'ui-icon-minus' }
		} ).click( function( event ) {
			event.preventDefault( );
			add_modifier_setting( this, -1 );
		} );

	},
	
	getModifiers: function( refresh ) {
		var selectedScreen = jQuery( '#currentModScreen' ).val( );
		var selectedItem = '#' + jQuery( '#modifierItems .selected' ).attr( 'id' );
		refresh = refresh == undefined ? true : refresh;
		
		jQuery.ajax( {
			url: 'getModifiers?bid=' + jQuery( '#modifierbid' ).val( ),
			dataType: 'json',
			success: function( data ) {
				if( !data || typeof( data.modifiers ) == undefined || data.error ) {
					jQuery.waffle.error( data && data.error || 'There was an error processing your request.' );
					return false;
				}
				
				ModifierController.setModifiers( data );
				
				if( refresh ) {
					ModifierController.showScreen( selectedScreen );
					ModifierController.selectItem( selectedItem );
				}
			}
		} );
	},
	
	setModifiers: function( data ) {
		var modifierItems = jQuery( '#modifierItems' );
		
		for( var i in data ) {
			modifierItems.data( i, data[i] );
		}
	},
	
	findCell: function( y, x, rows, cols ) {
		var target = jQuery( '#modifier-cell-' + y + '-' + x );
		
		while( target.children().size() != 0 ) {
			if( ++x >= cols ) {
				x = 0;
				if( ++y >= rows ) {
					return null;
				}
			}
			
			target = jQuery( '#modifier-cell-' + y + '-' + x );
		}
		
		return target;
	},
	
	showScreen: function( screen ) {
		var modifierItems = jQuery( '#modifierItems' );
		var modifierData = modifierItems.data( 'modifiers' );
		var data = modifierData[screen] || [];
		var items = data.items;
		
		modifierItems.html( '<table id="modifierTable" style="border: solid 1px black; border-collapse: collapse;" />' );
		for( var y = 0; y < data.rows; y++ ) {
			jQuery( '<tr id="modifier-row-' + y + '" />' ).appendTo( jQuery( '#modifierTable' ) );
			for( var x = 0; x < data.cols; x++ ) {
				jQuery( '<td class="screenCell" id="modifier-cell-' + y + '-' + x + '" />' ).data( 'y', y ).data( 'x', x ).appendTo( jQuery( '#modifier-row-' + y, modifierItems ) );
			}
		}
		
		var max = 0;
		
		var addItem = function( item, index ) {
			var target = ModifierController.findCell( 0, 0, data.rows, data.cols );
			if( target == null || target.length == 0 )
				return;
			
			var color = 'product-button-color-' + ( item.pos_color > 0 ? item.pos_color : 'default' );
			var button = jQuery( '<div id="modifierItem' + item.type + item.id + '" class="screenItem ' + color + '">'
				+ item.name + '</div>' ).data( 'index', index ).data( 'color', item.pos_color || 0 ).appendTo( target );

			// If on the modifier screen, make the modifiers drag and drop.
			if ( screen != '__root__') {

				var draggable_item = jQuery( '#modifierItem' + item.type + item.id );

				// Make the modifier Draggable
				draggable_item.draggable({
					opacity: 0.6,
					revert: true,
					snap: true,
					zIndex: 100
				});

				// Make the Modifier Droppable. If a modifier get dropped on another
				// update the sort_key.
				draggable_item.droppable({
					hoverClass: "ui-state-hover",
					drop: function( event, ui ) {
						var current_screen_id = jQuery( '#currentModScreen' ).val( );
						var drop_index = jQuery( this ).data( 'index' );
						var drop_data = modifierData[current_screen_id].items[drop_index];

						var drag_index = jQuery( ui.draggable ).data( 'index' );
						var drag_data = modifierData[current_screen_id].items[drag_index];

						jQuery( this ).replaceWith( ui.draggable );

						ModifierController.moveModifier( drag_data.id, drop_data.id );
					}
				});
			}

			if( item.preset == 1 || item.use_product_image == 1 ) {

				var span = '';
				if ( item.preset == 1 )
					span += '<span class="ui-icon ui-icon-check" />';
				if ( item.use_product_image == 1 )
					span += '<span class="ui-icon ui-icon-image" />';

				jQuery('<div class="buttonDecoration left product-button-color-8">' + span + '</div>').appendTo(button);
			}

			if( item.price )
				jQuery( '<div class="buttonDecoration right">' + Number( item.price ).toFixed( 2 ) + '</div>' ).appendTo( button );
			
			max = i > max ? i : max;
		};
		
		for( var i in items ) {
			addItem( items[i], i );
		}
		
		modifierItems.data( 'max', max );

		if( jQuery( '#currentModScreen' ).val( ) != screen )
			ModifierController.selectedIndex = 0;
		
		jQuery( '#currentModScreen' ).val( screen );
		
		var isRoot = screen == '__root__';
		jQuery( '#modifierBar .title' ).text( isRoot ? 'Modifier Groups' : modifierData['__root__'].items[screen].name );
		jQuery( '#modifierBrowserBack, #modifierNewItem' ).toggle( !isRoot );
		jQuery( '#modifierNewGroup' ).toggle( isRoot );
		jQuery( '#modifierEditGroup, #modifierDeleteGroup' ).toggle( isRoot && ModifierController.selectedIndex > 0 );
		jQuery( '#modifierEditItem, #modifierDeleteItem' ).toggle( !isRoot && ModifierController.selectedIndex > 0 );
		
		if( isRoot ) {
			jQuery( '#modifierSettings>div' ).addClass( 'ui-state-disabled' );
		} else {
			jQuery( '#modifierSettings>div' ).removeClass( 'ui-state-disabled' );
		}
		
		ModifierController.resizeScreen( );
		ModifierController.resetModifierForm( );
	},
	
	resizeScreen: function( ) {
		var screen = jQuery( '#currentModScreen' ).val( );
		var modifierItems = jQuery( '#modifierItems' );
		var modifierItemContainers = jQuery( '.screenCell', modifierItems );
		var testItem = jQuery( 'div', modifierItems ).first( );
		
		var scrollWidth = scrollbarWidth( );
		var paddingWidth = testItem.outerWidth( true ) - testItem.innerWidth( );
		var screenWidth = modifierItems.width( );
		var screenCols = modifierItems.data( 'modifiers' )[screen].cols;
		var width = Math.floor( ( screenWidth - scrollWidth - 1 ) / screenCols - paddingWidth );
		modifierItemContainers.width( width );
		ModifierController.gridSize = width;
		
		var paddingHeight = testItem.outerHeight( true ) - testItem.innerHeight( );
		
		modifierItems.height( modifierItems.data( 'modifiers' )[screen].cols * ( jQuery( 'div', modifierItems ).height( ) + paddingHeight ) );
		
		jQuery( '#modifierSettings' ).position( {
			my: 'right top',
			at: 'right top',
			of: '#modifierSettingsPlaceholder',
			collision: 'none'
		} );
	},
	
	selectItem: function( target ) {
		if( jQuery( target ).is( '.selected' ) )
			return;
		
		var modifierItems = jQuery( '#modifierItems' );
		
		modifierItems.find( '.selected' )/*.width( ScreenController.gridSize )*/.removeClass( 'selected' );
		
		modifierItems.find( '.changed' ).each( function( ) {
			var color = jQuery( this ).data( 'color' );
			jQuery( this ).attr( 'class', 'screenItem product-button-color-' + ( color > 0 ? color : 'default' ) );
		} );
		
		ModifierController.selectedIndex = null;
		
		if( jQuery( target ).length ) {
			jQuery( target ).addClass( 'selected' );
			ModifierController.selectedIndex = jQuery( target ).data( 'index' );
			ModifierController.showSettings( ModifierController.selectedIndex );
			
			var isRoot = jQuery( '#currentModScreen' ).val( ) == '__root__';
			jQuery( '#modifierDeleteItem' ).toggle( !isRoot );
			jQuery( '#modifierDeleteGroup' ).toggle( isRoot );
			jQuery( '#modifierEditGroup' ).toggle( isRoot );
			jQuery( '#modifierEditItem' ).toggle( !isRoot );
		} else {
			jQuery( '#modifierDeleteItem, #modifierDeleteGroup, #modifierEditItem, #modifierEditGroup' ).hide( );
		}
	},
	
	resetModifierForm: function( ) {
		jQuery( '#modifierButtonForm input[type=text]' ).val( '' );
		jQuery( '#modifierButtonForm option:selected' ).removeAttr( 'selected' );
		jQuery( '#modifierButtonForm input[name=modbutton_id]' ).val( 0 );
		jQuery( '#modifierButtonForm input[name=modbutton_type]' ).val( 0 );
		jQuery( '#modifierButtonForm input[name=modbutton_pos_color]' ).val( 0 ).change( );
		jQuery( '#modifierButtonForm input.value' ).val( '' ).removeData( );
		jQuery( '#modifierButtonForm .screenOption' ).attr( 'disabled', 'disabled' );
	},
	
	showSettings: function( index ) {
		var name = 'modbutton';
		var screen = jQuery( '#currentModScreen' ).val( );
		var data = jQuery( '#modifierItems' ).data( 'modifiers' );
		var d = data[screen].items;
		
		ModifierController.resetModifierForm( );
		jQuery( '#posmodifier .screenOption' ).attr( 'disabled', false );
		
		var t = function( n ) {
			return '#modifierButtonForm input[name=' + n + ']';
		};
		var s = function( n ) {
			return '#modifierButtonForm select[name=' + n + ']';
		};
		
		jQuery( '#posmodifier .settingsHeader' ).removeClass( 'ui-state-highlight' ).addClass( 'ui-state-default' );
		
		if( !d[index] )
			return;
		
		for( var i in d[index] ) {
			var elem = jQuery( t( name + '_' + i ) );
			if( elem.length ) {
				if( typeof( d[index][i] ) == 'object' )
					elem.data( 'object', d[index][i] );
				else elem.val( d[index][i] );
				if( elem.is( '.colorSelectorValue' ) )
					elem.change( );
			}
			
			elem = jQuery( s( name + '_' + i ) );
			if( elem.length ) {
				elem.find( 'option:selected' ).removeAttr( 'selected' );
				elem.find( 'option[value=' + d[index][i] + ']' ).attr( 'selected', 'selected' );
			}
		}
	},
	
	submitAjaxForm: function( ) {
		if( jQuery( this ).hasClass( 'noAutoSubmit' ) )
			return;
		
		var refresh = jQuery( this ).hasClass( 'noRefresh' ) ? false : true;
		
		var submitBtn = jQuery( this ).find( 'input[type=submit]' );
		submitBtn.attr( 'disabled', 'disabled' );
		
		var disabledFields = jQuery( this ).find( 'input:disabled' );
		disabledFields.removeAttr( 'disabled' );
		var formData = jQuery( this ).serialize( );
		disabledFields.attr( 'disabled', 'disabled' );
		
		var fn;
		if( (fn = jQuery( this ).data( 'serialize' )) )
			formData += eval( fn + '( this )' );
		
		var callback = jQuery( 'input[name=callback]', this ).val( );
		
		jQuery.ajax( {
			type: "POST",
			url: jQuery( this ).attr( 'action' ),
			dataType: 'json',
			data: formData,
			success: function( r ) {
				if( !r || !( r.id || r.success ) || r.error ) {
					jQuery.waffle.error( r && r.error || 'An error has occurred' );
				} else {
					ModifierController.getModifiers( refresh );
					
					if( callback != undefined )
						eval( callback + '();' );
					//alert( r.message );
				}
			}
		} );
		
		setTimeout( "jQuery('#" + submitBtn.attr( 'id' ) + "').attr('disabled', '')", 1000 );
	},
	
	saveModifierButton: function( ) {
		jQuery( '#modifierItems .changed' ).removeClass( 'changed' );
	},
	
	sortList: function( list ) {
		function _compareVal( o ) {
			if( jQuery( o ).is( 'optgroup' ) )
				o = jQuery( 'option:first-child', o );
			return jQuery( o ).data( 'order' ) ? jQuery( o ).data( 'order' )
				: jQuery( o ).attr( 'label' ) ? jQuery( o ).attr( 'label' )
				: jQuery( o ).text( );
		}

		function _compare( a, b ) {
			var compA = _compareVal( a ).toString( ).toUpperCase( );
			var compB = _compareVal( b ).toString( ).toUpperCase( );
			var numA, numB;
			var regex = /^\d+(?=\. |$)/;
			if( ( numA = compA.match( regex ) ) && ( numB = compB.match( regex ) ) ) {
				numA = parseInt( numA[0] );
				numB = parseInt( numB[0] );
				return ( numA < numB ) ? -1 : ( numA > numB ) ? 1 : 0;
			} else {
				return ( compA < compB ) ? -1 : ( compA > compB ) ? 1 : 0;
			}
		}

		function _sortList( selector ) {
			var elem = jQuery( selector );
			var listItems = elem.children( ).get( );
			listItems.sort( _compare );
			jQuery.each( listItems, function( idx, itm ) {
				var ji = jQuery( itm );
				ji.addClass( 'sort' + ji.val( ).substr( 0, 1 ).toUpperCase( ) );
				elem.append( itm );
			} );
		}
		
		_sortList( list );
	},

	moveModifier: function( modifier_to_move_id, existing_modifier_id ) {

		var sort_mods_waffle = jQuery.waffle.pop({
			autoClose: false,
			clickClose: false,
			spinner: true,
			content: 'Please Wait...',
			weight: -10
		});

		jQuery.ajax( {
			url: 'moveModifierButton?mod_to_move_id=' + modifier_to_move_id + '&existing_mod_id='
				 + existing_modifier_id,
			dataType: 'json',
			async: true,
			success: function( r ) {
				if( !r || !( r.id || r.success ) || r.error ) {
					jQuery.waffle.error( r && r.error || 'An error has occurred' );
				}
				ModifierController.getModifiers( true );
			},
			complete: function(jqXHR, textStatus) {
				jQuery.waffle.burn( sort_mods_waffle );
			}
		} );

	},

	showEditDialog: function( ) {
		var dialog = jQuery( '#modifierEditItemDialog' );
		var modifierData = jQuery( '#modifierItems' ).data( 'modifiers' );
		var data = modifierData[jQuery( '#currentModScreen' ).val( )].items[ModifierController.selectedIndex];

		jQuery( '#modifierItem_groupId', dialog ).val( modifierData['__root__']['items'][jQuery( '#currentModScreen' ).val( )].id );
		jQuery( '#modifierItem_id', dialog ).val( data.id );
		jQuery( '#modifierItem_name', dialog ).val( data.name );
		jQuery( '#modifierItem_price', dialog ).val( data.price );
		if( data.preset == 1 )
			jQuery( '#modifierItem_preset' ).attr( 'checked', 'checked' );
		else jQuery( '#modifierItem_preset' ).removeAttr( 'checked' );
		jQuery( '#modifier-pos-color', dialog ).val( data.pos_color ).change();

		ImageController.setFormDisabled( 'modifier', false );
		ImageController.setFormID( 'modifier', data.id );
		ImageController.getImages( 'modifier' );

		var modifier_form = jQuery( '#modifierEditItemForm' );
		modifier_form.find('input[name="use_product_image"]').prop( 'checked', data.use_product_image == 1 );
		modifier_form.find('input[name="hide_text"]').prop( 'checked', data.hide_text == 1 );
		modifier_form.find('input[name="image_display_type"]').val( data.image_display_type );
		modifier_form.find('input[name="icon_color"]').val( data.icon_color );

		dialog.dialog( 'open' );
	},
	
	__end_ModifierController: function( ){}
};

var FPAssignController = {
	initialize: function() {
		"use strict";

		FPAssignController.bid = jQuery( '#fpbid' ).val( );

		FPAssignController.content = jQuery( '#fpassign' );

		FPAssignController.listAssigned = jQuery( '#fpanelAssigned' );
		FPAssignController.listPanels = jQuery( '#fpanelList' );

		FPAssignController.btnMoveUp = jQuery( '#fpanelMoveUp' );
		FPAssignController.btnMoveDown = jQuery( '#fpanelMoveDown' );
		FPAssignController.btnAddPage = jQuery( '#fpanelAddPage' );
		FPAssignController.btnRemovePage = jQuery( '#fpanelRemovePage' );

		FPAssignController.initializeButtons();
		FPAssignController.initializeLists();
	},

	initializeButtons: function() {
		"use strict";

		var disable = FPAssignController._disable;

		disable( FPAssignController.btnMoveUp.button( {
			icons: {
				primary: 'ui-icon-arrowthick-1-n'
			}
		} ).on( 'click', FPAssignController.moveUp ) );

		disable( FPAssignController.btnMoveDown.button( {
			icons: {
				primary: 'ui-icon-arrowthick-1-s'
			}
		} ).on( 'click', FPAssignController.moveDown ) );

		disable( FPAssignController.btnAddPage.button( {
			icons: {
				primary: 'ui-icon-arrowthick-1-w'
			}
		} ).on( 'click', FPAssignController.addPage ) );

		disable( FPAssignController.btnRemovePage.button( {
			icons: {
				primary: 'ui-icon-arrowthick-1-e'
			}
		} ).on( 'click', FPAssignController.removePage ) );
	},

	_enable: function( e ) {
		e.prop( 'disabled', false ).removeClass( 'ui-state-disabled' );
	},

	_disable: function( e ) {
		e.prop( 'disabled', true ).addClass( 'ui-state-disabled' );
	},

	initializeLists: function() {
		"use strict";

		var enable = FPAssignController._enable;
		var disable = FPAssignController._disable;

		FPAssignController.listPanels.on( 'change', function( ) {
			if( jQuery( this ).val( ) ) {
				enable( FPAssignController.btnAddPage );
				FPAssignController.listAssigned.find( ':selected' ).prop( 'selected', false ).end().change( );
			} else {
				disable( FPAssignController.btnAddPage );
			}
		} );

		FPAssignController.listAssigned.on( 'change', function( ) {
			var $this = jQuery( this );
			var btns = FPAssignController.btnRemovePage.add( FPAssignController.btnMoveDown ).add( FPAssignController.btnMoveUp );
			if( $this.val( ) ) {
				enable( FPAssignController.btnRemovePage );

				if( $this.find( ':selected:not(:first-child)' ).size( ) ) {
					enable( FPAssignController.btnMoveUp );
				} else {
					disable( FPAssignController.btnMoveUp );
				}

				if( $this.find( ':selected:not(:last-child)' ).size( ) ) {
					enable( FPAssignController.btnMoveDown );
				} else {
					disable( FPAssignController.btnMoveDown );
				}

				FPAssignController.listPanels.find( ':selected' ).prop( 'selected', false ).end().change( );
			} else {
				disable( btns );
			}
		} );
	},

	refresh: function() {
		"use strict";
		jQuery( function( ) {
			FPAssignController._refresh();
		} );
	},

	_refresh: function() {
		"use strict";

		jQuery.ajax( {
			type: 'GET',
			url: 'getFPAssignment?bid=' + FPAssignController.bid,
			dataType: 'json',
			success: function( data ) {
				if( !data || !( data.assigned && data.available ) || data.error ) {
					jQuery.waffle.error( data && data.error || 'There was an error processing your request.' );
					return;
				}

				FPAssignController.updateAssignment( data.assigned, data.available );
			}
		} );
	},

	updateAssignment: function( assigned, available ) {
		"use strict";
		var update = function( el, items ) {
			var addList = function( i, a ) {
				var name = a.name + ' (' + a.cols + 'x' + a.rows + ')';
				list += '<option value="' + a.id + '">' + name + '</option>';
			};

			var list = '';
			var selected = el.find( ':selected' ).val( );
			jQuery.each( items, addList );
			el.empty().html( list );
			if( selected !== undefined ) {
				el.find( 'option[value="' + selected + '"]' ).prop( 'selected', true );
			}
			el.change( );
		};

		update( FPAssignController.listPanels, available );
		update( FPAssignController.listAssigned, assigned );
	},

	moveUp: function( ) {
		"use strict";
		var selected = FPAssignController.listAssigned.find( ':selected:not(:first-child)' );

		if( selected.size( ) ) {
			FPAssignController._movePage( selected.val( ), -1 );
		}
	},

	moveDown: function( ) {
		"use strict";
		var selected = FPAssignController.listAssigned.find( ':selected:not(:last-child)' );

		if( selected.size( ) ) {
			FPAssignController._movePage( selected.val( ), 1 );
		}
	},

	_movePage: function( p, d ) {
		"use strict";

		var i = 1;
		var pages = [];
		var val, off;
		FPAssignController.listAssigned.children( 'option' ).each( function( ) {
			val = jQuery( this ).val( );
			off = val == p ? d * 3 : 0;
			pages[i + off] = val;
			i += 2;
		} );

		jQuery.ajax( {
			type: 'POST',
			url: 'updateFPOrder',
			dataType: 'json',
			data: { bid: FPAssignController.bid, pages: pages },
			success: function( data ) {
				if( !data || !data.success || data.error ) {
					jQuery.waffle.error( data && data.error || 'There was an error processing your request.' );
					return;
				}

				FPAssignController.refresh( );
			},
		} );
	},

	addPage: function( ) {
		"use strict";
		var selected = FPAssignController.listPanels.find( ':selected' );

		if( selected ) {
			FPAssignController._addPage( selected.val( ) );
		}
	},

	_addPage: function( p ) {
		"use strict";
		jQuery.ajax( {
			type: 'POST',
			url: 'addFPPage',
			dataType: 'json',
			data: { 'bid': FPAssignController.bid, 'fpid': p },
			success: function( data ) {
				if( !data || !data.success || data.error ) {
					jQuery.waffle.error( data && data.error || 'There was an error processing your request.' );
					return;
				}

				FPAssignController.refresh( );
			},
		} );
	},

	removePage: function( ) {
		"use strict";
		var selected = FPAssignController.listAssigned.find( ':selected' );

		if( selected ) {
			FPAssignController._removePage( selected.val( ) );
		}
	},

	_removePage: function( p ) {
		"use strict";
		jQuery.ajax( {
			type: 'POST',
			url: 'removeFPPage',
			dataType: 'json',
			data: { 'bid': FPAssignController.bid, 'spid': p },
			success: function( data ) {
				if( !data || !data.success || data.error ) {
					jQuery.waffle.error( data && data.error || 'There was an error processing your request.' );
					return;
				}

				FPAssignController.refresh( );
			},
		} );
	},
};
