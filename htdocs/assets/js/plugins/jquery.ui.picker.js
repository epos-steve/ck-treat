/**
 * @class jQuery.ui.picker
 */
jQuery.widget( 'ui.picker', {
	/** @memberOf jQuery.ui.picker */
	name: 'jQuery.ui.picker',
	
	options: {
		type: 'class',
		width: 4,
		height: null
	},
	
	id: null,
	
	container: null,
	button: null,
	label: null,
	dropdown: null,
	dropdownWidth: null,
	dropdownHeight: null,
	
	_create: function( ) {
		var self = this,
			o = self.options,
			el = self.element;
		
		if( el.hasClass( 'picker-field' ) )
			return;
		
		var domel = el.get( 0 );
		var cssNames = domel.className != '' ? domel.className.split( /\s+/ ) : [];
		for( var i = 0, css = cssNames[i]; i < cssNames.length; i++, css = cssNames[i] ) {
			var m = css.match( /^picker-prefix-(.*)$/ );
			if( m ) {
				o.prefix = m[1];
			}
		}
		
		el.addClass( 'picker-field ui-helper-hidden' ).wrap( '<div class="ui-picker ui-state-default ui-corner-all" />' );
		this.button = el.parent( '.ui-picker' );
		this.button.wrap( '<div class="ui-picker-container" />' );
		this.container = this.button.parent( '.ui-picker-container' );
		
		this.label = jQuery( '<span class="ui-picker-label ui-picker-value ui-widget-content" />' ).appendTo( this.button );
		jQuery( '<span class="ui-picker-arrow ui-icon ui-icon-triangle-1-s" />' ).appendTo( this.button );
		
		this.id = Math.floor( Math.random( ) * 100000 );
		
		this._buildDropdown( );
		
		this._bindEvents( );
		
		this.element.change( );
	},
	
	_bindEvents: function( ) {
		var self = this,
			o = self.options;
		
		this.button.hover( function( ) {
			self.button.addClass( 'ui-state-hover' );
		}, function( ) {
			self.button.removeClass( 'ui-state-hover' );
		} );
		
		this.button.click( function( event ) {
			if( !self.dropdown.is( ':visible' ) )
				self.open( );
			event.preventDefault( );
		} );
		
		this.dropdown.on( 'click', function( event ) {
			event.preventDefault( );
			event.stopPropagation( );
			return false;
		} );
		
		this.dropdown.on( {
			mouseover: function( ) {
				jQuery( this ).addClass( 'ui-state-hover' );
			},
			mouseout: function( ) {
				jQuery( this ).removeClass( 'ui-state-hover' );
			},
			click: function( ) {
				self.element.val( jQuery( this ).data( 'value' ) ).change( );
				self.close( );
			}
		}, '.ui-picker-value' );
		
		this.element.on( 'change.picker', function( ) {
			self._renderValue( self.label, jQuery( this ).val( ) );
		} );
	},
	
	_buildDropdown: function( ) {
		var self = this,
			o = self.options;
		
		if( this.dropdown ) {
			this.dropdown.remove( );
			this.dropdown = null;
		}
		
		var children = this.element.children( );
		
		// determine size
		this.dropdownWidth = o.width;
		this.dropdownHeight = o.height;
		
		if( o.width == null && o.height == null )
			this.dropdownWidth = 4;
		
		if( this.dropdownWidth == null )
			this.dropdownWidth = Math.ceil( children.length / this.dropdownHeight );
		if( this.dropdownHeight == null )
			this.dropdownHeight = Math.ceil( children.length / this.dropdownWidth );
		
		// build the element
		this.dropdown = jQuery( '<div class="ui-picker-dropdown ui-widget-content ui-corner-all" />' ).appendTo( this.container );
		for( var i = 0; i < this.dropdownHeight; i++ ) {
			jQuery( '<div class="ui-picker-dropdown-row" id="picker-' + this.id + '-row-' + i + '" />' ).appendTo( this.dropdown );
		}
		
		var i = 0;
		children.each( function( ) {
			var row = jQuery( '#picker-' + self.id + '-row-' + Math.floor( i / self.dropdownWidth ) );
			var value = jQuery( this ).val( );
			var el = jQuery( '<div class="ui-picker-value ui-widget-content" />' ).appendTo( row ).data( 'value', value );
			self._renderValue( el, value );
			i++;
		} );
		
		this.dropdown/*.position( {
			my: 'left top',
			at: 'left bottom',
			of: this.button
		} )*/.hide( );
	},
	
	_renderValue: function( target, value ) {
		var self = this,
			o = self.options;
		
		switch( o.type ) {
			case 'class':
				target.empty( );
				var className = ( o.prefix || '' ) + value;
				jQuery( '<div class="' + className + '" />' ).css( {
					width: '48px',
					height: '48px',
					position: 'static',
					float: 'none',
					display: 'block',
					marginTop: '-12px',
					marginLeft: '-12px'
				} ).appendTo( target );
				break;
		}
	},
	
	toggle: function( show ) {
		if( show == undefined ) {
			show = !this.dropdown.is( ':visible' );
		}
		
		if( show ) {
			var self = this;
			// delay 1ms so the current event doesn't get caught when it bubbles up
			setTimeout( function( ) {
				jQuery( 'body' ).on( 'click.picker', function( event ) {
					if( event.target == self.button || event.target == self.dropdown )
						return;
					
					self.close( );
				} );
			}, 1 );
		} else {
			jQuery( 'body' ).off( 'click.picker' );
		}
		
		this.dropdown.stop( ).slideToggle( show );
	},
	
	open: function( ) {
		this.toggle( true );
	},
	
	close: function( ) {
		this.toggle( false );
	},
	
	destroy: function( ) {
		
	}
} );
