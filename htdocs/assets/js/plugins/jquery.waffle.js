( function( ) {
	
	if( typeof Array.prototype.indexOf != 'function' ) {
		Array.prototype.indexOf = function( v ) {
			for( var i = 0; i < this.length; i++ ) {
				if( this[i] == v )
					return i;
			}
			
			return -1;
		};
	}
	
	if( typeof String.prototype.trim != 'function' ) {
		String.prototype.trim = function( ) {
			return this.replace( /^\s*|\s*$/, '' );
		};
	}
	
	/** 
	 * @class Waffle
	 * @private
	 * @constructor
	 * 
	 * @param {jQuery.waffle} parent
	 * @param {Object} options
	 * 
	 * @property {jQuery.waffle} parent
	 * @property {Object} options
	 * @property {Number} id
	 * @property {Number} timeout
	 * @property {DOMElement} element
	 */
	var Waffle = function( parent, options ) {
		this.parent = parent;
		
		if( typeof options == 'string' ) {
			options = { content: options };
		}
		
		this.options = jQuery.extend( {}, Waffle.defaults, options );
		
		this.id = Waffle.newId++;
		this.timeout = null;
		this.element = null;
		
		this.parent.waffles.push( this );
		
		this.create( );
		this.pop( );
		
		return this;
	};
	
	/**
	 * @static
	 * @type Number
	 */
	Waffle.newId = 1;
	
	/**
	 * @static
	 * @type Object
	 */
	Waffle.defaults = {
		autoClose: true,
		clickClose: true,
		delay: 3500,
		showCloseButton: null,
		content: '',
		icon: null,
		spinner: false,
		error: false,
		popEffect: 'fade',
		popDuration: 200,
		burnEffect: 'fade',
		burnDuration: 600,
		maxWidth: 300,
		maxHeight: 64,
		weight: 0
	};
	
	Waffle.prototype.create = function( ) {
		this.element = jQuery( '<div class="waffle ui-widget ui-state-highlight ui-corner-all" />' ).html( this.options.content ).css( 'marginBottom', this.parent.options.padding );
		if( this.options.error )
			this.element.addClass( 'ui-state-error' );
		if( this.options.spinner )
			jQuery( '<img src="' + jQuery.waffle.spinnerImage + '" />' ).prependTo( this.element );
		if( this.options.icon )
			jQuery( '<span class="ui-icon ' + this.options.icon + ' icon" />' ).prependTo( this.element.addClass( 'icon' ) );
		if( this.options.clickClose && ( this.options.showCloseButton || ( this.options.showCloseButton === null && !this.options.autoClose ) ) )
			jQuery( '<span class="ui-icon ui-icon-closethick closebutton" />' ).appendTo( this.element.addClass( 'closebutton' ).on( {
				mouseenter: function( ) {
					jQuery( this ).find( '.closebutton' ).removeClass( 'ui-icon-closethick' ).addClass( 'ui-icon-circle-close' );
				},
				mouseleave: function( ) {
					jQuery( this ).find( '.closebutton' ).removeClass( 'ui-icon-circle-close' ).addClass( 'ui-icon-closethick' );
				}
			} ) );
	};
	
	/**
	 * @returns jQuery.Promise
	 */
	Waffle.prototype.pop = function( ) {
		var self = this;
		
		this.element.appendTo( this.parent.waffleIron );
		this.parent.sortWaffles( );
		
		var promise = this.element.fadeIn( this.options.popDuration );
		
		var burnFn = function( ) {
			self.burn( );
		};
		
		if( this.options.autoClose ) {
			jQuery.when( promise ).done( function( ) {
				self.timeout = window.setTimeout( burnFn, self.options.delay );
			} );
		}
		
		if( this.options.clickClose ) {
			this.element.css( 'cursor', 'pointer' );
			this.element.on( 'click', burnFn );
		}
		
		return promise;
	};
	
	/**
	 * @returns jQuery.Promise
	 */
	Waffle.prototype.burn = function( ) {
		var self = this;
		
		if( this.timeout != null ) {
			clearTimeout( this.timeout );
			this.timeout = null;
		}
		
		//var promise = this.element.fadeOut( this.options.burnDuration ).slideUp( );
		var promise = this.element.animate( {
			opacity: 0
		}, this.options.burnDuration ).animate( {
			height: 0
		}, 200 );
		jQuery.when( promise ).done( function( ) {
			self.element.remove( );
			var index = self.parent.findWaffleIndex( self );
			if( index != -1 ) {
				delete self.parent.waffles.splice( index, 1 );
			}
		} );
		return promise;
	};
	
	/**
	 * @returns {String}
	 */
	Waffle.prototype.toString = function( ) {
		return this.options.content + ' (' + this.options.weight + ')';
	};
	
	/** @class jQuery.waffle */
	jQuery.waffle = {
		/** @memberOf jQuery.waffle */
		name: 'jQuery.waffle',
		
		/**
		 * @private
		 * @type Object
		 */
		defaults: {
			position: 'top right',
			offset: '10 10',
			padding: 4
		},
		
		/**
		 * @public
		 * @type String
		 */
		spinnerImage: '/assets/images/ajax-loader-redmond.gif',
		
		/**
		 * @protected
		 * @type Object
		 */
		options: null,
		
		/**
		 * @protected
		 * @type DOMElement
		 */
		waffleIron: null,
		
		/**
		 * @private
		 * @type Number
		 */
		posX: 1,
		
		/**
		 * @private
		 * @type Number
		 */
		posY: 1,
		
		/**
		 * @private
		 * @type Number
		 */
		offX: 0,
		
		/**
		 * @private
		 * @type Number
		 */
		offY: 0,
		
		/**
		 * @protected
		 * @type Waffle[]
		 */
		waffles: [],
		
		/**
		 * @private
		 * @type Boolean
		 */
		initialized: false,
		
		/**
		 * Initialize the waffle engine
		 */
		init: function( ) {
			if( this.initialized )
				return;
			
			this.initialized = true;
			this.setup( );
			
			var self = this;
			jQuery( window ).resize( function( ) {
				self._positionWaffleIron( );
			} );
		},
		
		/**
		 * Set up options for the waffle engine
		 * 
		 * Available Options:
		 *     position - 'top right' - similar to jQuery.position syntax, should be like 'top right', 'bottom left', etc.
		 *     offset - '10 10' - offset from edge of screen, can be a single value or separate vertical/horizontal offsets - i.e. '10' or '5 20'
		 *     padding - 4 - pixels of padding between waffles
		 * 
		 * @param {object} o
		 */
		setup: function( o ) {
			var self = this;
			
			if( o === true ) {
				o = {};
				this.options = null;
			}
			
			if( this.options == null )
				this.options = this.defaults;
			
			this.options = jQuery.extend( this.options, o || {} );
			
			jQuery( document ).ready( function( ) { self._createWaffleIron( ); } );
		},
		
		/**
		 * @private
		 */
		_createWaffleIron: function( ) {
			if( this.waffleIron == null ) {
				this.waffleIron = jQuery( '<div class="waffleIron" />' ).appendTo( 'body' );
			}
			
			// 0 - left/top, 1 - center, 2 - right/bottom
			this.posX = 1;
			this.posY = 1;
			
			var posToken = this.options.position.split( ' ' );
			
			if( posToken.indexOf( 'top' ) > -1 )
				this.posY--;
			if( posToken.indexOf( 'bottom' ) > -1 )
				this.posY++;
			if( posToken.indexOf( 'left' ) > -1 )
				this.posX--;
			if( posToken.indexOf( 'right' ) > -1 )
				this.posX++;
			
			this.offX = this.offY = null;
			
			try {
				if( String( this.options.offset ).match( /^\s*-?\d+\s*$/ ) ) {
					this.offX = parseInt( this.options.offset );
					this.offY = parseInt( this.options.offset );
				} else if( String( this.options.offset ).match( /^\s*-?\d+\s+-?\d+\s*$/ ) ) {
					var offsets = String( this.options.offset ).trim( ).split( ' ' );
					this.offX = parseInt( offsets[0] );
					this.offY = parseInt( offsets[1] );
				}
			} catch( e ) {}
			
			if( this.offX == null )
				this.offX = 0;
			if( this.offY == null )
				this.offY = 0;
			
			this._positionWaffleIron( );
		},
		
		/**
		 * @private
		 */
		_positionWaffleIron: function( ) {
			if( !this.waffleIron )
				return;
			
			var left = 'auto',
				top = 'auto',
				right = 'auto',
				bottom = 'auto',
				width = 'auto',
				height = 'auto';
			
			switch( this.posX ) {
				case 0:
					left = this.offX + 'px';
					break;
				
				case 1:
					var sw = jQuery( window ).width( );
					var mw = this.waffleIron.width( );
					left = ( sw / 2 - mw / 2 ) + 'px';
					break;
				
				case 2:
					right = this.offX + 'px';
					break;
			}
			
			switch( this.posY ) {
				case 0:
					top = this.offY + 'px';
					break;
				
				case 1:
					var sh = jQuery( window ).height( );
					var mh = this.waffleIron.height( );
					top = ( sh / 2 - mh / 2 ) + 'px';
					break;
				
				case 2:
					bottom = this.offY + 'px';
					break;
			}
			
			this.waffleIron.css( {
				left: left,
				top: top,
				right: right,
				bottom: bottom,
				width: width,
				height: height
			} );		
		},
		
		/**
		 * Sort the waffles by weight and re-append to the waffle iron
		 * 
		 * @protected
		 */
		sortWaffles: function( ) {
			var waffleSort = this.waffles.slice( 0 );
			waffleSort.sort( function( a, b ) {
				return a.options.weight - b.options.weight;
			} );
			for( var i = 0; i < waffleSort.length; i++ ) {
				waffleSort[i].element.appendTo( this.waffleIron );
			}
		},
		
		/**
		 * Pop up a waffle with the given options
		 * 
		 * Available Options:
		 *     autoClose - true - close the waffle automatically after a given period of time
		 *     clickClose - true - close the waffle on click
		 *     delay - 3500 - time to display waffle before automatically closing (meaningless if autoClose is false)
		 *     showCloseButton - null - show the close button (meaningless if clickClose is false), null means show close button only if autoClose is disabled
		 *     content - empty - content to show in the waffle, can be HTML text, a DOM node or a jQuery element - anything jQuery.html can accept
		 *     icon - null - icon to show in the waffle, can be any jQuery icon (.ui-icon-*)
		 *     spinner - false - show a spinner in the waffle
		 *     error - false - set the error state on the waffle
		 *     popEffect - fade - effect to use when showing waffle (not yet implemented, always uses fade)
		 *     popDuration - 200 - duration for pop effect
		 *     burnEffect - fade - effect to use when hiding waffle (not yet implemented, always uses fade)
		 *     burnDuration - 600 - duration for burn effect
		 *     maxWidth - 300 - maximum width for the waffle (not yet implemented)
		 *     maxHeight - 64 - maximum height for the waffle (not yet implemented)
		 *     weight - 0 - waffle weight, heavier waffles fall to the bottom of the stack
		 * 
		 * @example
		 *     There are two ways to call this method. The first is to specify options as an object literal:
		 *         jQuery.waffle.pop( {
		 *             content: 'Waffle content goes <a href="http://www.google.com">here</a>',
		 *             autoClose: false,
		 *             weight: 10
		 *         } );
		 *     The second is a shortcut that specifies only content and weight:
		 *         jQuery.waffle.pop( 'Round waffles', -10 );
		 * 
		 * @param {Object} waffleOpts
		 * @param {Number} [weight]
		 * @returns Number Waffle ID
		 */
		pop: function( waffleOpts, weight ) {
			if( typeof waffleOpts == 'string' )
				waffleOpts = { content: waffleOpts };
			if( typeof weight != 'undefined' )
				waffleOpts.weight = weight;
			
			var w = new Waffle( this, waffleOpts );
			return w.id;
		},
		
		/**
		 * Pop up a waffle with error message defaults. Equivalent to:
		 * <code>
		 * 		jQuery.waffle.pop( {
		 * 			content: <content>,
		 * 			error: true,
		 * 			icon: 'ui-icon-alert',
		 * 			delay: <default delay * 1.5>
		 * 		} );
		 * </code>
		 * 
		 * @see jQuery.waffle.pop
		 * @param {Object} waffleOpts
		 * @param {Number} [weight]
		 * @returns Number Waffle ID
		 */
		error: function( waffleOpts, weight ) {
			if( typeof waffleOpts == 'string' )
				waffleOpts = { content: waffleOpts };
			waffleOpts.error = true;
			waffleOpts.icon = waffleOpts.icon || 'ui-icon-alert';
			waffleOpts.delay = waffleOpts.delay || ( Waffle.defaults.delay * 1.5 );
			return this.pop( waffleOpts, weight );
		},
		
		/**
		 * Pop up a waffle with success message defaults. Equivalent to:
		 * <code>
		 * 		jQuery.waffle.pop( {
		 * 			content: <content>,
		 * 			icon: 'ui-icon-circle-check',
		 * 			delay: <default delay * 1.5>
		 * 		} );
		 * </code>
		 * 
		 * @see jQuery.waffle.pop
		 * @param {Object} waffleOpts
		 * @param {Number} weight
		 * @returns Number Waffle ID
		 */
		success: function( waffleOpts, weight ) {
			if( typeof waffleOpts == 'string' )
				waffleOpts = { content: waffleOpts };
			waffleOpts.icon = waffleOpts.icon || 'ui-icon-circle-check';
			waffleOpts.delay = waffleOpts.delay || ( Waffle.defaults.delay * 1.5 );
			return this.pop( waffleOpts, weight );
		},
		
		/**
		 * Pop up a waffle with working (busy) defaults. Equivalent to:
		 * <code>
		 * 		jQuery.waffle.pop( {
		 * 			content: <content - default 'Working...'>,
		 * 			autoClose: false,
		 * 			clickClose: false,
		 * 			weight: -10,
		 * 			spinner: true
		 * 		} );
		 * </code>
		 * 
		 * @see jQuery.waffle.pop
		 * @param {Object} waffleOpts
		 * @param {Number} weight
		 * @returns Number Waffle ID
		 */
		working: function( waffleOpts, weight ) {
			if( typeof waffleOpts == 'string' )
				waffleOpts = { content: waffleOpts };
			waffleOpts = jQuery.extend( {
				content: 'Working...',
				autoClose: false,
				clickClose: false,
				spinner: true
			}, waffleOpts );
			return this.pop( waffleOpts, weight == undefined ? -10 : weight );
		},
		
		/**
		 * Burn a waffle with the given ID (returned by jQuery.waffle.pop)
		 * 
		 * @param {Number} waffleId
		 * @returns jQuery.Deferred
		 */
		burn: function( waffleId ) {
			var waffle = this.findWaffle( waffleId );
			if( waffle ) {
				return waffle.burn( );
			}
			
			return null;
		},
		
		
		compat: function( ) {
			if( this.compatTimer )
				return;
			
			// Firefox
			if( jQuery.browser.mozilla && jQuery.browser.version >= 3.5 )
				return;
			
			// WebKit (Chrome/Safari)
			if( jQuery.browser.webkit )
				return;
			
			// IE
			if( jQuery.browser.msie && jQuery.browser.version >= 9 && ( typeof document.documentMode == 'undefined' || document.documentMode >= 9 ) )
				return;
			
			var self = this;
			this.compatTimer = setInterval( function( ) {
				if( !jQuery.isReady )
					return;
				
				clearInterval( self.compatTimer );
				
				var id = null;
				try {
					id = self.error( {
						weight: -100,
						content: 'Your browser version may not support all the functionality of this page.<br />Use <a href="http://getfirefox.com">Firefox 3.5+</a>, <a href="http://www.google.com/chrome">Chrome</a>, or <a href="http://www.microsoft.com/IE9">Internet Explorer 9+</a> to ensure compatibility.',
						showCloseButton: true,
						delay: 30000
					} );
				} catch( e ) {}
				
				if( id === null )
					jQuery( '<div style="padding: 0.4em;" class="ui-widget-content ui-state-error"><div class="ui-icon ui-icon-alert" style="float: left; margin-right: 0.2em; display: inline-block; vertical-align: middle;" />Your browser version may not support all the functionality of this page. Use <a href="http://getfirefox.com">Firefox 3.5+</a>, <a href="http://www.google.com/chrome">Chrome</a>, or <a href="http://www.microsoft.com/IE9">Internet Explorer 9+</a> to ensure compatibility.</div>' ).prependTo( 'body' );
			}, 100 );
		},
		
		/**
		 * Find a waffle by id.
		 * @param {Number} waffleId Waffle id
		 * @returns Waffle The Waffle object, or null if not found
		 */
		findWaffle: function( waffleId ) {
			var index = this.findWaffleIndex( waffleId );
			if( index in this.waffles )
				return this.waffles[index];
			
			return null;
		},
		
		/**
		 * Find index of a waffle by object or id.
		 * @param {Number|Waffle} waffleId Waffle id or a Waffle object
		 * @returns Number index of the waffle
		 */
		findWaffleIndex: function( waffleId ) {
			if( waffleId instanceof Waffle ) 
				return this.waffles.indexOf( waffleId );
			
			for( var i = 0; i < this.waffles.length; i++ ) {
				if( this.waffles[i].id == waffleId )
					return i;
			}
			
			return -1;
		}
	};
	
	//jQuery( document ).ready( function( ) { jQuery.waffle.init( ); } );
	jQuery.waffle.init( );
} )( );
