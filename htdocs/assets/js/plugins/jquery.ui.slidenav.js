/**
 * @class jQuery.ui.slidenav
 */
jQuery.widget( 'ui.slidenav', {
	/** @memberOf jQuery.ui.slidenav */
	name: 'jQuery.ui.slidenav',
	
	options: {
		defaultPanel: null
	},
	
	firstPanel: null,
	anchor: null,
	tag: null,
	container: null,
	slider: null,
	slideTimer: null,
	
	_create: function( ) {
		var self = this,
			o = self.options,
			el = self.element;
		
		// check for previous instantiation
		if( el.hasClass( 'slidenav' ) )
			return;
		
		// only one allowed!
		if( jQuery( '.slidenav' ).length > 0 )
			throw "jquery.ui.slidenav: cannot create more than one slidenav";
		
		// attach to the element
		el.addClass( 'slidenav' );
		el.data( 'slidenav', this );
		
		// set up hashchange handler
		jQuery( window ).hashchange( function( ) {
			self.switchPanel( location.hash );
		} );
		
		// set first panel as default panel if exists
		if( o.defaultPanel && jQuery( o.defaultPanel ).length ) {
			this.firstPanel = jQuery( o.defaultPanel ).first( );
		}
		
		// assign functionality to nav links
		jQuery( '.navLink:not(.disabled)' ).each( function( ) {
			var panelId = jQuery( this ).attr( 'href' );
			var panel = jQuery( panelId.replace( '#', '#nav-' ) );
			
			if( panel.length ) {
				self.firstPanel = self.firstPanel || panel;
				var callback = jQuery( this ).attr( 'onclick' );
				jQuery( this ).removeAttr( 'onclick' );
				panel.data( 'callback', callback );
				
				jQuery( this ).click( function( event ) {
					window.location.hash = panelId;
					event.preventDefault( );
					return false;
				} );
			}
		} );
		
		jQuery( '.navLink.disabled' ).addClass( 'ui-state-disabled' );
		
		// modify the element
		this.anchor = el.parent( );
		
		el.show( );
		el.children( ).addClass( 'ui-state-default' );
		el.wrap( '<div class="slidenavContainer ui-widget"><div class="slidenavSlider ui-widget-content" /></div>' );
		el.width( el.children( ).outerWidth( true ) );
		
		this.slider = el.parent( );
		this.container = this.slider.parent( );
		this.container.appendTo( 'body' );
		
		this.tag = jQuery( '<div class="navTag ui-widget-header ui-corner-right"><span /></div>' )
					.appendTo( this.container );
		
		jQuery( '<div class="navTagHandle"><div class="navTagFade" /><div class="navTagFoot">&lt;&lt;&lt;&lt;&lt;&lt;</div></div>' ).appendTo( this.tag );
		/*.position( {
			my: 'right bottom',
			at: 'right bottom',
			of: this.tag
		} );*/
		
		var bgColor = this.tag.css( 'backgroundColor' );
		jQuery( '.navTagFoot', this.tag ).css( 'backgroundColor', bgColor );
		var bgColorA = bgColor.replace( 'rgb', 'rgba' ).replace( ')', ',0)' );
		
		var ntf = jQuery( '.navTagFade' ).css( 'background', 'transparent' );
		
		if( jQuery.browser.msie ) {
			var components, colorStr = 'ffffff';
			if( components = bgColor.match( /^\s*rgb\(\s*(\d+)\s*,\s*(\d+)\s*,\s*(\d+)\s*\).*$/ ) )
				colorStr = Number( components[1] ).toString( 16 ) + Number( components[2] ).toString( 16 ) + Number( components[3] ).toString( 16 );
			else if( components = bgColor.match( /^\s*[#]?([0-9A-Fa-f]{2})([0-9A-Fa-f]{2})([0-9A-Fa-f]{2})([0-9A-Fa-f]{2})?$/ ) ) 
				colorStr = '' + components[1] + components[2] + components[3];
			ntf.css( 'filter', 'progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#00' + colorStr + '\', endColorstr=\'#ff' + colorStr + '\')' );
		} else if( jQuery.browser.mozilla ) {
			ntf.css( 'background', '-moz-linear-gradient(top, ' + bgColorA + ',' + bgColor + ')' );
		} else if( jQuery.browser.webkit ) {
			ntf.css( 'background', '-webkit-linear-gradient(top, ' + bgColorA + ',' + bgColor + ')' );
		} else if( jQuery.browser.opera ) {
			ntf.css( 'background', '-o-linear-gradient(top, ' + bgColorA + ',' + bgColor + ')' );
		} else {
			ntf.css( 'background', 'linear-gradient(top, ' + bgColorA + ',' + bgColor + ')' );
		}
		
		this.slider.width( el.outerWidth( true ) );
		
		// set up events
		this.container.hover( function( ) {
			self.slideOpen( );	
		}, function( ) {
			self.slideClose( );
		} );
		
		el.children( 'a' ).hover( function( ) {
			jQuery( this ).removeClass( 'ui-state-default' );
			jQuery( this ).addClass( 'ui-state-hover' );
		}, function( ) {
			jQuery( this ).removeClass( 'ui-state-hover' );
			jQuery( this ).addClass( 'ui-state-default' );
		} );
		
		jQuery( window ).resize( function( ) {
			self.positionContainer( );
		} );
		
		jQuery( window ).scroll( function( ) {
			self.positionContainer( );
		} );
		
		// close the slider
		self.slideClose( );
		self.positionContainer( );
		
		// switch to panel in hash fragment, or default panel if no hash
		if( location.hash == undefined || jQuery( location.hash.replace( '#', '#nav-' ) ).length == 0 ) {
			this.replacePanel( );
		} else {
			this.switchPanel( location.hash );
		}
	},
	
	positionContainer: function( ) {
		var ao = this.anchor.offset( );
		if( jQuery( window ).scrollTop( ) > ao.top ) {
			this.container.addClass( 'slidenavFixed' ).css( {
				left: ao.left,
				top: 0
			} );
		} else {
			this.container.removeClass( 'slidenavFixed' ).position( {
				my: 'left top',
				at: 'left top',
				of: this.anchor,
				collision: 'none'
			} );
		}
		
		var ap = jQuery( '.slidenavAnchorPlaceholder', this.anchor ); 
		if( ap.length == 0 )
			ap = jQuery( '<div class="slidenavAnchorPlaceholder" />' ).appendTo( this.anchor );
		
		ap.height( this.container.outerHeight( true ) );
		
		this.container.css( 'max-height', jQuery( window ).height( ) - 24 );
		this.container.width( this.tag.outerWidth( true ) ); // chrome doesn't resize container when content shrinks
		
		var tagPadding = this.tag.outerHeight( true ) - this.tag.height( );
		this.tag.height( this.container.innerHeight( ) - tagPadding );
		var tagText = jQuery( 'span', this.tag );
		var tagTextPadding = tagText.outerWidth( true ) - tagText.width( );
		jQuery( 'span', this.tag ).width( this.tag.height( ) - tagTextPadding );
		
		var sliderPadding = this.slider.outerHeight( true ) - this.slider.height( );
		this.slider.css( 'max-height', this.container.innerHeight( ) - sliderPadding );
		
		this.updateTag( );
	},
	
	slideOpen: function( ) {
		if( this.slideTimer ) {
			clearTimeout( this.slideTimer );
			this.slideTimer = null;
		}
		this.slider.animate( {
			width: this.element.outerWidth( true )
		} );
	},
	
	slideClose: function( ) {
		var self = this;
		this.slideTimer = setTimeout( function( ) {
			clearTimeout( this.slideTimer );
			this.slideTimer = null;
			self.slider.animate( {
				width: 0
			} );
		}, 800 );
	},
	
	updateTag: function( tag ) {
		var tagText = jQuery( 'span', this.tag );
		if( tag != undefined )
			tagText.text( tag );
		/*tagText.position( {
			my: 'center',
			at: 'center',
			of: this.tag
		} );*/
	},
	
	switchPanel: function( panelId ) {
		var panel = jQuery( panelId.replace( '#', '#nav-' ) );
		
		if( panel.length ) {
			//jQuery( '.navPanel' ).stop( ).fadeOut( ).then( function( ) { panel.fadeIn( ); } );
			//jQuery( '.navPanel' ).hide( );
			//panel.show( );
			
			jQuery( '.navPanel' ).stop( );
			
			var visible = jQuery( '.navPanel:visible' );
			visible.trigger( 'uihide' );
			
			jQuery.when( visible.animate( {
				opacity: 0
			}, {
				duration: 600,
				queue: false
			} ) ).then( visible.hide( ) );
			panel.css( 'opacity', 0 ).show( ).animate( {
				opacity: 1
			}, {
				duration: 600,
				queue: false
			} );
			
			var link = jQuery( '.navLink[href=' + panelId + ']', this.element );
			if( link && link.length ) {
				this.updateTag( link.text( ) );
				this.element.children( ).removeClass( 'ui-state-highlight' ).addClass( 'ui-state-default' );
				link.removeClass( 'ui-state-default' ).addClass( 'ui-state-highlight' );
			}
			
			var callback = panel.data( 'callback' );
			if( callback != undefined ) {
				try {
					if( typeof callback == 'function' )
						callback( );
					else if( typeof callback == 'string' )
						eval( callback );
				} catch( e ) {}
			}
			panel.trigger( 'uishow' );
		} else {
			this.replacePanel( );
		}
	},
	
	replacePanel: function( ) {
		var loc = location.href.replace( location.hash, '' );
		var visiblePanel = jQuery( '.navPanel:visible' );
		
		var gotoPanel = visiblePanel.length ? visiblePanel : this.firstPanel;
		
		if( gotoPanel && gotoPanel.length ) {
			var hash = ( '#' + gotoPanel.attr( 'id' ) ).replace( '#nav-', '#' );
			location.replace( loc + hash );
			this.switchPanel( hash );
		}
	},
	
	destroy: function( ) {
		
	}
} );

jQuery( document ).ready( function( ) {
	jQuery( '.slidenavPanel' ).slidenav( );
} );
