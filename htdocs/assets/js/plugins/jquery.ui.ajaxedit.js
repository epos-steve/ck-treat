jQuery.widget( 'ui.ajaxedit', {
	name: 'jQuery.ui.ajaxedit',
	
	options: {
		cancel: null,
		submit: null,
		submitfailure: null,
		aftersubmit: null,
		datachange: null,
		data: null,
		floatFieldsets: false,
		floatSubmit: true,
		validateForm: null,
	},
	
	inputs: null,
	errorBox: null,
	
	waffle: null,
	
	resizeTimer: null,
	
	loadingData: false,
	
	_create: function( ) {
		var self = this,
			o = self.options,
			el = self.element;
		
		// check for previous instantiation
		if( el.hasClass( 'ajaxedit' ) )
			return;
		
		// attach to the element
		el.addClass( 'ajaxedit ui-widget' );
		el.data( 'ajaxedit', this );
		
		if( jQuery.browser.msie )
			el.addClass( 'ie' );
		
		if( o.floatSubmit ) {
			var saveFs = el.find( 'input[type=submit]' ).closest( 'fieldset' );
			if( saveFs.length ) {
				saveFs.html( '<div class="ajaxeditSaveBlock">' + saveFs.html( ) + '</div>' );
			}
		}
		
		// style elements
		this.styleForms( );
		this.styleInputs( );
		
		el.on( 'change', 'fieldset input, fieldset select, fieldset textarea', function( ) {
			self.makeDirty( this );
		} );
		
		el.on( {
			mouseenter: function( ) {
				jQuery( this ).addClass( 'ui-state-hover-nostyle' );
			},
			mouseleave: function( ) {
				jQuery( this ).removeClass( 'ui-state-hover-nostyle' );
			}
		}, 'fieldset .ajaxeditLabel.ui-state-error-text' );
		
		el.on( 'click', 'fieldset :reset', function( ) {
			self._trigger( 'cancel' );
		} );
		
		this.errorBox = jQuery( '<div class="ajaxeditErrorBox" />' ).prependTo( el );
		
		el.on( 'reset', function( ) {
			setTimeout( function( ) {
				self.inputs.change( );
				self.clearDirty( );
			}, 10 );
		} );
		
		//this.bindValidation( );
		
		// events
		jQuery( window ).on( {
			resize: function( ) {
				self.shouldResizeInputs( );
			},
			scroll: function( ) {
				self.positionSave( );
			}
		} );
		
		el.on( {
			panelgroupresize: function( ) {
				self.shouldResizeInputs( );
			},
			panelgroupresizing: function( ) {
				self.positionSave( );
			}
		} );
		
		el.on( {
			ajaxeditdatachange: function( ) {
				self.shouldResizeInputs( );
			},
			'submit.ajaxedit': function( event ) {
				event.preventDefault( );
				self.submit( );
				return false;
			}
		} );
		
		if( window.onbeforeunload == null ) {
			window.onbeforeunload = function( ) {
				var msg = self.formUnload( );
				if( msg != null )
					return msg;
			};
		} else {
			var _obu = window.onbeforeunload;
			window.onbeforeunload = function( ) {
				var rv = _obu();
				if( rv != null )
					return rv;
				
				rv = self.formUnload( );
				if( rv != null )
					return rv;
			};
		}
		
		if( o.resetAfterSubmit ) {
			el.on( 'ajaxeditaftersubmit', function( ) {
				self.reset( );
			} );
		}
		
		el.closest( '.navPanel' ).on( 'uishow', function( ) {
			self.shouldResizeInputs( );
		} );
		
		// handle callbacks
		/*var callbacks = {};
		
		if( o.submit ) callbacks['ajaxeditsubmit'] = o.submit;
		if( o.cancel ) callbacks['ajaxeditcancel'] = o.cancel;
		if( o.datachange ) callbacks['ajaxeditdatachange'] = o.datachange;
		if( o.submitfailure ) callbacks['ajaxeditsubmitfailure'] = o.submitfailure;
		if( o.aftersubmit ) callbacks['ajaxeditaftersubmit'] = o.aftersubmit;
		
		if( !jQuery.isEmptyObject( callbacks ) )
			el.on( callbacks );*/
		
		if( o.floatFieldsets ) {
			el.find( 'fieldset' ).addClass( 'ajaxeditFieldsetFloat' );
			el.find( 'fieldset' ).last( ).removeClass( 'ajaxeditFieldsetFloat' ).addClass( 'ajaxeditFieldsetLast' );
		}
		
		// clear form
		this.reset( );
	},
	
	positionSave: function( ) {
		var saveFs = this._saveFs = ( ( this._saveFs && this._saveFs.length ) ? this._saveFs : jQuery( '.ajaxeditSaveBlock:visible', this.element ) );
		if( saveFs.length && saveFs.closest( '.ui-helper-hidden-accessible' ).length == 0 ) {
			saveFs.css( 'position', 'static' ).removeClass( 'ajaxeditFloatingSave ui-widget-content' );
			var savePos = saveFs.offset( );
			
			this._window = ( this._window ? this._window : jQuery( window ) );
			var formTop = this.element.offset( ).top;
			var scrollTop = this._window.scrollTop( );
			var saveTo = scrollTop + this._window.height( ) - saveFs.outerHeight( true );
			
			if( saveTo < ( formTop + 56 ) )
				saveTo = formTop + 56;
			
			if( saveTo < savePos.top ) {
				saveFs.css( {
					position: 'relative',
					top: saveTo - savePos.top
				} ).addClass( 'ajaxeditFloatingSave ui-widget-content' );
			}
		}
	},
	
	shouldResizeInputs: function( ) {
		var self = this;
		
		if( this.resizeTimer )
			clearTimeout( this.resizeTimer );
		this.resizeTimer = setTimeout( function( ) {
			self.resizeInputs( );
		}, 1 );
	},
	
	resizeInputs: function( ) {
		var el = this.element;
		var fs = jQuery( 'fieldset', el ).first( );
		var fsP = fs.outerWidth( true ) - fs.width( );
		var lbl = fs.find( 'label' ).first( );
		var lP = lbl.outerWidth( true ) - lbl.width( );
		var maxW = Math.ceil( fs.parent( ).width( ) ) - fsP - lP;
		
		jQuery( '.ui-helper-fillheight', el ).height( '10000px' );
		
		this.inputs.each( function( ) {
			var elem = jQuery( this );
			if( elem.is( ':checkbox' ) || elem.is( 'select' ) || elem.is( ':radio' ) ) {
				elem = elem.parent( );
			}
			var elemP = elem.outerWidth( true ) - elem.width( );
			elem.css( 'max-width', maxW - elemP );
		} );
		
		el.find( 'fieldset' ).each( function( ) {
			var fs = jQuery( this );
			fs.addClass( 'nowrap-group nowrap-label' ).css( 'max-width', '' );
			if( jQuery.browser.msie ) {
				fs.removeClass( 'nowrap-group nowrap-label' );
			} else {
				if( fs.width( ) > maxW + lP )
					fs.removeClass( 'nowrap-group' );
				if( fs.width( ) > maxW + lP )
					fs.removeClass( 'nowrap-label' );
			}
			fs.css( 'max-width', maxW + lP );
		} );
		
		jQuery( '.ui-helper-fillheight', el ).each( function( ) {
			var pad = jQuery( this ).outerHeight( true ) - jQuery( this ).height( );
			jQuery( this ).height( jQuery( this ).parent( ).innerHeight( ) - pad );
		} );
		
		this.positionSave( );
	},
	
	styleForms: function( ) {
		var self = this,
			el = self.element;
		jQuery( 'fieldset', el ).addClass( 'ui-widget-content ui-helper-clearfix' );
		jQuery( 'legend', el ).addClass( 'ui-widget-header ui-corner-all' );
		
		jQuery( 'fieldset', el ).each( function( ) {
			if( jQuery( this ).find( 'legend' ).length == 0 ) {
				jQuery( '<legend class="emptyLegend">__</legend>' ).prependTo( this );
			}
		} );
	},
	
	styleInputs: function( inputs ) {
		var self = this,
			el = self.element;
		this.inputs = el.find( 'input, select, textarea' );
		inputs = inputs || this.inputs;
		
		jQuery.each( inputs, function( ) {
			var input = jQuery( this );
			if( input.hasClass( 'ajaxedit-input' ) )
				return;

			input.addClass( 'ajaxedit-input ui-state-default ui-corner-all' );
			input.not( '[type=button], :reset, :submit, [type=hidden]' ).wrap( '<label class="ajaxeditLabel" />' );
			
			var container = input.parent( '.ajaxeditLabel' );
			jQuery( '<div class="ajaxeditError ui-corner-all"><div class="ui-widget-content ui-state-error ui-corner-all" /></div><div class="ajaxeditIcon ui-icon ui-icon-alert" />' ).appendTo( container );
			
			var label = input.attr( 'alt' );
			if( label && label.length ) {
				if( input.is( ':checkbox' ) )
					input.after( '<span> ' + label + '</span>' );
				else
					input.before( '<span>' + label + ': </span>' );
			}
			
			if( input.is( ':reset, :submit' ) )
				self.button( this );
			else if( input.is( ':checkbox' ) )
				self.checkbox( this );
			else if( input.is( 'input[type="text"]' ) 
				|| input.is( 'textarea' ) 
				|| input.is( 'input[type="password"]' ) ) {
				var slider = false;
				var reg = new RegExp( "^(slider)-([0-9]{1,})(-([0-9]{1,}))?$", "i" );
				var cssNames = this.className != '' ? this.className.split( /\s+/ ) : [];
				if( cssNames.length ) {
					for( var i = 0, il = cssNames.length; i < il; i++ ) {
						var m = reg.exec( cssNames[i] );
						if( m && m.length >= 2 ) {
							slider = {
								max: m.length >= 4 ? m[4] : m[2],
								min: m.length >= 4 ? m[2] : undefined
							};
							break;
						}
					}
				}
				
				if( slider )
					self.slider( this, slider.min, slider.max );
				
				self.textelement( this );
			} else if( input.is( ':radio' ) ) {
				self.radio( this );
			} else if( input.is( 'select' ) ) {
				if( jQuery.ui.picker && input.hasClass( 'picker' ) ) {
					input.picker( );
				} else if( input.attr( 'size' ) != undefined ) {
					self.checklist( this );
				} else {
					self.select( this );
				}
			}
			
			if( input.hasClass( 'date' ) )
				input.datepicker( );
			
			if( input.is( ':radio, :checkbox' ) ) {
				input.data( 'defaultValue', input.is( ':checked' ) ? true : false );
			} else
				input.data( 'defaultValue', input.val( ) );
			
			input.change( function( ) {
				self.validateField( this );
			} );

			var replacement = input.data( 'replacedBy' ) || input;
			var title = input.prop( 'title' );
			if( title )
				replacement.prop( 'title', input.prop( 'title' ) );
		} );
		
		jQuery( '.ui-ajaxedit-selectgroup', el ).each( function( ) {
			var group = jQuery( this );
			var id = group.attr( 'id' );
			var tid = id.split( '_' );
			
			var selectValue = tid.pop( );
			var selectName = tid.join( '_' );
			
			var select = jQuery( '[name="' + selectName + '"]' );
			if( select.length ) {
				var groups = select.data( 'groups' ) || [];
				groups.push( group );
				select.data( 'groups', groups ).addClass( 'ui-ajaxedit-select-hasgroup' );
			}
		} );
		
		jQuery( '.ui-ajaxedit-select-hasgroup', el ).change( function( ) {
			var select = jQuery( this );
			var groups = select.data( 'groups' );
			var target = select.attr( 'name' ) + '_' + select.val( );
			jQuery.each( groups, function( ) {
				this.toggle( this.attr( 'id' ) == target );
			} );
		} ).change( );

		jQuery( 'fieldset', el ).on( 'change', 'input, select, textarea', function( ) {
			self.makeDirty( this );
		} );

		jQuery( 'fieldset', el ).on( {
			mouseenter: function( ) {
				jQuery( this ).addClass( 'ui-state-hover-nostyle' );
			},
			mouseleave: function( ) {
				jQuery( this ).removeClass( 'ui-state-hover-nostyle' );
			}
		}, '.ajaxeditLabel.ui-state-error-text' );

		jQuery( ':reset', el ).on( 'click', function( ) {
			setTimeout( function( ) {
				self._trigger( 'cancel' );
			}, 1);
		} );

		this.errorBox = jQuery( '<div class="ajaxeditErrorBox" />' ).prependTo( el );
		
		this.inputs.filter( '[readonly]' ).addClass( 'ui-state-disabled' ).parent( 'label' ).addClass( 'ui-state-disabled' );
		
		el.on( 'reset', function( ) {
			setTimeout( function( ) {
				self.inputs.change( );
				self.clearDirty( );
			}, 10 );
		} );

		jQuery( '.hover', el ).not( '.hover-ready' ).addClass( '.hover-ready' ).hover( function( ) {
			jQuery( this ).addClass( 'ui-state-hover' );
		}, function( ) {
			jQuery( this ).removeClass( 'ui-state-hover' );
		} );
		
		this.bindValidation( inputs );
	},
	
	reset: function( ) {
		this.element.get( 0 ).reset( );
		if( this.options.data && !this.loadingData ) {
			this.edit( this.options.data );
		}
	},
	
	fillDefault: function( ) {
		var self = this,
			el = self.element,
			inputs = this.inputs;
		
		this.reset( );
		jQuery.each( inputs, function( ) {
			if( jQuery( this ).is( ':checkbox, :radio' ) ) {
				jQuery( this ).data( 'defaultValue' ) && jQuery( this ).attr( 'checked', 'checked' ) || jQuery( this ).removeAttr( 'checked' );
			} else {
				jQuery( this ).val( jQuery( this ).data( 'defaultValue' ) );
			}
			jQuery( this ).change( );
		} );
	},
	
	textelement: function( element ) {
		var el = jQuery( element );
		el.on( {
			focusin: function( ) {
				jQuery( this ).addClass( 'ui-state-focus' );
			},
			focusout: function( ) {
				jQuery( this ).removeClass( 'ui-state-focus' );
			}
		} ).addClass( 'ui-state-default hover' );
		
		if( el.is( ':disabled' ) || el.attr( 'readonly' ) != undefined )
			el.addClass( 'ui-state-disabled' );
		
		if( el.is( '[type="password"]' ) )
			el.attr( 'autocomplete', 'off' ); // don't save passwords, this is an ajax form and probably shouldn't be a login form
	},
	
	slider: function( element, min, max ) {
		var el = jQuery( element );
		
		var slideMin = min == undefined ? 1 : min;
		var slideMax = max == undefined ? 10 : max;
		
		var slider = el.before( '<div id="' + el.attr( 'name' ) + '_slider" />' ).prev( );
		slider.slider( {
			animate: true,
			max: parseInt( slideMax ),
			min: parseInt( slideMin ),
			value: el.val( ),
			slide: function( event, ui ) {
				el.val( ui.value );
				el.change( );
			}
		} ).wrap( '<div class="ajaxeditSlideWrapper" />' );
		el.change( function( ) {
			slider.slider( 'value', el.val( ) );
		} );
	},
	
	button: function( element ) {
		var el = jQuery( element );
		if( el.is( ':submit' ) )
			el.addClass( 'ui-priority-primary ui-corner-all hover' );
		else if( el.is( ':reset' ) )
			el.addClass( 'ui-priority-secondary ui-corner-all hover' );
		
		el.on( {
			mousedown: function( ) {
				jQuery( this ).addClass( 'ui-state-active' );
			},
			mouseup: function( ) {
				jQuery( this ).removeClass( 'ui-state-active' );
			}
		} );
	},
	
	checkbox: function( element ) {
		var el = jQuery( element ).wrap( '<div style="display: inline-block;"></div>' );
		el.parent( 'div' ).after( '<span />' );
		var parent = el.parent( 'div' ).next( );
		el.addClass( 'ui-helper-hidden' );
		parent.css( {
			width: '16px',
			height: '16px',
			display: 'block'
		} );
		parent.wrap( '<span class="ui-state-default ui-corner-all" style="display: inline-block; width: 16px; height: 16px; margin-right: 5px;" />' );
		parent.parent( ).addClass( 'hover' );
		parent.parent().parent().attr( 'style', el.attr( 'style' ) );
		parent.parent( ).parent( ).css( 'cursor', 'pointer' );
		/*parent.parent( 'span' ).click( function( event ) {
			el.is( ':checked' ) && el.removeAttr( 'checked' ) || el.attr( 'checked', 'checked' );
		} );*/
		
		jQuery( element ).change( function( ) {
			var checked = jQuery( this ).is( ':checked' );
			parent.toggleClass( 'ui-icon ui-icon-check', checked );
			parent.parent( 'span' ).toggleClass( 'ui-state-active', checked );
		} ).change( );

		el.data( 'replacedBy', parent.parent( ) );
	},
	
	radio: function( element ) {
		var el = jQuery( element );
		el.parent( 'label' ).after( '<span />' );
		var parent = el.parent( 'label' ).next( );
		el.addClass( 'ui-helper-hidden' );
		parent.addClass( 'ui-icon ui-icon-radio-off' );
		parent.wrap( '<span class="ui-state-default ui-corner-all" style="display: inline-block; width: 16px; height: 16px; margin-right: 5px;" />' );
		parent.parent( ).addClass( 'hover' );
		parent.parent( 'span' ).click( function( event ) {
			el.attr( 'checked', 'checked' );
		} );
		
		jQuery( element ).change( function( ) {
			var checked = jQuery( this ).is( ':checked' );
			parent.toggleClass( 'ui-icon-radio-off', !checked ).toggleClass( 'ui-icon-bullet', checked );
			parent.parent( 'span' ).toggleClass( 'ui-state-active', checked );
		} ).change( );

		el.data( 'replacedBy', parent.parent( ) );
	},
	
	select: function( element ) {
		var el = jQuery( element ).wrap( '<div style="display: inline-block;"></div>' ),
			parent = el.parent( ),
			container = parent.parent( );
		
		container.css( {
			position: 'relative',
			cursor: 'pointer'
		} );
		parent.css( {
			height: '21px'
		} ).addClass( 'ui-state-default ui-corner-all hover' );
		el.addClass( 'ui-helper-hidden-accessible' );
		parent.append( '<span class="labeltext" style="float: left; margin-top: 1px; margin-left: 1px;" />' );
		parent.append( '<span style="float: right; display: inline-block; margin-top: 3px;" class="ui-icon ui-icon-triangle-1-s" />' );
		parent.after( '<ul class="ui-helper-reset ui-widget-content ui-helper-hidden" style="position: absolute; z-index: 50; right: 0; top: 22px; overflow-x: hidden; overflow-y: auto; max-height: 210px;"></ul>' );


		jQuery.each( el.find( 'option' ), function( ) {
			var $this = jQuery( this );
			var text = $this.html( );
			var tarray = text.split( '||' );
			var li = jQuery( '<li class="hover" style="border: solid 1px transparent;">' + tarray[0] + '</li>' );
			var ttcontent = $this.data( 'tooltip-content' );
			if( ttcontent )
				tarray[1] = ttcontent;
			if ( tarray[1] ) {
				li.data( 'tooltip-content', tarray[1] );
				li.attr( 'title', tarray[1] );
				li.tooltip({
					content: function() {
						return tarray[1];
					},
					//content:		tarray[1],
					tooltipClass:	"functionPanelfunction-tooltip"
				});
			}
			parent.next( 'ul' ).append( li );
			li.data( 'option', $this );
			$this.data( 'li', li );
		} );
		parent.next( 'ul' ).find( 'li' ).click( function( ) {
			el.val( jQuery( this ).data( 'option' ).val( ) ).change( );
		} );
		
		var openMenu = function( menu ) {
			menu.slideDown( 'fast' );
			// delay 1ms so the current event doesn't get caught when it bubbles up
			setTimeout( function( ) {
				jQuery( 'body' ).one( 'click', function( ) {
					closeMenu( menu );
				} );
			}, 1 );
		};
		
		var closeMenu = function( menu ) {
			jQuery( menu ).slideUp( 'fast' );
		};
		
		parent.click( function( event ) {
			var ul = jQuery( this ).next( );
			if( ul.is( ':not(:visible)' ) ) {
				openMenu( ul );
			}
			event.preventDefault( );
		} );

		/*
		parent.next( 'ul' ).add( parent ).appear( function( ) {
			//jQuery( this ).width( el.width( ) );
		} );
		*/
		parent.appear( function( ) {
			jQuery( this ).width( jQuery( this ).next( 'ul' ).width( ) );
		} );

		jQuery( element ).change( function( ) {
			var selected = jQuery( 'option[value=' + jQuery( this ).val( ) + ']', el );
			jQuery( '.labeltext', parent ).html( selected.length ? selected.html().split( '||' )[0] : '' );
		} ).change( );

		el.data( 'replacedBy', parent );
	},
	
	checklist: function( element ) {
		var el = jQuery( element ).wrap( '<div style="display: inline-block;"></div>' ),
			parent = el.parent( ),
			container = parent.parent( );
		
		container.css( {
			position: 'relative',
			cursor: 'pointer'
		} );
		parent.css( {
			height: '21px'
		} ).addClass( 'ui-state-default ui-corner-all hover' );
		el.addClass( 'ui-helper-hidden-accessible' );
		parent.append( '<span class="labeltext" style="float: left; margin-top: 1px; margin-left: 1px;" />' );
		parent.append( '<span style="float: right; display: inline-block; margin-top: 3px;" class="ui-icon ui-icon-triangle-1-s" />' );
		parent.after( '<ul class="ui-helper-reset ui-widget-content ui-helper-hidden" style="position: absolute; z-index: 50; right: 0; top: 22px;"></ul>' );
		jQuery.each( el.find( 'option' ), function( ) {
			var li = jQuery( '<li class="hover" style="border: solid 1px transparent;">' + jQuery( this ).html( ) + '</li>' );
			parent.next( 'ul' ).append( li );
			li.data( 'option', jQuery( this ) );
		} );
		parent.next( 'ul' ).find( 'li' ).click( function( ) {
			el.val( jQuery( this ).data( 'option' ).val( ) ).change( );
		} );
		
		var openMenu = function( menu ) {
			jQuery( menu ).slideDown( 'fast' );
			// delay 1ms so the current event doesn't get caught when it bubbles up
			setTimeout( function( ) {
				jQuery( 'body' ).one( 'click', function( ) {
					closeMenu( menu );
				} );
			}, 1 );
		};
		
		var closeMenu = function( menu ) {
			jQuery( menu ).slideUp( 'fast' );
		};
		
		parent.click( function( event ) {
			var ul = jQuery( this ).next( );
			if( ul.is( ':not(:visible)' ) ) {
				openMenu( ul );
			}
			event.preventDefault( );
		} );
		
		parent.next( 'ul' ).add( parent ).appear( function( ) {
			jQuery( this ).width( el.width( ) );
		} );
		
		jQuery( element ).change( function( ) {
			var selected = jQuery( 'option[value=' + jQuery( this ).val( ) + ']', el );
			jQuery( '.labeltext', parent ).html( selected.length ? selected.html( ) : '' );
		} ).change( );

		el.data( 'replacedBy', container );
	},
	
	edit: function( data ) {
		var self = this;
		
		this.loadingData = true;
		this.element.removeClass( 'ajaxeditBusy' );
		this.element.find( ':submit' ).removeAttr( 'disabled' );
		
		if( this.waffle ) {
			jQuery.waffle.burn( this.waffle );
			this.waffle = null;
		}
		
		if( data.__get ) {
			if( typeof data.__get == 'string' )
				data.__get = { id: data.__get };
			
			this.element.addClass( 'ajaxeditBusy' );
			this.element.find( ':submit' ).attr( 'disabled', 'disabled' );
			
			this.waffle = jQuery.waffle.pop( {
				autoClose: false,
				clickClose: false,
				spinner: true,
				content: 'Loading...',
				weight: -10
			} );
			
			jQuery.ajax( {
				type: 'GET',
				url: data.__url,
				dataType: 'json',
				data: data.__get,
				success: function( r ) {
					if( !r || !( r.id || r.success ) || r.error ) {
						var msg = r && r.error || 'An error has occurred';
						if( jQuery.waffle )
							jQuery.waffle.pop( {
								content: msg,
								icon: 'ui-icon-alert',
								error: true,
								delay: 8000
							} );
						else alert( msg );
					} else {
						self.edit( r );
					}
				}
			} ).always( function( ) {
				if( self.waffle ) {
					jQuery.waffle.burn( self.waffle );
					self.waffle = null;
				}
			} );
			return;
		}
		
		if( data.__isNew ) {
			this.fillDefault( );
		} else {
			this.reset( );
		}
		
		this.element.addClass( 'ajaxedit-loading' );
		
		for( var i in data ) {
			var field = jQuery( '[name="' + i + '"]', this.element );
			var split = false;
			
			if( !field.length ) {
				field = jQuery( '[name="' + i + '[]"]', this.element );
				if( field.length )
					split = true;
			}
			
			if( field.length ) {
				var name = i;
				if( i.match( /\[\]$/ ) ) {
					split = true;
					name = i.replace( '[]', '' );
				}
				
				if( split ) {
					field.removeAttr( 'checked' );
					if( data[i] ) {
						var selected = data[i].split( ',' );
						for( var j = 0; j < selected.length; j++ ) {
							var subField = jQuery( '[name="' + name + '[]"][value="' + selected[j] + '"]', this.element );
							if( subField.length )
								subField.attr( 'checked', 'checked' );
						}
					}
				} else {
					if( field.is( ':checkbox' ) || field.is( ':radio' ) ) {
                        data[i] == '1' && field.attr('checked', 'checked') || field.removeAttr('checked');
                        field.change();
                    } else if ( field.is( 'img' )) {
                        field.attr('src', data[i]);
					} else {
						field.val( data[i] ).change( );
					}
				}
			}
		}
		
		setTimeout( function( ) {
			self.element.removeClass( 'ajaxedit-loading' );
		}, 1 );
		
		this.clearDirty( );
		this.loadingData = false;
		this._trigger( 'datachange', null, data );
	},
	
	getName: function( ) {
		var nameElement = this.element.find( 'input[type=text][name$=name]' );
		if( nameElement.length == 0 )
			nameElement = this.element.find( 'input[type=text][name$=Name]' );
		
		return nameElement.length ? nameElement.val( ) : null;
	},
	
	formUnload: function( ) {
		if( !this.isDirty( ) )
			return null;
		
		var name = this.getName( );
		var saveElement = this.element.find( 'input[type=submit]' );
		
		var nameStr = name ? ' to "' + name + '"' : '';
		var save = saveElement.length ? saveElement.val( ) : 'submit';
		var msg = 'You have not saved your changes' + nameStr + '; return to the page and click the ' + save + ' button if you want to keep your changes';
		
		if( jQuery.browser.mozilla && jQuery.browser.version >= 4 ) {
			alert( msg );
		}
		
		return msg;
	},
	
	makeDirty: function( element ) {
		jQuery( element ).closest( 'fieldset' ).find( 'legend' ).addClass( 'ui-state-highlight' );
	},
	
	clearDirty: function( ) {
		jQuery( 'legend', this.element ).removeClass( 'ui-state-highlight' );
		jQuery( '.ajaxeditErrorBox', this.element ).empty( );
	},
	
	isDirty: function( ) {
		return ( jQuery( this.element ).has( 'legend.ui-state-highlight' ).length > 0 );
	},
	
	bindValidation: function( inputs ) {
		var reg = new RegExp( "^(validate)-([A-Z0-9]{1,})(-([A-Z0-9._]{1,}))?$", "i" );
		jQuery.each( inputs || this.inputs, function( ) {
			var cssNames = (this.className != '' && this.className !== undefined) ? this.className.split( /\s+/ ) : [];
			if( cssNames.length <= 0 )
				return;
			
			for( var i = 0, il = cssNames.length; i < il; i++ ) {
				var m = reg.exec( cssNames[i] );
				if( m && m.length >= 2 ) {
					var data = jQuery( this ).data( 'validate' ) || {};
					data[m[2]] = m[4] || true;
					jQuery( this ).data( 'validate', data );
				}
			}
		} );
	},
	
	validateField: function( field ) {
		field = jQuery( field );
		var data = field.data( 'validate' );
		var errors = [];
		
		if( !data )
			return;
		
		field.removeClass( 'ui-state-error' ).parent( 'label' ).removeClass( 'ui-state-error-text' );
		var value = String( field.val( ) ).trim( );
		
		if( data.type == 'fn' ) {
			try {
				var fn = eval( data.fn );
				var error = fn( this.element, field, value );
				if( error )
					errors.push( error );
			} catch( e ) {
				console.log( e );
			}
		}
		
		if( value != '' ) {
			switch( data.type ) {
				case 'upc':
				case 'integer':
					if( !value.match( /^\d+$/ ) )
						errors.push( 'Must be numeric - numbers 0-9 only, no decimals' );
					break;
				
				case 'double':
					if( !value.match( /^(\d+(\.\d*)?|\.\d*)$/ ) )
						errors.push( 'Must be decimal - numbers 0-9 and decimal only' );
					break;
				
				case 'fn':
					break;
				
				case 'string':
				default:
					
					break;
			}
		} else if( data.notempty )
			errors.push( 'Must not be empty' );
		
		if( errors.length ) {
			field.addClass( 'ui-state-error' ).parent( 'label' ).addClass( 'ui-state-error-text' );
			var label = field.parent( 'label' );
			var width = label.width( );
			var errorDiv = label.find( '.ajaxeditError' ).css( 'max-width', ( width > 150 ? width : 150 ) + 'px' );
			errorDiv.find( 'div' ).html( errors.join( '<br />' ) );
		}
	},
	
	isInvalid: function( ) {
		return this.inputs.filter( '.ui-state-error' ).length > 0;
	},
	
	validateForm: function( ) {
		this.element.find( ':submit' ).removeClass( 'ui-state-error' );
		if( this.options.validateForm ) {
			var error = this.options.validateForm( this.element );
			if( error ) {
				this.element.find( ':submit' ).addClass( 'ui-state-error' );
				return error;
			}
		}
	},

	serialize: function( ) {
		"use strict";
		return this.element.serialize();
	},
	
	submit: function( ) {
		var self = this;
		
		this.clearErrors( 'submit' );
		
		jQuery.each( this.inputs, function( ) { self.validateField( this ); } );
		
		var formError = this.validateForm( );
		
		if( this.isInvalid( ) ) {
			this.addError( formError || 'Some fields were invalid. Check fields marked in red for errors.', 'submit' );
			return false;
		}
		
		var action = this.element.get( 0 ).action;
		
		if( !self._trigger( 'submit' ) )
			return;
		
		this.waffle = jQuery.waffle ? jQuery.waffle.working( 'Saving...' ) : null;
		
		jQuery.ajax( {
			type: 'POST',
			url: action,
			dataType: 'json',
			data: this.element.serialize( ),
			success: function( r ) {
				if( self.waffle ) {
					jQuery.waffle.burn( self.waffle );
					self.waffle = null;
				}
				
				if( !r || !( r.id || r.success ) || r.error ) {
					var msg = r && r.error || 'An error has occurred';
					if( jQuery.waffle )
						jQuery.waffle.pop( { 
							content: msg,
							icon: 'ui-icon-alert',
							error: true,
							delay: 8000
						} );
					else alert( msg );
					self._trigger( 'submitfailure' );
					return;
				}
				
				self.clearDirty( );
				
				var name = self.getName( );
				var msg = name ? '"' + name + '" successfully updated' : 'Successfully updated';
				
				if( jQuery.waffle )
					jQuery.waffle.pop( { 
						content: msg,
						icon: 'ui-icon-check'
					} );
				else alert( msg );
				self._trigger( 'aftersubmit' );
			}
		} ).always( function( ) {
			if( self.waffle ) {
				jQuery.waffle.burn( self.waffle );
				self.waffle = null;
			}
		} );
	},
	
	addError: function( msg, tag ) {
		var tagClass = tag != undefined ? 'ajaxeditError-' + tag : '';
		var error = jQuery( '<div class="ui-state-error ui-corner-all ' + tagClass + '">' + msg + '</div>' ).appendTo( this.errorBox );
		
		var st = jQuery( 'html, body' ).scrollTop( );
		var eb = this.errorBox.offset( ).top;
		
		if( eb < st || ( eb + this.errorBox.height( ) ) > ( st + jQuery( window ).height( ) ) )
			jQuery( 'html, body' ).stop( ).animate( {
				scrollTop: eb - 24
			}, 200 );
		
		error.delay( 200 ).animate( {
			opacity: 0.4
		}, 400, 'easeInQuint' ).animate( {
			opacity: 1
		}, 400, 'easeOutQuint' ).delay( 200 ).animate( {
			opacity: 0.4
		}, 400, 'easeInQuint' ).animate( {
			opacity: 1
		}, 400, 'easeOutQuint' );
	},
	
	clearErrors: function( tag ) {
		var tagClass = tag != undefined ? '.ajaxeditError-' + tag : '*';
		jQuery( tagClass, this.errorBox ).remove( );
	},
	
	confirmDiscard: function( ) {
		
	},
	
	destroy: function( ) {
		
	}
} );

jQuery( document ).ready( function( ) {
	jQuery( '.ajaxeditForm' ).ajaxedit( );
} );
