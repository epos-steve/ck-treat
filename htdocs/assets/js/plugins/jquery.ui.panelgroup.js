/**
 * @class jQuery.ui.panelgroup
 */
jQuery.widget( 'ui.panelgroup', {
	/** @memberOf jQuery.ui.panelgroup */
	name: 'jQuery.ui.panelgroup',
	
	options: {},
	
	widgetId: null,
	
	_create: function( ) {
		var self = this,
			o = self.options,
			el = self.element;
		
		if( el.hasClass( 'ui-panelgroup' ) )
			return;
		
		el.addClass( 'ui-panelgroup ui-widget ui-helper-reset ui-helper-hidden-accessible' );
		
		var id = el.attr( 'id' );
		if( !id ) {
			var widgetCount = jQuery.panelgroup_widgetCount || 0;
			id = widgetCount;
			jQuery.panelgroup_widgetCount = ++widgetCount;
		}
		
		this.widgetId = 'panelgroup-' + id;
		
		el.children( ).each( function( ) {
			var panel = jQuery( this );
			var name = panel.attr( 'alt' ) || '...';
			
			var head = jQuery( '<h3 class="ui-panelgroup-header ui-helper-reset ui-corner-top ui-state-default ui-panelgroup-active ui-state-active" />' ).insertBefore( panel );
			head.html( '<span class="ui-icon ui-icon-triangle-1-s" /><span class="ui-text"></span>' );
			head.find( 'span.ui-text' ).text( name );
			head.attr( 'alt', name );
			
			panel.addClass( 'ui-panelgroup-content ui-helper-reset ui-widget-content ui-state-default ui-corner-bottom' );
			head.click( function( ) {
				self.toggle( head );
			} ).hover( function( ) {
				head.addClass( 'ui-state-hover' );
			}, function( ) {
				head.removeClass( 'ui-state-hover' );
			} );
			
			if( !panel.hasClass( 'ui-panelgroup-open' ) )
				self.toggle( head, false, true );
		} );
		
		this.restoreState( );
		
		el.removeClass( 'ui-helper-hidden-accessible' );
	},
	
	saveState: function( ) {
		var state = {};
		
		this.element.children( '.ui-panelgroup-header' ).each( function( ) {
			var name = jQuery( this ).attr( 'alt' );
			if( name && name != '...' )
				state[name] = jQuery( this ).hasClass( 'ui-panelgroup-active' );
		} );
		
		var serial = JSON.stringify( state );
		jQuery.cookie( 'panelgroup-state-' + this.widgetId, serial );
	},
	
	restoreState: function( ) {
		var self = this;
		var serial = jQuery.cookie( 'panelgroup-state-' + this.widgetId );
		
		if( serial ) {
			var state = JSON.parse( serial );
			jQuery.each( state, function( key, value ) {
				self.toggle( jQuery( '.ui-panelgroup-header[alt="' + key + '"]', self.element ), value, true );
			} );
		}
	},
	
	toggle: function( head, toggle, setup ) {
		var self = this;
		head = jQuery( head );
		
		if( toggle === undefined ) {
			toggle = !head.hasClass( 'ui-panelgroup-active' );
		}
		
		if( toggle ) {
			head.addClass( 'ui-panelgroup-active ui-state-active' ).removeClass( 'ui-corner-all' );
			head.find( 'span.ui-icon' ).removeClass( 'ui-icon-triangle-1-e' ).addClass( 'ui-icon-triangle-1-s' );
			jQuery.when( ( function( ) {
				if( setup )
					return head.next( ).show( );
				else {
					self._trigger( 'startshow' );
					self._startResize( );
					return head.next( ).show( 'blind', 'slow' );
				}
			} )( ) ).then( function( ) {
				self._stopResize( );
				if( !setup )
					self.saveState( );
				self._trigger( 'show' );
			} );
		} else {
			jQuery.when( ( function( ) {
				if( setup )
					return head.next( ).hide( );
				else {
					self._trigger( 'starthide' );
					self._startResize( );
					return head.next( ).hide( 'blind', 'slow' );
				}
			} )( ) ).then( function( ) {
				head.addClass( 'ui-corner-all' ).removeClass( 'ui-panelgroup-active ui-state-active' );
				head.find( 'span.ui-icon' ).removeClass( 'ui-icon-triangle-1-s' ).addClass( 'ui-icon-triangle-1-e' );
				self._stopResize( );
				if( !setup )
					self.saveState( );
				self._trigger( 'hide' );
			} );
		}
	},
	
	_resizeTimer: null,
	
	_startResize: function( ) {
		var self = this;
		
		if( this._resizeTimer ) {
			clearInterval( this._resizeTimer );
		}
		
		this._resizeTimer = setInterval( function( ) {
			self._trigger( 'resizing' );
		}, 80 );
	},
	
	_stopResize: function( ) {
		if( this._resizeTimer ) {
			clearInterval( this._resizeTimer );
			this._resizeTimer = null;
		}
		
		this._trigger( 'resize' );
	},
	
	destroy: function( ) {
		var el = this.element;
		
		el.removeClass( 'ui-panelgroup ui-widget ui-helper-reset' );
		el.children( '.ui-panelgroup-header' ).remove( );
		el.children( ).removeClass( 'ui-panelgroup-content ui-helper-reset ui-widget-content ui-corner-bottom' ).show( );
	}
} );