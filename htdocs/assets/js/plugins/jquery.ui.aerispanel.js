/**
 * @class jQuery.ui.aerispanel
 */
jQuery.widget( 'ui.aerispanel', {
	/** @memberOf jQuery.ui.aerispanel */
	name: 'jQuery.ui.aerispanel',

	options: {
		// display options
		rows: 4,
		cols: 4,
		scroll: false,
		autoAdjust: false,
		constrained: false,

		// callbacks
		reorder: null,
		click: null,
		dblclick: null,
		change: null,

		// data
		items: [],
		fields: {
			x: 'display_x',
			y: 'display_y',
			color: 'pos_color',
			id: 'id',
			label: 'name',
			width: 'width',
			height:'height'
		},

		// interaction
		selectable: true,
		orderable: true,
		resizable: false
	},

	table: null,
	scope: null,
	needSave: false,
	items: null,
	gridSize: 0,
	updatingOrder: false,
	needUpdateOrder: false,
	selectedIndex: null,
	eventsBound: false,
	saveWaffle: null,
	cellWidth: 0,
	cellHeight: 0,
	cellOuterWidth: 0,
	cellOuterHeight: 0,
	rowHeight: 68,
	currentPage: 1,

	_create: function( ) {
		var self = this,
			o = self.options,
			el = self.element;

		if( el.hasClass( 'aerispanel' ) )
			return;

		el.addClass( 'aerispanel' );
		el.data( 'aerispanel', this );

		this.items = o.items;

		if ( parseInt( o.rows ) > 0 ) {
			this.rows = o.rows;
		}
		if ( parseInt( o.cols ) > 0 ) {
			this.rows = o.cols;
		}

		this.scope = 'aerispanel-' + Math.floor( Math.random( ) * 100000 );

		this.table = jQuery( '<table id="' + self.scope + '-table" class="aerispanel-table" />' );
		this.table.appendTo( el );

		this._bindEvents( );


		this._renderTable( );
		this._renderButtons( );
	},

	_bindEvents: function( ) {
		if( this.eventsBound )
			return;

		var self = this;
		this.eventsBound = true;

		this.table.on( {
			'click.aerispanel': function( event, ui ) {
				self.selectedItem( this );
				if( self.options.click instanceof Function )
					self.options.click.apply( this, arguments );
			},
			'dblclick.aerispanel': function( event, ui ) {
				if( self.options.dblclick instanceof Function )
					self.options.dblclick.apply( this, arguments );
			}
		}, '.aerispanel-item' );

		this.element.on( {
			aerispanelchange: function( event, ui ) {
				if( self.options.change instanceof Function )
					self.options.change.apply( this, arguments );
			}
		} );

	},

	_renderTable: function( ) {
		var self = this,
			o = self.options,
			el = self.element;

		this.table.empty( );
		//this.table.replaceWith( jQuery( '<table class="aerispanel-table" />' ) );

		//console.log( "rows:" + o.rows + ", cols:" + o.cols );

		for( var y = 0; y < o.rows; y++ ) {
			var row = jQuery( '<tr id="' + self.scope + '-aeris-row-' + y + '"  class="aeris-row-' + y + '" />' ).appendTo( this.table );
			for( var x = 0; x < o.cols; x++ ) {
				var cellId = self.scope + '-aeris-cell-' + y + '-' + x;
				jQuery( '<td id="' + cellId + '"  class="aerispanel-cell aeris-cell-' + y + '-' + x + '" rowspan="1" colspan="1" />' )
					.data( 'y', y )
					.data( 'x', x )
					.appendTo( row );
			}
		}
		var td1 = jQuery( 'td', this.table ).first();
		this.cellWidth			= td1.width( );
		this.cellHeight			= td1.height( );
		this.cellOuterWidth		= td1.outerWidth( );
		this.cellOuterHeight	= td1.outerHeight( );

		// droppable accept none
		if( o.orderable ) {
			if ( !o.resizable ) {
				jQuery( '.aerispanel-cell', this.table ).droppable( {
					hoverClass: 'aerispanel-placeholder',
					scope: this.scope,
					drop: this._dropCell
				} );
			}
		}
	},

	setPage: function( page ) {
		var newPage = parseInt(page);
		/*
		 if ( this.currentPage == newPage ) {
		 return;
		 }
		 */

		this.currentPage = parseInt( page );
		this._renderButtons();
	},

	_renderButtons: function( ) {
		this.table.find( 'td' )
			.css( 'display',  '')
			.attr( 'rowspan', '1' )
			.attr( 'colspan', '1' )
			.height( this.cellHeight )
			.empty()
		;

		this.table.find( '.aerispanel-item' ).remove( );
		jQuery( 'td', this.table )
			.attr( 'rowspan', '1' )
			.attr( 'colspan', '1' )
			.css( 'display', 'table-cell' )
		;

		var self = this,
			o = self.options,
			el = self.element;

		var max = 0;
		var needSave = false;

		var addItem = function( item, index ) {
			var target = self._findCell( item[o.fields.y] || 0, item[o.fields.x] || 0, o.rows, o.cols );
			if( target == null )
				return;

			var y = target.data( 'y' ), x = target.data( 'x' );
			var width =  ( parseInt( item[o.fields.width]  ) >= 1 ) ? parseInt( item[o.fields.width]  ) : 1;
			var height = ( parseInt( item[o.fields.height] ) >= 1 ) ? parseInt( item[o.fields.height] ) : 1;

			if( item[o.fields.y] != y ) {
				item[o.fields.y] = target.data( 'y' );
				needSave = true;
			}

			if( item[o.fields.x] != x ) {
				item[o.fields.x] = target.data( 'x' );
				needSave = true;
			}

			var color = 'product-button-color-' + ( item[o.fields.color] > 0 ? item[o.fields.color] : 'default' );
			target.data( 'buttonId', parseInt( item[o.fields.id] ) );
			target.attr( 'x-button-id', parseInt( item[o.fields.id] ) );
			if ( width > 1 || height > 1 ) {
				// hide cells overlapped by new cell
				var targetCells =  self._getButtonCells( item[o.fields.x], item[o.fields.y], width, height );
				targetCells.data( 'buttonId', parseInt( item[o.fields.id] ) );
				targetCells.attr( 'x-button-id', parseInt( item[o.fields.id] ) );
				var newRow = jQuery( 'tr:eq(' + item[o.fields.y] + ')', self.table );
				var newCell = jQuery( 'td:eq(' + item[o.fields.x] + ')', newRow );

				var hiddenCells = targetCells.not( newCell );
				hiddenCells
					.attr( 'rowspan', '0' )
					.attr( 'colspan', '0' )
					.css( 'display', 'none' )
				;
				// resize new cell
				newCell
					.attr( 'rowspan', height )
					.attr( 'colspan', width  )
					.css( 'display', 'table-cell' )
				;

			}
			if ( o.resizable ) {
				var padding = 0;
				var newButtonWidth		=  target.width() - padding + 'px';
				var newButtonHeight		= target.height() - padding + 'px';
				var newSpanMaxWidth	= ( newButtonWidth - 	(1 * padding) ) + 'px';
				var newSpanMaxHeight	= ( newButtonHeight -	(1 * padding) ) + 'px';

			} else {
				var padding = 0;
				var newButtonWidth		=  '100%';
				var newButtonHeight		=  '100%';
				var newSpanMaxWidth		=  '100%';
				var newSpanMaxHeight	=  '100%';
			}
			var span = '<span style="display:table-cell;overflow:hidden; max-height:' + newSpanMaxHeight + '; max-width:	' + newSpanMaxWidth  + ';">' + item[o.fields.label] + '</span>';

			jQuery(
				'<div id="' + self.scope + '-' + item[o.fields.id] + '" class="aerispanel-item ' + color
					+ '" style="height:'	+ ( newButtonHeight ) + '; max-height:'	+ ( newButtonHeight ) + '; width:'	+ newButtonWidth + ';">\n'
					+ "<br />\n"
					+ span
					+ '\n</div></div>\n'
			)
				.data( 'index', index )
				.data( 'id',	parseInt( item[o.fields.id] ) )
				.data( 'color', item[o.fields.color]	)
				.appendTo( target );

			if ( o.resizable ) {

				var thisId = self.scope + '-' + item[o.fields.id];
				var thisButton = jQuery( '#' + thisId, self.table ), currentId;
				var container = self.table.children( 'tbody' );
				thisButton.data( 'item', item );
				thisButton.data( 'panel', self );
				thisButton.resizable({
					grid: [ 0, 0 ],
					handles: "n, e, s, w",
					helper: "ui-resizable-helper",
					start: function( event, ui ) {
						var currentButton = jQuery( this );
						var panel = currentButton.data( 'panel' );
						var width = panel.cellOuterWidth, height = panel.cellOuterHeight;
						currentButton.resizable( "option", "grid", [ width, height ] );
					},
					resize: function( event, ui ) {
					},
					stop: function( event, ui ) {
						var currentButton = jQuery( this );
						var d = currentButton.data();
						var item = d.item;
						var o = d.panel.options;
						var prevWidth	= parseInt( item[d.panel.options.fields.width] );
						var prevHeight	= parseInt( item[d.panel.options.fields.height] );
						var maxWidth	= parseInt( d.panel.options.cols );
						var maxHeight	= parseInt( d.panel.options.rows )
//						var newWidth	= Math.round( ui.size.width / d.panel.cellWidth );
						var newWidth	= Math.round( ui.size.width / d.panel.cellOuterWidth );
						if ( newWidth < 1 ) {
							newWidth = 1;
						}
//						var newHeight	= Math.round( ui.size.height / d.panel.cellHeight );
						var newHeight	= Math.round( ui.size.height / d.panel.cellOuterHeight );
						if ( newHeight < 1 ) {
							newHeight = 1 ;
						}
						if ( newWidth >= maxWidth || newHeight >= maxHeight ) {
							jQuery(this).resizable( "destroy" );
							d.panel._renderTable( );
							d.panel._renderButtons( );
							d.panel._resizePanel( );
							return false;
						}

						var deltaX = Math.round( ( ui.originalPosition.left - ui.position.left ) / d.panel.cellWidth   );
						var deltaY = Math.round( ( ui.originalPosition.top  - ui.position.top  ) / d.panel.cellHeight  );
						var prevX = parseInt( item[o.fields.x] ), prevY = parseInt( item[o.fields.y] );
						var newX = prevX - deltaX, newY = prevY - deltaY;
						var previousCell = ui.originalElement.parent('td');
						var newRow = jQuery( 'tr:eq(' + newY + ')', d.panel.table );
						var cell = jQuery( 'td:eq(' + newX + ')', newRow );
						var validPlacement = d.panel.canResizeButtonTo( currentButton, newX, newY, newWidth, newHeight );
						if ( !validPlacement ) {
							jQuery(this).resizable( "destroy" );
							d.panel._renderTable( );
							d.panel._renderButtons( );
							d.panel._resizePanel( );
							return false;
						}

						var currentElementHolder = ui.element.detach();

						// reset cells for previous position
						self._getButtonCells( prevX, prevY, prevWidth, prevHeight )
							.attr( 'rowspan', '1' )
							.attr( 'colspan', '1' )
							.css( 'display', 'table-cell' )
						;

						// hide cells overlapped by new cell
						self._getButtonCells( newX, newY, newWidth, newHeight )
							.attr( 'rowspan', '0' )
							.attr( 'colspan', '0' )
							.css( 'display', 'none' )
						;

						// resize new cell
						cell.attr( 'rowspan', newHeight )
							.attr( 'colspan', newWidth )
							.css( 'display', 'table-cell' )
						;
						item[o.fields.y] = cell.data( 'y' );
						item[o.fields.x] = cell.data( 'x' );
						item[o.fields.width] = newWidth;
						item[o.fields.height] = newHeight;

						currentElementHolder.appendTo( cell );
						currentElementHolder.width( cell.width() );
						currentElementHolder.height( cell.height() );

						self.saveOrder( );
						self.resize();
						self._renderButtons();
					}
				});

				var cursorAt	 = { top: Math.round( ( self.cellWidth  ) / 2 ), left: Math.round( ( self.cellHeight ) / 2 ) };
				thisButton.draggable({
					grid:				[ 0, 0 ],
					containment:		container,
					revert:				'invalid',
					revertDuration:		0,
					snap:				false,
					cursor:				"move",
//					cursorAt:			cursorAt,
					snapTolerance:		20,
					zIndex:				100,
					start: function( event, ui ) {
						var currentButton = jQuery( this );
						currentButton.draggable( "option", "grid", [ (self.cellWidth + 3 ), ( self.cellHeight + 3 ) ] );
						currentButton.data("deltaX",	0);
						currentButton.data("deltaY",	0);
						currentButton.css( 'cursor', 'move' );
						self.setDroppables( thisId );
					},
					drag: function( event, ui ) {
						var currentButton = jQuery( this );
						var x = ui.position.left / self.cellWidth;
						var y = ui.position.top / self.cellHeight;
						currentId = item[o.fields.id];
						var rval, elementPosition = currentButton.css( 'position' );
						if ( currentButton.revert == true ) {
							rval = 'true';
						} else {
							rval = 'false';
						}
						var buttonId = parseInt( currentButton.data( 'id' ) );
						var deltaX = Math.round( x );
						var deltaY = Math.round( y );

						if ( deltaX != currentButton.data( "deltaX" ) || deltaY != currentButton.data( "deltaY" )) {
							var width = parseInt( item[o.fields.width] ), height = parseInt( item[o.fields.height] );
							var prevX = parseInt( item[o.fields.x] ), prevY = parseInt( item[o.fields.y] );
							var newX = prevX + deltaX, newY = prevY + deltaY;
							var newCells = self._getButtonCells( newX, newY, width, height );
							var validPlacement = true;

							newCells.each( function() {
								var testCell = jQuery( this );
								var testCellButtonId = testCell.data( 'buttonId' );
								var testNotAvailable = !testCell.hasClass( 'available' );
								if ( testNotAvailable ) {
									if ( typeof testCellButtonId == 'undefined' ) {
										validPlacement = false;
									} else if ( testCellButtonId != buttonId ) {
										validPlacement = false;
									}
								}
							});

							if ( validPlacement ) {
								currentButton.draggable( "option", "cursor", "move" );
								currentButton.css( 'cursor', 'move' );
								currentButton.removeClass( 'invalid' );
								currentButton.attr('x-valid-target', 'true');
								currentButton.data('validTarget', 'true');
							} else {
								currentButton.draggable( "option", "cursor", "no-drop" );
//								currentButton.css( 'cursor', 'no-drop' );
								currentButton.addClass( 'invalid' );
								currentButton.removeAttr('x-valid-target');
								currentButton.removeData('validTarget');
							}
						}
						currentButton.data("deltaX",	deltaX );
						currentButton.data("deltaY",	deltaY );
					},
					stop: function( event, ui) {
						var currentButton = jQuery( this );
						currentButton.draggable( "option", "cursor", "auto" );
						var buttonId = parseInt( currentButton.data( 'id' ) );
						var width = parseInt( item[o.fields.width] ), height = parseInt( item[o.fields.height] );
						var deltaX = currentButton.data( 'deltaX' );
						var deltaY = currentButton.data( 'deltaY' );

						var prevX = parseInt( item[o.fields.x] ), prevY = parseInt( item[o.fields.y] );
						var newX = prevX + deltaX, newY = prevY + deltaY;
						if ( newX < 0 ) {
							deltaX -= newX;
							currentButton.data( 'deltaX', deltaX );
							newX = 0;
						}
						if ( newY < 0 ) {
							deltaY -= newY;
							currentButton.data( 'deltaY', deltaY );
							newY = 0;
						}


						var previousCell = currentButton.parent( 'td' );
						currentButton.removeClass( 'invalid' );
						//var newCell = self._findCell( newY, newX, o.rows, o.cols );

						var maxX = o.cols - width;
						var maxY = o.rows - height;
						if ( maxX < newX ) {
							deltaX -= ( newX - maxX );
							currentButton.data( 'deltaX', deltaX );
							newX = maxX;
						}
						if ( maxY < newY ) {
							deltaY -= ( newY - maxY );
							currentButton.data( 'deltaY', deltaY );
							newY = maxY;
						}

						var newCell = jQuery( '.aeris-cell-' + newY + '-' + newX, self.table );

						var newCells;
						var oldCells = self._getButtonCells( prevX, prevY, width, height );
						if ( width == 1 && height == 1 ) {
							//var newCells = self._getButtonCells( newX, newY, width, height );
							newCells = jQuery( '.aeris-cell-' + newY + '-' + newX, self.table );
						} else {
							newCells = self._getButtonCells( newX, newY, width, height );
						}

						var validPlacement = true;

						newCells.each( function() {
							var testCell = jQuery( this );
							var testCellButtonId = testCell.data( 'buttonId' );
							var testNotAvailable = !testCell.hasClass( 'available' );
							if ( testNotAvailable ) {
								if ( typeof testCellButtonId == 'undefined' ) {
									validPlacement = false;
								} else if ( testCellButtonId != buttonId ) {
									validPlacement = false;
								}
							}
						});

//						var validPlacement = ( newEmptyCells.length == newCells.length );


						currentButton.removeAttr('x-valid-target');
						currentButton.removeData('validTarget');

						if ( validPlacement ) {
//							console.log( 'valid placement' );
						} else {
//							console.log( 'invalid placement' );
//							console.log( rejectedCells );
							jQuery( '.available', self.table ).removeClass( 'available' );
							jQuery( '.invalid', self.table ).removeClass( 'invalid' );
							currentButton
								.css( 'top', 'auto' )
								.css( 'left', 'auto' )
								.css( 'max-height',	height + 'px !important' )
								.css( 'max-width',	width + 'px !important'  )
								.css( 'overflow',	'hidden')
								.css( 'display',	'')
							;
							return;
						}
						jQuery( '.available', self.table ).removeClass( 'available' );
						jQuery( '.invalid', self.table ).removeClass( 'invalid' );

						var currentButtonId = currentButton.data( 'id' );
						var button = currentButton.detach();
						width = parseInt( item[o.fields.width] ), height = parseInt( item[o.fields.height] );


						// reset cells for previous position
						oldCells
							.attr( 'rowspan', '1' )
							.attr( 'colspan', '1' )
							.css( 'display', '' )
							.removeData( 'buttonId' )
							.removeAttr( 'x-button-id' )
						;

						// hide cells overlapped by new cell
						var hiddenCells = self._getButtonCells( newX, newY, width, height ).not( newCell );
						hiddenCells
							.attr( 'rowspan', '0' )
							.attr( 'colspan', '0' )
							.css( 'display', 'none' )
							.data( 'buttonId', currentButtonId )
							.attr( 'x-button-id', currentButtonId )
						;
						newCell.data( 'buttonId', currentButtonId )
							.attr( 'x-button-id', currentButtonId );

						// resize new cell
						newCell.attr( 'rowspan', height )
							.attr( 'colspan', width )
							.css( 'display', '' )
						;
						item[o.fields.y] = newCell.data( 'y' );
						item[o.fields.x] = newCell.data( 'x' );
						item[o.fields.width] = width;
						item[o.fields.height] = height;

						newCell.css( 'left',	'auto' )
							.css( 'top',		'auto' )
						;

						button.width( newCell.width() );
						button.height( newCell.height() );
						button.css( 'left',		'auto' )
							.css( 'top',		'auto' )
						;
						button.css( 'max-height',	height + 'px !important' );
						button.css( 'max-width',	width + 'px !important'  );
						button.css( 'overflow',	'hidden');
						button.css( 'display',	'');

//						console.log( newCell );

						button.appendTo( newCell );
						button.css( 'display',	'none');
						button.width( newCell.width() );
						button.height( newCell.height() );
						button.css( 'left',		'auto' )
							.css( 'top',		'auto' )
						;
						button.css( 'max-height',	height + 'px !important' );
						button.css( 'max-width',	width + 'px !important'  );
						button.css( 'overflow',	'hidden');
						button.css( 'display',	'');
						self.saveOrder( );

						//document.title = 'new coordinates = (' + newx + ', ' + newy + ')';
						jQuery( '.aerispanel-cell', self.table  ).droppable({
							accept: 'none'
						});
						jQuery( '.available', self.table  ).removeClass( 'available' );
						jQuery( '.invalid', self.table  ).removeClass( 'invalid' );
						self.resize();
						currentButton.removeData( 'deltaX' );
						currentButton.removeData( 'deltaY' );
						self._renderButtons();
//						jQuery( 'td.aerispanel-cell:visible', self.table ).css( 'background-color', 'white' );
					}
				});

			}

			max = i > max ? i : max;
		};

		var items = this.items;

		for( var i in items ) {
			// skip items without offsets
			if( items[i][o.fields.y] == null || items[i][o.fields.x] == null ) {
				continue;
			}

			//console.log( items[i] );
			// if applicable filter items by page
			if( typeof items[i]['page'] == 'undefined' ) {
			} else {
				var itemPage = parseInt( items[i]['page'] );
				if ( itemPage != self.currentPage ) {
					continue;
				}
			}

			addItem( items[i], i );
		}

		for( var i in items ) {
			// now only items without offsets
			// this way new items don't push old items into an incorrect position (findCell)
			if( items[i][o.fields.y] != null && items[i][o.fields.x] != null ) {
				continue;
			}
			addItem( items[i], i );
			needSave = true;
		}

		//if( o.orderable && !o.resizable ) {
		if ( o.orderable ) {
			if ( !o.resizable ) {
				jQuery( '.aerispanel-item', this.table ).draggable( {
					containment: this.table,
					delay: 100,
					opacity: 0.6,
					revert: 'invalid',
					scope: this.scope,
					zIndex: 1000
				} );

				needSave |= this._updateConstraints( );
			}
		}

		if( needSave || this.needSave ) {
			this.needSave = false;
			this.saveOrder( );
		}
	},

	hasSpace: function( ) {
		"use strict";
		var cells = this.table.find( 'td' );
		for( var i = 0, il = cells.size(), cell = jQuery( cells[0] ); i < il; cell = jQuery( cells[++i] ) ) {
			if( this._isAvailable( cell ) )
				return true;
		}
		return false;
	},

	_isValidPlacement: function( buttonId, x, y, width, height ) {
		"use strict";
		var cells = this._getButtonCells( x, y, width, height );
		for( var i = 0, il = cells.length, cell = jQuery( cells[0] ); i < il; cell = jQuery( cells[++i] ) ) {
			var cellButtonId = cell.data( 'buttonId' );
			//var notAvailable = !cell.hasClass( 'available' );
			var notAvailable = !this._isAvailable( cell );
			if( notAvailable ) {
//				if( typeof cellButtonId === 'undefined' )
//					return false;
//				else if( typeof buttonId !== 'undefined' && cellButtonId !== buttonId )
//					return false;
				if( typeof cellButtonId === 'undefined' ||
					typeof buttonId === 'undefined' ||
					cellButtonId !== buttonId )
					return false;
			}
		}
		return true;
	},

	_isAvailable: function( cell ) {
		"use strict";
		if( cell.children().size( ) )
			return false;

		if( parseInt( cell.attr( 'colspan' ) ) < 1 || parseInt( cell.attr( 'rowspan' ) ) < 1 )
			return false;

		var w = cell.data( 'x' ), h = cell.data( 'y' );
		var test;

		for( var y = 0; y <= h; y++ ) {
			for( var x = 0; x <= w; x++ ) {
				test = jQuery( '.aeris-cell-' + y + '-' + x, this.table );
				if( test.children().size( ) > 0 &&
					( x + parseInt( test.attr( 'colspan' ) ) - 1 ) >= w &&
					( y + parseInt( test.attr( 'rowspan' ) ) - 1 ) >= h ) {
					return false;
				}
			}
		}
		return true;
	},

	_findCell: function( y, x, rows, cols, width, height, buttonId ) {
		width = width > 1 ? width : 1;
		height = height > 1 ? height : 1;
		var target = jQuery( '.aeris-cell-' + y + '-' + x, this.table );

		while( true ) {
			if( this._isValidPlacement( buttonId, x, y, width, height ) )
				//if( target.children().size() === 0 )
				break;

			if( ++x >= cols ) {
				x = 0;
				if( ++y >= rows ) {
					return null;
				}
			}

			target = jQuery( '.aeris-cell-' + y + '-' + x, this.table );
		}

		return target;
	},

	findCell: function( y, x, rows, cols, width, height, buttonId ) {
		rows = ( rows == undefined ) ? 1 : parseInt( rows );
		cols = ( cols == undefined ) ? 1 : parseInt( cols );
		width = width > 1 ? width : 1;
		height = height > 1 ? height : 1;
		return this._findCell( y, x, rows, cols, width, height, buttonId );
	},

	_getButtonCells: function( x, y, width, height ) {
		////console.log( 'x=' + x + ' y=' + y + ' width=' + width + ' height=' + height );
		var cells = this.table.find( 'td' )
				.filter( function( index ) {
					var foo = jQuery( this ).data();
					x = parseInt( x );
					y = parseInt( y );
					return ( foo.x >= x && foo.x < ( x + width ) && foo.y >= y && foo.y < ( y + height ));
				} )
			;
		////console.log( cells );
		return cells;
	},

	getButtonCells: function( x, y, width, height ) {
		//return this._getButtonCells( x, y, width, height );
		var self = this;
		var maxX, maxY, minX, minY;
		maxX = x + width;
		maxY = y + height;
		minX = x; minY = y;
		var cells = self.table.find( 'td' )
			.filter( function( index ) {
				var cell = jQuery( this );
				var cellX = parseInt( cell.data( 'x' ) );
				var cellY = parseInt( cell.data( 'y' ) );
				if ( cellX < minX || cellX > maxX || cellY < minY || cellY > maxY ) {
					return false;
				} else {
					return true;
				}
			});
		return cells;
	},

	canResizeButtonTo: function( buttonElement, newX, newY, width, height ) {
		var self = this;
		var prevCell = buttonElement.parent();
		var newCell = jQuery( '.aeris-cell-' + newY + '-' + newX, self.table );
		var prevX = prevCell.data('x');
		var prevY = prevCell.data('y');
		var buttonId = buttonElement.data('id');
		var elementId = buttonElement.attr('id');
		var success = true;

		var newCells, oldCells;
		oldCells = jQuery( 'td[x-button-id=' + buttonId + ']', self.table );
		if ( width == 1 && height == 1 ) {
			newCells = jQuery( '.aeris-cell-' + newY + '-' + newX, self.table );
		} else {
			newCells = self.getButtonCells( newX, newY, (width-1), (height-1) );
		}
		newCells.filter( function() {
			var t = jQuery( this );
			if ( t.attr('x-button-id') == undefined ) {
				return true;
			}
			if ( t.attr('x-button-id') == buttonId ) {
				return true;
			}
			success = false;
			return false;
		});
		return success;

	},

	/**
	 * check to see if button can be moved
	 * @param button jQuery
	 * @param newCells jQuery
	 * @return {Boolean}
	 */
	canMoveButtonTo: function( button, newCells ) {
		var validPlacement = true, buttonId = button.data( 'buttonId' );

		newCells.each( function() {
			if ( validPlacement ) {
				var testCell = jQuery( this );
				var testCellButtonId = testCell.data( 'buttonId' );
				var testNotAvailable = !testCell.hasClass( 'available' );
				if ( testNotAvailable ) {
					if ( typeof testCellButtonId == 'undefined' ) {
						validPlacement = false;
					} else if ( testCellButtonId != buttonId ) {
						validPlacement = false;
					}
				}
			}
		});

		return validPlacement;
	},

	/**
	 * @this DOMElement droppable target
	 * @param event
	 * @param ui
	 */
	_dropCell: function( event, ui ) {
		var self = jQuery( this ).closest( '.aerispanel' ).data( 'aerispanel' ),
			o = self.options,
			el = self.element;
		var cell = jQuery( this );
		var itemElem = jQuery( ui.draggable );
		var screenData = self.items;
		var item = screenData[itemElem.data( 'index' )];

		if( cell.children( ).not( item ).size( ) > 0 ) {
			var oldCell = jQuery( '.aeris-cell-' + item[o.fields.y] + '-' + item[o.fields.x], self.table );
			cell.children( ).not( item ).each( function( ) {
				var thisItem = screenData[jQuery( this ).data( 'index' )];
				thisItem[o.fields.y] = item[o.fields.y];
				thisItem[o.fields.x] = item[o.fields.x];
			} ).appendTo( oldCell );
		}

		itemElem.css( {
			left: 'auto',
			top: 'auto'
		} );

		item[o.fields.y] = cell.data( 'y' );
		item[o.fields.x] = cell.data( 'x' );

		itemElem.appendTo( cell );
		self.saveOrder( );
		self._updateConstraints( );
	},

	resize: function( ) {
		this._resizePanel( );
		return this.element;
	},

	_resizePanel: function( ) {
		var self = this,
			o = self.options,
			el = self.element;
		var screenData = self.items;

		var containers = jQuery( '.aerispanel-cell', this.table );
		var testCell = containers.first( );

		var testRow = jQuery( 'tr', this.table ).first( );
		var paddingHeight = testRow.outerHeight( true ) - testRow.height( );
		if( !o.autoAdjust ) {
			var tablePadding = this.table.outerHeight( true ) - this.table.height( );
			this.rowHeight = ( this.table.parent( ).height( ) - tablePadding ) / this.options.rows - paddingHeight;
		}
		this.table.find( 'tr' ).height( this.rowHeight );

		var testItem = jQuery( '.aerispanel-item', this.table ).last( );
		paddingHeight = ( testItem.outerHeight( true ) - testItem.height( ) ) + ( testCell.outerHeight( true ) - testCell.height( ) );

	},

	saveOrder: function( ) {
		var self = this,
			o = self.options,
			el = self.element;

		if( this.updatingOrder ) {
			this.needUpdateOrder = true;
			return;
		}

		this.saveWaffle = jQuery.waffle.pop( {
			autoClose: false,
			clickClose: false,
			content: '<img src="/assets/images/ajax-loader-redmond.gif" />Saving...',
			weight: -100
		} );

		this.updatingOrder = true;
		this.needUpdateOrder = false;

		var order = [];
		var items = this.items;

		for( var i in items ) {
			order[order.length] = {
				id:		items[i][o.fields.id],
				y:		items[i][o.fields.y],
				x:		items[i][o.fields.x],
				width:	items[i][o.fields.width],
				height:	items[i][o.fields.height]
			};
		}

		if( typeof o.reorder == 'function' ) {
			//o.reorder.call( el, order, function( ) { self._saveOrderComplete( ); } );
			jQuery.when( o.reorder.call( el, order ) ).always( function( ) { self._saveOrderComplete( ); } );
		} else {
			this._saveOrderComplete( );
		}

		return this.element;
	},

	show: function( data ) {

		var drows = parseInt( data.rows ),
			dcols = parseInt( data.cols );

		var needRender = this.options.cols != dcols
				|| this.options.rows != drows
			;
		if ( !needRender ) {
			var trs = jQuery( 'tr', this.table );
			var rowCount = trs.length;
			var colCount = trs.children('td').last().index() + 1;
			needRender = (
				rowCount != parseInt( this.options.rows ) || colCount != parseInt( this.options.cols )
				);
		}

		this.options.cols = dcols;
		this.options.rows = drows;
		this.options.items = this.items = data.items;
		if( needRender ) {
			this._renderTable( );

		}
		this._renderButtons( );
		this._resizePanel( );
		return this.element;
	},

	selected: function( sel ) {
		if( typeof sel != 'undefined' ) {
			if( typeof this.items[sel] != 'undefined' ) {
				this.selectedItem( this.items[sel] );
			}
			return this.element;
		}

		return this.selectedIndex;
	},

	selectedItem: function( item ) {
		if( typeof item != 'undefined' ) {
			if( !this.options.selectable || jQuery( item ).is( '.selected' ) )
				return;

			this.table.find( '.selected' ).removeClass( 'selected' );
			this.selectedIndex = null;

			if( jQuery( item ).length ) {
				jQuery( item ).addClass( 'selected' );
				this.selectedIndex = jQuery( item ).data( 'index' );
			}

			this._trigger( 'change', null, { index: this.selectedIndex } );

			return this.element;
		}

		return this.items[this.selectedIndex];
	},

	_saveOrderComplete: function( ) {
		if( this.saveWaffle != null ) {
			jQuery.waffle.burn( this.saveWaffle );
			this.saveWaffle = null;
		}

		this.updatingOrder = false;
		if( this.needUpdateOrder ) {
			this.saveOrder( );
		}
	},

	_updateConstraints: function( ) {
		if( !this.options.constrained )
			return;

		var self = this;

		var stateFilled = true;
		var needUpdate = false;
		var items = [];
		for( var y = 0; y < this.options.rows; y++ ) {
			for( var x = 0; x < this.options.cols; x++ ) {
				var itm = jQuery( '.aeris-cell-' + y + '-' + x, this.table ).children( );

				if( stateFilled == false && itm.length )
					needUpdate = true;

				if( stateFilled == true && !itm.length )
					stateFilled = false;

				itm.each( function( ) {
					items[items.length] = this;
				} );
			}
		}

		if( needUpdate ) {
			var y = 0, x = 0;
			jQuery.each( items, function( ) {
				if( y >= self.options.rows ) {
					return;
				}

				jQuery( '.aeris-cell-' + y + '-' + x, self.table ).append( this );
				var index = jQuery( this ).data( 'index' );
				self.items[index][self.options.fields.x] = x;
				self.items[index][self.options.fields.y] = y;

				x++;
				if( x >= self.options.cols ) {
					x = 0;
					y++;
				}
			} );
		}

		return needUpdate;
	},

	/**
	 * Find all cells that are valid targets for the draggable object and mark them
	 * @param itemId
	 */
	setDroppables: function( itemId ) {
		var self = this,
			item = jQuery( '#' + itemId, this.table ),
		// cell is the button that is moving
			cell = item.parent(),
			colspan = parseInt( cell.attr( 'colspan' ) ),
			rowspan = parseInt( cell.attr( 'rowspan' ) )
			;
		var x = cell.data( 'x' );
		var y = cell.data( 'y' );

		jQuery( '.available', self.table ).removeClass( 'available' )
			.css( 'background-color', 'white' )
		;
		jQuery( '.invalid', self.table  ).removeClass( 'invalid' );

		var availableCells = jQuery( '.aerispanel-cell:empty:visible', self.table )
				.filter( function() {
					var tmpCell = jQuery( this ),
						tmpX = colspan + tmpCell.data('x') - 1,
						tmpY = rowspan + tmpCell.data('y') - 1
						;
					if ( tmpX > self.options.cols ) {
						//return false;
					}
					if ( tmpY > self.options.rows ) {
						//return false;
					}
					return true;
				})
			;
		availableCells.addClass( 'available' );
		cell.addClass( 'available' );

		// go through all other buttons, check spacing if not 1x1
		var findButtonCells = jQuery( '.aerispanel-cell:not(:empty):visible', self.table );
		var buttons = findButtonCells.not( cell ).not(':empty').each( function() {
			// bcell is a stationary button
			var bcell = jQuery( this );
			var bcolspan = parseInt( bcell.attr( 'colspan' ) ),
				browspan = parseInt( bcell.attr( 'rowspan' ) );
			var skipCheck = ( rowspan < 2 && colspan < 2 );
			if ( rowspan < 2 && colspan < 2 ) {
				return;
			}

			var bx, by, bWidth, bHeight, movingButtonWidth, movingButtonHeight;
			bx = bcell.data( 'x' );
			by = bcell.data( 'y' );
			movingButtonWidth	= parseInt( cell.attr( 'colspan' ) );
			movingButtonHeight	= parseInt( cell.attr( 'rowspan' ) );
			bWidth				= parseInt( bcell.attr( 'colspan' ) );
			bHeight				= parseInt( bcell.attr( 'rowspan' ) );

			// ref1 is a reference point at the bottom right corner of this button for caclulating ref2
			// ref2 is a reference point at the top left corner of the area to be made invalid
			var ref1x, ref1y, ref2x, ref2y, refDelta;
			var isOneByOne = ( bWidth == 1 && bHeight == 1 );
			var clipCorner = true;

			refDelta = -1;
			ref1x = bx + bWidth + refDelta;
			ref1y = by + bHeight + refDelta;

			var deltaX, deltaY, delta;
			delta = 0;
			deltaX = delta + ( movingButtonWidth  > bWidth  ) ? movingButtonWidth  : bWidth ;
			deltaY = delta + ( movingButtonHeight > bHeight ) ? movingButtonHeight : bHeight;

			ref2x = ref1x - deltaX;
			ref2y = ref1y - deltaY;

			//correction for height
			ref2y++;
			deltaY--;

			if ( bWidth == 1 ) {
				ref2x++;
				ref2y++;
				deltaX--;
				deltaY--;
			}

			// do not let the button position outside of the grid
			if ( ref2x < 0 ) {
				deltaX += ref2x;
				ref2x = 0;
				clipCorner = false;
			}
			if ( ref2y < 0 ) {
				deltaY += ref2y;
				ref2y = 0;
				clipCorner = false;
			}

			var occupiedCells, tmp;
			tmp = self.getButtonCells( ref2x, ref2y, deltaX, deltaY );
			occupiedCells = tmp.each( function( index, value) {
				var tmpCell = jQuery( this );
				var tmpCell2= jQuery( value );
				if ( tmpCell.children().length < 1 ) {
					jQuery( value ).removeClass( 'available' ).addClass( 'invalid' );

				}

			} );
			// make sure current button cell is available
			cell.addClass( 'available' );
			var ref1Id = '.aeris-cell-' + ( ref1y - 1 ) + '-' + ( ref1x - 1);
			var ref1 = jQuery( ref1Id, self.table );
			var ref2 = jQuery( ref2Id, self.table );
			var ref2Id = '.aeris-cell-' + ( ref2y - 1 ) + '-' + ( ref2x - 1);
//			ref2 should be available unless we had to move it
			if ( clipCorner ) {
				ref2.addClass( 'available' );
			}


			jQuery( 'td:empty:visible' ).addClass( 'available' ).removeClass( 'invalid' );

			jQuery( '.ref1', self.table ).removeClass( 'ref1' );
			jQuery( '.ref2', self.table ).removeClass( 'ref2' );
			ref2.addClass( 'ref2' );
			jQuery( 'td:visible:not(.available)', self.table ).addClass( 'invalid' );
			var foo = 'bar';

		});

	},

	reset: function( ) {
		var self = this,
			o = self.options;
		o.rows = 4;
		o.cols = 4;
		o.items = [];
		self.table.empty();
		self.currentPage = 1;
		self.items = o.items;
		self.needSave = false;
		self.updatingOrder = false;
		self.needUpdateOrder = false;
		self.eventsBound = false;

		self._renderTable( );
		self._renderButtons( );
		self._resizePanel( );
	},

	debugTable: function( ) {

		var self = this,
			o = self.options;

		var out = '------------------\n';
		self.table.find( 'tr' ).each( function( index, element ) {
			var row = jQuery( this );
			var rows, rowStr = "";
			rows = row.find( 'td' ).each( function( ) {
				var cell = jQuery( this );
				var rowspan = cell.attr( 'rowspan' ),
					colspan = cell.attr( 'colspan' ),
					width	= cell.width(),
					height	= cell.height(),
					visible = ( cell.css ( 'display' ) != 'none' ) ? true : false
					;
				//var cellStr = colspan + 'x' + rowspan;
				var cellStr = width + 'x' + height;
				if (visible) {
					cellStr = '' + cellStr + '';
				} else {
					cellStr = '' + cellStr + '';
				}
				out += cellStr + '\t';
			});
			out += '\n';
		});
		out += '------------------\n';
		return out;

	},

	destroy: function( ) {

	}
} );
