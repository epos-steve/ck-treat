(function($){
	var Keyboard = function(){
		this._mainDivId            = 'ui-keyboard-div',
			this._contentClasses   = 'ui-widget ' +
									 'ui-widget-content ' +
								     'ui-corner-all',
			this._kbMode           = 'az',
			this._shiftActive      = false,
			this.kbDiv             = $('<div id="'+this._mainDivId+'" class="'+this._contentClasses+'"></div>')
										.css({
											'position': 'absolute',
											'text-align': 'center',
											'z-index': '2000',
											'padding': '10px'
											});
		   this._specialShifts = {
		   			1: '!',
		   			2: '@',
		   			3: '#',
		   			4: '$',
		   			5: '%',
		   			6: '^',
		   			7: '&',
		   			8: '*',
		   			9: '(',
		   			0: ')',
					';': ':'}
	};

	$.extend( Keyboard.prototype, {
		_attachKeyboard: function(target, options){
			var input = $(target);
			input
				.focus( function(){
					$.keyboard._showKeyboard(target);
				} )
				.click( function(){
					$.keyboard._showKeyboard(target);
				} )
				.attr('autocomplete', 'off');
		},
		_hideKeyboard: function(input){
			this.kbDiv.hide();
		},
		_showKeyboard: function(input){
			var input  = $(input),
				offset = input.offset();

			$.keyboard.kbDiv.hide().css({'top': offset.top - this.kbDiv.height() - 25+ 'px', 'left': (offset.left - 140) + 'px'}).show();
			this.target = input;
		},
		_loadKeys: function(){
			$.keyboard.kbDiv.html('');
			$.keyboard.kbDiv.append($.keyboard._generateHTML());
		},
		_renderMap: function(map){
			var obj = this;
			var insert = $('<div></div>');
			var top_row = $('<div align="right"></div>');
			var html = '';

			var del_button = $('<button>&lt;=</button>')
				.attr('class', 'kb-button-special ui-state-default ui-corner-all')
				.css({
					margin: '2px'
				})
				.attr('value', 'del')
				.hover(
					function(){
						$(this).addClass('ui-state-hover');
					},
					function(){
						$(this).removeClass('ui-state-hover');
					}
				)
				.click( function(){ $.keyboard._updateVal( $(this) ) } );

			var clr_button = $('<button>Clr</button>')
				.css({
					margin: '2px'
				})
				.attr('class', 'kb-button-special ui-state-default ui-corner-all')
				.attr('value', 'clr')
				.hover(
					function(){
						$(this).addClass('ui-state-hover');
					},
					function(){
						$(this).removeClass('ui-state-hover');
					}
				)
				.click( function(){ $.keyboard._updateVal( $(this) ) } );


			var shft_button = $('<button id="shft_button">Shift</button>')
				.css({
					margin: '2px'
				})
				.attr('class', 'kb-button-special ui-state-default ui-corner-all')
				.css({
					'float': 'left',
					'width': '84px'
				}).
				click( function(){ obj._toggleShift(); } );

			var caps_button = $('<button id="caps_button">Caps</button>')
				.css({
					margin: '2px'
				})
				.attr('class', 'kb-button-special ui-state-default ui-corner-all')
				.css({
					'float': 'left',
					'width': '84px'
				}).
				toggle(
					function(){
						$(this).addClass('ui-state-active');
						obj._toUpper();
					},
					function(){
						$(this).removeClass('ui-state-active');
						obj._toLower();
					}
				);



			top_row.append(clr_button);
			top_row.append(del_button);
			top_row.append(caps_button);
			top_row.append(shft_button);
			insert.append(top_row);


			var keys = $('<div style="clear: both;"></div>');
			$.each(map, function(index, row){
				$.each(row, function(index, k){
					k = (obj._shiftActive) ? k : k.toLowerCase();

					var button = $('<button>'+k+'</button>')
						.attr('class', 'kb-button ui-state-default ui-corner-all')
						.attr('value', k)
						.css({
							width: '40px',
							margin: '2px'
						})
						.hover(
							function(){
								$(this).addClass('ui-state-hover');
							},
							function(){
								$(this).removeClass('ui-state-hover');
							})
						.click( function(){ $.keyboard._updateVal( $(this) ) } );

					keys.append(button);
				});
				keys.append('<br />');
			} );
			insert.append( keys );



			return insert;
		},
		_toggleShift: function(){
			if(this._shiftActive){
				$('#shft_button').removeClass('ui-state-active');
				this._toLower();
				this._shiftActive = false;
			}
			else{
				this._shiftActive = true;
				$('#shft_button').addClass('ui-state-active');
				this._toUpper();
			}
		},
		_toUpper: function(){
			var obj = this;
			$('button.kb-button').each( function(){ 
				if(obj._shiftActive && obj._specialShifts[$(this).html()] != undefined ){
					$(this).attr('oldval', $(this).html());
					$(this).html( obj._specialShifts[$(this).html()] );
				}
				else
					$(this).html( $(this).html().toUpperCase() );
			} );
		},
		_toLower: function(){
			var obj = this;
			$('button.kb-button').each( function(){ 
				if(obj._shiftActive && obj._specialShifts[$(this).attr('oldval')] != undefined ){
					$(this).html( $(this).attr('oldval'));
				}
				else
					$(this).html( $(this).html().toLowerCase() ) 
			} );
		},
		_updateVal: function(button){
			var disp = button.html();
			var val  = button.attr('value');

			if( val == 'clr' )
				this.target.val('');
			else if( val == 'del' )
				this.target.val(this.target.val().substr(0, this.target.val().length - 1));
			else
				this.target.val( this.target.val() + disp );

			if(this._shiftActive)
				this._toggleShift();

			this.target.trigger( $.Event('keydown') );
			this.target.focus();
		},
		_checkExternalClick: function(event){
			var target = $(event.target);

			if(target[0].id != $.keyboard._mainDivId 
				&& target.parents('.ac_results').length == 0
				&& target.parents('#'+$.keyboard._mainDivId).length == 0)
					$.keyboard._hideKeyboard();

		},
		_generateHTML: function(){
			var map = this._getMap();
			return this._renderMap(map);
		},
		_getMap: function(){
			if(this._kbMode == 'az'){
				var map = [['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'],
						   ['Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P'],
						   ['A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', '@'],
						   ['Z', 'X', 'C', 'V', 'B', 'N', 'M', ',', '.']];
			}
			return map;
		}
	} );
	
	$.fn.keyboard = function( options ){
		if(!$.keyboard.initialized){
			$.keyboard._loadKeys();
			$(document)
				.mousedown($.keyboard._checkExternalClick)
				.find('body')
				.append($.keyboard.kbDiv);

			$.keyboard.kbDiv.hide();

			$.keyboard.initialized = true;
		}

		return this.each( function(){
			$.keyboard._attachKeyboard(this, options);
		} );
	};
	
	$.keyboard = new Keyboard();
	$.keyboard.initialized = false;

})(jQuery);
