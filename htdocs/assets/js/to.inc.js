//Ready
var global_to;
var nav_to;
var images = [];
var config = [];
var playClickSound = function(){
	$('#click_sound').each( function(){ this.Play(); } );
};
$( function(){
	//Sound
	/*
	$(document.body).prepend( $('<embed />')
		.css({height: '10px', visibility: 'hidden'})
		.attr({
			id: 'click_sound',
			autostart: false,
			controls: true,
			src: 'sound/round_pop_click.wav'
		})
	);
	*/
	/////
	//Link Helpers  [For non-acurate screen fingers]
	$('img:first').parent().attr('id', 'img_banner');
	$('a').not('#img_banner').each(function(){
		var wrapper = $('<span></span>');
		wrapper
			.css({padding: '5px'})
			.addClass('c_wrapper_sp')
			.addClass('ui-corner-all');

		var href = this.href;
		var ev   = this.onclick;

		$(this).wrap( wrapper );
		var wrapper = $(this).parent();
		//wrapper.mousedown( playClickSound );

		if(href.length > 0){
			this.href = 'javascript:void(0)';
			wrapper.click( function(){ setTimeout(function(){ window.location.href = href; }, 300); } );
		}
		if( ev != null ){
			this.onclick = null;
			wrapper.click( ev );
		}

	});

	//Click Confirmation
	$(document.body).click( function(event){
		var ev = event || window.event;
		var target = $(ev.target);

		var ele = null;

		if(target.hasClass('c_wrapper_sp')){
			ele = target;
		}
		else if(target.parents('.c_wrapper_sp').length > 0){
			ele = target.parents('.c_wrapper_sp');
		}


		if(ele != null){
			ele.effect('highlight', {}, 1000);
			setTimeout( function(){ ele.fadeIn(); } , 500 );
		}

	} );

	/// DEBUG TIMEOUT BUTTON
	$(document.body).append($('<button></button>')
		.html('Exit')
		.css({
			position: 'fixed',
			bottom: '0px',
			left: '0px'
		})
		.addClass('ui-state-default')
		.addClass('ui-corner-all')
		.click( function(){ throwTimeout(); })
	);
	////////////////////////

	//Wrap Body Contents
	$(document.body)
		.css('overflow', 'hidden')
		.wrapInner('<div id="body_wrapper"></div>');

	//Clock 
	new Clock();
	///////
	
	///////// UP DOWN BUTTONS /////////////
	var up_button = $('<button id="c_top_navbut" class="c_navbutton">&#9650;</button>');
	var down_button = $('<button id="c_bot_navbut" class="c_navbutton">&#9660;</button>');
	$(document.body).append( up_button ).append( down_button );
	$('.c_navbutton')
		.css({
			position: 'fixed',
			padding: '10px',
			right: '0px'
		})
		.addClass('ui-state-default')
		.addClass('ui-corner-all')
		.mousedown( function(){ 
			$(this).addClass('ui-state-active'); 
			var obj = this;
			$(document.body).mouseup( function(){ $(obj).removeClass('ui-state-active'); clearInterval( nav_to ); } );
			$(document.body).mouseout( function(){ $(obj).removeClass('ui-state-active'); clearInterval( nav_to ); } );
		} )
	
	$('#c_top_navbut')
		.css({
			top: '0px'
		})
		.mousedown( function(){ scrollPage( -1 ); } );
	$('#c_bot_navbut')
		.css({
			bottom: '0px'
		})
		.mousedown( function(){ scrollPage( 1 ); } );

	var bh = $(document.body).height();
	var wh = $(window).height();
	if( !(bh > wh) )
		$('.c_navbutton').hide();

	///////////////////////////////////////

	//Grab Configs, then continue preparing document
	$.get('ajax.php', {m: 'loadSettings'}, function(data){
		config = data;

		//Timeout Handlers
		global_to = setTimeout(throwTimeout, config.init_timeout * 1000);
		$(document.body).mousemove( function(){ clearTimeout( global_to ); global_to = setTimeout( throwTimeout, config.mouse_timeout * 1000 ); } );
		
		//Image Preloader
		preloadImages(); 

		//Load Login UI, and continue upon fetching
		$.get('ajax.php', {m: 'loadLoginBox'}, function(data){
			//Append UI
			$(document.body).append(data);

			$('#to_user').autocomplete( 'ajax.php', {extraParams: {m: 'loadEmails'}} );

			//On-Screen Keyboard
			$(':input').not('button').keyboard();

			//Dialog
			$('#login_dialog').dialog( {
				width: 420,
				show: 'slide',
				autoOpen: false,
				draggable: false,
				resizable: false
			} );

			//Big Dialog Buttons
			$('.big_button')
				.css('padding', '10px 10px 10px 50px')
				.css('font-size', '14pt')
				.css('background', '#132549')
				.css('border', '1px solid #fff')
				.css('color', '#fff')
				.css('cursor', 'pointer')
				.mouseenter( function(){ $(this).css('background-color', '#132549').css('border', '1px solid #d4c159'); } )
				.mouseleave( function(){ $(this).css('background-color', '#132549').css('border', '1px solid #fff'); } );
			
			//Login Button
			$('#d_login_button').click( function(){
				$('#dialog_menu').hide('slow');
				$('#dialog_form').show('slow');
				$('#login_dialog').dialog('option', 'buttons', {
					'Login': function(){ $('#login_form').submit(); },
					'Cancel': function(){ $("#dialog_menu").show('slow'); $('#dialog_form').hide('slow'); $(this).dialog('option', 'buttons', {}); }
				} );
			} );

			//Browse Button
			$('#d_browse_button').click( function(){
				window.location.href = 'intro.php?bid=' + getQueryString('bid');
			} );

			//BackDrop
			$('#back_drop')
				.css({
					'top': '0px',
					'left': '0px',
					'width': $(window).width() + 'px',
					'height': getScreenHeight()+'px',
					'background': '#000',
					'position': 'absolute'
					});
		}, 'text');
	}, 'json');

} );

//Functions
var scrollPage = function(dir){
	nav_to = setInterval( function(){ continueScroll(dir); }, 100);
};

var continueScroll = function(dir){
	var cur = $(window).scrollTop();
	var speed = 20;

	window.scrollTo(0, cur + (speed*dir));
}

var preloadImages = function(){
	$.get('ajax.php', {m: 'getImages'}, function(data){
		var path = data.path;
		for(i = 0; i < data.images.length; i++){
			var i_src             = path + '/' + data.images[i];
			var img               = document.createElement('img');
			img.src               = i_src;
			images[images.length] = img;
		}
	}, 'json');
}

var throwTimeout = function(){
	//Hide Body
	$('#body_wrapper').hide();

	$(document.body).css( {overflow: 'hidden'} );

	//UnBind Body.move
	$(document.body).unbind();

	$.get('ajax.php', {m: 'sessionTimeout'});

	//Show Box
	$('#back_drop')
		.css({
			width: $(window).width() + 'px',
			height: $(window).height() + 'px'
		});
		//.show('slow');
	$('#login_dialog').dialog('open');

	//Bind Inputs to delay hide
	var resetTO = function(){ clearTimeout( global_to ); global_to = setTimeout(loginTimeout, config.login_timeout * 1000); };
	$(':input', $('#login_dialog')).focus( resetTO ).keydown( resetTO );

	//Timeout Handlers for Box
	global_to = setTimeout( loginTimeout, config.login_timeout * 1000);
	$(document.body).mousemove( function(){ $('.ui-dialog').fadeIn(); resetTO.call(this); } );

	//Cycle Images
	new ImageCycler();

	//Kill Open Windows
	for( i in window ){
		if( i == 'popwin' || i == 'popwin2' ){
			window[i].close();
		}
	}
};

var Clock = function(){
	this.init();
	this.create();
	this.update();
};
Clock.prototype = {
	init: function(){
		var d = new Date();
		this.hour = d.getHours();
		this.min  = d.getMinutes();
		this.sec  = d.getSeconds();

		var obj = this;
		setInterval( function(){ obj.timer(); }, 1000 );
	},
	timer: function(){
		this.update();
		this.inc();
	},
	inc: function(){
		this.sec++
		if( this.sec >= 60 ){
			this.min++;
			this.sec = 0;

			if(this.min >= 60){
				this.hour++;
				this.min = 0;
			}
		}
	},
	update: function(){
		var h = this.hour;
		var m = this.min;
		if( h >= 12){
			this.ampm = 'PM';
			if( h > 12)
				h -= 12;
		}
		else{
			this.ampm = 'AM';
		}

		h = this.fix( h );
		m = this.fix( m );

		var t = h;
		if( this.sec % 2 == 0 )
			t += ':';
		else
			t += ' ';

		t += m;
		$('#c_time').html( t );
		$('#c_ampm').html( this.ampm );

		if( this.container.is(':hidden') )
			this.container.show();
	},
	fix: function(i){
		return (i < 10) ? '0'+ String(i) : String(i);
	},
	create: function(){
		this.container = $('<div></div>')
			.addClass('ui-corner-all')
			.css({
				'font-family': 'arial',
				background: '#000000',
				display: 'none',
				color: '#ffffff',
				position: 'fixed',
				bottom: '2px',
				padding: '5px',
				'z-index': '3000',
				'font-size': '8pt',
				right: '50px',
				opacity: .8,
				'text-shadow': '2px 2px #000000'
			});
		this.timeSp = $('<span id="c_time"></span>')
			.html('');
		this.ampmSp = $('<span id="c_ampm"></span>')
			.html('');
		this.container.append(this.timeSp).append( '&nbsp;' ).append(this.ampmSp);
		$(document.body).append( this.container );
	}
};

var ImageCycler = function(){
	this.cur     = 0;
	this.cur_img = null;
	this.images  = images;

	this.init();
};
ImageCycler.prototype = {
	init: function(){
		this.container = $('<div></div>');
		this.container
			.css({
				position: 'absolute',
				top: '0px',
				left: '0px'
			});

		$(document.body).append( this.container );

		this.loadImage( this.images[0] );
		var obj = this;
		setInterval( function(){ obj.loadNext(); }, config.image_cycle * 1000 );
	},
	loadNext: function(){
		this.loadImage(this.images[this.cur]);
	},
	loadImage: function( img ){
		if(this.cur_img != null)
			$(this.cur_img).fadeOut('fast').remove();
		
		$(img)
			.css('position', 'absolute')
			.css('top', '0px')
			.css('left', '0px')
			.css('display', 'none');

		this.container.append( img );
		$(img).fadeIn('fast');
		this.cur_img = $(img);
		this.cur++;
		if(this.cur >= this.images.length)
			this.cur = 0;
	}
};

var loginTimeout = function(){
	$('.ui-dialog').fadeOut();
	$('#'+$.keyboard._mainDivId).fadeOut();
	$('#to_user').val('');
	$('#to_pass').val('');
}

var getScreenHeight = function(){
	var w_h = $(window).height();
	var b_h = $(document.body).height();

	return ((w_h > b_h) ? w_h : b_h);
};

function isDefined(variable){
    return (typeof(window[variable]) == "undefined")?  false: true;
}

function getQueryString(key, default_)
{
  if (default_==null) default_="";
  key = key.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regex = new RegExp("[\\?&]"+key+"=([^&#]*)");
  var qs = regex.exec(window.location.href);
  if(qs == null)
    return default_;
  else
    return qs[1];
}
