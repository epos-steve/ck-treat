document.write( '<link rel="stylesheet" href="/assets/css/preloading/preLoadingMessage.css" />'
	+ '<div id="loading"><div id="overlay"></div><div id="message">Loading... Please Wait<br/><img src="/assets/images/preload/loading.gif" height="22" width="126"></div></div>'
);

// Created by: Simon Willison | http://simon.incutio.com/
function addLoadEvent(func) {
  var oldonload = window.onload;
  if (typeof window.onload != 'function') {
    window.onload = func;
  } else {
    window.onload = function() {
      if (oldonload) {
        oldonload();
      }
      func();
    }
  }
}

if ( typeof jQuery != 'undefined' ) {
	jQuery(window).load(function(){
//		alert( "jQuery window.load");
		jQuery("#loading").hide();
	});
}
else if ( Event && Event.observe ) {
	Event.observe(window,'load', function(){
//		alert( "Event.observe window.load");
		document.getElementById("loading").style.display="none";
	});
}
else {
	addLoadEvent(function() {
//		alert( "addLoadEvent window.load");
		document.getElementById("loading").style.display="none";
	});
}
