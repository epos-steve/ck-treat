

/*  Portions headed with an [EEF] were added to the file from the Essential Elements 
 *  Framework and were just added for ease of use.  These additions were written by Tim
 *  Frazier, and for use by Essential Elements only.
 *--------------------------------------------------------------------------*/



/////////////////////////////////
//////  Drag and Drop Sortable Table [EEF]
/////////////////////////////////
var dndTable = function(id, options){
	this.id = id;
	this.table = $(id);
	this.cellsAccross = this.table.rows[0].cells.length;
	this.options = options;
	this.active = false;

	this.bindEvents();
}
dndTable.prototype = {
	bindEvents: function(){
		for(i = 1; i < this.table.rows.length; i++){
			if(this.table.rows[i].cells.length == this.cellsAccross) this.bindCell(this.table.rows[i].cells[0]);
		}
	},
	bindCell: function(cell){
		var obj = this;
		cell.style.cursor = 'pointer';
		cell.onmousedown = function(){
			var cell = this;
			obj.active = true;
			document.body.onmousemove = function(e){
				var ev = e || window.event;

				obj.follow($(cell).up('tr'), e);
			}
			document.body.onmouseup = function(){ obj.endFollow($(cell).up('tr')); }
		}
	},
	follow: function(row, e){
		var ev = e || window.event;
		var mY = ev.clientY;

		if(!this.active){
			this.active = true;
			this.table.tBodies[0].removeChild(row);
		}


		var after = null;
		var rows = this.table.rows;
		var pos = row.positionedOffset();

		var l = null;
		for(i = 1; i < rows.length; i++){
			if(rows[i].cells.length == this.cellsAccross){
				l = rows[i];

				var p = $(l).positionedOffset();
				t = p[1];
				if(t > mY) break;
			}
		}

		if(this.active){
			l.insert({before: row});
			this.active = true;
		}
	},
	endFollow: function(row){ 
		this.active = false;
		document.body.onmouseup = null; 
		document.body.onmousemove = null;
	
		this.submitVals();
	},
	submitVals: function(){
		var str = this.serialize();

		var o = this.options;
		new Ajax.Request('ajaxtablesort.php', {parameters: {t: o.dbTable, s: o.sortColumn, i: o.idColumn, v: str}});
	},
	serialize: function(){
		var rows = this.table.rows;

		var a = [];
		for(i = 1; i < rows.length; i++){
			if(rows[i].cells.length == this.cellsAccross){
				a[a.length] = rows[i].id;
			}
		}


		return a.join(',');
	}
}

/////////////////////////////////
//////  HeaderScroll [EEF] //////
/////////////////////////////////
var headerScroll = function(id, options){
	this.id = id;
	this.options = options;
	this.table = $(this.id);
	this.scrollRow = $(options.head) || this.table.rows[0];
	//Grab widths
	var widths = [];
	for(i = 0; i < this.scrollRow.cells.length; i++){
		widths[i] = this.scrollRow.cells[i].offsetWidth;
	}
	$(this.scrollRow).absolutize();
	//Set widths again
	for(i = 0; i < this.scrollRow.cells.length; i++){
		this.scrollRow.cells[i].style.width = widths[i] + 'px';
	}
	var tr = document.createElement('tr');
	var td = document.createElement('td');
	td.style.height = parseInt(this.scrollRow.offsetHeight) + 'px';
	td.innerHTML = '&nbsp;';
	td.colSpan = this.scrollRow.cells.length;
	tr.appendChild(td);
	//this.table.insert({top: tr});
	new Insertion.After(this.scrollRow, tr);
	this.start = this.scrollRow.offsetTop;

	var obj = this;
	window.onscroll = function(){ 
		var os = document.body.scrollTop;
		if(os > obj.start){
			obj.scrollRow.style.top = os+"px";
		}
		else obj.scrollRow.style.top = obj.start+'px';
	}
}

/////////////////////////////////
//////// ArrowGrid [EEF] ////////
/////////////////////////////////
var ArrowGrid = function(tableid){
	this.tid = tableid; //Save tableid
	this.table = $(tableid); //Grab table

	this.bindInputs(); //Bind Event Listeners
}
ArrowGrid.prototype = {
	bindInputs: function(){
		var inputs = this.table.getElementsByTagName('input');  // Grab any <input> tag

		for(i = 0; i < inputs.length; i++){ //Loop through em.
			var input = inputs[i]; //Grab current input
			input.onkeydown = this.inputListener; //Bind listener funciton to KeyDown
		}

		var selects = this.table.getElementsByTagName('select'); // Grab any <select> tag

		for(i = 0; i < selects.length; i++){ //Loop through...
			var select = selects[i]; //Grag current select
			select.onkeydown = this.inputListener; //Bind listener function to keyDown
			
			/* Attempt to beat Firefox bug.  Faild thus far */
			select.onkeypress = function(event){var ev = event || window.event; if(ev.keyCode == 37 || ev.keyCode == 39) return false;}
		}
	},
	inputListener: function(e){  //Is called on input/select keydown
		var ev = e || window.event; //Init event
		var key = ev.keyCode; //Grab keycode
		/* Determine direction */
		var UP = (key == 38);
		var DOWN = (key == 40);
		var LEFT = (key == 37);
		var RIGHT = (key == 39);
		if(!UP && !DOWN && !LEFT && !RIGHT) return true;
		
		/* Discover Table Elements */
		var td = $(this).up('td');
		var tr = td.up('tr');
		var table = tr.up('table');

		/* Indexes */
		var row = tr.rowIndex;
		var col = td.cellIndex;

		/* Init Increment vars */
		var x = 0;
		var y = 0;

		var target = null;
		while(!target){ //Untill a target is found...
			/* Change indexes based on direction */
			if(UP){
				if(this.nodeName != 'SELECT') y = -1;
			}
			else if(DOWN){
				if(this.nodeName != 'SELECT') y = 1;
			}
			else if(LEFT){
				x = -1;
			}
			else if(RIGHT){
				x = 1;
			}
			col += x;
			row += y;

			/* Prevent an infinite loop by checking for ends of table */
			if (col < 0) break;
			if (row < 0) break;
			if (row > table.rows.length) break;
			if (col > table.rows[0].cells.length) break;

			var ix = hasInput(table.rows[row].cells[col]);
			if(ix > -1){ //If target has an input
				target = table.rows[row].cells[col]; //Exits loop
				target.childNodes[ix].focus(); //Focus target
			}
		}
	}
}
var hasInput = function(td){
	for(x = 0; x < td.childNodes.length; x++){ //Loop through TD children
		if(td.childNodes[x].nodeName == 'SELECT' || td.childNodes[x].nodeName == 'INPUT'){ //If a select or input is found..
			return x; // Return child Index
		}
	}
	return -1; // Otherwise no input exists [-1]
}

/////////////////////////////////
///////// Drag and Drop [EEF] ///
/////////////////////////////////

var EEFDraggable = function(id, opts){
	this.id = id;
	this.opts = opts || {};
	this.ele = $(id);
	this.dragging = false;
	obj = this;	
	
	this.regBindings();
}
EEFDraggable.prototype.regBindings = function(){
	var obj = this;
	this.ele.onmousedown = function(e){obj.startDrag(e); }
	this.ele.onmouseup = function(e){obj.endDrag(e); }
	//this.ele.onmousemove = function(e){obj.dragMove(e); }
}
	
EEFDraggable.prototype.startDrag = function(e){
	var ev = e || window.event;
	var obj = this;

	this.oldZI = this.ele.style.zIndex || 1;
	this.ele.style.zIndex = 1000;

	new Effect.Opacity(this.ele, {from: 1, to: .5, duration: .5});
	/**/ document.body.onmousemove = function(e){obj.dragMove(e);}
	/**/ document.body.onmouseup = function(e){obj.endDrag(e);}
	if(this.opts.onDragInit != null) this.opts.onDragInit.call(this, this);
	var coords = this.ele.positionedOffset();
	var cx = ev.clientX;
	var cy = ev.clientY;
	
	this.offsetLeft = cx - coords[0];
	this.offsetTop = cy - coords[1];

	this.dragging = true;
	this.ele.style.position = 'absolute';
}
EEFDraggable.prototype.dragMove = function(e){
	var ev = e || window.event;
	if(this.dragging){
		var newLeft = ev.clientX - this.offsetLeft;		
		var newTop = ev.clientY - this.offsetTop;		
		this.pos = [newLeft, newTop];
		this.ele.style.top = newTop +"px";
		this.ele.style.left = newLeft +"px";
		if(this.opts.onDrag != null) this.opts.onDrag.call(this, this);
	}
}
EEFDraggable.prototype.endDrag = function(){
	var obj = this;
	
	this.ele.style.zIndex = this.oldZI || 1;
	new Effect.Opacity(this.ele, {from: .5, to: 1, duration: .5});
	/**/document.body.onmousemove = null;
	/**/document.body.onmouseup = null;
	this.dragging = false;
	document.body.focus(); //Prevent FF3 DnD
	if(this.opts.onDrop != null) this.opts.onDrop.call(this, this);
}


/////////////////////////////////////////
///////////// CLONED DRAG [EEF] ////////
/////////////////////////////////////////

ClonedDrag = function(id, opts){
	this.id = id;
	this.opts = opts || {};
	this.ele = $(id);
	this.clones = [];
	var obj = this;	

	this.ele.onmousedown = function(e){obj.cloneEle(e);}
}
ClonedDrag.prototype.cloneEle = function(e){
	var ev = e || window.event;
	var ele = document.createElement('div');
	var index = this.clones.length;
	ele.id = this.id + "_clone" + index;
	ele.className = this.id;
	ele.style.position = 'absolute';
	ele.style.zIndex = '500';
	
	var pos = this.ele.positionedOffset();
	var cx = ev.clientX;
	var cy = ev.clientY;

	ele.style.left = pos[0] + 10 +"px";
	ele.style.top = pos[1] +"px";
	document.body.appendChild(ele);

	this.clones[index] = new EEFDraggable(ele.id, this.opts);
	this.clones[index].ele.focus();
	this.clones[index].startDrag(e);
	this.clones[index].offsetLeft = cx - pos[0];
	this.clones[index].offsetTop = cy - pos[1];


}
var $GET = function(variable){
	var str = new String(window.location);
	var exp = str.split('/');
	str = exp[exp.length-1];
	exp = str.split('?');
	str = exp[1];
	exp = str.split('&');
	var gets = [];
	for(var i = 0; i < exp.length; i++){
		var e = exp[i].split('=');
		var name = e[0];
		var value = e[1];
		gets[name] = value;
	}
	return gets[variable];
}

















Object.extend(Number.prototype, {
  toColorPart: function() {
    return this.toPaddedString(2, 16);
  },

  decRound: function(decimals) {
  	if(new String(this).split('.').length > 1){
		var offset = Math.pow(10, decimals);
		num = (this * offset).round();
		var floatVal = parseFloat(num / offset);
		var exp = new String(floatVal).split('.');	
		if(exp[1].length == decimals){
			return floatVal;
		}
		else{
			var add = decimals - exp[1].length;
			while(add > 0){
				floatVal += '0'
				add--;
			}
			return floatVal;
		}
	}
	else{
		var floatVal = this + '.';
		while(decimals > 0){
			floatVal += '0';
			decimals--;
		}
		return floatVal;
	}
  },

  toMoney: function(){
  	var num = this.decRound(2);
	if(new String(num).split('.').length > 1){
		var i = new String(num).split('.')[0];
		var d = new String(num).split('.')[1];
		if(parseInt(i) > 1000000){
			l = i.substring(0,1)+','+i.substring(1, 4)+","+i.substring(4);
		}
		else if(parseInt(i) > 100000){
			l = i.substring(0,3)+','+i.substring(3);
		}
		else if(parseInt(i) > 10000){
			l = i.substring(0,2)+','+i.substring(2);
		}
		else if(parseInt(i) > 1000){
			l = i.substring(0,1)+','+i.substring(1);
		}
		else l = i;
		return '$'+l+'.'+d;
	}
	else{
		return '$'+num;
	}
  },

  succ: function() {
    return this + 1;
  },

  times: function(iterator, context) {
    $R(0, this, true).each(iterator, context);
    return this;
  },

  toPaddedString: function(length, radix) {
    var string = this.toString(radix || 10);
    return '0'.times(length - string.length) + string;
  },

  toJSON: function() {
    return isFinite(this) ? this.toString() : 'null';
  }
});


Element.Methods = {
  visible: function(element) {
    return $(element).style.display != 'none';
  },
	
	/* custom */
	hitTest: function(element, pos) {
		element = $(element);
		elepos = element.cumulativeOffset();
		eleleft = elepos[0];
		eletop = elepos[1];
		eleright = eleleft + parseInt(element.offsetWidth);
		elebottom = eletop + parseInt(element.offsetHeight);
		alert(eleleft + ', ' + eletop + ', ' + eleright + ', ' + elebottom + '///' + pos[0] + ":" + pos[1]);

		if(pos[0] > eleleft && pos[0] < eleRight){
			if(pos[1] > eletop && pos[1] < elebottom){
				return true;
			}
		}
		return false;
	},	 

  inside: function(element, container) {
  	element = $(element);
	
	var l = element.positionedOffset()[0];
	var r = l + element.offsetWidth;
	var t = element.positionedOffset()[1];
	var b = t + element.offsetHeight;
	
	var cl = container.positionedOffset()[0];
	var cr = cl + container.offsetWidth;
	var ct = container.positionedOffset()[1];
	var cb = ct + container.offsetHeight;

	if((l >= cl && l <= cr) || (r >= cl && r <= cr)){
		if((t >= ct && t <= cb) || (t >= ct && t <= cb)){
			return true;
		}
	}
	return false;
  },

  toggle: function(element) {
		element = $(element);
    Element[Element.visible(element) ? 'hide' : 'show'](element);
    return element;
  },

  hide: function(element) {
    element = $(element);
    element.style.display = 'none';
    return element;
  },

  show: function(element) {
    element = $(element);
    element.style.display = '';
    return element;
  },

  remove: function(element) {
    element = $(element);
    element.parentNode.removeChild(element);
    return element;
  },

  update: function(element, content) {
    element = $(element);
    if (content && content.toElement) content = content.toElement();
    if (Object.isElement(content)) return element.update().insert(content);
    content = Object.toHTML(content);
    element.innerHTML = content.stripScripts();
    content.evalScripts.bind(content).defer();
    return element;
  },

  replace: function(element, content) {
    element = $(element);
    if (content && content.toElement) content = content.toElement();
    else if (!Object.isElement(content)) {
      content = Object.toHTML(content);
      var range = element.ownerDocument.createRange();
      range.selectNode(element);
      content.evalScripts.bind(content).defer();
      content = range.createContextualFragment(content.stripScripts());
    }
    element.parentNode.replaceChild(content, element);
    return element;
  },

  insert: function(element, insertions) {
    element = $(element);

    if (Object.isString(insertions) || Object.isNumber(insertions) ||
        Object.isElement(insertions) || (insertions && (insertions.toElement || insertions.toHTML)))
          insertions = {bottom:insertions};

    var content, insert, tagName, childNodes;

    for (var position in insertions) {
      content  = insertions[position];
      position = position.toLowerCase();
      insert = Element._insertionTranslations[position];

      if (content && content.toElement) content = content.toElement();
      if (Object.isElement(content)) {
        insert(element, content);
        continue;
      }

      content = Object.toHTML(content);

      tagName = ((position == 'before' || position == 'after')
        ? element.parentNode : element).tagName.toUpperCase();

      childNodes = Element._getContentFromAnonymousElement(tagName, content.stripScripts());

      if (position == 'top' || position == 'after') childNodes.reverse();
      childNodes.each(insert.curry(element));

      content.evalScripts.bind(content).defer();
    }

    return element;
  },

  wrap: function(element, wrapper, attributes) {
    element = $(element);
    if (Object.isElement(wrapper))
      $(wrapper).writeAttribute(attributes || { });
    else if (Object.isString(wrapper)) wrapper = new Element(wrapper, attributes);
    else wrapper = new Element('div', wrapper);
    if (element.parentNode)
      element.parentNode.replaceChild(wrapper, element);
    wrapper.appendChild(element);
    return wrapper;
  },

  inspect: function(element) {
    element = $(element);
    var result = '<' + element.tagName.toLowerCase();
    $H({'id': 'id', 'className': 'class'}).each(function(pair) {
      var property = pair.first(), attribute = pair.last();
      var value = (element[property] || '').toString();
      if (value) result += ' ' + attribute + '=' + value.inspect(true);
    });
    return result + '>';
  },

  recursivelyCollect: function(element, property) {
    element = $(element);
    var elements = [];
    while (element = element[property])
      if (element.nodeType == 1)
        elements.push(Element.extend(element));
    return elements;
  },

  ancestors: function(element) {
    return $(element).recursivelyCollect('parentNode');
  },

  descendants: function(element) {
    return $(element).select("*");
  },

  firstDescendant: function(element) {
    element = $(element).firstChild;
    while (element && element.nodeType != 1) element = element.nextSibling;
    return $(element);
  },

  immediateDescendants: function(element) {
    if (!(element = $(element).firstChild)) return [];
    while (element && element.nodeType != 1) element = element.nextSibling;
    if (element) return [element].concat($(element).nextSiblings());
    return [];
  },

  previousSiblings: function(element) {
    return $(element).recursivelyCollect('previousSibling');
  },

  nextSiblings: function(element) {
    return $(element).recursivelyCollect('nextSibling');
  },

  siblings: function(element) {
    element = $(element);
    return element.previousSiblings().reverse().concat(element.nextSiblings());
  },

  match: function(element, selector) {
    if (Object.isString(selector))
      selector = new Selector(selector);
    return selector.match($(element));
  },

  up: function(element, expression, index) {
    element = $(element);
    if (arguments.length == 1) return $(element.parentNode);
    var ancestors = element.ancestors();
    return Object.isNumber(expression) ? ancestors[expression] :
      Selector.findElement(ancestors, expression, index);
  },

  down: function(element, expression, index) {
    element = $(element);
    if (arguments.length == 1) return element.firstDescendant();
    return Object.isNumber(expression) ? element.descendants()[expression] :
      Element.select(element, expression)[index || 0];
  },

  previous: function(element, expression, index) {
    element = $(element);
    if (arguments.length == 1) return $(Selector.handlers.previousElementSibling(element));
    var previousSiblings = element.previousSiblings();
    return Object.isNumber(expression) ? previousSiblings[expression] :
      Selector.findElement(previousSiblings, expression, index);
  },

  next: function(element, expression, index) {
    element = $(element);
    if (arguments.length == 1) return $(Selector.handlers.nextElementSibling(element));
    var nextSiblings = element.nextSiblings();
    return Object.isNumber(expression) ? nextSiblings[expression] :
      Selector.findElement(nextSiblings, expression, index);
  },

  select: function() {
    var args = $A(arguments), element = $(args.shift());
    return Selector.findChildElements(element, args);
  },

  adjacent: function() {
    var args = $A(arguments), element = $(args.shift());
    return Selector.findChildElements(element.parentNode, args).without(element);
  },

  identify: function(element) {
    element = $(element);
    var id = element.readAttribute('id'), self = arguments.callee;
    if (id) return id;
    do { id = 'anonymous_element_' + self.counter++ } while ($(id));
    element.writeAttribute('id', id);
    return id;
  },

  readAttribute: function(element, name) {
    element = $(element);
    if (Prototype.Browser.IE) {
      var t = Element._attributeTranslations.read;
      if (t.values[name]) return t.values[name](element, name);
      if (t.names[name]) name = t.names[name];
      if (name.include(':')) {
        return (!element.attributes || !element.attributes[name]) ? null :
         element.attributes[name].value;
      }
    }
    return element.getAttribute(name);
  },

  writeAttribute: function(element, name, value) {
    element = $(element);
    var attributes = { }, t = Element._attributeTranslations.write;

    if (typeof name == 'object') attributes = name;
    else attributes[name] = Object.isUndefined(value) ? true : value;

    for (var attr in attributes) {
      name = t.names[attr] || attr;
      value = attributes[attr];
      if (t.values[attr]) name = t.values[attr](element, value);
      if (value === false || value === null)
        element.removeAttribute(name);
      else if (value === true)
        element.setAttribute(name, name);
      else element.setAttribute(name, value);
    }
    return element;
  },

  getHeight: function(element) {
    return $(element).getDimensions().height;
  },

  getWidth: function(element) {
    return $(element).getDimensions().width;
  },

  classNames: function(element) {
    return new Element.ClassNames(element);
  },

  hasClassName: function(element, className) {
    if (!(element = $(element))) return;
    var elementClassName = element.className;
    return (elementClassName.length > 0 && (elementClassName == className ||
      new RegExp("(^|\\s)" + className + "(\\s|$)").test(elementClassName)));
  },

  addClassName: function(element, className) {
    if (!(element = $(element))) return;
    if (!element.hasClassName(className))
      element.className += (element.className ? ' ' : '') + className;
    return element;
  },

  removeClassName: function(element, className) {
    if (!(element = $(element))) return;
    element.className = element.className.replace(
      new RegExp("(^|\\s+)" + className + "(\\s+|$)"), ' ').strip();
    return element;
  },

  toggleClassName: function(element, className) {
    if (!(element = $(element))) return;
    return element[element.hasClassName(className) ?
      'removeClassName' : 'addClassName'](className);
  },

  // removes whitespace-only text node children
  cleanWhitespace: function(element) {
    element = $(element);
    var node = element.firstChild;
    while (node) {
      var nextNode = node.nextSibling;
      if (node.nodeType == 3 && !/\S/.test(node.nodeValue))
        element.removeChild(node);
      node = nextNode;
    }
    return element;
  },

  empty: function(element) {
    return $(element).innerHTML.blank();
  },

  descendantOf: function(element, ancestor) {
    element = $(element), ancestor = $(ancestor);

    if (element.compareDocumentPosition)
      return (element.compareDocumentPosition(ancestor) & 8) === 8;

    if (ancestor.contains)
      return ancestor.contains(element) && ancestor !== element;

    while (element = element.parentNode)
      if (element == ancestor) return true;

    return false;
  },

  scrollTo: function(element) {
    element = $(element);
    var pos = element.cumulativeOffset();
    window.scrollTo(pos[0], pos[1]);
    return element;
  },

  getStyle: function(element, style) {
    element = $(element);
    style = style == 'float' ? 'cssFloat' : style.camelize();
    var value = element.style[style];
    if (!value || value == 'auto') {
      var css = document.defaultView.getComputedStyle(element, null);
      value = css ? css[style] : null;
    }
    if (style == 'opacity') return value ? parseFloat(value) : 1.0;
    return value == 'auto' ? null : value;
  },

  getOpacity: function(element) {
    return $(element).getStyle('opacity');
  },

  setStyle: function(element, styles) {
    element = $(element);
    var elementStyle = element.style, match;
    if (Object.isString(styles)) {
      element.style.cssText += ';' + styles;
      return styles.include('opacity') ?
        element.setOpacity(styles.match(/opacity:\s*(\d?\.?\d*)/)[1]) : element;
    }
    for (var property in styles)
      if (property == 'opacity') element.setOpacity(styles[property]);
      else
        elementStyle[(property == 'float' || property == 'cssFloat') ?
          (Object.isUndefined(elementStyle.styleFloat) ? 'cssFloat' : 'styleFloat') :
            property] = styles[property];

    return element;
  },

  setOpacity: function(element, value) {
    element = $(element);
    element.style.opacity = (value == 1 || value === '') ? '' :
      (value < 0.00001) ? 0 : value;
    return element;
  },

  getDimensions: function(element) {
    element = $(element);
    var display = element.getStyle('display');
    if (display != 'none' && display != null) // Safari bug
      return {width: element.offsetWidth, height: element.offsetHeight};

    // All *Width and *Height properties give 0 on elements with display none,
    // so enable the element temporarily
    var els = element.style;
    var originalVisibility = els.visibility;
    var originalPosition = els.position;
    var originalDisplay = els.display;
    els.visibility = 'hidden';
    els.position = 'absolute';
    els.display = 'block';
    var originalWidth = element.clientWidth;
    var originalHeight = element.clientHeight;
    els.display = originalDisplay;
    els.position = originalPosition;
    els.visibility = originalVisibility;
    return {width: originalWidth, height: originalHeight};
  },

  makePositioned: function(element) {
    element = $(element);
    var pos = Element.getStyle(element, 'position');
    if (pos == 'static' || !pos) {
      element._madePositioned = true;
      element.style.position = 'relative';
      // Opera returns the offset relative to the positioning context, when an
      // element is position relative but top and left have not been defined
      if (Prototype.Browser.Opera) {
        element.style.top = 0;
        element.style.left = 0;
      }
    }
    return element;
  },

  undoPositioned: function(element) {
    element = $(element);
    if (element._madePositioned) {
      element._madePositioned = undefined;
      element.style.position =
        element.style.top =
        element.style.left =
        element.style.bottom =
        element.style.right = '';
    }
    return element;
  },

  makeClipping: function(element) {
    element = $(element);
    if (element._overflow) return element;
    element._overflow = Element.getStyle(element, 'overflow') || 'auto';
    if (element._overflow !== 'hidden')
      element.style.overflow = 'hidden';
    return element;
  },

  undoClipping: function(element) {
    element = $(element);
    if (!element._overflow) return element;
    element.style.overflow = element._overflow == 'auto' ? '' : element._overflow;
    element._overflow = null;
    return element;
  },

  cumulativeOffset: function(element) {
    var valueT = 0, valueL = 0;
    do {
      valueT += element.offsetTop  || 0;
      valueL += element.offsetLeft || 0;
      element = element.offsetParent;
    } while (element);
    return Element._returnOffset(valueL, valueT);
  },

  positionedOffset: function(element) {
    var valueT = 0, valueL = 0;
    do {
      valueT += element.offsetTop  || 0;
      valueL += element.offsetLeft || 0;
      element = element.offsetParent;
      if (element) {
        if (element.tagName.toUpperCase() == 'BODY') break;
        var p = Element.getStyle(element, 'position');
        if (p !== 'static') break;
      }
    } while (element);
    return Element._returnOffset(valueL, valueT);
  },

  absolutize: function(element) {
    element = $(element);
    if (element.getStyle('position') == 'absolute') return element;
    // Position.prepare(); // To be done manually by Scripty when it needs it.

    var offsets = element.positionedOffset();
    var top     = offsets[1];
    var left    = offsets[0];
    var width   = element.clientWidth;
    var height  = element.clientHeight;

    element._originalLeft   = left - parseFloat(element.style.left  || 0);
    element._originalTop    = top  - parseFloat(element.style.top || 0);
    element._originalWidth  = element.style.width;
    element._originalHeight = element.style.height;

    element.style.position = 'absolute';
    element.style.top    = top + 'px';
    element.style.left   = left + 'px';
    element.style.width  = width + 'px';
    element.style.height = height + 'px';
    return element;
  },

  relativize: function(element) {
    element = $(element);
    if (element.getStyle('position') == 'relative') return element;
    // Position.prepare(); // To be done manually by Scripty when it needs it.

    element.style.position = 'relative';
    var top  = parseFloat(element.style.top  || 0) - (element._originalTop || 0);
    var left = parseFloat(element.style.left || 0) - (element._originalLeft || 0);

    element.style.top    = top + 'px';
    element.style.left   = left + 'px';
    element.style.height = element._originalHeight;
    element.style.width  = element._originalWidth;
    return element;
  },

  cumulativeScrollOffset: function(element) {
    var valueT = 0, valueL = 0;
    do {
      valueT += element.scrollTop  || 0;
      valueL += element.scrollLeft || 0;
      element = element.parentNode;
    } while (element);
    return Element._returnOffset(valueL, valueT);
  },

  getOffsetParent: function(element) {
    if (element.offsetParent) return $(element.offsetParent);
    if (element == document.body) return $(element);

    while ((element = element.parentNode) && element != document.body)
      if (Element.getStyle(element, 'position') != 'static')
        return $(element);

    return $(document.body);
  },

  viewportOffset: function(forElement) {
    var valueT = 0, valueL = 0;

    var element = forElement;
    do {
      valueT += element.offsetTop  || 0;
      valueL += element.offsetLeft || 0;

      // Safari fix
      if (element.offsetParent == document.body &&
        Element.getStyle(element, 'position') == 'absolute') break;

    } while (element = element.offsetParent);

    element = forElement;
    do {
      if (!Prototype.Browser.Opera || (element.tagName && (element.tagName.toUpperCase() == 'BODY'))) {
        valueT -= element.scrollTop  || 0;
        valueL -= element.scrollLeft || 0;
      }
    } while (element = element.parentNode);

    return Element._returnOffset(valueL, valueT);
  },

  clonePosition: function(element, source) {
    var options = Object.extend({
      setLeft:    true,
      setTop:     true,
      setWidth:   true,
      setHeight:  true,
      offsetTop:  0,
      offsetLeft: 0
    }, arguments[2] || { });

    // find page position of source
    source = $(source);
    var p = source.viewportOffset();

    // find coordinate system to use
    element = $(element);
    var delta = [0, 0];
    var parent = null;
    // delta [0,0] will do fine with position: fixed elements,
    // position:absolute needs offsetParent deltas
    if (Element.getStyle(element, 'position') == 'absolute') {
      parent = element.getOffsetParent();
      delta = parent.viewportOffset();
    }

    // correct by body offsets (fixes Safari)
    if (parent == document.body) {
      delta[0] -= document.body.offsetLeft;
      delta[1] -= document.body.offsetTop;
    }

    // set position
    if (options.setLeft)   element.style.left  = (p[0] - delta[0] + options.offsetLeft) + 'px';
    if (options.setTop)    element.style.top   = (p[1] - delta[1] + options.offsetTop) + 'px';
    if (options.setWidth)  element.style.width = source.offsetWidth + 'px';
    if (options.setHeight) element.style.height = source.offsetHeight + 'px';
    return element;
  }
};



/*--------------------------------------------------------------------------*/
Form.AutoComplete = function(inputid, options){
	this.input = $(inputid); // Holds supplied input
	this.form = null; //Holds form for supplied input
	this.keySelected = null; //Holds which option is currently hovered(Keyboard)
	this.options = options;
	this.searchList = null; //Holds list returned from server
	this.selectedIndex = null; 
	this.indexed = []; //Holds list returned from server
	this.isIndexed = false; //Which list is being used?
	this.render();
	this.grabRows();
}
Form.AutoComplete.prototype.render = function(){
	///// Holds List
	this.dropDown = document.createElement('div');
	this.dropDown.style.overflow = 'auto';
	this.dropDown.style.height = '200px';
	this.dropDown.style.fontSize = '12px';
	this.dropDown.style.background = 'white';

	//// Close Link
	var href = document.createElement('a');
	href.innerHTML = 'Close';
	href.style.fontSize = '12px';
	href.href = 'javascript:void(0)';
	href.onclick = function(){ obj.cancelOpen(); }

	var hCon = document.createElement('div');
	hCon.style.textAlign = 'right';
	hCon.appendChild(href);
	

	///// Holds DropDown.  (Wrapper.  Border won't glitch this way)
	this.dropDownCon = document.createElement('div');
	this.dropDownCon.style.position = 'absolute';
	Position.clone(this.input, this.dropDownCon);
	this.dropDownCon.style.display = 'none';
	this.dropDownCon.style.borderWidth = '1px';
	this.dropDownCon.style.borderStyle = 'solid';
	this.dropDownCon.style.borderColer = '#888888';
	this.dropDownCon.style.zIndex = '1000';
	this.dropDownCon.style.background = 'white';
	this.dropDownCon.style.width = parseInt(this.input.offsetWidth) + "px"; 
	this.dropDownCon.style.height = "215px"; 
	this.dropDownCon.style.top = parseInt(this.dropDownCon.style.top) + parseInt(this.input.offsetHeight) + 'px';

	this.dropDownCon.appendChild(this.dropDown);
	this.dropDownCon.appendChild(hCon);
	document.body.appendChild(this.dropDownCon);

	var obj = this;
	if(this.options.watermark != null){
		this.input.value = this.options.watermark;
		this.input.style.color = '#888';
	}
	this.input.autocomplete = 'off';
	this.input.onmouseover = function(){this.className = 'hover';}
	this.input.onmouseout = function(){if(obj.dropDownOpen != true) this.className = '';}
	this.input.onblur = function(e){if(obj.dropDownOpen != true) this.className = ''; obj.hideTest(e);}
	this.input.onkeyup = function(e){obj.inputListener(e);}
	this.input.onkeydown = function(e){ var ev = e || window.event; if(ev.keyCode == 13) return false; this.style.color = 'black'}
	this.input.onfocus = function(e){obj.fillContainer(); if(obj.options.watermark != null && this.value == obj.options.watermark) this.value = ''; if(obj.options.showOnEmpty || this.value.length > 0) obj.showMenu();}

	///// Build valInput
	this.valInput = document.createElement('input');
	this.valInput.type = 'hidden';
	this.valInput.id   = this.input.name+'_v';
	this.valInput.name = this.input.name; // Make submittable by existing form;
	this.valInput.value = '';
	this.input.name = this.input.name+'_disp';

	///// Find Existing Form
	var ele = this.input;
	if(this.options.form != null) this.form = $(this.options.form);
	else{
	  while(ele != null && ele != "undefined"){
		  if(ele.nodeName.toLowerCase() == 'form'){
			  this.form = ele;
		  }
		  ele = ele.parentNode;
	  }
	}
	if(this.form != null){ 
		this.form.appendChild(this.valInput); 
		this.form.onsubmit = function(){if(obj.dropDownOpen) return false; return true; }
	}
}
Form.AutoComplete.prototype.renderOpt = function(i, disp){
	var obj = this;
	var opt = document.createElement('div');
	opt.innerHTML = disp || this.searchList[i].disp;
	opt.style.padding = '2px';
	opt.custoIndex = i;
	var listI = this.dropDown.childNodes.length;
	this.dropDown.appendChild(opt);	

	opt.onmouseover = function(){this.className = 'mouseHover'; obj.keySelected = listI; }
	opt.onmouseout = function(){this.className = ''; }
	opt.onclick = function(){var index = i; obj.select(index);}
}
Form.AutoComplete.prototype.select = function(i){
	var search = (this.isIndexed) ? this.indexed : this.searchList;
	this.input.value = search[i].disp;
	this.valInput.value = search[i].val;
	this.selectedIndex = i;
	this.selectedDisp = this.input.value;
	this.hideMenu();
	this.input.className = '';

	if(this.options.watermark != null) this.input.style.color = 'black';
	if(this.onchange) this.onchange.call(this);
}
Form.AutoComplete.prototype.grabRows = function(){
	this.dropDown.innerHTML = 'Loading... please wait.';
	var p = [];
	p['dbPage'] = this.options.dbPage || '';
	p['dbBid']  = this.options.dbBid || '';
	p['dbTable'] = this.options.dbTable || '';
	p['dbVal'] = this.options.dbVal || '';
	if(p['dbVal'].split(', ').length > 1) p['valFormat'] = this.options.valFormat || p['dbVal'].split(', ').join(':');
	p['dbDisp'] = this.options.dbDisp || this.options.dbVal;
	if(p['dbDisp'].split(', ').length > 1) p['dispFormat'] = this.options.dispFormat || p['dbDisp'].split(', ').join(':');
	p['dbWhere'] = this.options.dbWhere || '';

	var obj = this;
	new Ajax.Request('ajaxautocom.php', {parameters: p, onSuccess: function(t){ obj.procResults(t); }});
		
}
Form.AutoComplete.prototype.procResults = function(t){
	eval(t.responseText);
	this.searchList = returnVals;
	this.fillContainer();
}
Form.AutoComplete.prototype.keyHover = function(i){
	if(this.keySelected == null) this.keySelected = -1;
	this.keySelected += i;
	if(this.keySelected >=  this.dropDown.childNodes.length) this.keySelected = this.dropDown.childNodes.length - 1;
	if(this.keySelected < 0) this.keySelected = 0;

	if(this.isIndexed){
		var search = this.indexed;
	}
	else{
		var search = this.searchList;
	}
	

	//Highlight
	var ele =  this.dropDown.childNodes[this.keySelected];
	if(ele != null){
		ele.className = 'keyHover'; 
	}
}
Form.AutoComplete.prototype.fillContainer = function(){
	var search = (this.isIndexed) ? this.indexed : this.searchList;
	var minLength = this.options.minLength || 0;
	var val = this.input.value;

	if(val.length >= minLength){
		if(this.options.watermark != null && val == this.options.watermark) val = '';
		if(val.length > 0){
			val = val.replace(/\(/g, "\\(");
			val = val.replace(/\)/g, "\\)");
			val = val.replace(/\./g, "\\.");
			var reg = new RegExp();
			reg.compile(val, "gi");
		}
		
		// Populate List
		this.dropDown.innerHTML = '';
		for(var i = 0; i < search.length; i++){
			if(search[i] != null && search[i] != 'undefined'){
				var item = search[i];
				var curVal = item.disp;
				//// Enhancing performance: Only regExp if theres something to compare
				if(val.length > 0){
					result = curVal.replace(reg, "<span class=\"matched\">"+val.replace(/\\/g, '')+"</span>");
			
					if(result.length > curVal.length){
						this.indexed[i] = {disp: search[i].disp, val: search[i].val};
						this.renderOpt(i, result);
					}
					else{ if(this.isIndexed) delete this.indexed[i]; }
				}
				else{
					this.renderOpt(i, item.disp);
				}
			}
		}
	}
	else{ this.dropDown.innerHTML = 'Searching..'; }
}
Form.AutoComplete.prototype.inputListener = function(e){
	var ev = e || window.event;
	var val = this.input.value;


	/////////////Fill OptionList
	//Handle Indexing
	if(this.input.value.length == 1){
		this.isIndexed = false;
		window.status = "Search List: Full";
		var search = this.searchList;
		this.indexed = [];
	}
	else if(ev.keyCode != 8 && this.indexed.length > 0){
		var search = this.indexed;
		this.isIndexed = true;
		window.status = "Search List: Indexed";
	}
	else{
		this.isIndexed = false;
		window.status = "Search List: Full";
		var search = this.searchList;
		this.indexed = [];
	}

	this.fillContainer();

	//////////// Arrow Keys
	//Up
	if(ev.keyCode == 38){
		this.keyHover(-1);
	}
	//Down
	if(ev.keyCode == 40){
		if(this.dropDownOpen != true) this.showMenu();
		this.keyHover(1);
	}
	//Enter
	if(ev.keyCode == 13){
		var xi = 0;
		var search = (this.isIndexed) ? this.indexed : this.searchList;
		for(i = 0; i < search.length; i++){
			if(search[i] != null){
				if(xi == this.keySelected){
					this.select(i);
					this.hideMenu();
				}
				xi++;
			}
		}
	}
	
	///// Fix Dimensions
	this.fixDimensions();
	if(this.dropDown.childNodes.length < 1){
		this.dropDown.innerHTML = 'No Results...';
	}
}
Form.AutoComplete.prototype.fixDimensions = function(){
	if(this.dropDown.childNodes.length > 0){
		this.dropDown.style.height = '200px';
	}
}
Form.AutoComplete.prototype.showMenu = function(){
	///// Fix Dimensions
	this.fixDimensions();
	Element.show(this.dropDownCon);
	this.dropDownOpen = true;
	this.input.select();
}
Form.AutoComplete.prototype.cancelOpen = function(){
	this.hideMenu();
	if(this.options.onClose != null) this.options.onClose.call(this, this);
	if(this.valInput.value.length > 0){
		this.input.value = this.selectedDisp;
	}
	else{
		if(this.options.watermark.length > 0){ 
			this.input.value = this.options.watermark;
			this.input.style.color = '#888';
		}
		else{ this.input.value = ''; }
	}
}
Form.AutoComplete.prototype.hideMenu = function(){
	Element.hide(this.dropDownCon);
	this.dropDownOpen = false;
	this.input.className = '';
}
Form.AutoComplete.prototype.hideTest = function(e){
	var obj = this;

}


// ----------------------------------
Form.EditInPlace = function(tableid, options){
	this.vals = []; //Holds values for cells incase swap is canceled
	this.cols = []; //Holds editable column names in db
	this.curRow; // Keeps track of current row being edited
	this.options = (options != null) ? options : {};
	this.table = $(tableid);
	this.cellsAccross = this.table.rows[0].cells.length;

	this.editableCols();
	this.registerBindings();
}
Form.EditInPlace.prototype.tdSwap = function(trigger){
	trigger.onclick = null; // Prevent from firing again
	this.curRow = trigger.parentNode.rowIndex;
	this.vals[trigger.parentNode.rowIndex+":"+trigger.cellIndex] = trigger.innerHTML;
	var obj = this;
	
	if(this.options.colorCells) trigger.className = 'curEdit';
	///////// IF CUSTOM HANDLER EXISTS, CALL IT AND RETURN
	if(this.options.specialSwaps != null && this.options.specialSwaps[this.cols[trigger.cellIndex]] != null){ 
		this.options.specialSwaps[this.cols[trigger.cellIndex]].call(this, trigger, this);
		return false;
	}
	///////// OTHERWISE HANDLE TEXT INPUT
	else this.textBoxSwap(trigger);
}
Form.EditInPlace.prototype.textBoxSwap = function(trigger){
	var obj = this;

	var val = trigger.innerHTML;
	var input = document.createElement('input');
	input.style.textAlign = 'right';
	input.onblur = function(){ obj.inputSwap(trigger); }
	input.onfocus = function(){ this.select(); }
	input.onkeydown = function(transport){      
		var e = (transport == null) ? window.event : transport;
		if(e.keyCode == 13) obj.inputSwap(trigger);      // Save upon [Enter]/[Return]
		else if(e.keyCode == 9){                         // Save and move to next Cell on [Tab]
			var i = this.parentNode.cellIndex + 1;
			while(!obj.isEditable(i)){
				if(i >= obj.table.rows[0].cells.length){
					i = -1;
					obj.curRow++;
				}
				i++;
			}
			obj.tdSwap(obj.table.rows[obj.curRow].cells[i]);
		}
	} 
	input.type = 'text';
	input.value = val;
	input.style.width = parseInt(trigger.offsetWidth) + 'px';

	trigger.innerHTML = '';
	trigger.appendChild(input);
	input.focus();
}
Form.EditInPlace.prototype.inputSwap = function(trigger, value, disp){
	if(this.options.colorCells) trigger.className = 'editable';
	var obj = this;
	trigger.onblur = null;
	var input = trigger.childNodes[0];
	var val = (value != null) ? value : input.value;

	this.options.parameters = {};
	this.options.parameters['table'] = this.table.id;
	this.options.parameters['value'] = val;
	this.options.parameters['column'] = this.cols[trigger.cellIndex];
	this.options.parameters['primary_key'] = this.options.primaryKey || 'id';
	this.options.parameters['id'] = trigger.parentNode.id;
	this.options.parameters['id'] = trigger.parentNode.id;

	new Ajax.Request('eip.php', this.options);

	trigger.innerHTML = '';
	val = disp || val;
	trigger.appendChild(document.createTextNode(val));
	trigger.onclick = function(){ obj.tdSwap(trigger); }

	if(this.options.onRowEdit != null) this.options.onRowEdit.call(this, this);
}
Form.EditInPlace.prototype.cancelSwap = function(trigger){
	var obj = this;
	if(this.options.colorCells) trigger.className = 'editable';
	trigger.innerHTML = this.vals[trigger.parentNode.rowIndex+":"+trigger.cellIndex];
	trigger.onclick = function(){ obj.tdSwap(trigger); }
}
Form.EditInPlace.prototype.registerBindings = function(){
	var obj = this;

	for(var row = 1; row < this.table.rows.length; row++){
		var curRow = this.table.rows[row];
		var numCells = curRow.cells.length;

		if(numCells == this.cellsAccross){
			for(cell = 0; cell < numCells; cell++){
				var x = this.table.rows[row].cells[cell];
				if(this.isEditable(x.cellIndex)){
					x.onclick = function(){ obj.tdSwap($(this)); }
					x.title = 'Click Here to Edit Value';
					if(this.options.colorCells) x.className = 'editable';
				}
			} 
		}
	}
}
Form.EditInPlace.prototype.isEditable = function(index){
	if(this.cols[index] != null) return true;
	else return false;
}
Form.EditInPlace.prototype.editableCols = function(){
	for(i in this.table.rows[0].cells){
		var cell = this.table.rows[0].cells[i];
		if(cell.id != null && cell.id != ''){
			this.cols[cell.cellIndex] = cell.id;
			if(this.options.colorCells) cell.className = 'editableHead';
		}
	}
}




/*--------------------------------------------------------------------------*/

Element.addMethods();