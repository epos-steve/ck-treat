/* 
 * These are the functions that were included in the head section of
 * /ta/buscatermenu4.php & may be duplicated elsewhere
 * 
 * /ta/buscatermenu5.php
 */

// Note: Use common.js instead of this file - this file is being deleted at some point

function changePage(newLoc)
{
	nextPage = newLoc.options[newLoc.selectedIndex].value;

	if (nextPage != "")
	{
		document.location.href = nextPage;
	}
}
