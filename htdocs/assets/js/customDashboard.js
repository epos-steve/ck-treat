var ObjSize = function(obj) {
	var size=0, key;
	for( key in obj ) {
		if( obj.hasOwnProperty( key ) ) size++;
	}
	return size;
};

jQuery.ajaxSetup({
	cache: false,
	complete: function( jqXHR, responseText ){
		if( responseText != 'success' ){
			alert( 'There was an error processing your request. Please try again.\n'+this.url+'('+responseText+')\n'+jqXHR.getAllResponseHeaders() );
		}
	}
});

var dashboardEmbedded = false;
var dashboardDefault = false;
var dashboardWho = null;

function createChartWindow( o ){
	if( o.id > 0 )
		wId = 'chart'+o.id;
	else wId = 'newWindow';

	var dashboard = 'dashboard_left';
	if( o.size == 2 ) dashboard = 'dashboard_right';
	if( o.size == 3 ) dashboard = 'dashboard_replace';

	jQuery("<div id='"+wId+"' class='eeChartContainer sortable eeLocked' />")
		.appendTo( '#'+dashboard );

	var w = jQuery('#'+wId);
	w.append("<div class='eeChartWindow ui-widget ui-widget-content ui-corner-all' />");

	chartTemplate =
		"<div class='ui-widget-header eeChartHeader ui-corner-top ui-helper-clearfix'>"+
		"	<span class='eeChartButtonsLeft'>";

	if( !dashboardDefault )
		chartTemplate +=
			"		<span class='bLock tt'>Lock/Unlock</span>"+
			"		<span class='bPublish tt'>Publish Changes</span>"+
			"		<span class='bDiscard tt'>Discard Changes</span>";

	chartTemplate +=
		"	</span>"+
		"	<span class='eeChartTitle ui-dialog-title'>&nbsp;"+o.title+"</span>"+
		"	<span class='eeChartButtonsRight'>"+
		"		<span class='bRefresh tt'>Refresh Data</span>"+
		"		<span class='bEChart tt'>Export Chart</span>"+
		"		<span class='bEData tt'>Export Data</span>";

	if( !dashboardDefault )
		chartTemplate +=
			"		<span class='bSettings tt'>Settings</span>"+
			"		<span class='bDelete tt'>Delete</span>"+
			"		<span class='bRemove tt'>Remove from Dashboard</span>";

	chartTemplate +=
		"	</span>"+
		"</div>";

	jQuery('.eeChartWindow',w).append( chartTemplate );

	jQuery('.bLock',w).button({
		icons: {primary:'ui-icon-locked'},
		text: false
	});
	jQuery('.bPublish',w).button({
		icons: {primary:'ui-icon-circle-check'},
		text: false
	});
	jQuery('.bDiscard',w).button({
		icons: {primary:'ui-icon-circle-close'},
		text: false
	});
	jQuery('.bRefresh',w).button({
		icons: {primary:'ui-icon-refresh'},
		text: false
	});
	jQuery('.bEChart',w).button({
		icons: {primary:'ui-icon-disk'},
		text: false
	});
	jQuery('.bEData',w).button({
		icons: {primary:'ui-icon-calculator'},
		text: false
	});
	jQuery('.bSettings',w).button({
		icons: {primary:'ui-icon-wrench'},
		text: false
	});
	jQuery('.bDelete',w).button({
		icons: {primary:'ui-icon-trash'},
		text: false
	});
	jQuery('.bRemove',w).button({
		icons: {primary:'ui-icon-closethick'},
		text: false
	});

	jQuery('.eeChartWindow',w).append("<div class='eeChartArea' />");

	var wHeight = o.height > 1000 ? 1000 : o.height < 100 ? 250 : o.height;

	jQuery('.eeChartArea',w)
		.height( wHeight )
		.resizable({
			handles: 's',
			minHeight: 100,
			cancel: '.eeLocked .eeChartArea',
			start: function(){
				jQuery('.eeChart',w).empty();
			},
			stop: function(){
				var c = jQuery('.eeChartArea',w).data('eeChart');
				updateDashboardOrder( w.parent().attr('id') );
				insertFC( w, c );
			}
		});

	updateSortables( );

	return w;
}

function insertFC( w, o )
{
	var queryAppend = '';
	if( dashboardDefault && dashboardWho != null ) {
		queryAppend = '&who=' + dashboardWho;
	}

	try {
		if( o.ctype == 'table' )
		{
			var url = '/ta/charts/ajax.chart.php?fn=getTable&viewid='+o.id+'&groupid='+getDashboardGroup()+queryAppend;
			jQuery('.eeChart',w)
				.width( jQuery('.eeChartArea',w).width() )
				.height( jQuery('.eeChartArea',w).height()-10 )
				.html('<div class="eeTable" />');
			jQuery('.eeTable',w)
				.load(url);
		}
		else
		{
			var url = '/ta/charts/ajax.chart.php?fn=getChart&viewid='+o.id+'&groupid='+getDashboardGroup()+queryAppend;
			jQuery('.eeChart',w).insertFusionCharts({
				type: o.ctype,
				data: url,
				dataFormat: 'URIData',
				width: jQuery('.eeChartArea',w).width(),
				height: jQuery('.eeChartArea',w).height()-10,
				wMode: 'opaque'
			});
		}
	} catch( e ) {
		jQuery('.eeChart',w).html( '<span>Failed to create chart of type "'+o.ctype+'" (chart id '+o.id+')</span><br /><span>('+e+')</span>' );
	}
}

var tooltipOptions = {
	items: '.tt',
	open: function( event, ui ){
		jQuery('.tt').not(event.target).tooltip( 'close' );
	},
	content: function(){
		var ttTitle = jQuery(this).attr('title');
		var ttBody = jQuery(this).data('ttbody');
		var ttHtml = '<div class="ttTitle">'+ttTitle+'</div><div class="ttBody">'+ttBody+'</div>';
		return ttHtml;
	}
};

function addChart( w, o )
{
	if( !( o.id > 0 ) )
		return false;

	jQuery('.bLock', w).click(function(){
		var icon = 'ui-icon-unlocked';
		if( w.toggleClass('eeLocked').hasClass('eeLocked') )
			icon = 'ui-icon-locked';

		jQuery(this).button( 'option', 'icons', {primary:icon} ).toggleClass( 'ui-state-highlight' );
	}).data( 'ttbody',
		'Unlock the tab to move/resize.'
	);

	// Button hidden by default
	jQuery('.bPublish', w).click(function(){
		if( confirm('Are you sure you want to publish these changes?') )
			jQuery.ajax({
				url: '/ta/charts/ajax.chart.php?fn=publishChart&viewid='+o.id,
				dataType: 'json',
				success: function(data){
					alert( data && data.error ? data.error : 'Chart published successfully.' );
					updateDbChart( o.id );
					/*if( textStatus == 'success' ) {
						alert( 'Chart published successfully.' );
						updateDbChart( o.id );
					} else alert( 'There was an error publishing your chart. Please try again.' );*/
				}
			});
	}).data( 'ttbody',
		'Charts can only be added to shared dashboards if they have been published, '+
		'and any changes made to a chart will not affect shared dashboards until those changes are published.'
	).hide();

	// Button hidden by default
	jQuery('.bDiscard', w).click(function(){
		if( confirm('You will lose all changes to this chart if you proceed.\nAre you sure you want to discard your changes?') )
			jQuery.ajax({
				url: '/ta/charts/ajax.chart.php?fn=discardChart&viewid='+o.id,
				dataType: 'json',
				success: function(data){
					alert( data && data.error ? data.error : 'Chart discarded successfully.' );
					updateDbChart( o.id );
					/*if( textStatus == 'success' ) {
						alert( 'Changes discarded.' );
						updateDbChart( o.id );
					} else alert( 'There was an error processing your request. Please try again.' );*/
				}
			});
	}).data( 'ttbody',
		'Restore the last published version of this chart.'
	)
	.hide();

	jQuery('.bRefresh', w).click(function(){
		jQuery.ajax({
			url: '/ta/charts/ajax.chart.php?fn=clearCache&viewid='+o.id,
			dataType: 'json',
			success: function(){
				updateDbChart( o.id );
			}
		});
	}).data( 'ttbody',
		'Refresh data and reload this chart.'
	);

	if( o.cacheId > 0 ) {
		jQuery('.bRefresh', w).data( 'ttbody',
			'<i>Cached Data: ' + o.cacheDate + '</i><br />Refresh data and reload this chart.'
		).button( 'option', 'icons', {
			primary: 'ui-icon-clock'
		});
	}

	jQuery('.bEChart', w).click(function(){
		var fcO = jQuery('#eeChart'+o.id+' :FusionCharts').get(0);
		if( fcO && fcO.hasRendered( ) )
			fcO.exportChart( );
	}).data( 'ttbody',
		'Save a copy of the rendered chart.'
	);

	jQuery('.bEData', w).click(function(){
		//window.location.href = '/ta/custom_db/display_graph.php?viewid='+o.id+'&ex=-1';
		window.location.href = '/ta/charts/ajax.chart.php?fn=exportData&viewid='+o.id;
	}).data( 'ttbody',
		'Save a copy of the chart data in Excel format.'
	);

	jQuery('.bSettings', w).click(function(){
		jQuery('#editChartDialog')
			.dialog('option', 'title', 'Edit Chart')
			.load('/ta/charts/ajax.chart.php?fn=editChart&viewid='+o.id)
			.dialog('open');
	}).data( 'ttbody',
		'Change chart settings, such as data sources and chart type.'
	);

	jQuery('.bDelete', w).click(function(){
		if( !confirm( 'Are you sure you want to delete this chart?' ) )
			return;

		jQuery.ajax({
			url: '/ta/charts/ajax.chart.php?fn=deleteChart&viewid='+o.id,
			dataType: 'json',
			success: function(){
				w.remove( );
				fixDashboardHeight( );
			}
		});
	}).data( 'ttbody',
		'Permanently delete the chart -- this cannot be undone!'
	);

	// Button hidden by default
	jQuery('.bRemove', w).click(function(){
		if( !confirm( 'Are you sure you want to remove this chart from this dashboard?' ) )
			return;

		jQuery.ajax({
			url: '/ta/charts/ajax.chart.php?fn=removeChart&viewid='+o.id+'&groupid='+getDashboardGroup(),
			dataType: 'json',
			success: function(){
				w.remove( );
				fixDashboardHeight( );
			}
		});
	}).data( 'ttbody',
		'Remove the chart from this dashboard. The chart itself will remain unchanged.'
	)
	.hide();

	if( typeof( o.ctype ) == 'undefined' || o.ctype == '' )
		return false;

	if( getDashboardGroup() == 'custom' ){
		if( !o.ptime )
			jQuery('.eeChartHeader',w).addClass('eeChartUnpub ui-state-highlight');

		if( o.ptime && o.ptime < o.mtime )
			jQuery('.eeChartHeader',w).addClass('eeChartModified ui-state-highlight');
	}

	// Switch buttons for non-edit view
	if( getDashboardGroup() != 'custom' ){
		jQuery('.bSettings, .bDelete',w).hide();
		jQuery('.bRemove',w).show();
	}

	// Read-only dashboards
	if( jQuery('#dashboardOwner').val() != 1 ){
		jQuery('.bLock, .bRemove',w).hide();
	}

	jQuery('.eeChartUnpub .bPublish',w).show();
	jQuery('.eeChartModified .bPublish',w).show();
	jQuery('.eeChartModified .bDiscard',w).show();

	jQuery('.tt').tooltip( tooltipOptions );

	jQuery('.eeChartArea',w).data('eeChart',jQuery.extend({
		viewid: o.id,
		graphname: o.title,
		graphtype: o.ctype
	},o)).prepend(
		"<div id='eeChart"+o.id+"' class='eeChart'></div>"
	);

	insertFC( w, o );
}

function updateDashboardOrder( id ) {
	if( dashboardDefault )
		return;

	var dbOrder = getDashboardOrder( id );

	jQuery.ajax({
		url: '/ta/charts/ajax.chart.php?fn=updateOrder',
		data: { graphs: dbOrder, groupid: getDashboardGroup() }
	});
}

function getDashboardOrder( id ) {
	var dbOrder = new Object;
	var counter = 0;
	var dbSideArray = { 'dashboard_left': 1, 'dashboard_right': 2 };
	var dbSide = dbSideArray[id];

	var getInfo = function(){
		var chartId = jQuery(this).attr('id');
		chartId = chartId.substring( 5, chartId.length );
		var dbHeight = jQuery('.eeChartArea',this).height();
		dbOrder[chartId] = { order: ++counter, side: dbSide, height: jQuery('.eeChartArea',this).height() };
	};

	jQuery('#'+id).children().each( getInfo );

	return dbOrder;
}

function fixDashboardHeight( ) {
	var g = jQuery('.eeChartWindow');
	if( g.length == 0 )
		jQuery('#dashboard_message').html('<p>There are no charts on this dashboard!</p>');
	else jQuery('#dashboard_message').empty();

	jQuery('#dashboard_left, #dashboard_right').height('auto');

	var lh = jQuery('#dashboard_left').height();
	var rh = jQuery('#dashboard_right').height();
	var h = lh > rh ? lh : rh;

	jQuery('#dashboard_left, #dashboard_right').height( h );
}

function updateSortables( ){
	var sortableOptions = {
		update: function(event, ui) {
			updateDashboardOrder( event.target.id );
		},
		placeholder: 'ui-state-highlight eeChartPlaceholder',
		tolerance: 'pointer',
		handle: "div.eeChartHeader",
		items: "div.eeChartContainer",
		cancel: ".eeLocked",
		revert: true,
		start: function(event,ui){
			jQuery(':FusionCharts',ui.item).remove( );
			jQuery('div.eeChartPlaceholder').height( ui.item.height() );
		},
		stop: function(event,ui){
			fixDashboardHeight( );
			var o = jQuery('.eeChartArea',ui.item).data('eeChart');
			insertFC( ui.item, o );
		}
	};

	jQuery('#dashboard_left, #dashboard_right').sortable(jQuery.extend({},sortableOptions,{
		connectWith: '.connectedDashboard'
	})).disableSelection( );
}

function reloadChart( id ){
	var c = jQuery('#chart'+id);
	jQuery(':FusionCharts',c).remove( );
	var o = jQuery('.eeChartArea',c).data('eeChart');
	insertFC( c, o );
}

var fcF, fcX;

function createFCExport( ){
	fcF = jQuery('#saveChartFrame');

	fcF.addClass('ui-widget ui-widget-content ui-fixed-bottom-right');
	fcF.html('<div class="ui-widget-header ui-corner-top ui-corner-top-left">'
			+'<span class="ui-dialog-title">Export Charts</span>'
			+'<span id="fcXClose">close</span>'
			+'</div><div id="fcX">&nbsp;</div>');
	fcF.width( jQuery(document).width() * 0.4 );

	jQuery('#fcXClose',fcF).button({
		icons: {primary: 'ui-icon-closethick' },
		text: false
	}).click(function(){
		fcF.hide();
	}).css({ float: 'right' });

	fcX = new FusionChartsExportObject('fcExporter1','/assets/fcswf/FCExporter.swf');
	fcX.componentAttributes.fullMode = '1';
	fcX.componentAttributes.saveMode = 'both';
	fcX.componentAttributes.showAllowedTypes = '1';
	fcX.componentAttributes.width = fcF.width();
	fcX.componentAttributes.height = '200';
	fcX.Render('fcX');

	fcF.hide();
}

function FC_ExportReady( DOMId ){
	fcF.show();
}

function updateDbChart( id ) {
	jQuery.ajax({
		url: '/ta/charts/ajax.chart.php?fn=getJson&viewid='+id+'&groupid='+getDashboardGroup(),
		dataType: 'json',
		cache: false,
		success: function( data ){
			if( data.error ){
				alert( 'updateDbChart: '+data.error );
				return;
			}
			var w = jQuery('#chart'+id);

			if( w.length > 0 ) {
				w.wrap('<div id="dashboard_replace" />');
				jQuery('#dashboard_replace').empty();
				w = createChartWindow(jQuery.extend({},data,{
					size: '3'
				}));
				w.unwrap();
			} else {
				w = createChartWindow( data );
			}
			addChart( w, data );

			updateDashboardOrder( jQuery('#chart'+id).parent().attr('id') );
			fixDashboardHeight( );

			jQuery('html').animate({
				scrollTop: w.offset().top - 12
			});
		}
	});
}

function createDbButton( o ) {
	jQuery('<span id="b'+o.id+'">'+o.name+'</span>').appendTo('#dashboardButtons');
	jQuery('#b'+o.id).button({
		icons: {primary: o.icon}
	}).click(function(){
		jQuery('#'+o.dialog)
			.dialog('option', 'title', o.name)
			.load('/ta/charts/ajax.chart.php?fn='+o.fn)
			.dialog('open');
	});
}

function createDashboard( ) {
	jQuery('html').animate({
		scrollTop: jQuery('#dashboard_header').offset().top - 6
	});

	var id = jQuery('#dashboardGroup').val();
	var msg = jQuery('#dashboard_message');

	jQuery('#dashboard_charts').children(':not(#dashboard_spacer)').add('#dashboardButtons').empty();
	msg.html('<p>Loading dashboard...</p>');

	jQuery.ajax({
		url: '/ta/charts/ajax.chart.php?fn=getDashboard&groupid='+id,
		dataType: 'json',
		cache: false,
		success: function( data ){
			if( data.error ){
				alert( 'createDashboard: '+data.error );
				return;
			}
			
			if( !dashboardEmbedded ) {
				if( data.dbWho && data.dbWho.length ) {
					dashboardDefault = true;
					dashboardWho = data.dbWho;
				} else {
					dashboardDefault = false;
					dashboardWho = '';
				}
			}
			
			var w = null, i;
			jQuery('#dashboardOwner').val( data.owner );

			for( i = 0; typeof( data.charts[i] ) != 'undefined'; i++ ) {
				w = createChartWindow( data.charts[i] );
				addChart( w, data.charts[i] );
			}

			if( !dashboardDefault && data.owner == 1 )
				for( i = 0; typeof( data.buttons[i] ) != 'undefined'; i++ ) {
					createDbButton( data.buttons[i] );
				}

			fixDashboardHeight( );

			if( w === null ) {
				msg.html('<p>There are no charts on this dashboard!</p>');
			} else msg.empty();

			_runInit( );
		}
	});
}

function createDbMenu( ) {
	var msg = jQuery('#dashboard_message');
	var m = jQuery('#dashboardMenu');
	m.empty( );

	msg.html('<p>Loading menus...</p>');

	jQuery.ajax({
		url: '/ta/charts/ajax.chart.php?fn=getGroupList',
		dataType: 'json',
		cache: false,
		success: function( data ){
			if( data.error ){
				alert( 'createDbMenu: '+data.error );
				return;
			}

			jQuery('#dashboardMenu').empty();

			var i, j, g = getDashboardGroup();
			var printOption = function( elem, d ){
				jQuery(elem)
					.append('<option value="'+
						d.groupid+
						'"'+
						( d.groupid == g ? ' selected' : '' )+
						'>'+
						d.groupname+
						'</option>'
					);
			};

			if( ObjSize( data.groups[2] ) > 0 )
				for( i = 1; typeof( data.groups[2][i] ) != 'undefined'; i++ )
					printOption( '#dashboardMenu', data.groups[2][i] );

			if( ObjSize( data.groups[1] ) > 0 ){
				jQuery('#dashboardMenu').append('<optgroup label="My Dashboards" id="dashboardMenuOwner" />');
				for( i = 1; typeof( data.groups[1][i] ) != 'undefined'; i++ )
					printOption( '#dashboardMenuOwner', data.groups[1][i] );
			}

			if( ObjSize( data.groups[0] ) > 0 ){
				var target = '#dashboardMenu';
				if( ObjSize( data.groups[2] ) > 0 || ObjSize( data.groups[1] ) > 0 ){
					jQuery('#dashboardMenu').append('<optgroup label="Shared Dashboards" id="dashboardMenuShared" />');
					target = '#dashboardMenuShared';
				}
				for( i = 1; typeof( data.groups[0][i] ) != 'undefined'; i++ )
					printOption( target, data.groups[0][i] );
			}

			/*for( j = 0; j < 3; j++ ){
				for( i = 1; typeof( data.groups[i] ) != 'undefined'; i++ ) {
					jQuery('#dashboardMenu')
						.append('<option value="'+
								data.groups[i].groupid+
								'"'+
								( data.groups[i].groupid == g ? ' selected' : '' )+
								'>'+
								data.groups[i].groupname+
								'</option>');
				}
			}*/

			jQuery('#dashboardMenu option[value=add]').addClass('menu-icon-plus');

			jQuery('#dashboardMenu').selectmenu({
				style: 'dropdown',
				width: 250,
				maxHeight: 400,
				icons: [
					{ find: '.menu-icon-plus', icon: 'ui-icon-plus' }
				],
				change: function(event, ui){
					switch( ui.value ) {
						case 'add':
							jQuery(this).selectmenu( 'value', getDashboardGroup() );

							jQuery('#editDbDialog')
								.dialog('option', 'title', 'New Dashboard Group')
								.load('/ta/charts/ajax.chart.php?fn=newGroup')
								.dialog('open');
							break;

						default:
							jQuery('#dashboardGroup').val( ui.value );
							createDashboard( );
							break;
					}
				}
			});

			msg.empty( );

			_runInit( );
		}
	});
}

function getDefaultGroup( ) {
	var dbGroup = jQuery('#dashboardGroup');

	if( dbGroup.val() > 0 )
		return _runInit( );

	jQuery.ajax({
		url: '/ta/charts/ajax.chart.php?fn=getDefaultGroup',
		dataType: 'json',
		cache: false,
		success: function( data ){
			if( data.error ){
				alert( 'getDefaultGroup: '+data.error );
				return;
			}
			
			if( data.groupid ){
				dbGroup.val( data.groupid );
				_runInit( );
			} else {
				jQuery('#dashboard_header').hide();
				jQuery('#dashboard_message').html('<p>Access Denied</p>');
			}
		}
	});
}

function getGroupId( ) {
	var dbGroup = jQuery( '#dashboardGroup' );
	var groupName = dbGroup.val( );

	jQuery.ajax({
		url: '/ta/charts/ajax.chart.php?fn=getGroupId&groupname=' + groupName,
		dataType: 'json',
		cache: false,
		success: function( data ) {
			if( data.error ){
				alert( 'getGroupId: '+data.error );
				return;
			}

			if( data.groupid ){
				dbGroup.val( data.groupid );
				_runInit( );
			} else {
				jQuery('#dashboard_message').html('<p>Access Denied</p>');
			}
		}
	});
}

function getDashboardGroup( ) {
	var dbGroup = jQuery('#dashboardGroup');
	return dbGroup.val();
}

function setDashboardGroup( groupid ) {
	var dbGroup = jQuery('#dashboardGroup');
	dbGroup.val( groupid );
}

function initializeDashboard( ) {
	jQuery('#editChartDialog').dialog({
		modal: true,
		draggable: false,
		resizable: false,
		autoOpen: false,
		width: '55%',
		height: '320',
		close: function(){
			if( jQuery(this).data('dirty') == true ) {
				updateDbChart( jQuery(this).data('viewid') );
				jQuery(this).data( 'dirty', false );
			}
		}
	});

	jQuery('#editDbDialog').dialog({
		modal: true,
		draggable: false,
		resizable: false,
		autoOpen: false,
		width: '55%',
		height: '160',
		close: function(){
			if( jQuery(this).data('dirty') == true ) {
				createDbMenu( );
				jQuery(this).data( 'dirty', false );
			}
		}
	});

	_runInit();
}

var dbInit;
var dbInitDefault = [ getDefaultGroup, createDbMenu, createDashboard ];
var dbInitStatus = -1;
var DB_INIT_COMPLETE = 99;

function _runInit( initList ) {
	if( dbInitStatus == DB_INIT_COMPLETE )
		return;

	if( typeof( dbInit ) == 'undefined' ) {
		if( typeof( initList ) == 'undefined' ) {
			dbInit = dbInitDefault;
		} else {
			dbInit = initList;
		}
	}

	if( ++dbInitStatus == dbInit.length ) {
		dbInitStatus = DB_INIT_COMPLETE;
		return;
	}

	dbInit[dbInitStatus]();
}

function initializeAdvancedSettings( viewid ) {
	/*jQuery('#trendline_check').change(function(){
		jQuery('#trendline').val( jQuery('#trendline_check').attr('checked') );
		jQuery('.trendOption').attr( 'disabled', jQuery('#trendline').val()=='false' );
	}).attr( 'checked', jQuery('#trendline').val()=='true' );
	jQuery('.trendOption').attr( 'disabled', jQuery('#trendline').val()=='false' );*/

	jQuery('#advancedButton').button({
		icons: {primary: 'ui-icon-plus'},
		text: false
	});
	jQuery('#advancedSettings').data( 'open', false );
	jQuery('.advancedHeader').css({ cursor: 'pointer' }).click(function(){
		if( jQuery('#advancedSettings').data('open') ) {
			jQuery('#advancedSettings').data( 'open', false ).hide();
			jQuery('#advancedButton').button( 'option', 'icons', {primary:'ui-icon-plus'} );
		} else {
			jQuery('#advancedSettings').data( 'open', true ).show();
			jQuery('#advancedButton').button('option','icons',{primary:'ui-icon-minus'});
		}
	});

	getTrendlines( '#trendlines', viewid );

	var t=function(n){return '#trendForm input[name='+n+']';};
	var s=function(n){return '#trendForm select[name='+n+']';};

	jQuery('#trendlines').change(function(){
		var index = jQuery(this).val();
		jQuery('.trendOption').attr( 'disabled', false );
		jQuery('#trendForm').resetForm();
		if( index == 'new' ) {
			jQuery('#deleteTrendline').attr( 'disabled', true );
			jQuery(t('trendid')).val( 'new' );
			jQuery(t('lineColor')).miniColors( 'value', '#ff0000' );
		} else {
			var d=jQuery(this).data('trends');
			jQuery(t('lineStart')).val( d[index].lineStart );
			jQuery(t('lineEnd')).val( d[index].lineEnd );
			jQuery(t('trendCaption')).val( d[index].trendCaption );
			jQuery(t('lineWidth')).val( d[index].lineWidth );

			//jQuery(t('lineColor')).val( '#'+d[index].lineColor );
			jQuery(t('lineColor')).miniColors( 'value', '#'+d[index].lineColor );

			//jQuery(s('lineType')+' option:selected').removeAttr('selected');
			//jQuery(s('lineType')+' option[value='+d[index].lineType+']').attr('selected','selected');
			jQuery(s('linePosition')+' option:selected').removeAttr('selected');
			jQuery(s('linePosition')+' option[value='+d[index].linePosition+']').attr('selected','selected');

			jQuery(t('trendid')).val( d[index].trendid );
		}
	});

	jQuery(t('lineColor')).miniColors();

	jQuery('#deleteTrendline').click(function(){
		if( confirm( 'Are you sure you want to delete this trendline?' ) )
			jQuery.ajax({
				url: '/ta/charts/ajax.chart.php?fn=deleteTrendline&viewid='+jQuery(t('viewid')).val()+
					'&trendid='+jQuery(t('trendid')).val(),
				dataType: 'json',
				success: reloadDialogJSON
			});
		return false;
	});

	jQuery('.trendOption').attr( 'disabled', true );

	jQuery('#advancedSettings').hide();
}

function getTrendlines( selector, viewid ) {
	jQuery.ajax({
		url: '/ta/charts/ajax.chart.php?fn=getTrendlines&viewid='+viewid,
		dataType: 'json',
		success: function( data ){
			if( !data || !data.trends || data.error ) {
				alert( data.error || 'There was an error processing your request.' );
				return false;
			}

			jQuery(selector).empty();
			jQuery(selector).data( 'trends', data.trends );
			for( var i in data.trends )
				jQuery(selector).append('<option value="'+i+'">'+data.trends[i].name+'</option>');

			jQuery(selector).append('<option value="new">(add new trendline)</option>');

			if( data.trends.length )
				jQuery('#advancedButton').click();
		}
	});
}

















