var ajaxUrl = 'ajax_pos.php';

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}

function doSetTimeout(m,ggAmt,ggAvg){
	setTimeout(function() {
		var lastGraph = jQuery('#currentGraph').attr( 'value' );
		lastGraph = parseInt(lastGraph) + 1;
		jQuery('#currentGraph').val(lastGraph);
		var height = Math.round(ggAmt / 3000 * 300);
		//var diff = 150 - height;
		var height2 = Math.round(ggAvg / 500 * 300 - height);
		var bgcolor = '#33FF00';
		var bgcolor2 = '#04B404';
		
		if(height2 < 0){
			height = height + height2;
			height2 = Math.abs(height2);
			bgcolor = '#00FFFF';
			bgcolor2 = '#3399CC';
		}
		
		//if(height < 1){height = 1;}
		//if(height2 < 1){height2 = 1;}
		
		var showAvg = (ggAvg / 100).toFixed(2);

		jQuery( '#graph' ).append("<td width=1 id=graph" + lastGraph  + "><div id='avg" + lastGraph + "' title=CheckAvg:" + showAvg + " style=\"background-color:" + bgcolor + ";height:" + height2 + "px;width:2px;opacity:0.8;\"></div><div id='cust" + lastGraph + "' title=Customers:" + ggAmt + " style=\"background-color:#0099CC;height:" + height + "px;width:2px;opacity:0.8;\"></div></td>");
		jQuery( '#avg' + lastGraph ).css("background-color",bgcolor2);
		jQuery( '#avg' + lastGraph ).animate({backgroundColor: bgcolor}, 15000);
		jQuery( '#cust' + lastGraph ).css("background-color","#0101DF");
		jQuery( '#cust' + lastGraph ).animate({backgroundColor: '#0099CC'}, 15000);
		
		//remove old
		if(lastGraph >= 601){
			var removeid = lastGraph - 600;
			jQuery( '#graph' +  removeid).remove();
		}
	}, 1400 * m);
}

function doSetTimeoutCC(inc, tempCount, tempAmt){
	setTimeout(function() {
		jQuery( '#custCount' ).html(tempAmt);
	}, inc * tempCount);
}

function doSetTimeoutCA(inc, tempCount, tempAmt){
	setTimeout(function() {
		jQuery( '#checkAvg' ).html("$" + (tempAmt / 100).toFixed(2));
	}, inc * tempCount);
}

function doSetTimeoutStocks(id, counter, text){
	setTimeout(function() {
		jQuery( id ).css("background-color","yellow");
		jQuery( id ).html(text);
		jQuery( id ).animate({backgroundColor: '#ffffff'}, 750);
	}, counter * 50);
}

function doSetTimeoutItemHist(counter, newHeight){
	setTimeout(function() {
		jQuery( '#day' + counter ).animate({height: newHeight}, 750);
	}, counter * 10);
}

function doSetTimeoutTrendData(counter, newHeight){
	setTimeout(function() {
		jQuery( '#trend' + counter ).animate({height: newHeight}, 750);
	}, counter * 10);
}

function doSetTimeoutTrendDetails(counter, newWidth){
	setTimeout(function() {
		jQuery( '#trendD' + counter ).animate({width: newWidth + '%'}, 750);
	}, counter * 10);
}

function clearItems(){
	jQuery( '#graphTitle' ).html("History &nbsp;&nbsp; <div onclick=\"showTrendOptions();\" style=\"display:inline;float:right;\"><font color=blue size=2>&nbsp;&nbsp;TREND&nbsp;&nbsp;</font></div> &nbsp;&nbsp; <div onclick=\"searchItems();\" style=\"display:inline;float:right;\"><font color=blue size=2>&nbsp;&nbsp;SEARCH&nbsp;&nbsp;</font></div> &nbsp;&nbsp;");
	jQuery( '#graphMain' ).css("width","");
	jQuery( '#graph' ).css("display","");
	jQuery( '#graphItem' ).css("display","none");
	jQuery( '#searchItem' ).css("display","none");
	jQuery( '#trendData' ).css("display","none");
	jQuery( '#trendOptions' ).css("display","none");
	jQuery( '#trendDetail' ).css("display","none");
	jQuery( '#trendDetailData' ).css("display","none");	
}

function searchItems(){
	jQuery( '#graph' ).css("display","none");
	jQuery( '#graphItem' ).css("display","none");
	jQuery( '#searchItem' ).css("display","");
	jQuery( '#graph' ).css("display","none");
	jQuery( '#trendData' ).css("display","none");
	jQuery( '#graphMain' ).css("width","100%");
}

function clearFilter(){
	jQuery( '#searchInput' ).val('');
	jQuery('.searchAble').each(function(index, data) {
		jQuery(this).css("display","");
	});
}

function filterList(){
	var search = jQuery( '#searchInput' ).val();
	search = search.toLowerCase();
	if(search.length > 2){
		jQuery('.searchAble').each(function(index, data) {
			if(jQuery(this).html().toLowerCase().indexOf(search) < 0){
				jQuery(this).css("display","none");
			}
			else{
				jQuery(this).css("display","block");
			}
		});
	}
}

function graphItem(itemid, itemName){
	jQuery( '#graphMain' ).css("width","100%");
	jQuery( '#graph' ).css("display","none");
	jQuery( '#trendData' ).css("display","none");
	jQuery( '#trendOptions' ).css("display","none");
	jQuery( '#graphItem' ).css("display","");
	jQuery( '#graphItem' ).html('');
	jQuery( '#graphTitle' ).html(itemName + " History &nbsp;&nbsp; <div onclick=\"searchItems();\" style=\"display:inline;float:right;\"><font color=blue size=2>&nbsp;&nbsp;SEARCH&nbsp;&nbsp;</font></div> &nbsp;&nbsp; <div onclick=\"clearItems();\" style=\"display:inline;float:right;\"><font color=blue size=2>&nbsp;&nbsp;CLOSE&nbsp;&nbsp;</font></div>");
	
	jQuery.ajax( {
		type: "POST",
		url: ajaxUrl,
		data: "todo=31&itemid=" + itemid,
		success: function( r ) {
			r = r.replace("<pre>","");
			var obj = JSON.parse(r);
			var date;
			var counter = 1;
			var max = 0;
			var height = 0;
			var day;
			var month;

			for(date in obj){
				if(parseInt(obj[date]) > max){
					max = parseInt(obj[date]);
				}
			}

			for(date in obj){
				day = date.substring(8,10);
				month = date.substring(5,7);
				height = Math.round(285 / max * obj[date]);
				jQuery( '#graphItem' ).append('<td><div id="day' + counter + '" ' + 'title=' + obj[date] + ' style="background-color:#0099CC;height:' + 0 + 'px;width:90%;opacity:0.8;"></div><center><font size=1>' + day + '&nbsp;</font></center></td>');
				doSetTimeoutItemHist(counter, height);	
				counter++;
			}
		}
	} );
}

function showTrendOptions(){
	var businessid = jQuery( '#businessid' ).val();
	
	jQuery( '#graph' ).css("display","none");
	jQuery( '#searchItem' ).css("display","none");
	jQuery( '#graphItem' ).css("display","none");
	jQuery( '#graphTitle' ).html("Trend Options &nbsp;&nbsp; <div onclick=\"clearItems();\" style=\"display:inline;float:right;\"><font color=blue size=2>&nbsp;&nbsp;CLOSE&nbsp;&nbsp;</font></div>");
	jQuery( '#trendOptions' ).css("display","");
}

function setTrendOptions(cur_id, cur_value){

	jQuery( '#' + cur_id ).val(cur_value);

	if(cur_id == "currentTrend"){
		jQuery(".trends").each(function() {
			jQuery( this ).css("background","white");
		});
		jQuery( '#' + cur_value + "Trend").css("background","yellow");
	}
	else if(cur_id == "currentTrendUnit"){
		jQuery(".units").each(function() {
			jQuery( this ).css("background","white");
		});
		jQuery( '#' + cur_value ).css("background","yellow");
	}

	trendData();
}

function trendData(){
	jQuery( '#graphMain' ).css("width","100%");
	jQuery( '#trendData' ).css("display","");
	jQuery( '#trendData' ).html('');
	
	var trend = jQuery( '#currentTrend' ).val(); 
	var unit = jQuery( '#currentTrendUnit' ).val();
	var units = unit.split("-");
	
	var color = "#00CC33";
	
	if(trend == "sales"){
		trendTitle = "Sales";
		color = "#00CC33";
	}
	else if(trend == "cos"){
		trendTitle = "Cost of Sales";
		color = "#0033FF";
	}
	else if(trend == "waste"){
		trendTitle = "Waste";
		color = "#FF0000";
	}
	else if(trend == "shrink"){
		trendTitle = "Shrink";
		color = "#CCCC00";
	}
	else if(trend == "customers"){
		trendTitle = "Customer";
		color = "#FF6600";
	}
	
	jQuery( '#graphTitle' ).html(trendTitle + " Trend &nbsp;&nbsp; <div onclick=\"clearItems();\" style=\"display:inline;float:right;\"><font color=blue size=2>&nbsp;&nbsp;CLOSE&nbsp;&nbsp;</font></div>");

	jQuery.ajax( {
		type: "POST",
		url: ajaxUrl,
		data: "todo=34&level=" + units[0] + "&id=" + units[1] + "&trend=" + trend,
		success: function( r ) {
			r = r.replace("<pre>","");
			var obj = JSON.parse(r);
			
			var date;
			var counter = 1;
			var max = 0;
			var height = 0;
			var day;
			//var month;
			//var year;
			var lastheight = 0;
			
			for(date in obj){
				if(parseInt(obj[date]) > max){
					max = parseInt(obj[date]);
				}
			}
			
			for(date in obj){
				day = date.substring(8,10);
				//month = date.substring(5,7);
				height = Math.round(285 / max * obj[date]);
				//console.log(date);
				jQuery( '#trendData' ).append('<td><div id="trend' + counter + '" ' + 'title=' + obj[date] + ' style="background-color:' + color + ';height:' + 0 + 'px;width:95%;opacity:0.8;" onclick="trendDetails(\'' + date.toString() + '\');"></div><center><font size=1>' + day + '&nbsp;</font></center></td>');
				doSetTimeoutTrendData(counter, height);	
				counter++;
			}
		}
	});
}

function trendDetails(date){
		var trend = jQuery( '#currentTrend' ).val();
        var unit = jQuery( '#currentTrendUnit' ).val();
        var units = unit.split("-");

        var color = "#00CC33";

        if(trend == "sales"){
                trendTitle = "Sales";
                color = "#00CC33";
        }
        else if(trend == "cos"){
                trendTitle = "Cost of Sales";
                color = "#0033FF";
        }
        else if(trend == "waste"){
                trendTitle = "Waste";
                color = "#FF0000";
        }
        else if(trend == "shrink"){
                trendTitle = "Shrink";
                color = "#CCCC00";
        }
		else if(trend == "customers"){
			trendTitle = "Customer";
			color = "#FF6600";
		}

		//only if company or district
		if(units[0] == "c" || units[0] == "d"){
			jQuery.ajax( {
                	type: "POST",
               		url: ajaxUrl,
                	data: "todo=35&date=" + date + "&trend=" + trend + "&level=" + units[0] + "&id=" + units[1],
                	success: function( r ) {
						r = r.replace("<pre>","");
									var obj = JSON.parse(r);

						//jQuery( '#trendData' ).css("display","none");
						jQuery( '#trendDetail' ).css("display","");
						jQuery( '#trendDetailData' ).css("display","");               
					
						var count;
						var counter = 1;
						var max = 0;
						
						//find max
						for(count in obj){
							if(parseInt(obj[count]['value']) > max){
								max = parseInt(obj[count]['value']);
							}
						}
						
						var display = "<table width=100%><tbody>";

						for(count in obj){
							display = display + '<tr><td width=30%>' + obj[count]['name'] + '</td><td width=70%><div id="trendD' + counter + '" ' + 'title=' + obj[count]['value'] + ' style="background-color:' + color + ';height:' + 15 + 'px;width:0%;opacity:0.8;"></div></td></tr>';
							counter++;
						}

						display = display + "</tbody></table>";

						jQuery( '#trendDetailData' ).html(display);
						
						//animate
						var width = 0;
						counter = 1;
						for(count in obj){
							width = Math.round(parseInt(obj[count]['value']) / max * 100);
							doSetTimeoutTrendDetails(counter, width);	
							counter++;
						}
					}
        	});
		}
}

function pastMetrics(interval, level, businessid, companyid){
	jQuery.ajax( {
		type: "POST",
		url: ajaxUrl,
		data: "todo=29&level=" + level + "&businessid=" + businessid + "&companyid=" + companyid + "&interval=" + interval,
		success: function( r ) {
			var temp = r.split(":");
			var newAmt = parseInt(temp[0]);
			var newAvg = parseFloat(temp[1]);

			newAvg = Math.round(newAvg / newAmt * 100) / 100;
			newAvg = newAvg.toFixed(2);
			newAmt = Math.round(newAmt / 2);
			
			if(interval == 2){
				jQuery( '#ccly' ).html(newAmt);
				jQuery( '#caly' ).html('$' + newAvg);
			}
			else{
				jQuery( '#cclw' ).html(newAmt);
				jQuery( '#calw' ).html('$' + newAvg);
			}
		}
	});
}

function updateMarqueeText(){
	jQuery.ajax( {
		type: "POST",
		url: ajaxUrl,
		data: "todo=33",
		success: function( r ) {
			r = r.replace("<pre>","");
			var obj = JSON.parse(r);
			var counter;
			var info = '';
			
			for(counter in obj){
				info = info + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + obj[counter];
			}
			
			var width = jQuery("#ticker").width();
			
			info = '<center><marquee style="width:' + width + 'px;">' + info + '</marquee></center>';
			
			jQuery( '#ticker' ).html(info);
		}
	});
}

function realtimeUpdate(){	
	var businessid = jQuery( '#businessid' ).val();
	var companyid = jQuery( '#companyid' ).val();
	var startlevel = jQuery( '#level' ).val();

	jQuery( '#start' ).attr( 'disabled', 'disabled' );
	
	setTimeout(function() {
		jQuery( '#startContent' ).html("Loading Data...");
	}, 1000);
	
	setTimeout(function() {
		jQuery( '#startContent' ).html("Getting Menu Items...");
	}, 5000);
	
	setTimeout(function() {
		jQuery( '#startContent' ).html("Scanning Checks...");
	}, 10000);
	
	setTimeout(function() {
		jQuery( '#startContent' ).html("");
	}, 16000);
	
	setTimeout(function() {
		updateMarqueeText();
	}, 10000);
	
	setInterval(function() {
		updateMarqueeText();
	}, 300000);
	
	//past metrics
	pastMetrics(2, startlevel, businessid, companyid);
	pastMetrics(1, startlevel, businessid, companyid);
	
	setInterval(function() {
		var level = jQuery( '#level' ).val();
		pastMetrics(2, level, businessid, companyid);
		pastMetrics(1, level, businessid, companyid);
	}, 28000);
	
	//get master menu items
	jQuery.ajax( {
		type: "POST",
		url: ajaxUrl,
		data: "todo=32",
		success: function( r ) {
			r = r.replace("<pre>","");
			var obj = JSON.parse(r);
			var count;

			for(count in obj){
				obj[count] = obj[count].replace("'", "");
				jQuery('#searchItems').append('<div onclick=\"graphItem(' + count + ',\'' + obj[count] + '\');\" class=\"searchAble\">' + obj[count] + '</div>');
			}
		}
	} );

	setInterval(function() {
		var ordertype = jQuery( '#ordertype' ).val();
		var level = jQuery( '#level' ).val();
		
		jQuery.ajax( {
			type: "POST",
			url: ajaxUrl,
			data: "todo=28&ordertype=" + ordertype + "&level=" + level + "&businessid=" + businessid + "&companyid=" + companyid,
			success: function( r ) {
				r = r.replace("<pre>","");
				var obj = JSON.parse(r);
				
				//loop
				var count;
				var counter = 1;
				var value;
				var amt;
				var moveDown = new Array();

				for(count in obj){
					value = jQuery( '#item' + counter ).html();

					if(value == obj[counter]['name']){
						amt = jQuery( '#count' + counter ).html();
						
						if(amt != obj[counter]['total']){
							setTimeout(function(x,itemAmt) {
								return function() { 
									jQuery( '#count' + x ).css("background-color","yellow");
									jQuery( '#count' + x ).html(itemAmt);
									jQuery( '#count' + x ).animate({backgroundColor: '#ffffff'}, 2000);
								}; 
							}(counter,obj[counter]['total']), 1000 * (11 - counter));
						}
					}
					else{	
						moveDown[value] = 1;
					
						setTimeout(function(x,item,itemAmt,bumped,cat_rank,rank) {
							return function() { 
								//remove item
								var s = 0;
								for(var i = x + 1; i <= 10; i++){
									if(jQuery( '#item' + i ).html() == item){
										jQuery( '#item' + i ).css("background-color","#0099CC");
										jQuery( '#item' + i ).html("-");
										jQuery( '#item' + i ).animate({backgroundColor: '#ffffff'}, 900);
										
										jQuery( '#count' + i ).css("background-color","#0099CC");
										jQuery( '#count' + i ).html("-");
										jQuery( '#count' + i ).animate({backgroundColor: '#ffffff'}, 900);
										
										jQuery( '#CR' + i ).css("background-color","#0099CC");
										jQuery( '#CR' + i ).html("-");
										jQuery( '#CR' + i ).animate({backgroundColor: '#ffffff'}, 900);

										jQuery( '#OR' + i ).css("background-color","#0099CC");
										jQuery( '#OR' + i ).html("-");
										jQuery( '#OR' + i ).animate({backgroundColor: '#ffffff'}, 900);

										s = 1;
									}
								}
								
								if(s == 1){
									var color = "#0099CC";
								}
								else if(item in bumped && item != "-"){
									var color = "#FF3366";
								}
								else{
									var color = "#33FF00";
								}
								
								//clear ranks
								if(cat_rank == 0){
									cat_rank = "-";
								}
								if(rank == 0){
									rank = "-";
								}
							
								jQuery( '#item' + x ).css("background-color",color);
								jQuery( '#item' + x ).html(item);
								jQuery( '#item' + x ).animate({backgroundColor: '#ffffff'}, 4000);
								
								jQuery( '#count' + x ).css("background-color",color);
								jQuery( '#count' + x ).html(itemAmt);
								jQuery( '#count' + x ).animate({backgroundColor: '#ffffff'}, 4000);

								jQuery( '#CR' + x ).css("background-color",color);
								jQuery( '#CR' + x ).html(cat_rank);
								jQuery( '#CR' + x ).animate({backgroundColor: '#ffffff'}, 4000);

								jQuery( '#OR' + x ).css("background-color",color);
								jQuery( '#OR' + x ).html(rank);
								jQuery( '#OR' + x ).animate({backgroundColor: '#ffffff'}, 4000);
							}; 
						}(counter,obj[counter]['name'],obj[counter]['total'],moveDown,obj[counter]['cat_rank'],obj[counter]['rank']), 1000 * counter);
					}

					counter++;
				}
				
				//clear
				while(counter <= 10){
					jQuery( '#item' + counter ).html("-");
					jQuery( '#count' + counter ).html("-");
					jQuery( '#CR' + counter ).html("-");
					jQuery( '#OR' + counter ).html("-");
					counter++;
				}
			}
		} );
	}, 30000);
	
	setInterval(function() {
		var level = jQuery( '#level' ).val();
	
		//displayed info
		var currentAmt = jQuery( '#custCount' ).html();
		currentAmt = parseInt(currentAmt);
		var currentAvg = jQuery( '#checkAvg' ).html();
		currentAvg = currentAvg.replace("$","");
		currentAvg = parseInt(currentAvg * 100);
		
		//get current
		jQuery.ajax( {
			type: "POST",
			url: ajaxUrl,
			data: "todo=29&level=" + level + "&businessid=" + businessid + "&companyid=" + companyid,
			success: function( r ) {
				var temp = r.split(":");
				var newAmt = parseInt(temp[0]);
				var newAvg = parseFloat(temp[1]);
				newAvg = Math.round(newAvg / newAmt * 100);
				
				if(isNaN(newAvg)){newAvg = 0;}
				
				var diff = Math.abs(newAmt - currentAmt);
				var inc = Math.round(10000 / diff);
				var tempAmt = currentAmt;
				
				//update graph
				var gAmt = currentAmt;
				var gAvg = currentAvg;
				var gAmtDiff = Math.round(Math.abs(newAmt - currentAmt) / 10);
				var gAvgDiff = Math.round(Math.abs(newAvg - currentAvg) / 10);
				
				for(var m = 1; m <= 10; m++){
					//customer diff
					if(gAmt == newAmt){
						//do nothing
					}
					else if(gAmt < newAmt){
						gAmt += gAmtDiff;
						if(gAmt > newAmt){
							gAmt = newAmt;
						}
					}
					else{
						gAmt -= gAmtDiff;
						if(gAmt < newAmt){
							gAmt = newAmt;
						}
					}
					
					//avg diff
					if(gAvg == newAvg){
						//do nothing
					}
					else if(gAvg < newAvg){
						gAvg += gAvgDiff;
						if(gAvg > newAvg){
							gAvg = newAvg;
						}
					}
					else{
						gAvg -= gAvgDiff;
						if(gAvg < newAvg){
							gAvg = newAvg;
						}
					}

					doSetTimeout(m,gAmt,gAvg);
				}
				
				//update customer count
				var tempCount = 1;
				while(tempAmt != newAmt){
					if(tempAmt < newAmt){
						tempAmt++;
					}
					else{
						tempAmt--;
					}
					
					doSetTimeoutCC(inc,tempCount,tempAmt);
				
					tempCount++;
				}
				
				//update check avg
				var diff2 = Math.abs(newAvg - currentAvg);
				var inc2 = Math.round(9000 / diff2);
				var tempAmt2 = currentAvg;
					
				tempCount = 1;
				while(tempAmt2 != newAvg){
					if(tempAmt2 < newAvg){
						tempAmt2++;
					}
					else{
						tempAmt2--;
					}
					
					doSetTimeoutCA(inc2,tempCount,tempAmt2);
				
					tempCount++;
				}
			}
		} );
	}, 17000);
	
	setInterval(function() {
		var ordertype = jQuery( '#ordertype' ).val();
		var stock = parseInt(jQuery( '#currentStock' ).val());
		if(stock == 91){
			jQuery( '#currentStock' ).val(1);
		}
		else{
			jQuery( '#currentStock' ).val(stock + 10);
		}	
		
		//get current
		jQuery.ajax( {
			type: "POST",
			url: ajaxUrl,
			data: "todo=30&ordertype=" + ordertype + "&stock=" + stock,
			success: function( r ) {
				r = r.replace("<pre>","");
				var obj = JSON.parse(r);
				
				var counter = 1;
				var overallCounter = 1;
				var showplus = "+";
				var color = "";
				
				for(count in obj){
					doSetTimeoutStocks('#mmRank' + counter, overallCounter, obj[counter]['rank']);
					overallCounter++;
					
					obj[counter]['name'] = obj[counter]['name'].replace("'", "");

					doSetTimeoutStocks('#mmItem' + counter, overallCounter, '<div onclick="graphItem(' + obj[counter]['id'] + ',\'' + obj[counter]['name'] + '\')" >' + obj[counter]['name'] + '</div>');
					overallCounter++;
					
					var line_counter = 1;
					while(line_counter <= 6){
						doSetTimeoutStocks('#mm' + line_counter + counter, overallCounter, obj[counter][line_counter]);
						overallCounter++;
						
						if(line_counter % 2 == 0){
							if(obj[counter][line_counter] == 0){
								doSetTimeoutStocks('#mm' + line_counter + "Percent" + counter, overallCounter, "0%");
							}
							else if(obj[counter][line_counter-1] == 0){
								doSetTimeoutStocks('#mm' + line_counter + "Percent" + counter, overallCounter, "<font color=#009900>+100%</font>");
							}
							else{
								var percent = ((obj[counter][line_counter] - obj[counter][line_counter-1]) / obj[counter][line_counter - 1] * 100).toFixed(2);
								
								if(percent > 0){
									showplus = "+";
									color = "#009900";
								}
								else{
									showplus = "";
									color = "#FF0000"
								}
								
								doSetTimeoutStocks('#mm' + line_counter + "Percent" + counter, overallCounter, "<font color=" + color + ">" + showplus + percent + "%</font>");
							}
						}
						
						line_counter++;
						overallCounter++;
					}
					
					counter++;
				}
			}
		} );
	}, 45000);
}

function showTrend(highestSales,dateLen,timeLen,t_period2,t_period){
	var colors = new Array("FFFF99","FFFF00","FFCC00","FF9900","FF6600","FF3300","FF0000","00FF00");

	highestSales = highestSales / 6;

	var sales = new Array();
	var i = 0;
	var j = 0;
	var k = 0;

	for (i=0;i<=7;i++){
		sales[i] = highestSales * i;
	}

	var curIdValue = '';
	var curAmount = 0;

	for(i=0;i<timeLen;i++){
		for(k=0;k<dateLen;k++){	
			curIdValue = jQuery( '#' + i + '-' + k ).html();
			curIdValue = curIdValue.replace('$', '');
			curIdValue = curIdValue.replace(',', '');
			curAmount = parseFloat(curIdValue);

			for(j=7;j>=0;j--){
				if(curAmount >= sales[j]){
					jQuery( '#' + i + '-' + k ).attr("bgColor",colors[j]);
					jQuery( '#cust' + i + '-' + k ).attr("bgColor",colors[j]);
					jQuery( '#avg' + i + '-' + k ).attr("bgColor",colors[j]);
					jQuery( '#pct' + i + '-' + k ).attr("bgColor",colors[j]);
					break;
				}
			}
		}
	}
}

function submit_tax( ) {
	$( '#tax_submit' ).attr( 'disabled', 'disabled' );
	
	var tax_name = $( '#new_tax_name' ).attr( 'value' );
	var tax_percent = $( '#new_percent' ).attr( 'value' );
	var bid = $( '#bid' ).attr( 'value' );
	var id = $( '#tax_id' ).attr( 'value' );
	
	$.ajax( {
		type: "POST",
		url: ajaxUrl,
		data: "tax_name=" + tax_name + "& tax_percent=" + tax_percent + "& bid=" + bid + "& todo=1"
				+ "& id=" + id
	} );
	
	if( id > 0 ) {
		$( "#tax" + id ).html( tax_name + " (" + tax_percent + "%)" );
	} else {
		$( "#tax_list" ).append( "<li>" + tax_name + " (" + tax_percent + "%)" );
	}
	
	setTimeout( "$('#tax_submit').removeAttr('disabled')", 1000 );
	
}

function update_tax( id, name, percent ) {
	$( "#new_percent" ).val( percent );
	$( "#new_tax_name" ).val( name );
	$( "#tax_id" ).val( id );
}

function import_order(orderid){
		var myFile = jQuery( "#fileinput" ).val();
		
		alert(myFile);
		
		jQuery.get(myfile, function(data) {
			alert(data);
		});
}

function update_order(orderid, item_id, item_code, field){
		if(field == "qty"){
			var amt = jQuery( "#qty" + item_id ).val();
		}
		else if(field == "new_cost"){
			var amt = jQuery( "#cost" + item_id ).val();
		}
		else if(field == "new_price"){
			var amt = jQuery( "#price" + item_id ).val();
		}
		
		var margin = Math.round((jQuery( "#price" + item_id ).val() - jQuery( "#cost" + item_id ).val()) / jQuery( "#price" + item_id ).val() * 100 * 10) / 10;
		
        jQuery.ajax( {
		type: "POST",
		url: ajaxUrl,
		data: "orderid=" + orderid + "&item_id=" + item_id + "&item_code=" + item_code + "&field=" + field + "&amt=" + amt + "& todo=27",
		success: function( r ) {
			jQuery( "#margin" + item_id).html(margin + "%");
			jQuery( "#row" + item_id ).css("background-color","yellow");
            jQuery( "#row" + item_id ).animate({backgroundColor: '#ffffff'}, 1500);
		}
		} );
}

function update_vmach( id, bid ) {
        $( ".vmach" ).css("background-color","white");
		$( "#" + id ).css("background-color","yellow");
        
        $.ajax( {
		type: "POST",
		url: ajaxUrl,
		data: "id=" + id + "&bid=" + bid + "& todo=23",
		success: function( r ) {
			$( "#vMachDetails" ).html( r );
                        $( '#newslot' ).focus();
		}
	} );
}

function addSlot( id, bid ) {
        $( '#addSlotButton' ).attr( 'disabled', 'disabled' );
    
        var newSlot = $( '#newslot' ).val();
        var newItem = $( '#newitem' ).val();
        
        if(newSlot == '' || newItem <= 0){
            alert("Please fill in all fields");
            setTimeout( "$('#addSlotButton').removeAttr('disabled')", 1000 );
        }
        else{
            $.ajax( {
		type: "POST",
		url: ajaxUrl,
		data: "id=" + id + "& bid=" + bid + "& newSlot=" + newSlot + "& newItem=" + newItem + "& todo=24",
		success: function( r ) {
                        $( '#newslot' ).val('');
                        $( '#newitem' ).val(0);
			$( "#vMachDetails" ).html( update_vmach(id, bid));
		}
            } );
        }
}

function deleteSlot( id, bid, vmach_id ) {
		$( "#row" + vmach_id ).css("background-color","red");
		
        if (confirm('Are you sure you want to delete this Slot?')){
			
            $.ajax( {
				type: "POST",
				url: ajaxUrl,
				data: "id=" + id + "& bid=" + bid + "& vmach_id=" + vmach_id + "& todo=25",
				success: function( r ) {
					$( "#vMachDetails" ).html( update_vmach(id, bid));
				}
			} );
        }
		else{
			$( "#row" + vmach_id ).css("background-color","#ffffff");
		}
}
          
function vmachUpdate( id, vmach_id, which ) {
    
            if(which == 1){
                var newValue = $( '#col' + vmach_id ).val();
            }
            else if(which == 2){
                var newValue = $( '#menu_item' + vmach_id ).val();
            }
            
            if(which == 1 && newValue == ''){
                alert("Field can not be blank");
            }
            else{
                $.ajax( {
                    type: "POST",
                    url: ajaxUrl,
                    data: "id=" + id + "& which=" + which + "& newValue=" + newValue + "& vmach_id=" + vmach_id + "& todo=26",
                    success: function( r ) {

							var resArr = r.split(",");
							
							if(which == 2){
								$( "#net" + vmach_id ).html(resArr[0]);
								$( "#gross" + vmach_id ).html(resArr[1]);
							}

                            $( "#row" + vmach_id ).css("background-color","yellow");
                            $( "#row" + vmach_id ).animate({backgroundColor: '#ffffff'}, 1500);
                    }
                } );
            }
}

function submit_group( ) {
	$( '#group_submit' ).attr( 'disabled', 'disabled' );
	
	var group_name = $( '#group_name' ).attr( 'value' );
	var bid = $( '#bid' ).attr( 'value' );
	var id = $( '#group_id' ).attr( 'value' );
	
	$.ajax( {
		type: "POST",
		url: ajaxUrl,
		data: "group_name=" + group_name + "& bid=" + bid + "& todo=2" + "& id=" + id
	} );
	
	if( id > 0 ) {
		$( "#group" + id ).html( group_name );
	} else {
		$( "#group_list" ).append( "<li>" + group_name );
	}
	
	setTimeout( "$('#group_submit').removeAttr('disabled')", 1000 );
}

function update_group( id, name ) {
	$( "#group_name" ).val( name );
	$( "#group_id" ).val( id );
	
	if( id > 0 ) {
		$( '#group_tax' ).show( );
	} else {
		$( '#group_tax' ).hide( );
	}
	$( "#group_tax_id" ).val( id );
}

function update_group_taxes( which, taxid ) {
	var groupid = $( '#group_tax_id' ).attr( 'value' );
	
	$.ajax( {
		type: "POST",
		url: ajaxUrl,
		data: "groupid=" + groupid + "& which=" + which + "& taxid=" + taxid + "& todo=7",
		success: function( r ) {
			alert( r );
		}
	} );
}

function submit_customer( ) {
	$( '#cust_submit' ).attr( 'disabled', 'disabled' );
	
	var bid = $( '#cust_bid' ).attr( 'value' );
	var customerid = $( '#customerid' ).attr( 'value' );
	var email = $( '#email' ).attr( 'value' );
	var user_pass = $( '#user_pass' ).attr( 'value' );
	var first_name = $( '#first_name' ).attr( 'value' );
	var last_name = $( '#last_name' ).attr( 'value' );
	var scancode = $( '#card_num' ).attr( 'value' );
	var old_scancode = $( '#old_scancode' ).attr( 'value' );
	
	$.ajax( {
		type: "POST",
		url: ajaxUrl,
		data: "email=" + email + "& user_pass=" + user_pass + "& bid=" + bid + "& todo=3"
				+ "& customerid=" + customerid + "& first_name=" + first_name + "& last_name="
				+ last_name + "& scancode=" + scancode + "& old_scancode=" + old_scancode
	} );
	
	if( customerid > 0 ) {
		$( "#cust" + customerid ).html( "<font color=black>" + last_name + ", " + first_name
										+ "</font>" );
	} else {
		$( "#customer_list" ).append( last_name + ", " + first_name + "<br>" );
	}
	
	setTimeout( "$('#cust_submit').removeAttr('disabled')", 1000 );
}

function set_customer( id, last_name, first_name, email, scancode, balance ) {
	$( "#email" ).val( email );
	$( "#customerid" ).val( id );
	$( "#last_name" ).val( last_name );
	$( "#first_name" ).val( first_name );
	$( "#card_num" ).val( scancode );
	$( "#old_scancode" ).val( scancode );
	$( "#cust_id" ).val( id );
        $( "#cust_id2" ).val( id );
	$( "#balance" ).html( balance );

	// //list transactions
	$.ajax( {
		type: "POST",
		url: ajaxUrl,
		data: "customerid=" + id + "& todo=6",
		success: function( r ) {
			$( "#cust_trans" ).html( r );
		}
	} );

	$( '#userFunctionForm' ).data( 'args', arguments ).find( '[name="cust_id"]' ).val( id );
	$( '#userFunctionList' ).html( '' );
	$.ajax( {
		type: 'POST',
		url: 'getUserFunctions',
		data: 'customerid=' + id,
		dataType: 'json',
		success: function( data ) {
			if( !data || !data.success || data.function === undefined || data.error ) {
				jQuery.waffle.error( ( data && data.error ) ? data.error : 'There was an error processing your request' );
				return false;
			}
			var ufl = $( '#userFunctionList' );
			var fns = data.function;
			var fn, fid, fname, fnelem;
			for( var i = 0, il = fns.length; i < il; i++ ) {
				fn = fns[i];
				fid = fn.id;
				fname = fn.name;
				fnelem = jQuery( '<div>Remove user function: ' + fname + '</div>' );
				fnelem.click( function( ) {
					jQuery.ajax( {
						type: 'POST',
						url: 'removeUserFunction',
						data: 'customerid=' + id + '&fnid=' + fid,
						dataType: 'json',
						success: function( idata ) {
							if( !idata || !idata.success || idata.error ) {
								jQuery.waffle.error( ( idata && idata.error ) ? idata.error : 'There was an error processing your request' );
								return false;
							}
							set_customer( id, last_name, first_name, email, scancode, balance );
						}
					} );
				} ).css( {
					cursor: 'pointer',
					fontWeight: 'bold'
				} );
				ufl.append( fnelem );
			}
		}
	} );
	/*
	 * $.ajax({ type: "POST", url: ajaxUrl, data: "email="+ email +"&
	 * user_pass="+ user_pass +"& bid="+ bid+"& todo=3"+"& customerid="+
	 * customerid+"& first_name="+ first_name+"& last_name="+ last_name+"&
	 * scancode="+ scancode, success: function (r) { $("#cust_trans").html( r ) }
	 * });
	 */
}

jQuery( function( ) {
	var form = jQuery( '#userFunctionForm' );
	var custid = form.find( '[name="cust_id"]' );
	var fnname = form.find( '[name="userFunction"]' );
	form.submit( function( ) {
		jQuery.ajax( {
			type: 'POST',
			url: 'addCannedUserFunction',
			data: 'customerid=' + custid.val( ) + '&fnname=' + fnname.val( ),
			dataType: 'json',
			success: function( data ) {
				if( !data || !data.success || data.error ) {
					jQuery.waffle.error( ( data && data.error ) ? data.error : 'There was an error processing your request' );
					return false;
				}

				set_customer.apply( window, form.data( 'args' ) );
			}
		} );
	} );
	var addbtn = form.find( ':submit' );
	addbtn.attr( 'disabled', true );
	fnname.change( function( ) {
		addbtn.attr( 'disabled', jQuery( this ).val( ) === '---' || !parseInt( custid.val( ) ) );
	} );
} );

function filter_trans( ) {
	var customerid = $( '#cust_id2' ).val();
	var date1 = $( '#filterDate1' ).val();
	var date2 = $( '#filterDate2' ).val();
        
        date1 = date1 + ' 00:00:00';
        date2 = date2 + ' 23:59:59';

	//list transactions
	$.ajax( {
		type: "POST",
		url: ajaxUrl,
		data: "customerid=" + customerid +  "& date1=" + date1 + "& date2=" + date2 + "& todo=6",
		success: function( r ) {
			$( "#cust_trans" ).html( r );
		}
	} );
}

function submit_cards( ) {
	$( '#scan_submit' ).attr( 'disabled', 'disabled' );
	
	var scan_start = $( '#scan_start' ).attr( 'value' );
	var scan_end = $( '#scan_end' ).attr( 'value' );
	var scan_value = $( '#scan_value' ).attr( 'value' );
	var bid = $( '#scan_bid' ).attr( 'value' );
	var scan_id = $( '#scan_id' ).attr( 'value' );
	
	$.ajax( {
		type: "POST",
		url: ajaxUrl,
		data: "bid=" + bid + "& todo=4" + "& scan_start=" + scan_start + "& scan_end=" + scan_end
				+ "& scan_value=" + scan_value + "& scan_id=" + scan_id
	} );
	
	if( scan_id > 0 ) {
		$( "#card" + scan_id ).html( scan_start + " - " + scan_end + " [$" + scan_value + "]" );
	} else {
		$( "#card_list" )
			.append( "<li>" + scan_start + " - " + scan_end + " [$" + scan_value + "]" );
	}
	
	setTimeout( "$('#scan_submit').removeAttr('disabled')", 1000 );
}

function submit_ordertype( bid ) {
	$( '#ot_submit' ).attr( 'disabled', 'disabled' );
	
	/*
	 * var scan_start = $('#scan_start').attr('value');
	 * 
	 * $.ajax({ type: "POST", url: ajaxUrl, data: "bid="+ bid+"&
	 * todo=4"+"& scan_start="+ scan_start+"& scan_end="+ scan_end+"&
	 * scan_value="+ scan_value+"& scan_id="+ scan_id });
	 * 
	 * if(scan_id > 0){$("#card" + scan_id).html(scan_start + " - " + scan_end + "
	 * [$" + scan_value + "]");} else{$("#card_list").append("<li>" +
	 * scan_start + " - " + scan_end + " [$" + scan_value + "]");}
	 */

	setTimeout( "$('#ot_submit').attr('disabled', '')", 1000 );
}

function update_cards( scan_id, scan_start, scan_end, scan_value ) {
	$( "#scan_start" ).val( scan_start );
	$( "#scan_end" ).val( scan_end );
	$( "#scan_value" ).val( scan_value );
	$( "#scan_id" ).val( scan_id );
}

function submit_trans( ) {
	$( '#trans_submit' ).attr( 'disabled', 'disabled' );
	
	var cust_id = $( '#cust_id' ).attr( 'value' );
	var dol_value = $( '#dol_value' ).attr( 'value' );
	var transtype = $( '#transtype' ).attr( 'value' );
	
	if( cust_id < 1 ) {
		alert( 'No customer chosen' );
	} else if( dol_value == 0 ) {
		alert( 'Enter a value' );
	} else {
		$.ajax( {
			type: "POST",
			dataType: 'json',
			url: 'processCustomerTransaction',
			data: "dol_value=" + dol_value + "&transtype=" + transtype + "&cust_id=" + cust_id,
			success: function( data ) {
				if( !data || data.error || !data.success ) {
					alert( 'Failed to add value: ' + data.error );
					return;
				}
				
				alert( 'Payment Successful.' );
				
				// //list transactions
				$.ajax( {
					type: "POST",
					url: ajaxUrl,
					data: "customerid=" + cust_id + "& todo=6",
					success: function( r ) {
						$( "#cust_trans" ).html( r );
					}
				} );
			}
		} );
	}
	
	setTimeout( "$('#trans_submit').removeAttr('disabled')", 1000 );
	
	return false;
}

jQuery(function(){
	var content = '';
	var ie7 = jQuery.browser.msie && jQuery.browser.version.substr( 0, 1 ) < 8;
	
	if( jQuery.support.inlineBlockNeedsLayout ) {
		content = '<span>This page may not display optimally in your browser.<br />';
		
		if( ie7 )
			content = content + 'You might want to upgrade to Internet Explorer 8.';
		else
			content = content + 'You might want to upgrade to a newer version of your browser, or switch to a standards-compliant browser such as Firefox or Chrome.';
		
		content = content + '</span>';
		
		jQuery('#headerMessage').addClass( 'ui-state-highlight' ).html( content );
	}
});
