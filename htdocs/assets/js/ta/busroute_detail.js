
function getRow( table, scrollRow, rowNumber ) {
	if ( typeof rowNumber == 'undefined' ) {
		rowNumber = table.rows.length - 1;
	}
	var lastRow = table.rows[ rowNumber ];
	if ( lastRow.cells.length != scrollRow.cells.length ) {
		lastRow = getRow( table, scrollRow, rowNumber - 1 );
	}
/*
	alert(
		"rowNumber = " + rowNumber + "\n"
		+ "table.rows.length = " + table.rows.length + "\n"
		+ "lastRow.cells.length = " + lastRow.cells.length + "\n"
		+ "scrollRow.cells.length = " + scrollRow.cells.length + "\n"
	);
*/
	return lastRow;
}


var headerScroll = function(id, options){
	this.id = id;
	this.options = options;
	this.table = $(this.id);
	this.scrollRow = $(options.head) || this.table.rows[0];
	this.lastRow = getRow( this.table, this.scrollRow );
	
	//Grab widths
	var widths = [];
//	alert ( "widths = " + widths );
	for ( var row = 0; row < this.table.rows.length; row++ ) {
		if ( this.table.rows[ row ].cells.length == this.scrollRow.cells.length ) {
			for ( var cell = 0; cell < this.table.rows[ row ].cells.length; cell++ ) {
				if ( widths[ cell ] ) {
					widths[ cell ] = Math.max( widths[ cell ], this.table.rows[ row ].cells[ cell ].offsetWidth );
				}
				else {
					widths[ cell ] = this.table.rows[ row ].cells[ cell ].offsetWidth;
				}
			}
		}
	}
//	alert ( "widths = " + widths );
//	for(i = 0; i < this.lastRow.cells.length; i++){
//		widths[i] = this.lastRow.cells[i].offsetWidth;
//	}
	$(this.scrollRow).absolutize();
	//Set widths again
	for ( row = 0; row < this.table.rows.length; row++ ) {
		if ( this.table.rows[ row ].cells.length == this.scrollRow.cells.length ) {
			for ( cell = 0; cell < this.table.rows[ row ].cells.length; cell++ ) {
				this.table.rows[ row ].cells[ cell ].style.width = widths[i] + 'px';
			}
		}
	}
	var totalWidth = 0;
	var tr = document.createElement('tr');
	for( var i = 0; i < this.scrollRow.cells.length; i++ ){
		this.scrollRow.cells[ i ].style.width = widths[ i ] + 'px';
		totalWidth += widths[ i ];
		var td = document.createElement('td');
		td.innerHTML = '&nbsp;';
		td.style.height = parseInt( this.scrollRow.offsetHeight ) + 'px';
		td.style.width = parseInt( widths[ i ] ) + 'px';
//		td.colSpan = this.scrollRow.cells.length;
		tr.appendChild(td);
	}
	//this.table.insert({top: tr});
	new Insertion.After(this.scrollRow, tr);
	this.start = this.scrollRow.offsetTop;

	var obj = this;
	window.onscroll = function(){ 
		var os = document.body.scrollTop;
		if(os > obj.start){
			obj.scrollRow.style.top = os+"px";
		}
		else obj.scrollRow.style.top = obj.start+'px';
	}
}




