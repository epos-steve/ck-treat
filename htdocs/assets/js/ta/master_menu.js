$(document).ready(function() {
	/* load menu item with ajax call */
	$('#menu_item').change(function(){		
		loadItem(this.value);
	});
	
	function loadItem(ele){
		clear_message();		
		clear_form();
		
		$('#loading').show();
		
		$.post('mastermenu/ajaxLoadMenuItem', {'upc' : ele}, function(data) {
			if(data.pass){
				$('#upc').val(data.upc);
				$('#item_name').val(data.item_name);
				$("#manufacturer").val(data.man_id);
				$("#category").val(data.cat_id);
				
				var length = data.nutrition.length;
				for(var x=0; x<length; x++){
					if(data.nutrition[x].key != ''){
						$('#'+data.nutrition[x].key).val(data.nutrition[x].value);
					}
				}

				length = data.categories.length;
				for(var x=0; x<length; x++){
					$('#cat'+data.categories[x].value).attr('checked','checked');
				}
			}else{
				$('#res_message').html('<span style="color:red;">Error loading item</span>.');
			}
		})
		.complete(function(){$("#upc").attr("disabled", "disabled"); $('#loading').hide();});
	}
	
	/* update select list of menu items with ajax call */	
	$('#search_form').submit(function(event){
		event.preventDefault();
		
		clear_message();
		
		$('#loading').show();
		
		$('#item_form')[0].reset();
		
		$.post('mastermenu/ajaxSearch',
		{'search_upc' : $('#search_upc').val(), search_name : $('#search_name').val(), search_man : $('#search_man').val(), search_cat : $('#search_cat').val()},
		function(data) {
			switch(data.status){
				case 'failed':
					$('#res_message').html('<span style="color:red;">No results</span> found.');
					break;
				case 'no_criteria':
					$('#res_message').html('<span style="color:red;">Search criteria</span> required.');
					break;
				default:
					$('#menu_item').html(data.option);
			}
			
			if(data.num_results == 1){
				loadItem( $("#menu_item :first-child").val() );
				$("#menu_item")[0].selectedIndex = 0;
			}
		})
		.complete(function(){
			$('#search').focus();
			$('#loading').hide();
		});		
	});
	
	/* save menu item with ajax call */
	$('#save').click(function(){
		var reload;
		
		clear_message();
		
		$('#loading').show();
		
		$('#upc').removeAttr('disabled');  //disabled form elements dont submit
		
		var form_data = $('#item_form').serialize();
		
		$('#upc').attr('disabled','disabled'); //disable element again
		
		$.post('mastermenu/ajaxSaveMenuItem',
		form_data,
		function(data) {
			if(data.passed){
				$("#menu_item option[value='"+data.upc+"']").text(data.item_name);
				
				$('#res_message').html('Menu item <span style="color:green;">successfully</span> updated.');
				
				if(data.update){
					reload = false;
				} else {
					reload = true;
				}				
			} else {
				$('#res_message').html('Menu item <span style="color:red;">failed</span> to save.');
				reload = false;
			}
		})
		.complete(function(){
			if(reload){
				load_menu(false);
			}
			
			$('#search_form')[0].reset();
			
			$('#loading').hide();
		});
	});
	
	/* mark menu item for deletion with ajax call */
	$('#delete').click(function(){
		clear_message();
		
		var delete_it = confirm_delete();
		
		if(delete_it){
			$('#loading').show();
		
			$.post('mastermenu/ajaxDeleteMenuItem',
			{'upc' : $('#upc').val()},
			function(data) {
				if(data.passed){
					$("#menu_item option[value='"+data.upc+"']").remove();
					
					$('#res_message').html('Menu item <span style="color:green;">successfully</span> deleted.');
				} else {
					$('#res_message').html('Menu item <span style="color:red;">failed</span> to delete.');
				}
			})
			.complete(function(){$('#loading').hide(); clear_form();});
		}
	});	
	
	/* reset form and make upc editable */
	$('#clear').click(function(){
		clear_message();
		clear_form();
	});
	
	/* calls load_menu function */
	$('#reset_list').click(function(){
		clear_message();
		load_menu(true);
	});
	
	/* clear message div */
	function clear_message(){
		$('#res_message').html('');
	}
	
	/* show confirmation of deletion popup */
	function confirm_delete(){
		var r = confirm("Really delete the menu item?");
		if (r){
			return true;
		} else {
			return false;
		}
	}
	
	/* clear form, enable upc */
	function clear_form(){
		$('#item_form')[0].reset();
		$("#upc").removeAttr("disabled");  //make upc editable for creating a new item
	}
	
	/* reload entire menu item list thru ajax */
	function load_menu(show_throbber){
		if(show_throbber){
			$('#loading').show();
		}
		
		$.post('mastermenu/ajaxAllMenuItems',
		function(data) {
			if(data != 'failed'){
				$('#menu_item').html(data);
			} else {
				$('#res_message').html('<span style="color:red;">No results</span> found.');
			}
		})
		.complete(function(){
			if(show_throbber){
				$('#loading').hide();
			}
		});
	}
});