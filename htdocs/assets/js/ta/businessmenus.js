jQuery().ready(function () {
	jQuery('a.promo_group').click(editPromoGroup);

	jQuery('#frm_promo_groups').submit(function (event) {
		event.preventDefault();
		var submit = $(this).find(':submit').attr('disabled', 'disabled');

		jQuery.post('/ta/ajax_pos.php', $(this).serialize(), function(data) {
			if (jQuery('#promo_group_id').val() == '') {
				jQuery('#promo_list').append($('<li></li>').html(jQuery('<a></a>').attr('id', data).addClass('promo_group').attr('href', '#').text($('#promo_group_name').val()).click(editPromoGroup)));
			} else {
				jQuery('#promo_list #'+data).text(jQuery('#promo_group_name').val());
			}
		});

		setTimeout(function () {
			jQuery('#frm_promo_groups :reset').click();
			jQuery(submit).removeAttr('disabled');
		}, 1000)
	});

	jQuery('#frm_promo_groups input[type=reset]').click(function (event) {
		event.preventDefault();
		jQuery(':input', '#frm_promo_groups').not(':checkbox, :button, :submit, :reset, [name=bid], [name=todo]')
			.val('');
		jQuery(':checkbox', '#frm_promo_groups')
			.removeAttr('checked');

		jQuery('#frm_promo_groups #promo_group_order').val('99999');
		jQuery('#promo_group_products').html('');
	});
});

/**
 * This is jQuery callback and should never be called directly
 */
function editPromoGroup(event) {
	event.preventDefault();
	jQuery('#frm_promo_groups :reset').click();

	jQuery.post('/ta/ajax_pos.php', {id: jQuery(this).attr('id'), todo: 20}, function (data) {
		jQuery('#frm_promo_groups #promo_group_id').val(data['id']);
		jQuery('#frm_promo_groups #promo_group_name').val(data['name']);
		jQuery('#frm_promo_groups #promo_group_order').val(data['display_order']);
		jQuery('#frm_promo_groups #promo_group_routing').val(data['routing']);

		if (data['visible'] == '1') {
			jQuery('#frm_promo_groups #promo_group_visible').attr('checked', 'checked');
		}
	}, 'json');

	jQuery.post('/ta/ajax_pos.php', {id: $(this).attr('id'), bid: jQuery('#promo_group_bid').val(), todo: 21}, function(data) {
		$('#promo_group_products').html(data).find('input:checkbox').change(updatePromoGroupDetail);
	});
}

function updatePromoGroupDetail()
{
	jQuery.post('/ta/ajax_pos.php', {id: jQuery('#promo_group_id').val(), item_id: $(this).attr('name'), bid: jQuery('#promo_group_bid').val(), checked: this.checked, todo: 22});
}