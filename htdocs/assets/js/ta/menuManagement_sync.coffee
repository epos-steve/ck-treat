$ = jQuery


showChanges = () ->
    $('#uploader-container').fadeOut 1000, ->
        $('#upload-menu-item-review').fadeIn 1000
    $('#importer-changes-display').show()

hideChanges = () ->
    $('#upload-menu-item-review').hide()
    $('#uploader-container').slideDown 1000
    $('#importer-changes-display tbody').empty()
    $('progress').hide()

window.importUploadError = (error) ->
    $('#import-uploader-error-display').show()
    $('#import-uploader-error-display .error-message').text(error)
    $('.upload-update-progress').hide()

menuSelection = () ->
    menu_selection = $('.menu-selection').val()

    $('.menu-selection-dependent').hide()

    if menu_selection != 'company'
        if menu_selection == 'district'
            $('.districts').show()
        else if menu_selection == 'kiosk'
            $('.kiosks').show()


class MenuUpdates
    changes : []
    skippedNum : 0
    limit : 'company'
    district : null
    kiosk : null

    constructor : (changes) ->
        @changes = changes
        @limit = $('.menu-selection').val()
        @district = $('.districts').val()
        @kiosk = $('.kiosks').val()

    skipped : (val) ->
        @skippedNum = val

    convertCSVToMenuItems : (raw) ->
        @doingConvert = true
        $.post(
            '/ta/kioskutilities/syncMenu'
            'convertToCSV' : 1
            'limit' : @limit
            'district' : @district
            'kiosk' : @kiosk
            'rawCSV' : raw
            (data) =>
                if data?
                    if data.changes?
                        @changes = data.changes
                    if data.skipped?
                        @skippedNum = data.skipped
                @display()
            'json'
        )

    display : () ->
        $('.upload-update-progress').hide()
        $('progress').hide()
        if @changes.length < 1
            $('#import-uploader-error-display').show()
            $('#import-uploader-error-display .error-message').text("No changes to process and skipped #{@skippedNum} items.")
            return

        showChanges()

        if @skippedNum > 0
            $('.skipped-message').show()
            $('.skipped-num').text(@skippedNum)
        else
            $('.skipped-num').text('0')
            $('.skipped-message').hide()

        $('.ignore-changes').click (event) ->
            hideChanges()

        $('.remove-row').live 'click', (event) ->
            $(event.currentTarget).parentsUntil('tr').parent().remove()

        $('.save-changes').click (event) =>
            event.preventDefault()
            $('.menu-item-data').each (index, item) =>
                $(item).find('.remove-row').hide()
                $(item).find('.update-progress').show()
                data = $(item).data()
                $.post(
                    '/ta/kioskutilities/syncMenu'
                        'changes' : 1
                        'upc' : $(item).attr('data-upc')
                        'name' : $(item).data('name')
                        'cost' : $(item).data('cost')
                        'price' : $(item).data('price')
                        'limit' : @limit
                        'district' : @district
                        'kiosk' : @kiosk
                    (data) ->
                        if data.error? and data.error is false
                            $(item).fadeOut 1000, ->
                                if $('#importer-changes-display tbody tr').filter(':visible').length < 1
                                    hideChanges()
                        else
                            $(item).replaceWith """
                            <tr>
                                <td colspan="5">
                                    Error! Did not save: #{item.message}
                                </td>
                            </tr>
                                            """
                    'json'
                )

        html = []
        for item in @changes
            cost = item.cost.toFixed(2)
            price = item.price.toFixed(2)
            row = []
            row[row.length] = '<tr'
            row[row.length] = ' data-name="' + item.name.replace('"', '&quot;') + '"'
            row[row.length] = ' data-upc="' + item.upc.replace('"', '&quot;') + '"'
            row[row.length] = ' data-cost="' + item.cost + '"'
            row[row.length] = ' data-price="' + item.price + '"'
            row[row.length] = ' class="menu-item-data">'
            row[row.length] = '<td style="width: 20px; text-align: center;">'
            row[row.length] = '<img src="/assets/images/delete.gif" style="width: 16px; height: 16px;" class="remove-row" />'
            row[row.length] = '<img src="/assets/images/ajax-loader-redmond.gif" style="display: none;" class="update-progress" />'
            row[row.length] = '</td>'
            row[row.length] = "<td>#{item.name}</td>"
            row[row.length] = "<td>#{item.upc}</td>"
            row[row.length] = "<td style='text-align: right;'>$#{cost}</td>"
            row[row.length] = "<td style='text-align: right;'>$#{price}</td>"
            row[row.length] = '</tr>'

            html[html.length] = row.join "\n"
        $('#importer-changes-display tbody').html html.join("\n")



class Uploader
    html5Support : false
    html4Support : true
    constructor: () ->
        @checkSupport()

    checkSupport: () ->
        if window.FileReader
            @html5Support = true
            @html4Support = false
        else
            @html5Support = false
            @html4Support = true

    hideUnsupportedElements: () ->
        if @html5Support
            $('.html4').hide()
            $('progress').hide()

            $('#importer-file').css('visibility', 'hidden')
                .css('position', 'absolute')
                .css('top', -50)
                .css('left', -50)
        else
            $('.html5').hide()

    handleUpload: () ->
        if @html5Support
            @html5Upload()
        else
            @html4Upload()

    html4Upload: () ->
        $('.import-file-button').click (event) ->
            $('#import-uploader-error-display').hide()
            $('#import-uploader-error-display .error-message').text('')
            $('.upload-update-progress').show()

        window.importUpload = (json, skipped = 0) ->
            $('#importer-file').val ''
            $('.upload-update-progress').hide()
            updates = new MenuUpdates(json)
            updates.skipped(skipped)
            updates.display()

    html5Upload: () ->
        $('.importer-choose-file').live 'click', ->
            $('#import-uploader-error-display').hide()
            $('#import-uploader-error-display .error-message').text('')
            $('#importer-file').val('').click()

        $('#importer-file').live 'change', (event) ->
            $('progress').show()
            $('progress').val(0).text('')
            if !event.currentTarget.files or event.currentTarget.files.length < 1
                return
            reader = new FileReader()
            reader.onload = (event) ->
                $('progress').val(100)
                    .text 'Done!'

                updates = new MenuUpdates([])
                updates.convertCSVToMenuItems(event.target.result)

                return
            reader.onprogress = (event) ->
                if event.lengthComputable
                    percent = Math.round((event.loaded / event.total) * 100)
                    if percent <= 100
                        $('progress').val(percent)
                            .text(percent + '%')
            reader.readAsText(event.currentTarget.files[0])


$ ->
    menuSelection()

    $('.menu-selection').live 'change', ->
        menuSelection()

    $('#exporter-file').click (event) ->
        $('#import-uploader-error-display').hide();

    uploader = new Uploader()
    uploader.hideUnsupportedElements()
    uploader.handleUpload()
