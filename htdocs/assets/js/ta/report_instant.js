$(document).ready(function(){
	$('#start_date').datepicker();
	$('#end_date').datepicker();
	$('#dates_dialog').dialog({autoOpen:false, width:500, height:250, modal:true});
	
	//click link, display date range selection dialog
	$('.report_name a').click(function(event){
		event.preventDefault();
		
//		$('#report_url').val( this.href );
		$('#reportrun_form').attr( 'action', this.href );
		
		$('#start_date').datepicker('disable');
		$('#end_date').datepicker('disable');
		
		$('#dates_dialog').dialog('open');
		
		$('#start_date').datepicker('enable');
		$('#end_date').datepicker('enable');
	});
	
	//submit call to report with dates and email added to url
//	$('#reportrun_form').submit(function(){
//		var form = this;
//		var action_url = $('#report_url').val();
//		
//		$(form).attr('action',action_url);
//	});
	
});
