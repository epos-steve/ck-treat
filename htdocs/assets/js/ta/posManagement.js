
if(typeof String.prototype.trim !== 'function') {
	String.prototype.trim = function() {
		return this.replace(/^\s+|\s+$/g, '');
	};
}

jQuery(document).ready(function() {
	jQuery( "#dialog-form" ).dialog({
			autoOpen: false,
			height: 250,
			width: 250,
			modal: true,
			buttons: {
				"OK": function() {
					jQuery(this).find(".dialog-state-color").removeClass("ui-state-error ui-state-highlight");
					var amount = jQuery(this).find("#cbalance").val();
					jQuery(this).find("#message").text("");
					var valid = String(amount).search(/^\s*(\+|-)?((\d+(\.\d\d)?)|(\.\d\d))\s*$/) != -1;
					if(!valid)
					{
						jQuery(this).find(".dialog-state-color").addClass("ui-state-error");
						jQuery(this).find("#message").text("The amount must be in currency format.");
						return;
					}

					jQuery.ajax({
						type: "POST",
						url: "/ta/businessMenus/updateCustomerBalance/",
						data: jQuery(this).find("form").serialize(),
						success: function(data) {
							jQuery("#dialog-form #message").text("Your request was successfull.");
							jQuery("#dialog-form #message").removeClass("ui-state-error").addClass("ui-state-highlight");
							jQuery('#balance').text(amount);
						},
						error: function(data){
							jQuery(this).find("#message").text("There was an error posting your request to the server.");
							jQuery(this).find("#message").removeClass("ui-state-highlight").addClass("ui-state-error");
						}
					});
				},
				Cancel: function() {
					$( this ).dialog( "close" );
				}
			},
			close: function() {
				jQuery(this).find("#message").text("");
				jQuery(this).find("#cbalance").val("");
				var success = jQuery(this).find('.dialog-state-color').hasClass('ui-state-highlight');
				jQuery(this).find(".dialog-state-color").removeClass("ui-state-error ui-state-highlight");
				if (success)
					window.location.reload();

			}
		});

		
	jQuery('#editBalanceLink').click(function()
	{
		var frm = jQuery('#editBalanceLink').closest('form');
		if(frm)
		{
			var bid = jQuery(frm).find("#cust_bid").val().trim();
			var cid = jQuery(frm).find("#customerid").val().trim();
			var cname = jQuery(frm).find("#first_name").val() + " " +  jQuery(frm).find("#last_name").val();
			cname = jQuery.trim(cname);
			var cbalance = jQuery(frm).find("#balance").text().trim();
			cbalance = parseFloat(cbalance).toFixed(2);
			var arr = [bid , cid];
			if(jQuery.inArray("", arr) == -1 && jQuery.inArray("0", arr) == -1)
			{
				var dlg = jQuery("#dialog-form");
				jQuery(dlg).find("#cid").val(cid);
				jQuery(dlg).find("#bid").val(bid);
				jQuery(dlg).find("#cname").text(cname);
				jQuery(dlg).find("#cbalance").val(cbalance);
				$(dlg).dialog("open");
			}
		}

		return false;
	});
	
	BrandingController.initialize( );
		
});


var BrandingController = {
	
	initialize: function( ) {
		
	}
	
};









