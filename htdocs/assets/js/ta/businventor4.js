
//<script language="JavaScript"
//	type="text/JavaScript">
function popup(url)
{
	popwin=window.open(url,"Tutorial","location=no,scrollbars=yes,menubar=no,titlebar=no,resizeable=no,height=50,width=500");
}
//</script>

// prototype-eef.js Override
Form.EditInPlace.prototype.inputSwap = function(trigger, value, disp){
	if(this.options.colorCells) trigger.className = 'editable';
	var obj = this;
	trigger.onblur = null;
	var input = trigger.childNodes[0];
	var val = (value != null) ? value : input.value;

	this.options.parameters = {};
	this.options.parameters['table'] = this.table.id;
	this.options.parameters['value'] = val;
	this.options.parameters['column'] = this.cols[trigger.cellIndex];
	this.options.parameters['primary_key'] = this.options.primaryKey || 'id';
	this.options.parameters['id'] = trigger.parentNode.id.replace( /^id[_-]/, '' );

	new Ajax.Request('/nutrition/recipe/eip', this.options);

	trigger.innerHTML = '';
	val = disp || val;
	trigger.appendChild(document.createTextNode(val));
	trigger.onclick = function(){ obj.tdSwap(trigger); }

	if(this.options.onRowEdit != null) this.options.onRowEdit.call(this, this);
}


//<script language="JavaScript" type="text/javascript"><!--
	var dragsort = ToolMan.dragsort()
	var junkdrawer = ToolMan.junkdrawer()

	window.onload = function() {
	  /*  COMMENTED 12-16-08 BY TIM TO RID ERRORS
		junkdrawer.restoreListOrder("numeric")
		junkdrawer.restoreListOrder("phonetic1")
		junkdrawer.restoreListOrder("phonetic2")
		junkdrawer.restoreListOrder("phonetic3")
		junkdrawer.restoreListOrder("phoneticlong")
		junkdrawer.restoreListOrder("boxes")
		junkdrawer.restoreListOrder("buttons")
		//junkdrawer.restoreListOrder("twolists1")
		//junkdrawer.restoreListOrder("twolists2")

		dragsort.makeListSortable(document.getElementById("numeric"),
				verticalOnly, saveOrder)

		dragsort.makeListSortable(document.getElementById("phonetic1"),
				verticalOnly, saveOrder)
		dragsort.makeListSortable(document.getElementById("phonetic2"),
				verticalOnly, saveOrder)
		dragsort.makeListSortable(document.getElementById("phonetic3"),
				verticalOnly, saveOrder)
		dragsort.makeListSortable(document.getElementById("phoneticlong"),
				verticalOnly, saveOrder)

		dragsort.makeListSortable(document.getElementById("boxes"),
				saveOrder)

		dragsort.makeListSortable(document.getElementById("buttons"),
				saveOrder)
		*/

		/*
		dragsort.makeListSortable(document.getElementById("twolists1"),
				saveOrder)
		dragsort.makeListSortable(document.getElementById("twolists2"),
				saveOrder)
		*/

		/************ TIM'S ADDITION 04-01-09: DRAG SORT ************/
		var test = new dndTable('recipe', {dbTable: 'recipe', sortColumn: 'sort', idColumn: 'recipeid'});
		/************ TIM'S ADDITION 12-16-08: SEARCH SELECT ************/
		var bid = $GET('bid');
		var cid = $GET('cid');
		var auto = new Form.AutoComplete('autoBox', {
							form: 'addItemForm',
							dbPage: 'businventor4',
							dbBid:   bid,
							dbTable: 'inv_items',
							dbDisp: 'item_name',
							dbVal: 'inv_itemid',
							dbWhere: "businessid = '"+bid+"' AND rec_num != '0' ORDER BY item_name",
							watermark: '(Add Ingredients)',
							showOnEmpty: true});
		auto.onchange = function(){ new Ajax.Request('/ta/ajaxreq.php', {parameters: {method: 'whichSize', inv_id: this.valInput.value}, onSuccess: procResults}); }
		/// EIP
		var customSwaps = [];
		customSwaps['rec_size'] = sizeSwap;
		var eip = new Form.EditInPlace('recipe', {primaryKey: 'recipeid', specialSwaps: customSwaps, onRowEdit: updateCost});
					
	}
	var sObj;
	var sTrig;
	var sizeSwap = function(trigger, obj){
		obj.options.onRowEdit = null;
		sObj = obj;
		sTrig = trigger;

		trigger.innerHTML = 'Loading..';

		new Ajax.Request('/ta/ajaxreq.php', {parameters: {method: 'sizeSwap', recipeid: obj.table.rows[obj.curRow].id.replace( /^id[_-]/, '' )}, onSuccess: procSize});
	}

	var pObj;
	var pTrig;
	var pSel;
	var procSize = function(t){
		var obj = sObj;
		var trigger = sTrig;
		var val = obj.vals[trigger.parentNode.rowIndex+":"+trigger.cellIndex];

		eval(t.responseText);
		var arr = returnVals;
		if(arr.length > 1){
			var sel = document.createElement('select');
			for(var i = 0; i < arr.length; i++){
				var opt = document.createElement('option');
				opt.value = arr[i].val;
				opt.innerHTML = arr[i].disp;
				if(val.replace(/\s/g, '') == arr[i].disp.replace(/\s/g, '')) opt.selected = true;
		
				sel.appendChild(opt);
			}
			trigger.innerHTML = '';
			trigger.appendChild(sel);
			sel.focus();
			sel.onblur = function(){obj.inputSwap(trigger, this.options[this.selectedIndex].value, " "+this.options[this.selectedIndex].innerHTML);}
			pObj = obj;
			pTrig = trigger;
			sel.onchange = performSize;
		}
		else{
			obj.cancelSwap(trigger);
		}
	}

	var performSize = function(){
		var obj = pObj;
		var trigger = pTrig;

		var val = this.options[this.selectedIndex].value;
		var disp = this.options[this.selectedIndex].innerHTML;
		var rid = obj.table.rows[trigger.parentNode.rowIndex].id.replace( /^id[_-]/, '' );
		
		new Ajax.Request('/ta/ajaxreq.php', {onError: function(){alert('fail'); }, parameters: {'method': 'changeSize', 'recipeid': rid, 'val': val}, onSuccess: function(t){ updateCost(obj);}});

		obj.inputSwap(trigger, val, disp);
	}

	var ccObj;
	var updateCost = function(obj){
		ccObj = obj;
		var p = [];
		p['method'] = 'updateCost';
		p['recipeid'] = obj.table.rows[obj.curRow].id.replace( /^id[_-]/, '' );
		p['editid'] = $GET('editid');

		new Ajax.Request('/ta/ajaxreq.php', {parameters: p, onSuccess: procCost});
	}

	var procCost = function(t){
		var obj = ccObj;
		if(obj.options.onRowEdit == null) obj.options.onRowEdit = function(){ updateCost(obj); }
		eval(t.responseText);

		var rowUpdate = new Number(returnVals['rowUpdate']);
		var cost = new Number(parseFloat(returnVals['cost']));
		var price = new Number(parseFloat(returnVals['price']));
		var profit = new Number(parseFloat(returnVals['profit']));
		var fc = returnVals['fc'];
		var servings = returnVals['servings'];
		var cost_s = new Number(cost / servings);
		var price_s = new Number(price / servings).toMoney();
		var profit_s = new Number(profit / servings).toMoney();
		var fc = returnVals['fc'];

		
		var row = obj.table.rows[obj.curRow];
		var cell = row.cells[row.cells.length -2]
		if(rowUpdate != cell.innerHTML){
			cell.innerHTML = rowUpdate.toMoney();
			new Effect.Highlight($(cell), {duration: 2});
		}

		var str = "&nbsp;&nbsp;<span class=\"dispVal\">Price:</span>&nbsp;"+price.toMoney()+"&nbsp;&nbsp;<span class=\"dispVal\">Profit:</span>&nbsp;"+profit.toMoney()+"&nbsp;&nbsp;<span class=\"dispVal\">FC:</span>&nbsp;"+fc+"%";
		$('ttlRest').innerHTML = str;
		new Effect.Highlight($('ttlRest'), {endcolor: '#e7e7e8', duration: 2});

		$('ttlCost').innerHTML = cost.toMoney();
		new Effect.Highlight($('ttlCost'), {duration: 2});

		$('sCost').innerHTML = cost_s.toMoney();
		new Effect.Highlight($('sCost'), {duration: 2});

		str = "&nbsp;&nbsp;<span class=\"dispVal\">Price:</span>&nbsp;"+price_s+"&nbsp;&nbsp;<span class=\"dispVal\">Profit:</span>&nbsp;"+profit_s;
		$('sRest').innerHTML = str;
		new Effect.Highlight($('sRest'), {endcolor: '#e7e7e8', duration: 2});

	}
	
	var procResults = function(t){
		eval(t.responseText);
		var sizes = returnVals;
		$('recSizeSelect').innerHTML = '';
		for(var i = 0; i < sizes.length; i++){
			var opt = document.createElement('option');
			opt.innerHTML = sizes[i];
			opt.value = i + 1;
			$('recSizeSelect').appendChild(opt);
		}
		$('recSizeSelect').style.display = 'inline';
	}
		/********************* END TIMS ADDITION ************************/
	


	function verticalOnly(item) {
		item.toolManDragGroup.verticalOnly()
	}

	function speak(id, what) {
		var element = document.getElementById(id);
		element.innerHTML = 'Clicked ' + what;
	}

	function saveOrder(item) {
		var group = item.toolManDragGroup
		var list = group.element.parentNode
		var id = list.getAttribute("id")
		if (id == null) return
		group.register('dragend', function() {
			ToolMan.cookies().set("list-" + id, 
					junkdrawer.serializeList(list), 365)
		})
	}

	//-->
//</script>

//<script language="javascript" type="text/javascript"><!--
function post(){
list_var = junkdrawer.serializeList(numeric)
document.order2.order.value = list_var
order2.submit()
} 
// --></script>

//<script language="javascript" type="text/javascript"><!--
function delconfirm(){return confirm('Are you sure you want to delete this area?');}
// --></script>

//<script language="javascript" type="text/javascript"><!--
function delconfirm2(){return confirm('Are you sure you want to delete this ingredient?');}
// --></script>

//<script language="javascript" type="text/javascript"><!--
function confirmcopy(){return confirm('Are you sure you want to copy?');}
// --></script>

//<script language="JavaScript" 
//	type="text/JavaScript">
function changePage(newLoc)
{
	nextPage = newLoc.options[newLoc.selectedIndex].value

	if (nextPage != "")
	{
		document.location.href = nextPage
	}
}
//</script>

//<script language="JavaScript" type="text/javascript">
//<!-- Web Site: http://dynamicdrive.com -->
//
//<!-- This script and many more are available free online at -->
//<!-- The JavaScript Source!! http://javascript.internet.com -->
//
//<!-- Begin
function disableForm(theform) {
if (document.all || document.getElementById) {
for (i = 0; i < theform.length; i++) {
var tempobj = theform.elements[i];
if (tempobj.type.toLowerCase() == "submit" || tempobj.type.toLowerCase() == "reset")
tempobj.disabled = true;
}

}
else {
alert("The form has been submitted.  But, since you're not using IE 4+ or NS 6, the submit button was not disabled on form submission.");
return false;
	}
}
//  End -->
//</script>

