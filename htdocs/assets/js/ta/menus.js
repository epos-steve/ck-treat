var sourceArray = [];
var idArray = [];

if( typeof String.prototype.trim !== 'function' ) {
	String.prototype.trim = function( ) {
		return this.replace( /^\s+|\s+$/g, '' );
	};
}

jQuery( function( ) { 
	jQuery( 'span.item' ).each( function( ) {
		var name = jQuery( this ).text( ).trim( );
		sourceArray.push( name );
		idArray[name] = jQuery( this ).attr( 'id' );
	} );
	
	jQuery( '#itemSelectBox' ).combo( {
		source: sourceArray,
		alwaysEmpty: true,
		alignment: 'right',
		select: function( event, ui ) {
			var val = jQuery( ui.item ).val( );
			id = idArray[val].substr( 7 );
			edit_menu_item( id, 1 );
		}
	} );
} );

/*jQuery( function( ) {
	var box = jQuery( '#itemSelectBox' );
	var btn = jQuery( '<span id="itemSelectBoxBtn">v</span>' );
	
	var container = box.wrap( '<div class="itemSelectContainer" />' );
	
	box.after( btn );
	
	jQuery( 'span.item' ).each( function( ) {
		var name = jQuery( this ).text( ).trim( );
		sourceArray.push( name );
		idArray[name] = jQuery( this ).attr( 'id' );
	} );
	
	btn.button( {
		text: false,
		icons: { primary: 'ui-icon-triangle-1-s' }
	} ).hover( function( ) {
		jQuery( this ).add( '#itemSelectBox' ).addClass( 'ui-state-hover' );
	}, function( ) {
		jQuery( this ).add( '#itemSelectBox' ).removeClass( 'ui-state-hover' );
	}).click( function( ) {
		jQuery( '#itemSelectBox' ).focus( );
	} ).removeClass( 'ui-corner-all' ).addClass( 'ui-corner-right' );
	
	box.autocomplete( {
		delay: 0,
		minLength: 0,
		source: sourceArray,
		position: {
			my: 'right top',
			at: 'right bottom',
			offset: ( btn.outerWidth( true ) - 1 ) + ' 0'
		}
	} ).hover( function( ) {
		jQuery( this ).add( '#itemSelectBoxBtn' ).addClass( 'ui-state-hover' );
	}, function( ) {
		jQuery( this ).add( '#itemSelectBoxBtn' ).removeClass( 'ui-state-hover' );
	} ).focus( function( ) {
		jQuery( this ).autocomplete( 'search' ).addClass( 'ui-state-focus' );
	} ).blur( function( ) {
		jQuery( this ).val( '' ).removeClass( 'ui-state-focus' );
	} ).bind( 'autocompleteselect', itemSelectBoxHandler ).addClass( 'ui-state-default ui-corner-left' );
	
	var cmd ='jQuery( ".itemSelectContainer" ).height( ' + box.height( ) + ' ).width( ' + box.outerWidth( true ) + ' + ' + btn.outerWidth( true ) + ' - 1 );'; 
	eval( cmd );
} );*/