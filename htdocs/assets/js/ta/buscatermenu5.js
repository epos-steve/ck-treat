jQuery.noConflict( );

var dataResponse = {
	status: "ERROR",
	message: "There was an error recieving the response and no data was returned"
};

function refresh_item_history( itemId, keepStatus ) {
	jQuery( "#dialog-status" ).html( "Retrieving item history" ).removeClass( )
		.addClass( "ui-state-default" );
	jQuery.ajax( {
		type: 'POST',
		cache: false,
		url: "/ta/buscatermenu5-1.php",
		data: {
			step: "V",
			item: itemId,
			bid: jQuery( '#global-businessid' ).val( ),
			cur_term: jQuery( '#global-terminalno' ).val( )
		},
		dataType: "html",
		success: function( data ) {
			jQuery( "#item-dialog-history" ).html( data );
			if( !keepStatus ) jQuery( "#dialog-status" ).html( "" ).removeClass( );
		},
		error: function( ) {
			jQuery( "#dialog-status" ).html( "There was an error retrieving the history" )
				.removeClass( ).addClass( "ui-state-error" );
		}
	} );
}

function updateItem(item_id){
	var newAmt = jQuery('#newAmt' + item_id).val();
	var type = jQuery('#newType' + item_id).val();
	var bid = jQuery('#global-businessid').val();
	
	jQuery( "#dialog-status" ).html( "Saving..." ).removeClass( )
		.addClass( "ui-state-default" );
	jQuery('#editForm' + item_id).html('Saving...<br><p><br>');
	jQuery.ajax( {
		type: 'POST',
		cache: false,
		url: "/ta/buscatermenu5-1.php",
		data: {
			step: "E",
			item: item_id,
			amt: newAmt,
			type: type,
			bid: bid
		},
		dataType: "html",
		success: function( data ) {
			jQuery( "#dialog-status" ).html( "Record Updated" ).removeClass( )
				.addClass( "ui-state-default" );
			var item_code = jQuery('#itemcode').val();
			refresh_item_history( item_code );
		},
		error: function( ) {
			alert('error');
		}
	} );
}

function open_dialog( itemId, itemName, machineNum ) {
	jQuery( "#item-dialog-product" ).html( itemId + " (" + itemName + ")" );
	jQuery( "#item-dialog-history" ).html( "" );
	jQuery( "#itemcode" ).val( itemId );
	jQuery( "#machine_num" ).val( machineNum );

	jQuery( "#dialog-form" ).dialog( "open" );

	refresh_item_history( itemId );

	return;
}

jQuery( document ).ready( function( ) {

	jQuery( ".max-order-change" ).live("click", function(event) {
		jQuery( "#dialog-form-item" ).dialog( "open" );
		jQuery("#dialog-form-item-data-id").val(jQuery(this).data("item-id"));
		jQuery("#dialog-form-item-data-max").val(jQuery(this).data("max"));
		jQuery("#dialog-form-item-data-reorder-point").val(jQuery(this).data("reorder-point"));
		jQuery("#dialog-form-item-data-reorder-amount").val(jQuery(this).data("reorder-amount"));
		jQuery( "#dialog-form-item" ).data( "element", this );
	});

	jQuery( "#amount" ).keypress( function( e ) {
		if( e.keyCode == 13 ) {
			jQuery( '.ui-dialog-buttonpane button:contains(Save)' ).trigger( 'click' );
			return false;
		}
	} );

	jQuery( 'select > option:[selected]', '#dialog-form-data' ).attr( "default", true );

	jQuery( "#upload_file_link" ).click( function( ) {
		jQuery( "#dialog-upload" ).dialog( "open" );
		return false;
	} ).addClass( "ui-state-default" );

	jQuery( "#dialog-upload" ).dialog( {
		autoOpen: false,
		height: 500,
		width: 450,
		modal: true,
		position: 'top',
		buttons: {
			Cancel: function( ) {
				jQuery( this ).dialog( "close" );
			},
			"Clear Results": function( ) {
				jQuery( "tr:first", "#file_list" ).nextAll( ).remove( );
			}
		},
		close: function( ) {
			jQuery( "#dialog-form-upload" )[0].reset( );
			jQuery( "#dialog-upload-status" ).removeClass( ).html( "" );
		}
	} );

	jQuery( '#dialog-form-upload' ).fileUploadUI( {
		uploadTable: jQuery( '#file_list' ),
		dragDropSupport: false,
		buildUploadRow: function( files, index ) {
			var fname = files[index].name.split( '\\' ).pop( );
			return jQuery( '<tr><td><small>' + fname + '</small><\/td>'
							+ '<td class="file_upload_progress"><div /><\/td>'
							+ '<td class="file_upload_cancel">'
							+ '<button class="ui-state-default ui-corner-all" title="Cancel">'
							+ '<span class="ui-icon ui-icon-cancel">Cancel<\/span>'
							+ '<\/button><\/td><\/tr>' );
		},
		onLoad: function( event, files, index, xhr, handler ) {
			var data = jQuery.parseJSON( typeof xhr.responseText !== 'undefined' ? xhr.responseText
				: xhr.contents( ).text( ) );
			response = jQuery.extend( {}.dataResponse, data );
			if( response.status == "SUCCESS" ) {
				jQuery( handler.uploadRow ).find( ".file_upload_progress" ).html( response.message )
					.addClass( "ui-state-default" );
			} else if( response.status === "ERROR" ) {
				jQuery( handler.uploadRow ).find( ".file_upload_progress" ).html( response.message )
					.addClass( "ui-state-error" );
			} else {
				jQuery( handler.uploadRow ).find( ".file_upload_progress" )
					.html( "There was an error uploading the file" ).addClass( "ui-state-error" );
			}
			jQuery( "button", handler.uploadRow ).attr( "title", "Delete from List" );
		},
		onError: function( event, files, index, xhr, handler ) {
			jQuery( handler.uploadRow ).html( "An unknown error has occurred" )
				.addClass( "ui-state-error" );
		}
	} );


	jQuery( "#dialog-form" ).dialog( {
		autoOpen: false,
		height: 500,
		width: 450,
		modal: true,
		position: 'top',
		buttons: {
			"Save": function( ) {
				var dialog_buttons = jQuery( '.ui-dialog-buttonpane button' );
				dialog_buttons.addClass( 'ui-state-disabled' );
				jQuery( "#dialog-status" ).html( "Sending data" ).removeClass( )
					.addClass( "ui-state-default" );
				jQuery.ajax( {
					type: 'POST',
					cache: false,
					url: "/ta/buscatermenu5-1.php",
					data: jQuery( "#dialog-form-data" ).serialize( ),
					dataType: "html",
					success: function( data ) {
						data = jQuery.parseJSON( data );
						response = jQuery.extend( {}.dataResponse, data );
						if( response.status == "SUCCESS" ) {
							jQuery( ':text, :password, :file', '#dialog-form-data' ).val( '' );
							//jQuery( 'select > option[selected]', '#dialog-form-data' )
								//.removeAttr( "selected" );
							//jQuery( 'select > option[default]', '#dialog-form-data' )
								//.attr( "selected", "selected" );
							refresh_item_history( jQuery( "#itemcode" ).val( ), true );
							jQuery( "#dialog-status" ).html( response.message ).removeClass( )
								.addClass( "ui-state-default" );
						} else if( response.status == "ERROR" ) {
							jQuery( "#dialog-status" ).html( response.message ).removeClass( )
								.addClass( "ui-state-error" );
						} else {
							jQuery( "#dialog-status" ).html( "There was an error adding the item" )
								.removeClass( ).addClass( "ui-state-error" );
						}
					},
					error: function( ) {
						jQuery( "#dialog-status" ).html( "There was an error calling the script" )
							.removeClass( ).addClass( "ui-state-error" );
					},
					complete: function( ) {
						dialog_buttons.removeClass( 'ui-state-disabled' );
					}
				} );
			},
			Cancel: function( ) {
				jQuery( this ).dialog( "close" );
			}
		},
		close: function( ) {
			jQuery( "#dialog-form-data" )[0].reset( );
			jQuery( "#dialog-status" ).removeClass( ).html( "" );
		}
	} );


	jQuery( "#dialog-form-item" ).dialog( {
		autoOpen: false,
		height: 250,
		width: 450,
		modal: true,
		position: 'top',
		buttons: {
			"Save": function() {
				var dialog_buttons = jQuery( '.ui-dialog-buttonpane button' );
				dialog_buttons.addClass( 'ui-state-disabled' );

				itemId = jQuery("#dialog-form-item-data-id").val();

				if (itemId < 1) {
					return;
				}

				var form = {
					"bid": jQuery("#dialog-form-item-data-bid").val(),
					"changes": {}
				};
				form["changes"][itemId] = {
					"max": {
						"value": jQuery("#dialog-form-item-data-max").val()
					},
					"reorder_point": {
						"value": jQuery("#dialog-form-item-data-reorder-point").val()
					},
					"reorder_amount": {
						"value": jQuery("#dialog-form-item-data-reorder-amount").val()
					}
				};

				jQuery.ajax( {
					type: 'POST',
					cache: false,
					url: "/ta/businessMenus/posItem",
					data: form,
					dataType: "json",
					success: function( data ) {
						var element = jQuery( "#dialog-form-item" ).data( "element" );
						var max = parseInt(jQuery("#dialog-form-item-data-max").val(), 10);
						var reorderPoint = parseInt(jQuery("#dialog-form-item-data-reorder-point").val(), 10);
						var reorderAmount = parseInt(jQuery("#dialog-form-item-data-reorder-amount").val(), 10);

						jQuery( element ).data( "max", max );
						jQuery( element ).data( "reorder-point", reorderPoint );
						jQuery( element ).data( "reorder-amount", reorderAmount );
						jQuery( element ).text( "" + max + "/" + reorderPoint );

						var status = jQuery( "#dialog-item-status" ).removeClass();
						if( "errors" in data && data.errors.length > 0 ) {
							var message = "";
							var i = data.errors.length - 1;
							for( ; i >= 0; i-- ) {
								message += data.errors[i] + "<br />";
							}
							status.html( message ).addClass( "ui-state-error" );
						} else {
							jQuery( ':text', '#dialog-form-item-data' ).val( '' );
							status.html( "Save completed" ).addClass( "ui-state-default" );
						}
					},
					error: function() {
						jQuery( "#dialog-item-status" )
							.html( "There was an error calling the script" )
							.removeClass()
							.addClass( "ui-state-error" );
					},
					complete: function() {
						dialog_buttons.removeClass( 'ui-state-disabled' ).addClass("ui-state-default");
					}
				} );
			},
			Cancel: function() {
				jQuery(this).dialog("close");
			}
		},
		close: function() {
			jQuery("#dialog-form-item-data")[0].reset();
			jQuery("#dialog-item-status")
				.removeClass()
				.html("");
		}
	} );


	jQuery( '.bubbleInfo' ).each( function( ) {
		// options
		var distance = 10;
		var time = 250;
		var hideDelay = 500;

		var hideDelayTimer = null;

		// tracker
		var beingShown = false;
		var shown = false;

		var trigger = jQuery( this ).parent( ).find( '.trigger' );
		var popup = jQuery( '.popup', this ).css( 'opacity', 0 );

		// set the mouseover and mouseout on both element
		jQuery( [trigger.get( 0 ), popup.get( 0 )] ).mouseover( function( ) {
			// stops the hide event if we move from the trigger to the
			// popup element
			if( hideDelayTimer ) clearTimeout( hideDelayTimer );

			// don't trigger the animation again if we're being shown,
			// or already visible
			if( beingShown || shown ) {
				return;
			} else {
				beingShown = true;

				// reset position of popup box
				popup.css( {
					top: -100,
					left: -33,
					display: 'block' // brings the popup back in to view
				} )

				// (we're using chaining on the popup) now animate it's
				// opacity and position
				.animate( {
					top: '-=' + distance + 'px',
					opacity: 1
				}, time, 'swing', function( ) {
					// once the animation is complete, set the tracker
					// variables
					beingShown = false;
					shown = true;
				} );
			}
		} ).mouseout( function( ) {
			// reset the timer if we get fired again - avoids double
			// animations
			if( hideDelayTimer ) clearTimeout( hideDelayTimer );

			// store the timer so that it can be cleared in the
			// mouseover if required
			hideDelayTimer = setTimeout( function( ) {
				hideDelayTimer = null;
				popup.animate( {
					top: '-=' + distance + 'px',
					opacity: 0
				}, time, 'swing', function( ) {
					// once the animate is complete, set the tracker
					// variables
					shown = false;
					// hide the popup entirely after the effect (opacity
					// alone doesn't do the job)
					popup.css( 'display', 'none' );
				} );
			}, hideDelay );
		} );
	} );
} );
