$(document).ready(function() {
	var num_selected = 0;

	/* load promo group with ajax call */
   $('#promo_groups_list').click(function(){
		clear_message();
		clear_form();
		
		$('li').removeClass('highlight');
		
		$('#loading').show();
   
		$.post('promomaster/ajaxLoadPromoGroup', {'promo_group_id' : this.value}, function(data) {			
			if(data.pass){
				$('#promo_group_name').val(data.group_name);
				$('#promo_group_id').val(data.group_id);
				
				for(var x=0; x<data.item_ids.length; x++){
					$('#item_'+data.item_ids[x]).attr('checked','checked');
					$('#item_'+data.item_ids[x]).parent().addClass('highlight');
					num_selected++;
				}
				
				$.each(data.items_in_cat, function(key,val){
					$('#catid_'+key+' span').text(val);
					$('#catid_'+key+' div').addClass('highlight');
				});

			}
		})
		.complete(function(){$('#loading').hide();});
   });
   
   /* add/remove highlighting when checking/unchecking a checkbox and update number of selected */   
   $('#frm_promo_groups [type=checkbox]').change(function(){		
		if( $(this).attr("checked") ){
			add_selected(this);
		} else {
			subtract_selected(this);
		}
   });
   
   /* add selected item to count of total in category */
   function add_selected(current_el){
		$(current_el).parent().addClass('highlight');
			
		// add 1 to num selected
		var el = $(current_el).parents('.category').prev().find('span');
		var value = parseInt( el.text() ) + 1;
		el.text(value);
		
		el.parent().addClass('highlight');
		
		num_selected++;
   }
   
   /* subtract selected item from count of total in category */
   function subtract_selected(current_el){
		$(current_el).parent().removeClass('highlight');
			
		// subtract 1 to num selected
		var el = $(current_el).parents('.category').prev().find('span');
		var value = parseInt( el.text() ) - 1;
		el.text(value);
		
		if(value == 0){
			el.parent().removeClass('highlight');
		}
		
		num_selected--;
   }

	/* create promo group with ajax call */	
	$('#frm_promo_groups').submit(function(event){
		event.preventDefault();
		
		clear_message();
		
		if(num_selected > 0){		
			$('#loading').show();
			
			var form_vals = $('#frm_promo_groups').serialize();
			
			$.post('promomaster/ajaxCreatePromoGroup',
			form_vals,
			function(data) {			
				if(data.passed){
					load_menu(false);
					
					$('#res_message').html('Promo Group <span style="color:green;">successfully</span> saved.');
				} else {
					if(data.error == 'req_missing'){
						$('#res_message').html('Missing <span style="color:red;">required field</span>: Promo Group Name.');
					}
				}
			})
			.complete(function(){$('#loading').hide();});		
		} else {
			$('#res_message').html('<span style="color:red;">No items selected</span>.');
		}
	});
	
	/* reset form */
   $('#clear').click(function(){		
		clear_message();
		clear_form();
   });
   
   /* reload entire promo group list thru ajax */
	function load_menu(show_throbber){		
		if(show_throbber){
			$('#loading').show();
		}
		
		$.post('promomaster/ajaxAllPromoGroups',		
		function(data) {			
			if(data != 'failed'){				
				$('#promo_groups_list').html(data);
			} else {
				$('#res_message').html('<span style="color:red;">No results</span> found.');
			}
		})
		.complete(function(){
			if(show_throbber){
				$('#loading').hide();
			}
		});
	}
   
   /* delete promo group and associated promo group detail rows with ajax call */
   $('#delete').click(function(){	
		clear_message();
		
		var delete_it = confirm_delete();
		
		if(delete_it){
			$('#loading').show();
		
			$.post('promomaster/ajaxDeletePromoGroup',
			{'promo_group_name' : $('#promo_group_name').val(), 'promo_group_id' : $('#promo_group_id').val()},
			function(data) {			
				if(data.passed){				
					$("#promo_groups_list option[value='"+data.promo_group_id+"']").remove();
					
					$('#res_message').html('Promo Group and associated Items <span style="color:green;">successfully</span> deleted.');
				} else {
					$('#res_message').html('Promo Group and associated Items <span style="color:red;">failed</span> to delete.');
				}
			})
			.complete(function(){$('#loading').hide(); clear_form(); $('li').removeClass('highlight');});
		}
   });
   
   /* show confirmation of deletion popup */
   function confirm_delete(){
		var r = confirm("Really delete the promo group and associated items?");
		if (r){
			return true;
		} else {
			return false;
		}
	}
   
   /* select all items in category */
   $('.select_all').click(function(){
		$(this).parent().parent().find('input:not(:checked)').each(function(index){
			$(this).attr("checked",true);
			add_selected(this);
		});
   });
   
   /* deselect all items in category */
   $('.deselect_all').click(function(){
		$(this).parent().parent().find('input:checked').each(function(){
			$(this).attr("checked",false);
			subtract_selected(this);
		});
   });
   
   /* hide all category groups */
   $('#hide').click(function(){
		$('.category').hide();
   });
   
   /* show all category groups */
   $('#show').click(function(){
		$('.category').show();
   });
   
   /* hide/show selected category group */
   $('.cat').click(function(){
		$(this).next().toggle();
   });
   
   /* clear message div */
   function clear_message(){
		$('#res_message').html('');
   }
   
   /* reset form */
   function clear_form(){
		$('#frm_promo_groups')[0].reset();
		$('#promo_group_id').val('-1');
		
		$('.cat span').each(function(){
			$(this).text('0');
		});
		
		$('.cat div').removeClass('highlight');
		$('.category li').removeClass('highlight');
		
		num_selected = 0;
	}
});