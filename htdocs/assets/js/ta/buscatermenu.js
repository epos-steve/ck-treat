function popup( url ) {
	popwin = window
		.open( url, "Nutrition", "location=no,menubar=no,titlebar=no,resizeable=no,height=250,width=400" );
}

function toggle_active( menu_item_id ) {
	var img = $( 'img' + menu_item_id );
	var ex = img.src.split( '/' );
	var src = ex[ex.length - 1];
	if( src == "on.gif" ) {
		img.src = "/ta/off.gif";
		img.alt = "INACTIVE";
		var val = 1;
		$( 'imgname' + menu_item_id ).style.textDecoration = 'line-through';
	} else {
		img.src = "/ta/on.gif";
		img.alt = "ACTIVE";
		var val = 0;
		$( 'imgname' + menu_item_id ).style.textDecoration = 'none';
	}
	new Ajax.Request( '/ta/ajaxmenu.php', {
		parameters: {
			'method': 'toggle_active',
			'menu_item_id': menu_item_id,
			'val': val
		}
	} );
}

function del_menu_item( menu_item_id, menu_counter ) {
	var oldcolor = $( 'row' + menu_counter ).style.backgroundColor;
	$( 'row' + menu_counter ).style.backgroundColor = 'yellow';
	$( 'rowD' + menu_counter ).style.backgroundColor = 'yellow';
	if( confirm( 'Are you sure you want to delete this item?' ) ) {
		$( 'rowD' + menu_counter ).hide( );
		$( 'rowC' + menu_counter ).hide( );
		$( 'rowB' + menu_counter ).hide( );
		$( 'row' + menu_counter ).hide( );
		
		new Ajax.Request( '/ta/ajaxmenu.php', {
			parameters: {
				'method': 'del_menu_item',
				'menu_item_id': menu_item_id
			}
		} );
	} else {
		$( 'row' + menu_counter ).style.backgroundColor = oldcolor;
		$( 'rowD' + menu_counter ).style.backgroundColor = oldcolor;
	}
}

function edit_menu_item( menu_item_id, menu_counter ) {
	$( 'row' + menu_counter ).style.backgroundColor = 'orange';
	//$( 'reportFloater' ).show( );
	// $('rowD'+menu_counter).show();
	initScrollBox( );
	
	new Ajax.Request( '/ta/ajaxmenu.php', {
		parameters: {
			'method': 'edit_menu_item',
			'menu_item_id': menu_item_id,
			'menu_counter': menu_counter
		}
	} );
}

function close_menu_item( menu_item_id, menu_counter ) {
	// $('rowC'+menu_counter).blindUp({duration: 1});
	$( 'reportFloater' ).hide( );
	$( 'row' + menu_counter ).highlight( {
		startcolor: '#FFA500',
		endcolor: '#FFFFFF',
		duration: 2.5
	} );
	$( 'row' + menu_counter ).style.backgroundColor = '#FFFFFF';
	
	new Ajax.Request( '/ta/ajaxmenu.php', {
		parameters: {
			'method': 'update_menu_item',
			'menu_item_id': menu_item_id
		}
	} );
}

function hide_show_leads( which ) {
	if( which == 1 ) {
		var rows = $$( '.inactive' );
		rows.each( function( row ) {
			row.hide( );
		} );
	} else {
		var rows = $$( '.inactive' );
		rows.each( function( row ) {
			row.show( );
		} );
	}
}

var grabScreenPOS = function( ) {
	if( $( 'reportFloater' ) ) { // Make sure DIV exists on page on page
		var b = $( 'reportFloater' ); // The Div Tag
		var bh = b.getHeight( ); // Div Height
		var wh = $( 'divbox' ).getHeight( ); // Window Height: Hard Coded to
												// avoid IE Bug
		var co = document.viewport.getScrollOffsets( ); // Array of scroll
														// offsets
		var sh = co[1]; // ScrollTop
		
		var n = ( wh / 2 ); // Grab 1/2 Window height
		n -= ( bh / 2 ); // Substract 1/2 Box height [This gets it centered]
		n += sh; // Add the scroll offset [Make it follow scrollbar]
		
		return n;
	}
};

var followScroll = function( ) {
	if( $( 'reportFloater' ) ) { // Make sure DIV exists on page on page
		var b = $( 'reportFloater' ); // The Div Tag
		
		var n = grabScreenPOS( );
		b.style.top = n + 'px'; // Set the new top value to you're div
	}
};
var initScrollBox = function( ) {
	if( $( 'reportFloater' ) ) { // Make sure DIV exists on page on page
		var b = $( 'reportFloater' ); // The Div Tag
		
		var n = grabScreenPOS( );
		b.style.top = n + 'px'; // Set the new top value to you're div
		
		if( !b.visible( ) ) b.show( ); // If display:none => set display: block
		// [Gets rid of jump onLoad]
	}
};

// Event.observe(window,'load', initScrollBox); //Make the box follow on page
// load
Event.observe( window, 'scroll', followScroll ); // Make the box follow on
													// page scroll

function delconfirm( ) {
	return confirm( 'Are you sure you want to delete this group?' );
}
function delconfirm2( ) {
	return confirm( 'Are you sure you want to delete this item?' );
}
function confirmsave( ) {
	return confirm( 'Are you sure you want to update these items?' );
}

function changePage( newLoc ) {
	nextPage = newLoc.options[newLoc.selectedIndex].value;
	
	if( nextPage != "" ) {
		document.location.href = nextPage;
	}
}

function checkKey( ) {
	
	var key = event.keyCode;
	var DaIndexId = event.srcElement.IndexId - 0;
	if( key == 38 ) {
		document.getElementByIndex( DaIndexId - 1 ).focus( );
		return false;
	} else if( key == 40 ) {
		document.getElementByIndex( DaIndexId + 1 ).focus( );
		return false;
	} else if( key == 37 ) {
		document.getElementByIndex( DaIndexId - 1000 ).focus( );
		return false;
	} else if( key == 39 ) {
		document.getElementByIndex( DaIndexId + 1000 ).focus( );
		return false;
	}
	return true;
} // end checkKey function

document.getElementByIndex = function( ) {
	for( var i = 0; i < document.all.length; i++ )
		if( document.all[i].IndexId == arguments[0] ) return document.all[i];
	return null;
};
