$ = jQuery

scrollTo = (element, complete = null) ->
    properties =
        scrollTop: $(element).offset().top - 40
    options =
        duration: 500

    if complete?
        options['complete'] = () ->
            complete(element)

    $('html body').animate(properties, options)


backgroundFade = (element, color = '#009900') ->
    properties =
        backgroundColor: color
    $(element).animate(
        properties
        750
        () ->
            $(element).animate(
                    backgroundColor: '#ffffff'
                750
            )
    )



class MenuItemRow
    id          : 0
    new_price   : false
    cost        : false
    tax_id      : false
    active      : false
    group_id    : false
    ordertype   : false
    max         : false
    item_code   : false
    upc         : false
    ebt_fs_allowed  : false
    reorder_point  : false
    reorder_amount : false

    constructor : (id) ->
        @id = id

    isQueued : (name) ->
        if @[name]?
            return @[name] is true

    queue : (name) ->
        if @[name]?
            @[name] = true
        return @

    unqueue : (name) ->
        if @[name]?
            @[name] = false
        return @

    count : () ->
        length = 0
        if @new_price is true
            length++
        if @cost is true
            length++
        if @tax_id is true
            length++
        if @active is true
            length++
        if @group_id is true
            length++
        if @ordertype is true
            length++
        if @max is true
            length++
        if @item_code is true
            length++
        if @upc is true
            length++
        if @reorder_point is true
            length++
        if @reorder_amount is true
            length++
        if @ebt_fs_allowed is true
            length++
        return length


class ActiveQueue
    store : []

    constructor: () ->
        @store = []

    menuItemExists : (id) ->
        for index, item of @store
            if id is item.id
                return index
        return -1

    menuItemCount : (id) ->
        index = @menuItemExists id
        if index > -1
            return @store[index].count()
        return 0

    addMenuItem : (id, fieldName) ->
        index = @menuItemExists id
        if index < 0
            length = @store.push new MenuItemRow id
            index = @menuItemExists id
        @store[index].queue fieldName

    removeMenuItem : (id, fieldName) ->
        index = @menuItemExists(id, fieldName)
        if index > -1
            @store[index].unqueue fieldName

    count : () ->
        length = 0;
        for index, item of @store
            if item.count() > 0
                length++
        return length

window.menuItemsActiveQueue = new ActiveQueue


class SaveQueueItem
    itemId : 0
    fieldName : ''
    isCheckbox : false
    isChecked : false

    element : null
    trElement: null

    constructor: (element) ->
        @element = element
        @trElement = $(element).parentsUntil('tr', 'td').parent()
        @itemId = parseInt $(element).data('id')
        @fieldName = $(element).data('field-name')
        @isCheckbox =  $(element).is(':checkbox')
        if @isCheckbox
            @isChecked = $(element).is(':checked')

        @start()
        @displayCount()

    getValue : () ->
        return $(@element).val()

    getCheckedValue : () ->
        return if @isChecked then 1 else 0

    start : () ->
        window.menuItemsActiveQueue.addMenuItem @itemId, @fieldName

    finished : () ->
        window.menuItemsActiveQueue.removeMenuItem @itemId, @fieldName

    success : (data) ->
        @finished()

        if not (data?)
            @displayError "Something unexpected might have happened. Refresh and check your work."
            @finished()
            return

        if data.errors? and data.errors.length > 0
            for index, error of data.errors
                @displayError error
        else if data.updates? and data.updates.completed < data.updates.queued
            @displayError "Only #{data.updates.completed} of #{data.updates.queued} updates completed."

        if data.fields[@fieldName]?
            status = data.fields[@fieldName]
            if status.didSave isnt true and status.didSave isnt 1
                @revertChange status.value.old || $(@element).data('current-value')
            else
                if @fieldName is 'ordertype'
                    @handleOrderType status.value.old
                else if @fieldName is 'active'
                    showall = if parseInt($('#pos-save-multi-form').data('showall')) is 1 then true else false
                    if parseInt(status.value.new) is 1 and showall is false
                        $(@trElement).css 'background-color', '#009900'
                        $(@trElement).fadeOut 1500
                $(@element).data('current-value', status.value.new)
        @displayCount()

    failure : (status, error) ->
        @finished()
        @displayError "#{status} : #{error}"
        @revertChange()
        @displayCount()

    displayCount : () ->
        count = window.menuItemsActiveQueue.menuItemCount @itemId
        if count < 1
            $(@trElement).find('.update-img').hide()
            $(@trElement).find('.update-counter-num').text(count).hide()
        else
            $(@trElement).find('.update-img').show()
            $(@trElement).find('.update-counter-num').show().text(count)


    displayError: (message) ->
        itemId = @itemId
        siblings = $(@trElement).siblings('.menu-item-error-message').filter (index) ->
            checkItemId = `$(this).data('item-id')`
            return itemId is checkItemId

        if siblings.length > 0
            siblings.find('th').append([message, '<br />'].join(' '))
        else
            html = """
                   <tr class="menu-item-error-message" data-item-id="#{itemId}">
                   <th colspan="18" style="border: 2px solid #990000; text-align: center;">
                   <img src="/assets/images/close2.gif" class="remove-error-row"
                   style="float: right; width: 10px; height: 10px; margin: 4px;">
            #{message}<br />
                   </th>
                   </tr>
                   """
            $(@trElement).after(html)

    revertChange: (value = null) ->
        if not (value?)
            value = $(@element).data('current-value')

        if $(@element).is(':checkbox')
            if parseInt value isnt 1
                $(@element).removeAttr('checked')
            else
                $(@element).attr('checked', 'checked')
        else
            $(@element).val value
        backgroundFade @trElement, '#aa0000'

    handleOrderType: (oldValue) ->
        oldValue = parseInt oldValue
        orderType = parseInt $(@element).val()
        itemName = $(@trElement).find('.item-name').eq(0).text()
        if oldValue isnt orderType
            filterTables = () ->
                `$(this).data('ordertype-id') == orderType`

            filterRows = () ->
                `itemName.localeCompare($(this).find('.item-name').eq(0).text()) <= 0`

            filteredTables = $('.menu-items').filter(filterTables)

            if filteredTables.length > 0
                tableElement = $(filteredTables).eq(0)

                rows = $(tableElement).find 'tbody tr'
                filteredRows = $(rows).filter filterRows

                tbody = $(tableElement).find('tbody')
                if tbody.is ':hidden'
                    $(tableElement).find('img.menu-item-open').hide()
                    $(tableElement).find('img.menu-item-close').show()
                    tbody.show()
                clonedElement = $(@trElement).detach()

                if filteredRows.length > 0
                    $(filteredRows).eq(0).before clonedElement
                else
                    tbody.append clonedElement

                scrollTo clonedElement, (element) ->
                    backgroundFade element
        return true


class SaveQueue
    queued : []
    processingAmount : 0
    doingProcess : false
    businessId : 0

    constructor : () ->
        $ =>
            @businessId = $('#pos-save-multi-form').data('business-id')

        runProcess = () =>
            @process()
        window.setInterval(runProcess, 5000)

    hasQueued : () ->
        return @queued.length > 0 or @doingProcess is true

    count : () ->
        return @queued.length + @processingAmount

    queue : (item) ->
        @queued.push item

    process : () ->
        if @queued.length < 1 or @businessId < 1 or @doingProcess is true
            return

        @doingProcess = true

        params =
            'bid' : @businessId
            'changes' : {}

        max = if @queued.length > 5 then 5 else @queued.length
        @processingAmount = max

        changes = []
        # Start at one because starting at '0' would add 1 to the end.
        for num in [1..max]
            changes.push @queued.shift()

        for index, item of changes
            if not (item?)
                continue
            if not (params.changes[item.itemId]?)
                params.changes[item.itemId] = {}

            params.changes[item.itemId][item.fieldName] = {
                'value' : item.getValue()
                'checked' : null
            }

            if item.isCheckbox
                params.changes[item.itemId][item.fieldName].checked = item.getCheckedValue()

        $.ajax(
            url: "/ta/businessMenus/posItem"
            data: params
            dataType: 'json'
            timeout: 10000
            type: 'POST'
            success: (data) =>
                for index, item of changes
                    if data[item.itemId]?
                        item.success(data[item.itemId])
                    else
                        @queued.push item
            error: (xhr, status, error) =>
                if error is 'timeout' or status is 'timeout'
                    for index, item of changes
                        @queued.push item
                    return
                status = status or 'HTTP'
                for index, item of changes
                    item.failure status, error
            complete : () =>
                @doingProcess = false
                @processingAmount = 0
        )


window.menuItemsSaveQueue = new SaveQueue

###
Focuses on the possible change state between the queued state.

The problem exists that since the queue is only inserted when the field is
changed, when the field is focused this event will not run. It is possible then
that a field has been changed, but has not yet lost focus.

This class and code will inform the user that changes might still need to be
made.
###
class ItemChangeState
    inTextbox : false
    hasKeyPressed : false

    constructor : () ->
        $(':text').live('focus', (event) =>
            @inTextbox = true
        )

        $(':text').live('keypress', (event) =>
            @hasKeyPressed = true
        )

        $(':text').live('mouseup', (event) =>
            switch event.which
                when 3
                    @hasKeyPressed = true
        )

        $(':text').live('blur', (event) =>
            @inTextbox = false
            @hasKeyPressed = false
        )

    hasChanges : () ->
        return @inTextbox is true and @hasKeyPressed is true

# Might not be needed, but attempt to prevent the object from going out of scope.
window.menuItemsActiveChanges = new ItemChangeState


window.onbeforeunload = (event) ->
    if window.menuItemsActiveChanges.hasChanges()
        return "The current item was not saved."

    if window.menuItemsSaveQueue.hasQueued()
        count = window.menuItemsSaveQueue.count()
        if count > 0
            return "There are currently #{count} items being updated."
        else
            return "Finishing updates, please wait."


class GuiControlHeader
    constructor: (menuItems) ->
        $(menuItems).filter(':not(:first)').find('tbody').hide()
        $(menuItems).find('.item-header').click @action

    action: (event) ->
        tbody = $(event.currentTarget).parentsUntil('table.menu-items').parent().find('tbody')
        if tbody.is(':visible')
            $(event.currentTarget).find('img.menu-item-open').show()
            $(event.currentTarget).find('img.menu-item-close').hide()
            tbody.hide()
        else
            $(event.currentTarget).find('img.menu-item-open').hide()
            $(event.currentTarget).find('img.menu-item-close').show()
            tbody.show()
        scrollTo event.currentTarget


class GuiControlSaveCell
    constructor: () ->
        $('.update-item').live 'change', (event) =>
            @trigger event

    trigger: (event) ->
        window.menuItemsSaveQueue.queue( new SaveQueueItem(event.currentTarget) )


class MenuItemsSetup
    headerControl : null
    saveCellControl : null

    constructor: () ->

    events : () ->
        $('.remove-error-row').live('click', (event) =>
            $(event.currentTarget).parentsUntil('tr').parent().remove()
        )

    guiOpener: () ->
        @headerControl = new GuiControlHeader $('table.menu-items')

    saveContent: () ->
        @saveCellControl = new GuiControlSaveCell


$ =>
    setup = new MenuItemsSetup
    setup.guiOpener()
    setup.events()
    setup.saveContent()