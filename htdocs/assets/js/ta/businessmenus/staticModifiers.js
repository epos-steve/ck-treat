jQuery( function( ) {
	StaticModifierController.initialize( );
} );

var StaticModifierController = {
	/** @memberOf StaticModifierController */
	isInitialized: false,
	
	gridSize: 0,
	selectedIndex: null,
	
	rows: null,
	cols: null,
	groups: null,
	dataReady: false,
	
	initialize: function( ) {
		if( StaticModifierController.isInitialized )
			return;
		
		StaticModifierController.isInitialized = true;
		
		var self = this;
		
		jQuery( '#staticModifierForm' ).ajaxedit( {
			data: { __get: { bid: jQuery( '#mdockbid' ).val( ) }, __url: 'getStaticModifierSettings' },
			resetAfterSubmit: true,
			datachange: function( evt, data ) {
				var needUpdate = self.dataReady && ( self.cols != data.mdock_cols || self.rows != data.mdock_rows );
				self.rows = data.mdock_rows;
				self.cols = data.mdock_cols;
				if( needUpdate ) {
					self.showModifierGroups( );
				}
			}
		} );
		
		StaticModifierController.initializePanels( );
		
		StaticModifierController.initializeDialogs( );
		
		StaticModifierController.initializeButtons( );
		
		StaticModifierController.getModifierGroups( );
	},
	
	initializePanels: function( ) {
		StaticModifierController.panel = jQuery( '#mdockGroups' ).aerispanel( {
			reorder: StaticModifierController.updateOrder,
			autoAdjust: true,
			constrained: true,
			change: function( ) {
				jQuery( '#mdockRemoveGroup' ).show( );
			}
		} );
	},
	
	initializeDialogs: function( ) {
		jQuery( '#mdockAddGroupDialog' ).dialog( {
			autoOpen: false,
			hide: 'slide',
			show: 'slide',
			modal: true,
			resizable: false,
			title: 'Add Modifier Group',
			height: 350,
			buttons: {
				'Add': function( ) {
					var dialog = jQuery( this );
					
					var bid = jQuery( '#mdockbid' ).val( );
					var groups = [];
					jQuery( '#mdock_AddGroupSelect :selected' ).each( function( ) {
						groups.push( jQuery( this ).attr( 'id' ) );
					} );
					
					jQuery.ajax( {
						type: 'POST',
						url: 'addStaticModifierToDock',
						dataType: 'json',
						data: { bid: bid, groups: groups },
						success: function( data ) {
							if( !data || !data.success || data.error ) {
								alert( data && data.error || 'There was an error processing your request.' );
								return false;
							}
							
							dialog.dialog( 'close' );
							StaticModifierController.getModifierGroups( );
						}
					} );
				}
			}
		} );
	},
	
	initializeButtons: function( ) {
		jQuery( '#mdockAddGroup' ).button( {
			text: true,
			icons: { primary: 'ui-icon-plusthick' }
		} ).click( function( event ) {
			event.preventDefault( );
			jQuery( '#mdockAddGroupDialog' ).dialog( 'open' );
			StaticModifierController.getGroupList( );
		} );
		
		jQuery( '#mdockRemoveGroup' ).button( {
			text: true,
			icons: { primary: 'ui-icon-closethick' }
		} ).click( function( event ) {
			event.preventDefault( );
			
			var bid = jQuery( '#mdockbid' ).val( );
			var selectedGroup = StaticModifierController.panel.aerispanel( 'selected' );
			
			if( !( selectedGroup >= 0 ) )
				return;
			
			var group = StaticModifierController.panel.data( 'groups' )[selectedGroup];
			
			if( confirm( 'Are you sure you want to remove "' + group.name + '"?' ) ) {
				jQuery.ajax( {
					type: 'POST',
					url: 'removeStaticModifierFromDock',
					dataType: 'json',
					data: { bid: bid, id: group.staticId },
					success: function( data ) {
						if( !data || !data.success || data.error ) {
							jQuery.waffle.error( data && data.error || 'There was an error processing your request.' );
							return false;
						}
						
						jQuery.waffle.pop( 'Group "' + group.name + '" removed successfully.' );
						StaticModifierController.getModifierGroups( );
					}
				} );
			}
		} );
	},
	
	getModifierGroups: function( async ) {
		async = async == undefined ? true : async;
		
		jQuery.ajax( {
			url: 'getStaticModifierDock?bid=' + jQuery( '#mdockbid' ).val( ),
			dataType: 'json',
			async: async,
			success: function( data ) {
				if( !data || typeof( data.groups ) == undefined || data.error ) {
					jQuery.waffle.error( data && data.error || 'There was an error processing your request.' );
					return false;
				}
				
				StaticModifierController.setModifierGroups( data );
			}
		} );
	},
	
	setModifierGroups: function( data ) {
		for( var i in data ) {
			StaticModifierController.panel.data( i, data[i] );
		}
		
		this.groups = data.groups;
		
		this.showModifierGroups( );
	},
	
	showModifierGroups: function( ) {
		var panel = {
			rows: this.rows,
			cols: this.cols,
			items: this.groups
		};
		StaticModifierController.panel.aerispanel( 'show', panel );
		this.dataReady = true;
	},
	
	updateOrder: function( ) {
		if( StaticModifierController.updatingOrder ) {
			StaticModifierController.needUpdateOrder = true;
			return;
		}
		
		StaticModifierController.updatingOrder = true;
		StaticModifierController.needUpdateOrder = false;
		
		var data = StaticModifierController.panel.data( 'groups' );
		var order = [];
		
		for( var i in data ) {
			order[order.length] = { id: data[i].staticId, y: data[i].display_y, x: data[i].display_x };
		}
		
		var bid = jQuery( '#mdockbid' ).val( );
		
		return jQuery.ajax( {
			type: 'POST',
			url: 'updateStaticModifierDockOrder',
			dataType: 'json',
			data: { bid: bid, order: order },
			success: function( adata ) {
				if( !adata || !adata.message || adata.error ) {
					jQuery.waffle.error( adata && adata.error || 'There was an error processing your request' );
					return;
				}
				
				StaticModifierController.updatingOrder = false;
				
				if( StaticModifierController.needUpdateOrder ) {
					StaticModifierController.updateOrder( );
				}
			}
		} );
	},
	
	inputTimeout: null,
	inputThis: null,

	delayedInputChange: function( self ) {
		if( StaticModifierController.inputTimeout )
			clearTimeout( StaticModifierController.inputTimeout );
		
		var parent = jQuery( self ).closest( '.itemListColumn' );
		var header = jQuery( '.itemListHeader', parent );
		
		jQuery( '.itemListFilterReset', header )
			.button( 'option', 'disabled', jQuery( self ).val( ).length == 0 );
		
		StaticModifierController.inputThis = this;
		StaticModifierController.inputTimeout = setTimeout( StaticModifierController.startInputChange, 150 );
	},

	startInputChange: function( ) {
		jQuery( '.spinner', jQuery( StaticModifierController.inputThis ).parent( ) ).show( );
		setTimeout( 'StaticModifierController.inputChange( StaticModifierController.inputThis );', 50 );
	},

	inputChange: function( self ) {
		var list = jQuery( '#' + jQuery( self ).attr( 'id' ).replace( 'Filter', 'Select' ) );
		var filterString = jQuery( self ).val( );
		
		list.children( ).removeClass( 'ui-filtered' ).show( );
		if( filterString.length > 0 ) {
			var filterTokens = filterString.toUpperCase( ).split( ' ' );
			
			for( var i in filterTokens ) {
				list.find( ':not(:iContains(' + filterTokens[i] + '))' ).addClass( 'ui-filtered' );
			}
			
			list.find( '.ui-filtered' ).hide( );
		}
		
		jQuery( '.spinner', jQuery( self ).parent( ) ).hide( );
	},
	
	sortList: function( list ) {
		function _compareVal( o ) {
			if( jQuery( o ).is( 'optgroup' ) )
				o = jQuery( 'option:first-child', o );
			return jQuery( o ).data( 'order' ) ? jQuery( o ).data( 'order' )
				: jQuery( o ).attr( 'label' ) ? jQuery( o ).attr( 'label' )
				: jQuery( o ).text( );
		}

		function _compare( a, b ) {
			var compA = _compareVal( a ).toString( ).toUpperCase( );
			var compB = _compareVal( b ).toString( ).toUpperCase( );
			var numA, numB;
			var regex = /^\d+(?=\. |$)/;
			if( ( numA = compA.match( regex ) ) && ( numB = compB.match( regex ) ) ) {
				numA = parseInt( numA[0] );
				numB = parseInt( numB[0] );
				return ( numA < numB ) ? -1 : ( numA > numB ) ? 1 : 0;
			} else {
				return ( compA < compB ) ? -1 : ( compA > compB ) ? 1 : 0;
			}
		}

		function _sortList( selector ) {
			var elem = jQuery( selector );
			var listItems = elem.children( ).get( );
			listItems.sort( _compare );
			jQuery.each( listItems, function( idx, itm ) {
				var ji = jQuery( itm );
				ji.addClass( 'sort' + ji.val( ).substr( 0, 1 ).toUpperCase( ) );
				elem.append( itm );
			} );
		}
		
		_sortList( list );
	},
	
	getGroupList: function( ) {
		var elemName = 'mdock_AddGroup';
		var elem = jQuery( '#mdock_AddGroupSelect' );
		
		if( !elem.is( ':visible' ) ) return;
		
		jQuery( '#' + elemName + 'Spinner' ).show( );
		elem.empty( ).parent( ).addClass( 'far-left' );
		
		jQuery.ajax( {
			url: 'getStaticModifierGroups?bid=' + jQuery( '#mdockbid' ).val( ),
			dataType: 'json',
			success: function( data ) {
				if( !data 
					|| ( typeof( data.content ) == 'undefined' && typeof( data.script ) == 'undefined' && typeof( data.data ) == 'undefined' ) 
					|| data.error 
				) {
					jQuery.waffle.error( data && data.error || 'There was an error retrieving data.' );
					return false;
				}
				
				if( typeof( data.data ) != 'undefined' ) {
					elem.data( 'data', data.data );
					var d = data.data;
					
					for( var i in d ) {
						jQuery( '<option id="' + d[i].id + '">' + d[i].name + '</option>' ).appendTo( elem );
					}
					
					StaticModifierController.sortList( elem );
					
					elem.parent( ).removeClass( 'far-left' );
					jQuery( '#' + elemName + 'Spinner' ).hide( );
				}
				
				StaticModifierController.setupItemList( '#mdock_AddGroupSelect' );
			}
		} );
	},
	
	setupItemList: function( selector ) {
		var list = jQuery( selector );
		var parent = list.parent( );
		var header = jQuery( '.itemListHeader', parent );
		
		list.width( parent.width( ) ).height( parent.height( ) - header.height( ) )
			.addClass( 'ui-widget-content ui-state-default ui-corner-all' );
		
		jQuery( '.itemListFilterReset', header ).button( {
			disabled: true,
			text: false,
			icons: {
				primary: 'ui-icon-cancel'
			}
		} ).click( function( ) {
			var filter = jQuery( '.itemListFilter', header );
			filter.val( '' );
			StaticModifierController.inputChange( filter );
			filter.keyup( ).focusout( );
		} ).removeClass( 'ui-corner-all' ).addClass( 'ui-corner-right' );
		
		jQuery( '.itemListFilter', header ).addClass( 'ui-widget-content ui-corner-left' )
			.width( parent.width( ) - jQuery( '.itemListFilterReset', parent ).width( ) - 9 )
			.after( '<img class="spinner" style="display: none;" src="/assets/images/ajax-loader-redmond.gif" />' )
			.hover( function( ) {
				jQuery( this ).addClass( 'ui-state-hover' );
			}, function( ) {
				jQuery( this ).removeClass( 'ui-state-hover' );
			} ).bind( 'keydown, keypress', StaticModifierController.delayedInputChange ).focusin( function( ) {
				if( jQuery( this ).val( ) == jQuery( this ).data( 'watermark' ) )
					jQuery( this ).removeClass( 'inputWatermark' ).val( '' );
				jQuery( this ).addClass( 'ui-state-focus' );
			} ).focusout( function( ) {
				if( jQuery( this ).val( ) == '' )
					jQuery( this ).addClass( 'inputWatermark' )
						.val( jQuery( this ).data( 'watermark' ) );
				jQuery( this ).removeClass( 'ui-state-focus' );
			} ).data( 'watermark', 'filter items' ).focusout( );
		
		jQuery( '.spinner', header ).show( ).position( {
			my: 'right',
			at: 'right',
			of: header,
			offset: '-22 -3'
		} ).hide( );
		
		list.change( );
		
		parent.removeClass( 'far-left' );
		jQuery( selector + 'Spinner' ).hide( );
	},
	
	
	__end_StaticModifierController: function( ){}
};

