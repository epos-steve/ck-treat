( function( ) {
	"use strict";

	jQuery( function() {
		MenuManagerController.initialize();
	} );

	var MenuManagerController = window.MenuManagerController = {
		/** @memberOf MenuManagerController */
		isInitialized: false,

		initialize: function() {
			MenuManagerController.isInitialized = true;

			jQuery( '#menuPanelGroup' ).panelgroup();

			jQuery( '#menuSpecialForm' ).ajaxedit( {
				floatSubmit: false,
				validateForm: MenuManagerController.validateMissingForm,
				aftersubmit: MenuManagerController.refreshMissingSpecials,
			} ).ajaxedit( 'validateForm' );

			jQuery( '#menuManagerBar' ).find( 'a' ).button();

			jQuery( '#refreshMissingSpecials' ).click( function() {
				MenuManagerController.refreshMissingSpecials();
			} );

			jQuery( '#menuMergeForm' ).ajaxedit( {
				floatSubmit: false,
				validateForm: MenuManagerController.validateMergeForm,
				aftersubmit: MenuManagerController.refreshMergeItems,
			} ).ajaxedit( 'validateForm' );

			jQuery( '#refreshMergeItems' ).click( function() {
				MenuManagerController.refreshMergeItems();
			} );

			jQuery( '#selectMissingItems' ).change( function() {
				jQuery( '#menuSpecialForm' )
					.find( 'input[type="checkbox"]' ).not( this )
					.prop( 'checked', jQuery( this ).prop( 'checked' ) )
					.change( );
			} );
		},

		refresh: function() {
			MenuManagerController.refreshMissingSpecials();
			//MenuManagerController.refreshMergeItems( );
		},

		refreshMissingSpecials: function( async ) {
			async = async == undefined ? true : async;
			jQuery.ajax( {
				url: 'getMissingSpecials?bid=' + jQuery( '#missingbid' ).val(),
				dataType: 'json',
				async: async,
				success: function( data ) {
					if( !data || typeof( data.items ) == undefined || data.error ) {
						jQuery.waffle.error( data && data.error || 'There was an error processing your request.' );
						return false;
					}

					MenuManagerController.setMissingSpecials( data );
				}
			} );
		},

		refreshMergeItems: function( async ) {
			var self = this;
			async = async == undefined ? true : async;

			if( this.waffle ) {
				jQuery.waffle.burn( this.waffle );
				this.waffle = null;
			}
			this.waffle = jQuery.waffle.pop( {
				autoClose: false,
				clickClose: false,
				spinner: true,
				content: 'Refreshing merge items, this will take a minute...',
				weight: -10
			} );

			jQuery.ajax( {
				url: 'getMergeItems?bid=' + jQuery( '#mergebid' ).val(),
				dataType: 'json',
				async: async,
				success: function( data ) {
					if( !data || typeof( data.items ) == undefined || data.error ) {
						jQuery.waffle.error( data && data.error || 'There was an error processing your request.' );
						return false;
					}

					MenuManagerController.setMergeItems( data );
				}
			} ).always( function() {
				if( self.waffle ) {
					jQuery.waffle.burn( self.waffle );
					self.waffle = null;
				}
			} );
		},

		setMissingSpecials: function( data ) {
			var form = jQuery( '#menuSpecialForm' );
			var table = jQuery( '#menuMissingItems' );
			table.find( 'tr' ).not( ':first' ).remove();
			if( !data.items.length ) {
				table.append( '<tr><td colspan="3">No missing specials found.</td></tr>' );
			} else {
				for( var i = 0, il = data.items.length; i < il; i++ ) {
					var item = data.items[i];
					var tr = jQuery( '<tr class="hover" style="border: solid 1px transparent;"/>' );
					tr.append( '<td style="align: center;"><input type="checkbox" name="merge[' + item.menu_item_id +
						']" /></td>' );
					tr.append( '<td>' + item.item_name + '</td>' );
					tr.append( '<td><input type="text" name="item_code[' +
						item.menu_item_id + ']" value="' + item.item_code + '" /></td>' );
					tr.find( 'input' ).data( 'id', item.menu_item_id );
					table.append( tr );

					tr.find( 'input[type=checkbox]' ).change( function() {
						var input = form.find( 'input[name="item_code[' + jQuery( this ).data( 'id' ) + ']"]' );
						setTimeout( function() {
							form.ajaxedit( 'validateField', input );
							form.ajaxedit( 'validateForm' );
						}, 0.1 );
					} );

					tr.click( function( evt ) {
						if( jQuery( evt.srcElement ).is( ':not(input, select, textarea)' ) ) {
							var check = jQuery( 'input[type=checkbox]', this );
							check.prop( 'checked', !check.prop( 'checked' ) );
							check.change();
							return false;
						}
						return true;
					} );
				}
			}
			var inputs = table.find( 'input, select, textarea' );
			form.ajaxedit( 'styleInputs', inputs ).ajaxedit( 'reset' );
			inputs.change();
			form.ajaxedit( 'validateForm' );
		},

		setMergeItems: function( data ) {
			var form = jQuery( '#menuMergeForm' );
			var table = jQuery( '#menuMergeItems' );
			table.find( 'tr' ).not( ':first' ).remove();
			if( !data.items.length ) {
				table.append( '<tr><td colspan="3">No unlinked merge items found.</td></tr>' );
			} else {
				for( var i = 0, il = data.items.length; i < il; i++ ) {
					var item = data.items[i];
					var tr = jQuery( '<tr class="hover" style="border: solid 1px transparent;"/>' );
					tr.append( '<td>' + item.name + '</td>' );
					var input = jQuery( '<input class="validate-type-fn validate-fn-MenuManagerController.validateMergeItemCode" type="text" name="item_code[' +
						item.id + ']" value="' + item.item_code + '" />' );
					input.data( 'item_code', item.item_code ).wrap( '<td />' ).appendTo( tr );
					var td = jQuery( '<td />' );
					tr.append( td );
					var id = item.id;
					var matches = item.matches;
					var sel = jQuery( '<select name="link[' + id + ']"><option value="">---</option></select>' );
					td.append( sel );
					for( var j = 0, jl = matches.length; j < jl; j++ ) {
						var match = matches[j];
						var opt = jQuery( '<option value="' + match.id + '">' + match.name + '</option>' );
						opt.data( 'item_code', match.item_code );
						sel.append( opt );
						//var mid = id + '][' + match.id;
						//td.append( '<div class="ui-helper-clearfix"><input type="checkbox" name="link[' + mid + ']" /><label for="link[' + mid + ']">' + match.name + '</label></div>' );
					}
					table.append( tr );
					tr.find( 'input, select' ).data( 'id', id );

					tr.find( 'select' ).change( function() {
						var input = form.find( 'input[name="item_code[' + jQuery( this ).data( 'id' ) + ']"]' );
						var option = jQuery( this ).find( 'option:selected' );
						var item_code = option.data( 'item_code' ) || "";
						if( ( item_code || !option.val() ) && input.val() === input.data( 'item_code' ) ) {
							input.val( item_code ).data( 'item_code', item_code );
						}
						setTimeout( function() {
							form.ajaxedit( 'validateField', input );
							form.ajaxedit( 'validateForm' );
						}, 0.1 );
					} );
				}
			}
			var inputs = table.find( 'input, select, textarea' );
			form.ajaxedit( 'styleInputs', inputs ).ajaxedit( 'reset' );
			inputs.change();
			form.ajaxedit( 'validateForm' );
		},

		validateMissingItemCode: function( form, field, value ) {
			if( !value ) {
				var id = field.data( 'id' );
				var check = form.find( 'input[name="merge[' + id + ']"]:checked' );
				if( check.length ) {
					return 'Must not be empty';
				}
			}
		},

		validateMergeItemCode: function( form, field, value ) {
			if( !value ) {
				var id = field.data( 'id' );
				var select = form.find( 'select[name="link[' + id + ']"]' );
				if( select.val() !== "" ) {
					return 'Must not be empty';
				}
			}
		},

		validateMissingForm: function( form ) {
			var checks = form.find( 'input[type=checkbox]:checked' );
			if( !checks.length ) {
				return 'No items selected to copy!';
			}
		},

		validateMergeForm: function( form ) {
			var selects = form.find( 'select' ).filter( function() { return jQuery( this ).val() !== ""; } );
			if( !selects.length ) {
				return 'No items selected to link!';
			}
		},

	};
} )( );
