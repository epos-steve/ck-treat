jQuery( function( ) {
	SetupController.initializeSetup( );
} );

var SetupController = {
	/** @memberOf SetupController */
	isInitialized: false,
	
	forms: [ 'display', 'adv' ],
	
	initializeSetup: function( ) {
		SetupController.isInitialized = true;
		
		jQuery( '#setupDisplayForm' ).ajaxedit( {
			floatFieldsets: true,
			data: { __get: { bid: jQuery( '#displaybid' ).val( ) }, __url: 'getMachineInfo' },
			resetAfterSubmit: true
		} );
		
		jQuery( '#setupPanelGroup' ).panelgroup( );
		
		jQuery( '#setupRequestSettings' ).button( ).click( function( ) {
			var b = jQuery( this );
			b.button( 'disable' );
			b.button( 'option', 'label', 'Requesting settings...' );
			jQuery.post( 'setupRequestSettings', { bid: jQuery( '#setupbid' ).val( ) }, function( data ) {
				if( !data || data.error || !data.success ) {
					b.button( 'option', 'label', 'There was a problem requesting settings' + ( data && data.error && ( ': ' + data.error ) || '' ) );
					return jQuery.waffle.error( data && data.error || 'An error has occurred' );
				}
				b.button( 'option', 'label', data.message );
			}, 'json' );
		} );
		
		jQuery( '#setupSyncAll' ).click( function( ) {
			SetupController.syncAll( );
		} );
		
		jQuery( '#setupResetAll' ).click( function( ) {
			SetupController.resetAll( );
		} );
	},
	
	confirmRushTime: function( ) {
		var timezone = parseInt( jQuery( '#setupTZOffset' ).val( ) ) - 6;
		var d = new Date( );
		var utcTime = d.getTime( ) + ( d.getTimezoneOffset( ) * 60000 );
		var locTime = new Date( utcTime + 3600000 * timezone );
		var locHour = locTime.getHours( );
		
		var rushStart = parseInt( jQuery( '#setupRushStart' ).val( ) ) || 11;
		var rushEnd = parseInt( jQuery( '#setupRushEnd' ).val( ) ) || 14;
		
		if( locHour >= rushStart && locHour <= rushEnd ) {
			return confirm( 'WARNING: Current location time is ' + locTime.toLocaleTimeString( ) + ', which seems likely to be in the middle of lunch rush. It is not advisable to sync all data during lunch rush.\n\nAre you ABSOLUTELY certain you wish to proceed?' );
		}
		
		return true;
	},
	
	syncAll: function( ) {
		if( confirm( 'Are you sure you want to re-sync all data?' ) && SetupController.confirmRushTime( ) ) {
			var w = jQuery.waffle.working( );
			var data = {
				bid: jQuery( '#displaybid' ).val( ),
				extras: jQuery( '.syncExtras:checked' ).map( function( ) { return jQuery(this ).prop( 'name' ); } ).get().concat( [0] ),
			};
			jQuery.ajax( {
				type: "POST",
				url: 'resetForSync',
				data: data,
				success: function( data ) {
					if( !data || data.error ) {
						return jQuery.waffle.error( data && data.error || 'There was an error processing your request' );
					}
					
					jQuery.waffle.success( 'Everything Reset for Sync' );
				}
			} ).always( function( ) {
				if( w )
					jQuery.waffle.burn( w );
			} );
		}
	},
	
	resetAll: function( ) {
		if( confirm( 'Are you sure you want to RESET all data?' ) && SetupController.confirmRushTime( ) ) {
			var w = jQuery.waffle.working( );
			var data = {
				bid: jQuery( '#displaybid' ).val( ),
				extras: jQuery( '.syncExtras:checked' ).map( function( ) { return jQuery(this ).prop( 'name' ); } ).get().concat( [0] ),
			};
			jQuery.ajax( {
				type: 'POST',
				url: 'resetAllData',
				data: data,
				success: function( data ) {
					if( !data || data.error ) {
						return jQuery.waffle.error( data && data.error || 'There was an error processing your request' );
					}
					
					jQuery.waffle.success( 'Successfully scheduled data reset' );
				}
			} ).always( function( ) {
				if( w )
					jQuery.waffle.burn( w );
			} );
		}
	},
	
	__end_SetupController: function( ){}
};

