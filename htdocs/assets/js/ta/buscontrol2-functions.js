/* 
 * These are the functions that were included in the head section of
 * /ta/buscontrol2.php & may be duplicated elsewhere
 */

function popup(url, px_width) {
	if ( !px_width ) {
		px_width = "500";
	}
	popwin = window.open(url,"Details","location=no,menubar=no,titlebar=no,scrollbars=yes,resizeable=no,height=600,width=" + px_width);
}


$(document).ready(function() {
	$('table.control_sheet_data tbody .data_line.account_name .total a').click(function(){
		popup( this.href, 600 );
		return false;
	});
});

