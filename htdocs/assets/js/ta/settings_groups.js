jQuery( function( ) {
	GroupsController.initialize( );
} );

var GroupsController = {
	/** @memberOf GroupsController */
	isInitialized: false,
	
	initialize: function( ) {
		if( GroupsController.isInitialized )
			return;
		
		GroupsController.getGroups( );
		
		jQuery( '#groupsList' ).change( GroupsController.setGroupForm );
		jQuery( '#deleteGroup' ).click( GroupsController.deleteGroup );
		
		jQuery( 'form .groupOption' ).live( {
			change: function( ) {
				jQuery( this ).closest( '.settingsContainer' ).find( '.settingsHeader' )
					.removeClass( 'ui-state-default' ).addClass( 'ui-state-highlight' );
			}
		} );
		
		jQuery( '#groupMembers_Submit' ).button( {
			text: false,
			icons: {
				primary: 'ui-icon-disk'
			}
		} ).click( GroupsController.saveGroupMembers );
		
		jQuery( '#groupBusiness_MoveButton' ).button( {
			text: false,
			icons: {
				primary: 'ui-icon-arrowthick-1-w'
			}
		} ).click( GroupsController.moveItem );
		jQuery( '#groupBusiness_Select' ).dblclick( GroupsController.moveItem );
		jQuery( '#groupBusiness_MoveButton, #groupBusiness_Select' ).data( 'from', 'groupBusiness_Select' ).data( 'to', 'groupMembers_Select' );
		
		jQuery( '#groupMembers_MoveButton' ).button( {
			text: false,
			icons: {
				primary: 'ui-icon-arrowthick-1-e'
			}
		} ).click( GroupsController.moveItem );
		jQuery( '#groupMembers_Select' ).dblclick( GroupsController.moveItem );
		jQuery( '#groupMembers_MoveButton, #groupMembers_Select' ).data( 'from', 'groupMembers_Select' ).data( 'to', 'groupBusiness_Select' );
		
		jQuery( '.groupOption' ).attr( 'disabled', true );
		
		jQuery( '#businessgroups' ).find( '.settingsForm' ).not( '.noAutoSubmit' ).submit( GroupsController.submitAjaxForm );
		
		GroupsController.resetGroupForm( );
	},
	
	getGroups: function( ) {
		var selectedGroup = jQuery( '#groupsList option:selected' ).attr( 'id' );
		
		jQuery.ajax( {
			url: 'getGroups',
			dataType: 'json',
			success: function( data ) {
				if( !data || typeof( data.groups ) == undefined || data.error ) {
					alert( data.error || 'There was an error processing your request.' );
					return false;
				}
				
				GroupsController.setGroupList( data.groups );
				
				if( typeof( selectedGroup ) != 'undefined' ) {
					jQuery( '#groupsList option#' + selectedGroup ).attr( 'selected', 'selected' );
				}
				
				jQuery( '#groupsList' ).change( );
			}
		} );
	},
	
	setGroupList: function( data, onlyData ) {
		var selector = '#groupsList';
		
		jQuery( selector ).empty( );
		jQuery( selector ).data( 'groups', data );
		
		if( onlyData == true ) return;
		
		var max = 0;
		for( var i in data ) {
			jQuery( selector ).append( '<option id="group' + data[i].group_id + '" value="' + i + '">'
										+ data[i].group_name + '</option>' );
			max = i;
		}
		jQuery( selector ).data( 'max', max );
		
		jQuery( selector ).append( '<option value="new">(add new group)</option>' );
	},
	
	resetGroupForm: function( ) {
		jQuery( '#groupDetailsForm input[type=text]' ).val( '' );
		jQuery( '#groupDetailsForm option:selected' ).removeAttr( 'selected' );
		jQuery( '#groupDetailsForm input[name=group_id]' ).val( 0 );
		jQuery( '#groupDetailsForm input.value' ).val( '' ).removeData( );
		jQuery( '#groupDetailsForm #deleteGroup' ).attr( 'disabled', 'disabled' );
	},
	
	setGroupForm: function( ) {
		var d = jQuery( this ).data( 'groups' );
		var index = jQuery( this ).val( );
		
		if( index == null ) {
			GroupsController.resetGroupForm( );
			jQuery( '.groupOption' ).attr( 'disabled', true );
			return;
		} else {
			jQuery( '.groupOption' ).attr( 'disabled', false );
			GroupsController.resetGroupForm( );
		}
		
		var t = function( n ) {
			return '#groupDetailsForm input[name=' + n + ']';
		};
		var s = function( n ) {
			return '#groupDetailsForm select[name=' + n + ']';
		};
		
		if( index == 'new' ) {
			jQuery( '#group_name' ).val( 'new group' );
			jQuery( '#group_id' ).val( 'new' );
		} else {
			for( var i in d[index] ) {
				var elem = jQuery( t( i ) );
				if( elem.length ) {
					if( typeof( d[index][i] ) == 'object' )
						elem.data( 'object', d[index][i] );
					else elem.val( d[index][i] );
				}
				
				elem = jQuery( s( i ) );
				if( elem.length ) {
					elem.find( 'option:selected' ).removeAttr( 'selected' );
					elem.find( 'option[value=' + d[index][i] + ']' ).attr( 'selected', 'selected' );
				}
			}
			
			jQuery( '#groupDetailsForm #deleteGroup' ).removeAttr( 'disabled' );
		}
		
		GroupsController.initGroupMembers( index != 'new' );
		
		jQuery( '.settingsHeader' ).removeClass( 'ui-state-highlight' ).addClass( 'ui-state-default' );
	},
	
	submitAjaxForm: function( ) {
		if( jQuery( this ).hasClass( 'noAutoSubmit' ) )
			return;
		
		var submitBtn = jQuery( this ).find( 'input[type=submit]' );
		submitBtn.attr( 'disabled', 'disabled' );
		
		var disabledFields = jQuery( this ).find( 'input:disabled' );
		disabledFields.removeAttr( 'disabled' );
		var formData = jQuery( this ).serialize( );
		disabledFields.attr( 'disabled', 'disabled' );
		
		var fn;
		if( (fn = jQuery( this ).data( 'serialize' )) )
			formData += eval( fn + '( this )' );
		
		jQuery.ajax( {
			type: "POST",
			url: jQuery( this ).attr( 'action' ),
			dataType: 'json',
			data: formData,
			success: function( r ) {
				if( !r || !r.id || r.error ) {
					alert( r && r.error || 'An error has occurred' );
				} else {
					GroupsController.getGroups( );
					alert( r.message );
					GroupsController.selectGroup( r.id );
				}
			}
		} );
		
		setTimeout( "jQuery('#" + submitBtn.attr( 'id' ) + "').attr('disabled', '')", 1000 );
	},
	
	selectGroup: function( id ) {
		var p = jQuery( '#groupsList' );
		jQuery( 'option:selected', p ).removeAttr( 'selected' );
		jQuery( 'option#group' + id, p ).attr( 'selected', 'selected' );
		p.change( );
	},
	
	deleteGroup: function( id ) {
		id = parseInt( id ) == id ? id : jQuery( '#groupDetailsForm input[name=group_id]' ).val( );
		
		if( confirm( "Are you sure you want to delete this group?" ) ) {
			jQuery.ajax( {
				url: 'deleteGroup?id=' + parseInt( id ),
				type: 'post',
				dataType: 'json',
				success: function( data ) {
					if( !data || !data.id || data.error ) {
						return alert( data && data.error || 'An error has occurred' );
					}
					GroupsController.getGroups( );
					alert( data.message );
				}
			} );
		}
		
		return false;
	},
	
	initGroupMembers: function( show ) {
		jQuery( '#groupMembersContainer' ).toggle( show );
		
		if( !show )
			return;
		
		GroupsController.getItemList( 'Members' );
		GroupsController.getItemList( 'Business' );
	},
	
	getItemList: function( name ) {
		var elemName = 'group' + name + '_Select';
		var elem = jQuery( '#' + elemName );
		
		if( !elem.is( ':visible' ) )
			return;
		
		jQuery( '#' + elemName + 'Spinner' ).show( );
		elem.empty( ).parent( ).addClass( 'far-left' );
		
		jQuery.ajax( {
			url: 'get' + name + 'List?id=' + jQuery( '#group_id' ).val( ),
			dataType: 'json',
			success: function( data ) {
				if( !data || !data.list || data.error ) {
					alert( data && data.error || 'There was an error retrieving data.' );
					return false;
				}
				
				elem.data( 'data', data.list );
				
				for( var i in data.list ) {
					jQuery( '<option id="bus' + data.list[i].businessid + '" name="' + data.list[i].businessid + '" value="' + i + '">' + data.list[i].businessname + '</option>' ).appendTo( elem );
				}
				
				GroupsController.sortList( elem );
				
				GroupsController.setupItemList( '#' + elemName );
			}
		} );
	},
	
	moveItem: function ( ) {
		var btn = jQuery( this );
		var fromSelector = btn.data( 'from' );
		var toSelector = btn.data( 'to' );
		
		var selected = jQuery( '#' + fromSelector + ' option:selected' );
		
		if( selected.length == 0 ) return;
		
		selected.each( function( idx, itm ) {
			var ji = jQuery( itm );
			var destList = jQuery( '#' + toSelector ).children( );
			var dest = null;
			
			destList.each( function( didx, ditm ) {
				if( dest ) return;
				if( GroupsController._compare( ji, ditm ) == -1 ) dest = ditm;
			} );
			
			if( dest )
				jQuery( dest ).before( ji );
			else
				jQuery( '#' + toSelector ).append( ji );
		} );
		
		GroupsController.itemListModified( );
	},
	
	itemListModified: function ( ) {
		jQuery( '#groupMembers_Submit' ).css( 'visibility', 'visible' )
			.closest( '.settingsContainer' ).find( '.settingsHeader' )
			.removeClass( 'ui-state-default' ).addClass( 'ui-state-highlight' );
	},
	
	inputTimeout: null,
	inputThis: null,

	delayedInputChange: function( self ) {
		if( GroupsController.inputTimeout )
			clearTimeout( GroupsController.inputTimeout );
		
		var parent = jQuery( self ).closest( '.itemListColumn' );
		var header = jQuery( '.itemListHeader', parent );
		
		jQuery( '.itemListFilterReset', header )
			.button( 'option', 'disabled', jQuery( self ).val( ).length == 0 );
		
		GroupsController.inputThis = this;
		GroupsController.inputTimeout = setTimeout( GroupsController.startInputChange, 150 );
	},

	startInputChange: function( ) {
		jQuery( '.spinner', jQuery( GroupsController.inputThis ).parent( ) ).show( );
		setTimeout( 'GroupsController.inputChange( GroupsController.inputThis );', 50 );
	},

	inputChange: function( self ) {
		var list = jQuery( '#' + jQuery( self ).attr( 'id' ).replace( 'Filter', 'Select' ) );
		var filterString = jQuery( self ).val( );
		
		list.children( ).removeClass( 'ui-filtered' ).show( );
		if( filterString.length > 0 ) {
			var filterTokens = filterString.toUpperCase( ).split( ' ' );
			
			for( var i in filterTokens ) {
				list.find( ':not(:iContains(' + filterTokens[i] + '))' ).addClass( 'ui-filtered' );
			}
			
			list.find( '.ui-filtered' ).hide( );
		}
		
		jQuery( '.spinner', jQuery( self ).parent( ) ).hide( );
	},
	
	_compare: function( a, b ) {
		function _compareVal( o ) {
			if( jQuery( o ).is( 'optgroup' ) )
				o = jQuery( 'option:first-child', o );
			return jQuery( o ).data( 'order' ) ? jQuery( o ).data( 'order' )
				: jQuery( o ).attr( 'label' ) ? jQuery( o ).attr( 'label' )
				: jQuery( o ).text( );
		}
		
		var compA = _compareVal( a ).toString( ).toUpperCase( );
		var compB = _compareVal( b ).toString( ).toUpperCase( );
		var numA, numB;
		var regex = /^\d+(?=\. |$)/;
		if( ( numA = compA.match( regex ) ) && ( numB = compB.match( regex ) ) ) {
			numA = parseInt( numA[0] );
			numB = parseInt( numB[0] );
			return ( numA < numB ) ? -1 : ( numA > numB ) ? 1 : 0;
		} else {
			return ( compA < compB ) ? -1 : ( compA > compB ) ? 1 : 0;
		}
	},
	
	sortList: function( list ) {
		function _sortList( selector ) {
			var elem = jQuery( selector );
			var listItems = elem.children( ).get( );
			listItems.sort( GroupsController._compare );
			jQuery.each( listItems, function( idx, itm ) {
				var ji = jQuery( itm );
				ji.addClass( 'sort' + ji.val( ).substr( 0, 1 ).toUpperCase( ) );
				elem.append( itm );
			} );
		}
		
		_sortList( list );
	},
	
	setupItemList: function( selector ) {
		var list = jQuery( selector );
		var parent = list.parent( );
		var header = jQuery( '.itemListHeader', parent );
		
		list.width( parent.width( ) ).height( parent.height( ) - header.height( ) )
			.addClass( 'ui-widget-content ui-state-default ui-corner-all' );
		
		jQuery( '.itemListFilterReset', header ).button( {
			disabled: true,
			text: false,
			icons: {
				primary: 'ui-icon-cancel'
			}
		} ).click( function( ) {
			var filter = jQuery( '.itemListFilter', header );
			filter.val( '' );
			GroupsController.inputChange( filter );
			filter.keyup( ).focusout( );
		} ).removeClass( 'ui-corner-all' ).addClass( 'ui-corner-right' );
		
		jQuery( '.itemListFilter', header ).addClass( 'ui-widget-content ui-corner-left' )
			.width( parent.width( ) - jQuery( '.itemListFilterReset', parent ).width( ) - 9 )
			.after( '<img class="spinner" style="display: none;" src="/assets/images/ajax-loader-redmond.gif" />' )
			.hover( function( ) {
				jQuery( this ).addClass( 'ui-state-hover' );
			}, function( ) {
				jQuery( this ).removeClass( 'ui-state-hover' );
			} ).bind( 'keydown, keypress', GroupsController.delayedInputChange ).focusin( function( ) {
				if( jQuery( this ).val( ) == jQuery( this ).data( 'watermark' ) )
					jQuery( this ).removeClass( 'inputWatermark' ).val( '' );
				jQuery( this ).addClass( 'ui-state-focus' );
			} ).focusout( function( ) {
				if( jQuery( this ).val( ) == '' )
					jQuery( this ).addClass( 'inputWatermark' )
						.val( jQuery( this ).data( 'watermark' ) );
				jQuery( this ).removeClass( 'ui-state-focus' );
			} ).data( 'watermark', 'filter items' ).focusout( );
		
		jQuery( '.spinner', header ).show( ).position( {
			my: 'right',
			at: 'right',
			of: header,
			offset: '-22 -3'
		} ).hide( );
		
		list.change( );
		
		parent.removeClass( 'far-left' );
		jQuery( selector + 'Spinner' ).hide( );
	},
	
	saveGroupMembers: function( ) {
		var selectedMembers = [];
		
		jQuery( '#groupMembers_Select' ).children( ).each( function( ) {
			selectedMembers.push( jQuery( this ).attr( 'name' ) );
		} );
		
		var data = { group_id: jQuery( '#group_id' ).val( ), members: selectedMembers };
		var self = jQuery( this );
		
		jQuery.ajax( {
			url: 'saveGroupMembers',
			type: 'post',
			dataType: 'json',
			data: data,
			success: function( rdata ) {
				if( !rdata || !rdata.success || rdata.error ) {
					alert( rdata && rdata.error || 'There was an error processing your request' );
					return false;
				}
				
				self.css( 'visibility', 'hidden' );
				self.closest( '.settingsContainer' ).find( '.settingsHeader' )
					.removeClass( 'ui-state-highlight' ).addClass( 'ui-state-default' );
				
				GroupsController.initGroupMembers( true );
				
				return true;
			}
		} );
		
		return false;
	},
	
	__end_GroupsController: function( ){}
};
