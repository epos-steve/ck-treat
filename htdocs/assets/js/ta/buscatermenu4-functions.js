/* 
 * These are the functions that were included in the head section of
 * /ta/buscatermenu4.php & may be duplicated elsewhere
 * 
 * /ta/buscatermenu5.php
 */

if (typeof jQuery != "undefined") {
	jQuery(document).ready(function () {
		jQuery('#manage').submit(function () {
			jQuery.post(jQuery(this).attr('action'), {businessid: jQuery('#businessid').val()}, function(data) {
				if (data == 1) {
					jQuery('#manage .info').text('POS syncronization scheduled. This may take a few minutes.');
				} else {
					jQuery('#manage .info').text('POS syncronization failed to schedule');
				}
			});

			return(false);
		});
	});
}
