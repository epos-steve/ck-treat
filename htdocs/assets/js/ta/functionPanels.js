/**
 * Created by ryan on 2/4/14.
 */
(function() {
	/* global $eeAction, $eeCid */
	"use strict";

	jQuery( function() {
		jQuery.waffle.compat();
		FunctionPanelController.initializeFunctionPanels();
	} );

	var FunctionPanelController = window.FunctionPanelController = {
		/** @memberOf FunctionPanelController */
		isInitialized: false,

		gridSize: 0,
		selectedIndex: null,
		needSave: false,

		updatingOrder: false,
		needUpdateOrder: false,
		panelList: null,
		panelItems: null,
		panelFunctions: null,
		PGc: null,
		editContainer: null,
		EDc: null,
		container: null,
		c: null,

		initializeFunctionPanels: function() {
			var content = jQuery( '#content' );
			FunctionPanelController.c = FunctionPanelController.container = content;
			FunctionPanelController.EDc = FunctionPanelController.editContainer = jQuery( '#functionPanelEditContainer' );
			FunctionPanelController.functionPanelShowInactive = jQuery( '#functionPanelShowInactive' );
			FunctionPanelController.functionPanelForm = jQuery( '#functionPanelForm' );
			FunctionPanelController.functionPanelList = jQuery( '#functionPanelList' );
			var buttonForm = FunctionPanelController.functionPanelButtonForm = jQuery( '#functionPanelButtonForm' );
			var forms = FunctionPanelController.forms = content.find( 'form' );

			FunctionPanelController.isInitialized = true;

			FunctionPanelController.functionPanelFunctionDescription = jQuery( '#functionPanelFunctionDescription' )
				.addClass( 'ui-widget-content ui-state-default ui-corner-all' );
			FunctionPanelController.functionPanelFunctionSelect = jQuery( '#function_id' ).change( function( ) {
				var $this = jQuery( this );
				var selected = $this.find( ':selected' );
				var ttcontent = selected.data( 'li' ).data( 'tooltip-content' );
				FunctionPanelController.functionPanelFunctionDescription.html( ttcontent ).toggle( !!ttcontent );
			} );

			FunctionPanelController.getFunctionPanelFunctions( false );
			FunctionPanelController.initializePanels();
			FunctionPanelController.getFunctionPanels();

			FunctionPanelController.functionPanelShowInactive.bind( 'change', function() {
				FunctionPanelController.editNone();
				FunctionPanelController.reloadPanels();
			} );

			FunctionPanelController.functionPanelForm.ajaxedit( {
				'submit': function() {
					var buttons = FunctionPanelController.panelItems.data( 'functionPanelButtons' );
					var editItemContainer = FunctionPanelController.editContainer.find( '.editItem' );
					var min = FunctionPanelController._getMinimumColForButtons( buttons );

					var cols = editItemContainer.find( '[name=cols]' ).val();
					if( parseInt( cols ) < min ) {
						jQuery.waffle.error( 'cannot remove columns occupied by buttons' );
						return false;
					}

					return true;
				},
				'aftersubmit': function() {
					FunctionPanelController.reloadPanels(true);
					//jQuery( '#functionPanelList option:selected', FunctionPanelController.c ).removeAttr( 'selected' );
					//FunctionPanelController.editNone();
					//jQuery( '#functionPanelList option:selected', FunctionPanelController.c ).removeAttr( 'selected' ).prev( 'option' ).attr( 'selected', 'selected' );
					//jQuery( '#functionPanelList' ).change( );
				},
				'cancel': function() {

					FunctionPanelController.functionPanelList.find( 'option:selected' ).removeAttr( 'selected' );
					FunctionPanelController.functionPanelForm.ajaxedit( 'fillDefault' );
					FunctionPanelController.editContainer.removeClass( 'ui-state-highlight' ).addClass( 'ui-state-default' );
					FunctionPanelController.editNone();
				}
			} );

			FunctionPanelController.functionPanelDuplicateButton = jQuery( '#duplicateFunctionPanelButton' );
			FunctionPanelController.functionPanelToggleActivationButton = jQuery( '#functionPanelToggleActivationButton' );

			jQuery( '#addFunctionPanelButton' ).click( function() {
				if( !FunctionPanelController.panelItems.aerispanel( 'hasSpace' ) )
					jQuery.waffle.error( 'No space left on panel!' );
				else FunctionPanelController.editButton();
			} );

			FunctionPanelController.functionPanelDuplicateButton
				.on( 'click', function() {
					FunctionPanelController.duplicatePanel();
				} );

			FunctionPanelController.functionPanelToggleActivationButton
				.on( 'click', function() {
					FunctionPanelController.toggleActivation();
				} );

			buttonForm.ajaxedit( {
				'cancel': function() {
					FunctionPanelController.functionPanelList.change();
				}
			} ).on( 'ajaxeditaftersubmit', function() {
				//FunctionPanelController.editContainer.find( '.editButton' ).addClass( 'ui-helper-hidden-accessible' )
				//	.end().find( '.editItem' ).removeClass( 'ui-helper-hidden-accessible' );
				//FunctionPanelController.forms.find( '.functionPanelOption' ).attr( 'disabled', false );
				FunctionPanelController.getFunctionPanel( true );
			} );
			buttonForm.addClass( 'ui-helper-hidden-accessible' );

			buttonForm.find( ':button[value="Delete"]' ).click( function() {
				var buttonId = parseInt( buttonForm.find( '[name=id]' ).val() );
				if( buttonId >= 1 ) {
					FunctionPanelController.deleteFunctionPanelButton( buttonId );
					FunctionPanelController.functionPanelList.change();
					FunctionPanelController.getFunctionPanel( true );
				}
			} );

			forms.on( 'change', '.functionPanelOption', function() {
				jQuery( this ).closest( '.functionPanelEditContainer' ).find( '.settingsHeader' ).removeClass( 'ui-state-default' ).addClass( 'ui-state-highlight' );
			} ).find( '.functionPanelOption' ).attr( 'disabled', true ).end().each( function( ) {
				var $this = jQuery( this );
				$this.attr( 'action', $eeAction( $this.attr( 'action' ) ) );
			} );

			/*jQuery( 'form .functionPanelOption' ).live( {
				change: function() {
					jQuery( this ).closest( '.functionPanelEditContainer' ).find( '.settingsHeader' ).removeClass( 'ui-state-default' ).addClass( 'ui-state-highlight' );
				}
			} );*/

			FunctionPanelController.resetFunctionPanelForm();
			try {
				FunctionPanelController.resizeScreen();
			} catch( e ) {
				//Function Panel Must not be enabled.
			}
		},

		initializePanels: function() {

			FunctionPanelController.panelItems = jQuery( '#functionPanelItems' ).aerispanel( {
				reorder: FunctionPanelController.updateItemsOrder,
				/*change: function( event, ui ) {
					//FunctionPanelController.updateItemButtons( ui && ui.index || -1 );
				},*/
				click: function() {
					FunctionPanelController.editButton( jQuery( this ).data( 'id' ) );
				},
				resizable: true,
			} );
			/*FunctionPanelController.addFunctionPanelButton = jQuery( '#addFunctionPanelButton' );
			FunctionPanelController.addFunctionPanelButton.click( function() {
				FunctionPanelController.editButton( jQuery( "option:selected", this ).val() );
			} );*/
		},

		getFunctionPanels: function( panelId ) {
			//selectNewPanel = selectNewPanel === true;
			var selectedFunctionPanel = FunctionPanelController.functionPanelList.find( ':selected' ).attr( 'id' );
			var selectNewPanel = selectedFunctionPanel === 'functionPanelNew';
			panelId = parseInt( panelId );
			if( panelId > 0 ) {
				selectedFunctionPanel = 'functionPanel' + panelId;
			}

			jQuery.ajax( {
				url: $eeAction( 'getFunctionPanels', { cid: $eeCid } ),
				dataType: 'json',
				success: function( data ) {
					try {
						if( !data || typeof ( data.functionPanels ) === "undefined" || data.error ) {
							jQuery.waffle.error( data && data.error || 'There was an error processing your request.' );
							return false;
						}
						FunctionPanelController.setFunctionPanelList( 'functionPanel', data.functionPanels );
						if( selectNewPanel && !( panelId > 0 ) ) {
							jQuery( '#functionPanelNew' ).prev( 'option' ).attr( 'selected', 'selected' );
							FunctionPanelController.functionPanelList.change( );
						} else {
							jQuery( '#' + selectedFunctionPanel ).attr( 'selected', 'selected' );
							FunctionPanelController.functionPanelList.change( );
						}
					} catch( e ) {
						jQuery.waffle.error( 'Error: ' + e );
					}
					return true;
				}
			} );
		},

		getPanelListChangeHandler: function() {
			return function() {
				try {
					//var activeButton = jQuery( '#functionPanelToggleActivationButton',
					//	FunctionPanelController.container );
					var panelId = FunctionPanelController.functionPanelList.find( ':selected' ).val();

					if( panelId != null ) {
						//FunctionPanelController.editContainer.removeClass( 'ajaxeditBusy' );
						//FunctionPanelController.editContainer.find( 'div.editItem form' ).removeClass( 'ajaxeditBusy' );
						try {
							FunctionPanelController.editPanel( panelId );
							return;
						} catch( e ) {
							jQuery.waffle.error( 'Error calling editPanel(' + panelId + '): ' + e );
						}
					/*} else {
						//activeButton.val( "Activate" ).attr( "disabled", "disabled" );
						//FunctionPanelController.editContainer.find( 'div.editItem form' ).addClass( 'ajaxeditBusy' );*/
					}
				} catch( e ) {
					jQuery.waffle.error( 'Error: ' + e );
				}

				FunctionPanelController.editNone( );
			};
		},

		setFunctionPanelList: function( name, data, onlyData ) {
			var list = FunctionPanelController.functionPanelList;
			list.unbind( 'change' );

			list.empty();
			list.removeData();
			list.data( name + 's', data );

			if( onlyData === true ) {
				list.bind( 'change', FunctionPanelController.getPanelListChangeHandler() );
				return;
			}

			var max = 0;
			var showInactive = FunctionPanelController.functionPanelShowInactive.attr( 'checked' ) === 'checked';
			var listItems = [];
			for( var i in data ) {
				if( data.hasOwnProperty( i ) ) {
					if( parseInt( data[i].active ) !== 1 && !showInactive ) {
						continue;
					}
					listItems[listItems.length] = jQuery( '<option id="functionPanel' + data[i].id + '" value="' + data[i].id + '">' + data[i].name +
						'</option>' ).data( 'index', i );
					max = i;
				}
			}
			listItems[listItems.length] = jQuery( '<option id="functionPanelNew" value="">(add new panel)</option>' );
			list.append.apply( list, listItems );
			list.data( 'max', max );
			list.data( 'name', name );

			list.bind( 'change', FunctionPanelController.getPanelListChangeHandler() );
		},

		/*__setFunctionPanelList: function( name, data, onlyData ) {
			var selector = '#functionPanelList';
			jQuery( selector ).empty().removeData();
			jQuery( selector ).unbind( 'change' ).bind( 'change', FunctionPanelController.getPanelListChangeHandler() );

			jQuery( selector ).data( name + 's', data );

			//if( onlyData == true ) return;
			var max = 0;
			for( var i in data ) {
				jQuery( '<option id="functionPanel' + data[i].id + '" value="' + data[i].id + '">' + data[i].name +
					'</option>' ).data( 'index', i ).appendTo( selector );
				max = i;
			}
			jQuery( selector ).append( '<option id="functionPanelNew" value="">(add new panel)</option>' );
			jQuery( selector ).data( 'max', max );
			jQuery( selector ).data( 'name', name );
		},*/

		editPanel: function( panelId ) {
			FunctionPanelController.panelItems.aerispanel( 'reset' );
			FunctionPanelController.panelItems.data( 'functionPanelButtons', null );
			var activeButton = FunctionPanelController.functionPanelToggleActivationButton;
			var panelData;
			if( panelId === "" ) {
				activeButton.attr( 'disabled', 'disabled' );

				panelData = {
					__isNew: true,
					id: panelId,
					rows: 4,
					cols: 4,
					active: 1
				};
			} else {

				panelData = FunctionPanelController.functionPanelList.data( 'functionPanels' ).filter( function( value ) {
					return value.id === panelId;
				} ).pop();

				activeButton.removeAttr( 'disabled' );
				var activeStatus = parseInt( panelData.active );
				if( activeStatus === 1 ) {
					activeButton.val( 'Deactivate' );
				} else {
					activeButton.val( 'Activate' );
				}

				FunctionPanelController.getFunctionPanel();
			}

			FunctionPanelController.editContainer.children().addClass( 'ui-helper-hidden-accessible' );
			//var buttons = FunctionPanelController.panelItems.data( 'functionPanelButtons' );
			var editItemContainer = FunctionPanelController.editContainer.find( '.editItem' );
			/*var min = FunctionPanelController._getMinimumColForButtons( buttons );
			//console.log(min);
			if( min > 1 ) {
				var colsInputElement = editItemContainer.find( '[name=cols]' );
				var reg = new RegExp( "^(slider)-([0-9]{1,})(-([0-9]{1,}))?$", "i" );
				var className = colsInputElement.attr( 'class' );
				var cssNames = className !== '' ? className.split( /\s+/ ) : [];
				if( cssNames.length ) {
					for( var i = 0, il = cssNames.length; i < il; i++ ) {
						var m = reg.exec( cssNames[i] );
						if( m && m.length >= 2 ) {
							//console.log( cssNames[i] );
							*//*
							 colsInputElement.removeClass( cssNames[i] );
							 colsInputElement.addClass( 'slider-' + min + '-' + m[4] );
							 *//*
							break;
						}
					}
				}

			}*/

			editItemContainer.removeClass( 'ui-helper-hidden-accessible' ).children( '.ajaxedit' ).ajaxedit( 'edit', panelData );
			FunctionPanelController.forms.find( '.functionPanelOption' ).attr( 'disabled', false );

		},


		editButton: function( buttonId ) {
			FunctionPanelController.resizeScreen();
			// new button
			var editContainer = FunctionPanelController.editContainer;
			editContainer.find( '.editButton' ).removeClass( 'ui-helper-hidden-accessible' );
			var buttonData;
			if( buttonId === undefined ) {
				var selectedFunctionPanelVal = FunctionPanelController.functionPanelList.find( ':selected' ).val();
				var emptySlots = FunctionPanelController.panelItems.find( '.aerispanel-cell:empty:visible' );
				if( emptySlots.length < 1 ) {
					throw new Error( "no empty slots" );
				}
				var slot = emptySlots.first();

				var slotData = slot.data();
				buttonData = {
					__isNew: true,
					id: '',
					pos_color: 0,
					fpanel_id: selectedFunctionPanelVal,
					display_x: slotData.x,
					display_y: slotData.y,
					width: parseInt( slot.attr( 'colspan' ) ),
					height: parseInt( slot.attr( 'rowspan' ) )
				};
			} else {

				buttonData = FunctionPanelController.panelItems.data( 'functionPanelButtons' ).filter(function( value ) {
					return parseInt( value.id ) === buttonId;
				} ).pop();
				FunctionPanelController.getFunctionPanel();
			}
			editContainer.children().addClass( 'ui-helper-hidden-accessible' );
			var editButton = editContainer.find( '.editButton' );
			editButton.removeClass( 'ui-helper-hidden-accessible' );
			editButton.children( '.ajaxedit' ).ajaxedit( 'edit', buttonData );
			/*var editOptions = editButton.find( '.ajaxedit' ).data( 'ajaxedit' ).options;
			editOptions.aftersubmit = function() {
				//console.log( this );
				//jQuery.waffle.pop( 'SAVED BUTTON EVENT FIRED' );
				jQuery( '.editButton', editContainer ).addClass( 'ui-helper-hidden-accessible' );
				jQuery( '.editItem', editContainer ).removeClass( 'ui-helper-hidden-accessible' );
				jQuery( '.functionPanelOption' ).attr( 'disabled', false );
			};*/
			FunctionPanelController.forms.find( '.functionPanelOption' ).attr( 'disabled', false );

			FunctionPanelController.functionPanelButtonForm.removeClass( 'ui-helper-hidden-accessible' );
		},

		duplicatePanel: function( ) {
			var form = FunctionPanelController.functionPanelForm;
//			form.find( '[name="id"]' ).val( 0 );
//			var namefield = form.find( '[name="name"]' );
//			namefield.val( namefield.val( ) + ' (copy)' );
			//var data = form.ajaxedit( 'serialize' );

			jQuery.ajax( {
				url: $eeAction( 'duplicateFunctionPanel', { fpid: form.find( '[name="id"]' ).val( ) } ),
				dataType: 'json',
				async: true,
				success: function( data ) {
					if( !data || typeof data.id === 'undefined' || data.error ) {
						jQuery.waffle.error( data && data.error || 'There was an error processing your request.' );
						return false;
					}

					FunctionPanelController.reloadPanels( data.id );
				}
			} );

		},

		/**
		 *
		 * @param {Boolean} [async=true]
		 */
		getFunctionPanelFunctions: function( async ) {
			async = async !== false;
			jQuery.ajax( {
				url: $eeAction( 'getFunctionPanelFunctions' ),
				dataType: 'json',
				async: async,
				success: function( data ) {
					if( !data || typeof ( data.functionPanelFunctions ) === 'undefined' || data.error ) {
						jQuery.waffle.error( data && data.error || 'There was an error processing your request.' );
						return false;
					}
					var fpfdata = FunctionPanelController.panelFunctions = data.functionPanelFunctions;
					var fselect = FunctionPanelController.functionPanelFunctionSelect;
					for( var i = 0, il = fpfdata.length; i < il; i++ ) {
						var description = fpfdata[i].description
							.replace( /(\r\n|\n\r|\r|\n)/g, '<br />' )
							.replace( /\t/g, '&nbsp;&nbsp;&nbsp;&nbsp;' );
						var option = jQuery( "<option value='" + fpfdata[i].id + "'>" + fpfdata[i].name + "</option>\n" )
							.data( 'tooltip-content', description );
						fselect.append( option );
					}

					return true;
				}
			} );
		},

		getFunctionPanel: function( refresh, async ) {
			var selector = FunctionPanelController.functionPanelList.find( ':selected' );
			var selectedFunctionPanel = selector.attr( 'id' );
			var selectedFunctionPanelVal = selector.val();
			var selectedItem = '#' + FunctionPanelController.panelItems.find( '.selected' ).attr( 'id' );
			refresh = refresh !== false;
			async = async !== false;

			jQuery.ajax( {
				url: $eeAction( 'getFunctionPanel', { fpid: selectedFunctionPanelVal } ),
				dataType: 'json',
				async: async,
				success: function( data ) {
					if( !data || typeof ( data.functionPanelButtons ) === 'undefined' || data.error ) {
						jQuery.waffle.error( data && data.error || 'There was an error processing your request.' );
						return false;
					}

					FunctionPanelController.setFunctionPanel( data );

					if( refresh ) {
						FunctionPanelController.showFunctionPanel( selectedFunctionPanel );
						FunctionPanelController.selectItem( selectedItem );
					}

					return true;
				}
			} );
		},

		clearButtons: function() {
			FunctionPanelController.panelItems.aerispanel( 'reset' );
		},

		setFunctionPanel: function( data ) {

			for( var i in data ) {
				if( data.hasOwnProperty( i ) )
					FunctionPanelController.panelItems.data( i, data[i] );
			}

			if( data.functionPanelButtons ) {
				var min = FunctionPanelController._getMinimumColForButtons( data.functionPanelButtons );
				var pos = FunctionPanelController.functionPanelList.find( ':selected' ).data( 'index' );
				var fpdata = FunctionPanelController.functionPanelList.data( "functionPanels" )[pos];
				var d = {
					items: data.functionPanelButtons,
					cols: fpdata.cols,
					rows: fpdata.rows,
					active: true,
					min: min
				};
				FunctionPanelController.panelItems.aerispanel( 'show', d );
			}
		},

		showFunctionPanel: function( functionPanel ) {
			var functionPanelData = FunctionPanelController.panelItems.data( 'functionPanelButtons' );
			if( !( functionPanel in functionPanelData ) ) {
				for( var i in functionPanelData ) {
					if( functionPanelData.hasOwnProperty( i ) ) {
						if( i === '__root__' ) {
							continue;
						}
						functionPanel = i;
						break;
					}
				}
			}

			jQuery( '#currentFunctionPanel' ).val( functionPanel );

		},

		selectItem: function( target ) {
			target = jQuery( target );
			if( target.is( '.selected' ) ) return;

			/*var functionPanelItems = jQuery( '#functionPanelItems' );

			functionPanelItems.find( '.selected' )*//*.width( ScreenController.gridSize )*//*.removeClass( 'selected' );

			functionPanelItems.find( '.changed' ).each( function() {
				var color = jQuery( this ).data( 'color' );
				jQuery( this ).attr( 'class',
					'functionPanelItems product-button-color-' + ( color > 0 ? color : 'default' ) );
			} );*/

			FunctionPanelController.panelItems.find( '.selected' ).removeClass( 'selected' ).end().find( '.changed' ).each( function( ) {
				var color = jQuery( this ).data( 'color' );
				jQuery( this ).attr( 'class',
					'functionPanelItems product-button-color-' + ( color > 0 ? color : 'default' ) );
			} );

			FunctionPanelController.selectedIndex = null;

			if( target.length ) {
				target.addClass( 'selected' )/*.width( ScreenController.gridSize - 8 )*/;
				FunctionPanelController.selectedIndex = target.data( 'index' );
			}
		},


		editNone: function() {
			FunctionPanelController.editContainer.children().addClass( 'ui-helper-hidden-accessible' );
			FunctionPanelController.editContainer.find( '.editNone' ).removeClass( 'ui-helper-hidden-accessible' );
			FunctionPanelController.clearButtons();

		},
		reloadPanels: function( panelId) {
			panelId = panelId > 0 ? panelId : 0;
			//jQuery( '#functionPanelList option:selected', FunctionPanelController.container ).removeAttr('selected');
			//FunctionPanelController.functionPanelList.removeData().empty();
			FunctionPanelController.clearButtons();
			FunctionPanelController.functionPanelForm.ajaxedit( 'fillDefault' );
			FunctionPanelController.editContainer.find( '.editItem' ).find( 'input.functionPanelOption' ).val( '' )
				.attr( 'disabled', 'disabled' ).removeClass( 'ui-state-highlight' );
				//.addClass( 'ui-state-default ajaxeditBusy' );
			FunctionPanelController.getFunctionPanels(panelId);
			//var activeButton = jQuery( '#functionPanelToggleActivationButton', FunctionPanelController.container );
			//FunctionPanelController.editContainer.children( ).addClass( 'ui-helper-hidden-accessible' );
			//jQuery( '#functionPanelEditContainer .editNone' ).removeClass( 'ui-helper-hidden-accessible' );

		},

		resizeScreen: function( ) {
			/*if( forceResize === true ) {
				 FunctionPanelController.panelGroups.aerispanel( 'resize' );
				 FunctionPanelController.panelItems.aerispanel( 'resize' );
			}*/

			var sp = jQuery( '#functionPanelProperties' );
			var spP = sp.outerWidth( true ) - sp.width();
			sp.width( 1 );
			var spX = sp.offset().left - sp.parent().offset().left;
			var parentW = sp.parent().width();
			var spW = parentW - spX - spP - 50;
			sp.width( spW );

			/*
			 sp.find( '.screenToolbar' ).each( function( ) {
			 var b = jQuery( this ).children( ).last( );
			 var bP = b.outerWidth( true ) - b.width( );
			 b.width( 1 );
			 var bX = b.offset( ).left - jQuery( this ).offset( ).left;
			 var parentW = jQuery( this ).width( );
			 var bW = parentW - bX - bP;
			 b.width( bW );
			 } );
			 */
		},


		resetFunctionPanelForm: function() {
			FunctionPanelController.functionPanelForm.find( '[type=text]' ).val( '' )
				.end().find( ':selected' ).removeAttr( 'selected' )
				.end().find( '[name=functionPanel_id]' ).val( 0 )
				.end().find( 'input.value' ).val( '' ).removeData();
			FunctionPanelController.resetFunctionPanelButtonForm();
		},

		resetFunctionPanelButtonForm: function() {
			var fpbf = FunctionPanelController.functionPanelButtonForm;
			fpbf.find( '[type=text],[name=id]' ).val( '' ).removeData()
				.end().find( ':selected' ).removeAttr( 'selected' )
				.end().find( '[name=fpanel_id]' ).val( 0 );
		},

		/*setFunctionPanelForm: function() {
			var name = jQuery( this ).data( 'name' );
			var d = jQuery( this ).data( name + 's' );
			var index = jQuery( this ).val();

			jQuery( '.functionPanelOption' ).attr( 'disabled', false );
			FunctionPanelController.resetFunctionPanelForm();

			var t = function( n ) {
				return '#functionPanelForm input[name=' + n + ']';
			};
			var s = function( n ) {
				return '#functionPanelForm select[name=' + n + ']';
			};

			for( var i in d[index] ) {
				var elem = jQuery( t( 'functionPanel_' + i ) );
				if( elem.length ) {
					if( typeof ( d[index][i] ) == 'object' ) {
						elem.data( 'object', d[index][i] );
					} else {
						elem.val( d[index][i] );
					}
				}

				elem = jQuery( s( 'functionPanel_' + i ) );
				if( elem.length ) {
					elem.find( 'option:selected' ).removeAttr( 'selected' );
					elem.find( 'option[value=' + d[index][i] + ']' ).attr( 'selected', 'selected' );
				}
			}

			if( name == 'nonFunctionPanel' ) {
				jQuery( '#deleteFunctionPanel' ).attr( 'disabled', true );
			}

			jQuery( '.settingsHeader' ).removeClass( 'ui-state-highlight' ).addClass( 'ui-state-default' );
		},*/

		/*submitAjaxForm: function() {
			if( jQuery( this ).hasClass( 'noAutoSubmit' ) ) return;

			var submitBtn = jQuery( this ).find( 'input[type=submit]' );
			submitBtn.attr( 'disabled', 'disabled' );

			var disabledFields = jQuery( this ).find( 'input:disabled' );
			disabledFields.removeAttr( 'disabled' );
			var formData = jQuery( this ).serialize();
			disabledFields.attr( 'disabled', 'disabled' );

			var fn;
			if( ( fn = jQuery( this ).data( 'serialize' ) ) ) formData += eval( fn + '( this )' );

			jQuery.ajax( {
				type: "POST",
				url: $eeAction( jQuery( this ).attr( 'action' ) ),
				dataType: 'json',
				data: formData,
				success: function( r ) {
					if( !r || !r.id || r.error ) {
						jQuery.waffle.error( r && r.error || 'An error has occurred' );
					} else {
						FunctionPanelController.reloadPanels();
						//FunctionPanelController.getFunctionPanels( );
						jQuery.waffle.pop( r.message );
						setTimeout( FunctionPanelController.selectFunctionPanel( r.id ), 1000 );
						FunctionPanelController.selectFunctionPanel( r.id );
					}
				}
			} );

			setTimeout( "jQuery('#" + submitBtn.attr( 'id' ) + "').attr('disabled', '')", 1000 );
		},*/

		/*selectFunctionPanel: function( id ) {
			FunctionPanelController.functionPanelList.find( ':selected' ).removeAttr( 'selected' )
				.end().find( '[value=' + id + ']' ).attr( 'selected', 'selected' )
				.end().change();
		},*/

		/*deleteFunctionPanel: function( id, cid ) {
			id = parseInt( id ) === id ? id : jQuery( '#functionPanelForm' ).find( 'input[name=functionPanel_id]' ).val();
			cid = parseInt( cid ) === cid ? cid : jQuery( '#functionPanelcid' ).val();

			jQuery.ajax( {
				url: $eeAction( 'deleteFunctionPanel', { fpid: parseInt( id ), cid: parseInt( cid ) } ),
				type: 'post',
				dataType: 'json',
				success: function( data ) {
					if( !data || !data.id || data.error ) {
						jQuery.waffle.error( data && data.error || 'An error has occurred' );
						return;
					}
					FunctionPanelController.getFunctionPanels();
					jQuery.waffle.pop( data.message );
				}
			} );
			return false;
		},*/

		deleteFunctionPanelButton: function( id ) {
			id = parseInt( id ) === id ? id : parseInt( FunctionPanelController.functionPanelButtonForm.find( '[name=id]' ).val() );

			jQuery.ajax( {
				url: $eeAction( 'deleteFunctionPanelButton', { id: parseInt( id ), cid: $eeCid } ),
				type: 'post',
				dataType: 'json',
				success: function( data ) {
					if( !data || !data.id || data.error ) {
						jQuery.waffle.error( data && data.error || 'An error has occurred' );
						return;
					}
					FunctionPanelController.getFunctionPanels();
					jQuery.waffle.pop( data.message );
				}
			} );
			return false;
		},

		toggleActivation: function() {
			var fpid = FunctionPanelController.functionPanelForm.find( ':hidden[name=id]' ).val();
			if( fpid === "" ) {
				return;
			}
			/*var panelData = FunctionPanelController.functionPanelList.data( 'functionPanels' ).filter(function( value ) {
				return value.id == fpid;
			} ).pop();

			var activeButton = jQuery( '#functionPanelToggleActivationButton' );*/

			jQuery.ajax( {
				url: $eeAction( 'toggleFunctionPanelActive', { fpid: parseInt( fpid ), cid: $eeCid } ),
				type: 'post',
				dataType: 'json',
				success: function( data ) {
					if( !data || !data.id || data.error ) {
						jQuery.waffle.error( data && data.error || 'An error has occurred' );
						return;
					}
					//activeButton.val( ( activeButton.val() == 'Deactivate' ) ? "Activate" : "Deactivate" );
					//FunctionPanelController.getFunctionPanels( );
					jQuery.waffle.pop( data.message );
					FunctionPanelController.reloadPanels();
					//var functionPanel =
					FunctionPanelController.functionPanelList.find( ':selected' ).removeAttr( 'selected' );
					FunctionPanelController.editNone();
				}
			} );


		},

		/*updateItemButtons: function( index ) {
			//jQuery( '#functionPanelEditItem', #functionPanelRemoveItem' ).toggle( index > -1 );

		},*/

		updateItemsOrder: function() {
			return FunctionPanelController._updateFunctionPanelOrder();
		},

		_updateFunctionPanelOrder: function() {
			if( FunctionPanelController.updatingOrder ) {
				FunctionPanelController.needUpdateOrder = true;
				return;
			}

			FunctionPanelController.updatingOrder = true;
			FunctionPanelController.needUpdateOrder = false;

			var items = FunctionPanelController.panelItems.data( "functionPanelButtons" );

			var buttons = [];

			for( var i in items ) {
				if( items.hasOwnProperty( i ) )
					buttons[buttons.length] = {
						id: items[i].id,
						fpanel_id: items[i].fpanel_id,
						display_x: items[i].display_x,
						display_y: items[i].display_y,
						width: items[i].width,
						height: items[i].height,
						name: items[i].name,
						pos_color: items[i].pos_color,
						function_id: items[i].function_id
					};
			}

			var functionPanelId = FunctionPanelController.functionPanelList.find( 'option:selected' ).val();

			return jQuery.ajax( {
				type: 'POST',
				url: $eeAction( 'updateFunctionPanelOrder' ),
				dataType: 'json',
				data: {
					cid: $eeCid,
					functionPanelId: functionPanelId,
					buttons: buttons
				},
				success: function( adata ) {
					if( !adata || !adata.message || adata.error ) {
						jQuery.waffle.error( adata && adata.error || 'There was an error processing your request' );
						return;
					}
					FunctionPanelController.updatingOrder = false;

					if( FunctionPanelController.needUpdateOrder ) {
						FunctionPanelController._updateFunctionPanelOrder( );
					}
				}
			} );


		},

		_getMinimumColForButtons: function( buttons ) {
			var min = 0;

			if( buttons != null ) {
				jQuery.each( buttons, function() {
					var i = this, val = parseInt( i.display_x ) + ( parseInt( i.width ) );
					if( min === undefined || min < val ) {
						min = val;
					}
				} );
			}
			return min;
		},
	};
})();
