$(document).ready(function() {	
	var	man = $('#manufacturer'),
		cat = $('#category'),
		man_wrap = $('#manufacturer_wrap'),
		cat_wrap = $('#category_wrap'),
		man_id = $('#man_id'),
		cat_id = $('#cat_id'),
		man_name = $('#man_name'),
		cat_name = $('#cat_name'),
		man_form = $('#manufacturer_form'),
		cat_form = $('#category_form'),
		delete_man = $('#delete_man'),
		delete_cat = $('#delete_cat'),
		mode = $('#mode'),
		clear_form = $('.clear_form');
	
	/* show manufacturer form */
	function show_manufacturer(){
		man.show();
		cat.hide();
		
		man_wrap.show();
		cat_wrap.hide();
		
		clear_forms();
	}
	
	/* show category form */
	function show_category(){
		cat.show();
		man.hide();
		
		cat_wrap.show();
		man_wrap.hide();
		
		clear_forms();
	}
	
	/* calls show_manufacturer or show_category functions */
	mode.change(function(){
		switch(this.value){
			case 'manufacturer':
				show_manufacturer();
				break;
			case 'category':
				show_category();
				break;
			default:
				break;
		}
	});
	
	/* calls clear_forms function */
	clear_form.click(function(){
		clear_forms();
	});
	
	/* populate manufacturer form with manufacturer id and name */
	man.change(function(){
		$('#loading').show();
	
		man_id.val(this.value);		
		man_name.val( $("#manufacturer option[value='"+this.value+"']").text() );		
		cat_id.val(-1);
		$('#res_message').html('');		
			
		$.post('promosettings/ajaxCheckDeleteMan',
		{'man_id' : man_id.val()},
		function(data) {			
			if(data.allow_delete){
				delete_man.show();
			} else {
				delete_man.hide();
			}
		})
		.complete(function(){$('#loading').hide();});
	});
	
	/* populate category form with category id and name */
	cat.change(function(){
		$('#loading').show();
	
		cat_id.val(this.value);
		cat_name.val( $("#category option[value='"+this.value+"']").text() );
		man_id.val(-1);
		$('#res_message').html('');
		
		$.post('promosettings/ajaxCheckDeleteCat',
		{'cat_id' : cat_id.val()},
		function(data) {			
			if(data.allow_delete){
				delete_cat.show();
			} else {
				delete_cat.hide();
			}
		})
		.complete(function(){$('#loading').hide();});
	});
	
	/* save manufacturer thru ajax */
	man_form.submit(function(event){
		event.preventDefault();
		
		$('#loading').show();
			
		$.post('promosettings/ajaxCreateManufacturer',
		man_form.serialize(),
		function(data) {			
			if(data.passed){				
				load_select('m');
				$('#res_message').html('Manufacturer <span style="color:green;">successfully</span> saved.');
			} else {
				if(data.error == 'req_missing'){
					$('#res_message').html('Missing <span style="color:red;">required field</span>: Manufacturer Name.');
				}
			}
		})
		.complete(function(){$('#loading').hide();});
	});
	
	/* save category thru ajax */
	cat_form.submit(function(event){
		event.preventDefault();
		
		$('#loading').show();
			
		$.post('promosettings/ajaxCreateCategory',
		cat_form.serialize(),
		function(data) {			
			if(data.passed){
				load_select('c');
				$('#res_message').html('Category <span style="color:green;">successfully</span> saved.');
			} else {
				if(data.error == 'req_missing'){
					$('#res_message').html('Missing <span style="color:red;">required field</span>: Category Name.');
				}
			}
		})
		.complete(function(){$('#loading').hide();});
	});
	
	/* load entire list of manufacturer/category depending on value passed in */
	function load_select(type){
		var url;
		
		if(type == 'm'){
			url = 'ajaxAllManufacturers';
		} else {
			url = 'ajaxAllCategories';
		}
	
		$.post('promosettings/'+url,		
		function(data) {			
			if(data != 'failed'){
				if(type == 'm'){
					man.html(data);
				} else {
					cat.html(data);
				}
			} else {
				$('#res_message').html('<span style="color:red;">No results</span> found.');
			}
		});
	}
	
	/* delete manufacturer row with ajax call */
   delete_man.click(function(){		
		var delete_it = confirm_delete('m');
		
		if(delete_it){
			$('#loading').show();
		
			$.post('promosettings/ajaxDeleteMan',
			{'man_id' : man_id.val()},
			function(data) {			
				if(data.pass){				
					$("#manufacturer option[value='"+data.man_id+"']").remove();
					
					$('#res_message').html('Manufacturer <span style="color:green;">successfully</span> deleted.');
				} else {
					$('#res_message').html('Manufacturer <span style="color:red;">failed</span> to delete.');
				}
			})
			.complete(function(){$('#loading').hide(); clear_forms(false); });
		}
   });
   
   /* delete category row with ajax call */
   delete_cat.click(function(){		
		var delete_it = confirm_delete('c');
		
		if(delete_it){
			$('#loading').show();
		
			$.post('promosettings/ajaxDeleteCat',
			{'cat_id' : cat_id.val()},
			function(data) {			
				if(data.pass){				
					$("#category option[value='"+data.cat_id+"']").remove();
					
					$('#res_message').html('Category <span style="color:green;">successfully</span> deleted.');
				} else {
					$('#res_message').html('Category <span style="color:red;">failed</span> to delete.');
				}
			})
			.complete(function(){$('#loading').hide(); clear_forms(false); });
		}
   });
	
	/* show confirmation of deletion popup */
   function confirm_delete(type){
		var message;
		
		if(type == 'm'){
			message = 'Manufacturer';
		} else {
			message = 'Category';
		}
   
		var r = confirm("Really delete the "+message+"?");
		if (r){
			return true;
		} else {
			return false;
		}
	}
	
	/* clear message and reset manufacturer and category forms; pass false to not clear message */
	function clear_forms(clear_msg){
		clear_msg = typeof clear_msg !== 'undefined' ? clear_msg : true;
		
		if(clear_msg){
			$('#res_message').html('');
		}
		
		man_form[0].reset();
		cat_form[0].reset();
		
		man[0].selectedIndex = 0;
		cat[0].selectedIndex = 0;
		
		man_id.val(-1);
		cat_id.val(-1);
		delete_man.hide();
		delete_cat.hide();
	}
});