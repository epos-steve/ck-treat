function hide_show_units(ordertype){	
	var img = $('#img'+ordertype);
	var ex = img.attr('src').split('/');
	var src = ex[ex.length-1];
	
	if(src == "exp.gif"){
		img.attr('src','/ta/exp2.gif');
		img.attr('alt','Show');
		var rows = $('.ot' + ordertype);
		
		rows.each(function(index){
			$(this).hide();
		});
	} else {
		img.attr('src','/ta/exp.gif');
		img.attr('alt','Hide');
		var rows = $('.ot' + ordertype);
		
		rows.each(function(index){
			$(this).show();
		});
	}
}