function import_menu( ){
	jQuery('<div />')
		.load('/ta/import_menu.php?bid=' + jQuery( 'input[name=businessid]' ).val( ) + '&return_url='+encodeURIComponent(document.location.href))
		.dialog({
			modal: true,
			title: 'Import Menu'
		});
}

function generateOpenUPC( ) {
	if( confirm( "This will generate an Open Item UPC, replacing any current UPC.\nOpen Items have no price and will pop up a dialog for price entry on Cashier stations." ) ) {
		jQuery.ajax( {
			url: 'generateUPC?prefix=292929&bid=' + jQuery( 'input[name=businessid]' ).val( ),
			dataType: 'json',
			success: function( data ) {
				if( !data || typeof( data.upc ) == undefined || data.error ) {
					alert( data.error || 'There was an error processing your request.' );
					return false;
				}
				
				jQuery( 'input[name=upc]' ).val( data.upc );
			}
		} );
	}
}

/*jQuery( function( ) {
	jQuery( '#generateOpenUPC' ).click( generateOpenUPC );
} );*/

$(document).ready(function() {
	$( '#generateOpenUPC' ).click( generateOpenUPC );
	
	/* before submitting form, enable disabled form fields so they come thru POST vars */
	$('#pos_form').submit(function(event){
		event.preventDefault();
		$('input').removeAttr('disabled');
		$('select').removeAttr('disabled');
		this.submit();
	});
});
