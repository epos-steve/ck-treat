(function() {
  var $, MenuUpdates, Uploader, hideChanges, menuSelection, showChanges;

  $ = jQuery;

  showChanges = function() {
    $('#uploader-container').fadeOut(1000, function() {
      return $('#upload-menu-item-review').fadeIn(1000);
    });
    return $('#importer-changes-display').show();
  };

  hideChanges = function() {
    $('#upload-menu-item-review').hide();
    $('#uploader-container').slideDown(1000);
    $('#importer-changes-display tbody').empty();
    return $('progress').hide();
  };

  window.importUploadError = function(error) {
    $('#import-uploader-error-display').show();
    $('#import-uploader-error-display .error-message').text(error);
    return $('.upload-update-progress').hide();
  };

  menuSelection = function() {
    var menu_selection;
    menu_selection = $('.menu-selection').val();
    $('.menu-selection-dependent').hide();
    if (menu_selection !== 'company') {
      if (menu_selection === 'district') {
        return $('.districts').show();
      } else if (menu_selection === 'kiosk') {
        return $('.kiosks').show();
      }
    }
  };

  MenuUpdates = (function() {

    MenuUpdates.prototype.changes = [];

    MenuUpdates.prototype.skippedNum = 0;

    MenuUpdates.prototype.limit = 'company';

    MenuUpdates.prototype.district = null;

    MenuUpdates.prototype.kiosk = null;

    function MenuUpdates(changes) {
      this.changes = changes;
      this.limit = $('.menu-selection').val();
      this.district = $('.districts').val();
      this.kiosk = $('.kiosks').val();
    }

    MenuUpdates.prototype.skipped = function(val) {
      return this.skippedNum = val;
    };

    MenuUpdates.prototype.convertCSVToMenuItems = function(raw) {
      var _this = this;
      this.doingConvert = true;
      return $.post('/ta/kioskutilities/syncMenu', {
        'convertToCSV': 1,
        'limit': this.limit,
        'district': this.district,
        'kiosk': this.kiosk,
        'rawCSV': raw
      }, function(data) {
        if (data != null) {
          if (data.changes != null) {
            _this.changes = data.changes;
          }
          if (data.skipped != null) {
            _this.skippedNum = data.skipped;
          }
        }
        return _this.display();
      }, 'json');
    };

    MenuUpdates.prototype.display = function() {
      var cost, html, item, price, row, _i, _len, _ref,
        _this = this;
      $('.upload-update-progress').hide();
      $('progress').hide();
      if (this.changes.length < 1) {
        $('#import-uploader-error-display').show();
        $('#import-uploader-error-display .error-message').text("No changes to process and skipped " + this.skippedNum + " items.");
        return;
      }
      showChanges();
      if (this.skippedNum > 0) {
        $('.skipped-message').show();
        $('.skipped-num').text(this.skippedNum);
      } else {
        $('.skipped-num').text('0');
        $('.skipped-message').hide();
      }
      $('.ignore-changes').click(function(event) {
        return hideChanges();
      });
      $('.remove-row').live('click', function(event) {
        return $(event.currentTarget).parentsUntil('tr').parent().remove();
      });
      $('.save-changes').click(function(event) {
        event.preventDefault();
        return $('.menu-item-data').each(function(index, item) {
          var data;
          $(item).find('.remove-row').hide();
          $(item).find('.update-progress').show();
          data = $(item).data();
          return $.post('/ta/kioskutilities/syncMenu', {
            'changes': 1,
            'upc': $(item).attr('data-upc'),
            'name': $(item).data('name'),
            'cost': $(item).data('cost'),
            'price': $(item).data('price'),
            'limit': _this.limit,
            'district': _this.district,
            'kiosk': _this.kiosk
          }, function(data) {
            if ((data.error != null) && data.error === false) {
              return $(item).fadeOut(1000, function() {
                if ($('#importer-changes-display tbody tr').filter(':visible').length < 1) {
                  return hideChanges();
                }
              });
            } else {
              return $(item).replaceWith("<tr>\n    <td colspan=\"5\">\n        Error! Did not save: " + item.message + "\n    </td>\n</tr>");
            }
          }, 'json');
        });
      });
      html = [];
      _ref = this.changes;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        item = _ref[_i];
        cost = item.cost.toFixed(2);
        price = item.price.toFixed(2);
        row = [];
        row[row.length] = '<tr';
        row[row.length] = ' data-name="' + item.name.replace('"', '&quot;') + '"';
        row[row.length] = ' data-upc="' + item.upc.replace('"', '&quot;') + '"';
        row[row.length] = ' data-cost="' + item.cost + '"';
        row[row.length] = ' data-price="' + item.price + '"';
        row[row.length] = ' class="menu-item-data">';
        row[row.length] = '<td style="width: 20px; text-align: center;">';
        row[row.length] = '<img src="/assets/images/delete.gif" style="width: 16px; height: 16px;" class="remove-row" />';
        row[row.length] = '<img src="/assets/images/ajax-loader-redmond.gif" style="display: none;" class="update-progress" />';
        row[row.length] = '</td>';
        row[row.length] = "<td>" + item.name + "</td>";
        row[row.length] = "<td>" + item.upc + "</td>";
        row[row.length] = "<td style='text-align: right;'>$" + cost + "</td>";
        row[row.length] = "<td style='text-align: right;'>$" + price + "</td>";
        row[row.length] = '</tr>';
        html[html.length] = row.join("\n");
      }
      return $('#importer-changes-display tbody').html(html.join("\n"));
    };

    return MenuUpdates;

  })();

  Uploader = (function() {

    Uploader.prototype.html5Support = false;

    Uploader.prototype.html4Support = true;

    function Uploader() {
      this.checkSupport();
    }

    Uploader.prototype.checkSupport = function() {
      if (window.FileReader) {
        this.html5Support = true;
        return this.html4Support = false;
      } else {
        this.html5Support = false;
        return this.html4Support = true;
      }
    };

    Uploader.prototype.hideUnsupportedElements = function() {
      if (this.html5Support) {
        $('.html4').hide();
        $('progress').hide();
        return $('#importer-file').css('visibility', 'hidden').css('position', 'absolute').css('top', -50).css('left', -50);
      } else {
        return $('.html5').hide();
      }
    };

    Uploader.prototype.handleUpload = function() {
      if (this.html5Support) {
        return this.html5Upload();
      } else {
        return this.html4Upload();
      }
    };

    Uploader.prototype.html4Upload = function() {
      $('.import-file-button').click(function(event) {
        $('#import-uploader-error-display').hide();
        $('#import-uploader-error-display .error-message').text('');
        return $('.upload-update-progress').show();
      });
      return window.importUpload = function(json, skipped) {
        var updates;
        if (skipped == null) {
          skipped = 0;
        }
        $('#importer-file').val('');
        $('.upload-update-progress').hide();
        updates = new MenuUpdates(json);
        updates.skipped(skipped);
        return updates.display();
      };
    };

    Uploader.prototype.html5Upload = function() {
      $('.importer-choose-file').live('click', function() {
        $('#import-uploader-error-display').hide();
        $('#import-uploader-error-display .error-message').text('');
        return $('#importer-file').val('').click();
      });
      return $('#importer-file').live('change', function(event) {
        var reader;
        $('progress').show();
        $('progress').val(0).text('');
        if (!event.currentTarget.files || event.currentTarget.files.length < 1) {
          return;
        }
        reader = new FileReader();
        reader.onload = function(event) {
          var updates;
          $('progress').val(100).text('Done!');
          updates = new MenuUpdates([]);
          updates.convertCSVToMenuItems(event.target.result);
        };
        reader.onprogress = function(event) {
          var percent;
          if (event.lengthComputable) {
            percent = Math.round((event.loaded / event.total) * 100);
            if (percent <= 100) {
              return $('progress').val(percent).text(percent + '%');
            }
          }
        };
        return reader.readAsText(event.currentTarget.files[0]);
      });
    };

    return Uploader;

  })();

  $(function() {
    var uploader;
    menuSelection();
    $('.menu-selection').live('change', function() {
      return menuSelection();
    });
    $('#exporter-file').click(function(event) {
      return $('#import-uploader-error-display').hide();
    });
    uploader = new Uploader();
    uploader.hideUnsupportedElements();
    return uploader.handleUpload();
  });

}).call(this);
