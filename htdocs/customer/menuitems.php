<head>
<SCRIPT LANGUAGE="JavaScript">
<!-- Web Site:  http://dynamicdrive.com -->

<!-- This script and many more are available free online at -->
<!-- The JavaScript Source!! http://javascript.internet.com -->

<!-- Begin
function disableForm(theform) {
if (document.all || document.getElementById) {
for (i = 0; i < theform.length; i++) {
var tempobj = theform.elements[i];
if (tempobj.type.toLowerCase() == "submit" || tempobj.type.toLowerCase() == "reset")
tempobj.disabled = true;
}

}
else {
alert("The form has been submitted.  But, since you're not using IE 4+ or NS 6, the submit button was not disabled on form submission.");
return false;
   }
}
//  End -->
</script>
</head>

<?php

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+3);
        if ($diff > 0 && $diff < '0.01'){$diff="0.01";}
        elseif($diff < 0 && $diff > '-0.01'){$diff="-0.01";}
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      if ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='32')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='08' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      $day=$day-31;  
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";} 
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

$username="mburris_user";
$password="bigdog";
$database="mburris_businesstrack";
$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");
$bbdayname=date("l");
$bbmonth=date("F");
$bbday=date("j");

$user=$_COOKIE["usercook"];
$pass=$_COOKIE["passcook"];
$reserveid=$_GET['reserveid'];
$accountid=$_GET['accountid'];
$groupid=$_GET['groupid'];
$showqty=$_GET['qty'];
$showname=$_GET['showname'];

mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM customer WHERE username = '$user' AND password = '$pass'";
$result = mysql_query($query);
$num=mysql_numrows($result);
mysql_close();

if ($num != 1 || $user == "" || $pass == "")
{
    echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
    $user=mysql_result($result,0,"username");
    $customerid=mysql_result($result,0,"customerid");
    $accountid=mysql_result($result,0,"accountid");
    $businessid=mysql_result($result,0,"businessid");

    echo "<body background=backdrop.jpg>"; 
    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%><tr><td colspan=4><img src=logo.jpg><p></td></tr>";

    mysql_connect($dbhost,$username,$password);
    @mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM company WHERE companyid = '1'";
    $result = mysql_query($query);
    mysql_close();

    $companyname=mysql_result($result,0,"companyname");

    mysql_connect($dbhost,$username,$password);
    @mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM business WHERE businessid = '$businessid'";
    $result = mysql_query($query);
    mysql_close();

    $taxrate=mysql_result($result,0,"tax");
    $phone=mysql_result($result,0,"phone");

    mysql_connect($dbhost,$username,$password);
    @mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM accounts WHERE accountid = '$accountid'";
    $result = mysql_query($query);
    mysql_close();

    $accountname=mysql_result($result,0,"name");
    $accountnum=mysql_result($result,0,"accountnum");
    $taxid=mysql_result($result,0,"taxid");

    $showname = str_replace("~", ' ', $showname);
    //if ($reserveid!=""&&$showqty!=""&&$showname!=""){$showcart="<a style=$style title='View Your Order' href=caterdetails.php?reserveid=$reserveid&accountid=$accountid#invoice><font color=red><b>$showqty $showname(s) added. View Your Order</b><img src=chefHat.gif width=29 height=27 border=0></font></a>";}
    if ($reserveid!=""){$showcart="<a style=$style href=caterdetails.php?reserveid=$reserveid&accountid=$accountid#invoice><font color=blue><b>Check Out </b><img src=chefHat.gif width=29 height=27 border=0></font></a>";}
    else {$showcart="";}

    echo "<tr bgcolor=black><td height=1 colspan=4></td></tr>";
    echo "<tr bgcolor=#CCCCFF><td width=10%><img src=weblogo.jpg height=75 width=80></td><td style='filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr=#FFFFFF, startColorstr=#CCCCFF, gradientType=1);'><font size=4><b>$companyname</b></font><br><b>Account: $accountname<br>Account #: $accountnum</td><td colspan=2 align=right valign=top style='filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr=#CCCCFF, startColorstr=#FFFFFF, gradientType=1);'><b>$bbdayname, $bbmonth $bbday, $year</b><br><font size=2>[<a style=$style href=logout.php><b><font color=blue>Log out</font></b></a>]</font><p>$showcart</td></tr>";
    echo "<tr bgcolor=black><td height=1 colspan=4></td></tr>";
    echo "</table></center><p>";

    if ($reserveid!=""){

       mysql_connect($dbhost,$username,$password);
       @mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM invoicedetail WHERE reserveid = '$reserveid' ORDER BY itemid DESC";
       $result = mysql_query($query);
       $num=mysql_numrows($result);
       mysql_close();

       echo "<center><table width=90% cellspacing=0 cellpadding=0><tr height=1><td colspan=6 bgcolor=black></td></tr><tr><td bgcolor=#E8E7E7 colspan=6><font size=2>Order Details:</font></td></tr>";
       $subtotal=0;
       $num--;
       $showcolor="white";

       while ($num>=0){
          $qty=mysql_result($result,$num,"qty");
          $item=mysql_result($result,$num,"item");
          $price=mysql_result($result,$num,"price");
          $detail=mysql_result($result,$num,"detail");

          if ($num==0&&$showqty!=""&&$showname!=""){$showcolor="yellow";}

          $itemtotal=money($qty*$price);
          $price=money($price);

          echo "<tr><td bgcolor=$showcolor valign=top width=5%><font size=2>$qty</td><td bgcolor=$showcolor valign=top width=20%><font size=2>$item</td><td bgcolor=$showcolor align=right valign=top width=10%><font size=2>$$price</td><td bgcolor=$showcolor align=right valign=top width=10%><font size=2>$$itemtotal</td><td bgcolor=$showcolor width=4%></td><td bgcolor=$showcolor><font size=2>$detail</font></td></td></tr>";

          if ($showcolor=="white"){$showcolor="#E8E7E7";}
          else {$showcolor="white";}
          $subtotal=$subtotal+($qty*$price);
          $num--;
       }
       $subtotal=money($subtotal);
       echo "<tr><td bgcolor=$showcolor colspan=2></td><td bgcolor=$showcolor align=right><font size=2>Subtotal:</font></td><td align=right bgcolor=$showcolor><font size=2>$$subtotal</font></td><td bgcolor=$showcolor colspan=2></td><tr>";
       echo "<tr height=1><td colspan=6 bgcolor=black></td></tr></table></center><p>";
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    mysql_connect($dbhost,$username,$password);
    @mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM menu_groups WHERE menu_group_id = '$groupid'";
    $result = mysql_query($query);
    mysql_close();

    $groupid=mysql_result($result,0,"menu_group_id");
    $groupname=mysql_result($result,0,"groupname");
    $description=mysql_result($result,0,"description");
    $image=mysql_result($result,0,"image");
    $img_height=mysql_result($result,0,"img_height");
    $img_width=mysql_result($result,0,"img_width");

    echo "<center><table width=90% style='filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr=#C0C0C0, startColorstr=#FFFFFF, gradientType=0);' cellspacing=0 cellpadding=0 border=0>";
    echo "<tr height=1 bgcolor=black><td colspan=4></td></tr>";
    echo "<tr><td width=5 rowspan=2 height=100></td><td width=100 height=100 rowspan=2><img src='pictures/$image' height=$img_height width=$img_width border=0></td><td><font size=4><b><i>$groupname</i></b></font></td><td align=right><a style=$style href=menu.php?reserveid=$reserveid&account=$accountid><font color=blue><b>Main Menu</b></font></a></td></tr>";
    echo "<tr><td colspan=3>$description</td></tr>";
    echo "<tr height=1 bgcolor=black><td colspan=4></td></tr></table></center><p>";

    mysql_connect($dbhost,$username,$password);
    @mysql_select_db($database) or die( "Unable to select database");
    $query2 = "SELECT * FROM menu_items WHERE businessid = '$businessid' AND groupid = '$groupid' AND deleted = '0' ORDER BY item_name DESC";
    $result2 = mysql_query($query2);
    $num2=mysql_numrows($result2);
    mysql_close();

    $num2--;

    if ($num2==-1){
       echo "<center><table width=90% border=0 bgcolor=white cellspacing=0 cellpadding=0><tr height=1 bgcolor=#CCCCCC><td colspan=5></td></tr><tr><td bgcolor=#CCCCCC width=1></td><td><b><i>Sorry, No Items Available at this Time</i></b></td><td align=right width=25% colspan=2>";
       echo "</td><td bgcolor=#CCCCCC width=1></td></tr>";
       echo "<tr><td bgcolor=#CCCCCC width=1></td><td colspan=3><i>If you would like items to be available in this section, please call us at $phone to let us know.  We appreciate your business and any input you have.</i></td><td bgcolor=#CCCCCC width=1></td></tr>";   
       echo "<tr height=1 bgcolor=#CCCCCC><td colspan=5></td></tr>"; 
       echo "</table></center><p>";
    }

    while ($num2>=0){
       $menu_item_id=mysql_result($result2,$num2,"menu_item_id");
       $item_name=mysql_result($result2,$num2,"item_name");
       $unit=mysql_result($result2,$num2,"unit");
       $parent=mysql_result($result2,$num2,"parent");
       $price=mysql_result($result2,$num2,"price");
       $description=mysql_result($result2,$num2,"description");
       $order_min=mysql_result($result2,$num2,"order_min");
       $price=money($price);

       $description=str_replace("\n","<br>",$description);

       if ($order_min>0){$showmin="<font size=2>(minimum $order_min)</font>";}
       else{$showmin="";}

       echo "<center><table width=90% border=0 style='filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr=#E8E7E7, startColorstr=#FFFFFF, gradientType=1);' cellspacing=0 cellpadding=0><tr height=1 bgcolor=#CCCCCC><td colspan=5></td></tr><tr height=1 valign=top><td bgcolor=#CCCCCC width=1></td><td><b><font color=blue>$item_name</font><br>$$price/$unit </b>$showmin<p>$description</td><td rowspan=2 width=30% colspan=2>";
       if ($reserveid!=""){

          $showname = str_replace(" ", "~", $item_name);
          echo "<form action=additem.php method=post onSubmit='return disableForm(this);'><input type=hidden name=groupid value=$groupid><input type=hidden name=item_name value=$showname><input type=hidden name=reserveid value=$reserveid><input type=hidden name=menu_item_id value=$menu_item_id>";

          if($parent!=0){$menu_item_id=$parent;}

          mysql_connect($dbhost,$username,$password);
          @mysql_select_db($database) or die( "Unable to select database");
          $query3 = "SELECT * FROM optiongroup WHERE menu_item_id = '$menu_item_id' ORDER BY orderid DESC, description DESC";
          $result3 = mysql_query($query3);
          $num3=mysql_numrows($result3);
          mysql_close();

          $num3--;
          echo "<input type=hidden name=optionnum value=$num3>";
          while ($num3>=0){
             $optiongroupid=mysql_result($result3,$num3,"optiongroupid");
             $optiongrouptype=mysql_result($result3,$num3,"type");
             $description3=mysql_result($result3,$num3,"description");

             echo "<center><table width=99% cellspacing=0 cellpadding=0 bgcolor=#FFFFFF><tr height=1><td colspan=3></td></tr><tr height=1 bgcolor=#CCCCCC><td colspan=3></td></tr><tr><td width=1 bgcolor=#CCCCCC></td><td style='filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr=#FFFFFF, startColorstr=#E8E7E7, gradientType=0);'><u>$description3</u></td><td bgcolor=#CCCCCC width=1></td></tr>";

             mysql_connect($dbhost,$username,$password);
             @mysql_select_db($database) or die( "Unable to select database");
             $query = "SELECT * FROM options WHERE optiongroupid = '$optiongroupid' ORDER BY orderid DESC, description DESC";
             $result = mysql_query($query);
             $num=mysql_numrows($result);
             mysql_close();

             $num--;

             if ($optiongrouptype==0){
                $firstnum=$num;
                while ($num>=0){
                   $optionid=mysql_result($result,$num,"optionid");
                   $description2=mysql_result($result,$num,"description");
                   $price=mysql_result($result,$num,"price");
                   if ($price!=0){
                      $price=money($price);
                      $showprice=" <i> - Add $$price</i>";
                   }
                   else {$showprice="";}

                   if ($firstnum==$num){$showsel="CHECKED";}
                   else{$showsel="";}

                   echo "<tr><td width=1 bgcolor=#CCCCCC></td><td><INPUT TYPE=radio name=$num3 value=$optionid $showsel> $description2 $showprice</td><td width=1 bgcolor=#CCCCCC></td></tr>";

                   $num--;
                }
             }
             elseif ($optiongrouptype==1){
                while ($num>=0){
                   $optionid=mysql_result($result,$num,"optionid");
                   $description2=mysql_result($result,$num,"description");
                   $price=mysql_result($result,$num,"price");
                   if ($price!=0){
                      $price=money($price);
                      $showprice=" <i> - Add $$price</i>";
                   }
                   else {$showprice="";}

                   $optionname="$num3-$num";
                   echo "<tr><td width=1 bgcolor=#CCCCCC></td><td><INPUT TYPE=checkbox value=$optionid name=$optionname> $description2 $showprice</td><td width=1 bgcolor=#CCCCCC></td></tr>";

                   $num--;
                }
             }
             elseif ($optiongrouptype==2){
                while ($num>=0){
                   $optionid=mysql_result($result,$num,"optionid");
                   $description2=mysql_result($result,$num,"description");
                   $price=mysql_result($result,$num,"price");
                   if ($price!=0){
                      $price=money($price);
                      $showprice=" <i> - Add $$price</i>";
                   }
                   else {$showprice="";}

                   $optionname="$num3-$num";
                   echo "<tr><td width=1 bgcolor=#CCCCCC></td><td><INPUT TYPE=text name=$optionid size=3> $description2 $showprice</td><td width=1 bgcolor=#CCCCCC></td></tr>";

                   $num--;
                }
             }

             echo "<tr height=1 bgcolor=#CCCCCC><td colspan=3></td></tr><tr height=1><td></td></tr></table></center>";
             $num3--;
          }

          echo "<center><table width=99% cellspacing=0 cellpadding=0><tr height=1><td colspan=3></td></tr><tr height=1 bgcolor=#CCCCCC><td colspan=3></td></tr><tr><td bgcolor=#CCCCCC width=1></td><td style='filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr=#FFFFFF, startColorstr=#E8E7E7, gradientType=0);'><u>Special Instructions</u><br><textarea name=special rows=3 cols=30></textarea></td><td bgcolor=#CCCCCC width=1></td></tr><tr height=1 bgcolor=#CCCCCC><td colspan=3></td></tr><tr height=1><td colspan=3></td></tr></table></center>";
          echo "<center><table width=99% cellspacing=0 cellpadding=0><tr height=1><td colspan=3></td></tr><tr height=1 bgcolor=#CCCCCC><td colspan=3></td></tr><tr><td bgcolor=#CCCCCC width=1></td><td align=right style='filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr=#E8E7E7, startColorstr=#FFFFFF, gradientType=0);'>Qty: <input type=text name=qty size=4> <input type=submit value='Add'></td><td bgcolor=#CCCCCC width=1></form></td></tr><tr height=1 bgcolor=#CCCCCC><td colspan=3></td></tr><tr height=1><td colspan=3></td></tr></table></center>";
       }

       else {echo "<center><table width=99% cellspacing=0 cellpadding=0><tr height=1><td colspan=3></td></tr><tr height=1 bgcolor=#CCCCCC><td colspan=3></td></tr><tr><td bgcolor=#CCCCCC width=1></td><td style='filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr=#FFFFFF, startColorstr=#E8E7E7, gradientType=0);'><center><a style=$style href=createorder.php#cater title='Schedule a new catering or add to an existing one.'><font color=blue>Start/Continue Your Order</font></a></center></td><td colspan=3></td></tr><tr height=1 bgcolor=#CCCCCC><td colspan=3></td></tr><tr height=1><td colspan=3></td></tr></table></center>";}

       echo "</td><td bgcolor=#CCCCCC width=1></td></tr>";
       echo "<tr valign=top><td bgcolor=#CCCCCC width=1></td><td colspan=1 valign=top></form></td><td bgcolor=#CCCCCC width=1></td></tr>";   
       echo "<tr height=1 bgcolor=#CCCCCC><td colspan=5></td></tr>"; 
       echo "</table></center><p>";
       $num2--;
    }

    echo "<center><a style=$style href=menu.php?reserveid=$reserveid&account=$accountid><font color=blue><b>Main Menu</b></font></a></center>";
    echo "</body>";
}
?>