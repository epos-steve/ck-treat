<head>

<SCRIPT LANGUAGE="JavaScript">
<!-- Web Site:  http://dynamicdrive.com -->

<!-- This script and many more are available free online at -->
<!-- The JavaScript Source!! http://javascript.internet.com -->

<!-- Begin
function disableForm(theform) {
if (document.all || document.getElementById) {
for (i = 0; i < theform.length; i++) {
var tempobj = theform.elements[i];
if (tempobj.type.toLowerCase() == "submit" || tempobj.type.toLowerCase() == "reset")
tempobj.disabled = true;
}

}
else {
alert("The form has been submitted.  But, since you're not using IE 4+ or NS 6, the submit button was not disabled on form submission.");
return false;
   }
}
//  End -->
</script>

<SCRIPT LANGUAGE=javascript><!--
function voidconfirm(){return confirm('Are you sure you want to cancel this order?');}
// --></SCRIPT>

<SCRIPT LANGUAGE=javascript><!--
function delconfirm(){return confirm('Are you sure you want to delete this item?');}
// --></SCRIPT>

<SCRIPT LANGUAGE=javascript><!--
function placeorder(){return confirm('We appreciate your business.  You will receive a confirmation email once we have reviewed your order.');}
// --></SCRIPT>

<SCRIPT LANGUAGE=javascript><!--
function savechanges(){return confirm('Are you sure you want to save this catering?  If it has already been submitted, it will return to a status of Pending.');}
// --></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" SRC="CalendarPopup.js"></SCRIPT>

<!-- This prints out the default stylehseets used by the DIV style calendar.
     Only needed if you are using the DIV style popup -->
<SCRIPT LANGUAGE="JavaScript">document.write(getCalendarStyles());</SCRIPT>

<!-- These styles are here only as an example of how you can over-ride the default
     styles that are included in the script itself. -->
<STYLE>
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation
			{
			background-color:#6677DD;
			text-align:center;
			vertical-align:center;
			text-decoration:none;
			color:#FFFFFF;
			font-weight:bold;
			}
	.TESTcpDayColumnHeader,
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation,
	.TESTcpCurrentMonthDate,
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDate,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDate,
	.TESTcpCurrentDateDisabled,
	.TESTcpTodayText,
	.TESTcpTodayTextDisabled,
	.TESTcpText
			{
			font-family:arial;
			font-size:8pt;
			}
	TD.TESTcpDayColumnHeader
			{
			text-align:right;
			border:solid thin #6677DD;
			border-width:0 0 1 0;
			}
	.TESTcpCurrentMonthDate,
	.TESTcpOtherMonthDate,
	.TESTcpCurrentDate
			{
			text-align:right;
			text-decoration:none;
			}
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDateDisabled
			{
			color:#D0D0D0;
			text-align:right;
			text-decoration:line-through;
			}
	.TESTcpCurrentMonthDate
			{
			color:#6677DD;
			font-weight:bold;
			}
	.TESTcpCurrentDate
			{
			color: #FFFFFF;
			font-weight:bold;
			}
	.TESTcpOtherMonthDate
			{
			color:#808080;
			}
	TD.TESTcpCurrentDate
			{
			color:#FFFFFF;
			background-color: #6677DD;
			border-width:1;
			border:solid thin #000000;
			}
	TD.TESTcpCurrentDateDisabled
			{
			border-width:1;
			border:solid thin #FFAAAA;
			}
	TD.TESTcpTodayText,
	TD.TESTcpTodayTextDisabled
			{
			border:solid thin #6677DD;
			border-width:1 0 0 0;
			}
	A.TESTcpTodayText,
	SPAN.TESTcpTodayTextDisabled
			{
			height:20px;
			}
	A.TESTcpTodayText
			{
			color:#6677DD;
			font-weight:bold;
			}
	SPAN.TESTcpTodayTextDisabled
			{
			color:#D0D0D0;
			}
	.TESTcpBorder
			{
			border:solid thin #6677DD;
			}
</STYLE>

</head>

<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once(DOC_ROOT.'/bootstrap.php');

function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+4);
        $diff=round($diff, 2);
        if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
        elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
        $diff = substr($diff, 0, $dot+3);
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      elseif ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      elseif ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '1')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='31')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;
$leap = date("L");

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   
   elseif ($leap==1&&$month=='03')
   {
      $day=$day+29;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      $day=$day-31;  
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";} 
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

/*$username="mburris_user";
$password="bigdog";
$database="mburris_businesstrack";*/
$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");
$bbdayname=date("l");
$bbmonth=date("F");
$bbday=date("j");
$startclose="$year-$month-01";
$month++;
if ($month<10){$month="0$month";}
if ($month==13){$month="01";$year++;}
$endclose="$year-$month-31";

$user = \EE\Controller\Base::getPostGetSessionOrCookieVariable( array('username', 'usercook') );
$pass = \EE\Controller\Base::getPostGetSessionOrCookieVariable( array('password', 'passcook') );
$reserveid = \EE\Controller\Base::getGetVariable('reserveid');

/*mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");*/
$query = "SELECT * FROM customer WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=Treat_DB_ProxyOld::mysql_numrows($result);
//mysql_close();

/*mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");*/
$query2 = "SELECT * FROM reserve WHERE reserveid = '$reserveid'";
$result2 = Treat_DB_ProxyOld::query($query2);
$num2=Treat_DB_ProxyOld::mysql_numrows($result2);
//mysql_close();

if ($num != 1 || $user == "" || $pass == "")
{
    echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
    $user=Treat_DB_ProxyOld::mysql_result($result,0,"username");
    $customerid=Treat_DB_ProxyOld::mysql_result($result,0,"customerid");
    $accountid=Treat_DB_ProxyOld::mysql_result($result,0,"accountid");
    $businessid=Treat_DB_ProxyOld::mysql_result($result,0,"businessid");

    $accountid2=Treat_DB_ProxyOld::mysql_result($result2,0,"accountid");

  if ($accountid!=$accountid2){echo "<center><h2>FAILED</h2></center>";}
  else{

    echo "<body background=backdrop.jpg>";
    echo "<div style=border:50px solid red;padding:10px;>";
    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%><tr><td colspan=4><img src=logo.jpg width=205 height=43><p></td></tr>";

    /*mysql_connect($dbhost,$username,$password);
    @mysql_select_db($database) or die( "Unable to select database");*/
    $query = "SELECT * FROM company WHERE companyid = '1'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $companyname=Treat_DB_ProxyOld::mysql_result($result,0,"companyname");

    /*mysql_connect($dbhost,$username,$password);
    @mysql_select_db($database) or die( "Unable to select database");*/
    $query = "SELECT * FROM business WHERE businessid = '$businessid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $taxrate=Treat_DB_ProxyOld::mysql_result($result,0,"tax");
    $caterdays=Treat_DB_ProxyOld::mysql_result($result,0,"caterdays");
    $caternum=Treat_DB_ProxyOld::mysql_result($result,0,"caternum");
    $srvchrg=Treat_DB_ProxyOld::mysql_result($result,0,"srvchrg");
    $srvchrgpcnt=Treat_DB_ProxyOld::mysql_result($result,0,"srvchrgpcnt");
    $weekend=Treat_DB_ProxyOld::mysql_result($result,0,"weekend");
    $cater_min=Treat_DB_ProxyOld::mysql_result($result,0,"cater_min");

    if ($weekend==0){$showweekend="cal18.setDisabledWeekDays(0,6)";}
    else{$showweekend="";}

    /*mysql_connect($dbhost,$username,$password);
    @mysql_select_db($database) or die( "Unable to select database");*/
    $query = "SELECT * FROM accounts WHERE accountid = '$accountid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $accountname=Treat_DB_ProxyOld::mysql_result($result,0,"name");
    $accountnum=Treat_DB_ProxyOld::mysql_result($result,0,"accountnum");
    $taxid=Treat_DB_ProxyOld::mysql_result($result,0,"taxid");

    /*mysql_connect($dbhost,$username,$password);
    @mysql_select_db($database) or die( "Unable to select database");*/
    $query = "SELECT * FROM reserve WHERE reserveid = '$reserveid'";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    $date=Treat_DB_ProxyOld::mysql_result($result,0,"date");
    $room=Treat_DB_ProxyOld::mysql_result($result,0,"roomid");
    $start_hour=Treat_DB_ProxyOld::mysql_result($result,0,"start_hour");
    $end_hour=Treat_DB_ProxyOld::mysql_result($result,0,"end_hour");
    $status=Treat_DB_ProxyOld::mysql_result($result,0,"status");
    $comment=Treat_DB_ProxyOld::mysql_result($result,0,"comment");
    $roomcomment=Treat_DB_ProxyOld::mysql_result($result,0,"roomcomment");
    $peoplenum=Treat_DB_ProxyOld::mysql_result($result,0,"peoplenum");
    $service=Treat_DB_ProxyOld::mysql_result($result,0,"service");
    $costcenter=Treat_DB_ProxyOld::mysql_result($result,0,"costcenter");
    $weekday=dayofweek($date);

/////CHECK ORDER TOTAL
    /*mysql_connect($dbhost,$username,$password);
    @mysql_select_db($database) or die( "Unable to select database");*/
    $query = "SELECT SUM(qty*price) AS subtotal FROM invoicedetail WHERE reserveid = '$reserveid'";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    $order_total=Treat_DB_ProxyOld::mysql_result($result,0,"subtotal");
/////CHECK QTY'S
    $order_min_met=0;

    /*mysql_connect($dbhost,$username,$password);
    @mysql_select_db($database) or die( "Unable to select database");*/
    $query = "SELECT menu_item_id FROM invoicedetail WHERE reserveid = '$reserveid'";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    $num--;
    while($num>=0){
       $menu_item_id=Treat_DB_ProxyOld::mysql_result($result,$num,"menu_item_id");

       /*mysql_connect($dbhost,$username,$password);
       @mysql_select_db($database) or die( "Unable to select database");*/
       $query5 = "SELECT order_min FROM menu_items WHERE menu_item_id = '$menu_item_id'";
       $result5 = Treat_DB_ProxyOld::query($query5);
       //mysql_close();

       $order_min=Treat_DB_ProxyOld::mysql_result($result5,0,"order_min");

       /*mysql_connect($dbhost,$username,$password);
       @mysql_select_db($database) or die( "Unable to select database");*/
       $query5 = "SELECT SUM(qty) AS totalqty FROM invoicedetail WHERE reserveid = '$reserveid' AND menu_item_id = '$menu_item_id'";
       $result5 = Treat_DB_ProxyOld::query($query5);
       //mysql_close();

       $totalqty=Treat_DB_ProxyOld::mysql_result($result5,0,"totalqty");

       if ($totalqty<$order_min){$order_min_met=1;}

       $num--;
    }
/////////////////
    $lastday=$today;
    for ($counter=0;$counter<$caterdays;$counter++){$lastday=nextday($lastday);}

    /*mysql_connect($dbhost,$username,$password);
    @mysql_select_db($database) or die( "Unable to select database");*/
    $query = "SELECT status FROM invoice WHERE reserveid = '$reserveid'";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    if ($num!=0){$curstatus=Treat_DB_ProxyOld::mysql_result($result,0,"status");}
    else {$curstatus=0;}

    if ($date<$lastday||($curstatus!=1&&$num==1)){$noedit=1;}
    else {$noedit=0;}

    echo "<tr bgcolor=black><td height=1 colspan=4></td></tr>";
    echo "<tr bgcolor=#CCCCFF><td width=10%><img src=weblogo.jpg height=75 width=80></td><td style='filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr=#FFFFFF, startColorstr=#CCCCFF, gradientType=1);'><font size=4><b>$companyname</b></font><br><b>Account: $accountname<br>Account #: $accountnum</td><td colspan=2 align=right valign=top style='filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr=#CCCCFF, startColorstr=#FFFFFF, gradientType=1);'><b>$bbdayname, $bbmonth $bbday, $year</b><br><font size=2>[<a style=$style href=logout.php><b><font color=blue>Log out</font></b></a>]</font><p>$showcart</td></tr>";
    echo "<tr bgcolor=black><td height=1 colspan=4></td></tr>";
    echo "<tr><td colspan=2><a href=catertrack.php><font color=blue>Order History</font></a> :: <a href=createorder.php><font color=blue>Schedule a Catering</font></a><p></td></tr>";
    echo "</table></center>";

    echo "<center><table width=90% cellspacing=0 cellpadding=0>";

    echo "<p><center><table width=90% bgcolor=#E8E7E7 cellspacing=0 cellpadding=0>";
    echo "<tr><td width=1% height=1%><a name='detail'></a><img src=dgrayur.gif height=20 width=20></td><td colspan=2></td><td width=1% height=1%><img src=dgrayul.gif height=20 width=20></td></tr>";
    
    if ($status==3){echo "<tr><td></td><td width=15% valign=top><img src=details.gif height=36 width=122></td><td align=right valign=top><img src=submitted.jpg height=59 width=75></td><td></td></tr>";}   
    if ($status==2){echo "<tr><td></td><td width=15% valign=top><img src=details.gif height=36 width=122></td><td align=right valign=top><img src=confirmed.jpg height=59 width=75></td><td></td></tr>";}   
    if ($status==1){echo "<tr><td></td><td width=15% valign=top><img src=details.gif height=36 width=122></td><td align=right valign=top><img src=pending.jpg height=59 width=75></td><td></td></tr>";} 

    if ($status==3){echo "<tr><td></td><td colspan=2><font color=blue><b><i>*This Order has been submitted.  Please wait for confirmation.  If you make any changes, it will return to a 'Pending' status.</i></b><p></td><td></td></tr>";}   
    if ($status==2){echo "<tr><td></td><td colspan=2><font color=blue><b><i>*This Order has already been confirmed.  If you make any changes it will return to a pending status.<br> Use the Return button to go back to the previous page.</i></b><p></td><td></td></tr>";}   
    if ($status==1){echo "<tr><td></td><td colspan=2><font color=blue><b><i>*This Order is still 'Pending'.  Place your order when ready. Use the Add Items Button at the bottom of the screen to add items to your order.</i></b><p></td><td></td></tr>";}

    /*mysql_connect($dbhost,$username,$password);
    @mysql_select_db($database) or die( "Unable to select database");*/
    $query = "SELECT * FROM caterclose WHERE businessid = '$businessid' AND date >= '$startclose' AND date <= '$endclose'";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();
    
    $num--;
    $closedmessage="";
    while ($num>=0){
       $dateclose=Treat_DB_ProxyOld::mysql_result($result,$num,"date");
       $closedmessage="$closedmessage cal18.addDisabledDates('$dateclose');";
       $num--;
    }

    echo "<tr><td></td><td align=right width=10%><FORM ACTION=updatereserve.php method=post onSubmit='return disableForm(this);'><INPUT TYPE=hidden name=reserveid value=$reserveid>Date: </td><td> <SCRIPT LANGUAGE='JavaScript' ID='js18'> var now = new Date(); var cal18 = new CalendarPopup('testdiv1');cal18.addDisabledDates(null,formatDate(now,'yyyy-MM-dd')); $closedmessage $showweekend; cal18.setCssPrefix('TEST');</SCRIPT><A HREF='#detail' onClick=cal18.select(document.forms[0].date,'anchor18','yyyy-MM-dd'); return false; TITLE=cal18.select(document.forms[0].date,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor18' ID='anchor18'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A> <INPUT TYPE=text NAME='date' VALUE=$date SIZE=8> $weekday</td><td></td></tr>"; 
    echo "<tr><td></td><td align=right>Reference: </td><td><INPUT TYPE=text name=comment value='$comment' size=30></td><td></td></tr>";
    echo "<tr><td></td><td align=right># of People: </td><td><INPUT TYPE=text name=peoplenum value=$peoplenum size=8></td><td></td></tr>";
    
    /*mysql_connect($dbhost,$username,$password);
    @mysql_select_db($database) or die( "Unable to select database");*/
    $query = "SELECT * FROM rooms WHERE businessid = '$businessid' AND deleted ='0' ORDER BY roomname DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();
   
    echo "<tr><td></td><td align=right>Location: </td><td><select name=room><option value=0>N/A</option>";
    $num--;
    while ($num>=0){
       $roomname=Treat_DB_ProxyOld::mysql_result($result,$num,"roomname");
       $roomid=Treat_DB_ProxyOld::mysql_result($result,$num,"roomid");
       if ($room==$roomid){$sel="SELECTED";}
       else {$sel="";}
       echo "<option value=$roomid $sel>$roomname</option>";
       $num--;
    } 
    echo "</select></td><td></td></tr>"; 

    echo "<tr><td></td><td></td><td><i>*If Location is N/A, Please specify. Include any other details below:<br><textarea name=roomcomment rows=3 cols=60>$roomcomment</textarea></td><td></td></tr>";

    if (strlen($start_hour)==3){
          $showstarthour=substr($start_hour,0,1);
          $showstartmin=substr($start_hour,1,3);
          $showstartam2="SELECTED";
    }
    elseif (strlen($start_hour)==1){
          $showstarthour="00";
          $showstartmin="0$start_hour";
          $showstartam2="SELECTED";
    }
    elseif (strlen($start_hour)==2){
          $showstarthour="00";
          $showstartmin=$start_hour;
          $showstartam2="SELECTED";
    }
    else {
          $showstarthour=substr($start_hour,0,2);
          $showstartmin=substr($start_hour,2,4);
          if ($showstarthour>12){$showstarthour=$showstarthour-12;$showstartam="SELECTED";}
          elseif ($showstarthour==12){$showstartam="SELECTED";}
          else {$showstartam2="SELECTED";}
    }

    if (strlen($end_hour)==3){
          $showendhour=substr($end_hour,0,1);
          $showendmin=substr($end_hour,1,3);
          $showendam2="SELECTED";
    }
    elseif (strlen($end_hour)==1){
          $showendhour="00";
          $showendmin="0$end_hour";
          $showendam2="SELECTED";
    }
    elseif (strlen($end_hour)==2){
          $showendhour="00";
          $showendmin=$end_hour;
          $showendam2="SELECTED";
    }
    else {
          $showendhour=substr($end_hour,0,2);
          $showendmin=substr($end_hour,2,4);
          if ($showendhour>12){$showendhour=$showendhour-12;$showendam="SELECTED";}
          elseif ($showendhour==12){$showendam="SELECTED";}
          else {$showendam2="SELECTED";}
    }

    if ($showstarthour==1){$c1="SELECTED";}
    elseif ($showstarthour==2){$c2="SELECTED";}
    elseif ($showstarthour==3){$c3="SELECTED";}
    elseif ($showstarthour==4){$c4="SELECTED";}
    elseif ($showstarthour==5){$c5="SELECTED";}
    elseif ($showstarthour==6){$c6="SELECTED";}
    elseif ($showstarthour==7){$c7="SELECTED";}
    elseif ($showstarthour==8){$c8="SELECTED";}
    elseif ($showstarthour==9){$c9="SELECTED";}
    elseif ($showstarthour==10){$c10="SELECTED";}
    elseif ($showstarthour==11){$c11="SELECTED";}
    elseif ($showstarthour==12||$showstarthour==0){$c12="SELECTED";}

    if ($showendhour==1){$c13="SELECTED";}
    elseif ($showendhour==2){$c14="SELECTED";}
    elseif ($showendhour==3){$c15="SELECTED";}
    elseif ($showendhour==4){$c16="SELECTED";}
    elseif ($showendhour==5){$c17="SELECTED";}
    elseif ($showendhour==6){$c18="SELECTED";}
    elseif ($showendhour==7){$c19="SELECTED";}
    elseif ($showendhour==8){$c20="SELECTED";}
    elseif ($showendhour==9){$c21="SELECTED";}
    elseif ($showendhour==10){$c22="SELECTED";}
    elseif ($showendhour==11){$c23="SELECTED";}
    elseif ($showendhour==12 || $showendhour==0){$c24="SELECTED";}

    if ($showstartmin=="00" || $showstartmin=="0"){$c25="SELECTED";}
    elseif ($showstartmin=="05" || $showstartmin=="5"){$c26="SELECTED";}
    elseif ($showstartmin=="10"){$c27="SELECTED";}
    elseif ($showstartmin==15){$c28="SELECTED";}
    elseif ($showstartmin==20){$c29="SELECTED";}
    elseif ($showstartmin==25){$c30="SELECTED";}
    elseif ($showstartmin==30){$c31="SELECTED";}
    elseif ($showstartmin==35){$c32="SELECTED";}
    elseif ($showstartmin==40){$c33="SELECTED";}
    elseif ($showstartmin==45){$c34="SELECTED";}
    elseif ($showstartmin==50){$c35="SELECTED";}
    elseif ($showstartmin==55){$c36="SELECTED";}

    if ($showendmin=="00" || $showendmin=="0"){$c37="SELECTED";}
    elseif ($showendmin=="05" || $showendmin=="5"){$c38="SELECTED";}
    elseif ($showendmin==10){$c39="SELECTED";}
    elseif ($showendmin==15){$c40="SELECTED";}
    elseif ($showendmin==20){$c41="SELECTED";}
    elseif ($showendmin==25){$c42="SELECTED";}
    elseif ($showendmin==30){$c43="SELECTED";}
    elseif ($showendmin==35){$c44="SELECTED";}
    elseif ($showendmin==40){$c45="SELECTED";}
    elseif ($showendmin==45){$c46="SELECTED";}
    elseif ($showendmin==50){$c47="SELECTED";}
    elseif ($showendmin==55){$c48="SELECTED";}
 
    echo "<tr><td></td><td align=right>Start Time:</td><td><select NAME=start_hour><OPTION VALUE='01' $c1>1</OPTION><OPTION VALUE='02' $c2>2</OPTION><OPTION VALUE='03' $c3>3</OPTION><OPTION VALUE='04' $c4>4</OPTION><OPTION VALUE='05' $c5>5</OPTION><OPTION VALUE='06' $c6>6</OPTION><OPTION VALUE='07' $c7>7</OPTION><OPTION VALUE='08' $c8>8</OPTION><OPTION VALUE='09' $c9>9</OPTION><OPTION VALUE='10' $c10>10</OPTION><OPTION VALUE='11' $c11>11</OPTION><OPTION VALUE='12' $c12>12</OPTION></SELECT><b>:</b><select NAME=start_min><OPTION VALUE='00' $c25>00</OPTION><OPTION VALUE='05' $c26>05</OPTION><OPTION VALUE='10' $c27>10</OPTION><OPTION VALUE='15' $c28>15</OPTION><OPTION VALUE='20' $c29>20</OPTION><OPTION VALUE='25' $c30>25</OPTION><OPTION VALUE='30' $c31>30</OPTION><OPTION VALUE='35' $c32>35</OPTION><OPTION VALUE='40' $c33>40</OPTION><OPTION VALUE='45' $c34>45</OPTION><OPTION VALUE='50' $c35>50</OPTION><OPTION VALUE='55' $c36>55</OPTION></SELECT> <select NAME=start_am><OPTION VALUE='AM' $showam2>AM</OPTION><OPTION VALUE='PM' $showstartam>PM</OPTION></SELECT></td><td></td></tr>";
    echo "<tr><td></td><td align=right>End Time:</td><td><select NAME=end_hour><OPTION VALUE='01' $c13>1</OPTION><OPTION VALUE='02' $c14>2</OPTION><OPTION VALUE='03' $c15>3</OPTION><OPTION VALUE='04' $c16>4</OPTION><OPTION VALUE='05' $c17>5</OPTION><OPTION VALUE='06' $c18>6</OPTION><OPTION VALUE='07' $c19>7</OPTION><OPTION VALUE='08' $c20>8</OPTION><OPTION VALUE='09' $c21>9</OPTION><OPTION VALUE='10' $c22>10</OPTION><OPTION VALUE='11' $c23>11</OPTION><OPTION VALUE='12' $c24>12</OPTION></SELECT><b>:</b><select NAME=end_min><OPTION VALUE='00' $c37>00</OPTION><OPTION VALUE='05' $c38>05</OPTION><OPTION VALUE='10' $c39>10</OPTION><OPTION VALUE='15' $c40>15</OPTION><OPTION VALUE='20' $c41>20</OPTION><OPTION VALUE='25' $c42>25</OPTION><OPTION VALUE='30' $c43>30</OPTION><OPTION VALUE='35' $c44>35</OPTION><OPTION VALUE='40' $c45>40</OPTION><OPTION VALUE='45' $c46>45</OPTION><OPTION VALUE='50' $c47>50</OPTION><OPTION VALUE='55' $c48>55</OPTION></SELECT> <select NAME=end_am><OPTION VALUE='AM' $showendam2>AM</OPTION><OPTION VALUE='PM' $showendam>PM</OPTION></SELECT></td><td></td></tr>";
    echo "<tr><td></td><td align=right>Cost Center: </td><td><INPUT TYPE=text name=costcenter value='$costcenter' size=18></td><td></td></tr>";

    if ($noedit!=1){echo "<tr><td></td><td></td><td><br><p><table><tr><td><INPUT TYPE=submit VALUE='Save Changes'></td><td></form></td>";}
    else {echo "<tr><td></td><td></td><td><br><p><table><tr><td></td><td></form></td>";}
    if ($status!=3 && $status !=2 && $order_total >= $cater_min && $order_min_met == 0){echo "<td><form action=submit.php method=post onclick='return placeorder()' onSubmit='return disableForm(this);'><input type=hidden name=reserveid value=$reserveid><input type=submit value='Place Your Order >>'></td><td></form></td>";}
    echo "<td><form action=createorder.php method=post onSubmit='return disableForm(this);'><input type=submit value='Return'></td><td></form></td>";
    if ($status==1){echo "<td><form action=voidorder.php method=post><input type=hidden name=reserveid value='$reserveid'><input type=submit value='Cancel Order' onclick='return voidconfirm()' onSubmit='return disableForm(this);'></td><td></form></td>";}
    echo "</tr></table><p></td><td></td></tr>";

    echo "<tr><td></td><td colspan=2><center><table border=0 bgcolor=white width=90% cellpadding=0 cellspacing=0>";
    echo "<tr><td height=1% width=1%><a name='invoice'></a><img src=whiteul.gif height=20 width=20></td><td colspan=6></td><td height=1% width=1% bgcolor=#E8E7E7><img src=whiteur.gif height=20 width=22></td></tr>";
    echo "<tr><td colspan=8><center><img src=menu.gif height=40 wdith=224><p><br></center></td></tr>";
    echo "<tr><td></td><td><h5><u>QTY</u></h5></td><td><h5><u>ITEM DESCRIPTION</u></h5></td><td></td><td align=right><h5><u>UNIT PRICE</u></h5></td><td align=right><h5><u>TOTAL</u></h5></td>";
    if ($noedit=='1'){echo "<td></td><td></td></tr>";}
    else {echo "<td align=right><h5><u>REMOVE</u></h5></td><td width=1></td></tr>";}
    echo "<tr><td></td><td colspan=6 bgcolor=#E8E7E7 height=1></td><td width=1%></td></tr>";

       /*mysql_connect($dbhost,$username,$password);
       @mysql_select_db($database) or die( "Unable to select database");*/
       $query = "SELECT * FROM invoicedetail WHERE reserveid = '$reserveid'";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();

       $num--;
       $total2=0;
       $subtotal=0;
       $taxtotal=0;
       $order_min_met=0;

       while ($num>=0){
          $itemid=Treat_DB_ProxyOld::mysql_result($result,$num,"itemid");
          $menu_item_id=Treat_DB_ProxyOld::mysql_result($result,$num,"menu_item_id");
          $qty=Treat_DB_ProxyOld::mysql_result($result,$num,"qty");
          $item=Treat_DB_ProxyOld::mysql_result($result,$num,"item");
          $price=Treat_DB_ProxyOld::mysql_result($result,$num,"price");
          $taxed=Treat_DB_ProxyOld::mysql_result($result,$num,"taxed");
          $detail=Treat_DB_ProxyOld::mysql_result($result,$num,"detail");
          $details=explode(";",$detail);
          $total=money($price*$qty);
          $price=money($price);

        /////////////QTY MIN MET
          /*mysql_connect($dbhost,$username,$password);
          @mysql_select_db($database) or die( "Unable to select database");*/
          $query5 = "SELECT order_min FROM menu_items WHERE menu_item_id = '$menu_item_id'";
          $result5 = Treat_DB_ProxyOld::query($query5);
          //mysql_close();

          $order_min=Treat_DB_ProxyOld::mysql_result($result5,0,"order_min");

          /*mysql_connect($dbhost,$username,$password);
          @mysql_select_db($database) or die( "Unable to select database");*/
          $query5 = "SELECT SUM(qty) AS totalqty FROM invoicedetail WHERE reserveid = '$reserveid' AND menu_item_id = '$menu_item_id'";
          $result5 = Treat_DB_ProxyOld::query($query5);
          //mysql_close();

          $totalqty=Treat_DB_ProxyOld::mysql_result($result5,0,"totalqty");

          if ($totalqty<$order_min){$shownoedit2="<font size=2 color=red>(Minimum total of $order_min required)</font>";$order_min_met=1;}
          else{$shownoedit2="";}
        /////////////QTY MIN END
 
          echo "<tr><td></td><td colspan=6><form action=editcateritem.php method=post onSubmit='return disableForm(this);'><INPUT TYPE=hidden name=reserveid value=$reserveid><INPUT TYPE=hidden name=itemid value=$itemid></td><td></td></tr>";

          if ($taxed==""){$item="$item <font color=red>(Non-Taxed)</font>";}
          if ($noedit==1){echo "<tr><td></td><td>$qty</td><td>$item</td><td></td><td align=right>$$price</td><td align=right>$$total</td>";}
          else {echo "<tr><td></td><td><input type=text name=qty value=$qty size=2></td><td>$item $shownoedit2</td><td></td><td align=right>$$price</td><td align=right>$$total</td>";}
          if ($noedit!=1){echo "<td align=right><a href=delitem.php?reserveid=$reserveid&itemid=$itemid onclick='return delconfirm()'><img border=0 src=delete.gif alt='Delete'></a></td><td></td><tr>";}
          else {echo "<td></td><td></td></tr>";}
          if ($noedit==1){echo "<tr><td></td><td></td><td><i>$detail</i></td><td></td><td></td><td></td><td></td><td></td></tr>";}
          else{echo "<tr><td></td><td></td><td><i>$details[0]</i><br><textarea cols=35 rows=2 name=detail>$details[1]</textarea></td><td><input type=hidden name=options value='$details[0]'><input type=submit value='Update'></td><td></form></td><td></td><td></td><td></td></tr>";}
          echo "<tr><td></td><td colspan=6 bgcolor=#E8E7E7 height=1></td><td></td></tr>";

          $num--;
          $total2=$total2+$total;
          $subtotal=$subtotal+($qty*$price);
       }
       $subtotal=money($subtotal);
       if($order_total < $cater_min){$shownoedit="<font color=red>*</font>";}
       else{$shownoedit="";}
       echo "<tr><td></td><td></td><td></td><td></td><td><b>SUBTOTAL$shownoedit:</b></td><td align=right><b>$$subtotal</b></td><td></td><td></td></tr>";
       if ($taxid==""){
          $taxtotal=$subtotal*($taxrate/100); 
          $taxtotal=money($taxtotal); 
          echo "<tr><td></td><td></td><td></td><td></td><td><b>TAXES:</b></td><td align=right><b>$$taxtotal</b></td><td></td><td></td></tr>";
       }
       if ($service!=0){
          $srvcharge=$subtotal*($service/100);
          $srvcharge=money($srvcharge);
          echo "<tr><td></td><td></td><td></td><td></td><td><b>SRV CHARGE:</b></td><td align=right><b>$$srvcharge</b></td><td></td><td></td></tr>";
       }
       $grandtotal=money($subtotal+$taxtotal+$srvcharge);
       echo "<tr><td></td><td></td><td></td><td></td><td><b>TOTAL*:</b></td><td align=right><b>$$grandtotal</b></td><td></td><td></td></tr>";
       
    if ($noedit!=1){echo "<tr><td style='filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr=#E8E7E7, startColorstr=white, gradientType=1);'></td><td colspan=3 align=left style='filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr=white, startColorstr=#E8E7E7, gradientType=1);'><p><br><p><form action='menu.php' method=post onSubmit='return disableForm(this);'><INPUT TYPE=hidden name=reserveid value=$reserveid><input type=submit value='<< Add Items'></form></td>";}
    else {echo "<tr><td></td><td colspan=3></td>";}
    if ($status!=3 && $status !=2 && $order_total >= $cater_min && $order_min_met == 0){echo "<td colspan=3 align=right style='filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr=#E8E7E7, startColorstr=white, gradientType=1);'><p><br><p><form action=submit.php method=post onclick='return placeorder()' onSubmit='return disableForm(this);'><input type=hidden name=reserveid value=$reserveid><input type=submit value='Place Your Order >>'></form></td><td style='filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr=white, startColorstr=#E8E7E7, gradientType=1);'></td></tr>";}
    else {echo "<td colspan=3></td><td></td></tr>";}
    if($subtotal<$cater_min&&$subtotal!=0){echo "<tr><td></td><td colspan=6><font color=red>*The minimum requirement for an online order is $$cater_min.</font></td><td></td></tr>";}
    echo "<tr><td></td><td colspan=6><p>*This is an estimate of your actual bill.  Please await your actual bill when your catering has been confirmed, which can be found under Order History. If you need a printable copy of this order, please <a href=caterprint.php?reserveid=$reserveid&accountid=$accountid&bid=$businessid&cid=1 target='_blank' style=$style>Click Here</a>.</td><td></td></tr>";
    echo "<tr><td height=1% width=1%><img src=whitell.gif height=20 width=20></td><td colspan=6></td><td height=1% width=1% bgcolor=#E8E7E7><img src=whitelr.gif height=20 width=22></td></tr>";

    echo "</table></td><td></td></tr>";
    echo "<tr><td width=1% height=1%><img src=dgrayll.gif height=20 width=20></td><td colspan=2></td><td width=1% height=1%><img src=dgraylr.gif height=20 width=20></td></tr>";
    echo "</table></center>";
    echo "<DIV ID=testdiv1 STYLE=position:absolute;visibility:hidden;background-color:white;layer-background-color:white;></DIV>";
    echo "<p><br><p><br><p><br><p><center><a href=http://essentialpos.com target='_blank'><img src=Logo5.jpg width=150 height=71 border=0></a></center></body>";
  }
}
?>