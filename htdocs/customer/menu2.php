<?php

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+3);
        if ($diff > 0 && $diff < '0.01'){$diff="0.01";}
        elseif($diff < 0 && $diff > '-0.01'){$diff="-0.01";}
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      if ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='31')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='08' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      $day=$day-31;  
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";} 
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

$username="mburris_user";
$password="bigdog";
$database="mburris_businesstrack";
$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today2="$year-$month-$day";
$todayname = date("l");
$bbdayname=date("l");
$bbmonth=date("F");
$bbday=date("j");
$today = date("l");
$todaynum = date("j");
$todaynum2 = date("j");
$year = date("Y");
$monthnum = date("n");
$day=1;
$high = 6;
$long = 1;
$leap = date("L");
$back = "white";
$count = 0;
$style = "text-decoration:none";

$user=$_POST['username'];
$pass=$_POST['password'];
$accountid=$_POST['accountid'];
if ($user==""&&$pass==""){
   $user=$_COOKIE["usercook"];
   $pass=$_COOKIE["passcook"];
}
$newmonth=$_GET['newmonth'];
$newyear=$_GET['newyear'];
$newstart=$_GET['newstart'];

if ($newstart == "")
{
   while ($todaynum > 7){$todaynum = $todaynum - 7; $count++;}
   if ($today == 'Sunday' && $todaynum == 1){$start=1;}
   if ($today == 'Monday' && $todaynum == 1){$start=2;}
   if ($today == 'Tuesday' && $todaynum == 1){$start=3;}
   if ($today == 'Wednesday' && $todaynum == 1){$start=4;}
   if ($today == 'Thursday' && $todaynum == 1){$start=5;}
   if ($today == 'Friday' && $todaynum == 1){$start=6;}
   if ($today == 'Saturday' && $todaynum == 1){$start=7;}
   if ($today == 'Sunday' && $todaynum == 2){$start=7;}
   if ($today == 'Monday' && $todaynum == 2){$start=1;}
   if ($today == 'Tuesday' && $todaynum == 2){$start=2;}
   if ($today == 'Wednesday' && $todaynum == 2){$start=3;}
   if ($today == 'Thursday' && $todaynum == 2){$start=4;}
   if ($today == 'Friday' && $todaynum == 2){$start=5;}
   if ($today == 'Saturday' && $todaynum == 2){$start=6;}
   if ($today == 'Sunday' && $todaynum == 3){$start=6;}
   if ($today == 'Monday' && $todaynum == 3){$start=7;}
   if ($today == 'Tuesday' && $todaynum == 3){$start=1;}
   if ($today == 'Wednesday' && $todaynum == 3){$start=2;}
   if ($today == 'Thursday' && $todaynum == 3){$start=3;}
   if ($today == 'Friday' && $todaynum == 3){$start=4;}
   if ($today == 'Saturday' && $todaynum == 3){$start=5;}
   if ($today == 'Sunday' && $todaynum == 4){$start=5;}
   if ($today == 'Monday' && $todaynum == 4){$start=6;}
   if ($today == 'Tuesday' && $todaynum == 4){$start=7;}
   if ($today == 'Wednesday' && $todaynum == 4){$start=1;}
   if ($today == 'Thursday' && $todaynum == 4){$start=2;}
   if ($today == 'Friday' && $todaynum == 4){$start=3;}
   if ($today == 'Saturday' && $todaynum == 4){$start=4;}
   if ($today == 'Sunday' && $todaynum == 5){$start=4;}
   if ($today == 'Monday' && $todaynum == 5){$start=5;}
   if ($today == 'Tuesday' && $todaynum == 5){$start=6;}
   if ($today == 'Wednesday' && $todaynum == 5){$start=7;}
   if ($today == 'Thursday' && $todaynum == 5){$start=1;}
   if ($today == 'Friday' && $todaynum == 5){$start=2;}
   if ($today == 'Saturday' && $todaynum == 5){$start=3;}
   if ($today == 'Sunday' && $todaynum == 6){$start=3;}
   if ($today == 'Monday' && $todaynum == 6){$start=4;}
   if ($today == 'Tuesday' && $todaynum == 6){$start=5;}
   if ($today == 'Wednesday' && $todaynum == 6){$start=6;}
   if ($today == 'Thursday' && $todaynum == 6){$start=7;}
   if ($today == 'Friday' && $todaynum == 6){$start=1;}
   if ($today == 'Saturday' && $todaynum == 6){$start=2;}
   if ($today == 'Sunday' && $todaynum == 7){$start=2;}
   if ($today == 'Monday' && $todaynum == 7){$start=3;}
   if ($today == 'Tuesday' && $todaynum == 7){$start=4;}
   if ($today == 'Wednesday' && $todaynum == 7){$start=5;}
   if ($today == 'Thursday' && $todaynum == 7){$start=6;}
   if ($today == 'Friday' && $todaynum == 7){$start=7;}
   if ($today == 'Saturday' && $todaynum == 7){$start=1;}
}
elseif ($newstart > 7)
{
   $start = $newstart - 7;
}
else 
{
   $start = $newstart;
}

if ($newmonth != ""){;}
else {$newmonth = $monthnum;}
if ($newyear != ""){$year=$newyear;}
else {$newyear = $year;}

mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM customer WHERE username = '$user' AND password = '$pass'";
$result = mysql_query($query);
$num=mysql_numrows($result);
mysql_close();

if ($num != 1 || $user == "" || $pass == "")
{
    echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
    $user=mysql_result($result,0,"username");
    $customerid=mysql_result($result,0,"customerid");
    $accountid=mysql_result($result,0,"accountid");
    $businessid=mysql_result($result,0,"businessid");

    mysql_connect($dbhost,$username,$password);
    @mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM business WHERE businessid = '$businessid'";
    $result = mysql_query($query);
    mysql_close();

    $taxrate=mysql_result($result,0,"tax");
    $default_menu=mysql_result($result,0,"default_menu");
    $caterdays=mysql_result($result,0,"caterdays");

    $finalday=$today2;
    for ($counter2=1;$counter2<=$caterdays;$counter2++){$finalday=nextday($finalday);}

    mysql_connect($dbhost,$username,$password);
    @mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM accounts WHERE accountid = '$accountid'";
    $result = mysql_query($query);
    mysql_close();

    $accountname=mysql_result($result,0,"name");
    $accountnum=mysql_result($result,0,"accountnum");
    $taxid=mysql_result($result,0,"taxid");
    $curmenu=mysql_result($result,0,"menu");

    if ($curmenu==-1){$curmenu=$default_menu;}

    setcookie("usercook",$user);
    setcookie("passcook",$pass);
    setcookie("curcust",$accountid);
    setcookie("curmenu",$curmenu);
    setcookie("finalday",$finalday);

    echo "<body background=backdrop.jpg>";
    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%><tr><td colspan=4><img src=logo.jpg><p></td></tr>";

    mysql_connect($dbhost,$username,$password);
    @mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM company WHERE companyid = '1'";
    $result = mysql_query($query);
    mysql_close();

    $companyname=mysql_result($result,0,"companyname");
    $companyid=1;

    echo "<tr bgcolor=black><td height=1 colspan=4></td></tr>";
    echo "<tr bgcolor=#CCCCFF><td width=10%><img src=weblogo.jpg height=75 width=80></td><td style='filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr=#FFFFFF, startColorstr=#CCCCFF, gradientType=1);'><font size=4><b>$companyname</b></font><br><b>Account: $accountname<br>Account #: $accountnum</td><td colspan=2 align=right valign=top style='filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr=#CCCCFF, startColorstr=#FFFFFF, gradientType=1);'><b>$bbdayname, $bbmonth $bbday, $year</b><p>$showcart</td></tr>";
    echo "<tr bgcolor=black><td height=1 colspan=4></td></tr>";
    echo "<tr><td colspan=2>Menu Schedule :: <a href=catertrack2.php><font color=blue>Order History</font></a><p></td></tr>";
    echo "</table></center>";

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    if ($newmonth == 1){$month = "January";}
    elseif ($newmonth == 2){$month = "February";}
    elseif ($newmonth == 3){$month = "March";}
    elseif ($newmonth == 4){$month = "April";}
    elseif ($newmonth == 5){$month = "May";}
    elseif ($newmonth == 6){$month = "June";}
    elseif ($newmonth == 7){$month = "July";}
    elseif ($newmonth == 8){$month = "August";}
    elseif ($newmonth == 9){$month = "September";}
    elseif ($newmonth == 10){$month = "October";}
    elseif ($newmonth == 11){$month = "November";}
    elseif ($newmonth == 12){$month = "December";}

    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%>";
    echo "<tr bgcolor=black><td height=1 colspan=2></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td colspan=2>";
    echo "<center><h3><p><br>$month $year</h3></center></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td colspan=2>";

    echo "<center><table border=1 cellpadding=0 cellspacing=0>";
    echo "<tr bgcolor=#FFFF99><td><center>Sunday</center></td><td><center>Monday</center></td><td><center>Tuesday</center></td><td><center>Wednesday</center></td><td><center>Thursday</center></td><td><center>Friday</center></td><td><center>Saturday</center></td></tr>";
    
    echo "<tr height=110>";
    $weekend = "F";
    while ($long < 8)
    {  
       if ($long >= $start)
       {
          if ($long == 1 || $long == 7){$weekend="T";}

          if ($day < 10 && $newmonth < 10){$date = "$newyear-0$newmonth-0$day";}
          elseif ($day < 10 && $newmonth > 10){$date = "$newyear-$newmonth-0$day";}
          elseif ($day > 10 && $newmonth < 10){$date = "$newyear-0$newmonth-$day";}
          else {$date = "$newyear-$newmonth-$day";}

          if ($newstart == ""){$newstart=$start;}
          $correcturl = "orderdatecust.php?date=$date&newmonth=$newmonth&newyear=$newyear&newstart=$newstart&day=$day&weekend=$weekend&bid=$businessid&cid=$companyid";

          if ($day == $todaynum2 && $monthnum == $newmonth && $year == $newyear){$color = "red";}
          else {$color = "black";}

          echo "<a style=$style href=$correcturl><td bgcolor=$back width=120 valign=top onMouseOver=this.bgColor='#999999' onMouseOut=this.bgColor='$back'>";
          
          echo "<a style=$style href=$correcturl><b><font color=$color>$day</font></b></a><br>";

          mysql_connect($dbhost,$username,$password);
          @mysql_select_db($database) or die( "Unable to select database");
          $query14 = "SELECT * FROM caterclose WHERE companyid = '$companyid' AND businessid = '$businessid' AND date = '$date'";
          $result14 = mysql_query($query14);
          $num14=mysql_numrows($result14);
          mysql_close();

    if ($num14==0){
/////////////////////////////////////////////////////////////
      
    mysql_connect($dbhost,$username,$password);
    @mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM menu_daypart WHERE menu_typeid = '$curmenu' ORDER BY orderid DESC";
    $result = mysql_query($query);
    $num=mysql_numrows($result);
    mysql_close();

    $num--;
    while ($num>=0){
       $menu_daypartid=mysql_result($result,$num,"menu_daypartid"); 
       $menu_daypartname=mysql_result($result,$num,"menu_daypartname"); 

       mysql_connect($dbhost,$username,$password);
       @mysql_select_db($database) or die( "Unable to select database");
       $query10 = "SELECT * FROM menu_daypartdetail WHERE daypartid = '$menu_daypartid' AND accountid = '$curcust'";
       $result10 = mysql_query($query10);
       $num10=mysql_numrows($result10);
       mysql_close();

       ///////BEGIN IF
       if ($num10!=0){

       echo "<b><font size=1 color=blue><u>$menu_daypartname</u></font></b><br>";

       mysql_connect($dbhost,$username,$password);
       @mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM order_item WHERE date = '$date' AND daypartid = '$menu_daypartid' AND businessid = '$businessid' ORDER BY order_itemid DESC";
       $result2 = mysql_query($query2);
       $num2=mysql_numrows($result2);
       mysql_close();

       $num2--;
       while ($num2>=0){
          $order_itemid=mysql_result($result2,$num2,"order_itemid"); 
          $menu_itemid=mysql_result($result2,$num2,"menu_itemid"); 

          mysql_connect($dbhost,$username,$password);
          @mysql_select_db($database) or die( "Unable to select database");
          $query3 = "SELECT * FROM menu_items WHERE menu_item_id = '$menu_itemid'";
          $result3 = mysql_query($query3);
          mysql_close();

          $item_name=mysql_result($result3,0,"item_name"); 

          echo "<font size=1>$item_name</font><br>";
          
          $num2--;
       }
       }////////////ENDIF
       $num--;
    }

/////////////////////////////////////////////////////////////
    }
    else{echo "<font size=2 color=red>CLOSED</font>";}
          $counter=0;

          echo "</td></a>";
          $back = "white";

          if ($day == 1 && $long == 1){$backstart=7;}
          elseif ($day == 1 && $long != 1){$backstart=$long-1;}

          $day++;
       }
       else
       {
          echo "<td width=110 bgcolor=#DCDCDC></td>";
       }
       $weekend="F";
       $long++;
    }
    echo "</tr>";
    
    while ($high > 1)
    {
       $long=1;
       echo "<tr height=110>";
       $weekend = "F";
       while ($long < 8)
       {
          if ($long == 1 || $long == 7){$weekend="T";}

          if ($day < 10 && $newmonth < 10){$date = "$newyear-0$newmonth-0$day";}
          elseif ($day < 10 && $newmonth > 10){$date = "$newyear-$newmonth-0$day";}
          elseif ($day >= 10 && $newmonth < 10){$date = "$newyear-0$newmonth-$day";}
          else {$date = "$newyear-$newmonth-$day";}      

          if ($newstart == ""){$newstart=$start;}
          $correcturl = "orderdatecust.php?date=$date&newmonth=$newmonth&newyear=$newyear&newstart=$newstart&day=$day&weekend=$weekend&bid=$businessid&cid=$companyid";

          $show = $day;
          if ($day > 28 && $newmonth == 2 && $leap == 0){$show = "";}
          elseif ($day > 29 && $newmonth == 2 and $leap == 1){$show = "";}
          elseif ($day > 30 && ($newmonth == 4 || $newmonth == 6 || $newmonth == 9 || $newmonth == 11)){$show = "";}
          elseif ($day > 31){$show = "";}
          else {;}
          
          if ($day > 28 && $newmonth == 2 && $leap == 0){$back = '#DCDCDC';}
          elseif ($day > 29 && $newmonth == 2 and $leap == 1){$back = '#DCDCDC';}
          elseif ($day > 30 && ($newmonth == 4 || $newmonth == 6 || $newmonth == 9 || $newmonth == 11)){$back = '#DCDCDC';}
          elseif ($day > 31){$back = '#DCDCDC';}
          else {$back = 'white';}

          if ($day == 29 && $newmonth == 2 && $leap ==0){$newstart=$long;}
          if ($day == 30 && $newmonth == 2 && $leap ==1){$newstart=$long;}
          if ($day == 31 && ($newmonth == 4 || $newmonth == 6 || $newmonth == 9 || $newmonth == 11)){$newstart=$long;}
          if ($day == 32 && ($newmonth == 1 || $newmonth == 3 || $newmonth == 5 || $newmonth == 7 || $newmonth == 8 || $newmonth == 10 || $newmonth == 12)){$newstart=$long;}     
         
          if ($day == $todaynum2 && $monthnum == $newmonth && $year == $newyear){$color = "red";}
          else {$color = "black";}

          echo "<a style=$style href=$correcturl><td bgcolor=$back width=120 valign=top onMouseOver=this.bgColor='#999999' onMouseOut=this.bgColor='$back'>";
          
          echo "<a style=$style href=$correcturl><b><font color=$color>$show</font></b></a><br>";

          mysql_connect($dbhost,$username,$password);
          @mysql_select_db($database) or die( "Unable to select database");
          $query14 = "SELECT * FROM caterclose WHERE companyid = '$companyid' AND businessid = '$businessid' AND date = '$date'";
          $result14 = mysql_query($query14);
          $num14=mysql_numrows($result14);
          mysql_close();

if ($num14==0){
/////////////////////////////////////////////////////////////
if ($back!="#DCDCDC"){     
    mysql_connect($dbhost,$username,$password);
    @mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM menu_daypart WHERE menu_typeid = '$curmenu' ORDER BY orderid DESC";
    $result = mysql_query($query);
    $num=mysql_numrows($result);
    mysql_close();

    $num--;
    while ($num>=0){
       $menu_daypartid=mysql_result($result,$num,"menu_daypartid"); 
       $menu_daypartname=mysql_result($result,$num,"menu_daypartname"); 

       mysql_connect($dbhost,$username,$password);
       @mysql_select_db($database) or die( "Unable to select database");
       $query10 = "SELECT * FROM menu_daypartdetail WHERE daypartid = '$menu_daypartid' AND accountid = '$curcust'";
       $result10 = mysql_query($query10);
       $num10=mysql_numrows($result10);
       mysql_close();

       ///////BEGIN IF
       if ($num10!=0){

       echo "<b><font size=1 color=blue><u>$menu_daypartname</u></font></b><br>";

       mysql_connect($dbhost,$username,$password);
       @mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM order_item WHERE date = '$date' AND daypartid = '$menu_daypartid' AND businessid = '$businessid' ORDER BY order_itemid DESC";
       $result2 = mysql_query($query2);
       $num2=mysql_numrows($result2);
       mysql_close();

       $num2--;
       while ($num2>=0){
          $order_itemid=mysql_result($result2,$num2,"order_itemid"); 
          $menu_itemid=mysql_result($result2,$num2,"menu_itemid"); 

          mysql_connect($dbhost,$username,$password);
          @mysql_select_db($database) or die( "Unable to select database");
          $query3 = "SELECT * FROM menu_items WHERE menu_item_id = '$menu_itemid'";
          $result3 = mysql_query($query3);
          mysql_close();

          $item_name=mysql_result($result3,0,"item_name"); 

          echo "<font size=1>$item_name</font><br>";
          
          $num2--;
       }
       }///////ENDIF
       $num--;
    }
}
/////////////////////////////////////////////////////////////
}
else{echo "<font size=2 color=red>CLOSED</font>";}
          $counter=0;
         
          echo "</td></a>";
          $back = "white";

          $day++;
          $weekend = "F";
          $long++;
       }
       echo "</tr>";
       $high--;
    } 
    echo "</table></center>";

    if ($newmonth == 5 || $newmonth == 7 || $newmonth == 10 || $newmonth == 12){$count=30;}
    elseif ($newmonth == 1 || $newmonth == 2 || $newmonth == 4 || $newmonth == 6 || $newmonth == 8 || $newmonth == 9 || $newmonth == 11){$count=31;}
    elseif ($newmonth == 3 && $leap == 0){$count=28;}
    elseif ($newmonth == 3 && $leap == 1){$count=29;}

    $count2 = $backstart;
    while ($count > 1)
    {
       if ($count2 == 1){$count2 = 8;}
       $count--;
       $count2--;
    }
    $backstart = $count2;

    $prevmonth = $newmonth - 1;
    $prevyear = $newyear;
    if ($prevmonth == 0){$prevmonth = 12; $prevyear = $year - 1;}
    $nextmonth = $newmonth + 1;
    $nextyear = $newyear;
    if ($nextmonth == 13){$nextmonth = 1; $nextyear=$year + 1;}
    echo "<center><a href='menu2.php?newmonth=$prevmonth&newyear=$prevyear&newstart=$backstart&bid=$businessid&cid=$companyid'>Previous Month</a> :: <a href='menu2.php?newmonth=$nextmonth&newyear=$nextyear&newstart=$newstart&bid=$businessid&cid=$companyid'>Next Month</a></center><p>";

    echo "</td></tr>";
    echo "<tr bgcolor=black><td height=1 colspan=2></td></tr></table>";
    echo "</body>";

}
?>