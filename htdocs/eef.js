/**** ESSENTIAL ELEMENTS JS FRAMEWORK *****\
| FILE: eef.js                             |
| BY:  Tim Frazier (Essential Elements)    |
| ---------------------------------------- |
| CREATED: 10-16-2008                      |
| MODIFIED: 11-06-2008                     |
| ---------------------------------------- |
| NOTES:                                   |
| Implemented Drag and Drop Functionalities|
| ---------------------------------------- |
| DESCRIPTION:                             |
|   This file houses many useful DOM       |
| shortcuts and implements AJAX            |
| functionality across multiple browsers   |
| ---------------------------------------- |
| RIGHTS:                                  |
|   This file may not be used or modified  |
| unless done so by an Essential Elements  |
| employee.                                |
\******************************************/


/////////////////////////////////
//////// DOM SHORTCUTS //////////
/////////////////////////////////

Array.prototype.find = function(needle, incKeys){
	for(i in this){
		if(incKeys != null && incKeys){
			if(i == needle || this[i] == needle) return true;
		}
		else{
			if(this[i] == needle) return true;
		}
	}
	return false;
}

var $ = function(id){
	return document.getElementById(id);
}

var $A = function(item, target){
	if(target != null) target.appendChild(item);
	else document.body.appendChild(item);
}
var $R = function(item, target){
	if(target == null) document.body.removeChild(item);
	else target.removeChild(item);
}
var $C = function(tag){
	return document.createElement(tag);
}

var $T = function(text){
	return document.createTextNode(text);
}

var $N = function(tagName){
	return document.getElementsByTagName(tagName);
}

var $I = function(ele, html){
	return ele.innerHTML = html;
}

var Element = {};
Element.hide = function(id){
	$(id).style.display = 'none';
}
Element.show = function(id){
	if($(id).nodeName.toLowerCase() == 'input') $(id).style.display = 'inline';
	else $(id).style.display = 'block';
}
Element.toggle = function(id){
	($(id).style.display == 'none') ? Element.show(id) : Element.hide(id);
}
Element.pos = function(ele){	
	var x = y = 0;

	while(ele != null){
		x += parseInt(ele.offsetLeft);
		y += parseInt(ele.offsetTop);
		ele = ele.offsetParent;
	}
	return [x, y];
}
Element.width = function(ele){
	return ele.offsetWidth; }

/////////////////////////////////
/////// AJAX FUNCTIONALITY //////
/////////////////////////////////

var Ajax = {}
Ajax.getXMLObj = function(){
	var xmlHttp; 
	try{
		// Firefox, Opera 8, & Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch(e){
		try{
			//IE 6+
			xmlHttp = new ActiveXObject('Msxml2.XMLHTTP');
		}
		catch(e){ try{
				//IE 5+
				xmlHttp = new ActiveXObject('Microsoft.XMLHTTP');
			}
			catch(e){
				alert("Your browser does not support AJAX!");
			}
		}
	}
	return xmlHttp;
}
Ajax.Request = function(url, options){
	this.xmlHttp = Ajax.getXMLObj();
	this.url = url;
	this.options = options;
	this.regOpts();

	if(this.onLoading != null) this.onLoading.call(this);
	this.send();
}
Ajax.Request.prototype.send = function(){
	var obj = this;

	this.xmlHttp.onreadystatechange = function(){ obj.callBack(); } 
	if(this.method == "GET"){
		//this.xmlHttp.setRequestHeader("Content-type", "text/html; charset=ISO-8859-1"); 
		this.xmlHttp.open(this.method, this.url+this.parseParams(), this.async);	
		this.xmlHttp.send(null);
	}
	else{
		this.xmlHttp.open(this.method, this.url, this.async);	
		this.xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		this.xmlHttp.send(this.parseParams());
	}
}
Ajax.Request.prototype.parseParams = function(){
	if(this.params == null) return null;
	else{
		var x = 0;
		var str = (this.method == "GET") ? '?' : '';
		for(var i in this.params){
			if(i != 'find'){
				//Array Passed
				if(typeof this.params[i] == 'object'){
					var arrI = 0;
					for(var item in this.params[i]){
						if(x == 0) str += i + '[' + arrI + ']' + "=" + this.params[i][arrI];
						else str += "&" + i + '[' + arrI + ']' + "=" + this.params[i][arrI];
						arrI++;
					}
				}
				//Normal Val: Int, Str, Double, Etc.
				else{
					if(x == 0) str += i + "=" + this.params[i];
					else str += "&" + i + "=" + this.params[i];
				}
				x++;
			}
		}
		return str;
	}
}
Ajax.Request.prototype.callBack = function(){
	if(this.xmlHttp.readyState == 4){
		if(this.onComplete != null) this.onComplete.call(this);

		if(this.xmlHttp.status == 200){
			if(this.onSuccess != null) this.onSuccess.call(this, this.xmlHttp);
		}
		else{
			if(this.onError != null) this.onError.call(this, this.xmlHttp);
		}
	}
}
Ajax.Request.prototype.regOpts = function(){
	var opts = this.options;
	for(var i in opts){
		this[i] = opts[i];
	}
	this.method = (this.method == null) ? 'GET' : this.method;
	this.async = (this.async == null) ? true : this.async;
}


/////////////////////////////////
///////// FORM CONTROL //////////
/////////////////////////////////

var Form = {};
Form.request = function(formid, options){
	var form = $(formid);
	var method = form.method.toUpperCase();
	var action = form.action;
	var inputs = Form.getInputs(formid);
	
	if(options == null) options = {};
	options.params = inputs;
	options.method = method;

	new Ajax.Request(action, options);
}
Form.serialize = function(formid, method){
	var inputs = Form.getInputs(formid);
	var str;

	if(method.toUpperCase() == "GET"){
		str = '?';
	}
	else str = '';

	var x = 0;
	for(i in inputs){
		if(x != 0) str += '&';
		str += i+"="+inputs[i];
		x++;
	}
	return str;
}

Form.getInputs = function(formid){
	var form = $(formid);
	var inputs = [];

	for(var i = 0; i < form.elements.length; i++){
		if(typeof form.elements[i] == 'object'){
			if(form.elements[i]["nodeName"].toLowerCase() == 'input'){
				if(form.elements[i].name != ''){
					inputs[form.elements[i].name] = form.elements[i].value;
				}
			}
		}
	}

	return inputs;
}

/////// EDIT IN PLACE FORM HANDLING
Form.EditInPlace = function(tableid, options){
	this.cols = []; //Holds editable column names in db
	this.curRow; // Keeps track of current row being edited
	this.options = (options != null) ? options : {};
	this.table = $(tableid);

	this.editableCols();
	this.registerBindings();
}
Form.EditInPlace.prototype.tdSwap = function(trigger){
	var obj = this;
	this.curRow = trigger.parentNode.rowIndex;
	
	trigger.onclick = null; // Prevent from firing again
	var val = trigger.innerHTML;
	var input = $C('input');
	input.onblur = function(){ obj.inputSwap(trigger); }
	input.onfocus = function(){ this.select(); }
	input.onkeydown = function(transport){      
		var e = (transport == null) ? window.event : transport;
		if(e.keyCode == 13) obj.inputSwap(trigger);      // Save upon [Enter]/[Return]
		else if(e.keyCode == 9){                         // Save and move to next Cell on [Tab]
			var i = this.parentNode.cellIndex + 1;
			if(i < obj.table.rows[0].cells.length){
				obj.tdSwap(obj.table.rows[obj.curRow].cells[this.parentNode.cellIndex+1]);
			}
		}
	} 
	input.type = 'text';
	input.value = (val == '[CLICK HERE]') ? '' : val;
	input.style.width = trigger.offsetWidth;

	trigger.innerHTML = '';
	$A(input, trigger);
	input.focus();
}
Form.EditInPlace.prototype.inputSwap = function(trigger){
	var obj = this;
	trigger.onblur = null;
	var input = trigger.childNodes[0];
	var val = input.value;

	this.options.params = {};
	this.options.params['table'] = this.table.id;
	this.options.params['value'] = val;
	this.options.params['column'] = this.cols[trigger.cellIndex];
	this.options.params['id'] = trigger.parentNode.id;

	new Ajax.Request('ajaxreq.php', this.options);

	trigger.innerHTML = '';
	$A($T(val), trigger);
	trigger.onclick = function(){ obj.tdSwap(trigger); }
}
Form.EditInPlace.prototype.registerBindings = function(){
	var obj = this;

	for(row in this.table.rows){
		if(row != 0 && row != 'find'){
			for(cell in this.table.rows[row].cells){
				if(cell != 'find'){
					var x = this.table.rows[row].cells[cell];
					if(this.isEditable(x.cellIndex)){
						x.onclick = function(){ obj.tdSwap(this); }
					}
				}
			}
		}
	}
}
Form.EditInPlace.prototype.bindRow = function(index){
	var obj = this;

	for(i in this.table.rows[index].cells){
		var cell = this.table.rows[index].cells[i];
		if(this.isEditable(cell.cellIndex) || cell.innerHTML == '[CLICK HERE]'){
			cell.onclick = function(){ obj.tdSwap(this); }
		}
	}
}
Form.EditInPlace.prototype.isEditable = function(index){
	if(this.options.noEdit != null && this.options.noEdit.find(index)) return false;
	else if(this.cols.find(index, true)) return true;
	else return false;
}
Form.EditInPlace.prototype.editableCols = function(){
	for(i in this.table.rows[0].cells){
		var cell = this.table.rows[0].cells[i];
		if(cell.id != null && cell.id != ''){
			this.cols[cell.cellIndex] = cell.id;
		}
	}
}
Form.EditInPlace.prototype.newRow = function(){
	var tr = $C('tr');
	var td;
	var index = (this.table.rows.length < 2) ? 1:parseInt(this.table.rows[this.table.rows.length - 1].id) + 1;

	tr.id = index;
	for(var i = 0; i < this.table.rows[0].cells.length; i++){
		td = $C('td');
		$A($T('[CLICK HERE]'), td);
		$A(td, tr);
	}
	

	$A(tr, this.table.tBodies[0]);
	this.bindRow(index);
	this.tdSwap(tr.cells[0]);
}

/////////////////////////////////
///////// Drag and Drop /////////
/////////////////////////////////

var Draggable = function(id, opts){
	this.id = id;
	this.opts = opts || {};
	this.ele = $(id);
	this.dragging = false;
	obj = this;	
	
	this.regBindings();
}
Draggable.prototype.regBindings = function(){
	var obj = this;
	this.ele.onmousedown = function(e){obj.startDrag(e); }
	this.ele.onmouseup = function(e){obj.endDrag(e); }
	//this.ele.onmousemove = function(e){obj.dragMove(e); }
}
	
Draggable.prototype.startDrag = function(e){
	var ev = e || window.event;
	var obj = this;
	/**/ document.body.onmousemove = function(e){obj.dragMove(e);}
	/**/ document.body.onmouseup = function(e){obj.endDrag(e);}
	if(this.opts.onDragInit != null) this.opts.onDragInit.call(this, this);
	var coords = Element.pos(this.ele);
	var cx = ev.clientX;
	var cy = ev.clientY;
	
	this.offsetLeft = cx - coords[0];
	this.offsetTop = cy - coords[1];

	this.dragging = true;
	this.ele.style.position = 'absolute';
	window.status = "Start Follow";
}
Draggable.prototype.dragMove = function(e){
	var ev = e || window.event;
	if(this.dragging){
		var newLeft = ev.clientX - this.offsetLeft;		
		var newTop = ev.clientY - this.offsetTop;		
		this.ele.style.top = newTop +"px";
		this.ele.style.left = newLeft +"px";
		window.status = "Following.. ";
		if(this.opts.onDrag != null) this.opts.onDrag.call(this, this);
	}
	else window.status = 'Click and hold to drag.';
}
Draggable.prototype.endDrag = function(){
	var obj = this;
	/**/document.body.onmousemove = null;
	/**/document.body.onmouseup = null;
	this.dragging = false;
	window.status = "Release";
	document.body.focus(); //Prevent FF3 DnD
	if(this.opts.onDrop != null) this.opts.onDrop.call(this, this);
}


/////////////////////////////////////////
///////////// CLONED DRAG //////////////
/////////////////////////////////////////

ClonedDrag = function(id, opts){
	this.id = id;
	this.opts = opts || {};
	this.ele = $(id);
	this.clones = [];
	var obj = this;	

	this.ele.onmousedown = function(e){obj.cloneEle(e);}
}
ClonedDrag.prototype.cloneEle = function(e){
	var ev = e || window.event;
	var ele = $C('div');
	var index = this.clones.length;
	ele.id = this.id + "_clone" + index;
	ele.className = this.id;
	ele.style.position = 'absolute';
	
	var pos = Element.pos(this.ele);
	var cx = ev.clientX;
	var cy = ev.clientY;

	ele.style.left = pos[0] + 10 +"px";
	ele.style.top = pos[1] +"px";
	$A(ele);

	this.clones[index] = new Draggable(ele.id, this.opts);
	this.clones[index].ele.focus();
	this.clones[index].startDrag(e);
	this.clones[index].offsetLeft = cx - pos[0];
	this.clones[index].offsetTop = cy - pos[1];


}


/////////////////////////////////////////
///////////// EEF HASH //////////////////
/////////////////////////////////////////

$H = function(hashobj){
	this.length = 0;
	this.items = new Array();
	this.keys = new Array();

	for(i in hashobj){
		this.items[i] = hashobj[i];
		this.keys[this.keys.length] = i;
		this.length++;
	}
}
$H.prototype.removeItem = function(in_key){
	var tmp_value;
	if (typeof(this.items[in_key]) != 'undefined') {
		this.length--;
		var tmp_value = this.items[in_key];
		delete this.items[in_key];
	}

	return tmp_value;
}
$H.prototype.getItem = function(in_key) {
	if(typeof in_key == 'number') alert(this.keys[in_key]);
	else return this.items[in_key];
}
$H.prototype.nextKey = function(key){
	return this.keys[key+1];
}	
$H.prototype.setItem = function(in_key, in_value){
	if (typeof(in_value) != 'undefined') {
		if (typeof(this.items[in_key]) == 'undefined') {
			this.length++;
		}
		this.items[in_key] = in_value;
	}

	return in_value;
}
$H.prototype.hasItem = function(in_key){
	return typeof(this.items[in_key]) != 'undefined';
}

//Hash Iterator
$HI = function(hash){
	this.index = 0;
	this.hash = hash;
}
$HI.prototype.hasNext = function(){
	return (this.index < this.hash.items.length);
}
$HI.prototype.next = function(){
	var ret = this.hash.getItem(this.index);
	this.index++;
	return ret;
}
