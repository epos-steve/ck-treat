<?php
define( 'SITECH_APP_PATH', realpath( dirname(__FILE__) . '/../application' ) );
define( 'EEF_APP_PATH', SITECH_APP_PATH );

/* Tell PHP to look into our library for includes as well */
#set_include_path( realpath( SITECH_APP_PATH . '/../lib/' ) . PATH_SEPARATOR . get_include_path() );
require_once( dirname( dirname( __FILE__ ) ) . '/application/bootstrap.php' );

/* Set up our Load Handler [For auto_load] */
#require_once( 'SiTech/Loader.php' );
#SiTech_Loader::registerAutoload();

# list these in route alpha order for ease of lookup later
SiTech_Controller::addRoute( '/menu/history\.php',                      'menu/hist', 'index' );
SiTech_Controller::addRoute( '/menu/history',                           'menu/hist', 'index' );
SiTech_Controller::addRoute( '/menu/history/display_set',               'menu/hist', 'display_set' );
SiTech_Controller::addRoute( '/menu/history/limit_delete',              'menu/hist', 'limit_delete' );
SiTech_Controller::addRoute( '/menu/history/limit_set',                 'menu/hist', 'limit_set' );
SiTech_Controller::addRoute( '/menu/nutrition\.php',                    'nutrition/recipe', 'menu_nutrition' );
SiTech_Controller::addRoute( '/menu/nutrition2\.php',                   'nutrition/recipe', 'menu_nutrition2' );
SiTech_Controller::addRoute( '/nutrition/recipe/ajaxautocom\.php',      'nutrition/recipe', 'ajaxautocom_php' );
SiTech_Controller::addRoute( '/ta/ajax_pos\.php',                       'ta/businessMenus', 'posAjax' );
SiTech_Controller::addRoute( '/ta/ajax/hierarchy/history/update/(.*)',  'ajax/hierarchy/history', 'update');
SiTech_Controller::addRoute( '/ta/buscatermenu\.php',                   'ta/businessMenus', 'menus' );
SiTech_Controller::addRoute( '/ta/buscatermenu3\.php',                  'ta/businessMenus', 'posMenuLinking' );
SiTech_Controller::addRoute( '/ta/buscatermenu4\.php',                  'ta/businessMenus', 'posMenu' );
SiTech_Controller::addRoute( '/ta/buscatermenu5\.php',                  'ta/businessMenus', 'posReporting' );
SiTech_Controller::addRoute( '/ta/buscatermenu6\.php',                  'ta/businessMenus', 'posManagement' );
SiTech_Controller::addRoute( '/ta/buscontrolc/csv_import_gl',           'ta/buscontrol', 'csv_import_gl' );
SiTech_Controller::addRoute( '/ta/buscontrolc/csv_import_budget',       'ta/buscontrol', 'csv_import_budget' );
SiTech_Controller::addRoute( '/ta/buscontrol2\.popup\.php',             'ta/buscontrol', 'popup2' );
SiTech_Controller::addRoute( '/ta/buscontrol2\.php',                    'ta/buscontrol', 'control_sheet2' );
SiTech_Controller::addRoute( '/ta/businessMenus/ajax_pos\.php',         'ta/businessMenus', 'posAjax' );
SiTech_Controller::addRoute( '/ta/businventor4\.php',                   'nutrition/recipe', 'business_inventory4' );
SiTech_Controller::addRoute( '/ta/editrecipe\.php',                     'nutrition/recipe', 'edit' );
SiTech_Controller::addRoute( '/ta/nutrition\.php',                      'nutrition/recipe', 'menu_nutrition' );
SiTech_Controller::addRoute( '/ta/nutrition/recipe/conversion',         'nutrition/recipe', 'conversion' );
SiTech_Controller::addRoute( '/ta/printrecipe\.php',                    'nutrition/recipe', 'printer_friendly' );
SiTech_Controller::addRoute( '/ta/printroute\.php',                     'ta/route', 'print_detail' );
SiTech_Controller::addRoute( '/ta/saverecipenotes\.php',                'nutrition/recipe', 'save_notes' );
SiTech_Controller::addRoute( '/ta/savecommission.php',                  'ta/route', 'save_commission_settings' );

try {
	$uri = new SiTech_Uri();
	if ( !defined( 'TREAT_BASEURL' ) ) {
		define( 'TREAT_BASEURL', (string)$uri );
	}
#	SiTech_Loader::loadBootstrap();
	SiTech_Controller::dispatch( $uri );
}
catch ( Exception $e ) {
	if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
		# throw the error for xdebug
		echo '<pre>';
#		throw new Exception( 'Debug exception', null, $e );
		throw $e;
		echo '</pre>';
	}
	else {
		switch ( $e->getCode() ) {
			case 404:
			case 500:
				$error_uri = new SiTech_Uri( '/error/e' . $e->getCode() );
#				SiTech_Loader::loadBootstrap( SITECH_APP_PATH . '/bootstrap.test.php' );
				Treat_Controller_Abstract::setOriginalUri( $uri );
				SiTech_Controller::dispatch( $error_uri );
		}
	}
}

