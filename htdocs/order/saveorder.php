<?
function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      elseif ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      elseif ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '1')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='31')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;
$leap = date("L");

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   
   elseif ($leap==1&&$month=='03')
   {
      $day=$day+29;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+3);
        if ($diff > 0 && $diff < '0.01'){$diff="0.01";}
        elseif($diff < 0 && $diff > '-0.01'){$diff="-0.01";}
        return $diff;
}

include("../ta/db.php");

$user=$_COOKIE["usercook"];
$pass=$_COOKIE["passcook"];
$cur_user=$_COOKIE["cur_user"];

if($user==""||$pass==""){
   $user=$_POST['username'];
   $pass=$_POST['password'];
}

$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$date=$today;
$showtoday = date("F j, Y, g:i a");
$hour = date("H");

$date=nextday($date);$date=nextday($date);
if ($hour>=10){$date=nextday($date);}
if(dayofweek($date)=="Saturday"||dayofweek($date)=="Sunday"){$date=nextday($date);$date=nextday($date);}

$date=$_POST['date'];

mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM login_route WHERE username = '$user' AND password = '$pass'";
$result = mysql_query($query);
$num=mysql_numrows($result);
//mysql_close();

if ($num!=1) 
{
    echo "<head><META HTTP-EQUIV='refresh' CONTENT='5;URL=index.html'></head>";
    echo "<body><center><h4>Failed</h4></center></body>";
}

elseif ($day > 28 && $newmonth == 2 && $leap == 0){echo "<center>Date does not exist. Use your back button.</center>";}
elseif ($day > 29 && $newmonth == 2 and $leap == 1){echo "<center>Date does not exist. Use your back button.</center>";}
elseif ($day > 30 && ($newmonth == 4 || $newmonth == 6 || $newmonth == 9 || $newmonth == 11)){echo "<center>Date does not exist. Use your back button.</center>";}
elseif ($day > 31){echo "<center>Date does not exist. Use your back button.</center>";}  

else
{
   echo "<head><META HTTP-EQUIV='refresh' CONTENT='4;URL=confirmed.php'></head>";
   echo "<body onload='window.print();'>";
   
   $caterbus=mysql_result($result,0,"businessid");
   $companyid=mysql_result($result,0,"companyid");
   $firstname=mysql_result($result,0,"firstname");
   $lastname=mysql_result($result,0,"lastname");
   $route=mysql_result($result,0,"route");
   $locationid=mysql_result($result,0,"locationid");
   $login_routeid=mysql_result($result,0,"login_routeid");

   if ($cur_user!=""){
      //mysql_connect($dbhost,$username,$password);
      //@mysql_select_db($database) or die( "Unable to select database");
      $query = "SELECT * FROM login_route WHERE login_routeid = '$cur_user'";
      $result = mysql_query($query);
      $num=mysql_numrows($result);
      //mysql_close();

      $caterbus=mysql_result($result,0,"businessid");
      $companyid=mysql_result($result,0,"companyid");
      $firstname=mysql_result($result,0,"firstname");
      $lastname=mysql_result($result,0,"lastname");
      $route=mysql_result($result,0,"route");
      $locationid=mysql_result($result,0,"locationid");
      $login_routeid=mysql_result($result,0,"login_routeid");
   }

   //mysql_connect($dbhost,$username,$password);
   //@mysql_select_db($database) or die( "Unable to select database");
   $query = "INSERT INTO audit_vend (login_routeid,user,date) VALUES ('$login_routeid','$user','$date')";
   $result = mysql_query($query);
   //mysql_close();

   $printdayname=dayofweek($date);
   echo "<table width=100%><tr><td><font size=2><font size=2>Order For: <u><b>$printdayname $date - $firstname $lastname, Route#$route</b></u></font></td><td align=right><font size=2>Printed: $showtoday</td></tr></table>";

   //mysql_connect($dbhost,$username,$password);
   //@mysql_select_db($database) or die( "Unable to select database");
   $query = "SELECT default_menu FROM business WHERE businessid = '$caterbus'";
   $result = mysql_query($query);
   //mysql_close();

   $curmenu=mysql_result($result,0,"default_menu");

   //mysql_connect($dbhost,$username,$password);
   //@mysql_select_db($database) or die( "Unable to select database");
   $query = "SELECT * FROM vend_locations WHERE locationid = '$locationid'";
   $result = mysql_query($query);
   //mysql_close();

   $location_name=mysql_result($result,0,"location_name");

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM menu_type WHERE menu_typeid = '$curmenu'";
    $result = mysql_query($query);
    //mysql_close();

    $menuname=mysql_result($result,0,"menu_typename");
    $dayname=dayofweek($date);

    $thismonth=substr($date,5,2);
    $nextday=nextday($date);
    $nextmonth=substr($nextday,5,2);
    $prevday=prevday($date);
    $prevmonth=substr($prevday,5,2);

/////////////////////////////////////////////ADD ITEMS

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM menu_groups WHERE menu_typeid = '$curmenu'";
    $result = mysql_query($query);
    $num=mysql_numrows($result);
    //mysql_close();

    $countme=1;

    $num--;
    while($num>=0){

       $groupname=mysql_result($result,$num,"groupname");
       $groupid=mysql_result($result,$num,"menu_group_id");

       echo "<center><table width=100%><tr valign=top><td width=10%><b><font size=2>$groupname</b></td><td width=90%><table width=100% border=1 cellpadding=0 cellspacing=0>";

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM menu_pricegroup WHERE menu_groupid = '$groupid' ORDER BY orderid DESC";
       $result2 = mysql_query($query2);
       $num2=mysql_numrows($result2);
       //mysql_close();

       $num2--;
       while($num2>=0){

          $pricegroupname=mysql_result($result2,$num2,"menu_pricegroupname");
          $pricegroupid=mysql_result($result2,$num2,"menu_pricegroupid");

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query3 = "SELECT * FROM vend_item WHERE groupid = '$pricegroupid' AND date = '$date' AND businessid = '$caterbus' ORDER BY vend_itemid DESC";
          $result3 = mysql_query($query3);
          $num3=mysql_numrows($result3);
          //mysql_close();

          if ($num3==0){echo "<tr><td width=15%><font size=1><b>$pricegroupname</td><td colspan=3><i><font size=1>&nbspNo Items</i></font></td></tr>";}

          $num3--;
          $firstnum=$num3;

          while($num3>=0){

             if ($firstnum==$num3){echo "<tr><td width=15%><font size=1><b>$pricegroupname</b></font></td>";}
             else{echo "<tr><td></td>";}

             $menu_itemid=mysql_result($result3,$num3,"menu_itemid");
             $vend_itemid=mysql_result($result3,$num3,"vend_itemid");

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query4 = "SELECT item_name,price,unit FROM menu_items WHERE menu_item_id = '$menu_itemid'";
             $result4 = mysql_query($query4);
             //mysql_close();
 
             $menu_itemname=mysql_result($result4,0,"item_name");
             $menu_itemprice=mysql_result($result4,0,"price");
             $menu_itemunit=mysql_result($result4,0,"unit");
             $menu_itemprice=money($menu_itemprice);

             $varname="amt$countme";

             $amount=$_POST[$varname];

                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query4 = "SELECT * FROM vend_order WHERE date = '$date' AND vend_itemid = '$vend_itemid' AND login_routeid = '$login_routeid'";
                $result4 = mysql_query($query4);
                $num4=mysql_numrows($result4);
                //mysql_close();

                if ($num4!=0){
                   //mysql_connect($dbhost,$username,$password);
                   //@mysql_select_db($database) or die( "Unable to select database");
                   $query5 = "UPDATE vend_order SET amount = '$amount', shipped = '$amount', received = '$amount', locationid = '$locationid' WHERE date = '$date' AND vend_itemid = '$vend_itemid' AND login_routeid = '$login_routeid'";
                   $result5 = mysql_query($query5);
                   //mysql_close();
                }
                elseif ($amount!=0&&$amount!=""){
                   //mysql_connect($dbhost,$username,$password);
                   //@mysql_select_db($database) or die( "Unable to select database");
                   $query5 = "INSERT INTO vend_order (businessid,amount,shipped,received,date,login_routeid,vend_itemid,locationid) VALUES ('$caterbus','$amount','$amount','$amount','$date','$login_routeid','$vend_itemid','$locationid')";
                   $result5 = mysql_query($query5);
                   //mysql_close();
                }

             echo "<td width=10% align=right><font size=1>$amount</td><td><font size=1>$menu_itemunit </td><td width=60%><font size=1>$menu_itemname</td></tr>";
             
             $num3--;
             $countme++;
          }

          $num2--;
       }
       echo "</table></td></tr></table></center><p>";
       $num--;
    }
}
mysql_close();
echo "</body>";
?>