<?
function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      elseif ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      elseif ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '1')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='31')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;
$leap = date("L");

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   
   elseif ($leap==1&&$month=='03')
   {
      $day=$day+29;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+3);
        if ($diff > 0 && $diff < '0.01'){$diff="0.01";}
        elseif($diff < 0 && $diff > '-0.01'){$diff="-0.01";}
        return $diff;
}

include("../ta/db.php");

$user=$_POST['username'];
$pass=$_POST['password'];
$message=$_POST['message'];

$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$date=$today;
$showtoday = date("F j, Y, g:i a");
$hour = date("H");


mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM login_route WHERE username = '$user' AND password = '$pass'";
$result = mysql_query($query);
$num=mysql_numrows($result);
mysql_close();

if ($num!=1) 
{
    echo "<head><META HTTP-EQUIV='refresh' CONTENT='5;URL=index.html'></head>";
    echo "<body><center><h4>Failed</h4></center></body>";
}

elseif ($day > 28 && $newmonth == 2 && $leap == 0){echo "<center>Date does not exist. Use your back button.</center>";}
elseif ($day > 29 && $newmonth == 2 and $leap == 1){echo "<center>Date does not exist. Use your back button.</center>";}
elseif ($day > 30 && ($newmonth == 4 || $newmonth == 6 || $newmonth == 9 || $newmonth == 11)){echo "<center>Date does not exist. Use your back button.</center>";}
elseif ($day > 31){echo "<center>Date does not exist. Use your back button.</center>";}  

else
{
   setcookie("usercook",$user);
   setcookie("passcook",$pass);

   $location="orderdate2.php";
   header('Location: ./' . $location);
   
   $caterbus=mysql_result($result,0,"businessid");
   $companyid=mysql_result($result,0,"companyid");
   $firstname=mysql_result($result,0,"firstname");
   $lastname=mysql_result($result,0,"lastname");
   $route=mysql_result($result,0,"route");
   $locationid=mysql_result($result,0,"locationid");
   $login_routeid=mysql_result($result,0,"login_routeid");
   $sec_level=mysql_result($result,0,"sec_level");

   mysql_connect($dbhost,$username,$password);
   @mysql_select_db($database) or die( "Unable to select database");
   if($sec_level==2){$query = "SELECT * FROM login_route WHERE locationid = '$locationid' AND businessid = '$caterbus'";}
   elseif($sec_level==3){$query = "SELECT * FROM login_route WHERE businessid = '$caterbus'";}
   $result = mysql_query($query);
   $num=mysql_numrows($result);
   mysql_close();

   $num--;
   while ($num>=0){
      $sel_login_routeid=mysql_result($result,$num,"login_routeid");

      if ($message!=""){

         $newmessage="$message --$firstname $lastname"; 

         mysql_connect($dbhost,$username,$password);
         @mysql_select_db($database) or die( "Unable to select database");
         $query3 = "INSERT INTO vend_bulletin (login_routeid,date,message) VALUES ('$sel_login_routeid','$date','$newmessage')";
         $result3 = mysql_query($query3);
         mysql_close();
      }

      $num--;
   }
}
?>