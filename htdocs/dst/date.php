<?php

//Returns todays date +/- X
function datefunc($numdays)
{
$dates = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d")+$numdays, date("Y")));
return $dates;

}


function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}

$dayname=dayofweek("2007-06-01");

echo "$dayname";

?>