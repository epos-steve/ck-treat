<?php

function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+3);
        if ($diff > 0 && $diff < '0.01'){$diff="0.01";}
        elseif($diff < 0 && $diff > '-0.01'){$diff="-0.01";}
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      elseif ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      elseif ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='32')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      $day=$day-31;  
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";} 
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}
include("db.php");
$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");
$bbdayname=date("l");
$bbmonth=date("F");
$bbday=date("j");
$startclose="$year-$month-01";
$month++;
if ($month<10){$month="0$month";}
if ($month==13){$month="01";$year++;}
$endclose="$year-$month-31";

$book=$_GET['book'];
$date7=$_GET['date'];

$reserveid=$_GET['reserveid'];

$user=$_COOKIE["usercook"];
$pass=$_COOKIE["passcook"];
$accountid=$_COOKIE["accountid"];
$businessid=$_COOKIE["businessid"];
$curmenu=$_COOKIE["curmenu"];
if($reserveid==""){$reserveid=$_COOKIE["reserveid"];}

if ($accountid<1){
    $location="logout.php?goto=3";
    header('Location: ./' . $location);
}
elseif ($businessid<1){
    $location="index.php";
    header('Location: ./' . $location);
}
else
{
?>
<head>
<SCRIPT LANGUAGE="JavaScript" SRC="CalendarPopup.js"></SCRIPT>

<!-- This prints out the default stylehseets used by the DIV style calendar.
     Only needed if you are using the DIV style popup -->
<SCRIPT LANGUAGE="JavaScript">document.write(getCalendarStyles());</SCRIPT>

<STYLE>
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation
			{
			background-color:#6677DD;
			text-align:center;
			vertical-align:center;
			text-decoration:none;
			color:#FFFFFF;
			font-weight:bold;
			}
	.TESTcpDayColumnHeader,
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation,
	.TESTcpCurrentMonthDate,
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDate,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDate,
	.TESTcpCurrentDateDisabled,
	.TESTcpTodayText,
	.TESTcpTodayTextDisabled,
	.TESTcpText
			{
			font-family:arial;
			font-size:8pt;
			}
	TD.TESTcpDayColumnHeader
			{
			text-align:right;
			border:solid thin #6677DD;
			border-width:0 0 1 0;
			}
	.TESTcpCurrentMonthDate,
	.TESTcpOtherMonthDate,
	.TESTcpCurrentDate
			{
			text-align:right;
			text-decoration:none;
			}
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDateDisabled
			{
			color:#D0D0D0;
			text-align:right;
			text-decoration:line-through;
			}
	.TESTcpCurrentMonthDate
			{
			color:#6677DD;
			font-weight:bold;
			}
	.TESTcpCurrentDate
			{
			color: #FFFFFF;
			font-weight:bold;
			}
	.TESTcpOtherMonthDate
			{
			color:#808080;
			}
	TD.TESTcpCurrentDate
			{
			color:#FFFFFF;
			background-color: #6677DD;
			border-width:1;
			border:solid thin #000000;
			}
	TD.TESTcpCurrentDateDisabled
			{
			border-width:1;
			border:solid thin #FFAAAA;
			}
	TD.TESTcpTodayText,
	TD.TESTcpTodayTextDisabled
			{
			border:solid thin #6677DD;
			border-width:1 0 0 0;
			}
	A.TESTcpTodayText,
	SPAN.TESTcpTodayTextDisabled
			{
			height:20px;
			}
	A.TESTcpTodayText
			{
			color:#6677DD;
			font-weight:bold;
			}
	SPAN.TESTcpTodayTextDisabled
			{
			color:#D0D0D0;
			}
	.TESTcpBorder
			{
			border:solid thin #6677DD;
			}
</STYLE>
</head>
<?php
    echo "<body background=backdrop.jpg>";
    echo "<div style=border:50px solid red;padding:10px;>";
    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%><tr><td colspan=4><img src=logo.jpg width=205 height=43><p></td></tr>";

    mysql_connect($dbhost,$username,$password);
    @mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM company WHERE companyid = '1'";
    $result = mysql_query($query);
    mysql_close();

    $companyname=mysql_result($result,0,"companyname");

    mysql_connect($dbhost,$username,$password);
    @mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM accounts WHERE accountid = '$accountid'";
    $result = mysql_query($query);
    mysql_close();

    $accountname=mysql_result($result,0,"name");
    $accountnum=mysql_result($result,0,"accountnum");
    $mycostcenter=mysql_result($result,0,"costcenter");

    echo "<tr bgcolor=black><td height=1 colspan=4></td></tr>";
    echo "<tr bgcolor=#CCCCFF><td width=10%><img src=weblogo.jpg height=75 width=80></td><td style='filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr=#FFFFFF, startColorstr=#CCCCFF, gradientType=1);'><font size=4><b>$companyname</b></font><br><b>Account: $accountname<br>Account #: $accountnum</td><td colspan=2 align=right valign=top style='filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr=#CCCCFF, startColorstr=#FFFFFF, gradientType=1);'><b>$bbdayname, $bbmonth $bbday, $year</b><br><font size=2>[<a style=$style href=logout.php><b><font color=blue>Sign Off</font></b></a>]</font><p>$showcart</td></tr>";
    echo "<tr bgcolor=black><td height=1 colspan=4></td></tr>";
    echo "</table></center>";

//////////////////ORDER DETAILS

    mysql_connect($dbhost,$username,$password);
    @mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM business WHERE businessid = '$businessid'";
    $result = mysql_query($query);
    mysql_close();

    $caterdays=mysql_result($result,0,"caterdays");
    $weekend=mysql_result($result,0,"weekend");

    mysql_connect($dbhost,$username,$password);
    @mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM caterclose WHERE businessid = '$businessid' AND date >= '$startclose' AND date <= '$endclose'";
    $result = mysql_query($query);
    $num=mysql_numrows($result);
    mysql_close();
    
    $num--;
    $closedmessage="";
    while ($num>=0){
       $dateclose=mysql_result($result,$num,"date");
       $closedmessage="$closedmessage cal18.addDisabledDates('$dateclose');";
       $num--;
    }

    echo "<p><br><center><table width=90%  cellpadding=0 cellspacing=0 style='filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr=#F7F7F7, startColorstr=#E8E7E7, gradientType=1);'>";
    echo "<tr><td width=1% height=1%><img src='dgrayur.gif' width=20 length=20></td><td colspan=2></td><td width=1% height=1%><img src=lgrayur.gif height=20 width=20></td></tr>";
    echo "<tr><td></td><td colspan=2><img src=details.gif height=36 width=122></td><td></td></tr>";
    if ($book==1){echo "<tr><td></td><td colspan=2><b><font color=red>That room is already booked.</td><td></td></tr>";} 
    elseif ($book==2){echo "<tr><td></td><td colspan=2><b><font color=red>Please specify the number of people.</td><td></td></tr>";} 
    elseif ($book==3){echo "<tr><td></td><td colspan=2><b><font color=red>Your End Time is before your Start Time.</td><td></td></tr>";} 
    elseif ($book==4){echo "<tr><td></td><td colspan=2><b><font color=red>That date has already past.</td><td></td></tr>";}   
    elseif ($book==6){echo "<tr><td></td><td colspan=2><b><font color=blue>Catering Successfully Scheduled.</td><td></td></tr>";}
    elseif ($book==7){echo "<tr><td></td><td colspan=2><b><font color=red>Please enter a Reference.</td><td></td></tr>";} 
    elseif ($book==8){echo "<tr><td></td><td colspan=2><b><font color=red>That Day is already Full.</td><td></td></tr>";}
    elseif ($book==9){echo "<tr><td></td><td colspan=2><b><font color=red>You must schedule at least $caterdays day(s) in advance.</td><td></td></tr>";} 
    elseif ($book==10){echo "<tr><td></td><td colspan=2><b><font color=red>Sorry, we are closed weekends.</td><td></td></tr>";}  
    elseif ($book==11){echo "<tr><td></td><td colspan=2><b><font color=red>Sorry, we are closed that day.</td><td></td></tr>";}    
    elseif ($book==5){echo "<tr><td></td><td colspan=2><b><font color=red>Please specify why location is N/A.</td><td></td></tr>";}           

    if ($date7!=""){$today=$date7;}
    if ($weekend==0){$showweekend="cal18.setDisabledWeekDays(0,6)";}
    else{$showweekend="";}

    $comment=$_GET['comment'];
    $roomcomment=$_GET['roomcomment'];
    $people=$_GET['people'];
    $start_hour=$_GET['start_hour'];
    $end_hour=$_GET['end_hour'];
    $sel_room=$_GET['roomid'];
    $costcenter=$_GET['costcenter'];
    if ($costcenter==""){$costcenter=$mycostcenter;}

    echo "<tr><td></td><td align=right width=20%><FORM ACTION=updatereserve.php method=post><input type=hidden name=reserveid value=$reserveid>Date: </td><td> <SCRIPT LANGUAGE='JavaScript' ID='js18'> var now = new Date(); var cal18 = new CalendarPopup('testdiv1'); cal18.addDisabledDates(null,formatDate(now,'yyyy-MM-dd')); $closedmessage $showweekend; cal18.setCssPrefix('TEST');</SCRIPT><A HREF=#cater onClick=cal18.select(document.forms[0].date,'anchor18','yyyy-MM-dd'); return false; TITLE=cal18.select(document.forms[0].date,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor18' ID='anchor18'><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A> <INPUT TYPE=text NAME='date' VALUE=$today SIZE=8></td><td></td></tr>"; 
    echo "<tr><td></td><td align=right>Reference: </td><td><INPUT TYPE=text name=comment size=30 value='$comment'></td><td></td></tr>";
    echo "<tr><td></td><td align=right># of People: </td><td><INPUT TYPE=text name=peoplenum size=8 value='$people'></td><td></td></tr>";
    
    mysql_connect($dbhost,$username,$password);
    @mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM rooms WHERE businessid = '$businessid' AND deleted = '0' ORDER BY roomname DESC";
    $result = mysql_query($query);
    $num=mysql_numrows($result);
    mysql_close();
   
    echo "<tr><td></td><td align=right>Location: </td><td><select name=room><option value=0>N/A</option>";
    $num--;
    while ($num>=0){
       $roomname=mysql_result($result,$num,"roomname");
       $roomid=mysql_result($result,$num,"roomid");
       if ($roomid==$sel_room){$selroom="SELECTED";}
       else {$selroom="";}
       echo "<option value=$roomid $selroom>$roomname</option>";
       $num--;
    } 
    echo "</select></td><td></td></tr>"; 

    echo "<tr><td></td><td></td><td><i>*If Location is N/A, Please specify. Include any other details below:<br><textarea name=roomcomment rows=3 cols=60>$roomcomment</textarea></td><td></td></tr>";

    $start_min=substr($start_hour,2,2);
    $end_min=substr($end_hour,2,2);
    $start_hour=substr($start_hour,0,2);
    $end_hour=substr($end_hour,0,2);

    if ($start_hour=="01"){$start1="SELECTED";$startam="SELECTED";}
    elseif ($start_hour=="02"){$start2="SELECTED";$startam="SELECTED";}
    elseif ($start_hour=="03"){$start3="SELECTED";$startam="SELECTED";}
    elseif ($start_hour=="04"){$start4="SELECTED";$startam="SELECTED";}
    elseif ($start_hour=="05"){$start5="SELECTED";$startam="SELECTED";}
    elseif ($start_hour=="06"){$start6="SELECTED";$startam="SELECTED";}
    elseif ($start_hour=="07"){$start7="SELECTED";$startam="SELECTED";}
    elseif ($start_hour=="08"){$start8="SELECTED";$startam="SELECTED";}
    elseif ($start_hour=="09"){$start9="SELECTED";$startam="SELECTED";}
    elseif ($start_hour=="10"){$start10="SELECTED";$startam="SELECTED";}
    elseif ($start_hour=="11"){$start11="SELECTED";$startam="SELECTED";}
    elseif ($start_hour=="12"){$start12="SELECTED";$startpm="SELECTED";}
    elseif ($start_hour=="13"){$start1="SELECTED";$startpm="SELECTED";}
    elseif ($start_hour=="14"){$start2="SELECTED";$startpm="SELECTED";}
    elseif ($start_hour=="15"){$start3="SELECTED";$startpm="SELECTED";}
    elseif ($start_hour=="16"){$start4="SELECTED";$startpm="SELECTED";}
    elseif ($start_hour=="17"){$start5="SELECTED";$startpm="SELECTED";}
    elseif ($start_hour=="18"){$start6="SELECTED";$startpm="SELECTED";}
    elseif ($start_hour=="19"){$start7="SELECTED";$startpm="SELECTED";}
    elseif ($start_hour=="20"){$start8="SELECTED";$startpm="SELECTED";}
    elseif ($start_hour=="21"){$start9="SELECTED";$startpm="SELECTED";}
    elseif ($start_hour=="22"){$start10="SELECTED";$startpm="SELECTED";}
    elseif ($start_hour=="23"){$start11="SELECTED";$startpm="SELECTED";}
    else {$start10="SELECTED";$startam="SELECTED";}

    if ($end_hour=="01"){$end1="SELECTED";$endam="SELECTED";}
    elseif ($end_hour=="02"){$end2="SELECTED";$endam="SELECTED";}
    elseif ($end_hour=="03"){$end3="SELECTED";$endam="SELECTED";}
    elseif ($end_hour=="04"){$end4="SELECTED";$endam="SELECTED";}
    elseif ($end_hour=="05"){$end5="SELECTED";$endam="SELECTED";}
    elseif ($end_hour=="06"){$end6="SELECTED";$endam="SELECTED";}
    elseif ($end_hour=="07"){$end7="SELECTED";$endam="SELECTED";}
    elseif ($end_hour=="08"){$end8="SELECTED";$endam="SELECTED";}
    elseif ($end_hour=="09"){$end9="SELECTED";$endam="SELECTED";}
    elseif ($end_hour=="10"){$end10="SELECTED";$endam="SELECTED";}
    elseif ($end_hour=="11"){$end11="SELECTED";$endam="SELECTED";}
    elseif ($end_hour=="12"){$end12="SELECTED";$endpm="SELECTED";}
    elseif ($end_hour=="13"){$end1="SELECTED";$endpm="SELECTED";}
    elseif ($end_hour=="14"){$end2="SELECTED";$endpm="SELECTED";}
    elseif ($end_hour=="15"){$end3="SELECTED";$endpm="SELECTED";}
    elseif ($end_hour=="16"){$end4="SELECTED";$endpm="SELECTED";}
    elseif ($end_hour=="17"){$end5="SELECTED";$endpm="SELECTED";}
    elseif ($end_hour=="18"){$end6="SELECTED";$endpm="SELECTED";}
    elseif ($end_hour=="19"){$end7="SELECTED";$endpm="SELECTED";}
    elseif ($end_hour=="20"){$end8="SELECTED";$endpm="SELECTED";}
    elseif ($end_hour=="21"){$end9="SELECTED";$endpm="SELECTED";}
    elseif ($end_hour=="22"){$end10="SELECTED";$endpm="SELECTED";}
    elseif ($end_hour=="23"){$end11="SELECTED";$endpm="SELECTED";}
    else {$end11="SELECTED";$endam="SELECTED";}

    if ($start_min=="00"){$startmin1="SELECTED";}
    elseif ($start_min=="05"){$startmin2="SELECTED";}
    elseif ($start_min=="10"){$startmin3="SELECTED";}
    elseif ($start_min=="15"){$startmin4="SELECTED";}
    elseif ($start_min=="20"){$startmin5="SELECTED";}
    elseif ($start_min=="25"){$startmin6="SELECTED";}
    elseif ($start_min=="30"){$startmin7="SELECTED";}
    elseif ($start_min=="35"){$startmin8="SELECTED";}
    elseif ($start_min=="40"){$startmin9="SELECTED";}
    elseif ($start_min=="45"){$startmin10="SELECTED";}
    elseif ($start_min=="50"){$startmin11="SELECTED";}
    elseif ($start_min=="55"){$startmin12="SELECTED";}

    if ($end_min=="00"){$endmin1="SELECTED";}
    elseif ($end_min=="05"){$endmin2="SELECTED";}
    elseif ($end_min=="10"){$endmin3="SELECTED";}
    elseif ($end_min=="15"){$endmin4="SELECTED";}
    elseif ($end_min=="20"){$endmin5="SELECTED";}
    elseif ($end_min=="25"){$endmin6="SELECTED";}
    elseif ($end_min=="30"){$endmin7="SELECTED";}
    elseif ($end_min=="35"){$endmin8="SELECTED";}
    elseif ($end_min=="40"){$endmin9="SELECTED";}
    elseif ($end_min=="45"){$endmin10="SELECTED";}
    elseif ($end_min=="50"){$endmin11="SELECTED";}
    elseif ($end_min=="55"){$endmin12="SELECTED";}

    echo "<tr><td></td><td align=right>Start Time:</td><td><select NAME=start_hour><OPTION VALUE='01' $start1>1</OPTION><OPTION VALUE='02' $start2>2</OPTION><OPTION VALUE='03' $start3>3</OPTION><OPTION VALUE='04' $start4>4</OPTION><OPTION VALUE='05' $start5>5</OPTION><OPTION VALUE='06' $start6>6</OPTION><OPTION VALUE='07' $start7>7</OPTION><OPTION VALUE='08' $start8>8</OPTION><OPTION VALUE='09' $start9>9</OPTION><OPTION VALUE='10' $start10>10</OPTION><OPTION VALUE='11' $start11>11</OPTION><OPTION VALUE='12' $start12>12</OPTION></SELECT><b>:</b><select NAME=start_min><OPTION VALUE='00' $startmin1>00</OPTION><OPTION VALUE='05' $startmin2>05</OPTION><OPTION VALUE='10' $startmin3>10</OPTION><OPTION VALUE='15' $startmin4>15</OPTION><OPTION VALUE='20' $startmin5>20</OPTION><OPTION VALUE='25' $startmin6>25</OPTION><OPTION VALUE='30' $startmin7>30</OPTION><OPTION VALUE='35' $startmin8>35</OPTION><OPTION VALUE='40' $startmin9>40</OPTION><OPTION VALUE='45' $startmin10>45</OPTION><OPTION VALUE='50' $startmin11>50</OPTION><OPTION VALUE='55' $startmin12>55</OPTION></SELECT> <select NAME=start_am><OPTION VALUE='AM' $startam>AM</OPTION><OPTION VALUE='PM' $startpm>PM</OPTION></SELECT></td><td></td></tr>";
    echo "<tr><td></td><td align=right>End Time:</td><td><select NAME=end_hour><OPTION VALUE='01' $end1>1</OPTION><OPTION VALUE='02' $end2>2</OPTION><OPTION VALUE='03' $end3>3</OPTION><OPTION VALUE='04' $end4>4</OPTION><OPTION VALUE='05' $end5>5</OPTION><OPTION VALUE='06' $end6>6</OPTION><OPTION VALUE='07' $end7>7</OPTION><OPTION VALUE='08' $end8>8</OPTION><OPTION VALUE='09' $end9>9</OPTION><OPTION VALUE='10' $end10>10</OPTION><OPTION VALUE='11' $end11>11</OPTION><OPTION VALUE='12' $end12>12</OPTION></SELECT><b>:</b><select NAME=end_min><OPTION VALUE='00' $endmin1>00</OPTION><OPTION VALUE='05' $endmin2>05</OPTION><OPTION VALUE='10' $endmin3>10</OPTION><OPTION VALUE='15' $endmin4>15</OPTION><OPTION VALUE='20' $endmin5>20</OPTION><OPTION VALUE='25' $endmin6>25</OPTION><OPTION VALUE='30' $endmin7>30</OPTION><OPTION VALUE='35' $endmin8>35</OPTION><OPTION VALUE='40' $endmin9>40</OPTION><OPTION VALUE='45' $endmin10>45</OPTION><OPTION VALUE='50' $endmin11>50</OPTION><OPTION VALUE='55' $endmin12>55</OPTION></SELECT> <select NAME=end_am><OPTION VALUE='AM' $endam>AM</OPTION><OPTION VALUE='PM' $endpm>PM</OPTION></SELECT></td><td></td></tr>";
    echo "<tr><td></td><td align=right>Cost Center:</td><td><INPUT TYPE=text name=costcenter size=18 value='$costcenter'></td><td></td></tr>";

    echo "<tr><td></td><td></td><td><br><p><input type=hidden name=comefrom value=1><INPUT TYPE=submit VALUE='Confirm Details'></td><td></td></tr>";
    echo "<tr><td><img src='dgrayll.gif' width=20 length=20></td><td colspan=2></td><td><img src=lgraylr.gif height=20 width=20></td></tr>";
    echo "</table></center>";
    echo "<DIV ID=testdiv1 STYLE=position:absolute;visibility:hidden;background-color:white;layer-background-color:white;></DIV>";
    echo "<p><br><p><br><p><br><p><center><a href=http://essentialpos.com target='_blank'><img src=Logo5.jpg width=150 height=71 border=0></a></center></body>";
}
?>
