<?php

function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+3);
        if ($diff > 0 && $diff < '0.01'){$diff="0.01";}
        elseif($diff < 0 && $diff > '-0.01'){$diff="-0.01";}
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      elseif ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      elseif ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '1')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='31')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;
$leap = date("L");

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   
   elseif ($leap==1&&$month=='03')
   {
      $day=$day+29;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      $day=$day-31;  
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";} 
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}
include("db.php");
$style = "text-decoration:none";

$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";

$reserveid=$_POST['reserveid'];
$accountid=$_COOKIE["accountid"];
$businessid=$_COOKIE["businessid"];
$curmenu=$_COOKIE["curmenu"];
if ($reserveid==""){$reserveid=$_COOKIE["reserveid"];}
$comefrom=$_POST['comefrom'];

mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM customer WHERE username = '$user' AND password = '$pass'";
$result = mysql_query($query);
$num=mysql_numrows($result);
mysql_close();

if ($accountid<1||$reserveid<1||$businessid<1){
    $location="caterdetails.php";
    header('Location: ./' . $location);
}

else
{
    $companyid="1";

    mysql_connect($dbhost,$username,$password);
    @mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM business WHERE businessid = '$businessid'";
    $result = mysql_query($query);
    mysql_close();

    $caterdays=mysql_result($result,0,"caterdays");
    $cater_spread=mysql_result($result,0,"cater_spread");
    $cater_spread--;
    $caternum=mysql_result($result,0,"caternum");
    $weekend=mysql_result($result,0,"weekend");
    $srvchrg=mysql_result($result,0,"srvchrg");
    $srvchrgpcnt=mysql_result($result,0,"srvchrgpcnt");
    if ($srvchrg==1){}else{$srvchrgpcnt=0;}
    $lastday=$today;
    for ($counter=0;$counter<$caterdays;$counter++){$lastday=nextday($lastday);}

    $date=$_POST['date'];
    $peoplenum=$_POST['peoplenum'];
    $roomid=$_POST['room'];
    $start_hour=$_POST['start_hour'];
    $start_min=$_POST['start_min'];
    $end_hour=$_POST['end_hour'];
    $end_min=$_POST['end_min'];
    $start_am=$_POST['start_am'];
    $end_am=$_POST['end_am'];
    $roomcomment=$_POST['roomcomment'];
    $comment=$_POST['comment'];
    $costcenter=$_POST['costcenter'];

    if ($start_am=="AM" && $start_hour==12){$start_hour="00";}
    elseif ($start_am=="PM" && $start_hour!=12){$start_hour=$start_hour+12;}
    if ($end_am=="AM" && $end_hour==12){$end_hour="00";}
    elseif ($end_am=="PM" && $end_hour!=12){$end_hour=$end_hour+12;}

    $start_hour2="$start_hour$start_min";
    $end_hour2="$end_hour$end_min";

    $start_min=$start_min-$cater_spread;
    if($start_min<0){$start_hour--;$start_min=60+$start_min;}
    if($start_hour<10){$start_hour="0$start_hour";}
    if($start_min<10){$start_min="0$start_min";}

    $end_min=$end_min+$cater_spread;
    if($end_min>=60){$end_hour++;$end_min=$end_min-60;}
    if($end_hour<10){$end_hour="0$end_hour";}
    if($end_min<10){$end_min="0$end_min";}

    $start_hour="$start_hour$start_min";
    $end_hour="$end_hour$end_min";

    mysql_connect($dbhost,$username,$password);
    @mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM caterclose WHERE businessid = '$businessid' AND date = '$date'";
    $result = mysql_query($query);
    $num5=mysql_numrows($result);
    mysql_close();

    mysql_connect($dbhost,$username,$password);
    @mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM reserve WHERE businessid = '$businessid' AND date = '$date'";
    $result = mysql_query($query);
    $num=mysql_numrows($result);
    mysql_close();
    $caterjobs=$num;

    mysql_connect($dbhost,$username,$password);
    @mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM reserve WHERE reserveid != '$reserveid' AND roomid = '$roomid' AND roomid != '0' AND date = '$date' AND ((start_hour <= '$start_hour' AND end_hour >= '$start_hour') OR (end_hour >= '$end_hour' AND start_hour <= '$end_hour') OR (start_hour >= '$start_hour' AND end_hour <= '$end_hour'))";
    $result = mysql_query($query);
    $num=mysql_numrows($result);
    mysql_close();

    $dayname=dayofweek($date);

    if ($num!=0){echo "<p><br><p><br><p><br><center><font color=red><b>Room already Booked.</b></font><p><i>Please use your browser's back button to return</center>";}
    elseif ($peoplenum==0 || $peoplenum==""){echo "<p><br><p><br><p><br><center><font color=red><b>Please enter number of guests.</b></font><p><i>Please use your browser's back button to return</center>";}
    elseif ($end_hour<$start_hour){echo "<p><br><p><br><p><br><center><font color=red><b>Time is Off.</b></font><p><i>Please use your browser's back button to return</center>";}
    elseif ($today>$date || $date<$lastday){echo "<p><br><p><br><p><br><center><font color=red><b>Not a valid Date.</b></font><p><i>Please use your browser's back button to return</center>";}
    elseif ($roomid==0&&$roomcomment==""){echo "<p><br><p><br><p><br><center><font color=red><b>Please specify why location is N/A.</b></font><p><i>Please use your browser's back button to return</center>";}
    elseif ($comment==""){echo "<p><br><p><br><p><br><center><font color=red><b>Please enter a reference.</b></font><p><i>Please use your browser's back button to return</center>";}
    elseif ($caterjobs>=$caternum){echo "<p><br><p><br><p><br><center><font color=red><b>That Day is already Full.</b></font><p><i>Please use your browser's back button to return</center>";}
    elseif (($dayname=="Saturday"||$dayname=="Sunday")&&$weekend==0){echo "<p><br><p><br><p><br><center><font color=red><b>Sorry, we are closed weekends.</b></font><p><i>Please use your browser's back button to return</center>";}
    elseif ($num5!=0){echo "<p><br><p><br><p><br><center><font color=red><b>Sorry, we are closed that day.</b></font><p><i>Please use your browser's back button to return</center>";}
    else{
       mysql_connect($dbhost,$username,$password);
       @mysql_select_db($database) or die( "Unable to select database");
       $query="UPDATE reserve SET date = '$date',start_hour = '$start_hour2',end_hour = '$end_hour2',peoplenum = '$peoplenum',roomid = '$roomid',roomcomment = '$roomcomment',comment = '$comment',status = '1', costcenter = '$costcenter', service = '$srvchrgpcnt' WHERE reserveid = '$reserveid'";
       $result = mysql_query($query);
       mysql_close();

       $location="caterdetails.php?reserveid=$reserveid#detail";
       if ($comefrom==1){$location="caterdetails.php?reserveid=$reserveid#detail";}
       header('Location: ./' . $location); 
    }
}
?>
