<?php

mb_internal_encoding( 'UTF-8' );
mb_http_output( 'UTF-8' );
ob_start( 'mb_output_handler' );

if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
include_once( DOC_ROOT . '/ta/db.php' );
require_once( DOC_ROOT . '/bootstrap.php' );

#mysql_connect($dbhost, $username, $password);
#mysql_select_db($database);

if ( $_POST ) {
	$p = new Treat_ArrayObject( $_POST );
}
elseif ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
	$p = new Treat_ArrayObject( $_GET );
}
$m = $p['method'];
$row = $p['row'];
$table = $p['table'];
$id = preg_replace( '@^id[_-]@', '', (string)$p['id'] );
$job = $p['jobtype'];

switch ( $m ) {
	case 'btEmailLink':
		//<editor-fold defaultstate="collapsed" desc="btEmailLink">
		header( 'Content-type: text/javascript' );
		
		$h = $p['h'];
		$exp = explode('?', $h);
		$q = $exp[1];
		$exp = explode('&', $q);
		
		$a = array();
		foreach($exp as $e){
			$x = explode('=', $e);
			$a[$x[0]] = $x[1];
		}
		
		$bid = $a['bid'];
		
		$q = Treat_DB_ProxyOld::query("SELECT email as e FROM login WHERE businessid = '$bid' AND security_level > '0' AND email != '' AND email IS NOT NULL AND is_deleted = '0'");
		$e = array();
		while($r = mysql_fetch_array($q)){
			$e[count($e)] = $r['e'];
		}
		$e_str = implode(',', $e);
		
		if(strlen($e_str) > 0){
			echo "$('mailStatus').update('Opening your Mail Client...'); ";
			echo "window.location.href = 'mailto: $e_str'; ";
		}
		else{
			echo "$('mailStatus').style.background = '#ff0'; ";
			echo "$('mailStatus').update('There are no emails for this unit in our Database.'); ";
		}
		echo "setTimeout(function(){ $('mailStatus').hide(); }, 2000); ";
		//</editor-fold>
	break;
	
	
	case 'updatePunchTime':
		//<editor-fold defaultstate="collapsed" desc="updatePunchTime">
		header( 'Content-type: text/javascript' );
		$r = mysql_fetch_array(Treat_DB_ProxyOld::query("SELECT TIMEDIFF(clockout, clockin) AS diff FROM labor_clockin WHERE clockinid = '$id'")) or die(mysql_error());
		
		/* update who edited the punch */
		$who = $_COOKIE['logincook'];
		Treat_DB_ProxyOld::query("UPDATE labor_clockin SET editby = '$who', edittime = NOW() WHERE clockinid = '$id'");
		
		$diff = $r['diff'];
		
		echo "var row = $('$id'); ";
		echo " var cell = $(row.cells[row.cells.length - 1]); ";
		echo "cell.update('$diff'); ";
		echo "cell.highlight({endcolor: '#e8e7e7'}); ";
		
		// Total
		$job = $p['job'];
		$eid = $p['eid'];
		$to = $p['to']." 00:00:00";
		$from = $p['from']." 00:00:00";
		$q = Treat_DB_ProxyOld::query("SELECT TIMEDIFF(clockout, clockin) AS diff FROM labor_clockin WHERE clockin > '$from' AND loginid = '$eid' AND jobtype = '$job' AND is_deleted = '0'");
		$hours = 0;
		while($r = mysql_fetch_array($q)){
			$diff = $r['diff'];
			$exp = explode(':', $diff);
			$h = $exp[0]*60;
			$m = $exp[1];
			$hours += $h + $m;
		}
		$hours = number_format($hours/60, 2);
		echo "$('et_$eid:$job').update('$hours'); ";
		echo "$('et_$eid:$job').highlight({endcolor: '#cccccc'}); ";
		//</editor-fold>
	break;
	
	
	case 'changeSize':
		//<editor-fold defaultstate="collapsed" desc="changeSize">
		$rid = preg_replace( '@^id[_-]@', '', (string)$p['recipeid'] );
		$val = $p['val'];
		$q = Treat_DB_ProxyOld::query("SELECT * FROM recipe WHERE recipeid = '$rid'") or die(mysql_error());
		$r = mysql_fetch_array($q) or die(mysql_error());
		$iid = $r['inv_itemid'];
		$ir = mysql_fetch_array(Treat_DB_ProxyOld::query("SELECT * FROM inv_items WHERE inv_itemid = '$iid'")) or die(mysql_error());
		$s1 = $ir['rec_size'];
		$s2 = $ir['rec_size2'];
		if($val == $s1){
			echo "Size 1";
			Treat_DB_ProxyOld::query("UPDATE recipe SET rec_order = '1' WHERE recipeid = '$rid'") or die(mysql_error());
		}
		else{
			echo "Size 2";
			Treat_DB_ProxyOld::query("UPDATE recipe SET rec_order = '2' WHERE recipeid = '$rid'") or die(mysql_error());
		}
		//</editor-fold>
	break;
	
	
	case 'sizeSwap':
		//<editor-fold defaultstate="collapsed" desc="sizeSwap">
		header( 'Content-type: text/javascript' );
		$rid = preg_replace( '@^id[_-]@', '', (string)$p['recipeid'] );
		
		$ir = Treat_Model_Nutrition_RecipeItem::getListForAjaxReqSizeSwap( $rid );
		
		$returnVals = array();
#		echo "var returnVals = [];\n";
		if ( $ir->rec_size ) {
			$returnVals[] = array(
				'val'  => $ir->rec_size,
				'disp' => Treat_Model_Nutrition_Sizes::getCachedSize( $ir->rec_size ),
			);
#			echo "returnVals[returnVals.length] = {val: $s, disp: '$sn'};\n";
		}
		if ( $ir->rec_size2 ) {
			$returnVals[] = array(
				'val'  => $ir->rec_size2,
				'disp' => Treat_Model_Nutrition_Sizes::getCachedSize( $ir->rec_size2 ),
			);
#			echo "returnVals[returnVals.length] = {val: $s2, disp: '$sn'};\n";
		}
		echo 'var returnVals = ', json_encode( $returnVals ), ';', PHP_EOL;
		//</editor-fold>
	break;
	
	
	case 'whichSize':
		//<editor-fold defaultstate="collapsed" desc="whichSize">
		header( 'Content-type: text/javascript' );
		$inv_id = preg_replace( '@^id[_-]@', '', (string)$p['inv_id'] );
		$recipe_item = Treat_Model_Nutrition_RecipeItem::getListForAjaxReqSizeSwapByInvItemId( $inv_id );
		
		$name1 = Treat_Model_Nutrition_Sizes::getCachedSize( $recipe_item->rec_size );
		
		echo "var returnVals = [];\n";
		echo "returnVals[returnVals.length] = '$name1';\n";
		if ( $recipe_item->rec_size > 0 ) {
			$name2 = Treat_Model_Nutrition_Sizes::getCachedSize( $recipe_item->rec_size2 );
			echo "returnVals[returnVals.length] = '$name2';\n";
		}
		//</editor-fold>
	break;
	
	
	case 'updateCost':
		//<editor-fold defaultstate="collapsed" desc="updateCost">
		header( 'Content-type: text/javascript' );
#		echo '/*', PHP_EOL;
#		var_dump( $p );
#		echo '*/', PHP_EOL;
		$editid = preg_replace( '@^id[_-]@', '', (string)$p['editid'] );
		$editedRecipeId = preg_replace( '@^id[_-]@', '', (string)$p['recipeid'] );
		
		require_once( dirname( DOC_ROOT ) . '/lib/inc/function.calc_recipe.php' );
		$page_business = Treat_Model_Business_Singleton::getSingleton();
		$menu_item = Treat_Model_Menu_Item::getMenuItemById( $editid );
		$calc_recipe = calc_recipe(
			$editid
			,$menu_item->serving_size
			,$menu_item->serving_size
			,false
			,$page_business->use_nutrition
		);
		foreach ( $calc_recipe['items'] as $item_obj ) {
			if ( $item_obj['recipeid'] == $editedRecipeId ) {
				$item = $item_obj;
				break;
			}
		}
		$item_price_calced = $menu_item->price * $menu_item->serving_size;
		$profit = $item_price_calced - $calc_recipe['totalprice'];
		if ( $item_price_calced ) {
			$fcprcnt = round( $calc_recipe['totalprice'] / $item_price_calced * 100, 1 );
		}
		else {
			$fcprcnt = 0;
		}
		if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
#			echo '/*', PHP_EOL;
#			echo '$editid = ';
#			var_dump( $editid );
#			echo '$editedRecipeId = ';
#			var_dump( $editedRecipeId );
#			echo '$calc_recipe[\'items\'] = ';
#			var_dump( $calc_recipe['items'] );
#			echo '*/', PHP_EOL;
		}
		$returnVals = array(
			'rowUpdate' => number_format( $item['newprice'], 2 ),
			'cost'      => number_format( $calc_recipe['totalprice'], 2 ),
			'price'     => number_format( $item_price_calced, 2 ),
			'profit'    => number_format( $profit, 2 ),
			'fc'        => number_format( $fcprcnt, 1 ),
			'servings'  => $menu_item->serving_size,
		);
		echo 'var returnVals = ', json_encode( $returnVals ), ';', PHP_EOL;
		/// Divide above by $serving_size to get per serving
		//</editor-fold>
	break;
	
	
	case 'updateEdited':
		//<editor-fold defaultstate="collapsed" desc="updateEdited">
		$user = $_COOKIE['usercook'];
		$date = date('m/d/y');
		$d = date('Y-m-d');
		$str = "$user $date";
		
		echo "\$I($('$table').rows[$row].cells[$('$table').rows[$row].cells.length-2], '$str');\n ";
		Treat_DB_ProxyOld::query("UPDATE nutrition SET date = '$d', user = '$user' WHERE item_code = '$id'");
		//</editor-fold>
	break;
	
	
	case 'buildSizeSelect':
		//<editor-fold defaultstate="collapsed" desc="buildSizeSelect">
		header( 'Content-type: text/javascript' );
		echo "sizeSelect = \$C('select');\n";
		
		$q = Treat_DB_ProxyOld::query("SELECT * FROM size");
		while($r = mysql_fetch_array($q)){
			$sizeid = $r['sizeid'];
			$disp = $r['sizename'];
			echo "var opt = \$C('option');\n";
			echo "opt.value = '$sizeid';\n";
			echo "\$I(opt, '$disp');\n";
			echo "\$A(opt, sizeSelect);\n";
		}
		
		//</editor-fold>
	break;
	
}

//@mysql_close();

function money($diff){
	$findme='.';
	$double='.00';
	$single='0';
	$double2='00';
	$pos1 = strpos($diff, $findme);
	$pos2 = strlen($diff);
	if ($pos1==""){$diff="$diff$double";}
	elseif ($pos2-$pos1==2){$diff="$diff$single";}
	elseif ($pos2-$pos1==1){$diff="$diff$double2";}
	else{}
	$dot = strpos($diff, $findme);
	$diff = substr($diff, 0, $dot+4);
	$diff=round($diff, 2);
	if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
	elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
	$diff = substr($diff, 0, $dot+3);
	$pos1 = strpos($diff, $findme);
	$pos2 = strlen($diff);
	if ($pos1==""){$diff="$diff$double";}
	elseif ($pos2-$pos1==2){$diff="$diff$single";}
	elseif ($pos2-$pos1==1){$diff="$diff$double2";}
	else{}
	return $diff;
}

?>
