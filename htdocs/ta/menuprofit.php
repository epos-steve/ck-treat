<?php

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+4);
        $diff=round($diff, 2);
        if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
        elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
        $diff = substr($diff, 0, $dot+3);
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        return $diff;
}

function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}

function batch_recipe($editid){

    $query = "SELECT * FROM recipe WHERE menu_itemid = '-$editid' ORDER BY recipeid DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=mysql_numrows($result);

    $totalprice=0;
    $totalserv=0;
    $counter=1;
    $num--;
    while ($num>=0){
       $recipeid=mysql_result($result,$num,"recipeid");
       $inv_itemid=mysql_result($result,$num,"inv_itemid");
       $rec_num=mysql_result($result,$num,"rec_num");
       $rec_size=mysql_result($result,$num,"rec_size");
       $srv_num=mysql_result($result,$num,"srv_num");
       $rec_order=mysql_result($result,$num,"rec_order");

       $query2 = "SELECT * FROM inv_items WHERE inv_itemid = '$inv_itemid'";
       $result2 = Treat_DB_ProxyOld::query($query2);

       $supc=mysql_result($result2,0,"item_code");
       $inv_itemname=mysql_result($result2,0,"item_name");
       $order_size=mysql_result($result2,0,"order_size");
       $price=mysql_result($result2,0,"price");
       if ($rec_order==1){$inv_rec_num=mysql_result($result2,0,"rec_num");}
       elseif ($rec_order==2){$inv_rec_num=mysql_result($result2,0,"rec_num2");}
       $batch=mysql_result($result2,0,"batch");

       if($batch==1){
          $newvalue=batch_recipe($inv_itemid);
          $newvalue=explode("~",$newvalue);
          $totalserv+=$newvalue[0];
          $totalprice+=($newvalue[1]/$inv_rec_num*$rec_num);
       }
       else{

       $query2 = "SELECT * FROM conversion WHERE sizeid = '$rec_size' AND sizeidto = '$order_size'";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=mysql_numrows($result2);

       if ($num2!=0){$convert=mysql_result($result2,0,"convert");}
       elseif($num2==0){$convert=$inv_rec_num;}
       else{$convert=1;}

       $newprice=money($price/$convert*$rec_num);
       $totalprice=$totalprice+$newprice;

       $query2 = "SELECT * FROM size WHERE sizeid = '$rec_size'";
       $result2 = Treat_DB_ProxyOld::query($query2);

       $sizename=mysql_result($result2,0,"sizename");

       $totalserv=$totalserv+($rec_num*$srv_num);

       }

       $num--;
       $counter++;
    }

    $newvalue="$totalserv~$totalprice";
    return $newvalue;
}

function batch_recipe2($editid,$curdate){
   //////////////////////////////FIND PREVIOUS FC%//////////
    $query = "SELECT * FROM recipe WHERE menu_itemid = '-$editid' ORDER BY recipeid DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=mysql_numrows($result);

    $query2 = "SELECT price FROM price_history WHERE menu_itemid = '-$editid' AND date >= '$curdate' ORDER BY date LIMIT 0,1";
    $result2 = Treat_DB_ProxyOld::query($query2);
    $num2=mysql_numrows($result2);

    if($num2>0){$item_price2=mysql_result($result2,0,"price");}
    else{$item_price2=$item_price;}

    $totalprice2=0;
    $totalserv2=0;
    $counter=1;
    $num--;
    while ($num>=0){
       $recipeid=mysql_result($result,$num,"recipeid");
       $inv_itemid=mysql_result($result,$num,"inv_itemid");
       $rec_num=mysql_result($result,$num,"rec_num");
       $rec_size=mysql_result($result,$num,"rec_size");
       $srv_num=mysql_result($result,$num,"srv_num");
       $rec_order=mysql_result($result,$num,"rec_order");

       $query2 = "SELECT * FROM inv_items WHERE inv_itemid = '$inv_itemid'";
       $result2 = Treat_DB_ProxyOld::query($query2);

       $supc=mysql_result($result2,0,"item_code");
       $inv_itemname=mysql_result($result2,0,"item_name");
       $order_size=mysql_result($result2,0,"order_size");
       $price2=mysql_result($result2,0,"price");
       if ($rec_order==1){$inv_rec_num=mysql_result($result2,0,"rec_num");}
       elseif ($rec_order==2){$inv_rec_num=mysql_result($result2,0,"rec_num2");}
       $batch=mysql_result($result2,0,"batch");

       if($batch==1){
          $newvalue=batch_recipe2($inv_itemid,$curdate);
          $newvalue=explode("~",$newvalue);
          $totalserv2+=$newvalue[0];
          $totalprice2+=($newvalue[1]/$inv_rec_num*$rec_num);
       }
       else{

       $query2 = "SELECT * FROM cost_history WHERE inv_itemid = '$inv_itemid' AND date >= '$curdate' ORDER BY date LIMIT 0,1";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=mysql_numrows($result2);

       if ($num2>0){$price2=mysql_result($result2,0,"cost");}

       $query2 = "SELECT * FROM conversion WHERE sizeid = '$rec_size' AND sizeidto = '$order_size'";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=mysql_numrows($result2);

       if ($num2!=0){$convert=mysql_result($result2,0,"convert");}
       elseif($num2==0){$convert=$inv_rec_num;}
       else{$convert=1;}

       $newprice2=money($price2/$convert*$rec_num);
       $totalprice2=$totalprice2+$newprice2;

       $query2 = "SELECT * FROM size WHERE sizeid = '$rec_size'";
       $result2 = Treat_DB_ProxyOld::query($query2);

       $sizename=mysql_result($result2,0,"sizename");

       $totalserv2=$totalserv2+($rec_num*$srv_num);

       }

       $num--;
       $counter++;
    }
}

define('DOC_ROOT', dirname(dirname(__FILE__)));
require_once(DOC_ROOT.'/bootstrap.php');

$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";


$curmenu=$_GET["curmenu"];
$businessid=$_GET["bid"];
$numserv=1;

    //$nutrition=array();

/////////////////////////MENU PROFITABLITIY////////////////
  echo "<body>";

  $curdate=$today;
  for($counter2=1;$counter2<=30;$counter2++){$curdate=prevday($curdate);}

  $query12 = "SELECT businessname,contract FROM business WHERE businessid = '$businessid'";
  $result12 = Treat_DB_ProxyOld::query($query12);

  $businessname=mysql_result($result12,0,"businessname");
  $contract=mysql_result($result12,0,"contract");

  $query12 = "SELECT menu_typename FROM menu_type WHERE menu_typeid = '$curmenu'";
  $result12 = Treat_DB_ProxyOld::query($query12);

  $curmenuname=mysql_result($result12,0,"menu_typename");

  echo "<center><table width=100%><tr><td colspan=5><center><b>Menu Profitability: $curmenuname - $businessname ($today)</b></td></tr>";

  $query12 = "SELECT * FROM menu_items WHERE businessid = '$businessid' AND menu_typeid = '$curmenu' ORDER BY groupid DESC, item_name DESC";
  $result12 = Treat_DB_ProxyOld::query($query12);
  $num12=mysql_numrows($result12);

  $totalitems=$num12;
  $totalfcprcnt=0;
  $totalchange=0;

  $lastgroupid=0;
  $num12--;
  while($num12>=0){

    $editid=mysql_result($result12,$num12,"menu_item_id");
    $itemname=mysql_result($result12,$num12,"item_name");
    $groupid=mysql_result($result12,$num12,"groupid");
    $serving_size=mysql_result($result12,$num12,"serving_size");
    $item_price=mysql_result($result12,$num12,"price");

    ///////SHOW GROUPS
    if ($lastgroupid!=$groupid){
          if($contract==2){
             $query5 = "SELECT menu_pricegroupname,menu_groupid FROM menu_pricegroup WHERE menu_pricegroupid = '$groupid'";
             $result5 = Treat_DB_ProxyOld::query($query5); 

             $groupname=mysql_result($result5,0,"menu_pricegroupname");
             $menu_groupid=mysql_result($result5,0,"menu_groupid");

             $query5 = "SELECT groupname FROM menu_groups WHERE menu_group_id = '$menu_groupid'";
             $result5 = Treat_DB_ProxyOld::query($query5); 

             $groupname2=mysql_result($result5,0,"groupname");

             $groupname = "$groupname2 - $groupname";
          }
          else{
             $query5 = "SELECT groupname FROM menu_groups WHERE menu_group_id = '$groupid'";
             $result5 = Treat_DB_ProxyOld::query($query5); 

             $groupname=mysql_result($result5,0,"groupname");
          }
       echo "<tr><td bgcolor=#E8E7E7><font size=2><b>$groupname</td><td bgcolor=#E8E7E7 align=right width=10%><font size=2><b>Cost</td><td bgcolor=#E8E7E7 align=right width=10%><font size=2><b>Price</td><td bgcolor=#E8E7E7 align=right width=10%><font size=2><b>+/-</td><td bgcolor=#E8E7E7 align=right width=10%><font size=2><b>Food Cost</td></tr>";
    }
    else{echo "<tr height=1><td colspan=5 bgcolor=#E8E7E7></td></tr>";}

    $query = "SELECT * FROM recipe WHERE menu_itemid = '$editid' ORDER BY recipeid DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=mysql_numrows($result);

    $totalprice=0;
    $totalserv=0;
    $counter=1;
    $num--;
    while ($num>=0){
       $recipeid=mysql_result($result,$num,"recipeid");
       $inv_itemid=mysql_result($result,$num,"inv_itemid");
       $rec_num=mysql_result($result,$num,"rec_num");
       $rec_size=mysql_result($result,$num,"rec_size");
       $srv_num=mysql_result($result,$num,"srv_num");
       $rec_order=mysql_result($result,$num,"rec_order");

       $query2 = "SELECT * FROM inv_items WHERE inv_itemid = '$inv_itemid'";
       $result2 = Treat_DB_ProxyOld::query($query2);

       $supc=mysql_result($result2,0,"item_code");
       $inv_itemname=mysql_result($result2,0,"item_name");
       $order_size=mysql_result($result2,0,"order_size");
       $price=mysql_result($result2,0,"price");
       if ($rec_order==1){$inv_rec_num=mysql_result($result2,0,"rec_num");}
       elseif ($rec_order==2){$inv_rec_num=mysql_result($result2,0,"rec_num2");}
       $batch=mysql_result($result2,0,"batch");

       if($batch==1){
          $newvalue=batch_recipe($inv_itemid);
          $newvalue=explode("~",$newvalue);
          $totalserv+=$newvalue[0];
          $totalprice+=($newvalue[1]/$inv_rec_num*$rec_num);
       }
       else{

       $query2 = "SELECT * FROM conversion WHERE sizeid = '$rec_size' AND sizeidto = '$order_size'";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=mysql_numrows($result2);

       if ($num2!=0){$convert=mysql_result($result2,0,"convert");}
       elseif($num2==0){$convert=$inv_rec_num;}
       else{$convert=1;}

       $newprice=money($price/$convert*$rec_num);
       $totalprice=$totalprice+$newprice;

       $query2 = "SELECT * FROM size WHERE sizeid = '$rec_size'";
       $result2 = Treat_DB_ProxyOld::query($query2);

       $sizename=mysql_result($result2,0,"sizename");

       $totalserv=$totalserv+($rec_num*$srv_num);

       }

       $num--;
       $counter++;
    }

    $item_price=money($item_price*$serving_size);
    $totalprice=money($totalprice);
    $profit=money($item_price-$totalprice);
    $fcprcnt=round($totalprice/$item_price*100,1);

    $totalfcprcnt=$totalfcprcnt+$fcprcnt;

    //////////////////////////////FIND PREVIOUS FC%//////////
    $query = "SELECT * FROM recipe WHERE menu_itemid = '$editid' ORDER BY recipeid DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=mysql_numrows($result);

    $query2 = "SELECT price FROM price_history WHERE menu_itemid = '$editid' AND date >= '$curdate' ORDER BY date LIMIT 0,1";
    $result2 = Treat_DB_ProxyOld::query($query2);
    $num2=mysql_numrows($result2);

    if($num2>0){$item_price2=mysql_result($result2,0,"price");}
    else{$item_price2=$item_price;}

    $totalprice2=0;
    $totalserv2=0;
    $counter=1;
    $num--;
    while ($num>=0){
       $recipeid=mysql_result($result,$num,"recipeid");
       $inv_itemid=mysql_result($result,$num,"inv_itemid");
       $rec_num=mysql_result($result,$num,"rec_num");
       $rec_size=mysql_result($result,$num,"rec_size");
       $srv_num=mysql_result($result,$num,"srv_num");
       $rec_order=mysql_result($result,$num,"rec_order");

       $query2 = "SELECT * FROM inv_items WHERE inv_itemid = '$inv_itemid'";
       $result2 = Treat_DB_ProxyOld::query($query2);

       $supc=mysql_result($result2,0,"item_code");
       $inv_itemname=mysql_result($result2,0,"item_name");
       $order_size=mysql_result($result2,0,"order_size");
       $price2=mysql_result($result2,0,"price");
       if ($rec_order==1){$inv_rec_num=mysql_result($result2,0,"rec_num");}
       elseif ($rec_order==2){$inv_rec_num=mysql_result($result2,0,"rec_num2");}
       $batch=mysql_result($result2,0,"batch");

       if($batch==1){
          $newvalue=batch_recipe2($inv_itemid,$curdate);
          $newvalue=explode("~",$newvalue);
          $totalserv2+=$newvalue[0];
          $totalprice2+=($newvalue[1]/$inv_rec_num*$rec_num);
       }
       else{

       $query2 = "SELECT * FROM cost_history WHERE inv_itemid = '$inv_itemid' AND date >= '$curdate' ORDER BY date LIMIT 0,1";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=mysql_numrows($result2);

       if ($num2>0){$price2=mysql_result($result2,0,"cost");}

       $query2 = "SELECT * FROM conversion WHERE sizeid = '$rec_size' AND sizeidto = '$order_size'";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=mysql_numrows($result2);

       if ($num2!=0){$convert=mysql_result($result2,0,"convert");}
       elseif($num2==0){$convert=$inv_rec_num;}
       else{$convert=1;}

       $newprice2=money($price2/$convert*$rec_num);
       $totalprice2=$totalprice2+$newprice2;

       $query2 = "SELECT * FROM size WHERE sizeid = '$rec_size'";
       $result2 = Treat_DB_ProxyOld::query($query2);

       $sizename=mysql_result($result2,0,"sizename");

       $totalserv2=$totalserv2+($rec_num*$srv_num);

       }

       $num--;
       $counter++;
    }

    $fcprcnt2=round($totalprice2/$item_price2*100,1);

    $prcnt_diff=round($fcprcnt-$fcprcnt2,1);

    $totalchange=$totalchange+$prcnt_diff;

    if ($prcnt_diff>0){$prcnt_diff="<font color=red>+$prcnt_diff%</font>";}
    elseif ($prcnt_diff<0){$prcnt_diff="<font color=green>$prcnt_diff%</font>";}
    else {$prcnt_diff="";}
    /////////////////////////////////////////////////////////////////////////////////////////////////

    if ($fcprcnt>=40){$fcprcnt="<font color=red><b>$fcprcnt</b></font>";}
    if($serving_size>1){$totalprice=money($totalprice/$serving_size);$item_price=money($item_price/$serving_size);}

    echo "<tr onMouseOver=this.bgColor='#999999' onMouseOut=this.bgColor='white'><td><a href=menuprofithistory.php?curmenu=$curmenu&bid=$businessid&editid=$editid style=$style title='Click to view Food Cost History'><font size=2 color=blue>$itemname</font></a></td><td align=right><font size=2>$totalprice</td><td align=right><font size=2>$item_price</td><td align=right><font size=2>$prcnt_diff</td><td align=right><font size=2>$fcprcnt%</td></tr>";
    
    $lastgroupid=$groupid;
    $num12--;
  }
  $av_fc=round($totalfcprcnt/$totalitems,1);
  $totalchange=round($av_fc-($totalchange/$totalitems),1);

  echo "<tr height=1><td colspan=5 bgcolor=#E8E7E7></td></tr>";
  echo "<tr><td colspan=3><font size=1 color=gray>+/- column is calculated based on costs/pricing 30 days prior to $today.</td><td align=right colspan=1><font size=2><b>$totalchange%</td><td align=right colspan=1><font size=2><b>$av_fc%</td></tr>";
  echo "</table></body>";

  google_page_track();
?>