<?php

if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
require_once( dirname(__FILE__) . '/../../application/bootstrap.php' );

$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";

$user = \EE\Controller\Base::getPostVariable('username');
$pass = \EE\Controller\Base::getPostVariable('password');
$businessid = \EE\Controller\Base::getPostVariable('businessid');
$companyid = \EE\Controller\Base::getPostVariable('companyid');
$debitname = \EE\Controller\Base::getPostVariable('debitname');
$debittypeid = \EE\Controller\Base::getPostVariable('debittype');
$account = \EE\Controller\Base::getPostVariable('account');
$tender = \EE\Controller\Base::getPostVariable('tender');
$debitid = \EE\Controller\Base::getPostVariable('debitid');
$edit = \EE\Controller\Base::getPostVariable('edit');
$heldcheck = \EE\Controller\Base::getPostVariable('heldcheck');
$category = \EE\Controller\Base::getPostVariable('category');

if ($user=="" && $pass==""){
	$user = \EE\Controller\Base::getSessionCookieVariable('usercook');
	$pass = \EE\Controller\Base::getSessionCookieVariable('passcook');
	$businessid = \EE\Controller\Base::getGetVariable('bid');
	$companyid = \EE\Controller\Base::getGetVariable('cid');
	$edit = \EE\Controller\Base::getGetVariable('edit');
	$debitid = \EE\Controller\Base::getGetVariable('debitid');
}

if ($account=="Acct #"){$account="";}

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=Treat_DB_ProxyOld::mysql_numrows($result);

$location="editbus.php?bid=$businessid&cid=$companyid#debits";
header('Location: ./' . $location);

if ($num != 1){
	echo "<center><h3>Failed</h3></center>";
} else {
	if ($tender=="on"){
		$inv_tender=1;
	} else {
		$inv_tender=0;
	} 

	if ($heldcheck=="on"){
		$heldcheck=1;
	} else {
		$heldcheck=0;
	}
	
	if ($edit==1&&$debitname!=""){
		$query = "UPDATE debits SET debitname = '$debitname', debittype = '$debittypeid', account = '$account', inv_tender = '$inv_tender', heldcheck = '$heldcheck', category = '$category' WHERE debitid = '$debitid'";
		$result = Treat_DB_ProxyOld::query($query);
	} elseif ($edit==2){
		$query = "UPDATE debits SET is_deleted = '1', del_date = '$today' WHERE debitid = '$debitid'";
		$result = Treat_DB_ProxyOld::query($query);
	} elseif($debitname!=""){
		$query = "INSERT INTO debits (businessid, companyid, debitname, debittype, account, inv_tender, heldcheck, category) VALUES ('$businessid', '$companyid', '$debitname', '$debittypeid', '$account', '$inv_tender', '$heldcheck', '$category')";
		$result = Treat_DB_ProxyOld::query($query);
	}
}

?>