<?php
if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
#include_once( 'db.php' );
require_once( DOC_ROOT . '/bootstrap.php' );

$style = "text-decoration:none";

$user = \EE\Controller\Base::getSessionCookieVariable( 'usercook' );
$pass = \EE\Controller\Base::getSessionCookieVariable( 'passcook' );
$businessid = \EE\Controller\Base::getPostVariable( 'businessid' );
$companyid = \EE\Controller\Base::getPostVariable( 'companyid' );
$rec_counter = \EE\Controller\Base::getPostVariable( 'rec_counter' );
$menu_recipes = \EE\Controller\Base::getPostVariable( 'menu_recipes' );


$passed_cookie_login = require_once( realpath( DOC_ROOT . '/../lib/inc/cookie-login.php' ) );
if (
	$passed_cookie_login
	&& (
		(
			$GLOBALS['security_level'] == 1
			&& $GLOBALS['bid'] == $page_business->getBusinessId()
			&& $GLOBALS['cid'] == $page_business->getCompanyId()
		)
		|| (
			$GLOBALS['security_level'] > 1
			&& $GLOBALS['cid'] == $page_business->getCompanyId()
		)
	)
) {
?>
<html>
<head>
	<title>Essential Elements :: Treat America :: copybusrecipes</title>
<script type="text/javascript" src="/assets/js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="/assets/js/preload/preLoadingMessage.js"></script>

<?php /*
<script language=javascript type="text/javascript">
function apply()
{
	document.frm.sub.disabled=true;
	if(document.frm.chk.checked==true)
	{
		document.frm.sub.disabled=false;
	}
	if(document.frm.chk.checked==false)
	{
		document.frm.sub.enabled=false;
	}
}
</script> 
*/ ?>

</head>

<body id="access_control" onload="document.forms[0].grant_button.disabled=true;">
<?php /*
* / ?>
	<pre><?php var_dump( $_POST ); ?></pre>
<?php /*
*/ ?>
	<center>
		<table cellspacing="0" cellpadding="0" border="0" width="90%">
			<tr>
				<td colspan="2">
					<img src="logo.jpg" alt="logo"/>
					<p>&nbsp;</p>
				</td>
			</tr>
		</table>
<?php
	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
#	$query = "SELECT * FROM company WHERE companyid = '$companyid'";
#	$result = Treat_DB_ProxyOld::query( $query, true );
	//mysql_close();

#	$companyname = @mysql_result( $result, 0, 'companyname' );

	$menu_item = array();
	$missing_inv_items = array();
	$inv_items_needed = array();
	$no_supc_items = array();

	///////////////////////////////////////////////
	///////////GET MENU ITEMS//////////////////////
	///////////////////////////////////////////////
	$mycounter = 0;
	foreach ( $menu_recipes as $menu_itemid => $checkbox_value ) {
		//echo "$mycounter: $menu_itemid<br>";

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query = "SELECT businessid FROM menu_items WHERE menu_item_id = '$menu_itemid'";
		$result = Treat_DB_ProxyOld::query( $query, true );
		//mysql_close();

		$busid = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'businessid' );

		if ( $menu_itemid && $busid ) {
#			$menu_item[ $menu_itemid ] = $busid;
			$menu_item[ $menu_itemid ] = 0;
		}

		$mycounter++;
	}

	///////////////////////////////////////////////
	///////////GET INGREDIENTS/////////////////////
	///////////////////////////////////////////////
	# $key = $menu_itemid
	# $value = $busid
	foreach ( $menu_item as $menu_itemid => $busid ) {
		
		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query = sprintf( '
				SELECT
					r.*
					,ii.item_code
					,ii.item_name
					,ii.businessid
				FROM recipe r
					JOIN inv_items ii
						ON r.inv_itemid = ii.inv_itemid
				WHERE r.menu_itemid = %d
				ORDER BY r.recipeid
			'
			,$menu_itemid
		);
		$result = Treat_DB_ProxyOld::query( $query, true );
		$num = Treat_DB_ProxyOld::mysql_numrows( $result );
		//mysql_close();

		while ( $row = Treat_DB_ProxyOld::mysql_fetch_object( $result ) ) {
			if ( $row->item_code ) {
				# $key = $item_code
				# $value = $inv_itemid
				$inv_items_needed[ $row->inv_itemid ] = $row->item_code;
			}
			else {
				# $key = $inv_itemid
				# $value = $inv_itemid
				$no_supc_items[ $row->inv_itemid ] = $row->inv_itemid;
			}
		}
	}

	///////////////////////////////////////////////
	///////////COMPARE EXISTING INGREDIENTS////////
	///////////////////////////////////////////////
	# $key = $item_code
	# $value = $inv_itemid
	foreach ( $inv_items_needed as $inv_itemid => $item_code ) {
		$query = sprintf( '
				SELECT *
				FROM inv_items
				WHERE businessid = %d
				AND item_code LIKE "%s"
			'
			,$businessid
			,mysql_escape_string( $item_code )
		);
		$result = Treat_DB_ProxyOld::query( $query, true );
		$num = Treat_DB_ProxyOld::mysql_numrows( $result );
		//mysql_close();
		$inv_item = @Treat_DB_ProxyOld::mysql_fetch_object( $result );
		
		if ( $num > 0 ) {
			//$inv_items_needed[ $item_code ]=1;
		}
		else {
			# $key = $item_code
			# $value = $inv_itemid
			$missing_inv_items[ $inv_itemid ] = $item_code;
#			$inv_items_needed[ $inv_itemid ] = 0;
			unset( $inv_items_needed[ $inv_itemid ] );
		}
	}

	///////////////////////////////////////////////
	///////////AGREEMENT///////////////////////////
	///////////////////////////////////////////////
?>
		<form action="/ta/copybusrecipes2.php" method="post" name="frm">
<?php /*
			<table width="90%" border="1">
				<tr>
					<td bgcolor="yellow">
						<strong>Please read the following Notes and check the box to continue:</strong>
						<br/>
							<strong><i>Inventory Items for these Recipes will fall into 1 of 3 Categories:</i></strong>
						<ul>
							<li>
								<font color="blue">1. Inventory items already exist in this unit.</font>
								<ul>
									<li>
										<font color="blue">Recipe Units will be Updated</font>
									</li>
									<li>
										<font color="blue">Recipe Units in Order Units will be Updated</font>
									</li>
								</ul>
							</li>
							<li>
								<font color="red">2. No inventory item with corresponding SUPC.</font>
								<ul>
									<li>
										<font color="red">By Default, similar inventory items will be selected</font>
									</li>
									<li>
										<font color="red">
											If you choose an existing item, Recipe Units will 
											be updated and you'll be prompted to update Recipe 
											Units in Order Units.
										</font>
									</li>
								</ul>
							</li>
							<li>
								<font color="black">3. Inventory items missing SUPC`s</font>
								<ul>
									<li>
										<font color="black">By Default, Inventory Item will be created</font>
									</li>
									<li>
										<font color="black">
											If you choose an existing item, Recipe Units will 
											be updated and you'll be prompted to update Recipe 
											Units in Order Units.
										</font>
									</li>
								</ul>
							</li>
							<li>
								<font color="green">
									IF YOU HAVE EXISTING RECIPES, THIS WILL AFFECT THEM BY 
									CHANGING YOUR 'RECIPE UNITS' AND 'RECIPE UNITS IN ORDER 
									UNITS' PER INVENTORY ITEM!
								</font>
							</li>
							<li>
								<font color="green">
									It is NOT recommended that you copy from multiple 
									locations, as recipe units may vary.
								</font>
							</li>
						</ul>
						<input type="checkbox" name="chk" onclick="apply();" />
						<strong>I Understand, Check to Continue</strong>
					</td>
				</tr>
			</table>
			<p>&nbsp;</p>
*/ ?>
<?php
	///////////////////////////////////////////////
	///////////DISPLAY/////////////////////////////
	///////////////////////////////////////////////
	
	//////////WHICH MENU TO COPY TO
?>
			<table width="90%" border="1">
				<tr bgcolor="#E8E7E7">
					<td>
						<strong>Choose which Menu to copy to</strong>
					</td>
				</tr>
				<tr>
					<td>
						<select name="which_menu">
<?php
	
	$query41 = sprintf( '
			SELECT *
			FROM menu_type
			WHERE (
					companyid = %1$d
					AND businessid = %2$d
				)
				OR (
					companyid = %1$d
					AND businessid = 0
				)
		'
		,$companyid
		,$businessid
	);
	$result41 = Treat_DB_ProxyOld::query( $query41, true );
	
	while ( $r = Treat_DB_ProxyOld::mysql_fetch_array( $result41 ) ) {
?>
							<option value="<?php echo $r['menu_typeid']; ?>"<?php
		if ( 'Cafe Menu' == $r['menu_typename'] ) {
			echo ' selected="selected"';
		}
							?>><?php echo $r['menu_typename']; ?></option>
<?php
	}
	
?>
						</select>
					</td>
				</tr>
			</table>
			<p>&nbsp;</p>
<?php
	//////////END MENU

?>
			<table width="90%" border="1">
				<tr bgcolor="#E8E7E7">
					<td colspan="3">
						<font color="blue">
							<strong>Existing Inventory Items that will be Updated</strong>
						</font>
					</td>
				</tr>
<?php
	$loopcount = 0;
	# $key = $item_code
	# $value = $inv_itemid
	foreach ( $inv_items_needed as $inv_itemid => $item_code ) {

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query = sprintf( '
				SELECT *
				FROM inv_items
				WHERE businessid = %d
				AND item_code LIKE "%s"
			'
			,$businessid
			,mysql_escape_string( $item_code )
		);
		$result = Treat_DB_ProxyOld::query( $query, true );
		//mysql_close();
		$inv_item = @Treat_DB_ProxyOld::mysql_fetch_object( $result );
		
		if ( $inv_itemid ) {
?>
				<tr>
					<td>
						<font color="blue"><?php echo $inv_item->item_code; ?></font>
						<?php echo $inv_item->inv_itemid; ?>
					</td>
					<td colspan="2">
						<font color="blue"><?php echo $inv_item->item_name; ?></font>
					</td>
				</tr>
<?php
			$loopcount++;
		}
	}
	if ( $loopcount == 0 ) {
?>
				<tr>
					<td colspan="3">
						<font color="blue">
							<i>None</i>
						</font>
					</td>
				</tr>
<?php
	}

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "
		SELECT
			ii.item_name
			,ii.inv_itemid
			,ii.item_code
			,n.category
		FROM inv_items ii
			LEFT JOIN nutrition n
				ON ii.item_code = n.item_code
		WHERE ii.businessid = '$businessid'
			AND n.category >= '0'
		ORDER BY
			ii.item_name DESC
			,n.category DESC
	";
	$result = Treat_DB_ProxyOld::query( $query, true );
	$num = Treat_DB_ProxyOld::mysql_numrows( $result );
	$possible_inv_items = array();
	while ( $row = Treat_DB_ProxyOld::mysql_fetch_object( $result ) ) {
		$possible_inv_items[] = $row;
	}
	//mysql_close();
	$possible_suggested  = '<option value="%s" style="background-color: yellow;" selected="selected">%s</option>';
	$possible_near_match = '<option value="%s" style="background-color: #e8e7e7;">%s</option>';
	$possible_no_match   = '<option value="%s">%s</option>';
?>
				<tr bgcolor="#E8E7E7">
					<td colspan="3">
						<font color="red">
							<strong>Missing Inventory Items (Pick from existing items or create new ones)</strong>
						</font>
					</td>
				</tr>
<?php

	$mycounter2 = -1;
	$loopcount = 0;
	
	# $key = $item_code
	# $value = $inv_itemid
	foreach ( $missing_inv_items as $inv_itemid => $item_code ) {
		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query2 = "
			SELECT
				ii.item_name
				,ii.item_code
				,n.category
			FROM inv_items ii
				LEFT JOIN nutrition n
					ON ii.item_code = n.item_code
			WHERE ii.inv_itemid = '$inv_itemid'
		";
		$result2 = Treat_DB_ProxyOld::query( $query2, true );
		//mysql_close();
		$missing_inv_item = @Treat_DB_ProxyOld::mysql_fetch_object( $result2 );
?>
				<tr>
					<td>
						<font color="red"><?php echo $item_code; ?></font>
					</td>
					<td>
						<font color="red"><?php echo $missing_inv_item->item_name; ?></font>
					</td>
					<td>
						<select name="items_missing[<?php echo $item_code; ?>]">
							<option value="<?php echo $inv_itemid; ?>">(Create New Item)</option>
<?php

		$suggested = array();
		$others    = array();
		foreach ( $possible_inv_items as $possible_item ) {
			if ( $missing_inv_item->category == $possible_item->category ) {
				$suggested[] = sprintf(
					$possible_suggested
					,$possible_item->inv_itemid
					,$possible_item->item_name
				);
			}
			elseif ( round( $missing_inv_item->category, 0 ) == round( $possible_item->category, 0 ) ) {
				$others[] = sprintf(
					$possible_near_match
					,$possible_item->inv_itemid
					,$possible_item->item_name
				);
			}
			else {
				$others[] = sprintf(
					$possible_no_match
					,$possible_item->inv_itemid
					,$possible_item->item_name
				);
			}
		}
		echo "\n\t\t\t\t\t\t\t"
			,implode( "\n\t\t\t\t\t\t\t", $suggested )
			,"\n\t\t\t\t\t\t\t"
			,implode( "\n\t\t\t\t\t\t\t", $others )
			,"\n"
		;
?>
						</select>
					</td>
				</tr>
<?php
		$mycounter2--;
		$loopcount++;
	}
	if ( $loopcount == 0 ) {
?>
				<tr>
					<td colspan="3">
						<font color="red">
							<i>None</i>
						</font>
					</td>
				</tr>
<?php
	}

?>
				<tr bgcolor="#E8E7E7">
					<td colspan="3">
						<strong>Items Missing SUPC`s (Pick from existing items or create new ones)</strong>
					</td>
				</tr>
<?php

	$mycounter = 0;
	$loopcount = 0;
	# $key = $inv_itemid
	# $value = $inv_itemid
	foreach ( $no_supc_items as $key => $inv_itemid ) {

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query2 = "SELECT item_name FROM inv_items WHERE inv_itemid = '$inv_itemid'";
		$result2 = Treat_DB_ProxyOld::query( $query2, true );
		//mysql_close();

		$item_name = @Treat_DB_ProxyOld::mysql_result( $result2, 0, 'item_name' );

?>
				<tr>
					<td>
						<?php echo $key; ?>
						<?php echo $inv_itemid; ?>
					</td>
					<td>
						<?php echo $item_name; ?>
					</td>
					<td>
						<select name="items_no_supc[<?php echo $inv_itemid; ?>]">
							<option value="<?php echo $inv_itemid; ?>">(Create New Item)</option>
<?php
#		$suggested = array();
		$others    = array();
		foreach ( $possible_inv_items as $possible_item ) {
#			else {
				$others[] = sprintf(
					$possible_no_match
					,$possible_item->inv_itemid
					,$possible_item->item_name
				);
#			}
		}
		echo "\n\t\t\t\t\t\t\t"
#			,implode( "\n\t\t\t\t\t\t\t", $suggested )
#			,"\n\t\t\t\t\t\t\t"
			,implode( "\n\t\t\t\t\t\t\t", $others )
			,"\n"
		;
?>
						</select>
					</td>
				</tr>
<?php

		$mycounter++;
		$loopcount++;
	}
	if ( $loopcount == 0 ) {
?>
				<tr>
					<td colspan="3">
						<font color="black">
							<i>None</i>
						</font>
					</td>
				</tr>
<?php
	}

	$menu_item = urlencode( serialize( $menu_item ) );
	$inv_items_needed = urlencode( serialize( $inv_items_needed ) );
	$missing_inv_items = urlencode( serialize( $missing_inv_items ) );
	$no_supc_items = urlencode( serialize( $no_supc_items ) );

?>
				<tr>
					<td colspan="3">
						<input type="hidden" name="mycounter" value="<?php echo $mycounter; ?>" />
						<input type="hidden" name="mycounter2" value="<?php echo $mycounter2; ?>" />
						<input type="hidden" name="businessid" value="<?php echo $businessid; ?>" />
						<input type="hidden" name="companyid" value="<?php echo $companyid; ?>" />
						<input type="hidden" name="menu_item" value="<?php echo $menu_item; ?>" />
						<input type="hidden" name="inv_items_needed" value="<?php echo $inv_items_needed; ?>" />
						<input type="hidden" name="missing_inv_items" value="<?php echo $missing_inv_items; ?>" />
						<input type="hidden" name="no_supc_items" value="<?php echo $no_supc_items; ?>" />
<?php
#						<input type="submit" name="sub" value="COPY" disabled="disabled" />
?>
						<input type="submit" name="sub" value="COPY" />
					</td>
				</tr>
			</table>
		</form>
	</center>

	<p>&nbsp;</p>
	<center>
		<div>
			<a href="/ta/comrecipe.php?cid=<?php echo $companyid; ?>" style="<?php echo $style; ?>">
				<font color="blue">
					Return
				</font>
			</a>
		</div>
	</center>
<?php
}
?>
	</body>
</html>