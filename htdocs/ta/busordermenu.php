<?php

function isLeapYear($year) 
{ 
    /*
    # Check for valid parameters # 
    if (!is_int($year) || $year < 0) 
    { 
        printf('Wrong parameter for $year in function isLeapYear. It must be a positive integer.'); 
        exit(); 
    } 
    */
        
    # In the Gregorian calendar there is a leap year every year divisible by four 
    # except for years which are both divisible by 100 and not divisible by 400. 
        
    if ($year % 4 != 0) 
    { 
        return 28; 
    } 
    else 
    { 
        if ($year % 100 != 0) 
        { 
            return 29;    # Leap year 
        } 
        else 
        { 
            if ($year % 400 != 0) 
            { 
                return 28; 
            } 
            else 
            { 
                return 29;    # Leap year 
            } 
        } 
    } 
} 

define('DOC_ROOT', dirname(dirname(__FILE__)) );
require_once(DOC_ROOT.'/bootstrap.php');

/*$user=$_COOKIE["usercook"];
$pass=$_COOKIE["passcook"];
$cid=$_COOKIE["compcook"];
$caterbus=$_COOKIE["caterbus"];
$curmenu=$_COOKIE["curmenu"];
$curdaypart=$_COOKIE["curdaypart"];
$number=$_GET['number'];
$newmonth=$_GET['newmonth'];
$newyear=$_GET['newyear'];
$newstart=$_GET['newstart'];
$busid=$_GET['bid'];
$adddate=$_GET['adddate'];
$menu=$_GET['menu'];
$newcurdaypart=$_GET['curdaypart'];*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook');
$cid = \EE\Controller\Base::getSessionCookieVariable('compcook');
$caterbus = \EE\Controller\Base::getSessionCookieVariable('caterbus');
$curmenu = \EE\Controller\Base::getSessionCookieVariable('curmenu');
$curdaypart = \EE\Controller\Base::getSessionCookieVariable('curdaypart');
$number = \EE\Controller\Base::getGetVariable('number');
$newmonth = \EE\Controller\Base::getGetVariable('newmonth');
$newyear = \EE\Controller\Base::getGetVariable('newyear');
$newstart = \EE\Controller\Base::getGetVariable('newstart');
$busid = \EE\Controller\Base::getGetVariable('bid');
$adddate = \EE\Controller\Base::getGetVariable('adddate');
$menu = \EE\Controller\Base::getGetVariable('menu');
$newcurdaypart = \EE\Controller\Base::getGetVariable('curdaypart');

if($newcurdaypart!=""){setcookie("curdaypart",$newcurdaypart);$curdaypart=$newcurdaypart;}

$today = date("l");
$todaynum = date("j");
$todaynum2 = date("j");
$year = date("Y");
$monthnum = date("n");
$day=1;
$high = 6;
$long = 1;
$leap = date("L");
$back = "white";
$count = 0;
$style = "text-decoration:none";
$brief ="";
$you = "";

?>
<head>

<SCRIPT LANGUAGE="JavaScript" SRC="CalendarPopup.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">document.write(getCalendarStyles());</SCRIPT>

<STYLE>
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation
			{
			background-color:#6677DD;
			text-align:center;
			vertical-align:center;
			text-decoration:none;
			color:#FFFFFF;
			font-weight:bold;
			}
	.TESTcpDayColumnHeader,
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation,
	.TESTcpCurrentMonthDate,
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDate,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDate,
	.TESTcpCurrentDateDisabled,
	.TESTcpTodayText,
	.TESTcpTodayTextDisabled,
	.TESTcpText
			{
			font-family:arial;
			font-size:8pt;
			}
	TD.TESTcpDayColumnHeader
			{
			text-align:right;
			border:solid thin #6677DD;
			border-width:0 0 1 0;
			}
	.TESTcpCurrentMonthDate,
	.TESTcpOtherMonthDate,
	.TESTcpCurrentDate
			{
			text-align:right;
			text-decoration:none;
			}
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDateDisabled
			{
			color:#D0D0D0;
			text-align:right;
			text-decoration:line-through;
			}
	.TESTcpCurrentMonthDate
			{
			color:#6677DD;
			font-weight:bold;
			}
	.TESTcpCurrentDate
			{
			color: #FFFFFF;
			font-weight:bold;
			}
	.TESTcpOtherMonthDate
			{
			color:#808080;
			}
	TD.TESTcpCurrentDate
			{
			color:#FFFFFF;
			background-color: #6677DD;
			border-width:1;
			border:solid thin #000000;
			}
	TD.TESTcpCurrentDateDisabled
			{
			border-width:1;
			border:solid thin #FFAAAA;
			}
	TD.TESTcpTodayText,
	TD.TESTcpTodayTextDisabled
			{
			border:solid thin #6677DD;
			border-width:1 0 0 0;
			}
	A.TESTcpTodayText,
	SPAN.TESTcpTodayTextDisabled
			{
			height:20px;
			}
	A.TESTcpTodayText
			{
			color:#6677DD;
			font-weight:bold;
			}
	SPAN.TESTcpTodayTextDisabled
			{
			color:#D0D0D0;
			}
	.TESTcpBorder
			{
			border:solid thin #6677DD;
			}
</STYLE>

<script language="JavaScript" 
   type="text/JavaScript">
function changePage(newLoc)
 {
   nextPage = newLoc.options[newLoc.selectedIndex].value
		
   if (nextPage != "")
   {
      document.location.href = nextPage
   }
 }
</script>

<SCRIPT LANGUAGE="JavaScript">
<!-- Web Site:  http://dynamicdrive.com -->

<!-- This script and many more are available free online at -->
<!-- The JavaScript Source!! http://javascript.internet.com -->

<!-- Begin
function disableForm(theform) {
if (document.all || document.getElementById) {
for (i = 0; i < theform.length; i++) {
var tempobj = theform.elements[i];
if (tempobj.type.toLowerCase() == "submit" || tempobj.type.toLowerCase() == "reset")
tempobj.disabled = true;
}

}
else {
alert("The form has been submitted.  But, since you're not using IE 4+ or NS 6, the submit button was not disabled on form submission.");
return false;
   }
}
//  End -->
</script>

</head>

<?php

if ($newstart == "")
{
   while ($todaynum > 7){$todaynum = $todaynum - 7; $count++;}
   if ($today == 'Sunday' && $todaynum == 1){$start=1;}
   if ($today == 'Monday' && $todaynum == 1){$start=2;}
   if ($today == 'Tuesday' && $todaynum == 1){$start=3;}
   if ($today == 'Wednesday' && $todaynum == 1){$start=4;}
   if ($today == 'Thursday' && $todaynum == 1){$start=5;}
   if ($today == 'Friday' && $todaynum == 1){$start=6;}
   if ($today == 'Saturday' && $todaynum == 1){$start=7;}
   if ($today == 'Sunday' && $todaynum == 2){$start=7;}
   if ($today == 'Monday' && $todaynum == 2){$start=1;}
   if ($today == 'Tuesday' && $todaynum == 2){$start=2;}
   if ($today == 'Wednesday' && $todaynum == 2){$start=3;}
   if ($today == 'Thursday' && $todaynum == 2){$start=4;}
   if ($today == 'Friday' && $todaynum == 2){$start=5;}
   if ($today == 'Saturday' && $todaynum == 2){$start=6;}
   if ($today == 'Sunday' && $todaynum == 3){$start=6;}
   if ($today == 'Monday' && $todaynum == 3){$start=7;}
   if ($today == 'Tuesday' && $todaynum == 3){$start=1;}
   if ($today == 'Wednesday' && $todaynum == 3){$start=2;}
   if ($today == 'Thursday' && $todaynum == 3){$start=3;}
   if ($today == 'Friday' && $todaynum == 3){$start=4;}
   if ($today == 'Saturday' && $todaynum == 3){$start=5;}
   if ($today == 'Sunday' && $todaynum == 4){$start=5;}
   if ($today == 'Monday' && $todaynum == 4){$start=6;}
   if ($today == 'Tuesday' && $todaynum == 4){$start=7;}
   if ($today == 'Wednesday' && $todaynum == 4){$start=1;}
   if ($today == 'Thursday' && $todaynum == 4){$start=2;}
   if ($today == 'Friday' && $todaynum == 4){$start=3;}
   if ($today == 'Saturday' && $todaynum == 4){$start=4;}
   if ($today == 'Sunday' && $todaynum == 5){$start=4;}
   if ($today == 'Monday' && $todaynum == 5){$start=5;}
   if ($today == 'Tuesday' && $todaynum == 5){$start=6;}
   if ($today == 'Wednesday' && $todaynum == 5){$start=7;}
   if ($today == 'Thursday' && $todaynum == 5){$start=1;}
   if ($today == 'Friday' && $todaynum == 5){$start=2;}
   if ($today == 'Saturday' && $todaynum == 5){$start=3;}
   if ($today == 'Sunday' && $todaynum == 6){$start=3;}
   if ($today == 'Monday' && $todaynum == 6){$start=4;}
   if ($today == 'Tuesday' && $todaynum == 6){$start=5;}
   if ($today == 'Wednesday' && $todaynum == 6){$start=6;}
   if ($today == 'Thursday' && $todaynum == 6){$start=7;}
   if ($today == 'Friday' && $todaynum == 6){$start=1;}
   if ($today == 'Saturday' && $todaynum == 6){$start=2;}
   if ($today == 'Sunday' && $todaynum == 7){$start=2;}
   if ($today == 'Monday' && $todaynum == 7){$start=3;}
   if ($today == 'Tuesday' && $todaynum == 7){$start=4;}
   if ($today == 'Wednesday' && $todaynum == 7){$start=5;}
   if ($today == 'Thursday' && $todaynum == 7){$start=6;}
   if ($today == 'Friday' && $todaynum == 7){$start=7;}
   if ($today == 'Saturday' && $todaynum == 7){$start=1;}
}
elseif ($newstart > 7)
{
   $start = $newstart - 7;
}
else 
{
   $start = $newstart;
}

if ($newmonth != ""){;}
else {$newmonth = $monthnum;}
if ($newyear != ""){$year=$newyear;}
else {$newyear = $year;}

if ($adddate==""){
$day4 = date("d");
$year4 = date("Y");
$month4 = date("m");
$adddate="$year4-$month4-$day4";
}

$hour = date("H");
$min = date("i");
$year1 = date("y");
$mon = date("m");
$day2 = date("d");
$time2 = "1$year1$mon$day2$hour$min";

$style = "text-decoration:none";

$hour = date("H");
$min = date("i");
$year2 = date("y");
$mon = date("m");
$day2 = date("d");
$now = "1$year2$mon$day2$hour$min";
$dif = $now-$time;

if ($curmenu==""){
   //mysql_connect($dbhost,$username,$password);
   //@mysql_select_db($database) or die( "Unable to select database");
   $query = "SELECT default_menu FROM business WHERE businessid = '$caterbus'";
   $result = Treat_DB_ProxyOld::query($query);
   //mysql_close();

   $curmenu=Treat_DB_ProxyOld::mysql_result($result,0,"default_menu");
}

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM menu_type WHERE menu_typeid = '$curmenu'";
$result = Treat_DB_ProxyOld::query($query);
//mysql_close();

$menuname=Treat_DB_ProxyOld::mysql_result($result,0,"menu_typename");

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=Treat_DB_ProxyOld::mysql_numrows($result);
//mysql_close();

if ($num!=1 || ($user == "" && $pass == "")) 
{
    echo "<center><h4>Failed</h4></center>";
}

else
{
    
    $mymonth=$newmonth;$myyear=$newyear;$mystart=$newstart;

    echo "<body onload=focus();additem6.menu_itemid.focus()>";  

    if($curdaypart>0){$fontsize=2;}
    else{$fontsize=1;}

    $bid=$caterbus;
    $cid=mysql_result($result,0,"companyid"); 
    $security_level=Treat_DB_ProxyOld::mysql_result($result,0,"security_level");

    if ($newmonth == 1){$month = "January";}
    elseif ($newmonth == 2){$month = "February";}
    elseif ($newmonth == 3){$month = "March";}
    elseif ($newmonth == 4){$month = "April";}
    elseif ($newmonth == 5){$month = "May";}
    elseif ($newmonth == 6){$month = "June";}
    elseif ($newmonth == 7){$month = "July";}
    elseif ($newmonth == 8){$month = "August";}
    elseif ($newmonth == 9){$month = "September";}
    elseif ($newmonth == 10){$month = "October";}
    elseif ($newmonth == 11){$month = "November";}
    elseif ($newmonth == 12){$month = "December";}

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query23 = "SELECT * FROM menu_daypart WHERE menu_typeid = '$curmenu' ORDER BY orderid DESC";
    $result23 = Treat_DB_ProxyOld::query($query23);
    $num23=Treat_DB_ProxyOld::mysql_numrows($result23);
    //mysql_close();

    $num23--;

    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=99%><tr><td colspan=2><a href=businesstrack.php><img src=logo.jpg border=0 height=43 width=205></a><p></td></tr>";
    echo "</table><p><table style=\"border:2px solid #CCCCCC;\" width=99% cellspacing=0 cellpadding=0>";
    echo "<tr bgcolor=#CCCCFF><td colspan=2><center><br><form action=busordermenu.php?newmonth=$newmonth&newyear=$newyear&newstart=$newstart&curdaypart=0 method=post><h3>$menuname <select name=curdaypart onChange='changePage(this.form.curdaypart)'><option value=busordermenu.php?newmonth=$newmonth&newyear=$newyear&newstart=$newstart&curdaypart=0>All Menus</option>";

    $leap=isLeapYear($year);
    if($leap==29){$leap=1;}
    else{$leap=0;}

    while($num23>=0){
       $daypartid=Treat_DB_ProxyOld::mysql_result($result23,$num23,"menu_daypartid"); 
       $daypartname=Treat_DB_ProxyOld::mysql_result($result23,$num23,"menu_daypartname");
       if ($curdaypart==$daypartid){$showsel="SELECTED";}
       else{$showsel="";}
       echo "<option value=busordermenu.php?newmonth=$newmonth&newyear=$newyear&newstart=$newstart&curdaypart=$daypartid $showsel>$daypartname</option>";
       $num23--;
    }

    echo "</select> - $month $year</h3></center></form></td></tr>";

////////////////////////ADD ITEMS
    if ($curdaypart!=""&&$curdaypart!=0){

          echo "<tr bgcolor=#CCCCFF><td colspan=2><a name=add><center><FORM ACTION=orderadd.php METHOD=post onSubmit='return disableForm(this);' name=additem6><input type=hidden name=businessid value=$caterbus><input type=hidden name=daypartid value=$curdaypart>";
          echo "<input type=hidden name=username value=$user>";
          echo "<input type=hidden name=password value=$pass>";
          echo "<input type=hidden name=date value=$date>";
          echo "<input type=hidden name=newstart value=$newstart>";
          echo "<input type=hidden name=newmonth value=$newmonth>";
          echo "<input type=hidden name=newyear value=$newyear>";
          echo "<input type=hidden name=direct value=1>";
          echo "<input type=hidden name=weekend value=$weekend>";
          echo "<SCRIPT LANGUAGE='JavaScript' ID='js18'> var cal18 = new CalendarPopup('testdiv1');cal18.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date5 VALUE='$adddate' SIZE=8> <A HREF='javascript:void();' onClick=cal18.select(document.forms[1].date5,'anchor18','yyyy-MM-dd'); return false; TITLE=cal18.select(document.forms[1].date5,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor18' ID='anchor18'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A>";
          echo " <select name=menu_itemid>";
          $lastgroupid=0;

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query4 = "SELECT * FROM menu_items WHERE businessid = '$caterbus' AND deleted = '0' ORDER BY groupid, item_name DESC";
          $result4 = Treat_DB_ProxyOld::query($query4);
          $num4=Treat_DB_ProxyOld::mysql_numrows($result4);
          //mysql_close();

          $num4--;
          while ($num4>=0){

             $item_name2=Treat_DB_ProxyOld::mysql_result($result4,$num4,"item_name");
             $menu_item_id2=Treat_DB_ProxyOld::mysql_result($result4,$num4,"menu_item_id");
             $price2=Treat_DB_ProxyOld::mysql_result($result4,$num4,"price");
             $menu_unit2=Treat_DB_ProxyOld::mysql_result($result4,$num4,"unit");
             $groupid2=Treat_DB_ProxyOld::mysql_result($result4,$num4,"groupid");
       
             if ($lastgroupid!=$groupid2){

                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query8 = "SELECT groupname FROM menu_groups WHERE menu_group_id = '$groupid2'";
                $result8 = Treat_DB_ProxyOld::query($query8);
                //mysql_close();

                $groupname=Treat_DB_ProxyOld::mysql_result($result8,0,"groupname");
                echo "<option value=0>===============================$groupname</option>";
                $lastgroupid=$groupid2;
             }
             else{
                echo "<option value=$menu_item_id2>$item_name2</option>";
                $num4--;
             }

          } 
          echo "</select> <input type=submit value='Add'></center></a></td></tr><tr bgcolor=#CCCCFF><td colspan=2></form></td></tr>";

    }
////////////////////////////////
    echo "<tr bgcolor=#CCCCFF><td colspan=2>";

    echo "<center><table border=1 cellpadding=0 cellspacing=0>";
    echo "<tr bgcolor=#FFFF99><td><center>Sunday</center></td><td><center>Monday</center></td><td><center>Tuesday</center></td><td><center>Wednesday</center></td><td><center>Thursday</center></td><td><center>Friday</center></td><td><center>Saturday</center></td></tr>";
    
    echo "<tr height=110>";
    $weekend = "F";
    while ($long < 8)
    {  
       if ($long >= $start)
       {
          if ($long == 1 || $long == 7){$weekend="T";}

          if ($day < 10 && $newmonth < 10){$date = "$newyear-0$newmonth-0$day";}
          elseif ($day < 10 && $newmonth > 10){$date = "$newyear-$newmonth-0$day";}
          elseif ($day > 10 && $newmonth < 10){$date = "$newyear-0$newmonth-$day";}
          else {$date = "$newyear-$newmonth-$day";}

          if ($newstart == ""){$newstart=$start;}
          $correcturl = "orderdate.php?date=$date&newmonth=$newmonth&newyear=$newyear&newstart=$newstart&day=$day&weekend=$weekend";

          if ($day == $todaynum2 && $monthnum == $newmonth && $year == $newyear){$color = "red";}
          else {$color = "black";}

          echo "<a style=$style href=$correcturl><td bgcolor=$back width=130 valign=top onMouseOver=this.bgColor='#999999' onMouseOut=this.bgColor='$back'>";
          
          echo "<a style=$style href=$correcturl><b><font color=$color>$day</font></b></a><br>";
/////////////////////////////////////////////////////////////
      
    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    if ($curdaypart!=""&&$curdaypart!=0){$query = "SELECT * FROM menu_daypart WHERE menu_daypartid = '$curdaypart'";}
    else{$query = "SELECT * FROM menu_daypart WHERE menu_typeid = '$curmenu' ORDER BY orderid DESC";}
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    $num--;
    while ($num>=0){
       $menu_daypartid=Treat_DB_ProxyOld::mysql_result($result,$num,"menu_daypartid"); 
       $menu_daypartname=Treat_DB_ProxyOld::mysql_result($result,$num,"menu_daypartname"); 

       if ($curdaypart==0||$curdaypart==""){echo "<b><font size=1 color=blue><u>$menu_daypartname</u></font></b><br>";}

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM order_item WHERE date = '$date' AND daypartid = '$menu_daypartid' AND businessid = '$caterbus' ORDER BY orderid DESC";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
       //mysql_close();

       $num2--;
       while ($num2>=0){
          $order_itemid=Treat_DB_ProxyOld::mysql_result($result2,$num2,"order_itemid"); 
          $prev_itemid=Treat_DB_ProxyOld::mysql_result($result2,$num2+1,"order_itemid"); 
          $menu_itemid=Treat_DB_ProxyOld::mysql_result($result2,$num2,"menu_itemid"); 
          $orderid=Treat_DB_ProxyOld::mysql_result($result2,$num2,"orderid"); 

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query3 = "SELECT * FROM menu_items WHERE menu_item_id = '$menu_itemid'";
          $result3 = Treat_DB_ProxyOld::query($query3);
          //mysql_close();

          $item_name=Treat_DB_ProxyOld::mysql_result($result3,0,"item_name"); 

          if ($fontsize==2){
             $deleteitem="<a href=orderadd.php?bid=$caterbus&edit=2&deleteid=$order_itemid&date=$date&number=$number&newmonth=$newmonth&newyear=$newyear&newstart=$newstart&weekend=$weekend&daypartid=$menu_daypartid&direct=1 style=$style><sup><b><font color=red>x</font></b></sup></a>";
             if($prev_itemid>0){$moveitem="<a href=orderadd.php?bid=$caterbus&edit=3&deleteid=$order_itemid&date=$date&number=$number&newmonth=$newmonth&newyear=$newyear&newstart=$newstart&weekend=$weekend&daypartid=$menu_daypartid&direct=1&prev_itemid=$prev_itemid style=$style><sup><b><font color=green>+</font></b></sup></a>";}
             else{$movitem="";}
          }

          echo "<font size=$fontsize>$item_name $deleteitem $moveitem</font><br>";
          
          $num2--;
       }
       $num--;
    }

/////////////////////////////////////////////////////////////
          $counter=0;

          echo "</td></a>";
          $back = "white";

          if ($day == 1 && $long == 1){$backstart=7;}
          elseif ($day == 1 && $long != 1){$backstart=$long-1;}

          $day++;
       }
       else
       {
          echo "<td width=110 bgcolor=#DCDCDC></td>";
       }
       $weekend="F";
       $long++;
    }
    echo "</tr>";
    
    while ($high > 1)
    {
       $long=1;
       echo "<tr height=110>";
       $weekend = "F";
       while ($long < 8)
       {
          if ($long == 1 || $long == 7){$weekend="T";}

          if ($day < 10 && $newmonth < 10){$date = "$newyear-0$newmonth-0$day";}
          elseif ($day < 10 && $newmonth > 10){$date = "$newyear-$newmonth-0$day";}
          elseif ($day >= 10 && $newmonth < 10){$date = "$newyear-0$newmonth-$day";}
          else {$date = "$newyear-$newmonth-$day";}      

          if ($newstart == ""){$newstart=$start;}
          $correcturl = "orderdate.php?date=$date&newmonth=$newmonth&newyear=$newyear&newstart=$newstart&day=$day&weekend=$weekend";

          $show = $day;
          if ($day > 28 && $newmonth == 2 && $leap == 0){$show = "";}
          elseif ($day > 29 && $newmonth == 2 and $leap == 1){$show = "";}
          elseif ($day > 30 && ($newmonth == 4 || $newmonth == 6 || $newmonth == 9 || $newmonth == 11)){$show = "";}
          elseif ($day > 31){$show = "";}
          else {;}
          
          if ($day > 28 && $newmonth == 2 && $leap == 0){$back = '#DCDCDC';}
          elseif ($day > 29 && $newmonth == 2 and $leap == 1){$back = '#DCDCDC';}
          elseif ($day > 30 && ($newmonth == 4 || $newmonth == 6 || $newmonth == 9 || $newmonth == 11)){$back = '#DCDCDC';}
          elseif ($day > 31){$back = '#DCDCDC';}
          else {$back = 'white';}

          if ($day == 29 && $newmonth == 2 && $leap ==0){$newstart=$long;}
          if ($day == 30 && $newmonth == 2 && $leap ==1){$newstart=$long;}
          if ($day == 31 && ($newmonth == 4 || $newmonth == 6 || $newmonth == 9 || $newmonth == 11)){$newstart=$long;}
          if ($day == 32 && ($newmonth == 1 || $newmonth == 3 || $newmonth == 5 || $newmonth == 7 || $newmonth == 8 || $newmonth == 10 || $newmonth == 12)){$newstart=$long;}     
         
          if ($day == $todaynum2 && $monthnum == $newmonth && $year == $newyear){$color = "red";}
          else {$color = "black";}

          echo "<a style=$style href=$correcturl><td bgcolor=$back width=130 valign=top onMouseOver=this.bgColor='#999999' onMouseOut=this.bgColor='$back'>";
          
          echo "<a style=$style href=$correcturl><b><font color=$color>$show</font></b></a><br>";
/////////////////////////////////////////////////////////////
if ($back!="#DCDCDC"){     
    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    if ($curdaypart!=""&&$curdaypart!=0){$query = "SELECT * FROM menu_daypart WHERE menu_daypartid = '$curdaypart'";}
    else{$query = "SELECT * FROM menu_daypart WHERE menu_typeid = '$curmenu' ORDER BY orderid DESC";}
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    $num--;
    while ($num>=0){
       $menu_daypartid=Treat_DB_ProxyOld::mysql_result($result,$num,"menu_daypartid"); 
       $menu_daypartname=Treat_DB_ProxyOld::mysql_result($result,$num,"menu_daypartname"); 

       if ($curdaypart==0||$curdaypart==""){echo "<b><font size=1 color=blue><u>$menu_daypartname</u></font></b><br>";}

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM order_item WHERE date = '$date' AND daypartid = '$menu_daypartid' AND businessid = '$caterbus' ORDER BY orderid DESC";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
       //mysql_close();

       $num2--;
       while ($num2>=0){
          $order_itemid=Treat_DB_ProxyOld::mysql_result($result2,$num2,"order_itemid");
          $prev_itemid=Treat_DB_ProxyOld::mysql_result($result2,$num2+1,"order_itemid");  
          $menu_itemid=Treat_DB_ProxyOld::mysql_result($result2,$num2,"menu_itemid"); 
          $orderid=Treat_DB_ProxyOld::mysql_result($result2,$num2,"orderid"); 

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query3 = "SELECT * FROM menu_items WHERE menu_item_id = '$menu_itemid'";
          $result3 = Treat_DB_ProxyOld::query($query3);
          //mysql_close();

          $item_name=Treat_DB_ProxyOld::mysql_result($result3,0,"item_name"); 

          if ($fontsize==2){
             $deleteitem="<a href=orderadd.php?bid=$caterbus&edit=2&deleteid=$order_itemid&date=$date&number=$number&newmonth=$newmonth&newyear=$newyear&newstart=$newstart&weekend=$weekend&daypartid=$menu_daypartid&direct=1 style=$style><sup><b><font color=red>x</font></b></sup></a>";
             if($prev_itemid>0){$moveitem="<a href=orderadd.php?bid=$caterbus&edit=3&deleteid=$order_itemid&date=$date&number=$number&newmonth=$newmonth&newyear=$newyear&newstart=$newstart&weekend=$weekend&daypartid=$menu_daypartid&direct=1&prev_itemid=$prev_itemid style=$style><sup><b><font color=green>+</font></b></sup></a>";}
             else{$movitem="";}
          }

          echo "<font size=$fontsize>$item_name $deleteitem $moveitem</font><br>";
          
          $num2--;
       }

       $num--;
    }
}
/////////////////////////////////////////////////////////////
          $counter=0;
         
          echo "</td></a>";
          $back = "white";

          $day++;
          $weekend = "F";
          $long++;
       }
       echo "</tr>";
       $high--;
    } 
    echo "</table></center>";

    if ($newmonth == 5 || $newmonth == 7 || $newmonth == 10 || $newmonth == 12){$count=30;}
    elseif ($newmonth == 1 || $newmonth == 2 || $newmonth == 4 || $newmonth == 6 || $newmonth == 8 || $newmonth == 9 || $newmonth == 11){$count=31;}
    elseif ($newmonth == 3 && $leap == 0){$count=28;}
    elseif ($newmonth == 3 && $leap == 1){$count=29;}

    $count2 = $backstart;
    while ($count > 1)
    {
       if ($count2 == 1){$count2 = 8;}
       $count--;
       $count2--;
    }
    $backstart = $count2;

    $prevmonth = $newmonth - 1;
    $prevyear = $newyear;
    if ($prevmonth == 0){$prevmonth = 12; $prevyear = $year - 1;}
    $nextmonth = $newmonth + 1;
    $nextyear = $newyear;
    if ($nextmonth == 13){$nextmonth = 1; $nextyear=$year + 1;}
    echo "<center><a href='busordermenu.php?newmonth=$prevmonth&newyear=$prevyear&newstart=$backstart'>Previous Month</a> :: <a href='busordermenu.php?newmonth=$nextmonth&newyear=$nextyear&newstart=$newstart'>Next Month</a></center><p>";

    echo "</td></tr>";
    echo "</table><p>";

////////////////////////////////////////////COPY MENUS
  if ($fontsize==2){
    echo "<center><table width=99% cellspacing=0 cellpadding=0 style=\"border:2px solid #CCCCCC;\"><tr bgcolor=#E8E7E7><td><FORM action=copymenu.php method='post' onSubmit='return disableForm(this);'>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<input type=hidden name=businessid value=$caterbus>";
    echo "<input type=hidden name=companyid value=$companyid>";
    echo "<input type=hidden name=curdaypart value=$curdaypart>";
    echo "<input type=hidden name=newmonth value=$mymonth>";
    echo "<input type=hidden name=newyear value=$myyear>";
    echo "<input type=hidden name=newstart value=$mystart>";

    echo "<a name=copy>Copy this menu to <select name=menu_type onChange='changePage(this.form.menu_type)'>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query3 = "SELECT * FROM menu_type WHERE type = '1' ORDER BY menu_typename DESC";
    $result3 = Treat_DB_ProxyOld::query($query3);
    $num3=Treat_DB_ProxyOld::mysql_numrows($result3);
    //mysql_close();

    while ($num3>=0){
       $menu_typeid=Treat_DB_ProxyOld::mysql_result($result3,$num3,"menu_typeid");
       $menu_typename=Treat_DB_ProxyOld::mysql_result($result3,$num3,"menu_typename");
       if ($menu==$menu_typeid){$showsel="SELECTED";}
       else{$showsel="";}
       echo "<option value=busordermenu.php?newstart=$mystart&newyear=$myyear&newmonth=$mymonth&cid=$companyid&bid=$businessid&menu=$menu_typeid#copy $showsel>$menu_typename</option>";
       $num3--;
    }
    echo "</select>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query3 = "SELECT * FROM menu_daypart WHERE menu_typeid = '$menu' AND menu_daypartid != '$curdaypart' ORDER BY orderid DESC";
    $result3 = Treat_DB_ProxyOld::query($query3);
    $num3=Treat_DB_ProxyOld::mysql_numrows($result3);
    //mysql_close();

    $num3--;
    echo " <select name=daypart>";
    while ($num3>=0){
       $menu_daypartid=Treat_DB_ProxyOld::mysql_result($result3,$num3,"menu_daypartid");
       $menu_daypartname=Treat_DB_ProxyOld::mysql_result($result3,$num3,"menu_daypartname");
       echo "<option value=$menu_daypartid>$menu_daypartname</option>";
       $num3--;
    }
    echo "</select>";

    echo " <INPUT TYPE=submit VALUE='Copy'></a></td><td width=1></form></td></tr></table></center><p>";
  }

/////////////////////COPY 1 DAY TO MANY
    if($fontsize==2){$formnum=3;}
    else{$formnum=1;}

    echo "<center><table width=99% bgcolor=#E8E7E7 style=\"border:2px solid #CCCCCC;\" cellspacing=0 cellpadding=0><tr><td><form action=ordercopyday.php method=post onSubmit='return disableForm(this);'>";
    echo "<input type=hidden name=businessid value=$caterbus>";
    echo "<input type=hidden name=companyid value=$companyid>";
    echo "<input type=hidden name=curdaypart value=$curdaypart>";
    echo "<input type=hidden name=newmonth value=$mymonth>";
    echo "<input type=hidden name=newyear value=$myyear>";
    echo "<input type=hidden name=newstart value=$mystart>";
    echo "<input type=hidden name=curmenu value=$curmenu>";

    echo "Copy All Menus From ";

    echo "<SCRIPT LANGUAGE='JavaScript' ID='js19'> var cal19 = new CalendarPopup('testdiv1');cal19.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date9 VALUE='$adddate' SIZE=8> <A HREF='javascript:void();' onClick=cal19.select(document.forms[$formnum].date9,'anchor19','yyyy-MM-dd'); return false; TITLE=cal19.select(document.forms[$formnum].date9,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor19' ID='anchor19'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A> starting ";
    echo "<SCRIPT LANGUAGE='JavaScript' ID='js20'> var cal20 = new CalendarPopup('testdiv1');cal20.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date10 VALUE='$adddate' SIZE=8> <A HREF='javascript:void();' onClick=cal20.select(document.forms[$formnum].date10,'anchor20','yyyy-MM-dd'); return false; TITLE=cal20.select(document.forms[$formnum].date10,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor20' ID='anchor20'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A> ";
    echo "for <input type=text size=5 name=days> Days, Include Weekends <input type=checkbox name=weekenddays value=1> <input type=submit value='Copy'></td>";

    echo "<td width=1></form></td></tr></table></center>";
///////////////////////////////////////////////////// 


    $action2 = "buscatermenu.php?bid=$caterbus&cid=$cid";
    $method = "post";
    echo "<center><FORM ACTION='$action2' method=$method>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<INPUT TYPE=submit VALUE=' Return '></FORM></center><DIV ID=testdiv1 STYLE=position:absolute;visibility:hidden;background-color:white;layer-background-color:white;></DIV></body>";
	
	google_page_track();
}
?>