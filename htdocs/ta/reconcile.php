<?php

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+3);
        if ($diff > 0 && $diff < '0.01'){$diff="0.01";}
        elseif($diff < 0 && $diff > '-0.01'){$diff="-0.01";}
        return $diff;
}

function nextday($nextd,$day_format=''){ //Function returns next day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monn = substr($nextd, 5, 2);
   $dayn = substr($nextd, 8, 2);
   $yearn = substr($nextd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monn, $dayn+1, $yearn));
   return $tempdate;
}

function prevday($prevd,$day_format=''){ //Function returns previous day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monp = substr($prevd, 5, 2);
   $dayp = substr($prevd, 8, 2);
   $yearp = substr($prevd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monp, $dayp-1, $yearp));
   return $tempdate;
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      $day=$day-31;  
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";} 
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}


if ( !defined('DOC_ROOT'))
{
	define('DOC_ROOT', dirname(dirname(__FILE__)));
}
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');
$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");
$creditamount="creditamount";
$creditidnty="creditidnty";
$creditdate="creditdate";
$debitamount="debitamount";
$debitidnty="debitidnty";
$debitdate="debitdate";
$quatertotal=0;
$monthtotal=0;
$total=0;
$counter=0;
$findme='.';
$double='.00';
$single='0';
$double2='00';

$user = isset($_COOKIE["usercook"])?$_COOKIE["usercook"]:'';
$pass = isset($_COOKIE["passcook"])?$_COOKIE["passcook"]:'';
$date1 = isset($_COOKIE["date1cook"])?$_COOKIE["date1cook"]:'';
$date2 = isset($_COOKIE["date2cook"])?$_COOKIE["date2cook"]:'';
$acct = isset($_COOKIE["acctcook"])?$_COOKIE["acctcook"]:'';
$rec_access = isset($_COOKIE["rec_access"])?$_COOKIE["rec_access"]:'';
$businessid = isset($_GET["bid"])?$_GET["bid"]:'';
$companyid = isset($_GET["cid"])?$_GET["cid"]:'';
$finalresults = isset($_GET["results"])?$_GET["results"]:'';
$finalmessage = isset($_GET["message"])?$_GET["message"]:'';
$pmessage = isset($_GET["pmessage"])?$_GET["pmessage"]:'';
$findamount = isset($_GET["findamount"])?$_GET["findamount"]:'';

if ($businessid==""||$companyid==""){
   $businessid = isset($_POST["businessid"])?$_POST["businessid"]:'';
   //$companyid = isset($_POST["companyid"])?$_POST["companyid"]:'';
   $companyid = isset($_COOKIE["compcook"])?$_COOKIE["compcook"]:'';
   $update=1;
}
if($rec_access == "" || $rec_access == "logged_out"){
   $rec_access = isset($_POST["rec_access"])?$_POST["rec_access"]:'';
   $update=1;
}
if(isset($_POST["date2"])){
   $acct = isset($_POST["acct"])?$_POST["acct"]:'';
   $date1 = isset($_POST["date1"])?$_POST["date1"]:'';
   $date2 = isset($_POST["date2"])?$_POST["date2"]:'';
   $new = isset($_POST["new"])?$_POST["new"]:'';
   $update=1;
   
   setcookie("date1cook",$date1);
   setcookie("date2cook",$date2);
   setcookie("acctcook",$acct);
}

mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = mysql_query($query);
$num=mysql_numrows($result);
//mysql_close();

$mysecurity=@mysql_result($result,0,"security");

////NEW SECURITY
    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query56 = "SELECT * FROM security WHERE securityid = '$mysecurity'";
    $result56 = mysql_query($query56);
    //mysql_close();

$security_level=@mysql_result($result56,0,"reconcile");
$bid=@mysql_result($result,0,"businessid");
$cid=@mysql_result($result,0,"companyid");
$loginid=@mysql_result($result,0,"loginid");

if ($num != 1 || $user == "" || $pass == "" || $security_level != 1)
{
    echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
    
    setcookie("rec_access",$rec_access);

if ($date1==""){$date1=$today;}
if ($date2==""){$date2=$today;}
if ($acct==""){$acct="10500";}

?>

<html>
<head>
<SCRIPT LANGUAGE=javascript><!--
function confirmclose(){return confirm('PLEASE USE THE LOGOUT BUTTON! PRESS THE CANCEL BUTTON.');}
// --></SCRIPT>

<script type="text/javascript">    
var szColorTo = '#0000FF';    
var szColorOriginal = '#FF0000';        
function change_row_colour(pTarget)    {       
  var pTR = pTarget.parentNode;               
  if(pTR.nodeName.toLowerCase() != 'td')        {            return;        } 
     if(pTarget.checked == true)        {            pTR.style.backgroundColor = szColorTo;        }   
     else        {            pTR.style.backgroundColor = szColorOriginal;        }    
}

</script>

<SCRIPT LANGUAGE="JavaScript" SRC="CalendarPopup.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">document.write(getCalendarStyles());</SCRIPT>

<STYLE>
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation
			{
			background-color:#6677DD;
			text-align:center;
			vertical-align:center;
			text-decoration:none;
			color:#FFFFFF;
			font-weight:bold;
			}
	.TESTcpDayColumnHeader,
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation,
	.TESTcpCurrentMonthDate,
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDate,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDate,
	.TESTcpCurrentDateDisabled,
	.TESTcpTodayText,
	.TESTcpTodayTextDisabled,
	.TESTcpText
			{
			font-family:arial;
			font-size:8pt;
			}
	TD.TESTcpDayColumnHeader
			{
			text-align:right;
			border:solid thin #6677DD;
			border-width:0 0 1 0;
			}
	.TESTcpCurrentMonthDate,
	.TESTcpOtherMonthDate,
	.TESTcpCurrentDate
			{
			text-align:right;
			text-decoration:none;
			}
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDateDisabled
			{
			color:#D0D0D0;
			text-align:right;
			text-decoration:line-through;
			}
	.TESTcpCurrentMonthDate
			{
			color:#6677DD;
			font-weight:bold;
			}
	.TESTcpCurrentDate
			{
			color: #FFFFFF;
			font-weight:bold;
			}
	.TESTcpOtherMonthDate
			{
			color:#808080;
			}
	TD.TESTcpCurrentDate
			{
			color:#FFFFFF;
			background-color: #6677DD;
			border-width:1;
			border:solid thin #000000;
			}
	TD.TESTcpCurrentDateDisabled
			{
			border-width:1;
			border:solid thin #FFAAAA;
			}
	TD.TESTcpTodayText,
	TD.TESTcpTodayTextDisabled
			{
			border:solid thin #6677DD;
			border-width:1 0 0 0;
			}
	A.TESTcpTodayText,
	SPAN.TESTcpTodayTextDisabled
			{
			height:20px;
			}
	A.TESTcpTodayText
			{
			color:#6677DD;
			font-weight:bold;
			}
	SPAN.TESTcpTodayTextDisabled
			{
			color:#D0D0D0;
			}
	.TESTcpBorder
			{
			border:solid thin #6677DD;
			}
</STYLE>
</head>

<?
    $inthegreen=0;
    $inthered=0;

    echo "<body onLoad='focus();mark.findamount.focus()' onClose='confirmclose()'>";
    echo "<div style=border:50px solid red;padding:10px;>";
    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%><tr><td colspan=2>";
    echo "<a href=businesstrack.php><img src=logo.jpg border=0></a><p></td></tr>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM company WHERE companyid = '$companyid'";
    $result = mysql_query($query);
    //mysql_close();

    $companyname=@mysql_result($result,0,"companyname");
    $rec_accesscode=@mysql_result($result,0,"rec_accesscode");

    //echo "<tr bgcolor=black><td colspan=4 height=1></td></tr>";
    echo "<tr bgcolor=#CCCCFF><td colspan=2><font size=4><b>Reconcile - $companyname</b></font></td><td align=right><form action=logout_rec.php method=post><b>LOGOUT WHEN DONE>>></font></b><input type=submit value='Log Out'></td><td width=0></form></td></tr>";
    echo "<tr bgcolor=black><td colspan=4 height=1></td></tr>";
    echo "</table></center><p>";

if ($rec_accesscode!=$rec_access){echo "<center><table width=90%><tr><td><FORM ACTION='reconcile.php' method='post'>Access: <input type=hidden name=new value=1><input type=hidden name=companyid value=$companyid><input type=text name=rec_access size=10><input type=submit value='Enter'></form></td></tr></table></center>"; setcookie ("date1cook", "", time() - 3600);}
else{
  ////////////////////////USERS//////////////////////////
    if ($new==1){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "INSERT INTO reconcile_user (loginid,user,account) VALUES ('$loginid','$user','$acct')";
       $result = mysql_query($query);
       //mysql_close();
    }
       
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "UPDATE reconcile_user SET time = NOW( ), account = '$acct' WHERE user LIKE '$user'";
       $result = mysql_query($query);
       //mysql_close();

  ////////////////////////USERS END///////////////////////

    echo "<center><table width=90% border=0 cellspacing=0 cellpadding=0><tr><td width=30% rowspan=2><table width=100% border=0 cellspacing=0 cellpadding=0><tr height=1><td colspan=3 bgcolor=black></td></tr><tr><td bgcolor=black width=1></td><td bgcolor=yellow><font size=1><b>CURRENT USERS</b></td><td bgcolor=black width=1></td></tr><tr height=1><td colspan=3 bgcolor=black></td></tr><tr><td bgcolor=black width=1></td><td><iframe src='rec_users.php?users=$totusers' WIDTH=100% HEIGHT=60 frameborder=0></iframe></td><td bgcolor=black width=1></td></tr><tr height=1><td colspan=3 bgcolor=black></td></tr></table></td>";
   
    if ($finalresults>1){$myfontcolor="red";}
    else{$myfontcolor="blue";}
    if ($finalmessage!=""){$finalmessage=", $finalmessage";}
    if ($finalresults!=""){$finalmessage=str_replace("_", " ", $finalmessage);$finalmessage=str_replace("@", "&", $finalmessage);$showfinal="<font size=2 color=$myfontcolor><font color=black>$$findamount Results:</font> $finalresults$finalmessage</font>";}
    else{$showfinal="";}

    $pmessage=str_replace("_", " ", $pmessage);
    $pmessage=str_replace("@", "&", $pmessage);
    $pmessage=str_replace(",", "<br>", $pmessage);
    if ($pmessage!=""&&$finalresults==0){$showpossible="<br><table border=1 bgcolor=#00FFFF><tr><td><font size=2><u><b>Possibilities</b></u>: $pmessage</font></td></tr></table>";}
    else{$showpossible="";}

    echo "<td align=right><FORM ACTION='reconcile.php' method='post'>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<input type=hidden name=rec_access value=$rec_access>";
    echo "<input type=hidden name=businessid value=$businessid>";
    echo "<input type=hidden name=companyid value=$companyid>";  
    echo "<SCRIPT LANGUAGE='JavaScript' ID='js18'> var cal18 = new CalendarPopup('testdiv1');cal18.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date1 VALUE='$date1' SIZE=8> <A HREF=# onClick=cal18.select(document.forms[1].date1,'anchor18','yyyy-MM-dd'); return false; TITLE=cal18.select(document.forms[1].date1,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor18' ID='anchor18'><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A> to ";
    echo "<SCRIPT LANGUAGE='JavaScript' ID='js19'> var cal19 = new CalendarPopup('testdiv1');cal19.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date2 VALUE='$date2' SIZE=8> <A HREF=# onClick=cal19.select(document.forms[1].date2,'anchor19','yyyy-MM-dd'); return false; TITLE=cal19.select(document.forms[1].date2,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor19' ID='anchor19'><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A>";

    echo " for Acct# <input type=text name=acct value='$acct' size=9> <INPUT TYPE=submit VALUE='GO'></FORM></td></tr><tr><td align=right><form action=mark_bankrec.php method=post name=mark><input type=hidden name=companyid value=$companyid> $showfinal <input type=text size=8 name=findamount><input type=submit value='Check'>$showpossible</form></td></tr></table>";
    echo "<DIV ID=testdiv1 STYLE=position:absolute;visibility:hidden;background-color:white;layer-background-color:white;></DIV>";
///////////START TABLE    
    $tempdate=$date1;
    
    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM business WHERE companyid = '$companyid' ORDER BY unit DESC";
    $result = mysql_query($query);
    $num=mysql_numrows($result);
    //mysql_close();

    echo "<form action=savereconcile.php method=post><input type=hidden name=companyid value=$companyid>";
    echo "<center><table width=90% style=\"border:1px solid #999999;\" cellspacing=0>";

    $tempdate=$date1;
    echo "<tr bgcolor=#E8E7E7><td style=\"border:1px solid #999999;\"><font size=2><b>Unit</b></font></td>";
    while($tempdate<=$date2){
       $showday=substr($tempdate,8,2);
       $showmonth=substr($tempdate,5,2);
       $showyear=substr($tempdate,2,2);
       echo "<td align=right colspan=2 style=\"border:1px solid #999999;\"><font size=2><b>$showmonth/$showday/$showyear</b></td>";
       $tempdate=nextday($tempdate);
    }
    echo "<td align=right style=\"border:1px solid #999999;\"><b><font size=2>Totals</font></td></tr>";

    $counter=1;
    $grandtotalcolor="#00FF00";
    $coltotal[]="";
    $num--;
    while($num>=0){

       $businessid=@mysql_result($result,$num,"businessid");
       $businessname=@mysql_result($result,$num,"businessname");
       $unit=@mysql_result($result,$num,"unit");


       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query5 = "SELECT * FROM debits WHERE businessid = '$businessid' AND account LIKE '$acct%'";
       $result5 = mysql_query($query5);
       $num5=mysql_numrows($result5);
       //mysql_close();

       $num5--;
       while($num5>=0){
          $tempdate=$date1;
          $acctid=@mysql_result($result5,$num5,"debitid");
          $acctname=@mysql_result($result5,$num5,"debitname");

          echo "<tr><td width=35% style=\"border:1px solid #999999;\"><font size=2>$unit $businessname - $acctname</td>";

          $rowtotal=0;
          while($tempdate<=$date2){
             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query2 = "SELECT * FROM debitdetail WHERE debitid = '$acctid' AND date = '$tempdate'";
             $result2 = mysql_query($query2);
             $num2=mysql_numrows($result2);
             //mysql_close();

             $num2--;
             if ($num2<0){echo "<td width=1% style=\"border:1px solid #999999;\"><font size=2>&nbsp;</font></td><td width=1% style=\"border:1px solid #999999;\" bgcolor=#FFFF99><font size=2>&nbsp;</font></td>";$counter++;}
             else{
             while ($num2>=0){
                $debitdetailid=@mysql_result($result2,$num2,"debitdetailid");
                $amount=@mysql_result($result2,$num2,"amount");
                $bank_rec=@mysql_result($result2,$num2,"bank_rec");

                $mycolor="red";
                if ($bank_rec==1){$mycolor="#00FF00";$showcheck="CHECKED";$inthegreen++;}
                elseif($bank_rec==2){$mycolor="#00FFFF";$showcheck="";$grandtotalcolor="red";$inthered++;}
                elseif($bank_rec==0&&$amount==0){$mycolor="yellow";$showcheck="";}
                else{$showcheck="";$grandtotalcolor="red";$inthered++;}

                $amount=money($amount);
                $rowtotal=$rowtotal+$amount;
                $coltotal[$tempdate]=$coltotal[$tempdate]+$amount;

                $varname="check$counter";
                $showamount=number_format($amount,2);

                echo "<td align=right width=1% bgcolor=$mycolor style=\"border:1px solid #999999;\"><font size=2><b>$$showamount</b></td><td width=1% bgcolor=$mycolor style=\"border:1px solid #999999;\"><input type=checkbox name=$varname value=1 $showcheck onclick='change_row_colour(this)'></td>";
  
                $num2--;
                $counter++;
             }
             }
             $tempdate=nextday($tempdate);
          }
          $rowtotal=money($rowtotal);
          $showrow=number_format($rowtotal,2);

          echo "<td align=right bgcolor=#E8E7E7 style=\"border:1px solid #999999;\"><font size=2><b>$$showrow</b></td></tr>";
          $num5--;
       }
       $num--;
    }

    $tempdate=$date1;
    $grandtotal=0;
    echo "<tr bgcolor=#E8E7E7><td style=\"border:1px solid #999999;\">&nbsp;</td>";
    $columnspan=2;
    while($tempdate<=$date2){
       $coltotal[$tempdate]=money($coltotal[$tempdate]);
       $grandtotal=$grandtotal+$coltotal[$tempdate];
       $showcol=number_format($coltotal[$tempdate],2);
       echo "<td align=right colspan=2 style=\"border:1px solid #999999;\"><font size=2><b>$$showcol</b></td>";
       $tempdate=nextday($tempdate);
       $columnspan=$columnspan+2;
    }
    $grandtotal=money($grandtotal);
    $grandtotal=number_format($grandtotal,2);
    $columnspan++;
    echo "<td align=right bgcolor=$grandtotalcolor style=\"border:1px solid #999999;\"><b>$$grandtotal</b></td></tr>";
    $inthetotal=$inthered+$inthegreen;
    echo "<tr bgcolor=#E8E7E7><td colspan='$columnspan-1' align=right style=\"border:1px solid #999999;\"><font size=2><b>Reconciled: $inthegreen of $inthetotal </b></font> <input type=submit value='Save'></td></tr></table></center></form>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM audit_reconcile WHERE companyid = '$companyid' AND acct LIKE '$acct' AND ((date1 >= '$date1' AND date1 <= '$date2') OR (date2 >= '$date1' AND date2 <= '$date2')) ORDER BY auditid DESC";
    $result = mysql_query($query);
    $num=mysql_numrows($result);
    //mysql_close();

    echo "<center><table width=90%><tr><td><font size=1><b>AUDIT</b></font></td></tr>";
    $num--;
    while($num>=0){
       $date5=@mysql_result($result,$num,"date");
       $date6=@mysql_result($result,$num,"date1");
       $date7=@mysql_result($result,$num,"date2");
       $saveuser=@mysql_result($result,$num,"user");

       echo "<tr><td><font size=1>Date Range $date6 through $date7 Saved by $saveuser on $date5.</td></tr>";
       $num--;
    }
    echo "</table></center>";
   
    mysql_close();
	google_page_track();
}
///////////END TABLE
    //echo "<br><FORM ACTION=businesstrack.php method=post>";
    //echo "<input type=hidden name=username value=$user>";
    //echo "<input type=hidden name=password value=$pass>";
    //echo "<center><INPUT TYPE=submit VALUE=' Return to Main Page'></FORM></center></body>";
}
?>