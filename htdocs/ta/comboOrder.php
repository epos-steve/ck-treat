			

<?php 

 /**
 * this file execute query ans is call for comboOrder.js
 * @author: Juan Ramon Lepe
 * @version: 01/08/2015
 * @company: essential elements 
 */
 
	require_once( __DIR__ . '/../../application/bootstrap.php');
	require_once( __DIR__ . '/../../application/controllers/ta/comboOrder.php' );	
	$obcomboOrder = new Ta_ComboOrderController();

	
	if(isset($_POST['IDElement']))
	{$obcomboOrder->lstrIDElement = $_POST['IDElement'];}

	if(isset($_POST['IDAction']))
	{$lstrAction=$_POST['IDAction'];}

	if(isset($_POST['ArrayElements']))
	{$obcomboOrder->lstrArrayElements = $_POST['ArrayElements'];}

	if(isset($_POST['ArrayElementsColor']))
	{$obcomboOrder->lstrArrayElementsColor = $_POST['ArrayElementsColor'];}

	if(isset($_POST['numberElements']))
	{$obcomboOrder->lstrNumberElements = $_POST['numberElements'];}



	$numberElements =0;

	switch ($lstrAction){
		
		case 'dragCombo':

			$obcomboOrder->dragCombo();
			
			break;

		case 'saveOrder':
			$obcomboOrder->saveOrder();
			
			break;
		
	}

	


?>
