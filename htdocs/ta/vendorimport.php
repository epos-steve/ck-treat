<?php
define( 'DOC_ROOT', realpath( dirname( __FILE__ ) . '/../' ) );
require_once( DOC_ROOT . '/bootstrap.php' );

ob_implicit_flush( true );

$style = "text-decoration:none";

$debugimport = isset( $_GET['debug'] );

$today = $_GET["date"];
$only = $_GET["only"];
$day = date( "d" );
$year = date( "Y" );
$month = date( "m" );
//if($today==""){$today="$year$month$day";}
$date = "$year-$month-$day";
$updatedate = $date;

//$emailmessage = "$today\n\n";
$emailmessage = '';

$import_total = 0;
$import_success = 0;

$vendorArray = array(
	'kc' => array(
		'vendor_name' => 'Sysco Kansas City',
		'ftp_server' => 'ftp.sysco.com',
		'ftp_user_name' => 'taexport',
		'ftp_user_pass' => 'sysco',
		'remote_file' => 'exportinv-%s_0615_dvt900912-057-34838-ALL.csv',
		'vendor_limit' => 'AND vendor = "395"'
	),
	'ne' => array(
		'vendor_name' => 'Sysco Pegler Omaha',
		'ftp_server' => 'ftp.sysco.com',
		'ftp_user_name' => 'taexport',
		'ftp_user_pass' => 'sysco',
		'remote_file' => 'exportinv-wk%s_csv-061-85484-ALL.csv',
		'vendor_limit' => ''
	),
	'pa' => array(
		'vendor_name' => 'Sysco PA',
		'ftp_server' => 'ftp.sysco.com',
		'ftp_user_name' => 'taexport',
		'ftp_user_pass' => 'sysco',
		'remote_file' => 'exportinv-%s_0615_grp146-051-36672-ALL.csv'
	),
	'stl' => array(
		'vendor_name' => 'Sysco St. Louis',
		'ftp_server' => 'ftp.sysco.com',
		'ftp_user_name' => 'taexport',
		'ftp_user_pass' => 'sysco',
		'remote_file' => 'exportinv-wk%s_0615_grp605-064-37923-ALL.csv',
		'vendor_limit' => ''
	),
	'ca' => array(
		'vendor_name' => 'Sysco Sacremento',
		'ftp_server' => 'ftp.sysco.com',
		'ftp_user_name' => 'taexport',
		'ftp_user_pass' => 'sysco',
		'remote_file' => 'exportinv-wk%s_0615_dvttreata-031-39420-ALL.csv',
		'vendor_limit' => ''
	),
	'in' => array(
		'vendor_name' => 'Sysco Indiana',
		'ftp_server' => 'ftp.sysco.com',
		'ftp_user_name' => 'taexport',
		'ftp_user_pass' => 'sysco',
		'remote_file' => 'exportinv-wk%s_0615_grp217-038-43309-ALL.csv',
		'vendor_limit' => 'AND (vendor = "926" OR vendor = "1061")'
	),
	'ia' => array(
		'vendor_name' => 'Sysco Iowa',
		'ftp_server' => 'ftp.sysco.com',
		'ftp_user_name' => 'taexport',
		'ftp_user_pass' => 'sysco',
		'remote_file' => 'exportinv-%s_0615_contreat-039-43825-ALL.csv',
		'vendor_limit' => ''
	),
	'bs' => array(
		'vendor_name' => 'Sysco Boston',
		'ftp_server' => 'ftp.sysco.com',
		'ftp_user_name' => 'taexport',
		'ftp_user_pass' => 'sysco',
		'remote_file' => 'exportinv-wk%s-056-85473-ALL.csv',
		'vendor_limit' => ''
	),
	'wv' => array(
		'vendor_name' => 'Sysco West Virginia',
		'ftp_server' => 'ftp.sysco.com',
		'ftp_user_name' => 'taexport',
		'ftp_user_pass' => 'sysco',
		'remote_file' => 'exportinv-wk%s-007-85376-ALL.csv',
		'vendor_limit' => ''
	),
	'mn' => array(
		'vendor_name' => 'Sysco Minnesota',
		'ftp_server' => 'ftp.sysco.com',
		'ftp_user_name' => 'taexport',
		'ftp_user_pass' => 'sysco',
		'remote_file' => 'exportinv-wk%s-047-85348-ALL.csv',
		'vendor_limit' => ''
	),
	'co' => array(
		'vendor_name' => 'Sysco Denver',
		'ftp_server' => 'ftp.sysco.com',
		'ftp_user_name' => 'taexport',
		'ftp_user_pass' => 'sysco',
		'remote_file' => 'exportinv-wk%s-059-85340-ALL.csv',
		'vendor_limit' => ''
	)
);

function updateVendor( $vendorCode ) {
	global $vendorArray, $today;
	
	if( $today != "" ) {
		importVendorCSV( $vendorCode, $today );
		return;
	}
	
	$todayDate = new DateTime( );
	$oneDay = new DateInterval( 'P1D');
	
	$query = "SELECT fileDate FROM vendor_import_dates WHERE importId='$vendorCode'";
	$result = Treat_DB_ProxyOld::query( $query );
	
	if( mysql_num_rows( $result ) == 1 ) {
		$fileDate = @mysql_result( $result, 0, 'fileDate' );
		$fDate = DateTime::createFromFormat( 'Y-m-d', $fileDate )->setTime( 0, 0, 0 );
		$fDate->add( $oneDay );
	}
	else {
		$fDate = clone $todayDate;
		$fDate->sub( $oneDay );
	}
	
	while( $fDate <= $todayDate ) {
		if( importVendorCSV( $vendorCode, $fDate->format( 'Ymd' ) ) ) {
			$query = "
				REPLACE INTO vendor_import_dates( importId, fileDate )
					VALUES( '$vendorCode', '" . $fDate->format( 'Y-m-d' ) . "' )
			";
			Treat_DB_ProxyOld::query( $query );
		}
		$fDate->add( $oneDay );
	}
}

function importVendorCSV( $vendorCode, $today ) {
	global $vendorArray, $emailmessage, $import_total, $import_success, $debugimport, $date, $updatedate;
	
	if( !isset( $vendorArray[$vendorCode] ) ) return false;
	
	$vendorSettings = $vendorArray[$vendorCode];
	
	echo "<font size=3 color=blue><b>Connecting to {$vendorSettings['vendor_name']}...</b></font><br>";
	$emailmessage .= "$today: {$vendorSettings['vendor_name']} ";
	$myFile = DIR_VENDOR_IMPORTS . "/sysco.csv";
	$handle = fopen( $myFile, 'w' );
	if( $handle === false ) {
		echo "<p>error: Couldn't open $myFile for write</p>";
		return false;
	}
	
	$conn_id = ftp_connect( $vendorSettings['ftp_server'] )
			or die( "Couldn't connect to {$vendorSettings['ftp_server']}" );
	if( $conn_id === false ) {
		echo "<p>error: Couldn't connect to {$vendorSettings['ftp_server']}</p>";
		fclose( $handle );
		return false;
	}
	
	$login_result = ftp_login( $conn_id, $vendorSettings['ftp_user_name'],
			$vendorSettings['ftp_user_pass'] );
	if( !$login_result ) {
		echo "<p>error: Couldn't login as {$vendorSettings['ftp_user_name']}</p>";
		fclose( $handle );
		ftp_close( $conn_id );
		return false;
	}
	
	if( !ftp_pasv( $conn_id, true ) ) {
		echo "<p>error: Couldn't switch to passive mode</p>";
		fclose( $handle );
		ftp_close( $conn_id );
		return false;
	}
	
	if( !ftp_chdir( $conn_id, 'Outbound' ) ) {
		echo "<p>error: Couldn't change directory</p>";
		fclose( $handle );
		ftp_close( $conn_id );
		return false;
	}
	
	$remote_file = sprintf( $vendorSettings['remote_file'], $today );
	echo "<p>Retrieving data from $remote_file...</p>";
	
	if( ftp_fget( $conn_id, $handle, $remote_file, FTP_ASCII, 0 ) ) {
		echo "<font size=2 color=green>$today file successfully written to $myFile</font><br>";
		$emailmessage .= "Success\n";
		$import_success++;
	}
	else {
		echo "<font size=2 color=red>There was a problem</font><br>";
		$emailmessage .= "Error\n";
		fclose( $handle );
		ftp_close( $conn_id );
		return false;
	}
	$import_total++;
	
	fclose( $handle );
	
	ftp_close( $conn_id );
	echo "<font size=2 color=blue>Connection Closed</font><br>";
	
	//////////////////BEGIN IMPORTING
	echo "<font size=2 color=blue>Start Import...</font><br>";
	$handle = fopen( $myFile, 'r' );
	
	while( !feof( $handle ) ) {
		$data = fgets( $handle, 512 );
		$pieces = explode( ",", $data );
		
		$query = "SELECT * FROM vendor_import WHERE customerid = '$pieces[0]' "
				. $vendorSettings['vendor_limit'];
		if( $debugimport ) echo "<p>$query</p>";
		$result = Treat_DB_ProxyOld::query( $query );
		$num = mysql_numrows( $result );
		
		if( $num == 0 ) {
			echo "<font color=orange size=2>$pieces[0] skipped...<br></font>";
		}
		else {
			$query43 = "UPDATE vendor_import SET last_update = '$updatedate' WHERE customerid = '$pieces[0]'";
			if( $debugimport ) echo "<p>$query43</p>";
			$result43 = Treat_DB_ProxyOld::query( $query43 );
		}
		
		$pieces[9] = str_replace( "'", "", $pieces[9] );
		$pieces[6] = str_replace( " ", "", $pieces[6] );
		$pieces[8] = round( $pieces[8] / $pieces[7] , 2);
		
		$num--;
		while( $num >= 0 ) {
			
			$businessid = @mysql_result( $result, $num, "businessid" );
			$companyid = @mysql_result( $result, $num, "companyid" );
			$vendor = @mysql_result( $result, $num, "vendor" );
			
			if( strlen( $pieces[6] ) == 6 ) {
				$pieces[6] = "0$pieces[6]";
			}
			
			$query2 = "SELECT * FROM inv_items WHERE businessid = '$businessid' AND item_code = '$pieces[6]' AND vendor = '$vendor'";
			if( $debugimport ) echo "<p>$query2</p>";
			$result2 = Treat_DB_ProxyOld::query( $query2 );
			$num2 = mysql_numrows( $result2 );
			
			$itemid = @mysql_result( $result2, 0, "inv_itemid" );
			$cost = @mysql_result( $result2, 0, "price" );
			
			if( $num2 == 0 ) {
				$query3 = "INSERT INTO inv_items (businessid,companyid,item_name,item_code,units,price,vendor,price_date,pack_size,pack_qty,v_update) VALUES ('$businessid','$companyid','$pieces[9]','$pieces[6]','$pieces[10]','$pieces[8]','$vendor','$date','$pieces[10]','1','1')";
				if( $debugimport ) echo "<p>$query3</p>";
				$result3 = Treat_DB_ProxyOld::query( $query3 );
				echo "<font size=2 color=green>$pieces[6] $pieces[9] added to $businessid</font><br>";
			}
			else {
				$num2--;
				while( $num2 >= 0 ) {
					$inv_itemid = @mysql_result( $result2, $num2, "inv_itemid" );
					$pack_qty = @mysql_result( $result2, $num2, "pack_qty" );
					
					$query3 = "UPDATE inv_items SET price = '$pieces[8]', pack_size = '$pieces[10]', price_date = '$date', v_update = '1' WHERE inv_itemid = '$inv_itemid'";
					if( $debugimport ) echo "<p>$query3</p>";
					$result3 = Treat_DB_ProxyOld::query( $query3 );
					
					if( $pieces[8] != $cost && $cost != 0 ) {
						$query32 = "INSERT INTO cost_history (inv_itemid,date,cost) VALUES ('$inv_itemid','$date','$cost')";
						if( $debugimport ) echo "<p>$query32</p>";
						$result32 = Treat_DB_ProxyOld::query( $query32 );
						echo " <font color=orange size=2>PRICE UPDATE</font> ";
					}
					
					if( $pack_qty > 1 ) {
						$count_cost = round( $pieces[8] / $pack_qty, 2 );
					}
                                        else{
                                                $count_cost = $pieces[8];
                                        }
                                        
					
					$query3 = "UPDATE inv_count SET price = '$count_cost' WHERE inv_itemid = '$inv_itemid' AND date >= '$date'";
					if( $debugimport ) echo "<p>$query3</p>";
					$result3 = Treat_DB_ProxyOld::query( $query3 );
					
					echo "<font size=2 color=blue>$pieces[9] price updated for $businessid</font><br>";
					
					$num2--;
				}
			}
			$num--;
		}
	}
	
	fclose( $handle );
	
	return true;
}



if( $only == '' ) {
	foreach( $vendorArray as $vendorCode => $value ) {
		updateVendor( $vendorCode );
	}
}
else {
	updateVendor( $only );
}


/*
if( $only == "gfs" || $only == "" ) {
	
	$myFile = DIR_VENDOR_IMPORTS . "/gfs.txt";
	
	//////////////////BEGIN IMPORTING
	if( file_exists( $myFile ) ) {
		
		$import_success++;
		$import_total++;
		echo "<font size=2 color=blue>Start GFS Import...</font><br>";
		$handle = fopen( $myFile, 'r' );
		
		while( !feof( $handle ) ) {
			$data = fgets( $handle, 512 );
			$pieces = explode( "\t", $data );
			
			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query = "SELECT * FROM vendor_import WHERE customerid = '$pieces[0]' AND vendor = '1106'";
			$result = Treat_DB_ProxyOld::query( $query );
			$num = mysql_numrows( $result );
			//mysql_close(); 
			
			if( $num == 0 ) {
				echo "<font color=orange size=2>$pieces[0] skipped...<br></font>";
			}
			else {
				$query43 = "UPDATE vendor_import SET last_update = '$updatedate' WHERE customerid = '$pieces[0]'";
				$result43 = Treat_DB_ProxyOld::query( $query43 );
			}
			
			$pieces[7] = str_replace( "'", "", $pieces[7] );
			
			$num--;
			while( $num >= 0 ) {
				
				$businessid = @mysql_result( $result, $num, "businessid" );
				$companyid = @mysql_result( $result, $num, "companyid" );
				$vendor = @mysql_result( $result, $num, "vendor" );
				
				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( "Unable to select database");
				$query2 = "SELECT * FROM inv_items WHERE businessid = '$businessid' AND item_code = '$pieces[4]' AND vendor = '$vendor'";
				$result2 = Treat_DB_ProxyOld::query( $query2 );
				$num2 = mysql_numrows( $result2 );
				//mysql_close(); 
				
				$itemid = @mysql_result( $result2, 0, "inv_itemid" );
				$cost = @mysql_result( $result2, 0, "price" );
				
				if( $num2 == 0 ) {
					
					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( "Unable to select database");
					$query3 = "INSERT INTO inv_items (businessid,companyid,item_name,item_code,units,price,vendor,price_date,pack_size,pack_qty,v_update) VALUES ('$businessid','$companyid','$pieces[7]','$pieces[4]','$pieces[9]','$pieces[6]','$vendor','$date','$pieces[9]','1','1')";
					$result3 = Treat_DB_ProxyOld::query( $query3 );
					//mysql_close(); 
					
					echo "<font size=2 color=green>$pieces[6] $pieces[9] added to $businessid</font><br>";
				}
				else {
					$num2--;
					while( $num2 >= 0 ) {
						$inv_itemid = @mysql_result( $result2, $num2, "inv_itemid" );
						$pack_qty = @mysql_result( $result2, $num2, "pack_qty" );
						
						//mysql_connect($dbhost,$username,$password);
						//@mysql_select_db($database) or die( "Unable to select database");
						$query3 = "UPDATE inv_items SET price = '$pieces[6]', pack_size = '$pieces[9]', price_date = '$date', v_update = '1' WHERE inv_itemid = '$inv_itemid'";
						$result3 = Treat_DB_ProxyOld::query( $query3 );
						//mysql_close(); 
						
						if( $pieces[8] != $cost && $cost != 0 ) {
							//mysql_connect($dbhost,$username,$password);
							//@mysql_select_db($database) or die( "Unable to select database");
							$query32 = "INSERT INTO cost_history (inv_itemid,date,cost) VALUES ('$inv_itemid','$date','$cost')";
							$result32 = Treat_DB_ProxyOld::query( $query32 );
							//mysql_close();
							echo " <font color=orange size=2>PRICE UPDATE</font> ";
						}
						
						if( $pack_qty > 1 ) {
							$pieces[6] = money( $pieces[6] / $pack_qty );
						}
						
						//mysql_connect($dbhost,$username,$password);
						//@mysql_select_db($database) or die( "Unable to select database");
						$query3 = "UPDATE inv_count SET price = '$pieces[6]' WHERE inv_itemid = '$inv_itemid' AND date >= '$date'";
						$result3 = Treat_DB_ProxyOld::query( $query3 );
						//mysql_close(); 
						
						echo "<font size=2 color=blue>$pieces[7] price updated for $businessid</font><br>";
						
						$num2--;
					}
				}
				$num--;
			}
		}
		
		fclose( $handle );
		unlink( $myFile );
		$emailmessage .= "GFS Success";
	}
}*/
echo "<font size=5 color=blue><b>Import Finished</b></font>";

//echo '<pre>'.$emailmessage.'</pre>';
mail( "smartin@essentialpos.com",
		"EDI ($import_success/$import_total) for Sysco", "$emailmessage",
		"from:support@treatamerica.com\r\n" );
?>
