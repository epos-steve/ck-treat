<?php

if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
include_once( 'db.php' );
require_once( DOC_ROOT . '/bootstrap.php' );

/*$_POST = new Treat_ArrayObject( $_POST );
$user = $_COOKIE['usercook'];
$pass = $_COOKIE['passcook'];
#$page_business = Treat_Model_Business_Singleton::getSingleton();
$businessid = $_POST["businessid"];
$companyid = $_POST["companyid"];*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook');
$businessid = \EE\Controller\Base::getPostVariable('businessid');
$companyid = \EE\Controller\Base::getPostVariable('companyid');

if ( !( $inv_items = $_POST['inv_items'] ) ) {
	$inv_items = new Treat_ArrayObject();
}

$update_rec_units = unserialize( urldecode( $_POST["update_rec_units"] ) );

$passed_cookie_login = require_once( realpath( DOC_ROOT . '/../lib/inc/cookie-login.php' ) );
if (
	$passed_cookie_login
	&& (
		(
			$GLOBALS['security_level'] == 1
			&& $GLOBALS['bid'] == $page_business->getBusinessId()
			&& $GLOBALS['cid'] == $page_business->getCompanyId()
		)
		|| (
			$GLOBALS['security_level'] > 1
			&& $GLOBALS['cid'] == $page_business->getCompanyId()
		)
	)
) {
	if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {

?>
	<pre style="text-align: left;"><?php
		echo '$_POST = ';
		var_dump( $_POST );
		echo '$update_rec_units';
		var_dump( $update_rec_units );
	?></pre>
<?php

	}
	# $key = $inv_itemid
	# $value = 1
	foreach ( $update_rec_units as $inv_itemid => $value ) {
		// inv_items[$inv_itemid][rec_num]
		if ( isset( $inv_items[ $inv_itemid ] ) ) {
			if ( isset( $inv_items[ $inv_itemid ]['rec_num'] ) ) {
				$rec_num = $inv_items[ $inv_itemid ]['rec_num'];
				$query = "UPDATE inv_items SET rec_num = '$rec_num' WHERE inv_itemid = '$inv_itemid'";
				$result = Treat_DB_ProxyOld::query( $query, true );
			}
			if ( isset( $inv_items[ $inv_itemid ]['rec_num2'] ) ) {
				$rec_num = $inv_items[ $inv_itemid ]['rec_num2'];
				$query = "UPDATE inv_items SET rec_num2 = '$rec_num' WHERE inv_itemid = '$inv_itemid'";
				$result = Treat_DB_ProxyOld::query( $query, true );
			}
		}
	}
	//mysql_close();
	
	# need to change the cookie-login.php code to use Treat_Model_User_Login
	# before being able to uncomment & debug this code
#	if ( $page_business->use_nutrition ) {
#		$location = sprintf(
#			'/ta/nutrition/recipe/conversion?bid=%d&cid=%d'
#			,$page_business->getBusinessId()
#			,$page_business->getCompanyId()
#		);
#	}
#	else {
		$location = sprintf(
			'/ta/businventor4.php?cid=%d&bid=%d'
			,$companyid
			,$businessid
		);
#	}
	if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
		echo sprintf(
			'<a href="%1$s">%1$s</a>'
			,$location
		);
	}
	else {
		header( 'Location: ' . $location );
	}
}
?>