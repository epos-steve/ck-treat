<?php
header("Content-Type: text/plain");
//ini_set("display_errors", "true");

function money($diff){
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+4);
        if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
        elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
        $diff = substr($diff, 0, $dot+3);
        return $diff;
}

function nextday($nextd,$day_format){ //Function returns next day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monn = substr($nextd, 5, 2);
   $dayn = substr($nextd, 8, 2);
   $yearn = substr($nextd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monn, $dayn+1, $yearn));
   return $tempdate;
}

function prevday($prevd,$day_format){ //Function returns previous day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monp = substr($prevd, 5, 2);
   $dayp = substr($prevd, 8, 2);
   $yearp = substr($prevd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monp, $dayp-1, $yearp));
   return $tempdate;
}

function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}

///////////////////////
////////DATA///////////
///////////////////////
define('DOC_ROOT', dirname(dirname(__FILE__)));
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');
include('graph/open-flash-chart.php');
require('lib/class.dashboard.php');

$who=$_GET["viewtype"];
$who2=$who;
$who=explode("-",$who);
$viewtype=$who[0];
$viewbus=$who[1];
$year=$who[2];
$month=$who[3];
$startdate="$year-$month-01";

/////find last day of month
$month=substr($startdate,5,2);
$year=substr($startdate,0,4);
$enddate=date("t", mktime(0, 0, 0, $month, 1, $year));
$enddate="$year-$month-$enddate";

////figure last day of report period
$tempdate=date("Y-m-d");
while(dayofweek($tempdate)!="Saturday"){$tempdate=prevday($tempdate);}
while(dayofweek($tempdate)!="Thursday"){$tempdate=prevday($tempdate);}
$lastdate=$tempdate;

////get values into array
	$db_data = array();
	$dates = array();

	$tempdate=$startdate;
	$counter=1;
	$month=$month*1;
	while($tempdate <= $enddate){
		$dates[] = "$month/$counter";
		$amount = DashBoard::get_data($tempdate,$tempdate,'sales,sales_bgt',$viewtype,$viewbus);
		$amount = explode(",",$amount);

		foreach($amount AS $key2 => $value2){
			if($tempdate<=$lastdate||$key2==1){$db_data[$key2][] = (int)round($value2 + $lastvalue[$key2],0);}
			$lastvalue[$key2] += $value2;
		}
		$tempdate=nextday($tempdate);
		$counter++;
	}
////////////////////////
//////GRAPH/////////////
////////////////////////
$color1 = "#6666CC";
$color2 = "#66CC66";
$count = count($db_data[0]);
//$array1 = array($db_data[0][0]);
//$array2 = array($db_data[1][0]);
//$array1 = $db_data[0];
//$array2 = $db_data[1];
//$array1 = array(1, 2, 3, 4, 5, 6, 2, 2, 2, 2, 2, 20000, 2, 2, 2, 2, 2, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7);
//$array2 = array(2, 3, 4, 5, 6, 7, 7, 7, 7, 7, 7, 7, 7, 70000, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7);

///figure dimensions
$max=0;
$min=0;
$count=1;
foreach($db_data[0] AS $key => $value){
		//if($count==1){$min=round($value);}
		if($value>$max){$max=round($value,0);}
		if($value<$min){$min=round($value,0);}
		$count++;
}
foreach($db_data[1] AS $key => $value){
		if($value>$max){$max=round($value,0);}
		if($value<$min){$min=round($value,0);}
}

$max=round($max * 1.1);

$d = new solid_dot();
$d->size(3)->halo_size(0)->colour("$color1");

$d2 = new solid_dot();
$d2->size(3)->halo_size(0)->colour("$color2");


$line = new line();
$line->set_default_dot_style($d2);
$line->set_values( $db_data[1] );
$line->set_width( 2 );
$line->set_colour( "$color2" );
$line->on_show(new line_on_show(false, $cascade[0], $delay[0]));

$line2 = new line();
$line2->set_default_dot_style($d);
$line2->set_values( $db_data[0] );
$line2->set_width( 2 );
$line2->set_colour( "$color1" );
$line2->on_show(new line_on_show(false, $cascade[0], $delay[0]));

$y = new y_axis();
$y->set_range( 0, $max, 5 );
$y->set_colour('#C0C0C0');
$y->set_grid_colour( '#C0C0C0' );
//$y->set_labels( array(' ',' ',' ',' ',' ',' ') );

$x = new x_axis();
$x->colour('#C0C0C0');
$x->set_grid_colour( '#C0C0C0' );
//$x->set_range( 1, $count );
$x->set_labels_from_array( $dates );
$x->set_rotate = 45;


$chart = new open_flash_chart();
//$chart->set_title( $title );
$chart->add_element( $line );
$chart->add_element( $line2 );
$chart->set_y_axis( $y );
$chart->set_x_axis( $x );
$chart->set_bg_colour('#FFFFFF');

echo $chart->toPrettyString();
?>