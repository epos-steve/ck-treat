<?php

function money($diff){
        $diff = number_format($diff,2,'.','');
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      elseif ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      elseif ($month == "10"){$month="11";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='32')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      $day=$day-31;
      if ($day<10){$day="0$day";}
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";}
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";}
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";}
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";}
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

define('DOC_ROOT', dirname(dirname(__FILE__)));
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');

$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");
$creditamount="creditamount";
$creditidnty="creditidnty";
$creditdate="creditdate";
$debitamount="debitamount";
$debitidnty="debitidnty";
$debitdate="debitdate";
$quatertotal=0;
$monthtotal=0;
$total=0;
$counter=0;
$findme='.';
$double='.00';
$single='0';
$double2='00';

$user=$_COOKIE["usercook"];
$pass=$_COOKIE["passcook"];
$view=$_COOKIE["viewcook"];
$sorting=$_COOKIE["sortcook"];
$date1=$_COOKIE["date1cook"];
$date2=$_COOKIE["date2cook"];
$findaccount=$_COOKIE["acctcook"];
$businessid=$_GET["bid"];
$companyid=$_GET["cid"];

if ($businessid==""&&$companyid==""){
   $businessid=$_POST["businessid"];
   $companyid=$_POST["companyid"];
   $view=$_POST["view"];
   $date1=$_POST["date1"];
   $date2=$_POST["date2"];
   $findinv=$_POST["findinv"];
   $findamount=$_POST["findamount"];
   $findaccount=$_POST["findaccount"];
   $sorting=$_POST["sorting"];
}

if ($date2==""||$date1==""){
   $date2=$today;$date1=$today;$view="All";$sorting="invoiceid";
}

if ($sorting=="submit"||$sorting=="unsubmit"||$sorting=="invoicenum"||$sorting=="master_vendor_id"){$sorting="invoiceid";}

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=mysql_numrows($result);
//mysql_close();

$security_level=mysql_result($result,0,"security_level");
$bid=mysql_result($result,0,"businessid");
$cid=mysql_result($result,0,"companyid");

if ($num != 1 || ($security_level==1 && ($bid != $businessid || $cid != $companyid)) || ($security_level > 1 AND $cid != $companyid) || ($user == "" && $pass == ""))
{
    echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
    setcookie("viewcook",$view);
    setcookie("sortcook",$sorting);
    setcookie("date1cook",$date1);
    setcookie("date2cook",$date2);
    setcookie("acctcook",$findaccount);

?>

<html>
<head>

<STYLE>
#loading {
 	width: 200px;
 	height: 48px;
 	background-color: ;
 	position: absolute;
 	left: 50%;
 	top: 50%;
 	margin-top: -50px;
 	margin-left: -100px;
 	text-align: center;
}
</STYLE>

<script type="text/javascript" src="preLoadingMessage.js"></script>

<script type="text/javascript" src="public_smo_scripts.js"></script>

<script type="text/javascript">
var szColorTo = '#00FF00';
var szColorOriginal = '';
function change_row_colour(pTarget)    {
  var pTR = pTarget.parentNode;
  if(pTR.nodeName.toLowerCase() != 'td')        {            return;        }
     if(pTarget.checked == true)        {            pTR.style.backgroundColor = szColorTo;        }
     else        {            pTR.style.backgroundColor = szColorOriginal;        }
}

</script>

<SCRIPT LANGUAGE="JavaScript" SRC="CalendarPopup.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">document.write(getCalendarStyles());</SCRIPT>

<STYLE>
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation
			{
			background-color:#6677DD;
			text-align:center;
			vertical-align:center;
			text-decoration:none;
			color:#FFFFFF;
			font-weight:bold;
			}
	.TESTcpDayColumnHeader,
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation,
	.TESTcpCurrentMonthDate,
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDate,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDate,
	.TESTcpCurrentDateDisabled,
	.TESTcpTodayText,
	.TESTcpTodayTextDisabled,
	.TESTcpText
			{
			font-family:arial;
			font-size:8pt;
			}
	TD.TESTcpDayColumnHeader
			{
			text-align:right;
			border:solid thin #6677DD;
			border-width:0 0 1 0;
			}
	.TESTcpCurrentMonthDate,
	.TESTcpOtherMonthDate,
	.TESTcpCurrentDate
			{
			text-align:right;
			text-decoration:none;
			}
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDateDisabled
			{
			color:#D0D0D0;
			text-align:right;
			text-decoration:line-through;
			}
	.TESTcpCurrentMonthDate
			{
			color:#6677DD;
			font-weight:bold;
			}
	.TESTcpCurrentDate
			{
			color: #FFFFFF;
			font-weight:bold;
			}
	.TESTcpOtherMonthDate
			{
			color:#808080;
			}
	TD.TESTcpCurrentDate
			{
			color:#FFFFFF;
			background-color: #6677DD;
			border-width:1;
			border:solid thin #000000;
			}
	TD.TESTcpCurrentDateDisabled
			{
			border-width:1;
			border:solid thin #FFAAAA;
			}
	TD.TESTcpTodayText,
	TD.TESTcpTodayTextDisabled
			{
			border:solid thin #6677DD;
			border-width:1 0 0 0;
			}
	A.TESTcpTodayText,
	SPAN.TESTcpTodayTextDisabled
			{
			height:20px;
			}
	A.TESTcpTodayText
			{
			color:#6677DD;
			font-weight:bold;
			}
	SPAN.TESTcpTodayTextDisabled
			{
			color:#D0D0D0;
			}
	.TESTcpBorder
			{
			border:solid thin #6677DD;
			}
</STYLE>
</head>

<?
    echo "<body>";
    echo "<div style=border:50px solid red;padding:10px;>";
    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%><tr><td colspan=2>";
    echo "<a href=businesstrack.php><img src=logo.jpg border=0 height=43 width=205></a><p></td></tr>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM company WHERE companyid = '$companyid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $companyname=mysql_result($result,0,"companyname");

    //echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";
    echo "<tr bgcolor=#CCCCFF><td colspan=2><font size=4><b>Invoicing - $companyname</b></font></td>";
    echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";
    echo "<tr><td colspan=2>Invoice List :: <a href=invoice2.php?cid=$companyid><font color=blue>Aging</font></a> :: <a href=invoice3.php?cid=$companyid><font color=blue>Payment Detail</font></a></td></tr>";
    echo "</table></center><p>";

    if ($view==""){$view="All";}
    if ($sorting==""){$sorting="date";}

    if ($findinv!=""){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM invoice WHERE companyid = '$companyid' AND (invoiceid = '$findinv' OR invoice_num = '$findinv')";
       $result = Treat_DB_ProxyOld::query($query);
       $num=mysql_numrows($result);
       //mysql_close();
    }
    elseif ($findamount!=""){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM invoice WHERE companyid = '$companyid' AND total = '$findamount' AND date >= '$date1' AND date <= '$date2'";
       $result = Treat_DB_ProxyOld::query($query);
       $num=mysql_numrows($result);
       //mysql_close();
    }
    elseif ($view=="All" && $findaccount==""){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM invoice WHERE companyid = '$companyid' AND date >= '$date1' AND date <= '$date2' ORDER BY $sorting";
       $result = Treat_DB_ProxyOld::query($query);
       $num=mysql_numrows($result);
       //mysql_close();
    }
    elseif ($view=="Today" && $findaccount==""){
       $c1="SELECTED";
       $date1=$today;
       $date2=$today;
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM invoice WHERE companyid = '$companyid' AND date = '$today' AND (status ='2' OR status = '4') ORDER BY $sorting";
       $result = Treat_DB_ProxyOld::query($query);
       $num=mysql_numrows($result);
       //mysql_close();
    }
    elseif ($view=="Yesterday" && $findaccount==""){
       $c2="SELECTED";
       $yesterday=pastday(1);
       $date1=$yesterday;
       $date2=$yesterday;
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM invoice WHERE companyid = '$companyid' AND date = '$yesterday' AND (status ='2' OR status = '4') ORDER BY $sorting";
       $result = Treat_DB_ProxyOld::query($query);
       $num=mysql_numrows($result);
       //mysql_close();
    }
    elseif ($view=="All" && $findaccount!=""){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM invoice WHERE companyid = '$companyid' AND date >= '$date1' AND date <= '$date2' AND accountid = '$findaccount' ORDER BY $sorting";
       $result = Treat_DB_ProxyOld::query($query);
       $num=mysql_numrows($result);
       //mysql_close();

    }
    elseif ($view=="Today" && $findaccount!=""){
       $c1="SELECTED";
       $date1=$today;
       $date2=$today;
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM invoice WHERE companyid = '$companyid' AND date = '$today' AND (status ='2' OR status = '4') AND accountid = '$findaccount' ORDER BY $sorting";
       $result = Treat_DB_ProxyOld::query($query);
       $num=mysql_numrows($result);
       //mysql_close();

    }
    elseif ($view=="Yesterday" && $findaccount!=""){
       $c2="SELECTED";
       $yesterday=pastday(1);
       $date1=$yesterday;
       $date2=$yesterday;
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM invoice WHERE companyid = '$companyid' AND date = '$yesterday' AND (status ='2' OR status = '4') AND accountid = '$findaccount' ORDER BY $sorting";
       $result = Treat_DB_ProxyOld::query($query);
       $num=mysql_numrows($result);
       //mysql_close();

    }
    elseif ($findaccount!=""){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM invoice WHERE companyid = '$companyid' AND date >= '$date1' AND date <= '$date2' AND status = '$view' AND accountid = '$findaccount' ORDER BY $sorting";
       $result = Treat_DB_ProxyOld::query($query);
       $num=mysql_numrows($result);
       //mysql_close();

    }
    else{
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM invoice WHERE companyid = '$companyid' AND date >= '$date1' AND date <= '$date2' AND status = '$view' ORDER BY $sorting";
       $result = Treat_DB_ProxyOld::query($query);
       $num=mysql_numrows($result);
       //mysql_close();
    }

    if ($findaccount!=""){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query15 = "SELECT status FROM accounts WHERE accountid = '$findaccount'";
       $result15 = Treat_DB_ProxyOld::query($query15);
       //mysql_close();

       $accountstatus=mysql_result($result15,0,"status");
    }

//echo "<center><table width=90% border=1><tr><td>$query</td></tr></table><p></center>";

    echo "<center><table width=90% border=0 cellspacing=0 cellpadding=0><tr valign=bottom><td colspan=7><FORM ACTION='invoice.php' method='post' style=\"margin:0;padding:0;display:inline;\">";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<input type=hidden name=businessid value=$businessid>";
    echo "<input type=hidden name=companyid value=$companyid><select name=view><option value='All'>All</option><option value='Today' $c1>Todays Total</option><option value='Yesterday' $c2>Yesterdays Total</option>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query5 = "SELECT * FROM invstatus ORDER BY statusname DESC";
    $result5 = Treat_DB_ProxyOld::query($query5);
    $num5=mysql_numrows($result5);
    //mysql_close();

    $num5--;
    while ($num5>=0){
       $statusname=mysql_result($result5,$num5,"statusname");
       $statusid=mysql_result($result5,$num5,"statusid");
       if ($statusid==$view){$selected="SELECTED";}
       else {$selected="";}
       echo "<option value='$statusid' $selected>$statusname</option>";
       $num5--;
    }

    if ($sorting=="invoiceid"){$s1="SELECTED";}
    elseif ($sorting=="businessid"){$s2="SELECTED";}
    elseif ($sorting=="accountid"){$s3="SELECTED";}
    elseif ($sorting=="date"){$s4="SELECTED";}
    elseif ($sorting=="type"){$s5="SELECTED";}
    elseif ($sorting=="status"){$s6="SELECTED";}
    elseif ($sorting=="total"){$s7="SELECTED";}

    echo "</select> from ";
    echo "<SCRIPT LANGUAGE='JavaScript' ID='js18'> var cal18 = new CalendarPopup('testdiv1');cal18.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date1 VALUE='$date1' SIZE=8> <A HREF=# onClick=cal18.select(document.forms[0].date1,'anchor18','yyyy-MM-dd'); return false; TITLE=cal18.select(document.forms[0].date1,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor18' ID='anchor18'><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A> to ";
    echo "<SCRIPT LANGUAGE='JavaScript' ID='js19'> var cal19 = new CalendarPopup('testdiv1');cal19.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date2 VALUE='$date2' SIZE=8> <A HREF=# onClick=cal19.select(document.forms[0].date2,'anchor19','yyyy-MM-dd'); return false; TITLE=cal19.select(document.forms[0].date2,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor19' ID='anchor19'><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A>";
    echo " Sort by <select name=sorting><option value='invoiceid' $s1>Invoice#</option><option value='businessid' $s2>Business</option><option value='accountid' $s3>Account</option><option value='date' $s4>Date</option><option value='type' $s5>Type</option><option value='status' $s6>Status</option><option value='total' $s7>Total</option></select> <select name=findaccount>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query5 = "SELECT * FROM accounts WHERE companyid = '$companyid' AND ((is_deleted = '0') OR (is_deleted = '1' AND del_date >= '$date1')) AND accountnum != '' ORDER BY accountnum DESC";
    $result5 = Treat_DB_ProxyOld::query($query5);
    $num5=mysql_numrows($result5);
    //mysql_close();

    echo "<option value=''>All Accounts</option>";

    $num5--;
    while ($num5>=0){
       $accountid=mysql_result($result5,$num5,"accountid");
       $accountname=mysql_result($result5,$num5,"name");
       $accountnum=mysql_result($result5,$num5,"accountnum");
       if ($findaccount==$accountid){$checked="SELECTED";}
       else {$checked="";}
       echo "<option value=$accountid $checked>$accountnum - $accountname</option>";
       $num5--;
    }

    echo "</select><br><i><b><u>OR</u></b></i> Find Invoice#: <INPUT TYPE=text name=findinv size=10> <i><b><u>OR</u></b></i> Find Total: <b>$</b><INPUT TYPE=text name=findamount size=10> (Date range applies) <INPUT TYPE=submit VALUE='GO'></FORM></td></tr>";

/////////////FIRST TABLE

    //echo "<tr bgcolor=black><td height=1 colspan=7></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td><form action=printinvoices.php method=post target='_blank' style=\"margin:0;padding:0;display:inline;\"><input type=hidden name=companyid value=$companyid><input type=checkbox name=checkall onclick='checkUncheckAll(this);' onclick='change_row_colour(this)'> <b>Invoice#</b></td><td><b>Business</b></td><td><b>Account</b></td><td><b>Date</b></td><td><b>Type</b></td><td><b>Status</b></td><td align=right><b>Total</b></td></tr>";
    echo "<tr bgcolor=black><td height=1 colspan=7></td></tr>";

    $grandtotal=0;
    $results=$num;
    $num--;
    $counter=1;
    while ($num>=0){
          $invoiceid=mysql_result($result,$num,"invoiceid");
          $businessid=mysql_result($result,$num,"businessid");
          $accountid=mysql_result($result,$num,"accountid");
          $invoicedate=mysql_result($result,$num,"date");
          $type=mysql_result($result,$num,"type");
          $status=mysql_result($result,$num,"status");
          $consolidate=mysql_result($result,$num,"consolidate");
          $total=mysql_result($result,$num,"total");
		  $invoice_num=mysql_result($result,$num,"invoice_num");
          $total=money($total);

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query2 = "SELECT name,accountnum,status FROM accounts WHERE accountid = '$accountid'";
          $result2 = Treat_DB_ProxyOld::query($query2);
          //mysql_close();

          $accountname=mysql_result($result2,0,"name");
          $accountnum=mysql_result($result2,0,"accountnum");

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query3 = "SELECT typename FROM invtype WHERE typeid = '$type'";
          $result3 = Treat_DB_ProxyOld::query($query3);
          //mysql_close();

          $typename=mysql_result($result3,0,"typename");

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query3 = "SELECT businessname FROM business WHERE businessid = '$businessid'";
          $result3 = Treat_DB_ProxyOld::query($query3);
          //mysql_close();

          $businessname=mysql_result($result3,0,"businessname");

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query4 = "SELECT statusname FROM invstatus WHERE statusid = '$status'";
          $result4 = Treat_DB_ProxyOld::query($query4);
          //mysql_close();

          $statusname=mysql_result($result4,0,"statusname");

          $statusname=mysql_result($result4,0,"statusname");
          if ($statusname=="Pending"){$statusname="<font color=green>$statusname</font>";}
          if ($statusname=="Void"){$statusname="<font color=red>$statusname</font>";}

          if ($consolidate==1){$showcon="<font color=red>C</font>";}
          else {$showcon="";}

          $accountname=substr($accountname,0,20);
          $var_name="invoiceid$counter";

		  if($invoice_num!=""){$showinvoice = $invoice_num;}
		  else{$showinvoice = $invoiceid;}

          echo "<tr onMouseOver=this.bgColor='#999999' onMouseOut=this.bgColor='white'><td><input type=checkbox name=$var_name value=$invoiceid onclick='change_row_colour(this)'> <a style=$style href=saveinvoice.php?bid=$businessid&cid=$companyid&invoiceid=$invoiceid&direct=1><font color=blue size=2>$showinvoice</font></a>$showcon</td><td><font size=2>$businessname</td><td><font size=2>$accountname ($accountnum)</td><td><font size=2>$invoicedate</td><td><font size=2>$typename</td><td><font size=2>$statusname</td><td align=right><font size=2>$$total</td></tr>";
          echo "<tr bgcolor=#CCCCCC><td height=1 colspan=7></td></tr>";

          $grandtotal=$grandtotal+$total;
          $num--;
          $counter++;
    }

/////////////////SECOND TABLE

    if($sorting=="invoiceid"){$sorting="invoice_num";}
    elseif($sorting=="accountid"){$sorting="cust_num";}

    if ($findinv!=""){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM invoice2 WHERE companyid = '$companyid' AND invoice_num = '$findinv'";
       $result = Treat_DB_ProxyOld::query($query);
       $num=mysql_numrows($result);
       //mysql_close();
    }
    elseif ($findamount!=""){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM invoice2 WHERE companyid = '$companyid' AND total = '$findamount' AND date >= '$date1' AND date <= '$date2'";
       $result = Treat_DB_ProxyOld::query($query);
       $num=mysql_numrows($result);
       //mysql_close();
    }
    elseif ($view=="All" && $findaccount==""){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM invoice2 WHERE companyid = '$companyid' AND date >= '$date1' AND date <= '$date2' ORDER BY $sorting";
       $result = Treat_DB_ProxyOld::query($query);
       $num=mysql_numrows($result);
       //mysql_close();
    }
    elseif ($view=="Today" && $findaccount==""){
       $c1="SELECTED";
       $date1=$today;
       $date2=$today;
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM invoice2 WHERE companyid = '$companyid' AND date = '$today' ORDER BY $sorting";
       $result = Treat_DB_ProxyOld::query($query);
       $num=mysql_numrows($result);
       //mysql_close();
    }
    elseif ($view=="Yesterday" && $findaccount==""){
       $c2="SELECTED";
       $yesterday=pastday(1);
       $date1=$yesterday;
       $date2=$yesterday;
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM invoice2 WHERE companyid = '$companyid' AND date = '$yesterday' ORDER BY $sorting";
       $result = Treat_DB_ProxyOld::query($query);
       $num=mysql_numrows($result);
       //mysql_close();
    }
    else{
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM invoice2 WHERE companyid = '$companyid' AND date >= '$date1' AND date <= '$date2' AND status = '$view' ORDER BY $sorting";
       $result = Treat_DB_ProxyOld::query($query);
       $num=mysql_numrows($result);
       //mysql_close();
    }

    $results=$results+$num;
    $num--;
    while ($num>=0){
          $invoiceid2=mysql_result($result,$num,"invoiceid2");
          $invoice_num=mysql_result($result,$num,"invoice_num");
          $businessid=mysql_result($result,$num,"businessid");
          $accountid=mysql_result($result,$num,"accountid");
          $invoicedate=mysql_result($result,$num,"date");
          $type=mysql_result($result,$num,"type");
          $status=mysql_result($result,$num,"status");
          $cust_num=mysql_result($result,$num,"cust_num");
          $cust_name=mysql_result($result,$num,"cust_name");
          $total=mysql_result($result,$num,"total");
          $total=money($total);

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query3 = "SELECT businessname FROM business WHERE businessid = '$businessid'";
          $result3 = Treat_DB_ProxyOld::query($query3);
          //mysql_close();

          $businessname=mysql_result($result3,0,"businessname");

          $typename="Imported";
          if($status==1){$statusname="Posted";}
          else{$statusname="Paid";}

          echo "<tr onMouseOver=this.bgColor='#999999' onMouseOut=this.bgColor='white'><td><font size=2>$invoice_num</font></td><td><font size=2>$businessname</td><td><font size=2>$cust_name ($cust_num)</td><td><font size=2>$invoicedate</td><td><font size=2>$typename</td><td><font size=2>$statusname</td><td align=right><font size=2>$$total</td></tr>";
          echo "<tr bgcolor=#CCCCCC><td height=1 colspan=7></td></tr>";

          $grandtotal=$grandtotal+$total;
          $num--;
          $counter++;
    }


/////////////////END TABLES
    $totalcount=$counter-1;
    echo "<tr bgcolor=black><td height=1 colspan=7></td></tr>";
    $grandtotal=money($grandtotal);
    echo "<tr><td>Results: $results</td><td colspan=5></td><td align=right><b>Total: $$grandtotal</b></td></tr>";
    echo "<tr bgcolor=white><td height=3 colspan=5><input type=hidden name=totalcount value=$totalcount><input type=submit value='Print Selected'></form></td></tr>";
    if ($findaccount!=""){
       if ($accountstatus==1){$act1="SELECTED";}
       elseif($accountstatus==2){$act2="SELECTED";}
       elseif($accountstatus==3){$act3="SELECTED";}
       echo "<tr><td colspan=3><form action=acct_status.php method=post><input type=hidden name=companyid value=$companyid><input type=hidden name=accountid value=$findaccount>Account Status: <select name=acct_status><option value=0>Active</option><option value=1 $act1>Inactive</option><option value=2 $act2>Suspended</option><option value=3 $act3>COD</option></select><input type=submit value='Change'></td><td colspan=2></form></td></tr>";
    }
    echo "</table></FORM></center>";

    //mysql_close();

//////END TABLE////////////////////////////////////////////////////////////////////////////////////

    echo "<br><FORM ACTION=businesstrack.php method=post>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<center><INPUT TYPE=submit VALUE=' Return to Main Page'></FORM></center>";
    echo "<DIV ID=testdiv1 STYLE=position:absolute;visibility:hidden;background-color:white;layer-background-color:white;></DIV></body>";

	google_page_track();
}
?>