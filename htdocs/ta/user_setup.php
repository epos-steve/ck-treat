<?php

/////////////////////////////////////////REVISION HISTORY////////////////////////////////////////////
//2/21/07 - Added edit user by business -SM
/////////////////////////////////////////REVISION HISTORY////////////////////////////////////////////
# This should probably be worked into the Ta_UserController or a new Ta_SecurityController, but
# at this point I'm not sure if there is "security" other than the user security.


if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
require_once( DOC_ROOT . '/bootstrap.php' );

$style = "text-decoration:none;";

$user = Treat_Controller_Abstract::getPostVariable( 'username', '' );
$pass = Treat_Controller_Abstract::getPostVariable( 'password', '' );
$view = Treat_Controller_Abstract::getPostVariable( 'view', '' );
if ( $user == '' && $pass == '' ) {
	$user = Treat_Controller_Abstract::getSessionCookieVariable( 'usercook', '' );
	$pass = Treat_Controller_Abstract::getSessionCookieVariable( 'passcook', '' );
}

$compcook = Treat_Controller_Abstract::getSessionCookieVariable( 'compcook', '' );
$edituser = Treat_Controller_Abstract::getGetVariable( 'edituser', '' );
$editsecurity = Treat_Controller_Abstract::getGetVariable( 'editsecurity', '' );
$editlocation = Treat_Controller_Abstract::getGetVariable( 'editlocation', '' );
$editcommission = Treat_Controller_Abstract::getGetVariable( 'editcommission', '' );
$showall = Treat_Controller_Abstract::getGetVariable( 'showall', '' );
if ( $showall < 1 ) {
	$showall = 0;
}

$passed_cookie_login = require_once( realpath( DOC_ROOT . '/../lib/inc/cookie-login.php' ) );
if (
	!$passed_cookie_login
	|| (
		( $user_login = $_SESSION->getUser( Treat_Session::TYPE_USER ) )
		&& $compcook != $user_login->getCompanyId()
	)
)
{
	include_once( 'inc/template-login-failed.php' );
/*
?>
<html>
	<head>
		<title>Login Failed</title>
	</head>
	<body>
		<center>
			<h3>Login Failed</h3>
			<p>
				Use your browser's back button to try again or 
				go back to the <a href="/">login page</a>.
			</p>
		</center>
	</body>
</html>
<?php
*/
}
# this is to handle the security drop down if javascript fails
elseif ( array_key_exists( 'security', $_POST ) && $_POST['security'] ) {
		header( 'Location: ' . $_POST['security'] );
		exit();
}
else
{
	$security_level = @$user_login->security_level;
	$mysecurity = @$user_login->security;
	$security_level2 = $security_level;
	if ( $user_login->security_level < 8 ) {
		$location = '/ta/businesstrack.php';
		header( 'Location: ' . $location );
		exit();
	}

?>
<html>
	<head>
		<title>Treat America :: user_setup.php</title>
		<link type="text/css" rel="stylesheet" href="/assets/css/ta/user_security.css" />
<script language="JavaScript" type="text/javascript">
function changePage(newLoc)
{
	nextPage = newLoc.options[newLoc.selectedIndex].value

	if (nextPage != "")
	{
		document.location.href = nextPage
	}
}
</script>

<script language="javascript" type="text/javascript"><!--
function delconfirm(){return confirm('ARE YOU SURE YOU WANT TO DELETE THIS SECURITY?');}
// --></script>
	</head>
	<body class="user_security">
		<center>
			<table cellspacing="0" cellpadding="0" border="0" width="90%">
				<tr>
					<td colspan="2">
						<a href="/ta/businesstrack.php">
							<img src="/assets/images/logo.jpg" border="0" height="43" width="205" alt="logo" style="margin-bottom: -4px" />
						</a>
					</td>
				</tr>
<?php

	$companyid = @$user_login->getCompanyId();
	$businessid = @$user_login->getBusinessId();
	$security_level = @$user_login->security_level;

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
#	$query = "SELECT * FROM company WHERE companyid = '$companyid'";
#	$result = Treat_DB_ProxyOld::query( $query );
	//mysql_close();

#	$companyname = @mysql_result( $result, 0, 'companyname' );
	$page_business = Treat_Model_Business_Singleton::getSingleton();
	$companyname = @$page_business->companyname;

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
#	$query = "SELECT * FROM security WHERE securityid = '$mysecurity'";
#	$result = Treat_DB_ProxyOld::query( $query );
	//mysql_close();

	$sec_routes = @$user_login->routes;
	$sec_securities = @$user_login->securities;

	//echo "<tr bgcolor=black><td height=1 colspan=2></td></tr>";
?>
				<tr bgcolor="#ccccff">
					<td colspan="2">
						<font size="4">
							<b>Users and Security - <?php
								echo $companyname;
							?></b>
						</font>
					</td>
				</tr>
				<tr bgcolor="black">
					<td height="1" colspan="2"></td>
				</tr>
<?php

/*////////////////////JOB TYPES/////////////////////////////////

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM jobtype WHERE companyid = '$companyid' ORDER BY name DESC";
	$result = Treat_DB_ProxyOld::query( $query );
	$num = @mysql_numrows( $result );
	//mysql_close();

	$num--;

	echo "<tr bgcolor=#E8E7E7><td colspan=2><p><br><b><u>JOB TYPES</u></b></td></tr><tr bgcolor=#E8E7E7><td><ul>";
	while ( $num >= 0 ) {
		$jobtypename = @mysql_result( $result, $num, 'name' );
		echo "<li>$jobtypename";
		$num--;
	}

	echo "<FORM ACTION=addnewjobtype.php method=post>";
	echo "<input type=hidden name=username value=$user>";
	echo "<input type=hidden name=password value=$pass>";
	echo "<input type=hidden name=companyid value=$companyid>";
	echo "<font color=red><li> <INPUT TYPE=submit VALUE='  Add Job Type  '></font></FORM>";
	echo "</ul></td></tr><tr bgcolor=black><td colspan=2 height=1></td></tr>";
/////////////////////END JOBTYPES//////////////////////////////////*/
	/////////////////////EMPLOYEES/////////////////////////////////

?>
				<tr bgcolor="#e8e7e7">
					<td colspan="2">
<?php
	$user_list = Treat_Model_User_Login::getSecurityLoginList( $user_login, $showall );
?>
						<form action="/ta/edit_employee.php" method="post" style="margin-bottom: 0; padding-bottom: 14px;">
							<input type="hidden" name="username" value="<?php echo $user; ?>" />
							<input type="hidden" name="password" value="<?php echo $pass; ?>" />
							<input type="hidden" name="companyid" value="<?php echo $companyid; ?>" />
							<p></p>
							<br>
							<b><u>
								EMPLOYEES
							</u></b>
							<ul style="margin-top: 0; padding-top: 0;">
								<li>
									Employee:
									<select name="employeeid">
<?php
	$lastseclvl = null;
	foreach ( $user_list as $employee ) {
		if ( $lastseclvl !== $employee->security_level ) {
			$lastseclvl = $employee->security_level;
?>
										<option value="-1">(Level <?php
											echo $employee->security_level;
										?>)==============================(<?php
											echo $employee->security_level;
										?>)</option>
<?php
		}
#		else {
?>
										<option value="<?php
											echo $employee->loginid;
										?>"><?php
											$temp = '';
											if ( $employee->temp_expire != '0000-00-00' ) {
												$temp = ' (TEMP)';
											}
											if ( $employee->security_level != 1 ) {
												echo sprintf(
													'%s, %s%s'
													,$employee->lastname
													,$employee->firstname
													,$temp
												);
											}
											elseif ( $employee->security_level == 1 ) {
												echo sprintf(
													'%s, %s - %s%s'
													,$employee->lastname
													,$employee->firstname
													,$employee->businessname
													,$temp
												);
											}
										?></option>
<?php
#		}
	}
?>
									</select>
									<input type="submit" value="Edit Employee" />
									<a href="/ta/user_setup.php?cid=<?php
										echo $companyid;
									?>&showall=1" style="<?php
										echo $style;
									?>">
										<font size="2" color="blue">
											[This Company Only]
										</font>
									</a>
								</li>
							</ul>
						</form>
					</td>
				</tr>
<?php
	/////////////////////EMPLOYEES BY Business/////////////////////////////////

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	if ( $security_level > 8 ) {
		$query = "SELECT * FROM business WHERE companyid != '2' ORDER BY businessname DESC";
	}
	else {
		$query = "SELECT * FROM business WHERE companyid = '$companyid' ORDER BY businessname DESC";
	}
	$result = Treat_DB_ProxyOld::query( $query );
	$num = @mysql_numrows( $result );
	//mysql_close();

	$num--;
?>
				<tr bgcolor="#e8e7e7">
					<td colspan="2">
						<form action="/ta/edit_employee.php" method="post">
							<input type="hidden" name="username" value="<?php echo $user; ?>" />
							<input type="hidden" name="password" value="<?php echo $pass; ?>" />
							<input type="hidden" name="companyid" value="<?php echo $companyid; ?>" />
							<b><u>
								EMPLOYEES BY BUSINESS
							</u></b>
							<ul style="margin-top: 0; padding-top: 0;">
								<li>
									Employee:
									<select name="employeeid">
<?php
	while ( $num >= 0 ) {
		$busname = @mysql_result( $result, $num, 'businessname' );
		$userbusid = @mysql_result( $result, $num, 'businessid' );

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query5 = "SELECT * FROM login WHERE businessid = '$userbusid' AND is_deleted = '0'";
		$result5 = Treat_DB_ProxyOld::query( $query5 );
		$num5 = @mysql_numrows( $result5 );
		//mysql_close();

		$num5--;
		while ( $num5 >= 0 ) {
			$lastname = @mysql_result( $result5, $num5, 'lastname' );
			$firstname = @mysql_result( $result5, $num5, 'firstname' );
			$loginid = @mysql_result( $result5, $num5, 'loginid' );
			$temp_expire = @mysql_result( $result5, $num5, 'temp_expire' );

			if ( $temp_expire != '0000-00-00' ) {
				$showtemp = '(TEMP)';
			}
			else {
				$showtemp = '';
			}
?>
										<option value="<?php
											echo $loginid;
										?>"><?php
											echo $busname, ' - ', $lastname, ', ', $firstname, ' ', $showtemp;
										?></option>
<?php
			$num5--;
		}
		$num--;
	}
?>
									</select>
									<input type="submit" value="Edit Employee" />
								</li>
							</ul>
						</form>
					</td>
				</tr>

				<tr bgcolor="#e8e7e7">
					<td colspan="2">
						<form action="/ta/edit_employee.php" method="post">
							<input type="hidden" name="username" value="<?php echo $user; ?>" />
							<input type="hidden" name="password" value="<?php echo $pass; ?>" />
							<input type="hidden" name="companyid" value="<?php echo $companyid; ?>" />
							<input type="hidden" name="new_empl" value="NEW" />
							<ul>
								<li>
									<font color="red">
										<input type="submit" value="  Add Employee  " />
									</font>
								</li>
							</ul>
						</form>
					</td>
				</tr>
				<tr bgcolor="black">
					<td colspan="2" height="1"></td>
				</tr>
<?php
	/////////////////////END EMPLOYEES///////////////////////////////////

/*
	/////////////////////VENDING/////////////////////////////////
	if ( $sec_routes == 1 ) {
		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query = "SELECT * FROM login_route WHERE companyid = '$companyid' AND is_deleted = '0' ORDER BY businessid,locationid,route DESC";
		$result = Treat_DB_ProxyOld::query( $query );
		$num = @mysql_numrows( $result );
		//mysql_close();

		echo "<tr bgcolor=#E8E7E7><td colspan=2><p><br><b><u>ROUTE MANAGEMENT</u></b></td></tr>";
		echo "<tr bgcolor=#E8E7E7><td colspan=2><ul><li><form action=user_setup.php method=post>Route: <select name=user onChange='changePage(this.form.user)'><option value=user_setup.php?cid=$companyid>============Please Choose============";

		$lastlocationid=-1;
		$lastuserbus=-1;
		$num--;
		while ( $num >= 0 ) {

			$login_routeid = @mysql_result( $result, $num, 'login_routeid' );
			$lastname = @mysql_result( $result, $num, 'lastname' );
			$firstname = @mysql_result( $result, $num, 'firstname' );
			$id1 = @mysql_result( $result, $num, 'username' );
			$id2 = @mysql_result( $result, $num, 'password' );
			$route = @mysql_result( $result, $num, 'route' );
			$userbus = @mysql_result( $result, $num, 'businessid' );
			$locationid = @mysql_result( $result, $num, 'locationid' );
			$isactive = @mysql_result( $result, $num, 'active' );

			if ( $login_routeid == $edituser ) {
				$showsel = "SELECTED";
			}
			else {
				$showsel = '';
			}

			if ( $isactive == 1 ) {
				$showinactive = '(Inactive)';
			}
			else {
				$showinactive = '';
			}

			if ( $lastuserbus != $userbus ) {
				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( "Unable to select database");
				$query5 = "SELECT businessname FROM business WHERE businessid = '$userbus'";
				$result5 = Treat_DB_ProxyOld::query( $query5 );
				//mysql_close();
				$busname = @mysql_result( $result5, 0, 'businessname' );

				echo "<option value=user_setup.php?cid=$companyid>~~~~~~~~~~~~~~~~~~~~$busname</option>";
			}
			if ( $lastlocationid != $locationid ) {
				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( "Unable to select database");
				$query5 = "SELECT location_name FROM vend_locations WHERE locationid = '$locationid'";
				$result5 = Treat_DB_ProxyOld::query( $query5 );
				//mysql_close();
				$locname = @mysql_result( $result5, 0, 'location_name' );

				echo "<option value=user_setup.php?cid=$companyid>====================$locname</option>";
			}

			echo "<option value=user_setup.php?edituser=$login_routeid&cid=$companyid $showsel>Route# $route, $firstname $lastname $showinactive</option>";
			$lastuserbus=$userbus;
			$lastlocationid=$locationid;
			$num--;
		}
		echo "</select></form></td></tr>";

		if ( $edituser != '' ) {
			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query = "SELECT * FROM login_route WHERE login_routeid = '$edituser'";
			$result = Treat_DB_ProxyOld::query( $query );
			//mysql_close();

			$login_routeid = @mysql_result( $result, 0, 'login_routeid' );
			$lastname = @mysql_result( $result, 0, 'lastname' );
			$firstname = @mysql_result( $result, 0, 'firstname' );
			$id1 = @mysql_result( $result, 0, 'username' );
			$id2 = @mysql_result( $result, 0, 'password' );
			$route = @mysql_result( $result, 0, 'route' );
			$truck = @mysql_result( $result, 0, 'truck' );
			$userbus = @mysql_result( $result, 0, 'businessid' );
			$locationid = @mysql_result( $result, 0, 'locationid' );
			$sec_level = @mysql_result( $result, 0, 'sec_level' );
			$active = @mysql_result( $result, 0, 'active' );
			$commissionid = @mysql_result( $result, 0, 'commissionid' );

			if ( $active == 1 ) {
				$showactive = "CHECKED";
			}
			else {
				$showactive = '';
			}

			echo "<tr bgcolor=#E8E7E7><td colspan=2><ul><table bgcolor=#E8E7E7>";
			echo "<tr><td align=right><form action=edit_venduser.php method=post><input type=hidden name=login_routeid value=$login_routeid><input type=hidden name=edit value=1><input type=hidden name=username value=$user><input type=hidden name=password value=$pass><b>Route#</b>:</td><td><input type=text size=5 name=route value='$route'> <font size=2><i>*Should be at least 3 digits long (ie 001) for route 1.</i></font></td></tr>";
			echo "<tr><td align=right>Truck#:</td><td><input type=text size=5 name=truck value='$truck'></td></tr>";
			echo "<tr><td align=right>Last Name:</td><td><input type=text size=30 name=lastname value='$lastname'></td></tr>";
			echo "<tr><td align=right>First Name:</td><td><input type=text size=30 name=firstname value='$firstname'></td></tr>";
			echo "<tr><td align=right>ID1:</td><td><input type=text size=10 name=id1 value='$id1'></td></tr>";
			echo "<tr><td align=right>ID2:</td><td><input type=text size=10 name=id2 value='$id2'></td></tr>";
			echo "<tr><td align=right>Location:</td><td><select name=newlocid>";

			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query = "SELECT * FROM vend_locations ORDER BY businessid, location_name DESC";
			$result = Treat_DB_ProxyOld::query( $query );
			$num = @mysql_numrows( $result );
			//mysql_close();

			$num--;
			while ( $num >= 0 ) {
				$newlocid = @mysql_result( $result, $num, 'locationid' );
				$newlocname = @mysql_result( $result, $num, 'location_name' );
				$busid = @mysql_result( $result, $num, 'businessid' );
				if ( $newlocid == $locationid ) {
					$showsel2 = "SELECTED";
				}
				else {
					$showsel2 = '';
				}

				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( "Unable to select database");
				$query4 = "SELECT businessname FROM business WHERE businessid = '$busid'";
				$result4 = Treat_DB_ProxyOld::query( $query4 );
				//mysql_close();

				$busname = @mysql_result( $result4, 0, 'businessname' );

				echo "<option value=$newlocid $showsel2>$busname - $newlocname</option>";
				$num--;
			}
			echo "</select></td></tr>";

			if ( $sec_level == 1 ) {
				$sec1 = "SELECTED";
			}
			elseif ( $sec_level == 2 ) {
				$sec2 = "SELECTED";
			}
			elseif ( $sec_level == 3 ) {
				$sec3 = "SELECTED";
			}

			echo "<tr><td align=right>Security:</td><td><select name=sec_level><option value=1 $sec1>1 - Driver</option><option value=2 $sec2>2 - Route Mgr</option><option value=3 $sec3>3 - Admin</option></select></td></tr>";

			echo "<tr><td align=right>Commission:</td><td><select name=newcommissionid><option value=0></option>";

			$query = "SELECT business.companyid FROM vend_locations,business WHERE vend_locations.locationid = '$locationid' AND vend_locations.unitid = business.businessid";
			$result = Treat_DB_ProxyOld::query( $query );

			$mycomp = @mysql_result( $result, 0, 'business.companyid' );

			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query = "SELECT * FROM vend_commission WHERE companyid = '$mycomp'";
			$result = Treat_DB_ProxyOld::query( $query );
			$num = @mysql_numrows( $result );
			//mysql_close();

			$num--;
			while ( $num >= 0 ) {
				$newcommissionid = @mysql_result( $result, $num, 'commissionid' );
				$commission_name = @mysql_result( $result, $num, 'name' );
				if ( $commissionid == $newcommissionid ) {
					$showsel2 = "SELECTED";
				}
				else {
					$showsel2 = '';
				}

				echo "<option value=$newcommissionid $showsel2>$commission_name</option>";
				$num--;
			}
			echo "</select></td></tr>";

			echo "<tr><td align=right>Inactive:</td><td><input type=checkbox name=active value=1 $showactive></td></tr>";
			echo "<tr><td></td><td><input type=submit value='Save'></td></tr></table>";
			echo "</form></td></tr>";
		}

		else {

			echo "<tr bgcolor=#E8E7E7><td colspan=2><ul><table bgcolor=#E8E7E7>";
			echo "<tr><td align=right><form action=edit_venduser.php method=post><input type=hidden name=username value=$user><input type=hidden name=password value=$pass><b>Route#</b>:</td><td><input type=text size=5 name=route> <font size=2><i>*Should be at least 3 digits long (ie 001) for route 1.</i></font></td></tr>";
			echo "<tr><td align=right>Truck#:</td><td><input type=text size=5 name=truck></td></tr>";
			echo "<tr><td align=right>Last Name:</td><td><input type=text size=30 name=lastname></td></tr>";
			echo "<tr><td align=right>First Name:</td><td><input type=text size=30 name=firstname></td></tr>";
			echo "<tr><td align=right>ID1:</td><td><input type=text size=10 name=id1></td></tr>";
			echo "<tr><td align=right>ID2:</td><td><input type=text size=10 name=id2></td></tr>";
			echo "<tr><td align=right>Location:</td><td><select name=newlocid>";

			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query = "SELECT * FROM vend_locations ORDER BY businessid, location_name DESC";
			$result = Treat_DB_ProxyOld::query( $query );
			$num = @mysql_numrows( $result );
			//mysql_close();

			$num--;
			while ( $num >= 0 ) {
				$newlocid = @mysql_result( $result, $num, 'locationid' );
				$newlocname = @mysql_result( $result, $num, 'location_name' );
				$busid = @mysql_result( $result, $num, 'businessid' );

				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( "Unable to select database");
				$query4 = "SELECT businessname FROM business WHERE businessid = '$busid'";
				$result4 = Treat_DB_ProxyOld::query( $query4 );
				//mysql_close();

				$busname = @mysql_result( $result4, 0, 'businessname' );

				echo "<option value=$newlocid>$busname - $newlocname</option>";
				$num--;
			}
			echo "</select></td></tr>";
			echo "<tr><td align=right>Security:</td><td><select name=sec_level><option value=1>1 - Driver</option><option value=2>2 - Route Mgr</option><option value=3>3 - Admin</option></select></td></tr>";
			echo "<tr><td align=right>Inactive:</td><td><input type=checkbox name=active value=1 $showactive></td></tr>";
			echo "<tr><td></td><td><input type=submit value='Add'></td></tr></table>";
			echo "</form></td></tr>";
		}

		if ( $edituser != '' ) {
			echo "<tr bgcolor=#E8E7E7><td colspan=2><font color=red><ul><li><form action=edit_venduser.php method=post onclick='return delconfirm()'><input type=hidden name=deleteuser value='$edituser'><input type=hidden name=username value=$user><input type=hidden name=password value=$pass><input type=submit value='DELETE ROUTE'></form></td></tr>";
		}

		//////VENDING LOCATIONS
		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query = "SELECT * FROM vend_locations";
		$result = Treat_DB_ProxyOld::query( $query );
		$num = @mysql_numrows( $result );
		//mysql_close();

		echo "<tr bgcolor=#E8E7E7><td colspan=2><p><br><b><u><a name=vendloc>VENDING LOCATION MANAGEMENT</a></u></b></td></tr>";
		echo "<tr bgcolor=#E8E7E7><td colspan=2><ul><li><form action=user_setup.php method=post>Location: <select name=location onChange='changePage(this.form.location)'><option value=user_setup.php?cid=$companyid>============Please Choose============</option>";

		$num--;
		while ( $num >= 0 ) {

			$locationid = @mysql_result( $result, $num, 'locationid' );
			$location_name = @mysql_result( $result, $num, 'location_name' );

			if ( $locationid == $editlocation ) {
				$showsel="SELECTED";
				$v_location_name = @mysql_result( $result, $num, 'location_name' );
				$v_businessid = @mysql_result( $result, $num, 'businessid' );
				$v_accountid = @mysql_result( $result, $num, 'accountid' );
				$v_unitid = @mysql_result( $result, $num, 'unitid' );
			}
			else{$showsel='';}

			echo "<option value=user_setup.php?editlocation=$locationid&cid=$companyid#vendloc $showsel>$location_name</option>";
			$num--;
		}
		echo "</select></form></td></tr>";
		if ( $editlocation > 0 ) {
			echo "<tr bgcolor=#E8E7E7><td align=right><form action=savelocation.php method=post><input type=hidden name=locationid value='$editlocation'><input type=hidden name=edit value=1><input type=hidden name=username value=$user><input type=hidden name=password value=$pass>Location Name:</td><td><input type=text size=25 name=location_name value='$v_location_name'></td></tr>";
			echo "<tr bgcolor=#E8E7E7><td align=right>Ordering Business:</td><td><select name=v_businessid>";

			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query4 = "SELECT * FROM business WHERE companyid != '2' ORDER BY businessname DESC";
			$result4 = Treat_DB_ProxyOld::query( $query4 );
			$num4 = @mysql_numrows( $result4 );
			//mysql_close();

			$num4--;
			while ( $num4 >= 0 ) {
				$businessname = @mysql_result( $result4, $num4, 'businessname' );
				$busid = @mysql_result( $result4, $num4, 'businessid' );

				if ( $v_businessid == $busid ) {
					$showsel4 = "SELECTED";
				}
				else {
					$showsel4 = '';
				}

				echo "<option value=$busid $showsel4>$businessname</option>";
				$num4--;
			}

			echo "</select></td></tr>";
			echo "<tr bgcolor=#E8E7E7><td align=right>AR Account:</td><td><select name=v_accountid>";

			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query4 = "SELECT * FROM accounts WHERE companyid != '2' ORDER BY accountnum DESC";
			$result4 = Treat_DB_ProxyOld::query( $query4 );
			$num4 = @mysql_numrows( $result4 );
			//mysql_close();

			//$num4--;
			while ( $num4 >= 0 ) {
				$accountname = @mysql_result( $result4, $num4, 'name' );
				$accountid = @mysql_result( $result4, $num4, 'accountid' );
				$accountnum = @mysql_result( $result4, $num4, 'accountnum' );

				if ( $v_accountid == $accountid ) {
					$showsel4 = "SELECTED";
				}
				else {
					$showsel4 = '';
				}

				echo "<option value=$accountid $showsel4>$accountnum - $accountname</option>";
				$num4--;
			}

			echo "</select></td></tr>";
			echo "<tr bgcolor=#E8E7E7><td align=right>Money Room Business:</td><td><select name=v_unitid>";

			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query4 = "SELECT * FROM business WHERE companyid != '2' ORDER BY businessname DESC";
			$result4 = Treat_DB_ProxyOld::query( $query4 );
			$num4 = @mysql_numrows( $result4 );
			//mysql_close();

			$num4--;
			while ( $num4 >= 0 ) {
				$businessname = @mysql_result( $result4, $num4, 'businessname' );
				$busid = @mysql_result( $result4, $num4, 'businessid' );

				if ( $v_unitid == $busid ) {
					$showsel4 = "SELECTED";
				}
				else {
					$showsel4 = '';
				}

				echo "<option value=$busid $showsel4>$businessname</option>";
				$num4--;
			}

			echo "</select></td></tr>";
			echo "<tr bgcolor=#E8E7E7><td align=right></td><td><input type=submit value='Save'></form></td></tr>";
		}
		echo "<tr bgcolor=#E8E7E7><td colspan=2><font color=red><ul><li><form action=savelocation.php method=post><input type=hidden name=username value=$user><input type=hidden name=password value=$pass></font>New Location: <input type=text size=20 name=location_name> <input type=submit value='Add'></form></ul></td></tr>";
		echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";

		//////////////////VENDING COMMISSIONS

		$query = "SELECT * FROM vend_commission WHERE companyid = '$companyid'";
		$result = Treat_DB_ProxyOld::query( $query );
		$num = @mysql_numrows( $result );

		echo "<tr bgcolor=#E8E7E7><td colspan=2><p><br><b><u><a name=vendcom>COMMISSION MANAGEMENT</a></u></b></td></tr>";
		echo "<tr bgcolor=#E8E7E7><td colspan=2><ul><li><form action=user_setup.php method=post>Commission Type: <select name=commission onChange='changePage(this.form.commission)'><option value=user_setup.php?cid=$companyid>============Please Choose============</option>";

		$num--;
		while ( $num >= 0 ) {

			$commissionid = @mysql_result( $result, $num, 'commissionid' );
			$commission_name = @mysql_result( $result, $num, 'name' );

			if ( $commissionid == $editcommission ) {
				$showsel="SELECTED";
				$base_link = @mysql_result( $result, $num, 'base_link' );
				$commission_link = @mysql_result( $result, $num, 'commission_link' );
				$freevend_link = @mysql_result( $result, $num, 'freevend_link' );
				$csv_link = @mysql_result( $result, $num, 'csv_link' );
				$weekend_link = @mysql_result( $result, $num, 'weekend_link' );
			}
			else {
				$showsel='';
			}

			echo "<option value=user_setup.php?editcommission=$commissionid&cid=$companyid#vendcom $showsel>$commission_name</option>";
			$num--;
		}
		echo "</select></form></td></tr>";

		if ( $editcommission > 0 ) {
			///////////COMMISSION LINKING
			echo "<tr bgcolor=#E8E7E7><td colspan=2><table style=\"border:1px solid black;\" width=60% bgcolor=#FFFF99>";
			echo "<tr><td colspan=5><b><u>Payroll Codes</td></tr>";
			echo "<tr><td>Base</td><td>Commission</td><td>FV/Subsidy</td><td>CSV</td><td>Weekend</td><td></td></tr>";
			echo "<tr>";

			$query2 = "SELECT * FROM payroll_code_comm WHERE companyid = '$companyid' ORDER BY orderid DESC";
			$result2 = Treat_DB_ProxyOld::query( $query2 );
			$num2 = @mysql_numrows( $result2 );

			echo "<td><form action=savecommission.php method=post><input type=hidden name=commissionid value=$editcommission><select name=base_link><option value=0></option>";
			$temp=$num2-1;
			while ( $temp >= 0 ) {
				$codeid = @mysql_result( $result2, $temp, 'codeid' );
				$code_name = @mysql_result( $result2, $temp, 'code_name' );

				if ( $codeid == $base_link ) {
					$showsel = "SELECTED";
				}
				else {
					$showsel = '';
				}

				echo "<option value=$codeid $showsel>$code_name</option>";
				$temp--;
			}
			echo "</select></td>";

			echo "<td><select name=commission_link><option value=0></option>";
			$temp = $num2 - 1;
			while ( $temp >= 0 ) {
				$codeid = @mysql_result( $result2, $temp, 'codeid' );
				$code_name = @mysql_result( $result2, $temp, 'code_name' );

				if ( $codeid == $commission_link ) {
					$showsel = "SELECTED";
				}
				else {
					$showsel = '';
				}

				echo "<option value=$codeid $showsel>$code_name</option>";
				$temp--;
			}
			echo "</select></td>";

			echo "<td><select name=freevend_link><option value=0></option>";
			$temp = $num2 - 1;
			while ( $temp >= 0 ) {
				$codeid = @mysql_result( $result2, $temp, 'codeid' );
				$code_name = @mysql_result( $result2, $temp, 'code_name' );

				if ( $codeid == $freevend_link ) {
					$showsel = "SELECTED";
				}
				else {
					$showsel = '';
				}

				echo "<option value=$codeid $showsel>$code_name</option>";
				$temp--;
			}
			echo "</select></td>";

			echo "<td><select name=csv_link><option value=0></option>";
			$temp=$num2-1;
			while ( $temp >= 0 ) {
				$codeid = @mysql_result( $result2, $temp, 'codeid' );
				$code_name = @mysql_result( $result2, $temp, 'code_name' );

				if ( $codeid == $csv_link ) {
					$showsel = "SELECTED";
				}
				else {
					$showsel = '';
				}

				echo "<option value=$codeid $showsel>$code_name</option>";
				$temp--;
			}
			echo "</select></td>";

			echo "<td><select name=weekend_link><option value=0></option>";
			$temp=$num2-1;
			while ( $temp >= 0 ) {
				$codeid = @mysql_result( $result2, $temp, 'codeid' );
				$code_name = @mysql_result( $result2, $temp, 'code_name' );

				if ( $codeid == $weekend_link ) {
					$showsel = "SELECTED";
				}
				else {
					$showsel = '';
				}

				echo "<option value=$codeid $showsel>$code_name</option>";
				$temp--;
			}
			echo "</select></td>";

			echo "<td><input type=hidden name=edit value=5><input type=submit value='Save'></td></tr>";
			echo "<tr><td colspan=5></form></td></tr>";
			echo "</table></td></tr>";
			echo "<tr bgcolor=#E8E7E7 height=16><td colspan=2></td></tr>";
			//////////END LINKING

			echo "<tr bgcolor=#E8E7E7><td colspan=2><table style=\"border:1px solid black;\" width=60% bgcolor=#FFFF99>";
			echo "<tr><td align=right width=20%>Lower $ Limit</td><td align=right width=20%>Upper $ Limit</td><td align=right width=20%>% of Sales</td><td align=right width=20%>Base</td><td align=right width=20%></td></tr>";
			echo "<tr height=1 bgcolor=black><td colspan=5></td></tr>";

			$query = "SELECT * FROM vend_commission_detail WHERE commissionid = '$editcommission' ORDER BY lower DESC";
			$result = Treat_DB_ProxyOld::query( $query );
			$num = @mysql_numrows( $result );

			$num--;
			while ( $num >= 0 ) {
				$id = @mysql_result( $result, $num, 'id' );
				$lower = @mysql_result( $result, $num, 'lower' );
				$upper = @mysql_result( $result, $num, 'upper' );
				$percent = @mysql_result( $result, $num, 'percent' );
				$base = @mysql_result( $result, $num, 'base' );

				$showdelete="<a href=commissiondetail.php?delid=$id&commissionid=$editcommission><img src=delete.gif height=16 width=16 border=0></a>";

				echo "<tr><td align=right>$lower</td><td align=right>$upper</td><td align=right>$percent</td><td align=right>$base</td><td>$showdelete</td></tr>";

				$num--;
			}
			echo "<tr height=1 bgcolor=black><td colspan=5></td></tr>";
			echo "<tr><td align=right><form action=commissiondetail.php method=post><input type=hidden name=commissionid value=$editcommission><input type=text size=4 name=lower></td><td align=right><input type=text size=4 name=upper></td><td align=right><input type=text size=4 name=percent></td><td align=right><input type=text size=4 name=base></td><td><input type=submit value='Add'></td></tr>";
			echo "<tr><td colspan=5></form></td></tr>";
			echo "</table></td></tr>";
		}
		echo "<tr bgcolor=#E8E7E7 height=10><td colspan=2></td></tr>";
		echo "<tr bgcolor=#E8E7E7><td colspan=2><font color=red><ul><li><form action=savecommission.php method=post></font>New Commission: <input type=text size=20 name=commission_name> <input type=submit value='Add'></form></ul></td></tr>";
		//////////////////END COMMISSIONS
	}
*/
	if ( $sec_securities == 1 ) {


		/////////////////////SECURITY/////////////////////////////////

?>
				<tr bgcolor="#e8e7e7">
					<td colspan="2">
						<p></p>
						<br>
						<b><u>
							<a name="security">SECURITY</a>
						</u></b>
					</td>
				</tr>
				<tr bgcolor="#e8e7e7">
					<td colspan="2" style="margin: 0; padding: 0; border-collapse: collapse;">
<?php
	include( DOC_ROOT . '/../application/views/ta/user/security.php' );
?>
					</td>
				</tr>
<?php
		//echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";
	}
?>
			</table>
<?php


	$action2 = "businesstrack.php";
	$method = "post";
?>
			<form action="<?php echo $action2; ?>" method="<?php echo $method; ?>" style="margin-top: 0; padding-top: 0;">
				<input type="hidden" name="username" value="<?php echo $user; ?>" />
				<input type="hidden" name="password" value="<?php echo $pass; ?>" />
				<input type="submit" value=" Return to Main Page" />
			</form>
		</center>
<?php

	//mysql_close();

	google_page_track();
?>
	</body>
</html>
<?php
}
