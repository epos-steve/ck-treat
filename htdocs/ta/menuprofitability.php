<?php

function nextday($nextd,$day_format){ //Function returns next day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monn = substr($nextd, 5, 2);
   $dayn = substr($nextd, 8, 2);
   $yearn = substr($nextd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monn, $dayn+1, $yearn));
   return $tempdate;
}

function prevday($prevd,$day_format){ //Function returns previous day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monp = substr($prevd, 5, 2);
   $dayp = substr($prevd, 8, 2);
   $yearp = substr($prevd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monp, $dayp-1, $yearp));
   return $tempdate;
}

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+4);
        $diff=round($diff, 2);
        if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
        elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
        $diff = substr($diff, 0, $dot+3);
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        return $diff;
}

function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}

function batch_recipe($editid){

    $query = "SELECT * FROM recipe WHERE menu_itemid = '-$editid' ORDER BY recipeid DESC";
    $result = DB_Proxy::query($query);
    $num=mysql_numrows($result);

    $totalprice=0;
    $totalserv=0;
    $counter=1;
    $num--;
    while ($num>=0){
       $recipeid=mysql_result($result,$num,"recipeid");
       $inv_itemid=mysql_result($result,$num,"inv_itemid");
       $rec_num=mysql_result($result,$num,"rec_num");
       $rec_size=mysql_result($result,$num,"rec_size");
       $srv_num=mysql_result($result,$num,"srv_num");
       $rec_order=mysql_result($result,$num,"rec_order");

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM inv_items WHERE inv_itemid = '$inv_itemid'";
       $result2 = DB_Proxy::query($query2);
       //mysql_close();

       $supc=mysql_result($result2,0,"item_code");
       $inv_itemname=mysql_result($result2,0,"item_name");
       $order_size=mysql_result($result2,0,"order_size");
       $price=mysql_result($result2,0,"price");
       if ($rec_order==1){$inv_rec_num=mysql_result($result2,0,"rec_num");}
       elseif ($rec_order==2){$inv_rec_num=mysql_result($result2,0,"rec_num2");}
       $batch=mysql_result($result2,0,"batch");

       if($batch==1){
          $newvalue=batch_recipe($inv_itemid);
          $newvalue=explode("~",$newvalue);
          $totalserv+=$newvalue[0];
          $totalprice+=($newvalue[1]/$inv_rec_num*$rec_num);
       }
       else{

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM conversion WHERE sizeid = '$rec_size' AND sizeidto = '$order_size'";
       $result2 = DB_Proxy::query($query2);
       $num2=mysql_numrows($result2);
       //mysql_close();

       if ($num2!=0){$convert=mysql_result($result2,0,"convert");}
       elseif($num2==0){$convert=$inv_rec_num;}
       else{$convert=1;}

       $newprice=money($price/$convert*$rec_num);
       $totalprice=$totalprice+$newprice;

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM size WHERE sizeid = '$rec_size'";
       $result2 = DB_Proxy::query($query2);
       //mysql_close();

       $sizename=mysql_result($result2,0,"sizename");

       $totalserv=$totalserv+($rec_num*$srv_num);

       }

       $num--;
       $counter++;
    }

    $newvalue="$totalserv~$totalprice";
    return $newvalue;
}

function batch_recipe2($editid,$curdate){
   //////////////////////////////FIND PREVIOUS FC%//////////
    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM recipe WHERE menu_itemid = '-$editid' ORDER BY recipeid DESC";
    $result = DB_Proxy::query($query);
    $num=mysql_numrows($result);
    //mysql_close();

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query2 = "SELECT price FROM price_history WHERE menu_itemid = '-$editid' AND date >= '$curdate' ORDER BY date LIMIT 0,1";
    $result2 = DB_Proxy::query($query2);
    $num2=mysql_numrows($result2);
    //mysql_close();

    if($num2>0){$item_price2=mysql_result($result2,0,"price");}
    else{$item_price2=$item_price;}

    $totalprice2=0;
    $totalserv2=0;
    $counter=1;
    $num--;
    while ($num>=0){
       $recipeid=mysql_result($result,$num,"recipeid");
       $inv_itemid=mysql_result($result,$num,"inv_itemid");
       $rec_num=mysql_result($result,$num,"rec_num");
       $rec_size=mysql_result($result,$num,"rec_size");
       $srv_num=mysql_result($result,$num,"srv_num");
       $rec_order=mysql_result($result,$num,"rec_order");

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM inv_items WHERE inv_itemid = '$inv_itemid'";
       $result2 = DB_Proxy::query($query2);
       //mysql_close();

       $supc=mysql_result($result2,0,"item_code");
       $inv_itemname=mysql_result($result2,0,"item_name");
       $order_size=mysql_result($result2,0,"order_size");
       $price2=mysql_result($result2,0,"price");
       if ($rec_order==1){$inv_rec_num=mysql_result($result2,0,"rec_num");}
       elseif ($rec_order==2){$inv_rec_num=mysql_result($result2,0,"rec_num2");}
       $batch=mysql_result($result2,0,"batch");

       if($batch==1){
          $newvalue=batch_recipe2($inv_itemid,$curdate);
          $newvalue=explode("~",$newvalue);
          $totalserv2+=$newvalue[0];
          $totalprice2+=($newvalue[1]/$inv_rec_num*$rec_num);
       }
       else{

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM cost_history WHERE inv_itemid = '$inv_itemid' AND date >= '$curdate' ORDER BY date LIMIT 0,1";
       $result2 = DB_Proxy::query($query2);
       $num2=mysql_numrows($result2);
       //mysql_close();

       if ($num2>0){$price2=mysql_result($result2,0,"cost");}

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM conversion WHERE sizeid = '$rec_size' AND sizeidto = '$order_size'";
       $result2 = DB_Proxy::query($query2);
       $num2=mysql_numrows($result2);
       //mysql_close();

       if ($num2!=0){$convert=mysql_result($result2,0,"convert");}
       elseif($num2==0){$convert=$inv_rec_num;}
       else{$convert=1;}

       $newprice2=money($price2/$convert*$rec_num);
       $totalprice2=$totalprice2+$newprice2;

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM size WHERE sizeid = '$rec_size'";
       $result2 = DB_Proxy::query($query2);
       //mysql_close();

       $sizename=mysql_result($result2,0,"sizename");

       $totalserv2=$totalserv2+($rec_num*$srv_num);

       }

       $num--;
       $counter++;
    }
}

include("db.php");
require('lib/class.db-proxy.php');
//DB_Proxy::$debug = true;
$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");

$curmenu=$_GET["curmenu"];
$businessid=$_GET["bid"];

$date1=$_POST["date1"];
$date2=$_POST["date2"];
$locationid=$_POST["locationid"];
$rep_price=$_POST["rep_price"];

if($date1==""){
	$date1=$today;
	$date2=$today;
	$rep_price=1;
	
	for($counter=1;$counter<=30;$counter++){$date1=prevday($date1);}
	$date2=prevday($date2);
}

$numserv=1;

    //$nutrition=array();

/////////////////////////MENU PROFITABLITIY////////////////
  ?>
	<head>
	<script type="text/javascript" src="javascripts/prototype.js"></script>
	<script type="text/javascript" src="javascripts/scriptaculous.js"></script>
	<script src="sorttable.js"></script>
	
	<script language="JavaScript"
	type="text/JavaScript">
	function showhide()
	{
     document.all.filter.style.display = 'block';
	}
	
	function hide_show(groupid){
		var rows = $$('.group' + groupid);
		rows.each(function(row){
			if (row.style.display == 'none'){
				row.show();
			}
			else{
				row.hide();
			}
		});
	}
	</script>
	
	<SCRIPT LANGUAGE="JavaScript" SRC="CalendarPopup.js"></SCRIPT>

	<!-- This prints out the default stylehseets used by the DIV style calendar.
     Only needed if you are using the DIV style popup -->
	<SCRIPT LANGUAGE="JavaScript">document.write(getCalendarStyles());</SCRIPT>

<STYLE>
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation
			{
			background-color:#6677DD;
			text-align:center;
			vertical-align:center;
			text-decoration:none;
			color:#FFFFFF;
			font-weight:bold;
			}
	.TESTcpDayColumnHeader,
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation,
	.TESTcpCurrentMonthDate,
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDate,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDate,
	.TESTcpCurrentDateDisabled,
	.TESTcpTodayText,
	.TESTcpTodayTextDisabled,
	.TESTcpText
			{
			font-family:arial;
			font-size:8pt;
			}
	TD.TESTcpDayColumnHeader
			{
			text-align:right;
			border:solid thin #6677DD;
			border-width:0 0 1 0;
			}
	.TESTcpCurrentMonthDate,
	.TESTcpOtherMonthDate,
	.TESTcpCurrentDate
			{
			text-align:right;
			text-decoration:none;
			}
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDateDisabled
			{
			color:#D0D0D0;
			text-align:right;
			text-decoration:line-through;
			}
	.TESTcpCurrentMonthDate
			{
			color:#6677DD;
			font-weight:bold;
			}
	.TESTcpCurrentDate
			{
			color: #FFFFFF;
			font-weight:bold;
			}
	.TESTcpOtherMonthDate
			{
			color:#808080;
			}
	TD.TESTcpCurrentDate
			{
			color:#FFFFFF;
			background-color: #6677DD;
			border-width:1;
			border:solid thin #000000;
			}
	TD.TESTcpCurrentDateDisabled
			{
			border-width:1;
			border:solid thin #FFAAAA;
			}
	TD.TESTcpTodayText,
	TD.TESTcpTodayTextDisabled
			{
			border:solid thin #6677DD;
			border-width:1 0 0 0;
			}
	A.TESTcpTodayText,
	SPAN.TESTcpTodayTextDisabled
			{
			height:20px;
			}
	A.TESTcpTodayText
			{
			color:#6677DD;
			font-weight:bold;
			}
	SPAN.TESTcpTodayTextDisabled
			{
			color:#D0D0D0;
			}
	.TESTcpBorder
			{
			border:solid thin #6677DD;
			}
</STYLE>
	
	</head>
  <?

/////choose commissary
if($businessid<1){
	echo "<body style=\"margin:0;padding:0;\"><table width=100% cellspacing=0 cellpadding=0 style=\"border: 1px solid #E8E7E7;\">";
	echo "<tr bgcolor=#E8E7E7><td colspan=3 style=\"border: 1px solid #E8E7E7;\">Please Choose a Commissary</td></tr>";

	$query12 = "SELECT businessid,businessname,companyid,default_menu FROM business WHERE contract = '2' ORDER BY businessname";
	$result12 = DB_Proxy::query($query12);
	
	$counter=1;
	while($r=mysql_fetch_array($result12)){
		$combusid=$r["businessid"];
		$combusname=$r["businessname"];
		$comcomid=$r["companyid"];
		$curmenu=$r["default_menu"];
		
		if($counter==1){echo "<tr>";}
		
		echo "<td width=33% style=\"border: 1px solid #E8E7E7;\"><center><a href=menuprofitability.php?bid=$combusid&cid=$comcomid&curmenu=$curmenu style=$style><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'><b>$combusname</b></font></a></center></td>";
		
		$counter++;
		if($counter==4){echo "</tr>";$counter=1;}
	}
	if($counter==2){echo "<td style=\"border: 1px solid #E8E7E7;\">&nbsp;</td><td style=\"border: 1px solid #E8E7E7;\">&nbsp;</td></tr>";}
	elseif($counter==3){echo "<td style=\"border: 1px solid #E8E7E7;\">&nbsp;</td></tr>";}
	elseif($counter==1){echo "</tr>";}
	
	echo"</table></body>";
}
///else display report
else{
  echo "<body style=\"margin:0;padding:0;\">";

  $curdate=$today;
  for($counter2=1;$counter2<=30;$counter2++){$curdate=prevday($curdate);}

  $query12 = "SELECT businessname,contract FROM business WHERE businessid = '$businessid'";
  $result12 = DB_Proxy::query($query12);
  //mysql_close();

  $businessname=mysql_result($result12,0,"businessname");
  $contract=mysql_result($result12,0,"contract");
  
  if($locationid>0){
	$query12 = "SELECT location_name FROM vend_locations WHERE locationid = '$locationid'";
	$result12 = DB_Proxy::query($query12);

	$location_name=mysql_result($result12,0,"location_name");
  }
  elseif($locationid<0){
	$newroute=$locationid*-1;
	$query12 = "SELECT route FROM login_route WHERE login_routeid = '$newroute'";
	$result12 = DB_Proxy::query($query12);

	$location_name=mysql_result($result12,0,"route");
	$location_name="Route $location_name";
  }
  else{$location_name="All Locations";}

  //mysql_connect($dbhost,$username,$password);
  //@mysql_select_db($database) or die( "Unable to select database");
  $query12 = "SELECT menu_typename FROM menu_type WHERE menu_typeid = '$curmenu'";
  $result12 = DB_Proxy::query($query12);
  //mysql_close();

  $curmenuname=mysql_result($result12,0,"menu_typename");

  if($rep_price==1){
	$showheader="Route Figures";
  }
  else{
	$showheader="Commissary Figures";
  }
  
  $showday=substr($date1,8,2);
  $showmonth=substr($date1,5,2);
  $showyear=substr($date1,2,2);
  $showdate1="$showmonth/$showday/$showyear";
  $showday=substr($date2,8,2);
  $showmonth=substr($date2,5,2);
  $showyear=substr($date2,2,2);
  $showdate2="$showmonth/$showday/$showyear";
  
  echo "<center><table width=100% style=\"border: 2px solid #CCCCCC;\" bgcolor=#E8E7E7><tr><td colspan=11><center><b>Menu Profitability: $curmenuname - $businessname [$showdate1 to $showdate2 $showheader for $location_name]</b> &nbsp;&nbsp;<a href=\"javascript:void();\" onclick=\"showhide();\" style=$style><font size=2 COLOR=#0000FF onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#0000FF'>Filter</font></a> | <a href=menuprofitability.php style=$style><font size=2 COLOR=#0000FF onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#0000FF'>Change Location</font></a></td></tr>";

  /////filter
  echo "<tr style=\"display: none;\" id='filter'><td colspan=11><center>";
	echo "&nbsp;<FORM action='menuprofitability.php?bid=$businessid&curmenu=$curmenu&com=1' method='post' style=\"margin:0;padding:0;display:inline;\">";
    echo "<input type=hidden name=businessid value=$businessid>";
    echo "<input type=hidden name=companyid value=$companyid>";

    echo "<SCRIPT LANGUAGE='JavaScript' ID='js34'> var cal34 = new CalendarPopup('testdiv1');cal34.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date1 VALUE='$date1' SIZE=8> <A HREF=\"javascript:void();\" onClick=cal34.select(document.forms[0].date1,'anchor34','yyyy-MM-dd'); return false; TITLE=cal34.select(document.forms[0].date1,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor34' ID='anchor34'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A> to <SCRIPT LANGUAGE='JavaScript' ID='js45'> var cal45 = new CalendarPopup('testdiv1');cal45.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date2 VALUE='$date2' SIZE=8> <A HREF=\"javascript:void();\" onClick=cal45.select(document.forms[0].date2,'anchor45','yyyy-MM-dd'); return false; TITLE=cal45.select(document.forms[0].date2,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor45' ID='anchor45'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A>";

    echo "<input type=hidden name=curmenu value='$curmenu'> <select name=locationid>";

	$query3 = "SELECT * FROM vend_locations WHERE businessid = '$businessid' ORDER BY locationid DESC";
    $result3 = DB_Proxy::query($query3);
    $num3=mysql_numrows($result3);

	echo "<option value=0>All Locations</option>";
    $num3--;
    while ($num3>=0){
       $newlocation_name=mysql_result($result3,$num3,"location_name");
       $newlocationid=mysql_result($result3,$num3,"locationid");
	   
	   if($locationid==$newlocationid){$sel="SELECTED";}
	   else{$sel="";}
	   
       echo "<option value=$newlocationid $sel>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$newlocation_name</option>";
	   
		/////show routes
		$query4 = "SELECT * FROM login_route WHERE locationid = '$newlocationid' AND active = '0' AND is_deleted = '0' ORDER BY route";
		$result4 = DB_Proxy::query($query4);
		
		while($r=mysql_fetch_array($result4)){
			$login_routeid=$r["login_routeid"];
			$route=$r["route"];
			
			if(($locationid*-1)==$login_routeid){$sel="SELECTED";}
			else{$sel="";}
			
			echo "<option value='-$login_routeid' $sel>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$route</option>";
		}
	   
       $num3--;
    }
	
	if($rep_price==1){$sel1="SELECTED";}
	
	echo "</select> <select name=rep_price><option value=0>Commissary Figures</option><option value=1 $sel1>Route Figures</option></select> <INPUT TYPE=submit VALUE='GO'></form>&nbsp;";
  echo "</center></td></tr>";
  //////end filter
  
  echo "</table><br>";
  
  //mysql_connect($dbhost,$username,$password);
  //@mysql_select_db($database) or die( "Unable to select database");
  $query12 = "SELECT menu_items.*, menu_pricegroup.menu_groupid FROM menu_items,menu_pricegroup WHERE menu_items.businessid = '$businessid' AND menu_items.menu_typeid = '$curmenu' AND menu_items.groupid = menu_pricegroup.menu_pricegroupid ORDER BY menu_pricegroup.menu_groupid, menu_items.groupid DESC, menu_items.item_name DESC";
  $result12 = DB_Proxy::query($query12);
  $num12=mysql_numrows($result12);
  //mysql_close();

  $totalitems=$num12;
  $totalfcprcnt=0;
  $totalchange=0;

  $lastgroupid=0;
  $lastmenu_pricegroupid=0;
  $num12--;
  while($num12>=0){

    $editid=mysql_result($result12,$num12,"menu_item_id");
    $itemname=mysql_result($result12,$num12,"item_name");
    $groupid=mysql_result($result12,$num12,"groupid");
    $serving_size=mysql_result($result12,$num12,"serving_size");
    $item_price=mysql_result($result12,$num12,"price"); 
    $su_in_ou=mysql_result($result12,$num12,"su_in_ou");
	
	$menu_pricegroupid=mysql_result($result12,$num12,"menu_pricegroup.menu_groupid");

    ///////SHOW GROUPS
    if ($lastgroupid!=$groupid){
		  /////show totals
		  if($lastgroupid!=0){
		  
			 if($rep_price==1){$sheetprofit=$sheetprice-$sheetcosts-$sheetwaste;}
			 else{$sheetprofit=$sheetprice-$sheetcosts;}
			 $sheetprofitpercent=round($sheetprofit/$sheetprice*100,2);
			 $sheetprofit=number_format($sheetprofit,2);
			 $sheetcosts=number_format($sheetcosts,2);
			 $sheetprice=number_format($sheetprice,2);
			 echo "</tbody><tfoot><tr bgcolor=#ffff99><td colspan=8><font size=2><b>Totals</td><td align=right><font size=2><b>$sheetcosts</td><td align=right><font size=2><b>$sheetprice</td><td align=right><font size=2><b>$sheetprofit</td><td align=right><font size=2><b>$sheetprofitpercent%</td></tr></tfoot></table></center> <center><div style=\"display: none;\" class=\"group$lastmenu_pricegroupid\"><br></div></center>";
			 $sheetprice=0;
			 $sheetcosts=0;
			 $sheetprofit=0;
			 $sheetprofitpercent=0;
			 $sheetwaste=0;
		  }
		  
		  ////show menu group totals
		  if($lastmenu_pricegroupid!=$menu_pricegroupid&&$lastmenu_pricegroupid!=0){
			 $groupname=explode("-",$groupname);
			 $menugroup=$groupname[0];
			 
			 $allgroup_totalamt+=$group_totalamt;
			 $allgroup_totalwaste+=$group_totalwaste;
			 $allgroup_totalwasteamt+=$group_totalwasteamt;
			 $allgroup_totalprice+=$group_totalprice;
			 $allgroup_item_price+=$group_item_price;
			 $allgroup_profit+=$group_profit;
			 $allgroup_grossprofit+=$group_grossprofit;
			 $allgroup_totalcosts+=$group_totalcosts;
			 $allgroup_totalsales+=$group_totalsales;
			 $allgroup_count+=$group_count;
			 
			 $group_wasteprcnt=round($group_totalwaste/$group_totalamt*100,1);
			 $group_totalprice=number_format($group_totalprice/$group_count,2);
			 $group_item_price=number_format($group_item_price/$group_count,2);
			 $group_profit=number_format($group_item_price-$group_totalprice,2);
			 $group_fcprcnt=round($group_totalprice/$group_item_price*100,1);
			 if($rep_price==1){$group_grossprofit=money($group_totalsales-$group_totalcosts-$group_totalwasteamt);}
			 else{$group_grossprofit=money($group_totalsales-$group_totalcosts);}
			 $group_profitpercent=round($group_grossprofit/$group_totalsales*100,1);
			 $group_grossprofit=number_format($group_grossprofit,2);
			 $group_totalcosts=number_format($group_totalcosts,2);
			 $group_totalsales=number_format($group_totalsales,2);
	 
			 echo "<center><table width=100% style=\"border: 2px solid #999999;\"><thead><tr><th bgcolor=#CCCC00><font size=2><b>Group Total</b> <a href=\"javascript:void(0);\" style=$style onmousedown=\"hide_show($lastmenu_pricegroupid);\"><font color=blue>[Hide/Show Items]</a></th><th bgcolor=#CCCC00 align=right width=6%><font size=2><b>Ordered</th><th bgcolor=#CCCC00 align=right width=6%><font size=2><b>Waste</th><th bgcolor=#CCCC00 align=right width=6%><font size=2><b>Waste%</th><th bgcolor=#CCCC00 align=right width=6%><font size=2><b>Cost</th><th bgcolor=#CCCC00 align=right width=6%><font size=2><b>Price</th><th bgcolor=#CCCC00 align=right width=6%><font size=2><b>Profit</th><th bgcolor=#CCCC00 align=right width=6%><font size=2><b>Food Cost</th><th bgcolor=#CCCC00 align=right width=6%><font size=2><b>Total Costs</th><th bgcolor=#CCCC00 align=right width=6%><font size=2><b>Total Sales</th><th bgcolor=#CCCC00 align=right width=6%><font size=2><b>Gross Profit</th><th bgcolor=#CCCC00 align=right width=6%><font size=2><b>Profit%</th></tr></thead><tbody>";
			 echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor='white'><td><font size=2>$menugroup</font></td><td align=right><font size=2>$group_totalamt</td><td align=right><font size=2>$group_totalwaste</td><td align=right><font size=2>$group_wasteprcnt%</td><td align=right><font size=2>$group_totalprice</td><td align=right><font size=2>$group_item_price</td><td align=right><font size=2>$group_profit</td><td align=right><font size=2>$group_fcprcnt%</td><td align=right><font size=2>$group_totalcosts</td><td align=right><font size=2>$group_totalsales</td><td align=right><font size=2>$group_grossprofit</td><td align=right><font size=2>$group_profitpercent%</td></tr></table><br>";
			 
			 $group_totalamt=0;
			 $group_totalwaste=0;
			 $group_totalwasteamt=0;
			 $group_totalprice=0;
			 $group_item_price=0;
			 $group_profit=0;
			 $group_grossprofit=0;
			 $group_totalcosts=0;
			 $group_totalsales=0;
			 $group_count=0;
		  }
	
          if($contract==2){
             $query5 = "SELECT menu_pricegroupname,menu_groupid,price,foodcost FROM menu_pricegroup WHERE menu_pricegroupid = '$groupid'";
             $result5 = DB_Proxy::query($query5); 

             $groupname=mysql_result($result5,0,"menu_pricegroupname");
             $menu_groupid=mysql_result($result5,0,"menu_groupid");
			 $menu_groupprice=mysql_result($result5,0,"price");
			 $menu_group_fc=mysql_result($result5,0,"foodcost");

             $query5 = "SELECT groupname FROM menu_groups WHERE menu_group_id = '$menu_groupid'";
             $result5 = DB_Proxy::query($query5); 

             $groupname2=mysql_result($result5,0,"groupname");

             $groupname = "$groupname2 - $groupname";
          }
          else{
             $query5 = "SELECT groupname FROM menu_groups WHERE menu_group_id = '$groupid'";
             $result5 = DB_Proxy::query($query5); 

             $groupname=mysql_result($result5,0,"groupname");
          }
		echo "<center><table width=100% style=\"border: 2px solid #CCCCCC; display: none;\" class=\"group$menu_pricegroupid sortable\"><thead><tr><th bgcolor=#E8E7E7><font size=2><b>$groupname</th><th bgcolor=#E8E7E7 align=right width=6%><font size=2><b>Ordered</th><th bgcolor=#E8E7E7 align=right width=6%><font size=2><b>Waste</th><th bgcolor=#E8E7E7 align=right width=6%><font size=2><b>Waste%</th><th bgcolor=#E8E7E7 align=right width=6%><font size=2><b>Cost</th><th bgcolor=#E8E7E7 align=right width=6%><font size=2><b>Price</th><th bgcolor=#E8E7E7 align=right width=6%><font size=2><b>Profit</th><th bgcolor=#E8E7E7 align=right width=6%><font size=2><b>Food Cost</th><th bgcolor=#E8E7E7 align=right width=6%><font size=2><b>Total Costs</th><th bgcolor=#E8E7E7 align=right width=6%><font size=2><b>Total Sales</th><th bgcolor=#E8E7E7 align=right width=6%><font size=2><b>Gross Profit</th><th bgcolor=#E8E7E7 align=right width=6%><font size=2><b>Profit%</th></tr></thead><tbody>";
    }

    ////////get recipe if commissary
	if($rep_price!=1){
    $query = "SELECT * FROM recipe WHERE menu_itemid = '$editid' ORDER BY recipeid DESC";
    $result = DB_Proxy::query($query);
    $num=mysql_numrows($result);

    $totalprice=0;
    $totalserv=0;
    $counter=1;
    $num--;
    while ($num>=0){
       $recipeid=mysql_result($result,$num,"recipeid");
       $inv_itemid=mysql_result($result,$num,"inv_itemid");
       $rec_num=mysql_result($result,$num,"rec_num");
       $rec_size=mysql_result($result,$num,"rec_size");
       $srv_num=mysql_result($result,$num,"srv_num");
       $rec_order=mysql_result($result,$num,"rec_order");

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM inv_items WHERE inv_itemid = '$inv_itemid'";
       $result2 = DB_Proxy::query($query2);
       //mysql_close();

       $supc=mysql_result($result2,0,"item_code");
       $inv_itemname=mysql_result($result2,0,"item_name");
       $order_size=mysql_result($result2,0,"order_size");
       $price=mysql_result($result2,0,"price");
       if ($rec_order==1){$inv_rec_num=mysql_result($result2,0,"rec_num");}
       elseif ($rec_order==2){$inv_rec_num=mysql_result($result2,0,"rec_num2");}
       $batch=mysql_result($result2,0,"batch");

       if($batch==1){
          $newvalue=batch_recipe($inv_itemid);
          $newvalue=explode("~",$newvalue);
          $totalserv+=$newvalue[0];
          $totalprice+=($newvalue[1]/$inv_rec_num*$rec_num);
       }
       else{

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM conversion WHERE sizeid = '$rec_size' AND sizeidto = '$order_size'";
       $result2 = DB_Proxy::query($query2);
       $num2=mysql_numrows($result2);
       //mysql_close();

       if ($num2!=0){$convert=mysql_result($result2,0,"convert");}
       elseif($num2==0){$convert=$inv_rec_num;}
       else{$convert=1;}

       $newprice=money($price/$convert*$rec_num);
       $totalprice=$totalprice+$newprice;

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM size WHERE sizeid = '$rec_size'";
       $result2 = DB_Proxy::query($query2);
       //mysql_close();

       $sizename=mysql_result($result2,0,"sizename");

       $totalserv=$totalserv+($rec_num*$srv_num);

       }

       $num--;
       $counter++;
    }
	//////end recipe
	}

	if($rep_price==1){
		$item_price=money($item_price*$serving_size);
		$totalprice=money($menu_groupprice);
	}
	else{
		$item_price=money($menu_groupprice);
		$totalprice=money($totalprice);
	}
	
    $profit=money($item_price-$totalprice);
    $fcprcnt=round($totalprice/$item_price*100,1);

    $totalfcprcnt=$totalfcprcnt+$fcprcnt;

	/*
    //////////////////////////////FIND PREVIOUS FC%//////////
    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM recipe WHERE menu_itemid = '$editid' ORDER BY recipeid DESC";
    $result = DB_Proxy::query($query);
    $num=mysql_numrows($result);
    //mysql_close();

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query2 = "SELECT price FROM price_history WHERE menu_itemid = '$editid' AND date >= '$curdate' ORDER BY date LIMIT 0,1";
    $result2 = DB_Proxy::query($query2);
    $num2=mysql_numrows($result2);
    //mysql_close();

    if($num2>0){$item_price2=mysql_result($result2,0,"price");}
    else{$item_price2=$item_price;}

    $totalprice2=0;
    $totalserv2=0;
    $counter=1;
    $num--;
    while ($num>=0){
       $recipeid=mysql_result($result,$num,"recipeid");
       $inv_itemid=mysql_result($result,$num,"inv_itemid");
       $rec_num=mysql_result($result,$num,"rec_num");
       $rec_size=mysql_result($result,$num,"rec_size");
       $srv_num=mysql_result($result,$num,"srv_num");
       $rec_order=mysql_result($result,$num,"rec_order");

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM inv_items WHERE inv_itemid = '$inv_itemid'";
       $result2 = DB_Proxy::query($query2);
       //mysql_close();

       $supc=mysql_result($result2,0,"item_code");
       $inv_itemname=mysql_result($result2,0,"item_name");
       $order_size=mysql_result($result2,0,"order_size");
       $price2=mysql_result($result2,0,"price");
       if ($rec_order==1){$inv_rec_num=mysql_result($result2,0,"rec_num");}
       elseif ($rec_order==2){$inv_rec_num=mysql_result($result2,0,"rec_num2");}
       $batch=mysql_result($result2,0,"batch");

       if($batch==1){
          $newvalue=batch_recipe2($inv_itemid,$curdate);
          $newvalue=explode("~",$newvalue);
          $totalserv2+=$newvalue[0];
          $totalprice2+=($newvalue[1]/$inv_rec_num*$rec_num);
       }
       else{

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM cost_history WHERE inv_itemid = '$inv_itemid' AND date >= '$curdate' ORDER BY date LIMIT 0,1";
       $result2 = DB_Proxy::query($query2);
       $num2=mysql_numrows($result2);
       //mysql_close();

       if ($num2>0){$price2=mysql_result($result2,0,"cost");}

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM conversion WHERE sizeid = '$rec_size' AND sizeidto = '$order_size'";
       $result2 = DB_Proxy::query($query2);
       $num2=mysql_numrows($result2);
       //mysql_close();

       if ($num2!=0){$convert=mysql_result($result2,0,"convert");}
       elseif($num2==0){$convert=$inv_rec_num;}
       else{$convert=1;}

       $newprice2=money($price2/$convert*$rec_num);
       $totalprice2=$totalprice2+$newprice2;

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM size WHERE sizeid = '$rec_size'";
       $result2 = DB_Proxy::query($query2);
       //mysql_close();

       $sizename=mysql_result($result2,0,"sizename");

       $totalserv2=$totalserv2+($rec_num*$srv_num);

       }

       $num--;
       $counter++;
    }

    $fcprcnt2=round($totalprice2/$item_price2*100,1);

    $prcnt_diff=round($fcprcnt-$fcprcnt2,1);

    $totalchange=$totalchange+$prcnt_diff;

    if ($prcnt_diff>0){$prcnt_diff="<font color=red>+$prcnt_diff%</font>";}
    elseif ($prcnt_diff<0){$prcnt_diff="<font color=green>$prcnt_diff%</font>";}
    else {$prcnt_diff="";}
    /////////////////////////////////////////////////////////////////////////////////////////////////
	*/
	
	//////////GET ITEMS ORDERED
	if($locationid<0){$login_routeid=$locationid*-1;}
	
	if($locationid>0){$query2 = "SELECT SUM(vend_order.amount) AS totalamt FROM vend_item,vend_order WHERE vend_order.locationid = '$locationid' AND vend_item.vend_itemid = vend_order.vend_itemid AND vend_item.menu_itemid = '$editid' AND vend_item.date >= '$date1' AND vend_item.date <= '$date2'";}
	elseif($locationid<0){$query2 = "SELECT SUM(vend_order.amount) AS totalamt FROM vend_item,vend_order WHERE vend_order.login_routeid = '$login_routeid' AND vend_item.vend_itemid = vend_order.vend_itemid AND vend_item.menu_itemid = '$editid' AND vend_item.date >= '$date1' AND vend_item.date <= '$date2'";}
	else{$query2 = "SELECT SUM(vend_order.amount) AS totalamt FROM vend_order,vend_item WHERE vend_order.vend_itemid = vend_item.vend_itemid AND vend_item.menu_itemid = '$editid' AND vend_item.date >= '$date1' AND vend_item.date <= '$date2'";}
    $result2 = DB_Proxy::query($query2);
	
	$totalamt=mysql_result($result2,0,"totalamt");
	
	//////////GET WASTE
	if($locationid>0){$query2 = "SELECT SUM(vend_waste_detail.qty) AS totalwaste FROM vend_waste,vend_waste_detail WHERE vend_waste.date >= '$date1' AND vend_waste.date <= '$date2' AND vend_waste.locationid = '$locationid' AND vend_waste.vend_wasteid = vend_waste_detail.vend_wasteid AND vend_waste_detail.menu_itemid = '$editid'";}
	elseif($locationid<0){$query2 = "SELECT SUM(vend_waste_detail.qty) AS totalwaste FROM vend_waste,vend_waste_detail WHERE vend_waste.date >= '$date1' AND vend_waste.date <= '$date2' AND vend_waste.login_routeid = '$login_routeid' AND vend_waste.vend_wasteid = vend_waste_detail.vend_wasteid AND vend_waste_detail.menu_itemid = '$editid'";}
	else{$query2 = "SELECT SUM(vend_waste_detail.qty) AS totalwaste FROM vend_waste,vend_waste_detail WHERE vend_waste.date >= '$date1' AND vend_waste.date <= '$date2' AND vend_waste.vend_wasteid = vend_waste_detail.vend_wasteid AND vend_waste_detail.menu_itemid = '$editid'";}
    $result2 = DB_Proxy::query($query2);
	
	$totalwaste=mysql_result($result2,0,"totalwaste");

    if ($fcprcnt>=$menu_group_fc){$fcprcnt="<font color=red><b>$fcprcnt</b></font>";}
    if($serving_size>1){$totalprice=money($totalprice/$serving_size);$item_price=money($item_price/$serving_size);}

	$wasteprcnt=round($totalwaste/$totalamt*100,1);
	if($wasteprcnt>50){$wasteprcnt="<font color=red><b>$wasteprcnt</b></font>";}
	
	$totalcosts=money($totalamt*$totalprice);
	$totalsales=money($totalamt*$item_price);
	if($rep_price==1){$grossprofit=money($totalsales-$totalcosts-($totalwaste*$totalprice));}
	else{$grossprofit=money($totalsales-$totalcosts);}
	$profitpercent=round($grossprofit/$totalsales*100,2);
	
    if($totalamt>0||$totalwaste>0){
		$sheetcosts+=$totalcosts;
		$sheetprice+=$totalsales;
		$sheetwaste+=$totalwaste*$totalprice;
	
		$group_totalamt+=$totalamt;
		$group_totalwaste+=$totalwaste;
		$group_totalwasteamt+=$totalwaste*$totalprice;
		$group_totalprice+=$totalprice;
		$group_item_price+=$item_price;
		$group_profit+=$profit;
		$group_grossprofit+=$grossprofit;
		$group_totalcosts+=$totalcosts;
		$group_totalsales+=$totalsales;
		$group_count++;
	
		$totalcosts=number_format($totalcosts,2);
		$totalsales=number_format($totalsales,2);
		$grossprofit=number_format($grossprofit,2);
		
		echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor='white'><td><font size=2>$itemname</font></td><td align=right><font size=2>$totalamt</td><td align=right><font size=2>$totalwaste</td><td align=right><font size=2>$wasteprcnt%</td><td align=right><font size=2>$totalprice</td><td align=right><font size=2>$item_price</td><td align=right><font size=2>$profit</td><td align=right><font size=2>$fcprcnt%</td><td align=right><font size=2>$totalcosts</td><td align=right><font size=2>$totalsales</td><td align=right><font size=2>$grossprofit</td><td align=right><font size=2>$profitpercent%</td></tr>";
		//echo "<tr height=1><td colspan=11 bgcolor=#E8E7E7></td></tr>";
	}
    
    $lastgroupid=$groupid;
	$lastmenu_pricegroupid=$menu_pricegroupid;
    $num12--;
  }
  
  //////last group total
  if($rep_price==1){$sheetprofit=$sheetprice-$sheetcosts-$sheetwaste;}
  else{$sheetprofit=$sheetprice-$sheetcosts;}
  $sheetprofitpercent=round($sheetprofit/$sheetprice*100,2);
  $sheetprofit=number_format($sheetprofit,2);
  $sheetcosts=number_format($sheetcosts,2);
  $sheetprice=number_format($sheetprice,2);
  echo "</tbody><tfoot><tr bgcolor=#ffff99><td colspan=8><font size=2><b>Totals</td><td align=right><font size=2><b>$sheetcosts</td><td align=right><font size=2><b>$sheetprice</td><td align=right><font size=2><b>$sheetprofit</td><td align=right><font size=2><b>$sheetprofitpercent%</td></tr></tfoot></table>";
  $sheetprice=0;
  $sheetcosts=0;
  $sheetprofit=0;
  $sheetprofitpercent=0;
  
  $av_fc=round($totalfcprcnt/$totalitems,1);
  $totalchange=round($av_fc-($totalchange/$totalitems),1);

  echo "</table>";
  
  
  
  /////last menu group
  ////show menu group totals
			 echo "<div style=\"display: none;\" class=\"group$lastmenu_pricegroupid\"><br></div>";

			 $groupname=explode("-",$groupname);
			 $menugroup=$groupname[0];
			 
			 $allgroup_totalamt+=$group_totalamt;
			 $allgroup_totalwaste+=$group_totalwaste;
			 $allgroup_totalwasteamt+=$group_totalwasteamt;
			 $allgroup_totalprice+=$group_totalprice;
			 $allgroup_item_price+=$group_item_price;
			 $allgroup_profit+=$group_profit;
			 $allgroup_grossprofit+=$group_grossprofit;
			 $allgroup_totalcosts+=$group_totalcosts;
			 $allgroup_totalsales+=$group_totalsales;
			 $allgroup_count+=$group_count;
			 
			 $group_wasteprcnt=round($group_totalwaste/$group_totalamt*100,1);
			 $group_totalprice=number_format($group_totalprice/$group_count,2);
			 $group_item_price=number_format($group_item_price/$group_count,2);
			 $group_profit=number_format($group_item_price-$group_totalprice,2);
			 $group_fcprcnt=round($group_totalprice/$group_item_price*100,1);
			 if($rep_price==1){$group_grossprofit=money($group_totalsales-$group_totalcosts-$group_totalwasteamt);}
			 else{$group_grossprofit=money($group_totalsales-$group_totalcosts);}
			 $group_profitpercent=round($group_grossprofit/$group_totalsales*100,1);
			 $group_grossprofit=number_format($group_grossprofit,2);
			 $group_totalcosts=number_format($group_totalcosts,2);
			 $group_totalsales=number_format($group_totalsales,2);
			 
			 echo "<center><table width=100% style=\"border: 2px solid #999999;\"><thead><tr><th bgcolor=#CCCC00><font size=2><b>Group Total</b> <a href=\"javascript:void(0);\" style=$style onmousedown=\"hide_show($lastmenu_pricegroupid);\"><font color=blue>[Hide/Show Items]</a></th><th bgcolor=#CCCC00 align=right width=6%><font size=2><b>Ordered</th><th bgcolor=#CCCC00 align=right width=6%><font size=2><b>Waste</th><th bgcolor=#CCCC00 align=right width=6%><font size=2><b>Waste%</th><th bgcolor=#CCCC00 align=right width=6%><font size=2><b>Cost</th><th bgcolor=#CCCC00 align=right width=6%><font size=2><b>Price</th><th bgcolor=#CCCC00 align=right width=6%><font size=2><b>Profit</th><th bgcolor=#CCCC00 align=right width=6%><font size=2><b>Food Cost</th><th bgcolor=#CCCC00 align=right width=6%><font size=2><b>Total Costs</th><th bgcolor=#CCCC00 align=right width=6%><font size=2><b>Total Sales</th><th bgcolor=#CCCC00 align=right width=6%><font size=2><b>Gross Profit</th><th bgcolor=#CCCC00 align=right width=6%><font size=2><b>Profit%</th></tr></thead><tbody>";
			 echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor='white'><td><font size=2>$menugroup</font></td><td align=right><font size=2>$group_totalamt</td><td align=right><font size=2>$group_totalwaste</td><td align=right><font size=2>$group_wasteprcnt%</td><td align=right><font size=2>$group_totalprice</td><td align=right><font size=2>$group_item_price</td><td align=right><font size=2>$group_profit</td><td align=right><font size=2>$group_fcprcnt%</td><td align=right><font size=2>$group_totalcosts</td><td align=right><font size=2>$group_totalsales</td><td align=right><font size=2>$group_grossprofit</td><td align=right><font size=2>$group_profitpercent%</td></tr></table><br>";
			 
	///////sheet totals
			 $allgroup_wasteprcnt=round($allgroup_totalwaste/$allgroup_totalamt*100,1);
			 $allgroup_totalprice=number_format($allgroup_totalprice/$allgroup_count,2);
			 $allgroup_item_price=number_format($allgroup_item_price/$allgroup_count,2);
			 $allgroup_profit=number_format($allgroup_item_price-$allgroup_totalprice,2);
			 $allgroup_fcprcnt=round($allgroup_totalprice/$allgroup_item_price*100,1);
			 if($rep_price==1){$allgroup_grossprofit=money($allgroup_totalsales-$allgroup_totalcosts-$allgroup_totalwasteamt);}
			 else{$allgroup_grossprofit=money($allgroup_totalsales-$allgroup_totalcosts);}
			 $allgroup_profitpercent=round($allgroup_grossprofit/$allgroup_totalsales*100,1);
			 $allgroup_grossprofit=number_format($allgroup_grossprofit,2);
			 $allgroup_totalcosts=number_format($allgroup_totalcosts,2);
			 $allgroup_totalsales=number_format($allgroup_totalsales,2);
			 
			 echo "<center><table width=100% style=\"border: 2px solid #999999;\"><thead><tr><th bgcolor=#CCCC00><font size=2><b>&nbsp;</b> </th><th bgcolor=#CCCC00 align=right width=6%><font size=2><b>Ordered</th><th bgcolor=#CCCC00 align=right width=6%><font size=2><b>Waste</th><th bgcolor=#CCCC00 align=right width=6%><font size=2><b>Waste%</th><th bgcolor=#CCCC00 align=right width=6%><font size=2><b>Cost</th><th bgcolor=#CCCC00 align=right width=6%><font size=2><b>Price</th><th bgcolor=#CCCC00 align=right width=6%><font size=2><b>Profit</th><th bgcolor=#CCCC00 align=right width=6%><font size=2><b>Food Cost</th><th bgcolor=#CCCC00 align=right width=6%><font size=2><b>Total Costs</th><th bgcolor=#CCCC00 align=right width=6%><font size=2><b>Total Sales</th><th bgcolor=#CCCC00 align=right width=6%><font size=2><b>Gross Profit</th><th bgcolor=#CCCC00 align=right width=6%><font size=2><b>Profit%</th></tr></thead><tbody>";
			 echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor='white'><td><font size=2>Sheet Totals</font></td><td align=right><font size=2>$allgroup_totalamt</td><td align=right><font size=2>$allgroup_totalwaste</td><td align=right><font size=2>$allgroup_wasteprcnt%</td><td align=right><font size=2>$allgroup_totalprice</td><td align=right><font size=2>$allgroup_item_price</td><td align=right><font size=2>$allgroup_profit</td><td align=right><font size=2>$allgroup_fcprcnt%</td><td align=right><font size=2>$allgroup_totalcosts</td><td align=right><font size=2>$allgroup_totalsales</td><td align=right><font size=2>$allgroup_grossprofit</td><td align=right><font size=2>$allgroup_profitpercent%</td></tr></table><br>";
	 
  
  echo "<DIV ID=testdiv1 STYLE=position:absolute;visibility:hidden;background-color:white;layer-background-color:white;></DIV></body>";
}
//mysql_close();
google_page_track();
?>