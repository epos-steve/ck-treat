<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once(DOC_ROOT.'/bootstrap.php');

function rndm_color_code($b_safe = TRUE) {
    
    //make sure the parameter is boolean
    if(!is_bool($b_safe)) {return FALSE;}
    
    //if a browser safe color is requested then set the array up
    //so that only a browser safe color can be returned
    if($b_saafe) {
        $ary_codes = array('00','33','66','99','CC','FF');
        $max = 5; //the highest array offest
    //if a browser safe color is not requested then set the array
    //up so that any color can be returned.
    } else {
        $ary_codes = array();
        for($i=0;$i<16;$i++) {
            $t_1 = dechex($i);
            for($j=0;$j<16;$j++) {
                $t_2 = dechex($j);
                $ary_codes[] = "$t_1$t_2";
            } //end for j
        } //end for i
        $max = 256; //the highest array offset
    } //end if
    
    $retVal = '';
    
    //generate a random color code
    for($i=0;$i<3;$i++) {
        $offset = rand(0,$max);
        $retVal .= $ary_codes[$offset];
    } //end for i
    
    return $retVal;
} //end rndm_color_code

$style = "text-decoration:none";

/*$user=$_COOKIE["usercook"];
$pass=$_COOKIE["passcook"];
$companyid=$_COOKIE["compcook"];*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook');
$companyid = \EE\Controller\Base::getSessionCookieVariable('compcook');

/*$businessid=$_POST["businessid"];
$year=$_POST["startyear"];
$budgetyear=$_POST["budgetyear"];
$service=$_POST["service"];*/

$businessid = \EE\Controller\Base::getPostVariable('businessid');
$year = \EE\Controller\Base::getPostVariable('startyear');
$budgetyear = \EE\Controller\Base::getPostVariable('budgetyear');
$service = \EE\Controller\Base::getPostVariable('service');
$businessid2 = \EE\Controller\Base::getPostVariable('businessid2');

//$businessid2=unserialize(urldecode($_POST["businessid2"]));
$businessid2=unserialize(urldecode($businessid2));

/*$newbudget=$_POST["newbudget"];
$budget_bus=$_POST["mybusid"];*/

$newbudget = \EE\Controller\Base::getPostVariable('newbudget');
$budget_bus = \EE\Controller\Base::getPostVariable('mybusid');

//if($businessid==""){$businessid[0]=$_COOKIE["budgetbus"];}
if($businessid==""){$businessid[0]= \EE\Controller\Base::getSessionCookieVariable('budgetbus');}

//if ($year==""){$year=$_COOKIE["startyear"];}
if ($year==""){$year= \EE\Controller\Base::getSessionCookieVariable('budgetbus');}
if ($year==""){$year=date("Y");}

//if ($budgetyear==""){$budgetyear=$_COOKIE["budgetyear"];}
if ($budgetyear==""){$budgetyear= \EE\Controller\Base::getSessionCookieVariable('budgetyear');}
if ($budgetyear==""){$budgetyear=date("Y");$budgetyear++;}

setcookie("startyear",$year);
setcookie("budgetyear",$budgetyear);
setcookie("budgetbus",$businessid[0]);

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( mysql_error());

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=Treat_DB_ProxyOld::mysql_numrows($result);

$security_level=Treat_DB_ProxyOld::mysql_result($result,0,"security_level");
$bid=Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
$bid2=Treat_DB_ProxyOld::mysql_result($result,0,"busid2");
$bid3=Treat_DB_ProxyOld::mysql_result($result,0,"busid3");
$bid4=Treat_DB_ProxyOld::mysql_result($result,0,"busid4");
$bid5=Treat_DB_ProxyOld::mysql_result($result,0,"busid5");
$bid6=Treat_DB_ProxyOld::mysql_result($result,0,"busid6");
$bid7=Treat_DB_ProxyOld::mysql_result($result,0,"busid7");
$bid8=Treat_DB_ProxyOld::mysql_result($result,0,"busid8");
$bid9=Treat_DB_ProxyOld::mysql_result($result,0,"busid9");
$bid10=Treat_DB_ProxyOld::mysql_result($result,0,"busid10");
$cid=Treat_DB_ProxyOld::mysql_result($result,0,"companyid");
$loginid=Treat_DB_ProxyOld::mysql_result($result,0,"loginid");

if ($num != 1)
{
    echo "<center><h3>Failed</h3></center>";
}

else
{

?>

<html>
<head>

<script type="text/javascript" src="javascripts/prototype.js"></script>
<script type="text/javascript" src="javascripts/scriptaculous.js"></script>

<SCRIPT LANGUAGE="JavaScript" SRC="CalendarPopup.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">document.write(getCalendarStyles());</SCRIPT>

<STYLE>
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation
			{
			background-color:#6677DD;
			text-align:center;
			vertical-align:center;
			text-decoration:none;
			color:#FFFFFF;
			font-weight:bold;
			}
	.TESTcpDayColumnHeader,
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation,
	.TESTcpCurrentMonthDate,
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDate,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDate,
	.TESTcpCurrentDateDisabled,
	.TESTcpTodayText,
	.TESTcpTodayTextDisabled,
	.TESTcpText
			{
			font-family:arial;
			font-size:8pt;
			}
	TD.TESTcpDayColumnHeader
			{
			text-align:right;
			border:solid thin #6677DD;
			border-width:0 0 1 0;
			}
	.TESTcpCurrentMonthDate,
	.TESTcpOtherMonthDate,
	.TESTcpCurrentDate
			{
			text-align:right;
			text-decoration:none;
			}
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDateDisabled
			{
			color:#D0D0D0;
			text-align:right;
			text-decoration:line-through;
			}
	.TESTcpCurrentMonthDate
			{
			color:#6677DD;
			font-weight:bold;
			}
	.TESTcpCurrentDate
			{
			color: #FFFFFF;
			font-weight:bold;
			}
	.TESTcpOtherMonthDate
			{
			color:#808080;
			}
	TD.TESTcpCurrentDate
			{
			color:#FFFFFF;
			background-color: #6677DD;
			border-width:1;
			border:solid thin #000000;
			}
	TD.TESTcpCurrentDateDisabled
			{
			border-width:1;
			border:solid thin #FFAAAA;
			}
	TD.TESTcpTodayText,
	TD.TESTcpTodayTextDisabled
			{
			border:solid thin #6677DD;
			border-width:1 0 0 0;
			}
	A.TESTcpTodayText,
	SPAN.TESTcpTodayTextDisabled
			{
			height:20px;
			}
	A.TESTcpTodayText
			{
			color:#6677DD;
			font-weight:bold;
			}
	SPAN.TESTcpTodayTextDisabled
			{
			color:#D0D0D0;
			}
	.TESTcpBorder
			{
			border:solid thin #6677DD;
			}
</STYLE>

<STYLE>
#loading {
 	width: 170px;
 	height: 48px;
 	background-color: ;
 	position: absolute;
 	left: 50%;
 	top: 50%;
 	margin-top: -50px;
 	margin-left: -100px;
 	text-align: center;
}
</STYLE>

<script type="text/javascript" src="preLoadingMessage.js"></script>

<SCRIPT LANGUAGE=javascript><!--
function contconfirm(){return confirm('Are you sure you want to save?');}
// --></SCRIPT>

</head>

<?
    if($newbudget!=0){
       $myFile2 = DIR_EXPORTS."/report$loginid.csv";
       $fh = fopen($myFile2, 'w'); 
    }   

    echo "<body>";
    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%><tr><td colspan=2>";
    echo "<a href=businesstrack.php><img src=logo.jpg border=0 height=43 width=205></a><p></td></tr>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM company WHERE companyid = '$companyid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $companyname=Treat_DB_ProxyOld::mysql_result($result,0,"companyname");

    //echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";
    echo "<tr bgcolor=#CCCCFF><td colspan=1><font size=4><b>Budget Calculator - $companyname</b></font></td><td align=right></td><td width=0%></td></tr>";
    echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";
	echo "<tr><td colspan=3><a href=budget_calc.php style=$style><font color=blue>Date Range Average</font></a> :: Yearly Actual</td></tr>";
    echo "</table></center><p>";

    /////////////////////
    //////FILTER/////////
    /////////////////////
    echo "<center><table width=90%><tr><td width=75%><form action=budget_calc2.php method=post><select name=businessid[] size=8>";

    $query = "SELECT * FROM business WHERE companyid != '2' ORDER BY companyid DESC, districtid DESC, businessname DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);

    $lastcompany=-1;
    $lastdistrict=-1;
    $num--;
    while($num>=0){
       $businessname=Treat_DB_ProxyOld::mysql_result($result,$num,"businessname");
       $busid=Treat_DB_ProxyOld::mysql_result($result,$num,"businessid");
       $comid=Treat_DB_ProxyOld::mysql_result($result,$num,"companyid");
       $distid=Treat_DB_ProxyOld::mysql_result($result,$num,"districtid");
       $unit=Treat_DB_ProxyOld::mysql_result($result,$num,"unit");

       if(in_array($busid,$businessid)){$showsel="SELECTED";}
       else{$showsel="";}

       if($lastcompany!=$comid){
          if($lastcompany!=-1){echo "</optgroup>";}

          $query2 = "SELECT companyname FROM company WHERE companyid = '$comid'";
          $result2 = Treat_DB_ProxyOld::query($query2);

          $comname=Treat_DB_ProxyOld::mysql_result($result2,0,"companyname");

          echo "<optgroup label='$comname'>";
       }
       if($lastdistrict!=$distid){
          if($lastdistrict!=-1){echo "</optgroup>";}

          $query2 = "SELECT districtname FROM district WHERE districtid = '$distid'";
          $result2 = Treat_DB_ProxyOld::query($query2);

          $distname=Treat_DB_ProxyOld::mysql_result($result2,0,"districtname");

          echo "<optgroup label='   $distname'>";
       }

       echo "<option value=$busid $showsel>$businessname #$unit</option>";

       $lastcompany=$comid;
       $lastdistrict=$distid;
       $num--;
    }
    echo "</optgroup></optgroup></select> ";

    if($service==1){$showserv="CHECKED";}
	echo "<select name=startyear size=8><optgroup label='Sales from...'>";
	$startyear=date("Y");
	for($counter=1;$counter<=5;$counter++){
		if($startyear==$year){$sel="SELECTED";}
		else{$sel="";}
		echo "<option value=$startyear $sel>$startyear</option>";
		$startyear--;
	}
	
    echo "</optgroup></select>";

	echo "<select name=budgetyear size=8> <optgroup label='Budget for... '>";
	$startyear=date("Y");
	$startyear++;
	for($counter=1;$counter<=5;$counter++){
		if($startyear==$budgetyear){$sel="SELECTED";}
		else{$sel="";}
		echo "<option value=$startyear $sel>$startyear</option>";
		$startyear--;
	}
	
    echo "</optgroup></select> ";
	
	echo "<input type=checkbox name=service value=1 $showserv>Include Service Total <input type=submit value='GO'></form></td><td style=\"border:2px solid #E8E7E7;\" valign=top><font size=2><ul><li>Days in yellow are marked as closed.<li>Click on the amount in the Sales or Fig. Bgt columns to open or close a day.<li>Monthly totals are entered at the bottom of the page.<li>Click on the month to graph sales vs budget.</ul></font></td></tr></table></center>";

    //////////////////////
    ///////REPORT/////////
    //////////////////////
    if($businessid!=""){

		$startdate="$year-01-01";
		$enddate="$year-12-31";

		if($service==1){$myquery="credittype = '1' OR credittype = '5'";}
		else{$myquery="credittype = '1'";}

		foreach($businessid AS $key => $value){
          if($myquery2==""){$myquery2="businessid = '$value'";}
          else{$myquery2.=" OR businessid = '$value'";}

          $query4 = "SELECT businessname FROM business WHERE businessid = '$value'";
          $result4 = Treat_DB_ProxyOld::query($query4);

          $busname2=Treat_DB_ProxyOld::mysql_result($result4,0,"businessname");

          if($showclose==""){$showclose="<a style=$style href='calender.php?caterbus=$value&cid=$companyid' target='_blank'><font color=blue size=2>$busname2</font></a>";}
          else{$showclose.=", <a style=$style href='calender.php?caterbus=$value&cid=$companyid' target='_blank'><font color=blue size=2>$busname2</font></a>";}
		}

		$query = "SELECT * FROM credits WHERE ($myquery) AND ($myquery2) AND ((is_deleted = '0') OR (is_deleted = '1' AND del_date >= '$startdate')) ORDER BY credittype DESC, creditname DESC";
		$result = Treat_DB_ProxyOld::query($query);
		$num=Treat_DB_ProxyOld::mysql_numrows($result);

		$numcredits=$num;

		$num--;
		while ($num>=0){

			$day=$startdate;

			$creditname=Treat_DB_ProxyOld::mysql_result($result,$num,"creditname");
			$creditid=Treat_DB_ProxyOld::mysql_result($result,$num,"creditid");
			$account=Treat_DB_ProxyOld::mysql_result($result,$num,"account");
			$invoicesales=Treat_DB_ProxyOld::mysql_result($result,$num,"invoicesales");
			$invoicesales_notax=Treat_DB_ProxyOld::mysql_result($result,$num,"invoicesales_notax");
			$invoicetax=Treat_DB_ProxyOld::mysql_result($result,$num,"invoicetax");
			$servchrg=Treat_DB_ProxyOld::mysql_result($result,$num,"servchrg");
			$salescode=Treat_DB_ProxyOld::mysql_result($result,$num,"salescode");
			$prepayment=Treat_DB_ProxyOld::mysql_result($result,$num,"heldcheck");
			$showaccount=substr($account,0,3);

			$prevamount=0;
			$credittotal=0;
			$monthtotal=0;
			while ($day<=$enddate){

			$prevamount=0;

			///MANUAL
			if ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0&&$prepayment==0&&($salescode==0||$salescode=="")){
                  $query5 = "SELECT amount FROM creditdetail WHERE creditid = '$creditid' AND date = '$day'";
                  $result5 = Treat_DB_ProxyOld::query($query5);
                  $num5=Treat_DB_ProxyOld::mysql_numrows($result5);

                  if ($num5!=0){$prevamount=Treat_DB_ProxyOld::mysql_result($result5,0,"amount");}
                  $prevamount = money($prevamount);

            }
			///INVOICE
            elseif ($invoicesales==1&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0&&$prepayment==0&&($salescode=="0" || $salescode=="")){
                  $query5 = "SELECT SUM(total-taxtotal-servamt) AS totalamt FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND taxable = 'on' AND salescode = '0' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = Treat_DB_ProxyOld::query($query5);
                  $num6=Treat_DB_ProxyOld::mysql_numrows($result5);

                  $prevamount=0;

                  $totalamt=Treat_DB_ProxyOld::mysql_result($result5,0,"totalamt");
                  $prevamount+=$totalamt;

                  $prevamount=money($prevamount);
            }
			///INVOICE TAX EXEMPT
            elseif ($invoicesales==0&&$invoicesales_notax==1&&$invoicetax==0&&$servchrg==0&&$prepayment==0&&($salescode=="0" || $salescode=="")){
                  $query5 = "SELECT SUM(total-servamt) AS totalamt FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND taxable = 'off' AND salescode = '0' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = Treat_DB_ProxyOld::query($query5);

                  $prevamount=0;
                  $totalamt=Treat_DB_ProxyOld::mysql_result($result5,0,"totalamt");
                  $prevamount+=$totalamt;

                  $prevamount=money($prevamount);
            }
			///INVOICE TAX
            elseif ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==1&&$servchrg==0&&$prepayment==0&&($salescode=="0" || $salescode=="")){
                  $query5 = "SELECT SUM(taxtotal) AS totalamt FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND taxable = 'on' AND salescode = '0' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = Treat_DB_ProxyOld::query($query5);

                  $prevamount=0;
                  $totalamt=Treat_DB_ProxyOld::mysql_result($result5,0,"totalamt");
                  $prevamount+=$totalamt;
                  $prevamount=money($prevamount);
            }
			///INVOICE SERVICE CHARGE
            elseif ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==1&&$prepayment==0&&($salescode=="0" || $salescode=="")){
                  $query5 = "SELECT SUM(servamt) AS totalamt FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND salescode = '0' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = Treat_DB_ProxyOld::query($query5);
                  
                  $prevamount=0;
                  $totalamt=Treat_DB_ProxyOld::mysql_result($result5,0,"totalamt");
                  $prevamount+=$totalamt;
                  $prevamount=money($prevamount);
            }
			 
            if(dayofweek($day)=="Sunday"){$sales[Sunday]+=$prevamount;}
            elseif(dayofweek($day)=="Monday"){$sales[Monday]+=$prevamount;}
            elseif(dayofweek($day)=="Tuesday"){$sales[Tuesday]+=$prevamount;}
            elseif(dayofweek($day)=="Wednesday"){$sales[Wednesday]+=$prevamount;}
            elseif(dayofweek($day)=="Thursday"){$sales[Thursday]+=$prevamount;}
            elseif(dayofweek($day)=="Friday"){$sales[Friday]+=$prevamount;}
            elseif(dayofweek($day)=="Saturday"){$sales[Saturday]+=$prevamount;}

            $daysales[$day]+=$prevamount;
			$day=nextday($day);
			}
		$day=$startdate;

		$num--;
		}
		
		///////business details
		$query = "SELECT companyid,unit,operate FROM business WHERE businessid = '$businessid[0]'";
        $result = Treat_DB_ProxyOld::query($query);
		
		$operate=Treat_DB_ProxyOld::mysql_result($result,0,"operate");
		$companyid=Treat_DB_ProxyOld::mysql_result($result,0,"companyid");
		$unit=Treat_DB_ProxyOld::mysql_result($result,0,"unit");
		
		///////get monthly budget
		$query55 = "SELECT * FROM budget WHERE type = '10' AND year = '$budgetyear' AND businessid = '$unit' AND companyid = '$companyid'";
        $result55 = Treat_DB_ProxyOld::query($query55);
	
		//////////////////////
		/////DISPLAY//////////
		//////////////////////
		$canupdate=0;
		
		echo "<center><a name=tab><form action=budget_calc_update2.php method=post style=\"margin:0;padding:0;display:inline;\"><input type=hidden name=budgetyear value='$budgetyear'><input type=hidden name=businessid value='$businessid[0]'><table width=100% cellspacing=0 cellpadding=0 style=\"border:1px solid #CCCCCC;font-size:9px;\">";
		echo "<tr><th style=\"border:1px solid #CCCCCC;\" bgcolor=#CCCCCC width=4%>&nbsp;</th>";
		for($counter=1;$counter<=12;$counter++){
			if($counter==1){$monthname="January";}
			elseif($counter==2){$monthname="February";}
			elseif($counter==3){$monthname="March";}
			elseif($counter==4){$monthname="April";}
			elseif($counter==5){$monthname="May";}
			elseif($counter==6){$monthname="June";}
			elseif($counter==7){$monthname="July";}
			elseif($counter==8){$monthname="August";}
			elseif($counter==9){$monthname="September";}
			elseif($counter==10){$monthname="October";}
			elseif($counter==11){$monthname="November";}
			elseif($counter==12){$monthname="December";}
			echo "<th style=\"border:1px solid #CCCCCC; border-right-width: 4px;\" title='Click to view Graph' onMouseOver=\"this.style.backgroundColor='#FFFF99';\" onMouseOut=\"this.style.backgroundColor='#E8E7E7';\" colspan=4 width=8% bgcolor=#E8E7E7 id='month$counter' onclick=\"$('graph$counter').show();\"><center>$monthname</center></th>";
		}
		echo "</tr>";
		
		////LIST
		echo "<tr><td style=\"border:1px solid #CCCCCC;\">";
		$dayletter="Mo";
		echo "<div width=100% style=\"border:1px solid #CCCCCC;background-color:#E8E7E7; height:20px;\" id='dayletter$counter'><center>Days</center></div>";
		for($counter=1;$counter<=42;$counter++){
			echo "<div width=100% style=\"border:1px solid #CCCCCC;background-color:#E8E7E7; height:20px;\" id='dayletter$counter'><center>$dayletter</center></div>";
			if($dayletter=="Mo"){$dayletter="Tu";}
			elseif($dayletter=="Tu"){$dayletter="We";}
			elseif($dayletter=="We"){$dayletter="Th";}
			elseif($dayletter=="Th"){$dayletter="Fr";}
			elseif($dayletter=="Fr" && ($operate== 6 || $operate == 7)){$dayletter="Sa";}
			elseif($dayletter=="Fr"){$dayletter="Mo";}
			elseif($dayletter=="Sa" && $operate== 7){$dayletter="Su";}
			elseif($dayletter=="Sa"){$dayletter="Mo";}
			elseif($dayletter=="Su"){$dayletter="Mo";}
		}
		
		////show totals for month
		echo "<div width=100% style=\"border:1px solid #CCCCCC; background-color: #E8E7E7;\" align=right>Total</div>";
		
		/////show #days
			for($counter=1;$counter<=7;$counter++){
				if($counter==1){$day="Mondays";}
				elseif($counter==2){$day="Tuesdays";}
				elseif($counter==3){$day="Wednesdays";}
				elseif($counter==4){$day="Thursdays";}
				elseif($counter==5){$day="Fridays";}
				elseif($counter==6){$day="Saturdays";}
				elseif($counter==7){$day="Sundays";}
				if($operate>=$counter){echo "<div width=100% style=\"border:1px solid orange;background-color:#E8E7E7;\" align=right>$day</div>";}
			}
			
		echo "<div width=100% style=\"border:1px solid orange;background-color:#E8E7E7;\" align=right>Op Days</div>";
		
		echo "</td>";
		
		for($colcounter=1;$colcounter<=12;$colcounter++){
		
			//////////////////////
			//////SALES STATS/////
			//////////////////////
			$salesdaynum = array();
			$salesdaynumtotal=0;
			$salesdaynumavg = array();
			
			$month=$colcounter;
			if(strlen($month)<2){$month="0$month";}
			$monthstart="$year-$month-01";
			
			$thismonth=substr($monthstart,5,2);
			while($thismonth==substr($monthstart,5,2)){
			
				$query = "SELECT catercloseid FROM caterclose WHERE date = '$monthstart' AND businessid = '$businessid[0]'";
                $result = Treat_DB_ProxyOld::query($query);
				$num = Treat_DB_ProxyOld::mysql_numrows($result);
			
				if($num==0 && $operate > 4 && dayofweek($monthstart) != "Saturday" && dayofweek($monthstart) != "Sunday"){
					$salesdaynum[dayofweek($monthstart)]++;
					$salesdaynumtotal++;
					$salesdaynumavg[dayofweek($monthstart)]+=$daysales[$monthstart];
				}
				elseif($num==0 && $operate > 5 && dayofweek($monthstart) != "Sunday"){
					$salesdaynum[dayofweek($monthstart)]++;
					$salesdaynumtotal++;
					$salesdaynumavg[dayofweek($monthstart)]+=$daysales[$monthstart];
				}
				elseif($num==0 && $operate > 6 ){
					$salesdaynum[dayofweek($monthstart)]++;
					$salesdaynumtotal++;
					$salesdaynumavg[dayofweek($monthstart)]+=$daysales[$monthstart];
				}
				$monthstart=nextday($monthstart);
			}
			
			foreach($salesdaynumavg AS $key => $value){
				$salesdaynumavg[$key]=round($value/$salesdaynum[$key],2);
			}
			
			//////////////////////
			//////Budget STATS////
			//////////////////////
			$budgetdaynum = array();
			$budgetdaynumtotal = 0;
			
			$month=$colcounter;
			if(strlen($month)<2){$month="0$month";}
			$monthstart="$budgetyear-$month-01";
			
			$thismonth=substr($monthstart,5,2);
			while($thismonth==substr($monthstart,5,2)){
			
				$query = "SELECT catercloseid FROM caterclose WHERE date = '$monthstart' AND businessid = '$businessid[0]'";
                $result = Treat_DB_ProxyOld::query($query);
				$num = Treat_DB_ProxyOld::mysql_numrows($result);
			
				if($num==0 && $operate > 4 && dayofweek($monthstart) != "Saturday" && dayofweek($monthstart) != "Sunday"){
					$budgetdaynum[dayofweek($monthstart)]++;
					$budgetdaynumtotal++;
				}
				elseif($num==0 && $operate > 5 && dayofweek($monthstart) != "Sunday"){
					$budgetdaynum[dayofweek($monthstart)]++;
					$budgetdaynumtotal++;
				}
				elseif($num==0 && $operate > 6 ){
					$budgetdaynum[dayofweek($monthstart)]++;
					$budgetdaynumtotal++;
				}
				$monthstart=nextday($monthstart);
			}
		
			//////////////////////
			//////SALES///////////
			//////////////////////
			$salestotal=0;
			$salesequiv = array();
			echo "<td colspan=1 style=\"border:1px solid #CCCCCC;\" width=4%>";
			
			$month=$colcounter;
			if(strlen($month)<2){$month="0$month";}
			$monthstart="$year-$month-01";
			$dayname=dayofweek($monthstart);
			if($dayname=="Monday"){$skipdays=0;}
			elseif($dayname=="Tuesday"){$skipdays=1;}
			elseif($dayname=="Wednesday"){$skipdays=2;}
			elseif($dayname=="Thursday"){$skipdays=3;}
			elseif($dayname=="Friday"){$skipdays=4;}
			elseif($dayname=="Saturday" && ($operate== 6 || $operate == 7)){$skipdays=5;}
			elseif($dayname=="Sunday" && $operate == 7){$skipdays=6;}
			else{$skipdays=0;}
			
			$salesskipdays=$skipdays;
			
			echo "<div width=100% style=\"border:1px solid #FFFF99; height:20px;\"><center>Sales</center></div>";
			
			for($counter=1;$counter<=$skipdays;$counter++){echo "<div width=100% style=\"border:1px solid #FFFF99; height:20px;\">&nbsp;</div>";}
			
			$thismonth=substr($monthstart,5,2);
			while($thismonth==substr($monthstart,5,2)){
			
				$query = "SELECT catercloseid FROM caterclose WHERE date = '$monthstart' AND businessid = '$businessid[0]'";
				$result = Treat_DB_ProxyOld::query($query);
				$num = Treat_DB_ProxyOld::mysql_numrows($result);
			
				if(($operate > 4 && dayofweek($monthstart) != "Saturday" && dayofweek($monthstart) != "Sunday") || ($operate > 5 && dayofweek($monthstart) != "Sunday") || ($operate > 6 )){
				
					if($num>0){$showcolor="#FFFF99";}
					else{$showcolor="white";}
				
					$showday=substr($monthstart,8,2);
					$showmonth=substr($monthstart,5,2);
					$showyear=substr($monthstart,2,2);
					$weekday=dayofweek($monthstart);
					$showdate="$weekday $showmonth/$showday/$showyear";
			
					$skipdays++;
			
					$salesequiv[$skipdays]=$daysales[$monthstart];
					$salestotal+=$daysales[$monthstart];
					$showsales=number_format($daysales[$monthstart],2);
					echo "<div width=100% style=\"border:1px solid #FFFF99;background-color:$showcolor; height:20px;\" align=right title='$showdate' onMouseOver=\"this.style.backgroundColor='#90EE90'; dayletter$skipdays.style.backgroundColor='#90EE90'; month$colcounter.style.backgroundColor='#90EE90';\" onMouseOut=\"this.style.backgroundColor='$showcolor'; dayletter$skipdays.style.backgroundColor='#E8E7E7'; month$colcounter.style.backgroundColor='#E8E7E7';\"><a href=caterclose2.php?bid=$businessid[0]&date=$monthstart style=$style onclick=\"return confirm('Are you sure you want to change?');\"><font color=black>$showsales</font></a></div>";
				}
				$monthstart=nextday($monthstart);
			}
			
			while($skipdays<42){
				echo "<div width=100% style=\"border:1px solid #FFFF99; height:20px;\">&nbsp;</div>";
				$skipdays++;
			}
			
			////show sales total for month
			$showsalestotal=number_format($salestotal,2);
			echo "<div width=100% style=\"border:1px solid #FFFF99; background-color: #E8E7E7;\" align=right>$showsalestotal</div>";
			
			/////show sales #days
			for($counter=1;$counter<=7;$counter++){
				if($counter==1){$day="Monday";}
				elseif($counter==2){$day="Tuesday";}
				elseif($counter==3){$day="Wednesday";}
				elseif($counter==4){$day="Thursday";}
				elseif($counter==5){$day="Friday";}
				elseif($counter==6){$day="Saturday";}
				elseif($counter==7){$day="Sunday";}
				if($operate>=$counter){echo "<div width=100% style=\"border:1px solid orange;\" align=right>$salesdaynum[$day]</div>";}
			}
			
			////total operating days
			echo "<div width=100% style=\"border:1px solid orange;\" align=right>$salesdaynumtotal</div>";
			
			echo "</td>";
			
			//////////////////////
			//////figure diff/////
			//////////////////////
			$diff=0;
			$budgettotal=0;
			
			$month=$colcounter;
			if(strlen($month)<2){$month="0$month";}
			$monthstart="$budgetyear-$month-01";
			$dayname=dayofweek($monthstart);
			
			if($dayname=="Monday"){$skipdays=0;}
			elseif($dayname=="Tuesday"){$skipdays=1;}
			elseif($dayname=="Wednesday"){$skipdays=2;}
			elseif($dayname=="Thursday"){$skipdays=3;}
			elseif($dayname=="Friday"){$skipdays=4;}
			elseif($dayname=="Saturday" && ($operate== 6 || $operate == 7)){$skipdays=5;}
			elseif($dayname=="Sunday" && $operate == 7){$skipdays=6;}
			else{$skipdays=0;}
			
			if($skipdays<$salesskipdays){$skipdays+=$salesskipdays+1;}
			
			$thismonth=substr($monthstart,5,2);
			while($thismonth==substr($monthstart,5,2)){
				
				$query = "SELECT catercloseid FROM caterclose WHERE date = '$monthstart' AND businessid = '$businessid[0]'";
				$result = Treat_DB_ProxyOld::query($query);
				$num = Treat_DB_ProxyOld::mysql_numrows($result);
				
				if(($operate > 4 && dayofweek($monthstart) != "Saturday" && dayofweek($monthstart) != "Sunday") || ($operate > 5 && dayofweek($monthstart) != "Sunday") || ($operate > 6 )){
				
					$showday=substr($monthstart,8,2);
					$showmonth=substr($monthstart,5,2);
					$showyear=substr($monthstart,2,2);
					$showdate="$showmonth/$showday/$showyear";
			
					$skipdays++;
					
					//////figure new budget!!!!!!!!!
					if($num==0){
						if($salesequiv[$skipdays]>0){
							$getmonth=$colcounter;
							if(strlen($getmonth)<2){$getmonth="0$getmonth";}
							$monthbgt=Treat_DB_ProxyOld::mysql_result($result55,0,"$getmonth");
							$showbudget=$monthbgt*($salesequiv[$skipdays]/$salestotal);
							$budgettotal+=$showbudget;
						}
						else{
							$showbudget=$monthbgt*($salesdaynumavg[dayofweek($monthstart)]/$salestotal);
							$budgettotal+=$showbudget;
						}
					}
					else{
						$showbudget=0;
					}
	
					$showbudget=number_format($showbudget,2);
				}
				$monthstart=nextday($monthstart);
			}
			$diff=($monthbgt-$budgettotal)/$budgetdaynumtotal;
			
			//////////////////////
			//////BUDGET//////////
			//////////////////////
			$budgettotal=0;
			echo "<td colspan=1 style=\"border:1px solid #CCCCCC;\" width=4%>";
			
			$month=$colcounter;
			if(strlen($month)<2){$month="0$month";}
			$monthstart="$budgetyear-$month-01";
			$dayname=dayofweek($monthstart);
			
			if($dayname=="Monday"){$skipdays=0;}
			elseif($dayname=="Tuesday"){$skipdays=1;}
			elseif($dayname=="Wednesday"){$skipdays=2;}
			elseif($dayname=="Thursday"){$skipdays=3;}
			elseif($dayname=="Friday"){$skipdays=4;}
			elseif($dayname=="Saturday" && ($operate== 6 || $operate == 7)){$skipdays=5;}
			elseif($dayname=="Sunday" && $operate == 7){$skipdays=6;}
			else{$skipdays=0;}
			
			if($skipdays<$salesskipdays){$skipdays+=$salesskipdays+1;}
			
			echo "<div width=100% style=\"border:1px solid #FFFF99; background-color: #FFFFE0; height:20px;\"><center>Fig. Bgt</center></div>";
			
			for($counter=1;$counter<=$skipdays;$counter++){echo "<div width=100% style=\"border:1px solid #FFFF99; background-color: #FFFFE0; height:20px;\">&nbsp;</div>";}
			
			$thismonth=substr($monthstart,5,2);
			while($thismonth==substr($monthstart,5,2)){
				
				$query = "SELECT catercloseid FROM caterclose WHERE date = '$monthstart' AND businessid = '$businessid[0]'";
				$result = Treat_DB_ProxyOld::query($query);
				$num = Treat_DB_ProxyOld::mysql_numrows($result);
				
				if(($operate > 4 && dayofweek($monthstart) != "Saturday" && dayofweek($monthstart) != "Sunday") || ($operate > 5 && dayofweek($monthstart) != "Sunday") || ($operate > 6 )){
				
					if($num>0){$showcolor="#FFFF99";}
					else{$showcolor="#FFFFE0";}
				
					$showday=substr($monthstart,8,2);
					$showmonth=substr($monthstart,5,2);
					$showyear=substr($monthstart,2,2);
					$weekday=dayofweek($monthstart);
					$showdate="$weekday $showmonth/$showday/$showyear";
			
					$skipdays++;
					
					//////figure new budget!!!!!!!!!
					if($num==0){
						if($salesequiv[$skipdays]>0){
							$getmonth=$colcounter;
							if(strlen($getmonth)<2){$getmonth="0$getmonth";}
							$monthbgt=Treat_DB_ProxyOld::mysql_result($result55,0,"$getmonth");
							$showbudget=$monthbgt*($salesequiv[$skipdays]/$salestotal)+$diff;
							$figurebudget[$monthstart]=round($showbudget,2);
							$budgettotal+=$showbudget;
						}
						else{
							$showbudget=$monthbgt*($salesdaynumavg[dayofweek($monthstart)]/$salestotal)+$diff;
							$figurebudget[$monthstart]=round($showbudget,2);
							$budgettotal+=$showbudget;
						}
					}
					else{
						$showbudget=0;
					}
	
					$showbudget=number_format($showbudget,2);
					echo "<div width=100% style=\"border:1px solid #FFFF99;background-color:$showcolor; height:20px;\" align=right title='$showdate' onMouseOver=\"this.style.backgroundColor='#90EE90'; dayletter$skipdays.style.backgroundColor='#90EE90'; month$colcounter.style.backgroundColor='#90EE90';\" onMouseOut=\"this.style.backgroundColor='$showcolor'; dayletter$skipdays.style.backgroundColor='#E8E7E7'; month$colcounter.style.backgroundColor='#E8E7E7';\"><a href=caterclose2.php?bid=$businessid[0]&date=$monthstart style=$style  onclick=\"return confirm('Are you sure you want to change?');\"><font color=#555555>$showbudget</font></a></div>";
				}
				$monthstart=nextday($monthstart);
			}
			
			while($skipdays<42){
				echo "<div width=100% style=\"border:1px solid #FFFF99; background-color: #FFFFE0; height:20px;\">&nbsp;</div>";
				$skipdays++;
			}
			
			////show budget total for month
			$finalbudgettotal=$budgettotal;
			$budgettotal=number_format($budgettotal,2);
			echo "<div width=100% style=\"border:1px solid #FFFF99; background-color: #E8E7E7\" align=right>$budgettotal</div>";
			
			/////show budget #days
			for($counter=1;$counter<=7;$counter++){
				if($counter==1){$day="Monday";}
				elseif($counter==2){$day="Tuesday";}
				elseif($counter==3){$day="Wednesday";}
				elseif($counter==4){$day="Thursday";}
				elseif($counter==5){$day="Friday";}
				elseif($counter==6){$day="Saturday";}
				elseif($counter==7){$day="Sunday";}
				if($operate>=$counter){echo "<div width=100% style=\"border:1px solid orange; background-color: #FAEBD7;\" align=right>$budgetdaynum[$day]</div>";}
			}
			
			////total operating days
			echo "<div width=100% style=\"border:1px solid orange; background-color: #FAEBD7;\" align=right>$budgetdaynumtotal</div>";
			
			echo "</td>";
			
			//////////////////////
			////ADJUSTMENT////////
			//////////////////////
			$totaladjustamt=0;
			$budgettotal=0;
			echo "<td colspan=1 style=\"border:1px solid #CCCCCC;\" width=4%>";
			
			$month=$colcounter;
			if(strlen($month)<2){$month="0$month";}
			$monthstart="$budgetyear-$month-01";
			$dayname=dayofweek($monthstart);
			
			if($dayname=="Monday"){$skipdays=0;}
			elseif($dayname=="Tuesday"){$skipdays=1;}
			elseif($dayname=="Wednesday"){$skipdays=2;}
			elseif($dayname=="Thursday"){$skipdays=3;}
			elseif($dayname=="Friday"){$skipdays=4;}
			elseif($dayname=="Saturday" && ($operate== 6 || $operate == 7)){$skipdays=5;}
			elseif($dayname=="Sunday" && $operate == 7){$skipdays=6;}
			else{$skipdays=0;}
			
			if($skipdays<$salesskipdays){$skipdays+=$salesskipdays+1;}
			
			echo "<div width=100% style=\"border:1px solid #FFFF99; background-color: #FFFFE0; height:20px;\"><center>Adjust</center></div>";
			
			for($counter=1;$counter<=$skipdays;$counter++){echo "<div width=100% style=\"border:1px solid #FFFF99; background-color: #FFFFE0; height:20px;\">&nbsp;</div>";}
			
			$thismonth=substr($monthstart,5,2);
			while($thismonth==substr($monthstart,5,2)){
				
				$query = "SELECT catercloseid FROM caterclose WHERE date = '$monthstart' AND businessid = '$businessid[0]'";
				$result = Treat_DB_ProxyOld::query($query);
				$num = Treat_DB_ProxyOld::mysql_numrows($result);
				
				if(($operate > 4 && dayofweek($monthstart) != "Saturday" && dayofweek($monthstart) != "Sunday") || ($operate > 5 && dayofweek($monthstart) != "Sunday") || ($operate > 6 )){
				
					$adjustamt = "";
					if($num>0){$showcolor="#FFFF99";$is_disable="DISABLED";}
					else{$showcolor="#FFFFE0";$is_disable="";}
				
					$showday=substr($monthstart,8,2);
					$showmonth=substr($monthstart,5,2);
					$showyear=substr($monthstart,2,2);
					$weekday=dayofweek($monthstart);
					$showdate="$weekday $showmonth/$showday/$showyear";
			
					$skipdays++;
					
					$adjustamt=0;
					
					$query = "SELECT amount FROM budget_adjust WHERE businessid = '$businessid[0]' AND date = '$monthstart'";
					$result = Treat_DB_ProxyOld::query($query);
					$num = Treat_DB_ProxyOld::mysql_numrows($result);
					
					if($num>0){$adjustamt = Treat_DB_ProxyOld::mysql_result($result,0,"amount");}
	
					$adjustbudget[$monthstart]=$adjustamt;
					$totaladjustamt+=$adjustamt;
					
					if($adjustamt==0){$adjustamt="";}
					
					echo "<div width=100% style=\"border:1px solid #FFFF99;background-color:$showcolor; height:20px;\" align=right title='$showdate' onMouseOver=\"this.style.backgroundColor='#90EE90'; dayletter$skipdays.style.backgroundColor='#90EE90'; month$colcounter.style.backgroundColor='#90EE90';\" onMouseOut=\"this.style.backgroundColor='$showcolor'; dayletter$skipdays.style.backgroundColor='#E8E7E7'; month$colcounter.style.backgroundColor='#E8E7E7';\"><input type=text size=6 name='adj$monthstart' value='$adjustamt' style=\"font-size:8px;\" $is_disable></div>";
				}
				$monthstart=nextday($monthstart);
			}
			
			while($skipdays<42){
				echo "<div width=100% style=\"border:1px solid #FFFF99; background-color: #FFFFE0; height:20px;\">&nbsp;</div>";
				$skipdays++;
			}

                        $totaladjustamt=round($totaladjustamt, 2);
			
			////show budget total for month
			if($totaladjustamt!=0){$mycolor="red";}
			else{$mycolor="#00FF00";}

			$canupdate+=$totaladjustamt;
			
			echo "<div width=100% style=\"border:1px solid #FFFF99; background-color: $mycolor\" align=right>$totaladjustamt</div>";
			
			/////show budget #days
			for($counter=1;$counter<=7;$counter++){
				if($counter==1){$day="Monday";}
				elseif($counter==2){$day="Tuesday";}
				elseif($counter==3){$day="Wednesday";}
				elseif($counter==4){$day="Thursday";}
				elseif($counter==5){$day="Friday";}
				elseif($counter==6){$day="Saturday";}
				elseif($counter==7){$day="Sunday";}
				if($operate>=$counter){echo "<div width=100% style=\"border:1px solid #FFFF99; background-color: #FAEBD7;\" align=right>&nbsp;</div>";}
			}
			
			////total operating days
			echo "<div width=100% style=\"border:1px solid #FFFF99; background-color: #FAEBD7;\" align=right>&nbsp;</div>";
			
			echo "</td>";
			
			//////////////////////
			////FINAL BUDGET///////
			//////////////////////
			$budgettotal=0;
			echo "<td colspan=1 style=\"border:1px solid #CCCCCC; border-right-width: 4px;\" width=4%>";
			
			$month=$colcounter;
			if(strlen($month)<2){$month="0$month";}
			$monthstart="$budgetyear-$month-01";
			$dayname=dayofweek($monthstart);
			
			if($dayname=="Monday"){$skipdays=0;}
			elseif($dayname=="Tuesday"){$skipdays=1;}
			elseif($dayname=="Wednesday"){$skipdays=2;}
			elseif($dayname=="Thursday"){$skipdays=3;}
			elseif($dayname=="Friday"){$skipdays=4;}
			elseif($dayname=="Saturday" && ($operate== 6 || $operate == 7)){$skipdays=5;}
			elseif($dayname=="Sunday" && $operate == 7){$skipdays=6;}
			else{$skipdays=0;}
			
			if($skipdays<$salesskipdays){$skipdays+=$salesskipdays+1;}
			
			echo "<div width=100% style=\"border:1px solid #FFFF99; background-color: #FFFFE0; height:20px;\"><center>Final</center></div>";
			
			for($counter=1;$counter<=$skipdays;$counter++){echo "<div width=100% style=\"border:1px solid #FFFF99; background-color: #FFFFE0; height:20px;\">&nbsp;</div>";}
			
			$thismonth=substr($monthstart,5,2);
			while($thismonth==substr($monthstart,5,2)){
				
				$query = "SELECT catercloseid FROM caterclose WHERE date = '$monthstart' AND businessid = '$businessid[0]'";
				$result = Treat_DB_ProxyOld::query($query);
				$num = Treat_DB_ProxyOld::mysql_numrows($result);
				
				if(($operate > 4 && dayofweek($monthstart) != "Saturday" && dayofweek($monthstart) != "Sunday") || ($operate > 5 && dayofweek($monthstart) != "Sunday") || ($operate > 6 )){
				
					if($num>0){$showcolor="#FFFF99";}
					else{$showcolor="#FFFFE0";}
				
					$showday=substr($monthstart,8,2);
					$showmonth=substr($monthstart,5,2);
					$showyear=substr($monthstart,2,2);
					$weekday=dayofweek($monthstart);
					$showdate="$weekday $showmonth/$showday/$showyear";
			
					$skipdays++;
	
					$finalbudget=number_format($figurebudget[$monthstart]+$adjustbudget[$monthstart],2);
					$updatebudget[$monthstart]=round($figurebudget[$monthstart]+$adjustbudget[$monthstart],2);
					echo "<div width=100% style=\"border:1px solid #FFFF99;background-color:$showcolor; height:20px;\" align=right title='$showdate' onMouseOver=\"this.style.backgroundColor='#90EE90'; dayletter$skipdays.style.backgroundColor='#90EE90'; month$colcounter.style.backgroundColor='#90EE90';\" onMouseOut=\"this.style.backgroundColor='$showcolor'; dayletter$skipdays.style.backgroundColor='#E8E7E7'; month$colcounter.style.backgroundColor='#E8E7E7';\"><font color=#555555>$finalbudget</font></div>";
				}
				$monthstart=nextday($monthstart);
			}
			
			while($skipdays<42){
				echo "<div width=100% style=\"border:1px solid #FFFF99; background-color: #FFFFE0; height:20px;\">&nbsp;</div>";
				$skipdays++;
			}
			
			////show budget total for month
			$finaltotal=number_format($finalbudgettotal+$totaladjustamt,2);
			echo "<div width=100% style=\"border:1px solid #FFFF99; background-color: #E8E7E7\" align=right>$finaltotal</div>";
			
			/////show budget #days
			for($counter=1;$counter<=7;$counter++){
				if($counter==1){$day="Monday";}
				elseif($counter==2){$day="Tuesday";}
				elseif($counter==3){$day="Wednesday";}
				elseif($counter==4){$day="Thursday";}
				elseif($counter==5){$day="Friday";}
				elseif($counter==6){$day="Saturday";}
				elseif($counter==7){$day="Sunday";}
				if($operate>=$counter){echo "<div width=100% style=\"border:1px solid #FFFF99; background-color: #FAEBD7;\" align=right>&nbsp;</div>";}
			}
			
			////total operating days
			echo "<div width=100% style=\"border:1px solid #FFFF99; background-color: #FAEBD7;\" align=right>&nbsp;</div>";
			
			echo "</td>";
	
		}
		echo "</tr>";
		
		////////////////
		////new budgets
		///////////////
		echo "<tr bgcolor=#E8E7E7><td style=\"border:1px solid #CCCCCC;\" bgcolor=#CCCCCC><center>BGT:</center></td>";
		for($counter=1;$counter<=12;$counter++){
			if($counter==1){$monthname="January";}
			elseif($counter==2){$monthname="February";}
			elseif($counter==3){$monthname="March";}
			elseif($counter==4){$monthname="April";}
			elseif($counter==5){$monthname="May";}
			elseif($counter==6){$monthname="June";}
			elseif($counter==7){$monthname="July";}
			elseif($counter==8){$monthname="August";}
			elseif($counter==9){$monthname="September";}
			elseif($counter==10){$monthname="October";}
			elseif($counter==11){$monthname="November";}
			elseif($counter==12){$monthname="December";}
			
			$getmonth=$counter;
			if(strlen($getmonth)<2){$getmonth="0$getmonth";}
			$showbgt=Treat_DB_ProxyOld::mysql_result($result55,0,"$getmonth");
			
			echo "<td style=\"border:1px solid #CCCCCC; border-right-width: 4px;\" colspan=4 align=right>&nbsp;$monthname: <input type=text name='budget$counter' size=7 style=\"font-size:10px;\" value=$showbgt>&nbsp;</td>";
		}
		echo "</tr>";
		
		if($canupdate!=0){$is_disable="DISABLED";$message="<font color=red size=3><b>Budget must balance before updating.</b></font>";}
		else{$is_disable="";$message="";}
		
		$graphbudget=$updatebudget;
		$updatebudget=urlencode(serialize($updatebudget));
		echo "<tr><td align=right colspan=49 style=\"border:1px solid #CCCCCC;\" bgcolor=#CCCCCC>$message&nbsp;<input type=submit value='Save'></form> <form action=budget_calc_update2.php method=post style=\"margin:0;padding:0;display:inline;\"><input type=hidden name=budgetyear value='$budgetyear'><input type=hidden name=businessid value='$businessid[0]'><input type=hidden name=finalize value=1><input type=hidden name=updatebudget value='$updatebudget'><input type=submit value='Update Budget' onclick=\"return confirm('Are you sure you want to update the budget?');\" $is_disable></form></td></tr>";
		echo "</table></a></center>";
		
		/////////////////////////
		//////create graphs/////
		/////////////////////////
		for($counter=1;$counter<=12;$counter++){
			$leftmarg = 150*$counter;
			if($counter==1){$monthname="January";}
			elseif($counter==2){$monthname="February";}
			elseif($counter==3){$monthname="March";}
			elseif($counter==4){$monthname="April";}
			elseif($counter==5){$monthname="May";}
			elseif($counter==6){$monthname="June";}
			elseif($counter==7){$monthname="July";}
			elseif($counter==8){$monthname="August";}
			elseif($counter==9){$monthname="September";}
			elseif($counter==10){$monthname="October";}
			elseif($counter==11){$monthname="November";}
			elseif($counter==12){$monthname="December";}
			echo "<div id='graph$counter' onclick=\"$('graph$counter').hide();\" style=\"border:3px solid #777777; display: none; position: absolute; height: 400px; width: 600px; left: {$leftmarg}px; top: 300px; text-align: center; z-index: 1000; background-color: #FFFFFF;\"><p><center><b><font size=2>$monthname</font></b></center>";
			
			?>
			<applet code="LineGraphApplet.class" archive="Linegraph.jar" width="90%" height=300">
			<PARAM name="LOADINGMESSAGE" value="Creating Chart - Please Wait.">
			<PARAM name="STEXTCOLOR"     value="#000060">
			<PARAM name="STARTUPCOLOR"   value="#FFFFFF">
			<?
			$series1="series1";
			$series2="series2";

			/////sales data
			$counter2=1;
			$month=$counter;
			if(strlen($month)<2){$month="0$month";}
			$monthstart="$year-$month-01";
			////starting point
			$salesstart=dayofweek($monthstart);
			if($salesstart=="Monday"){$salesstart=1;}
			elseif($salesstart=="Tuesday"){$salesstart=2;}
			elseif($salesstart=="Wednesday"){$salesstart=3;}
			elseif($salesstart=="Thursday"){$salesstart=4;}
			elseif($salesstart=="Friday"){$salesstart=5;}
			elseif($salesstart=="Saturday" && ($operate == 6 || $operate == 7)){$salesstart=6;}
			elseif($salesstart=="Saturday"){$salesstart=1;}
			elseif($salesstart=="Sunday" && $operate == 7){$salesstart=7;}
			elseif($salesstart=="Sunday"){$salesstart=1;}
			
			$thismonth=substr($monthstart,5,2);
			while($thismonth==substr($monthstart,5,2)){
				if(($operate > 4 && dayofweek($monthstart) != "Saturday" && dayofweek($monthstart) != "Sunday") || ($operate > 5 && dayofweek($monthstart) != "Sunday") || ($operate > 6 )){
					echo "<PARAM name=data$counter2$series1 value='$daysales[$monthstart]'>";
					$counter2++;
				}
				$monthstart=nextday($monthstart);
			}
		
			/////budget data
			$monthstart="$budgetyear-$month-01";
			
			/////starting point
			$budgetstart=dayofweek($monthstart);
			if($budgetstart=="Monday"){$budgetstart=1;}
			elseif($budgetstart=="Tuesday"){$budgetstart=2;}
			elseif($budgetstart=="Wednesday"){$budgetstart=3;}
			elseif($budgetstart=="Thursday"){$budgetstart=4;}
			elseif($budgetstart=="Friday"){$budgetstart=5;}
			elseif($budgetstart=="Saturday" && ($operate == 6 || $operate == 7)){$budgetstart=6;}
			elseif($budgetstart=="Saturday"){$budgetstart=1;}
			elseif($budgetstart=="Sunday" && $operate == 7){$budgetstart=7;}
			elseif($budgetstart=="Sunday"){$budgetstart=1;}
			
			////find diff
			if($budgetstart==1&&($salesstart==5||$salestart==6||$salestart==7)&&$operate==5){$newstart=1;}
			elseif($budgetstart==1&&($salesstart==6||$salestart==7)&&$operate==6){$newstart=1;}
			else{$newstart=$budgetstart-$salesstart;}

			for($mycount=0;$mycount<=$newstart;$mycount++){
				echo "<PARAM name=data$counter2$series1 value='0'>";
				$counter2++;
			}
			
			$counter2=1+$newstart;
			$thismonth=substr($monthstart,5,2);
			
			while($thismonth==substr($monthstart,5,2)){
				if(($operate > 4 && dayofweek($monthstart) != "Saturday" && dayofweek($monthstart) != "Sunday") || ($operate > 5 && dayofweek($monthstart) != "Sunday") || ($operate > 6 )){
					echo "<PARAM name=data$counter2$series2 value='$graphbudget[$monthstart]'>";
					$counter2++;
				}
				$monthstart=nextday($monthstart);
			}
			?>
			<PARAM name="ylabels"		  value="S|M|T|W|T|F|S">
			<PARAM name="xlabels"		  value="S|M|T|W|T|F|S">
			<PARAM name="titlefontsize"	  value="0">
			<PARAM name="xtitlefontsize"	  value="0">
			<PARAM name="ytitlefontsize"	  value="0">
			<PARAM name="autoscale"            value="true">
			<PARAM name="gridbgcolor"          value="#FFFFFF">
			<PARAM name="series1"              value="red|6|5|false|dotted">
			<PARAM name="series2"              value="blue|6|5|true|dotted">
			<PARAM name="ndecplaces"           value="0">
			<PARAM name="labelOrientation"     value="Up Angle">
			<PARAM name="xlabel_pre"           value="">
			<PARAM name="xlabel_font"          value="Arial,N,10">
			<?
			
			/////label
			$counter2=1;
			$month=$counter;
			if(strlen($month)<2){$month="0$month";}
			$monthstart="$year-$month-01";
			for($mycount=1;$mycount<=35;$mycount++){
				if(($operate > 4 && dayofweek($monthstart) != "Saturday" && dayofweek($monthstart) != "Sunday") || ($operate > 5 && dayofweek($monthstart) != "Sunday") || ($operate > 6 )){
					$dayname=substr(dayofweek($monthstart),0,3);
					echo "<PARAM name='label$counter2' value='$dayname'>";
					$counter2++;
				}
				$monthstart=nextday($monthstart);
			}
			echo "</applet>";
			echo "<center><font color=red size=2>Sales $year </font> <font color=black size=2>|</font> <font color=blue size=2>Budget $budgetyear</font></center><p><font size=1>Click to Close</font>";
			echo "</div>";
		}
    }
	
//////////////////////////////END
    echo "<DIV ID=testdiv1 STYLE=position:absolute;visibility:hidden;background-color:white;layer-background-color:white;></DIV>";
    echo "<br><FORM ACTION=businesstrack.php method=post>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<center><INPUT TYPE=submit VALUE=' Return to Main Page'></FORM></center></body>";
}
//mysql_close();
fclose($fh);
?>