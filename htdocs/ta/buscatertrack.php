<?php

function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}

function nextday($nextd,$day_format=''){ //Function returns next day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monn = substr($nextd, 5, 2);
   $dayn = substr($nextd, 8, 2);
   $yearn = substr($nextd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monn, $dayn+1, $yearn));
   return $tempdate;
}

function prevday($prevd,$day_format=''){ //Function returns previous day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monp = substr($prevd, 5, 2);
   $dayp = substr($prevd, 8, 2);
   $yearp = substr($prevd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monp, $dayp-1, $yearp));
   return $tempdate;
}

function money($diff){
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+3);
        if ($diff > 0 && $diff < '0.01'){$diff="0.01";}
        elseif($diff < 0 && $diff > '-0.01'){$diff="-0.01";}
        return $diff;
}

function findsubs($reserveid,$accountid,$curmenu,$level){
    $style = "text-decoration:none";
    $count=$level*10;
    for($counter=1;$counter<=$count;$counter++){$indent.="&nbsp;";}

    $query = "SELECT * FROM reserve WHERE parent = '$reserveid' AND accountid = '$accountid' AND curmenu = '$curmenu' ORDER BY date DESC, start_hour DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);

    $num--;
    while ($num>=0){
       $date=@Treat_DB_ProxyOld::mysql_result($result,$num,"date");
       $roomid=@Treat_DB_ProxyOld::mysql_result($result,$num,"roomid");
       $start_hour=@Treat_DB_ProxyOld::mysql_result($result,$num,"start_hour");
       $status=@Treat_DB_ProxyOld::mysql_result($result,$num,"status");
       $comment=@Treat_DB_ProxyOld::mysql_result($result,$num,"comment");
       $reserveid=@Treat_DB_ProxyOld::mysql_result($result,$num,"reserveid");
       $peoplenum=@Treat_DB_ProxyOld::mysql_result($result,$num,"peoplenum");
       $businessid=@Treat_DB_ProxyOld::mysql_result($result,$num,"businessid");

       $weekday=dayofweek($date);
       $weekday=substr($weekday,0,3);

       if (strlen($start_hour)==3){
          $showhour=substr($start_hour,0,1);
          $showmin=substr($start_hour,1,3);
          if ($showhour>=13){$showhour=$showhour-12;$showam="PM";}
          else {$showam="AM";}
       }
       elseif (strlen($start_hour)==2){
          $showhour="12";
          $showmin=$start_hour;
          $showam="AM";
       }
       elseif (strlen($start_hour)==1){
          $showhour="12";
          $showmin="0$start_hour";
          $showam="AM";
       }
       else {
          $showhour=substr($start_hour,0,2);
          $showmin=substr($start_hour,2,4);
          if ($showhour>12){$showhour=$showhour-12;$showam="PM";}
          elseif ($showhour==12){$showam="PM";}
          else {$showam="AM";}
       }

       if ($status==1){$showstatus="Pending";}
       elseif ($status==2){$showstatus="Confirmed";}
       elseif ($status==3){$showstatus="Submitted";}
       elseif ($status == 5){$showstatus="Invoiced";}

       if ($roomid!=0){
          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query2 = "SELECT roomname FROM rooms WHERE roomid = '$roomid'";
          $result2 = Treat_DB_ProxyOld::query($query2);
          //mysql_close();

          $roomname=@Treat_DB_ProxyOld::mysql_result($result2,0,"roomname");
       }
       else {$roomname="N/A";}

       if($status==5){$color="#99CCFF";}
       elseif ($status==3){$color="yellow";}
       elseif ($status==1){$color="#F0F0F0";}
       elseif ($status==2){$color="#00FF66";}
       else {$color="white";}

       //$create_sub="<a href=createsub.php?reserveid=$reserveid title='Create Sub Order'><img src=request.gif height=16 width=16 border=0></a>";

       echo "<tr bgcolor='$color' onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor='$color'><td>$indent&nbsp;<a style=$style href=caterdetails.php?reserveid=$reserveid&account=$accountid&bid=$businessid#detail><font color=blue>$comment</font></a> ($peoplenum People)</td><td></td><td>$date $weekday</td><td>$showhour:$showmin $showam</td><td>$roomname</td><td align=right>$showstatus&nbsp;</td></tr>";

       //////list sub orders
       //findsubs($reserveid,$accountid,$curmenu,$level+1);

       $num--;
    }
}


if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
require_once( DOC_ROOT . '/bootstrap.php' );



$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$prodtoday=$today;
$todayname = date("l");
$creditamount="creditamount";
$creditidnty="creditidnty";
$creditdate="creditdate";
$debitamount="debitamount";
$debitidnty="debitidnty";
$debitdate="debitdate";
$quatertotal=0;
$monthtotal=0;
$total=0;
$counter=0;
$findme='.';
$double='.00';
$single='0';
$double2='00';

/*$user = isset($_COOKIE["usercook"])?$_COOKIE["usercook"]:'';
$pass = isset($_COOKIE["passcook"])?$_COOKIE["passcook"]:'';
$businessid = isset($_GET["bid"])?$_GET["bid"]:'';
$companyid = isset($_GET["cid"])?$_GET["cid"]:'';
$prevperiod = isset($_GET["prevperiod"])?$_GET["prevperiod"]:'';
$nextperiod = isset($_GET["nextperiod"])?$_GET["nextperiod"]:'';*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook','');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook','');
$businessid = \EE\Controller\Base::getGetVariable('bid','');
$companyid = \EE\Controller\Base::getGetVariable('cid','');
$prevperiod = \EE\Controller\Base::getGetVariable('prevperiod','');
$nextperiod = \EE\Controller\Base::getGetVariable('nextperiod','');

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=Treat_DB_ProxyOld::mysql_numrows($result);

$loginid=@Treat_DB_ProxyOld::mysql_result($result,0,"loginid");
$security_level=@Treat_DB_ProxyOld::mysql_result($result,0,"security_level");
$mysecurity=@Treat_DB_ProxyOld::mysql_result($result,0,"security");
$bid=@Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
$cid=@Treat_DB_ProxyOld::mysql_result($result,0,"companyid");
$bid2=@Treat_DB_ProxyOld::mysql_result($result,0,"busid2");
$bid3=@Treat_DB_ProxyOld::mysql_result($result,0,"busid3");
$bid4=@Treat_DB_ProxyOld::mysql_result($result,0,"busid4");
$bid5=@Treat_DB_ProxyOld::mysql_result($result,0,"busid5");
$bid6=@Treat_DB_ProxyOld::mysql_result($result,0,"busid6");
$bid7=@Treat_DB_ProxyOld::mysql_result($result,0,"busid7");
$bid8=@Treat_DB_ProxyOld::mysql_result($result,0,"busid8");
$bid9=@Treat_DB_ProxyOld::mysql_result($result,0,"busid9");
$bid10=@Treat_DB_ProxyOld::mysql_result($result,0,"busid10");

$pr1=@Treat_DB_ProxyOld::mysql_result($result,0,"payroll");
$pr2=@Treat_DB_ProxyOld::mysql_result($result,0,"pr2");
$pr3=@Treat_DB_ProxyOld::mysql_result($result,0,"pr3");
$pr4=@Treat_DB_ProxyOld::mysql_result($result,0,"pr4");
$pr5=@Treat_DB_ProxyOld::mysql_result($result,0,"pr5");
$pr6=@Treat_DB_ProxyOld::mysql_result($result,0,"pr6");
$pr7=@Treat_DB_ProxyOld::mysql_result($result,0,"pr7");
$pr8=@Treat_DB_ProxyOld::mysql_result($result,0,"pr8");
$pr9=@Treat_DB_ProxyOld::mysql_result($result,0,"pr9");
$pr10=@Treat_DB_ProxyOld::mysql_result($result,0,"pr10");

if ($security_level<3){
    $order_only=@Treat_DB_ProxyOld::mysql_result($result,0,"order_only");
    $oo2=@Treat_DB_ProxyOld::mysql_result($result,0,"oo2");
    $oo3=@Treat_DB_ProxyOld::mysql_result($result,0,"oo3");
    $oo4=@Treat_DB_ProxyOld::mysql_result($result,0,"oo4");
    $oo5=@Treat_DB_ProxyOld::mysql_result($result,0,"oo5");
    $oo6=@Treat_DB_ProxyOld::mysql_result($result,0,"oo6");
    $oo7=@Treat_DB_ProxyOld::mysql_result($result,0,"oo7");
    $oo8=@Treat_DB_ProxyOld::mysql_result($result,0,"oo8");
    $oo9=@Treat_DB_ProxyOld::mysql_result($result,0,"oo9");
    $oo10=@Treat_DB_ProxyOld::mysql_result($result,0,"oo10");
    $busid1=@Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
    $busid2=@Treat_DB_ProxyOld::mysql_result($result,0,"busid2");
    $busid3=@Treat_DB_ProxyOld::mysql_result($result,0,"busid3");
    $busid4=@Treat_DB_ProxyOld::mysql_result($result,0,"busid4");
    $busid5=@Treat_DB_ProxyOld::mysql_result($result,0,"busid5");
    $busid6=@Treat_DB_ProxyOld::mysql_result($result,0,"busid6");
    $busid7=@Treat_DB_ProxyOld::mysql_result($result,0,"busid7");
    $busid8=@Treat_DB_ProxyOld::mysql_result($result,0,"busid8");
    $busid9=@Treat_DB_ProxyOld::mysql_result($result,0,"busid9");
    $busid10=@Treat_DB_ProxyOld::mysql_result($result,0,"busid10");
}

if ($num != 1 || ($security_level==1 && ($bid != $businessid || $cid != $companyid)) || ($security_level > 1 AND $cid != $companyid) || ($user == "" && $pass == ""))
{
    echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
    ////dates///////////////////
        /*$date1 = isset($_GET["date1"])?$_GET["date1"]:'';
        $date2 = isset($_GET["date2"])?$_GET["date2"]:'';*/
	
		$date1 = \EE\Controller\Base::getGetVariable('date1','');
		$date2 = \EE\Controller\Base::getGetVariable('date2','');

        if($date1 == "" || $date2 == ""){
            /*$date1 = isset($_POST["date1"])?$_POST["date1"]:'';
            $date2 = isset($_POST["date2"])?$_POST["date2"]:'';*/
			
			$date1 = \EE\Controller\Base::getPostVariable('date1','');
			$date2 = \EE\Controller\Base::getPostVariable('date2','');
        }

        if($date1 == "" || $date2 == ""){
            /*$date1 = isset($_COOKIE["date1"])?$_COOKIE["date1"]:'';
            $date2 = isset($_COOKIE["date2"])?$_COOKIE["date2"]:'';*/
			
			$date1 = \EE\Controller\Base::getSessionCookieVariable('date1','');
			$date2 = \EE\Controller\Base::getSessionCookieVariable('date2','');
        }

        if($date1 == "" || $date2 == ""){
            $date1=date("Y-m-d");
            $date2=$date1;
            while(dayofweek($date1)!="Monday"){$date1=prevday($date1);}
            while(dayofweek($date2)!="Sunday"){$date2=nextday($date2);}
        }
    ////////////////////////////

    setcookie("date1","$date1");
    setcookie("date2","$date2");

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM company WHERE companyid = '$companyid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $companyname=@Treat_DB_ProxyOld::mysql_result($result,0,"companyname");
	$com_logo=@Treat_DB_ProxyOld::mysql_result($result,0,"logo");

	if($com_logo == ""){$com_logo = "talogo.gif";}

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM security WHERE securityid = '$mysecurity'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $sec_bus_sales=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_sales");
    $sec_bus_invoice=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_invoice");
    $sec_bus_order=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_order");
    $sec_bus_payable=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_payable");
    $sec_bus_inventor1=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor1");
    $sec_bus_control=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_control");
    $sec_bus_menu=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_menu");
    $sec_bus_order_setup=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_order_setup");
    $sec_bus_request=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_request");
    $sec_route_collection=@Treat_DB_ProxyOld::mysql_result($result,0,"route_collection");
    $sec_route_labor=@Treat_DB_ProxyOld::mysql_result($result,0,"route_labor");
    $sec_route_detail=@Treat_DB_ProxyOld::mysql_result($result,0,"route_detail");
    $sec_bus_labor=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_labor");
    $sec_bus_timeclock=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_timeclock");
	$sec_bus_budget=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_budget");

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM business WHERE businessid = '$businessid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $businessname=@Treat_DB_ProxyOld::mysql_result($result,0,"businessname");
    $businessid=@Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
    $street=@Treat_DB_ProxyOld::mysql_result($result,0,"street");
    $city=@Treat_DB_ProxyOld::mysql_result($result,0,"city");
    $state=@Treat_DB_ProxyOld::mysql_result($result,0,"state");
    $zip=@Treat_DB_ProxyOld::mysql_result($result,0,"zip");
    $phone=@Treat_DB_ProxyOld::mysql_result($result,0,"phone");
    $contract=@Treat_DB_ProxyOld::mysql_result($result,0,"contract");
    $default_menu=@Treat_DB_ProxyOld::mysql_result($result,0,"default_meu");
    $bus_unit=@Treat_DB_ProxyOld::mysql_result($result,0,"unit");
    $allow_sub_orders=@Treat_DB_ProxyOld::mysql_result($result,0,"allow_sub_orders");

    if ($contract==1){
       $location="busordertrack.php?bid=$businessid&cid=$companyid";
       header('Location: ./' . $location);
		exit();
    }
    elseif ($contract==2){
       $location="busordertrack3.php?bid=$businessid&cid=$companyid";
       header('Location: ./' . $location);
		exit();
    }
?>
<head>

<script language="JavaScript"
   type="text/JavaScript">
function changePage(newLoc)
 {
   nextPage = newLoc.options[newLoc.selectedIndex].value

   if (nextPage != "")
   {
      document.location.href = nextPage
   }
 }
</script>

<SCRIPT LANGUAGE="JavaScript" SRC="CalendarPopup.js"></SCRIPT>

<!-- This prints out the default stylehseets used by the DIV style calendar.
     Only needed if you are using the DIV style popup -->
<SCRIPT LANGUAGE="JavaScript">document.write(getCalendarStyles());</SCRIPT>

<STYLE>
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation
			{
			background-color:#6677DD;
			text-align:center;
			vertical-align:center;
			text-decoration:none;
			color:#FFFFFF;
			font-weight:bold;
			}
	.TESTcpDayColumnHeader,
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation,
	.TESTcpCurrentMonthDate,
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDate,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDate,
	.TESTcpCurrentDateDisabled,
	.TESTcpTodayText,
	.TESTcpTodayTextDisabled,
	.TESTcpText
			{
			font-family:arial;
			font-size:8pt;
			}
	TD.TESTcpDayColumnHeader
			{
			text-align:right;
			border:solid thin #6677DD;
			border-width:0 0 1 0;
			}
	.TESTcpCurrentMonthDate,
	.TESTcpOtherMonthDate,
	.TESTcpCurrentDate
			{
			text-align:right;
			text-decoration:none;
			}
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDateDisabled
			{
			color:#D0D0D0;
			text-align:right;
			text-decoration:line-through;
			}
	.TESTcpCurrentMonthDate
			{
			color:#6677DD;
			font-weight:bold;
			}
	.TESTcpCurrentDate
			{
			color: #FFFFFF;
			font-weight:bold;
			}
	.TESTcpOtherMonthDate
			{
			color:#808080;
			}
	TD.TESTcpCurrentDate
			{
			color:#FFFFFF;
			background-color: #6677DD;
			border-width:1;
			border:solid thin #000000;
			}
	TD.TESTcpCurrentDateDisabled
			{
			border-width:1;
			border:solid thin #FFAAAA;
			}
	TD.TESTcpTodayText,
	TD.TESTcpTodayTextDisabled
			{
			border:solid thin #6677DD;
			border-width:1 0 0 0;
			}
	A.TESTcpTodayText,
	SPAN.TESTcpTodayTextDisabled
			{
			height:20px;
			}
	A.TESTcpTodayText
			{
			color:#6677DD;
			font-weight:bold;
			}
	SPAN.TESTcpTodayTextDisabled
			{
			color:#D0D0D0;
			}
	.TESTcpBorder
			{
			border:solid thin #6677DD;
			}
</STYLE>

<meta http-equiv='refresh' content='600'>
</head>
<?php

    echo "<body><center><table cellspacing=0 cellpadding=0 border=0 width=90%><tr><td colspan=2>";
    echo "<div style=border:50px solid red;padding:10px;>";
    echo "<a href=businesstrack.php><img src=logo.jpg border=0 height=43 width=205></a><p></td></tr>";

    //echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";
    echo "<tr bgcolor=#CCCCFF><td width=1%><img src=logo/$com_logo></td><td><font size=4><b>$businessname #$bus_unit</b></font><br>$street<br>$city, $state $zip<br>$phone</td>";
    echo "<td align=right valign=top>";
    echo "<br><FORM ACTION=editbus.php method=post>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<input type=hidden name=businessid value=$businessid>";
    echo "<input type=hidden name=companyid value=$companyid>";
    echo "<INPUT TYPE=submit VALUE=' Store Setup '></FORM></td></tr>";
    //echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";

    echo "<tr><td colspan=3><font color=#999999>";
    if ($sec_bus_sales>0){echo "<a href=busdetail.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Sales</font></a>";}
    if ($sec_bus_invoice>0){echo " | <a href=businvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Invoicing</font></a>";}
    if ($sec_bus_order>0){echo " | <a href=buscatertrack.php?bid=$businessid&cid=$companyid><font color=red onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='red' style='text-decoration:none'>Orders</font></a>";}
    if ($sec_bus_payable>0){echo " | <a href=busapinvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Payables</font></a>";}
    if ($sec_bus_inventor1>0){echo " | <a href=businventor.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Inventory</font></a>";}
    if ($sec_bus_control>0){echo " | <a href=buscontrol.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Control Sheet</font></a>";}
    if ($sec_bus_menu>0){echo " | <a href=buscatermenu.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Menus</font></a>";}
    if ($sec_bus_order_setup>0){echo " | <a href=buscaterroom.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Order Setup</font></a>";}
    if ($sec_bus_request>0){echo " | <a href=busrequest.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Requests</font></a>";}
    if ($sec_route_collection>0||$sec_route_labor>0||$sec_route_detail>0){echo " | <a href=busroute_collect.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Route Collections</font></a>";}
    if ($sec_bus_labor>0||$sec_bus_timeclock>0){echo " | <a href=buslabor.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Labor</font></a>";}
    if ($sec_bus_budget>0||($businessid == $bid && $pr1 == 1)||($businessid == $bid2 && $pr2 == 1)||($businessid == $bid3 && $pr3 == 1)||($businessid == $bid4 && $pr4 == 1)||($businessid == $bid5 && $pr5 == 1)||($businessid == $bid6 && $pr6 == 1)||($businessid == $bid7 && $pr7 == 1)||($businessid == $bid8 && $pr8 == 1)||($businessid == $bid9 && $pr9 == 1)||($businessid == $bid10 && $pr10 == 1)){echo " | <a href=busbudget.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Budget</font></a>";}
	echo "</td></tr>";

    echo "</table></center><p>";

if ($security_level>0){
    echo "<p><center><table width=90%><tr><td width=50%><form action=businvoice.php method=post>";
    echo "<select name=gobus onChange='changePage(this.form.gobus)'>";

    $districtquery="";
    if ($security_level>2&&$security_level<7){
       ////mysql_connect($dbhost,$username,$password);
       ////@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM login_district WHERE loginid = '$loginid'";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       ////mysql_close();

       $num--;
       while($num>=0){
          $districtid=@Treat_DB_ProxyOld::mysql_result($result,$num,"districtid");
          if($districtquery==""){$districtquery="districtid = '$districtid'";}
          else{$districtquery="$districtquery OR districtid = '$districtid'";}
          $num--;
       }
    }

    ////mysql_connect($dbhost,$username,$password);
    ////@mysql_select_db($database) or die( "Unable to select database");
    if($security_level>6){$query = "SELECT * FROM business WHERE companyid = '$companyid' ORDER BY businessname DESC";}
    elseif($security_level==2||$security_level==1){$query = "SELECT * FROM business WHERE companyid = '$companyid' AND (businessid = '$bid' OR businessid = '$bid2' OR businessid = '$bid3' OR businessid = '$bid4' OR businessid = '$bid5' OR businessid = '$bid6' OR businessid = '$bid7' OR businessid = '$bid8' OR businessid = '$bid9' OR businessid = '$bid10') ORDER BY businessname DESC";}
    elseif($security_level>2&&$security_level<7){$query = "SELECT * FROM business WHERE companyid = '$companyid' AND ($districtquery) ORDER BY businessname DESC";}
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    ////mysql_close();

    $num--;
    while($num>=0){
       $businessname=@Treat_DB_ProxyOld::mysql_result($result,$num,"businessname");
       $busid=@Treat_DB_ProxyOld::mysql_result($result,$num,"businessid");

       if ($busid==$businessid){$show="SELECTED";}
       else{$show="";}

       echo "<option value=buscatertrack.php?bid=$busid&cid=$companyid $show>$businessname</option>";
       $num--;
    }

    echo "</select></td><td></form></td><td width=50% align=right></td></tr></table></center><p>";
}
////////////////////////////////////////////////

    //////consolidate invoice form
    if($allow_sub_orders == 1){

         ///prev week
        $prevdate1=$date1;
        while(dayofweek($prevdate1)!="Monday"){$prevdate1=prevday($prevdate1);}
        $prevdate2=$prevdate1;
        for($counter=1;$counter<=6;$counter++){$prevdate2=nextday($prevdate2);}

        for($counter=1;$counter<=7;$counter++){$prevdate1=prevday($prevdate1);$prevdate2=prevday($prevdate2);}

        ///next week
        $nextdate1=$date1;
        while(dayofweek($nextdate1)!="Monday"){$nextdate1=nextday($nextdate1);}
        $nextdate2=$nextdate1;
        for($counter=1;$counter<=6;$counter++){$nextdate2=nextday($nextdate2);}

        for($counter=1;$counter<=7;$counter++){$nextdate1=nextday($nextdate1);$nextdate2=nextday($nextdate2);}

        ////display filter
        echo "<center><table width=90% cellspacing=0 cellpadding=0>";
        echo "<tr><td>";
            echo "<form action=buscatertrack.php?date1=$prevdate1&date2=$prevdate2&bid=$businessid&cid=$companyid method=post style=\"margin:0;padding:0;display:inline;\"><input type=submit value='< Prev Week' style=\"border: 1px solid #666666; background-color:#E8E7E7;\"></form>&nbsp;";
            echo "<form action=buscatertrack.php?date1=$nextdate1&date2=$nextdate2&bid=$businessid&cid=$companyid method=post style=\"margin:0;padding:0;display:inline;\"><input type=submit value='Next Week >' style=\"border: 1px solid #666666; background-color:#E8E7E7;\"></form>&nbsp;";
            echo "<form action=buscatertrack.php?bid=$businessid&cid=$companyid method=post style=\"margin:0;padding:0;display:inline;\">";
            echo "<SCRIPT LANGUAGE='JavaScript' ID='cust1'> var cal1 = new CalendarPopup('testdiv1');cal1.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME='date1' VALUE='$date1' SIZE=8> <A HREF=\"javascript:void(0);\" onClick=cal1.select(document.forms[4].date1,'anchor1','yyyy-MM-dd'); return false; TITLE=cal1.select(document.forms[4].date1,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor1' ID='anchor1'><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A> to ";
            echo "<SCRIPT LANGUAGE='JavaScript' ID='cust2'> var cal2 = new CalendarPopup('testdiv1');cal2.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME='date2' VALUE='$date2' SIZE=8> <A HREF=\"javascript:void(0);\" onClick=cal2.select(document.forms[4].date2,'anchor2','yyyy-MM-dd'); return false; TITLE=cal2.select(document.forms[4].date2,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor2' ID='anchor2'><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A>&nbsp;";
            echo "<input type=submit value='GO' style=\"border: 1px solid #666666; background-color:#E8E7E7;\"></form>&nbsp;";
        echo "</td><td align=right>";
        echo "</td></tr></table></center><p>";

        echo "<form action=consolidate_invoices.php method=post style=\"margin:0;padding:0;display:inline;\"><input type=hidden name=businessid value=$businessid><input type=hidden name=companyid value=$companyid>";

    //////view orders
    echo "<center><table width=90% cellspacing=0 cellpadding=0>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM reserve WHERE businessid = '$businessid' AND date >= '$date1' AND date <= '$date2' AND parent = '0' ORDER BY date DESC, start_hour DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    //echo "<tr bgcolor=black><td height=1 colspan=6></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td width=35%><b>Orders</b></td><td width=20%><b>Account</b></td><td width=10%><b>Date</b></td><td width=10%><b>Time</b></td><td><b>Location</b></td><td align=right><b>Status</b></td></tr>";
    echo "<tr bgcolor=black><td height=1 colspan=6></td></tr>";
    echo "<tr bgcolor=#CCCCCC><td height=1 colspan=6></td></tr>";

    $num--;
    while ($num>=0){
       $date=Treat_DB_ProxyOld::mysql_result($result,$num,"date");
       $roomid=Treat_DB_ProxyOld::mysql_result($result,$num,"roomid");
       $start_hour=Treat_DB_ProxyOld::mysql_result($result,$num,"start_hour");
       $status=Treat_DB_ProxyOld::mysql_result($result,$num,"status");
       $comment=Treat_DB_ProxyOld::mysql_result($result,$num,"comment");
       $reserveid=Treat_DB_ProxyOld::mysql_result($result,$num,"reserveid");
       $accountid=Treat_DB_ProxyOld::mysql_result($result,$num,"accountid");
       $peoplenum=Treat_DB_ProxyOld::mysql_result($result,$num,"peoplenum");
       $curmenu=Treat_DB_ProxyOld::mysql_result($result,$num,"curmenu");

       if (strlen($start_hour)==3){
          $showhour=substr($start_hour,0,1);
          $showmin=substr($start_hour,1,3);
          if ($showhour>=13){$showhour=$showhour-12;$showam="PM";}
          else {$showam="AM";}
       }
       elseif (strlen($start_hour)==2){
          $showhour="12";
          $showmin=$start_hour;
          $showam="AM";
       }
       elseif (strlen($start_hour)==1){
          $showhour="12";
          $showmin="0$start_hour";
          $showam="AM";
       }
       else {
          $showhour=substr($start_hour,0,2);
          $showmin=substr($start_hour,2,4);
          if ($showhour>12){$showhour=$showhour-12;$showam="PM";}
          elseif ($showhour==12){$showam="PM";}
          else {$showam="AM";}
       }

       if ($status==1){$showstatus="Pending";}
       elseif ($status==2){$showstatus="Confirmed";}
       elseif ($status==3){$showstatus="Submitted";}
       elseif ($status==4){$showstatus="Canceled";}
       elseif ($status == 5){$showstatus="Invoiced";}


       if ($roomid!=0){
          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query2 = "SELECT roomname FROM rooms WHERE roomid = '$roomid'";
          $result2 = Treat_DB_ProxyOld::query($query2);
          //mysql_close();

          $roomname=Treat_DB_ProxyOld::mysql_result($result2,0,"roomname");
       }
       else {$roomname="N/A";}

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query3 = "SELECT * FROM accounts WHERE accountid = '$accountid'";
       $result3 = Treat_DB_ProxyOld::query($query3);
       //mysql_close();

       $accountname=Treat_DB_ProxyOld::mysql_result($result3,0,"name");
       $accountnum=Treat_DB_ProxyOld::mysql_result($result3,0,"accountnum");

       if($status==5){$color="#99CCFF";}
       elseif ($status==3){$color="yellow";}
       elseif ($status==1){$color="#F0F0F0";}
       elseif ($status==2){$color="#00FF66";}
       else {$color="white";}

       //////consolidate invoices
       if($allow_sub_orders==1&&($status==2||$status==5)){
           $showcheckbox="<input type=checkbox name='con$reserveid' value=$reserveid>";
       }
       else{
           $showcheckbox="";
       }

       ///show day of week
       $showday=dayofweek($date);
       $showday=substr($showday,0,3);

       echo "<tr bgcolor='$color' onMouseOver=this.bgColor='#999999' onMouseOut=this.bgColor='$color'><td>$showcheckbox<a style=$style href=caterdetails.php?reserveid=$reserveid&account=$accountid&bid=$businessid><font color=blue>$comment</font></a> ($peoplenum People)</td><td>$accountname</td><td>$date $showday</td><td>$showhour:$showmin $showam</td><td>$roomname</td><td align=right>$showstatus&nbsp;</td></tr>";

       //////list sub orders
       if($allow_sub_orders==1){findsubs($reserveid,$accountid,$curmenu,1);}

       echo "<tr bgcolor=#CCCCCC><td height=1 colspan=6></td></tr>";

       $num--;
    }

    echo "<tr bgcolor=black><td height=1 colspan=6></td></tr>";

    echo "<tr><td colspan=6>Account: <select name=accountid>";

    $query3 = "SELECT accountid,name FROM accounts WHERE businessid = '$businessid' AND is_deleted = '0' ORDER BY name";
    $result3 = Treat_DB_ProxyOld::query($query3);

    while($r=Treat_DB_ProxyOld::mysql_fetch_array($result3)){
        $accountid = $r["accountid"];
        $name = $r["name"];

        echo "<option value=$accountid>$name</option>";
    }

    echo "</select> <input type=submit value='Create Invoice for Selected Events' onclick=\"return confirm('Are you sure you want to create a consolidated invoice?');\"></form></td></tr>";
    echo "</table></center>";

    }
    ///////////////////////////////////
    /////////Normal////////////////////
    ///////////////////////////////////
    else{

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM reserve WHERE businessid = '$businessid' AND date < '$today' AND date != '0000-00-00' AND status != '1' AND parent = '0' ORDER BY date, start_hour DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

if ($num!=0){
    echo "<p><center><table width=90% cellspacing=0 cellpadding=0>";
    //echo "<tr bgcolor=black><td height=1 colspan=6></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td width=35%><img src=error.gif height=16 width=20> <b>Past</b></td><td width=20%><b>Account</b></td><td width=10%><b>Date</b></td><td width=10%><b>Time</b></td><td><b>Location</b></td><td align=right><b>Status</b></td></tr>";
    echo "<tr bgcolor=black><td height=1 colspan=6></td></tr>";
    echo "<tr bgcolor=#CCCCCC><td height=1 colspan=6></td></tr>";

    $num--;
    while ($num>=0){
       $date=@Treat_DB_ProxyOld::mysql_result($result,$num,"date");
       $roomid=@Treat_DB_ProxyOld::mysql_result($result,$num,"roomid");
       $start_hour=@Treat_DB_ProxyOld::mysql_result($result,$num,"start_hour");
       $status=@Treat_DB_ProxyOld::mysql_result($result,$num,"status");
       $comment=@Treat_DB_ProxyOld::mysql_result($result,$num,"comment");
       $reserveid=@Treat_DB_ProxyOld::mysql_result($result,$num,"reserveid");
       $accountid=@Treat_DB_ProxyOld::mysql_result($result,$num,"accountid");
       $peoplenum=@Treat_DB_ProxyOld::mysql_result($result,$num,"peoplenum");
       $curmenu=@Treat_DB_ProxyOld::mysql_result($result,$num,"curmenu");

       if (strlen($start_hour)==3){
          $showhour=substr($start_hour,0,1);
          $showmin=substr($start_hour,1,3);
          if ($showhour>=13){$showhour=$showhour-12;$showam="PM";}
          else {$showam="AM";}
       }
       elseif (strlen($start_hour)==2){
          $showhour="12";
          $showmin=$start_hour;
          $showam="AM";
       }
       elseif (strlen($start_hour)==1){
          $showhour="12";
          $showmin="0$start_hour";
          $showam="AM";
       }
       else {
          $showhour=substr($start_hour,0,2);
          $showmin=substr($start_hour,2,4);
          if ($showhour>12){$showhour=$showhour-12;$showam="PM";}
          elseif ($showhour==12){$showam="PM";}
          else {$showam="AM";}
       }

       if ($status==1){$showstatus="Pending";}
       elseif ($status==2){$showstatus="Confirmed";}
       elseif ($status==3){$showstatus="Submitted";}


       if ($roomid!=0){
          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query2 = "SELECT roomname FROM rooms WHERE roomid = '$roomid'";
          $result2 = Treat_DB_ProxyOld::query($query2);
          //mysql_close();

          $roomname=@Treat_DB_ProxyOld::mysql_result($result2,0,"roomname");
       }
       else {$roomname="N/A";}

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query3 = "SELECT * FROM accounts WHERE accountid = '$accountid'";
      //// $result3 = Treat_DB_ProxyOld::query($query3);
       //mysql_close();

       $accountname=@Treat_DB_ProxyOld::mysql_result($result3,0,"name");
       $accountnum=@Treat_DB_ProxyOld::mysql_result($result3,0,"accountnum");

       if ($status==3){$color="yellow";}
       elseif ($status==1){$color="#F0F0F0";}
       elseif ($status==2){$color="#00FF66";}
       else {$color="white";}

       //////consolidate invoices
       if($allow_sub_orders==1&&$status==2){
           $showcheckbox="<input type=checkbox name='con$reserveid' value=$reserveid>";
       }
       else{
           $showcheckbox="";
       }

       echo "<tr bgcolor='$color' onMouseOver=this.bgColor='#999999' onMouseOut=this.bgColor='$color'><td>$showcheckbox<a style=$style href=caterdetails.php?reserveid=$reserveid&account=$accountid&bid=$businessid><font color=blue> $comment</font></a> ($peoplenum People)</td><td>$accountname</td><td>$date</td><td>$showhour:$showmin $showam</td><td>$roomname</td><td align=right>$showstatus</td></tr>";

       //////list sub orders
       if($allow_sub_orders==1){findsubs($reserveid,$accountid,$curmenu,1);}

       echo "<tr bgcolor=#CCCCCC><td height=1 colspan=6></td></tr>";

       $num--;
    }

    echo "<tr bgcolor=black><td height=1 colspan=6></td></tr>";
    echo "</table></center>";
}

////////////////////////////////////////////////

    echo "<p><center><table width=90% cellspacing=0 cellpadding=0>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM reserve WHERE businessid = '$businessid' AND date = '$today' AND status != '1' AND parent = '0' ORDER BY date, start_hour DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    //echo "<tr bgcolor=black><td height=1 colspan=6></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td width=35%><b>Today's Scheduled Orders</b></td><td width=20%><b>Account</b></td><td width=10%><b>Date</b></td><td width=10%><b>Time</b></td><td><b>Location</b></td><td align=right><b>Status</b></td></tr>";
    echo "<tr bgcolor=black><td height=1 colspan=6></td></tr>";
    echo "<tr bgcolor=#CCCCCC><td height=1 colspan=6></td></tr>";

    $num--;
    while ($num>=0){
       $date=@Treat_DB_ProxyOld::mysql_result($result,$num,"date");
       $roomid=@Treat_DB_ProxyOld::mysql_result($result,$num,"roomid");
       $start_hour=@Treat_DB_ProxyOld::mysql_result($result,$num,"start_hour");
       $status=@Treat_DB_ProxyOld::mysql_result($result,$num,"status");
       $comment=@Treat_DB_ProxyOld::mysql_result($result,$num,"comment");
       $reserveid=@Treat_DB_ProxyOld::mysql_result($result,$num,"reserveid");
       $accountid=@Treat_DB_ProxyOld::mysql_result($result,$num,"accountid");
       $peoplenum=@Treat_DB_ProxyOld::mysql_result($result,$num,"peoplenum");
       $curmenu=@Treat_DB_ProxyOld::mysql_result($result,$num,"curmenu");

       if (strlen($start_hour)==3){
          $showhour=substr($start_hour,0,1);
          $showmin=substr($start_hour,1,3);
          if ($showhour>=13){$showhour=$showhour-12;$showam="PM";}
          else {$showam="AM";}
       }
       elseif (strlen($start_hour)==2){
          $showhour="12";
          $showmin=$start_hour;
          $showam="AM";
       }
       elseif (strlen($start_hour)==1){
          $showhour="12";
          $showmin="0$start_hour";
          $showam="AM";
       }
       else {
          $showhour=substr($start_hour,0,2);
          $showmin=substr($start_hour,2,4);
          if ($showhour>12){$showhour=$showhour-12;$showam="PM";}
          elseif ($showhour==12){$showam="PM";}
          else {$showam="AM";}
       }

       if ($status==1){$showstatus="Pending";}
       elseif ($status==2){$showstatus="Confirmed";}
       elseif ($status==3){$showstatus="Submitted";}


       if ($roomid!=0){
          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query2 = "SELECT roomname FROM rooms WHERE roomid = '$roomid'";
          $result2 = Treat_DB_ProxyOld::query($query2);
          //mysql_close();

          $roomname=@Treat_DB_ProxyOld::mysql_result($result2,0,"roomname");
       }
       else {$roomname="N/A";}

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query3 = "SELECT * FROM accounts WHERE accountid = '$accountid'";
      // $result3 = Treat_DB_ProxyOld::query($query3);
       //mysql_close();

       $accountname=@Treat_DB_ProxyOld::mysql_result($result3,0,"name");
       $accountnum=@Treat_DB_ProxyOld::mysql_result($result3,0,"accountnum");

       if ($status==3){$color="yellow";}
       elseif ($status==1){$color="#F0F0F0";}
       elseif ($status==2){$color="#00FF66";}
       else {$color="white";}

       //////consolidate invoices
       if($allow_sub_orders==1&&$status==2){
           $showcheckbox="<input type=checkbox name='con$reserveid' value=$reserveid>";
       }
       else{
           $showcheckbox="";
       }

       echo "<tr bgcolor='$color' onMouseOver=this.bgColor='#999999' onMouseOut=this.bgColor='$color'><td>$showcheckbox<a style=$style href=caterdetails.php?reserveid=$reserveid&account=$accountid&bid=$businessid><font color=blue>$comment</font></a> ($peoplenum People)</td><td>$accountname</td><td>$date</td><td>$showhour:$showmin $showam</td><td>$roomname</td><td align=right>$showstatus</td></tr>";

       //////list sub orders
       if($allow_sub_orders==1){findsubs($reserveid,$accountid,$curmenu,1);}

       echo "<tr bgcolor=#CCCCCC><td height=1 colspan=6></td></tr>";

       $num--;
    }

    echo "<tr bgcolor=black><td height=1 colspan=6></td></tr>";
    echo "</table></center>";
///////////////////////////////////////////////////////
    $today = nextday($today);

    echo "<p><center><table width=90% cellspacing=0 cellpadding=0>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM reserve WHERE businessid = '$businessid' AND date = '$today' AND status != '1' AND parent = '0' ORDER BY date, start_hour DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    //echo "<tr bgcolor=black><td height=1 colspan=6></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td width=35%><b>Tomorrow's Scheduled Orders</b></td><td width=20%><b>Account</b></td><td width=10%><b>Date</b></td><td width=10%><b>Time</b></td><td><b>Location</b></td><td align=right><b>Status</b></td></tr>";
    echo "<tr bgcolor=black><td height=1 colspan=6></td></tr>";
    echo "<tr bgcolor=#CCCCCC><td height=1 colspan=6></td></tr>";

    $num--;
    while ($num>=0){
       $date=@Treat_DB_ProxyOld::mysql_result($result,$num,"date");
       $roomid=@Treat_DB_ProxyOld::mysql_result($result,$num,"roomid");
       $start_hour=@Treat_DB_ProxyOld::mysql_result($result,$num,"start_hour");
       $status=@Treat_DB_ProxyOld::mysql_result($result,$num,"status");
       $comment=@Treat_DB_ProxyOld::mysql_result($result,$num,"comment");
       $reserveid=@Treat_DB_ProxyOld::mysql_result($result,$num,"reserveid");
       $accountid=@Treat_DB_ProxyOld::mysql_result($result,$num,"accountid");
       $peoplenum=@Treat_DB_ProxyOld::mysql_result($result,$num,"peoplenum");
       $curmenu=@Treat_DB_ProxyOld::mysql_result($result,$num,"curmenu");

       if (strlen($start_hour)==3){
          $showhour=substr($start_hour,0,1);
          $showmin=substr($start_hour,1,3);
          if ($showhour>=13){$showhour=$showhour-12;$showam="PM";}
          else {$showam="AM";}
       }
       elseif (strlen($start_hour)==2){
          $showhour="12";
          $showmin=$start_hour;
          $showam="AM";
       }
       elseif (strlen($start_hour)==1){
          $showhour="12";
          $showmin="0$start_hour";
          $showam="AM";
       }
       else {
          $showhour=substr($start_hour,0,2);
          $showmin=substr($start_hour,2,4);
          if ($showhour>12){$showhour=$showhour-12;$showam="PM";}
          elseif ($showhour==12){$showam="PM";}
          else {$showam="AM";}
       }

       if ($status==1){$showstatus="Pending";}
       elseif ($status==2){$showstatus="Confirmed";}
       elseif ($status==3){$showstatus="Submitted";}


       if ($roomid!=0){
          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query2 = "SELECT roomname FROM rooms WHERE roomid = '$roomid'";
          $result2 = Treat_DB_ProxyOld::query($query2);
          //mysql_close();

          $roomname=@Treat_DB_ProxyOld::mysql_result($result2,0,"roomname");
       }
       else {$roomname="N/A";}

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query3 = "SELECT * FROM accounts WHERE accountid = '$accountid'";
      // $result3 = Treat_DB_ProxyOld::query($query3);
       //mysql_close();

       $accountname=@Treat_DB_ProxyOld::mysql_result($result3,0,"name");
       $accountnum=@Treat_DB_ProxyOld::mysql_result($result3,0,"accountnum");

       if ($status==3){$color="yellow";}
       elseif ($status==1){$color="#F0F0F0";}
       elseif ($status==2){$color="#00FF66";}
       else {$color="white";}

       //////consolidate invoices
       if($allow_sub_orders==1&&$status==2){
           $showcheckbox="<input type=checkbox name='con$reserveid' value=$reserveid>";
       }
       else{
           $showcheckbox="";
       }

       echo "<tr bgcolor='$color' onMouseOver=this.bgColor='#999999' onMouseOut=this.bgColor='$color'><td>$showcheckbox<a style=$style href=caterdetails.php?reserveid=$reserveid&account=$accountid&bid=$businessid><font color=blue>$comment</font></a> ($peoplenum People)</td><td>$accountname</td><td>$date</td><td>$showhour:$showmin $showam</td><td>$roomname</td><td align=right>$showstatus</td></tr>";

       //////list sub orders
       if($allow_sub_orders==1){findsubs($reserveid,$accountid,$curmenu,1);}

       echo "<tr bgcolor=#CCCCCC><td height=1 colspan=6></td></tr>";

       $num--;
    }

    echo "<tr bgcolor=black><td height=1 colspan=6></td></tr>";
    echo "</table></center>";

///////////////////////////////////////////////////////
    $today = nextday($today);

    echo "<p><center><table width=90% cellspacing=0 cellpadding=0>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM reserve WHERE businessid = '$businessid' AND date >= '$today' AND status != '1' AND parent = '0' ORDER BY date DESC, start_hour DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    //echo "<tr bgcolor=black><td height=1 colspan=6></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td width=35%><b>Future Scheduled Orders</b></td><td width=20%><b>Account</b></td><td width=10%><b>Date</b></td><td width=10%><b>Time</b></td><td><b>Location</b></td><td align=right><b>Status</b></td></tr>";
    echo "<tr bgcolor=black><td height=1 colspan=6></td></tr>";
    echo "<tr bgcolor=#CCCCCC><td height=1 colspan=6></td></tr>";

    $num--;
    while ($num>=0){
       $date=@Treat_DB_ProxyOld::mysql_result($result,$num,"date");
       $roomid=@Treat_DB_ProxyOld::mysql_result($result,$num,"roomid");
       $start_hour=@Treat_DB_ProxyOld::mysql_result($result,$num,"start_hour");
       $status=@Treat_DB_ProxyOld::mysql_result($result,$num,"status");
       $comment=@Treat_DB_ProxyOld::mysql_result($result,$num,"comment");
       $reserveid=@Treat_DB_ProxyOld::mysql_result($result,$num,"reserveid");
       $accountid=@Treat_DB_ProxyOld::mysql_result($result,$num,"accountid");
       $peoplenum=@Treat_DB_ProxyOld::mysql_result($result,$num,"peoplenum");
       $curmenu=@Treat_DB_ProxyOld::mysql_result($result,$num,"curmenu");

       if (strlen($start_hour)==3){
          $showhour=substr($start_hour,0,1);
          $showmin=substr($start_hour,1,3);
          if ($showhour>=13){$showhour=$showhour-12;$showam="PM";}
          else {$showam="AM";}
       }
       elseif (strlen($start_hour)==2){
          $showhour="12";
          $showmin=$start_hour;
          $showam="AM";
       }
       elseif (strlen($start_hour)==1){
          $showhour="12";
          $showmin="0$start_hour";
          $showam="AM";
       }
       else {
          $showhour=substr($start_hour,0,2);
          $showmin=substr($start_hour,2,4);
          if ($showhour>12){$showhour=$showhour-12;$showam="PM";}
          elseif ($showhour==12){$showam="PM";}
          else {$showam="AM";}
       }

       if ($status==1){$showstatus="Pending";}
       elseif ($status==2){$showstatus="Confirmed";}
       elseif ($status==3){$showstatus="Submitted";}


       if ($roomid!=0){
          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query2 = "SELECT roomname FROM rooms WHERE roomid = '$roomid'";
          $result2 = Treat_DB_ProxyOld::query($query2);
          //mysql_close();

          $roomname=@Treat_DB_ProxyOld::mysql_result($result2,0,"roomname");
       }
       else {$roomname="N/A";}

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query3 = "SELECT * FROM accounts WHERE accountid = '$accountid'";
      // $result3 = Treat_DB_ProxyOld::query($query3);
       //mysql_close();

       $accountname=@Treat_DB_ProxyOld::mysql_result($result3,0,"name");
       $accountnum=@Treat_DB_ProxyOld::mysql_result($result3,0,"accountnum");

       if ($status==3){$color="yellow";}
       elseif ($status==1){$color="#F0F0F0";}
       elseif ($status==2){$color="#00FF66";}
       else {$color="white";}

       //////consolidate invoices
       if($allow_sub_orders==1&&$status==2){
           $showcheckbox="<input type=checkbox name='con$reserveid' value=$reserveid>";
       }
       else{
           $showcheckbox="";
       }

       echo "<tr bgcolor='$color' onMouseOver=this.bgColor='#999999' onMouseOut=this.bgColor='$color'><td>$showcheckbox<a style=$style href=caterdetails.php?reserveid=$reserveid&account=$accountid&bid=$businessid><font color=blue>$comment</font></a> ($peoplenum People)</td><td>$accountname</td><td>$date</td><td>$showhour:$showmin $showam</td><td>$roomname</td><td align=right>$showstatus</td></tr>";

       //////list sub orders
       if($allow_sub_orders==1){findsubs($reserveid,$accountid,$curmenu,1);}

       echo "<tr bgcolor=#CCCCCC><td height=1 colspan=6></td></tr>";

       $num--;
    }

    echo "<tr bgcolor=black><td height=1 colspan=6></td></tr>";
    echo "</table></center><a name='end'></a>";

/////////////////////////PENDING////////////////////////////

    echo "<p><center><table width=90% cellspacing=0 cellpadding=0>";

    $today=date("Y-m-d");

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM reserve WHERE businessid = '$businessid' AND status = '1' AND curmenu = '$default_menu' AND date >= '$today' ORDER BY date DESC, start_hour DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    //echo "<tr bgcolor=black><td height=1 colspan=6></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td width=35%><b>Orders In Progress</b></td><td width=20%><b>Account</b></td><td width=10%><b>Date</b></td><td width=10%><b>Time</b></td><td><b>Location</b></td><td align=right><b>Status</b></td></tr>";
    echo "<tr bgcolor=black><td height=1 colspan=6></td></tr>";
    echo "<tr bgcolor=#CCCCCC><td height=1 colspan=6></td></tr>";

    $num--;
    while ($num>=0){
       $date=@Treat_DB_ProxyOld::mysql_result($result,$num,"date");
       $roomid=@Treat_DB_ProxyOld::mysql_result($result,$num,"roomid");
       $start_hour=@Treat_DB_ProxyOld::mysql_result($result,$num,"start_hour");
       $status=@Treat_DB_ProxyOld::mysql_result($result,$num,"status");
       $comment=@Treat_DB_ProxyOld::mysql_result($result,$num,"comment");
       $reserveid=@Treat_DB_ProxyOld::mysql_result($result,$num,"reserveid");
       $accountid=@Treat_DB_ProxyOld::mysql_result($result,$num,"accountid");
       $peoplenum=@Treat_DB_ProxyOld::mysql_result($result,$num,"peoplenum");
       $curmenu=@Treat_DB_ProxyOld::mysql_result($result,$num,"curmenu");

       if (strlen($start_hour)==3){
          $showhour=substr($start_hour,0,1);
          $showmin=substr($start_hour,1,3);
          if ($showhour>=13){$showhour=$showhour-12;$showam="PM";}
          else {$showam="AM";}
       }
       elseif (strlen($start_hour)==2){
          $showhour="12";
          $showmin=$start_hour;
          $showam="AM";
       }
       elseif (strlen($start_hour)==1){
          $showhour="12";
          $showmin="0$start_hour";
          $showam="AM";
       }
       else {
          $showhour=substr($start_hour,0,2);
          $showmin=substr($start_hour,2,4);
          if ($showhour>12){$showhour=$showhour-12;$showam="PM";}
          elseif ($showhour==12){$showam="PM";}
          else {$showam="AM";}
       }

       if ($status==1){$showstatus="Pending";}
       elseif ($status==2){$showstatus="Confirmed";}
       elseif ($status==3){$showstatus="Submitted";}


       if ($roomid!=0){
          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query2 = "SELECT roomname FROM rooms WHERE roomid = '$roomid'";
          $result2 = Treat_DB_ProxyOld::query($query2);
          //mysql_close();

          $roomname=@Treat_DB_ProxyOld::mysql_result($result2,0,"roomname");
       }
       else {$roomname="N/A";}

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query3 = "SELECT * FROM accounts WHERE accountid = '$accountid'";
      // $result3 = Treat_DB_ProxyOld::query(/);
       //mysql_close();

       $accountname=@Treat_DB_ProxyOld::mysql_result($result3,0,"name");
       $accountnum=@Treat_DB_ProxyOld::mysql_result($result3,0,"accountnum");

       if ($status==3){$color="yellow";}
       elseif ($status==1){$color="#F0F0F0";}
       elseif ($status==2){$color="#00FF66";}
       else {$color="white";}

       echo "<tr bgcolor='$color' onMouseOver=this.bgColor='#999999' onMouseOut=this.bgColor='$color'><td><a style=$style href=caterdetails.php?reserveid=$reserveid&account=$accountid&bid=$businessid><font color=blue>$comment</font></a> ($peoplenum People)</td><td>$accountname</td><td>$date</td><td>$showhour:$showmin $showam</td><td>$roomname</td><td align=right>$showstatus</td></tr>";

       //////list sub orders
       if($allow_sub_orders==1){findsubs($reserveid,$accountid,$curmenu,1);}

       echo "<tr bgcolor=#CCCCCC><td height=1 colspan=6></td></tr>";

       $num--;
    }

    echo "<tr bgcolor=black><td height=1 colspan=6></td></tr>";

    echo "</table></center><a name='end'></a>";
    /////////////////////////////////
    /////////End Normal//////////////
    /////////////////////////////////
    }

    //////production
    if($allow_sub_orders==1){$formnum=6;}
    else{$formnum=2;}
    echo "<p><br><center><form method=post action='production.php' target='_blank'><input type=hidden name=companyid value='$companyid'><input type=hidden name=businessid value='$businessid'><SCRIPT LANGUAGE='JavaScript' ID='js18'> var now = new Date(); var cal18 = new CalendarPopup('testdiv1'); cal18.setCssPrefix('TEST');</SCRIPT><A HREF='javascript:void();' onClick=cal18.select(document.forms[$formnum].date,'anchor18','yyyy-MM-dd'); return false; TITLE=cal18.select(document.forms[$formnum].date,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor18' ID='anchor18'><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A> <INPUT TYPE=text NAME='date' VALUE=$prodtoday SIZE=8> <input type=submit value='Production Sheet'></form></center><p><br>";

    echo "<FORM ACTION=businesstrack.php method=post>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<center><INPUT TYPE=submit VALUE=' Return to Main Page'></FORM></center>";
    echo "<DIV ID=testdiv1 STYLE=position:absolute;visibility:hidden;background-color:white;layer-background-color:white;></DIV></body>";

	google_page_track();
}
//mysql_close();
?>