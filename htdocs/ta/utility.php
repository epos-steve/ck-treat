<?php

if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
require_once( DOC_ROOT . '/bootstrap.php' );

#$db = new MyDB($dbhost, $dbname, $dbuser, $dbpass);
$db = Treat_DB_Adrian::singleton();
$style = "text-decoration:none";

$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");

$user = isset($_COOKIE["usercook"])?$_COOKIE["usercook"]:'';
$pass = isset($_COOKIE["passcook"])?$_COOKIE["passcook"]:'';
$date1 = isset($_COOKIE["date1cook"])?$_COOKIE["date1cook"]:'';
$unsubid = (isset($_GET["unsubid"]))?$_GET["unsubid"]:'';
$unsubid2= (isset($_GET["unsubid2"]))?$_GET["unsubid2"]:'';
$udate= (isset($_GET["udate"]))?$_GET["udate"]:'';
$companyid=(isset($_GET["cid"]))?$_GET["cid"]:'';
$from= (isset($_GET["from"]))?$_GET["from"]:'';
$invoiceid= (isset($_POST["invoiceid"]))?$_POST["invoiceid"]:'';
$date1= (isset($_POST["date1"]))?$_POST["date1"]:'';
$date2=(isset($_POST["date2"]))?$_POST["date2"]:'';
$viewdetail= (isset($_POST["viewdetail"]))?$_POST["viewdetail"]:'';
$import_bid = $_GET['import_bid'];
$import_date = $_GET['date'];

$page_business = Treat_Model_Business_Singleton::getSingleton( $import_bid );
if ( !$page_business ) {
	$page_business = new Treat_ArrayObject();
}

$sql_user = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$rs_user = $db->queryForArray($sql_user);
$num = count($rs_user);

$security_level= $rs_user["security_level"];
$bid=$rs_user["businessid"];
$cid=$rs_user["companyid"];

if (
	$num == 0
	|| (
		$security_level==1
		&& ($bid != $businessid || $cid != $companyid)
	)
	|| ($security_level > 1 && $cid != $companyid)
	|| ($user == "" && $pass == "")
) {
	echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}

else
{


?>
<html>
<head>
	<title>Treat America :: Utility</title>
	<script type="text/javascript" src="/assets/js/calendarPopup.js"></script>
	<script type="text/javascript">document.write(getCalendarStyles());</script>
	<link type="text/css" rel="stylesheet" href="/assets/css/calendarPopup.css" />
	<link type="text/css" rel="stylesheet" href="/assets/css/ta/utility.css" />
	<script type="text/javascript" src="javascripts/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="javascripts/jquery-ui-1.7.2.custom.min.js"></script>
    <script type="text/javascript">$(document).ready(function (){$('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' });});</script>
	<link rel="stylesheet" href="css/jq_themes/redmond/jquery-ui-1.7.2.custom.css">
</head>
<body>
	<div>
		<center>
			<table cellspacing=0 cellpadding=0 border=0 width=90%>
				<tr>
					<td colspan=2>
						<a href="/ta/businesstrack.php"><img src="/ta/logo.jpg" border=0 height=43 width=205 alt="logo" /></a>
						<p></p>
					</td>
				</tr>
<?php

	$sql_company = "SELECT * FROM company WHERE companyid = '$companyid'";
	$rs_company = $db->queryForArray($sql_company);

	$companyname=$rs_company['companyname'];

	//echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";
?>
				<tr bgcolor=#CCCCFF>
					<td colspan=2>
						<font size=4>
							<b>Utilities - <?php echo $companyname; ?></b>
						</font>
					</td>
				</tr>
				<tr bgcolor=black>
					<td colspan=2 height=1></td>
				</tr>
			</table>
		</center>
<?php

	if ($date1==""){$date1=$today;$date2=$today;}

?>
		<center>
			<table width=90% border=0 cellspacing=0 cellpadding=0>
				<tr valign=bottom bgcolor=#E8E7E7>
					<td colspan=2>
						<table width=100%>
							<tr bgcolor=#E8E7E7>
								<td>
									<FORM ACTION='unsubmit.php' method='post'>
										<p>
											<input type=hidden name=username value="<?php echo $user; ?>" />
											<input type=hidden name=password value="<?php echo $pass; ?>" />
											<input type=hidden name=companyid value="<?php echo $companyid; ?>" />
											<b>UNSUBMIT SALES</b>
											<SCRIPT LANGUAGE='JavaScript' ID='js18'>
												var cal18 = new CalendarPopup('testdiv1');
												cal18.setCssPrefix('TEST');
											</SCRIPT>
											<A href="javascript:void();"
												onClick="cal18.select(document.forms[0].date1,'anchor18','yyyy-MM-dd'); return false;"
												TITLE="cal18.select(document.forms[0].date1,'anchor1x','yyyy-MM-dd'); return false;"
												NAME='anchor18'
												ID='anchor18'
											><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'/></A>
											<INPUT TYPE=text NAME=date1 VALUE='<?php echo $date1; ?>' SIZE=8>
											<b>FOR</b>

<?php

	echo pick_company($companyid);

?>
											<INPUT TYPE=submit VALUE='GO'>
										</p>
									</FORM>
								</td>
								<td align=right>

								</td>
							</tr>
						</table>
					</td>
				</tr>
<?php

	if ($unsubid!="" && $udate != "" && $unsubid != 0 && $from != 1){

		$sql_business = "SELECT businessname FROM business WHERE businessid = '$unsubid'";
		$rs_business = $db->queryForArray($sql_business);

		$businessname=$rs_business['businessname'];
?>
				<tr bgcolor=#E8E7E7>
					<td>
						<font color=red>
							<b><?php echo $businessname; ?> unsubmitted for <?php echo $udate; ?>.</b>
						</font>
					</td>
				</tr>
<?php
	}

?>
				<tr bgcolor=black>
					<td height=2></td>
				</tr>
<?php

	///UNSUBMIT LABOR

?>
				<tr valign=bottom bgcolor=#E8E7E7>
					<td colspan=2>
						<FORM ACTION='unsubmit_labor.php' method='post'>
							<input type=hidden name=companyid value="<?php echo $companyid; ?>" />
							<table width=100%>
								<tr bgcolor=#E8E7E7>
									<td>
										<p><br>
											<b>UNSUBMIT LABOR</b>
											<SCRIPT LANGUAGE='JavaScript' ID='js38'>
												var cal38 = new CalendarPopup('testdiv1');
												cal38.setCssPrefix('TEST');
											</SCRIPT>
											<A HREF="javascript:void();"
												onClick="cal38.select(document.forms[1].date1,'anchor38','yyyy-MM-dd'); return false;"
												TITLE="cal38.select(document.forms[1].date1,'anchor1x','yyyy-MM-dd'); return false;"
												NAME='anchor38' ID='anchor38'
											><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A>
											<INPUT TYPE=text NAME=date1 VALUE="<?php echo $date1; ?>" SIZE=8>
											<b>TO</b>
											<SCRIPT LANGUAGE='JavaScript' ID='js39'>
												var cal39 = new CalendarPopup('testdiv1');
												cal39.setCssPrefix('TEST');
											</SCRIPT>
											<A HREF="javascript:void();"
												onClick="cal39.select(document.forms[1].date2,'anchor39','yyyy-MM-dd'); return false;"
												TITLE="cal39.select(document.forms[1].date2,'anchor1x','yyyy-MM-dd'); return false;"
												NAME='anchor39' ID='anchor39'
											><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A>
											<INPUT TYPE=text NAME=date2 VALUE="<?php echo $date2; ?>" SIZE=8>
											<b>FOR</b>
<?php

	echo pick_company($companyid);

?>
											<INPUT TYPE=submit VALUE='GO' />
										</p>
									</td>
									<td align=right></td>
								</tr>
							</table>
						</FORM>
					</td>
				</tr>
<?php

	$su=(isset($_GET["su"]))?$_GET['su']:'';
	if ($su>0){
		$sql_su = "SELECT businessname FROM business WHERE businessid = '$su'";
		$rs_su = $db->queryForArray($sql_su);

		$businessname= $rs_su["businessname"];
?>
				<tr bgcolor=#E8E7E7>
					<td>
						<font color=red>
							<b><?php echo $businessname; ?> unsubmitted.</b>
						</font>
					</td>
				</tr>
<?php
	}

?>
				<tr bgcolor=black>
					<td height=2></td>
				</tr>
<?php


	///UNSUBMIT INVENTORY

	$year1=date("Y");
	$month1=date("n");
	$thismonthdate="$year1-$month1-01";
	$monthname=date("F");
	$lastmonth=$month-1;
	if ($lastmonth==1){$lastmonthname="January";}
	elseif ($lastmonth==2){$lastmonthname="February";}
	elseif ($lastmonth==3){$lastmonthname="March";}
	elseif ($lastmonth==4){$lastmonthname="April";}
	elseif ($lastmonth==5){$lastmonthname="May";}
	elseif ($lastmonth==6){$lastmonthname="June";}
	elseif ($lastmonth==7){$lastmonthname="July";}
	elseif ($lastmonth==8){$lastmonthname="August";}
	elseif ($lastmonth==9){$lastmonthname="September";}
	elseif ($lastmonth==10){$lastmonthname="October";}
	elseif ($lastmonth==11){$lastmonthname="November";}
	elseif ($lastmonth==0){$lastmonthname="December";}

	if ($lastmonth<10){$lastmonth="0$lastmonth";}
	if ($lastmonth==0){$year1--;$lastmonth="12";}
	$lastmonthdate="$year1-$lastmonth-01";

?>
				<tr valign=bottom bgcolor=#E8E7E7>
					<td colspan=2>
						<FORM ACTION='unsubmitinv.php' method='post'>
							<input type=hidden name=username value="<?php echo $user; ?>" />
							<input type=hidden name=password value="<?php echo $pass; ?>" />
							<input type=hidden name=companyid value="<?php echo $companyid; ?>" />
							<table width=100%>
								<tr bgcolor=#E8E7E7>
									<td>
										<p>
											<br/>
											<b>UNSUBMIT</b>
											<select name=date1>
												<option value="<?php echo $thismonthdate;
													?>"><?php echo $monthname;
												?></option>
												<option value="<?php echo $lastmonthdate;
													?>"><?php echo $lastmonthname;
												?></option>
											</select>
											<b>INVENTORY FOR</b>
<?php

	echo pick_company($companyid);

?>
											<INPUT TYPE=submit VALUE='GO'/>
										</p>
									</td>
									<td align=right></td>
								</tr>
							</table>
						</FORM>
					</td>
				</tr>
<?php

	if ($unsubid!="" && $udate != "" && $unsubid != 0 && $from == 1){

		$sql_unset = "SELECT businessname FROM business WHERE businessid = '$unsubid'";
		$rs_unset = $db->queryForArray($sql_unset);

		$businessname= $rs_unset["businessname"];
?>
				<tr bgcolor=#E8E7E7>
					<td>
						<font color=red>
							<b><?php echo $businessname; ?> inventory unsubmitted for <?php echo $udate; ?>.</b>
						</font>
					</td>
				</tr>
<?php
	}

?>
				<tr bgcolor=black>
					<td height=2></td>
				</tr>
<?php
	if($security_level>=7){
?>
				<tr bgcolor=#E8E7E7>
					<td>
						<p>
							<br/>
							<form action=copybus.php method='post'>
								<b>COPY FROM</b>
<?php

		echo pick_company($companyid, 'copyfrom');

?>
								<input type=hidden name=username value="<?php echo $user; ?>" />
								<input type=hidden name=password value="<?php echo $pass; ?>" />
								<input type=hidden name=companyid value="<?php echo $companyid; ?>" />
								<b>TO</b>
<?php


		echo pick_company($companyid, 'copyto');

?>
								<input type=submit value='Select Details' />
							</form>
						</p>
					</td>
				</tr>
				<tr bgcolor=black>
					<td height=2></td>
				</tr>

				<tr bgcolor=#E8E7E7>
					<td>
						<p>
							<br/>
							<form action="utility.php?cid=<?php echo $companyid; ?>" method=post>
								<b>VIEW EMAIL DETAILS FOR INVOICE#</b>
								<input type=text name=invoiceid value="<?php echo $invoiceid; ?>" size=10>
								<input type=hidden name=username value="<?php echo $user; ?>" />
								<input type=hidden name=password value="<?php echo $pass; ?>" />
								<input type=hidden name=companyid value="<?php echo $companyid; ?>" />
								<input type=submit value='GO' />
							</form>
						</p>
					</td>
				</tr>
				<tr bgcolor=#E8E7E7>
					<td></td>
				</tr>
<?php
	}



	if ($invoiceid!=""){
?>
				<tr bgcolor=#E8E7E7>
					<td>
						<center>
							<br/>
							<table width=90% bgcolor=white>
<?php
		$sql_invoice = "SELECT * FROM audit_invoice WHERE invoiceid = '$invoiceid' ORDER BY invoice_audit_id DESC";
		$rs_invoice = $db->queryForArray($sql_invoice);
		foreach($rs_invoice as $row){
			$type= $row["type"];
			$email=$row["email"];
			$userid=$row["userid"];
			$time=$row["time"];

			if ($type==0){
?>
								<tr>
									<td width=20% valign=top><?php echo $time; ?></td>
									<td valign=top>
										Invoice# <a style="<?php echo $style; ?>" href="saveinvoice.php?bid=<?php
											echo $businessid; ?>&amp;cid=<?php
											echo $companyid; ?>&amp;invoiceid=<?php
											echo $invoiceid; ?>"
										><font color=blue><?php echo $invoiceid; ?></font></a>
										emailed to <?php echo $email; ?> by <?php echo $userid; ?>.
									</td>
								</tr>
<?php
			}
			elseif ($type==1){
?>
								<tr>
									<td width=20% valign=top><?php echo $time; ?></td>
									<td valign=top>
										<font color=green>
											Invoice# <a style="<?php echo $style; ?>" href="saveinvoice.php?bid=<?php
												echo $businessid; ?>&amp;cid=<?php
												echo $companyid; ?>&amp;invoiceid=<?php
												echo $invoiceid; ?>"
											><font color=blue><?php echo $invoiceid; ?></font></a>
											viewed by account: <?php echo $userid; ?>.
										</font>
									</td>
								</tr>
<?php
			}
		}
?>
							</table>
						</center>
					</td>
				</tr>
				<tr bgcolor=#E8E7E7>
					<td height=2><br /></td>
				</tr>
<?php
	}
//		</td></tr>
?>

				<tr bgcolor=#E8E7E7>
					<td>
						<form ACTION='utility.php?cid=$companyid' method='post'>
							<div>
								<br/>
								<input type=hidden name=username value="<?php echo $user; ?>" />
								<input type=hidden name=password value="<?php echo $pass; ?>" />
								<input type=hidden name=companyid value="<?php echo $companyid; ?>" />
								<input type=hidden name=viewdetail value=2 />
								<b>VIEW UNREAD EMAILED INVOICES FROM</b>
								<SCRIPT LANGUAGE='JavaScript' ID='js19'>
									var cal19 = new CalendarPopup('testdiv1');
									cal19.setCssPrefix('TEST');
								</SCRIPT>
								<INPUT TYPE=text NAME=date1 VALUE='<?php echo $date1; ?>' SIZE=8 />*
								<A href="javascript:void();"
									onClick="cal19.select(document.forms[5].date1,'anchor19','yyyy-MM-dd'); return false;"
									TITLE="cal19.select(document.forms[5].date1,'anchor1x','yyyy-MM-dd'); return false;"
									NAME='anchor19' ID='anchor19'
								><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A>
								<b>TO</b>
								<SCRIPT LANGUAGE='JavaScript' ID='js20'>
									var cal20 = new CalendarPopup('testdiv1');
									cal20.setCssPrefix('TEST');
								</SCRIPT>
								<INPUT TYPE=text NAME=date2 VALUE='<?php echo $date2; ?>' SIZE=8 />*
								<A href="javascript:void();"
									onClick="cal20.select(document.forms[5].date2,'anchor20','yyyy-MM-dd'); return false;"
									TITLE="cal19.select(document.forms[5].date2,'anchor1x','yyyy-MM-dd'); return false;"
									NAME='anchor20' ID='anchor20'
								><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A>
								<input type=submit value='GO'>
								<br/>
								<font size=2>*Date of invoice</font>
							</div>
						</form>
					</td>
				</tr>

<?php
	///////

	if ($viewdetail=="2"){
		$lastbusinessname="";
?>
				<tr bgcolor=#E8E7E7>
					<td>
						<center>
							<table width=90% bgcolor=white>
<?php


		$query = "SELECT * FROM invoice WHERE companyid = '$companyid' AND date >= '$date1' AND date <= '$date2' ORDER BY businessid, date";
		$result = mysql_query($query);
		$num=@mysql_numrows($result);


		$num--;
		While($num>=0){

			$invoiceid=@mysql_result($result,$num,"invoiceid");
			$businessid=@mysql_result($result,$num,"businessid");
			$accountid=@mysql_result($result,$num,"accountid");


			$query2 = "SELECT businessname FROM business WHERE businessid = '$businessid'";
			$result2 = mysql_query($query2);


			$businessname=@mysql_result($result2,0,"businessname");


			$query2 = "SELECT name FROM accounts WHERE accountid = '$accountid'";
			$result2 = mysql_query($query2);


			$acctname=@mysql_result($result2,0,"name");


			$query2 = "SELECT * FROM audit_invoice WHERE invoiceid = '$invoiceid' AND type = '0' ORDER BY invoice_audit_id DESC";
			$result2 = mysql_query($query2);
			$num2=@mysql_numrows($result2);


			$query3 = "SELECT * FROM audit_invoice WHERE invoiceid = '$invoiceid' AND type = '1' ORDER BY invoice_audit_id DESC";
			$result3 = mysql_query($query3);
			$num3=@mysql_numrows($result3);

			if ($num2!=0&&$num3==0){
				if ($lastbusinessname!=$businessname){
?>
								<tr>
									<td colspan=2>
										<font color=blue><?php echo $businessname; ?></font>
									</td>
								</tr>
								<tr height=1 bgcolor=black>
									<td colspan=2></td>
								</tr>
<?php
				}
				$num2--;
				while ($num2>=0){
					$type=@mysql_result($result2,$num2,"type");
					$email=@mysql_result($result2,$num2,"email");
					$userid=@mysql_result($result2,$num2,"userid");
					$time=@mysql_result($result2,$num2,"time");

?>
								<tr>
									<td width=20% valign=top><?php echo $time; ?></td>
									<td valign=top>
										Invoice# <a style="<?php echo $style; ?>" href="saveinvoice.php?bid=<?php
											echo $businessid; ?>&amp;cid=<?php
											echo $companyid; ?>&amp;invoiceid=<?php
											echo $invoiceid; ?>"
										><font color=blue><?php echo $invoiceid; ?></font></a>
										emailed to <?php echo $email; ?>
										(<?php echo $acctname; ?>)
										by <?php echo $userid; ?>.
									</td>
								</tr>
<?php
					$num2--;
				}
				$lastbusinessname=$businessname;
			}
			$num--;
		}
?>
							</table>
						</center>
					</td>
				</tr>
				<tr bgcolor=#E8E7E7 height=20>
					<td></td>
				</tr>
<?php

	}

	//////
?>
				<tr bgcolor=black>
					<td height=2></td>
				</tr>
<?php
	//////PAYROLL////////////////////////////////////////////////
	if($security_level>=8){
		$sql_payroll = "SELECT * FROM payroll_date WHERE companyid = '$companyid' ORDER BY date DESC LIMIT 0,1";
		$rs_payroll = $db->queryForArray($sql_payroll);

		$lastimport= $rs_payroll["date"];
		$nextimport=$lastimport;
		for($counter=1;$counter<=14;$counter++){$nextimport=nextday($nextimport);}
		if($lastimport==""){$nextimport=date("Y-m-d");$lastimport="Never";}

?>
				<tr bgcolor=#E8E7E7>
					<td>
						<font color=red>
							Last Payroll ended <?php echo $lastimport; ?>.
							Next Payroll import should be <?php echo $nextimport; ?>.
						</font>
					</td>
				</tr>
				<tr bgcolor=#E8E7E7>
					<td>
						<form action=payroll.php enctype='multipart/form-data' method=post>
							<a name='bottom'></a>
							<p>
								<br/>
								<input type=hidden name=companyid value="<?php echo $companyid; ?>" />
								<input type=hidden name=password value="<?php echo $pass; ?>" />
								<input type=hidden name=username value="<?php echo $user; ?>" />
								<b>UPLOAD PAYROLL FILE:</b>
								<input type=file name=uploadedfile size=50>
								<SCRIPT LANGUAGE='JavaScript' ID='js21'>
									var cal21 = new CalendarPopup('testdiv1');
									cal21.setCssPrefix('TEST');
								</SCRIPT>
								<INPUT TYPE=text NAME=date1 VALUE='<?php echo $nextimport; ?>' SIZE=8 />*
								<A HREF="javascript:void();"
									onClick="cal21.select(document.forms[6].date1,'anchor21','yyyy-MM-dd'); return false;"
									TITLE="cal21.select(document.forms[6].date1,'anchor1x','yyyy-MM-dd'); return false;"
									NAME='anchor21' ID='anchor21'
								><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A>
								<input type=submit value='Upload' />
								<br/>
								<font size=2>*Pay Period End Date.</font>
							</p>
						</form>
					</td>
				</tr>
				<tr bgcolor=#E8E7E7>
					<td>
						<form action=payrollterm.php enctype='multipart/form-data' method=post>
							<div>
								<input type=hidden name=companyid value="<?php echo $companyid; ?>" />
								<input type=hidden name=password value="<?php echo $pass; ?>" />
								<input type=hidden name=username value="<?php echo $user; ?>" />
								<b>UPLOAD TERMED EMPLOYEE FILE:</b>
								<input type=file name=uploadedfile size=50 />
								<input type=submit value='GO' />
							</div>
						</form>
					</td>
				</tr>
				<tr bgcolor=black>
					<td height=2></td>
				</tr>
<?php
	}
	/////////////UPLOAD AGING AR
?>
				<tr bgcolor=#E8E7E7>
					<td>
						<p>
							<br>
							<form action="/ta/utility/ar_outstanding.php" target='_blank' enctype='multipart/form-data' method=post>
								<input type=hidden name=companyid value="<?php echo $companyid; ?>" />
								<b>UPLOAD AGING AR:</b>
								<input type=file name=uploadedfile size=50 />
								<input type=submit value='GO' />
								<br/>
								<font size=2>*You must upload AR for the entire company, not just a single unit.</font>
							</form>
						</p>
					</td>
				</tr>
				<tr bgcolor=black>
					<td height=2></td>
				</tr>
<?php

	/////////////UPLOAD MEI SALES
?>
				<tr valign=bottom bgcolor=#E8E7E7>
					<td colspan=2>
						<FORM ACTION='/ta/utility/import_mei_sales.php' method='post' enctype='multipart/form-data' target='_blank'>
							<input type=hidden name=companyid value="<?php echo $companyid; ?>" />
							<table width=100%>
								<tr bgcolor=#E8E7E7>
									<td>
										<p>
											<br />
											<b>UPLOAD:</b>
											<select name="what"><option value="1">MEI Sales</option><option value="2">Cashless</option><option value="3">FV/Subsidy</option><option value="4">CSV</option><option value="5">CK Sales</option></select>
											<input type=file name=uploadedfile size=50 />
											<SCRIPT LANGUAGE='JavaScript' ID='js48'>
												var cal48 = new CalendarPopup('testdiv1');
												cal48.setCssPrefix('TEST');
											</SCRIPT>
											<A HREF="javascript:void();"
												onClick="cal48.select(document.forms[9].date1,'anchor48','yyyy-MM-dd'); return false;"
												TITLE="cal48.select(document.forms[9].date1,'anchor1x','yyyy-MM-dd'); return false;"
												NAME='anchor48' ID='anchor48'
											><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A>
											<INPUT TYPE=text NAME=date1 VALUE='<?php echo $date1; ?>' SIZE=8 />
											<b>AND</b>
											<SCRIPT LANGUAGE='JavaScript' ID='js49'>
												var cal49 = new CalendarPopup('testdiv1');
												cal49.setCssPrefix('TEST');
											</SCRIPT>
											<A HREF="javascript:void();"
												onClick="cal49.select(document.forms[9].date2,'anchor49','yyyy-MM-dd'); return false;"
												TITLE="cal49.select(document.forms[9].date2,'anchor1x','yyyy-MM-dd'); return false;"
												NAME='anchor49' ID='anchor49'
											><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A>
											<INPUT TYPE=text NAME=date2 VALUE='<?php echo $date2; ?>' SIZE=8 />
											<b>FOR</b>
<?php


	echo pick_company($companyid);


?>
											<INPUT TYPE=submit VALUE='GO' />
										</p>
									</td>
									<td align=right></td>
								</tr>
							</table>
						</FORM>
					</td>
				</tr>
				<tr bgcolor=black>
					<td height=2></td>
				</tr>

<?php
	/////////////UPLOAD PAYROLL CHANGES
?>
				<tr valign=bottom bgcolor=#E8E7E7>
					<td colspan=2>
						<FORM ACTION='utility/import_payroll.php' method='post' enctype='multipart/form-data' target='_blank'>
							<input type=hidden name=companyid value=$companyid>
							<table width=100%>
								<tr bgcolor=#E8E7E7>
									<td>
										<p>
											<br/>
											<b>UPLOAD PAYROLL CHANGES:</b>
											<input type=file name=uploadedfile size=50 />
											<INPUT TYPE=submit VALUE='GO' />
										</p>
									</td>
									<td align=right></td>
								</tr>
							</table>
						</FORM>
					</td>
				</tr>
				<tr bgcolor=black>
					<td height=2></td>
				</tr>
<?php

        ////////////////////////////////
        /////Import GL for vending//////
        ////////////////////////////////

		if ( $import_bid > 0 ) {
			$bgcolor = "#FFFF99";
		}
		else {
			$bgcolor = "#E8E7E7";
		}

?>



<?php
		$query = sprintf( '
				SELECT businessid
					,businessname
				FROM business
				WHERE companyid = %s
					AND import_pl = 1
			'
			,intval( $companyid )
		);
		$result = Treat_DB_ProxyOld::query($query);
		
		$import_gl_businesses = array(
			array(
				'businessid'   => 0,
				'businessname' => 'Please Choose...',
				'selected'     => '',
			),
		);
		while ( $row = mysql_fetch_array($result) ) {
			$tmp = array(
				'businessid'   => $row['businessid'],
				'businessname' => $row['businessname'],
				'selected'     => '',
			);
			if ( $import_bid == $row['businessid'] ) {
				$tmp['selected'] = ' selected="selected"';
			}
			$import_gl_businesses[] = $tmp;
		}
		if ( $import_date != '' ) {
			$date1 = $import_date;
		}
		if ( $page_business->import_pl ) {
			$cs_class = 'import_pl_on';
			$cs_disabled = '';
		}
		else {
			$cs_class = 'import_pl_off';
			$cs_disabled = ' disabled="disabled"';
		}
?>
				<tr class="import_gl <?php
					echo $cs_class;
				?>">
					<td>
						<a name=bottom></a>
						<br />
						<form action="/ta/buscontrolc/csv_import_gl" enctype="multipart/form-data" method="post">
							<div>
								<input type="hidden" name="companyid" value="<?php echo $companyid; ?>"<?php echo $cs_disabled; ?> />
								<b>&nbsp;IMPORT GL: </b> <input type="file" name="uploadedfile" size="50"<?php echo $cs_disabled; ?> />
								<select name="businessid"<?php echo $cs_disabled; ?>>
<?php
		foreach ( $import_gl_businesses as $business ) {
?>
									<option value="<?php echo $business['businessid']; ?>"<?php
										echo $business['selected'];
									?>><?php echo $business['businessname']; ?></option>
<?php
		}
?>
								</select>
								<select name="import_number"<?php echo $cs_disabled; ?>>
									<option value="1" selected="selected">File 1</option>
									<option value="2">File 2</option>
									<option value="3">File 3</option>
									<option value="4">File 4</option>
									<option value="5">File 5</option>
								</select>
								<script type="text/javascript" language="JavaScript" id="js88">
									var cal88 = new CalendarPopup('testdiv1');
									cal88.setCssPrefix('TEST');
								</script>
								<a href="javascript:void();"
									onclick="cal88.select(document.forms[11].date1,'anchor88','yyyy-MM-dd'); return false;"
									title="Choose a Date"
									name="anchor88"
									id="anchor88"
								><img src="calendar.gif" border="0" height="16" width="16" alt="Choose a Date" /></a>
								<input type="text" class="date" name="date1" value="<?php echo $date1; ?>" size="8"<?php echo $cs_disabled; ?> />
								<input type="submit" value="GO"<?php echo $cs_disabled; ?> />
							</div>
						</form>
					</td>
				</tr>
				<tr bgcolor="black">
					<td height="2"></td>
				</tr>



				<tr class="import_budget <?php
					echo $cs_class;
				?>">
					<td>
						<br />
						<form action="/ta/buscontrolc/csv_import_budget" enctype="multipart/form-data" method="post">
							<div>
								<input type="hidden" name="companyid" value="<?php echo $companyid; ?>"<?php echo $cs_disabled; ?> />
								<label for="uploadedfile_ib"
									style="font-weight: bold; padding-left: .5em;"
								>IMPORT Budget:</label>
								<input type="file" name="uploadedfile" id="uploadedfile_ib" size="50"<?php echo $cs_disabled; ?> />
								<select name="businessid"<?php echo $cs_disabled; ?>>
<?php
		foreach ( $import_gl_businesses as $business ) {
?>
									<option value="<?php echo $business['businessid']; ?>"<?php
										echo $business['selected'];
									?>><?php echo $business['businessname']; ?></option>
<?php
		}
?>
								</select>
								<script type="text/javascript" language="JavaScript" id="js89">
									var cal89 = new CalendarPopup('testdiv1');
									cal89.setCssPrefix('TEST');
								</script>
								<a href="javascript:void();"
									onclick="cal89.select(document.forms[12].date1,'anchor89','yyyy-MM-dd'); return false;"
									title="Choose a Date"
									name="anchor89"
									id="anchor89"
								><img src="calendar.gif" border="0" height="16" width="16" alt="Choose a Date" /></a>
								<input type="text" class="date" name="date1" value="<?php echo $date1; ?>" size="8"<?php echo $cs_disabled; ?> />
								<input type="submit" value="GO"<?php echo $cs_disabled; ?> />
							</div>
						</form>
					</td>
				</tr>
				<tr bgcolor="black">
					<td height="2"></td>
				</tr>



				<tr class="import_final <?php
					echo $cs_class;
				?>">
					<td>
						<br />
						<form action="/ta/buscontrolc/csv_import_final" enctype="multipart/form-data" method="post">
							<div>
								<input type="hidden" name="companyid" value="<?php echo $companyid; ?>"<?php echo $cs_disabled; ?> />
								<label for="uploadedfile_ib"
								>IMPORT Final:</label>
								<input type="file" name="uploadedfile" id="uploadedfile_ib" size="50"<?php echo $cs_disabled; ?> />
								<select name="businessid"<?php echo $cs_disabled; ?>>
<?php
		foreach ( $import_gl_businesses as $business ) {
?>
									<option value="<?php echo $business['businessid']; ?>"<?php
										echo $business['selected'];
									?>><?php echo $business['businessname']; ?></option>
<?php
		}
?>
								</select>
								<script type="text/javascript" language="JavaScript" id="js90">
									var cal90 = new CalendarPopup('testdiv1');
									cal90.setCssPrefix('TEST');
								</script>
								<a href="javascript:void();"
									onclick="cal90.select(document.forms[13].date1,'anchor90','yyyy-MM-dd'); return false;"
									title="Choose a Date"
									name="anchor90"
									id="anchor90"
								><img src="calendar.gif" border="0" height="16" width="16" alt="Choose a Date" /></a>
								<input type="text" class="date" name="date1" value="<?php echo $date1; ?>" size="8"<?php echo $cs_disabled; ?> />
								<input type="submit" value="GO"<?php echo $cs_disabled; ?> />
							</div>
						</form>
					</td>
				</tr>

				<tr valign=bottom bgcolor=#E8E7E7>
					<td colspan=2>
						<FORM ACTION='/ta/utility/import_mei_sales.php' method='post' enctype='multipart/form-data' target='_blank'>
							<input type=hidden name=companyid value="<?php echo $companyid; ?>" />
						</FORM>
					</td>
				</tr>
				
				<?php
				$today = date("Y-m-d");
				?>

				<tr bgcolor=#E8E7E7>
					<td>
						<form action='utility/import_cc_fees.php' enctype='multipart/form-data' method=post>
							<a name='bottom'></a>
							<p>
								<br/>
								<input type=hidden name=companyid value="<?php echo $companyid; ?>" />
								<b>UPLOAD CREDIT CARD FEES:</b>
								<input type=file name=uploadedfile size=50>
								<INPUT TYPE=text NAME=date1 VALUE='<?php echo $today; ?>' SIZE=8 class="datepicker"/>
								<input type=submit value='GO' />
							</p>
						</form>
					</td>
				</tr>
<?php
	//////////END

?>
			</table>
		</center>
		<div id="testdiv1" style="position:absolute;visibility:hidden;background-color:white;layer-background-color:white;">
		</div>

		<br/>
		<FORM ACTION=businesstrack.php method=post>
			<center>
				<input type=hidden name=username value="<?php echo $user; ?>" />
				<input type=hidden name=password value="<?php echo $pass; ?>" />
				<INPUT TYPE=submit VALUE=' Return to Main Page' />
			</center>
		</FORM>

	</div>
<?php

}
function pick_company($companyid, $name = 'businessid') {

	if($name === 'businessid') {
		static $select;
	}
	if(empty($select)){
		global $db;
		$query = "SELECT * FROM business WHERE companyid = '$companyid' ORDER BY businessname";
		$result = $db->query($query, array(), false);

		$select = '';
		$select .= sprintf('<select name="%s">', $name);
		$select .= '<option value="0">=====Please Choose=====</option>';

		foreach($result as $row) {
			$select .= sprintf('<option value="%s">%s</option>', $row['businessid'], $row['businessname']);

		}

		$select .=  "</select>";
	}

	return $select;
}
google_page_track();
?>
</body>
</html>