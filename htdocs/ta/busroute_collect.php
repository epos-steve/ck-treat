<?php

function dayofweek($date1)
{
	$day=substr($date1,8,2);
	$month=substr($date1,5,2);
	$year=substr($date1,0,4);
	$dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
	return $dayofweek;
}

function money($diff){   
	$findme='.';
	$double='.00';
	$single='0';
	$double2='00';
	$pos1 = strpos($diff, $findme);
	$pos2 = strlen($diff);
	if ($pos1==""){$diff="$diff$double";}
	elseif ($pos2-$pos1==2){$diff="$diff$single";}
	elseif ($pos2-$pos1==1){$diff="$diff$double2";}
	else{}
	$dot = strpos($diff, $findme);
	$diff = substr($diff, 0, $dot+4);
	$diff=round($diff, 2);
	if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
	elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
	$diff = substr($diff, 0, $dot+3);
	$pos1 = strpos($diff, $findme);
	$pos2 = strlen($diff);
	if ($pos1==""){$diff="$diff$double";}
	elseif ($pos2-$pos1==2){$diff="$diff$single";}
	elseif ($pos2-$pos1==1){$diff="$diff$double2";}
	else{}
	return $diff;
}

function nextday($date2)
{
	$day=substr($date2,8,2);
	$month=substr($date2,5,2);
	$year=substr($date2,0,4);
	$leap = date("L");

	if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
	{
		if ($month == "01"){$month="02";}
		elseif ($month == "03"){$month="04";}
		elseif ($month == "05"){$month="06";}
		elseif ($month == "07"){$month="08";}
		elseif ($month == "08"){$month="09";}
		elseif ($month == "10"){$month="11";}
		$day='01';    
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($month=='02' && $day == '29' && $leap == '1')
	{
		$month='03';
		$day='01';
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($month=='02' && $day == '28' && $leap == '0')
	{
		$month='03';
		$day='01';
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
	{
		if ($month == "04"){$month="05";}
		if ($month == "06"){$month="07";}
		if ($month == "09"){$month="10";}
		if ($month == "11"){$month="12";}
		$day='01';
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($month=='12' && $day=='31')
	{
		$day='01';
		$month='01';
		$year++;
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else
	{
		$day=$day+1;
		if ($day<10){$day="0$day";}
		$tomorrow="$year-$month-$day";
		return $tomorrow;
	}
}

function prevday($date2) {
	$day=substr($date2,8,2);
	$month=substr($date2,5,2);
	$year=substr($date2,0,4);
	$day=$day-1;
	$leap = date("L");

	if ($day <= 0)
	{
		if ($month == 01)
		{
			$month = '12';
			$year--;
			$day=$day+31;
			$yesterday = "$year-$month-$day";
			return $yesterday;
		}
		else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
		{
			$month--;
			if ($month < 10)
			{
				$month="0$month";
			}
			$day=$day+31;
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
		else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
		{
			$month--;
			if ($month < 10)
			{
				$month="0$month";
			}
			$day=$day+30;
			$yesterday="$year-$month-$day";
			return $yesterday;
		}

		elseif ($leap==1&&$month=='03')
		{
			$day=$day+29;
			$month='02';
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
		else
		{
			$day=$day+28;
			$month='02';
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
	}
	else
	{
		if ($day < 10)
		{
			$day="0$day";
		}
		$yesterday="$year-$month-$day";
		return $yesterday;
	}
}

function futureday($num) {
	$day = date("d");
	$year = date("Y");
	$month = date("m");
	$leap = date("L");
	$day=$day+$num;

	if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
	{
		if ($month == "03"){$month="04";}
		if ($month == "05"){$month="06";}
		if ($month == "07"){$month="08";}
		if ($month == "08"){$month="09";}
		$day=$day-31;  
		if ($day<10){$day="0$day";} 
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($month=='02' && $leap == '1' && $day >= '29')
	{
		$month='03';
		$day=$day-29;
		if ($day<10){$day="0$day";} 
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($month=='02' && $leap == '0' && $day >= '28')
	{
		$month='03';
		$day=$day-28;
		if ($day<10){$day="0$day";} 
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
	{
		if ($month == "04"){$month="05";}
		if ($month == "06"){$month="07";}
		if ($month == "09"){$month="10";}
		if ($month == "11"){$month="12";}
		$day=$day-30;
		if ($day<10){$day="0$day";} 
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($month==12 && $day>=32)
	{
		$day=$day-31;
		if ($day<10){$day="0$day";} 
		$month='01';
		$year++;
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else
	{
		if ($day<10){$day="0$day";}
		$tomorrow="$year-$month-$day";
		return $tomorrow;
	}
}

function pastday($num) {
	$day = date("d");
	$day=$day-$num;
	if ($day <= 0)
	{
		$year = date("Y");
		$month = date("m");
		if ($month == 01)
		{
			$month = '12';
			$year--;
			$day=$day+31;
			$yesterday = "$year-$month-$day";
			return $yesterday;
		}
		else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
		{
			$month--;
			if ($month < 10)
			{
				$month="0$month";
			}
			$day=$day+31;
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
		else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
		{
			$month--;
			if ($month < 10)
			{
				$month="0$month";
			}
			$day=$day+30;
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
		else
		{
			$day=$day+28;
			$month='02';
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
	}
	else
	{
		if ($day < 10)
		{
			$day="0$day";
		}
		$year=date("Y");
		$month=date("m");
		$yesterday="$year-$month-$day";
		return $yesterday;
	}
}

if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
//include_once( 'db.php' );
require_once( DOC_ROOT . '/bootstrap.php' );
$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$month2 = date("n");
$today = "$year-$month-$day";
$todayname = date("l");

/*$user = $_COOKIE['usercook'];
$pass = $_COOKIE['passcook'];
$view = $_COOKIE['viewcook'];
$sort_ap = $_COOKIE['sort_ap'];
$subtotal = $_COOKIE['subtotal'];
$date6 = $_COOKIE['date6cook'];
$autosave = $_GET['autosave'];*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook');
$view = \EE\Controller\Base::getSessionCookieVariable('viewcook');
$sort_ap = \EE\Controller\Base::getSessionCookieVariable('sort_ap');
$subtotal = \EE\Controller\Base::getSessionCookieVariable('subtotal');
$date6 = \EE\Controller\Base::getSessionCookieVariable('date6cook');
$autosave = \EE\Controller\Base::getGetVariable('autosave');

//if($autosave==""){$autosave = $_COOKIE['autosave'];}
if($autosave==""){$autosave = \EE\Controller\Base::getSessionCookieVariable('autosave');}

/*$businessid = $_GET['bid'];
$companyid = $_GET['cid'];
$audit = $_GET['audit'];*/

$businessid = \EE\Controller\Base::getGetVariable('bid');
$companyid = \EE\Controller\Base::getGetVariable('cid');
$audit = \EE\Controller\Base::getGetVariable('audit');

if ($businessid==""||$companyid==""){
	/*$businessid = $_POST['businessid'];
	$companyid = $_POST['companyid'];
	$date6 = $_POST['date6'];*/

	$businessid = \EE\Controller\Base::getPostVariable('businessid');
	$companyid = \EE\Controller\Base::getPostVariable('companyid');
	$date6 = \EE\Controller\Base::getPostVariable('date6');
}

if ($date6==""){$date6=$today;}
if ($autosave==""){$autosave=1;}

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query( $query );
$num = Treat_DB_ProxyOld::mysql_numrows( $result );
//mysql_close();

$security_level = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'security_level' );
$mysecurity = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'security' );
$bid = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'businessid' );
$cid = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'companyid' );
$bid2 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid2' );
$bid3 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid3' );
$bid4 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid4' );
$bid5 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid5' );
$bid6 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid6' );
$bid7 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid7' );
$bid8 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid8' );
$bid9 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid9' );
$bid10 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid10' );
$loginid = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'loginid' );

$pr1 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'payroll' );
$pr2 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr2' );
$pr3 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr3' );
$pr4 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr4' );
$pr5 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr5' );
$pr6 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr6' );
$pr7 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr7' );
$pr8 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr8' );
$pr9 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr9' );
$pr10 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr10' );

if ($num != 1 || ($security_level==1 && ($bid != $businessid || $cid != $companyid)) || $user == "" || $pass == "")
{
	echo "<center><h3>Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM company WHERE companyid = '$companyid'";
	$result = Treat_DB_ProxyOld::query( $query );
	//mysql_close();

	$companyname = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'companyname' );
	$com_logo=@Treat_DB_ProxyOld::mysql_result($result,0,"logo");

	if($com_logo == ""){$com_logo = "talogo.gif";}

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM security WHERE securityid = '$mysecurity'";
	$result = Treat_DB_ProxyOld::query( $query );
	//mysql_close();

	$sec_bus_sales = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_sales' );
	$sec_bus_invoice = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_invoice' );
	$sec_bus_order = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_order' );
	$sec_bus_payable = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_payable' );
	$sec_bus_inventor1 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_inventor1' );
	$sec_bus_control = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_control' );
	$sec_bus_menu = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_menu' );
	$sec_bus_order_setup = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_order_setup' );
	$sec_bus_request = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_request' );
	$sec_route_collection = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'route_collection' );
	$sec_route_labor = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'route_labor' );
	$sec_route_detail = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'route_detail' );
	$sec_route_machine = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'route_machine' );
	$sec_bus_labor = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_labor' );
	$sec_bus_budget = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_budget' );

	if($sec_route_machine > 0 && $sec_route_collection < 1 && $sec_route_labor < 1 && $sec_route_detail < .5){
		$location = "busroute_machine.php?bid=$businessid&cid=$companyid";
		header('Location: ./' . $location);
	}
	elseif ( $sec_route_collection < 1 && $sec_route_labor < 1 && $sec_route_detail < .5 ) {
		$location = 'businesstrack.php';
		header('Location: ./' . $location);
	}
	elseif ( $sec_route_collection < 1 && $sec_route_labor > 0 ) {
		$location = "busroute_labor.php?bid=$businessid&cid=$companyid";
		header('Location: ./' . $location);
	}
	elseif ( $sec_route_collection < 1 && $sec_route_labor < 1 && $sec_route_detail > 0 ) {
		$location = "busroute_detail.php?bid=$businessid&cid=$companyid";
		header('Location: ./' . $location);
	}

	$dayname = dayofweek( $date6 );
	setcookie( 'date6cook', $date6 );
	setcookie( 'autosave', $autosave );

?>

<html>
<head>

<?php
if ( $autosave == 0 ) {
	echo "<script type='text/javascript' language='javascript'>";
	echo "function reFresh() {";
		echo "document.form.submit();";
	echo "}";
	echo "window.setInterval('reFresh()',185000);";
	echo "</script>";
}
?>

<script language="JavaScript">
TargetDate = new Date().valueOf() + 3*60000; 
BackColor = "red";
ForeColor = "white";
CountActive = true;
CountStepper = -1;
LeadingZero = true;
DisplayFormat = "%%M%%:%%S%%";
FinishMessage = " SAVING...";
</script>


<script language="JavaScript" src="CalendarPopup.js"></script>

<script language="JavaScript">document.write(getCalendarStyles());</script>

<style type="text/css">
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation
			{
			background-color:#6677DD;
			text-align:center;
			vertical-align:center;
			text-decoration:none;
			color:#FFFFFF;
			font-weight:bold;
			}
	.TESTcpDayColumnHeader,
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation,
	.TESTcpCurrentMonthDate,
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDate,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDate,
	.TESTcpCurrentDateDisabled,
	.TESTcpTodayText,
	.TESTcpTodayTextDisabled,
	.TESTcpText
			{
			font-family:arial;
			font-size:8pt;
			}
	TD.TESTcpDayColumnHeader
			{
			text-align:right;
			border:solid thin #6677DD;
			border-width:0 0 1 0;
			}
	.TESTcpCurrentMonthDate,
	.TESTcpOtherMonthDate,
	.TESTcpCurrentDate
			{
			text-align:right;
			text-decoration:none;
			}
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDateDisabled
			{
			color:#D0D0D0;
			text-align:right;
			text-decoration:line-through;
			}
	.TESTcpCurrentMonthDate
			{
			color:#6677DD;
			font-weight:bold;
			}
	.TESTcpCurrentDate
			{
			color: #FFFFFF;
			font-weight:bold;
			}
	.TESTcpOtherMonthDate
			{
			color:#808080;
			}
	TD.TESTcpCurrentDate
			{
			color:#FFFFFF;
			background-color: #6677DD;
			border-width:1;
			border:solid thin #000000;
			}
	TD.TESTcpCurrentDateDisabled
			{
			border-width:1;
			border:solid thin #FFAAAA;
			}
	TD.TESTcpTodayText,
	TD.TESTcpTodayTextDisabled
			{
			border:solid thin #6677DD;
			border-width:1 0 0 0;
			}
	A.TESTcpTodayText,
	SPAN.TESTcpTodayTextDisabled
			{
			height:20px;
			}
	A.TESTcpTodayText
			{
			color:#6677DD;
			font-weight:bold;
			}
	SPAN.TESTcpTodayTextDisabled
			{
			color:#D0D0D0;
			}
	.TESTcpBorder
			{
			border:solid thin #6677DD;
			}
</style>

<script language="JavaScript">
<!-- Web Site:  http://dynamicdrive.com -->

<!-- This script and many more are available free online at -->
<!-- The JavaScript Source!! http://javascript.internet.com -->

<!-- Begin
function disableForm(theform) {
if (document.all || document.getElementById) {
for (i = 0; i < theform.length; i++) {
var tempobj = theform.elements[i];
if (tempobj.type.toLowerCase() == "submit" || tempobj.type.toLowerCase() == "reset")
tempobj.disabled = true;
}

}
else {
alert("The form has been submitted.  But, since you're not using IE 4+ or NS 6, the submit button was not disabled on form submission.");
return false;
   }
}
//  End -->
</script>

<script language=javascript><!--
function confirm1(){return confirm('MAKE SURE YOU HAVE SAVED YOUR WORK!!! Press "OK" if you are sure you want to SUBMIT. YOU WILL NOT BE ABLE TO EDIT THIS SHEET AGAIN ONCE SUBMITTED.');}
// --></script>

<script language=javascript><!--
function autooff(){return confirm('MAKE SURE YOU HAVE SAVED YOUR WORK!!! Press "Cancel" if not.');}
// --></script>

<script type="text/javascript" language="javascript">
function checkKey(){

var key = event.keyCode;
var DaIndexId = event.srcElement.IndexId-0
if(key==38) {
document.getElementByIndex(DaIndexId-1).focus();
return false;
}
else if(key==40) {
document.getElementByIndex(DaIndexId+1).focus();
return false;
}
else if(key==13) {
document.getElementByIndex(DaIndexId+1).focus();
return false;
}
else if(key==37) {
document.getElementByIndex(DaIndexId-1000).focus();
return false;
}
else if(key==39) {
document.getElementByIndex(DaIndexId+1000).focus();
return false;
}
return true
} // end checkKey function
document.getElementByIndex=function(){
for(var i=0;i<document.all.length;i++)
if(document.all[i].IndexId==arguments[0])
return document.all[i]
return null
}
</script>

<script language="JavaScript" 
   type="text/JavaScript">
function changePage(newLoc)
 {
   nextPage = newLoc.options[newLoc.selectedIndex].value
		
   if (nextPage != "")
   {
      document.location.href = nextPage
   }
 }
</script>

<script type="text/javascript" src="javascripts/prototype.js"></script>

<script language="JavaScript" 
   type="text/JavaScript">
window.onload = function(){
   new headerScroll('test',{head: 'header'});
} 
</script>

</head>

<?php

	echo "<body>";

	echo "<div style=border:50px solid red;padding:10px;>";
	echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%><tr><td colspan=2>";
	echo "<a href=businesstrack.php><img src=logo.jpg border=0 height=43 width=205></a><p></td></tr>";

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM business WHERE companyid = '$companyid' AND businessid = '$businessid'";
	$result = Treat_DB_ProxyOld::query( $query );
	//mysql_close();

	$businessname = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'businessname' );
	$businessid = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'businessid' );
	$street = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'street' );
	$city = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'city' );
	$state = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'state' );
	$zip = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'zip' );
	$phone = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'phone' );
	$districtid = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'districtid' );
	$bus_unit = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'unit' );

	//echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";
	echo "<tr bgcolor=#CCCCFF><td width=1%><img src=logo/$com_logo></td><td><font size=4><b>$businessname #$bus_unit</b></font><br>$street<br>$city, $state $zip<br>$phone</td>";
	echo "<td align=right valign=top><br>";
	echo "<FORM ACTION='editbus.php' method=post name='store'>";
	echo "<input type=hidden name=username value=$user>";
	echo "<input type=hidden name=password value=$pass>";
	echo "<input type=hidden name=businessid value=$businessid>";
	echo "<input type=hidden name=companyid value=$companyid>";
	echo "<INPUT TYPE=submit VALUE=' Store Setup '> ";
	echo "</form></td></tr>";
	//echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";
	
	echo "<tr><td colspan=3><font color=#999999>";
	if ($sec_bus_sales>0){echo "<a href=busdetail.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Sales</font></a>";}
	if ($sec_bus_invoice>0){echo " | <a href=businvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Invoicing</font></a>";}
	if ($sec_bus_order>0){echo " | <a href=buscatertrack.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Orders</font></a>";}
	if ($sec_bus_payable>0){echo " | <a href=busapinvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Payables</font></a>";}
	if ($sec_bus_inventor1>0){echo " | <a href=businventor.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Inventory</font></a>";}
	if ($sec_bus_control>0){echo " | <a href=buscontrol.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Control Sheet</font></a>";}
	if ($sec_bus_menu>0){echo " | <a href=buscatermenu.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Menus</font></a>";}
	if ($sec_bus_order_setup>0){echo " | <a href=buscaterroom.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Order Setup</font></a>";}
	if ($sec_bus_request>0){echo " | <a href=busrequest.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Requests</font></a>";}
	if ($sec_route_collection>0||$sec_route_labor>0||$sec_route_detail>0){echo " | <a href=busroute_collect.php?bid=$businessid&cid=$companyid><font color=red onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='red' style='text-decoration:none'>Route Collections</font></a>";}
	if ($sec_bus_labor>0){echo " | <a href=buslabor.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Labor</font></a>";}
	if ($sec_bus_budget>0||($businessid == $bid && $pr1 == 1)||($businessid == $bid2 && $pr2 == 1)||($businessid == $bid3 && $pr3 == 1)||($businessid == $bid4 && $pr4 == 1)||($businessid == $bid5 && $pr5 == 1)||($businessid == $bid6 && $pr6 == 1)||($businessid == $bid7 && $pr7 == 1)||($businessid == $bid8 && $pr8 == 1)||($businessid == $bid9 && $pr9 == 1)||($businessid == $bid10 && $pr10 == 1)){echo " | <a href=busbudget.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Budget</font></a>";}
	echo "</td></tr>";   

	echo "</table></center><p>";

	if ( $security_level > 0 ) {
		echo "<p><center><table width=90%><tr><td width=50%><form action=busrequest.php method=post>";
		echo "<select name=gobus onChange='changePage(this.form.gobus)'>";

		$districtquery="";
		if ( $security_level > 2 && $security_level < 7 ) {
			//////mysql_connect($dbhost,$username,$password);
			//////@mysql_select_db($database) or die( "Unable to select database");
			$query = "SELECT * FROM login_district WHERE loginid = '$loginid'";
			$result = Treat_DB_ProxyOld::query( $query );
			$num = Treat_DB_ProxyOld::mysql_numrows( $result );
			//////mysql_close();

			$num--;
			while ( $num >= 0 ) {
				$districtid = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'districtid' );
				if($districtquery==""){$districtquery="districtid = '$districtid'";}
				else{$districtquery="$districtquery OR districtid = '$districtid'";}
				$num--;
			}
		}

		//////mysql_connect($dbhost,$username,$password);
		//////@mysql_select_db($database) or die( "Unable to select database");
		if($security_level>6){$query = "SELECT * FROM business WHERE companyid = '$companyid' ORDER BY businessname DESC";}
		elseif($security_level==2||$security_level==1){$query = "SELECT * FROM business WHERE companyid = '$companyid' AND (businessid = '$bid' OR businessid = '$bid2' OR businessid = '$bid3' OR businessid = '$bid4' OR businessid = '$bid5' OR businessid = '$bid6' OR businessid = '$bid7' OR businessid = '$bid8' OR businessid = '$bid9' OR businessid = '$bid10') ORDER BY businessname DESC";}
		elseif($security_level>2&&$security_level<7){$query = "SELECT * FROM business WHERE companyid = '$companyid' AND ($districtquery) ORDER BY businessname DESC";}
		$result = Treat_DB_ProxyOld::query( $query );
		$num = Treat_DB_ProxyOld::mysql_numrows( $result );
		//////mysql_close();

		$num--;
		while ( $num >= 0 ) {
			$businessname = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'businessname' );
			$busid = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'businessid' );

			if ($busid==$businessid){$show="SELECTED";}
			else{$show="";}

			echo "<option value=busroute_collect.php?bid=$busid&cid=$companyid $show>$businessname</option>";
			$num--;
		}
		
		echo "</select></td><td></form></td><td width=50% align=right></td></tr></table></center><p>";
	}

	$security_level=$sec_route_collection;

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM vend_collection WHERE businessid = '$businessid' AND date = '$date6'";
	$result = Treat_DB_ProxyOld::query( $query );
	$num = Treat_DB_ProxyOld::mysql_numrows( $result );
	//mysql_close();

	$vc_nickel_dime = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'nickel_dime' );
	$vc_coin = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'coin' );
	$vc_currency = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'currency' );
	$vc_checks = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'checks' );
	$vc_changers_made = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'changers_made' );
	$vc_in_room = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'in_room' );
	$vc_out_room = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'out_room' );
	$vc_escrow_in = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'escrow_in' );
	$vc_escrow_out = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'escrow_out' );
	$vc_brinks = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'brinks' );
	$vc_bal_on_hand = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bal_on_hand' );
	$notes = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'notes' );
	$posted = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'posted' );

	if ($posted==1){$showpost="<font color=red><b>SUBMITTED</b></font>";}
	else{$showpost="";}

	if ($autosave==0&&$posted==0){$showautosave="<a href=busroute_collect.php?bid=$businessid&cid=$companyid&autosave=1 title='Click to Change' style=$style onclick='return autooff()'><font color=blue>Auto-Save On <span style='font-size:20px; font-weight:bold; font-family:arial'><script language=JavaScript src=countdown.js></script></span></font></a>";}
	elseif($autosave==1&&$posted==0){$showautosave="<a href=busroute_collect.php?bid=$businessid&cid=$companyid&autosave=0 title='Click to Change' style=$style><font color=blue>Auto-Save Off</font></a>";}

	echo "<center><table width=90% cellspacing=0 cellpadding=0>";
	echo "<tr><td colspan=7>";
	echo "<table cellspacing=0 cellpadding=0 width=100% id='test'>";

	echo "<tr bgcolor=#CCCCCC valign=top><td width=1% bgcolor=#E8E7E7><img src=leftcrn.jpg height=6 width=6 align=top></td><td bgcolor=#E8E7E7 width=13%><center><a href=busroute_collect.php?cid=$companyid&bid=$businessid style=$style><font color=black>Collection Sheet</a></center></td><td bgcolor=#E8E7E7 width=1% align=right><img src=rightcrn.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1% ><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=13%><center><a href=busroute_balance.php?cid=$companyid&bid=$businessid style=$style style=$style><font color=blue>Balance Sheet</a></center></td><td width=1% align=right><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1%><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=13%><center><a href=busroute_report.php?cid=$companyid&bid=$businessid style=$style><font color=blue>Reporting</a></center></td><td width=1% align=right><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1%><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=13%><center><a href=busroute_labor.php?cid=$companyid&bid=$businessid style=$style><font color=blue>Payroll/Route Linking</a></center></td><td width=1% align=right><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1%><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=13%><center><a href=busroute_detail.php?cid=$companyid&bid=$businessid style=$style><font color=blue>Route Details</a></center></td><td width=1% align=right><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1%><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=13%><center><a href=busroute_machine.php?cid=$companyid&bid=$businessid style=$style><font color=blue>Machine Mgmt</a></center></td><td width=1% align=right><img src=rightcrn2.jpg height=6 width=6 align=top></td><td bgcolor=white width=25%></td></tr>";

	echo "<tr height=2><td colspan=3 bgcolor=#E8E7E7></td><td></td><td colspan=4></td><td colspan=4></td><td colspan=4></td><td colspan=4></td><td colspan=4></td><td colspan=1></td></tr>";

	echo "</table></td></tr>";    

	echo "<tr bgcolor=#E8E7E7><td colspan=6><form action=busroute_collect.php method=post><input type=hidden name=businessid value=$businessid><input type=hidden name=companyid value=$companyid><a name=start><b><font style=background-color:yellow;>$dayname Route Collections</font></b></a> <SCRIPT LANGUAGE='JavaScript' ID='js18'> var cal18 = new CalendarPopup('testdiv1');cal18.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date6 VALUE='$date6' SIZE=8> <A HREF=# onClick=cal18.select(document.forms[2].date6,'anchor18','yyyy-MM-dd'); return false; TITLE=cal18.select(document.forms[2].date6,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor18' ID='anchor18'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A> <input type=submit value='GO'> $showpost</td><td align=right>$showautosave</td></tr>";
	echo "<tr bgcolor=black height=1><td colspan=7></form><form action=savecollection.php method=post onSubmit='return disableForm(this);' name=form><input type=hidden name=businessid value=$businessid><input type=hidden name=companyid value=$companyid><input type=hidden name=date6 value='$date6'></td></tr>";

	$findcounter=1;
	$num=0;
	$newdate=prevday($date6);
	while($num==0&&$findcounter<10){
		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query = "SELECT bal_on_hand FROM vend_collection WHERE businessid = '$businessid' AND date = '$newdate'";
		$result = Treat_DB_ProxyOld::query( $query );
		$num = Treat_DB_ProxyOld::mysql_numrows( $result );
		//mysql_close();

		$balance_forward = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bal_on_hand' );

		$newdate=prevday($newdate);
		$findcounter++;
	}
	$balance_forward=money($balance_forward);
	$balance_forwardshow=number_format($balance_forward,2);

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM vend_locations WHERE unitid = '$businessid'";
	$result = Treat_DB_ProxyOld::query( $query );
	$num = Treat_DB_ProxyOld::mysql_numrows( $result );
	//mysql_close();

	$amount1tot=0;
	$amount2tot=0;
	$amount3tot=0;
	$amount4tot=0;
	$amount5tot=0;

	$counter=1;
	$counter2=1;
	$num--;
	while ( $num >= 0 ) {
		$location_name = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'location_name' );
		$locationid = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'locationid' );

		echo "<tr bgcolor=#CCCCFF><td colspan=4>$location_name</td><td align=right colspan=3>Balance Forward: $$balance_forwardshow</td></tr>";
		echo "<tr bgcolor=#E8E7E7 id='header'><td width=5%><font size=2><b>Route</font>&nbsp;</td><td width=17% align=right><font size=2><b>Route Total</td><td width=17% align=right><font size=2><b>Route Currency</td><td width=17% align=right><font size=2><b>Changer Expected</td><td width=5% align=right><font size=2></td><td width=17% align=right><font size=2><b>Changer Total</td><td width=17% align=right><font size=2><b>Changer Currency</td></tr>";

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query2 = "SELECT * FROM login_route WHERE locationid = '$locationid' AND active = '0' ORDER BY route DESC";
		$result2 = Treat_DB_ProxyOld::query( $query2 );
		$num2 = Treat_DB_ProxyOld::mysql_numrows( $result2 );
		//mysql_close();

		$num2--;
		while ( $num2 >= 0 ) {
			$login_routeid = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'login_routeid' );
			$route = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'route' );

			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query3 = "SELECT * FROM vend_route_collection WHERE login_routeid = '$login_routeid' AND date = '$date6'";
			$result3 = Treat_DB_ProxyOld::query( $query3, true );
			$num3 = Treat_DB_ProxyOld::mysql_numrows( $result3 );
			//mysql_close();

			$amount1 = @Treat_DB_ProxyOld::mysql_result( $result3, 0, 'route_total' );
			$amount2 = @Treat_DB_ProxyOld::mysql_result( $result3, 0, 'route_currency' );
			$amount3 = @Treat_DB_ProxyOld::mysql_result( $result3, 0, 'changer_expected' );
			$amount4 = @Treat_DB_ProxyOld::mysql_result( $result3, 0, 'changer_total' );
			$amount5 = @Treat_DB_ProxyOld::mysql_result( $result3, 0, 'changer_currency' );

			if ($amount1==0){$amount1="";}
			else{$amount1=money($amount1);}
			if ($amount2==0){$amount2="";}
			else{$amount2=money($amount2);}
			if ($amount3==0){$amount3="";}
			else{$amount3=money($amount3);}
			if ($amount4==0){$amount4="";}
			else{$amount4=money($amount4);}
			if ($amount5==0){$amount5="";}
			else{$amount5=money($amount5);}

			$amount1tot = $amount1tot + $amount1;
			$amount2tot = $amount2tot + $amount2;
			$amount3tot = $amount3tot + $amount3;
			$amount4tot = $amount4tot + $amount4;
			$amount5tot = $amount5tot + $amount5;

			$rt = "rt$counter";
			$rc = "rc$counter";
			$ce = "ce$counter";
			$ct = "ct$counter";
			$cc = "cc$counter";

			$thiscount2 = $counter2 + 1000;
			$thiscount3 = $counter2 + 2000;
			$thiscount4 = $counter2 + 3000;
			$thiscount5 = $counter2 + 4000;

			if ($posted==0||$security_level==9){echo "<tr onMouseOver=this.bgColor='#E8E7E7' onMouseOut=this.bgColor='white' id='row$counter'><td><font size=2>$route</td><td align=right><input type=text size=5 name=$rt value='$amount1' onkeydown='return checkKey()' IndexId=$counter2 onfocus=\"row$counter.style.backgroundColor='yellow'\" onblur=\"row$counter.style.backgroundColor=''\"></td><td align=right><input type=text size=5 name=$rc value='$amount2' onkeydown='return checkKey()' IndexId=$thiscount2 onfocus=\"row$counter.style.backgroundColor='yellow'\" onblur=\"row$counter.style.backgroundColor=''\"></td><td align=right><input type=text size=5 name=$ce value='$amount3' onkeydown='return checkKey()' IndexId=$thiscount3 onfocus=\"row$counter.style.backgroundColor='yellow'\" onblur=\"row$counter.style.backgroundColor=''\"></td><td align=right><font size=2>$route</td><td align=right><input type=text size=5 name=$ct value='$amount4' onkeydown='return checkKey()' IndexId=$thiscount4 onfocus=\"row$counter.style.backgroundColor='yellow'\" onblur=\"row$counter.style.backgroundColor=''\"></td><td align=right><input type=text size=5 name=$cc value='$amount5' onkeydown='return checkKey()' IndexId=$thiscount5 onfocus=\"row$counter.style.backgroundColor='yellow'\" onblur=\"row$counter.style.backgroundColor=''\"></td></tr>";}
			else{echo "<tr onMouseOver=this.bgColor='#E8E7E7' onMouseOut=this.bgColor='white'><td><font size=2>$route</td><td align=right>$amount1</td><td align=right>$amount2</td><td align=right>$amount3</td><td align=right><font size=2>$route</td><td align=right>$amount4</td><td align=right>$amount5</td></tr>";}
			echo "<tr bgcolor=#E8E7E7 height=1><td colspan=7></td></tr>";

			$num2--;
			$counter++;
			$counter2++;
		}

		$num--;
	}

	$thiscount5 = $counter2 + 4000;
	$vc_nickel_dime = money( $vc_nickel_dime );
	if ($vc_nickel_dime==0){$vc_nickel_dime="";}
	if ($posted==0||$security_level==9){echo "<tr onMouseOver=this.bgColor='#E8E7E7' onMouseOut=this.bgColor='white'><td><font size=2>Nickels/Dimes</td><td align=right></td><td align=right></td><td align=right></td><td align=right></td><td></td><td align=right><input type=text size=5 name=vc_nickel_dime value='$vc_nickel_dime' onkeydown='return checkKey()' IndexId=$thiscount5  onblur=this.style.backgroundColor='white' onfocus=this.style.backgroundColor='yellow'></td></tr>";}
	else{$vc_nickel_dime=money($vc_nickel_dime);echo "<tr onMouseOver=this.bgColor='#E8E7E7' onMouseOut=this.bgColor='white'><td><font size=2>Nickels/Dimes</td><td align=right></td><td align=right></td><td align=right></td><td align=right></td><td></td><td align=right>$vc_nickel_dime</td></tr>";}
	echo "<tr bgcolor=#E8E7E7 height=1><td colspan=7></td></tr>";

	$amount1tot = money( $amount1tot );
	$amount2tot = money( $amount2tot );
	$amount3tot = money( $amount3tot );
	$amount4tot = money( $amount4tot );
	$amount5tot = money( $amount5tot + $vc_nickel_dime );

	$amount1show = number_format( $amount1tot, 2 );
	$amount2show = number_format( $amount2tot, 2 );
	$amount3show = number_format( $amount3tot, 2 );
	$amount4show = number_format( $amount4tot, 2 );
	$amount5show = number_format( $amount5tot, 2 );

	echo "<tr bgcolor=#E8E7E7><td><font size=2><b><a name=bottom>Totals:</a></b></td><td align=right><font size=2><b>$$amount1show</td><td align=right><font size=2><b>$$amount2show</td><td align=right><font size=2><b>$$amount3show</td><td></td><td align=right><font size=2><b>$$amount4show</td><td align=right><font size=2><b>$$amount5show</td></tr>";
	$totalcollect = money( $amount1tot + $amount4tot );
	$totalcollectshow = number_format( $totalcollect, 2 );
	echo "<tr bgcolor=black height=1><td colspan=7></td></tr>";
	echo "<tr bgcolor=#E8E7E7><td align=right colspan=6><font size=2><b>Total Collections:</td><td align=right><font size=2><b>$$totalcollectshow</td></tr>";
	$totalonhand = money( $totalcollect + $balance_forward );
	$totalonhandshow = number_format( $totalonhand, 2 );
	echo "<tr bgcolor=#E8E7E7><td align=right colspan=6><font size=2><b>Total On Hand:</td><td align=right><font size=2><b>$$totalonhandshow</td></tr>";
	echo "<tr bgcolor=black height=1><td colspan=7></td></tr>";
	$vc_cash_total = money( $vc_coin + $vc_currency + $vc_checks );
	$vc_cash_totalshow = number_format( $vc_cash_total, 2 );
	if($vc_coin==0){$vc_coin="";}
/*
	echo "<tr bgcolor=#E8E7E7><td align=right></td><td align=right><font size=2><b>Deposits:</td><td align=right><font size=2>Coin: <input type=text name=vc_coin value='$vc_coin' size=8 onkeydown='return checkKey()' IndexId=$counter2></td><td align=right></td><td></td><td></td><td></td></tr>";
	$counter2++;
	if($vc_currency==0){$vc_currency="";}
	echo "<tr bgcolor=#E8E7E7><td align=right></td><td align=right></td><td align=right><font size=2>Currency: <input type=text name=vc_currency value='$vc_currency' size=8 onkeydown='return checkKey()' IndexId=$counter2></td><td align=right></td><td></td><td></td><td></td></tr>";
	$counter2++;
	if($vc_checks==0){$vc_checks="";}
	echo "<tr bgcolor=#E8E7E7><td align=right></td><td align=right></td><td align=right><font size=2>Checks: <input type=text name=vc_checks value='$vc_checks' size=8 onkeydown='return checkKey()' IndexId=$counter2></td><td align=right></td><td></td><td align=right><font size=2><b>Total Deposit:</td><td align=right><font size=2><b>$$vc_cash_totalshow</td></tr>";
	$counter2++;
	echo "<tr bgcolor=black height=1><td colspan=7></td></tr>";
*/
///////////////////////////////NEW DEPOSITS 5/20/09
	$query3 = "SELECT * FROM debits WHERE businessid = '$businessid' AND debittype = '3' AND ((is_deleted = '0') OR (is_deleted = '1' AND del_date >= '$date6')) ORDER BY debitname DESC";
	$result3 = Treat_DB_ProxyOld::query( $query3 );
	$num3 = Treat_DB_ProxyOld::mysql_numrows( $result3 );

	$num3--;
	$tempnum = $num3;
	$tempcounter = 1;
	$temptotal = 0; 
	$vc_cash_total = 0;
	while ( $num3 >= 0 ) {
		$debitid = @Treat_DB_ProxyOld::mysql_result( $result3, $num3, 'debitid' );
		$debitname = @Treat_DB_ProxyOld::mysql_result( $result3, $num3, 'debitname' );

		$query4 = "SELECT * FROM debitdetail WHERE debitid = '$debitid' AND date = '$date6'";
		$result4 = Treat_DB_ProxyOld::query( $query4 );

		$amount = @Treat_DB_ProxyOld::mysql_result( $result4, 0, 'amount' );

		$vc_cash_total += $amount;
		$temptotal += $amount;

		if($num3==$tempnum){echo "<tr bgcolor=#E8E7E7><td align=right></td><td align=right><font size=2><b>Deposits:</td><td align=right><font size=2>$debitname: <input type=text name='debit$debitid' value='$amount' size=8 onkeydown='return checkKey()' IndexId=$counter2></td><td align=right></td><td></td><td></td><td></td></tr>";}
		elseif($num3==0){$vc_cash_totalshow=number_format($vc_cash_total,2); echo "<tr bgcolor=#E8E7E7><td align=right></td><td align=right></td><td align=right><font size=2><font size=2>$debitname: <input type=text name='debit$debitid' value='$amount' size=8 onkeydown='return checkKey()' IndexId=$counter2></td><td align=right></td><td></td><td align=right><font size=2><b>Total Deposit:</td><td align=right><font size=2><b>$$vc_cash_totalshow</td></tr>";}
		else{echo "<tr bgcolor=#E8E7E7><td align=right></td><td align=right></td><td align=right><font size=2><font size=2>$debitname: <input type=text name='debit$debitid' value='$amount' size=8 onkeydown='return checkKey()' IndexId=$counter2></td><td align=right></td><td></td><td></td><td></td></tr>";}

		if ( ( $tempcounter == 3 ) || ( $tempcounter != 3 && $num3 == 0 ) ) {
			//$mylen=strlen($temptotal);
			//while($mylen<=15){$showspace.="&nbsp;";$mylen++;}

			$temptotal = number_format( $temptotal, 2 );

			echo "<tr bgcolor=#E8E7E7><td align=right></td><td align=right></td><td align=right><font size=2><font size=2><b> $$temptotal</b></td><td align=right></td><td></td><td></td><td></td></tr>";
			$temptotal = 0;
			$showspace = '';
		}

		$num3--;
		$counter2++;
		$tempcounter++;

	}

	
	$counter2++;

	echo "<tr bgcolor=black height=1><td colspan=7></td></tr>";
//////////////////////////////NEW

	$vc_total_changers = money( ( $vc_changers_made * -1 ) + $vc_in_room - $vc_out_room );
	$vc_total_changersshow = number_format( $vc_total_changers, 2 );
	$vc_changer_balance = money( $totalonhand + $vc_brinks + $vc_escrow_in - $vc_cash_total + $vc_total_changers - $vc_escrow_out );
	$vc_changer_balanceshow = number_format( $vc_changer_balance, 2 );

	if($vc_changers_made==0){$vc_changers_made="";}
	if($vc_in_room==0){$vc_in_room="";}
	if($vc_out_room==0){$vc_out_room="";}

	echo "<tr bgcolor=#E8E7E7><td align=right></td><td align=right><font size=2><b>Changers:</td><td align=right><font size=2>Chgrs Made: <input type=text name=vc_changers_made value='$vc_changers_made' size=8 onkeydown='return checkKey()' IndexId=$counter2></td><td align=right></td><td></td><td align=right><font size=2><b>Total Changers:</td><td align=right><font size=2><b>$$vc_total_changersshow</td></tr>";
	$counter2++;
	echo "<tr bgcolor=#E8E7E7><td align=right></td><td align=right></td><td align=right><font size=2>Add In Room: <input type=text name=vc_in_room value='$vc_in_room' size=8 onkeydown='return checkKey()' IndexId=$counter2></td><td align=right></td><td></td><td></td><td></td></tr>";
	$counter2++;
	echo "<tr bgcolor=#E8E7E7><td align=right></td><td align=right></td><td align=right><font size=2>Out Of Room: <input type=text name=vc_out_room value='$vc_out_room' size=8 onkeydown='return checkKey()' IndexId=$counter2></td><td align=right></td><td></td><td align=right><font size=2><b>Exp Balance:</td><td align=right><font size=2><b>$$vc_changer_balanceshow</td></tr>";
	$counter2++;

	if($vc_escrow_in==0){$vc_escrow_in="";}
	if($vc_escrow_out==0){$vc_escrow_out="";}
	if($vc_brinks==0){$vc_brinks="";}

	echo "<tr bgcolor=#E8E7E7 height=16><td colspan=7></td></tr>";
	echo "<tr bgcolor=#E8E7E7><td align=right></td><td align=right></td><td align=right><font size=2>Escrow In: <input type=text name=vc_escrow_in value='$vc_escrow_in' size=8 onkeydown='return checkKey()' IndexId=$counter2></td><td align=right></td><td align=right></td><td></td><td align=right></td></tr>";
	$counter2++;
	$vc_bal_on_hand = money( $vc_bal_on_hand );
	$vc_bal_on_handshow = number_format( $vc_bal_on_hand, 2 );
	echo "<tr bgcolor=#E8E7E7><td align=right></td><td align=right></td><td align=right><font size=2>Escrow Out: <input type=text name=vc_escrow_out value='$vc_escrow_out' size=8 onkeydown='return checkKey()' IndexId=$counter2></td><td></td><td align=right></td><td align=right><font size=2><b>Act Balance On Hand:</td><td align=right><font size=2><b>$$vc_bal_on_handshow</td></tr>";
	$counter2++;
	$cash_over_short = money( $vc_bal_on_hand - $vc_changer_balance );
	$cash_over_shortshow = number_format( $cash_over_short, 2 );
	echo "<tr bgcolor=#E8E7E7><td align=right></td><td align=right></td><td align=right><font size=2>Brinks: <input type=text name=vc_brinks value='$vc_brinks' size=8 onkeydown='return checkKey()' IndexId=$counter2></td><td align=right></td><td></td><td align=right><font size=2><b>Cash Over/Short:</td><td align=right><font size=2><b>$$cash_over_shortshow</td></tr>";

	echo "<tr bgcolor=black height=1><td colspan=7></td></tr>";
	echo "<tr bgcolor=#E8E7E7 valign=top><td align=right colspan=7><b><font size=2>Notes: </font></b><textarea name=notes rows=5 cols=55>$notes</textarea></td></tr>";

	echo "<tr bgcolor=#CCCCFF height=1><td colspan=7></td></tr>";

	if ($posted==0||$security_level==9){echo "<tr bgcolor=#CCCCFF><td colspan=5><input type=submit value='Save'></td><td></form></td>";}
	else{echo "<tr bgcolor=#CCCCFF><td colspan=5></td><td></form></td>";}

	if ($posted==0){echo "<td align=right><form action=submitroute.php method=post onSubmit='return disableForm(this);' onclick='return confirm1()'><input type=hidden name=businessid value=$businessid><input type=hidden name=companyid value=$companyid><input type=hidden name=date6 value='$date6'><input type=submit value='Submit'></td>";}
	else{echo "<td></td>";}

	echo "</tr>";
	echo "<tr bgcolor=#CCCCFF height=1><td colspan=7></form></td></tr>";

	if ( $audit == 1 ) {
		echo "<tr><td colspan=7><font size=2><b><u><a name=audit>Audit Trail</a></b></td></tr>";

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query3 = "SELECT * FROM audit_collection WHERE businessid = '$businessid' AND date = '$date6' ORDER BY edittime ASC";
		$result3 = Treat_DB_ProxyOld::query( $query3 );
		$num3 = Treat_DB_ProxyOld::mysql_numrows( $result3 );
		//mysql_close();

		$num3--;
		while ( $num3 >= 0 ) {
			$audituser = @Treat_DB_ProxyOld::mysql_result( $result3, $num3, 'user' );
			$edittime = @Treat_DB_ProxyOld::mysql_result( $result3, $num3, 'edittime' );
			$type = @Treat_DB_ProxyOld::mysql_result( $result3, $num3, 'type' );

			if ($type==0){echo "<tr><td colspan=7><font size=2>Collection Sheet Saved $edittime by $audituser</font></td></tr>";}
			elseif ($type==1){echo "<tr><td colspan=7><font size=2>Balance Sheet Saved $edittime by $audituser</font></td></tr>";}
			elseif ($type==3){echo "<tr><td colspan=7><font size=2 color=red>Submitted $edittime by $audituser</font></td></tr>";}
			$num3--;
		}
	}
	else{echo "<tr><td colspan=7><a href=busroute_collect.php?cid=$companyid&bid=$businessid&audit=1#audit style=$style><font size=2 color=blue><b>Audit Trail</b></font></a></td></tr>";}

	echo "</table></center><DIV ID=testdiv1 STYLE=position:absolute;visibility:hidden;background-color:white;layer-background-color:white;></DIV>";

	echo "<br><FORM ACTION=businesstrack.php method=post>";
	echo "<input type=hidden name=username value=$user>";
	echo "<input type=hidden name=password value=$pass>";
	echo "<center><INPUT TYPE=submit VALUE=' Return to Main Page'></FORM></center></body>";
	
	google_page_track();
}
//mysql_close();
?>