<?php

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      elseif ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      elseif ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='31')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+4);
        $diff=round($diff, 2);
        if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
        elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
        $diff = substr($diff, 0, $dot+3);
        return $diff;
}

include("db.php");
$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";

$user=$_COOKIE["usercook"];
$pass=$_COOKIE["passcook"];
$businessid=$_GET['bid'];
$companyid=$_GET['cid'];
$godate=$_GET['date'];
$inv_areaid=$_GET['inv_areaid'];
$alpha=$_GET['alpha'];

mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = mysql_query($query);
$num=mysql_numrows($result);

$security_level=mysql_result($result,0,"security_level");

if ($num != 1)
{
    echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
    echo "<html>";
?>
<head>

<style type="text/css"><!--
	.statusbox {
		font-size: 13px;
		font-family: Monaco, monospace;
		width: 15em;
	}
	ul.boxy li { margin: 0px; }
	#phonetics td {
		margin: 0px;
		padding: 0px 1em;
		vertical-align: top;
		width: 100px;
	}
	#phonetic1 li, #phonetic2 li, #phonetic3 li { margin: 0px; }
	#phonetic2 li {
		margin-bottom: 4px;
	}
	#phonetic3 { margin-top: -4px; }
	#phonetic3 li { margin-top: 4px; }
	#phoneticlong {
		margin-bottom: 1em;
	}
	#phoneticlong li, #buttons li {
		margin-bottom: 0px;
		margin-top: 4px;
	}

	#boxes {
		font-family: Arial, sans-serif;
		list-style-type: none;
		margin: 0px;
		padding: 0px;
		width: 300px;
	}
	#boxes li {
		cursor: move;
		position: relative;
		float: left;
		margin: 2px 2px 0px 0px;
		width: 33px;
		height: 28px;
		border: 1px solid #000;
		text-align: center;
		padding-top: 5px;
		background-color: #eeeeff;
	}

	#twolists td {
		width: 300px;
		vertical-align: top;
	}
	#twolists1 li {
		font-family: sans-serif;
	}
	#twolists2 {
		border: 1px dashed #fff;
	}
	#twolists2 li {
		font-family: serif;
		background-color: #eedddd;
	}
	.inspector {
		font-size: 11px;
	}
	//-->
</style>

<script language="JavaScript" type="text/javascript" src="source/org/tool-man/core.js"></script>
<script language="JavaScript" type="text/javascript" src="source/org/tool-man/events.js"></script>
<script language="JavaScript" type="text/javascript" src="source/org/tool-man/css.js"></script>
<script language="JavaScript" type="text/javascript" src="source/org/tool-man/coordinates.js"></script>
<script language="JavaScript" type="text/javascript" src="source/org/tool-man/drag.js"></script>
<script language="JavaScript" type="text/javascript" src="source/org/tool-man/dragsort.js"></script>
<script language="JavaScript" type="text/javascript" src="source/org/tool-man/cookies.js"></script>

<script language="JavaScript" type="text/javascript"><!--
	var dragsort = ToolMan.dragsort()
	var junkdrawer = ToolMan.junkdrawer()

	window.onload = function() {
		junkdrawer.restoreListOrder("numeric")
		junkdrawer.restoreListOrder("phonetic1")
		junkdrawer.restoreListOrder("phonetic2")
		junkdrawer.restoreListOrder("phonetic3")
		junkdrawer.restoreListOrder("phoneticlong")
		junkdrawer.restoreListOrder("boxes")
		junkdrawer.restoreListOrder("buttons")
		//junkdrawer.restoreListOrder("twolists1")
		//junkdrawer.restoreListOrder("twolists2")

		dragsort.makeListSortable(document.getElementById("numeric"),
				verticalOnly, saveOrder)

		dragsort.makeListSortable(document.getElementById("phonetic1"),
				verticalOnly, saveOrder)
		dragsort.makeListSortable(document.getElementById("phonetic2"),
				verticalOnly, saveOrder)
		dragsort.makeListSortable(document.getElementById("phonetic3"),
				verticalOnly, saveOrder)
		dragsort.makeListSortable(document.getElementById("phoneticlong"),
				verticalOnly, saveOrder)

		dragsort.makeListSortable(document.getElementById("boxes"),
				saveOrder)

		dragsort.makeListSortable(document.getElementById("buttons"),
				saveOrder)

		/*
		dragsort.makeListSortable(document.getElementById("twolists1"),
				saveOrder)
		dragsort.makeListSortable(document.getElementById("twolists2"),
				saveOrder)
		*/
	}

	function verticalOnly(item) {
		item.toolManDragGroup.verticalOnly()
	}

	function speak(id, what) {
		var element = document.getElementById(id);
		element.innerHTML = 'Clicked ' + what;
	}

	function saveOrder(item) {
		var group = item.toolManDragGroup
		var list = group.element.parentNode
		var id = list.getAttribute("id")
		if (id == null) return
		group.register('dragend', function() {
			ToolMan.cookies().set("list-" + id, 
					junkdrawer.serializeList(list), 365)
		})
	}

	//-->
</script>

<SCRIPT LANGUAGE=javascript><!--
function post(){
list_var = junkdrawer.serializeList(numeric)
document.order2.order.value = list_var
order2.submit()
} 
// --></SCRIPT>

</head>
<?

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT inv_areaname FROM inv_area WHERE inv_areaid = '$inv_areaid'";
    $result = mysql_query($query);
    //mysql_close();

    $inv_areaname=mysql_result($result,0,"inv_areaname");

    echo "<body><center><table width=90%><tr><td colspan=2>";
    echo "<img src=logo.jpg><p></td></tr></table></center><p>";
    echo "<center><table width=90% style=\"border:2px solid #CCCCCC;\" cellspacing=0 cellpadding=0><tr bgcolor=#CCCCFF><td><h3><center>Location: $inv_areaname</center></h3></td></tr>";
    echo "<tr bgcolor=#CCCCFF><td>Click and drag the items to sort your inventory count page. Save when finished.<p></td></tr>";
    echo "<tr bgcolor=#CCCCFF><td><ul id='numeric' class='boxy'>";

    $counter=0;
    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM inv_areadetail WHERE areaid = '$inv_areaid' AND businessid = '$businessid' AND active = '0' ORDER BY orderid";
    $result = mysql_query($query);
    $num=mysql_numrows($result);
    //mysql_close();

    $num--;
    while($num>=0){
       $inv_areadetailid=mysql_result($result,$num,"inv_areadetailid");
       $inv_itemid=mysql_result($result,$num,"inv_itemid");

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query9 = "SELECT item_name FROM inv_items WHERE inv_itemid = '$inv_itemid'";
       $result9 = mysql_query($query9);
       //mysql_close();

       $myname="count$counter";
       $myname2="id$counter";
       $item_name=mysql_result($result9,0,"item_name");
       echo "<li itemID=$counter><table bgcolor=#E8E7E7 style=\"border:1px solid #999999;\" onmouseover=\"this.style.cursor='move';\"><tr><td>$item_name</td></tr></table></li>";

       $num--;
       $counter++;
    }

    echo "</ul></td></tr>";
    //echo "<input class='inspector' type=button value='Inspect' onclick=junkdrawer.inspectListOrder('numeric')>";
    echo "<tr bgcolor=#CCCCFF><td><form action='saveinv_sort.php' name='order2' id='order2' method=post>";
    echo "<input type=hidden name=counter value=$counter>";
    echo "<input type=hidden id='order' name='order'>";
    echo "<input type=hidden name=businessid value='$businessid'>";
    echo "<input type=hidden name=companyid value='$companyid'>";
    echo "<input type=hidden name=inv_areaid value='$inv_areaid'>";
    echo "<input type=hidden name=date value='$godate'>";
    echo "<input name=button type=button class=Submit onClick=post(); value='Save'>";
    echo "</form></td></tr></table></center>"; 
    echo "</body></html>";
}
mysql_close();
?>