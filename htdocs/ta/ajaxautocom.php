<?php
header( 'Content-type: text/javascript' );
mb_internal_encoding( 'UTF-8' );
mb_http_output( 'UTF-8' );
ob_start( 'mb_output_handler' );

/*
if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
include_once( DOC_ROOT . '/ta/db.php' );
require_once( DOC_ROOT . '/bootstrap.php' );
*/
require_once __DIR__ . '/../../application/bootstrap.php';

use EE\ArrayObject;
use EE\Model\Inventory\Item as InventoryItem;
use Essential\Treat\Db\ProxyOld;

if ( $_POST ) {
	$p = new ArrayObject( $_POST );
}
elseif ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
	if ( $_POST ) {
		$p = new ArrayObject( $_POST );
	}
	else {
		$p = new ArrayObject( $_GET );
	}
}

if (
	'businventor4' == $p['dbPage']
	&& ( $p['dbBid'] || $p['bid'] )
) {
	$business_id = $p['dbBid'];
	if ( !$business_id ) {
		$business_id = $p['bid'];
	}
	$items = InventoryItem::getByIdForAjax( $business_id );
	$output = array();
	foreach ( $items as $item ) {
		$output[] = array(
			'val' => $item->inv_itemid,
			'disp' => $item->item_name,
		);
	}
	if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
/*
		echo sprintf(
			'alert( "%s\n%s" );'
			,$p['dbPage']
			,$business_id
		), PHP_EOL;
*/
		
		
		$out = preg_replace(
			array(
				'@^\[(.+)\]$@',
				'@(\{)([^\{\}]+)(\},?)@',
			)
			,array(
				"[\n\$1]",
				"\t\$1 \$2 \$3\n",
			)
			,json_encode( $output )
		);
		echo 'var returnVals = '
			,$out
			,';'
			,PHP_EOL
		;
	}
	else {
		echo 'var returnVals = '
			,json_encode( $output )
			,';'
			,PHP_EOL
		;
	}
}
else {
	$t = $p['dbTable'];
	$v = $p['dbVal'];
	$v = (count(explode(', ', $v)) > 1) ? explode(', ', $v) : $v;
	$d = $p['dbDisp'];
	$d = (count(explode(', ', $d)) > 1) ? explode(', ', $d) : $d;
	$w = $p['dbWhere'];
	$w = (strlen($w) > 0) ? " WHERE $w": '';
	
	$query = "SELECT * FROM $t$w";
	echo sprintf(
		"/*\n%s\n*/\n"
		,$query
	);
	
	//echo $query."\n";
	//print_r($p);
	$q = ProxyOld::query( $query );
	echo "var returnVals = [];\n";
	if ( $q ) {
		while($r = ProxyOld::mysql_fetch_array($q)){
			//Multi Column Support
			if(is_array($d)){
				$disp_col = $p['dispFormat'];
				foreach($d as $disp){
					$disp_col = str_replace($disp, $r[$disp], $disp_col);
				}
			}
			else{
				$disp_col = $r[$d];
			}
			if(is_array($v)){
				$val_col = $p['valFormat'];
				foreach($v as $val){
					$val_col = str_replace($val, $r[$val], $val_col);
				}
			}
			else{
				$val_col = $r[$v];
			}
			
			$disp_col = str_replace( '\'' , "\'", $disp_col);
			
			echo "returnVals[returnVals.length] = {val: '$val_col', disp: '$disp_col'}; \n";
		}
	}
}

?>
