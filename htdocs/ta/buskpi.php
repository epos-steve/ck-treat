<?php

function money($diff){   
        $diff=round($diff, 0);
        return $diff;
}

function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      elseif ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      elseif ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '1')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='31')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;
$leap = date("L");

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   
   elseif ($leap==1&&$month=='03')
   {
      $day=$day+29;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');

$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");
$creditamount="creditamount";
$creditidnty="creditidnty";
$creditdate="creditdate";
$debitamount="debitamount";
$debitidnty="debitidnty";
$debitdate="debitdate";
$quatertotal=0;
$monthtotal=0;
$total=0;
$counter=0;
$findme='.';
$double='.00';
$single='0';
$double2='00';
$overallpcnt=0;
$message2="";

/*$user=$_COOKIE["usercook"];
$pass=$_COOKIE["passcook"];
$businessid=$_GET["bid"];
$companyid=$_GET["cid"];
$prevperiod=$_GET["prevperiod"];
$nextperiod=$_GET["nextperiod"];
$viewmonth=$_GET["viewmonth"];
$update=$_GET["update"];
$audit=$_GET["audit"];
$nouser=$_GET["nouser"];
$businessid=$_GET["bid"];*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook');
$businessid = \EE\Controller\Base::getGetVariable('bid');
$companyid = \EE\Controller\Base::getGetVariable('cid');
$prevperiod = \EE\Controller\Base::getGetVariable('prevperiod');
$nextperiod = \EE\Controller\Base::getGetVariable('nextperiod');
$viewmonth = \EE\Controller\Base::getGetVariable('viewmonth');
$update = \EE\Controller\Base::getGetVariable('update');
$audit = \EE\Controller\Base::getGetVariable('audit');
$nouser = \EE\Controller\Base::getGetVariable('nouser');
$nouser = \EE\Controller\Base::getGetVariable('nouser');


if ($nouser!="taauto") 
{
    echo "<center><h2>Failed</h2></center>";
}

else
{

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM company WHERE companyid = '$companyid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $companyname=@Treat_DB_ProxyOld::mysql_result($result,0,"companyname");


/////////MAIN LOOP///////////////////////////////////////////////////////////////////////////////////////////

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query99 = "SELECT * FROM business WHERE businessid = '$businessid'";
$result99 = Treat_DB_ProxyOld::query($query99);
$num99=@Treat_DB_ProxyOld::mysql_numrows($result99);
//mysql_close();

$num99--;
while ($num99>=0){

    $message="<table>";
    $curbusinessid=@Treat_DB_ProxyOld::mysql_result($result99,$num99,"businessid");

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM business WHERE companyid = '$companyid' AND businessid = '$curbusinessid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $businessname=@Treat_DB_ProxyOld::mysql_result($result,0,"businessname");
    $businessid=@Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
    $street=@Treat_DB_ProxyOld::mysql_result($result,0,"street");
    $city=@Treat_DB_ProxyOld::mysql_result($result,0,"city");
    $state=@Treat_DB_ProxyOld::mysql_result($result,0,"state");
    $zip=@Treat_DB_ProxyOld::mysql_result($result,0,"zip");
    $phone=@Treat_DB_ProxyOld::mysql_result($result,0,"phone");
    $over_short_acct=@Treat_DB_ProxyOld::mysql_result($result,0,"over_short_acct");
    $count1=@Treat_DB_ProxyOld::mysql_result($result,0,"count1");
    $count2=@Treat_DB_ProxyOld::mysql_result($result,0,"count2");
    $count3=@Treat_DB_ProxyOld::mysql_result($result,0,"count3");
    $benefitprcnt=@Treat_DB_ProxyOld::mysql_result($result,0,"benefit");
    $operate=@Treat_DB_ProxyOld::mysql_result($result,0,"operate");
    $unit=@Treat_DB_ProxyOld::mysql_result($result,0,"unit");
    $districtid=@Treat_DB_ProxyOld::mysql_result($result,0,"districtid");
                        
    $message="$message<tr><td colspan=11 bgcolor=#E8E7E7><b>$unit - $businessname</b></td></tr>";
    $message="$message<tr><td colspan=2 bgcolor=#E8E7E7><center><font color=black>Current Week</td><td colspan=2 bgcolor=#E8E7E7><center><font color=black>CW Bdgt</td><td colspan=2 bgcolor=#E8E7E7><center><font color=black>Month to Date</td><td colspan=2 bgcolor=#E8E7E7><center><font color=black>MTD Bdgt</td><td bgcolor=#E8E7E7 colspan=3><center>Total Month Bdgt</center></td></tr>";

    $lastdistrictid=$districtid;
    $showyear=$year;
    $gomonth=substr($viewmonth,5,2);
    $goyear=substr($viewmonth,0,4);

    if ($gomonth=="01" || ($viewmonth==""&&$month=="01")){$showm1="SELECTED";}
    if ($gomonth=="02" || ($viewmonth==""&&$month=="02")){$showm2="SELECTED";}
    if ($gomonth=="03" || ($viewmonth==""&&$month=="03")){$showm3="SELECTED";}
    if ($gomonth=="04" || ($viewmonth==""&&$month=="04")){$showm4="SELECTED";}
    if ($gomonth=="05" || ($viewmonth==""&&$month=="05")){$showm5="SELECTED";}
    if ($gomonth=="06" || ($viewmonth==""&&$month=="06")){$showm6="SELECTED";}
    if ($gomonth=="07" || ($viewmonth==""&&$month=="07")){$showm7="SELECTED";}
    if ($gomonth=="08" || ($viewmonth==""&&$month=="08")){$showm8="SELECTED";}
    if ($gomonth=="09" || ($viewmonth==""&&$month=="09")){$showm9="SELECTED";}
    if ($gomonth=="10" || ($viewmonth==""&&$month=="10")){$showm10="SELECTED";}
    if ($gomonth=="11" || ($viewmonth==""&&$month=="11")){$showm11="SELECTED";}
    if ($gomonth=="12" || ($viewmonth==""&&$month=="12")){$showm12="SELECTED";}

//////START TABLE//////////////////////////////////////////////////////////////////////////////////

    if ($viewmonth!=""){$startdate=$viewmonth;$temp_date=$today;}
    else {$startdate=$today;$temp_date=$today;}

    $totaloperate=0;
    $operate1=0;
    $operate2=0;
    $operate3=0;
    $operate4=0;
    $operate5=0;
    $operate6=0;

    $startmonth=substr($startdate,5,2);
    $startyear=substr($startdate,0,4);
    $startmonth++;
    if ($startmonth==13){$startmonth="01";$startyear++;$viewnext="$startyear-$startmonth-01";}
    else {
      if ($startmonth<10){$startmonth="0$startmonth";}
      $viewnext="$startyear-$startmonth-01";
    }
    
    $startmonth=substr($startdate,5,2);
    $startyear=substr($startdate,0,4);
    $startmonth--;
    if ($startmonth==0){$startmonth="12";$startyear--;$viewprev="$startyear-$startmonth-01";}
    else {
      if ($startmonth<10){$startmonth="0$startmonth";}
      $viewprev="$startyear-$startmonth-01";
    }
    
    $currentmonth = date("m");
    $weekday = date("l");
    $startmonth=substr($startdate,5,2);
    $startyear=substr($startdate,0,4);
    $startdate="$startyear-$startmonth-01";

    $leap = date("L");
    if ($startmonth==1 || $startmonth==3 || $startmonth==5 || $startmonth==7 || $startmonth==8 || $startmonth==10 || $startmonth==12){$endingdate=31;}
    elseif ($startmonth==4 || $startmonth==6 || $startmonth==9 || $startmonth==11){$endingdate=30;}
    elseif ($startmonth==2 && $leap==1){$endingdate=29;}
    elseif ($startmonth==2 && $leap==0){$endingdate=28;}
    $enddate="$startyear-$startmonth-$endingdate";

    if ($startmonth=="01"){$showmonth="January";}
    elseif ($startmonth=="02"){$showmonth="February";}
    elseif ($startmonth=="03"){$showmonth="March";}
    elseif ($startmonth=="04"){$showmonth="April";}
    elseif ($startmonth=="05"){$showmonth="May";}
    elseif ($startmonth=="06"){$showmonth="June";}
    elseif ($startmonth=="07"){$showmonth="July";}
    elseif ($startmonth=="08"){$showmonth="August";}
    elseif ($startmonth=="09"){$showmonth="September";}
    elseif ($startmonth=="10"){$showmonth="October";}
    elseif ($startmonth=="11"){$showmonth="November";}
    elseif ($startmonth=="12"){$showmonth="December";}

    $weekday = date("l");
    $temp_date=$today;
    while ($temp_date>$startdate){
          $temp_date=prevday($temp_date);
          if ($weekday=="Sunday"){$weekday="Saturday";}
          elseif ($weekday=="Monday"){$weekday="Sunday";}
          elseif ($weekday=="Tuesday"){$weekday="Monday";}
          elseif ($weekday=="Wednesday"){$weekday="Tuesday";}
          elseif ($weekday=="Thursday"){$weekday="Wednesday";}
          elseif ($weekday=="Friday"){$weekday="Thursday";}
          elseif ($weekday=="Saturday"){$weekday="Friday";}
    }
    $startweekday=$weekday;

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM credits WHERE (credittype = '1' OR credittype = '5') AND businessid = '$businessid' AND ((is_deleted = '0') OR (is_deleted = '1' AND del_date >= '$startdate')) ORDER BY credittype DESC, creditname DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    for($mycounter=1;$mycounter<=4;$mycounter++){$today=nextday($today);}

    $weekcount=1;
    $goweek=0;
    $weekdayname=$weekday;
    $startweekdayname=$weekday;
    $day=$startdate;

    while ($day<=$enddate){
          if ($weekdayname=="Sunday"){$weekdayname="Monday";}
          elseif ($weekdayname=="Saturday"){$weekdayname="Sunday";}
          elseif ($weekdayname=="Friday"){$weekdayname="Saturday";}
          elseif ($weekdayname=="Thursday"&&$day!=$enddate){$weekdayname="Friday";$weekcount++;}
          elseif ($weekdayname=="Thursday"&&$day==$enddate){$weekdayname="Friday";}
          elseif ($weekdayname=="Wednesday"){$weekdayname="Thursday";}
          elseif ($weekdayname=="Tuesday"){$weekdayname="Wednesday";}
          elseif ($weekdayname=="Monday"){$weekdayname="Tuesday";}
          if ($today==$day){$goweek=$weekcount-1;}
          $day=nextday($day);
    }
    if ($goweek==0){$goweek=$weekcount;}
    $day = date("d");
    $year = date("Y");
    $month = date("m");
    $today="$year-$month-$day";

    if ($weekcount==4){$colnum=14;}
    elseif($weekcount==5){$colnum=16;}
    elseif($weekcount==6){$colnum=18;}

///////Operating DAys and Budget////////////////////////////////////////////////////////////////////////////////////////////////
       $wk1laborhour=0;
       $wk2laborhour=0;
       $wk3laborhour=0;
       $wk4laborhour=0;
       $wk5laborhour=0;
       $wk6laborhour=0;

       $cashwk1=0;
       $cashwk2=0;
       $cashwk3=0;
       $cashwk4=0;
       $cashwk5=0;
       $cashwk6=0;

       $day=$startdate;
       $weekdayname2=dayofweek($startdate);
       $currentweek=1;
       while ($day<=$enddate){  
          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query5 = "SELECT * FROM stats WHERE businessid = '$businessid' AND date = '$day'";
          $result5 = Treat_DB_ProxyOld::query($query5);
          //mysql_close();

          $closed=@Treat_DB_ProxyOld::mysql_result($result5,0,"closed");
          $labor_hour=@Treat_DB_ProxyOld::mysql_result($result5,0,"labor_hour");

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query6 = "SELECT * FROM cash WHERE businessid = '$businessid' AND date = '$day'";
          $result6 = Treat_DB_ProxyOld::query($query6);
          //mysql_close();
          $cashamount=@Treat_DB_ProxyOld::mysql_result($result6,0,"amount");
          $cashtype=@Treat_DB_ProxyOld::mysql_result($result6,0,"type");
 
          if ($currentweek==1){if (($weekdayname2!="Sunday"&&$weekdayname2!="Saturday"&&$operate==5&&$closed!=1)||($weekdayname2!="Sunday"&&$operate==6&&$closed!=1)||($operate==7&&$closed!=1)){$operate1++;$totaloperate++;}$wk1laborhour=$wk1laborhour+$labor_hour;}
          elseif ($currentweek==2){if (($weekdayname2!="Sunday"&&$weekdayname2!="Saturday"&&$operate==5&&$closed!=1)||($weekdayname2!="Sunday"&&$operate==6&&$closed!=1)||($operate==7&&$closed!=1)){$operate2++;$totaloperate++;}$wk2laborhour=$wk2laborhour+$labor_hour;}
          elseif ($currentweek==3){if (($weekdayname2!="Sunday"&&$weekdayname2!="Saturday"&&$operate==5&&$closed!=1)||($weekdayname2!="Sunday"&&$operate==6&&$closed!=1)||($operate==7&&$closed!=1)){$operate3++;$totaloperate++;}$wk3laborhour=$wk3laborhour+$labor_hour;}
          elseif ($currentweek==4){if (($weekdayname2!="Sunday"&&$weekdayname2!="Saturday"&&$operate==5&&$closed!=1)||($weekdayname2!="Sunday"&&$operate==6&&$closed!=1)||($operate==7&&$closed!=1)){$operate4++;$totaloperate++;}$wk4laborhour=$wk4laborhour+$labor_hour;}
          elseif ($currentweek==5){if (($weekdayname2!="Sunday"&&$weekdayname2!="Saturday"&&$operate==5&&$closed!=1)||($weekdayname2!="Sunday"&&$operate==6&&$closed!=1)||($operate==7&&$closed!=1)){$operate5++;$totaloperate++;}$wk5laborhour=$wk5laborhour+$labor_hour;}
          elseif ($currentweek==6){if (($weekdayname2!="Sunday"&&$weekdayname2!="Saturday"&&$operate==5&&$closed!=1)||($weekdayname2!="Sunday"&&$operate==6&&$closed!=1)||($operate==7&&$closed!=1)){$operate6++;$totaloperate++;}$wk6laborhour=$wk6laborhour+$labor_hour;}
          
          if ($currentweek==1){if($cashtype==0){$cashwk1=$cashwk1+$cashamount;}else{$cashwk1=$cashwk1-$cashamount;}}
          elseif ($currentweek==2){if($cashtype==0){$cashwk2=$cashwk2+$cashamount;}else{$cashwk2=$cashwk2-$cashamount;}}
          elseif ($currentweek==3){if($cashtype==0){$cashwk3=$cashwk3+$cashamount;}else{$cashwk3=$cashwk3-$cashamount;}}
          elseif ($currentweek==4){if($cashtype==0){$cashwk4=$cashwk4+$cashamount;}else{$cashwk4=$cashwk4-$cashamount;}}
          elseif ($currentweek==5){if($cashtype==0){$cashwk5=$cashwk5+$cashamount;}else{$cashwk5=$cashwk5-$cashamount;}}
          elseif ($currentweek==6){if($cashtype==0){$cashwk6=$cashwk6+$cashamount;}else{$cashwk6=$cashwk6-$cashamount;}}

          if ($weekdayname2=="Sunday"){$weekdayname2="Monday";}
          elseif ($weekdayname2=="Saturday"){$weekdayname2="Sunday";}
          elseif ($weekdayname2=="Friday"){$weekdayname2="Saturday";}
          elseif ($weekdayname2=="Thursday"){$weekdayname2="Friday";$currentweek++;}
          elseif ($weekdayname2=="Wednesday"){$weekdayname2="Thursday";}
          elseif ($weekdayname2=="Tuesday"){$weekdayname2="Wednesday";}
          elseif ($weekdayname2=="Monday"){$weekdayname2="Tuesday";}
          $day=nextday($day);
        }

    $year5=substr($startdate,0,4);
    $month5=substr($startdate,5,2);

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query2 = "SELECT * FROM budget WHERE businessid = '$unit' and type = '10' and year = '$year5'";
    $result2 = Treat_DB_ProxyOld::query($query2);
    //mysql_close();
    $bgtotsales=@Treat_DB_ProxyOld::mysql_result($result2,0,$month5);

    $comtotsalesbgt=$comtotsalesbgt+$bgtotsales;
    $disttotsalesbgt=$disttotsalesbgt+$bgtotsales;
    
        if ($goweek==1){$wksalesbgt=$bgtotsales/$totaloperate*$operate1;$mtdsalesbgt=$wksalesbgt;}
    elseif ($goweek==2){$wksalesbgt=$bgtotsales/$totaloperate*$operate2;$mtdsalesbgt=$bgtotsales/$totaloperate*($operate1+$operate2);}
    elseif ($goweek==3){$wksalesbgt=$bgtotsales/$totaloperate*$operate3;$mtdsalesbgt=$bgtotsales/$totaloperate*($operate1+$operate2+$operate3);}
    elseif ($goweek==4){$wksalesbgt=$bgtotsales/$totaloperate*$operate4;$mtdsalesbgt=$bgtotsales/$totaloperate*($operate1+$operate2+$operate3+$operate4);}
    elseif ($goweek==5){$wksalesbgt=$bgtotsales/$totaloperate*$operate5;$mtdsalesbgt=$bgtotsales/$totaloperate*($operate1+$operate2+$operate3+$operate4+$operate5);}
    elseif ($goweek==6){$wksalesbgt=$bgtotsales/$totaloperate*$operate6;$mtdsalesbgt=$bgtotsales/$totaloperate*($operate1+$operate2+$operate3+$operate4+$operate5+$operate6);}

    $comsalesbgt=$wksalesbgt+$comsalesbgt;
    $commtdsalesbgt=$mtdsalesbgt+$commtdsalesbgt;
    $distsalesbgt=$wksalesbgt+$distsalesbgt;
    $distmtdsalesbgt=$mtdsalesbgt+$distmtdsalesbgt;

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query2 = "SELECT * FROM budget WHERE businessid = '$unit' and type = '20' and year = '$year5'";
    $result2 = Treat_DB_ProxyOld::query($query2);
    //mysql_close();
    $bgtcos=@Treat_DB_ProxyOld::mysql_result($result2,0,$month5);

    $comtotcosbgt=$comtotcosbgt+$bgtcos;
    $disttotcosbgt=$disttotcosbgt+$bgtcos;

    if ($goweek==1){$wkcosbgt=$bgtcos/$totaloperate*$operate1;$mtdcosbgt=$wkcosbgt;}
    elseif ($goweek==2){$wkcosbgt=$bgtcos/$totaloperate*$operate2;$mtdcosbgt=$bgtcos/$totaloperate*($operate1+$operate2);}
    elseif ($goweek==3){$wkcosbgt=$bgtcos/$totaloperate*$operate3;$mtdcosbgt=$bgtcos/$totaloperate*($operate1+$operate2+$operate3);}
    elseif ($goweek==4){$wkcosbgt=$bgtcos/$totaloperate*$operate4;$mtdcosbgt=$bgtcos/$totaloperate*($operate1+$operate2+$operate3+$operate4);}
    elseif ($goweek==5){$wkcosbgt=$bgtcos/$totaloperate*$operate5;$mtdcosbgt=$bgtcos/$totaloperate*($operate1+$operate2+$operate3+$operate4+$operate5);}
    elseif ($goweek==6){$wkcosbgt=$bgtcos/$totaloperate*$operate6;$mtdcosbgt=$bgtcos/$totaloperate*($operate1+$operate2+$operate3+$operate4+$operate5+$operate6);}

    $comcosbgt=$comcosbgt+$wkcosbgt;
    $commtdcosbgt=$commtdcosbgt+$mtdcosbgt;
    $distcosbgt=$distcosbgt+$wkcosbgt;
    $distmtdcosbgt=$distmtdcosbgt+$mtdcosbgt;

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query2 = "SELECT * FROM budget WHERE businessid = '$unit' and type = '30' and year = '$year5'";
    $result2 = Treat_DB_ProxyOld::query($query2);
    //mysql_close();
    $bgtlabor=@Treat_DB_ProxyOld::mysql_result($result2,0,$month5);

    $comtotlbrbgt=$comtotlbrbgt+$bgtlabor;
    $disttotlbrbgt=$disttotlbrbgt+$bgtlabor;

    if ($goweek==1){$wklaborbgt=$bgtlabor/$totaloperate*$operate1;$mtdlaborbgt=$wklaborbgt;}
    elseif ($goweek==2){$wklaborbgt=$bgtlabor/$totaloperate*$operate2;$mtdlaborbgt=$bgtlabor/$totaloperate*($operate1+$operate2);}
    elseif ($goweek==3){$wklaborbgt=$bgtlabor/$totaloperate*$operate3;$mtdlaborbgt=$bgtlabor/$totaloperate*($operate1+$operate2+$operate3);}
    elseif ($goweek==4){$wklaborbgt=$bgtlabor/$totaloperate*$operate4;$mtdlaborbgt=$bgtlabor/$totaloperate*($operate1+$operate2+$operate3+$operate4);}
    elseif ($goweek==5){$wklaborbgt=$bgtlabor/$totaloperate*$operate5;$mtdlaborbgt=$bgtlabor/$totaloperate*($operate1+$operate2+$operate3+$operate4+$operate5);}
    elseif ($goweek==6){$wklaborbgt=$bgtlabor/$totaloperate*$operate6;$mtdlaborbgt=$bgtlabor/$totaloperate*($operate1+$operate2+$operate3+$operate4+$operate5+$operate6);}

    $comlbrbgt=$comlbrbgt+$wklaborbgt;
    $commtdlbrbgt=$commtdlbrbgt+$mtdlaborbgt;
    $distlbrbgt=$distlbrbgt+$wklaborbgt;
    $distmtdlbrbgt=$distmtdlbrbgt+$mtdlaborbgt;

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query2 = "SELECT * FROM budget WHERE businessid = '$unit' and type = '40' and year = '$year5'";
    $result2 = Treat_DB_ProxyOld::query($query2);
    //mysql_close();
    $bgttoe=@Treat_DB_ProxyOld::mysql_result($result2,0,$month5);

    $comtottoebgt=$comtottoebgt+$bgttoe;
    $disttottoebgt=$disttottoebgt+$bgttoe;

    if ($goweek==1){$wktoebgt=$bgttoe/$totaloperate*$operate1;$mtdtoebgt=$wktoebgt;}
    elseif ($goweek==2){$wktoebgt=$bgttoe/$totaloperate*$operate2;$mtdtoebgt=$bgttoe/$totaloperate*($operate1+$operate2);}
    elseif ($goweek==3){$wktoebgt=$bgttoe/$totaloperate*$operate3;$mtdtoebgt=$bgttoe/$totaloperate*($operate1+$operate2+$operate3);}
    elseif ($goweek==4){$wktoebgt=$bgttoe/$totaloperate*$operate4;$mtdtoebgt=$bgttoe/$totaloperate*($operate1+$operate2+$operate3+$operate4);}
    elseif ($goweek==5){$wktoebgt=$bgttoe/$totaloperate*$operate5;$mtdtoebgt=$bgttoe/$totaloperate*($operate1+$operate2+$operate3+$operate4+$operate5);}
    elseif ($goweek==6){$wktoebgt=$bgttoe/$totaloperate*$operate6;$mtdtoebgt=$bgttoe/$totaloperate*($operate1+$operate2+$operate3+$operate4+$operate5+$operate6);}

    $comtoebgt=$comtoebgt+$wktoebgt;
    $commtdtoebgt=$commtdtoebgt+$mtdtoebgt;
    $disttoebgt=$disttoebgt+$wktoebgt;
    $distmtdtoebgt=$distmtdtoebgt+$mtdtoebgt;

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query2 = "SELECT * FROM budget WHERE businessid = '$unit' and type = '50' and year = '$year5'";
    $result2 = Treat_DB_ProxyOld::query($query2);
    //mysql_close();
    $bgtnetpft=@Treat_DB_ProxyOld::mysql_result($result2,0,$month5);

    $comtotplbgt=$comtotplbgt+$bgtnetpft;
    $disttotplbgt=$disttotplbgt+$bgtnetpft;

    if ($goweek==1){$wknetpftbgt=$bgtnetpft/$totaloperate*$operate1;$mtdnetpftbgt=$wknetpftbgt;}
    elseif ($goweek==2){$wknetpftbgt=$bgtnetpft/$totaloperate*$operate2;$mtdnetpftbgt=$bgtnetpft/$totaloperate*($operate1+$operate2);}
    elseif ($goweek==3){$wknetpftbgt=$bgtnetpft/$totaloperate*$operate3;$mtdnetpftbgt=$bgtnetpft/$totaloperate*($operate1+$operate2+$operate3);}
    elseif ($goweek==4){$wknetpftbgt=$bgtnetpft/$totaloperate*$operate4;$mtdnetpftbgt=$bgtnetpft/$totaloperate*($operate1+$operate2+$operate3+$operate4);}
    elseif ($goweek==5){$wknetpftbgt=$bgtnetpft/$totaloperate*$operate5;$mtdnetpftbgt=$bgtnetpft/$totaloperate*($operate1+$operate2+$operate3+$operate4+$operate5);}
    elseif ($goweek==6){$wknetpftbgt=$bgtnetpft/$totaloperate*$operate6;$mtdnetpftbgt=$bgtnetpft/$totaloperate*($operate1+$operate2+$operate3+$operate4+$operate5+$operate6);}

    $complbgt=$complbgt+$wknetpftbgt;
    $commtdplbgt=$commtdplbgt+$mtdnetpftbgt;
    $distplbgt=$distplbgt+$wknetpftbgt;
    $distmtdplbgt=$distmtdplbgt+$mtdnetpftbgt;

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query2 = "SELECT * FROM budget WHERE businessid = '$unit' and type = '60' and year = '$year5'";
    $result2 = Treat_DB_ProxyOld::query($query2);
    //mysql_close();
    $bgtamtdue=@Treat_DB_ProxyOld::mysql_result($result2,0,$month5);

    if ($goweek==1){$wkamtduebgt=$bgtamtdue/$totaloperate*$operate1;$mtdamtduebgt=$wkamtduebgt;}
    elseif ($goweek==2){$wkamtduebgt=$bgtamtdue/$totaloperate*$operate2;$mtdamtduebgt=$bgtamtdue/$totaloperate*($operate1+$operate2);}
    elseif ($goweek==3){$wkamtduebgt=$bgtamtdue/$totaloperate*$operate3;$mtdamtduebgt=$bgtamtdue/$totaloperate*($operate1+$operate2+$operate3);}
    elseif ($goweek==4){$wkamtduebgt=$bgtamtdue/$totaloperate*$operate4;$mtdamtduebgt=$bgtamtdue/$totaloperate*($operate1+$operate2+$operate3+$operate4);}
    elseif ($goweek==5){$wkamtduebgt=$bgtamtdue/$totaloperate*$operate5;$mtdamtduebgt=$bgtamtdue/$totaloperate*($operate1+$operate2+$operate3+$operate4+$operate5);}
    elseif ($goweek==6){$wkamtduebgt=$bgtamtdue/$totaloperate*$operate6;$mtdamtduebgt=$bgtamtdue/$totaloperate*($operate1+$operate2+$operate3+$operate4+$operate5+$operate6);}


/////////////BUDGETS//////////////////////////////////////////////////////////////////////////////////////////
    $cosprcnt=round($bgtcos/$bgtotsales*100,1);
    $lbrprcnt=round($bgtlabor/$bgtotsales*100,1);
    $toeprcnt=round($bgttoe/$bgtotsales*100,1);
    $netpftprcnt=round($bgtnetpft/$bgtotsales*100,1);
    $bgtotsales = number_format($bgtotsales);
    $bgtcos = number_format($bgtcos);
    $bgtlabor = number_format($bgtlabor);
    $bgttoe = number_format($bgttoe);
    $bgtnetpft = number_format($bgtnetpft);

///////////////////////END OPERATING DAYS//////////////////////////////////////////////////////////////////
    $weekday=$startweekday;

    $wk1=0;
    $wk2=0;
    $wk3=0;
    $wk4=0;
    $wk5=0;
    $wk6=0;
    $currentweek=1;

    $num--;
    while ($num>=0){

       $day=$startdate;

          $creditname=@Treat_DB_ProxyOld::mysql_result($result,$num,"creditname");
          $creditid=@Treat_DB_ProxyOld::mysql_result($result,$num,"creditid");
          $account=@Treat_DB_ProxyOld::mysql_result($result,$num,"account");
          $invoicesales=@Treat_DB_ProxyOld::mysql_result($result,$num,"invoicesales");
          $invoicesales_notax=@Treat_DB_ProxyOld::mysql_result($result,$num,"invoicesales_notax");
          $invoicetax=@Treat_DB_ProxyOld::mysql_result($result,$num,"invoicetax");
          $servchrg=@Treat_DB_ProxyOld::mysql_result($result,$num,"servchrg");
          $salescode=@Treat_DB_ProxyOld::mysql_result($result,$num,"salescode");
          $prepayment=@Treat_DB_ProxyOld::mysql_result($result,$num,"heldcheck");
          $showaccount=substr($account,0,5);

       $prevamount=0;
       $credittotal=0;
       $monthtotal=0;
       while ($day<=$enddate){

          $prevamount=0;

///MANUAL
          if ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0&&$prepayment==0&&($salescode==0||$salescode=="")){
                  //mysql_connect($dbhost,$username,$password);
                  //@mysql_select_db($database) or die( "Unable to select database");
                  $query5 = "SELECT amount FROM creditdetail WHERE creditid = '$creditid' AND date = '$day'";
                  $result5 = Treat_DB_ProxyOld::query($query5);
                  $num5=Treat_DB_ProxyOld::mysql_numrows($result5);
                  //mysql_close();

                  if ($num5!=0){$prevamount=@Treat_DB_ProxyOld::mysql_result($result5,0,"amount");}
                  $prevamount = money($prevamount);

             }
///INVOICE
             elseif ($invoicesales==1&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0&&$prepayment==0&&($salescode=="0" || $salescode=="")){
                  //mysql_connect($dbhost,$username,$password);
                  //@mysql_select_db($database) or die( "Unable to select database");
                  $query5 = "SELECT total,taxtotal,servamt FROM invoice WHERE type = '1' AND companyid = '$companyid' AND businessid = '$businessid' AND date = '$day' AND taxable = 'on' AND salescode = '0' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = Treat_DB_ProxyOld::query($query5);
                  $num6=Treat_DB_ProxyOld::mysql_numrows($result5);
                  //mysql_close();
                  $num6--;
                  $prevamount=0;
                  while ($num6>=0){
                     $invtotal=@Treat_DB_ProxyOld::mysql_result($result5,$num6,"total");
                     $invtax=@Treat_DB_ProxyOld::mysql_result($result5,$num6,"taxtotal");
                     $servamt=@Treat_DB_ProxyOld::mysql_result($result5,$num6,"servamt");
                     $prevamount=$prevamount+($invtotal-$invtax-$servamt);
                     $num6--;
                  }
                  $prevamount=money($prevamount);
             }
///INVOICE TAX EXEMPT
             elseif ($invoicesales==0&&$invoicesales_notax==1&&$invoicetax==0&&$servchrg==0&&$prepayment==0&&($salescode=="0" || $salescode=="")){
                  //mysql_connect($dbhost,$username,$password);
                  //@mysql_select_db($database) or die( "Unable to select database");
                  $query5 = "SELECT total,taxtotal,servamt FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND taxable = 'off' AND salescode = '0' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = Treat_DB_ProxyOld::query($query5);
                  $num6=Treat_DB_ProxyOld::mysql_numrows($result5);
                  //mysql_close();
                  $num6--;
                  $prevamount=0;
                  while ($num6>=0){
                     $invtotal=@Treat_DB_ProxyOld::mysql_result($result5,$num6,"total");
                     $servamt=@Treat_DB_ProxyOld::mysql_result($result5,$num6,"servamt");
                     $prevamount=$prevamount+$invtotal-$servamt;
                     $num6--;
                  }
                  $prevamount=money($prevamount);
             }
///INVOICE TAX
             elseif ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==1&&$servchrg==0&&$prepayment==0&&($salescode=="0" || $salescode=="")){
                  //mysql_connect($dbhost,$username,$password);
                  //@mysql_select_db($database) or die( "Unable to select database");
                  $query5 = "SELECT taxtotal FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND taxable = 'on' AND salescode = '0' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = Treat_DB_ProxyOld::query($query5);
                  $num6=Treat_DB_ProxyOld::mysql_numrows($result5);
                  //mysql_close();
                  $num6--;
                  $prevamount=0;
                  while ($num6>=0){
                     $taxtotal=@Treat_DB_ProxyOld::mysql_result($result5,$num6,"taxtotal");
                     $prevamount=$prevamount+$taxtotal;
                     $num6--;
                  }
                  $prevamount=money($prevamount);
             }
///INVOICE SERVICE CHARGE
             elseif ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==1&&$prepayment==0&&($salescode=="0" || $salescode=="")){
                  //mysql_connect($dbhost,$username,$password);
                  //@mysql_select_db($database) or die( "Unable to select database");
                  $query5 = "SELECT servamt FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND salescode = '0' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = Treat_DB_ProxyOld::query($query5);
                  $num6=Treat_DB_ProxyOld::mysql_numrows($result5);
                  //mysql_close();
                  $num6--;
                  $prevamount=0;
                  while ($num6>=0){
                     $servamt=@Treat_DB_ProxyOld::mysql_result($result5,$num6,"servamt");
                     $prevamount=$prevamount+$servamt;
                     $num6--;
                  }
                  $prevamount=money($prevamount);
             }
///INVOICE (SALESCODE)////////////////////////////////////////////////////////////////////////////////////////////////////
             elseif ($invoicesales==1&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0&&$prepayment==0&&$salescode!="0" &&$salescode!=""){
                  //mysql_connect($dbhost,$username,$password);
                  //@mysql_select_db($database) or die( "Unable to select database");
                  $query5 = "SELECT total,taxtotal,servamt FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND taxable = 'on' AND salescode = '$salescode' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = Treat_DB_ProxyOld::query($query5);
                  $num6=Treat_DB_ProxyOld::mysql_numrows($result5);
                  //mysql_close();
                  $num6--;
                  $prevamount=0;
                  while ($num6>=0){
                     $invtotal=@Treat_DB_ProxyOld::mysql_result($result5,$num6,"total");
                     $invtax=@Treat_DB_ProxyOld::mysql_result($result5,$num6,"taxtotal");
                     $servamt=@Treat_DB_ProxyOld::mysql_result($result5,$num6,"servamt");
                     $prevamount=$prevamount+($invtotal-$invtax-$servamt);
                     $num6--;
                  }
                  $prevamount=money($prevamount);
             }
///INVOICE TAX EXEMPT (SALESCODE)
             elseif ($invoicesales==0&&$invoicesales_notax==1&&$invoicetax==0&&$servchrg==0 && $salescode!="0" && $prepayment==0){
                  //mysql_connect($dbhost,$username,$password);
                  //@mysql_select_db($database) or die( "Unable to select database");
                  $query5 = "SELECT total,taxtotal,servamt FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND taxable = 'off' AND salescode = '$salescode' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = Treat_DB_ProxyOld::query($query5);
                  $num6=Treat_DB_ProxyOld::mysql_numrows($result5);
                  //mysql_close();
                  $num6--;
                  $prevamount=0;
                  while ($num6>=0){
                     $invtotal=@Treat_DB_ProxyOld::mysql_result($result5,$num6,"total");
                     $servamt=@Treat_DB_ProxyOld::mysql_result($result5,$num6,"servamt");
                     $prevamount=$prevamount+$invtotal-$servamt;
                     $num6--;
                  }
                  $prevamount=money($prevamount);
             }
///INVOICE TAX (SALESCODE)
             elseif ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==1&&$servchrg==0&&$salescode!="0"&&$salescode!=""&&$prepayment==0){
                  //mysql_connect($dbhost,$username,$password);
                  //@mysql_select_db($database) or die( "Unable to select database");
                  $query5 = "SELECT taxtotal FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND taxable = 'on' AND salescode = '$salescode' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = Treat_DB_ProxyOld::query($query5);
                  $num6=Treat_DB_ProxyOld::mysql_numrows($result5);
                  //mysql_close();
                  $num6--;
                  $prevamount=0;
                  while ($num6>=0){
                     $taxtotal=@Treat_DB_ProxyOld::mysql_result($result5,$num6,"taxtotal");
                     $prevamount=$prevamount+$taxtotal;
                     $num6--;
                  }
                  $prevamount=money($prevamount);
             }
///INVOICE SERVICE CHARGE (SALESCODE)
             elseif ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==1&&$salescode!="0"&&$salescode!=""&&$prepayment==0){
                  //mysql_connect($dbhost,$username,$password);
                  //@mysql_select_db($database) or die( "Unable to select database");
                  $query5 = "SELECT servamt FROM invoice WHERE type = '1' AND businessid = '$businessid' AND salescode = '$salescode' AND date = '$day' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = Treat_DB_ProxyOld::query($query5);
                  $num6=Treat_DB_ProxyOld::mysql_numrows($result5);
                  //mysql_close();
                  $num6--;
                  $prevamount=0;
                  while ($num6>=0){
                     $servamt=@Treat_DB_ProxyOld::mysql_result($result5,$num6,"servamt");
                     $prevamount=$prevamount+$servamt;
                     $num6--;
                  }
                  $prevamount=money($prevamount);
             }
///PREPAYMENT
             elseif ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0&&$prepayment==1){
                  //mysql_connect($dbhost,$username,$password);
                  //@mysql_select_db($database) or die( "Unable to select database");
                  $query5 = "SELECT amount FROM invtender WHERE businessid = '$businessid' AND date = '$day'";
                  $result5 = Treat_DB_ProxyOld::query($query5);
                  $num6=Treat_DB_ProxyOld::mysql_numrows($result5);
                  //mysql_close();
                  $num6--;
                  $prevamount=0;
                  while ($num6>=0){
                     $amount=@Treat_DB_ProxyOld::mysql_result($result5,$num6,"amount");
                     $prevamount=$prevamount+$amount;
                     $num6--;  
                  }
                  $prevamount=money($prevamount);
                  
             }

          $credittotal=$prevamount+$credittotal;

          if ($currentweek==1){$wk1=$wk1+$prevamount;}
          elseif ($currentweek==2){$wk2=$wk2+$prevamount;}
          elseif ($currentweek==3){$wk3=$wk3+$prevamount;}
          elseif ($currentweek==4){$wk4=$wk4+$prevamount;}
          elseif ($currentweek==5){$wk5=$wk5+$prevamount;}
          elseif ($currentweek==6){$wk6=$wk6+$prevamount;}

          if ($weekday=="Thursday" || $day == $enddate){
             if ($credittotal!=0){$credittotal=money($credittotal);}
             $monthtotal=$credittotal+$monthtotal;
             
             $credittotal=0;
             $currentweek++;
          }

          $day=nextday($day);

          if ($weekday=="Sunday"){$weekday="Monday";}
          elseif ($weekday=="Saturday"){$weekday="Sunday";}
          elseif ($weekday=="Friday"){$weekday="Saturday";}
          elseif ($weekday=="Thursday"){$weekday="Friday";}
          elseif ($weekday=="Wednesday"){$weekday="Thursday";}
          elseif ($weekday=="Tuesday"){$weekday="Wednesday";}
          elseif ($weekday=="Monday"){$weekday="Tuesday";}
       }
       $day=$startdate;
       $weekday=$startweekday;

       $monthtotal=money($monthtotal);

       $num--;
       $currentweek=1;
    }

    $weektotal=$wk1+$wk2+$wk3+$wk4+$wk5+$wk6;
    $wk1=money($wk1);$saleswk1=$wk1;
    $wk2=money($wk2);$saleswk2=$wk2;
    $wk3=money($wk3);$saleswk3=$wk3;
    $wk4=money($wk4);$saleswk4=$wk4;
    $wk5=money($wk5);$saleswk5=$wk5;
    $wk6=money($wk6);$saleswk6=$wk6;
    $weektotal=money($weektotal);
    $salestotal=$weektotal;
    
    $wk2mtd=money($wk1+$wk2);
    $sales2mtd=$wk2mtd;
    $wk3mtd=money($wk2mtd+$wk3);
    $sales3mtd=$wk3mtd;
    $wk4mtd=money($wk3mtd+$wk4);
    $sales4mtd=$wk4mtd;
    $wk5mtd=money($wk4mtd+$wk5);
    $sales5mtd=$wk5mtd;
    $wk6mtd=money($wk5mtd+$wk6);
    $sales6mtd=$wk6mtd;

    $wksalesbgt=money($wksalesbgt);$wksalesbgt1 = number_format($wksalesbgt);
    $mtdsalesbgt=money($mtdsalesbgt);$mtdsalesbgt1 = number_format($mtdsalesbgt);
   
    $wkcosbgt=round($wkcosbgt/$wksalesbgt*100,1);
    $mtdcosbgt=round($mtdcosbgt/$mtdsalesbgt*100,1);
    
    $wklaborbgt=round($wklaborbgt/$wksalesbgt*100,1);
    $mtdlaborbgt=round($mtdlaborbgt/$mtdsalesbgt*100,1);
    
    $wktoebgt=round($wktoebgt/$wksalesbgt*100,1);
    $mtdtoebgt=round($mtdtoebgt/$mtdsalesbgt*100,1);
    
    $wknetpftbgt=money($wknetpftbgt);$wknetpftbgt = number_format($wknetpftbgt);
    $mtdnetpftbgt=money($mtdnetpftbgt);$mtdnetpftbgt = number_format($mtdnetpftbgt);

    if ($goweek==1){$with_commas = number_format($saleswk1);$message="$message <tr><td align=right>SALES:</td><td align=right>$$with_commas</td><td align=right></td><td align=right>$$wksalesbgt1</td><td align=right></td><td align=right>$$with_commas</td><td align=right></td><td align=right>$$mtdsalesbgt1</td><td align=right></td><td align=right>$$bgtotsales</td><td align=right></td></tr>";$comsales=$saleswk1+$comsales;$commtdsales=$commtdsales+$saleswk1;$distsales=$saleswk1+$distsales;$distmtdsales=$distmtdsales+$saleswk1;}
    elseif ($goweek==2){$with_commas = number_format($saleswk2);$with_commas2 = number_format($sales2mtd);$message="$message<tr><td align=right>SALES:</td><td align=right>$$with_commas</td><td align=right></td><td align=right>$$wksalesbgt1</td><td align=right></td><td align=right>$$with_commas2</td><td align=right></td><td align=right>$$mtdsalesbgt1</td><td align=right></td><td align=right>$$bgtotsales</td><td align=right></td></tr>";$comsales=$saleswk2+$comsales;$commtdsales=$commtdsales+$sales2mtd;$distsales=$saleswk2+$distsales;$distmtdsales=$distmtdsales+$sales2mtd;}
    elseif ($goweek==3){$with_commas = number_format($saleswk3);$with_commas2 = number_format($sales3mtd);$message="$message<tr><td align=right>SALES:</td><td align=right>$$with_commas</td><td align=right></td><td align=right>$$wksalesbgt1</td><td align=right></td><td align=right>$$with_commas2</td><td align=right></td><td align=right>$$mtdsalesbgt1</td><td align=right></td><td align=right>$$bgtotsales</td><td align=right></td></tr>";$comsales=$saleswk3+$comsales;$commtdsales=$commtdsales+$sales3mtd;$distsales=$saleswk3+$distsales;$distmtdsales=$distmtdsales+$sales3mtd;}
    elseif ($goweek==4){$with_commas = number_format($saleswk4);$with_commas2 = number_format($sales4mtd);$message="$message<tr><td align=right>SALES:</td><td align=right>$$with_commas</td><td align=right></td><td align=right>$$wksalesbgt1</td><td align=right></td><td align=right>$$with_commas2</td><td align=right></td><td align=right>$$mtdsalesbgt1</td><td align=right></td><td align=right>$$bgtotsales</td><td align=right></td></tr>";$comsales=$saleswk4+$comsales;$commtdsales=$commtdsales+$sales4mtd;$distsales=$saleswk4+$distsales;$distmtdsales=$distmtdsales+$sales4mtd;}
    elseif ($goweek==5){$with_commas = number_format($saleswk5);$with_commas2 = number_format($sales5mtd);$message="$message<tr><td align=right>SALES:</td><td align=right>$$with_commas</td><td align=right></td><td align=right>$$wksalesbgt1</td><td align=right></td><td align=right>$$with_commas2</td><td align=right></td><td align=right>$$mtdsalesbgt1</td><td align=right></td><td align=right>$$bgtotsales</td><td align=right></td></tr>";$comsales=$saleswk5+$comsales;$commtdsales=$commtdsales+$sales5mtd;$distsales=$saleswk5+$distsales;$distmtdsales=$distmtdsales+$sales5mtd;}
    elseif ($goweek==6){$with_commas = number_format($saleswk6);$with_commas2 = number_format($sales6mtd);$message="$message<tr><td align=right>SALES:</td><td align=right>$$with_commas</td><td align=right></td><td align=right>$$wksalesbgt1</td><td align=right></td><td align=right>$$with_commas2</td><td align=right></td><td align=right>$$mtdsalesbgt1</td><td align=right></td><td align=right>$$bgtotsales</td><td align=right></td></tr>";$comsales=$saleswk6+$comsales;$commtdsales=$commtdsales+$sales6mtd;$distsales=$saleswk6+$distsales;$distmtdsales=$distmtdsales+$sales6mtd;}

//////END SALES/START COS//////////////////////////////////////////////////////////////////////////////////////////////////////////
    $totalinv=0;

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM acct_payable WHERE businessid = '$businessid' AND cos = '1' AND cs = '0' AND ((is_deleted = '0') OR (is_deleted = '1' AND del_date >= '$startdate')) ORDER BY accountnum DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    $wk1=0;
    $wk2=0;
    $wk3=0;
    $wk4=0;
    $wk5=0;
    $wk6=0;
    $currentweek=1;
    $num--;

    while ($num>=0){

       $acct_payableid=@Treat_DB_ProxyOld::mysql_result($result,$num,"acct_payableid");
       $accountnum=@Treat_DB_ProxyOld::mysql_result($result,$num,"accountnum");
       $acct_name=@Treat_DB_ProxyOld::mysql_result($result,$num,"acct_name");
       $cs_type=@Treat_DB_ProxyOld::mysql_result($result,$num,"cs_type");
       $showaccount=substr($accountnum,0,5);

       $day=$startdate;  

       $prevamount=0;
       $aptotal=0;
       $monthtotal=0;

        if ($cs_type==0){
         while ($day<=$enddate){
         $prevamount=0;

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query5 = "SELECT * FROM apinvoice WHERE (businessid = '$businessid' AND date = '$day' AND transfer < '3') OR (vendor = '$businessid' AND date = '$day' AND transfer = '1')";
          $result5 = Treat_DB_ProxyOld::query($query5);
          $num5=Treat_DB_ProxyOld::mysql_numrows($result5);
          //mysql_close();

          $prevamount=0;
          $num5--;
          while ($num5>=0){

             $apinvoiceid=@Treat_DB_ProxyOld::mysql_result($result5,$num5,"apinvoiceid");
             $transfer=@Treat_DB_ProxyOld::mysql_result($result5,$num5,"transfer");
             $vendor=@Treat_DB_ProxyOld::mysql_result($result5,$num5,"vendor");
     
             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             if($transfer==1&&$vendor==$businessid){
                $query6 = "SELECT amount FROM apinvoicedetail WHERE apinvoiceid = '$apinvoiceid' AND tfr_acct = '$acct_payableid'";
             }
             else{
                $query6 = "SELECT amount FROM apinvoicedetail WHERE apinvoiceid = '$apinvoiceid' AND apaccountid = '$acct_payableid'";
             }
             $result6 = Treat_DB_ProxyOld::query($query6);
             $num6=Treat_DB_ProxyOld::mysql_numrows($result6);
             //mysql_close();

             $num6--;
             while ($num6>=0){
                $apamount=@Treat_DB_ProxyOld::mysql_result($result6,$num6,"amount");
                if ($transfer==1&&$vendor==$businessid){$apamount=$apamount*-1;}
                $prevamount=$prevamount+$apamount;
                $num6--;
             }

             $num5--;
          }

          $aptotal=$prevamount+$aptotal;

          if ($weekday=="Thursday" || $day == $enddate){
             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query65 = "SELECT amount FROM inventor2 WHERE acct_payableid = '$acct_payableid' AND date < '$day'";
             $result65 = Treat_DB_ProxyOld::query($query65);
             $num65=Treat_DB_ProxyOld::mysql_numrows($result65);
             //mysql_close();
             $beginamt=@Treat_DB_ProxyOld::mysql_result($result65,$num65-1,"amount");

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query65 = "SELECT amount FROM inventor2 WHERE acct_payableid = '$acct_payableid' AND date = '$day' ORDER by date DESC";
             $result65 = Treat_DB_ProxyOld::query($query65);
             $num65=Treat_DB_ProxyOld::mysql_numrows($result65);
             //mysql_close();
             $endamt=@Treat_DB_ProxyOld::mysql_result($result65,$num65-1,"amount");

             if ($beginamt!=0 && $endamt != 0){$aptotal=$beginamt+$aptotal-$endamt;}

             if ($currentweek==1){$wk1=$wk1+$aptotal;}
             elseif ($currentweek==2){$wk2=$wk2+$aptotal;}
             elseif ($currentweek==3){$wk3=$wk3+$aptotal;}
             elseif ($currentweek==4){$wk4=$wk4+$aptotal;}
             elseif ($currentweek==5){$wk5=$wk5+$aptotal;}
             elseif ($currentweek==6){$wk6=$wk6+$aptotal;}

             $prcnt=0;
             if ($currentweek==1&&$aptotal!=0&&$saleswk1!=0){$prcnt=($aptotal/$saleswk1)*100;$prcnt=round($prcnt,1);}
             elseif ($currentweek==2&&$aptotal!=0&&$saleswk2!=0){$prcnt=($aptotal/$saleswk2)*100;$prcnt=round($prcnt,1);}
             elseif ($currentweek==3&&$aptotal!=0&&$saleswk3!=0){$prcnt=($aptotal/$saleswk3)*100;$prcnt=round($prcnt,1);}
             elseif ($currentweek==4&&$aptotal!=0&&$saleswk4!=0){$prcnt=($aptotal/$saleswk4)*100;$prcnt=round($prcnt,1);}
             elseif ($currentweek==5&&$aptotal!=0&&$saleswk5!=0){$prcnt=($aptotal/$saleswk5)*100;$prcnt=round($prcnt,1);}
             elseif ($currentweek==6&&$aptotal!=0&&$saleswk6!=0){$prcnt=($aptotal/$saleswk6)*100;$prcnt=round($prcnt,1);}

             if ($aptotal!=0){$aptotal=money($aptotal);}
             $monthtotal=$aptotal+$monthtotal;
             $aptotal=0;
             $currentweek++;
          }

          $day=nextday($day);
          if ($weekday=="Sunday"){$weekday="Monday";}
          elseif ($weekday=="Saturday"){$weekday="Sunday";}
          elseif ($weekday=="Friday"){$weekday="Saturday";}
          elseif ($weekday=="Thursday"){$weekday="Friday";}
          elseif ($weekday=="Wednesday"){$weekday="Thursday";}
          elseif ($weekday=="Tuesday"){$weekday="Wednesday";}
          elseif ($weekday=="Monday"){$weekday="Tuesday";}
         }
        
        }
        elseif ($cs_type==9){
           //mysql_connect($dbhost,$username,$password);
           //@mysql_select_db($database) or die( "Unable to select database");
           $query7 = "SELECT * FROM controlsheet WHERE businessid = '$businessid' AND date = '$startdate' AND acct_payableid = '$acct_payableid'";
           $result7 = Treat_DB_ProxyOld::query($query7);
           $num7=Treat_DB_ProxyOld::mysql_numrows($result7);
           //mysql_close();

           if ($num7!=0){
              $csid=@Treat_DB_ProxyOld::mysql_result($result7,0,"csid");
              $amount1=@Treat_DB_ProxyOld::mysql_result($result7,0,"amount1");$amount1=money($amount1);
              $amount2=@Treat_DB_ProxyOld::mysql_result($result7,0,"amount2");$amount2=money($amount2);
              $amount3=@Treat_DB_ProxyOld::mysql_result($result7,0,"amount3");$amount3=money($amount3);
              $amount4=@Treat_DB_ProxyOld::mysql_result($result7,0,"amount4");$amount4=money($amount4);
              $amount5=@Treat_DB_ProxyOld::mysql_result($result7,0,"amount5");$amount5=money($amount5);
              $amount6=@Treat_DB_ProxyOld::mysql_result($result7,0,"amount6");$amount6=money($amount6);
           }
           else{
              $amount1=0;
              $amount2=0;
              $amount3=0;
              $amount4=0;
              $amount5=0;
              $amount6=0;
           }

           if ($weekcount==5){$monthtotal=$amount1+$amount2+$amount3+$amount4+$amount5;}
           elseif ($weekcount==6){$monthtotal=$amount1+$amount2+$amount3+$amount4+$amount5+$amount6;}
           else{$monthtotal=$amount1+$amount2+$amount3+$amount4;}
           $wk1=$wk1+$amount1;
           $wk2=$wk2+$amount2;
           $wk3=$wk3+$amount3;
           $wk4=$wk4+$amount4;
           if ($weekcount==5){$wk5=$wk5+$amount5;}
           if ($weekcount==6){$wk5=$wk5+$amount5;$wk6=$wk6+$amount6;}

           $amount1prcnt=0;
           $amount2prcnt=0;
           $amount3prcnt=0;
           $amount4prcnt=0;
           $amount5prcnt=0;
           $amount6prcnt=0;
           if ($amount1!=0&&$saleswk1!=0){$amount1prcnt=round(($amount1/$saleswk1)*100,1);}
           if ($amount2!=0&&$saleswk2!=0){$amount2prcnt=round(($amount2/$saleswk2)*100,1);}
           if ($amount3!=0&&$saleswk3!=0){$amount3prcnt=round(($amount3/$saleswk3)*100,1);}
           if ($amount4!=0&&$saleswk4!=0){$amount4prcnt=round(($amount4/$saleswk4)*100,1);}
           if ($amount5!=0&&$saleswk5!=0){$amount5prcnt=round(($amount5/$saleswk5)*100,1);}
           if ($amount6!=0&&$saleswk6!=0){$amount6prcnt=round(($amount6/$saleswk6)*100,1);}

           if ($amount1==0){$amount1=0;}
           if ($amount2==0){$amount2=0;}
           if ($amount3==0){$amount3=0;}
           if ($amount4==0){$amount4=0;}
           if ($amount5==0){$amount5=0;}
           if ($amount6==0){$amount6=0;}
          
        }
     elseif($cs_type==10){
       $day=$startdate;  
       $month=substr($day,5,2);
       $recuryear=substr($day,0,4);
       $leap = date("L");
       if ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10' || $month == '12'){$monthcount=31;}
       elseif($month=='04' || $month=='06' || $month=='09' || $month=='11'){$monthcount=30;}
       elseif($month=='02' && $leap == 0){$monthcount=28;}
       elseif($month=='02' && $leap == 1){$monthcount=29;}

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query12 = "SELECT `$month` FROM recur WHERE year = '$recuryear' AND apaccountid = '$acct_payableid'";
       $result12 = Treat_DB_ProxyOld::query($query12);
       //mysql_close();

       $recuramount=@Treat_DB_ProxyOld::mysql_result($result12,0,"$month");
       $recuramount=$recuramount/$monthcount;

       $prevamount=0;
       $aptotal=0;
       $monthtotal=0;
       while ($day<=$enddate){

          $prevamount=$prevamount+$recuramount;

          if ($currentweek==1){$wk1=$wk1+$recuramount;}
          elseif ($currentweek==2){$wk2=$wk2+$recuramount;}
          elseif ($currentweek==3){$wk3=$wk3+$recuramount;}
          elseif ($currentweek==4){$wk4=$wk4+$recuramount;}
          elseif ($currentweek==5){$wk5=$wk5+$recuramount;}
          elseif ($currentweek==6){$wk6=$wk6+$recuramount;}

          $prcnt=0;
          if ($currentweek==1&&$prevamount!=0&&$saleswk1!=0){$prcnt=($prevamount/$saleswk1)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==2&&$prevamount!=0&&$saleswk2!=0){$prcnt=($prevamount/$saleswk2)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==3&&$prevamount!=0&&$saleswk3!=0){$prcnt=($prevamount/$saleswk3)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==4&&$prevamount!=0&&$saleswk4!=0){$prcnt=($prevamount/$saleswk4)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==5&&$prevamount!=0&&$saleswk5!=0){$prcnt=($prevamount/$saleswk5)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==6&&$prevamount!=0&&$saleswk6!=0){$prcnt=($prevamount/$saleswk6)*100;$prcnt=round($prcnt,1);}

          if ($weekday=="Thursday" || $day == $enddate){
             $showamount=$prevamount;
             if ($showamount!=0){$showamount=money($showamount);}
             //echo "<td  align=right><font size=2>$$showamount</td><td align=right><font size=1 color=blue>$prcnt%</td>";
             $monthtotal=$prevamount+$monthtotal;
             $currentweek++;
             $prevamount=0;
          }

          $day=nextday($day);
          if ($weekday=="Sunday"){$weekday="Monday";}
          elseif ($weekday=="Saturday"){$weekday="Sunday";}
          elseif ($weekday=="Friday"){$weekday="Saturday";}
          elseif ($weekday=="Thursday"){$weekday="Friday";}
          elseif ($weekday=="Wednesday"){$weekday="Thursday";}
          elseif ($weekday=="Tuesday"){$weekday="Wednesday";}
          elseif ($weekday=="Monday"){$weekday="Tuesday";}
       }

     }
       
       $day=$startdate;
       $weekday=$startweekday;
////////////Inventory Adjustment Was Here///////////////////////////////////////
       

       $monthprcnt=0;
       $monthtotal=money($monthtotal);
       if ($monthtotal!=0&&$salestotal!=0){$monthprcnt=round(($monthtotal/$salestotal)*100,1);}
       
       $num--;
       $currentweek=1;
    }

    $weektotal=$wk1+$wk2+$wk3+$wk4+$wk5+$wk6;
    $wk1=money($wk1);
    $wk2=money($wk2);
    $wk3=money($wk3);
    $wk4=money($wk4);
    $wk5=money($wk5);
    $wk6=money($wk6);
    $weektotal=money($weektotal);
    
    if ($wk1!=0&&$saleswk1!=0){$coswk1prcnt=round($wk1/$saleswk1*100,1);} else{$coswk1prcnt=0;}
    if ($wk2!=0&&$saleswk2!=0){$coswk2prcnt=round($wk2/$saleswk2*100,1);} else{$coswk2prcnt=0;}
    if ($wk3!=0&&$saleswk3!=0){$coswk3prcnt=round($wk3/$saleswk3*100,1);} else{$coswk3prcnt=0;}
    if ($wk4!=0&&$saleswk4!=0){$coswk4prcnt=round($wk4/$saleswk4*100,1);} else{$coswk4prcnt=0;}
    if ($wk5!=0&&$saleswk5!=0){$coswk5prcnt=round($wk5/$saleswk5*100,1);} else{$coswk5prcnt=0;}
    if ($wk6!=0&&$saleswk6!=0){$coswk6prcnt=round($wk6/$saleswk6*100,1);} else{$coswk6prcnt=0;}
    if ($salestotal!=0&&$weektotal!=0){$monthprcnt=round($weektotal/$salestotal*100,1);} else{$monthprcnt=0;}

    $overallpcnt=$overallpcnt+$monthprcnt;
    
    $apwk1=$wk1;
    $apwk2=$wk2;
    $apwk3=$wk3;
    $apwk4=$wk4;
    $apwk5=$wk5;
    $apwk6=$wk6;
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    if ($weektotal!=0&&$salestotal!=0){$wkprcnt=round($weektotal/$salestotal*100,1);}
 
    $wk2mtd=money($wk1+$wk2);
    $wk3mtd=money($wk2mtd+$wk3);
    $wk4mtd=money($wk3mtd+$wk4);
    $wk5mtd=money($wk4mtd+$wk5);
    $wk6mtd=money($wk5mtd+$wk6);
    $commtdcos=$commtdcos+$wk1;

    if ($sales2mtd!=0){$coswk2prcntmtd=round($wk2mtd/$sales2mtd*100,1);}else{$coswk2prcntmtd=0;}
    if ($sales3mtd!=0){$coswk3prcntmtd=round($wk3mtd/$sales3mtd*100,1);}else{$coswk3prcntmtd=0;}
    if ($sales4mtd!=0){$coswk4prcntmtd=round($wk4mtd/$sales4mtd*100,1);}else{$coswk4prcntmtd=0;}
    if ($sales5mtd!=0){$coswk5prcntmtd=round($wk5mtd/$sales5mtd*100,1);}else{$coswk5prcntmtd=0;}
    if ($sales6mtd!=0){$coswk6prcntmtd=round($wk6mtd/$sales6mtd*100,1);}else{$coswk6prcntmtd=0;}

    $showtab="";
    if ($goweek==1&&strlen($coswk1prcnt)<5){$showtab="\t";}
    elseif ($goweek==2&&strlen($coswk2prcnt)<5){$showtab="\t";}
    elseif ($goweek==3&&strlen($coswk3prcnt)<5){$showtab="\t";}
    elseif ($goweek==4&&strlen($coswk4prcnt)<5){$showtab="\t";}
    elseif ($goweek==5&&strlen($coswk5prcnt)<5){$showtab="\t";}
    elseif ($goweek==6&&strlen($coswk6prcnt)<5){$showtab="\t";}
    else{$showtab="";}

    if ($goweek==1){$message="$message<tr><td align=right>COS:</td><td align=right>$coswk1prcnt%</td><td align=right></td><td align=right>$wkcosbgt%</td><td align=right></td><td align=right>$coswk1prcnt%</td><td align=right></td><td align=right>$mtdcosbgt%</td><td align=right></td><td align=right>$$bgtcos</td><td align=right>$cosprcnt%</td></tr>";$comcos=$comcos+$wk1;$commtdcos=$commtdcos+$wk1;$distcos=$distcos+$wk1;$distmtdcos=$distmtdcos+$wk1;}
    elseif ($goweek==2){$message="$message<tr><td align=right>COS:</td><td align=right>$coswk2prcnt%</td><td align=right></td><td align=right>$wkcosbgt%</td><td align=right></td><td align=right>$coswk2prcntmtd%</td><td align=right></td><td align=right>$mtdcosbgt%</td><td align=right></td><td align=right>$$bgtcos</td><td align=right>$cosprcnt%</td></tr>";$comcos=$comcos+$wk2;$commtdcos=$commtdcos+$wk2mtd;$distcos=$distcos+$wk2;$distmtdcos=$distmtdcos+$wk2mtd;}
    elseif ($goweek==3){$message="$message<tr><td align=right>COS:</td><td align=right>$coswk3prcnt%</td><td align=right></td><td align=right>$wkcosbgt%</td><td align=right></td><td align=right>$coswk3prcntmtd%</td><td align=right></td><td align=right>$mtdcosbgt%</td><td align=right></td><td align=right>$$bgtcos</td><td align=right>$cosprcnt%</td></tr>";$comcos=$comcos+$wk3;$commtdcos=$commtdcos+$wk3mtd;$distcos=$distcos+$wk3;$distmtdcos=$distmtdcos+$wk3mtd;}
    elseif ($goweek==4){$message="$message<tr><td align=right>COS:</td><td align=right>$coswk4prcnt%</td><td align=right></td><td align=right>$wkcosbgt%</td><td align=right></td><td align=right>$coswk4prcntmtd%</td><td align=right></td><td align=right>$mtdcosbgt%</td><td align=right></td><td align=right>$$bgtcos</td><td align=right>$cosprcnt%</td></tr>";$comcos=$comcos+$wk4;$commtdcos=$commtdcos+$wk4mtd;$distcos=$distcos+$wk4;$distmtdcos=$distmtdcos+$wk4mtd;}
    elseif ($goweek==5){$message="$message<tr><td align=right>COS:</td><td align=right>$coswk5prcnt%</td><td align=right></td><td align=right>$wkcosbgt%</td><td align=right></td><td align=right>$coswk5prcntmtd%</td><td align=right></td><td align=right>$mtdcosbgt%</td><td align=right></td><td align=right>$$bgtcos</td><td align=right>$cosprcnt%</td></tr>";$comcos=$comcos+$wk5;$commtdcos=$commtdcos+$wk5mtd;$distcos=$distcos+$wk5;$distmtdcos=$distmtdcos+$wk5mtd;}
    elseif ($goweek==6){$message="$message<tr><td align=right>COS:</td><td align=right>$coswk6prcnt%</td><td align=right></td><td align=right>$wkcosbgt%</td><td align=right></td><td align=right>$coswk6prcntmtd%</td><td align=right></td><td align=right>$mtdcosbgt%</td><td align=right></td><td align=right>$$bgtcos</td><td align=right>$cosprcnt%</td></tr>";$comcos=$comcos+$wk6;$commtdcos=$commtdcos+$wk6mtd;$distcos=$distcos+$wk6;$distmtdcos=$distmtdcos+$wk6mtd;}

//////END COS/START LABOR//////////////////////////////////////////////////////////////////////////////////////////////////////////

    $benwk1=0;
    $benwk2=0;
    $benwk3=0;
    $benwk4=0;
    $benwk5=0;
    $benwk6=0;
    $wk1=0;
    $wk2=0;
    $wk3=0;
    $wk4=0;
    $wk5=0;
    $wk6=0;
    $currentweek=1;

       

       $day=$startdate;  

       $prevamount=0;
       $aptotal=0;
       $monthtotal=0;
       while ($day<=$enddate){

          $prevamount=0;

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query5 = "SELECT * FROM stats WHERE businessid = '$businessid' AND date = '$day'";
          $result5 = Treat_DB_ProxyOld::query($query5);
          $num5=Treat_DB_ProxyOld::mysql_numrows($result5);
          //mysql_close();

          $prevamount=0;
          $num5--;
          while ($num5>=0){

             $labor=@Treat_DB_ProxyOld::mysql_result($result5,$num5,"labor");
     
             $prevamount=$prevamount+$labor;

             $num5--;
          }

          $aptotal=$prevamount+$aptotal;

          if ($currentweek==1){$wk1=$wk1+$prevamount;$benwk1=$benwk1+$prevamount;}
          elseif ($currentweek==2){$wk2=$wk2+$prevamount;$benwk2=$benwk2+$prevamount;}
          elseif ($currentweek==3){$wk3=$wk3+$prevamount;$benwk3=$benwk3+$prevamount;}
          elseif ($currentweek==4){$wk4=$wk4+$prevamount;$benwk4=$benwk4+$prevamount;}
          elseif ($currentweek==5){$wk5=$wk5+$prevamount;$benwk5=$benwk5+$prevamount;}
          elseif ($currentweek==6){$wk6=$wk6+$prevamount;$benwk6=$benwk6+$prevamount;}

          $prcnt=0;
          if ($currentweek==1&&$aptotal!=0&&$saleswk1!=0){$prcnt=($aptotal/$saleswk1)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==2&&$aptotal!=0&&$saleswk2!=0){$prcnt=($aptotal/$saleswk2)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==3&&$aptotal!=0&&$saleswk3!=0){$prcnt=($aptotal/$saleswk3)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==4&&$aptotal!=0&&$saleswk4!=0){$prcnt=($aptotal/$saleswk4)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==5&&$aptotal!=0&&$saleswk5!=0){$prcnt=($aptotal/$saleswk5)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==6&&$aptotal!=0&&$saleswk6!=0){$prcnt=($aptotal/$saleswk6)*100;$prcnt=round($prcnt,1);}

          if ($weekday=="Thursday" || $day == $enddate){
             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query55 = "SELECT * FROM payroll_date WHERE date = '$day'";
             $result55 = Treat_DB_ProxyOld::query($query55);
             $num55=Treat_DB_ProxyOld::mysql_numrows($result55);
             //mysql_close();

             if ($num55==1){
                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query56 = "SELECT reg2,ot2 FROM payroll WHERE date = '$day' AND unit_num = '$unit'";
                $result56 = Treat_DB_ProxyOld::query($query56);
                $num56=Treat_DB_ProxyOld::mysql_numrows($result56);
                //mysql_close();
   
                $num56--;
                $totalgross=0;
                while($num56>=0){
                   $reg2=0;$ot2=0;
                   $reg2=@Treat_DB_ProxyOld::mysql_result($result56,$num56,"reg2");
                   $ot2=@Treat_DB_ProxyOld::mysql_result($result56,$num56,"ot2");
                   $totalgross=$totalgross+$reg2+$ot2;
                   $num56--;
                }
                
                $labortemp=$day;
                for($counter=1;$counter<14;$counter++){$labortemp=prevday($labortemp);}

                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query56 = "SELECT labor FROM stats WHERE businessid = '$businessid' AND date >= '$labortemp' AND date <= '$day'";
                $result56 = Treat_DB_ProxyOld::query($query56);
                $num56=Treat_DB_ProxyOld::mysql_numrows($result56);
                //mysql_close();
   
                $num56--;
                $totallabor=0;
                while($num56>=0){
                   $labor=@Treat_DB_ProxyOld::mysql_result($result56,$num56,"labor");
                   $totallabor=$totallabor+$labor;
                   $num56--;
                }
                
                $payroll_adj=money($totalgross-$totallabor);
                $monthtotal=$monthtotal+$payroll_adj;

                if ($currentweek==1){$wk1=$wk1+$payroll_adj;$benwk1=$benwk1+$payroll_adj;}
                elseif ($currentweek==2){$wk2=$wk2+$payroll_adj;$benwk2=$benwk2+$payroll_adj;}
                elseif ($currentweek==3){$wk3=$wk3+$payroll_adj;$benwk3=$benwk3+$payroll_adj;}
                elseif ($currentweek==4){$wk4=$wk4+$payroll_adj;$benwk4=$benwk4+$payroll_adj;}
                elseif ($currentweek==5){$wk5=$wk5+$payroll_adj;$benwk5=$benwk5+$payroll_adj;}
                elseif ($currentweek==6){$wk6=$wk6+$payroll_adj;$benwk6=$benwk6+$payroll_adj;}
             }
             else{$payroll_adj=0;}

             if ($aptotal!=0){$aptotal=money($aptotal);}
             $monthtotal=$aptotal+$monthtotal;
             $aptotal=0;
             $currentweek++;
          }

          $day=nextday($day);
          if ($weekday=="Sunday"){$weekday="Monday";}
          elseif ($weekday=="Saturday"){$weekday="Sunday";}
          elseif ($weekday=="Friday"){$weekday="Saturday";}
          elseif ($weekday=="Thursday"){$weekday="Friday";}
          elseif ($weekday=="Wednesday"){$weekday="Thursday";}
          elseif ($weekday=="Tuesday"){$weekday="Wednesday";}
          elseif ($weekday=="Monday"){$weekday="Tuesday";}

       }

       $day=$startdate;
       $weekday=$startweekday;

       $prcnt=0;
       $monthtotal=money($monthtotal);
       if ($salestotal!=0){$prcnt=round($monthtotal/$salestotal*100,1);}
       else{$prcnt=0;}
       
       $currentweek=1;


    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM acct_payable WHERE businessid = '$businessid' AND cos = '0' AND cs = '0' AND ((cs_type > 3 AND cs_type < 6) OR cs_type = '8' OR cs_type = '11' OR cs_type = '13') AND ((is_deleted = '0') OR (is_deleted = '1' AND del_date >= '$startdate')) ORDER BY accountnum DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    $num--;
    while ($num>=0){

       $acct_payableid=@Treat_DB_ProxyOld::mysql_result($result,$num,"acct_payableid");
       $accountnum=@Treat_DB_ProxyOld::mysql_result($result,$num,"accountnum");
       $acct_name=@Treat_DB_ProxyOld::mysql_result($result,$num,"acct_name");
       $cs_type=@Treat_DB_ProxyOld::mysql_result($result,$num,"cs_type");
       $daily_fixed=@Treat_DB_ProxyOld::mysql_result($result,$num,"daily_fixed");
       $daily_labor=@Treat_DB_ProxyOld::mysql_result($result,$num,"daily_labor");
       $showaccount=substr($accountnum,0,5);

       if ($cs_type==5){$acct_name="<a style=$style target='_blank' href='edit_daily_amount.php?acct_payableid=$acct_payableid'><font size=2 color=gray>$acct_name</font></a>";}

       
    if ($cs_type==4){
       $day=$startdate;  

       $prevamount=0;
       $aptotal=0;
       $monthtotal=0;
       while ($day<=$enddate){

          $prevamount=0;

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query5 = "SELECT * FROM apinvoice WHERE (businessid = '$businessid' AND date = '$day' AND transfer != '3') OR (vendor = '$businessid' AND date = '$day' AND transfer = '1')";
          $result5 = Treat_DB_ProxyOld::query($query5);
          $num5=Treat_DB_ProxyOld::mysql_numrows($result5);
          //mysql_close();

          $prevamount=0;
          $num5--;
          while ($num5>=0){

             $apinvoiceid=@Treat_DB_ProxyOld::mysql_result($result5,$num5,"apinvoiceid");
             $transfer=@Treat_DB_ProxyOld::mysql_result($result5,$num5,"transfer");
             $vendor=@Treat_DB_ProxyOld::mysql_result($result5,$num5,"vendor");
     
             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             if($transfer==1&&$vendor==$businessid){
                $query6 = "SELECT amount FROM apinvoicedetail WHERE apinvoiceid = '$apinvoiceid' AND tfr_acct = '$acct_payableid'";
             }
             else{
                $query6 = "SELECT amount FROM apinvoicedetail WHERE apinvoiceid = '$apinvoiceid' AND apaccountid = '$acct_payableid'";
             }
             $result6 = Treat_DB_ProxyOld::query($query6);
             $num6=Treat_DB_ProxyOld::mysql_numrows($result6);
             //mysql_close();

             $num6--;
             while ($num6>=0){
                $apamount=@Treat_DB_ProxyOld::mysql_result($result6,$num6,"amount");
                if ($transfer==1&&$vendor==$businessid){$apamount=$apamount*-1;}
                $prevamount=$prevamount+$apamount;
                $num6--;
             }

             $num5--;
          }

          $aptotal=$prevamount+$aptotal;

          if ($currentweek==1){$wk1=$wk1+$prevamount;if($showaccount!="50900"){$benwk1+=$prevamount;}}
          elseif ($currentweek==2){$wk2=$wk2+$prevamount;if($showaccount!="50900"){$benwk2+=$prevamount;}}
          elseif ($currentweek==3){$wk3=$wk3+$prevamount;if($showaccount!="50900"){$benwk3+=$prevamount;}}
          elseif ($currentweek==4){$wk4=$wk4+$prevamount;if($showaccount!="50900"){$benwk4+=$prevamount;}}
          elseif ($currentweek==5){$wk5=$wk5+$prevamount;if($showaccount!="50900"){$benwk5+=$prevamount;}}
          elseif ($currentweek==6){$wk6=$wk6+$prevamount;if($showaccount!="50900"){$benwk6+=$prevamount;}}

          $prcnt=0;
          if ($currentweek==1&&$aptotal!=0&&$saleswk1!=0){$prcnt=($aptotal/$saleswk1)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==2&&$aptotal!=0&&$saleswk2!=0){$prcnt=($aptotal/$saleswk2)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==3&&$aptotal!=0&&$saleswk3!=0){$prcnt=($aptotal/$saleswk3)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==4&&$aptotal!=0&&$saleswk4!=0){$prcnt=($aptotal/$saleswk4)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==5&&$aptotal!=0&&$saleswk5!=0){$prcnt=($aptotal/$saleswk5)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==6&&$aptotal!=0&&$saleswk6!=0){$prcnt=($aptotal/$saleswk6)*100;$prcnt=round($prcnt,1);}

          if ($weekday=="Thursday" || $day == $enddate){
             if ($aptotal!=0){$aptotal=money($aptotal);}
             $monthtotal=$aptotal+$monthtotal;
             $aptotal=0;
             $currentweek++;
          }

          $day=nextday($day);
          if ($weekday=="Sunday"){$weekday="Monday";}
          elseif ($weekday=="Saturday"){$weekday="Sunday";}
          elseif ($weekday=="Friday"){$weekday="Saturday";}
          elseif ($weekday=="Thursday"){$weekday="Friday";}
          elseif ($weekday=="Wednesday"){$weekday="Thursday";}
          elseif ($weekday=="Tuesday"){$weekday="Wednesday";}
          elseif ($weekday=="Monday"){$weekday="Tuesday";}
       }
     }
     elseif($cs_type==5){
       $day=$startdate;  

       $day=$startdate;  

       $prevamount=0;
       $aptotal=0;
       $monthtotal=0;
       while ($day<=$enddate){

          $prevamount=$prevamount+$daily_labor;

          if ($currentweek==1){$wk1=$wk1+$daily_labor;$benwk1=$benwk1+$daily_labor;}
          elseif ($currentweek==2){$wk2=$wk2+$daily_labor;$benwk2=$benwk2+$daily_labor;}
          elseif ($currentweek==3){$wk3=$wk3+$daily_labor;$benwk3=$benwk3+$daily_labor;}
          elseif ($currentweek==4){$wk4=$wk4+$daily_labor;$benwk4=$benwk4+$daily_labor;}
          elseif ($currentweek==5){$wk5=$wk5+$daily_labor;$benwk5=$benwk5+$daily_labor;}
          elseif ($currentweek==6){$wk6=$wk6+$daily_labor;$benwk6=$benwk6+$daily_labor;}

          $prcnt=0;
          if ($currentweek==1&&$prevamount!=0&&$saleswk1!=0){$prcnt=($prevamount/$saleswk1)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==2&&$prevamount!=0&&$saleswk2!=0){$prcnt=($prevamount/$saleswk2)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==3&&$prevamount!=0&&$saleswk3!=0){$prcnt=($prevamount/$saleswk3)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==4&&$prevamount!=0&&$saleswk4!=0){$prcnt=($prevamount/$saleswk4)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==5&&$prevamount!=0&&$saleswk5!=0){$prcnt=($prevamount/$saleswk5)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==6&&$prevamount!=0&&$saleswk6!=0){$prcnt=($prevamount/$saleswk6)*100;$prcnt=round($prcnt,1);}

          if ($weekday=="Thursday" || $day == $enddate){
             if ($prevamount!=0){$prevamount=money($prevamount);}
             $monthtotal=$prevamount+$monthtotal;
             $currentweek++;
             $prevamount=0;
          }

          $day=nextday($day);
          if ($weekday=="Sunday"){$weekday="Monday";}
          elseif ($weekday=="Saturday"){$weekday="Sunday";}
          elseif ($weekday=="Friday"){$weekday="Saturday";}
          elseif ($weekday=="Thursday"){$weekday="Friday";}
          elseif ($weekday=="Wednesday"){$weekday="Thursday";}
          elseif ($weekday=="Tuesday"){$weekday="Wednesday";}
          elseif ($weekday=="Monday"){$weekday="Tuesday";}
       }
     }
     elseif($cs_type==8){
       $day=$startdate;  
       $month=substr($day,5,2);
       $leap = date("L");
       if ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10' || $month == '12'){$monthcount=31;}
       elseif($month=='04' || $month=='06' || $month=='09' || $month=='11'){$monthcount=30;}
       elseif($month=='02' && $leap == 0){$monthcount=28;}
       elseif($month=='02' && $leap == 1){$monthcount=29;}

       $daily_fixed=$daily_fixed/$monthcount;

       $prevamount=0;
       $aptotal=0;
       $monthtotal=0;
       while ($day<=$enddate){

          $prevamount=$prevamount+$daily_fixed;

          if ($currentweek==1){$wk1=$wk1+$daily_fixed;$benwk1=$benwk1+$daily_fixed;}
          elseif ($currentweek==2){$wk2=$wk2+$daily_fixed;$benwk2=$benwk2+$daily_fixed;}
          elseif ($currentweek==3){$wk3=$wk3+$daily_fixed;$benwk3=$benwk3+$daily_fixed;}
          elseif ($currentweek==4){$wk4=$wk4+$daily_fixed;$benwk4=$benwk4+$daily_fixed;}
          elseif ($currentweek==5){$wk5=$wk5+$daily_fixed;$benwk5=$benwk5+$daily_fixed;}
          elseif ($currentweek==6){$wk6=$wk6+$daily_fixed;$benwk6=$benwk6+$daily_fixed;}

          $prcnt=0;
          if ($currentweek==1&&$prevamount!=0&&$saleswk1!=0){$prcnt=($prevamount/$saleswk1)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==2&&$prevamount!=0&&$saleswk2!=0){$prcnt=($prevamount/$saleswk2)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==3&&$prevamount!=0&&$saleswk3!=0){$prcnt=($prevamount/$saleswk3)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==4&&$prevamount!=0&&$saleswk4!=0){$prcnt=($prevamount/$saleswk4)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==5&&$prevamount!=0&&$saleswk5!=0){$prcnt=($prevamount/$saleswk5)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==6&&$prevamount!=0&&$saleswk6!=0){$prcnt=($prevamount/$saleswk6)*100;$prcnt=round($prcnt,1);}

          if ($weekday=="Thursday" || $day == $enddate){
             $showamount=$prevamount;
             if ($showamount!=0){$showamount=money($showamount);}
             $monthtotal=$prevamount+$monthtotal;
             $currentweek++;
             $prevamount=0;
          }

          $day=nextday($day);
          if ($weekday=="Sunday"){$weekday="Monday";}
          elseif ($weekday=="Saturday"){$weekday="Sunday";}
          elseif ($weekday=="Friday"){$weekday="Saturday";}
          elseif ($weekday=="Thursday"){$weekday="Friday";}
          elseif ($weekday=="Wednesday"){$weekday="Thursday";}
          elseif ($weekday=="Tuesday"){$weekday="Wednesday";}
          elseif ($weekday=="Monday"){$weekday="Tuesday";}
       }

     }
     elseif($cs_type==11){
       $day=$startdate;  
       $month=substr($day,5,2);
       $recuryear=substr($day,0,4);
       $leap = date("L");
       if ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10' || $month == '12'){$monthcount=31;}
       elseif($month=='04' || $month=='06' || $month=='09' || $month=='11'){$monthcount=30;}
       elseif($month=='02' && $leap == 0){$monthcount=28;}
       elseif($month=='02' && $leap == 1){$monthcount=29;}

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query12 = "SELECT `$month` FROM recur WHERE year = '$recuryear' AND apaccountid = '$acct_payableid'";
       $result12 = Treat_DB_ProxyOld::query($query12);
       //mysql_close();

       $recuramount=@Treat_DB_ProxyOld::mysql_result($result12,0,"$month");
       $recuramount=$recuramount/$monthcount;

       $prevamount=0;
       $aptotal=0;
       $monthtotal=0;
       while ($day<=$enddate){

          $prevamount=$prevamount+$recuramount;

          if ($currentweek==1){$wk1=$wk1+$recuramount;}
          elseif ($currentweek==2){$wk2=$wk2+$recuramount;}
          elseif ($currentweek==3){$wk3=$wk3+$recuramount;}
          elseif ($currentweek==4){$wk4=$wk4+$recuramount;}
          elseif ($currentweek==5){$wk5=$wk5+$recuramount;}
          elseif ($currentweek==6){$wk6=$wk6+$recuramount;}

          $prcnt=0;
          if ($currentweek==1&&$prevamount!=0&&$saleswk1!=0){$prcnt=($prevamount/$saleswk1)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==2&&$prevamount!=0&&$saleswk2!=0){$prcnt=($prevamount/$saleswk2)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==3&&$prevamount!=0&&$saleswk3!=0){$prcnt=($prevamount/$saleswk3)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==4&&$prevamount!=0&&$saleswk4!=0){$prcnt=($prevamount/$saleswk4)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==5&&$prevamount!=0&&$saleswk5!=0){$prcnt=($prevamount/$saleswk5)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==6&&$prevamount!=0&&$saleswk6!=0){$prcnt=($prevamount/$saleswk6)*100;$prcnt=round($prcnt,1);}

          if ($weekday=="Thursday" || $day == $enddate){
             $showamount=$prevamount;
             if ($showamount!=0){$showamount=money($showamount);}
             //echo "<td  align=right><font size=2>$$showamount</td><td align=right><font size=1 color=blue>$prcnt%</td>";
             $monthtotal=$prevamount+$monthtotal;
             $currentweek++;
             $prevamount=0;
          }

          $day=nextday($day);
          if ($weekday=="Sunday"){$weekday="Monday";}
          elseif ($weekday=="Saturday"){$weekday="Sunday";}
          elseif ($weekday=="Friday"){$weekday="Saturday";}
          elseif ($weekday=="Thursday"){$weekday="Friday";}
          elseif ($weekday=="Wednesday"){$weekday="Thursday";}
          elseif ($weekday=="Tuesday"){$weekday="Wednesday";}
          elseif ($weekday=="Monday"){$weekday="Tuesday";}
       }

     }
     elseif($cs_type==13){
       $day=$startdate;  
       $month=substr($day,5,2);
       $recuryear=substr($day,0,4);
       $leap = date("L");
       if ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10' || $month == '12'){$monthcount=31;}
       elseif($month=='04' || $month=='06' || $month=='09' || $month=='11'){$monthcount=30;}
       elseif($month=='02' && $leap == 0){$monthcount=28;}
       elseif($month=='02' && $leap == 1){$monthcount=29;}

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query12 = "SELECT `$month` FROM recur WHERE year = '$recuryear' AND apaccountid = '$acct_payableid'";
       $result12 = Treat_DB_ProxyOld::query($query12);
       //mysql_close();

       $recuramount=@Treat_DB_ProxyOld::mysql_result($result12,0,"$month");
       $recuramount=$recuramount/$monthcount;

       $prevamount=0;
       $aptotal=0;
       $monthtotal=0;
       while ($day<=$enddate){

          $prevamount=$prevamount+$recuramount;

          if ($currentweek==1){$wk1=$wk1+$recuramount;$benwk1=$benwk1+$recuramount;}
          elseif ($currentweek==2){$wk2=$wk2+$recuramount;$benwk2=$benwk2+$recuramount;}
          elseif ($currentweek==3){$wk3=$wk3+$recuramount;$benwk3=$benwk3+$recuramount;}
          elseif ($currentweek==4){$wk4=$wk4+$recuramount;$benwk4=$benwk4+$recuramount;}
          elseif ($currentweek==5){$wk5=$wk5+$recuramount;$benwk5=$benwk5+$recuramount;}
          elseif ($currentweek==6){$wk6=$wk6+$recuramount;$benwk6=$benwk6+$recuramount;}

          $prcnt=0;
          if ($currentweek==1&&$prevamount!=0&&$saleswk1!=0){$prcnt=($prevamount/$saleswk1)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==2&&$prevamount!=0&&$saleswk2!=0){$prcnt=($prevamount/$saleswk2)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==3&&$prevamount!=0&&$saleswk3!=0){$prcnt=($prevamount/$saleswk3)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==4&&$prevamount!=0&&$saleswk4!=0){$prcnt=($prevamount/$saleswk4)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==5&&$prevamount!=0&&$saleswk5!=0){$prcnt=($prevamount/$saleswk5)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==6&&$prevamount!=0&&$saleswk6!=0){$prcnt=($prevamount/$saleswk6)*100;$prcnt=round($prcnt,1);}

          if ($weekday=="Thursday" || $day == $enddate){
             $showamount=$prevamount;
             if ($showamount!=0){$showamount=money($showamount);}
             //echo "<td  align=right><font size=2>$$showamount</td><td align=right><font size=1 color=blue>$prcnt%</td>";
             $monthtotal=$prevamount+$monthtotal;
             $currentweek++;
             $prevamount=0;
          }

          $day=nextday($day);
          if ($weekday=="Sunday"){$weekday="Monday";}
          elseif ($weekday=="Saturday"){$weekday="Sunday";}
          elseif ($weekday=="Friday"){$weekday="Saturday";}
          elseif ($weekday=="Thursday"){$weekday="Friday";}
          elseif ($weekday=="Wednesday"){$weekday="Thursday";}
          elseif ($weekday=="Tuesday"){$weekday="Wednesday";}
          elseif ($weekday=="Monday"){$weekday="Tuesday";}
       }

     }
       $day=$startdate;
       $weekday=$startweekday;

       $monthprcnt=0;
       $monthtotal=money($monthtotal);
       if ($monthtotal!=0&&$salestotal!=0){$monthprcnt=round(($monthtotal/$salestotal)*100,1);}
       
     $num--;
     $currentweek=1;
   }
/////////////////////TRANSFER LABOR/////////////////////////////////
        

        //mysql_connect($dbhost,$username,$password);
        //@mysql_select_db($database) or die( "Unable to select database");
        $query7 = "SELECT * FROM controlsheet WHERE businessid = '$businessid' AND date = '$startdate' AND type = '1'";
        $result7 = Treat_DB_ProxyOld::query($query7);
        $num7=Treat_DB_ProxyOld::mysql_numrows($result7);
        //mysql_close();

        if ($num7!=0){
           $csid=@Treat_DB_ProxyOld::mysql_result($result7,0,"csid");
           $amount1=@Treat_DB_ProxyOld::mysql_result($result7,0,"amount1");$amount1=money($amount1);
           $amount2=@Treat_DB_ProxyOld::mysql_result($result7,0,"amount2");$amount2=money($amount2);
           $amount3=@Treat_DB_ProxyOld::mysql_result($result7,0,"amount3");$amount3=money($amount3);
           $amount4=@Treat_DB_ProxyOld::mysql_result($result7,0,"amount4");$amount4=money($amount4);
           $amount5=@Treat_DB_ProxyOld::mysql_result($result7,0,"amount5");$amount5=money($amount5);
           $amount6=@Treat_DB_ProxyOld::mysql_result($result7,0,"amount6");$amount6=money($amount6);
        }
        else{
           $amount1=0;
           $amount2=0;
           $amount3=0;
           $amount4=0;
           $amount5=0;
           $amount6=0;
        }


        if ($weekcount==5){$monthtotal=money($amount1+$amount2+$amount3+$amount4+$amount5);}
        elseif ($weekcount==6){$monthtotal=money($amount1+$amount2+$amount3+$amount4+$amount5+$amount6);}
        else{$monthtotal=money($amount1+$amount2+$amount3+$amount4);}
        $wk1=$wk1+$amount1;
        $wk2=$wk2+$amount2;
        $wk3=$wk3+$amount3;
        $wk4=$wk4+$amount4;
        if ($weekcount==5){$wk5=$wk5+$amount5;}
        if ($weekcount==6){$wk5=$wk5+$amount5;$wk6=$wk6+$amount6;}

        $amount1prcnt=0;
        $amount2prcnt=0;
        $amount3prcnt=0;
        $amount4prcnt=0;
        $amount5prcnt=0;
        $amount6prcnt=0;
        if ($amount1!=0&&$saleswk1!=0){$amount1prcnt=round(($amount1/$saleswk1)*100,1);}
        if ($amount2!=0&&$saleswk2!=0){$amount2prcnt=round(($amount2/$saleswk2)*100,1);}
        if ($amount3!=0&&$saleswk3!=0){$amount3prcnt=round(($amount3/$saleswk3)*100,1);}
        if ($amount4!=0&&$saleswk4!=0){$amount4prcnt=round(($amount4/$saleswk4)*100,1);}
        if ($amount5!=0&&$saleswk5!=0){$amount5prcnt=round(($amount5/$saleswk5)*100,1);}
        if ($amount6!=0&&$saleswk6!=0){$amount6prcnt=round(($amount6/$saleswk6)*100,1);}
        if ($monthtotal!=0&&$salestotal!=0){$monthprcnt=round(($monthtotal/$salestotal)*100,1);}

//////////////////BENEFITS//////////////////////////////////////
       $benwk1=money($benefitprcnt/100*$benwk1);$wk1=$wk1+$benwk1;
       $benwk2=money($benefitprcnt/100*$benwk2);$wk2=$wk2+$benwk2;
       $benwk3=money($benefitprcnt/100*$benwk3);$wk3=$wk3+$benwk3;
       $benwk4=money($benefitprcnt/100*$benwk4);$wk4=$wk4+$benwk4;
       $benwk5=money($benefitprcnt/100*$benwk5);$wk5=$wk5+$benwk5;
       $benwk6=money($benefitprcnt/100*$benwk6);$wk6=$wk6+$benwk6;
       if ($benwk1=="0.00"){$benwk1=0;}
       if ($benwk2=="0.00"){$benwk2=0;}
       if ($benwk3=="0.00"){$benwk3=0;}
       if ($benwk4=="0.00"){$benwk4=0;}
       if ($benwk5=="0.00"){$benwk5=0;}
       if ($benwk6=="0.00"){$benwk6=0;}
       $totalben=money($benwk1+$benwk2+$benwk3+$benwk4+$benwk5+$benwk6);
//////////////////END BENEFITS/////////////////////////////////

    $apwk1=$apwk1+$wk1;
    $apwk2=$apwk2+$wk2;
    $apwk3=$apwk3+$wk3;
    $apwk4=$apwk4+$wk4;
    $apwk5=$apwk5+$wk5;
    $apwk6=$apwk6+$wk6;

    $weektotal=$wk1+$wk2+$wk3+$wk4+$wk5+$wk6;
    $wk1=money($wk1);
    $wk2=money($wk2);
    $wk3=money($wk3);
    $wk4=money($wk4);
    $wk5=money($wk5);
    $wk6=money($wk6);
    $weektotal=money($weektotal);
    
    if ($wk1!=0&&$saleswk1!=0){$lbrwk1prcnt=round($wk1/$saleswk1*100,1);} else{$lbrwk1prcnt=0;}
    if ($wk2!=0&&$saleswk2!=0){$lbrwk2prcnt=round($wk2/$saleswk2*100,1);} else{$lbrwk2prcnt=0;}
    if ($wk3!=0&&$saleswk3!=0){$lbrwk3prcnt=round($wk3/$saleswk3*100,1);} else{$lbrwk3prcnt=0;}
    if ($wk4!=0&&$saleswk4!=0){$lbrwk4prcnt=round($wk4/$saleswk4*100,1);} else{$lbrwk4prcnt=0;}
    if ($wk5!=0&&$saleswk5!=0){$lbrwk5prcnt=round($wk5/$saleswk5*100,1);} else{$lbrwk5prcnt=0;}
    if ($wk6!=0&&$saleswk6!=0){$lbrwk6prcnt=round($wk6/$saleswk6*100,1);} else{$lbrwk6prcnt=0;}
    if ($salestotal!=0){$monthprcnt=round($weektotal/$salestotal*100,1);} else{$monthprcnt=0;}

    $overallpcnt=$overallpcnt+$monthprcnt;

    $wk2mtd=money($wk1+$wk2);
    $wk3mtd=money($wk2mtd+$wk3);
    $wk4mtd=money($wk3mtd+$wk4);
    $wk5mtd=money($wk4mtd+$wk5);
    $wk6mtd=money($wk5mtd+$wk6);

    if ($sales2mtd!=0){$lbrwk2prcntmtd=round($wk2mtd/$sales2mtd*100,1);}else{$lbrwk2prcntmtd=0;}	
    if ($sales3mtd!=0){$lbrwk3prcntmtd=round($wk3mtd/$sales3mtd*100,1);}else{$lbrwk3prcntmtd=0;}
    if ($sales4mtd!=0){$lbrwk4prcntmtd=round($wk4mtd/$sales4mtd*100,1);}else{$lbrwk4prcntmtd=0;}
    if ($sales5mtd!=0){$lbrwk5prcntmtd=round($wk5mtd/$sales5mtd*100,1);}else{$lbrwk5prcntmtd=0;}
    if ($sales6mtd!=0){$lbrwk6prcntmtd=round($wk6mtd/$sales6mtd*100,1);}else{$lbrwk6prcntmtd=0;}

    $showtab="";
    if ($goweek==1&&strlen($lbrwk1prcnt)<5){$showtab="\t";}
    elseif ($goweek==2&&strlen($lbrwk2prcnt)<5){$showtab="\t";}
    elseif ($goweek==3&&strlen($lbrwk3prcnt)<5){$showtab="\t";}
    elseif ($goweek==4&&strlen($lbrwk4prcnt)<5){$showtab="\t";}
    elseif ($goweek==5&&strlen($lbrwk5prcnt)<5){$showtab="\t";}
    elseif ($goweek==6&&strlen($lbrwk6prcnt)<5){$showtab="\t";}
    else{$showtab="";}

    if ($goweek==1){$message="$message<tr><td align=right>LABOR:</td><td align=right>$lbrwk1prcnt%</td><td align=right></td><td align=right>$wklaborbgt%</td><td align=right></td><td align=right>$lbrwk1prcnt%</td><td align=right></td><td align=right>$mtdlaborbgt%</td><td align=right></td><td align=right>$$bgtlabor</td><td align=right>$lbrprcnt%</td></tr>";$comlbr=$comlbr+$wk1;$commtdlbr=$commtdlbr+$wk1;$distlbr=$distlbr+$wk1;$distmtdlbr=$distmtdlbr+$wk1;}
    elseif ($goweek==2){$message="$message<tr><td align=right>LABOR:</td><td align=right>$lbrwk2prcnt%</td><td align=right></td><td align=right>$wklaborbgt%</td><td align=right></td><td align=right>$lbrwk2prcntmtd%</td><td align=right></td><td align=right>$mtdlaborbgt%</td><td align=right></td><td align=right>$$bgtlabor</td><td align=right>$lbrprcnt%</td></tr>";$comlbr=$comlbr+$wk2;$commtdlbr=$commtdlbr+$wk2mtd;$distlbr=$distlbr+$wk2;$distmtdlbr=$distmtdlbr+$wk2mtd;}
    elseif ($goweek==3){$message="$message<tr><td align=right>LABOR:</td><td align=right>$lbrwk3prcnt%</td><td align=right></td><td align=right>$wklaborbgt%</td><td align=right></td><td align=right>$lbrwk3prcntmtd%</td><td align=right></td><td align=right>$mtdlaborbgt%</td><td align=right></td><td align=right>$$bgtlabor</td><td align=right>$lbrprcnt%</td></tr>";$comlbr=$comlbr+$wk3;$commtdlbr=$commtdlbr+$wk3mtd;$distlbr=$distlbr+$wk2;$distmtdlbr=$distmtdlbr+$wk3mtd;}
    elseif ($goweek==4){$message="$message<tr><td align=right>LABOR:</td><td align=right>$lbrwk4prcnt%</td><td align=right></td><td align=right>$wklaborbgt%</td><td align=right></td><td align=right>$lbrwk4prcntmtd%</td><td align=right></td><td align=right>$mtdlaborbgt%</td><td align=right></td><td align=right>$$bgtlabor</td><td align=right>$lbrprcnt%</td></tr>";$comlbr=$comlbr+$wk4;$commtdlbr=$commtdlbr+$wk4mtd;$distlbr=$distlbr+$wk2;$distmtdlbr=$distmtdlbr+$wk4mtd;}
    elseif ($goweek==5){$message="$message<tr><td align=right>LABOR:</td><td align=right>$lbrwk5prcnt%</td><td align=right></td><td align=right>$wklaborbgt%</td><td align=right></td><td align=right>$lbrwk5prcntmtd%</td><td align=right></td><td align=right>$mtdlaborbgt%</td><td align=right></td><td align=right>$$bgtlabor</td><td align=right>$lbrprcnt%</td></tr>";$comlbr=$comlbr+$wk5;$commtdlbr=$commtdlbr+$wk5mtd;$distlbr=$distlbr+$wk2;$distmtdlbr=$distmtdlbr+$wk5mtd;}
    elseif ($goweek==6){$message="$message<tr><td align=right>LABOR:</td><td align=right>$lbrwk6prcnt%</td><td align=right></td><td align=right>$wklaborbgt%</td><td align=right></td><td align=right>$lbrwk6prcntmtd%</td><td align=right></td><td align=right>$mtdlaborbgt%</td><td align=right></td><td align=right>$$bgtlabor</td><td align=right>$lbrprcnt%</td></tr>";$comlbr=$comlbr+$wk6;$commtdlbr=$commtdlbr+$wk6mtd;$distlbr=$distlbr+$wk2;$distmtdlbr=$distmtdlbr+$wk6mtd;}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///START TOE///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM acct_payable WHERE businessid = '$businessid' AND cos = '0' AND cs = '0' AND (cs_type < 4 OR cs_type = 6 OR cs_type = 7 OR cs_type = 12) AND ((is_deleted = '0') OR (is_deleted = '1' AND del_date >= '$startdate')) ORDER BY accountnum DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    $wk1=0;
    $wk2=0;
    $wk3=0;
    $wk4=0;
    $wk5=0;
    $wk6=0;
    $currentweek=1;
    $num--;
    while ($num>=0){

       $acct_payableid=@Treat_DB_ProxyOld::mysql_result($result,$num,"acct_payableid");
       $accountnum=@Treat_DB_ProxyOld::mysql_result($result,$num,"accountnum");
       $acct_name=@Treat_DB_ProxyOld::mysql_result($result,$num,"acct_name");
       $cs_type=@Treat_DB_ProxyOld::mysql_result($result,$num,"cs_type");
       $daily_fixed=@Treat_DB_ProxyOld::mysql_result($result,$num,"daily_fixed");
       $cs_glacct=@Treat_DB_ProxyOld::mysql_result($result,$num,"cs_glacct");
       $cs_prcnt=@Treat_DB_ProxyOld::mysql_result($result,$num,"cs_prcnt");
       $showaccount=substr($accountnum,0,5);

     if ($cs_type==0){
       $day=$startdate;  

       $prevamount=0;
       $aptotal=0;
       $monthtotal=0;
       while ($day<=$enddate){

          $prevamount=0;

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query5 = "SELECT * FROM apinvoice WHERE (businessid = '$businessid' AND date = '$day' AND transfer != '3') OR (vendor = '$businessid' AND date = '$day' AND transfer = '1')";
          $result5 = Treat_DB_ProxyOld::query($query5);
          $num5=Treat_DB_ProxyOld::mysql_numrows($result5);
          //mysql_close();

          $prevamount=0;
          $num5--;
          while ($num5>=0){

             $apinvoiceid=@Treat_DB_ProxyOld::mysql_result($result5,$num5,"apinvoiceid");
             $transfer=@Treat_DB_ProxyOld::mysql_result($result5,$num5,"transfer");
             $vendor=@Treat_DB_ProxyOld::mysql_result($result5,$num5,"vendor");
     
             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             if($transfer==1&&$vendor==$businessid){
                $query6 = "SELECT amount FROM apinvoicedetail WHERE apinvoiceid = '$apinvoiceid' AND tfr_acct = '$acct_payableid'";
             }
             else{
                $query6 = "SELECT amount FROM apinvoicedetail WHERE apinvoiceid = '$apinvoiceid' AND apaccountid = '$acct_payableid'";
             }
             $result6 = Treat_DB_ProxyOld::query($query6);
             $num6=Treat_DB_ProxyOld::mysql_numrows($result6);
             //mysql_close();

             $num6--;
             while ($num6>=0){
                $apamount=@Treat_DB_ProxyOld::mysql_result($result6,$num6,"amount");
                if ($transfer==1&&$vendor==$businessid){$apamount=$apamount*-1;}
                $prevamount=$prevamount+$apamount;
                $num6--;
             }

             $num5--;
          }

          $aptotal=$prevamount+$aptotal;

          if ($currentweek==1){$wk1=$wk1+$prevamount;}
          elseif ($currentweek==2){$wk2=$wk2+$prevamount;}
          elseif ($currentweek==3){$wk3=$wk3+$prevamount;}
          elseif ($currentweek==4){$wk4=$wk4+$prevamount;}
          elseif ($currentweek==5){$wk5=$wk5+$prevamount;}
          elseif ($currentweek==6){$wk6=$wk6+$prevamount;}

          $prcnt=0;
          if ($currentweek==1&&$aptotal!=0&&$saleswk1!=0){$prcnt=($aptotal/$saleswk1)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==2&&$aptotal!=0&&$saleswk2!=0){$prcnt=($aptotal/$saleswk2)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==3&&$aptotal!=0&&$saleswk3!=0){$prcnt=($aptotal/$saleswk3)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==4&&$aptotal!=0&&$saleswk4!=0){$prcnt=($aptotal/$saleswk4)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==5&&$aptotal!=0&&$saleswk5!=0){$prcnt=($aptotal/$saleswk5)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==6&&$aptotal!=0&&$saleswk6!=0){$prcnt=($aptotal/$saleswk6)*100;$prcnt=round($prcnt,1);}

          if ($weekday=="Thursday" || $day == $enddate){
             if ($aptotal!=0){$aptotal=money($aptotal);}
             $monthtotal=$aptotal+$monthtotal;
             $aptotal=0;
             $currentweek++;
          }

          $day=nextday($day);
          if ($weekday=="Sunday"){$weekday="Monday";}
          elseif ($weekday=="Saturday"){$weekday="Sunday";}
          elseif ($weekday=="Friday"){$weekday="Saturday";}
          elseif ($weekday=="Thursday"){$weekday="Friday";}
          elseif ($weekday=="Wednesday"){$weekday="Thursday";}
          elseif ($weekday=="Tuesday"){$weekday="Wednesday";}
          elseif ($weekday=="Monday"){$weekday="Tuesday";}
       }
     }
     elseif($cs_type==1){
       $day=$startdate;  

       $prevamount=0;
       $aptotal=0;
       $monthtotal=0;

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query5 = "SELECT * FROM debits WHERE businessid = '$businessid' AND debittype = '1' AND ((is_deleted = '0') OR (is_deleted = '1' AND del_date >= '$startdate'))";
       $result5 = Treat_DB_ProxyOld::query($query5);
       $num5=Treat_DB_ProxyOld::mysql_numrows($result5);
       //mysql_close();
       $debitnum=$num5;

       while ($day<=$enddate){
          $num5=$debitnum;
          $num5--;
          while ($num5>=0){
             $debitid=@Treat_DB_ProxyOld::mysql_result($result5,$num5,"debitid");

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query6 = "SELECT amount FROM debitdetail WHERE debitid = '$debitid' AND date = '$day'";
             $result6 = Treat_DB_ProxyOld::query($query6);
             $num6=Treat_DB_ProxyOld::mysql_numrows($result6);
             //mysql_close();

             $num6--;
             while ($num6>=0){
                $amount=@Treat_DB_ProxyOld::mysql_result($result6,$num6,"amount");
                $prevamount=$prevamount+$amount;
                $num6--;
             }
             $num5--;
          }

          if ($weekday=="Thursday" || $day == $enddate){
             if ($prevamount!=0){$prevamount=money($prevamount*.035);}
             $prcnt=0;
             if ($currentweek==1&&$prevamount!=0&&$saleswk1!=0){$prcnt=($prevamount/$saleswk1)*100;$prcnt=round($prcnt,1);}
             elseif ($currentweek==2&&$prevamount!=0&&$saleswk2!=0){$prcnt=($prevamount/$saleswk2)*100;$prcnt=round($prcnt,1);}
             elseif ($currentweek==3&&$prevamount!=0&&$saleswk3!=0){$prcnt=($prevamount/$saleswk3)*100;$prcnt=round($prcnt,1);}
             elseif ($currentweek==4&&$prevamount!=0&&$saleswk4!=0){$prcnt=($prevamount/$saleswk4)*100;$prcnt=round($prcnt,1);}
             elseif ($currentweek==5&&$prevamount!=0&&$saleswk5!=0){$prcnt=($prevamount/$saleswk5)*100;$prcnt=round($prcnt,1);}
             elseif ($currentweek==6&&$prevamount!=0&&$saleswk6!=0){$prcnt=($prevamount/$saleswk6)*100;$prcnt=round($prcnt,1);}

             if ($currentweek==1){$wk1=$wk1+$prevamount;}
             elseif ($currentweek==2){$wk2=$wk2+$prevamount;}
             elseif ($currentweek==3){$wk3=$wk3+$prevamount;}
             elseif ($currentweek==4){$wk4=$wk4+$prevamount;}
             elseif ($currentweek==5){$wk5=$wk5+$prevamount;}
             elseif ($currentweek==6){$wk6=$wk6+$prevamount;}
            $monthtotal=$prevamount+$monthtotal;
             $currentweek++;
             $prevamount=0;
          }

          $day=nextday($day);
          if ($weekday=="Sunday"){$weekday="Monday";}
          elseif ($weekday=="Saturday"){$weekday="Sunday";}
          elseif ($weekday=="Friday"){$weekday="Saturday";}
          elseif ($weekday=="Thursday"){$weekday="Friday";}
          elseif ($weekday=="Wednesday"){$weekday="Thursday";}
          elseif ($weekday=="Tuesday"){$weekday="Wednesday";}
          elseif ($weekday=="Monday"){$weekday="Tuesday";}
       }
     }
     elseif($cs_type==2){
       $day=$startdate;  

       $prevamount=0;
       $aptotal=0;
       $monthtotal=0;
       while ($day<=$enddate){

          $prevamount=$prevamount+$daily_fixed;

          if ($currentweek==1){$wk1=$wk1+$daily_fixed;}
          elseif ($currentweek==2){$wk2=$wk2+$daily_fixed;}
          elseif ($currentweek==3){$wk3=$wk3+$daily_fixed;}
          elseif ($currentweek==4){$wk4=$wk4+$daily_fixed;}
          elseif ($currentweek==5){$wk5=$wk5+$daily_fixed;}
          elseif ($currentweek==6){$wk6=$wk6+$daily_fixed;}

          $prcnt=0;
          if ($currentweek==1&&$prevamount!=0&&$saleswk1!=0){$prcnt=($prevamount/$saleswk1)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==2&&$prevamount!=0&&$saleswk2!=0){$prcnt=($prevamount/$saleswk2)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==3&&$prevamount!=0&&$saleswk3!=0){$prcnt=($prevamount/$saleswk3)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==4&&$prevamount!=0&&$saleswk4!=0){$prcnt=($prevamount/$saleswk4)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==5&&$prevamount!=0&&$saleswk5!=0){$prcnt=($prevamount/$saleswk5)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==6&&$prevamount!=0&&$saleswk6!=0){$prcnt=($prevamount/$saleswk6)*100;$prcnt=round($prcnt,1);}

          if ($weekday=="Thursday" || $day == $enddate){
             if ($prevamount!=0){$prevamount=money($prevamount);}
             $monthtotal=$prevamount+$monthtotal;
             $currentweek++;
             $prevamount=0;
          }

          $day=nextday($day);
          if ($weekday=="Sunday"){$weekday="Monday";}
          elseif ($weekday=="Saturday"){$weekday="Sunday";}
          elseif ($weekday=="Friday"){$weekday="Saturday";}
          elseif ($weekday=="Thursday"){$weekday="Friday";}
          elseif ($weekday=="Wednesday"){$weekday="Thursday";}
          elseif ($weekday=="Tuesday"){$weekday="Wednesday";}
          elseif ($weekday=="Monday"){$weekday="Tuesday";}
       }

     }
     elseif($cs_type==3){
        //mysql_connect($dbhost,$username,$password);
        //@mysql_select_db($database) or die( "Unable to select database");
        $query7 = "SELECT * FROM controlsheet WHERE businessid = '$businessid' AND date = '$startdate' AND acct_payableid = '$acct_payableid'";
        $result7 = Treat_DB_ProxyOld::query($query7);
        $num7=Treat_DB_ProxyOld::mysql_numrows($result7);
        //mysql_close();

        if ($num7!=0){
           $csid=@Treat_DB_ProxyOld::mysql_result($result7,0,"csid");
           $amount1=@Treat_DB_ProxyOld::mysql_result($result7,0,"amount1");$amount1=money($amount1);
           $amount2=@Treat_DB_ProxyOld::mysql_result($result7,0,"amount2");$amount2=money($amount2);
           $amount3=@Treat_DB_ProxyOld::mysql_result($result7,0,"amount3");$amount3=money($amount3);
           $amount4=@Treat_DB_ProxyOld::mysql_result($result7,0,"amount4");$amount4=money($amount4);
           $amount5=@Treat_DB_ProxyOld::mysql_result($result7,0,"amount5");$amount5=money($amount5);
           $amount6=@Treat_DB_ProxyOld::mysql_result($result7,0,"amount6");$amount6=money($amount6);
        }
        else{
           $amount1=0;
           $amount2=0;
           $amount3=0;
           $amount4=0;
           $amount5=0;
           $amount6=0;
        }

        if ($weekcount==5){$monthtotal=$amount1+$amount2+$amount3+$amount4+$amount5;}
        elseif ($weekcount==6){$monthtotal=$amount1+$amount2+$amount3+$amount4+$amount5+$amount6;}
        else{$monthtotal=$amount1+$amount2+$amount3+$amount4;}
        $wk1=$wk1+$amount1;
        $wk2=$wk2+$amount2;
        $wk3=$wk3+$amount3;
        $wk4=$wk4+$amount4;
        if ($weekcount==5){$wk5=$wk5+$amount5;}
        if ($weekcount==6){$wk5=$wk5+$amount5;$wk6=$wk6+$amount6;}

        $amount1prcnt=0;
        $amount2prcnt=0;
        $amount3prcnt=0;
        $amount4prcnt=0;
        $amount5prcnt=0;
        $amount6prcnt=0;
        if ($amount1!=0&&$saleswk1!=0){$amount1prcnt=round(($amount1/$saleswk1)*100,1);}
        if ($amount2!=0&&$saleswk2!=0){$amount2prcnt=round(($amount2/$saleswk2)*100,1);}
        if ($amount3!=0&&$saleswk3!=0){$amount3prcnt=round(($amount3/$saleswk3)*100,1);}
        if ($amount4!=0&&$saleswk4!=0){$amount4prcnt=round(($amount4/$saleswk4)*100,1);}
        if ($amount5!=0&&$saleswk5!=0){$amount5prcnt=round(($amount5/$saleswk5)*100,1);}
        if ($amount6!=0&&$saleswk6!=0){$amount6prcnt=round(($amount6/$saleswk6)*100,1);}

     }

     elseif($cs_type==6){
       $day=$startdate;  

       $prevamount=0;
       $aptotal=0;
       $monthtotal=0;
              
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query5 = "SELECT * FROM credits WHERE businessid = '$businessid' AND (credittype = '1' OR credittype = '5') AND ((is_deleted = '0') OR (is_deleted = '1' AND del_date >= '$startdate'))";
       $result5 = Treat_DB_ProxyOld::query($query5);
       $num5=Treat_DB_ProxyOld::mysql_numrows($result5);
       //mysql_close();
       $debitnum=$num5;
       
       while ($day<=$enddate){
          $num5=$debitnum;
          $num5--;
          while ($num5>=0){
             $creditid=@Treat_DB_ProxyOld::mysql_result($result5,$num5,"creditid");
             $invoicesales=@Treat_DB_ProxyOld::mysql_result($result5,$num5,"invoicesales");
             $invoicesales_notax=@Treat_DB_ProxyOld::mysql_result($result5,$num5,"invoicesales_notax");
             $servchrg=@Treat_DB_ProxyOld::mysql_result($result5,$num5,"servchrg");

             if ($invoicesales==0&&$invoicesales_notax==0&&$servchrg==0){
                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query6 = "SELECT amount FROM creditdetail WHERE creditid = '$creditid' AND date = '$day'";
                $result6 = Treat_DB_ProxyOld::query($query6);
                $num6=Treat_DB_ProxyOld::mysql_numrows($result6);
                //mysql_close();

                $num6--;
                while ($num6>=0){
                   $amount=@Treat_DB_ProxyOld::mysql_result($result6,$num6,"amount");
                   if ($cs_glacct==-1||$cs_glacct==$creditid){$prevamount=$prevamount+$amount;}
                   $num6--;
                }
             }
             elseif ($invoicesales==1){
                  //mysql_connect($dbhost,$username,$password);
                  //@mysql_select_db($database) or die( "Unable to select database");
                  $query7 = "SELECT total,taxtotal,servamt FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND taxable = 'on' AND salescode = '0' AND (status = '4' OR status = '2' OR status = '6')";
                  $result7 = Treat_DB_ProxyOld::query($query7);
                  $num7=Treat_DB_ProxyOld::mysql_numrows($result7);
                  //mysql_close();
                  $num7--;
                  while ($num7>=0){
                     $invtotal=@Treat_DB_ProxyOld::mysql_result($result7,$num7,"total");
                     $invtax=@Treat_DB_ProxyOld::mysql_result($result7,$num7,"taxtotal");
                     $servamt=@Treat_DB_ProxyOld::mysql_result($result7,$num7,"servamt");
                     $prevamount=$prevamount+($invtotal-$invtax-$servamt);
                     $num7--;
                  }
             }
             elseif ($invoicesales_notax==1){
                  //mysql_connect($dbhost,$username,$password);
                  //@mysql_select_db($database) or die( "Unable to select database");
                  $query7 = "SELECT total,taxtotal,servamt FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND taxable = 'off' AND salescode = '0' AND (status = '4' OR status = '2' OR status = '6')";
                  $result7 = Treat_DB_ProxyOld::query($query7);
                  $num7=Treat_DB_ProxyOld::mysql_numrows($result7);
                  //mysql_close();
                  $num7--;
                  while ($num7>=0){
                     $invtotal=@Treat_DB_ProxyOld::mysql_result($result7,$num7,"total");
                     $servamt=@Treat_DB_ProxyOld::mysql_result($result7,$num7,"servamt");
                     $prevamount=$prevamount+$invtotal-$servamt;
                     $num7--;
                  }
             }
             elseif ($servchrg==1){
                  //mysql_connect($dbhost,$username,$password);
                  //@mysql_select_db($database) or die( "Unable to select database");
                  $query7 = "SELECT servamt FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND (status = '4' OR status = '2' OR status = '6')";
                  $result7 = Treat_DB_ProxyOld::query($query7);
                  $num7=Treat_DB_ProxyOld::mysql_numrows($result7);
                  //mysql_close();
                  $num7--;
                  while ($num7>=0){
                     $servamt=@Treat_DB_ProxyOld::mysql_result($result7,$num7,"servamt");
                     $prevamount=$prevamount+$servamt;
                     $num7--;
                  }
             }
             $num5--;
          }

          if ($weekday=="Thursday" || $day == $enddate){
             if ($prevamount!=0){$prevamount=money($prevamount*$cs_prcnt/100);}
             $prcnt=0;
             if ($currentweek==1&&$prevamount!=0&&$saleswk1!=0){$prcnt=($prevamount/$saleswk1)*100;$prcnt=round($prcnt,1);}
             elseif ($currentweek==2&&$prevamount!=0&&$saleswk2!=0){$prcnt=($prevamount/$saleswk2)*100;$prcnt=round($prcnt,1);}
             elseif ($currentweek==3&&$prevamount!=0&&$saleswk3!=0){$prcnt=($prevamount/$saleswk3)*100;$prcnt=round($prcnt,1);}
             elseif ($currentweek==4&&$prevamount!=0&&$saleswk4!=0){$prcnt=($prevamount/$saleswk4)*100;$prcnt=round($prcnt,1);}
             elseif ($currentweek==5&&$prevamount!=0&&$saleswk5!=0){$prcnt=($prevamount/$saleswk5)*100;$prcnt=round($prcnt,1);}
             elseif ($currentweek==6&&$prevamount!=0&&$saleswk6!=0){$prcnt=($prevamount/$saleswk6)*100;$prcnt=round($prcnt,1);}

             if ($currentweek==1){$wk1=$wk1+$prevamount;}
             elseif ($currentweek==2){$wk2=$wk2+$prevamount;}
             elseif ($currentweek==3){$wk3=$wk3+$prevamount;}
             elseif ($currentweek==4){$wk4=$wk4+$prevamount;}
             elseif ($currentweek==5){$wk5=$wk5+$prevamount;}
             elseif ($currentweek==6){$wk6=$wk6+$prevamount;}

             $monthtotal=$prevamount+$monthtotal;
             $currentweek++;
             $prevamount=0;
          }

          $day=nextday($day);
          if ($weekday=="Sunday"){$weekday="Monday";}
          elseif ($weekday=="Saturday"){$weekday="Sunday";}
          elseif ($weekday=="Friday"){$weekday="Saturday";}
          elseif ($weekday=="Thursday"){$weekday="Friday";}
          elseif ($weekday=="Wednesday"){$weekday="Thursday";}
          elseif ($weekday=="Tuesday"){$weekday="Wednesday";}
          elseif ($weekday=="Monday"){$weekday="Tuesday";}
       }
     }
     elseif($cs_type==7){
       $day=$startdate;  
       $month=substr($day,5,2);
       $leap = date("L");
       if ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10' || $month == '12'){$monthcount=31;}
       elseif($month=='04' || $month=='06' || $month=='09' || $month=='11'){$monthcount=30;}
       elseif($month=='02' && $leap == 0){$monthcount=28;}
       elseif($month=='02' && $leap == 1){$monthcount=29;}

       $daily_fixed=$daily_fixed/$monthcount;

       $prevamount=0;
       $aptotal=0;
       $monthtotal=0;
       while ($day<=$enddate){

          $prevamount=$prevamount+$daily_fixed;

          if ($currentweek==1){$wk1=$wk1+$daily_fixed;}
          elseif ($currentweek==2){$wk2=$wk2+$daily_fixed;}
          elseif ($currentweek==3){$wk3=$wk3+$daily_fixed;}
          elseif ($currentweek==4){$wk4=$wk4+$daily_fixed;}
          elseif ($currentweek==5){$wk5=$wk5+$daily_fixed;}
          elseif ($currentweek==6){$wk6=$wk6+$daily_fixed;}

          $prcnt=0;
          if ($currentweek==1&&$prevamount!=0&&$saleswk1!=0){$prcnt=($prevamount/$saleswk1)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==2&&$prevamount!=0&&$saleswk2!=0){$prcnt=($prevamount/$saleswk2)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==3&&$prevamount!=0&&$saleswk3!=0){$prcnt=($prevamount/$saleswk3)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==4&&$prevamount!=0&&$saleswk4!=0){$prcnt=($prevamount/$saleswk4)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==5&&$prevamount!=0&&$saleswk5!=0){$prcnt=($prevamount/$saleswk5)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==6&&$prevamount!=0&&$saleswk6!=0){$prcnt=($prevamount/$saleswk6)*100;$prcnt=round($prcnt,1);}

          if ($weekday=="Thursday" || $day == $enddate){
             $showamount=$prevamount;
             if ($showamount!=0){$showamount=money($showamount);}
             $monthtotal=$prevamount+$monthtotal;
             $currentweek++;
             $prevamount=0;
          }

          $day=nextday($day);
          if ($weekday=="Sunday"){$weekday="Monday";}
          elseif ($weekday=="Saturday"){$weekday="Sunday";}
          elseif ($weekday=="Friday"){$weekday="Saturday";}
          elseif ($weekday=="Thursday"){$weekday="Friday";}
          elseif ($weekday=="Wednesday"){$weekday="Thursday";}
          elseif ($weekday=="Tuesday"){$weekday="Wednesday";}
          elseif ($weekday=="Monday"){$weekday="Tuesday";}
       }

     }
     elseif($cs_type==12){
       $day=$startdate;  
       $month=substr($day,5,2);
       $recuryear=substr($day,0,4);
       $leap = date("L");
       if ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10' || $month == '12'){$monthcount=31;}
       elseif($month=='04' || $month=='06' || $month=='09' || $month=='11'){$monthcount=30;}
       elseif($month=='02' && $leap == 0){$monthcount=28;}
       elseif($month=='02' && $leap == 1){$monthcount=29;}

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query12 = "SELECT `$month` FROM recur WHERE year = '$recuryear' AND apaccountid = '$acct_payableid'";
       $result12 = Treat_DB_ProxyOld::query($query12);
       //mysql_close();

       $recuramount=@Treat_DB_ProxyOld::mysql_result($result12,0,"$month");
       $recuramount=$recuramount/$monthcount;

       $prevamount=0;
       $aptotal=0;
       $monthtotal=0;
       while ($day<=$enddate){

          $prevamount=$prevamount+$recuramount;

          if ($currentweek==1){$wk1=$wk1+$recuramount;}
          elseif ($currentweek==2){$wk2=$wk2+$recuramount;}
          elseif ($currentweek==3){$wk3=$wk3+$recuramount;}
          elseif ($currentweek==4){$wk4=$wk4+$recuramount;}
          elseif ($currentweek==5){$wk5=$wk5+$recuramount;}
          elseif ($currentweek==6){$wk6=$wk6+$recuramount;}

          $prcnt=0;
          if ($currentweek==1&&$prevamount!=0&&$saleswk1!=0){$prcnt=($prevamount/$saleswk1)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==2&&$prevamount!=0&&$saleswk2!=0){$prcnt=($prevamount/$saleswk2)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==3&&$prevamount!=0&&$saleswk3!=0){$prcnt=($prevamount/$saleswk3)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==4&&$prevamount!=0&&$saleswk4!=0){$prcnt=($prevamount/$saleswk4)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==5&&$prevamount!=0&&$saleswk5!=0){$prcnt=($prevamount/$saleswk5)*100;$prcnt=round($prcnt,1);}
          elseif ($currentweek==6&&$prevamount!=0&&$saleswk6!=0){$prcnt=($prevamount/$saleswk6)*100;$prcnt=round($prcnt,1);}

          if ($weekday=="Thursday" || $day == $enddate){
             $showamount=$prevamount;
             if ($showamount!=0){$showamount=money($showamount);}
             //echo "<td  align=right><font size=2>$$showamount</td><td align=right><font size=1 color=blue>$prcnt%</td>";
             $monthtotal=$prevamount+$monthtotal;
             $currentweek++;
             $prevamount=0;
          }

          $day=nextday($day);
          if ($weekday=="Sunday"){$weekday="Monday";}
          elseif ($weekday=="Saturday"){$weekday="Sunday";}
          elseif ($weekday=="Friday"){$weekday="Saturday";}
          elseif ($weekday=="Thursday"){$weekday="Friday";}
          elseif ($weekday=="Wednesday"){$weekday="Thursday";}
          elseif ($weekday=="Tuesday"){$weekday="Wednesday";}
          elseif ($weekday=="Monday"){$weekday="Tuesday";}
       }

     }
 
       $day=$startdate;
       $weekday=$startweekday;

       $monthprcnt=0;
       $monthtotal=money($monthtotal);
       if ($monthtotal!=0&&$salestotal!=0){$monthprcnt=round(($monthtotal/$salestotal)*100,1);}
       
       $num--;
       $currentweek=1;
    }

    $cashtotal=money($cashwk1+$cashwk2+$cashwk3+$cashwk4+$cashwk5+$cashwk6);
    $cashwk1=money($cashwk1);
    $cashwk2=money($cashwk2);
    $cashwk3=money($cashwk3);
    $cashwk4=money($cashwk4);
    $cashwk5=money($cashwk5);
    $cashwk6=money($cashwk6);

    $apwk1=$apwk1+$wk1+$cashwk1;
    $apwk2=$apwk2+$wk2+$cashwk2;
    $apwk3=$apwk3+$wk3+$cashwk3;
    $apwk4=$apwk4+$wk4+$cashwk4;
    $apwk5=$apwk5+$wk5+$cashwk5;
    $apwk6=$apwk6+$wk6+$cashwk6;

    $weektotal=$wk1+$wk2+$wk3+$wk4+$wk5+$wk6+$cashwk1+$cashwk2+$cashwk3+$cashwk4+$cashwk5+$cashwk6;
    $wk1=money($wk1+$cashwk1);
    $wk2=money($wk2+$cashwk2);
    $wk3=money($wk3+$cashwk3);
    $wk4=money($wk4+$cashwk4);
    $wk5=money($wk5+$cashwk5);
    $wk6=money($wk6+$cashwk6);
    $weektotal=money($weektotal);
  
    if ($wk1!=0&&$saleswk1!=0){$toewk1prcnt=round($wk1/$saleswk1*100,1);} else{$toewk1prcnt=0;}
    if ($wk2!=0&&$saleswk2!=0){$toewk2prcnt=round($wk2/$saleswk2*100,1);} else{$toewk2prcnt=0;}
    if ($wk3!=0&&$saleswk3!=0){$toewk3prcnt=round($wk3/$saleswk3*100,1);} else{$toewk3prcnt=0;}
    if ($wk4!=0&&$saleswk4!=0){$toewk4prcnt=round($wk4/$saleswk4*100,1);} else{$toewk4prcnt=0;}
    if ($wk5!=0&&$saleswk5!=0){$toewk5prcnt=round($wk5/$saleswk5*100,1);} else{$toewk5prcnt=0;}
    if ($wk6!=0&&$saleswk6!=0){$toewk6prcnt=round($wk6/$saleswk6*100,1);} else{$toewk6prcnt=0;}
    if ($salestotal!=0){$monthprcnt=round($weektotal/$salestotal*100,1);} else{$monthprcnt=0;}

    $overallpcnt=$overallpcnt+$monthprcnt;

    $wk2mtd=money($wk1+$wk2);
    $wk3mtd=money($wk2mtd+$wk3);
    $wk4mtd=money($wk3mtd+$wk4);
    $wk5mtd=money($wk4mtd+$wk5);
    $wk6mtd=money($wk5mtd+$wk6);

    if ($sales2mtd!=0){$toewk2prcntmtd=round($wk2mtd/$sales2mtd*100,1);}else{$toewk2prcntmtd=0;}
    if ($sales3mtd!=0){$toewk3prcntmtd=round($wk3mtd/$sales3mtd*100,1);}else{$toewk3prcntmtd=0;}
    if ($sales4mtd!=0){$toewk4prcntmtd=round($wk4mtd/$sales4mtd*100,1);}else{$toewk4prcntmtd=0;}
    if ($sales5mtd!=0){$toewk5prcntmtd=round($wk5mtd/$sales5mtd*100,1);}else{$toewk5prcntmtd=0;}
    if ($sales6mtd!=0){$toewk6prcntmtd=round($wk6mtd/$sales6mtd*100,1);}else{$toewk6prcntmtd=0;}

    $showtab="";
    if ($goweek==1&&strlen($toewk1prcnt)<5){$showtab="\t";}
    elseif ($goweek==2&&strlen($toewk2prcnt)<5){$showtab="\t";}
    elseif ($goweek==3&&strlen($toewk3prcnt)<5){$showtab="\t";}
    elseif ($goweek==4&&strlen($toewk4prcnt)<5){$showtab="\t";}
    elseif ($goweek==5&&strlen($toewk5prcnt)<5){$showtab="\t";}
    elseif ($goweek==6&&strlen($toewk6prcnt)<5){$showtab="\t";}
    else{$showtab="";}

    if ($goweek==1){$message="$message<tr><td align=right>TOE:</td><td align=right>$toewk1prcnt%</td><td align=right></td><td align=right>$wktoebgt%</td><td align=right></td><td align=right>$toewk1prcnt%</td><td align=right></td><td align=right>$mtdtoebgt%</td><td align=right></td><td align=right>$$bgttoe</td><td align=right>$toeprcnt%</td></tr>";$comtoe=$comtoe+$wk1;$commtdtoe=$commtdtoe+$wk1;$disttoe=$disttoe+$wk1;$distmtdtoe=$distmtdtoe+$wk1;}
    elseif ($goweek==2){$message="$message<tr><td align=right>TOE:</td><td align=right>$toewk2prcnt%</td><td align=right></td><td align=right>$wktoebgt%</td><td align=right></td><td align=right>$toewk2prcntmtd%</td><td align=right></td><td align=right>$mtdtoebgt%</td><td align=right></td><td align=right>$$bgttoe</td><td align=right>$toeprcnt%</td></tr>";$comtoe=$comtoe+$wk2;$commtdtoe=$commtdtoe+$wk2mtd;$disttoe=$disttoe+$wk2;$distmtdtoe=$distmtdtoe+$wk2mtd;}
    elseif ($goweek==3){$message="$message<tr><td align=right>TOE:</td><td align=right>$toewk3prcnt%</td><td align=right></td><td align=right>$wktoebgt%</td><td align=right></td><td align=right>$toewk3prcntmtd%</td><td align=right></td><td align=right>$mtdtoebgt%</td><td align=right></td><td align=right>$$bgttoe</td><td align=right>$toeprcnt%</td></tr>";$comtoe=$comtoe+$wk3;$commtdtoe=$commtdtoe+$wk3mtd;$disttoe=$disttoe+$wk3;$distmtdtoe=$distmtdtoe+$wk3mtd;}
    elseif ($goweek==4){$message="$message<tr><td align=right>TOE:</td><td align=right>$toewk4prcnt%</td><td align=right></td><td align=right>$wktoebgt%</td><td align=right></td><td align=right>$toewk4prcntmtd%</td><td align=right></td><td align=right>$mtdtoebgt%</td><td align=right></td><td align=right>$$bgttoe</td><td align=right>$toeprcnt%</td></tr>";$comtoe=$comtoe+$wk4;$commtdtoe=$commtdtoe+$wk4mtd;$disttoe=$disttoe+$wk4;$distmtdtoe=$distmtdtoe+$wk4mtd;}
    elseif ($goweek==5){$message="$message<tr><td align=right>TOE:</td><td align=right>$toewk5prcnt%</td><td align=right></td><td align=right>$wktoebgt%</td><td align=right></td><td align=right>$toewk5prcntmtd%</td><td align=right></td><td align=right>$mtdtoebgt%</td><td align=right></td><td align=right>$$bgttoe</td><td align=right>$toeprcnt%</td></tr>";$comtoe=$comtoe+$wk5;$commtdtoe=$commtdtoe+$wk5mtd;$disttoe=$disttoe+$wk5;$distmtdtoe=$distmtdtoe+$wk5mtd;}
    elseif ($goweek==6){$message="$message<tr><td align=right>TOE:</td><td align=right>$toewk6prcnt%</td><td align=right></td><td align=right>$wktoebgt%</td><td align=right></td><td align=right>$toewk6prcntmtd%</td><td align=right></td><td align=right>$mtdtoebgt%</td><td align=right></td><td align=right>$$bgttoe</td><td align=right>$toeprcnt%</td></tr>";$comtoe=$comtoe+$wk6;$commtdtoe=$commtdtoe+$wk6mtd;$disttoe=$disttoe+$wk6;$distmtdtoe=$distmtdtoe+$wk6mtd;}

/////////////////////BILL TO CLIENT/////////////////////////////////
        
        //mysql_connect($dbhost,$username,$password);
        //@mysql_select_db($database) or die( "Unable to select database");
        $query7 = "SELECT * FROM controlsheet WHERE businessid = '$businessid' AND date = '$startdate' AND type = '2'";
        $result7 = Treat_DB_ProxyOld::query($query7);
        $num7=Treat_DB_ProxyOld::mysql_numrows($result7);
        //mysql_close();

        if ($num7!=0){
           $csid=@Treat_DB_ProxyOld::mysql_result($result7,0,"csid");
           $amount1=@Treat_DB_ProxyOld::mysql_result($result7,0,"amount1");$amount1=money($amount1);
           $amount2=@Treat_DB_ProxyOld::mysql_result($result7,0,"amount2");$amount2=money($amount2);
           $amount3=@Treat_DB_ProxyOld::mysql_result($result7,0,"amount3");$amount3=money($amount3);
           $amount4=@Treat_DB_ProxyOld::mysql_result($result7,0,"amount4");$amount4=money($amount4);
           $amount5=@Treat_DB_ProxyOld::mysql_result($result7,0,"amount5");$amount5=money($amount5);
           $amount6=@Treat_DB_ProxyOld::mysql_result($result7,0,"amount6");$amount6=money($amount6);
        }
        else{
           $amount1=0;
           $amount2=0;
           $amount3=0;
           $amount4=0;
           $amount5=0;
           $amount6=0;
        }
    if ($goweek==1){$showamtdue=money($amount1);}
    elseif ($goweek==2){$showamtdue=money($amount2);}
    elseif ($goweek==3){$showamtdue=money($amount3);}
    elseif ($goweek==4){$showamtdue=money($amount4);}
    elseif ($goweek==5){$showamtdue=money($amount5);}
    elseif ($goweek==6){$showamtdue=money($amount6);}
    $showamtduemtd=money($amount1+$amount2+$amount3+$amount4+$amount5+$amount6);

//////////////////////////AMOUNT DUE
    $wkamtduebgt=number_format($wkamtduebgt);
    $mtdamtduebgt=number_format($mtdamtduebgt);
    $bgtamtdue=number_format($bgtamtdue);

    $message="$message<tr><td align=right>AMT DUE:</td><td align=right>$$showamtdue</td><td align=right></td><td align=right>$$wkamtduebgt</td><td align=right></td><td align=right>$$showamtduemtd</td><td></td><td align=right>$$mtdamtduebgt</td><td align=right></td><td align=right>$$bgtamtdue</td><td></td></tr>";

//////////////////BENEFITS//////////////////////////////////////

///////P&L////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        

    $apmonthtotal=money($salestotal-($apwk1+$apwk2+$apwk3+$apwk4+$apwk5+$apwk6-$invadj)+$monthtotal);
    $apwk1=money($saleswk1-$apwk1+$amount1);
    $apwk1m=$apwk1;
    if ($apwk1==0){$apwk1=0;}
    $apwk2=money($saleswk2-$apwk2+$amount2);
    $apwk2m=money($apwk1m+$apwk2);
    if ($apwk2==0){$apwk2=0;}
    $apwk3=money($saleswk3-$apwk3+$amount3);
    $apwk3m=money($apwk2m+$apwk3);
    if ($apwk3==0){$apwk3=0;}
    $apwk4=money($saleswk4-$apwk4+$amount4);
    $apwk4m=money($apwk3m+$apwk4);
    if ($apwk4==0){$apwk4=0;}
    $apwk5=money($saleswk5-$apwk5+$amount5);
    $apwk5m=money($apwk4m+$apwk5);
    if ($apwk5==0){$apwk5=0;}
    $apwk6=money($saleswk6-$apwk6+$amount6);
    $apwk6m=money($apwk5m+$apwk6);
    if ($apwk6==0){$apwk6=0;}

    if ($goweek==1){$with_commas = number_format($apwk1);$message="$message<tr><td align=right>P&L:</td><td align=right>$$with_commas</td><td align=right></td><td align=right>$$wknetpftbgt</td><td align=right></td><td align=right>$$with_commas</td><td align=right></td><td align=right>$$mtdnetpftbgt</td><td align=right></td><td align=right>$$bgtnetpft</td><td align=right>$netpftprcnt%</td></tr>";$compl=$compl+$apwk1;$commtdpl=$commtdpl+$apwk1;$distpl=$distpl+$apwk1;$distmtdpl=$distmtdpl+$apwk1;}
    elseif ($goweek==2){$with_commas = number_format($apwk2);$with_commas2 = number_format($apwk2m);$message="$message<tr><td align=right>P&L:</td><td align=right>$$with_commas</td><td align=right></td><td align=right>$$wknetpftbgt</td><td align=right></td><td align=right>$$with_commas2</td><td align=right></td><td align=right>$$mtdnetpftbgt</td><td align=right></td><td align=right>$$bgtnetpft</td><td align=right>$netpftprcnt%</td></tr>";$compl=$compl+$apwk2;$commtdpl=$commtdpl+$apwk2m;$distpl=$distpl+$apwk2;$distmtdpl=$distmtdpl+$apwk2m;}
    elseif ($goweek==3){$with_commas = number_format($apwk3);$with_commas2 = number_format($apwk3m);$message="$message<tr><td align=right>P&L:</td><td align=right>$$with_commas</td><td align=right></td><td align=right>$$wknetpftbgt</td><td align=right></td><td align=right>$$with_commas2</td><td align=right></td><td align=right>$$mtdnetpftbgt</td><td align=right></td><td align=right>$$bgtnetpft</td><td align=right>$netpftprcnt%</td></tr>";$compl=$compl+$apwk3;$commtdpl=$commtdpl+$apwk3m;$distpl=$distpl+$apwk3;$distmtdpl=$distmtdpl+$apwk3m;}
    elseif ($goweek==4){$with_commas = number_format($apwk4);$with_commas2 = number_format($apwk4m);$message="$message<tr><td align=right>P&L:</td><td align=right>$$with_commas</td><td align=right></td><td align=right>$$wknetpftbgt</td><td align=right></td><td align=right>$$with_commas2</td><td align=right></td><td align=right>$$mtdnetpftbgt</td><td align=right></td><td align=right>$$bgtnetpft</td><td align=right>$netpftprcnt%</td></tr>";$compl=$compl+$apwk4;$commtdpl=$commtdpl+$apwk4m;$distpl=$distpl+$apwk4;$distmtdpl=$distmtdpl+$apwk4m;}
    elseif ($goweek==5){$with_commas = number_format($apwk5);$with_commas2 = number_format($apwk5m);$message="$message<tr><td align=right>P&L:</td><td align=right>$$with_commas</td><td align=right></td><td align=right>$$wknetpftbgt</td><td align=right></td><td align=right>$$with_commas2</td><td align=right></td><td align=right>$$mtdnetpftbgt</td><td align=right></td><td align=right>$$bgtnetpft</td><td align=right>$netpftprcnt%</td></tr>";$compl=$compl+$apwk5;$commtdpl=$commtdpl+$apwk5m;$distpl=$distpl+$apwk5;$distmtdpl=$distmtdpl+$apwk5m;}
    elseif ($goweek==6){$with_commas = number_format($apwk6);$with_commas2 = number_format($apwk6m);$message="$message<tr><td align=right>P&L:</td><td align=right>$$with_commas</td><td align=right></td><td align=right>$$wknetpftbgt</td><td align=right></td><td align=right>$$with_commas2</td><td align=right></td><td align=right>$$mtdnetpftbgt</td><td align=right></td><td align=right>$$bgtnetpft</td><td align=right>$netpftprcnt%</td></tr>";$compl=$compl+$apwk6;$commtdpl=$commtdpl+$apwk6m;$distpl=$distpl+$apwk6;$distmtdpl=$distmtdpl+$apwk6m;}


/////////FIND EXCEPTIONS/////////////////////////////////////////////////////////////////////////////////////////

    if ($goweek==1){$error1=$coswk1prcnt-$wkcosbgt;$error2=$lbrwk1prcnt-$wklaborbgt;$error3=$toewk1prcnt-$wktoebgt;$error4=$coswk1prcnt-$mtdcosbgt;$error5=$lbrwk1prcnt-$mtdlaborbgt;$error6=$toewk1prcnt-$mtdtoebgt;}
    elseif ($goweek==2){$error1=$coswk2prcnt-$wkcosbgt;$error2=$lbrwk2prcnt-$wklaborbgt;$error3=$toewk2prcnt-$wktoebgt;$error4=$coswk2prcntmtd-$mtdcosbgt;$error5=$lbrwk2prcntmtd-$mtdlaborbgt;$error6=$toewk2prcntmtd-$mtdtoebgt;}
    elseif ($goweek==3){$error1=$coswk3prcnt-$wkcosbgt;$error2=$lbrwk3prcnt-$wklaborbgt;$error3=$toewk3prcnt-$wktoebgt;$error4=$coswk3prcntmtd-$mtdcosbgt;$error5=$lbrwk3prcntmtd-$mtdlaborbgt;$error6=$toewk3prcntmtd-$mtdtoebgt;}
    elseif ($goweek==4){$error1=$coswk4prcnt-$wkcosbgt;$error2=$lbrwk4prcnt-$wklaborbgt;$error3=$toewk4prcnt-$wktoebgt;$error4=$coswk4prcntmtd-$mtdcosbgt;$error5=$lbrwk4prcntmtd-$mtdlaborbgt;$error6=$toewk4prcntmtd-$mtdtoebgt;}
    elseif ($goweek==5){$error1=$coswk5prcnt-$wkcosbgt;$error2=$lbrwk5prcnt-$wklaborbgt;$error3=$toewk5prcnt-$wktoebgt;$error4=$coswk5prcntmtd-$mtdcosbgt;$error5=$lbrwk5prcntmtd-$mtdlaborbgt;$error6=$toewk5prcntmtd-$mtdtoebgt;}
    elseif ($goweek==6){$error1=$coswk6prcnt-$wkcosbgt;$error2=$lbrwk6prcnt-$wklaborbgt;$error3=$toewk6prcnt-$wktoebgt;$error4=$coswk6prcntmtd-$mtdcosbgt;$error5=$lbrwk6prcntmtd-$mtdlaborbgt;$error6=$toewk6prcntmtd-$mtdtoebgt;}

    if ($error1 > 5 || $error1 < -5 || $error2 > 5 || $error2 < -5 || $error3 > 5 || $error3 < -5 || $error4 > 5 || $error4 < -5 || $error5 > 5 || $error5 < -5 || $error6 > 5 || $error6 < -5 || 1 == 1){

      
       $message7="<table><tr><td colspan=11> *WTD: ";
       if ($error1 > 5){$message7="$message7 <font color=red>COS $error1 pts over bdgt</font> - ";}
       elseif ($error1 < -5){$error1=$error1*-1;$message7="$message7 <font color=green>COS $error1 pts under bdgt</font> - ";}
       if ($error2 > 5){$message7="$message7 <font color=red>LABOR $error2 pts over bdgt</font> - ";}
       elseif ($error2 < -5){$error2=$error2*-1;$message7="$message7 <font color=green>LABOR $error2 pts under bdgt</font> - ";}
       if ($error3 > 5){$message7="$message7 <font color=red>TOE $error3 pts over bdgt</font>";}
       elseif ($error3 < -5){$error3=$error3*-1;$message7="$message7 <font color=green>TOE $error3 pts under bdgt</font>";}
       fwrite($fh3, "$message7");
       $message7="$message7 </td></tr>";
    
       $message7="$message7 <tr><td colspan=11> *MTD: ";
       fwrite($fh3, "<tr><td colspan=11> *MTD: ");
       if ($error4 > 5){$message7="$message7 <font color=red>COS $error4 pts over bdgt</font> - ";fwrite($fh3, " <font color=red>COS $error4 pts over bdgt</font> - ");}
       elseif ($error4 < -5){$error4=$error4*-1;$message7="$message7 <font color=green>COS $error4 pts under bdgt</font> - ";fwrite($fh3, "<font color=green>COS $error4 pts under bdgt</font> - ");}
       if ($error5 > 5){$message7="$message7 <font color=red>LABOR $error5 pts over bdgt</font> - ";fwrite($fh3, " <font color=red>LABOR $error5 pts over bdgt</font> - ");}
       elseif ($error5 < -5){$error5=$error5*-1;$message7="$message7 <font color=green>LABOR $error5 pts under bdgt</font> - ";fwrite($fh3, " <font color=green>LABOR $error5 pts under bdgt</font> - ");}
       if ($error6 > 5){$message7="$message7 <font color=red>TOE $error6 pts over bdgt</font>";fwrite($fh3, " <font color=red>TOE $error6 pts over bdgt</font>");}
       elseif ($error6 < -5){$error6=$error6*-1;$message7="$message7 <font color=green>TOE $error6 pts under bdgt</font>";fwrite($fh3, " <font color=green>TOE $error6 pts under bdgt</font>");}
       $message7="$message7 </td></tr></table>";

       if($goweek==1){$showlaborhour=round($saleswk1/$wk1laborhour,0);$show_labor=$wk1laborhour;}
       elseif($goweek==2){$showlaborhour=round($saleswk2/$wk2laborhour,0);$show_labor=$wk2laborhour;}
       elseif($goweek==3){$showlaborhour=round($saleswk3/$wk3laborhour,0);$show_labor=$wk3laborhour;}
       elseif($goweek==4){$showlaborhour=round($saleswk4/$wk4laborhour,0);$show_labor=$wk4laborhour;}
       elseif($goweek==5){$showlaborhour=round($saleswk5/$wk5laborhour,0);$show_labor=$wk5laborhour;}
       elseif($goweek==6){$showlaborhour=round($saleswk6/$wk6laborhour,0);$show_labor=$wk6laborhour;}

       $showlaborhourtotal=round($salestotal/($wk1laborhour+$wk2laborhour+$wk3laborhour+$wk4laborhour+$wk5laborhour+$wk6laborhour),0);

    }
    $message="$message</table>";
    echo "$message<br>";
    echo "$message7";
    $error1=0;$error2=0;$error3=0;$error4=0;$error5=0;$error6=0;

/////////MEMO///////////////////////////////////////////////////////////////////////////////////////////
       $cs_year = date("Y");
       $cs_month = date("m");

       $query56 = "SELECT * FROM cs_memo WHERE year = '$cs_year' AND month = '$cs_month' AND week = '$goweek' AND businessid = '$businessid'";
       $result56 = Treat_DB_ProxyOld::query($query56);
       $num56=Treat_DB_ProxyOld::mysql_numrows($result56);
       //mysql_close();

       $memo=@Treat_DB_ProxyOld::mysql_result($result56,0,"memo");

       echo "$memo";
 
   $num99--;   
} 

//mysql_close();

//////END TABLE////////////////////////////////////////////////////////////////////////////////////

}
?>