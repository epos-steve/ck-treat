<?php

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+3);
        if ($diff > 0 && $diff < '0.01'){$diff="0.01";}
        elseif($diff < 0 && $diff > '-0.01'){$diff="-0.01";}
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      if ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='32')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='08' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      $day=$day-31;  
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";} 
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

define('DOC_ROOT', dirname(dirname(__FILE__)));
require_once(DOC_ROOT.'/bootstrap.php');

$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";

$user=$_COOKIE["usercook"];
$pass=$_COOKIE["passcook"];
$companyid=$_POST["companyid"];
$businessid=$_POST["businessid"];
$itemid=$_POST["itemid"];
$new_item=$_POST["new_item"];
$newitem_code=$_POST["newitem_code"];
$notes=$_POST["notes"];
$del=$_GET["del"];

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=mysql_numrows($result);


$security_level=mysql_result($result,0,"security_level");
$bid=mysql_result($result,0,"businessid");
$cid=mysql_result($result,0,"companyid");

if ($num != 1)
{
    echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}
else
{


/////////////////////////////////////////////////////////////////////////////////

       $item_name=$_POST["item_name"];
       $item_name=str_replace("'","`",$item_name);
       $calories=$_POST["calories"];
       $protein=$_POST["protein"];
       $unsat_fat=$_POST["unsat_fat"];
       $sat_fat=$_POST["sat_fat"];
       $total_fat=$_POST["total_fat"];
       $fiber=$_POST["fiber"];
       $cholesterol=$_POST["cholesterol"];
       $sodium=$_POST["sodium"];
       $iron=$_POST["iron"];
       $calcium=$_POST["calcium"];
       $vit_A=$_POST["vit_A"];
       $thiamine=$_POST["thiamine"];
       $riboflavin=$_POST["riboflavin"];
       $vit_C=$_POST["vit_C"];
       $niacin=$_POST["niacin"];
       $magnesium=$_POST["magnesium"];
       $zinc=$_POST["zinc"];
       $phosphorus=$_POST["phosphorus"];
       $potassium=$_POST["potassium"];
       $complex_cabs=$_POST["complex_cabs"];
       $sugar=$_POST["sugar"];
       $cal_from_fat=$_POST["cal_from_fat"];
       $weight_density=$_POST["weight_density"];
       $serv_size_gram=$_POST["serv_size_gram"];
       $hh_units=$_POST["hh_units"];
       $rec_size=$_POST["rec_size"];
       $notes=$_POST["notes"];

    if($del==1){
       $item_code=$_GET["item_code"];
       $companyid=$_GET["cid"];

       $query = "DELETE FROM nutrition WHERE item_code = '$item_code'";
       $result = Treat_DB_ProxyOld::query($query);
    }
    elseif ($new_item=="NEW"&&$item_name!=""&&$newitem_code!=""){

       $query = "INSERT INTO nutrition (item_code,item_name,calories,protein,unsat_fat,sat_fat,total_fat,fiber,cholesterol,sodium,iron,calcium,vit_A,thiamine,riboflavin,vit_C,niacin,magnesium,zinc,phosphorus,potassium,complex_cabs,sugar,cal_from_fat,weight_density,serv_size_gram,serv_size_hh,rec_size,notes,date,user,inv_rec_size,inv_rec_num,inv_rec_size2,inv_rec_num2) VALUES ('$newitem_code','$item_name','$calories','$protein','$unsat_fat','$sat_fat','$total_fat','$fiber','$cholesterol','$sodium','$iron','$calcium','$vit_A','$thiamine','$riboflavin','$vit_C','$niacin','$magnesium','$zinc','$phosphorus','$potassium','$complex_cabs','$sugar','$cal_from_fat','$weight_density','$serv_size_gram','$serv_size_hh','$rec_size','$notes','$today','$user','$inv_rec_size','$inv_rec_num','$inv_rec_size2','$inv_rec_num2')";
       $result = Treat_DB_ProxyOld::query($query);
       $num=mysql_numrows($result);

    }
    else{

       $query = "UPDATE nutrition SET item_name='$item_name', calories='$calories', protein='$protein', unsat_fat='$unsat_fat', sat_fat='$sat_fat', total_fat='$total_fat', fiber='$fiber', cholesterol='$cholesterol', sodium='$sodium', iron='$iron', calcium='$calcium', vit_A='$vit_A', thiamine='$thiamine', riboflavin='$riboflavin', vit_C='$vit_C', niacin='$niacin', magnesium='$magnesium', zinc='$zinc', phosphorus='$phosphorus', potassium='$potassium', complex_cabs='$complex_cabs', sugar='$sugar', cal_from_fat='$cal_from_fat', weight_density='$weight_density', serv_size_gram='$serv_size_gram', serv_size_hh='$serv_size_hh', rec_size='$rec_size', notes='$notes', date = '$today', user = '$user', inv_rec_size = '$inv_rec_size', inv_rec_num = '$inv_rec_num', inv_rec_size2 = '$inv_rec_size2', inv_rec_num2 = '$inv_rec_num2' WHERE item_code = '$itemid'";
       $result = Treat_DB_ProxyOld::query($query);
       $num=mysql_numrows($result);

    }


    $location="caternutrition.php?cid=$companyid&bid=$businessid";
    header('Location: ./' . $location);
       
}
?>