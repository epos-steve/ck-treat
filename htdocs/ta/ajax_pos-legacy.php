<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once(DOC_ROOT.'/bootstrap.php');

// ajax fix for ie
mb_internal_encoding( "UTF-8" );
mb_http_output( "UTF-8" );
ob_start( "mb_output_handler" );

function error_exit( $errorMessage ) {
	if( stristr( $_SERVER['HTTP_ACCEPT'], 'application/json' ) )
		$errorOut = '{"error":"' . $errorMessage . '"}';
	else $errorOut = '<p>' . $errorMessage . '</p>';
	
	echo $errorOut;
	exit( 1 );
}

$rqFile = $_SERVER['PHP_SELF'];

// initialize db connection so mysql_real_escape_string can be used
$query = 'SELECT 1';
Treat_DB_ProxyOld::query( $query );

/*$user = isset( $_COOKIE["usercook"] ) ? mysql_real_escape_string( $_COOKIE["usercook"] ) : '';
$pass = isset( $_COOKIE["passcook"] ) ? mysql_real_escape_string( $_COOKIE["passcook"] ) : '';*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook', '');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook', '');

$query = "
	SELECT 
		login.*, 
		1 AS sec_pos_setup
	FROM login 
	JOIN security ON login.security = security.securityid
	WHERE username = '$user' 
	AND password = '$pass'
";
$result = Treat_DB_ProxyOld::query( $query );
$num = Treat_DB_ProxyOld::mysql_num_rows( $result );

$bypass = $_GET['bypass'] == 'essent1al';

if( !$bypass && $num != 1 ) {
	error_exit( 'Not logged in' );
}

$loginid = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'loginid' );
$sec_pos_setup = $bypass ? 1 : @Treat_DB_ProxyOld::mysql_result( $result, 0, 'sec_pos_setup' );

if( $sec_pos_setup == 0 ) {
	error_exit( 'Access denied' );
}

$todo = intval( $_REQUEST['todo'] );

$todoList = array(
	1 => 'updateMenuTax',
	2 => 'updateMenuGroups',
	3 => 'updateCustomer',
	4 => 'updateCustomerScancodes',
	5 => 'processCustomerTransaction',
	6 => 'menuCheckDetail',
	7 => 'processMenuTax',
	8 => 'resetForSync',
	9 => 'resetBusiness',
	10 => 'updatePromotions',
	11 => 'getPromoTargets',
	12 => 'updatePromoTargets',
	13 => NULL,
	14 => 'getPromoSchedule',
	15 => 'updatePromoSchedule',
	16 => 'getPromotions',
	17 => 'togglePromoActive',
	18 => 'reorderPromo'
);

if( ( $fn = $todoList[$todo] ) ) {
	$fn( );
}
else error_exit( 'Bad function' );



///////////////////////////////////////////////

/*
 * Begin ajax functions
 */

function updateMenuTax( ) {
	$bid = htmlspecialchars( trim( $_POST['bid'] ) );
	$tax_name = htmlspecialchars( trim( $_POST['tax_name'] ) );
	$tax_percent = htmlspecialchars( trim( $_POST['tax_percent'] ) );
	$id = htmlspecialchars( trim( $_POST['id'] ) );
	
	if( $id > 0 ) {
		$query = "UPDATE menu_tax SET name = '$tax_name', percent = '$tax_percent', pos_processed = 0 WHERE id = $id";
		$result = Treat_DB_ProxyOld::query( $query );
	}
	else {
		$query = "SELECT MAX(pos_id) as pos_id FROM menu_tax WHERE businessid = $bid";
		$result = Treat_DB_ProxyOld::query( $query );
		
		$pos_id = Treat_DB_ProxyOld::mysql_result( $result, 0, "pos_id" );
		$pos_id++;
		
		$uuid = \EE\Model\Menu\ItemNew::genUuid();
		$query = "INSERT INTO menu_tax (pos_id,businessid,name,percent, uuid_menu_tax) VALUES ($pos_id,$bid,'$tax_name','$tax_percent', '$uuid')";
		$result = Treat_DB_ProxyOld::query( $query );
		$id = Treat_DB_ProxyOld::mysql_insert_id( $result );
	}
	
	Treat_Model_KioskSync::addSync( $bid, $id, Treat_Model_KioskSync::TYPE_TAX );
}

function updateMenuGroups( ) {
	$bid = htmlspecialchars( trim( $_POST['bid'] ) );
	$group_name = htmlspecialchars( trim( $_POST['group_name'] ) );
	$id = htmlspecialchars( trim( $_POST['id'] ) );
	
	if( $id > 0 ) {
		$query = "UPDATE menu_groups_new SET name = '$group_name', pos_processed = 0 WHERE id = $id";
		$result = Treat_DB_ProxyOld::query( $query );
		Treat_Model_KioskSync::addSync( $bid, $id, Treat_Model_KioskSync::TYPE_GROUP );
	}
	else {
		$new_group = \EE\Model\Menu\GroupsNew::create($bid);
		$new_group->name = $group_name;
		$new_group->save();
	}
}

function updateCustomer( ) {
	$bid = htmlspecialchars( trim( $_POST['bid'] ) );
	$customerid = htmlspecialchars( trim( $_POST['customerid'] ) );
	$email = htmlspecialchars( trim( $_POST['email'] ) );
	$user_pass = htmlspecialchars( trim( $_POST['user_pass'] ) );
	$last_name = htmlspecialchars( trim( $_POST['last_name'] ) );
	$first_name = htmlspecialchars( trim( $_POST['first_name'] ) );
	$scancode = htmlspecialchars( trim( $_POST['scancode'] ) );
	
	if( $customerid > 0 ) {
		$query = "UPDATE customer SET pos_processed = 0, username = '$email', last_name = '$last_name', first_name = '$first_name', scancode = '$scancode' WHERE customerid = $customerid";
		$result = Treat_DB_ProxyOld::query( $query );
	}
	else {
		$query = "INSERT INTO customer (pos_processed,username,password,businessid,scancode,first_name,last_name) VALUES (0,'$email','password',$bid,'$scancode','$first_name','$last_name')";
		$result = Treat_DB_ProxyOld::query( $query );
	}
}

function updateCustomerScancodes( ) {
	$bid = htmlspecialchars( trim( $_POST['bid'] ) );
	$scan_start = htmlspecialchars( trim( $_POST['scan_start'] ) );
	$scan_end = htmlspecialchars( trim( $_POST['scan_end'] ) );
	$scan_value = htmlspecialchars( trim( $_POST['scan_value'] ) );
	$scan_id = htmlspecialchars( trim( $_POST['scan_id'] ) );
	
	if( $scan_id > 0 ) {
		$query = "UPDATE customer_scancodes SET scan_start = '$scan_start', scan_end = '$scan_end', value = '$scan_value' WHERE id = $scan_id";
		$result = Treat_DB_ProxyOld::query( $query );
	}
	else {
		$query = "INSERT INTO customer_scancodes (businessid,scan_start,scan_end,value) VALUES ($bid,'$scan_start','$scan_end','$scan_value')";
		$result = Treat_DB_ProxyOld::query( $query );
	}
}

function processCustomerTransaction( ) {
	$cust_id = htmlspecialchars( trim( $_POST['cust_id'] ) );
	$dol_value = htmlspecialchars( trim( $_POST['dol_value'] ) );
	$transtype = htmlspecialchars( trim( $_POST['transtype'] ) );
	
	if( $transtype == 3 ) {
		$reason = "refund";
	}
	elseif( $transtype == 4 ) {
		$reson = "promo";
	}
	
	$audit = isset( $_COOKIE["usercook"] ) ? $_COOKIE["usercook"] : '';
	
	$query = "INSERT INTO customer_transactions (oid,trans_type_id,customer_id,subtotal,chargetotal,response,authresponse) VALUES
												('$reason',$transtype,$cust_id,'$dol_value','$dol_value','APPROVED','$audit')";
	$result = Treat_DB_ProxyOld::query( $query );
	
	///update balance
	$query = "UPDATE customer SET balance = balance + $dol_value WHERE customerid = $cust_id";
	$result = Treat_DB_ProxyOld::query( $query );
}

function menuCheckDetail( ) {
	$customerid = htmlspecialchars( trim( $_POST['customerid'] ) );
        $date1 = htmlspecialchars( trim( $_POST['date1'] ) );
        $date2 = htmlspecialchars( trim( $_POST['date2'] ) );
	
	include( 'lib/class.menu_items.php' );
	menu_items::check_detail( $customerid, $date1, $date2 );
}

function processMenuTax( ) {
	$groupid = htmlspecialchars( trim( $_POST['groupid'] ) );
	$taxid = htmlspecialchars( trim( $_POST['taxid'] ) );
	$which = htmlspecialchars( trim( $_POST['which'] ) );
	
	$query = "SELECT id,businessid FROM menu_items_new WHERE group_id = $groupid";
	$result = Treat_DB_ProxyOld::query( $query );
	
	$counter = 0;
	while( $r = Treat_DB_ProxyOld::mysql_fetch_array( $result ) ) {
		$id = $r["id"];
		
		if( $which == 1 ) {
			$uuid = \EE\Model\Menu\ItemNew::genUuid();
			$query2 = "INSERT INTO menu_item_tax (menu_itemid,tax_id,uuid_menu_item_tax) VALUES ($id,$taxid, '$uuid')";
			$counter++;
		}
		elseif( $which == 2 ) {
			$query2 = "DELETE FROM menu_item_tax WHERE menu_itemid = $id AND tax_id = $taxid";
			$counter++;
		}
		$result2 = Treat_DB_ProxyOld::query( $query2 );
		
		Treat_Model_KioskSync::addSync( $r['businessid'], $id, Treat_Model_KioskSync::TYPE_ITEM );
		Treat_Model_KioskSync::addSync( $r['businessid'], $id, Treat_Model_KioskSync::TYPE_ITEM_TAX );
	}
	
	$query = "UPDATE menu_items_new SET pos_processed = 0 WHERE group_id = $groupid";
	$result = Treat_DB_ProxyOld::query( $query );
	
	echo "Updated $counter items";
}

function resetForSync( ) {
	$bid = htmlspecialchars( trim( $_POST['bid'] ) );
	$audit = isset( $_COOKIE["usercook"] ) ? $_COOKIE["usercook"] : '';
	
	///taxes
	$query = "UPDATE menu_tax SET pos_processed = 0 WHERE businessid = $bid";
	$result = Treat_DB_ProxyOld::query( $query );
	Treat_Model_KioskSync::addType( $bid, Treat_Model_KioskSync::TYPE_TAX );
	Treat_Model_KioskSync::addType( $bid, Treat_Model_KioskSync::TYPE_ITEM_TAX );
	
	///groups
	$query = "UPDATE menu_groups_new SET pos_processed = 0 WHERE businessid = $bid";
	$result = Treat_DB_ProxyOld::query( $query );
	Treat_Model_KioskSync::addType( $bid, Treat_Model_KioskSync::TYPE_GROUP );
	
	///items
	$query = "UPDATE menu_items_new SET pos_processed = 0 WHERE businessid = $bid";
	$result = Treat_DB_ProxyOld::query( $query );
	Treat_Model_KioskSync::addType( $bid, Treat_Model_KioskSync::TYPE_ITEM );
	
	///customers
	$query = "UPDATE customer SET pos_processed = 0 WHERE businessid = $bid";
	$result = Treat_DB_ProxyOld::query( $query );
	
	///promotions
	$query = "UPDATE promotions SET pos_processed = 0 WHERE businessid = $bid";
	$result = Treat_DB_ProxyOld::query( $query );
	Treat_Model_KioskSync::addType( $bid, Treat_Model_KioskSync::TYPE_PROMO );
	
	/*
	///balances
	$query = "SELECT * FROM customer WHERE businessid = $bid";
	$result = DB_Proxy::query($query);
	
	while($r = mysql_fetch_array($result)){
	    $customerid = $r["customerid"];
	    $balance = $r["balance"];
	
	    $query2 = "INSERT INTO customer_transactions (oid,trans_type_id,customer_id,subtotal,chargetotal,response,authresponse) VALUES
	                                                ('reset',5,$customerid,'$balance','$balance','APPROVED','$audit')";
	    $result2 = DB_Proxy::query($query2);
	}
	 */
	
	echo "Everything Reset for Sync";
}

function resetBusiness( ) {
	$bid = htmlspecialchars( trim( $_POST['bid'] ) );
	
	$query = "UPDATE business SET setup = 2 WHERE businessid = $bid";
	$result = Treat_DB_ProxyOld::query( $query );
}

function updatePromotions( ) {
	$bid = htmlspecialchars( trim( $_POST['bid'] ) );
	$promo_name = htmlspecialchars( trim( $_POST['promo_name'] ) );
	$promo_type = htmlspecialchars( trim( $_POST['promo_type'] ) );
	$promo_target = htmlspecialchars( trim( $_POST['promo_target'] ) );
	$promo_value = htmlspecialchars( trim( $_POST['promo_value'] ) );
	$promo_trigger = htmlspecialchars( trim( $_POST['promo_trigger'] ) );
	$promo_tax = intval( $_POST['promo_tax'] );
	$promo_tax = $promo_tax ? : 'NULL';
	$promo_discount = htmlspecialchars( trim( $_POST['promo_option_discount'] ) );
	$promo_discount_type = htmlspecialchars( trim( $_POST['promo_option_discount_type'] ) );
	$promo_discount_limit = intval( $_POST['promo_option_discount_limit'] );
	$promo_discount_n = intval( $_POST['promo_option_discount_n'] );
	$promo_trigger_amount_limit = htmlspecialchars( trim( $_POST['promo_trigger_amount_limit'] ) );
	$promo_trigger_amount_type = htmlspecialchars( trim( $_POST['promo_trigger_amount_type'] ) );
	$promo_trigger_amount = intval( $_POST['promo_trigger_amount'] );
	$promo_trigger_amount_2 = intval( $_POST['promo_trigger_amount_2'] );
	$id = intval( $_POST['promo_id'] );
	
	if( $id > 0 ) {
		$query = "UPDATE promotions SET 
					name = '$promo_name', 
					type = '$promo_type', 
					target = '$promo_target', 
					value = '$promo_value', 
					promo_trigger = '$promo_trigger', 
					tax_id = $promo_tax,
					promo_discount = '$promo_discount',
					promo_discount_type = '$promo_discount_type',
					promo_discount_limit = '$promo_discount_limit',
					promo_discount_n = '$promo_discount_n',
					promo_trigger_amount_limit = '$promo_trigger_amount_limit',
					promo_trigger_amount_type = '$promo_trigger_amount_type',
					promo_trigger_amount = '$promo_trigger_amount',
					promo_trigger_amount_2 = '$promo_trigger_amount_2'
				WHERE id = $id";
		$result = Treat_DB_ProxyOld::query( $query );
		
		$message = "Record Updated";
	}
	else {
		$pos_id = 0;
		
		$query = "SELECT MAX(pos_id)+1 AS pos_id, MAX(rule_order)+1 AS rule_order FROM promotions WHERE businessid = $bid";
		$result = Treat_DB_ProxyOld::query( $query );
		
		//$v = @mysql_fetch_assoc( $result );
		$v = @Treat_DB_ProxyOld::mysql_fetch_array( $result,MYSQL_ASSOC );
		
		$uuid = \EE\Model\Promotion::genUuid();
		$query = "INSERT INTO promotions (businessid,pos_id,name,type,target,value,promo_trigger,rule_order,tax_id, uuid_promotions) VALUES ($bid,{$v['pos_id']},'$promo_name','$promo_type','$promo_target','$promo_value','$promo_trigger',{$v['rule_order']},'$promo_tax', '$uuid')";
		$result = Treat_DB_ProxyOld::query( $query );
		$id = Treat_DB_ProxyOld::mysql_insert_id( );
		
		$message = "Record Added";
	}
	
	Treat_Model_KioskSync::addSync( $bid, $id, Treat_Model_KioskSync::TYPE_PROMO );
	
	echo json_encode( array( 'promo_id' => $id, 'message' => $message ) );
}

function getPromoTargets( ) {
	$promo_id = htmlspecialchars( trim( $_REQUEST['promoid'] ) );
	
	$query = "SELECT
				promotions.businessid,
				promo_trigger.target_id,
				promotions.promo_trigger
				FROM promotions
				JOIN promo_trigger ON promotions.promo_trigger = promo_trigger.id
				WHERE promotions.id = $promo_id";
	$result = Treat_DB_ProxyOld::query( $query );
	
	$promo_trigger = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'promo_trigger' );
	
	$bid = Treat_DB_ProxyOld::mysql_result( $result, 0, "businessid" );
	$target = Treat_DB_ProxyOld::mysql_result( $result, 0, "target_id" );
	
	if( $target == 1 ) {
		$query = "SELECT
					menu_items_new.*
					,menu_groups_new.name AS group_name
					FROM menu_items_new
					JOIN menu_groups_new ON menu_groups_new.id = menu_items_new.group_id
					WHERE menu_items_new.businessid = $bid AND menu_items_new.active = 0 ORDER BY menu_items_new.group_id,menu_items_new.name";
		$result = Treat_DB_ProxyOld::query( $query );
		
		//echo "<table width=100% style=\"border:1px solid black;\" cellspacing=0 cellpadding=0><tr valign=top>";
		echo "<div class='settingsTable'><div class='settingsRow'><div class='settingsThird'>";
		
		$lastgroup = 0;
		$counter = 1;
		while( $r = Treat_DB_ProxyOld::mysql_fetch_array( $result ) ) {
			$id = $r["pos_id"];
			$name = $r["name"];
			$group_name = $r["group_name"];
			
			if( $lastgroup != $group_name ) {
				//if($lastgroup != 0){echo "</td>";}
				//if($counter == 4){echo "</tr><tr valign=top>";$counter = 1;}
				//echo "<td style=\"border:1px solid black;font-size:11px;\"><div style=\"background-color:#999999;width:100%;font-weight:bold;border-bottom:2px solid black;color:white;\">&nbsp;$group_name</div>";
				if( $lastgroup != 0 ) {
					echo "</div></div>";
					if( $counter == 4 ) {
						echo "</div><div class='settingsRow'>";
						$counter = 1;
					}
					echo "<div class='settingsThird'>";
				}
				echo "<div class='settingsHeader ui-state-default'>$group_name</div><div class='settingsForm'>";
				$counter++;
			}
			
			$query2 = "SELECT * FROM promo_detail WHERE promo_id = $promo_id AND type = 1 AND id = $id";
			$result2 = Treat_DB_ProxyOld::query( $query2 );
			$num2 = Treat_DB_ProxyOld::mysql_numrows( $result2 );
			
			if( $num2 == 1 ) {
				$val = "CHECKED";
			}
			else {
				$val = "";
			}
			
			echo "<div class='settingsItem'><input type=checkbox name=mid$id id='mid$id' value=1 onchange=\"promo_detail($promo_id,1,$id);\" $val /><label for='mid$id'>$name</label></div>";
			
			$lastgroup = $group_name;
		}
		
		echo "</div></div>";
	}
	elseif( $target == 2 ) {
		$query = "SELECT * FROM menu_groups_new WHERE businessid = $bid ORDER BY name";
		$result = Treat_DB_ProxyOld::query( $query );
		
		//echo "<table width=100% style=\"border:2px solid black;\" cellspacing=0 cellpadding=0><tr><td>";
		//echo "<div style=\"background-color:#999999;width:100%;font-weight:bold;border-bottom:2px solid black;color:white;\">&nbsp;Menu Groups</div>";
		echo '<div class="settingsContainer"><div class="settingsHeader ui-state-default">Menu Groups</div><div class="settingsForm">';
		
		while( $r = Treat_DB_ProxyOld::mysql_fetch_array( $result ) ) {
			$id = $r["pos_id"];
			$name = $r["name"];
			
			$query2 = "SELECT * FROM promo_detail WHERE promo_id = $promo_id AND type = 2 AND id = $id";
			$result2 = Treat_DB_ProxyOld::query( $query2 );
			$num2 = Treat_DB_ProxyOld::mysql_numrows( $result2 );
			
			if( $num2 == 1 ) {
				$val = "CHECKED";
			}
			else {
				$val = "";
			}
			
			echo "<div class='settingsItem settingsFloat'><input type=checkbox name=gid$id id='gid$id' value=1 onclick=\"promo_detail($promo_id,2,$id);\" $val /><label for='gid$id'>$name</label></div>";
		}
		
		//echo "</td></tr></table>";
		echo '</div></div>';
	}
	elseif( $target == 3 ) {
		echo '<div class="settingsContainer"><div class="settingsHeader ui-state-default">All Products Selected</div></div>';
		exit( );
	}
	elseif( $target == 4 ) {
		$query = "SELECT * FROM menu_groups_new WHERE businessid = $bid ORDER BY name";
		$result = Treat_DB_ProxyOld::query( $query );
		
		//echo "<table width=100% style=\"border:2px solid black;\" cellspacing=0 cellpadding=0><tr><td>";
		//echo "<div style=\"background-color:#999999;width:100%;font-weight:bold;border-bottom:2px solid black;color:white;\">&nbsp;Menu Groups</div>";
		echo '<div class="settingsContainer"><div class="settingsHeader ui-state-default">Menu Groups</div><div class="settingsForm">';
		
		$selectedOptions = array( );
		$query2 = "SELECT * FROM promo_detail WHERE promo_id = $promo_id AND ( type = 31 OR type = 32 )";
		$result2 = Treat_DB_ProxyOld::query( $query2 );
		//while( $r = mysql_fetch_assoc( $result2 ) ) {
		while( $r = Treat_DB_ProxyOld::mysql_fetch_array( $result2,MYSQL_ASSOC ) ) {
			$selectedOptions[$r['type']] = $r['id'];
		}
		
		$menuGroupOptions = '<option value="">(none)</option>';
		while( $r = Treat_DB_ProxyOld::mysql_fetch_array( $result ) ) {
			$id = $r["pos_id"];
			$name = $r["name"];
			
			$menuGroupOptions .= "<option value='$id'>$name</option>";
		}
		
		echo '<label for="promo_group_1">1:</label> <select class="promoOption" name="promo_group_1" id="promo_group_1" onchange="promoDetailClear( '
				. $promo_id . ', 31, this );">' . $menuGroupOptions . '</select>';
		echo '<label for="promo_group_2">2:</label> <select class="promoOption" name="promo_group_2" id="promo_group_2" onchange="promoDetailClear( '
				. $promo_id . ', 32, this );">' . $menuGroupOptions . '</select>';
		echo '<script type="text/javascript" language="javascript">';
		echo 'jQuery(function(){';
		if( !empty( $selectedOptions[31] ) )
			echo 'jQuery("#promo_group_1 option[value=' . $selectedOptions[31]
					. ']").attr("selected","selected");';
		if( !empty( $selectedOptions[32] ) )
			echo 'jQuery("#promo_group_2 option[value=' . $selectedOptions[32]
					. ']").attr("selected","selected");';
		echo '});';
		echo '</script>';
		//echo "</td></tr></table>";
		echo '</div></div>';
	}
}

function updatePromoTargets( ) {
	$businessid = intval( $_POST['businessid'] );
	$promo_id = htmlspecialchars( trim( $_POST['promo_id'] ) );
	$id = htmlspecialchars( trim( $_POST['id'] ) );
	$type = htmlspecialchars( trim( $_POST['type'] ) );
	$promo_value = htmlspecialchars( trim( $_POST['promo_value'] ) );
	$clear = $_POST['clear'];
	
	if( $clear == 'true' ) {
		$query = "DELETE FROM promo_detail WHERE promo_id = $promo_id AND type = $type";
		Treat_DB_ProxyOld::query( $query );
	}
	
	if( $promo_value == "true" ) {
		$query = "INSERT INTO promo_detail (promo_id,type,id) VALUES ($promo_id,$type,$id)";
		$result = Treat_DB_ProxyOld::query( $query );
	}
	else {
		$query = "DELETE FROM promo_detail WHERE promo_id = $promo_id AND type = $type AND id = $id";
		$result = Treat_DB_ProxyOld::query( $query );
	}
	
	Treat_Model_KioskSync::addSync( $businessid, $promo_id, Treat_Model_KioskSync::TYPE_PROMO );
	
	echo $promo_value;
}

function getPromoSchedule( ) {
?>
<script language="javascript" type="text/javascript">
	jQuery(function(){
		jQuery('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' });
		
		jQuery('#schedule_form').submit(function(){
			var formData = jQuery(this).serialize( );
			jQuery.ajax({
				type: "POST",
				dataType: 'json',
				url: "ajax_pos.php",
				data: "todo=15&" + formData,
				success: function(data){
					if( data.error ) {
						alert( data.error );
					}
				}
			});
			
			return false;
		});
	});
</script>
<?
	for( $counter = 1; $counter <= 12; $counter++ ) {
		$hours .= "<option value=$counter>$counter</option>";
	}
	
	for( $counter = 0; $counter <= 55; $counter += 5 ) {
		if( strlen( $counter ) < 2 ) {
			$counter = "0$counter";
		}
		$minutes .= "<option value=$counter>$counter</option>";
	}
	
	$ampm = "<option value=am>AM</option><option value=pm>PM</option>";
	
	$businessid = intval( $_REQUEST['bid'] );
	$promo_id = htmlspecialchars( trim( $_REQUEST['promoid'] ) );
	
	$query = "SELECT * FROM promotions WHERE id = $promo_id";
	$result = Treat_DB_ProxyOld::query( $query );
	
	$start_date = Treat_DB_ProxyOld::mysql_result( $result, 0, "start_date" );
	$start_time = Treat_DB_ProxyOld::mysql_result( $result, 0, "start_time" );
	$end_date = Treat_DB_ProxyOld::mysql_result( $result, 0, "end_date" );
	$end_time = Treat_DB_ProxyOld::mysql_result( $result, 0, "end_time" );
	
	$monday = Treat_DB_ProxyOld::mysql_result( $result, 0, "monday" );
	if( $monday == 1 ) {
		$show_mon = "CHECKED";
	}
	$tuesday = Treat_DB_ProxyOld::mysql_result( $result, 0, "tuesday" );
	if( $tuesday == 1 ) {
		$show_tue = "CHECKED";
	}
	$wednesday = Treat_DB_ProxyOld::mysql_result( $result, 0, "wednesday" );
	if( $wednesday == 1 ) {
		$show_wed = "CHECKED";
	}
	$thursday = Treat_DB_ProxyOld::mysql_result( $result, 0, "thursday" );
	if( $thursday == 1 ) {
		$show_thu = "CHECKED";
	}
	$friday = Treat_DB_ProxyOld::mysql_result( $result, 0, "friday" );
	if( $friday == 1 ) {
		$show_fri = "CHECKED";
	}
	$saturday = Treat_DB_ProxyOld::mysql_result( $result, 0, "saturday" );
	if( $saturday == 1 ) {
		$show_sat = "CHECKED";
	}
	$sunday = Treat_DB_ProxyOld::mysql_result( $result, 0, "sunday" );
	if( $sunday == 1 ) {
		$show_sun = "CHECKED";
	}
	
	//echo "<table width=100% style=\"border:1px solid black;\" cellspacing=0 cellpadding=0>";
	//echo "<tr><td style=\"border:1px solid black;\"><div style=\"background-color:#999999;width:100%;font-weight:bold;color:white;\">&nbsp;Schedule</div></td></tr>";
	
	echo '<div class="settingsContainer"><div class="settingsHeader ui-state-default">Schedule</div>';
	
	//echo "<tr><td style=\"border:1px solid black;\">
	//			&nbsp;<form id='schedule_form' onsubmit='return false;' style=\"margin:0;padding:0;display:inline;\">
	
	echo '<form class="settingsForm" onsubmit="return false;" id="schedule_form">';
	
	echo "<input style='float: right;' type='submit' value='Update Schedule' />";
	
	echo "<input type='hidden' name='promo_id' value='$promo_id' />";
	echo "<input type='hidden' name='businessid' value='$businessid' />";
	echo "Start Date: <input type=text name='start_date' id='start_date' value='$start_date' size=10 class=\"datepicker\" />";
	echo "Start Time: <select class='settingsNospace' name=start_hour id='start_hour'>";
	
	for( $counter = 1; $counter <= 12; $counter++ ) {
		$hour = substr( $start_time, 0, 2 );
		if( $hour > 12 ) {
			$hour = $hour - 12;
		}
		if( $hour == $counter ) {
			$sel = 'SELECTED';
		}
		else {
			$sel = '';
		}
		echo "<option value=$counter $sel>$counter</option>";
	}
	
	echo "</select>:<select class='settingsNospace' name=start_min id='start_min'>";
	
	for( $counter = 0; $counter <= 55; $counter += 5 ) {
		if( strlen( $counter ) < 2 ) {
			$counter = "0$counter";
		}
		if( substr( $start_time, 3, 2 ) == $counter ) {
			$sel = 'SELECTED';
		}
		else {
			$sel = '';
		}
		echo "<option value=$counter $sel>$counter</option>";
	}
	
	echo "</select><select name=start_ampm id='start_ampm'>";
	
	if( substr( $start_time, 0, 2 ) > 11 ) {
		$sel = 'SELECTED';
	}
	else {
		$sel = '';
	}
	echo "<option value=am>AM</option><option value=pm $sel>PM</option>";
	
	echo "</select><br />";
	
	echo "End Date: <input type=text name='end_date' id='end_date' value='$end_date' size=10 class=\"datepicker\" />";
	echo "End Time: <select class='settingsNospace' name=end_hour id='end_hour'>";
	
	for( $counter = 1; $counter <= 12; $counter++ ) {
		$hour = substr( $end_time, 0, 2 );
		if( $hour > 12 ) {
			$hour = $hour - 12;
		}
		if( $hour == $counter ) {
			$sel = 'SELECTED';
		}
		else {
			$sel = '';
		}
		echo "<option value=$counter $sel>$counter</option>";
	}
	
	echo "</select>:<select class='settingsNospace' name=end_min id='end_min'>";
	
	for( $counter = 0; $counter <= 55; $counter += 5 ) {
		if( strlen( $counter ) < 2 ) {
			$counter = "0$counter";
		}
		if( substr( $end_time, 3, 2 ) == $counter ) {
			$sel = 'SELECTED';
		}
		else {
			$sel = '';
		}
		echo "<option value=$counter $sel>$counter</option>";
	}
	
	echo "</select><select name=end_ampm id='end_ampm'>";
	
	if( substr( $end_time, 0, 2 ) > 11 ) {
		$sel = 'SELECTED';
	}
	else {
		$sel = '';
	}
	echo "<option value=am>AM</option><option value=pm $sel>PM</option>";
	
	echo "</select><br>";
	
	echo "<span>Available Days:</span> ";
	
	$daysArray = array(
		'mon' => array(
			'name' => 'monday', 'show' => $show_mon
		),
		'tue' => array(
			'name' => 'tuesday', 'show' => $show_tue
		),
		'wed' => array(
			'name' => 'wednesday', 'show' => $show_wed
		),
		'thu' => array(
			'name' => 'thursday', 'show' => $show_thu
		),
		'fri' => array(
			'name' => 'friday', 'show' => $show_fri
		),
		'sat' => array(
			'name' => 'saturday', 'show' => $show_sat
		),
		'sun' => array(
			'name' => 'sunday', 'show' => $show_sun
		)
	);
	
	foreach( $daysArray as $sday => $day ) {
		echo "<div class='settingsItem'>";
		echo "<input type='checkbox' name='{$day['name']}' id='$sday' value='1' {$day['show']} />";
		echo "<label for='$sday'>" . ucfirst( $day['name'] ) . "</label></div>";
	}
	
	echo "</form></div>";
}

function updatePromoSchedule( ) {
	// run query to open db - needed for mysql_real_escape_string
	Treat_DB_ProxyOld::query( 'SELECT 1' );
	
	$businessid = intval( $_POST['businessid'] );
	$promo_id = intval( $_POST['promo_id'] );
	$start_date = mysql_real_escape_string( $_POST['start_date'] );
	$_start_hour = mysql_real_escape_string( $_POST['start_hour'] );
	$_start_min = mysql_real_escape_string( $_POST['start_min'] );
	$_start_ampm = $_POST['start_ampm'] == 'am' ? 0 : 12;
	if( $_start_hour == 12 )
		$_start_hour = 0;
	$start_time = ( $_start_hour + $_start_ampm ) . ':' . $_start_min;
	$end_date = mysql_real_escape_string( $_POST['end_date'] );
	$_end_hour = mysql_real_escape_string( $_POST['end_hour'] );
	$_end_min = mysql_real_escape_string( $_POST['end_min'] );
	$_end_ampm = $_POST['end_ampm'] == 'am' ? 0 : 12;
	if( $_end_hour == 12 )
		$_end_hour = 0;
	$end_time = ( $_end_hour + $_end_ampm ) . ':' . $_end_min;
	$monday = isset( $_POST['monday'] ) ? 1 : 0;
	$tuesday = isset( $_POST['tuesday'] ) ? 1 : 0;
	$wednesday = isset( $_POST['wednesday'] ) ? 1 : 0;
	$thursday = isset( $_POST['thursday'] ) ? 1 : 0;
	$friday = isset( $_POST['friday'] ) ? 1 : 0;
	$saturday = isset( $_POST['saturday'] ) ? 1 : 0;
	$sunday = isset( $_POST['sunday'] ) ? 1 : 0;
	
	$returnArray = array(
		'promo_id' => $promo_id
	);
	
	$query = "
		UPDATE promotions SET
			start_date = '$start_date',
			start_time = '$start_time',
			end_date = '$end_date',
			end_time = '$end_time',
			monday = $monday,
			tuesday = $tuesday,
			wednesday = $wednesday,
			thursday = $thursday,
			friday = $friday,
			saturday = $saturday,
			sunday = $sunday
		WHERE id = $promo_id
		AND businessid = $businessid
	";
	Treat_DB_ProxyOld::query( $query );
	
	if( mysql_errno( ) ) {
		$error = "An error occurred: " . mysql_error( );
		$returnArray = array(
			'error' => $error
		);
	}
	
	$returnArray['query'] = $query;
	
	Treat_Model_KioskSync::addSync( $businessid, $promo_id, Treat_Model_KioskSync::TYPE_PROMO );
	
	echo json_encode( $returnArray );
}

function getPromotions( ) {
	$businessid = intval( $_GET['bid'] );
	$query = "SELECT * FROM promotions WHERE businessid=$businessid ORDER BY rule_order";
	$result = Treat_DB_ProxyOld::query( $query );
	
	$data = array( );
	//while( $r = mysql_fetch_assoc( $result ) ) {
	while( $r = Treat_DB_ProxyOld::mysql_fetch_array( $result,MYSQL_ASSOC ) ) {
		$d = array( );
		$d['promo_id'] = $r['id'];
		$d['promo_name'] = $r['name'];
		$d['promo_type'] = $r['type'];
		$d['promo_value'] = $r['value'];
		$d['promo_trigger'] = $r['promo_trigger'];
		$d['promo_target'] = $r['target'];
		$d['promo_active'] = $r['activeFlag'];
		$d['promo_order'] = $r['rule_order'];
		$d['promo_tax'] = $r['tax_id'];
		$d['promo_discount'] = $r['promo_discount'];
		$d['promo_discount_type'] = $r['promo_discount_type'];
		$d['promo_discount_n'] = $r['promo_discount_n'];
		$d['promo_discount_limit'] = $r['promo_discount_limit'];
		$d['promo_trigger_amount_limit'] = $r['promo_trigger_amount_limit'];
		$d['promo_trigger_amount_type'] = $r['promo_trigger_amount_type'];
		$d['promo_trigger_amount'] = $r['promo_trigger_amount'];
		$d['promo_trigger_amount_2'] = $r['promo_trigger_amount_2'];
		$data[] = $d;
	}
	
	$query = "SELECT id,vivipos_name FROM promo_type";
	$result = Treat_DB_ProxyOld::query( $query );
	
	$viviposTypes = array( );
	//while( $r = mysql_fetch_assoc( $result ) ) {
	while( $r = Treat_DB_ProxyOld::mysql_fetch_array( $result,MYSQL_ASSOC ) ) {
		$viviposTypes[$r['id']] = $r['vivipos_name'];
	}
	
	$query = "SELECT id,vivipos_name FROM promo_trigger";
	$result = Treat_DB_ProxyOld::query( $query );
	
	$viviposTriggers = array( );
	//while( $r = mysql_fetch_assoc( $result ) ) {
	while( $r = Treat_DB_ProxyOld::mysql_fetch_array( $result,MYSQL_ASSOC ) ) {
		$viviposTriggers[$r['id']] = $r['vivipos_name'];
	}
	
	echo json_encode( 
		array( 
			'promos' => $data,
			'viviposTypes' => $viviposTypes,
			'viviposTriggers' => $viviposTriggers
		)
	);
}

function togglePromoActive( ) {
	$businessid = intval( $_REQUEST['bid'] );
	$promo_id = intval( $_REQUEST['promo_id'] );
	$query = "UPDATE promotions SET activeFlag=ABS( activeFlag - 1 ) WHERE id=$promo_id AND businessid=$businessid";
	Treat_DB_ProxyOld::query( $query );
	
	if( Treat_DB_ProxyOld::mysql_affected_rows( ) != 1 ) {
		echo json_encode( array( 'error' => mysql_error( ) ) );
	}
	else {
		Treat_Model_KioskSync::addSync( $businessid, $promo_id, Treat_Model_KioskSync::TYPE_PROMO );
		echo json_encode( array( 'promo_id' => $promo_id ) );
	}
}

function reorderPromo( ) {
	$businessid = intval( $_REQUEST['bid'] );
	$promo_id = intval( $_REQUEST['promo_id'] );
	$dir = intval( $_REQUEST['dir'] );
	
	$compArray = array(
		1 => '<', 2 => '>'
	);
	$orderArray = array(
		1 => 'DESC', 2 => 'ASC'
	);
	
	$comp = $compArray[$dir];
	$order = $orderArray[$dir];
	
	$query = "
		SELECT 
			p.id, 
			p.rule_order AS rule_order,
			q.rule_order AS this_order
		FROM promotions p 
		JOIN promotions q
			ON q.id=$promo_id
			AND q.businessid=p.businessid
		WHERE p.rule_order $comp q.rule_order
		AND p.businessid=$businessid
		ORDER BY rule_order $order
		LIMIT 1
	";
	$result = Treat_DB_ProxyOld::query( $query );
	
	//$swapWith = mysql_fetch_assoc( $result );
	$swapWith = Treat_DB_ProxyOld::mysql_fetch_array( $result,MYSQL_ASSOC );
	
	if( $swapWith && $swapWith['id'] > 0 ) {
		$query = "
			UPDATE promotions SET
				rule_order = %s
			WHERE id = %s
		";
		
		$ruleOrder = $swapWith['this_order'];
		
		$query1 = str_replace( array( "\t", "\n", "\r" ), ' ',
				sprintf( $query, $ruleOrder, $swapWith['id'] ) );
		$query2 = str_replace( array( "\t", "\n", "\r" ), ' ',
				sprintf( $query, $swapWith['rule_order'], $promo_id ) );
		Treat_DB_ProxyOld::query( $query1 );
		Treat_DB_ProxyOld::query( $query2 );
		
		Treat_Model_KioskSync::addSync( $businessid, $promo_id, Treat_Model_KioskSync::TYPE_PROMO );
		Treat_Model_KioskSync::addSync( $businessid, $swapWith['id'],
				Treat_Model_KioskSync::TYPE_PROMO );
	}
}
?>
