<head>

<SCRIPT LANGUAGE="JavaScript" SRC="CalendarPopup.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">document.write(getCalendarStyles());</SCRIPT>

<STYLE>
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation
			{
			background-color:#6677DD;
			text-align:center;
			vertical-align:center;
			text-decoration:none;
			color:#FFFFFF;
			font-weight:bold;
			}
	.TESTcpDayColumnHeader,
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation,
	.TESTcpCurrentMonthDate,
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDate,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDate,
	.TESTcpCurrentDateDisabled,
	.TESTcpTodayText,
	.TESTcpTodayTextDisabled,
	.TESTcpText
			{
			font-family:arial;
			font-size:8pt;
			}
	TD.TESTcpDayColumnHeader
			{
			text-align:right;
			border:solid thin #6677DD;
			border-width:0 0 1 0;
			}
	.TESTcpCurrentMonthDate,
	.TESTcpOtherMonthDate,
	.TESTcpCurrentDate
			{
			text-align:right;
			text-decoration:none;
			}
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDateDisabled
			{
			color:#D0D0D0;
			text-align:right;
			text-decoration:line-through;
			}
	.TESTcpCurrentMonthDate
			{
			color:#6677DD;
			font-weight:bold;
			}
	.TESTcpCurrentDate
			{
			color: #FFFFFF;
			font-weight:bold;
			}
	.TESTcpOtherMonthDate
			{
			color:#808080;
			}
	TD.TESTcpCurrentDate
			{
			color:#FFFFFF;
			background-color: #6677DD;
			border-width:1;
			border:solid thin #000000;
			}
	TD.TESTcpCurrentDateDisabled
			{
			border-width:1;
			border:solid thin #FFAAAA;
			}
	TD.TESTcpTodayText,
	TD.TESTcpTodayTextDisabled
			{
			border:solid thin #6677DD;
			border-width:1 0 0 0;
			}
	A.TESTcpTodayText,
	SPAN.TESTcpTodayTextDisabled
			{
			height:20px;
			}
	A.TESTcpTodayText
			{
			color:#6677DD;
			font-weight:bold;
			}
	SPAN.TESTcpTodayTextDisabled
			{
			color:#D0D0D0;
			}
	.TESTcpBorder
			{
			border:solid thin #6677DD;
			}
</STYLE>

<SCRIPT LANGUAGE=javascript><!--
function delconfirm(){return confirm('Are you sure you want to delete this?');}
// --></SCRIPT>

</head>

<?php

function money($diff){
				$findme='.';
				$double='.00';
				$single='0';
				$double2='00';
				$pos1 = strpos($diff, $findme);
				$pos2 = strlen($diff);
				if ($pos1==""){$diff="$diff$double";}
				elseif ($pos2-$pos1==2){$diff="$diff$single";}
				elseif ($pos2-$pos1==1){$diff="$diff$double2";}
				else{}
				$dot = strpos($diff, $findme);
				$diff = substr($diff, 0, $dot+4);
				$diff=round($diff, 2);
				if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
				elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
				$diff = substr($diff, 0, $dot+3);
				$pos1 = strpos($diff, $findme);
				$pos2 = strlen($diff);
				if ($pos1==""){$diff="$diff$double";}
				elseif ($pos2-$pos1==2){$diff="$diff$single";}
				elseif ($pos2-$pos1==1){$diff="$diff$double2";}
				else{}
				return $diff;
}

function nextday($date2)
{
	$day=substr($date2,8,2);
	$month=substr($date2,5,2);
	$year=substr($date2,0,4);
	$leap = date("L");

	if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
	{
			if ($month == "01"){$month="02";}
			elseif ($month == "03"){$month="04";}
			elseif ($month == "05"){$month="06";}
			elseif ($month == "07"){$month="08";}
			elseif ($month == "08"){$month="09";}
			elseif ($month == "10"){$month="11";}
			$day='01';
			$tomorrow = "$year-$month-$day";
			return $tomorrow;
	}
	else if ($month=='02' && $day == '29' && $leap == '0')
	{
			$month='03';
			$day='01';
			$tomorrow = "$year-$month-$day";
			return $tomorrow;
	}
	else if ($month=='02' && $day == '28')
	{
			$month='03';
			$day='01';
			$tomorrow = "$year-$month-$day";
			return $tomorrow;
	}
	else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
	{
			if ($month == "04"){$month="05";}
			if ($month == "06"){$month="07";}
			if ($month == "09"){$month="10";}
			if ($month == "11"){$month="12";}
			$day='01';
			$tomorrow = "$year-$month-$day";
			return $tomorrow;
	}
	else if ($month=='12' && $day=='31')
	{
			$day='01';
			$month='01';
			$year++;
			$tomorrow = "$year-$month-$day";
			return $tomorrow;
	}
	else
	{
			$day=$day+1;
			if ($day<10){$day="0$day";}
			$tomorrow="$year-$month-$day";
			return $tomorrow;
	}
}

define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once(DOC_ROOT.'/bootstrap.php');

$style = "text-decoration:none";

$user=$_COOKIE["usercook"];
$pass=$_COOKIE["passcook"];
$businessid=$_POST["businessid"];
$companyid=$_POST["companyid"];
$employeeid=$_POST["employeeid"];
$new_empl=$_POST["new_empl"];
$addme=$_POST["addme"];
$pendid=$_POST["pendid"];
$date1=$_POST["date1"];
$date2=$_POST["date2"];
$find_empl_no=$_POST["find_empl_no"];
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";

if ($companyid==""){$companyid=$_GET["cid"];}
if ($employeeid==""){$employeeid=$_GET["eid"];}
$bid=$_GET["bid"];
$success=$_GET["success"];
$pendrate=$_GET["pendrate"];
$pendjob=$_GET["pendjob"];

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");

if($find_empl_no!=""){
	$query = "SELECT * FROM login WHERE companyid = '$companyid' AND empl_no = '$find_empl_no'";
	$result = Treat_DB_ProxyOld::query($query);
	$num=mysql_numrows($result);

	if($num==1){
			$employeeid = mysql_result($result,0,"loginid");
	}
}

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=mysql_numrows($result);
//mysql_close();

if ($num != 1 || $employeeid == "-1" || ($new_empl!="NEW" && $employeeid == ""))
{
		echo "<center><h3>Employee Not Found</h3>Use your browser's back button to try again.</center>";
}

else
{
		$security_level=mysql_result($result,0,"security_level");
		$mysecurity=mysql_result($result,0,"security");
		$myid=mysql_result($result,0,"loginid");

		echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%><tr><td colspan=2>";
		echo "<img src=logo.jpg><p></td></tr>";

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query = "SELECT * FROM company WHERE companyid = '$companyid'";
		$result = Treat_DB_ProxyOld::query($query);
		//mysql_close();

		$companyname=mysql_result($result,0,"companyname");

//////////////////////////////////////////////BEGIN EDIT ACCOUNT////////////////////////////////////////////////////////////
		if($new_empl=="NEW" && $addme == 1){$new_empl="NEW2";}
		elseif ($new_empl=="NEW"){
			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query = "SELECT * FROM login WHERE login_pendid = '$pendid'";
			$result = Treat_DB_ProxyOld::query($query);
			//mysql_close();

			$lastname=mysql_result($result,0,"lastname");
			$firstname=mysql_result($result,0,"firstname");
			$oo2=mysql_result($result,0,"oo2");
			$employeeid=mysql_result($result,0,"loginid");

			$deletepend="<form action=save_emp_labor.php method=post><input type=hidden name=deleteme value=deleteme><input type=hidden name=pendid value=$pendid><input type=hidden name=companyid value=$companyid><input type=submit value='DELETE'></form>";
		}
		else{
		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query = "SELECT * FROM login WHERE loginid = '$employeeid'";
		$result = Treat_DB_ProxyOld::query($query);
		//mysql_close();

		$lastname=mysql_result($result,0,"lastname");
		$firstname=mysql_result($result,0,"firstname");
		$sec_level=mysql_result($result,0,"security_level");
		$security=mysql_result($result,0,"security");
		$email=mysql_result($result,0,"email");
		$busid=mysql_result($result,0,"businessid");
		$payroll=mysql_result($result,0,"payroll");
		$busid2=mysql_result($result,0,"busid2");
		$busid3=mysql_result($result,0,"busid3");
		$busid4=mysql_result($result,0,"busid4");
		$busid5=mysql_result($result,0,"busid5");
		$busid6=mysql_result($result,0,"busid6");
		$busid7=mysql_result($result,0,"busid7");
		$busid8=mysql_result($result,0,"busid8");
		$busid9=mysql_result($result,0,"busid9");
		$busid10=mysql_result($result,0,"busid10");
		$empl_user=mysql_result($result,0,"username");
		$empl_pass=mysql_result($result,0,"password");
		$empl_no=mysql_result($result,0,"empl_no");
		$pr2=mysql_result($result,0,"pr2");
		$pr3=mysql_result($result,0,"pr3");
		$pr4=mysql_result($result,0,"pr4");
		$pr5=mysql_result($result,0,"pr5");
		$pr6=mysql_result($result,0,"pr6");
		$pr7=mysql_result($result,0,"pr7");
		$pr8=mysql_result($result,0,"pr8");
		$pr9=mysql_result($result,0,"pr9");
		$pr10=mysql_result($result,0,"pr10");
		$temp_expire=mysql_result($result,0,"temp_expire");
		$order_only=mysql_result($result,0,"order_only");
		$oo2=mysql_result($result,0,"oo2");
		$oo3=mysql_result($result,0,"oo3");
		$oo4=mysql_result($result,0,"oo4");
		$oo5=mysql_result($result,0,"oo5");
		$oo6=mysql_result($result,0,"oo6");
		$oo7=mysql_result($result,0,"oo7");
		$oo8=mysql_result($result,0,"oo8");
		$oo9=mysql_result($result,0,"oo9");
		$oo10=mysql_result($result,0,"oo10");
		$is_deleted=mysql_result($result,0,"is_deleted");
		$del_date=mysql_result($result,0,"del_date");
		}

		$temp_date2=$today;
		$expiresin=0;
		if ($temp_expire!="0000-00-00"&&$new_empl!="NEW"){
			while($temp_date2<$temp_expire){$temp_date2=nextday($temp_date2);$expiresin++;}
		}
		else{$expiresin="";}

		if ($sec_level>0){$sec_disable="DISABLED";}

//////////////////////EMPLOYEE DETAILS
		if($is_deleted==1){$showdeletion="<b><font color=red>DELETED ON $del_date</font></b>";}

		echo "<FORM ACTION=save_emp_labor.php method=post style=\"margin:0;padding:0;display:inline;\">";
		echo "<input type=hidden name=username value=$user>";
		echo "<input type=hidden name=password value=$pass>";
		echo "<input type=hidden name=businessid value=$businessid>";
		echo "<input type=hidden name=companyid value=$companyid>";
		echo "<input type=hidden name=employeeid value=$employeeid>";
		echo "<input type=hidden name=new_empl value=$new_empl>";
		echo "<input type=hidden name=pendid value=$pendid>";
		echo "<input type=hidden name=oldoo2 value=$oo2>";
		echo "<input type=hidden name=sec_level value=$sec_level>";
		echo "<center><table cellspacing=0 cellpadding=0 border=0 width=75%>";
		//echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";
		echo "<tr bgcolor=#CCCCFF><td colspan=2><b>[$employeeid] Employee Details</b> $showdeletion</td></tr>";
		echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";
		echo "<tr valign=top bgcolor=#E8E7E7><td align=right><font color=red>Lastname:</font></td><td> <INPUT TYPE=text NAME=lastname SIZE=30 VALUE='$lastname'>";
		if ($employeeid>0){echo " <input type=checkbox name=is_deleted value=1 $sec_disable> Delete This Employee";}
		echo "</td></tr>";
		echo "<tr bgcolor=#E8E7E7><td align=right><font color=red>Firstname:</font></td><td> <INPUT TYPE=text NAME=firstname SIZE=30 VALUE='$firstname'></td></tr>";

		echo "<tr bgcolor=#E8E7E7><td align=right>Email:</td><td> <INPUT TYPE=text NAME=email SIZE=30 VALUE='$email' $sec_disable></td></tr>";

		echo "<tr bgcolor=#E8E7E7><td align=right><font color=red>Employee #:</font></td><td><INPUT TYPE=text NAME=empl_no SIZE=12 VALUE='$empl_no'></td></tr>";
///////////////////LOCATION/BUSINESS
		//echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";
		echo "<tr bgcolor=#CCCCFF><td colspan=2><b>Location Details</b></td></tr>";
		echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";

		echo "<tr bgcolor=#E8E7E7><td align=right><font color=red>Business ID:</font></td><td><select name=busid>";

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query = "SELECT * FROM business WHERE companyid = '$companyid' ORDER BY unit DESC";
		$result = Treat_DB_ProxyOld::query($query);
		$num=mysql_numrows($result);
		//mysql_close();
		$startnum=$num;

		while ($num>=0){
			$bus_id=mysql_result($result,$num,"businessid");
			$bus_name=mysql_result($result,$num,"businessname");
			$bus_unit=mysql_result($result,$num,"unit");
			if ($bus_id==$oo2){$check="SELECTED";}
			else{$check="";}
			echo "<option value='$bus_id' $check>$bus_unit - $bus_name</option>";
			$num--;
		}

		echo "</select> ";

		echo "</td></tr>";
	/////pending transfer
	$query = "SELECT to_business FROM labor_move WHERE loginid = '$employeeid' AND move_date = '0000-00-00 00:00:00'";
		$result = Treat_DB_ProxyOld::query($query);
	$num=mysql_numrows($result);

	if($num>0){
		while($r=mysql_fetch_array($result)){
			$to_business=$r["to_business"];

			$query2 = "SELECT businessname FROM business WHERE businessid = '$to_business'";
			$result2 = Treat_DB_ProxyOld::query($query2);

			$to_bus=mysql_result($result2,0,"businessname");

			echo "<tr bgcolor=#E8E7E7><td colspan=2><font style=\"background-color: #FFFF99;\">&nbsp;This employee is scheduled to move to $to_bus at week end.&nbsp;</font></td></tr>";
		}
	}

////////////////////////JOB TYPES
if($employeeid!=""){
		//echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";
		echo "<tr bgcolor=#CCCCFF><td colspan=2><b>Job Details</b></td></tr>";
		echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query3 = "SELECT * FROM jobtypedetail WHERE loginid = '$employeeid' AND is_deleted = '0'";
		$result3 = Treat_DB_ProxyOld::query($query3);
		$num3=mysql_numrows($result3);
		//mysql_close();

		if ($num3==0){echo "<tr bgcolor=#E8E7E7><td align=right><img src=error.gif height=16 width=20>&nbsp;</td><td><i>No Defined Jobs</i></td></tr>";}

		$num3--;
		while ($num3>=0){

			$jobtypeid=mysql_result($result3,$num3,"jobtype");
			$rate=mysql_result($result3,$num3,"rate");
			$rateCode = mysql_result( $result3, $num3, 'rateCode' );
			$jobid=mysql_result($result3,$num3,"jobid");

			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query4 = "SELECT * FROM jobtype WHERE jobtypeid = '$jobtypeid'";
			$result4 = Treat_DB_ProxyOld::query($query4);
			//mysql_close();

			$jobtypename=mysql_result($result4,0,"name");
			$hourly=mysql_result($result4,0,"hourly");

			if($hourly==1){$showhour="- Per Hour";}
			elseif($hourly==0){$showhour=" - Salary";}

			$rate=money($rate);

			echo "<tr bgcolor=#E8E7E7><td align=right>$jobtypename $<input type=text size=6 name=rate$num3 value='$rate'></td><td>$showhour ";
			echo "&mdash; Paycor Rate Code: <input type='text' size='1' name='rateCode$num3' value='$rateCode' /> ";
			echo "<a href=addjobtype.php?eid=$employeeid&jobid=$jobid&cid=$companyid&bid=$bid onclick='return delconfirm()'><img src=delete.gif height=16 width=16 border=0></a></td></tr>";

			$num3--;
		}

		////deleted jobs
		$query3 = "SELECT * FROM jobtypedetail WHERE loginid = '$employeeid' AND is_deleted = '1'";
		$result3 = Treat_DB_ProxyOld::query($query3);

		while($r3 = mysql_fetch_array($result3)){
			$jobtypeid=$r3["jobtype"];
			$rate=$r3["rate"];
			$rateCode = $r3["rateCode"];
			$jobid=$r3["jobid"];

			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query4 = "SELECT * FROM jobtype WHERE jobtypeid = '$jobtypeid'";
			$result4 = Treat_DB_ProxyOld::query($query4);
			//mysql_close();

			$jobtypename=mysql_result($result4,0,"name");
			$hourly=mysql_result($result4,0,"hourly");

			if($hourly==1){$showhour="- Per Hour";}
			elseif($hourly==0){$showhour=" - Salary";}

			$rate=money($rate);

			echo "<tr bgcolor=#E8E7E7 style=\"color:#999999;\"><td align=right>Deleted: $jobtypename $$rate</td><td>$showhour ";
			echo "&mdash; Paycor Rate Code: $rateCode ";
			echo "</td></tr>";
		}

		echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";
}
		echo "<tr bgcolor=#E8E7E7><td></td><td align=right><input type=hidden name=bid value=$bid><INPUT TYPE=submit VALUE='  Save  '></form>$deletepend</td></tr>";
		echo "<tr bgcolor=black><td colspan=2 height=1></FORM></td></tr>";

////////////////////////ADD JOB TYPE
if($employeeid!=""){
		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query3 = "SELECT * FROM jobtype WHERE jobtypeid = '$pendjob'";
		$result3 = Treat_DB_ProxyOld::query($query3);
		//mysql_close();

		$pendjobtypename=mysql_result($result3,0,"name");
		$pendrate=money($pendrate);

		echo "<tr bgcolor=white height=16><td colspan=2></td></tr>";
		if ($pendjob>0){echo "<tr bgcolor=white><td colspan=2><b><i><font color=blue>Requested: $pendjobtypename @ $$pendrate</td></tr>";}
		echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";
		echo "<tr bgcolor=#E8E7E7><td colspan=2><form action=addjobtype.php method=post><input type=hidden name=bid value=$bid><input type=hidden name=companyid value=$companyid><input type=hidden name=employeeid value=$employeeid><select name=jobtypeid>";

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query3 = "SELECT * FROM jobtype WHERE companyid = '$companyid' ORDER BY name DESC";
		$result3 = Treat_DB_ProxyOld::query($query3);
		$num3=mysql_numrows($result3);
		//mysql_close();

		$num3--;
		while($num3>=0){
			$jobtypename=mysql_result($result3,$num3,"name");
			$jobtypeid=mysql_result($result3,$num3,"jobtypeid");

			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query4 = "SELECT * FROM jobtypedetail WHERE loginid = '$employeeid' AND jobtype = '$jobtypeid' AND is_deleted = '0'";
			$result4 = Treat_DB_ProxyOld::query($query4);
			$num4=mysql_numrows($result4);
			//mysql_close();

			if($num4==0){echo "<option value=$jobtypeid>$jobtypename</option>";}
			$num3--;
		}
		echo "</select> Rate: $<input type=text size=5 name=rate> <input type=submit value=' Add '></td></tr>";

		echo "<tr bgcolor=black><td colspan=2 height=1></form></td></tr>";

////////////////EMPLOYEE CARDS
		if($success==1){$showsuccess="<font color=green>Updated Successfully</font>";}

		echo "<tr bgcolor=white><td colspan=2 height=16></td></tr>";
		echo "<tr bgcolor=#CCCCFF><td colspan=2><b>Employee Card</b></td></tr>";
		echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";

		$query = "SELECT * FROM login_cards WHERE loginid = '$employeeid'";
		$result = Treat_DB_ProxyOld::query($query);
		$num=mysql_numrows($result);

		$card_num=mysql_result($result,0,"card_num");

		echo "<tr bgcolor=#E8E7E7><td colspan=2><form action=emp_card.php method=post style=\"margin:0;padding:0;display:inline;\"><input type=hidden name=loginid value=$employeeid><input type=hidden name=companyid value=$companyid>Card#: <input type=text name=card_num size=10 value='$card_num'> <input type=submit value='Update'></form> <form action=emp_card.php method=post style=\"margin:0;padding:0;display:inline;\"><input type=hidden name=loginid value=$employeeid><input type=hidden name=companyid value=$companyid><input type=hidden name=card_num value='-1'> <input type=submit value='Deactivate'></form>  $showsuccess</td></tr>";
		echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";
////////////////END CARDS

////////////////DEPARTMENTS

		echo "<tr bgcolor=white><td colspan=2 height=16></td></tr>";
		echo "<tr bgcolor=#CCCCFF><td colspan=2><b>Departments</b></td></tr>";
		echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";

		$query = "SELECT * FROM payroll_departmentdetail WHERE loginid = '$employeeid'";
		$result = Treat_DB_ProxyOld::query($query);
		$num=mysql_numrows($result);

		if($num==0){echo "<tr bgcolor=#E8E7E7><td colspan=2><img src=error.gif height=16 width=20> <i>No Defined Departments</i></td></tr>";}
		$num--;
		while($num>=0){
			$dept_id=mysql_result($result,$num,"dept_id");
			$detailid=mysql_result($result,$num,"dept_detail_id");

			$query2 = "SELECT * FROM payroll_department WHERE dept_id = '$dept_id'";
			$result2 = Treat_DB_ProxyOld::query($query2);

			$dept_name=mysql_result($result2,0,"dept_name");
			$dept_code=mysql_result($result2,0,"dept_code");

			echo "<tr bgcolor=#E8E7E7><td colspan=2>$dept_name ($dept_code) <a href=edit_dept.php?eid=$employeeid&detailid=$detailid&cid=$companyid onclick='return delconfirm()'><img src=delete.gif height=16 width=16 border=0></a></td></tr>";

			$num--;
		}

		echo "<tr bgcolor=#E8E7E7><td colspan=2><form action=edit_dept.php method=post style=\"margin:0;padding:0;display:inline;\"><input type=hidden name=employeeid value=$employeeid><input type=hidden name=companyid value=$companyid><select name=dept_id>";

		$query3 = "SELECT * FROM payroll_department ORDER BY dept_name DESC";
		$result3 = Treat_DB_ProxyOld::query($query3);
		$num3=mysql_numrows($result3);

		$num3--;
		while($num3>=0){
			$dept_id=mysql_result($result3,$num3,"dept_id");
			$dept_name=mysql_result($result3,$num3,"dept_name");

			$query4 = "SELECT * FROM payroll_departmentdetail WHERE loginid = '$employeeid' AND dept_id = '$dept_id'";
			$result4 = Treat_DB_ProxyOld::query($query4);
			$num4=mysql_numrows($result4);

			if($num4==0){echo "<option value=$dept_id>$dept_name</option>";}
			$num3--;
		}

		echo "</select> <input type=submit value='Add'></form></td></tr>";
		echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";
////////////////END DEPARTMENTS

		////HISTORY
		if ($date1==""){$date1=$today;}
		if ($date2==""){$date2=$today;}
		echo "<tr bgcolor=white><td colspan=2 height=16></td></tr>";
		//echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";
		echo "<tr bgcolor=#CCCCFF><td colspan=2><a name=hist></a><form action=edit_emp_labor.php#hist method=post><b>History</b><input type=hidden name=businessid value=$businessid><input type=hidden name=companyid value=$companyid><input type=hidden name=employeeid value=$employeeid><SCRIPT LANGUAGE='JavaScript' ID='js18'> var cal18 = new CalendarPopup('testdiv1'); cal18.setCssPrefix('TEST');</SCRIPT> <A HREF='javascript:void();' onClick=cal18.select(document.forms[5].date1,'anchor18','yyyy-MM-dd'); return false; TITLE=cal18.select(document.forms[5].date1,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor18' ID='anchor18'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A> <input type=text name=date1 value='$date1' size=8> <b><i>to</i></b> <SCRIPT LANGUAGE='JavaScript' ID='js19'> var cal19 = new CalendarPopup('testdiv1'); cal19.setCssPrefix('TEST');</SCRIPT> <A HREF='javascript:void();' onClick=cal19.select(document.forms[5].date2,'anchor19','yyyy-MM-dd'); return false; TITLE=cal19.select(document.forms[5].date2,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor19' ID='anchor19'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A> <input type=text name=date2 value='$date2' size=8> <input type=submit value='GO'></td></tr>";
		echo "<tr bgcolor=black><td colspan=2 height=1></form></td></tr>";
		echo "<tr bgcolor=#E8E7E7><td colspan=2><table width=100%>";

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query3 = "SELECT * FROM labor WHERE loginid = '$employeeid' AND date >= '$date1' AND date <= '$date2' ORDER BY date DESC";
		$result3 = Treat_DB_ProxyOld::query($query3);
		$num3=mysql_numrows($result3);
		//mysql_close();

		$numresults=$num3;

		echo "<tr><td width=15%><font size=2><b>Date</td><td width=15%><font size=2><b>Jobtype</td><td align=right width=10%><font size=2><b>Hours</td><td align=right width=10%><font size=2><b>Coded</td><td align=right width=10%><font size=2><b>Rate</td><td align=right width=10%><font size=2><b>Tips</td><td align=right width=30%><font size=2><b>Unit</td></tr>";

		$num3--;
		while($num3>=0){
			$date=mysql_result($result3,$num3,"date");
			$busid=mysql_result($result3,$num3,"businessid");
			$rate=mysql_result($result3,$num3,"rate");
			$coded=mysql_result($result3,$num3,"coded");
			$jobtypeid=mysql_result($result3,$num3,"jobtypeid");
			$tips=mysql_result($result3,$num3,"tips");
			$hours=mysql_result($result3,$num3,"hours");

			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query4 = "SELECT businessname FROM business WHERE businessid = '$busid'";
			$result4 = Treat_DB_ProxyOld::query($query4);
			//mysql_close();

			$businessname=mysql_result($result4,0,"businessname");

			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query4 = "SELECT name FROM jobtype WHERE jobtypeid = '$jobtypeid'";
			$result4 = Treat_DB_ProxyOld::query($query4);
			//mysql_close();

			$jobname=mysql_result($result4,0,"name");

			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query4 = "SELECT code FROM payroll_code WHERE codeid = '$coded'";
			$result4 = Treat_DB_ProxyOld::query($query4);
			//mysql_close();

			$showcode=mysql_result($result4,0,"code");

			echo "<tr><td><font size=2>$date</td><td><font size=2>$jobname</td><td align=right><font size=2>$hours</td><td align=right><font size=2>$showcode</td><td align=right><font size=2>$$rate</td><td align=right><font size=2>$$tips</td><td align=right><font size=2>$businessname</td></tr>";

			$num3--;
		}

		echo "</table></td></tr>";
		echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";
		echo "<tr><td colspan=2><font size=2><b>Results: $numresults</td></tr>";
}
		echo "</table></center>";

//////////////////////////////////////////////END Edit User////////////////////////////////////////////////////////////

		if($bid>0){echo "<FORM ACTION=buslabor.php?cid=$companyid&bid=$bid method=post>";}
		else{echo "<FORM ACTION=emp_labor.php?cid=$companyid method=post>";}
		echo "<input type=hidden name=username value=$user>";
		echo "<input type=hidden name=password value=$pass>";
		echo "<center><INPUT TYPE=submit VALUE=' Return '></FORM></center><DIV ID=testdiv1 STYLE=position:absolute;visibility:hidden;background-color:white;layer-background-color:white;></DIV>";
}
//mysql_close();
?>
