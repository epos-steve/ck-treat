<?php

function money($diff){
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+4);
        if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
        elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
        $diff = substr($diff, 0, $dot+3);
        return $diff;
}

function nextday($nextd,$day_format){ //Function returns next day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monn = substr($nextd, 5, 2);
   $dayn = substr($nextd, 8, 2);
   $yearn = substr($nextd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monn, $dayn+1, $yearn));
   return $tempdate;
}

function prevday($prevd,$day_format){ //Function returns previous day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monp = substr($prevd, 5, 2);
   $dayp = substr($prevd, 8, 2);
   $yearp = substr($prevd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monp, $dayp-1, $yearp));
   return $tempdate;
}

function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}

define('DOC_ROOT', dirname(dirname(__FILE__)));
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');
require_once('lib/class.dashboard.php');
$style = "text-decoration:none";
$today=date("Y-m-d");

$user=$_COOKIE["usercook"];
$pass=$_COOKIE["passcook"];
$viewtype=$_GET["viewtype"];
$viewbus=$_GET["viewbus"];
$thismonth=$_GET["thismonth"];
$thisyear=$_GET["thisyear"];

if($thismonth==""){
	$thismonth=date("m");
}
if($thisyear==""){
	$thisyear=date("Y");
}

$query = "SELECT loginid FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query( $query );
$num=mysql_numrows($result);

if ($num != 1 || $user == "" || $pass == "") {
    echo "<center><h3>Failed</h3></center>";
}
else{
	?>
	<html><head>

	<script type="text/javascript" src="javascripts/swfobject.js"></script>

	<head><body style="margin: 0px;">
	<?php

	////next month
	$nextmonth=$thismonth+1;
	$nextyear=$thisyear;
	if($nextmonth==13){
		$nextmonth="01";
		$nextyear=$thisyear+1;
	}
	if(strlen($nextmonth)<2){$nextmonth="0$nextmonth";}

	/////previous month
	$prevmonth=$thismonth-1;
	$prevyear=$thisyear;
	if($prevmonth==0){
		$prevmonth=12;
		$prevyear=$thisyear-1;
	}
	if(strlen($prevmonth)<2){$prevmonth="0$prevmonth";}

	////create date array
	$dates=array();
	$dates[0]="$thisyear-$thismonth-01";
	$tempdate=nextday($dates[0]);
	$counter=1;
	while(substr(nextday($tempdate),5,2) == substr($tempdate,5,2)){
		if(dayofweek($tempdate)=="Thursday"){$dates[$counter]=$tempdate;$counter++;}
		$tempdate=nextday($tempdate);
	}
	$dates[$counter]=$tempdate;

	////get values into array
	$db_data = array();

	foreach($dates AS $key => $value){
		if($key!=0){
			if($key==1){$startdate = $dates[$key-1];}
			else{$startdate = nextday($dates[$key-1]);}
			$amount = DashBoard::get_data($startdate,$value,'cos,cos_bgt,labor,labor_bgt,toe,toe_bgt,sales,sales_bgt',$viewtype,$viewbus);
			$amount = explode(",",$amount);

			foreach($amount AS $key2 => $value2){
				$db_data[$key2][$key] = $value2;
			}
		}
	}

	////titles
	if($thismonth==1){$monthname="January";}
	elseif($thismonth==2){$monthname="February";}
	elseif($thismonth==3){$monthname="March";}
	elseif($thismonth==4){$monthname="April";}
	elseif($thismonth==5){$monthname="May";}
	elseif($thismonth==6){$monthname="June";}
	elseif($thismonth==7){$monthname="July";}
	elseif($thismonth==8){$monthname="August";}
	elseif($thismonth==9){$monthname="September";}
	elseif($thismonth==10){$monthname="October";}
	elseif($thismonth==11){$monthname="November";}
	elseif($thismonth==12){$monthname="December";}

	////get rid of values in the future and add MTD figures to end of array
	$tempdate=$today;
	while(dayofweek($tempdate)!="Saturday"){$tempdate=prevday($tempdate);}
	while(dayofweek($tempdate)!="Thursday"){$tempdate=prevday($tempdate);}
	$today=$tempdate;

	foreach($db_data AS $key => $value){
		foreach($db_data[$key] AS $key2 => $value2){
			if($dates[$key2]>$today){
				$db_data[$key][$key2] = 0;
			}
		}
		$db_data[$key][$key2+1]=array_sum($db_data[$key]);
	}

	////create labels
	if(count($db_data[0])==5){
		$labels = array('<font size=2>Wk1</font>', '<font size=2>Wk2</font>', '<font size=2>Wk3</font>', '<font size=2>Wk4</font>', '<font size=2>MTD</font>');
	}
	elseif(count($db_data[0])==6){
		$labels = array('<font size=2>Wk1</font>', '<font size=2>Wk2</font>', '<font size=2>Wk3</font>', '<font size=2>Wk4</font>', '<font size=2>Wk5</font>', '<font size=2>MTD</font>');
	}
	elseif(count($db_data[0])==7){
		$labels = array('<font size=2>Wk1</font>', '<font size=2>Wk2</font>', '<font size=2>Wk3</font>', '<font size=2>Wk4</font>', '<font size=2>Wk5</font>', '<font size=2>Wk6</font>', '<font size=2>MTD</font>');
	}

	///display graphs
	echo "<table width=100% cellspacing=0 cellpadding=1><tr>";

	////sales vs budget
	echo "<td colspan=2>";

	echo "<table width=100% cellspacing=0 cellpadding=1 style=\"border: 2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td>&nbsp;<a href=dashboard_expenses.php?viewtype=$viewtype&viewbus=$viewbus&thisyear=$prevyear&thismonth=$prevmonth style=$style><font size=1 color=blue>[PREV]</font></a> Sales $monthname $thisyear <a href=dashboard_expenses.php?viewtype=$viewtype&viewbus=$viewbus&thisyear=$nextyear&thismonth=$nextmonth style=$style><font size=1 color=blue>[NEXT]</font></a></td></tr><tr><td><center>";
	echo "<div id='sales_chart' style=\"width: 100%;\"></div>";

		?>
			<script type="text/javascript">
			swfobject.embedSWF(
			"flash/open-flash-chart2DZ.swf", "sales_chart", "1000", "250",
			"9.0.0", "expressInstall.swf",
			{"data-file":"<?php echo "dashboard_expenses_graph.php?viewtype=$viewtype-$viewbus-$thisyear-$thismonth" ?>"}, {"wmode": "transparent"} );
			</script>
		<?

	echo "</center></td></tr></table>";

	echo "</td></tr><tr><td colspan=2>&nbsp;</td></tr><tr><td width=50%>";

	///cos
	echo "<table width=100% cellspacing=0 cellpadding=1 style=\"border: 2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td>&nbsp;<a href=dashboard_expenses.php?viewtype=$viewtype&viewbus=$viewbus&thisyear=$prevyear&thismonth=$prevmonth style=$style><font size=1 color=blue>[PREV]</font></a> Cost of Sales $monthname $thisyear <a href=dashboard_expenses.php?viewtype=$viewtype&viewbus=$viewbus&thisyear=$nextyear&thismonth=$nextmonth style=$style><font size=1 color=blue>[NEXT]</font></a></td></tr><tr><td><center>";
	$cos_graph = DashBoard::bar_graph($db_data[0], $db_data[1], $labels, 250, 560, '#0099FF', '#99FF66', '#000066', '#006600', 1, $db_data[6], $db_data[7]);
	echo $cos_graph;
	echo "</center></td></tr></table>";

	echo "</td><td width=50%>";

	///labor
	echo "<table width=100% cellspacing=0 cellpadding=1 style=\"border: 2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td>&nbsp;<a href=dashboard_expenses.php?viewtype=$viewtype&viewbus=$viewbus&thisyear=$prevyear&thismonth=$prevmonth style=$style><font size=1 color=blue>[PREV]</font></a> Labor $monthname $thisyear <a href=dashboard_expenses.php?viewtype=$viewtype&viewbus=$viewbus&thisyear=$nextyear&thismonth=$nextmonth style=$style><font size=1 color=blue>[NEXT]</font></a></td></tr><tr><td><center>";
	$cos_graph = DashBoard::bar_graph($db_data[2], $db_data[3], $labels, 250, 560, '#0099FF', '#99FF66', '#000066', '#006600', 1, $db_data[6], $db_data[7]);
	echo $cos_graph;
	echo "</center></td></tr></table>";

	echo "</td></tr><tr><td colspan=2>&nbsp;</td></tr><tr><td width=50%>";

	///toe
	echo "<table width=100% cellspacing=0 cellpadding=1 style=\"border: 2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td>&nbsp;<a href=dashboard_expenses.php?viewtype=$viewtype&viewbus=$viewbus&thisyear=$prevyear&thismonth=$prevmonth style=$style><font size=1 color=blue>[PREV]</font></a> TOE $monthname $thisyear <a href=dashboard_expenses.php?viewtype=$viewtype&viewbus=$viewbus&thisyear=$nextyear&thismonth=$nextmonth style=$style><font size=1 color=blue>[NEXT]</font></a></td></tr><tr><td><center>";
	$cos_graph = DashBoard::bar_graph($db_data[4], $db_data[5], $labels, 250, 560, '#0099FF', '#99FF66', '#000066', '#006600', 1, $db_data[6], $db_data[7]);
	echo $cos_graph;
	echo "</center></td></tr></table>";

	echo "</td><td>";

	///total expenses
		////create sums of arrays
		$expense_sum = array();
		$budget_sum = array();
		foreach($db_data[0] AS $key => $value){
			$expense_sum[$key] = $db_data[0][$key] + $db_data[2][$key] + $db_data[4][$key];
			$budget_sum[$key] = $db_data[1][$key] + $db_data[3][$key] + $db_data[5][$key];
		}

	echo "<table width=100% cellspacing=0 cellpadding=1 style=\"border: 2px solid #CCCCCC;\"><tr bgcolor=#CCCCCC><td>&nbsp;<a href=dashboard_expenses.php?viewtype=$viewtype&viewbus=$viewbus&thisyear=$prevyear&thismonth=$prevmonth style=$style><font size=1 color=blue>[PREV]</font></a> <b>Total Expenses $monthname $thisyear</b> <a href=dashboard_expenses.php?viewtype=$viewtype&viewbus=$viewbus&thisyear=$nextyear&thismonth=$nextmonth style=$style><font size=1 color=blue>[NEXT]</font></a></td></tr><tr><td><center>";
	$cos_graph = DashBoard::bar_graph($expense_sum, $budget_sum, $labels, 250, 560, '#6666CC', '#66CC66', '#000066', '#006600', 1, $db_data[6], $db_data[7]);
	echo $cos_graph;
	echo "</center></td></tr></table>";

	echo "</td></tr>";

	echo "</table>";
}
?>
</body></html>