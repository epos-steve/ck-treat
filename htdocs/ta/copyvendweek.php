<?
function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      elseif ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      elseif ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '1')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='31')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;
$leap = date("L");

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   
   elseif ($leap==1&&$month=='03')
   {
      $day=$day+29;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+3);
        if ($diff > 0 && $diff < '0.01'){$diff="0.01";}
        elseif($diff < 0 && $diff > '-0.01'){$diff="-0.01";}
        return $diff;
}

//include("db.php");
define('DOC_ROOT', dirname(dirname(__FILE__)));
require_once(DOC_ROOT.'/bootstrap.php');

/*$user=$_COOKIE["usercook"];
$pass=$_COOKIE["passcook"];
$curmenu=$_COOKIE["curmenu"];
$curcust=$_COOKIE["curcust"];
$businessid=$_POST['businessid'];
$companyid=$_POST['companyid'];
$date=$_POST['date'];
$week1=$_POST['week1'];
$week2=$_POST['week2'];
$curgroupid=$_POST['curgroupid'];*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook');
$curmenu = \EE\Controller\Base::getSessionCookieVariable('curmenu');
$curcust = \EE\Controller\Base::getSessionCookieVariable('curcust');
$businessid = \EE\Controller\Base::getPostVariable('businessid');
$companyid = \EE\Controller\Base::getPostVariable('companyid');
$date = \EE\Controller\Base::getPostVariable('date');
$week1 = \EE\Controller\Base::getPostVariable('week1');
$week2 = \EE\Controller\Base::getPostVariable('week2');
$curgroupid = \EE\Controller\Base::getPostVariable('curgroupid');

$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";

/*mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");*/
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=Treat_DB_ProxyOld::mysql_numrows($result);
//mysql_close();

if ($num!=1||$week1==$week2) 
{
    echo "<center><h4>Failed,$week1,$week2</h4></center>";
}
else
{
   $location="orderweek2.php?date=$date&newmonth=$newmonth&newyear=$newyear&newstart=$newstart&weekend=$weekend&bid=$businessid&cid=$companyid#copy";
   header('Location: ./' . $location);

   //mysql_connect($dbhost,$username,$password);
   //@mysql_select_db($database) or die( "Unable to select database");
   $query = "SELECT vend_start,vend_end,default_menu FROM business WHERE businessid = '$businessid'";
   $result = Treat_DB_ProxyOld::query($query);
   //mysql_close();

   $curmenu=Treat_DB_ProxyOld::mysql_result($result,0,"default_menu");
   $vend_start=Treat_DB_ProxyOld::mysql_result($result,0,"vend_start");
   $vend_end=Treat_DB_ProxyOld::mysql_result($result,0,"vend_end");

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM menu_type WHERE menu_typeid = '$curmenu'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $menuname=Treat_DB_ProxyOld::mysql_result($result,0,"menu_typename");
    $dayname=dayofweek($date);

/////////////////////////////////////////////ADD ITEMS

    $weekstart=array();
    $weekending=array();
    $temp_date=$vend_start;
    $counter=1;
    while($temp_date<=$vend_end){
       if(dayofweek($temp_date)=="Monday"){
          $weekstart[$counter]=$temp_date;
          $temp_date2=$temp_date;
          for($mycount=1;$mycount<=4;$mycount++){$temp_date2=nextday($temp_date2);}
          $weekending[$counter]=$temp_date2;
          $counter++;
       }
       $temp_date=nextday($temp_date);
    }
    $totalweeks=$counter-1;

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM menu_groups WHERE menu_group_id = '$curgroupid'";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    $varcount=1;

    $num--;
    while($num>=0){

       $groupname=Treat_DB_ProxyOld::mysql_result($result,$num,"groupname");
       $groupid=Treat_DB_ProxyOld::mysql_result($result,$num,"menu_group_id");

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM menu_pricegroup WHERE menu_groupid = '$groupid' ORDER BY orderid DESC";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
       //mysql_close();

       $num2--;
       while($num2>=0){

          $pricegroupname=Treat_DB_ProxyOld::mysql_result($result2,$num2,"menu_pricegroupname");
          $pricegroupid=Treat_DB_ProxyOld::mysql_result($result2,$num2,"menu_pricegroupid");

         $date1=$weekstart[$week1];
         $date2=$weekstart[$week2];
         for($copycount=1;$copycount<=5;$copycount++){

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query3 = "SELECT * FROM vend_item WHERE groupid = '$pricegroupid' AND date = '$date1' AND businessid = '$businessid' ORDER BY vend_itemid DESC";
          $result3 = Treat_DB_ProxyOld::query($query3);
          $num3=Treat_DB_ProxyOld::mysql_numrows($result3);
          //mysql_close();

          $num3--;
          $firstnum=$num3;

          while($num3>=0){

             $menu_itemid=Treat_DB_ProxyOld::mysql_result($result3,$num3,"menu_itemid");
             $vend_itemid=Treat_DB_ProxyOld::mysql_result($result3,$num3,"vend_itemid");
             $newgroupid=Treat_DB_ProxyOld::mysql_result($result3,$num3,"groupid");

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query99="INSERT INTO vend_item (menu_itemid,date,businessid,groupid) VALUES ('$menu_itemid','$date2','$businessid','$newgroupid')";
             $result99=Treat_DB_ProxyOld::query($query99);
             //mysql_close();
             
             $num3--;
          }

         $date1=nextday($date1);
         $date2=nextday($date2);
         }///////END FOR LOOP

          $num2--;
       }
       $num--;
    }
}
?>