<?php

if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
require_once( dirname(__FILE__) . '/../../application/bootstrap.php' );

$style = "text-decoration:none";

$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";

$user = \EE\Controller\Base::getPostVariable('username', '');
$pass = \EE\Controller\Base::getPostVariable('password', '');
$businessid = \EE\Controller\Base::getPostVariable('businessid', '');
$companyid = \EE\Controller\Base::getPostVariable('companyid', '');
$creditname = \EE\Controller\Base::getPostVariable('creditname', '');
$credittypeid = \EE\Controller\Base::getPostVariable('credittype', '');
$account = \EE\Controller\Base::getPostVariable('account', '');
$group1 = \EE\Controller\Base::getPostVariable('group1', '');
$creditid = \EE\Controller\Base::getPostVariable('creditid', '');
$edit = \EE\Controller\Base::getPostVariable('edit', '');
$salescode = \EE\Controller\Base::getPostVariable('salescode', '');
$tax_prcnt = \EE\Controller\Base::getPostVariable('tax_prcnt', '');
$parent = \EE\Controller\Base::getPostVariable('parent', '');
$category = \EE\Controller\Base::getPostVariable('category', '');

if ($user=="" && $pass==""){
	$user = \EE\Controller\Base::getSessionCookieVariable('usercook', '');
	$pass = \EE\Controller\Base::getSessionCookieVariable('passcook', '');
	$businessid = \EE\Controller\Base::getGetVariable('bid', '');
	$companyid = \EE\Controller\Base::getGetVariable('cid', '');
	$edit = \EE\Controller\Base::getGetVariable('edit', '');
	$editcredit = \EE\Controller\Base::getGetVariable('creditid', '');
}

if ($account=="Acct #"){
	$account="";
}

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num = Treat_DB_ProxyOld::mysql_numrows($result);

$location="editbus.php?bid=$businessid&cid=$companyid#credits";
header('Location: ./' . $location);

if ($num != 1)
{
	echo "<center><h3>Failed</h3></center>";
} else {
	if ($group1=="na" OR $group1==""){
		$invoicesales=0;$invoicesales_notax=0;$invoicetax=0;$servchrg=0;
	} elseif ($group1==1){
		$invoicesales=1;
		$invoicesales_notax=0;
		$invoicetax=0;
		$servchrg=0;
		$heldcheck=0;
	} elseif ($group1==2){
		$invoicesales=0;
		$invoicesales_notax=1;
		$invoicetax=0;
		$servchrg=0;
		$heldcheck=0;
	} elseif ($group1==3){
		$invoicesales=0;
		$invoicesales_notax=0;
		$invoicetax=1;
		$servchrg=0;
		$heldcheck=0;
	} elseif ($group1==4){
		$invoicesales=0;
		$invoicesales_notax=0;
		$invoicetax=0;
		$servchrg=1;
		$heldcheck=0;
	} elseif ($group1==5){
		$invoicesales=0;
		$invoicesales_notax=0;
		$invoicetax=0;
		$servchrg=0;
		$heldcheck=1;
	}

	if ($edit==1&&$creditname!=""){
		$query = "UPDATE credits SET creditname = '$creditname', credittype = '$credittypeid', account = '$account', invoicesales = '$invoicesales', invoicesales_notax = '$invoicesales_notax', invoicetax = '$invoicetax', servchrg = '$servchrg', salescode = '$salescode', heldcheck = '$heldcheck', tax_prcnt = '$tax_prcnt', parent = '$parent', category = '$category' WHERE creditid = '$creditid'";
		$result = Treat_DB_ProxyOld::query($query);
	}
	elseif ($edit==2){
		$query = "UPDATE credits SET is_deleted = '1', del_date = '$today' WHERE creditid = '$editcredit'";
		$result = Treat_DB_ProxyOld::query($query);
	}
	elseif($creditname!=""){
		$query = "INSERT INTO credits (businessid, companyid, creditname, credittype, account, invoicesales, invoicesales_notax, invoicetax, servchrg, salescode, heldcheck, tax_prcnt, parent, category) VALUES ('$businessid', '$companyid', '$creditname', '$credittypeid', '$account', '$invoicesales', '$invoicesales_notax', '$invoicetax', '$servchrg', '$salescode', '$heldcheck', '$tax_prcnt', '$parent', '$category')";
		$result = Treat_DB_ProxyOld::query($query);
	}
}

?>