<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once(DOC_ROOT.'/bootstrap.php');


class DataColumns {
	protected static $_column_names = array();
	protected static $_column_count = 0;
	
	public static function processColumns( array $data ) {
		$keys = array_keys( $data );
		$cnt = count( $keys );
		if ( $cnt > self::$_column_count ) {
			self::$_column_count = $cnt;
			self::$_column_names = $keys;
		}
	}
	
	public static function getColumnKeys() {
		return self::$_column_names;
	}
}

class QueryDebug {
	protected static $_query = array();
	public static $_num_queries = 1;
	
	public static function setQuery( $num, $query_str ) {
		if ( !array_key_exists( 'query' . $num, self::$_query ) ) {
			self::$_query['query' . $num] = array();
		}
		if ( count( self::$_query['query' . $num] ) < self::$_num_queries ) {
			self::$_query['query' . $num][] = $query_str;
		}
	}
	
	public static function getQueries() {
		return self::$_query;
	}
	
	public static function getQuery( $num ) {
		if ( array_key_exists( 'query' . $num, self::$_query ) ) {
			return self::$_query['query' . $num];
		}
		elseif ( array_key_exists( $num, self::$_query ) ) {
			return self::$_query[$num];
		}
		else {
			return null;
		}
	}
	
	public static function varDump() {
		ksort( self::$_query );
		var_dump( self::$_query );
	}
}


function getRouteInfo( array $route_array, $login_routeid, $machineid ) {
	if ( array_key_exists( $login_routeid, $route_array ) ) {
		if (
			is_array( $route_array[$login_routeid] )
			&& array_key_exists( $machineid, $route_array[$login_routeid] )
		) {
			return $route_array[$login_routeid][$machineid];
		}
		else {
			return $route_array[$login_routeid];
		}
	}
}

function array_walk_csv( &$vals, $key, $filehandler ) {
	fputcsv( $filehandler, $vals, ',', '"' );
}

function getData( $data ) {
	if ( is_array( $data ) ) {
		if ( array_key_exists( 'data', $data ) ) {
			if ( $data['data'] ) {
				$out = array();
				foreach ( $data['data'] as $key => $val ) {
					$tmp = getData( $val );
					// need to make sure that $tmp is an array so
					// that array_merge does not explode
					if ( !is_array( $tmp ) ) {
						$tmp = array( $tmp );
					}
					
					// each layer of the array *should* have a 'companyname' key
					if ( array_key_exists( 'companyname', $tmp ) ) {
						$out[$key] = $tmp;
					}
					else {
						$out = array_merge( $out, $tmp );
					}
				}
				return $out;
			}
			elseif ( array_key_exists( 'options', $data ) ) {
				DataColumns::processColumns( $data['options'] );
				return $data['options'];
			}
		}
	}
	return $data;
}


if ( require_once( realpath( DOC_ROOT . '/../lib/inc/cookie-login.php' ) ) ) {
	$reporttype = isset($_POST['reporttype'])?$_POST['reporttype']:'';
	$date1 = isset($_POST['date1'])?$_POST['date1']:'';
	$date2 = isset($_POST['date2'])?$_POST['date2']:'';
	$dpp = isset($_POST['dpp'])?$_POST['dpp']:'';
	$account = isset($_POST['account'])?$_POST['account']:'';
	$menu_typeid = isset($_POST['menu_typeid'])?$_POST['menu_typeid']:'';
	$breakdown = isset($_POST['breakdown'])?$_POST['breakdown']:'';
	
	$master_data_array = array();

	$weekday = dayofweek($date1);
	$daynum = date( 'N', strtotime( $date1 ) );

	$query = sprintf( "
			SELECT *
			FROM company
			WHERE companyid = '%d'
		"
		, $companyid
	);
	$result = DB_Proxy::query($query);

	$companyname = @mysql_result($result,0,"companyname");

	$query = sprintf( "
			SELECT *
			FROM business
			WHERE businessid = '%d'
		"
		, $businessid
	);
	$result = DB_Proxy::query($query);

	$bustax = @mysql_result($result,0,"tax");
	$curmenu = @mysql_result($result,0,"default_menu");
	$static_menu = @mysql_result($result,0,"static_menu");

	////added 6/3/10
	if ( $menu_typeid > 0 ) {
		$curmenu = $menu_typeid;
		
		$query = sprintf( "
				SELECT static_menu
				FROM menu_type
				WHERE menu_typeid = '%d'
			"
			, $curmenu
		);
		$result = DB_Proxy::query($query);
		
		$static_menu = @mysql_result($result,0,"static_menu");
	}
	
	$colwidth = 60 / $dpp;
	
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////LOCATIONS///////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
	
	$query = sprintf( "
			SELECT *
			FROM menu_groups
			WHERE menu_typeid = '%d'
			ORDER BY menu_group_id DESC
		"
		, $curmenu
	);
	$result = DB_Proxy::query($query);
	$num = mysql_numrows($result);
	
	$num--;
	while ($num >= 0) {
		
		$groupname = @mysql_result($result,$num,"groupname");
		$groupid = @mysql_result($result,$num,"menu_group_id");
		
		$query2 = sprintf( "
				SELECT *
				FROM vend_locations
				WHERE businessid = '%d'
					AND menu_typeid = '%d'
				ORDER BY locationid DESC
			"
			, $businessid
			, $curmenu
		);
		$result2 = DB_Proxy::query($query2);
		$num2 = mysql_numrows($result2);
		QueryDebug::setQuery( 2, $query2 );
		
		$num2--;
		while ($num2 >= 0) {
			$location_name = @mysql_result($result2,$num2,"location_name");
			$locationid = @mysql_result($result2,$num2,"locationid");
			$tmp2 = array(
				'options' => array(),
				'data' => array(),
			);
			$options = array(
				'companyname'   => $companyname,
				'bustax'        => $bustax,
				'curmenu'       => $curmenu,
				'static_menu'   => $static_menu,
				'groupname'     => $groupname,
				'groupid'       => $groupid,
				'location_name' => $location_name,
				'locationid'    => $locationid,
			);
			$tmp2['options'] = $options;
			
			$drivers = 1;
			$counter = 0;
			
			$lastacct =- 1;
			$routetotal = array();
			$grouptotal = array();
			
			$query3 = sprintf( "
SELECT
	lr.login_routeid
	, lr.route
	, lr.order_by_machine
	, vm.machineid
	, vm.name
	, vm.machine_num
	, vms.day
FROM (
	vend_machine vm
		RIGHT JOIN vend_machine_schedule vms
			ON vm.machineid = vms.machineid
			AND vms.day = '%s'
)
	RIGHT JOIN login_route lr
		ON lr.login_routeid = vm.login_routeid
		AND lr.order_by_machine = '1'
		AND lr.cstore = '0' 
		AND lr.locationid = '%d' 
WHERE lr.active = '0'
	AND lr.is_deleted = '0'
	AND lr.locationid = '%d'
ORDER BY lr.route
#LIMIT %d,%d
				"
				,$daynum
				,$locationid
				,$locationid
				,$counter
				,$dpp
			);
			QueryDebug::setQuery( 3, $query3 );
			
			$result3 = DB_Proxy::query($query3);
			$num3 = mysql_numrows($result3);
			
			
			$drivers = $num3;
			
			$query4 = sprintf( "
					SELECT *
					FROM menu_pricegroup
					WHERE menu_groupid = '%d'
					ORDER BY account DESC
						,orderid DESC
				"
				,$groupid
			);
			$result4 = DB_Proxy::query($query4);
			$num4 = mysql_numrows($result4);
			QueryDebug::setQuery( 4, $query4 );
			
			if ($num3 > 0) {
				
				$mycount = 0;
				$totalmachines = 0;
				$lessroutes = 0;
				$routes = array();
				while ($mycount < $num3) {
					$login_routeid = @mysql_result($result3,$mycount,"login_routeid");
					$route = @mysql_result($result3,$mycount,"route");
					$obm = @mysql_result($result3,$mycount,"order_by_machine");
					$machineid = @mysql_result($result3,$mycount,"machineid");
					$machine_num = @mysql_result($result3,$mycount,"machine_num");
					$machine_name = @mysql_result($result3,$mycount,"name");
					$tmp3 = array(
						'login_routeid'    => $login_routeid,
						'route'            => $route,
						'order_by_machine' => $obm,
						'machineid'        => $machineid,
						'machine_num'      => $machine_num,
						'machine_name'     => $machine_name,
					);
					
					if ( $machineid ) {
						$routes[$login_routeid][$machineid] = $tmp3;
					}
					else {
						$routes[$login_routeid] = $tmp3;
					}
					/////ORDER BY MACHINE
					$mycount++;
				}
				$tmp3 = null;
				$num3 = $drivers;
				
				$num4--;
				while ($num4 >= 0) {
					$added_set = false;
					$pricegroupname = @mysql_result($result4,$num4,"menu_pricegroupname");
					$pricegroupid = @mysql_result($result4,$num4,"menu_pricegroupid");
					$price = @mysql_result($result4,$num4,"price");
					$percent = @mysql_result($result4,$num4,"percent");
					$account = @mysql_result($result4,$num4,"account");
					$tmp4 = array(
						'options' => array(),
						'data' => array(),
					);
					$tmp_options = array(
						'pricegroupname' => $pricegroupname,
						'pricegroupid'   => $pricegroupid,
						'price'          => $price,
						'percent'        => $percent,
						'account'        => $account,
					);
					$options = array_merge( $options, $tmp_options );
					$tmp4['options'] = $options;
					
					if ($static_menu == 1) {
						$query5 = sprintf( "
								SELECT *
								FROM vend_item
								WHERE groupid = '%d'
									AND date = '0000-00-00'
									AND businessid = '%d'
								ORDER BY vend_itemid DESC
							"
							,$pricegroupid
							,$businessid
						);
					}
					else {
						$query5 = sprintf( "
								SELECT *
								FROM vend_item
								WHERE groupid = '%d'
									AND date = '%s'
									AND businessid = '%d'
								ORDER BY vend_itemid DESC
							"
							,$pricegroupid
							,$date1
							,$businessid
						);
					}
					$result5 = DB_Proxy::query($query5);
					$num5 = mysql_numrows($result5);
					QueryDebug::setQuery( 5, $query5 );
					
					$num5--;
					$firstnum = $num5;
					while ($num5 >= 0) {
						$menu_itemid = @mysql_result($result5,$num5,"menu_itemid");
						$vend_itemid = @mysql_result($result5,$num5,"vend_itemid");
						
						$query6 = sprintf( "
								SELECT item_name
									,item_code
									,price
									,unit
									,su_in_ou
								FROM menu_items
								WHERE menu_item_id = '%d'
							"
							,$menu_itemid
						);
						$result6 = DB_Proxy::query($query6);
						QueryDebug::setQuery( 6, $query6 );
						
						$menu_itemname       = @mysql_result($result6,0,"item_name");
						$item_code           = @mysql_result($result6,0,"item_code");
						$menu_itemprice      = @mysql_result($result6,0,"price");
						$menu_itemunit       = @mysql_result($result6,0,"unit");
						$menu_item_case_size = @mysql_result($result6,0,"su_in_ou");
						$menu_itemprice = money($menu_itemprice);
						$tmp5 = array(
							'options' => array(),
							'data' => array(),
						);
						$tmp_options = array(
							'menu_itemid'         => $menu_itemid,
							'vend_itemid'         => $vend_itemid,
							'menu_itemname'       => $menu_itemname,
							'item_code'           => $item_code,
							'menu_itemprice'      => $menu_itemprice,
							'menu_itemunit'       => $menu_itemunit,
							'menu_item_case_size' => $menu_item_case_size,
						);
						$options = array_merge( $options, $tmp_options );
						$tmp5['options'] = $options;
						
						$mycount = 0;
						while ($mycount < $num3) {
							$login_routeid = @mysql_result($result3,$mycount,"login_routeid");
							$machineid = @mysql_result($result3,$mycount,"machineid");
							$obm = @mysql_result($result3,$mycount,"order_by_machine");
							
							if ($obm == 1 && $lastlogin_routeid != $login_routeid) {
								$myquery = sprintf(
									"AND (machineid = '%d' OR machineid = '0')"
									,$machineid
								);
							}
							elseif ($obm == 1) {
								$myquery = sprintf(
									"AND machineid = '%d'"
									,$machineid
								);
							}
							else {
								$myquery = '';
							}
							
							$query7 = sprintf( "
									SELECT SUM(amount) AS amount
									FROM vend_order
									WHERE date = '%s'
										AND vend_itemid = '%d'
										AND login_routeid = '%d'
										%s
								"
								,$date1
								,$vend_itemid
								,$login_routeid
								,$myquery
							);
							$result7 = DB_Proxy::query($query7);
							QueryDebug::setQuery( 7, $query7 );
							
							$amount = @mysql_result($result7,0,"amount");
							
							//////breakdown cases
							if ($breakdown == 1 && $menu_item_case_size > 1) {
								$amount = $amount * $menu_item_case_size;
							}
							
							$routetotal[$mycount] = $routetotal[$mycount]+$amount;
							$grouptotal[$mycount] = $grouptotal[$mycount]+$amount;
							
							$tmp6 = array(
								'options' => array(),
								'data' => array(),
							);
							$tmp_options = array(
								'login_routeid'    => $login_routeid,
								'machineid'        => $machineid,
								'order_by_machine' => $obm,
								'amount'           => $amount,
							);
							$tmp_route = getRouteInfo( $routes, $login_routeid, $machineid );
							$options = array_merge( $options, $tmp_route, $tmp_options );
							$tmp6['options'] = $options;
							
							
							$lastlogin_routeid = $login_routeid;
							$tmp5['data']['row'
								. str_pad( $num, 3, '0', STR_PAD_LEFT )
								. ':'
								. str_pad( $num2, 3, '0', STR_PAD_LEFT )
								. ':'
								. str_pad( $num3, 3, '0', STR_PAD_LEFT )
								. ':'
								. str_pad( $num4, 3, '0', STR_PAD_LEFT )
								. ':'
								. str_pad( $num5, 3, '0', STR_PAD_LEFT )
								. ':'
								. str_pad( $mycount, 3, '0', STR_PAD_LEFT )
							] = $tmp6;
							$added_set = true;
							$mycount++;
						}
						$num3 = $drivers;
						
						$tmp4['data']['row'
							. str_pad( $num, 3, '0', STR_PAD_LEFT )
							. ':'
							. str_pad( $num2, 3, '0', STR_PAD_LEFT )
							. ':'
							. str_pad( $num3, 3, '0', STR_PAD_LEFT )
							. ':'
							. str_pad( $num4, 3, '0', STR_PAD_LEFT )
							. ':'
							. str_pad( $num5, 3, '0', STR_PAD_LEFT )
						] = $tmp5;
						$num5--;
					}
					
					$tmp2['data']['row'
						. str_pad( $num, 3, '0', STR_PAD_LEFT )
						. ':'
						. str_pad( $num2, 3, '0', STR_PAD_LEFT )
						. ':'
						. str_pad( $num3, 3, '0', STR_PAD_LEFT )
						. ':'
						. str_pad( $num4, 3, '0', STR_PAD_LEFT )
					] = $tmp4;
					$num4--;
				}
				$routetotal = array();
				$routetotal2 = array();
				
				$counter = $counter + $dpp;
				$num3--;
			}
			
			$master_data_array['row' . $num2] = $tmp2;
			$num2--;
		}
		$num--;
	}
	
#	QueryDebug::varDump();
#	echo '$master_data_array = ';
#	echo '<pre>';
#	print_r( $master_data_array );
#	echo '</pre>';
	
	$test_data = array();
	foreach ( $master_data_array as $val ) {
		$tmp = getData( $val );
		if ( !is_array( $tmp ) ) {
			$tmp = array( $tmp );
		}
		$test_data = array_merge( $test_data, $tmp );
	}
#	echo '$test_data = ';
#	var_dump( $test_data );
#	echo '<pre>';
#	print_r( $test_data );
#	echo '</pre>';
	
	$columns = DataColumns::getColumnKeys();
#	echo 'DataColumns::getColumnKeys() = ';
#	var_dump( $columns );
	
	header( 'Content-type: text/plain' );
	$outstream = fopen( 'php://output', 'w' );
	// output column names
	array_walk_csv( $columns, null, $outstream );
	// output the rest of the array
	array_walk( $test_data, 'array_walk_csv', $outstream );
	fclose( $outstream );
}
