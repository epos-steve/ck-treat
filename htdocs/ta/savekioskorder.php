<?php

define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once(DOC_ROOT.'/bootstrap.php');

if ( require_once( realpath( DOC_ROOT . '/../lib/inc/cookie-login.php' ) ) ) {

	$businessid = $_POST["businessid"];
	$companyid = $_POST["companyid"];
	$order_date = $_POST["order_date"];
	$order_bus = $_POST["order_bus"];
	$login_routeid = $_POST["login_routeid"];
	$menu_typeid = $_POST["menu_typeid"];
	$machineid = $_POST["machineid"];

	////locationid
	$query = "SELECT locationid FROM login_route WHERE login_routeid = $login_routeid";
	$result = DB_Proxy::query($query);

	$locationid = mysql_result($result,0,"locationid");

	////query
	$query = "SELECT
								menu_groups.groupname
								,menu_pricegroup.menu_pricegroupname
								,menu_items.item_name
								,menu_items.menu_item_id
								,menu_items.item_code
								,menu_items_new.id AS myid
								,menu_items_new.pos_id
								,vend_item.vend_itemid
								,vend_order.amount
								,vend_order.vend_orderid
							FROM menu_items
							JOIN menu_pricegroup ON menu_pricegroup.menu_pricegroupid = menu_items.groupid
							JOIN menu_groups ON menu_groups.menu_group_id = menu_pricegroup.menu_groupid
							JOIN vend_item ON vend_item.menu_itemid = menu_items.menu_item_id AND vend_item.date = '$order_date'
							LEFT JOIN vend_order ON vend_order.vend_itemid = vend_item.vend_itemid AND vend_order.login_routeid = $login_routeid AND vend_order.date = '$order_date' AND vend_order.machineid = $machineid
							LEFT JOIN menu_items_new ON menu_items_new.item_code = menu_items.item_code AND menu_items_new.businessid = $businessid
							WHERE menu_items.businessid = $order_bus AND menu_items.menu_typeid = $menu_typeid
							ORDER BY menu_groups.orderid,menu_pricegroup.orderid";
	$result = DB_Proxy::query($query);

	while($r = mysql_fetch_array($result)){
		$vend_itemid = $r["vend_itemid"];
		$vend_orderid = $r["vend_orderid"];
		$myid = $r["myid"];

		$amount = $_POST["amount$vend_itemid"];
		$rollover = $_POST["rollover$myid"];

		if($vend_orderid > 0){
			$query2 = "UPDATE vend_order SET amount = '$amount' WHERE vend_orderid = $vend_orderid";
			$result2 = DB_Proxy::query($query2);
		}
		elseif($amount != 0){
			$query2 = "INSERT INTO vend_order (businessid,amount,date,login_routeid,vend_itemid,locationid,machineid) VALUES
												($order_bus,'$amount','$order_date',$login_routeid,$vend_itemid,$locationid,$machineid)";
			$result2 = DB_Proxy::query($query2);
		}

		$query2 = "UPDATE menu_items_new SET rollover = '$rollover' WHERE id = $myid";
		$result2 = DB_Proxy::query($query2);
	}

	header( 'Location: ' . $_SERVER[ 'HTTP_REFERER' ] . "&goto=1" );
}
