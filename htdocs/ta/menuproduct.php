<?php

function prevday($date2) {
	$day=substr($date2,8,2);
	$month=substr($date2,5,2);
	$year=substr($date2,0,4);
	$day=$day-1;

	if ($day <= 0)
	{
		if ($month == 01)
		{
			$month = '12';
			$year--;
			$day=$day+31;
			$yesterday = "$year-$month-$day";
			return $yesterday;
		}
		else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
		{
			$month--;
			if ($month < 10)
			{
				$month="0$month";
			}
			$day=$day+31;
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
		else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
		{
			$month--;
			if ($month < 10)
			{
				$month="0$month";
			}
			$day=$day+30;
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
		else
		{
			$day=$day+28;
			$month='02';
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
	}
	else
	{
		if ($day < 10)
		{
			$day="0$day";
		}
		$yesterday="$year-$month-$day";
		return $yesterday;
	}
}

function money($diff){
	$findme='.';
	$double='.00';
	$single='0';
	$double2='00';
	$pos1 = strpos($diff, $findme);
	$pos2 = strlen($diff);
	if ($pos1==""){$diff="$diff$double";}
	elseif ($pos2-$pos1==2){$diff="$diff$single";}
	elseif ($pos2-$pos1==1){$diff="$diff$double2";}
	else{}
	$dot = strpos($diff, $findme);
	$diff = substr($diff, 0, $dot+4);
	$diff=round($diff, 2);
	if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
	elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
	$diff = substr($diff, 0, $dot+3);
	$pos1 = strpos($diff, $findme);
	$pos2 = strlen($diff);
	if ($pos1==""){$diff="$diff$double";}
	elseif ($pos2-$pos1==2){$diff="$diff$single";}
	elseif ($pos2-$pos1==1){$diff="$diff$double2";}
	else{}
	return $diff;
}

function dayofweek($date1)
{
	$day=substr($date1,8,2);
	$month=substr($date1,5,2);
	$year=substr($date1,0,4);
	$dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
	return $dayofweek;
}

if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
require_once( DOC_ROOT . '/bootstrap.php' );

$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";


$curmenu=$_GET["curmenu"];
$businessid=$_GET["bid"];
$com=$_GET["com"];
$numserv=1;

if($com==1){
	$date1=$_POST["date1"];
	$date2=$_POST["date2"];
}

	//$nutrition=array();

/////////////////////////MENU PROFITABLITIY////////////////
?>
<head>

<STYLE>
#loading {
	width: 200px;
	height: 48px;
	background-color: ;
	position: absolute;
	left: 50%;
	top: 50%;
	margin-top: -50px;
	margin-left: -100px;
	text-align: center;
}
</STYLE>

<script type="text/javascript" src="preLoadingMessage.js"></script>

<SCRIPT LANGUAGE="JavaScript" SRC="CalendarPopup.js"></SCRIPT>

<STYLE>
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation
			{
			background-color:#6677DD;
			text-align:center;
			vertical-align:center;
			text-decoration:none;
			color:#FFFFFF;
			font-weight:bold;
			}
	.TESTcpDayColumnHeader,
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation,
	.TESTcpCurrentMonthDate,
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDate,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDate,
	.TESTcpCurrentDateDisabled,
	.TESTcpTodayText,
	.TESTcpTodayTextDisabled,
	.TESTcpText
			{
			font-family:arial;
			font-size:8pt;
			}
	TD.TESTcpDayColumnHeader
			{
			text-align:right;
			border:solid thin #6677DD;
			border-width:0 0 1 0;
			}
	.TESTcpCurrentMonthDate,
	.TESTcpOtherMonthDate,
	.TESTcpCurrentDate
			{
			text-align:right;
			text-decoration:none;
			}
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDateDisabled
			{
			color:#D0D0D0;
			text-align:right;
			text-decoration:line-through;
			}
	.TESTcpCurrentMonthDate
			{
			color:#6677DD;
			font-weight:bold;
			}
	.TESTcpCurrentDate
			{
			color: #FFFFFF;
			font-weight:bold;
			}
	.TESTcpOtherMonthDate
			{
			color:#808080;
			}
	TD.TESTcpCurrentDate
			{
			color:#FFFFFF;
			background-color: #6677DD;
			border-width:1;
			border:solid thin #000000;
			}
	TD.TESTcpCurrentDateDisabled
			{
			border-width:1;
			border:solid thin #FFAAAA;
			}
	TD.TESTcpTodayText,
	TD.TESTcpTodayTextDisabled
			{
			border:solid thin #6677DD;
			border-width:1 0 0 0;
			}
	A.TESTcpTodayText,
	SPAN.TESTcpTodayTextDisabled
			{
			height:20px;
			}
	A.TESTcpTodayText
			{
			color:#6677DD;
			font-weight:bold;
			}
	SPAN.TESTcpTodayTextDisabled
			{
			color:#D0D0D0;
			}
	.TESTcpBorder
			{
			border:solid thin #6677DD;
			}
</STYLE>

</head>
<?php
	echo "<body>";

	$curdate=$today;
	for($counter2=1;$counter2<=7;$counter2++){$curdate=prevday($curdate);}

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query12 = "SELECT businessname,contract FROM business WHERE businessid = '$businessid'";
	$result12 = Treat_DB_ProxyOld::query($query12);
	//mysql_close();

	$businessname=mysql_result($result12,0,"businessname");
	$contract=mysql_result($result12,0,"contract");

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query12 = "SELECT menu_typename FROM menu_type WHERE menu_typeid = '$curmenu'";
	$result12 = Treat_DB_ProxyOld::query($query12);
	//mysql_close();

	$curmenuname=mysql_result($result12,0,"menu_typename");

	if ($date1!=""){$showdate=" for $date1 to $date2";}

	echo "<form action=menuproductdetail.php method=post><input type=hidden name=businessid value=$businessid><input type=hidden name=curmenu value=$curmenu><center><table width=100%><tr><td colspan=2><center><b>Menu Production $showdate</b></td></tr></table>";
	echo "<table width=100%><tr><td colspan=2></td></tr>";

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query12 = "SELECT * FROM menu_items WHERE businessid = '$businessid' AND menu_typeid = '$curmenu' ORDER BY groupid DESC, item_name DESC";
	$result12 = Treat_DB_ProxyOld::query($query12);
	$num12=mysql_numrows($result12);
	//mysql_close();

	$lastgroupid=0;
	$num12--;
	while($num12>=0){

		$editid=mysql_result($result12,$num12,"menu_item_id");
		$itemname=mysql_result($result12,$num12,"item_name");
		$groupid=mysql_result($result12,$num12,"groupid");
		$serving_size=mysql_result($result12,$num12,"serving_size");
		$item_price=mysql_result($result12,$num12,"price");

		///////SHOW GROUPS
		if ($lastgroupid!=$groupid){
			if($contract==2){
				$query5 = "SELECT menu_pricegroupname,menu_groupid FROM menu_pricegroup WHERE menu_pricegroupid = '$groupid'";
				$result5 = Treat_DB_ProxyOld::query($query5); 

				$groupname=mysql_result($result5,0,"menu_pricegroupname");
				$menu_groupid=mysql_result($result5,0,"menu_groupid");

				$query5 = "SELECT groupname FROM menu_groups WHERE menu_group_id = '$menu_groupid'";
				$result5 = Treat_DB_ProxyOld::query($query5); 

				$groupname2=mysql_result($result5,0,"groupname");

				$groupname = "$groupname2 - $groupname";
			}
			else{
				$query5 = "SELECT groupname FROM menu_groups WHERE menu_group_id = '$groupid'";
				$result5 = Treat_DB_ProxyOld::query($query5); 

				$groupname=mysql_result($result5,0,"groupname");
			}
			echo "<tr><td bgcolor=#E8E7E7><font size=2><b>$groupname</td><td bgcolor=#E8E7E7 align=right><font size=2><b>Servings</td></tr>";
		}
		else{echo "<tr height=1><td colspan=2 bgcolor=#E8E7E7></td></tr>";}

		if($com==1){
			$query33 = "SELECT SUM(vend_order.amount) AS totalamt FROM vend_order,vend_item WHERE vend_item.businessid = '$businessid' AND vend_item.date >= '$date1' AND vend_item.date <= '$date2' AND vend_item.menu_itemid = '$editid' AND vend_item.vend_itemid = vend_order.vend_itemid";
			$result33 = Treat_DB_ProxyOld::query($query33);

			$totalamt=mysql_result($result33,0,"totalamt");
		}
		else{$totalamt="";}

		echo "<tr onMouseOver=this.bgColor='#999999' onMouseOut=this.bgColor='white'><td><font size=2 color=black>$itemname</font></td><td align=right><input type=text name=$editid value='$totalamt' size=3 STYLE='font-size: 10px;'></td></tr>";
		
		$lastgroupid=$groupid;
		$num12--;
	}
	echo "<tr><td colspan=2 bgcolor=#E8E7E7 align=right><input type=hidden name=date1 value='$date1'><input type=hidden name=date2 value='$date2'>Based on Inventory Count from: <SCRIPT LANGUAGE='JavaScript' ID='js19'> var cal19 = new CalendarPopup('testdiv1');cal19.setCssPrefix('TEST');</SCRIPT> <A HREF='javascript:void();' onClick=cal19.select(document.forms[0].newdate,'anchor19','yyyy-MM-dd'); return false; TITLE=cal19.select(document.forms[0].newdate,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor19' ID='anchor19'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A> <input type=text name=newdate value='$today' size=8> <input type=submit value='GO'></td></tr>";
	echo "</table></form><DIV ID=testdiv1 STYLE=position:absolute;visibility:hidden;background-color:white;layer-background-color:white;></DIV></body>";

	//mysql_close();
	google_page_track();
?>