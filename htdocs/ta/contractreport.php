<?php

define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once(DOC_ROOT.'/bootstrap.php');

\EE\Model\AbstractModel\Treat::db()->setReadFromWriter();
\EE\Model\Base::db()->setReadFromWriter();

function nextday($date2)
{
	return date('Y-m-d', Treat_Date::getNextDay($date2));
}

function money($diff){   
	$findme='.';
	$double='.00';
	$single='0';
	$double2='00';
	$pos1 = strpos($diff, $findme);
	$pos2 = strlen($diff);
	if ($pos1 == "")
	{
		$diff="$diff$double";
	}
	elseif ( ($pos2 - $pos1) == 2)
	{
		$diff="$diff$single";
	}
	elseif ( ($pos2 - $pos1) == 1)
	{
		$diff="$diff$double2";
	}

	$dot = strpos($diff, $findme);
	$diff = substr($diff, 0, $dot+4);
	$diff = round($diff, 2);
	if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
	elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
	$diff = substr($diff, 0, $dot+3);
	$pos1 = strpos($diff, $findme);
	$pos2 = strlen($diff);
	if ($pos1==""){$diff="$diff$double";}
	elseif ($pos2-$pos1==2){$diff="$diff$single";}
	elseif ($pos2-$pos1==1){$diff="$diff$double2";}
	else{}
	return $diff;
}

function dayofweek($date1)
{
	$timestamp = Treat_Date::getValidTimeStamp($date1);
	if (!$timestamp)
	{
		return false;
	}
	return date('l', $timestamp);
}

function order_by_assign_and_name($left, $right)
{
	if ($left->assign == $right->assign)
	{
		return strcmp($left->name, $right->name);
	}
	if ( is_numeric($left->assign) && is_numeric($right->assign) )
	{
		return ($left->assign > $right->assign ? 1 : -1);
	}
	return strcmp($left->assign, $right->assign);
}

$user = \EE\Controller\Base::getSessionCookieVariable('usercook');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook');
$companyid = \EE\Controller\Base::getSessionCookieVariable('compcook');
$businessid = \EE\Controller\Base::getPostVariable('businessid');
$reporttype = \EE\Controller\Base::getPostVariable('reporttype');
$date1 = \EE\Controller\Base::getPostVariable('date1');
$date2 = \EE\Controller\Base::getPostVariable('date2', $date1);
$uuid = \EE\Controller\Base::genUuid();

//$account = $_POST['account'];
$account = \EE\Controller\Base::getPostVariable('account');
$day = date("d");
$year = date("Y");
$month = date("m");
$today = "$month/$day/$year";

$weekend = dayofweek($date1);

if($weekend=="Saturday"||$weekend=="Sunday")
{
   $date2=$date1;
}
else
{
	$date2 = nextday($date1);
	$date2name = dayofweek($date2);
	if ($date2name=="Saturday")
	{
		$date2=nextday($date2);
		$date2=nextday($date2);
	}
	elseif ($date2name=="Sunday")
	{
		$date2=nextday($date2);
	}
}

$query = "SELECT * FROM caterclose WHERE businessid = '$businessid' AND date = '$date2'";
$result = Treat_DB_ProxyOld::query($query, true);
$num = Treat_DB_ProxyOld::mysql_numrows($result);

if ($num>0){
   $date2=nextday($date2);
   $date2name=dayofweek($date2);
   if ($date2name=="Saturday"){$date2=nextday($date2);$date2=nextday($date2);}
   elseif ($date2name=="Sunday"){$date2=nextday($date2);}
}

$query = "SELECT * FROM caterclose WHERE businessid = '$businessid' AND date = '$date1'";
$result = Treat_DB_ProxyOld::query($query, true);
$num = Treat_DB_ProxyOld::mysql_numrows($result);

if ($num>0){$date2=$date1;}

$passed_cookie_login = require_once( realpath( DOC_ROOT . '/../lib/inc/cookie-login.php' ) );

if ( $passed_cookie_login )
{
	$menucount = array();
	$menuprice = array();

	$company = Treat_Model_Company::getCompanyById($companyid);
	if ( !$company )
	{
		$company = new Treat_Model_Company_Object;
	}

	$business = Treat_Model_Business::getById($businessid);
	$bustax = $business->tax;
	$default_menu = $business->default_menu;

	$accounts = array();
	if ( $account[0] == -1 )
	{
		$accounts = Treat_Model_Account::getByBusinessId( $businessid );
	}
	else
	{
		foreach ( $account as $key )
		{
			$accounts[] = Treat_Model_Account::getByAccountId( $key );
		}
		// This is not yet needed, since really the sort should be by driver.
		//usort($accounts, 'order_by_assign_and_name');
	}

	foreach($accounts as $account)
	{
		$accountid    = $account->accountid;
		$accountname  = $account->name;
		$acct_menu    = $account->menu;
		if ( $acct_menu == -1 )
		{
			$acct_menu = $default_menu;
		}
		$allow_sub    = $account->allow_sub;
		$contract_num = $account->accountnum;
		$assign       = $account->assign;

		$daypartname[] = "";
		$daypartid[]   = "";
		$portionname[] = "";
		$portionid[]   = "";
		$custqty[]     = "";

////////////////CHECK FOR ORDERS

		$total_qty = 0;
		$totalQuantityOrderDetail = Treat_Model_Order_Detail::getTotalQtyByAccountIdAndDates($account->accountid, $date1, $date2);
		if ( $totalQuantityOrderDetail )
		{
			$total_qty = intval($totalQuantityOrderDetail->total_qty);
		}
		$custtotal = $total_qty;

////////////////////////////////////

		if ($custtotal != 0)
		{
?>
	<table width=100%>
		<tr>
			<td width=33%>
				<img src=login.jpg height=75 width=80>
			</td>
			<td width=33%>
				<font size=2>
				<br>
				<center>
					<b><?php echo $company->companyname; ?></b>
					<br>
					Production &amp; Order Sheet
					<br>
					<?php echo dayofweek($date1); ?> <?php echo $date1; ?>
				</center>
				</font>
			</td>
			<td width=33% align=right>
				<br>
				<table border=1 width=90%>
					<tr>
						<td>
							<font size=1>Site: <?php echo $account->accountnum; ?> <?php echo $account->name; ?>
						</td>
					</tr>
				</table>
				<font size=1>Date: <?php echo $today; ?>
			</td>
		</tr>
	</table>
<?php
			$query2 = "SELECT * FROM menu_type WHERE menu_typeid = '$acct_menu'";
			$result2 = Treat_DB_ProxyOld::query($query2, true);
			$num2 = Treat_DB_ProxyOld::mysql_numrows($result2);

			$num2--;
			while ( $menuTypeRow = Treat_DB_ProxyOld::mysql_fetch_object( $result2 ) )
			{
				$menu_typeid = $menuTypeRow->menu_typeid;

				$query3 = "SELECT * FROM menu_daypart WHERE menu_typeid = '$menu_typeid' ORDER BY orderid ASC";
				$result3 = Treat_DB_ProxyOld::query($query3, true);
				$num3 = Treat_DB_ProxyOld::mysql_numrows($result3);

				$query4 = "SELECT * FROM menu_portion WHERE menu_typeid = '$menu_typeid' ORDER BY orderid ASC";
				$result4 = Treat_DB_ProxyOld::query($query4, true);
				$num4 = Treat_DB_ProxyOld::mysql_numrows($result4);

				$colwidth = 70 / ($num4 * 3);

				$num3--;
				$daypartcount = 0;
				while ( $menuDayPart = Treat_DB_ProxyOld::mysql_fetch_object( $result3 ) )
				{
					$menu_daypartid = $menuDayPart->menu_daypartid;
					$menu_daypartname = $menuDayPart->menu_daypartname;
					$breakfast = $menuDayPart->breakfast;
					if( $menuDayPart->breakfast == 1 )
					{
						$temp_date = $date2;
					}
					else
					{
						$temp_date = $date1;
					}

////////////////CHECK FOR ORDERS FOR DAYPART

					$total_qty = 0;
					$totalQtyMenuDayPart = Treat_Model_Order_Detail::getTotalQtyByDayPartIdForDate($accountid, $menuDayPart->menu_daypartid, $temp_date);
					if ( $totalQtyMenuDayPart )
					{
						$total_qty = intval($totalQtyMenuDayPart->total_qty);
					}
					$daypartmenutotal = $total_qty;

////////////////////////////////////
					if ($daypartmenutotal != 0)
					{
?>
	<table width=100%>
		<tr>
			<td width=50%>
				<table border=1>
					<tr>
						<td>
							<font size=1>MEAL - <?php echo $menuDayPart->menu_daypartname; ?></font>
						</td>
					</tr>
				</table>
			</td>
			<td align=right width=50%>
				<table>
					<tr>
						<td align=right>
							<font size=1>TOTAL MEALS: <?php echo $daypartmenutotal; ?></font>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<br>
	<table border=1 width=100%>
		<tr>
			<td width=15%>
				<font size=1>Food Item</font>
			</td>
<?php

						$result4 = Treat_DB_ProxyOld::query($query4, true);
						$num5 = $num4 - 1;
						while ( $menuPortion = Treat_DB_ProxyOld::mysql_fetch_object( $result4 ) )
						{
							$menu_portionid = $menuPortion->menu_portionid;
							$menu_portionname = $menuPortion->menu_portionname;

							$daypartname[$menuDayPart->menu_daypartid] = $menuPortion->menu_portionname;
							$daypartid[$menuDayPart->menu_daypartid] = $menuPortion->menu_portionid;

							$total_qty = 0;
							$totalQtyMenuPortion = Treat_Model_Order_Detail::getTotalQtyByMenuPortionIdForDate($accountid, $menu_daypartid, $menu_portionid, $temp_date);
/*
							$query6 = "SELECT qty FROM order_detail WHERE menu_daypartid = '$menu_daypartid' AND menu_portionid = '$menu_portionid' AND accountid = '$accountid' AND date = '$temp_date' AND qty > 0";
							$result6 = Treat_DB_ProxyOld::query($query6, true);
							$num6 = Treat_DB_ProxyOld::mysql_numrows($result6);
*/

							if ( $totalQtyMenuPortion )
							{
								$total_qty = $totalQtyMenuPortion->total_qty;
							}
							$orderqty[$menu_portionid] = $total_qty;
							// $orderqty[$num5] = $total_qty;

							$query57 = "SELECT * FROM order_matrixcust WHERE daypartid = '$menu_daypartid' AND portionid = '$menu_portionid' AND accountid = '$accountid'";
							$result57 = Treat_DB_ProxyOld::query($query57, true);
							$num57 = Treat_DB_ProxyOld::mysql_numrows($result57);
							$orderMatrixCust = Treat_DB_ProxyOld::mysql_fetch_object($result57);

							if (floatval($orderMatrixCust->price) > 0)
							{
?>
			<td width="<?php echo $colwidth; ?>%">
				<font size=1><?php echo $menuPortion->menu_portionname; ?>
				<br>
				Portion
			</td>
			<td width="<?php echo $colwidth; ?>%">
				<font size="1">No. of<br><?php echo $menuPortion->menu_portionname; ?></font>
			</td>
<?php /*    removed 3/3/11
			<td width="<?php echo $colwidth; ?>%">
				<font size=1>Dec.<br>Equiv</font>
			</td>
*/ ?>
<?php
								if ( !array_key_exists($menu_daypartid, $menucount) )
								{
									$menucount[$menu_daypartid] = 0;
								}
								if ( !array_key_exists($menu_daypartid, $menuprice) )
								{
									$menuprice[$menu_daypartid] = 0;
								}
								$menucount[$menu_daypartid] += $total_qty;
								$menuprice[$menu_daypartid] += (floatval($orderMatrixCust->price) * $total_qty);
							}

							$num5--;
						}
?>
			<td width=15%>
				<font size=1>Total Qty<br>Needed</font>
			</td>
		</tr>
<?php
						$query8 = "SELECT * FROM order_item WHERE daypartid = '$menu_daypartid' AND businessid = '$businessid' AND date = '$temp_date' ORDER BY order_itemid DESC";
						$result8 = Treat_DB_ProxyOld::query($query8, true);
						$num8 = Treat_DB_ProxyOld::mysql_numrows($result8);

						$num8--;
						while ( $orderItem = Treat_DB_ProxyOld::mysql_fetch_object( $result8 ) )
						{
							$menu_itemid = $orderItem->menu_itemid;
							$order_itemid = $orderItem->order_itemid;

							$query9 = "SELECT * FROM menu_items WHERE menu_item_id = '$menu_itemid'";
							$result9 = Treat_DB_ProxyOld::query($query9, true);
							$menuItemsForOrderItems = Treat_DB_ProxyOld::mysql_fetch_object( $result9 );

							$menu_itemname = $menuItemsForOrderItems->item_name;
							$true_unit = $menuItemsForOrderItems->true_unit;
							$order_unit = $menuItemsForOrderItems->order_unit;
							$su_in_ou = $menuItemsForOrderItems->su_in_ou;
							$contract_option = $menuItemsForOrderItems->contract_option;

							$query9 = "SELECT * FROM size WHERE sizeid = '$true_unit'";
							$result9 = Treat_DB_ProxyOld::query($query9, true);
							$sizeObject = Treat_DB_ProxyOld::mysql_fetch_object( $result9 );

							$sizename = '';
							if ( is_object($sizeObject) )
							{
								$sizename = $sizeObject->sizename;
							}

							$query9 = "SELECT * FROM size WHERE sizeid = '$order_unit'";
							$result9 = Treat_DB_ProxyOld::query($query9, true);
							$sizeObject = Treat_DB_ProxyOld::mysql_fetch_object( $result9 );

							$serving = '';
							if ( is_object($sizeObject) )
							{
								$serving = $sizeObject->sizename;
							}
?>
		<tr>
			<td>
				<font size=1><?php echo $menu_itemname; ?></font>
			</td>
<?php
							$totaldec = 0;
							$result4 = Treat_DB_ProxyOld::query($query4, true);
							$num5 = $num4 - 1;
							while($menuPortion = Treat_DB_ProxyOld::mysql_fetch_object( $result4 ))
							{
								$menu_portionid= $menuPortion->menu_portionid;
								$menu_portionname= $menuPortion->menu_portionname;

								$query9 = "
									SELECT 
										*
									FROM 
										menu_portiondetail
									WHERE 
										menu_itemid = '$menu_itemid'
										AND menu_typeid = '$menu_typeid'
										AND menu_portionid = '$menu_portionid'
										AND menu_daypartid = '$menu_daypartid'
								";
								$result9 = Treat_DB_ProxyOld::query($query9, true);
								$menuPortionDetail = Treat_DB_ProxyOld::mysql_fetch_object( $result9 );

								$srv_size = $portiondetail = '';
								
								if ( is_object( $menuPortionDetail ) )
								{
									$portiondetail = $menuPortionDetail->portion;

									$srv_size = $menuPortionDetail->srv_size;
									if ( $srv_size == "" )
									{
										$srv_size = "$portiondetail $sizename";
									}
								}

								if ( $contract_option == 1 && $allow_sub == 1 )
								{
									$query62 = "
										SELECT
											*
										FROM
											order_subs
										WHERE
											daypartid = '$menu_daypartid'
											AND portionid = '$menu_portionid'
											AND accountid = '$accountid'
											AND order_itemid = '$order_itemid'
									";
									$result62 = Treat_DB_ProxyOld::query($query62, true);
									$num62 = Treat_DB_ProxyOld::mysql_numrows($result62);
									$object62 = Treat_DB_ProxyOld::mysql_fetch_object($result62);
									$subvalue = 0;
									if ( is_object($object62) )
									{
										$subvalue = $object62->amount;
									}
								}

								$decimalequiv=0;
								if ($contract_option==1 && $allow_sub==1){
									$decimalequiv=($portiondetail*$subvalue)/$su_in_ou*10;
									$decimalequiv=ceil($decimalequiv);
									$decimalequiv=$decimalequiv/10;
								}
								else
								{
									$decimalequiv=($portiondetail*$orderqty[$menu_portionid])/$su_in_ou*10;
									$decimalequiv=ceil($decimalequiv);
									$decimalequiv=$decimalequiv/10;
								}
								$totaldec = $totaldec + $decimalequiv;

								$query57 = "SELECT * FROM order_matrixcust WHERE daypartid = '$menu_daypartid' AND portionid = '$menu_portionid' AND accountid = '$accountid'";
								$result57 = Treat_DB_ProxyOld::query($query57, true);
								$num57 = Treat_DB_ProxyOld::mysql_numrows($result57);

								$p57 = Treat_DB_ProxyOld::mysql_result($result57,0,"price");

								if ( $p57 > 0 )
								{
									if( $contract_option == 1 && $allow_sub == 1 )
									{
										echo "<td><font size=1>$srv_size</td><td align=right><font size=1>$subvalue</td>";
									}
									else
									{
										echo "<td><font size=1>$srv_size</td><td align=right><font size=1>$orderqty[$menu_portionid]</td>";
									}

									////removed 3/3/11
									//<td align=right><font size=1>$decimalequiv</td>
								}

								$num5--;
							}
?>
			<td align=right>
				<center>
					<table>
						<tr>
							<td align=right width=50%><font size=1><?php echo $totaldec; ?></font></td>
							<td><font size=1><?php echo $serving; ?></td>
						</tr>
					</table>
				</center>
			</td>
		</tr>

<?php
/////////UPDATE TOTALS/////////////////////////////////////////////////////////////////
							$query30 = "SELECT * FROM contract_temp WHERE businessid = '$businessid' AND menu_itemid = '$menu_itemid' AND uuid_value = '$uuid'";
							$result30 = Treat_DB_ProxyOld::query($query30, true);
							$num30 = Treat_DB_ProxyOld::mysql_numrows($result30);

							if ( ($contractTemp = Treat_DB_ProxyOld::mysql_fetch_object( $result30 )) )
							{
								$grandtotal = doubleval($contractTemp->total) + $totaldec;

								$query31 = "UPDATE contract_temp SET total = '$grandtotal' WHERE businessid = '$businessid' AND menu_itemid = '$menu_itemid' AND uuid_value = '$uuid'";
								$result31 = Treat_DB_ProxyOld::query($query31, true);
							}
							else
							{
								$query31 = "INSERT INTO contract_temp (businessid,menu_itemid,item_name,total,order_units,uuid_value) VALUES ('$businessid','$menu_itemid','$menu_itemname','$totaldec','$serving','$uuid')";
								$result31 = Treat_DB_ProxyOld::query($query31, true);
							}
/////////END TOTALS/////////////////////////////////////////////////////////////////////

							$num8--;
						}
?>
	</table>
	<br>
<?php
					} //END IF STATEMENT
 
					$daypartcount++;
					$num3--;
				}
				$num2--;
			}

			////Add-Ons
			$addon = array();
			$addon[] = '<table width=100% border=1 cellspacing=0 style="font-size:12px;">';

			$query39 = "SELECT add_on.*,menu_items.item_name FROM add_on
						 JOIN menu_items ON menu_items.menu_item_id = add_on.menu_itemid
						 WHERE add_on.accountid = $accountid AND add_on.date = '$temp_date'";
			$result39 = Treat_DB_ProxyOld::query($query39, true);

			$count39 = 0;
			while ( ($addOn = Treat_DB_ProxyOld::mysql_fetch_object($result39)) )
			{
				$ao_amount = number_format($addOn->qty * $addOn->price, 2);

				$addon[] = '	<tr>';
				$addon[] = "		<td>{$addOn->item_name}</td>";
				$addon[] = "		<td align=right>{$addOn->qty}</td>";
				$addon[] = "		<td align=right>{$ao_amount}</td>";
				$addon[] = '	</tr>';
				$count39++;
			}
			$addon[] = "</table>";

			if ($count39 == 0) {
				$addon = "________________________<br>________________________<br>________________________";
			} else {
				$addon = implode("\n", $addon);
			}
?>

	<br>
	<br>
	<table width=100% style="font-size:12px;">
		<tr>
			<td width=10% align=right>Add Ons:</td>
			<td rowspan=3><?php echo $addon; ?></td>
			<td><center>___________________________________</center></td>
		</tr>
		<tr>
			<td></td>
			<td><center>(Receipt Signature)</center></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
		</tr>
	</table>
	<br>
	
	Driver: <?php echo $assign; ?> 
	<p style='page-break-before: always'></p>

<?php
		} // END Cust Total check IF

	} // END Accounts Foreach
?>
	<br>
	<br>
	<center>
		<table width=100%>
			<tr valign=top>
				<td width=50%>
					<center>
						<table border=1 width=90%>
							<tr>
								<td colspan=3>
									<b>TOTALS</b>
								</td>
							</tr>
<?php

	$query10 = "SELECT * FROM contract_temp WHERE businessid = '$businessid' AND uuid_value = '$uuid'";
	$result10 = Treat_DB_ProxyOld::query($query10, true);
	$num10 = Treat_DB_ProxyOld::mysql_numrows($result10);

	$num10--;
	while ( ($contractTemp = Treat_DB_ProxyOld::mysql_fetch_object( $result10 )) )
	{
?>
							<tr>
								<td><?php echo $contractTemp->item_name; ?></td>
								<td align=right><?php echo $contractTemp->total; ?></td>
								<td><?php echo $contractTemp->order_units; ?></td>
							</tr>
<?php
		$num10--;
	}
?>
						</table>
					</center>
				</td>
				<td>
					<center>
						<table border=1 width=90%>

<?php
	//////////MENU TOTALS
	$menutotal = 0;
	$menutotalprice = 0;

	$query10 = "SELECT * FROM menu_daypart, menu_type WHERE menu_type.businessid = '$businessid' AND menu_type.menu_typeid = menu_daypart.menu_typeid ORDER BY menu_daypart.orderid ASC";
	$result10 = Treat_DB_ProxyOld::query($query10, true);
	$num10 = Treat_DB_ProxyOld::mysql_numrows($result10);

	$num10--;
	while ( ($menuDayPartJoinedWithType = Treat_DB_ProxyOld::mysql_fetch_object( $result10 )) ) {
		if ( array_key_exists($menuDayPartJoinedWithType->menu_daypartid, $menucount) )
		{
			$menutotal += $menucount[$menuDayPartJoinedWithType->menu_daypartid];
		}
		if ( array_key_exists($menuDayPartJoinedWithType->menu_daypartid, $menuprice) )
		{
			$menutotalprice += $menuprice[$menuDayPartJoinedWithType->menu_daypartid];
		}
?>
							<tr>
								<td><?php echo $menuDayPartJoinedWithType->menu_daypartname; ?></td>
								<?php if ( !array_key_exists($menuDayPartJoinedWithType->menu_daypartid, $menucount) || !array_key_exists($menuDayPartJoinedWithType->menu_daypartid, $menuprice) ) : ?>
								<td align=right>0</td>
								<td>$0</td>
								<?php else : ?>
								<td align=right><?php echo $menucount[$menuDayPartJoinedWithType->menu_daypartid]; ?></td>
								<td>$<?php echo money($menuprice[$menuDayPartJoinedWithType->menu_daypartid]); ?></td>
								<?php endif; ?>
							</tr>

<?php
		$num10--;
	}
?>
							<tr>
								<td>
									<b>Totals</b>
								</td>
								<td align=right><?php echo $menutotal; ?></td>
								<td>$<?php echo money($menutotalprice); ?></td>
							</tr>
						</table>
					</center>
				</td>
			</tr>
		</table>
	</center>

<?php
	$query10 = "DELETE FROM contract_temp WHERE businessid = '$businessid' AND uuid_value = '$uuid'";
	//$result10 = Treat_DB_ProxyOld::query($query10, true);

	$query10 = "OPTIMIZE TABLE contract_temp";
	//$result10 = Treat_DB_ProxyOld::query($query10, true);
}
\EE\Model\AbstractModel\Treat::db()->unsetReadFromWriter();
\EE\Model\Base::db()->unsetReadFromWriter();

?>
