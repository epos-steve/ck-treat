<?php
//include("db.php");
define('DOC_ROOT', dirname(dirname(__FILE__)));
require_once(DOC_ROOT.'/bootstrap.php');
$style = "text-decoration:none";

/*$user=$_COOKIE["usercook"];
$pass=$_COOKIE["passcook"];
$copyfrom=$_POST['copyfrom'];
$copyto=$_POST['copyto'];
$companyid=$_POST['companyid'];
$totalcredit=$_POST['totalcredit'];
$totaldebit=$_POST['totaldebit'];
$totalap=$_POST['totalap'];
$totalvendor=$_POST['totalvendor'];
$corpmenu=$_POST['corpmenu'];
$menu=$_POST['menu'];
$copymenu=$_POST['copymenu'];
$copyposmenu=$_POST["copyposmenu"];
$inventory=$_POST['inventory'];
$copyinventory=$_POST['copyinventory'];   
$pos_menu = $_POST['pos_menu'];*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook');
$copyfrom = \EE\Controller\Base::getPostVariable('copyfrom');
$copyto = \EE\Controller\Base::getPostVariable('copyto');
$companyid = \EE\Controller\Base::getPostVariable('companyid');
$totalcredit = \EE\Controller\Base::getPostVariable('totalcredit');
$totaldebit = \EE\Controller\Base::getPostVariable('totaldebit');
$totalap = \EE\Controller\Base::getPostVariable('totalap');
$totalvendor = \EE\Controller\Base::getPostVariable('totalvendor');
$corpmenu = \EE\Controller\Base::getPostVariable('corpmenu');
$menu = \EE\Controller\Base::getPostVariable('menu');
$copymenu = \EE\Controller\Base::getPostVariable('copymenu');
$copyposmenu = \EE\Controller\Base::getPostVariable('copyposmenu');
$inventory = \EE\Controller\Base::getPostVariable('inventory');
$copyinventory = \EE\Controller\Base::getPostVariable('copyinventory');
$pos_menu = \EE\Controller\Base::getPostVariable('pos_menu');

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=Treat_DB_ProxyOld::mysql_numrows($result);

if ($num != 1 || $user =="" || $pass == "" || $copyto==0 || $copyfrom==0)
{
	echo "<center><h3>Failed</h3>Please choose valid units.</center>";
}

else
{
	$security_level = Treat_DB_ProxyOld::mysql_result($result,0,"security_level");
	if ($security_level <= 2) {
		$location = "busdetail.php?bid=$copyfrom&cid=$companyid"; 
		header('Location: ./' . $location);
	}

	$query = "SELECT * FROM business WHERE companyid = '$companyid' AND businessid = '$copyfrom'";
	$result = Treat_DB_ProxyOld::query($query);

	$businessname=Treat_DB_ProxyOld::mysql_result($result,0,"businessname");
	$unitnum=Treat_DB_ProxyOld::mysql_result($result,0,"unit");

	$query = "SELECT * FROM business WHERE companyid = '$companyid' AND businessid = '$copyto'";
	$result = Treat_DB_ProxyOld::query($query);

	$businessname2=Treat_DB_ProxyOld::mysql_result($result,0,"businessname");
	$unitnum2=Treat_DB_ProxyOld::mysql_result($result,0,"unit");
	$newcompanyid=Treat_DB_ProxyOld::mysql_result($result,0,"companyid");

	echo "<center><h3>Items copied from $businessname ($unitnum) to $businessname2 ($unitnum2)</h3></center>";

//////////////////CREDITS///////////////////////////////////
	for ($count = 0; $count < $totalcredit; $count++) {
		$getme="cred$count";
		$getme2="2cred$count";

		$creditid = $_POST[$getme];
		$chgacct = $_POST[$getme2];

		if ($creditid != ""){
			$query = "SELECT * FROM credits WHERE creditid = '$creditid'";
			$result = Treat_DB_ProxyOld::query($query);

			$creditname=Treat_DB_ProxyOld::mysql_result($result,0,"creditname");
			$credittype=Treat_DB_ProxyOld::mysql_result($result,0,"credittype");
			$account=Treat_DB_ProxyOld::mysql_result($result,0,"account");
			$invoicesales=Treat_DB_ProxyOld::mysql_result($result,0,"invoicesales");
			$invoicesales_notax=Treat_DB_ProxyOld::mysql_result($result,0,"invoicesales_notax");
			$invoicetax=Treat_DB_ProxyOld::mysql_result($result,0,"invoicetax");
			$servchrg=Treat_DB_ProxyOld::mysql_result($result,0,"servchrg");
			$payment=Treat_DB_ProxyOld::mysql_result($result,0,"heldcheck");
			$category=Treat_DB_ProxyOld::mysql_result($result,0,"category");

			if ($chgacct != 1) {
				$account = substr($account,0,11);
				$account = "$account$unitnum2";
			}

			$query = "
				INSERT INTO credits (
					businessid,
					companyid,
					creditname,
					credittype,
					account,
					invoicesales,
					invoicesales_notax,
					invoicetax,
					servchrg,
					heldcheck,
					category
				) VALUES (
					'$copyto',
					'$companyid',
					'$creditname',
					'$credittype',
					'$account',
					'$invoicesales',
					'$invoicesales_notax',
					'$invoicetax',
					'$servchrg',
					'$heldcheck',
					'$category'
				)
			";
			$result = Treat_DB_ProxyOld::query($query);

			echo "<li>$creditname ($account) added to $businessname2<br>";
		}

	}
	echo "<p><br>";
///////////////////////////////////////////////////////////////////////////////////////////////////
	for ($count=0;$count<$totaldebit;$count++){

		$getme="deb$count";
		$getme2="2deb$count";

		$debitid=$_POST[$getme];
		$chgacct=$_POST[$getme2];

		if ($debitid != ""){
			$query = "SELECT * FROM debits WHERE debitid = '$debitid'";
			$result = Treat_DB_ProxyOld::query($query);

			$debitname=Treat_DB_ProxyOld::mysql_result($result,0,"debitname");
			$debittype=Treat_DB_ProxyOld::mysql_result($result,0,"debittype");
			$account=Treat_DB_ProxyOld::mysql_result($result,0,"account");
			$inv_tender=Treat_DB_ProxyOld::mysql_result($result,0,"inv_tender");
			$heldcheck=Treat_DB_ProxyOld::mysql_result($result,0,"heldcheck");
			$category=Treat_DB_ProxyOld::mysql_result($result,0,"category");

			if ($chgacct!=1){
				$account = substr($account,0,11);
				$account = "$account$unitnum2";
			}

			$query = "
				INSERT INTO debits (
					businessid,
					companyid,
					debitname,
					debittype,
					account,
					inv_tender,
					heldcheck,
					category
				) VALUES (
					'$copyto',
					'$companyid',
					'$debitname',
					'$debittype',
					'$account',
					'$inv_tender',
					'$heldcheck',
					'$category'
				)
			";
			$result = Treat_DB_ProxyOld::query($query);

			echo "<li>$debitname ($account) added to $businessname2<br>";
		}

	}
	echo "<p><br>";
	////////////////////////////////////////////////////////////////////////////////////////////////////
	for ($count = 0; $count < $totalvendor; $count++) {
		$getme="ven$count";

		//$vendorid=$_POST[$getme];
		$vendorid = \EE\Controller\Base::getPostVariable($getme);

		if ($vendorid != "") {
			$query = "SELECT * FROM vendors WHERE vendorid = '$vendorid'";
			$result = Treat_DB_ProxyOld::query($query);

			$name=Treat_DB_ProxyOld::mysql_result($result,0,"name");
			$vendor_num=Treat_DB_ProxyOld::mysql_result($result,0,"vendor_num");
			$master=Treat_DB_ProxyOld::mysql_result($result,0,"master");

			$query = "INSERT INTO vendors (businessid,name,vendor_num,master) VALUES ('$copyto','$name','$vendor_num','$master')";
			$result = Treat_DB_ProxyOld::query($query);

			echo "<li>$name added to $businessname2<br>";
		}

	}
	echo "<p><br>";
////////////////////////////////////////////////////////////////////////////////////////////////////
	for ($count=0;$count<$totalap;$count++){

		$getme="ap$count";
		$getme2="2ap$count";

		/*$acct_payableid=$_POST[$getme];
		$chgacct=$_POST[$getme2];*/

		$acct_payableid = \EE\Controller\Base::getPostVariable($getme);
		$chgacct = \EE\Controller\Base::getPostVariable($getme2);

		if ($acct_payableid != "") {
			$query = "SELECT * FROM acct_payable WHERE acct_payableid = '$acct_payableid'";
			$result = Treat_DB_ProxyOld::query($query);

			$acct_name=Treat_DB_ProxyOld::mysql_result($result,0,"acct_name");
			$acct_payableid=Treat_DB_ProxyOld::mysql_result($result,0,"acct_payableid");
			$account=Treat_DB_ProxyOld::mysql_result($result,0,"accountnum");
			$cos=Treat_DB_ProxyOld::mysql_result($result,0,"cos");
			$cs_type=Treat_DB_ProxyOld::mysql_result($result,0,"cs_type");
			$daily_fixed=Treat_DB_ProxyOld::mysql_result($result,0,"daily_fixed");
			$daily_labor=Treat_DB_ProxyOld::mysql_result($result,0,"daily_labor");
			$cs_glacct=Treat_DB_ProxyOld::mysql_result($result,0,"cs_glacct");
			$cs_prcnt=Treat_DB_ProxyOld::mysql_result($result,0,"cs_prcnt");

			if($chgacct != 1){
				$account=substr($account,0,11);
				$account="$account$unitnum2";
			}

			$query = "
				INSERT INTO acct_payable (
					businessid,
					companyid,
					acct_name,
					accountnum,
					cos,
					cs_type,
					daily_fixed,
					daily_labor,
					cs_glacct,
					cs_prcnt
				) VALUES (
					'$copyto',
					'$companyid',
					'$acct_name',
					'$account',
					'$cos',
					'$cs_type',
					'$daily_fixed',
					'$daily_labor',
					'$glacct',
					'$cs_prcnt'
				)
			";
			$result = Treat_DB_ProxyOld::query($query);

			echo "<li>$acct_name ($account) added to $businessname2<br>";
		}

	}
////////////////////////////////////////////////////////////////////////////////////////////////////
	if ($corpmenu == 1) {
		$query = "SELECT * FROM menu_items WHERE businessid = '0' AND companyid = '$companyid'";
		$result = Treat_DB_ProxyOld::query($query);
		$num=Treat_DB_ProxyOld::mysql_numrows($result);

		$num--;
		while ($num>=0) {
			$menu_item_id=Treat_DB_ProxyOld::mysql_result($result,$num,"menu_item_id");
			$item_name=Treat_DB_ProxyOld::mysql_result($result,$num,"item_name");
			$price=Treat_DB_ProxyOld::mysql_result($result,$num,"price");
			$parent=Treat_DB_ProxyOld::mysql_result($result,$num,"parent");
			$groupid=Treat_DB_ProxyOld::mysql_result($result,$num,"groupid");
			$deleted=Treat_DB_ProxyOld::mysql_result($result,$num,"deleted");
			$description=Treat_DB_ProxyOld::mysql_result($result,$num,"description");
			$unit=Treat_DB_ProxyOld::mysql_result($result,$num,"unit");

			$query2 = "
				INSERT INTO menu_items (
					businessid,
					companyid,
					item_name,
					price,
					parent,
					deleted,
					description,
					groupid,
					unit
				) VALUES (
					'$copyto',
					'$companyid',
					'$item_name',
					'$price',
					'$menu_item_id',
					'$deleted',
					'$description',
					'$groupid',
					'$unit'
				)
			";
			$result2 = Treat_DB_ProxyOld::query($query2);

			$num--;
		}
		echo "<li>Corporate Catering Menu Copied<br>";
	}
////////////////////////////////////////////////////////////////////////////////////////////////////
	if ($menu == 1 && $copymenu != 0) {
		///find catering menu/
		$query = "SELECT default_menu FROM business WHERE businessid = '$copymenu'";
		$result = Treat_DB_ProxyOld::query($query);

		$default_menu = Treat_DB_ProxyOld::mysql_result($result,0,"default_menu");

		$query = "SELECT * FROM menu_items WHERE businessid = '$copymenu' AND menu_typeid = '$default_menu'";
		$result = Treat_DB_ProxyOld::query($query);
		$num=Treat_DB_ProxyOld::mysql_numrows($result);

		$num--;
		while ($num >= 0){
			$menu_item_id=Treat_DB_ProxyOld::mysql_result($result,$num,"menu_item_id");
			$item_name=Treat_DB_ProxyOld::mysql_result($result,$num,"item_name");
			$price=Treat_DB_ProxyOld::mysql_result($result,$num,"price");
			$parent=Treat_DB_ProxyOld::mysql_result($result,$num,"parent");
			$groupid=Treat_DB_ProxyOld::mysql_result($result,$num,"groupid");
			$deleted=Treat_DB_ProxyOld::mysql_result($result,$num,"deleted");
			$description=Treat_DB_ProxyOld::mysql_result($result,$num,"description");
			$unit=Treat_DB_ProxyOld::mysql_result($result,$num,"unit");
			$menu_typeid=Treat_DB_ProxyOld::mysql_result($result,$num,"menu_typeid");
			$image=Treat_DB_ProxyOld::mysql_result($result,$num,"image");

			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query2 = "
				INSERT INTO menu_items (
					businessid,
					companyid,
					item_name,
					price,
					parent,
					deleted,
					description,
					groupid,
					unit,
					menu_typeid,
					image
				) VALUES (
					'$copyto',
					'$companyid',
					'$item_name',
					'$price',
					'$parent',
					'$deleted',
					'$description',
					'$groupid',
					'$unit',
					'$menu_typeid',
					'$image'
				)
			";
			$result2 = Treat_DB_ProxyOld::query($query2);
			$newmenu_itemid = Treat_DB_ProxyOld::mysql_insert_id();

			//////find option groups
			$query3 = "SELECT * FROM optiongroup WHERE menu_item_id = '$menu_item_id'";
			$result3 = Treat_DB_ProxyOld::query($query3);

			while($r3 = Treat_DB_ProxyOld::mysql_fetch_array($result3)){
				$optiongroupid = $r3["optiongroupid"];
				$description = $r3["description"];
				$type = $r3["type"];
				$orderid = $r3["orderid"];

				$query4 = "
					INSERT INTO optiongroup (
						menu_item_id,
						description,
						type,
						orderid
					) VALUES (
						'$newmenu_itemid',
						'$description',
						'$type',
						'$orderid'
					)
				";
				$result4 = Treat_DB_ProxyOld::query($query4);
				$newoptiongroupid = Treat_DB_ProxyOld::mysql_insert_id();

				/////option details
				$query4 = "SELECT * FROM options WHERE optiongroupid = '$optiongroupid'";
				$result4 = Treat_DB_ProxyOld::query($query4);

				while($r4 = Treat_DB_ProxyOld::mysql_fetch_array($result4)){
					$description = $r4["description"];
					$price = $r4["price"];
					$orderid = $r4["orderid"];
					$noprint = $r4["noprint"];

					$query5 = "
						INSERT INTO options (
							optiongroupid,
							description,
							price,
							orderid,
							noprint
						) VALUES (
							'$newoptiongroupid',
							'$description',
							'$price',
							'$orderid',
							'$noprint'
						)
					";
					$result5 = Treat_DB_ProxyOld::query($query5);
				}
			}

			$num--;
		}

		$query = "SELECT businessname FROM business WHERE businessid = '$copymenu'";
		$result = Treat_DB_ProxyOld::query($query);

		$newbusinessname=Treat_DB_ProxyOld::mysql_result($result,0,"businessname");

		echo "<li>Catering Menu from $newbusinessname copied.<br><p>";
	}

////////////////////////////////////////////////////////////////////////////////////////////////////
	if ($pos_menu == 1 && $copyposmenu != 0) {
		////clear out menu
		$query = "DELETE FROM menu_items_new WHERE businessid = $copyto";
		$result = Treat_DB_ProxyOld::query($query);

		$query = "DELETE FROM menu_groups_new WHERE businessid = $copyto";
		$result = Treat_DB_ProxyOld::query($query);

		////groups
		$query = "SELECT * FROM menu_groups_new WHERE businessid = '$copyposmenu'";
		$result = Treat_DB_ProxyOld::query($query);

		while ($r = Treat_DB_ProxyOld::mysql_fetch_array($result)){
			$group_id = $r["id"];
			$pos_id = $r["pos_id"];
			$parent_id = $r["parent_id"];
			$name = $r["name"];
			$group_type = $r["group_type"];

			if($parent_id == ""){$parent_id = "NULL";}
			if($group_type == ""){$group_type = "NULL";}
			
			$uuid = \EE\Model\Menu\GroupsNew::genUuid();

			$query2 = "
				INSERT INTO menu_groups_new (
					businessid,
					pos_id,
					parent_id,
					name,
					group_type,
					uuid_menu_groups_new
				) VALUES (
					$copyto,
					$pos_id,
					$parent_id,
					'$name',
					$group_type,
					'$uuid'
				)
			";
			$result2 = Treat_DB_ProxyOld::query($query2);
			$group[$group_id] = Treat_DB_ProxyOld::mysql_insert_id();
		}

		////items
		$query = "SELECT * FROM menu_items_new WHERE businessid = '$copyposmenu'";
		$result = Treat_DB_ProxyOld::query($query);

		while ($r = Treat_DB_ProxyOld::mysql_fetch_array($result)) {
			$id = $r["id"];
			$pos_id = $r["pos_id"];
			$group_id = $r["group_id"];
			$name = $r["name"];
			$ordertype = $r["ordertype"];
			$active = $r["active"];
			$item_code = $r["item_code"];
			$upc = $r["upc"];
			$reorder = $r["reorder"];
			$max = $r["max"];
			$reorder_point = $r["reorder_point"];
			$reorder_amount = $r["reorder_amount"];
			$is_button = $r["is_button"];

			$uuid = \EE\Model\Menu\ItemNew::genUuid();
			$query2 = "
				INSERT INTO menu_items_new (
					businessid,
					pos_id,
					group_id,
					name,
					ordertype,
					active,
					item_code,
					upc,
					reorder,
					max,
					reorder_point,
					reorder_amount,
					is_button,
					uuid_menu_items_new
				) VALUES (
					$copyto,
					$pos_id,
					'$group[$group_id]',
					'$name',
					$ordertype,
					1,
					'$item_code',
					'$upc',
					'$reorder',
					'$max',
					'$reorder_point',
					'$reorder_amount',
					'$is_button',
					'$uuid'
				)
			";
			$result2 = Treat_DB_ProxyOld::query($query2);
			$new_id = Treat_DB_ProxyOld::mysql_insert_id();

			///price
			$query2 = "SELECT * FROM menu_items_price WHERE menu_item_id = $id";
			$result2 = Treat_DB_ProxyOld::query($query2);

			while ($r2 = Treat_DB_ProxyOld::mysql_fetch_array($result2)) {
				$pos_price_id = $r2["pos_price_id"];
				$price = $r2["price"];
				$cost = $r2["cost"];

				$query3 = "
					INSERT INTO menu_items_price (
						businessid,
						menu_item_id,
						pos_price_id,
						price,
						new_price,
						cost
					) VALUES (
						$copyto,
						$new_id,
						$pos_price_id,
						'$price',
						NULL,
						'$cost'
					)
				";
				$result3 = Treat_DB_ProxyOld::query($query3);
			}
		}

		echo "<li>POS Menu copied.<br><p>";
	}
////////////////////////////////////////////////////////////////////////////////////////////////////
	if ($inventory == 1 && $copyinventory != 0) {
		$query = "SELECT * FROM inv_items WHERE businessid = '$copyinventory'";
		$result = Treat_DB_ProxyOld::query($query);
		$num=Treat_DB_ProxyOld::mysql_numrows($result);

		$num--;
		while ($num>=0){
			$item_name=Treat_DB_ProxyOld::mysql_result($result,$num,"item_name");
			$item_code=Treat_DB_ProxyOld::mysql_result($result,$num,"item_code");
			$price=Treat_DB_ProxyOld::mysql_result($result,$num,"price");
			$units=Treat_DB_ProxyOld::mysql_result($result,$num,"units");
			$apaccountid=Treat_DB_ProxyOld::mysql_result($result,$num,"apaccountid");
			$vendor=Treat_DB_ProxyOld::mysql_result($result,$num,"vendor");
			$price_date=Treat_DB_ProxyOld::mysql_result($result,$num,"price_date");
			$pack_size=Treat_DB_ProxyOld::mysql_result($result,$num,"pack_size");
			$pack_qty=Treat_DB_ProxyOld::mysql_result($result,$num,"pack_qty");

			$rec_num=Treat_DB_ProxyOld::mysql_result($result,$num,"rec_num");
			$rec_size=Treat_DB_ProxyOld::mysql_result($result,$num,"rec_size");
			$rec_num2=Treat_DB_ProxyOld::mysql_result($result,$num,"rec_num2");
			$rec_size2=Treat_DB_ProxyOld::mysql_result($result,$num,"rec_size2");
			$order_size=Treat_DB_ProxyOld::mysql_result($result,$num,"order_size");
			$each_size=Treat_DB_ProxyOld::mysql_result($result,$num,"each_size");

			//////Find corresponding vendor for copyto unit
			$query3 = "SELECT master FROM vendors WHERE vendorid = '$vendor'";
			$result3 = Treat_DB_ProxyOld::query($query3);
			$num3 = Treat_DB_ProxyOld::mysql_numrows($result3);

			$vendorid = 0;
			if ($num3 != 0){
				$master=Treat_DB_ProxyOld::mysql_result($result3,0,"master");

				$query3 = "SELECT vendorid FROM vendors WHERE master = '$vendor' AND businessid = '$copyto'";
				$result3 = Treat_DB_ProxyOld::query($query3);
				$num3=Treat_DB_ProxyOld::mysql_numrows($result3);

				if ($num3 != 0) {
					$vendorid=Treat_DB_ProxyOld::mysql_result($result3,0,"vendorid");
				}
			}

			//////Find corresponding GL account
			$query3 = "SELECT accountnum FROM acct_payable WHERE acct_payableid = '$apaccountid'";
			$result3 = Treat_DB_ProxyOld::query($query3);
			$num3 = Treat_DB_ProxyOld::mysql_numrows($result3);

			$apaccountid = 0;
			if ($num3 != 0) {
				$accountnum = Treat_DB_ProxyOld::mysql_result($result3,0,"accountnum"); 
				$accountnum = substr($accountnum,0,5);

				$query3 = "SELECT acct_payableid FROM acct_payable WHERE accountnum LIKE '$accountnum%' AND businessid = '$copyto'";
				$result3 = Treat_DB_ProxyOld::query($query3);
				$num3 = Treat_DB_ProxyOld::mysql_numrows($result3);

				if ($num3 != 0){
					$apaccountid = Treat_DB_ProxyOld::mysql_result($result3,0,"acct_payableid");
				}
			}

			$query2 = "SELECT * FROM inv_items WHERE item_code LIKE '$item_code' AND businessid = '$copyto'";
			$result2 = Treat_DB_ProxyOld::query($query2);
			$num2 = Treat_DB_ProxyOld::mysql_numrows($result2);

			if ($num2 == 0) {
				$query2 = "
					INSERT INTO inv_items (
						businessid,
						companyid,
						item_name,
						item_code,
						price,
						units,
						apaccountid,
						vendor,
						price_date,
						pack_size,
						pack_qty,
						rec_num,
						rec_size,
						rec_num2,
						rec_size2,
						order_size,
						each_size
					) VALUES (
						'$copyto',
						'$companyid',
						'$item_name',
						'$item_code',
						'$price',
						'$units',
						'$apaccountid',
						'$vendor',
						'$price_date',
						'$pack_size',
						'$pack_qty',
						'$rec_num',
						'$rec_size',
						'$rec_num2',
						'$rec_size2',
						'$order_size',
						'$each_size'
					)
				";
				$result2 = Treat_DB_ProxyOld::query($query2);
			}

			$num--;
		}

		$query = "SELECT businessname FROM business WHERE businessid = '$copyinventory'";
		$result = Treat_DB_ProxyOld::query($query);

		$newbusinessname = Treat_DB_ProxyOld::mysql_result($result,0,"businessname");

		echo "<li>Inventory Items from $newbusinessname copied.<br><p>";
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////
?>
	<p>
		<br>
		<center>
			Please double check all details for
			<a href="busdetail.php?cid=<?php echo $companyid; ?>&bid=<?php echo $copyto; ?>">
				<?php echo $businessname2; ?>
			</a>.
		</center>
		<br>
		<form action="businesstrack.php" method="POST">
		<input type="hidden" name="username" value="<?php echo $user; ?>">
		<input type="hidden" name="password" value="<?php echo $pass; ?>">
		<center>
			<input type="submit" value=" Return to Main Page">
		</center>
		</form>
	</p>
<?php
}
?>