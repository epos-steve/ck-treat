<head>
<script type=text/javascript>
window.onload = function(){
	var inputs = document.getElementsByTagName('input');

	for(i = 0; i < inputs.length; i++){
		var input = inputs[i];
		if(input.type == 'text') input.onkeydown = inputListener;
	}

        var selects = document.getElementsByTagName('select');

	for(i = 0; i < selects.length; i++){
		var select = selects[i];
		select.onkeydown = inputListener;
                select.onkeypress = function(event){var ev = event || window.event; if(ev.keyCode == 37 || ev.keyCode == 39) return false;}
	}
}

var inputListener = function(e){
	var ev = e || window.event;
	var key = ev.keyCode;
	var UP = 38;
	var DOWN = 40;
	var LEFT = 37;
	var RIGHT = 39;
	
	var td = this.parentNode;
	var tr = td.parentNode;

	var row = tr.rowIndex;
	var col = td.cellIndex;
	var tbody = tr.parentNode;
        var table = tbody.parentNode;

	if(key == UP){
           if(this.nodeName != 'SELECT'){
		var new_row = row - 1;
		if(new_row > -1){
			var next_row = table.rows[new_row];
			var target = next_row.cells[col];
			target.childNodes[0].focus();			
		}
           }
	}
	else if(key == DOWN){
           if(this.nodeName != 'SELECT'){
		var new_row = row + 1;
		if(new_row < table.rows.length){
			var next_row = table.rows[new_row];
			var target = next_row.cells[col];
			target.childNodes[0].focus();			
		}
           }
	}
	else if(key == LEFT){
		var new_col = col - 1;
		if(new_col > -1){
			var target = table.rows[row].cells[new_col];
			target.childNodes[0].focus();
                        return false;
		}
	}
	else if(key == RIGHT){
		var new_col = col + 1;
		if(new_col < table.rows[row].cells.length){
			var target = table.rows[row].cells[new_col];
			target.childNodes[0].focus();
                        return false;
		}
	}
}
</script>
</head>
<body>
<?PHP

echo "<table border=1><tbody>";

for ($counter=1;$counter<=1000;$counter++){

  $counter2=$counter+1000;
  $counter3=$counter+2000;
  $counter4=$counter+3000;

  echo "<tr><td><input type=text size=3 name=$counter></td><td><select name=myselect><option value=a1>1</option><option value=a2>2</option><option value=a3>3</option></select></td><td><input type=text size=3 name=$counter2></td><td><input type=text size=3 name=$counter3></td><td><input type=text size=3 name=$counter4></td></tr>";
}
echo "</tbody></table></body>";