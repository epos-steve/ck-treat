<?php

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+4);
        if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
        elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
        $diff = substr($diff, 0, $dot+3);
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      elseif ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      elseif ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='32')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      $day=$day-31;  
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";} 
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}


if ( !defined('DOC_ROOT'))
{
	define('DOC_ROOT', dirname(dirname(__FILE__)));
}
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');
$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";


$user = isset($_COOKIE["usercook"])?$_COOKIE["usercook"]:'';
$pass = isset($_COOKIE["passcook"])?$_COOKIE["passcook"]:'';
$loginid = isset($_COOKIE["logincook"])?$_COOKIE["logincook"]:'';

mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = mysql_query($query);
$num=mysql_numrows($result);

if ($num!=0){
    $security_level=@mysql_result($result,0,"security_level");
    $bid=@mysql_result($result,0,"businessid");
    $cid=@mysql_result($result,0,"companyid");
    $loginid=@mysql_result($result,0,"loginid");
    $hide_expense_reports=@mysql_result($result,0,"hide_expense_reports");
}

if ($num != 1 || $user == "" || $pass == "") 
{
    echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
    echo "<body><table width=100% cellspacing=0 cellpadding=0>";    

	////expense reports
    if($hide_expense_reports == 1){$query35 = "SELECT apinvoice.* FROM apinvoice,purchase_card WHERE (apinvoice.dm = '-1' OR apinvoice.dm = '') AND apinvoice.transfer = '3' AND apinvoice.businessid = purchase_card.businessid AND apinvoice.vendor = purchase_card.loginid AND apinvoice.pc_type = purchase_card.type AND purchase_card.approve_loginid = '$loginid' AND purchase_card.is_deleted = 0 AND apinvoice.posted != '0' ORDER BY apinvoice.date, apinvoice.apinvoiceid";}
    else{$query35 = "SELECT apinvoice.* FROM apinvoice,purchase_card WHERE apinvoice.dm = '' AND apinvoice.transfer = '3' AND apinvoice.businessid = purchase_card.businessid AND apinvoice.vendor = purchase_card.loginid AND apinvoice.pc_type = purchase_card.type AND purchase_card.approve_loginid = '$loginid' AND purchase_card.is_deleted = 0 AND apinvoice.posted != '0' ORDER BY apinvoice.date, apinvoice.apinvoiceid";}
    $result35 = mysql_query($query35);
    $num35=mysql_numrows($result35); 
	$num34=$num35;

    while($r=mysql_fetch_array($result35)){
          $apinvoiceid=$r["apinvoiceid"];
          $er_date=$r["date"];
          $vendor=$r["vendor"];
          $pc_type=$r["pc_type"];
          $er_businessid=$r["businessid"];
          $total=$r["total"];
          $notes=$r["notes"];
          $interco_trans=$r["interco_trans"];

          if($pc_type==0){$query36 = "SELECT firstname,lastname FROM login WHERE loginid = '$vendor'";}
          else{$query36 = "SELECT firstname,lastname FROM login_route WHERE loginid = '$vendor'";}
          $result36 = mysql_query($query36);

          $firstname=@mysql_result($result36,0,"firstname");
          $lastname=@mysql_result($result36,0,"lastname");

          $query36 = "SELECT businessname FROM business WHERE businessid = '$er_businessid'";
          $result36 = mysql_query($query36);

          $er_busname=@mysql_result($result36,0,"businessname");

          $total=number_format($total,2);

          $notes=str_replace("\n","<br>",$notes);

          echo "<tr bgcolor=#CCFFCC><td width=25%>&nbsp;<img src=greendot.gif height=16 width=16 alt='Expense Report'> <b>$lastname, $firstname</td><td width=20%><b>$er_busname</td><td width=25%></td><td width=10%><b>$er_date</td><td align=right width=10%><b>$$total</td><td align=right width=10%><form action=dm_approve.php?apinvoiceid=$apinvoiceid&goto=10&bid=$businessid&cid=$companyid method=post style=\"margin:0;padding:0;display:inline;\"><input type=submit value='Approve' style=\"border: 1px solid #CCCCCC; font-size: 12px; background-color:#FFFF99;\" onMouseOver=this.style.color='#FF9900';this.style.cursor='hand' onMouseOut=this.style.color='#000000'></form></td></tr>";
          echo "<tr><td></td><td colspan=4><i>$notes</td><td></td></tr>";
          echo "<tr height=1><td></td><td colspan=4 bgcolor=#E8E7E7></td></tr>";

          $query36 = "SELECT * FROM apinvoicedetail WHERE apinvoiceid = '$apinvoiceid' ORDER BY itemid";
          $result36 = mysql_query($query36);

          while($r2=mysql_fetch_array($result36)){
             $pcard=$r2["pcard"];
             $amount=$r2["amount"];
             $apaccountid=$r2["apaccountid"];
             $item=$r2["item"];

             $query37 = "SELECT * FROM acct_payable WHERE acct_payableid = '$apaccountid'";
             $result37 = mysql_query($query37);

             $acct_name=@mysql_result($result37,0,"acct_name");
             $accountnum=@mysql_result($result37,0,"accountnum");

             $accountnum=substr($accountnum,0,5);

             if($pcard==1){$showicon="<img src=credit.gif height=25 width=34 align=bottom alt='PCard'>";}
             else{$showicon="<img src=cash.gif height=25 width=26 align=bottom alt='Petty Cash'>";}

             $amount=number_format($amount,2);

             echo "<tr valign=top><td></td><td>$accountnum $acct_name</td><td>$item</td><td align=right>$showicon</td><td align=right>$$amount</td><td></td></tr>";
             echo "<tr height=1><td></td><td colspan=4 bgcolor=#E8E7E7></td></tr>";
          }

          if($interco_trans==-2){
             echo "<tr><td></td><td bgcolor=yellow colspan=4>*Expense Report funds were transferred to other accounts.</td></tr>";
             echo "<tr height=1><td></td><td colspan=4 bgcolor=#E8E7E7></td></tr>";
          }
    }
	
	///////purhcase orders
	$query35 = "SELECT apinvoice.* FROM apinvoice,business WHERE apinvoice.dm = '' AND apinvoice.transfer = '4' AND apinvoice.posted = '2' AND apinvoice.businessid = business.businessid AND business.po_approval = '$loginid'";
    $result35 = mysql_query($query35); 
    $num35=mysql_numrows($result35);

	/*
    if($num36==0){
       echo "<a href=businesstrack.php style=$style target='_parent'>Close</a>";
    } 
	*/	

    while($r=mysql_fetch_array($result35)){
          $apinvoiceid=$r["apinvoiceid"];
          $er_date=$r["date"];
          $vendor=$r["vendor"];
          $pc_type=$r["pc_type"];
          $er_businessid=$r["businessid"];
		  $er_companyid=$r["companyid"];
          $total=$r["total"];
          $notes=$r["notes"];
          $submitby=$r["submitby"];

          $query36 = "SELECT businessname,unit FROM business WHERE businessid = '$er_businessid'";
          $result36 = mysql_query($query36);

          $er_busname=@mysql_result($result36,0,"businessname");
		  $bus_unit=@mysql_result($result36,0,"unit");
		  
		  $query36 = "SELECT name FROM vendors WHERE vendorid = '$vendor'";
          $result36 = mysql_query($query36);

          $vendor_name=@mysql_result($result36,0,"name");

          $total=number_format($total,2);

          $notes=str_replace("\n","<br>",$notes);

          echo "<tr bgcolor=#CCCCFF><td width=25%>&nbsp;<img src=bluedot.gif height=16 width=16 alt='Purchase Order'> <b>$vendor_name</td><td width=20%><b>$er_busname</td><td width=25%></td><td width=10%><b>$er_date</td><td align=right width=10%><b>$$total</td><td align=right width=10%><form action=po_reject.php?apinvoiceid=$apinvoiceid&goto=10&bid=$businessid&cid=$companyid&transfer=4 method=post style=\"margin:0;padding:0;display:inline;\"><input type=submit value='Reject' onclick=\"return confirm('Are you sure you want to reject this Purchase Order?');\" style=\"border: 1px solid #CCCCCC; font-size: 12px; background-color:#FFFF99;\" onMouseOver=this.style.color='#FF9900';this.style.cursor='hand' onMouseOut=this.style.color='#000000'></form> <form action=dm_approve.php?apinvoiceid=$apinvoiceid&goto=10&bid=$businessid&cid=$companyid&transfer=4 method=post style=\"margin:0;padding:0;display:inline;\"><input type=submit value='Approve' style=\"border: 1px solid #CCCCCC; font-size: 12px; background-color:#FFFF99;\" onMouseOver=this.style.color='#FF9900';this.style.cursor='hand' onMouseOut=this.style.color='#000000'></form></td></tr>";
          echo "<tr><td></td><td colspan=4><i>$notes</td><td></td></tr>";
          echo "<tr height=1><td></td><td colspan=4 bgcolor=#E8E7E7></td></tr>";
		  
		  /////warnings
		  ///////budget
			$query72 = "SELECT DISTINCT date FROM apinvoicedetail WHERE apinvoiceid = '$apinvoiceid' ORDER BY date";
			$result72 = mysql_query($query72);
		
			$lastdate="0000-00-00";
			while($r72=mysql_fetch_array($result72)){
		
			$finddate=$r72["date"];
		
			if(substr($lastdate,5,2) != substr($finddate,5,2)){
		
			$budget_year=substr($finddate,0,4);
			$budget_month=substr($finddate,5,2);
			$b_date1="$budget_year-$budget_month-01";
			$b_date2="$budget_year-$budget_month-31";
		
			if($budget_month==1){$monthname="January";}
			elseif($budget_month==2){$monthname="February";}
			elseif($budget_month==3){$monthname="March";}
			elseif($budget_month==4){$monthname="April";}
			elseif($budget_month==5){$monthname="May";}
			elseif($budget_month==6){$monthname="June";}
			elseif($budget_month==7){$monthname="July";}
			elseif($budget_month==8){$monthname="August";}
			elseif($budget_month==9){$monthname="September";}
			elseif($budget_month==10){$monthname="October";}
			elseif($budget_month==11){$monthname="November";}
			elseif($budget_month==12){$monthname="December";}
		
			$query1 = "SELECT * FROM po_category ORDER BY category_name";
			$result1 = mysql_query($query1);
	
			while($r99=mysql_fetch_array($result1)){
				$categoryid = $r99["categoryid"];
				$category_name = $r99["category_name"];
				$budget_type = $r99["budget_type"];
				
				/////////if category exists for this purchase order
				$query2 = "SELECT categoryid FROM apinvoicedetail WHERE apinvoiceid = '$apinvoiceid' AND categoryid = '$categoryid' AND date >= '$b_date1' AND date <= '$b_date2'";
				$result2 = mysql_query($query2);
				$num_cat=mysql_numrows($result2);
		
				if($num_cat>0){
				//////totals for month
				$b_date1="$budget_year-$budget_month-01";
				$b_date2="$budget_year-$budget_month-31";
				$query2 = "SELECT SUM(apinvoicedetail.amount) AS totalamt FROM apinvoice,apinvoicedetail WHERE apinvoice.businessid = '$er_businessid' AND apinvoice.transfer = '4' AND (apinvoice.posted = '1' OR apinvoice.apinvoiceid = '$apinvoiceid') AND apinvoice.apinvoiceid = apinvoicedetail.apinvoiceid AND apinvoicedetail.categoryid = '$categoryid' AND apinvoicedetail.date >= '$b_date1' AND apinvoicedetail.date <= '$b_date2'";
				$result2 = mysql_query($query2);
		
				$totalamt=@mysql_result($result2,0,"totalamt");
		
				if(strlen($default_month)<2){$default_month="0$default_month";}
				$query2 = "SELECT `$budget_month` AS budgetamt FROM budget WHERE businessid = '$bus_unit' AND companyid = '$er_companyid' AND type = '$budget_type' AND year = '$budget_year'";
				$result2 = mysql_query($query2);
		
				$budgetamt=@mysql_result($result2,0,"budgetamt");
		
				if($budgetamt<$totalamt){$shownumber=number_format($totalamt-$budgetamt,2); $shownumber="<font color=red>+$$shownumber</font>"; echo "<tr><td></td><td colspan=4 style=\"border:2px solid red;\" bgcolor=white>&nbsp;<img src=restrict.gif height=16 width=16> <b>This Purchase Order will exceed the $monthname $category_name budget! $shownumber</b></td><td></td></tr><tr height=6><td colspan=5></td></tr>";}
				elseif($totalamt==$budgetamt){echo "<tr><td></td><td colspan=4 style=\"border:2px solid red;\" bgcolor=white>&nbsp;<img src=error.gif height=16 width=20> <b>This Purchase Order will exhaust the $monthname $category_name budget!</b></td><td></td></tr><tr height=6><td colspan=5></td></tr>";}
				elseif($totalamt>=($budgetamt-($budgetamt*.05))){$shownumber=number_format($budgetamt-$totalamt,2); $shownumber="<font color=green>$$shownumber</font>"; echo "<tr><td></td><td colspan=4 style=\"border:2px solid yellow;\" bgcolor=white>&nbsp;<img src=error.gif height=16 width=20> <b>This Purchase Order will put you within 5% of the $monthname $category_name budget! $shownumber left.</b></td><td></td></tr><tr height=6><td colspan=5></td></tr>";}
				elseif($totalamt>=($budgetamt-($budgetamt*.1))){$shownumber=number_format($budgetamt-$totalamt,2); $shownumber="<font color=green>$$shownumber</font>"; echo "<tr><td></td><td colspan=4 style=\"border:2px solid yellow;\" bgcolor=white>&nbsp;<img src=error.gif height=16 width=20> <b>This Purchase Order will put you within 10% of the $monthname $category_name budget! $shownumber left.</b></td><td></td></tr><tr height=6><td colspan=5></td></tr>";}
				
				//////totals for year
				$b_date1="$budget_year-01-01";
				$b_date2="$budget_year-12-31";
				$query2 = "SELECT SUM(apinvoicedetail.amount) AS totalamt FROM apinvoice,apinvoicedetail WHERE apinvoice.businessid = '$er_businessid' AND apinvoice.transfer = '4' AND (apinvoice.posted = '1' OR apinvoice.apinvoiceid = '$apinvoiceid') AND apinvoice.apinvoiceid = apinvoicedetail.apinvoiceid AND apinvoicedetail.categoryid = '$categoryid' AND apinvoicedetail.date >= '$b_date1' AND apinvoicedetail.date <= '$b_date2'";
				$result2 = mysql_query($query2);
		
				$totalamt=@mysql_result($result2,0,"totalamt");
		
				$query2 = "SELECT (`01`+`02`+`03`+`04`+`05`+`06`+`07`+`08`+`09`+`10`+`11`+`12`) AS budgetamt FROM budget WHERE businessid = '$bus_unit' AND companyid = '$er_companyid' AND type = '$budget_type' AND year = '$budget_year'";
				$result2 = mysql_query($query2);
		
				$budgetamt=@mysql_result($result2,0,"budgetamt");
			
				if($budgetamt<$totalamt){$shownumber=number_format($totalamt-$budgetamt,2); $shownumber="<font color=red>+$$shownumber</font>"; echo "<tr><td></td><td colspan=4 style=\"border:2px solid red;\" bgcolor=#FFFF99>&nbsp;<img src=restrict.gif height=16 width=16> <b>This Purchase Order will exceed the $budget_year $category_name budget! $shownumber</b></td><td></td></tr><tr height=10><td colspan=7></td></tr>";}
				elseif($totalamt==$budgetamt){echo "<tr><td></td><td colspan=4 style=\"border:2px solid red;\" bgcolor=#FFFF99>&nbsp;<img src=error.gif height=16 width=20> <b>This Purchase Order will exhaust the $budget_year $category_name budget!</b></td><td></td></tr><tr height=10><td colspan=7></td></tr>";}
				elseif($totalamt>=($budgetamt-($budgetamt*.05))){$shownumber=number_format($budgetamt-$totalamt,2); $shownumber="<font color=green>$$shownumber</font>"; echo "<tr><td></td><td colspan=4 style=\"border:2px solid yellow;\" bgcolor=#FFFF99>&nbsp;<img src=error.gif height=16 width=20> <b>This Purchase Order will put you within 5% of the $budget_year $category_name budget! $shownumber left.</b></td><td></td></tr><tr height=10><td colspan=7></td></tr>";}
				elseif($totalamt>=($budgetamt-($budgetamt*.1))){$shownumber=number_format($budgetamt-$totalamt,2); $shownumber="<font color=green>$$shownumber</font>"; echo "<tr><td></td><td colspan=4 style=\"border:2px solid yellow;\" bgcolor=#FFFF99>&nbsp;<img src=error.gif height=16 width=20> <b>This Purchase Order will put you within 10% of the $budget_year $category_name budget! $shownumber left.</b></td><td></td></tr><tr height=10><td colspan=7></td></tr>";}
				}
			}
			}
			$lastdate=$finddate;
			}
		
		  /////end warnings

          $query36 = "SELECT * FROM apinvoicedetail WHERE apinvoiceid = '$apinvoiceid' ORDER BY date,itemid";
          $result36 = mysql_query($query36);

          while($r2=mysql_fetch_array($result36)){
             $categoryid=$r2["categoryid"];
             $amount=$r2["amount"];
             $apaccountid=$r2["apaccountid"];
             $item=$r2["item"];
			 $date=$r2["date"];

             $query37 = "SELECT * FROM acct_payable WHERE acct_payableid = '$apaccountid'";
             $result37 = mysql_query($query37);

             $acct_name=@mysql_result($result37,0,"acct_name");
             $accountnum=@mysql_result($result37,0,"accountnum");

             $accountnum=substr($accountnum,0,5);

			 $query37 = "SELECT * FROM po_category WHERE categoryid = '$categoryid'";
             $result37 = mysql_query($query37);

             $cat_name=@mysql_result($result37,0,"category_name");

             $amount=number_format($amount,2);

             echo "<tr valign=top><td></td><td>$cat_name</td><td>$item</td><td>$date</td><td align=right>$$amount</td><td></td></tr>";
             echo "<tr height=1><td></td><td colspan=4 bgcolor=#E8E7E7></td></tr>";
          }
		  
		  $query36 = "SELECT message FROM po_note WHERE apinvoiceid = '$apinvoiceid'";
          $result36 = mysql_query($query36);

		  while($r36=mysql_fetch_array($result36)){
			$message=$r36["message"];
			$message=str_replace("\n","<br>",$message);
			
			echo "<tr><td></td><td bgcolor=#E8E7E7 colspan=4><ul><li>$message</td></tr>";
			echo "<tr height=1><td></td><td colspan=4 bgcolor=#E8E7E7></td></tr>";
		  }
		  
		  echo "<tr><td></td><td bgcolor=yellow colspan=4>*Purchase Order Submitted by $submitby.</td></tr>";
          echo "<tr height=1><td></td><td colspan=4 bgcolor=#E8E7E7></td></tr>";
    }
	
	/////////purchase order receiving
	$query35 = "SELECT apinvoice.* FROM apinvoice,business WHERE apinvoice.dm != '' AND apinvoice.dm != '-1' AND apinvoice.transfer = '4' AND apinvoice.posted = '1' AND apinvoice.pc_type = '0' AND apinvoice.businessid = business.businessid AND business.po_notify_email = '$loginid' ORDER BY apinvoice.businessid,apinvoice.invoicenum";
    $result35 = mysql_query($query35); 
    $num37=mysql_numrows($result35);
	
	//////export purchase orders
	if($num37>0){
		echo "<tr height=1><td colspan=5><a href=po_export.php style=$style><font color=blue>Export Purchase Orders</font></a></td></tr>";
	}
	
	while($r=mysql_fetch_array($result35)){
          $apinvoiceid=$r["apinvoiceid"];
		  $invoicenum=$r["invoicenum"];
          $er_date=$r["date"];
          $vendor=$r["vendor"];
          $pc_type=$r["pc_type"];
          $er_businessid=$r["businessid"];
		  $er_companyid=$r["companyid"];
          $total=$r["total"];
          $notes=$r["notes"];
          $submitby=$r["submitby"];

          $query36 = "SELECT businessname,unit FROM business WHERE businessid = '$er_businessid'";
          $result36 = mysql_query($query36);

          $er_busname=@mysql_result($result36,0,"businessname");
		  $bus_unit=@mysql_result($result36,0,"unit");
		  
		  $query36 = "SELECT name FROM vendors WHERE vendorid = '$vendor'";
          $result36 = mysql_query($query36);

          $vendor_name=@mysql_result($result36,0,"name");

          $total=number_format($total,2);

          $notes=str_replace("\n","<br>",$notes);

          echo "<tr bgcolor=#FFCCCC><td width=25%>&nbsp;<img src=reddot.gif height=16 width=16 alt='Purchase Order'> <b>$vendor_name</td><td width=45% colspan=2><b>PO# $invoicenum ($er_busname)</td><td width=10%><b>$er_date</td><td align=right width=10%><b>$$total</td><td align=right width=10%><form action=dm_approve.php?apinvoiceid=$apinvoiceid&goto=10&bid=$businessid&cid=$companyid&transfer=4b method=post style=\"margin:0;padding:0;display:inline;\"><input type=submit value='Receive' style=\"border: 1px solid #CCCCCC; font-size: 12px; background-color:#FFFF99;\" onMouseOver=this.style.color='#FF9900';this.style.cursor='hand' onMouseOut=this.style.color='#000000'></form></td></tr>";
          echo "<tr><td></td><td colspan=4><i>$notes</td><td></td></tr>";
          echo "<tr height=1><td></td><td colspan=4 bgcolor=#E8E7E7></td></tr>";

          $query36 = "SELECT * FROM apinvoicedetail WHERE apinvoiceid = '$apinvoiceid' ORDER BY date,itemid";
          $result36 = mysql_query($query36);

          while($r2=mysql_fetch_array($result36)){
             $categoryid=$r2["categoryid"];
             $amount=$r2["amount"];
             $apaccountid=$r2["apaccountid"];
             $item=$r2["item"];
			 $date=$r2["date"];

             $query37 = "SELECT * FROM acct_payable WHERE acct_payableid = '$apaccountid'";
             $result37 = mysql_query($query37);

             $acct_name=@mysql_result($result37,0,"acct_name");
             $accountnum=@mysql_result($result37,0,"accountnum");

             $accountnum=substr($accountnum,0,5);

			 $query37 = "SELECT * FROM po_category WHERE categoryid = '$categoryid'";
             $result37 = mysql_query($query37);

             $cat_name=@mysql_result($result37,0,"category_name");

             $amount=number_format($amount,2);
			 
			 $item=str_replace("<i>","",$item);
			 $item=str_replace("</i>","",$item);

             echo "<tr valign=top><td></td><td>$cat_name</td><td>$item</td><td>$date</td><td align=right>$$amount</td><td></td></tr>";
             echo "<tr height=1><td></td><td colspan=4 bgcolor=#E8E7E7></td></tr>";
          }
		  
		  //echo "<tr><td></td><td bgcolor=yellow colspan=4>*Purchase Order Submitted by $submitby.</td></tr>";
          echo "<tr height=1><td></td><td colspan=4 bgcolor=#E8E7E7></td></tr>";
    }
	
    if(($num35==0&&$num34==0&&$num37==0) || $hide_expense_reports == 1){
       echo "<a href=businesstrack.php style=$style target='_parent'>Close</a>";
    }

    echo "</table></body>";

}
mysql_close();
?>
