<?php
if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
include_once( 'db.php' );
require_once( DOC_ROOT . '/bootstrap.php' );

$passed_cookie_login = require_once( realpath( DOC_ROOT . '/../lib/inc/cookie-login.php' ) );
$login_user = Treat_Model_User_Login::login( $GLOBALS['user'], $GLOBALS['pass'] );
$page_business = Treat_Model_Business_Singleton::getSingleton();

if (
	(
		$passed_cookie_login
		&& (
			(
				$GLOBALS['security_level'] == 1
				&& $GLOBALS['bid'] == $page_business->getBusinessId()
				&& $GLOBALS['cid'] == $page_business->getCompanyId()
			)
			|| (
				$GLOBALS['security_level'] > 1
				&& $GLOBALS['cid'] == $page_business->getCompanyId()
			)
		)
	)
	|| (
		$login_user
		&& (
			(
				1 == $login_user->security_level
				&& $login_user->getBusinessId() == $page_business->getBusinessId()
				&& $login_user->getCompanyId() == $page_business->getCompanyId()
			)
			|| (
				$login_user->security_level > 1
				&& $login_user->getCompanyId() == $page_business->getCompanyId()
			)
		)
	)
) {
	$p = new Treat_ArrayObject( $_POST );

/*
$style = "text-decoration:none";

$user=$_POST['username'];
$pass=$_POST['password'];
$businessid=$_POST['businessid'];
$companyid=$_POST['companyid'];
$accountid=$_POST['accountid'];
$accountname=$_POST['accountname'];
$accountnum=$_POST['accountnum'];
$accountattn=$_POST['accountattn'];
$accountstreet=$_POST['accountstreet'];
$accountcity=$_POST['accountcity'];
$accountstate=$_POST['accountstate'];
$accountzip=$_POST['accountzip'];

$b_mailing=$_POST['b_mailing'];
$b_accountstreet=$_POST['b_accountstreet'];
$b_accountcity=$_POST['b_accountcity'];
$b_accountstate=$_POST['b_accountstate'];
$b_accountzip=$_POST['b_accountzip'];

$accountphone=$_POST['accountphone'];
$accountfax=$_POST['accountfax'];
$accountparent=$_POST['acct_parent'];
$accountemail=$_POST['accountemail'];
$is_parent=$_POST['is_parent'];
$accounttaxid=$_POST['taxid'];
$costcenter=$_POST['costcenter'];
$salescode=$_POST['salescode'];
$menu=$_POST['menu'];
$localacct=$_POST['localacct'];
$assign=$_POST['assign'];
$acct_status=$_POST['acct_status'];
$acct_charge=$_POST['acct_charge'];
$acct_tax_rate=$_POST['tax_rate'];
$acct_charge2=$_POST['acct_charge2'];
$allow_sub=$_POST['allow_sub'];

mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = mysql_query($query);
$num=mysql_numrows($result);
mysql_close();

$location="busedit_account.php?bid=$businessid&cid=$companyid&aid=$accountid";
header('Location: ./' . $location);

if ($num != 1)
{
	echo "<center><h3>Failed</h3></center>";
}

else
{
	echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%><tr><td colspan=2>";
	echo "<img src=/ta/logo.jpg><p></td></tr>";

	if ( 'on' == $is_parent ) {
		$accountparent = -1;
	}
#	mysql_connect($dbhost,$username,$password);
#	@mysql_select_db($database) or die( "Unable to select database");
*/
	$tmp = new stdClass();
	$tmp->accountid       = $p['accountid'];
	$tmp->attn            = $p['accountattn'];
	$tmp->street          = $p['accountstreet'];
	$tmp->city            = $p['accountcity'];
	$tmp->state           = $p['accountstate'];
	$tmp->zip             = $p['accountzip'];
	$tmp->b_mailing       = ( 'on' == $p['b_mailing'] ? true : false );
	$tmp->b_street        = $p['b_accountstreet'];
	$tmp->b_city          = $p['b_accountcity'];
	$tmp->b_state         = $p['b_accountstate'];
	$tmp->b_zip           = $p['b_accountzip'];
	$tmp->phone           = $p['accountphone'];
	$tmp->fax             = $p['accountfax'];
	$tmp->email           = $p['accountemail'];
	$tmp->taxid           = $p['taxid'];
	$tmp->costcenter      = $p['costcenter'];
	$tmp->menu            = $p['menu'];
	$tmp->contract_num    = $p['localacct'];
	$tmp->assign          = $p['assign'];
	$tmp->status          = $p['acct_status'];
	$tmp->deliver_charge  = $p['acct_charge'];
	$tmp->deliver_charge2 = $p['acct_charge2'];
	$tmp->print_labels    = ( 'on' == $p['print_labels'] ? true : false );
	$tmp->tax_rate        = $p['tax_rate'];
	$tmp->allow_sub       = ( 'on' == $p['allow_sub'] ? true : false );
	$account = new Treat_Model_Account();
	$account->addSetFields( (array)$tmp );
#	echo '<pre>';
#	echo '$_POST = ';
#	var_dump( $_POST );
#	echo '$tmp = ';
#	var_dump( $tmp );
#	echo '$account = ';
#	var_dump( $account );
#	echo '</pre>';
	
	if ( $account->save() ) {
		$_SESSION->addFlashMessage( 'Account Successfully Saved.' );
	}
	else {
		$_SESSION->addFlashError( 'Error encountered while attempting to save Account.' );
	}
	
/*
	$query = "
		UPDATE accounts
		SET attn = '$accountattn'
			, street = '$accountstreet'
			, city = '$accountcity'
			, state = '$accountstate'
			, zip = '$accountzip'
			, b_mailing = '$b_mailing'
			, b_street = '$b_accountstreet'
			, b_city = '$b_accountcity'
			, b_state = '$b_accountstate'
			, b_zip = '$b_accountzip'
			, phone = '$accountphone'
			, fax = '$accountfax'
			, email = '$accountemail'
			, taxid = '$accounttaxid'
			, costcenter = '$costcenter'
			, menu = '$menu'
			, contract_num = '$localacct'
			, assign = '$assign'
			, status = '$acct_status'
			, deliver_charge = '$acct_charge'
			, deliver_charge2 = '$acct_charge2'
			, tax_rate = '$acct_tax_rate'
			, allow_sub = '$allow_sub'
		WHERE accountid = '$accountid'
	";
	$result = Treat_DB_ProxyOld::query( $query );
*/
#	mysql_close();

#	echo "</table></center>";

#	echo "<p><center><a style=$style href=busdetail.php?bid=$businessid&cid=$companyid><font color=blue>$businessname</font></a></center>";


	$location = sprintf(
		'/ta/busedit_account.php?bid=%d&cid=%d&aid=%d'
		,$_POST['businessid']
		,$_POST['companyid']
		,$_POST['accountid']
	);
	header( 'Location: ' . $location );
#}
}
?>