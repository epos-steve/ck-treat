<?php

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+3);
        if ($diff > 0 && $diff < '0.01'){$diff="0.01";}
        elseif($diff < 0 && $diff > '-0.01'){$diff="-0.01";}
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      elseif ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      elseif ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='32')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      $day=$day-31;  
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";} 
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}


if ( !defined('DOC_ROOT'))
{
	define('DOC_ROOT', dirname(dirname(__FILE__)));
}
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');
$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");
$creditamount="creditamount";
$creditidnty="creditidnty";
$creditdate="creditdate";
$debitamount="debitamount";
$debitidnty="debitidnty";
$debitdate="debitdate";
$quatertotal=0;
$monthtotal=0;
$total=0;
$counter=0;
$findme='.';
$double='.00';
$single='0';
$double2='00';

$user = isset($_COOKIE["usercook"])?$_COOKIE["usercook"]:'';
$pass = isset($_COOKIE["passcook"])?$_COOKIE["passcook"]:'';
$view = isset($_COOKIE["viewcook"])?$_COOKIE["viewcook"]:'';
$sorting = isset($_COOKIE["sortcook"])?$_COOKIE["sortcook"]:'';
$date1 = isset($_COOKIE["date1cook"])?$_COOKIE["date1cook"]:'';
$date2 = isset($_COOKIE["date2cook"])?$_COOKIE["date2cook"]:'';
$findaccount = isset($_COOKIE["acctcook"])?$_COOKIE["acctcook"]:'';
$aged = isset($_COOKIE["aged"])?$_COOKIE["aged"]:'';
$businessid = isset($_GET["bid"])?$_GET["bid"]:'';
$companyid = isset($_GET["cid"])?$_GET["cid"]:'';
$invoiceid = isset($_GET["invoiceid"])?$_GET["invoiceid"]:'';
$error = isset($_GET["error"])?$_GET["error"]:'';
$thisdate = isset($_GET["tender_date"])?$_GET["tender_date"]:'';

if ($businessid==""&&$companyid==""){
   $businessid = isset($_POST["businessid"])?$_POST["businessid"]:'';
   $companyid = isset($_POST["companyid"])?$_POST["companyid"]:'';
   $view = isset($_POST["view"])?$_POST["view"]:'';
   $date1 = isset($_POST["date1"])?$_POST["date1"]:'';
   $date2 = isset($_POST["date2"])?$_POST["date2"]:'';
   $findinv = isset($_POST["findinv"])?$_POST["findinv"]:'';
   $findamount = isset($_POST["findamount"])?$_POST["findamount"]:'';
   $findaccount = isset($_POST["findaccount"])?$_POST["findaccount"]:'';
   $sorting = isset($_POST["sorting"])?$_POST["sorting"]:'';
   $aged = isset($_POST["aged"])?$_POST["aged"]:'';
   $days = isset($_POST["days"])?$_POST["days"]:'';
}

if ($sorting=="submit"||$sorting=="unsubmit"||$sorting=="invoicenum"||$sorting=="master_vendor_id"){$sorting="invoiceid";}

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = DB_Proxy::query($query);
$num=mysql_numrows($result);
//mysql_close();

$security_level=@mysql_result($result,0,"security_level");
$bid=@mysql_result($result,0,"businessid");
$cid=@mysql_result($result,0,"companyid");

if ($num != 1 || ($security_level==1 && ($bid != $businessid || $cid != $companyid)) || ($security_level > 1 AND $cid != $companyid) || ($user == "" && $pass == ""))
{
    echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
    //setcookie("viewcook",$view);
    setcookie("sortcook",$sorting);
    //setcookie("date1cook",$date1);
    //setcookie("date2cook",$date2);
    setcookie("acctcook",$findaccount);
    setcookie("aged",$aged);

?>

<html>
<head>

<STYLE>
#loading {
 	width: 200px;
 	height: 48px;
 	background-color: ;
 	position: absolute;
 	left: 50%;
 	top: 50%;
 	margin-top: -50px;
 	margin-left: -100px;
 	text-align: center;
}
</STYLE>

<script type="text/javascript" src="public_smo_scripts.js"></script>

<script type="text/javascript">    
var szColorTo = '#00FF00';    
var szColorOriginal = '';        
function change_row_colour(pTarget)    {       
  var pTR = pTarget.parentNode;               
  if(pTR.nodeName.toLowerCase() != 'td')        {            return;        } 
     if(pTarget.checked == true)        {            pTR.style.backgroundColor = szColorTo;        }   
     else        {            pTR.style.backgroundColor = szColorOriginal;        }    
}

</script>

<SCRIPT LANGUAGE="JavaScript" SRC="CalendarPopup.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">document.write(getCalendarStyles());</SCRIPT>

<STYLE>
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation
			{
			background-color:#6677DD;
			text-align:center;
			vertical-align:center;
			text-decoration:none;
			color:#FFFFFF;
			font-weight:bold;
			}
	.TESTcpDayColumnHeader,
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation,
	.TESTcpCurrentMonthDate,
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDate,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDate,
	.TESTcpCurrentDateDisabled,
	.TESTcpTodayText,
	.TESTcpTodayTextDisabled,
	.TESTcpText
			{
			font-family:arial;
			font-size:8pt;
			}
	TD.TESTcpDayColumnHeader
			{
			text-align:right;
			border:solid thin #6677DD;
			border-width:0 0 1 0;
			}
	.TESTcpCurrentMonthDate,
	.TESTcpOtherMonthDate,
	.TESTcpCurrentDate
			{
			text-align:right;
			text-decoration:none;
			}
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDateDisabled
			{
			color:#D0D0D0;
			text-align:right;
			text-decoration:line-through;
			}
	.TESTcpCurrentMonthDate
			{
			color:#6677DD;
			font-weight:bold;
			}
	.TESTcpCurrentDate
			{
			color: #FFFFFF;
			font-weight:bold;
			}
	.TESTcpOtherMonthDate
			{
			color:#808080;
			}
	TD.TESTcpCurrentDate
			{
			color:#FFFFFF;
			background-color: #6677DD;
			border-width:1;
			border:solid thin #000000;
			}
	TD.TESTcpCurrentDateDisabled
			{
			border-width:1;
			border:solid thin #FFAAAA;
			}
	TD.TESTcpTodayText,
	TD.TESTcpTodayTextDisabled
			{
			border:solid thin #6677DD;
			border-width:1 0 0 0;
			}
	A.TESTcpTodayText,
	SPAN.TESTcpTodayTextDisabled
			{
			height:20px;
			}
	A.TESTcpTodayText
			{
			color:#6677DD;
			font-weight:bold;
			}
	SPAN.TESTcpTodayTextDisabled
			{
			color:#D0D0D0;
			}
	.TESTcpBorder
			{
			border:solid thin #6677DD;
			}
</STYLE>

</head>

<?
    echo "<body>";
    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%><tr><td colspan=2>";
    echo "<a href=businesstrack.php><img src=logo.jpg border=0 height=43 width=205></a><p></td></tr>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM company WHERE companyid = '$companyid'";
    $result = DB_Proxy::query($query);
    //mysql_close();

    $companyname=@mysql_result($result,0,"companyname");

    //echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";
    echo "<tr bgcolor=#CCCCFF><td colspan=2><font size=4><b>Invoice Aging - $companyname</b></font></td>";
    echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";
    echo "<tr><td colspan=2><a href=invoice.php?cid=$companyid><font color=blue>Invoice List</font></a> :: Aging :: <a href=invoice3.php?cid=$companyid><font color=blue>Payment Detail</font></a></td></tr>";
    echo "</table></center><p>";

    if ($view==""){$view="All";}
    if ($sorting==""){$sorting="date";}

    if($aged=="0"){
       $date2=$today;
       $date1=$today;
       for($counter=1;$counter<=30;$counter++){$date1=prevday($date1);}
       $d0="SELECTED";
    }
    elseif($aged==1){
       $date2=$today;
       for($counter=1;$counter<=31;$counter++){$date2=prevday($date2);}
       $date1=$today;
       for($counter=1;$counter<=60;$counter++){$date1=prevday($date1);}
       $d1="SELECTED";
    }
    elseif($aged==2){
       $date2=$today;
       for($counter=1;$counter<=61;$counter++){$date2=prevday($date2);}
       $date1=$today;
       for($counter=1;$counter<=90;$counter++){$date1=prevday($date1);}
       $d2="SELECTED";
    }
    elseif($aged==3){
       $date2=$today;
       for($counter=1;$counter<=91;$counter++){$date2=prevday($date2);}
       $date1=$today;
       for($counter=1;$counter<=120;$counter++){$date1=prevday($date1);}
       $d3="SELECTED";
    }
    elseif($aged==4){
       $date2=$today;
       for($counter=1;$counter<=121;$counter++){$date2=prevday($date2);}
       $date1="1900-01-01";
       $d4="SELECTED";
    }
    elseif($aged==5){
       $date2=$today;
       for($counter=1;$counter<=$days;$counter++){$date2=prevday($date2);}
       $date1="1900-01-01";
       $d5="SELECTED";
    }
    else{
       $date2="0000-00-00";
       $date1="0000-00-00";
    }

    if ($sorting=="invoiceid"){$s1="SELECTED";}
    elseif ($sorting=="businessid"){$s2="SELECTED";}
    elseif ($sorting=="accountid"){$s3="SELECTED";}
    elseif ($sorting=="date"){$s4="SELECTED";}
    elseif ($sorting=="type"){$s5="SELECTED";}
    elseif ($sorting=="status"){$s6="SELECTED";}
    elseif ($sorting=="total"){$s7="SELECTED";}

    if ($invoiceid>0){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM invoice WHERE invoiceid = '$invoiceid'";
       $result = DB_Proxy::query($query);
       $num=mysql_numrows($result);
       //mysql_close();

    }
    elseif ($findaccount!=""){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM invoice WHERE companyid = '$companyid' AND date >= '$date1' AND date <= '$date2' AND posted = '1' AND cleared = '0' AND accountid = '$findaccount' ORDER BY $sorting";
       $result = DB_Proxy::query($query);
       $num=mysql_numrows($result);
       //mysql_close();

    }
    else{
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM invoice WHERE companyid = '$companyid' AND date >= '$date1' AND date <= '$date2' AND posted = '1' AND cleared = '0' ORDER BY $sorting";
       $result = DB_Proxy::query($query);
       $num=mysql_numrows($result);
       //mysql_close();
    }

    if ($findaccount!=""){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query15 = "SELECT status FROM accounts WHERE accountid = '$findaccount'";
       $result15 = DB_Proxy::query($query15);
       //mysql_close();

       $accountstatus=@mysql_result($result15,0,"status");
    }

//echo "<center><table width=90% border=1><tr><td>$query</td></tr></table><p></center>";

    if($days==""){$days="#days";}

    echo "<center><table width=90% border=0 cellspacing=0 cellpadding=0><tr valign=bottom><td colspan=8><FORM ACTION='invoice2.php' method='post' name='filter'>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<input type=hidden name=businessid value=$businessid>";
    echo "<input type=hidden name=companyid value=$companyid><select name=aged onchange=\"if (this.selectedIndex==6){this.form['days'].style.visibility='visible'}else {this.form['days'].style.visibility='hidden'};\">";
    echo "<option value='-1'>Display None</option><option value='0' $d0>0-30 days</option><option value='1' $d1>31-60 days</option><option value='2' $d2>61-90 days</option><option value='3' $d3>91-120 days</option><option value='4' $d4>more than 120 days</option><option value=5 $d5>Outstanding more than...</option>";
    echo "</select> <input if ($days!=0){style=\"display:inline;\"} else {style=\"visibility:hidden;display:inline;\"} type=text name=days size=3 onfocus=\"this.value=''\" value='$days'> ";
    echo " Sort by <select name=sorting><option value='invoiceid' $s1>Invoice#</option><option value='businessid' $s2>Business</option><option value='accountid' $s3>Account</option><option value='date' $s4>Date</option><option value='type' $s5>Type</option><option value='status' $s6>Status</option><option value='total' $s7>Total</option></select> <select name=findaccount>";
    
    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query5 = "SELECT * FROM accounts WHERE companyid = '$companyid' AND ((is_deleted = '0') OR (is_deleted = '1' AND del_date >= '$date1')) AND accountnum != '' ORDER BY accountnum DESC";
    $result5 = DB_Proxy::query($query5);
    $num5=mysql_numrows($result5);
    //mysql_close();

    echo "<option value=''>All Accounts</option>";
   
    $num5--;
    while ($num5>=0){
       $accountid=@mysql_result($result5,$num5,"accountid");
       $accountname=@mysql_result($result5,$num5,"name");
       $accountnum=@mysql_result($result5,$num5,"accountnum");
       if ($findaccount==$accountid){$checked="SELECTED";}
       else {$checked="";}
       echo "<option value=$accountid $checked>$accountnum - $accountname</option>";
       $num5--;
    }

    echo "</select> <INPUT TYPE=submit VALUE='GO'></FORM></td></tr>";

    if($thisdate==""){$tender_date=date("Y-m-d");}
    else{$tender_date=$thisdate;}

    if($error==1){$showerror="<font color=red><b>Invoice Not Found.</font>";}
    elseif($error==2){$showerror="<font color=blue><b>Partial Payment.</font>";}
    elseif($error==3){$showerror="<font color=orange><b>Invoice Over Paid.</font>";}
    elseif($error==4){$showerror="<font color=green><b>Invoice Paid.</font>";}
    elseif($error==5){$showerror="<font color=green><b>Invoices Paid.</font>";}
    else{$showerror="";}

    //echo "<tr><td colspan=7 align=right bgcolor=#E8E7E7 style=\"border:1px solid black;\"><form action=addtender2.php method=post name='payment'><input type=hidden name=companyid value=$companyid>$showerror Invoice#: <input type=text name=invoiceid size=30> $<input type=text name=amount size=8> Check#:<input type=text name=checknum size=8> <SCRIPT LANGUAGE='JavaScript' ID='js25'> var cal25 = new CalendarPopup('testdiv1');cal25.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=tender_date VALUE='$tender_date' SIZE=8> <A HREF='javascript:void();' onClick=cal25.select(document.forms[1].tender_date,'anchor25','yyyy-MM-dd'); return false; TITLE=cal25.select(document.forms[1].tender_date,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor25' ID='anchor25'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A> <input type=submit value='Add'></td></tr>";
    //echo "<tr height=16><td colspan=7></form></td></tr>";

    //echo "<tr bgcolor=black><td height=1 colspan=8></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td><form action=printinvoices.php method=post target='_blank'><input type=hidden name=companyid value=$companyid><input type=checkbox name=checkall onclick='checkUncheckAll(this);' onclick='change_row_colour(this)'> <b>Invoice#</b></td><td><b>Business</b></td><td><b>Account</b></td><td><b>Date</b></td><td><b>Type</b></td><td><b>Status</b></td><td align=right><b>Balance</b></td><td align=right><b>Total</b></td></tr>";
    echo "<tr bgcolor=black><td height=1 colspan=8></td></tr>";

    $grandtotal=0;
    $results=$num;
    $num--;
    $counter=1;
    while ($num>=0){
          $invoiceid=@mysql_result($result,$num,"invoiceid");
          $businessid=@mysql_result($result,$num,"businessid");
          $accountid=@mysql_result($result,$num,"accountid");
          $invoicedate=@mysql_result($result,$num,"date");
          $type=@mysql_result($result,$num,"type");
          $status=@mysql_result($result,$num,"status");
          $consolidate=@mysql_result($result,$num,"consolidate");
          $total=@mysql_result($result,$num,"total");
          $outstanding=@mysql_result($result,$num,"outstanding");
          $total=money($total);
          $outstanding=money($outstanding);

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query2 = "SELECT name,accountnum,status FROM accounts WHERE accountid = '$accountid'";
          $result2 = DB_Proxy::query($query2);
          //mysql_close();

          $accountname=@mysql_result($result2,0,"name");
          $accountnum=@mysql_result($result2,0,"accountnum");

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query3 = "SELECT typename FROM invtype WHERE typeid = '$type'";
          $result3 = DB_Proxy::query($query3);
          //mysql_close();

          $typename=@mysql_result($result3,0,"typename");

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query3 = "SELECT businessname FROM business WHERE businessid = '$businessid'";
          $result3 = DB_Proxy::query($query3);
          //mysql_close();

          $businessname=@mysql_result($result3,0,"businessname");

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query4 = "SELECT statusname FROM invstatus WHERE statusid = '$status'";
          $result4 = DB_Proxy::query($query4);
          //mysql_close();

          $statusname=@mysql_result($result4,0,"statusname");

          $statusname=@mysql_result($result4,0,"statusname");
          if ($statusname=="Pending"){$statusname="<font color=green>$statusname</font>";}
          if ($statusname=="Void"){$statusname="<font color=red>$statusname</font>";}

          if ($consolidate==1){$showcon="<font color=red>C</font>";}
          else {$showcon="";}

          $accountname=substr($accountname,0,20);
          $var_name="invoiceid$counter";

          echo "<tr onMouseOver=this.bgColor='#999999' onMouseOut=this.bgColor='white'><td><input type=checkbox name=$var_name value=$invoiceid onclick='change_row_colour(this)'> <a style=$style href=saveinvoice.php?bid=$businessid&cid=$companyid&invoiceid=$invoiceid&direct=9><font color=blue size=2>$invoiceid</font></a>$showcon</td><td><font size=2>$businessname</td><td><font size=2>$accountname ($accountnum)</td><td><font size=2>$invoicedate</td><td><font size=2>$typename</td><td><font size=2>$statusname</td><td align=right><font size=2>$$outstanding</td><td align=right><font size=2>$$total</td></tr>";
          echo "<tr bgcolor=#CCCCCC><td height=1 colspan=8></td></tr>";

          $grandtotal=$grandtotal+$total;
          $tot_outstanding+=$outstanding;
          $num--;
          $counter++;
    }
    $totalcount=$counter-1;
    echo "<tr bgcolor=black><td height=1 colspan=8></td></tr>";
    $grandtotal=money($grandtotal);
    $tot_outstanding=money($tot_outstanding);
    echo "<tr><td>Results: $results</td><td colspan=5></td><td align=right><b>$$tot_outstanding</td><td align=right><b>$$grandtotal</b></td></tr>";
    echo "<tr bgcolor=white><td height=3 colspan=6><input type=hidden name=totalcount value=$totalcount><input type=submit value='Print Selected'></form></td></tr>";
    if ($findaccount!=""){
       if ($accountstatus==1){$act1="SELECTED";}
       elseif($accountstatus==2){$act2="SELECTED";}
       elseif($accountstatus==3){$act3="SELECTED";}
       echo "<tr><td colspan=3><form action=acct_status.php method=post><input type=hidden name=companyid value=$companyid><input type=hidden name=accountid value=$findaccount>Account Status: <select name=acct_status><option value=0>Active</option><option value=1 $act1>Inactive</option><option value=2 $act2>Suspended</option><option value=3 $act3>COD</option></select><input type=submit value='Change'></td><td colspan=2></form></td></tr>";
    }
    echo "</table></center>";

    //mysql_close();

//////END TABLE////////////////////////////////////////////////////////////////////////////////////

    echo "<br><FORM ACTION=businesstrack.php method=post>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<center><INPUT TYPE=submit VALUE=' Return to Main Page'></FORM></center>";
    echo "<DIV ID=testdiv1 STYLE=position:absolute;visibility:hidden;background-color:white;layer-background-color:white;></DIV></body></html>";
}
?>