<?php
define('DOC_ROOT', dirname(dirname(__FILE__)));
require_once(DOC_ROOT.'/bootstrap.php');
$style = "text-decoration:none";

/*$user = isset($_POST['username'])?$_POST['username']:'';
$pass = isset($_POST['password'])?$_POST['password']:'';
$businessid = isset($_POST['businessid'])?$_POST['businessid']:'';
$companyid = isset($_POST['companyid'])?$_POST['companyid']:'';
$accountname = isset($_POST['accountname'])?$_POST['accountname']:'';
$accountnum = isset($_POST['accountnum'])?$_POST['accountnum']:'';
$accountattn = isset($_POST['accountattn'])?$_POST['accountattn']:'';
$accountstreet = isset($_POST['accountstreet'])?$_POST['accountstreet']:'';
$accountcity = isset($_POST['accountcity'])?$_POST['accountcity']:'';
$accountstate = isset($_POST['accountstate'])?$_POST['accountstate']:'';
$accountzip = isset($_POST['accountzip'])?$_POST['accountzip']:'';
$accountphone = isset($_POST['accountphone'])?$_POST['accountphone']:'';
$accountfax = isset($_POST['accountfax'])?$_POST['accountfax']:'';
$accountparent = isset($_POST['acct_parent'])?$_POST['acct_parent']:'';
$accountemail = isset($_POST['accountemail'])?$_POST['accountemail']:'';
$accounttaxid = isset($_POST['taxid'])?$_POST['taxid']:'';
$costcenter = isset($_POST['costcenter'])?$_POST['costcenter']:'';*/

$user = \EE\Controller\Base::getPostVariable('username', '');
$pass = \EE\Controller\Base::getPostVariable('password', '');
$businessid = \EE\Controller\Base::getPostVariable('businessid', '');
$companyid = \EE\Controller\Base::getPostVariable('companyid', '');
$accountname = \EE\Controller\Base::getPostVariable('accountname', '');
$accountnum = \EE\Controller\Base::getPostVariable('accountnum', '');
$accountattn = \EE\Controller\Base::getPostVariable('accountattn', '');
$accountstreet = \EE\Controller\Base::getPostVariable('accountstreet', '');
$accountcity = \EE\Controller\Base::getPostVariable('accountcity', '');
$accountstate = \EE\Controller\Base::getPostVariable('accountstate', '');
$accountzip = \EE\Controller\Base::getPostVariable('accountzip', '');
$accountphone = \EE\Controller\Base::getPostVariable('accountphone', '');
$accountfax = \EE\Controller\Base::getPostVariable('accountfax', '');
$accountparent = \EE\Controller\Base::getPostVariable('acct_parent', '');
$accountemail = \EE\Controller\Base::getPostVariable('accountemail', '');
$accounttaxid = \EE\Controller\Base::getPostVariable('taxid', '');
$costcenter = \EE\Controller\Base::getPostVariable('costcenter', '');

$accountname=str_replace("'","`",$accountname);
$accountattn=str_replace("'","`",$accountattn);
$accountstreet=str_replace("'","`",$accountstreet);
$accountcity=str_replace("'","`",$accountcity);

if ($accountnum=="Acct #"){$accountnum="";}

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=Treat_DB_ProxyOld::mysql_numrows($result);

$location="busrequest.php?bid=$businessid&cid=$companyid";
header('Location: ./' . $location);

if ($num != 1 || $accountname == "" || $user == "" || $pass == "")
{
    echo "<center><h3>Failed</h3></center>";
}

else
{
    $query = "SELECT districtid,businessname,acct_counter,unit,ar_unit FROM business WHERE businessid = '$businessid'";
    $result = Treat_DB_ProxyOld::query($query);

    $districtid=@Treat_DB_ProxyOld::mysql_result($result,0,"districtid");
    $busname=@Treat_DB_ProxyOld::mysql_result($result,0,"businessname");
    $acct_counter=@Treat_DB_ProxyOld::mysql_result($result,0,"acct_counter");
    $unit=@Treat_DB_ProxyOld::mysql_result($result,0,"unit");
    $ar_unit=@Treat_DB_ProxyOld::mysql_result($result,0,"ar_unit");

    $accountnum="$ar_unit$acct_counter";

    $query = "UPDATE business SET acct_counter = acct_counter + 1 WHERE businessid = '$businessid'";
    $result = Treat_DB_ProxyOld::query($query);

    $today=date("Y-m-d");

    if ($is_parent=="on"){$accountparent="-1";}
    $query = "INSERT INTO accounts (businessid,companyid,accountnum,name,attn,street,city,state,zip,phone,fax,email,taxid,costcenter,export,create_date) VALUES ('$businessid','$companyid','$accountnum','$accountname','$accountattn','$accountstreet','$accountcity','$accountstate','$accountzip','$accountphone','$accountfax','$accountemail','$accounttaxid','$costcenter','1','$today')";
    $result = Treat_DB_ProxyOld::query($query);

    $location="busrequest.php?bid=$businessid&cid=$companyid&success=1";

    $query = "SELECT arrequest FROM district WHERE districtid = '$districtid'";
    $result = Treat_DB_ProxyOld::query($query);

    $arrequest=@Treat_DB_ProxyOld::mysql_result($result,0,"arrequest");

    $query = "SELECT * FROM login WHERE loginid = '$arrequest'";
    $result = Treat_DB_ProxyOld::query($query);

    $firstname=@Treat_DB_ProxyOld::mysql_result($result,0,"firstname");
    $email=@Treat_DB_ProxyOld::mysql_result($result,0,"email");

    $todayis = date("l, F j, Y, g:i a") ;
    $visitormail="support@treatamerica.com";
    $from = "From: $visitormail\r\n";
    $subject = "A/R Request";

    $message = "$firstname,\n\n$user has added $accountname to $busname's accounts list.\n\nSupport";

    mail($email, $subject, $message, $from);
}
header('Location: ./' . $location);
?>