<?php
$end = empty($_GET['end'])? strtotime('next Saturday') : strtotime($_GET['end']);
$start = date('Y-m-d', (empty($_GET['start'])? strtotime('-1 week +1 day', $end) : strtotime($_GET['start'])));
$end = date('Y-m-d', $end);

$company = Treat_Model_Company::getById($companyid);
$companyTypes = Treat_Model_Company_Type::getByBusinessType($company->business_hierarchy);
setlocale(LC_MONETARY, 'en_US');
$_SESSION['oldcompany'] = $company->companyid;
// Now go through the levels... yikes.
?>
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery('#company-types h1').click(function () {
			jQuery(this).next().toggle('fast', function () {
				var isShown = (jQuery(this).css('display') == 'none')? 0 : 1;
				var id = jQuery(this).parent().attr('id');
				jQuery.get('ajax/hierarchy/history/update/'+id+'/t/'+isShown);
			});
			return(false);
		});
		jQuery('.company h2').click(function () {
			jQuery(this).next().toggle('fast', function () {
				var isShown = (jQuery(this).css('display') == 'none')? 0 : 1;
				var id = jQuery(this).parent().attr('id');
				jQuery.get('ajax/hierarchy/history/update/'+id+'/c/'+isShown);
			});
			return(false);
		});
		jQuery('.district h3').click(function () {
			jQuery(this).next().toggle('fast', function () {
				var isShown = (jQuery(this).css('display') == 'none')? 0 : 1;
				var id = jQuery(this).parent().attr('id');
				jQuery.get('ajax/hierarchy/history/update/'+id+'/d/'+isShown);
			});
			return(false);
		});
		jQuery('.business h4 a').click(function() {
			var cid = jQuery(this).attr('companyid');
			var href = jQuery(this).attr('href');
			jQuery.ajax({
				type: 'POST',
				url: 'changecomp.php',
				data: {newcompany: cid},
				success: function () {
					document.location = href;
				}
			});
			return(false);
		});
		jQuery('.datepicker').datepicker({dateFormat: 'yy-mm-dd'});
	});
</script>
<style type="text/css">
	#company {
		border: 1px solid black;
		box-shadow: 2px 2px 10px #666;
		font-size: 38px;
		margin: 20px;
		padding: 5px;
		text-shadow: 2px 2px 5px #000;
		width: 500px;
	}

	.company-type {
		border: 1px solid #000;
		box-shadow: 2px 2px 10px #666;
		clear: both;
		margin: 10px 0px;
		padding: 0px 10px;
		text-align: left;
		width: 88%;
	}

	#dates {
		width: 90%;
	}

	.company {
		padding: 0 20px;
		clear: both;
	}

	.district {
		clear: both;
		padding: 0 20px;
	}

	.business {
		box-shadow: 2px 2px 5px #aaa;
		float: left;
		margin: 10px;
		width: 350px;
	}

	.business h4 {
		margin: 0;
		padding: 2px;
		text-shadow: 1px 1px 5px #000;
	}

	.business div {
		padding: 2px;
	}

	.company-type h1 a, .company h2 a, .district h3 a, .business h4 a {
		text-decoration: none;
	}

	.company-type h1 a, .company h2 a, .district h3 a {
		color: blue;
	}

</style>
<div id="company" class="ui-widget ui-corner-all ui-widget-header"><?php echo $company->companyname; ?></div>
<div id="dates" class="ui-widget ui-corner-all ui-widget-header">
	<form method="GET" action="businesstrack.php">
		<label for="start">Start Date:</label>
		<input type="text" name="start" id="start" class="datepicker" value="<?php echo $start; ?>" />
		&rarr;
		<label for="end">End Date:</label>
		<input type="text" name="end" id="end" class="datepicker" value="<?php echo $end; ?>" />
		&rarr;
		<input type="submit" value="GO" />
	</form>
</div>
<div id="company-types">
<?php foreach ($companyTypes as $type): $companies = Treat_Model_Company::getByType($type->typeid, $company->business_hierarchy); $sales = $type->getDashboardSales($start, $end, $company->business_hierarchy); ?>
	<div class="company-type ui-corner-all ui-widget ui-widget-content" id="<?php echo $type->typeid; ?>">
		<h1><a href="#"><?php echo $type->type_name; ?></a><div>Total Sales: $<?php echo number_format($sales, 2); ?></div><div>Admin Fee: $<?php echo number_format(($sales * 0.0595), 2); ?></div></h1>
		<div<?php if (isset($_SESSION['hierarchy']['t'][$type->typeid]) && $_SESSION['hierarchy']['t'][$type->typeid] === false) echo ' style="display: none"'; ?>>
			<?php foreach ($companies as $comp): $districts = Treat_Model_District::getByCompany($comp, $company->business_hierarchy); $sales = $comp->getDashboardSales($start, $end, $company->business_hierarchy); ?>
			<div class="company" id="<?php echo $comp->companyid; ?>">
				<h2><a href="#"><?php echo $comp->companyname; ?></a><div>Total Sales: $<?php echo number_format($sales, 2); ?></div><div>Admin Fee: $<?php echo number_format(($sales * 0.0595), 2); ?></div></h2>
				<div<?php if (isset($_SESSION['hierarchy']['c'][$comp->companyid]) && $_SESSION['hierarchy']['c'][$comp->companyid] === false) echo ' style="display: none"'; ?>>
					<?php foreach ($districts as $district): $businesses = Treat_Model_Business::getByDistrict($district, $company->business_hierarchy); $sales = $district->getDashboardSales($start, $end, $company->business_hierarchy); ?>
					<div class="district" id="<?php echo $district->districtid; ?>">
						<h3><a href="#"><?php echo $district->districtname; ?></a><div>Total Sales: $<?php echo number_format($sales, 2); ?></div><div>Admin Fee: $<?php echo number_format(($sales * 0.0595), 2); ?></div></h3>
						<div style="display: <?php echo (isset($_SESSION['hierarchy']['d'][$district->districtid]) && $_SESSION['hierarchy']['d'][$district->districtid] === true)? 'block' : 'none'; ?>">
							<?php foreach ($businesses as $business): $sales = $business->getDashboardSales($start, $end); ?>
							<div class="business ui-widget ui-widget-content ui-corner-all">
								<h4 class="ui-widget-header">
									<div style="float: right; cursor: pointer;" class="ui-icon ui-icon-wrench" onclick="show_setup(<?php echo $business->businessid; ?>);">
									</div>
									<a companyid="<?php echo $comp->companyid; ?>" href="busdetail.php?bid=<?php echo $business->businessid; ?>&cid=<?php echo $comp->companyid; ?>"><?php echo $business->businessname; ?></a>
								</h4>
								<div>
									<div>Total Sales: $<?php echo number_format($sales, 2); ?></div>
									<div>Admin Fee: $<?php echo number_format(($sales * 0.0595), 2); ?></div>
								</div>
							</div>
							<?php endforeach; ?>
							<br style="clear: both;" />
						</div>
					</div>
					<?php endforeach; ?>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
<?php endforeach; ?>
</div>