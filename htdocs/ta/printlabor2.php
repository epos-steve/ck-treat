<?php

function fixDate($date){
	$exp = explode(' ', $date);
	$d = $exp[0];
	$t = $exp[1];

	$exp = explode(':', $t);
	$h = $exp[0];
	$m = strval($exp[1]);

	$ap = 'a';
	if($h >= 12){
		$ap = 'p';
		if($h != 12) $h -= 12;
	}
	else if($h < 1) $h = 12;

	$t = "$h:$m$ap";
	$n_date = "$t $d";

	return $n_date;
}

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+4);
        $diff=round($diff, 2);
        if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
        elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
        $diff = substr($diff, 0, $dot+3);
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      elseif ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      elseif ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '1')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='31')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;
$leap = date("L");

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   
   elseif ($leap==1&&$month=='03')
   {
      $day=$day+29;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      $day=$day-31;  
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";} 
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}

//include("db.php");
//require('lib/class.db-proxy.php');

if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
require_once( DOC_ROOT . '/bootstrap.php' );

$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";

$user=$_COOKIE["usercook"];
$pass=$_COOKIE["passcook"];
$businessid=$_GET["bid"];
$companyid=$_GET["cid"];
$date1=$_GET["date1"];
$date2=$_GET["date2"];
$eid=$_GET["eid"];

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=mysql_numrows($result);

$loginid=@Treat_DB_ProxyOld::mysql_result($result,0,"loginid");
$security_level=@Treat_DB_ProxyOld::mysql_result($result,0,"security_level");
$mysecurity=@Treat_DB_ProxyOld::mysql_result($result,0,"security");
$bid=@Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
$bid2=@Treat_DB_ProxyOld::mysql_result($result,0,"busid2");
$bid3=@Treat_DB_ProxyOld::mysql_result($result,0,"busid3");
$bid4=@Treat_DB_ProxyOld::mysql_result($result,0,"busid4");
$bid5=@Treat_DB_ProxyOld::mysql_result($result,0,"busid5");
$bid6=@Treat_DB_ProxyOld::mysql_result($result,0,"busid6");
$bid7=@Treat_DB_ProxyOld::mysql_result($result,0,"busid7");
$bid8=@Treat_DB_ProxyOld::mysql_result($result,0,"busid8");
$bid9=@Treat_DB_ProxyOld::mysql_result($result,0,"busid9");
$bid10=@Treat_DB_ProxyOld::mysql_result($result,0,"busid10");
$cid=@Treat_DB_ProxyOld::mysql_result($result,0,"companyid");

if ($num != 1 || $user == "" || $pass == "")
{
    echo "<center><h3>Failed</h3>Use your browser's back button to try again.</center>";
}

else
{

    $day = date("d");
    $year = date("Y");
    $month = date("m");
    $today2="$year-$month-$day";

    if ($date1==""){$date1=$today2;}
    if ($date2==""){$date2=$today2;}

    $query = "SELECT * FROM security WHERE securityid = '$mysecurity'";
    $result = Treat_DB_ProxyOld::query($query);

    $sec_emp_labor=@Treat_DB_ProxyOld::mysql_result($result,0,"emp_labor");
    $sec_bus_sales=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_sales");
    $sec_bus_invoice=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_invoice");
    $sec_bus_order=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_order");
    $sec_bus_payable=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_payable");
    $sec_bus_inventor1=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor1");
    $sec_bus_inventor2=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor2");
    $sec_bus_inventor3=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor3");
    $sec_bus_inventor4=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor4");
    $sec_bus_inventor5=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor5");
    $sec_bus_control=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_control");
    $sec_bus_menu=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_menu");
    $sec_bus_order_setup=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_order_setup");
    $sec_bus_request=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_request");
    $sec_route_collection=@Treat_DB_ProxyOld::mysql_result($result,0,"route_collection");
    $sec_route_labor=@Treat_DB_ProxyOld::mysql_result($result,0,"route_labor");
    $sec_route_detail=@Treat_DB_ProxyOld::mysql_result($result,0,"route_detail");
    $sec_bus_labor=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_labor");

    
    //if ($date2==$date1){for($counter=1;$counter<7;$counter++){$date2=nextday($date2);}}

    $query = "SELECT * FROM company WHERE companyid = '$companyid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $week_end=@Treat_DB_ProxyOld::mysql_result($result,0,"week_end");

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM business WHERE businessid = '$businessid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $businessname=@Treat_DB_ProxyOld::mysql_result($result,0,"businessname");
    $businessid=@Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
    $street=@Treat_DB_ProxyOld::mysql_result($result,0,"street");
    $city=@Treat_DB_ProxyOld::mysql_result($result,0,"city");
    $state=@Treat_DB_ProxyOld::mysql_result($result,0,"state");
    $zip=@Treat_DB_ProxyOld::mysql_result($result,0,"zip");
    $phone=@Treat_DB_ProxyOld::mysql_result($result,0,"phone");
    $districtid=@Treat_DB_ProxyOld::mysql_result($result,0,"districtid");
    $operate=@Treat_DB_ProxyOld::mysql_result($result,0,"operate");

///////////////////DISPLAY
    echo "<body onLoad=window.print()>";

    $query = "SELECT * FROM submit_labor WHERE date >= '$date1' AND date <= '$date2' AND businessid = '$businessid'";
    $result = Treat_DB_ProxyOld::query($query);
    $num=mysql_numrows($result);

    echo "<img src=\"/assets/images/vendlogo.jpg\" height=43 width=205>";
    echo "<table width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid black;\">";

    $today=date("Y-m-d");
    $today="$today 00:00:00";
    $date1="$date1 00:00:00";
    $date2="$date2 23:59:59";

    $query = "SELECT labor_clockin.clockinid,labor_clockin.loginid,labor_clockin.clockin,labor_clockin.clockout,labor_clockin.jobtype,login.firstname,login.lastname,login.empl_no FROM labor_clockin,login WHERE labor_clockin.loginid = '$eid' AND labor_clockin.businessid = '$businessid' AND labor_clockin.clockin >= '$date1' AND labor_clockin.clockin <= '$date2' AND labor_clockin.is_deleted = '0' AND labor_clockin.loginid = login.loginid ORDER BY login.lastname DESC,labor_clockin.jobtype,labor_clockin.clockin DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=mysql_numrows($result);

    if ($num==0){echo "<tr><td colspan=6><i>Nothing to Display</i></td></tr>";}
    else{echo "<tr bgcolor=#E8E7E7 style=\"font-weight: bold;\"><td width=7%><font size=2><b>Empl#</b></td><td width=18%><font size=2><b>Employee</b></td><td width=15%><font size=2><b>Jobtype</b></td><td id='clockin' width=25% align=right>Clockin</td><td id='clockout' width=25% align=right>Clockout</td><td align=right width=10%><b><font size=2>Total</td></tr>";}

    $lastloginid=-1;
    $lastjobtype=-1;
    $lastclockin=-1;
    $lastdate=-1;
    $hours=0;
    $mins=0;
    $counter=0;

    $num--;
    while($num>=-1){
       $clockinid=@Treat_DB_ProxyOld::mysql_result($result,$num,"labor_clockin.clockinid");
       $loginid=@Treat_DB_ProxyOld::mysql_result($result,$num,"labor_clockin.loginid");
       $clockin=@Treat_DB_ProxyOld::mysql_result($result,$num,"labor_clockin.clockin");
       $clockout=@Treat_DB_ProxyOld::mysql_result($result,$num,"labor_clockin.clockout");
       $jobtype=@Treat_DB_ProxyOld::mysql_result($result,$num,"labor_clockin.jobtype");
       $firstname=@Treat_DB_ProxyOld::mysql_result($result,$num,"login.firstname");
       $lastname=@Treat_DB_ProxyOld::mysql_result($result,$num,"login.lastname");
       $empl_no=@Treat_DB_ProxyOld::mysql_result($result,$num,"login.empl_no");

       $query2="SELECT name FROM jobtype WHERE jobtypeid = '$jobtype'";
       $result2 = Treat_DB_ProxyOld::query($query2);

       $jobname=@Treat_DB_ProxyOld::mysql_result($result2,0,"name");

       if($clockinid==$addclock){$rowcolor="yellow";}
       else{$rowcolor="";}

       $thisdate=substr($clockin,0,10);

       $temp_date=$lastdate;
       while($temp_date<$thisdate&&$thisdate!=""&&$lastdate!=-1){
          if(dayofweek($temp_date)==$week_end){$showweek=1;}
          $temp_date=nextday($temp_date);
       }

       if($showweek==1||$num==-1){
          $week_hours=$week_hours*60;
          $week_totmins=round(($week_hours+$week_mins)/60,2);

          echo "<tr bgcolor=#E8E7E7><td colspan=3></td><td colspan=3 align=right>Week Total: $week_totmins</td></tr>";

          $week_hours=0;
          $week_mins=0;
          $showweek=0;
       }

       if (($lastloginid!=$loginid||$lastjobtype!=$jobtype)&&$lastloginid!=-1&&$lastjobtype!=-1){
          $hours=$hours*60;
          $totmins=round(($hours+$mins)/60,2);

          echo "<tr bgcolor=black height=2><td colspan=6></td></tr>";
          echo "<tr bgcolor=#E8E7E7><td colspan=3><b>$counter Record(s) </td><td colspan=3 id=\"et_$lastloginid:$lastjobtype\" align=right><b>Total Hours: $totmins</td></tr>";

          $counter=0;
          $hours=0;
          $mins=0;
       }

       if($clockin<$today&&$clockout=="0000-00-00 00:00:00"){$showcolor="red";}
       elseif($clockout=="0000-00-00 00:00:00"){$showcolor="#444499";}
       else{$showcolor="black";}

       if($clockout=="0000-00-00 00:00:00"){$clockout="";}

       $counter++;

       $query2="SELECT TimeDiff(clockout,clockin) AS totalamt FROM labor_clockin WHERE clockinid = '$clockinid'";
       $result2 = Treat_DB_ProxyOld::query($query2);

       $totalamt=@Treat_DB_ProxyOld::mysql_result($result2,0,"totalamt");

       $showclockin=fixDate($clockin);
       if($clockout!=""){$showclockout=fixDate($clockout);}

       if($num!=-1){echo "<tr id='$clockinid' height=24><td id=\"$loginid\"><font size=2 color=$showcolor>$empl_no</font></td><td id=\"$jobtype\"><font size=2 color=$showcolor>$lastname, $firstname</font></td><td><font size=2 color=$showcolor>$jobname</font></td><td align=right><font size=2>$showclockin</font></td><td align=right><font size=2>$showclockout</font></td><td align=right><font size=2 color=$showcolor>$totalamt</td></tr>";}

       $pieces=explode(":",$totalamt);
       $hours+=$pieces[0];
       $mins+=$pieces[1];
       $week_hours+=$pieces[0];
       $week_mins+=$pieces[1];

       $lastloginid=$loginid;
       $lastjobtype=$jobtype;
       $lastclockin=$clockin;
       $lastdate=substr($clockin,0,10);

       $num--;
    }

///////////////////END
    echo "</table></center></body>";

    //mysql_close(); 
	google_page_track();

}
?>
