<?php
define('DOC_ROOT', dirname(dirname(__FILE__)) );
require_once(DOC_ROOT.'/bootstrap.php');
Treat_DB_ProxyOldProcessHost::$debug = true;

function dateDiff($start, $end) {
  $start_ts = strtotime($start);
  $end_ts = strtotime($end);
  $diff = $end_ts - $start_ts;
  return floor($diff / 86400);
}

function daysBetween($start_date, $end_date, $weekDay) {
   $totaldays=0;
   while($start_date<=$end_date){
      if($weekDay==dayofweek($start_date)){$totaldays++;}
      $start_date=nextday($start_date);
   }
   return $totaldays;
}

function rndm_color_code($b_safe = TRUE) {
    
    //make sure the parameter is boolean
    if(!is_bool($b_safe)) {return FALSE;}
    
    //if a browser safe color is requested then set the array up
    //so that only a browser safe color can be returned
    if($b_saafe) {
        $ary_codes = array('00','33','66','99','CC','FF');
        $max = 5; //the highest array offest
    //if a browser safe color is not requested then set the array
    //up so that any color can be returned.
    } else {
        $ary_codes = array();
        for($i=0;$i<16;$i++) {
            $t_1 = dechex($i);
            for($j=0;$j<16;$j++) {
                $t_2 = dechex($j);
                $ary_codes[] = "$t_1$t_2";
            } //end for j
        } //end for i
        $max = 256; //the highest array offset
    } //end if
    
    $retVal = '';
    
    //generate a random color code
    for($i=0;$i<3;$i++) {
        $offset = rand(0,$max);
        $retVal .= $ary_codes[$offset];
    } //end for i
    
    return $retVal;
} //end rndm_color_code

$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");
$wtf = 0;

/*$user=$_COOKIE["usercook"];
$pass=$_COOKIE["passcook"];
$businessid=$_GET["bid"];
$companyid=$_GET["cid"];
$nouser=$_GET["nouser"];*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook');
$businessid = \EE\Controller\Base::getGetVariable('bid');
$companyid = \EE\Controller\Base::getGetVariable('cid');
$nouser = \EE\Controller\Base::getGetVariable('nouser');

if ($businessid==""&&$companyid==""){
   /*$businessid=$_POST["businessid"];
   $companyid=$_POST["companyid"];
   $date1=$_POST["date1"];
   $date2=$_POST["date2"];
   $period=$_POST["period"];
   $periodnum=$_POST["periodnum"];
   $curreportid=$_POST["curreportid"];
   $savefile=$_POST["savefile"];
   $seperate=$_POST["seperate"];*/
	
	$businessid = \EE\Controller\Base::getPostVariable('businessid');
	$companyid = \EE\Controller\Base::getPostVariable('companyid');
	$date1 = \EE\Controller\Base::getPostVariable('date1');
	$date2 = \EE\Controller\Base::getPostVariable('date2');
	$period = \EE\Controller\Base::getPostVariable('period');
	$periodnum = \EE\Controller\Base::getPostVariable('periodnum');
	$curreportid = \EE\Controller\Base::getPostVariable('curreportid');
	$savefile = \EE\Controller\Base::getPostVariable('savefile');
	$seperate = \EE\Controller\Base::getPostVariable('seperate');

   /////validate dates
   $date1 = strtotime($date1);
   $date1 = date("Y-m-d", $date1);
   if($date1 < "2000-01-01"){$date1 = "2005-01-01";}
}

if(substr($nouser,0,6)=="taauto"){

   $companyid=substr($nouser,6,2)*1;
   $curreportid=substr($nouser,8,2)*1;
   $nouser=substr($nouser,0,6);
   
   if($nouser=="taauto"){
	   $seperate=1;
	   $period=1;
	   $periodnum=46;
	   $date1=date("Y-m-d"); 
	   for($counter=1;$counter<=14;$counter++){$date1=prevday($date1);}   
	   $wtf = 1;
	}
}

if ($businessid<0){
$districtid=$businessid * -1;
}
if ($periodnum==""){$periodnum=1;}
if ($date1==""){$date1=$today;}

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( mysql_error());

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOldProcessHost::query($query);
$num=Treat_DB_ProxyOldProcessHost::mysql_numrows($result);
//mysql_close();

$security_level=Treat_DB_ProxyOldProcessHost::mysql_result($result,0,"security_level");
$bid=Treat_DB_ProxyOldProcessHost::mysql_result($result,0,"businessid");
$bid2=Treat_DB_ProxyOldProcessHost::mysql_result($result,0,"busid2");
$bid3=Treat_DB_ProxyOldProcessHost::mysql_result($result,0,"busid3");
$bid4=Treat_DB_ProxyOldProcessHost::mysql_result($result,0,"busid4");
$bid5=Treat_DB_ProxyOldProcessHost::mysql_result($result,0,"busid5");
$bid6=Treat_DB_ProxyOldProcessHost::mysql_result($result,0,"busid6");
$bid7=Treat_DB_ProxyOldProcessHost::mysql_result($result,0,"busid7");
$bid8=Treat_DB_ProxyOldProcessHost::mysql_result($result,0,"busid8");
$bid9=Treat_DB_ProxyOldProcessHost::mysql_result($result,0,"busid9");
$bid10=Treat_DB_ProxyOldProcessHost::mysql_result($result,0,"busid10");
$cid=Treat_DB_ProxyOldProcessHost::mysql_result($result,0,"companyid");
$loginid=Treat_DB_ProxyOldProcessHost::mysql_result($result,0,"loginid");

if ($num != 1 && $nouser != "taauto")
{
    echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}

else
{

?>

<html>
<head>

<SCRIPT LANGUAGE="JavaScript">
function go(name)
{
var mylist=document.getElementById(name)

var chbox = document.getElementById("chxbox")

if(mylist.options[mylist.selectedIndex].text == "Separated")
{

if(chbox.checked==true)
chbox.checked=false
chbox.disabled=true
}
else
{
chbox.disabled=false
}

} 
</script>


<SCRIPT LANGUAGE="JavaScript" SRC="CalendarPopup.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">document.write(getCalendarStyles());</SCRIPT>

<STYLE>
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation
			{
			background-color:#6677DD;
			text-align:center;
			vertical-align:center;
			text-decoration:none;
			color:#FFFFFF;
			font-weight:bold;
			}
	.TESTcpDayColumnHeader,
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation,
	.TESTcpCurrentMonthDate,
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDate,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDate,
	.TESTcpCurrentDateDisabled,
	.TESTcpTodayText,
	.TESTcpTodayTextDisabled,
	.TESTcpText
			{
			font-family:arial;
			font-size:8pt;
			}
	TD.TESTcpDayColumnHeader
			{
			text-align:right;
			border:solid thin #6677DD;
			border-width:0 0 1 0;
			}
	.TESTcpCurrentMonthDate,
	.TESTcpOtherMonthDate,
	.TESTcpCurrentDate
			{
			text-align:right;
			text-decoration:none;
			}
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDateDisabled
			{
			color:#D0D0D0;
			text-align:right;
			text-decoration:line-through;
			}
	.TESTcpCurrentMonthDate
			{
			color:#6677DD;
			font-weight:bold;
			}
	.TESTcpCurrentDate
			{
			color: #FFFFFF;
			font-weight:bold;
			}
	.TESTcpOtherMonthDate
			{
			color:#808080;
			}
	TD.TESTcpCurrentDate
			{
			color:#FFFFFF;
			background-color: #6677DD;
			border-width:1;
			border:solid thin #000000;
			}
	TD.TESTcpCurrentDateDisabled
			{
			border-width:1;
			border:solid thin #FFAAAA;
			}
	TD.TESTcpTodayText,
	TD.TESTcpTodayTextDisabled
			{
			border:solid thin #6677DD;
			border-width:1 0 0 0;
			}
	A.TESTcpTodayText,
	SPAN.TESTcpTodayTextDisabled
			{
			height:20px;
			}
	A.TESTcpTodayText
			{
			color:#6677DD;
			font-weight:bold;
			}
	SPAN.TESTcpTodayTextDisabled
			{
			color:#D0D0D0;
			}
	.TESTcpBorder
			{
			border:solid thin #6677DD;
			}
</STYLE>

<STYLE>
#loading {
 	width: 170px;
 	height: 48px;
 	background-color: ;
 	position: absolute;
 	left: 50%;
 	top: 50%;
 	margin-top: -50px;
 	margin-left: -100px;
 	text-align: center;
}
</STYLE>

<script type="text/javascript" src="preLoadingMessage.js"></script>
</head>

<?
    if($nouser=="taauto"){$security_level=9;}
    $districtquery="";
    if ($security_level>2&&$security_level<7){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM login_district WHERE loginid = '$loginid'";
       $result = Treat_DB_ProxyOldProcessHost::query($query);
       $num=Treat_DB_ProxyOldProcessHost::mysql_numrows($result);
       //mysql_close();

       $num--;
       while($num>=0){
          $mydistrictid=Treat_DB_ProxyOldProcessHost::mysql_result($result,$num,"districtid");
          if($districtquery==""){$districtquery="districtid = '$mydistrictid'";}
          else{$districtquery="$districtquery OR districtid = '$mydistrictid'";}
          $num--;
       }
    }

    echo "<body>";
    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=100%><tr><td colspan=2>";
    echo "<a href=businesstrack.php><img src=logo.jpg border=0 height=43 width=205></a><p></td></tr>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM company WHERE companyid = '$companyid'";
    $result = Treat_DB_ProxyOldProcessHost::query($query);
    //mysql_close();

    $companyname=Treat_DB_ProxyOldProcessHost::mysql_result($result,0,"companyname");

    //echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";
    echo "<tr bgcolor=#CCCCFF><td colspan=1><font size=4><b>Reporting - $companyname</b></font></td><td align=right><form action='comreportsetup.php?cid=$companyid' method=post><input type=submit value='Setup'></td><td width=0%></form></td></tr>";
    echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";
    echo "</table></center><p>";

    echo "<center><table width=100%><tr><td><form action=comreport.php method=post><input type=hidden name=companyid value=$companyid><select name=curreportid>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
	if($security_level > 8){$query = "SELECT * FROM reports ORDER BY report_name DESC";}
    else{$query = "SELECT * FROM reports WHERE companyid = $companyid ORDER BY report_name DESC";}
    $result = Treat_DB_ProxyOldProcessHost::query($query);
    $num=Treat_DB_ProxyOldProcessHost::mysql_numrows($result);
    //mysql_close();

    $num--;
    echo "<option value=0>===Choose Report===</option>";
    while($num>=0){
       $reportname=Treat_DB_ProxyOldProcessHost::mysql_result($result,$num,"report_name");
       $reportid=Treat_DB_ProxyOldProcessHost::mysql_result($result,$num,"reportid");

       if ($curreportid==$reportid){$show="SELECTED";}
       else{$show="";}

       echo "<option value=$reportid $show>$reportname</option>";
       $num--;
    }

    echo "</select><select name=businessid>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    if($security_level>6){$query = "SELECT * FROM business WHERE companyid = '$companyid' ORDER BY districtid DESC, businessname DESC";}
    elseif($security_level==1){$query = "SELECT * FROM business WHERE companyid = '$companyid' AND businessid = '$bid' ORDER BY districtid DESC, businessname DESC";}
    elseif($security_level==2){$query = "SELECT * FROM business WHERE companyid = '$companyid' AND (businessid = '$bid' OR businessid = '$bid2' OR businessid = '$bid3' OR businessid = '$bid4' OR businessid = '$bid5' OR businessid = '$bid6' OR businessid = '$bid7' OR businessid = '$bid8' OR businessid = '$bid9' OR businessid = '$bid10') ORDER BY districtid DESC, businessname DESC";}
    elseif($security_level>2&&$security_level<7){$query = "SELECT * FROM business WHERE companyid = '$companyid' AND ($districtquery) ORDER BY districtid DESC, businessname DESC";}
    $result = Treat_DB_ProxyOldProcessHost::query($query);
    $num=Treat_DB_ProxyOldProcessHost::mysql_numrows($result);
    //mysql_close();

    $lastdistrictid=0;
    if ($security_level==9){echo "<option value=>========>ALL UNITS</option>";}
    $num--;
    while($num>=0){
       $businessname=Treat_DB_ProxyOldProcessHost::mysql_result($result,$num,"businessname");
       $busid=Treat_DB_ProxyOldProcessHost::mysql_result($result,$num,"businessid");
       $districtids=Treat_DB_ProxyOldProcessHost::mysql_result($result,$num,"districtid");

       if ($lastdistrictid!=$districtids&&$security_level>2){
          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query4 = "SELECT districtname FROM district WHERE districtid = '$districtids'";
          $result4 = Treat_DB_ProxyOldProcessHost::query($query4);
          //mysql_close();

          $districtname=Treat_DB_ProxyOldProcessHost::mysql_result($result4,0,"districtname");
          
          $thisdistrictid=$districtids*-1;
			if ($districtid==$districtids){$selb="SELECTED";}
			else{$selb="";}
          echo "<option value=$thisdistrictid $selb>========>$districtname</option>";
       }

       if ($busid==$businessid){$show="SELECTED";}
       else{$show="";}

       echo "<option value=$busid $show>$businessname</option>";
       $num--;
       $lastdistrictid=$districtids;
    }

    if ($seperate==0){$s0="SELECTED";}
    elseif($seperate==1){$s1="SELECTED";}

    echo "</select><select name=seperate onchange='go(this.name)'><option value=0 $s0>Consolidated</option><option value=1 $s1>Separated</option></select>";

    echo " Starting: <SCRIPT LANGUAGE='JavaScript' ID='js18'> var cal18 = new CalendarPopup('testdiv1');cal18.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date1 VALUE='$date1' SIZE=8> <A HREF=# onClick=cal18.select(document.forms[1].date1,'anchor18','yyyy-MM-dd'); return false; TITLE=cal18.select(document.forms[1].date1,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor18' ID='anchor18'><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A>";

    if ($period==1){$c1="SELECTED";}
    elseif($period==2){$c2="SELECTED";}
    elseif($period==3){$c3="SELECTED";}
    elseif($period==4){$c4="SELECTED";}

    if($seperate==1){$showg="DISABLED";}
    elseif ($graph==1){$showg="CHECKED";}

    echo " for <input type=text name=periodnum size=2 value='$periodnum'><select name=period><option value=1 $c1>Day(s)</option><option value=2 $c2>Week(s)</option><option value=3 $c3>Month(s)</option></select> <input type=checkbox name=graph value=1 $showg id='chxbox'>Graph <input type=submit value='GO'></form></td></tr></table>";

    echo "<DIV ID=testdiv1 STYLE=position:absolute;visibility:hidden;background-color:white;layer-background-color:white;></DIV>";


//////////////////////////////START REPORT
    if ($curreportid!=""&&$periodnum!=""&&$date1!=""){
       $file="$reportname.csv";
       $data="";
       $totaldays=0;

       $periodcounter=1;
       $tempdate=$date1;
       ///DAYS
       if ($period==1){
           while($periodcounter<=$periodnum){
              $startdate[$periodcounter]=$tempdate;
              $enddate[$periodcounter]=$tempdate;
              $tempdate=nextday($tempdate);
              $periodcounter++;
              $totaldays++;
           }
       }
       ///WEEKS
       elseif($period==2){
           while($periodcounter<=$periodnum){
              $startdate[$periodcounter]=$tempdate;
              for($counter=1;$counter<=6;$counter++){$tempdate=nextday($tempdate);$totaldays++;}
              $enddate[$periodcounter]=$tempdate;
              $tempdate=nextday($tempdate);
              $periodcounter++;
           }
       }
       ///MONTHS
       elseif($period==3){
           while($periodcounter<=$periodnum){
              $startdate[$periodcounter]=$tempdate;

              $tempdatemonth=substr($tempdate,5,2);
              $tempdateyear=substr($tempdate,0,4);

              $tempdatemonth++;
              if ($tempdatemonth<10){$tempdatemonth="0$tempdatemonth";}
              if ($tempdatemonth==13){$tempdatemonth="01";$tempdateyear++;}

              $tempdate="$tempdateyear-$tempdatemonth-01";
              $enddate[$periodcounter]=prevday($tempdate);
              
              $periodcounter++;
           }
       }

   ////////////////////////////////////////////////////
   ///////////CONSOLIDATED/////////////////////////////
   ////////////////////////////////////////////////////
       echo "<center><table border=1 width=100%><tr bgcolor=#CCCCFF><td></td>";

       $data=",";
       for ($counter=1;$counter<=$periodnum;$counter++){$showday=substr($startdate[$counter],8,2);$showmonth=substr($startdate[$counter],5,2);$showyear=substr($startdate[$counter],2,2);echo "<td align=right><font size=2><b>$showmonth/$showday/$showyear</b></td>";$data="$data$showmonth/$showday/$showyear,";}
       echo "<td align=right><font size=2><b>Total</b></td></tr>";$data="$data Total\r\n";

    if($seperate==0){
       $counter2=1;
       
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM reportdetail WHERE reportid = '$curreportid' ORDER BY orderid DESC";
       $result = Treat_DB_ProxyOldProcessHost::query($query);
       $num=Treat_DB_ProxyOldProcessHost::mysql_numrows($result);
       //mysql_close();

       $totalgraph=0;
       $num--;
       ///////////////////////MAIN REPORT DETAIL LOOP
       $rowtotal=array();
       while($num>=0){

          $detailid=Treat_DB_ProxyOldProcessHost::mysql_result($result,$num,"reportdetailid");
          $detailname=Treat_DB_ProxyOldProcessHost::mysql_result($result,$num,"name");
          $type=Treat_DB_ProxyOldProcessHost::mysql_result($result,$num,"type");
          $reportquery=Treat_DB_ProxyOldProcessHost::mysql_result($result,$num,"query");
          $orderid=Treat_DB_ProxyOldProcessHost::mysql_result($result,$num,"orderid");
          $avg=Treat_DB_ProxyOldProcessHost::mysql_result($result,$num,"avg");
          $bold=Treat_DB_ProxyOldProcessHost::mysql_result($result,$num,"bold");
          $hide=Treat_DB_ProxyOldProcessHost::mysql_result($result,$num,"hide");
          $decimal=Treat_DB_ProxyOldProcessHost::mysql_result($result,$num,"num_decimal");
          $suffix=Treat_DB_ProxyOldProcessHost::mysql_result($result,$num,"suffix");
          $is_graphed=Treat_DB_ProxyOldProcessHost::mysql_result($result,$num,"graph_it");

          if($is_graphed==1){$totalgraph=1;}

          if ($bold==1&&$hide==0){echo "<tr><td bgcolor=#E8E7E7><font size=2>$detailname</td>";}
          elseif($hide==0){echo "<tr><td><font size=2>$detailname</td>";}
          $filedata=str_replace(",","/",$detailname);
          if($hide==0){$data="$data$filedata,";}
          for ($counter=1;$counter<=$periodnum;$counter++){
          $arrayname="$counter2.$counter";

          ////CREDITS
          if ($type==1){
             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             if ($businessid>0){$query2 = "SELECT * FROM credits WHERE account LIKE '$reportquery' AND companyid = '$companyid' AND businessid = '$businessid'";}
             else if ($districtid!=""){ $query2 = "SELECT credits.* FROM district INNER JOIN (business INNER JOIN credits ON business.businessid = credits.businessid) ON district.districtid = business.districtid WHERE district.districtid='$districtid' AND credits.companyid='$companyid' AND credits.account LIKE '$reportquery'";}
             else{$query2 = "SELECT * FROM credits WHERE account LIKE '$reportquery' AND companyid = '$companyid'";}
             $result2 = Treat_DB_ProxyOldProcessHost::query($query2);
             $num2=Treat_DB_ProxyOldProcessHost::mysql_numrows($result2);
             //mysql_close();

             $num2--;
             while($num2>=0){
                if ($districtid!=""){
                $creditid=Treat_DB_ProxyOldProcessHost::mysql_result($result2,$num2,"credits.creditid");
                $invoicesales=Treat_DB_ProxyOldProcessHost::mysql_result($result2,$num2,"credits.invoicesales");
                $invoicesales_notax=Treat_DB_ProxyOldProcessHost::mysql_result($result2,$num2,"credits.invoicesales_notax");
                $invoicetax=Treat_DB_ProxyOldProcessHost::mysql_result($result2,$num2,"credits.invoicetax");
                $servchrg=Treat_DB_ProxyOldProcessHost::mysql_result($result2,$num2,"credits.servchrg");
                $salescode=Treat_DB_ProxyOldProcessHost::mysql_result($result2,$num2,"credits.salescode");
                $busid=Treat_DB_ProxyOldProcessHost::mysql_result($result2,$num2,"credits.businessid");
	        }
               else{
                $creditid=Treat_DB_ProxyOldProcessHost::mysql_result($result2,$num2,"creditid");
                $invoicesales=Treat_DB_ProxyOldProcessHost::mysql_result($result2,$num2,"invoicesales");
                $invoicesales_notax=Treat_DB_ProxyOldProcessHost::mysql_result($result2,$num2,"invoicesales_notax");
                $invoicetax=Treat_DB_ProxyOldProcessHost::mysql_result($result2,$num2,"invoicetax");
                $servchrg=Treat_DB_ProxyOldProcessHost::mysql_result($result2,$num2,"servchrg");
                $salescode=Treat_DB_ProxyOldProcessHost::mysql_result($result2,$num2,"salescode");
                $busid=Treat_DB_ProxyOldProcessHost::mysql_result($result2,$num2,"businessid");
   		
		}
               if ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0&&$prepayment==0){
                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query3 = "SELECT sum(amount) as totalamt FROM creditdetail WHERE creditid = '$creditid' AND date >= '$startdate[$counter]' AND date <= '$enddate[$counter]'";
                $result3 = Treat_DB_ProxyOldProcessHost::query($query3);
                $num3=Treat_DB_ProxyOldProcessHost::mysql_numrows($result3);
                //mysql_close();

		   $amount=Treat_DB_ProxyOldProcessHost::mysql_result($result3,0,"totalamt");
                   $rowtotal[$arrayname]=$rowtotal[$arrayname]+$amount;
                 
               }
               elseif ($invoicesales==1&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0&&$prepayment==0){
                  //mysql_connect($dbhost,$username,$password);
                  //@mysql_select_db($database) or die( "Unable to select database");
                  $query5 = "SELECT sum(total-taxtotal-servamt) as totalamt FROM invoice WHERE type = '1' AND companyid = '$companyid' AND businessid = '$busid' AND date >= '$startdate[$counter]' AND date <= '$enddate[$counter]' AND taxable = 'on' AND salescode = '0' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = Treat_DB_ProxyOldProcessHost::query($query5);
                  $num6=Treat_DB_ProxyOldProcessHost::mysql_numrows($result5);
                  //mysql_close();
                     $invtotal=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,"totalamt");
	               $amount=round($invtotal,2);
                     $rowtotal[$arrayname]=$rowtotal[$arrayname]+$amount;
                }
                elseif ($invoicesales==0&&$invoicesales_notax==1&&$invoicetax==0&&$servchrg==0&&$prepayment==0){
                  //mysql_connect($dbhost,$username,$password);
                  //@mysql_select_db($database) or die( "Unable to select database");
                  $query5 = "SELECT sum(total-servamt) as totalamt FROM invoice WHERE type = '1' AND companyid = '$companyid' AND businessid = '$busid' AND date >= '$startdate[$counter]' AND date <= '$enddate[$counter]' AND taxable = 'off' AND salescode = '0' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = Treat_DB_ProxyOldProcessHost::query($query5);
                  $num6=Treat_DB_ProxyOldProcessHost::mysql_numrows($result5);
                  //mysql_close();
                     $invtotal=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,"totalamt");
	             $amount=round($invtotal,2);
                     $rowtotal[$arrayname]=$rowtotal[$arrayname]+$amount;
                }
                elseif ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==1&&$servchrg==0&&$prepayment==0){
                  //mysql_connect($dbhost,$username,$password);
                  //@mysql_select_db($database) or die( "Unable to select database");
                  $query5 = "SELECT sum(taxtotal) as taxamt FROM invoice WHERE type = '1' AND companyid = '$companyid' AND businessid = '$busid'  AND date >= '$startdate[$counter]' AND date <= '$enddate[$counter]' AND taxable = 'on' AND salescode = '0' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = Treat_DB_ProxyOldProcessHost::query($query5);
                  $num6=Treat_DB_ProxyOldProcessHost::mysql_numrows($result5);
                  //mysql_close();
		     $taxtotal=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,"taxamt");
                     $taxtotal=round($taxtotal,2);
                     $rowtotal[$arrayname]=$rowtotal[$arrayname]+$taxtotal;
                }
                elseif ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==1&&$prepayment==0){
                  //mysql_connect($dbhost,$username,$password);
                  //@mysql_select_db($database) or die( "Unable to select database");
                  $query5 = "SELECT sum(servamt) as totalamt FROM invoice WHERE type = '1' AND companyid = '$companyid' AND businessid = '$busid'  AND date >= '$startdate[$counter]' AND date <= '$enddate[$counter]' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = Treat_DB_ProxyOldProcessHost::query($query5);
                  $num6=Treat_DB_ProxyOldProcessHost::mysql_numrows($result5);
                  //mysql_close();
		     $totalamt=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,"totalamt");
                     $totalamt=round($totalamt,2);
                     $rowtotal[$arrayname]=$rowtotal[$arrayname]+$totalamt;
                }

                $num2--;
             }
          }
          //////DEBITS
          elseif ($type==2){

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             if ($businessid>0){$query2 = "SELECT Sum(debitdetail.amount) AS totalamt FROM business INNER JOIN (debitdetail INNER JOIN debits ON debitdetail.debitid = debits.debitid) ON business.businessid = debitdetail.businessid WHERE debitdetail.date>='$startdate[$counter]' AND debitdetail.date<='$enddate[$counter]' AND debits.account LIKE '$reportquery' AND business.companyid='$companyid' and business.businessid = '$businessid'";}
             else if ($districtid!=""){ $query2 = "SELECT Sum(debitdetail.amount) AS totalamt FROM business INNER JOIN (debitdetail INNER JOIN debits ON debitdetail.debitid = debits.debitid) ON business.businessid = debitdetail.businessid WHERE debitdetail.date>='$startdate[$counter]' AND debitdetail.date<='$enddate[$counter]' AND debits.account LIKE '$reportquery' AND business.companyid='$companyid' AND business.districtid = '$districtid'";}
             else{$query2 = "SELECT Sum(debitdetail.amount) AS totalamt FROM business INNER JOIN (debitdetail INNER JOIN debits ON debitdetail.debitid = debits.debitid) ON business.businessid = debitdetail.businessid WHERE debitdetail.date>='$startdate[$counter]' AND debitdetail.date<='$enddate[$counter]' AND debits.account LIKE '$reportquery' AND business.companyid='$companyid'";}
             $result2 = Treat_DB_ProxyOldProcessHost::query($query2);
             $num2=Treat_DB_ProxyOldProcessHost::mysql_numrows($result2);
             //mysql_close();
                   $amount=Treat_DB_ProxyOldProcessHost::mysql_result($result2,0,"totalamt");
                   $rowtotal[$arrayname]=$rowtotal[$arrayname]+$amount;

          }
          //////AP
          elseif ($type==3){
             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             if ($businessid>0){$query2 = "SELECT Sum(apinvoicedetail.amount) AS totalamt FROM (apinvoicedetail INNER JOIN apinvoice ON apinvoicedetail.apinvoiceid = apinvoice.apinvoiceid) INNER JOIN acct_payable ON apinvoicedetail.apaccountid = acct_payable.acct_payableid WHERE apinvoice.date>='$startdate[$counter]' AND apinvoice.date<='$enddate[$counter]' AND acct_payable.companyid='$companyid' AND acct_payable.businessid = '$businessid' AND acct_payable.accountnum LIKE '$reportquery'";}
             else if ($districtid!=""){$query2 = "SELECT Sum(apinvoicedetail.amount) AS totalamt FROM business INNER JOIN ((apinvoicedetail INNER JOIN apinvoice ON apinvoicedetail.apinvoiceid = apinvoice.apinvoiceid) INNER JOIN acct_payable ON apinvoicedetail.apaccountid = acct_payable.acct_payableid) ON business.businessid = apinvoice.businessid WHERE apinvoice.date >='$startdate[$counter]' and apinvoice.date<='$enddate[$counter]' AND acct_payable.accountnum LIKE '$reportquery' AND acct_payable.companyid='$companyid' AND business.districtid='$districtid'";}
             else {$query2 = "SELECT Sum(apinvoicedetail.amount) AS totalamt FROM (apinvoicedetail INNER JOIN apinvoice ON apinvoicedetail.apinvoiceid = apinvoice.apinvoiceid) INNER JOIN acct_payable ON apinvoicedetail.apaccountid = acct_payable.acct_payableid WHERE apinvoice.date>='$startdate[$counter]' AND apinvoice.date<='$enddate[$counter]' AND acct_payable.companyid='$companyid' AND acct_payable.accountnum LIKE '$reportquery'";}
             $result2 = Treat_DB_ProxyOldProcessHost::query($query2);
             $num2=Treat_DB_ProxyOldProcessHost::mysql_numrows($result2);
             //mysql_close();

	     $amount=mysql_result($result2,0,"totalamt");

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             if ($businessid>0){$query2 = "SELECT Sum(apinvoicedetail.amount) AS totalamt FROM (apinvoicedetail INNER JOIN apinvoice ON apinvoicedetail.apinvoiceid = apinvoice.apinvoiceid) INNER JOIN acct_payable ON apinvoicedetail.tfr_acct = acct_payable.acct_payableid WHERE apinvoice.date>='$startdate[$counter]' AND apinvoice.date<='$enddate[$counter]' AND acct_payable.companyid='$companyid' AND acct_payable.businessid = '$businessid' AND acct_payable.accountnum LIKE '$reportquery'";}
             else if ($districtid!=""){$query2 = "SELECT Sum(apinvoicedetail.amount) AS totalamt FROM business INNER JOIN ((apinvoicedetail INNER JOIN apinvoice ON apinvoicedetail.apinvoiceid = apinvoice.apinvoiceid) INNER JOIN acct_payable ON apinvoicedetail.tfr_acct = acct_payable.acct_payableid) ON business.businessid = apinvoice.businessid WHERE apinvoice.date >='$startdate[$counter]' and apinvoice.date<='$enddate[$counter]' AND acct_payable.accountnum LIKE '$reportquery' AND acct_payable.companyid='$companyid' AND business.districtid='$districtid'";}
             else {$query2 = "SELECT Sum(apinvoicedetail.amount) AS totalamt FROM (apinvoicedetail INNER JOIN apinvoice ON apinvoicedetail.apinvoiceid = apinvoice.apinvoiceid) INNER JOIN acct_payable ON apinvoicedetail.tfr_acct = acct_payable.acct_payableid WHERE apinvoice.date>='$startdate[$counter]' AND apinvoice.date<='$enddate[$counter]' AND acct_payable.companyid='$companyid' AND acct_payable.accountnum LIKE '$reportquery'";}
             $result2 = Treat_DB_ProxyOldProcessHost::query($query2);
             $num2=Treat_DB_ProxyOldProcessHost::mysql_numrows($result2);
             //mysql_close();

             $amount2=Treat_DB_ProxyOldProcessHost::mysql_result($result2,0,"totalamt");
             $amount2=$amount2*-1;
		
             $rowtotal[$arrayname]=$amount2+$amount;

          }
          //////INVENTORY
          elseif ($type==4){
             if($period<3){$inventor="inventor2";}
             else{$inventor="inventor";}

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             if ($businessid>0){$query2 = "SELECT Sum($inventor.amount) AS totalamt FROM ($inventor INNER JOIN acct_payable ON $inventor.acct_payableid = acct_payable.acct_payableid) INNER JOIN business ON acct_payable.businessid = business.businessid WHERE acct_payable.accountnum LIKE '$reportquery' AND business.businessid='$businessid' AND business.companyid='$companyid' AND $inventor.date>='$startdate[$counter]' AND $inventor.date<='$enddate[$counter]'";}
             else if ($districtid!=""){ $query2 = "SELECT Sum($inventor.amount) AS totalamt FROM ($inventor INNER JOIN acct_payable ON $inventor.acct_payableid = acct_payable.acct_payableid) INNER JOIN business ON acct_payable.businessid = business.businessid WHERE acct_payable.accountnum LIKE '$reportquery' AND business.districtid='$districtid' AND business.companyid='$companyid' AND $inventor.date>='$startdate[$counter]' AND $inventor.date<='$enddate[$counter]'";}
             else{$query2 = "SELECT Sum($inventor.amount) AS totalamt FROM ($inventor INNER JOIN acct_payable ON $inventor.acct_payableid = acct_payable.acct_payableid) INNER JOIN business ON acct_payable.businessid = business.businessid WHERE acct_payable.accountnum LIKE '$reportquery' AND business.companyid='$companyid' AND $inventor.date>='$startdate[$counter]' AND $inventor.date<='$enddate[$counter]'";}
             $result2 = Treat_DB_ProxyOldProcessHost::query($query2);
             $num2=Treat_DB_ProxyOldProcessHost::mysql_numrows($result2);
             //mysql_close();
                   $amount=Treat_DB_ProxyOldProcessHost::mysql_result($result2,0,"totalamt");
                   $rowtotal[$arrayname]=$rowtotal[$arrayname]+$amount;

          }
          //////General Stats including date
          elseif ($type==7&&strstr($reportquery, "." )&&substr($reportquery,0,7)=="invoice"){

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             if ($businessid>0){$query2 = "SELECT Sum($reportquery) AS totalamt FROM invoice WHERE invoice.businessid='$businessid' AND invoice.date>='$startdate[$counter]' AND invoice.date<='$enddate[$counter]'";}
             else if ($districtid!=""){$query2 = "SELECT Sum($reportquery) AS totalamt FROM (invoice INNER JOIN business ON invoice.businessid = business.businessid)  WHERE business.districtid='$districtid' AND business.companyid='$companyid' AND invoice.date>='$startdate[$counter]' AND invoice.date<='$enddate[$counter]'";}
             else{$query2 = "SELECT Sum($reportquery) AS totalamt FROM invoice WHERE invoice.companyid='$companyid' AND invoice.date>='$startdate[$counter]' AND invoice.date<='$enddate[$counter]'";}
             $result2 = Treat_DB_ProxyOldProcessHost::query($query2);
             $num2=Treat_DB_ProxyOldProcessHost::mysql_numrows($result2);
             //mysql_close();
                   $amount=Treat_DB_ProxyOldProcessHost::mysql_result($result2,0,"totalamt");
                   $rowtotal[$arrayname]=$rowtotal[$arrayname]+$amount;

          }
          //////Vend Stats including date
          elseif ($type==7&&strstr($reportquery, "." )&&substr($reportquery,0,15)=="vend_collection"){

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             if ($businessid>0){$query2 = "SELECT Sum($reportquery) AS totalamt FROM vend_collection WHERE vend_collection.businessid='$businessid' AND vend_collection.date>='$startdate[$counter]' AND vend_collection.date<='$enddate[$counter]'";}
             else if ($districtid!=""){$query2 = "SELECT Sum($reportquery) AS totalamt FROM (vend_collection INNER JOIN business ON vend_collection.businessid = business.businessid)  WHERE business.districtid='$districtid' AND business.companyid='$companyid' AND vend_collection.date>='$startdate[$counter]' AND vend_collection.date<='$enddate[$counter]'";}
             else{$query2 = "SELECT Sum($reportquery) AS totalamt FROM vend_collection WHERE vend_collection.companyid='$companyid' AND vend_collection.date>='$startdate[$counter]' AND vend_collection.date<='$enddate[$counter]'";}
             $result2 = Treat_DB_ProxyOldProcessHost::query($query2);
             $num2=Treat_DB_ProxyOldProcessHost::mysql_numrows($result2);
             //mysql_close();
                   $amount=Treat_DB_ProxyOldProcessHost::mysql_result($result2,0,"totalamt");
                   $rowtotal[$arrayname]=$rowtotal[$arrayname]+$amount;

          }
          //////General Stats
          elseif ($type==7&&strstr($reportquery, "." )){

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             if ($businessid>0){$query2 = "SELECT Sum($reportquery) AS totalamt FROM business WHERE business.businessid='$businessid' AND business.companyid='$companyid'";}
             else if ($districtid!=""){$query2 = "SELECT Sum($reportquery) AS totalamt FROM business WHERE business.districtid='$districtid' AND business.companyid='$companyid'";}
             else{$query2 = "SELECT Sum($reportquery) AS totalamt FROM business WHERE business.companyid='$companyid'";}
             $result2 = Treat_DB_ProxyOldProcessHost::query($query2);
             $num2=Treat_DB_ProxyOldProcessHost::mysql_numrows($result2);
             //mysql_close();
                   $amount=Treat_DB_ProxyOldProcessHost::mysql_result($result2,0,"totalamt");
                   $rowtotal[$arrayname]=$rowtotal[$arrayname]+$amount;

          }
          /////STATS
          elseif ($type==7){

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             if ($businessid>0){$query2 = "SELECT Sum(stats.$reportquery) AS totalamt FROM stats INNER JOIN business ON stats.businessid = business.businessid WHERE stats.date>='$startdate[$counter]' AND stats.date<='$enddate[$counter]' AND business.businessid='$businessid' AND business.companyid='$companyid'";}
             else if ($districtid!=""){ $query2 = "SELECT Sum(stats.$reportquery) AS totalamt FROM stats INNER JOIN business ON stats.businessid = business.businessid WHERE stats.date>='$startdate[$counter]' AND stats.date<='$enddate[$counter]' AND business.districtid='$districtid' AND business.companyid='$companyid'";}
             else{$query2 = "SELECT Sum(stats.$reportquery) AS totalamt FROM stats INNER JOIN business ON stats.businessid = business.businessid WHERE stats.date>='$startdate[$counter]' AND stats.date<='$enddate[$counter]' AND business.companyid='$companyid'";}
             $result2 = Treat_DB_ProxyOldProcessHost::query($query2);
             $num2=Treat_DB_ProxyOldProcessHost::mysql_numrows($result2);
             //mysql_close();
                   $amount=Treat_DB_ProxyOldProcessHost::mysql_result($result2,0,"totalamt");
                   $rowtotal[$arrayname]=$rowtotal[$arrayname]+$amount;

          }
          //////Opertaing Days
          elseif ($type==8){
             $totalunit=0;
             $amount=dateDiff($startdate[$counter],$enddate[$counter])+1;
             $saturday=daysBetween($startdate[$counter], $enddate[$counter], "Saturday");
             $sunday=daysBetween($startdate[$counter], $enddate[$counter], "Sunday");

             if ($businessid>0){
                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query5 = "SELECT operate FROM business WHERE businessid = '$businessid'";
                $result5 = Treat_DB_ProxyOldProcessHost::query($query5);
                //mysql_close();

                $operate=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,"operate");

                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                if ($businessid>0){$query2 = "SELECT Sum(stats.closed) AS totalamt FROM stats INNER JOIN business ON stats.businessid = business.businessid WHERE stats.date>='$startdate[$counter]' AND stats.date<='$enddate[$counter]' AND business.businessid='$businessid' AND business.companyid='$companyid'";}
                else if ($districtid!=""){ $query2 = "SELECT Sum(stats.closed) AS totalamt FROM stats INNER JOIN business ON stats.businessid = business.businessid WHERE stats.date>='$startdate[$counter]' AND stats.date<='$enddate[$counter]' AND business.districtid='$districtid' AND business.companyid='$companyid'";}
                else{$query2 = "SELECT Sum(stats.closed) AS totalamt FROM stats INNER JOIN business ON stats.businessid = business.businessid WHERE stats.date>='$startdate[$counter]' AND stats.date<='$enddate[$counter]' AND business.companyid='$companyid'";}
                $result2 = Treat_DB_ProxyOldProcessHost::query($query2);
                $num2=Treat_DB_ProxyOldProcessHost::mysql_numrows($result2);
                //mysql_close();
                $closed=Treat_DB_ProxyOldProcessHost::mysql_result($result2,0,"totalamt");

                if ($operate==5){$rowtotal[$arrayname]=$amount-$saturday-$sunday-$closed;}
                elseif ($operate==6){$rowtotal[$arrayname]=$amount-$sunday-$closed;}
                elseif ($operate==7){$rowtotal[$arrayname]=$amount-$closed;}
             }
             elseif($districtid!=""){
                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query5 = "SELECT operate FROM business WHERE districtid = '$districtid' AND operate = '5'";
                $result5 = Treat_DB_ProxyOldProcessHost::query($query5);
                $num5=Treat_DB_ProxyOldProcessHost::mysql_numrows($result5);
                //mysql_close();

                $rowtotal[$arrayname]=($amount-$saturday-$sunday)*$num5;
                $totalunit=$num5;
 
                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query5 = "SELECT operate FROM business WHERE districtid = '$districtid' AND operate = '6'";
                $result5 = Treat_DB_ProxyOldProcessHost::query($query5);
                $num5=Treat_DB_ProxyOldProcessHost::mysql_numrows($result5);
                //mysql_close();

                $rowtotal[$arrayname]=$rowtotal[$arrayname]+($amount-$sunday)*$num5;
                $totalunit=$totalunit+$num5;

                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query5 = "SELECT operate FROM business WHERE districtid = '$districtid' AND operate = '7'";
                $result5 = Treat_DB_ProxyOldProcessHost::query($query5);
                $num5=Treat_DB_ProxyOldProcessHost::mysql_numrows($result5);
                //mysql_close();

                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                if ($businessid>0){$query2 = "SELECT Sum(stats.closed) AS totalamt FROM stats INNER JOIN business ON stats.businessid = business.businessid WHERE stats.date>='$startdate[$counter]' AND stats.date<='$enddate[$counter]' AND business.businessid='$businessid' AND business.companyid='$companyid'";}
                else if ($districtid!=""){ $query2 = "SELECT Sum(stats.closed) AS totalamt FROM stats INNER JOIN business ON stats.businessid = business.businessid WHERE stats.date>='$startdate[$counter]' AND stats.date<='$enddate[$counter]' AND business.districtid='$districtid' AND business.companyid='$companyid'";}
                else{$query2 = "SELECT Sum(stats.closed) AS totalamt FROM stats INNER JOIN business ON stats.businessid = business.businessid WHERE stats.date>='$startdate[$counter]' AND stats.date<='$enddate[$counter]' AND business.companyid='$companyid'";}
                $result2 = Treat_DB_ProxyOldProcessHost::query($query2);
                $num2=Treat_DB_ProxyOldProcessHost::mysql_numrows($result2);
                //mysql_close();
                $closed=Treat_DB_ProxyOldProcessHost::mysql_result($result2,0,"totalamt");

                $rowtotal[$arrayname]=$rowtotal[$arrayname]+($amount)*$num5-$closed;
                $totalunit=$totalunit+$num5;
                $rowtotal[$arrayname]=$rowtotal[$arrayname]/$totalunit;
             }
             else{
                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query5 = "SELECT operate FROM business WHERE companyid = '$companyid' AND operate = '5'";
                $result5 = Treat_DB_ProxyOldProcessHost::query($query5);
                $num5=Treat_DB_ProxyOldProcessHost::mysql_numrows($result5);
                //mysql_close();

                $rowtotal[$arrayname]=($amount-$saturday-$sunday)*$num5;
                $totalunit=$totalunit+$num5;
 
                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query5 = "SELECT operate FROM business WHERE companyid = '$companyid' AND operate = '6'";
                $result5 = Treat_DB_ProxyOldProcessHost::query($query5);
                $num5=Treat_DB_ProxyOldProcessHost::mysql_numrows($result5);
                //mysql_close();

                $rowtotal[$arrayname]=$rowtotal[$arrayname]+($amount-$sunday)*$num5;
                $totalunit=$totalunit+$num5;

                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query5 = "SELECT operate FROM business WHERE companyid = '$companyid' AND operate = '7'";
                $result5 = Treat_DB_ProxyOldProcessHost::query($query5);
                $num5=Treat_DB_ProxyOldProcessHost::mysql_numrows($result5);
                //mysql_close();

                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                if ($businessid>0){$query2 = "SELECT Sum(stats.closed) AS totalamt FROM stats INNER JOIN business ON stats.businessid = business.businessid WHERE stats.date>='$startdate[$counter]' AND stats.date<='$enddate[$counter]' AND business.businessid='$businessid' AND business.companyid='$companyid'";}
                else if ($districtid!=""){ $query2 = "SELECT Sum(stats.closed) AS totalamt FROM stats INNER JOIN business ON stats.businessid = business.businessid WHERE stats.date>='$startdate[$counter]' AND stats.date<='$enddate[$counter]' AND business.districtid='$districtid' AND business.companyid='$companyid'";}
                else{$query2 = "SELECT Sum(stats.closed) AS totalamt FROM stats INNER JOIN business ON stats.businessid = business.businessid WHERE stats.date>='$startdate[$counter]' AND stats.date<='$enddate[$counter]' AND business.companyid='$companyid'";}
                $result2 = Treat_DB_ProxyOldProcessHost::query($query2);
                $num2=Treat_DB_ProxyOldProcessHost::mysql_numrows($result2);
                //mysql_close();
                $closed=Treat_DB_ProxyOldProcessHost::mysql_result($result2,0,"totalamt");

                $rowtotal[$arrayname]=$rowtotal[$arrayname]+($amount)*$num5-$closed;
                $totalunit=$totalunit+$num5;
                $rowtotal[$arrayname]=$rowtotal[$arrayname]/$totalunit;
             }
          }
          ///////Labor Adjustment
          elseif ($type==9){
             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             if ($businessid>0){$query2 = "SELECT Sum(payroll.reg2+payroll.ot2) AS totalamt FROM business INNER JOIN payroll ON business.unit = payroll.unit_num WHERE payroll.date>='$startdate[$counter]' AND payroll.date<='$enddate[$counter]' AND business.businessid='$businessid' AND business.companyid='$companyid'";}
             else if ($districtid!=""){$query2 = "SELECT Sum(payroll.reg2+payroll.ot2) AS totalamt FROM business INNER JOIN payroll ON business.unit = payroll.unit_num WHERE payroll.date>='$startdate[$counter]' AND payroll.date<='$enddate[$counter]' AND business.districtid='$districtid' AND business.companyid='$companyid'";}
             else{$query2 = "SELECT Sum(payroll.reg2+payroll.ot2) AS totalamt FROM business INNER JOIN payroll ON business.unit = payroll.unit_num WHERE payroll.date>='$startdate[$counter]' AND payroll.date<='$enddate[$counter]' AND business.companyid='$companyid'";}
             $result2 = Treat_DB_ProxyOldProcessHost::query($query2);
             $num2=Treat_DB_ProxyOldProcessHost::mysql_numrows($result2);
             //mysql_close();
                   $amount=Treat_DB_ProxyOldProcessHost::mysql_result($result2,0,"totalamt");
                   $rowtotal[$arrayname]=$rowtotal[$arrayname]+$amount;
            

          }

          ///////Budget
          elseif ($type==10){
             $amount=0;

             if ($businessid>0){
                
                $query5 = "SELECT operate,unit FROM business WHERE businessid = '$businessid'";
                $result5 = Treat_DB_ProxyOldProcessHost::query($query5);

                $operate=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,"operate");
                $unit=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,"unit");

                $temp_date=$startdate[$counter];
                $numdays=0;
                while($temp_date<=$enddate[$counter]){
                   $amount=0;

                   $thismonth=substr($temp_date,5,2);
                   $thisyear=substr($temp_date,0,4);
                   $month_start=substr($temp_date,0,7);
                   $month_start="$month_start-01";

                   $query5 = "SELECT * FROM budget WHERE companyid = '$companyid' AND businessid = '$unit' AND year = '$thisyear' AND type = '$reportquery'";
                   $result5 = Treat_DB_ProxyOldProcessHost::query($query5);

                   $monthbudget=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,$thismonth);

                   $operatingdays=0;
                   while($thismonth==substr($month_start,5,2)){
                      $query5 = "SELECT closed FROM stats WHERE businessid = '$businessid' AND date = '$month_start'";
                      $result5 = Treat_DB_ProxyOldProcessHost::query($query5);

                      $closed=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,"closed");

                      if($operate==7&&$closed==0){$operatingdays++;}
                      elseif($operate==6&&$closed==0&&dayofweek($month_start)!="Sunday"){$operatingdays++;}
                      elseif($operate==5&&$closed==0&&dayofweek($month_start)!="Saturday"&&dayofweek($month_start)!="Sunday"){$operatingdays++;}

                      $month_start=nextday($month_start);
                   }
                   $numdays++;
                  
                   $amount+=($monthbudget/$operatingdays);
                   $temp_date=nextday($temp_date);
                }
                
             }
             elseif ($districtid!=""){

                $query55 = "SELECT business.businessid FROM business,district WHERE business.districtid = district.districtid AND district.districtid = '$districtid'";
                $result55 = Treat_DB_ProxyOldProcessHost::query($query55);
                $num55=Treat_DB_ProxyOldProcessHost::mysql_numrows($result55);
                
                $num55--;
                $amount=0;
                while($num55>=0){
                   $businessid=Treat_DB_ProxyOldProcessHost::mysql_result($result55,$num55,"business.businessid");

                   $query5 = "SELECT operate,unit FROM business WHERE businessid = '$businessid'";
                   $result5 = Treat_DB_ProxyOldProcessHost::query($query5);

                   $operate=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,"operate");
                   $unit=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,"unit");

                   $temp_date=$startdate[$counter];

                   while($temp_date<=$enddate[$counter]){

                      $thismonth=substr($temp_date,5,2);
                      $thisyear=substr($temp_date,0,4);
                      $month_start=substr($temp_date,0,7);
                      $month_start="$month_start-01";

                      $query5 = "SELECT * FROM budget WHERE companyid = '$companyid' AND businessid = '$unit' AND year = '$thisyear' AND type = '$reportquery'";
                      $result5 = Treat_DB_ProxyOldProcessHost::query($query5);

                      $monthbudget=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,$thismonth);

                      $operatingdays=0;
                      $numdays=0;
                      while($thismonth==substr($month_start,5,2)){
                         $query5 = "SELECT closed FROM stats WHERE businessid = '$businessid' AND date = '$month_start'";
                         $result5 = Treat_DB_ProxyOldProcessHost::query($query5);

                         $closed=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,"closed");

                         if($operate==7&&$closed==0){$operatingdays++;}
                         elseif($operate==6&&$closed==0&&dayofweek($month_start)!="Sunday"){$operatingdays++;}
                         elseif($operate==5&&$closed==0&&dayofweek($month_start)!="Saturday"&&dayofweek($month_start)!="Sunday"){$operatingdays++;}

                         $month_start=nextday($month_start);
                         $numdays++;
                      }
                      
                      $temp_date=nextday($temp_date);
                   }   
                   $amount+=($monthbudget/$operatingdays);
                   $num55--;
                }
                $businessid=0;
             }
             else{

                $query55 = "SELECT businessid FROM business WHERE companyid = '$companyid'";
                $result55 = Treat_DB_ProxyOldProcessHost::query($query55);
                $num55=Treat_DB_ProxyOldProcessHost::mysql_numrows($result55);
                
                $num55--;
                $amount=0;
                while($num55>=0){
                   $businessid=Treat_DB_ProxyOldProcessHost::mysql_result($result55,$num55,"business.businessid");

                   $query5 = "SELECT operate,unit FROM business WHERE businessid = '$businessid'";
                   $result5 = Treat_DB_ProxyOldProcessHost::query($query5);

                   $operate=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,"operate");
                   $unit=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,"unit");

                   $temp_date=$startdate[$counter];

                   while($temp_date<=$enddate[$counter]){

                      $thismonth=substr($temp_date,5,2);
                      $thisyear=substr($temp_date,0,4);
                      $month_start=substr($temp_date,0,7);
                      $month_start="$month_start-01";

                      $query5 = "SELECT * FROM budget WHERE companyid = '$companyid' AND businessid = '$unit' AND year = '$thisyear' AND type = '$reportquery'";
                      $result5 = Treat_DB_ProxyOldProcessHost::query($query5);

                      $monthbudget=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,$thismonth);

                      $operatingdays=0;
                      $numdays=0;
                      while($thismonth==substr($month_start,5,2)){
                         $query5 = "SELECT closed FROM stats WHERE businessid = '$businessid' AND date = '$month_start'";
                         $result5 = Treat_DB_ProxyOldProcessHost::query($query5);

                         $closed=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,"closed");

                         if($operate==7&&$closed==0){$operatingdays++;}
                         elseif($operate==6&&$closed==0&&dayofweek($month_start)!="Sunday"){$operatingdays++;}
                         elseif($operate==5&&$closed==0&&dayofweek($month_start)!="Saturday"&&dayofweek($month_start)!="Sunday"){$operatingdays++;}

                         $month_start=nextday($month_start);
                         $numdays++;
                      }
                      
                      $temp_date=nextday($temp_date);
                   }
                   $amount+=($monthbudget/$operatingdays);
                   $num55--;
                }
                $businessid=0;
             }

             $rowtotal[$arrayname]=$rowtotal[$arrayname]+$amount;
             
          }

          ///////Recur
          elseif ($type==11){
             $amount=0;

             if ($businessid>0){
                
                $query5 = "SELECT operate,unit FROM business WHERE businessid = '$businessid'";
                $result5 = Treat_DB_ProxyOldProcessHost::query($query5);

                $operate=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,"operate");
                $unit=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,"unit");

                $temp_date=$startdate[$counter];
                $numdays=0;
                while($temp_date<=$enddate[$counter]){
                   $amount=0;

                   $thismonth=substr($temp_date,5,2);
                   $thisyear=substr($temp_date,0,4);
                   $month_start=substr($temp_date,0,7);
                   $month_start="$month_start-01";

                   $query5 = "SELECT * FROM recur WHERE businessid = '$unit' AND year = '$thisyear' AND acct_num LIKE '$reportquery'";
                   $result5 = Treat_DB_ProxyOldProcessHost::query($query5);

                   $monthbudget=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,$thismonth);

                   $operatingdays=0;
                   while($thismonth==substr($month_start,5,2)){
                      $query5 = "SELECT closed FROM stats WHERE businessid = '$businessid' AND date = '$month_start'";
                      $result5 = Treat_DB_ProxyOldProcessHost::query($query5);

                      $closed=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,"closed");

                      if($operate==7&&$closed==0){$operatingdays++;}
                      elseif($operate==6&&$closed==0&&dayofweek($month_start)!="Sunday"){$operatingdays++;}
                      elseif($operate==5&&$closed==0&&dayofweek($month_start)!="Saturday"&&dayofweek($month_start)!="Sunday"){$operatingdays++;}

                      $month_start=nextday($month_start);
                   }
                   $numdays++;
                  
                   $amount+=($monthbudget/$operatingdays);
                   $temp_date=nextday($temp_date);
                }
                
             }
             elseif ($districtid!=""){

                $query55 = "SELECT business.businessid FROM business,district WHERE business.districtid = district.districtid AND district.districtid = '$districtid'";
                $result55 = Treat_DB_ProxyOldProcessHost::query($query55);
                $num55=Treat_DB_ProxyOldProcessHost::mysql_numrows($result55);
                
                $num55--;
                $amount=0;
                while($num55>=0){
                   $businessid=Treat_DB_ProxyOldProcessHost::mysql_result($result55,$num55,"business.businessid");

                   $query5 = "SELECT operate,unit FROM business WHERE businessid = '$businessid'";
                   $result5 = Treat_DB_ProxyOldProcessHost::query($query5);

                   $operate=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,"operate");
                   $unit=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,"unit");

                   $temp_date=$startdate[$counter];

                   while($temp_date<=$enddate[$counter]){

                      $thismonth=substr($temp_date,5,2);
                      $thisyear=substr($temp_date,0,4);
                      $month_start=substr($temp_date,0,7);
                      $month_start="$month_start-01";

                      $query5 = "SELECT * FROM budget WHERE businessid = '$unit' AND year = '$thisyear' AND type = '$reportquery'";
                      $result5 = Treat_DB_ProxyOldProcessHost::query($query5);

                      $monthbudget=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,$thismonth);

                      $operatingdays=0;
                      $numdays=0;
                      while($thismonth==substr($month_start,5,2)){
                         $query5 = "SELECT closed FROM stats WHERE businessid = '$businessid' AND date = '$month_start'";
                         $result5 = Treat_DB_ProxyOldProcessHost::query($query5);

                         $closed=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,"closed");

                         if($operate==7&&$closed==0){$operatingdays++;}
                         elseif($operate==6&&$closed==0&&dayofweek($month_start)!="Sunday"){$operatingdays++;}
                         elseif($operate==5&&$closed==0&&dayofweek($month_start)!="Saturday"&&dayofweek($month_start)!="Sunday"){$operatingdays++;}

                         $month_start=nextday($month_start);
                         $numdays++;
                      }
                      
                      $temp_date=nextday($temp_date);
                   }   
                   $amount+=($monthbudget/$operatingdays);
                   $num55--;
                }
                $businessid=0;
             }
             else{

                $query55 = "SELECT businessid FROM business WHERE companyid = '$companyid'";
                $result55 = Treat_DB_ProxyOldProcessHost::query($query55);
                $num55=Treat_DB_ProxyOldProcessHost::mysql_numrows($result55);
                
                $num55--;
                $amount=0;
                while($num55>=0){
                   $businessid=Treat_DB_ProxyOldProcessHost::mysql_result($result55,$num55,"business.businessid");

                   $query5 = "SELECT operate,unit FROM business WHERE businessid = '$businessid'";
                   $result5 = Treat_DB_ProxyOldProcessHost::query($query5);

                   $operate=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,"operate");
                   $unit=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,"unit");

                   $temp_date=$startdate[$counter];

                   while($temp_date<=$enddate[$counter]){

                      $thismonth=substr($temp_date,5,2);
                      $thisyear=substr($temp_date,0,4);
                      $month_start=substr($temp_date,0,7);
                      $month_start="$month_start-01";

                      $query5 = "SELECT * FROM budget WHERE businessid = '$unit' AND year = '$thisyear' AND type = '$reportquery'";
                      $result5 = Treat_DB_ProxyOldProcessHost::query($query5);

                      $monthbudget=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,$thismonth);

                      $operatingdays=0;
                      $numdays=0;
                      while($thismonth==substr($month_start,5,2)){
                         $query5 = "SELECT closed FROM stats WHERE businessid = '$businessid' AND date = '$month_start'";
                         $result5 = Treat_DB_ProxyOldProcessHost::query($query5);

                         $closed=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,"closed");

                         if($operate==7&&$closed==0){$operatingdays++;}
                         elseif($operate==6&&$closed==0&&dayofweek($month_start)!="Sunday"){$operatingdays++;}
                         elseif($operate==5&&$closed==0&&dayofweek($month_start)!="Saturday"&&dayofweek($month_start)!="Sunday"){$operatingdays++;}

                         $month_start=nextday($month_start);
                         $numdays++;
                      }
                      
                      $temp_date=nextday($temp_date);
                   }
                   $amount+=($monthbudget/$operatingdays);
                   $num55--;
                }
                $businessid=0;
             }

             $rowtotal[$arrayname]=$rowtotal[$arrayname]+$amount;
             
          }

          ///////Benefit%
          elseif ($type==12){
             if($businessid>0){
                $query2 = "SELECT benefit FROM business WHERE businessid = '$businessid'";
                $result2 = Treat_DB_ProxyOldProcessHost::query($query2);
                $num2=Treat_DB_ProxyOldProcessHost::mysql_numrows($result2);

                $amount=Treat_DB_ProxyOldProcessHost::mysql_result($result2,0,"benefit");
             }
             elseif($districtid!=""){
                $query2 = "SELECT avg(benefit) AS totalamt FROM business WHERE districtid = '$districtid'";
                $result2 = Treat_DB_ProxyOldProcessHost::query($query2);
                $num2=Treat_DB_ProxyOldProcessHost::mysql_numrows($result2);

                $amount=Treat_DB_ProxyOldProcessHost::mysql_result($result2,0,"totalamt");
             }
             elseif($companyid!=""){
                $query2 = "SELECT avg(benefit) AS totalamt FROM business WHERE companyid = '$companyid'";
                $result2 = Treat_DB_ProxyOldProcessHost::query($query2);
                $num2=Treat_DB_ProxyOldProcessHost::mysql_numrows($result2);

                $amount=Treat_DB_ProxyOldProcessHost::mysql_result($result2,0,"totalamt");
             }
                   
                   $rowtotal[$arrayname]=$rowtotal[$arrayname]+$amount;
            

          }

          //////FORMULA
          elseif ($type==6){
             $qlength=strlen($reportquery);
             $qcount=0;
             $formcount=0;
             $formarray=array();
             $pos1=0;
             $qchar=substr($reportquery,$qcount,1);
             while($qcount<=$qlength){
                $pos2=0;
                while($qchar!="+"&&$qchar!="-"&&$qchar!="*"&&$qchar!="/"&&$qchar!="~"&&$qcount<=$qlength){
                   $qcount++;
                   $qchar=substr($reportquery,$qcount,1);
                   $pos2++;
                }
                $formarray[$formcount]=substr($reportquery,$pos1,$pos2);
                $formcount++;
                if ($qchar!=""){
                   $formarray[$formcount]=$qchar;
                   $formcount++;
                }
                $pos1=$qcount+1;
                $qcount++;
                $qchar=substr($reportquery,$qcount,1);
             }

             $arrayname2="$formarray[0].$counter";
             if (substr($formarray[0],0,1)=="["){$qlength=strlen($formarray[0]);$rowtotal[$arrayname]=substr($formarray[0],1,$qlength-2);}
             else{$rowtotal[$arrayname]=$rowtotal[$arrayname2];}
             $mycount=1;
             while($mycount<=$formcount){
                $mycount2=$mycount+1;
                $arrayname2="$formarray[$mycount2].$counter";
                if ($formarray[$mycount]=="+"){
                   if (substr($formarray[$mycount2],0,1)=="["){$qlength=strlen($formarray[$mycount2]);$rowtotal[$arrayname]=$rowtotal[$arrayname]+substr($formarray[$mycount2],1,$qlength-2);}
                   else{$rowtotal[$arrayname]=$rowtotal[$arrayname]+$rowtotal[$arrayname2];}
                }
                elseif ($formarray[$mycount]=="-"){
                   if (substr($formarray[$mycount2],0,1)=="["){$qlength=strlen($formarray[$mycount2]);$rowtotal[$arrayname]=$rowtotal[$arrayname]-substr($formarray[$mycount2],1,$qlength-2);}
                   else{$rowtotal[$arrayname]=$rowtotal[$arrayname]-$rowtotal[$arrayname2];}
                }
                elseif ($formarray[$mycount]=="*"){
                   if (substr($formarray[$mycount2],0,1)=="["){$qlength=strlen($formarray[$mycount2]);$rowtotal[$arrayname]=$rowtotal[$arrayname]*substr($formarray[$mycount2],1,$qlength-2);}
                   else{$rowtotal[$arrayname]=$rowtotal[$arrayname]*$rowtotal[$arrayname2];}
                }
                elseif ($formarray[$mycount]=="/"){
                   if (substr($formarray[$mycount2],0,1)=="["){$qlength=strlen($formarray[$mycount2]);$rowtotal[$arrayname]=$rowtotal[$arrayname]/substr($formarray[$mycount2],1,$qlength-2);}
                   else{$rowtotal[$arrayname]=$rowtotal[$arrayname]/$rowtotal[$arrayname2];}
                }
                elseif ($formarray[$mycount]=="~"){
                   $starthere=$formarray[$mycount-1];
                   $endhere=$formarray[$mycount+1];
                   $rowtotal[$arrayname]=0;
                   while($starthere<=$endhere){
                      $startarray="$starthere.$counter";
                      $rowtotal[$arrayname]=$rowtotal[$arrayname]+$rowtotal[$startarray];
                      $starthere++;
                   }
                }
                $mycount=$mycount+2;
             }
          }

          if ($rowtotal[$arrayname]==""){$rowtotal[$arrayname]=0;}
          $showrow=money($rowtotal[$arrayname]);
          if($hide==0){$data="$data$showrow,";}
          $showrow=number_format($rowtotal[$arrayname],$decimal);
          if ($bold==1&&$hide==0){echo "<td align=right bgcolor=#E8E7E7><font size=2><b>$showrow$suffix</b></td>";}
          elseif($hide==0){echo "<td align=right><font size=2>$showrow</td>";}
 
          }////END PERIOD LOOP
          $coltotal=0;
          if ($avg==0){for ($counter=1;$counter<=$periodnum;$counter++){$arrayname="$counter2.$counter";$coltotal=$coltotal+$rowtotal[$arrayname];}}
          elseif ($avg==1){for ($counter=1;$counter<=$periodnum;$counter++){$arrayname="$counter2.$counter";$coltotal=$coltotal+$rowtotal[$arrayname];}$coltotal=$coltotal/$periodnum;}
          $coltotal=money($coltotal);
          if($hide==0){$data="$data$coltotal\r\n";}
          $colshow=number_format($coltotal,$decimal);
          if ($hide==0){echo "<td align=right bgcolor=#E8E7E7><font size=2><b>$colshow$suffix</td></tr>";}
          $num--;
          $counter2++;
       }
    }
  }
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////SEPERATE REPORT/////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////

if ($seperate==1&&($businessid<0||$businessid=="")){
   //mysql_connect($dbhost,$username,$password);
   //@mysql_select_db($database) or die( "Unable to select database");
   if($businessid==""){$query88 = "SELECT * FROM business WHERE companyid = '$companyid' ORDER BY unit DESC";}
   else{$query88 = "SELECT * FROM business WHERE districtid = '$districtid' ORDER BY unit DESC";}
   $result88 = Treat_DB_ProxyOldProcessHost::query($query88);
   $num88=Treat_DB_ProxyOldProcessHost::mysql_numrows($result88);
   //mysql_close();

   $num88--;
   while($num88>=0){
       ////////////////////////////////////////////////
       ///////////////REPEAT REPORTS///////////////////
       ////////////////////////////////////////////////
       $busname=Treat_DB_ProxyOldProcessHost::mysql_result($result88,$num88,"businessname");
       $businessid=Treat_DB_ProxyOldProcessHost::mysql_result($result88,$num88,"businessid");
       $unit_num=Treat_DB_ProxyOldProcessHost::mysql_result($result88,$num88,"unit");
	   $categoryid = Treat_DB_ProxyOldProcessHost::mysql_result($result88,$num88,"categoryid");
       $counter2=1;
       $totaldays=0;

       for ($counter=1;$counter<=$periodnum;$counter++){$showday=substr($startdate[$counter],8,2);$showmonth=substr($startdate[$counter],5,2);$showyear=substr($startdate[$counter],2,2);}

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM reportdetail WHERE reportid = '$curreportid' ORDER BY orderid DESC";
       $result = Treat_DB_ProxyOldProcessHost::query($query);
       $num=Treat_DB_ProxyOldProcessHost::mysql_numrows($result);
       //mysql_close();

       $num--;
       ///////////////////////MAIN REPORT DETAIL LOOP
       $rowtotal=array();
       while($num>=0){

          $detailid=Treat_DB_ProxyOldProcessHost::mysql_result($result,$num,"reportdetailid");
          $detailname=Treat_DB_ProxyOldProcessHost::mysql_result($result,$num,"name");
          $type=Treat_DB_ProxyOldProcessHost::mysql_result($result,$num,"type");
          $reportquery=Treat_DB_ProxyOldProcessHost::mysql_result($result,$num,"query");
          $orderid=Treat_DB_ProxyOldProcessHost::mysql_result($result,$num,"orderid");
          $avg=Treat_DB_ProxyOldProcessHost::mysql_result($result,$num,"avg");
          $bold=Treat_DB_ProxyOldProcessHost::mysql_result($result,$num,"bold");
          $hide=Treat_DB_ProxyOldProcessHost::mysql_result($result,$num,"hide");
          $decimal=Treat_DB_ProxyOldProcessHost::mysql_result($result,$num,"num_decimal");
          $suffix=Treat_DB_ProxyOldProcessHost::mysql_result($result,$num,"suffix");
          $wtdb=Treat_DB_ProxyOldProcessHost::mysql_result($result,$num,"wtdb");
          $table=Treat_DB_ProxyOldProcessHost::mysql_result($result,$num,"table");
          $field=Treat_DB_ProxyOldProcessHost::mysql_result($result,$num,"field");

          if ($bold==1&&$hide==0){echo "<tr><td bgcolor=#E8E7E7><font size=2>$unit_num $busname $detailname</td>";}
          elseif($hide==0){echo "<tr><td><font size=2>$unit_num $busname $detailname</td>";}
          $filedata=str_replace(",","/",$detailname);
          $filedata="$unit_num $busname $detailname";
          if($hide==0){$data="$data$filedata,";}
          for ($counter=1;$counter<=$periodnum;$counter++){
          $arrayname="$counter2.$counter";

          ////CREDITS
          if ($type==1){
             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             if ($businessid>0){$query2 = "SELECT * FROM credits WHERE account LIKE '$reportquery' AND companyid = '$companyid' AND businessid = '$businessid'";}
             else if ($districtid!=""){ $query2 = "SELECT credits.* FROM district INNER JOIN (business INNER JOIN credits ON business.businessid = credits.businessid) ON district.districtid = business.districtid WHERE district.districtid='$districtid' AND credits.companyid='$companyid' AND credits.account LIKE '$reportquery'";}
             else{$query2 = "SELECT * FROM credits WHERE account LIKE '$reportquery' AND companyid = '$companyid'";}
             $result2 = Treat_DB_ProxyOldProcessHost::query($query2);
             $num2=Treat_DB_ProxyOldProcessHost::mysql_numrows($result2);
             //mysql_close();

             $num2--;
             while($num2>=0){
                if ($districtid!=""){
                $creditid=Treat_DB_ProxyOldProcessHost::mysql_result($result2,$num2,"credits.creditid");
                $invoicesales=Treat_DB_ProxyOldProcessHost::mysql_result($result2,$num2,"credits.invoicesales");
                $invoicesales_notax=Treat_DB_ProxyOldProcessHost::mysql_result($result2,$num2,"credits.invoicesales_notax");
                $invoicetax=Treat_DB_ProxyOldProcessHost::mysql_result($result2,$num2,"credits.invoicetax");
                $servchrg=Treat_DB_ProxyOldProcessHost::mysql_result($result2,$num2,"credits.servchrg");
                $salescode=Treat_DB_ProxyOldProcessHost::mysql_result($result2,$num2,"credits.salescode");
                $busid=Treat_DB_ProxyOldProcessHost::mysql_result($result2,$num2,"credits.businessid");
	        }
               else{
                $creditid=Treat_DB_ProxyOldProcessHost::mysql_result($result2,$num2,"creditid");
                $invoicesales=Treat_DB_ProxyOldProcessHost::mysql_result($result2,$num2,"invoicesales");
                $invoicesales_notax=Treat_DB_ProxyOldProcessHost::mysql_result($result2,$num2,"invoicesales_notax");
                $invoicetax=Treat_DB_ProxyOldProcessHost::mysql_result($result2,$num2,"invoicetax");
                $servchrg=Treat_DB_ProxyOldProcessHost::mysql_result($result2,$num2,"servchrg");
                $salescode=Treat_DB_ProxyOldProcessHost::mysql_result($result2,$num2,"salescode");
                $busid=Treat_DB_ProxyOldProcessHost::mysql_result($result2,$num2,"businessid");
   		
		}
               if ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0&&$prepayment==0){
                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query3 = "SELECT sum(amount) as totalamt FROM creditdetail WHERE creditid = '$creditid' AND date >= '$startdate[$counter]' AND date <= '$enddate[$counter]'";
                $result3 = Treat_DB_ProxyOldProcessHost::query($query3);
                $num3=Treat_DB_ProxyOldProcessHost::mysql_numrows($result3);
                //mysql_close();

		   $amount=Treat_DB_ProxyOldProcessHost::mysql_result($result3,0,"totalamt");
                   $rowtotal[$arrayname]=$rowtotal[$arrayname]+$amount;
                 
               }
               elseif ($invoicesales==1&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0&&$prepayment==0){
                  //mysql_connect($dbhost,$username,$password);
                  //@mysql_select_db($database) or die( "Unable to select database");
                  $query5 = "SELECT sum(total-taxtotal-servamt) as totalamt FROM invoice WHERE type = '1' AND companyid = '$companyid' AND businessid = '$busid' AND date >= '$startdate[$counter]' AND date <= '$enddate[$counter]' AND taxable = 'on' AND salescode = '0' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = Treat_DB_ProxyOldProcessHost::query($query5);
                  $num6=Treat_DB_ProxyOldProcessHost::mysql_numrows($result5);
                  //mysql_close();
                     $invtotal=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,"totalamt");
	               $amount=round($invtotal,2);
                     $rowtotal[$arrayname]=$rowtotal[$arrayname]+$amount;
                }
                elseif ($invoicesales==0&&$invoicesales_notax==1&&$invoicetax==0&&$servchrg==0&&$prepayment==0){
                  //mysql_connect($dbhost,$username,$password);
                  //@mysql_select_db($database) or die( "Unable to select database");
                  $query5 = "SELECT sum(total-servamt) as totalamt FROM invoice WHERE type = '1' AND companyid = '$companyid' AND businessid = '$busid' AND date >= '$startdate[$counter]' AND date <= '$enddate[$counter]' AND taxable = 'off' AND salescode = '0' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = Treat_DB_ProxyOldProcessHost::query($query5);
                  $num6=Treat_DB_ProxyOldProcessHost::mysql_numrows($result5);
                  //mysql_close();
                     $invtotal=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,"totalamt");
	             $amount=round($invtotal,2);
                     $rowtotal[$arrayname]=$rowtotal[$arrayname]+$amount;
                }
                elseif ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==1&&$servchrg==0&&$prepayment==0){
                  //mysql_connect($dbhost,$username,$password);
                  //@mysql_select_db($database) or die( "Unable to select database");
                  $query5 = "SELECT sum(taxtotal) as taxamt FROM invoice WHERE type = '1' AND companyid = '$companyid' AND businessid = '$busid'  AND date >= '$startdate[$counter]' AND date <= '$enddate[$counter]' AND taxable = 'on' AND salescode = '0' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = Treat_DB_ProxyOldProcessHost::query($query5);
                  $num6=Treat_DB_ProxyOldProcessHost::mysql_numrows($result5);
                  //mysql_close();
		     $taxtotal=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,"taxamt");
                     $taxtotal=round($taxtotal,2);
                     $rowtotal[$arrayname]=$rowtotal[$arrayname]+$taxtotal;
                }
                elseif ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==1&&$prepayment==0){
                  //mysql_connect($dbhost,$username,$password);
                  //@mysql_select_db($database) or die( "Unable to select database");
                  $query5 = "SELECT sum(servamt) as totalamt FROM invoice WHERE type = '1' AND companyid = '$companyid' AND businessid = '$busid'  AND date >= '$startdate[$counter]' AND date <= '$enddate[$counter]' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = Treat_DB_ProxyOldProcessHost::query($query5);
                  $num6=Treat_DB_ProxyOldProcessHost::mysql_numrows($result5);
                  //mysql_close();
		     $totalamt=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,"totalamt");
                     $totalamt=round($totalamt,2);
                     $rowtotal[$arrayname]=$rowtotal[$arrayname]+$totalamt;
                }

                $num2--;
             }
          }
          //////DEBITS
          elseif ($type==2){

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             if ($businessid>0){$query2 = "SELECT Sum(debitdetail.amount) AS totalamt FROM business INNER JOIN (debitdetail INNER JOIN debits ON debitdetail.debitid = debits.debitid) ON business.businessid = debitdetail.businessid WHERE debitdetail.date>='$startdate[$counter]' AND debitdetail.date<='$enddate[$counter]' AND debits.account LIKE '$reportquery' AND business.companyid='$companyid' and business.businessid = '$businessid'";}
             else if ($districtid!=""){ $query2 = "SELECT Sum(debitdetail.amount) AS totalamt FROM business INNER JOIN (debitdetail INNER JOIN debits ON debitdetail.debitid = debits.debitid) ON business.businessid = debitdetail.businessid WHERE debitdetail.date>='$startdate[$counter]' AND debitdetail.date<='$enddate[$counter]' AND debits.account LIKE '$reportquery' AND business.companyid='$companyid' AND business.districtid = '$districtid'";}
             else{$query2 = "SELECT Sum(debitdetail.amount) AS totalamt FROM business INNER JOIN (debitdetail INNER JOIN debits ON debitdetail.debitid = debits.debitid) ON business.businessid = debitdetail.businessid WHERE debitdetail.date>='$startdate[$counter]' AND debitdetail.date<='$enddate[$counter]' AND debits.account LIKE '$reportquery' AND business.companyid='$companyid'";}
             $result2 = Treat_DB_ProxyOldProcessHost::query($query2);
             $num2=Treat_DB_ProxyOldProcessHost::mysql_numrows($result2);
             //mysql_close();
                   $amount=Treat_DB_ProxyOldProcessHost::mysql_result($result2,0,"totalamt");
                   $rowtotal[$arrayname]=$rowtotal[$arrayname]+$amount;

          }
          //////AP
          elseif ($type==3){
             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             if ($businessid>0){$query2 = "SELECT Sum(apinvoicedetail.amount) AS totalamt FROM (apinvoicedetail INNER JOIN apinvoice ON apinvoicedetail.apinvoiceid = apinvoice.apinvoiceid) INNER JOIN acct_payable ON apinvoicedetail.apaccountid = acct_payable.acct_payableid WHERE apinvoice.date>='$startdate[$counter]' AND apinvoice.date<='$enddate[$counter]' AND acct_payable.companyid='$companyid' AND acct_payable.businessid = '$businessid' AND acct_payable.accountnum LIKE '$reportquery'";}
             else if ($districtid!=""){$query2 = "SELECT Sum(apinvoicedetail.amount) AS totalamt FROM business INNER JOIN ((apinvoicedetail INNER JOIN apinvoice ON apinvoicedetail.apinvoiceid = apinvoice.apinvoiceid) INNER JOIN acct_payable ON apinvoicedetail.apaccountid = acct_payable.acct_payableid) ON business.businessid = apinvoice.businessid WHERE apinvoice.date >='$startdate[$counter]' and apinvoice.date<='$enddate[$counter]' AND acct_payable.accountnum LIKE '$reportquery' AND acct_payable.companyid='$companyid' AND business.districtid='$districtid'";}
             else {$query2 = "SELECT Sum(apinvoicedetail.amount) AS totalamt FROM (apinvoicedetail INNER JOIN apinvoice ON apinvoicedetail.apinvoiceid = apinvoice.apinvoiceid) INNER JOIN acct_payable ON apinvoicedetail.apaccountid = acct_payable.acct_payableid WHERE apinvoice.date>='$startdate[$counter]' AND apinvoice.date<='$enddate[$counter]' AND acct_payable.companyid='$companyid' AND acct_payable.accountnum LIKE '$reportquery'";}
             $result2 = Treat_DB_ProxyOldProcessHost::query($query2);
             $num2=Treat_DB_ProxyOldProcessHost::mysql_numrows($result2);
             //mysql_close();

	     $amount=Treat_DB_ProxyOldProcessHost::mysql_result($result2,0,"totalamt");

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             if ($businessid>0){$query2 = "SELECT Sum(apinvoicedetail.amount) AS totalamt FROM (apinvoicedetail INNER JOIN apinvoice ON apinvoicedetail.apinvoiceid = apinvoice.apinvoiceid) INNER JOIN acct_payable ON apinvoicedetail.tfr_acct = acct_payable.acct_payableid WHERE apinvoice.date>='$startdate[$counter]' AND apinvoice.date<='$enddate[$counter]' AND acct_payable.companyid='$companyid' AND acct_payable.businessid = '$businessid' AND acct_payable.accountnum LIKE '$reportquery'";}
             else if ($districtid!=""){$query2 = "SELECT Sum(apinvoicedetail.amount) AS totalamt FROM business INNER JOIN ((apinvoicedetail INNER JOIN apinvoice ON apinvoicedetail.apinvoiceid = apinvoice.apinvoiceid) INNER JOIN acct_payable ON apinvoicedetail.tfr_acct = acct_payable.acct_payableid) ON business.businessid = apinvoice.businessid WHERE apinvoice.date >='$startdate[$counter]' and apinvoice.date<='$enddate[$counter]' AND acct_payable.accountnum LIKE '$reportquery' AND acct_payable.companyid='$companyid' AND business.districtid='$districtid'";}
             else {$query2 = "SELECT Sum(apinvoicedetail.amount) AS totalamt FROM (apinvoicedetail INNER JOIN apinvoice ON apinvoicedetail.apinvoiceid = apinvoice.apinvoiceid) INNER JOIN acct_payable ON apinvoicedetail.tfr_acct = acct_payable.acct_payableid WHERE apinvoice.date>='$startdate[$counter]' AND apinvoice.date<='$enddate[$counter]' AND acct_payable.companyid='$companyid' AND acct_payable.accountnum LIKE '$reportquery'";}
             $result2 = Treat_DB_ProxyOldProcessHost::query($query2);
             $num2=Treat_DB_ProxyOldProcessHost::mysql_numrows($result2);
             //mysql_close();

             $amount2=Treat_DB_ProxyOldProcessHost::mysql_result($result2,0,"totalamt");
             $amount2=$amount2*-1;
		
             $rowtotal[$arrayname]=$amount2+$amount;

          }
          //////INVENTORY
          elseif ($type==4){
             if($period<3){$inventor="inventor2";}
             else{$inventor="inventor";}

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             if ($businessid>0){$query2 = "SELECT Sum($inventor.amount) AS totalamt FROM ($inventor INNER JOIN acct_payable ON $inventor.acct_payableid = acct_payable.acct_payableid) INNER JOIN business ON acct_payable.businessid = business.businessid WHERE acct_payable.accountnum LIKE '$reportquery' AND business.businessid='$businessid' AND business.companyid='$companyid' AND $inventor.date>='$startdate[$counter]' AND $inventor.date<='$enddate[$counter]'";}
             else if ($districtid!=""){ $query2 = "SELECT Sum($inventor.amount) AS totalamt FROM ($inventor INNER JOIN acct_payable ON $inventor.acct_payableid = acct_payable.acct_payableid) INNER JOIN business ON acct_payable.businessid = business.businessid WHERE acct_payable.accountnum LIKE '$reportquery' AND business.districtid='$districtid' AND business.companyid='$companyid' AND $inventor.date>='$startdate[$counter]' AND $inventor.date<='$enddate[$counter]'";}
             else{$query2 = "SELECT Sum($inventor.amount) AS totalamt FROM ($inventor INNER JOIN acct_payable ON $inventor.acct_payableid = acct_payable.acct_payableid) INNER JOIN business ON acct_payable.businessid = business.businessid WHERE acct_payable.accountnum LIKE '$reportquery' AND business.companyid='$companyid' AND $inventor.date>='$startdate[$counter]' AND $inventor.date<='$enddate[$counter]'";}
             $result2 = Treat_DB_ProxyOldProcessHost::query($query2);
             $num2=Treat_DB_ProxyOldProcessHost::mysql_numrows($result2);
             //mysql_close();
                   $amount=Treat_DB_ProxyOldProcessHost::mysql_result($result2,0,"totalamt");
                   $rowtotal[$arrayname]=$rowtotal[$arrayname]+$amount;

          }
          //////General Stats including date
          elseif ($type==7&&strstr($reportquery, "." )&&substr($reportquery,0,7)=="invoice"){

				if ($businessid>0){$query2 = "SELECT Sum($reportquery) AS totalamt FROM invoice WHERE invoice.businessid='$businessid' AND invoice.date>='$startdate[$counter]' AND invoice.date<='$enddate[$counter]'";}
				else if ($districtid!=""){$query2 = "SELECT Sum($reportquery) AS totalamt FROM (invoice INNER JOIN business ON invoice.businessid = business.businessid)  WHERE business.districtid='$districtid' AND business.companyid='$companyid' AND invoice.date>='$startdate[$counter]' AND invoice.date<='$enddate[$counter]'";}
				else{$query2 = "SELECT Sum($reportquery) AS totalamt FROM invoice WHERE invoice.companyid='$companyid' AND invoice.date>='$startdate[$counter]' AND invoice.date<='$enddate[$counter]'";}
				$result2 = Treat_DB_ProxyOldProcessHost::query($query2);
				$num2=Treat_DB_ProxyOldProcessHost::mysql_numrows($result2);

				$amount=Treat_DB_ProxyOldProcessHost::mysql_result($result2,0,"totalamt");
				$rowtotal[$arrayname]=$rowtotal[$arrayname]+$amount;

          }
          //////Vend Stats including date
          elseif ($type==7&&strstr($reportquery, "." )&&substr($reportquery,0,15)=="vend_collection"){

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             if ($businessid>0){$query2 = "SELECT Sum($reportquery) AS totalamt FROM vend_collection WHERE vend_collection.businessid='$businessid' AND vend_collection.date>='$startdate[$counter]' AND vend_collection.date<='$enddate[$counter]'";}
             else if ($districtid!=""){$query2 = "SELECT Sum($reportquery) AS totalamt FROM (vend_collection INNER JOIN business ON vend_collection.businessid = business.businessid)  WHERE business.districtid='$districtid' AND business.companyid='$companyid' AND vend_collection.date>='$startdate[$counter]' AND vend_collection.date<='$enddate[$counter]'";}
             else{$query2 = "SELECT Sum($reportquery) AS totalamt FROM vend_collection WHERE vend_collection.companyid='$companyid' AND vend_collection.date>='$startdate[$counter]' AND vend_collection.date<='$enddate[$counter]'";}
             $result2 = Treat_DB_ProxyOldProcessHost::query($query2);
             $num2=Treat_DB_ProxyOldProcessHost::mysql_numrows($result2);
             //mysql_close();
                   $amount=Treat_DB_ProxyOldProcessHost::mysql_result($result2,0,"totalamt");
                   $rowtotal[$arrayname]=$rowtotal[$arrayname]+$amount;

          }
          //////General Stats
          elseif ($type==7&&strstr($reportquery, "." )){

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             if ($businessid>0){$query2 = "SELECT Sum($reportquery) AS totalamt FROM business WHERE business.businessid='$businessid' AND business.companyid='$companyid'";}
             else if ($districtid!=""){$query2 = "SELECT Sum($reportquery) AS totalamt FROM business WHERE business.districtid='$districtid' AND business.companyid='$companyid'";}
             else{$query2 = "SELECT Sum($reportquery) AS totalamt FROM business WHERE business.companyid='$companyid'";}
             $result2 = Treat_DB_ProxyOldProcessHost::query($query2);
             $num2=Treat_DB_ProxyOldProcessHost::mysql_numrows($result2);
             //mysql_close();
                   $amount=Treat_DB_ProxyOldProcessHost::mysql_result($result2,0,"totalamt");
                   $rowtotal[$arrayname]=$rowtotal[$arrayname]+$amount;

          }
          /////STATS
          elseif ($type==7){
			 /////kiosks
			 if($categoryid == 4 && $reportquery == "number"){
				$query3 = "SELECT COUNT(check_number) AS amount FROM checks WHERE businessid = '$businessid' AND bill_datetime >= '$startdate[$counter] 00:00:00' AND checks.bill_datetime <= '$startdate[$counter] 10:00:00' AND is_void = 0 AND total > 0";
				$result3 = Treat_DB_ProxyOldProcessHost::query($query3);
				$amount = @Treat_DB_ProxyOldProcessHost::mysql_result($result3,0,"amount");
				$rowtotal[$arrayname]=$rowtotal[$arrayname]+$amount;
			 }
			 elseif($categoryid == 4 && $reportquery == "lunch"){
				$query3 = "SELECT COUNT(check_number) AS amount FROM checks WHERE businessid = '$businessid' AND bill_datetime >= '$startdate[$counter] 10:00:01' AND checks.bill_datetime <= '$startdate[$counter] 15:00:00' AND is_void = 0 AND total > 0";
				$result3 = Treat_DB_ProxyOldProcessHost::query($query3);
				$amount = @Treat_DB_ProxyOldProcessHost::mysql_result($result3,0,"amount");
				$rowtotal[$arrayname]=$rowtotal[$arrayname]+$amount;
			 }
			 elseif($categoryid == 4 && $reportquery == "dinner"){
				$query3 = "SELECT COUNT(check_number) AS amount FROM checks WHERE businessid = '$businessid' AND bill_datetime >= '$startdate[$counter] 15:00:01' AND checks.bill_datetime <= '$startdate[$counter] 23:59:59' AND is_void = 0 AND total > 0";
				$result3 = Treat_DB_ProxyOldProcessHost::query($query3);
				$amount = @Treat_DB_ProxyOldProcessHost::mysql_result($result3,0,"amount");
				$rowtotal[$arrayname]=$rowtotal[$arrayname]+$amount;
			 }
			 else{
             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             if ($businessid>0){$query2 = "SELECT Sum(stats.$reportquery) AS totalamt FROM stats INNER JOIN business ON stats.businessid = business.businessid WHERE stats.date>='$startdate[$counter]' AND stats.date<='$enddate[$counter]' AND business.businessid='$businessid' AND business.companyid='$companyid'";}
             else if ($districtid!=""){ $query2 = "SELECT Sum(stats.$reportquery) AS totalamt FROM stats INNER JOIN business ON stats.businessid = business.businessid WHERE stats.date>='$startdate[$counter]' AND stats.date<='$enddate[$counter]' AND business.districtid='$districtid' AND business.companyid='$companyid'";}
             else{$query2 = "SELECT Sum(stats.$reportquery) AS totalamt FROM stats INNER JOIN business ON stats.businessid = business.businessid WHERE stats.date>='$startdate[$counter]' AND stats.date<='$enddate[$counter]' AND business.companyid='$companyid'";}
             $result2 = Treat_DB_ProxyOldProcessHost::query($query2);
             $num2=Treat_DB_ProxyOldProcessHost::mysql_numrows($result2);
             //mysql_close();
                   $amount=Treat_DB_ProxyOldProcessHost::mysql_result($result2,0,"totalamt");
                   $rowtotal[$arrayname]=$rowtotal[$arrayname]+$amount;
			 }

          }
          //////Opertaing Days
          elseif ($type==8){
             $totalunit=0;
             $amount=dateDiff($startdate[$counter],$enddate[$counter])+1;
             $saturday=daysBetween($startdate[$counter], $enddate[$counter], "Saturday");
             $sunday=daysBetween($startdate[$counter], $enddate[$counter], "Sunday");

             if ($businessid>0){
                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query5 = "SELECT operate FROM business WHERE businessid = '$businessid'";
                $result5 = Treat_DB_ProxyOldProcessHost::query($query5);
                //mysql_close();

                $operate=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,"operate");

                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                if ($businessid>0){$query2 = "SELECT Sum(stats.closed) AS totalamt FROM stats INNER JOIN business ON stats.businessid = business.businessid WHERE stats.date>='$startdate[$counter]' AND stats.date<='$enddate[$counter]' AND business.businessid='$businessid' AND business.companyid='$companyid'";}
                else if ($districtid!=""){ $query2 = "SELECT Sum(stats.closed) AS totalamt FROM stats INNER JOIN business ON stats.businessid = business.businessid WHERE stats.date>='$startdate[$counter]' AND stats.date<='$enddate[$counter]' AND business.districtid='$districtid' AND business.companyid='$companyid'";}
                else{$query2 = "SELECT Sum(stats.closed) AS totalamt FROM stats INNER JOIN business ON stats.businessid = business.businessid WHERE stats.date>='$startdate[$counter]' AND stats.date<='$enddate[$counter]' AND business.companyid='$companyid'";}
                $result2 = Treat_DB_ProxyOldProcessHost::query($query2);
                $num2=Treat_DB_ProxyOldProcessHost::mysql_numrows($result2);
                //mysql_close();
                $closed=Treat_DB_ProxyOldProcessHost::mysql_result($result2,0,"totalamt");

                if ($operate==5){$rowtotal[$arrayname]=$amount-$saturday-$sunday-$closed;}
                elseif ($operate==6){$rowtotal[$arrayname]=$amount-$sunday-$closed;}
                elseif ($operate==7){$rowtotal[$arrayname]=$amount-$closed;}
             }
             elseif($districtid!=""){
                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query5 = "SELECT operate FROM business WHERE districtid = '$districtid' AND operate = '5'";
                $result5 = Treat_DB_ProxyOldProcessHost::query($query5);
                $num5=Treat_DB_ProxyOldProcessHost::mysql_numrows($result5);
                //mysql_close();

                $rowtotal[$arrayname]=($amount-$saturday-$sunday)*$num5;
                $totalunit=$num5;
 
                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query5 = "SELECT operate FROM business WHERE districtid = '$districtid' AND operate = '6'";
                $result5 = Treat_DB_ProxyOldProcessHost::query($query5);
                $num5=Treat_DB_ProxyOldProcessHost::mysql_numrows($result5);
                //mysql_close();

                $rowtotal[$arrayname]=$rowtotal[$arrayname]+($amount-$sunday)*$num5;
                $totalunit=$totalunit+$num5;

                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query5 = "SELECT operate FROM business WHERE districtid = '$districtid' AND operate = '7'";
                $result5 = Treat_DB_ProxyOldProcessHost::query($query5);
                $num5=Treat_DB_ProxyOldProcessHost::mysql_numrows($result5);
                //mysql_close();

                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                if ($businessid>0){$query2 = "SELECT Sum(stats.closed) AS totalamt FROM stats INNER JOIN business ON stats.businessid = business.businessid WHERE stats.date>='$startdate[$counter]' AND stats.date<='$enddate[$counter]' AND business.businessid='$businessid' AND business.companyid='$companyid'";}
                else if ($districtid!=""){ $query2 = "SELECT Sum(stats.closed) AS totalamt FROM stats INNER JOIN business ON stats.businessid = business.businessid WHERE stats.date>='$startdate[$counter]' AND stats.date<='$enddate[$counter]' AND business.districtid='$districtid' AND business.companyid='$companyid'";}
                else{$query2 = "SELECT Sum(stats.closed) AS totalamt FROM stats INNER JOIN business ON stats.businessid = business.businessid WHERE stats.date>='$startdate[$counter]' AND stats.date<='$enddate[$counter]' AND business.companyid='$companyid'";}
                $result2 = Treat_DB_ProxyOldProcessHost::query($query2);
                $num2=Treat_DB_ProxyOldProcessHost::mysql_numrows($result2);
                //mysql_close();
                $closed=Treat_DB_ProxyOldProcessHost::mysql_result($result2,0,"totalamt");

                $rowtotal[$arrayname]=$rowtotal[$arrayname]+($amount)*$num5-$closed;
                $totalunit=$totalunit+$num5;
                $rowtotal[$arrayname]=$rowtotal[$arrayname]/$totalunit;
             }
             else{
                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query5 = "SELECT operate FROM business WHERE companyid = '$companyid' AND operate = '5'";
                $result5 = Treat_DB_ProxyOldProcessHost::query($query5);
                $num5=Treat_DB_ProxyOldProcessHost::mysql_numrows($result5);
                //mysql_close();

                $rowtotal[$arrayname]=($amount-$saturday-$sunday)*$num5;
                $totalunit=$totalunit+$num5;
 
                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query5 = "SELECT operate FROM business WHERE companyid = '$companyid' AND operate = '6'";
                $result5 = Treat_DB_ProxyOldProcessHost::query($query5);
                $num5=Treat_DB_ProxyOldProcessHost::mysql_numrows($result5);
                //mysql_close();

                $rowtotal[$arrayname]=$rowtotal[$arrayname]+($amount-$sunday)*$num5;
                $totalunit=$totalunit+$num5;

                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query5 = "SELECT operate FROM business WHERE companyid = '$companyid' AND operate = '7'";
                $result5 = Treat_DB_ProxyOldProcessHost::query($query5);
                $num5=Treat_DB_ProxyOldProcessHost::mysql_numrows($result5);
                //mysql_close();

                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                if ($businessid>0){$query2 = "SELECT Sum(stats.closed) AS totalamt FROM stats INNER JOIN business ON stats.businessid = business.businessid WHERE stats.date>='$startdate[$counter]' AND stats.date<='$enddate[$counter]' AND business.businessid='$businessid' AND business.companyid='$companyid'";}
                else if ($districtid!=""){ $query2 = "SELECT Sum(stats.closed) AS totalamt FROM stats INNER JOIN business ON stats.businessid = business.businessid WHERE stats.date>='$startdate[$counter]' AND stats.date<='$enddate[$counter]' AND business.districtid='$districtid' AND business.companyid='$companyid'";}
                else{$query2 = "SELECT Sum(stats.closed) AS totalamt FROM stats INNER JOIN business ON stats.businessid = business.businessid WHERE stats.date>='$startdate[$counter]' AND stats.date<='$enddate[$counter]' AND business.companyid='$companyid'";}
                $result2 = Treat_DB_ProxyOldProcessHost::query($query2);
                $num2=Treat_DB_ProxyOldProcessHost::mysql_numrows($result2);
                //mysql_close();
                $closed=Treat_DB_ProxyOldProcessHost::mysql_result($result2,0,"totalamt");

                $rowtotal[$arrayname]=$rowtotal[$arrayname]+($amount)*$num5-$closed;
                $totalunit=$totalunit+$num5;
                $rowtotal[$arrayname]=$rowtotal[$arrayname]/$totalunit;
             }
          }
          ///////Labor Adjustment
          elseif ($type==9){
             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             if ($businessid>0){$query2 = "SELECT Sum(payroll.reg2+payroll.ot2) AS totalamt FROM business INNER JOIN payroll ON business.unit = payroll.unit_num WHERE payroll.date>='$startdate[$counter]' AND payroll.date<='$enddate[$counter]' AND business.businessid='$businessid' AND business.companyid='$companyid'";}
             else if ($districtid!=""){$query2 = "SELECT Sum(payroll.reg2+payroll.ot2) AS totalamt FROM business INNER JOIN payroll ON business.unit = payroll.unit_num WHERE payroll.date>='$startdate[$counter]' AND payroll.date<='$enddate[$counter]' AND business.districtid='$districtid' AND business.companyid='$companyid'";}
             else{$query2 = "SELECT Sum(payroll.reg2+payroll.ot2) AS totalamt FROM business INNER JOIN payroll ON business.unit = payroll.unit_num WHERE payroll.date>='$startdate[$counter]' AND payroll.date<='$enddate[$counter]' AND business.companyid='$companyid'";}
             $result2 = Treat_DB_ProxyOldProcessHost::query($query2);
             $num2=Treat_DB_ProxyOldProcessHost::mysql_numrows($result2);
             //mysql_close();
                   $amount=Treat_DB_ProxyOldProcessHost::mysql_result($result2,0,"totalamt");
                   $rowtotal[$arrayname]=$rowtotal[$arrayname]+$amount;
            

          }

          ///////Budget
          elseif ($type==10){
             $amount=0;

             if ($businessid>0){
                
                $query5 = "SELECT operate,unit,budget_type FROM business WHERE businessid = '$businessid'";
                $result5 = Treat_DB_ProxyOldProcessHost::query($query5);

                $operate=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,"operate");
                $unit=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,"unit");
                $budget_type=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,"budget_type");

                if($budget_type==1){
                   $temp_date=$startdate[$counter];
                   $query5 = "SELECT SUM(amount) AS totalamt FROM vend_budget WHERE businessid = '$businessid' AND date >= '$temp_date' AND date <= '$enddate[$counter]' AND type = '$reportquery'";
                   $result5 = Treat_DB_ProxyOldProcessHost::query($query5);

                   $amount=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,"totalamt");
                }
                else{
                   $temp_date=$startdate[$counter];
                   $numdays=0;
                   while($temp_date<=$enddate[$counter]){
                      $amount=0;

                      $thismonth=substr($temp_date,5,2);
                      $thisyear=substr($temp_date,0,4);
                      $month_start=substr($temp_date,0,7);
                      $month_start="$month_start-01";

                      $query5 = "SELECT * FROM budget WHERE companyid = '$companyid' AND businessid = '$unit' AND year = '$thisyear' AND type = '$reportquery'";
                      $result5 = Treat_DB_ProxyOldProcessHost::query($query5);

                      $monthbudget=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,$thismonth);

                      $operatingdays=0;
                      while($thismonth==substr($month_start,5,2)){
                         $query5 = "SELECT closed FROM stats WHERE businessid = '$businessid' AND date = '$month_start'";
                         $result5 = Treat_DB_ProxyOldProcessHost::query($query5);

                         $closed=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,"closed");

                         if($operate==7&&$closed==0){$operatingdays++;}
                         elseif($operate==6&&$closed==0&&dayofweek($month_start)!="Sunday"){$operatingdays++;}
                         elseif($operate==5&&$closed==0&&dayofweek($month_start)!="Saturday"&&dayofweek($month_start)!="Sunday"){$operatingdays++;}

                         $month_start=nextday($month_start);
                      }
                      $numdays++;
                  
                      $amount+=($monthbudget/$operatingdays);
                      $temp_date=nextday($temp_date);
                   }
                }
             }

             $rowtotal[$arrayname]=$rowtotal[$arrayname]+$amount;    
          }

          ///////Recur
          elseif ($type==11){
             $amount=0;

             if ($businessid>0){
                
                $query5 = "SELECT operate,unit FROM business WHERE businessid = '$businessid'";
                $result5 = Treat_DB_ProxyOldProcessHost::query($query5);

                $operate=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,"operate");
                $unit=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,"unit");

                $temp_date=$startdate[$counter];
                $numdays=0;
                while($temp_date<=$enddate[$counter]){
                   $amount=0;

                   $thismonth=substr($temp_date,5,2);
                   $thisyear=substr($temp_date,0,4);
                   $month_start=substr($temp_date,0,7);
                   $month_start="$month_start-01";

                   $query5 = "SELECT * FROM recur WHERE companyid = '$companyid' AND businessid = '$unit' AND year = '$thisyear' AND acct_num LIKE '$reportquery'";
                   $result5 = Treat_DB_ProxyOldProcessHost::query($query5);

                   $monthbudget=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,$thismonth);

                   $operatingdays=0;
                   while($thismonth==substr($month_start,5,2)){
                      $query5 = "SELECT closed FROM stats WHERE businessid = '$businessid' AND date = '$month_start'";
                      $result5 = Treat_DB_ProxyOldProcessHost::query($query5);

                      $closed=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,"closed");

                      if($operate==7&&$closed==0){$operatingdays++;}
                      elseif($operate==6&&$closed==0&&dayofweek($month_start)!="Sunday"){$operatingdays++;}
                      elseif($operate==5&&$closed==0&&dayofweek($month_start)!="Saturday"&&dayofweek($month_start)!="Sunday"){$operatingdays++;}

                      $month_start=nextday($month_start);
                   }
                   $numdays++;
                  
                   $amount+=($monthbudget/$operatingdays);
                   $temp_date=nextday($temp_date);
                }
                
             }
             elseif ($districtid!=""){

                $query55 = "SELECT business.businessid FROM business,district WHERE business.districtid = district.districtid AND district.districtid = '$districtid'";
                $result55 = Treat_DB_ProxyOldProcessHost::query($query55);
                $num55=Treat_DB_ProxyOldProcessHost::mysql_numrows($result55);
                
                $num55--;
                $amount=0;
                while($num55>=0){
                   $businessid=Treat_DB_ProxyOldProcessHost::mysql_result($result55,$num55,"business.businessid");

                   $query5 = "SELECT operate,unit FROM business WHERE businessid = '$businessid'";
                   $result5 = Treat_DB_ProxyOldProcessHost::query($query5);

                   $operate=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,"operate");
                   $unit=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,"unit");

                   $temp_date=$startdate[$counter];

                   while($temp_date<=$enddate[$counter]){

                      $thismonth=substr($temp_date,5,2);
                      $thisyear=substr($temp_date,0,4);
                      $month_start=substr($temp_date,0,7);
                      $month_start="$month_start-01";

                      $query5 = "SELECT * FROM budget WHERE companyid = '$companyid' AND businessid = '$unit' AND year = '$thisyear' AND type = '$reportquery'";
                      $result5 = Treat_DB_ProxyOldProcessHost::query($query5);

                      $monthbudget=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,$thismonth);

                      $operatingdays=0;
                      $numdays=0;
                      while($thismonth==substr($month_start,5,2)){
                         $query5 = "SELECT closed FROM stats WHERE businessid = '$businessid' AND date = '$month_start'";
                         $result5 = Treat_DB_ProxyOldProcessHost::query($query5);

                         $closed=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,"closed");

                         if($operate==7&&$closed==0){$operatingdays++;}
                         elseif($operate==6&&$closed==0&&dayofweek($month_start)!="Sunday"){$operatingdays++;}
                         elseif($operate==5&&$closed==0&&dayofweek($month_start)!="Saturday"&&dayofweek($month_start)!="Sunday"){$operatingdays++;}

                         $month_start=nextday($month_start);
                         $numdays++;
                      }
                      
                      $temp_date=nextday($temp_date);
                   }   
                   $amount+=($monthbudget/$operatingdays);
                   $num55--;
                }
                $businessid=0;
             }
             else{

                $query55 = "SELECT businessid FROM business WHERE companyid = '$companyid'";
                $result55 = Treat_DB_ProxyOldProcessHost::query($query55);
                $num55=Treat_DB_ProxyOldProcessHost::mysql_numrows($result55);
                
                $num55--;
                $amount=0;
                while($num55>=0){
                   $businessid=Treat_DB_ProxyOldProcessHost::mysql_result($result55,$num55,"business.businessid");

                   $query5 = "SELECT operate,unit FROM business WHERE businessid = '$businessid'";
                   $result5 = Treat_DB_ProxyOldProcessHost::query($query5);

                   $operate=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,"operate");
                   $unit=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,"unit");

                   $temp_date=$startdate[$counter];

                   while($temp_date<=$enddate[$counter]){

                      $thismonth=substr($temp_date,5,2);
                      $thisyear=substr($temp_date,0,4);
                      $month_start=substr($temp_date,0,7);
                      $month_start="$month_start-01";

                      $query5 = "SELECT * FROM budget WHERE businessid = '$unit' AND year = '$thisyear' AND type = '$reportquery'";
                      $result5 = Treat_DB_ProxyOldProcessHost::query($query5);

                      $monthbudget=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,$thismonth);

                      $operatingdays=0;
                      $numdays=0;
                      while($thismonth==substr($month_start,5,2)){
                         $query5 = "SELECT closed FROM stats WHERE businessid = '$businessid' AND date = '$month_start'";
                         $result5 = Treat_DB_ProxyOldProcessHost::query($query5);

                         $closed=Treat_DB_ProxyOldProcessHost::mysql_result($result5,0,"closed");

                         if($operate==7&&$closed==0){$operatingdays++;}
                         elseif($operate==6&&$closed==0&&dayofweek($month_start)!="Sunday"){$operatingdays++;}
                         elseif($operate==5&&$closed==0&&dayofweek($month_start)!="Saturday"&&dayofweek($month_start)!="Sunday"){$operatingdays++;}

                         $month_start=nextday($month_start);
                         $numdays++;
                      }
                      
                      $temp_date=nextday($temp_date);
                   }
                   $amount+=($monthbudget/$operatingdays);
                   $num55--;
                }
                $businessid=0;
             }

             $rowtotal[$arrayname]=$rowtotal[$arrayname]+$amount;
             
          }

          ///////Benefit%
          elseif ($type==12){
             if($businessid>0){
                $query2 = "SELECT benefit FROM business WHERE businessid = '$businessid'";
                $result2 = Treat_DB_ProxyOldProcessHost::query($query2);
                $num2=Treat_DB_ProxyOldProcessHost::mysql_numrows($result2);

                $amount=Treat_DB_ProxyOldProcessHost::mysql_result($result2,0,"benefit");
             }
             elseif($districtid!=""){
                $query2 = "SELECT avg(benefit) AS totalamt FROM business WHERE districtid = '$districtid'";
                $result2 = Treat_DB_ProxyOldProcessHost::query($query2);
                $num2=Treat_DB_ProxyOldProcessHost::mysql_numrows($result2);

                $amount=Treat_DB_ProxyOldProcessHost::mysql_result($result2,0,"totalamt");
             }
             elseif($companyid!=""){
                $query2 = "SELECT avg(benefit) AS totalamt FROM business WHERE companyid = '$companyid'";
                $result2 = Treat_DB_ProxyOldProcessHost::query($query2);
                $num2=Treat_DB_ProxyOldProcessHost::mysql_numrows($result2);

                $amount=Treat_DB_ProxyOldProcessHost::mysql_result($result2,0,"totalamt");
             }
                   
                   $rowtotal[$arrayname]=$rowtotal[$arrayname]+$amount;
            

          }

          //////FORMULA
          elseif ($type==6){
             $qlength=strlen($reportquery);
             $qcount=0;
             $formcount=0;
             $formarray=array();
             $pos1=0;
             $qchar=substr($reportquery,$qcount,1);
             while($qcount<=$qlength){
                $pos2=0;
                while($qchar!="+"&&$qchar!="-"&&$qchar!="*"&&$qchar!="/"&&$qchar!="~"&&$qcount<=$qlength){
                   $qcount++;
                   $qchar=substr($reportquery,$qcount,1);
                   $pos2++;
                }
                $formarray[$formcount]=substr($reportquery,$pos1,$pos2);
                $formcount++;
                if ($qchar!=""){
                   $formarray[$formcount]=$qchar;
                   $formcount++;
                }
                $pos1=$qcount+1;
                $qcount++;
                $qchar=substr($reportquery,$qcount,1);
             }

             $arrayname2="$formarray[0].$counter";
             if (substr($formarray[0],0,1)=="["){$qlength=strlen($formarray[0]);$rowtotal[$arrayname]=substr($formarray[0],1,$qlength-2);}
             else{$rowtotal[$arrayname]=$rowtotal[$arrayname2];}
             $mycount=1;
             while($mycount<=$formcount){
                $mycount2=$mycount+1;
                $arrayname2="$formarray[$mycount2].$counter";
                if ($formarray[$mycount]=="+"){
                   if (substr($formarray[$mycount2],0,1)=="["){$qlength=strlen($formarray[$mycount2]);$rowtotal[$arrayname]=$rowtotal[$arrayname]+substr($formarray[$mycount2],1,$qlength-2);}
                   else{$rowtotal[$arrayname]=$rowtotal[$arrayname]+$rowtotal[$arrayname2];}
                }
                elseif ($formarray[$mycount]=="-"){
                   if (substr($formarray[$mycount2],0,1)=="["){$qlength=strlen($formarray[$mycount2]);$rowtotal[$arrayname]=$rowtotal[$arrayname]-substr($formarray[$mycount2],1,$qlength-2);}
                   else{$rowtotal[$arrayname]=$rowtotal[$arrayname]-$rowtotal[$arrayname2];}
                }
                elseif ($formarray[$mycount]=="*"){
                   if (substr($formarray[$mycount2],0,1)=="["){$qlength=strlen($formarray[$mycount2]);$rowtotal[$arrayname]=$rowtotal[$arrayname]*substr($formarray[$mycount2],1,$qlength-2);}
                   else{$rowtotal[$arrayname]=$rowtotal[$arrayname]*$rowtotal[$arrayname2];}
                }
                elseif ($formarray[$mycount]=="/"){
                   if (substr($formarray[$mycount2],0,1)=="["){$qlength=strlen($formarray[$mycount2]);$rowtotal[$arrayname]=$rowtotal[$arrayname]/substr($formarray[$mycount2],1,$qlength-2);}
                   else{$rowtotal[$arrayname]=$rowtotal[$arrayname]/$rowtotal[$arrayname2];}
                }
                elseif ($formarray[$mycount]=="~"){
                   $starthere=$formarray[$mycount-1];
                   $endhere=$formarray[$mycount+1];
                   $rowtotal[$arrayname]=0;
                   while($starthere<=$endhere){
                      $startarray="$starthere.$counter";
                      $rowtotal[$arrayname]=$rowtotal[$arrayname]+$rowtotal[$startarray];
                      $starthere++;
                   }
                }
                $mycount=$mycount+2;
             }
          }

          if ($rowtotal[$arrayname]==""){$rowtotal[$arrayname]=0;}
          $showrow=money($rowtotal[$arrayname]);
          if($hide==0){$data="$data$showrow,";}
          $showrow=number_format($rowtotal[$arrayname],$decimal);
          if ($bold==1&&$hide==0){echo "<td align=right bgcolor=#E8E7E7><font size=2><b>$showrow$suffix</b></td>";}
          elseif($hide==0){echo "<td align=right><font size=2>$showrow</td>";}

          ///////////////////////////////////////
          /////////////////WRITE TO DB (if daily)
          ///////////////////////////////////////
          if($wtdb==1&&$period==1){
             $query32 = "SELECT dashid FROM dashboard WHERE businessid = '$businessid' AND date = '$startdate[$counter]'";
             $result32 = Treat_DB_ProxyOldProcessHost::query($query32);
             $num32=Treat_DB_ProxyOldProcessHost::mysql_numrows($result32);

             if($num32>0){
                $dashid=Treat_DB_ProxyOldProcessHost::mysql_result($result32,0,"dashid");

                $query32 = "UPDATE $table SET $field = '$rowtotal[$arrayname]' WHERE dashid = '$dashid'";
                $result32 = Treat_DB_ProxyOldProcessHost::query($query32);
             }
             else{
                $query32 = "INSERT INTO $table (businessid,date,$field) VALUES ('$businessid','$startdate[$counter]','$rowtotal[$arrayname]')";
                $result32 = Treat_DB_ProxyOld::query($query32);
             }
          
          }
 
          }////END PERIOD LOOP
          $coltotal=0;
          if ($avg==0){for ($counter=1;$counter<=$periodnum;$counter++){$arrayname="$counter2.$counter";$coltotal=$coltotal+$rowtotal[$arrayname];}}
          elseif ($avg==1){for ($counter=1;$counter<=$periodnum;$counter++){$arrayname="$counter2.$counter";$coltotal=$coltotal+$rowtotal[$arrayname];}$coltotal=$coltotal/$periodnum;}
          $coltotal=money($coltotal);
          if($hide==0){$data="$data$coltotal\r\n";}
          $colshow=number_format($coltotal,$decimal);
          if ($hide==0){echo "<td align=right bgcolor=#E8E7E7><font size=2><b>$colshow$suffix</td></tr>";}
          $num--;
          $counter2++;
       }
    
       ////////////////////////////////////////////////
       ////////////////////////////////////////////////
       ////////////////////////////////////////////////
       $num88--;
   }
}

//////////////////////////////////////////////////////////////////////////////////////////
///////////////END SEPERATE REPORT////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
    echo "</table></center>";

    if ($curreportid!=""){$filename=HTTP_EXPORTS."/report$loginid.csv";$showfile="<a href=$filename style=$style><font color=blue size=2>Excel File</font></a>";}
    echo "<center><table width=100%><tr><td>$showfile</td></tr></table></center>";

	if($wtf != 1){
		$myFile2 = DIR_EXPORTS."/report$loginid.csv";
		$fh = fopen($myFile2, 'w');
		fwrite($fh, $data);
		fclose($fh);
	}

if ($graph==1){
     
/////////////////////JAVA GRAPH/////////////////////////////////////////
if ($totalgraph==1){
?>
<p><center><applet code="LineGraphApplet.class" archive="Linegraph.jar" width="100%" height="400">

<!-- Start Up Parameters -->
<PARAM name="LOADINGMESSAGE" value="Creating Chart - Please Wait.">
<PARAM name="STEXTCOLOR"     value="#000060">
<PARAM name="STARTUPCOLOR"   value="#FFFFFF">
<PARAM name="3D" value="false">
<PARAM name="depth3D" value="25">
<PARAM name="outline" value="true">
<!-- Line Data -->
<!--   value,URL,Target Frame -->
<?php
////////////////////////////////////////////////////////////////////////
      //mysql_connect($dbhost,$username,$password);
      //@mysql_select_db($database) or die( "Unable to select database");
      $query = "SELECT * FROM reportdetail WHERE reportid = '$curreportid' ORDER BY orderid DESC";
      $result = Treat_DB_ProxyOldProcessHost::query($query);
      $num=Treat_DB_ProxyOldProcessHost::mysql_numrows($result);
      //mysql_close();
      $num--;$num2=$num;

      $counter2=1;
      $mycount2=1;
      while ($num>=0){
         $graph_it=Treat_DB_ProxyOldProcessHost::mysql_result($result,$num,"graph_it");
         $g_color=Treat_DB_ProxyOldProcessHost::mysql_result($result,$num,"g_color");
         $detailname=Treat_DB_ProxyOldProcessHost::mysql_result($result,$num,"name");
         $on_pie=Treat_DB_ProxyOldProcessHost::mysql_result($result,$num,"on_pie");

         if ($graph_it==1){
            $mycount=1;
	    $pie_sum=0;
            for($counter=1;$counter<=$periodnum;$counter++){
               $data="data$mycount";
               $series="series$mycount2";

               $arrayname="$counter2.$counter";
               echo "<PARAM name=$data$series value=$rowtotal[$arrayname]>";
               $mycount++;
            }
            $mycount2++;
         }               
         $num--;
         $counter2++;
      }
   

      $counter=1;
      $mycount=1;
      while($num2>=0){
         $g_color=Treat_DB_ProxyOldProcessHost::mysql_result($result,$num2,"g_color");
         $graph_it=Treat_DB_ProxyOldProcessHost::mysql_result($result,$num2,"graph_it");
         $detailname=Treat_DB_ProxyOldProcessHost::mysql_result($result,$num2,"name");
         $on_pie=Treat_DB_ProxyOldProcessHost::mysql_result($result,$num2,"on_pie");

         if ($g_color==""){$color="#" . rndm_color_code(FALSE);}
         elseif ($g_color!=""){$color=$g_color;}
         $series="series$mycount";

  	 if ($graph_it==1){
	    echo "<PARAM name=$series value=$color|3|5|true|$detailname>";
            $mycount++;
         }

         $num2--; 
         $counter++;     
      }	

?>
<PARAM name="autoscale"            value="true">
<PARAM name="gridbgcolor"          value="#FFFFFF">
<PARAM name="ndecplaces"           value="0">
<PARAM name="legend"               value="true">
<PARAM name="LegendStyle"          value="Horizontal">
<PARAM name="legendfont"           value="Arial,N,10">
<PARAM name="labelOrientation"     value="Up Angle">

<PARAM name="ylabel_pre"           value="">
<PARAM name="ylabel_font"          value="Arial,N,10">

<PARAM name="popup_pre"            value="">

<PARAM name="xlabel_font"          value="Arial,N,10">

<?php

      for ($counter=1;$counter<=$periodnum;$counter++){$showday=substr($startdate[$counter],8,2);$showmonth=substr($startdate[$counter],5,2);$showyear=substr($startdate[$counter],2,2);$showdate="$showmonth/$showday/$showyear";$showlabel="label$counter";echo "<PARAM name=$showlabel value=$showdate>";}

      echo "</applet></center>";
}
/////////////PIE CHART
      echo "<br><center>";
      //mysql_connect($dbhost,$username,$password);
      //@mysql_select_db($database) or die( "Unable to select database");
      $query = "SELECT * FROM reportdetail WHERE reportid = '$curreportid' ORDER BY orderid DESC";
      $result = Treat_DB_ProxyOldProcessHost::query($query);
      $num=Treat_DB_ProxyOldProcessHost::mysql_numrows($result);
      //mysql_close();
      $num--;$num2=$num;

      $counter2=1;
      $mycount2=1;
      while ($num>=0){
         $graph_it=Treat_DB_ProxyOldProcessHost::mysql_result($result,$num,"graph_it");
         $g_color=Treat_DB_ProxyOldProcessHost::mysql_result($result,$num,"g_color");
         $detailname=Treat_DB_ProxyOldProcessHost::mysql_result($result,$num,"name");
         $on_pie=Treat_DB_ProxyOldProcessHost::mysql_result($result,$num,"on_pie");
         if ($g_color==""){$color="#" . rndm_color_code(FALSE);}
         elseif ($g_color!=""){$color=$g_color;}

           if ($on_pie==1){
	    $pie_sum=0;
            for($counter=1;$counter<=$periodnum;$counter++){
               $arrayname="$counter2.$counter";
	       $pie_sum+=$rowtotal[$arrayname];
             }
	   if ($on_pie==1&&$pie_sum>1){
	    $pie_data.= "$pie_sum*";
	    $pie_series.=$detailname."*";
	    $pie_colorb=str_replace("#","",$g_color);
	    $pie_color.= "$pie_colorb~";
	   }
            $mycount2++;
           }               
         $num--;
         $counter2++;
       }


$t_data="*";
$pie_data=trim($pie_data,$t_data);
$pie_series=trim($pie_series,$t_data);
$t_data="~";
$pie_color=trim($pie_color,$t_data);
//echo "pie_color $pie_color pie_data $pie_data pie_series $pie_series <br>";
if ($pie_data!=""){
echo "<img src='piechart.php?pie_color=$pie_color&data=$pie_data&label=$pie_series' /> ";
}
echo "</center>";

}
//mysql_close();

//////////////////////////////END REPORT
    echo "<br><FORM ACTION=businesstrack.php method=post>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<center><INPUT TYPE=submit VALUE=' Return to Main Page'></FORM></center></body>";
	
	google_page_track();
}
?>