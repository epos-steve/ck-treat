<?php

define( 'DOC_ROOT', realpath( dirname( __FILE__ ) . '/../' ) );
require_once( DOC_ROOT . '/bootstrap.php' );
require_once( 'PHPExcel.php' );
require_once( 'lib/class.menu_items.php' );
require_once( 'lib/class.Kiosk.php' );

//DB_Proxy::$debug = true;
//ini_set('display_errors','true');

if( require_once( realpath( DOC_ROOT . '/../lib/inc/cookie-login.php' ) ) ) {
	$businessid = $_GET["bid"];

	$query = "SELECT
				menu_items_new.id,
				menu_items_new.name,
				menu_items_new.item_code,
				menu_items_new.upc,
				menu_items_new.active,
				menu_items_new.max,
				menu_items_new.reorder_point,
				menu_items_new.reorder_amount,
				menu_items_new.ordertype,
				menu_groups_new.name AS group_name,
				menu_items_price.price,
				menu_items_price.cost,
				menu_tax.name AS tax,
				menu_tax.percent
			FROM menu_items_new
			JOIN menu_items_price ON menu_items_price.menu_item_id = menu_items_new.id
			JOIN menu_groups_new ON menu_groups_new.id = menu_items_new.group_id
			LEFT JOIN menu_item_tax ON menu_item_tax.menu_itemid = menu_items_new.id
			LEFT JOIN menu_tax ON menu_tax.id = menu_item_tax.tax_id
			WHERE menu_items_new.businessid = $businessid
			GROUP BY menu_items_new.group_id, menu_items_new.name, menu_items_new.item_code
	";
	$result = DB_Proxy::query( $query );

	$xls = new PHPExcel( );

	$xls->getProperties( )->setCreator( 'Essential Elements' )
			->setLastModifiedBy( 'Essential Elements' )->setTitle( 'Menu Export' )
			->setSubject( 'Menu Export' )->setDescription( 'Menu Export' )
			->setKeywords( 'menu export pos' );

	$columns = array(
		'id' => array(
			'title' => 'Id',
			'type' => PHPExcel_Cell_DataType::TYPE_STRING
		),
		'name' => array(
			'title' => 'Name'
		),
		'item_code' => array(
			'title' => 'Item Code',
			'type' => PHPExcel_Cell_DataType::TYPE_STRING
		),
		'upc' => array(
			'title' => 'UPC',
			'type' => PHPExcel_Cell_DataType::TYPE_STRING
		),
		'active' => array(
			'title' => 'Active'
		),
		'max' => array(
			'title' => 'Max'
		),
		'reorder_point' => array(
			'title' => 'Reorder Point'
		),
		'reorder_amount' => array(
			'title' => 'Reorder Amount'
		),
		'ordertype' => array(
			'title' => 'OrderType',
			'type' => PHPExcel_Cell_DataType::TYPE_NUMERIC
		),
		'group_name' => array(
			'title' => 'Group Name'
		),
		'price' => array(
			'title' => 'Price',
			'type' => PHPExcel_Cell_DataType::TYPE_NUMERIC,
			'format' => PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE
		),
		'cost' => array(
			'title' => 'Cost',
			'type' => PHPExcel_Cell_DataType::TYPE_NUMERIC,
			'format' => PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE
		),
		'tax' => array(
			'title' => 'Tax',
			'type' => PHPExcel_Cell_DataType::TYPE_STRING
		),
		'percent' => array(
			'title' => 'Percent',
			'type' => PHPExcel_Cell_DataType::TYPE_NUMERIC
		)
	);

	$colId = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$lastCol = $colId[count($columns) - 1];

	$xls->setActiveSheetIndex( 0 );
	$xlsAs = $xls->getActiveSheet( );

	$xls->getDefaultStyle( )->getFont( )->setName( 'Arial' )->setSize( '10' );
	$xlsAs->getPageSetup( )->setRowsToRepeatAtTopByStartAndEnd( 1, 1 );

	$colNum = 0;
	foreach( $columns as $col ) {
		$xlsAs->setCellValueByColumnAndRow( $colNum, 1, $col['title'] );
		$colNum++;
	}

	$rowNum = 2;
	while( $r = mysql_fetch_array( $result ) ) {
		$colNum = 0;
		foreach( $columns as $colKey => $col ) {
			$xlsAc = $xlsAs->getCellByColumnAndRow( $colNum, $rowNum );

			if( isset( $col['type'] ) )
				$xlsAc->setValueExplicit( $r[$colKey], $col['type'] );
			else $xlsAc->setValue( $r[$colKey] );

			if( isset( $col['format'] ) )
				$xlsAs->getStyleByColumnAndRow( $colNum, $rowNum )->getNumberFormat( )
					->setFormatCode( $col['format'] );
			$colNum++;
		}
		$rowNum++;
	}
	$lastRow = $rowNum - 1;

	$headerStyle = array(
		'borders' => array(
			'outline' => array(
				'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
				'color' => array( 'argb' => 'FF000000' )
			)
		)
	);
	$xlsAs->getStyle( 'A1:' . $lastCol . '1' )
		->applyFromArray( $headerStyle );

	for( $counter = 0; $counter < count( $columns ); $counter++ ) {
		$xlsAs->getColumnDimension( $colId[$counter] )->setAutoSize( true );
	}

	header( "Content-type: application/vnd.ms-excel" );
	header( "Content-Disposition: attachment; filename=menu.xls" );
	header( 'Cache-Control: max-age=0' );
	header( "Pragma: no-cache" );
	header( "Expires: 100" );

	$xlsWriter = PHPExcel_IOFactory::createWriter( $xls, 'Excel5' );
	$xlsWriter->save( 'php://output' );
}
?>
