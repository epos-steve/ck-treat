<head>
<STYLE>
#loading {
 	width: 170px;
 	height: 48px;
 	background-color: white;
 	position: absolute;
 	left: 50%;
 	top: 50%;
 	margin-top: -50px;
 	margin-left: -100px;
 	text-align: center;
}
</STYLE>

<script type="text/javascript" src="preLoadingMessage.js"></script>
</head>

<?php

ini_set("display_errors","true");
/*
 * Old functions
 * Added back in due to discrepancies in exported data
 */
function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+4);
        $diff=round($diff, 2);
        if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
        elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
        $diff = substr($diff, 0, $dot+3);
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        return $diff;
}

function nextday($nextd,$day_format=''){ //Function returns next day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monn = substr($nextd, 5, 2);
   $dayn = substr($nextd, 8, 2);
   $yearn = substr($nextd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monn, $dayn+1, $yearn));
   return $tempdate;
}

function prevday($prevd,$day_format=''){ //Function returns previous day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monp = substr($prevd, 5, 2);
   $dayp = substr($prevd, 8, 2);
   $yearp = substr($prevd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monp, $dayp-1, $yearp));
   return $tempdate;
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      $day=$day-31;  
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";} 
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

if ( !defined('DOC_ROOT'))
{
	define('DOC_ROOT', dirname(dirname(__FILE__)));
}
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');

$style = "text-decoration:none";
$creditamount="creditamount";
$creditidnty="creditidnty";
$creditdate="creditdate";
$debitamount="debitamount";
$debitidnty="debitidnty";
$debitdate="debitdate";
$quatertotal=0;
$monthtotal=0;
$total=0;
$counter=0;
$findme='.';
$double='.00';
$single='0';
$double2='00';
$cat="CAT";

$user = isset($_COOKIE["usercook"])?$_COOKIE["usercook"]:'';
$pass = isset($_COOKIE["passcook"])?$_COOKIE["passcook"]:'';
$businessid = isset($_POST["exportid"])?$_POST["exportid"]:'';
$companyid = isset($_POST["cid"])?$_POST["cid"]:'';
$date1 = isset($_POST["date1"])?$_POST["date1"]:'';
$date2 = isset($_POST["date2"])?$_POST["date2"]:'';
$override = isset($_POST["override"])?$_POST["override"]:'';
$monthrange = isset($_POST["month"])?$_POST["month"]:'';

$expsales = isset($_POST["sales"])?$_POST["sales"]:'';
$expinvoice = isset($_POST["invoice"])?$_POST["invoice"]:'';
$expapinvoice = isset($_POST["apinvoice"])?$_POST["apinvoice"]:'';
$expdeposit = isset($_POST["deposit"])?$_POST["deposit"]:'';
$expinventor = isset($_POST["inventory"])?$_POST["inventory"]:'';
$expfees = isset($_POST["fees"])?$_POST["fees"]:'';
$exparaccounts = isset($_POST["araccounts"])?$_POST["araccounts"]:'';
$exportid=$businessid;

$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");

if ($monthrange<10){$monthrange="0$monthrange";}
if ($monthrange==0){$monthrange="12";$year--;$date1="$year-$monthrange-01";$date2="$year-$monthrange-31";}
else {$date1="$year-$monthrange-01";$date2="$year-$monthrange-31";}

$feedate1=$date1;
$feedate2=$date2;

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=mysql_numrows($result);

if ($num!=0){
    $security_level=@mysql_result($result,0,"security_level");
}

if ($num != 1 || $user == "" || $pass == "") 
{
    echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}

elseif ($security_level>=7)
{
    echo "<center><b>$date1 <i>through</i> $date2</b></center>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "UPDATE company SET exporting = '1' WHERE companyid = '$companyid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM company WHERE companyid = '$companyid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $company_code=@mysql_result($result,0,"code");
    $import_num=@mysql_result($result,0,"import_num");
    $import_num2=@mysql_result($result,0,"import_num2");
    $exportdate=@mysql_result($result,0,"exportdate");
    $my_export=@mysql_result($result,0,"last_export");
	$bus_category=@mysql_result($result,0,"business_hierarchy");
    $lastexportmonth=$my_export;

    //if ($my_export==$monthrange){$newnum=$import_num+1;}
    //elseif ($lastexportmonth<$month){$import_num2=$import_num+1;$import_num=1;$newnum=2;}
    //elseif ($monthrange<$month){$import_num=$import_num2;$import_num2++;$newnum=$import_num;}

    if ($import_num==99){$newnum=1;}
    else{$newnum=$import_num+1;}

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "UPDATE company SET exportby = '$user', exportdate = '$today', last_export = '$monthrange', import_num = '$newnum', import_num2 = '$import_num2' WHERE companyid = '$companyid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    if ($import_num<10){$import_num="0$import_num";}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Export all businesses ////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
if($expsales=="on"){
    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query9 = "SELECT * FROM business WHERE exp_sales = '0' AND companyid = '$companyid'";
    $result9 = Treat_DB_ProxyOld::query($query9);
    $num9=mysql_numrows($result9);
    //mysql_close();

    $myFile = DIR_EXPORTS."/sales$today-$companyid.csv";
    $fh = fopen($myFile, 'w');


//////CREDITS/////////////////////////////////////////////////////////////////////////////////////////////////////
$num9--;
while ($num9>=0){
    $businessid=@mysql_result($result9,$num9,"businessid");
    $businessname=@mysql_result($result9,$num9,"businessname");
	$businessname = str_replace(","," ",$businessname);
    $unit=@mysql_result($result9,$num9,"unit");
    $over_short_acct=@mysql_result($result9,$num9,"over_short_acct");

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query7 = "SELECT * FROM submit WHERE businessid = '$businessid' AND date >= '$date1' AND date <= '$date2' AND export = '0'";
    $result7 = Treat_DB_ProxyOld::query($query7);
    $num7=mysql_numrows($result7);
    //mysql_close();

$num7--;
while ($num7 >= 0){

    $submitid=@mysql_result($result7,$num7,"submitid");
    $day=@mysql_result($result7,$num7,"date");
    $credittotal=0;

    $exyear=substr($day,0,4);
    $exyear2=substr($day,2,2);
    $exmonth=substr($day,5,2);
    $exday=substr($day,8,2);
    $exdate="$exyear$exmonth$exday";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM credittype ORDER BY credittypename DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=mysql_numrows($result);
    //mysql_close();

    $num--;
    while ($num>=0){
       $credittypename=@mysql_result($result,$num,"credittypename");
       $credittypeid=@mysql_result($result,$num,"credittypeid");

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query2 = "SELECT * FROM credits WHERE businessid = '$businessid' AND credittype = '$credittypeid' AND is_deleted = '0' ORDER BY credittype,creditname DESC";
          $result2 = Treat_DB_ProxyOld::query($query2);
          $num2=mysql_numrows($result2);
          //mysql_close();

          $num2--;
          while ($num2>=0){
             $prevamount=0;
             $creditname=@mysql_result($result2,$num2,"creditname");
             $creditid=@mysql_result($result2,$num2,"creditid");
             $account=@mysql_result($result2,$num2,"account");
             $invoicesales=@mysql_result($result2,$num2,"invoicesales");
             $invoicesales_notax=@mysql_result($result2,$num2,"invoicesales_notax");
             $invoicetax=@mysql_result($result2,$num2,"invoicetax");
             $servchrg=@mysql_result($result2,$num2,"servchrg");
             $salescode=@mysql_result($result2,$num2,"salescode");
////////CREDIT
             if ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0){
                  //mysql_connect($dbhost,$username,$password);
                  //@mysql_select_db($database) or die( "Unable to select database");
                  $query5 = "SELECT amount FROM creditdetail WHERE creditid = '$creditid' AND date = '$day'";
                  $result5 = Treat_DB_ProxyOld::query($query5);
                  $num5=mysql_numrows($result5);
                  //mysql_close();

                  if ($num5==1&&$invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0){
                     $prevamount=@mysql_result($result5,0,"amount");
                     $prevamount = money($prevamount);
                  }
                  else{$prevamount="0";}
             }
///INVOICE
             elseif ($invoicesales==1&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0&&$salescode==0){
                  //mysql_connect($dbhost,$username,$password);
                  //@mysql_select_db($database) or die( "Unable to select database");
                  $query5 = "SELECT total,taxtotal,servamt FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND taxable = 'on' AND salescode = '0' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = Treat_DB_ProxyOld::query($query5);
                  $num6=mysql_numrows($result5);
                  //mysql_close();
                  $num6--;
                  $prevamount=0;
                  while ($num6>=0){
                     $invtotal=@mysql_result($result5,$num6,"total");
                     $invtax=@mysql_result($result5,$num6,"taxtotal");
                     $servamt=@mysql_result($result5,$num6,"servamt");
                     $prevamount=$prevamount+($invtotal-$invtax-$servamt);
                     $num6--;
                  }
                  $prevamount=money($prevamount);
             }
///INVOICE
             elseif ($invoicesales==0&&$invoicesales_notax==1&&$invoicetax==0&&$servchrg==0&&$salescode==0){
                  //mysql_connect($dbhost,$username,$password);
                  //@mysql_select_db($database) or die( "Unable to select database");
                  $query5 = "SELECT total,taxtotal,servamt FROM invoice WHERE type = '1' AND companyid = '$companyid' AND businessid = '$businessid' AND date = '$day' AND taxable = 'off' AND salescode = '0' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = Treat_DB_ProxyOld::query($query5);
                  $num6=mysql_numrows($result5);
                  //mysql_close();
                  $num6--;
                  $prevamount=0;
                  while ($num6>=0){
                     $invtotal=@mysql_result($result5,$num6,"total");
                     $servamt=@mysql_result($result5,$num6,"servamt");
                     $prevamount=$prevamount+$invtotal-$servamt;
                     $num6--;
                  }
                  $prevamount=money($prevamount); 
             }
///INVOICE
             elseif ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==1&&$servchrg==0&&$salescode==0){
                  //mysql_connect($dbhost,$username,$password);
                  //@mysql_select_db($database) or die( "Unable to select database");
                  $query5 = "SELECT taxtotal FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND taxable = 'on' AND salescode = '0' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = Treat_DB_ProxyOld::query($query5);
                  $num6=mysql_numrows($result5);
                  //mysql_close();
                  $num6--;
                  $prevamount=0;
                  while ($num6>=0){
                     $taxtotal=@mysql_result($result5,$num6,"taxtotal");
                     $prevamount=$prevamount+$taxtotal;
                     $num6--;
                  }
                  $prevamount=money($prevamount);  
             }
///INVOICE
             elseif ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==1&&$salescode==0){
                  //mysql_connect($dbhost,$username,$password);
                  //@mysql_select_db($database) or die( "Unable to select database");
                  $query5 = "SELECT servamt FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND salescode = '0' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = Treat_DB_ProxyOld::query($query5);
                  $num6=mysql_numrows($result5);
                  //mysql_close();
                  $num6--;
                  $prevamount=0;
                  while ($num6>=0){
                     $servamt=@mysql_result($result5,$num6,"servamt");
                     $prevamount=$prevamount+$servamt;
                     $num6--;
                  }
                  $prevamount=money($prevamount); 
             }
///INVOICE SALES (SALESCODE)
             elseif ($invoicesales==1&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0&&$salescode!="0"){
                  //mysql_connect($dbhost,$username,$password);
                  //@mysql_select_db($database) or die( "Unable to select database");
                  $query5 = "SELECT total,taxtotal,servamt FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND taxable = 'on' AND salescode = '$salescode' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = Treat_DB_ProxyOld::query($query5);
                  $num6=mysql_numrows($result5);
                  //mysql_close();
                  $num6--;
                  $prevamount=0;
                  while ($num6>=0){
                     $invtotal=@mysql_result($result5,$num6,"total");
                     $invtax=@mysql_result($result5,$num6,"taxtotal");
                     $servamt=@mysql_result($result5,$num6,"servamt");
                     $prevamount=$prevamount+($invtotal-$invtax-$servamt);
                     $num6--;
                  }
                  $prevamount=money($prevamount);  
             }
///INVOICE TAX EXEMPT (SALESCODE)
             elseif ($invoicesales==0&&$invoicesales_notax==1&&$invoicetax==0&&$servchrg==0&&$salescode!="0"){
                  //mysql_connect($dbhost,$username,$password);
                  //@mysql_select_db($database) or die( "Unable to select database");
                  $query5 = "SELECT total,taxtotal,servamt FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND taxable = 'off' AND salescode = '$salescode' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = Treat_DB_ProxyOld::query($query5);
                  $num6=mysql_numrows($result5);
                  //mysql_close();
                  $num6--;
                  $prevamount=0;
                  while ($num6>=0){
                     $invtotal=@mysql_result($result5,$num6,"total");
                     $servamt=@mysql_result($result5,$num6,"servamt");
                     $prevamount=$prevamount+$invtotal-$servamt;
                     $num6--;
                  }
                  $prevamount=money($prevamount); 
             }
///INVOICE TAX (SALESCODE)
             elseif ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==1&&$servchrg==0&&$salescode!="0"){
                  //mysql_connect($dbhost,$username,$password);
                  //@mysql_select_db($database) or die( "Unable to select database");
                  $query5 = "SELECT taxtotal FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND taxable = 'on' AND salescode = '$salescode' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = Treat_DB_ProxyOld::query($query5);
                  $num6=mysql_numrows($result5);
                  //mysql_close();
                  $num6--;
                  $prevamount=0;
                  while ($num6>=0){
                     $taxtotal=@mysql_result($result5,$num6,"taxtotal");
                     $prevamount=$prevamount+$taxtotal;
                     $num6--;
                  }
                  $prevamount=money($prevamount);  
             }
///INVOICE SERVICE CHARGE (SALESCODE)
             elseif ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==1&&$salescode!="0"){
                  //mysql_connect($dbhost,$username,$password);
                  //@mysql_select_db($database) or die( "Unable to select database");
                  $query5 = "SELECT servamt FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND salescode = '$salescode' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = Treat_DB_ProxyOld::query($query5);
                  $num6=mysql_numrows($result5);
                  //mysql_close();
                  $num6--;
                  $prevamount=0;
                  while ($num6>=0){
                     $servamt=@mysql_result($result5,$num6,"servamt");
                     $prevamount=$prevamount+$servamt;
                     $num6--;
                  }
                  $prevamount=money($prevamount); 
             }
             if ($prevamount!=0){
                $credittotal=$credittotal+$prevamount;
                $prevamount=money($prevamount);
                //echo "$day,$account,$unit,$prevamount<br>";
                $stringdata="$company_code,$exmonth$exday$import_num,$exdate,$account,Sales $unit $businessname - $creditname,$prevamount\n";
                fwrite($fh, $stringdata);
             }

             $num2--;
          }
       $num--;
    }

//////DEBITS/////////////////////////////////////////////////////////////////////////////////////////////////////
    $debittotal=0;   

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM debittype ORDER BY debittypename DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=mysql_numrows($result);
    //mysql_close();

    $num--;
    while ($num>=0){
       $debittypename=@mysql_result($result,$num,"debittypename");
       $debittypeid=@mysql_result($result,$num,"debittypeid");
       
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM debits WHERE businessid = '$businessid' AND debittype = '$debittypeid' ORDER BY debittype,debitname DESC";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=mysql_numrows($result2);
       //mysql_close();

       $num2--;
       while ($num2>=0){
          $prevamount=0;
          $debitname=@mysql_result($result2,$num2,"debitname");
          $debitid=@mysql_result($result2,$num2,"debitid");
          $account=@mysql_result($result2,$num2,"account");

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query5 = "SELECT amount FROM debitdetail WHERE debitid = '$debitid' AND date = '$day'";
          $result5 = Treat_DB_ProxyOld::query($query5);
          $num5=mysql_numrows($result5);
          //mysql_close();

          if ($num5!=0){$prevamount=@mysql_result($result5,0,"amount");}
          if ($prevamount!=0){$debittotal=$debittotal+$prevamount;$prevamount=$prevamount*-1;$prevamount=money($prevamount);}

          if ($prevamount!=0){
             //echo "$day,$account,$unit,$prevamount<br>";
             $stringdata="$company_code,$exmonth$exday$import_num,$exdate,$account,Sales $unit $businessname - $debitname,$prevamount\n";
             fwrite($fh, $stringdata);
          }

          $num2--;
       }
       $num--;
    }

    $cashamount=0;

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query2 = "SELECT * FROM cash WHERE businessid = '$businessid' AND date = '$day'";
    $result2 = Treat_DB_ProxyOld::query($query2);
    $num2=mysql_numrows($result2);
    //mysql_close();

    if ($num2!=0){
       $cashamount=@mysql_result($result2,0,"amount");
       $type=@mysql_result($result2,0,"type");
       $cashamount=money($cashamount);
    }

    if ($cashamount!=0){
       if ($type==0){$cashamount=$cashamount*-1;}
       $stringdata="$company_code,$exmonth$exday$import_num,$exdate,$over_short_acct,Sales $unit $businessname,$cashamount\n";
       //echo "$day,$over_short_acct,$unit,$cashamount<br>";
       fwrite($fh, $stringdata);
    }
 
    $day=nextday($day);

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query11 = "UPDATE submit SET export = '1' WHERE submitid = '$submitid'";
    $result11 = Treat_DB_ProxyOld::query($query11);
    //mysql_close();


$num7--;
}
$num9--;
}

///////////////////////////////////////////////////////////
////////////////////MIRROR ACCOUNTS////////////////////////
///////////////////////////////////////////////////////////
if($bus_category > 0){
	$query9 = "SELECT * FROM business WHERE categoryid = $bus_category";
    $result9 = Treat_DB_ProxyOld::query($query9);
    $num9=mysql_numrows($result9);


//////CREDITS/////////////////////////////////////////////////////////////////////////////////////////////////////
$num9--;
while ($num9>=0){
    $businessid=@mysql_result($result9,$num9,"businessid");
    $businessname=@mysql_result($result9,$num9,"businessname");
	$businessname = str_replace(","," ",$businessname);
    $unit=@mysql_result($result9,$num9,"unit");
    $over_short_acct=@mysql_result($result9,$num9,"over_short_acct");

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query7 = "SELECT * FROM mirror_submit WHERE businessid = '$businessid' AND date >= '$date1' AND date <= '$date2' AND export = '0'";
    $result7 = Treat_DB_ProxyOld::query($query7);
    $num7=mysql_numrows($result7);
    //mysql_close();

$num7--;
while ($num7 >= 0){

    $day=@mysql_result($result7,$num7,"date");
    $credittotal=0;

    $exyear=substr($day,0,4);
    $exyear2=substr($day,2,2);
    $exmonth=substr($day,5,2);
    $exday=substr($day,8,2);
    $exdate="$exyear$exmonth$exday";

    $query = "SELECT * FROM credittype ORDER BY credittypename DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=mysql_numrows($result);

    $num--;
    while ($num>=0){
       $credittypename=@mysql_result($result,$num,"credittypename");
       $credittypeid=@mysql_result($result,$num,"credittypeid");

          $query2 = "SELECT * FROM credits WHERE businessid = '$businessid' AND credittype = '$credittypeid' AND is_deleted = '0' ORDER BY credittype,creditname DESC";
          $result2 = Treat_DB_ProxyOld::query($query2);
          $num2=mysql_numrows($result2);

          $num2--;
          while ($num2>=0){
             $prevamount=0;
             $creditname=@mysql_result($result2,$num2,"creditname");
             $creditid=@mysql_result($result2,$num2,"creditid");
             $account=@mysql_result($result2,$num2,"account");
             $invoicesales=@mysql_result($result2,$num2,"invoicesales");
             $invoicesales_notax=@mysql_result($result2,$num2,"invoicesales_notax");
             $invoicetax=@mysql_result($result2,$num2,"invoicetax");
             $servchrg=@mysql_result($result2,$num2,"servchrg");
             $salescode=@mysql_result($result2,$num2,"salescode");

			 ///mirror account number
			 $query5 = "SELECT accountnum FROM mirror_accounts WHERE accountid = $creditid AND type = 1 AND category = $bus_category";
			 $result5 = Treat_DB_ProxyOld::query($query5);

			 $account = @mysql_result($result5,0,"accountnum");

			////////CREDIT
             $query5 = "SELECT amount FROM creditdetail WHERE creditid = '$creditid' AND date = '$day'";
             $result5 = Treat_DB_ProxyOld::query($query5);
             $num5=mysql_numrows($result5);

             if ($num5==1&&$invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0){
                     $prevamount=@mysql_result($result5,0,"amount");
                     $prevamount = money($prevamount);
             }
             else{$prevamount="0";}

             if ($prevamount!=0 && $account != -1){
                $credittotal=$credittotal+$prevamount;
                $prevamount=money($prevamount);
                $stringdata="$company_code,$exmonth$exday$import_num,$exdate,$account,Sales $unit $businessname - $creditname,$prevamount\n";
                fwrite($fh, $stringdata);
             }

             $num2--;
          }
       $num--;
    }

	//////DEBITS/////////////////////////////////////////////////////////////////////////////////////////////////////
    $debittotal=0;

    $query = "SELECT * FROM debittype ORDER BY debittypename DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=mysql_numrows($result);

    $num--;
    while ($num>=0){
       $debittypename=@mysql_result($result,$num,"debittypename");
       $debittypeid=@mysql_result($result,$num,"debittypeid");

       $query2 = "SELECT * FROM debits WHERE businessid = '$businessid' AND debittype = '$debittypeid' ORDER BY debittype,debitname DESC";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=mysql_numrows($result2);

       $num2--;
       while ($num2>=0){
          $prevamount=0;
          $debitname=@mysql_result($result2,$num2,"debitname");
          $debitid=@mysql_result($result2,$num2,"debitid");
          $account=@mysql_result($result2,$num2,"account");

		  ///mirror account number
		  $query5 = "SELECT accountnum FROM mirror_accounts WHERE accountid = $debitid AND type = 2 AND category = $bus_category";
		  $result5 = Treat_DB_ProxyOld::query($query5);

		  $account = @mysql_result($result5,0,"accountnum");

          $query5 = "SELECT amount FROM debitdetail WHERE debitid = '$debitid' AND date = '$day'";
          $result5 = Treat_DB_ProxyOld::query($query5);
          $num5=mysql_numrows($result5);

          if ($num5!=0){$prevamount=@mysql_result($result5,0,"amount");}
          if ($prevamount!=0){$debittotal=$debittotal+$prevamount;$prevamount=$prevamount*-1;$prevamount=money($prevamount);}

          if ($prevamount!=0 && $account != -1){
             //echo "$day,$account,$unit,$prevamount<br>";
             $stringdata="$company_code,$exmonth$exday$import_num,$exdate,$account,Sales $unit $businessname - $debitname,$prevamount\n";
             fwrite($fh, $stringdata);
          }

          $num2--;
       }
       $num--;
    }

    $query11 = "UPDATE mirror_submit SET export = '1' WHERE businessid = $businessid AND date = '$day'";
    $result11 = Treat_DB_ProxyOld::query($query11);

	$day=nextday($day);


$num7--;
}
$num9--;
}
}
///////////////////////TRANSFERS///////////////////////////

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM business WHERE companyid = '$companyid' AND exp_trans = '0'";
    $result = Treat_DB_ProxyOld::query($query);
    $num=mysql_numrows($result);
    //mysql_close();

    $num--;
    while ($num>=0){
       $businessid=@mysql_result($result,$num,"businessid");
       $unit=@mysql_result($result,$num,"unit");

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM apinvoice WHERE businessid = '$businessid' AND date >= '$date1' AND date <= '$date2' AND transfer = '1' AND export = '0' AND posted = '1'";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=mysql_numrows($result2);
       //mysql_close();

       $num2--;
       while ($num2>=0){

          $apinvoiceid=@mysql_result($result2,$num2,"apinvoiceid");
          $invoicenum=@mysql_result($result2,$num2,"invoicenum");
          $vendorid=@mysql_result($result2,$num2,"vendor");
          $invdate=@mysql_result($result2,$num2,"date");
          $invoicetotal=@mysql_result($result2,$num2,"total");
          $invoicetotal=money($invoicetotal);

          $exyear=substr($invdate,0,4);
          $exmonth=substr($invdate,5,2);
          $exday=substr($invdate,8,2);
          $exyear2=substr($invdate,2,2);
          $exdate="$exyear$exmonth$exday";

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query3 = "SELECT unit FROM business WHERE businessid = '$vendorid'";
          $result3 = Treat_DB_ProxyOld::query($query3);
          $num3=mysql_numrows($result3);
          //mysql_close();

          $tfr_unit=@mysql_result($result3,0,"unit");

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query3 = "SELECT * FROM apinvoicedetail WHERE apinvoiceid = '$apinvoiceid'";
          $result3 = Treat_DB_ProxyOld::query($query3);
          $num3=mysql_numrows($result3);
          //mysql_close();

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query7 = "UPDATE apinvoice SET export = '1' WHERE apinvoiceid = '$apinvoiceid'";
          $result7 = Treat_DB_ProxyOld::query($query7);
          //mysql_close();

          $num3--;
          while ($num3>=0){
             $apaccountid=@mysql_result($result3,$num3,"apaccountid");
             $apamount=@mysql_result($result3,$num3,"amount");
             $tfr_acct=@mysql_result($result3,$num3,"tfr_acct");
			 $tfr_item=@mysql_result($result3,$num3,"item");
			 $tfr_item=str_replace(","," ",$tfr_item);
             $apamount=money($apamount);

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query4 = "SELECT * FROM acct_payable WHERE acct_payableid = '$apaccountid'";
             $result4 = Treat_DB_ProxyOld::query($query4);
             //mysql_close();

             $apaccountnum=@mysql_result($result4,0,"accountnum");
             $from_co=@mysql_result($result4,0,"companyid");

             $query4 = "SELECT code FROM company WHERE companyid = '$from_co'";
             $result4 = Treat_DB_ProxyOld::query($query4);

             $from_co=@mysql_result($result4,0,"code");

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query4 = "SELECT * FROM acct_payable WHERE acct_payableid = '$tfr_acct'";
             $result4 = Treat_DB_ProxyOld::query($query4);
             //mysql_close();

             $tfraccountnum=@mysql_result($result4,0,"accountnum");
             $to_co=@mysql_result($result4,0,"companyid");

             $query4 = "SELECT code FROM company WHERE companyid = '$to_co'";
             $result4 = Treat_DB_ProxyOld::query($query4);

             $to_co=@mysql_result($result4,0,"code");

             $apamount=$apamount*-1;
             $stringdata="$from_co,$exmonth$exday$import_num,$exdate,$apaccountnum,TFR $unit - $tfr_unit: $tfr_item,$apamount\n";
             if ($apamount!=0){fwrite($fh, $stringdata);}
             $apamount=$apamount*-1;
             $stringdata="$to_co,$exmonth$exday$import_num,$exdate,$tfraccountnum,TFR $unit - $tfr_unit: $tfr_item,$apamount\n";
             if ($apamount!=0){fwrite($fh, $stringdata);}
             $num3--;
          }
          $num2--;
       }
       $num--;
    }

/////////////////////////END TRANSFERS/////////////////////
/////////////////////////PURACHASE CARDS///////////////////////////

    $day4 = date("d");
    $year4 = date("Y");
    $month4 = date("m");
    $invdate="$year4-$month4-$day4";
	
	if(substr($date2,5,2)!=$month4){
	   $invdate="$year4-$month4-01";
	   $invdate=prevday($invdate);
	}

    $exyear=substr($invdate,0,4);
    $exmonth=substr($invdate,5,2);
    $exday=substr($invdate,8,2);
    $exyear2=substr($invdate,2,2);
    $exdate="$exyear$exmonth$exday";

    $purch_total=0;
    $amex_purch_total=0;

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM business WHERE companyid = '$companyid' AND exp_trans = '0'";
    $result = Treat_DB_ProxyOld::query($query);
    $num=mysql_numrows($result);
    //mysql_close();

    $num--;
    while ($num>=0){
       $businessid=@mysql_result($result,$num,"businessid");
       $unit=@mysql_result($result,$num,"unit");

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM apinvoice WHERE businessid = '$businessid' AND date >= '$date1' AND date <= '$date2' AND transfer = '2' AND export = '0' AND posted = '1'";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=mysql_numrows($result2);
       //mysql_close();

       $num2--;
       while ($num2>=0){

          $apinvoiceid=@mysql_result($result2,$num2,"apinvoiceid");
          $invoicenum=@mysql_result($result2,$num2,"invoicenum");
          $vendorid=@mysql_result($result2,$num2,"vendor");
          $pc_type=@mysql_result($result2,$num2,"pc_type");
          $invdate=@mysql_result($result2,$num2,"date");
          $invoicetotal=@mysql_result($result2,$num2,"total");
          $invoicetotal=money($invoicetotal);

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          if($pc_type==0){$query37 = "SELECT firstname,lastname FROM login WHERE loginid = '$vendorid'";}
          elseif($pc_type==1){$query37 = "SELECT firstname,lastname FROM login_route WHERE login_routeid = '$vendorid'";}
          $result37 = Treat_DB_ProxyOld::query($query37);
          //mysql_close();

          $firstname=@mysql_result($result37,0,"firstname");
          $lastname=@mysql_result($result37,0,"lastname");

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query3 = "SELECT * FROM apinvoicedetail WHERE apinvoiceid = '$apinvoiceid'";
          $result3 = Treat_DB_ProxyOld::query($query3);
          $num3=mysql_numrows($result3);
          //mysql_close();

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query7 = "UPDATE apinvoice SET export = '1' WHERe apinvoiceid = '$apinvoiceid'";
          $result7 = Treat_DB_ProxyOld::query($query7);
          //mysql_close();

          $num3--;
          while ($num3>=0){
             $apaccountid=@mysql_result($result3,$num3,"apaccountid");
             $apamount=@mysql_result($result3,$num3,"amount");
             $itemname=@mysql_result($result3,$num3,"item");
             $itemname=str_replace(",",".",$itemname);
             $apamount=money($apamount*-1);

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query4 = "SELECT * FROM acct_payable WHERE acct_payableid = '$apaccountid'";
             $result4 = Treat_DB_ProxyOld::query($query4);
             //mysql_close();

             $apaccountnum=@mysql_result($result4,0,"accountnum");

             $stringdata="$company_code,$exmonth$exday$import_num,$exdate,$apaccountnum,PCARD: $firstname $lastname ($invdate) - $itemname,$apamount\n";
             if ($apamount!=0){fwrite($fh, $stringdata);}

             if($businessid==188){$amex_purch_total+=$apamount;}
             else{$purch_total=$purch_total+$apamount;}
             $num3--;
          }
          $num2--;
       }
       $num--;
    }

    $amex_purch_total=money($amex_purch_total*-1);
    $purch_total=money($purch_total*-1);

    $stringdata="$company_code,$exmonth$exday$import_num,$exdate,21950-0000-00100,PURCHASE CARD TOTAL,$purch_total\n";
    if ($purch_total!=0){fwrite($fh, $stringdata);}

    $stringdata="$company_code,$exmonth$exday$import_num,$exdate,21960-0000-00000,AMEX PURCHASE CARD TOTAL,$amex_purch_total\n";
    if ($amex_purch_total!=0){fwrite($fh, $stringdata);}

/////////////////////////END P-CARDS/////////////////////

fclose($fh);
}
//////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////INVOICES/////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
if($expinvoice=="on"){

    $myFile2 = DIR_EXPORTS."/invoice$today-$companyid.csv";
    $fh = fopen($myFile2, 'w');

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM business WHERE companyid = '$companyid' AND exp_ar = '0'";
    $result = Treat_DB_ProxyOld::query($query);
    $num=mysql_numrows($result);
    //mysql_close();

    $num--;
    while ($num>=0){
       $businessid=@mysql_result($result,$num,"businessid");
       $unit=@mysql_result($result,$num,"unit");
       $ar_unit=@mysql_result($result,$num,"ar_unit");

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM invoice WHERE businessid = '$businessid' AND status = '4' AND posted = '1' AND date >= '$date1' AND date <= '$date2' AND (type = '1' OR type = '5') AND export = '0' ";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=mysql_numrows($result2);
       //mysql_close();

       $num2--;
       while ($num2>=0){

          $invoiceid=@mysql_result($result2,$num2,"invoiceid");
          $accountid=@mysql_result($result2,$num2,"accountid");
          $invdate=@mysql_result($result2,$num2,"date");
          $taxpercent=@mysql_result($result2,$num2,"tax");
          $total=@mysql_result($result2,$num2,"total");
          $taxtotal=@mysql_result($result2,$num2,"taxtotal");
          $costcenter=@mysql_result($result2,$num2,"costcenter");
          $servamt=@mysql_result($result2,$num2,"servamt");
          $export=@mysql_result($result2,$num2,"export");
		  $invoice_num=@mysql_result($result2,$num2,"invoice_num");

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query3 = "SELECT * FROM invtender WHERE invoiceid = '$invoiceid'";
          $result3 = Treat_DB_ProxyOld::query($query3);
          $num3=mysql_numrows($result3);
          //mysql_close();

          $num3--;
          $tendertotal=0;
          while ($num3>=0){
             $tendamount=@mysql_result($result3,$num3,"amount");
             $tendertotal=$tendertotal+$tendamount;
             $num3--;
          }

          $total=money($total-$tendertotal);

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query3 = "SELECT accountnum FROM accounts WHERE accountid = '$accountid'";
          $result3 = Treat_DB_ProxyOld::query($query3);
          //mysql_close();

          $accountnum=@mysql_result($result3,0,"accountnum");

          $subtotal=$total-$taxtotal-$servamt;
          $subtotal=money($subtotal);
         
          if ($export!=1){
             $exyear=substr($invdate,0,4);
             $exmonth=substr($invdate,5,2);
             $exday=substr($invdate,8,2);
             $exdate="$exyear$exmonth$exday";
             $showunit="$ar_unit$cat";
			 if($invoice_num != ""){$showinvoice=$invoice_num;}
			 else{$showinvoice=$invoiceid;}
             $stringdata="$showinvoice,$accountnum,$showunit,$exdate,$total\n";
             if ($total!=0){fwrite($fh, $stringdata);}

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query4 = "UPDATE invoice SET export = '1' WHERE invoiceid = '$invoiceid'";
             $result4 = Treat_DB_ProxyOld::query($query4);
             //mysql_close();
          }

          $num2--;
       }
       $num--;
    }
    fclose($fh);
}
//////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////AP Accounts///////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
if($expapinvoice=="on"){

    $myFile2 = DIR_EXPORTS."/payables$today-$companyid.csv";
    $fh = fopen($myFile2, 'w');

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM business WHERE companyid = '$companyid' AND exp_ap = '0'";
    $result = Treat_DB_ProxyOld::query($query);
    $num=mysql_numrows($result);
    //mysql_close();

    $num--;
    while ($num>=0){
       $businessid=@mysql_result($result,$num,"businessid");
       $unit=@mysql_result($result,$num,"unit");
	   $ap_export_num=@mysql_result($result,$num,"ap_export_num");

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM apinvoice WHERE businessid = '$businessid' AND date >= '$date1' AND date <= '$date2' AND transfer = '0' AND export = '0' AND posted = '1'";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=mysql_numrows($result2);
       //mysql_close();

       $num2--;
       while ($num2>=0){

          $apinvoiceid=@mysql_result($result2,$num2,"apinvoiceid");
          $invoicenum=@mysql_result($result2,$num2,"invoicenum");
          $vendorid=@mysql_result($result2,$num2,"vendor");
          $invdate=@mysql_result($result2,$num2,"date");
          $invoicetotal=@mysql_result($result2,$num2,"total");
          $invoicetotal=money($invoicetotal);

          $exyear=substr($invdate,0,4);
          $exmonth=substr($invdate,5,2);
          $exday=substr($invdate,8,2);
          $exyear2=substr($invdate,2,2);
          $exdate="$exyear$exmonth$exday";

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query3 = "SELECT vendor_num FROM vendors WHERE vendorid = '$vendorid'";
          $result3 = Treat_DB_ProxyOld::query($query3);
          //mysql_close();

          $vendor_num=@mysql_result($result3,0,"vendor_num");

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query3 = "SELECT * FROM apinvoicedetail WHERE apinvoiceid = '$apinvoiceid'";
          $result3 = Treat_DB_ProxyOld::query($query3);
          $num3=mysql_numrows($result3);
          //mysql_close();

          $num3--;
          while ($num3>=0){
             $apaccountid=@mysql_result($result3,$num3,"apaccountid");
             $apamount=@mysql_result($result3,$num3,"amount");
             $apamount=money($apamount);

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query4 = "SELECT * FROM acct_payable WHERE acct_payableid = '$apaccountid'";
             $result4 = Treat_DB_ProxyOld::query($query4);
             //mysql_close();

             $apaccountnum=@mysql_result($result4,0,"accountnum");
             $act1 = substr($apaccountnum, 0, 5);
             $act2 = substr($apaccountnum, 6, 4);
             $act3 = substr($apaccountnum, 11, 5);

             $invlength = strlen($invoicenum);
             $invlength2=$invlength-10;
             if ($invlength>10){$invoicenum=substr($invoicenum, $invlength2, 10);}

             $stringdata="$company_code,$exdate,$act1$act2$act3,$unit,$apamount,$invoicenum,$vendor_num,$invoicetotal,$ap_export_num\n";
             if ($apamount!=0){fwrite($fh, $stringdata);}
             $num3--;
          }
          $num2--;
       }
       $num--;
    }
    fclose($fh);
}
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////AP Invoices//////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
if($expapinvoice=="on"){

    $myFile2 = DIR_EXPORTS."/apinvoice$today-$companyid.csv";
    $fh = fopen($myFile2, 'w');

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM business WHERE companyid = '$companyid' AND exp_ap = '0'";
    $result = Treat_DB_ProxyOld::query($query);
    $num=mysql_numrows($result);
    //mysql_close();

    $num--;
    while ($num>=0){
       $businessid=@mysql_result($result,$num,"businessid");
       $unit=@mysql_result($result,$num,"unit");

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM apinvoice WHERE businessid = '$businessid' AND date >= '$date1' AND date <= '$date2' AND transfer = '0' AND export = '0' AND posted = '1'";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=mysql_numrows($result2);
       //mysql_close();

       $num2--;
       while ($num2>=0){

          $apinvoiceid=@mysql_result($result2,$num2,"apinvoiceid");
          $vendorid=@mysql_result($result2,$num2,"vendor");
          $invdate=@mysql_result($result2,$num2,"date");
          $total=@mysql_result($result2,$num2,"total");
          $invoicenum=@mysql_result($result2,$num2,"invoicenum");

          $invlength = strlen($invoicenum);
          $invlength2=$invlength-10;
          if ($invlength>10){$invoicenum= substr($invoicenum, $invlength2, 10);}

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query3 = "SELECT vendor_num FROM vendors WHERE vendorid = '$vendorid'";
          $result3 = Treat_DB_ProxyOld::query($query3);
          //mysql_close();

          $vendor_num=@mysql_result($result3,0,"vendor_num");

             $exyear=substr($invdate,0,4);
             $exmonth=substr($invdate,5,2);
             $exday=substr($invdate,8,2);
             $exdate="$exyear$exmonth$exday";
             $showunit="$unit$cat";
             $total=money($total);
             $stringdata="$invoicenum,$vendor_num,$unit,$exdate,$total\n";
             if ($total!=0){fwrite($fh, $stringdata);}

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query4 = "UPDATE apinvoice SET export = '1',posted = '1' WHERE apinvoiceid = '$apinvoiceid'";
             $result4 = Treat_DB_ProxyOld::query($query4);
             //mysql_close();

          $num2--;
       }
       $num--;
    }
    fclose($fh);
}
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////Deposits//////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
if($expdeposit=="on"){

    $myFile2 = DIR_EXPORTS."/deposit$today-$companyid.csv";
    $fh = fopen($myFile2, 'w');

    $query = "SELECT * FROM deposit WHERE companyid = '$companyid' AND export = '1' AND date >= '$date1' AND date <= '$date2'";
    $result = Treat_DB_ProxyOld::query($query);
    $num=mysql_numrows($result);

    $num--;
    while ($num>=0){
       $depositid=@mysql_result($result,$num,"depositid");
       $d_date=@mysql_result($result,$num,"date");

       $query2 = "SELECT * FROM depositdetail WHERE depositid = '$depositid'";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=mysql_numrows($result2);

       $num2--;
       while ($num2>=0){

          $detailid=@mysql_result($result2,$num2,"detailid");
          $checknum=@mysql_result($result2,$num2,"checknum");
          $d_total=@mysql_result($result2,$num2,"total");
  
          $query3 = "SELECT * FROM depositinvoice WHERE detailid = '$detailid'";
          $result3 = Treat_DB_ProxyOld::query($query3);
          $num3=mysql_numrows($result3);

          $num3--;
          while($num3>=0){

             $invoiceid=@mysql_result($result3,$num3,"invoiceid");
             $type=@mysql_result($result3,$num3,"type");

             if($type==1){
                $query4 = "SELECT accounts.accountnum,invoice.total FROM accounts,invoice WHERE invoice.invoiceid = '$invoiceid' AND invoice.accountid = accounts.accountid";
                $result4 = Treat_DB_ProxyOld::query($query4);

                $accountnum=@mysql_result($result4,0,"accountnum");
                $inv_total=@mysql_result($result4,0,"total");
                $inv_num=$invoiceid;
             }
             elseif($type==2){
                $query4 = "SELECT * FROM invoice2 WHERE invoiceid2 = '$invoiceid'";
                $result4 = Treat_DB_ProxyOld::query($query4);

                $accountnum=@mysql_result($result4,0,"cust_num");
                $inv_total=@mysql_result($result4,0,"total");
                $inv_num=@mysql_result($result4,0,"invoice_num");
             }

             $stringdata="$checknum,$d_total,$inv_num,$accountnum,$inv_total\n";
             fwrite($fh, $stringdata);

             $num3--;
          }
          $num2--;

          $query5 = "UPDATE deposit SET export = '2' WHERE depositid = '$depositid'";
          $result5 = Treat_DB_ProxyOld::query($query5);
       }
       $num--;
    }
    fclose($fh);
}
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////Inventory/////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
if ($expinventor=="on"){
    
    $date2=$date1;
    $exyear=substr($date1,0,4);
    $exmonth=substr($date1,5,2);
    $exday=substr($date1,8,2);
    $exdate="$exyear$exmonth$exday";
    $exmonth++;
    if ($exmonth==13){$exmonth="01";$exyear++;}
    if ($exmonth<10){$exmonth="0$exmonth";}
    $nextmonthdate="$exyear-$exmonth-01";

    while ($date2<$nextmonthdate){$date2=nextday($date2);}
    $date2=prevday($date2);

    $exyear=substr($date2,0,4);
    $exmonth=substr($date2,5,2);
    $exday=substr($date2,8,2);
    $exdate="$exyear$exmonth$exday";

    $exdate2=substr($exdate,2,6);

    $myFile2 = DIR_EXPORTS."/inventory$today-$companyid.csv";
    $fh = fopen($myFile2, 'w');

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM business WHERE companyid = '$companyid' AND exp_inv = '0'";
    $result = Treat_DB_ProxyOld::query($query);
    $num=mysql_numrows($result);
    //mysql_close();

    $num--;
    while ($num>=0){
       $businessid=@mysql_result($result,$num,"businessid");
       $unit=@mysql_result($result,$num,"unit");
       $inventor_acct=@mysql_result($result,$num,"inventor_acct");

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM inventor WHERE businessid = '$businessid' AND date = '$date1' AND posted = '1' AND export = '0'";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=mysql_numrows($result2);
       //mysql_close();

       $num2--;

       $invtotal=0;
       while ($num2>=0){

          $inventorid=@mysql_result($result2,$num2,"inventorid");
          $acct_payableid=@mysql_result($result2,$num2,"acct_payableid");
          $amount=@mysql_result($result2,$num2,"amount");

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query3 = "SELECT accountnum FROM acct_payable WHERE acct_payableid = '$acct_payableid'";
          $result3 = Treat_DB_ProxyOld::query($query3);
          //mysql_close();

          $accountnum=@mysql_result($result3,0,"accountnum");

          $amount=money($amount);
          $stringdata="$company_code,$exdate2,$exdate,$accountnum,$unit,$amount\n";
          if ($amount!=0){fwrite($fh, $stringdata);}

          $invtotal=$invtotal+$amount;

          $num2--;
       }

       $invtotal=$invtotal*-1;
       $stringdata="$company_code,$exdate2,$exdate,$inventor_acct,$unit,$invtotal\n";
       if ($invtotal!=0){fwrite($fh, $stringdata);}

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "UPDATE inventor SET export = '1' WHERE businessid = '$businessid' AND date = '$date1' AND posted = '1'";
       $result2 = Treat_DB_ProxyOld::query($query2);
       //mysql_close();

       $num--;
    }
    fclose($fh);
}
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////FEES//////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
if ($expfees=="on"){

    $date2=$date1;

    $day = date("d");
    $year = date("Y");
    $month = date("m");
    $today="$year-$month-$day";

    if ($today>$feedate2){
       $exyear=substr($today,0,4);
       $exmonth=substr($today,5,2);

       $today="$exyear-$exmonth-01";
       $today=prevday($today);

       $exyear=substr($today,0,4);
       $exyear2=substr($today,2,2);
       $exmonth=substr($today,5,2);
       $exday=substr($today,8,2);
       $exdate="$exyear$exmonth$exday";
    }
    else{
       $exyear=substr($today,0,4);
       $exyear2=substr($today,2,2);
       $exmonth=substr($today,5,2);
       $exday=substr($today,8,2);
       $exdate="$exyear$exmonth$exday";
    }

    $today="$year-$month-$day";

    $acct696=0;
    $acct697=0;
    $acct209=0;
    
    $myFile2 = DIR_EXPORTS."/fees$today-$companyid.csv";
    $fh = fopen($myFile2, 'w');

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM business WHERE companyid = '$companyid'";
    $result = Treat_DB_ProxyOld::query($query);
    $num=mysql_numrows($result);
    //mysql_close();

    $num--;
    while ($num>=0){
       $businessid=@mysql_result($result,$num,"businessid");
       $businessname=@mysql_result($result,$num,"businessname");
	   $businessname = str_replace(","," ",$businessname);
       $unit=@mysql_result($result,$num,"unit");
       $inventor_acct=@mysql_result($result,$num,"inventor_acct");

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM acct_payable WHERE businessid = '$businessid' AND cs_type = '6' AND is_deleted = '0' AND (accountnum LIKE '696%' OR accountnum LIKE '697%' OR accountnum LIKE '698%' OR accountnum LIKE '627%')";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=mysql_numrows($result2);
       //mysql_close();

       $num2--;

       $invtotal=0;
       while ($num2>=0){

          $cs_prcnt=@mysql_result($result2,$num2,"cs_prcnt");
          $acct_payableid=@mysql_result($result2,$num2,"acct_payableid");
          $acct_name=@mysql_result($result2,$num2,"acct_name");
          $glacct=@mysql_result($result2,$num2,"cs_glacct");
          $accountnum=@mysql_result($result2,$num2,"accountnum");
          $account_specific=substr("$accountnum",0,5);

          $last_export=0;

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query7 = "SELECT * FROM export_history WHERE type = '0' AND gl_id = '$acct_payableid' AND date = '$feedate1'";
          $result7 = Treat_DB_ProxyOld::query($query7);
          $num7=mysql_numrows($result7);
          //mysql_close();

          if ($num7!=0){
             $last_export=@mysql_result($result7,0,"amount");
             $historyid=@mysql_result($result7,0,"historyid");
          }

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          if($glacct==-1){$query3 = "SELECT * FROM credits WHERE businessid = '$businessid' AND (credittype = '1' OR credittype = '5')";}
          else{$query3 = "SELECT * FROM credits WHERE businessid = '$businessid' AND creditid = '$glacct'";}
          $result3 = Treat_DB_ProxyOld::query($query3);
          $num3=mysql_numrows($result3);
          //mysql_close();

          $totalamt=0;

          $num3--;
          while($num3>=0){
             $creditid=@mysql_result($result3,$num3,"creditid");
             $creditname=@mysql_result($result3,$num3,"creditname");
             $invoicesales=@mysql_result($result3,$num3,"invoicesales");
             $invoicesales_notax=@mysql_result($result3,$num3,"invoicesales_notax");
             $servchrg=@mysql_result($result3,$num3,"servchrg");

             if ($invoicesales==0&&$invoicesales_notax==0&&$servchrg==0){
                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query4 = "SELECT SUM(amount) AS totalamt FROM creditdetail WHERE creditid = '$creditid' AND date >= '$feedate1' AND date <= '$feedate2'";
                $result4 = Treat_DB_ProxyOld::query($query4);
                //mysql_close();
             }
             elseif ($invoicesales==1&&$invoicesales_notax==0&&$servchrg==0){
                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query4 = "SELECT SUM(total-taxtotal-servamt) AS totalamt FROM invoice WHERE businessid = '$businessid' AND type = '1' AND taxable = 'on' AND date >= '$feedate1' AND date <= '$feedate2' AND (status = '4' OR status = '2' OR status = '6')";
                $result4 = Treat_DB_ProxyOld::query($query4);
                //mysql_close();
             }
             elseif ($invoicesales==0&&$invoicesales_notax==1&&$servchrg==0){
                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query4 = "SELECT SUM(total-taxtotal-servamt) AS totalamt FROM invoice WHERE businessid = '$businessid' AND type = '1' AND taxable = 'off' AND date >= '$feedate1' AND date <= '$feedate2' AND (status = '4' OR status = '2' OR status = '6')";
                $result4 = Treat_DB_ProxyOld::query($query4);
                //mysql_close();
             }
             elseif ($invoicesales==0&&$invoicesales_notax==0&&$servchrg==1){
                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query4 = "SELECT SUM(servamt) AS totalamt FROM invoice WHERE businessid = '$businessid' AND type = '1' AND date >= '$feedate1' AND date <= '$feedate2' AND (status = '4' OR status = '2' OR status = '6')";
                $result4 = Treat_DB_ProxyOld::query($query4);
                //mysql_close();
             }

             $amount=@mysql_result($result4,0,"totalamt");
             $totalamt=$totalamt+$amount;

             $num3--;
          }

          $totalamt=money($totalamt*($cs_prcnt/100));
          $exp_amt=money(-1*($totalamt-$last_export));

          $stringdata="$company_code,$exmonth$exday$import_num,$exdate,$accountnum,$acct_name $unit $businessname,$exp_amt\n";
          if ($exp_amt!=0){
             fwrite($fh, $stringdata);
             $exp_amt=$exp_amt*-1;
             if ($account_specific=="69700"){$accountnum="69700-0000-00000";$acct697=$acct697+$exp_amt;}
             elseif ($account_specific=="69600"||$account_specific=="69800"){$accountnum="69600-0000-00000";$acct696=$acct696+$exp_amt;}
             elseif ($account_specific=="62700"){$accountnum="62700-0000-00000";$acct209=$acct209+$exp_amt;}
          }

          if ($num7!=0){
             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query7 = "UPDATE export_history SET amount = '$totalamt' WHERE historyid = '$historyid'";
             $result7 = Treat_DB_ProxyOld::query($query7);
             //mysql_close();
          }
          else{
             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query7 = "INSERT INTO export_history (type,gl_id,date,amount) VALUES ('0','$acct_payableid','$feedate1','$totalamt')";
             $result7 = Treat_DB_ProxyOld::query($query7);
             //mysql_close();
          }

          $num2--;
       }

       $num--;
    }
    $acct697=money($acct697);
    $stringdata="$company_code,$exmonth$exday$import_num,$exdate,69700-0000-00000,Admin Fees,$acct697\n";
    if($acct697!=0){fwrite($fh, $stringdata);}
    $acct696=money($acct696);
    $stringdata="$company_code,$exmonth$exday$import_num,$exdate,69600-0000-00000,Mgmt Fees,$acct696\n";
    if($acct696!=0){fwrite($fh, $stringdata);}
    $acct209=money($acct209);
    $stringdata="$company_code,$exmonth$exday$import_num,$exdate,62700-0000-00000,Seminars/Classes/Training,$acct209\n";
    if($acct209!=0){fwrite($fh, $stringdata);}

    fclose($fh);
}
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////ACCOUNTS//////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
if($exparaccounts=="on"){

    $myFile2 = DIR_EXPORTS."/accts$today-$companyid.csv";
    $fh = fopen($myFile2, 'w');

    $query = "SELECT * FROM accounts WHERE companyid = '$companyid' AND export = '1'";
    $result = Treat_DB_ProxyOld::query($query);
    $num=mysql_numrows($result);

    $num_accts=$num;

    $num--;
    while ($num>=0){
       $accountid=@mysql_result($result,$num,"accountid");
       $acct_bid=@mysql_result($result,$num,"businessid");
       $acct_cid=@mysql_result($result,$num,"companyid");
       $acct_name=@mysql_result($result,$num,"name");
       $acct_num=@mysql_result($result,$num,"accountnum");
       $acct_attn=@mysql_result($result,$num,"attn");
       $acct_street=@mysql_result($result,$num,"street");
       $acct_city=@mysql_result($result,$num,"city");
       $acct_state=@mysql_result($result,$num,"state");
       $acct_zip=@mysql_result($result,$num,"zip");
       $acct_phone=@mysql_result($result,$num,"phone");
       $acct_fax=@mysql_result($result,$num,"fax");
       $acct_email=@mysql_result($result,$num,"email");

       $query2 = "UPDATE accounts SET export = '0' WHERE accountid = '$accountid'";
       $result2 = Treat_DB_ProxyOld::query($query2);

       fwrite($fh, "$accountid,$acct_bid,$acct_cid,\"$acct_name\",$acct_num,\"$acct_attn\",\"$acct_street\",$acct_city,$acct_state,$acct_zip,$acct_phone,$acct_fax,$acct_email\n");

       $num--;
    }
    fclose($fh);
}
//////////////////////////////////////////////////////////////////////////////////////////
    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "UPDATE company SET exporting = '0' WHERE companyid = '$companyid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();
//////////////////////////////////////////////////////////////////////////////////////////
   $style = "text-decoration:none";

   echo "<p><br><p><br><p><br><center><table style=\"border: 2px solid #999999;\">";
   if ($expsales=="on"){echo "<tr><td><li></td><td><a target='_blank' href='".HTTP_EXPORTS."/sales$today-$companyid.csv'>Download the Sales Export Here</a></td></tr>";}
   if ($expinvoice=="on"){echo "<tr><td><li></td><td><a target='_blank' href='".HTTP_EXPORTS."/invoice$today-$companyid.csv'>Download the Invoice Export Here</a></td></tr>";}
   //if ($expinvoice=="on"){echo "<tr><td><li></td><td><a target='_blank' href='".HTTP_EXPORTS."/invoice$today-$companyid.csv'>Download the Invoice Export Here</a></td></tr>";}
   if ($expapinvoice=="on"){echo "<tr><td><li></td><td><a target='_blank' href='".HTTP_EXPORTS."/payables$today-$companyid.csv'>Download the A/P Accounts Export Here</a></td></tr>";}
   if ($expdeposit=="on"){echo "<tr><td><li></td><td><a target='_blank' href='".HTTP_EXPORTS."/deposit$today-$companyid.csv'>Download the Desposits Here</a></td></tr>";}
   //echo "<tr><td>4.</td><td><a href='".HTTP_EXPORTS."/apinvoice$today-$companyid.csv'>Download the A/P Invoice Export Here</a></td></tr>";
   if ($expinventor=="on"){echo "<tr><td><li></td><td><a target='_blank' href='".HTTP_EXPORTS."/inventory$today-$companyid.csv'>Download the Inventory Export Here</a></td></tr>";}
   if ($expfees=="on"){echo "<tr><td><li></td><td><a target='_blank' href='".HTTP_EXPORTS."/fees$today-$companyid.csv'>Download the Fees Export Here</a></td></tr>";}
   if ($num_accts>0){echo "<tr><td><li></td><td><a target='_blank' href='".HTTP_EXPORTS."/accts$today-$companyid.csv'>Download the New/Updated AR Accounts</a></td></tr>";}
   echo "</table></center><p><br><p><br><p>";
   echo "<center><a href='javascript:self.close()' style=$style>Close window</a></center>";

   mysql_close();
   google_page_track();

}
?>