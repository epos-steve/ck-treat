<?php

function nextday($nextd,$day_format){ //Function returns next day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monn = substr($nextd, 5, 2);
   $dayn = substr($nextd, 8, 2);
   $yearn = substr($nextd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monn, $dayn+1, $yearn));
   return $tempdate;
}

function prevday($prevd,$day_format){ //Function returns previous day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monp = substr($prevd, 5, 2);
   $dayp = substr($prevd, 8, 2);
   $yearp = substr($prevd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monp, $dayp-1, $yearp));
   return $tempdate;
}

function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}

define('DOC_ROOT', dirname(dirname(__FILE__)));
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');
$style = "text-decoration:none";

$user=$_COOKIE["usercook"];
$pass=$_COOKIE["passcook"];
$view=$_COOKIE["viewcook"];
$date1=$_COOKIE["date1cook"];
$date2=$_COOKIE["date2cook"];
$businessid=$_GET["bid"];
$companyid=$_GET["cid"];
$viewdate=$_GET["viewdate"];

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=mysql_numrows($result);

$security_level=mysql_result($result,0,"security_level");
$bid=mysql_result($result,0,"businessid");
$cid=mysql_result($result,0,"companyid");

if ($num != 1 || $user == "" || $pass == "")
{
    echo "<center><h3>Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
    $query7 = "SELECT week_end FROM company WHERE companyid = '$companyid'";
    $result7 = Treat_DB_ProxyOld::query($query7);  

    $week_end=mysql_result($result7,0,"week_end");   

    $showprev=prevday($viewdate);
    while(dayofweek($showprev)!=$week_end&&$lastdate!=1){
       $lastdate=substr($showprev,8,2);
       $showprev=prevday($showprev);
    }
    $shownext=nextday($viewdate);
    while(dayofweek($shownext)!=$week_end&&substr(nextday($shownext),8,2)!=1){
       $shownext=nextday($shownext);
    }

    $show_items=array();
    $item_name=array();

    $query7 = "SELECT * FROM inv_items WHERE businessid = '$businessid'";
    $result7 = Treat_DB_ProxyOld::query($query7);  

    while($r=mysql_fetch_array($result7)){

       $itemid=$r["inv_itemid"];
       $item_name[$itemid]=$r["item_name"];

       $query8 = "SELECT SUM(price*amount) AS totalamt FROM inv_count WHERE inv_itemid = '$itemid' AND date = '$viewdate'";
       $result8 = Treat_DB_ProxyOld::query($query8); 

       $show_items[$itemid]=mysql_result($result8,0,"totalamt");   
    }

    arsort($show_items);

    $total=0;

    $dayname=dayofweek($viewdate);

    echo "<center><table width=100% bgcolor=#E8E7E7 cellspacing=0 cellpadding=0><tr><td>&nbsp;<font size=1>[<a href=printinventory3.php?bid=$businessid&cid=$companyid&viewdate=$showprev style=$style><font color=blue>PREV</font></a>]</font> <b><font size=4>$dayname $viewdate</font></b> <font size=1>[<a href=printinventory3.php?bid=$businessid&cid=$companyid&viewdate=$shownext style=$style><font color=blue>NEXT</font></a>]</font></td><td align=right><a href=businventor2.php?bid=$businessid&cid=$companyid><font color=blue onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='blue' style='text-decoration:none'>Return</font></a>&nbsp;</td></tr></table> </center>";

    echo "<center><table width=100% cellspacing=0 cellpadding=0 style=\"border:1px solid #E8E7E7;\">";
    foreach($show_items AS $key => $value){
       
       if($value!=0){
          $total+=$value;
          $value=number_format($value,2);
          echo "<tr onMouseOver=this.bgColor='yellow' onMouseOut=this.bgColor='white'><td style=\"border:1px solid #E8E7E7;\">&nbsp;$item_name[$key]</td><td align=right style=\"border:1px solid #E8E7E7;\">$value &nbsp;</td></tr>";
       }
    }
    $total=number_format($total,2);
    echo "<tr bgcolor=#E8E7E7><td colspan=2 align=right><b>Total: $total</td></tr>";
    echo "</table><center><p>";

    echo "<form action=businventor2.php?bid=$businessid&cid=$companyid method=post><input type=submit value='Return'></form></center>";

}
//mysql_close();
google_page_track();
?>