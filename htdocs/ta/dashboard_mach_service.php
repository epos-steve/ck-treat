<?php

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+4);
        if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
        elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
        $diff = substr($diff, 0, $dot+3);
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      elseif ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      elseif ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='32')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      $day=$day-31;  
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";} 
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

include("db.php");
$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";


$user=$_COOKIE["usercook"];
$pass=$_COOKIE["passcook"];
$viewtype=$_GET["viewtype"];
$companyid=$_GET["cid"];
$viewbus=$_GET["viewbus"];
$ytdstart=$_GET["ytdstart"];
$mysort=$_GET["sort"];
$view=$_GET["v"]; 

mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = mysql_query($query);
$num=mysql_numrows($result);

if ($num!=0){
    $security_level=mysql_result($result,0,"security_level");
    $bid=mysql_result($result,0,"businessid");
    $cid=mysql_result($result,0,"companyid");
    $loginid=mysql_result($result,0,"loginid");
}

if ($num != 1 || $user == "" || $pass == "") 
{
    echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
    if($viewtype==1&&$security_level>8){
       $compquery="business.companyid != '2'";
    }
    elseif($viewtype==2){
       $compquery="business.companyid = company.companyid AND company.company_type = '$viewbus'";
       $fromtables=",company";
    }
    elseif($viewtype==3){
       $compquery="business.companyid = company.companyid  AND company.companyid = '$viewbus'";
       $fromtables=",company";
    }
    elseif($viewtype==4){
       $compquery="business.districtid = district.districtid AND district.divisionid = '$viewbus'";
       $fromtables=",district";
    }
    elseif($viewtype==5){
       $compquery="business.districtid = '$viewbus'";
    }
    elseif($viewtype==6&&$viewbus>0){
       $compquery="business.businessid = '$viewbus'";
    }

    $today=date("Y-m-d");
    if(substr($today,0,4)!=substr($ytdstart,0,4)){$today=substr($ytdstart,0,4);$today="$today-12-31";}

       if(1==1){
          /////////////COMPLAINTS
          $query1="SELECT vend_machine_complaint.complaintid,vend_machine_complaint.complaint_name,vend_locations.locationid,vend_locations.location_name FROM vend_machine_complaint,vend_locations,business$fromtables WHERE vend_machine_complaint.locationid = vend_locations.locationid AND vend_locations.unitid = business.businessid AND ($compquery)";
          $result1 = mysql_query($query1) or die(mysql_error());
          $num1=mysql_numrows($result1);

          $num1--;
          while($num1>=0){
             $complaintid=mysql_result($result1,$num1,"complaintid");
             $complaint_name=mysql_result($result1,$num1,"complaint_name");
             $locationid=mysql_result($result1,$num1,"locationid");
             $location_name=mysql_result($result1,$num1,"location_name");

             $query2="SELECT COUNT(machineid) AS totalamt FROM vend_machine_service WHERE locationid = '$locationid' AND complaint = '$complaintid' AND date >= '$ytdstart' AND date <= '$today'";
             $result2 = mysql_query($query2) or die(mysql_error());

             $totalamt=mysql_result($result2,0,"totalamt");

             if($totalamt>0){
                $complaint[$complaintid]=$totalamt;
                $complaintname[$complaintid]=$complaint_name;
             }

             $num1--;
          }

          /////////REPAIRS
          $query1="SELECT vend_machine_repair.repairid,vend_machine_repair.repair_name,vend_locations.locationid,vend_locations.location_name FROM vend_machine_repair,vend_locations,business$fromtables WHERE vend_machine_repair.locationid = vend_locations.locationid AND vend_locations.unitid = business.businessid AND ($compquery)";
          $result1 = mysql_query($query1) or die(mysql_error());
          $num1=mysql_numrows($result1);

          $num1--;
          while($num1>=0){
             $repairid=mysql_result($result1,$num1,"repairid");
             $repair_name=mysql_result($result1,$num1,"repair_name");
             $locationid=mysql_result($result1,$num1,"locationid");
             $location_name=mysql_result($result1,$num1,"location_name");

             $query2="SELECT COUNT(machineid) AS totalamt FROM vend_machine_service WHERE locationid = '$locationid' AND repair = '$repairid' AND date >= '$ytdstart' AND date <= '$today'";
             $result2 = mysql_query($query2) or die(mysql_error());

             $totalamt=mysql_result($result2,0,"totalamt");

             if($totalamt>0){
                $repair[$repairid]=$totalamt;
                $repairname[$repairid]=$repair_name;
             }

             $num1--;
          }
       }


       if($mysort==1){arsort($complaint); arsort($repair);}
       else{asort($complaint); asort($repair);}

       echo "<table cellspacing=0 cellpadding=0 width=100%><tr valign=top><td width=50%><table width=100%><tr><td>";
       $counter=1;
       foreach($complaint AS $key => $total){
           if($complaintname[$key]==""){$complaintname[$key]="<i>Unknown</i>";}
           echo "<tr valign=top onMouseOver=this.bgColor='yellow' onMouseOut=this.bgColor='white'><td align=right width=5%><font size=2>$total&nbsp;</td><td width=95%><font size=2>$complaintname[$key]</td></tr>";
           if($counter==10&&$view!="all"){break;}
           $counter++;
       }

       echo "</table></td><td width=50%><table width=100%><tr><td>";

       $counter=1;
       foreach($repair AS $key => $total){
           if($repairname[$key]==""){$repairname[$key]="<i>No Repair</i>";}
           echo "<tr valign=top onMouseOver=this.bgColor='yellow' onMouseOut=this.bgColor='white'><td align=right width=5%><font size=2>$total&nbsp;</td><td width=95%><font size=2>$repairname[$key]</td></tr>";
           if($counter==10&&$view!="all"){break;}
           $counter++;
       }

       echo "</table></td></tr></table>";
}
mysql_close();
?>
