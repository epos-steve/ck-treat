<?php

define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once(DOC_ROOT.'/bootstrap.php');

if ( require_once( realpath( DOC_ROOT . '/../lib/inc/cookie-login.php' ) ) ) {

	$new = filter_input(INPUT_POST, 'new');
	$businessid = filter_input(INPUT_POST, 'businessid');
	
	$businessIds = array( );
	
	if($new==1){
		$item_name = $_POST["item_name"];
		$group_id = $_POST["group_id"];
		$manufacturer = $_POST["manufacturer"];
		$item_code = $_POST["item_code"];
		$upc = $_POST["upc"];
		$active = $_POST["active"];
		$is_button = $_POST["is_button"];
		$max = $_POST["max"];
		$reorder_point = $_POST["reorder_point"];
		$reorder_amount = $_POST["reorder_amount"];
		$new_price = $_POST["new_price"];
		$cost = $_POST["cost"];
		$ordertype = $_POST["ordertype"];
		$add_to_all = intval(@$_POST["add_to_all"]);
		
		$add_to_businesses = array();
		if($add_to_all == 1){
			$sql = "SELECT companyid, categoryid FROM business WHERE businessid = $businessid";
			$result = Treat_DB_ProxyOld::query( $sql );
			$companyid = mysql_result($result,0,"companyid");
			$categoryid = mysql_result($result,0,"categoryid");
			$sql = "SELECT businessid, businessname FROM business WHERE companyid = $companyid AND categoryid = $categoryid";
		}
		else{
			$sql = "SELECT businessid, businessname FROM business WHERE businessid = $businessid";
		}
		
		$result = Treat_DB_ProxyOld::query($sql);
		while($row = mysql_fetch_array($result)){
			$bid = $row["businessid"];
			$add_to_businesses[$bid] = $row["businessname"];
		}

		$sql = "SELECT MAX(pos_id) AS pos_id FROM menu_items_new WHERE businessid = $businessid";
		$result = Treat_DB_ProxyOld::query( $sql );

		$pos_id = mysql_result($result,0,"pos_id");
		$addme = "";
		$messages = array();
		foreach($add_to_businesses as $bid=>$bname)
		{
			$sql = "SELECT id FROM menu_items_new WHERE businessid = $bid AND upc = '$upc'";
			$result = Treat_DB_ProxyOld::query( $sql );
			$num_rows = mysql_num_rows($result);
			
			if($num_rows > 0){
				$messages[] = "Attempting to add item to $bname: <font color='red'>ALREADY EXISTS</font>";
				continue;
			}
			
			$pos_id++;
			
			$gid = ($bid == $businessid ? $group_id : "NULL");

			$uuid = \EE\Model\Menu\ItemNew::genUuid();
			$sql = "INSERT INTO menu_items_new (businessid
												,pos_id
												,group_id
												,manufacturer
												,name
												,ordertype
												,active
												,item_code
												,upc
												,max
												,reorder_point
												,reorder_amount
												,is_button
												,uuid_menu_items_new)
					VALUES
						($bid
						,$pos_id
						,$gid
						,$manufacturer
						,'$item_name'
						,'$ordertype'
						,'$active'
						,'$item_code'
						,'$upc'
						,'$max'
						,'$reorder_point'
						,'$reorder_amount'
						,'$is_button'
						,'$uuid'
						)";
			$result = Treat_DB_ProxyOld::query( $sql );
			$editid = mysql_insert_id();

			if($addme == "")$addme = "&id=$editid";

			$sql = "INSERT INTO menu_items_price (businessid,menu_item_id,pos_price_id,new_price,cost) VALUES ($bid,$editid,1,'$new_price','$cost')";
			$result = Treat_DB_ProxyOld::query( $sql );
			$new_mip_id = mysql_insert_id();
			$mipIds[$editid] = $new_mip_id;
			
			if($new_mip_id == 0){
				$messages[] = "Attempting to add item to $bname: <font color='red'>FAILURE</font>";
			}
			else{
				$messages[] = "Attempting to add item to $bname: <font color='green'>SUCCESS</font>";
				$businessIds[] = $bid;
				$editIds[$bid] = $editid;
			}
		}
		
		echo '<head>';
		echo '<script type="text/javascript" src="/assets/js/jquery-1.4.2.min.js"></script>';
		echo '<script type="text/javascript" src="/assets/js/jquery-ui-1.7.2.custom.min.js"></script>';
		echo '<link type="text/css" rel="stylesheet" href="/assets/css/jq_themes/to-theme/jquery-ui-1.7.2.custom.css" /> ';
		echo '</head>';
		
		echo '<div id="dialog-message" title="Results">';
		foreach($messages as $message){
			echo "<p>$message</p>";
		}
		echo '</div>';
		echo '<script type="text/javascript">';
		echo '$("#dialog-message").dialog({width: 750, modal: true, close: function(){window.location="' . $_SERVER[ 'HTTP_REFERER' ] . $addme  . '"}, buttons: {Ok: function() { $(this).dialog("close"); }}});';
		echo '</script>';
	}
	else{
		$editid = filter_input(INPUT_POST, 'editid');
		
		$sql = "SELECT item_code FROM menu_items_new WHERE id=$editid";
		$result = Treat_DB_ProxyOld::query( $sql );
		$originalItemCode = @mysql_result( $result, 0, 'item_code' );
		
		$sql = sprintf( '
			UPDATE `menu_items_new`
			SET
				`max`             = \'%f\'
				,`reorder_point`  = \'%f\'
				,`reorder_amount` = \'%f\'
							,`name` = \'%s\'
							,`group_id` = \'%d\'
							,`manufacturer` = \'%s\'
							,`pos_processed` = \'%d\'
							,`item_code` = \'%s\'
							,`upc` = \'%s\'
							,`active` = \'%s\'
							,`ordertype` = \'%d\'
							,`is_button` = \'%d\'
			WHERE
				`id` = \'%d\'
			'
			,filter_input( INPUT_POST, 'max' )
			,filter_input( INPUT_POST, 'reorder_point' )
			,filter_input( INPUT_POST, 'reorder_amount' )
			,addslashes(filter_input( INPUT_POST, 'item_name' ))
			,filter_input( INPUT_POST, 'group_id' )
			,filter_input( INPUT_POST, 'manufacturer')
			,'0'
			,filter_input( INPUT_POST, 'item_code' )
			,filter_input( INPUT_POST, 'upc' )
			,filter_input( INPUT_POST, 'active' )
			,filter_input( INPUT_POST, 'ordertype' )
			,filter_input( INPUT_POST, 'is_button' )
			,$editid
		);
		$result = Treat_DB_ProxyOld::query( $sql );
		$editIds[$businessid] = $editid;
		
		$sql = "SELECT companyid, categoryid FROM business WHERE businessid = $businessid";
		$result = Treat_DB_ProxyOld::query( $sql );
		$companyid = mysql_result($result,0,"companyid");
		$categoryid = mysql_result($result,0,"categoryid");
		$sql = "SELECT businessid, businessname FROM business WHERE companyid = $companyid AND categoryid = $categoryid";
		$result = Treat_DB_ProxyOld::query( $sql );
		
		while( $r = mysql_fetch_assoc( $result ) ) {
			$businessIds[] = $r['businessid'];
		}
		$businessList = implode( ',', $businessIds );
		
		$sql = "
			SELECT 
				id,
				businessid
			FROM menu_items_new 
			WHERE item_code = '".$originalItemCode."'
			AND businessid IN ( $businessList )
		";
		$result = Treat_DB_ProxyOld::query( $sql );
		
		while( $r = mysql_fetch_assoc( $result ) ) {
			if( $r['id'] == $editid )
				continue;
			
			$editIds[$r['businessid']] = $r['id'];
		}
		
		$sql = "
			UPDATE menu_items_new SET
				name = '".addslashes(filter_input( INPUT_POST, 'item_name' ))."',
				item_code = '".filter_input( INPUT_POST, 'item_code' )."',
				upc = '".filter_input( INPUT_POST, 'upc' )."',
				ordertype = '".filter_input( INPUT_POST, 'ordertype' )."'
			WHERE item_code = '$originalItemCode'
			AND businessid IN ( $businessList )
		";
		$result = Treat_DB_ProxyOld::query( $sql );
		
		header( 'Location: ' . $_SERVER[ 'HTTP_REFERER' ] . "$addme" );
	}

	// Holy hell I hope this works... it will insert a row into the new
	// kiosk_sync table for each kiosk that will be affected by this change.
	//Treat_Model_KioskSync::addSync( $businessid, $editid, Treat_Model_KioskSync::TYPE_ITEM);
	
	foreach( $editIds as $bId => $editId ) {
		Treat_Model_KioskSync::addSync( $bId, $editId, Treat_Model_KioskSync::TYPE_ITEM );
	}
	
	if ( isset( $_POST['new_price'] ) || isset( $_POST['cost'] ) ) {
		// if they set the price & then changed their mind they need
		// to be able to set the value to null
		if ( '' === trim( $_POST['new_price'] ) ) {
			$new_price = 'NULL';
		}
		else {
			$new_price	= sprintf( '\'%f\'', filter_input( INPUT_POST, 'new_price' ) );
			$cost		= sprintf( '\'%f\'', filter_input( INPUT_POST, 'cost' ) );
		}
		
		foreach( $editIds as $mId ) {
			if( $mId == $editid ) {
				$mip_id = filter_input( INPUT_POST, 'mip_id' );
			} else {
				$sql = "SELECT id FROM menu_items_price WHERE menu_item_id = $mId";
				$result = Treat_DB_ProxyOld::query( $sql );
				$mip_id = @mysql_result( $result, 0, 'id' );
			}
			
			if($mip_id < 1){$mip_id = $mipIds[$mId];}
			// if $mip_id isn't a number or it's 0 there's no sense in hitting the database to do nothing.
			if ( (string)intval( $mip_id ) == (string)$mip_id && $mip_id !== 0 ) {
				$newPrice = $mId == $editid ? ", `new_price` = $new_price" : '';
				
				$sql = sprintf( '
					UPDATE `menu_items_price`
					SET
						`cost` = %s
						%s
					WHERE
						`id` = \'%d\'
					'
					,$cost
					,$newPrice
					,$mip_id
				);
				$result = Treat_DB_ProxyOld::query( $sql );
			}
		}
	}

	////taxes
	$businessid = $_POST["businessid"];
	if($editid<1){$editid = $_POST["editid"];}
	
	$query = "SELECT * FROM menu_tax WHERE businessid = $businessid ORDER BY pos_id";
	$result = Treat_DB_ProxyOld::query( $query );

	while($r = mysql_fetch_array($result)){
		$tax_id = $r["id"];

		$has_tax = $_POST["tax$tax_id"];

		if($has_tax > 0){
			$uuid = \EE\Model\Menu\ItemNew::genUuid();
			$query2 = "INSERT INTO menu_item_tax (menu_itemid,tax_id, uuid_menu_item_tax) VALUES ($editid,$tax_id, '$uuid')";
			$result2 = Treat_DB_ProxyOld::query( $query2 );
			// Add to the sync table for the item tax
			Treat_Model_KioskSync::addSync($businessid, $editid, Treat_Model_KioskSync::TYPE_ITEM);
			Treat_Model_KioskSync::addSync($businessid, $editid, Treat_Model_KioskSync::TYPE_ITEM_TAX);
		}
		else{
			$query2 = "DELETE FROM menu_item_tax WHERE menu_itemid = $editid and tax_id = $tax_id";
			$result2 = Treat_DB_ProxyOld::query( $query2 );
			Treat_Model_KioskSync::addSync($businessid, $editid, Treat_Model_KioskSync::TYPE_ITEM_TAX);
		}

	}
}
