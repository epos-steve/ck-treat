<?php
setlocale( LC_ALL, 'en_US.utf8' );


function isLeapYear($year)
{
	/*
	# Check for valid parameters #
	if (!is_int($year) || $year < 0)
	{
		printf('Wrong parameter for $year in function isLeapYear. It must be a positive integer.');
		exit();
	}
	*/

	# In the Gregorian calendar there is a leap year every year divisible by four
	# except for years which are both divisible by 100 and not divisible by 400.

	if ($year % 4 != 0)
	{
		return 28;
	}
	else
	{
		if ($year % 100 != 0)
		{
			return 29;    # Leap year
		}
		else
		{
			if ($year % 400 != 0)
			{
				return 28;
			}
			else
			{
				return 29;    # Leap year
			}
		}
	}
}

function dayofweek($date1)
{
	$day=substr($date1,8,2);
	$month=substr($date1,5,2);
	$year=substr($date1,0,4);
	$dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
	return $dayofweek;
}

function nextday($date2)
{
	$day=substr($date2,8,2);
	$month=substr($date2,5,2);
	$year=substr($date2,0,4);
	$leap = date("L");
	
	if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
	{
		if ($month == "01"){$month="02";}
		elseif ($month == "03"){$month="04";}
		elseif ($month == "05"){$month="06";}
		elseif ($month == "07"){$month="08";}
		elseif ($month == "08"){$month="09";}
		elseif ($month == "10"){$month="11";}
		$day='01';
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($month=='02' && $day == '29' && $leap == '1')
	{
		$month='03';
		$day='01';
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($month=='02' && $day == '28' && $leap == '0')
	{
		$month='03';
		$day='01';
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
	{
		if ($month == "04"){$month="05";}
		if ($month == "06"){$month="07";}
		if ($month == "09"){$month="10";}
		if ($month == "11"){$month="12";}
		$day='01';
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($month=='12' && $day=='31')
	{
		$day='01';
		$month='01';
		$year++;
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else
	{
		$day=$day+1;
		if ($day<10){$day="0$day";}
		$tomorrow="$year-$month-$day";
		return $tomorrow;
	}
}

function prevday($date2) {
	$day=substr($date2,8,2);
	$month=substr($date2,5,2);
	$year=substr($date2,0,4);
	$day=$day-1;
	$leap = date("L");

	if ($day <= 0)
	{
		if ($month == 01)
		{
			$month = '12';
			$year--;
			$day=$day+31;
			$yesterday = "$year-$month-$day";
			return $yesterday;
		}
		else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
		{
			$month--;
			if ($month < 10)
			{
				$month="0$month";
			}
			$day=$day+31;
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
		else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
		{
			$month--;
			if ($month < 10)
			{
				$month="0$month";
			}
			$day=$day+30;
			$yesterday="$year-$month-$day";
			return $yesterday;
		}

		elseif ($leap==1&&$month=='03')
		{
			$day=$day+29;
			$month='02';
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
		else
		{
			$day=$day+28;
			$month='02';
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
	}
	else
	{
		if ($day < 10)
		{
			$day="0$day";
		}
		$yesterday="$year-$month-$day";
		return $yesterday;
	}
}

function money($diff){
	$findme='.';
	$double='.00';
	$single='0';
	$double2='00';
	$pos1 = strpos($diff, $findme);
	$pos2 = strlen($diff);
	if ($pos1==""){$diff="$diff$double";}
	elseif ($pos2-$pos1==2){$diff="$diff$single";}
	elseif ($pos2-$pos1==1){$diff="$diff$double2";}
	else{}
	$dot = strpos($diff, $findme);
	$diff = substr($diff, 0, $dot+3);
	if ($diff > 0 && $diff < '0.01'){$diff="0.01";}
	elseif($diff < 0 && $diff > '-0.01'){$diff="-0.01";}
	return $diff;
}

function futureday($num) {
	$day = date("d");
	$year = date("Y");
	$month = date("m");
	$leap = date("L");
	$day=$day+$num;
	
	if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
	{
		if ($month == "03"){$month="04";}
		if ($month == "05"){$month="06";}
		if ($month == "07"){$month="08";}
		if ($month == "08"){$month="09";}
		$day=$day-31;
		if ($day<10){$day="0$day";}
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($month=='02' && $leap == '1' && $day >= '29')
	{
		$month='03';
		$day=$day-29;
		if ($day<10){$day="0$day";}
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($month=='02' && $leap == '0' && $day >= '28')
	{
		$month='03';
		$day=$day-28;
		if ($day<10){$day="0$day";}
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
	{
		if ($month == "04"){$month="05";}
		if ($month == "06"){$month="07";}
		if ($month == "09"){$month="10";}
		if ($month == "11"){$month="12";}
		$day=$day-30;
		if ($day<10){$day="0$day";}
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($month==12 && $day>=31)
	{
		$day=$day-31;
		if ($day<10){$day="0$day";}
		$month='01';
		$year++;
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else
	{
		if ($day<10){$day="0$day";}
		$tomorrow="$year-$month-$day";
		return $tomorrow;
	}
}

function pastday($num) {
	$day = date("d");
	$day=$day-$num;
	if ($day <= 0)
	{
		$year = date("Y");
		$month = date("m");
		if ($month == 01)
		{
			$month = '12';
			$year--;
			$day=$day+31;
			$yesterday = "$year-$month-$day";
			return $yesterday;
		}
		else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
		{
			$month--;
			if ($month < 10)
			{
				$month="0$month";
			}
			$day=$day+31;
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
		else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
		{
			$month--;
			if ($month < 10)
			{
				$month="0$month";
			}
			$day=$day+30;
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
		else
		{
			$day=$day+28;
			$month='02';
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
	}
	else
	{
		if ($day < 10)
		{
			$day="0$day";
		}
		$year=date("Y");
		$month=date("m");
		$yesterday="$year-$month-$day";
		return $yesterday;
	}
}



if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
//include_once( 'db.php' );
require_once( DOC_ROOT . '/bootstrap.php' );


$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$prodtoday=$today;
$todayname = date("l");
$creditamount="creditamount";
$creditidnty="creditidnty";
$creditdate="creditdate";
$debitamount="debitamount";
$debitidnty="debitidnty";
$debitdate="debitdate";
$quatertotal=0;
$monthtotal=0;
$total=0;
$counter=0;
$findme='.';
$double='.00';
$single='0';
$double2='00';

/*$caterbus = $_COOKIE['caterbus'];
$number=$_GET['number'];
$newmonth=$_GET['newmonth'];
$newyear=$_GET['newyear'];
$newstart=$_GET['newstart'];
$busid=$_GET['bid'];*/

$caterbus = \EE\Controller\Base::getSessionCookieVariable('caterbus');
$number = \EE\Controller\Base::getGetVariable('number');
$newmonth = \EE\Controller\Base::getGetVariable('newmonth');
$newyear = \EE\Controller\Base::getGetVariable('newyear');
$newstart = \EE\Controller\Base::getGetVariable('newstart');
$busid = \EE\Controller\Base::getGetVariable('bid');

$today = date("l");
$todaynum = date("j");
$todaynum2 = date("j");
$year = date("Y");
$monthnum = date("n");
$day=1;
$high = 6;
$long = 1;
$leap = date("L");
$back = "white";
$count = 0;
$style = "text-decoration:none";
$brief ="";
$you = "";

if ($newstart == "")
{
	while ( $todaynum > 7 ) {
		$todaynum = $todaynum - 7;
		$count++;
	}
	if ($today == 'Sunday' && $todaynum == 1){$start=1;}
	if ($today == 'Monday' && $todaynum == 1){$start=2;}
	if ($today == 'Tuesday' && $todaynum == 1){$start=3;}
	if ($today == 'Wednesday' && $todaynum == 1){$start=4;}
	if ($today == 'Thursday' && $todaynum == 1){$start=5;}
	if ($today == 'Friday' && $todaynum == 1){$start=6;}
	if ($today == 'Saturday' && $todaynum == 1){$start=7;}
	if ($today == 'Sunday' && $todaynum == 2){$start=7;}
	if ($today == 'Monday' && $todaynum == 2){$start=1;}
	if ($today == 'Tuesday' && $todaynum == 2){$start=2;}
	if ($today == 'Wednesday' && $todaynum == 2){$start=3;}
	if ($today == 'Thursday' && $todaynum == 2){$start=4;}
	if ($today == 'Friday' && $todaynum == 2){$start=5;}
	if ($today == 'Saturday' && $todaynum == 2){$start=6;}
	if ($today == 'Sunday' && $todaynum == 3){$start=6;}
	if ($today == 'Monday' && $todaynum == 3){$start=7;}
	if ($today == 'Tuesday' && $todaynum == 3){$start=1;}
	if ($today == 'Wednesday' && $todaynum == 3){$start=2;}
	if ($today == 'Thursday' && $todaynum == 3){$start=3;}
	if ($today == 'Friday' && $todaynum == 3){$start=4;}
	if ($today == 'Saturday' && $todaynum == 3){$start=5;}
	if ($today == 'Sunday' && $todaynum == 4){$start=5;}
	if ($today == 'Monday' && $todaynum == 4){$start=6;}
	if ($today == 'Tuesday' && $todaynum == 4){$start=7;}
	if ($today == 'Wednesday' && $todaynum == 4){$start=1;}
	if ($today == 'Thursday' && $todaynum == 4){$start=2;}
	if ($today == 'Friday' && $todaynum == 4){$start=3;}
	if ($today == 'Saturday' && $todaynum == 4){$start=4;}
	if ($today == 'Sunday' && $todaynum == 5){$start=4;}
	if ($today == 'Monday' && $todaynum == 5){$start=5;}
	if ($today == 'Tuesday' && $todaynum == 5){$start=6;}
	if ($today == 'Wednesday' && $todaynum == 5){$start=7;}
	if ($today == 'Thursday' && $todaynum == 5){$start=1;}
	if ($today == 'Friday' && $todaynum == 5){$start=2;}
	if ($today == 'Saturday' && $todaynum == 5){$start=3;}
	if ($today == 'Sunday' && $todaynum == 6){$start=3;}
	if ($today == 'Monday' && $todaynum == 6){$start=4;}
	if ($today == 'Tuesday' && $todaynum == 6){$start=5;}
	if ($today == 'Wednesday' && $todaynum == 6){$start=6;}
	if ($today == 'Thursday' && $todaynum == 6){$start=7;}
	if ($today == 'Friday' && $todaynum == 6){$start=1;}
	if ($today == 'Saturday' && $todaynum == 6){$start=2;}
	if ($today == 'Sunday' && $todaynum == 7){$start=2;}
	if ($today == 'Monday' && $todaynum == 7){$start=3;}
	if ($today == 'Tuesday' && $todaynum == 7){$start=4;}
	if ($today == 'Wednesday' && $todaynum == 7){$start=5;}
	if ($today == 'Thursday' && $todaynum == 7){$start=6;}
	if ($today == 'Friday' && $todaynum == 7){$start=7;}
	if ($today == 'Saturday' && $todaynum == 7){$start=1;}
}
elseif ( $newstart > 7 )
{
	$start = $newstart - 7;
}
else
{
	$start = $newstart;
}

if ( $newmonth != '' ) {
}
else {
	$newmonth = $monthnum;
}
if ( $newyear != '' ) {
	$year = $newyear;
}
else {
	$newyear = $year;
}

/*$user = $_COOKIE['usercook'];
$pass = $_COOKIE['passcook'];
$businessid=$_GET["bid"];
$companyid=$_GET["cid"];
$prevperiod=$_GET["prevperiod"];
$nextperiod=$_GET["nextperiod"];
$curmenu = $_COOKIE['curmenu'];
$curcust = $_COOKIE['curcust'];
$curcust_num = $_COOKIE['curcust_num'];*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook');
$businessid = \EE\Controller\Base::getGetVariable('bid');
$companyid = \EE\Controller\Base::getGetVariable('cid');
$prevperiod = \EE\Controller\Base::getGetVariable('prevperiod');
$nextperiod = \EE\Controller\Base::getGetVariable('nextperiod');
$curmenu = \EE\Controller\Base::getSessionCookieVariable('curmenu');
$curcust = \EE\Controller\Base::getSessionCookieVariable('curcust');
$curcust_num = \EE\Controller\Base::getSessionCookieVariable('curcust_num');

#mysql_connect($dbhost,$username,$password);
#@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query( $query );
$num = Treat_DB_ProxyOld::mysql_numrows( $result );
//mysql_close();

$security_level = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'security_level' );
$mysecurity = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'security' );
$bid = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'businessid' );
$cid = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'companyid' );
$loginid = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'loginid' );

if ($security_level<3){
	$order_only = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'order_only' );
	$oo2 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'oo2' );
	$oo3 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'oo3' );
	$oo4 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'oo4' );
	$oo5 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'oo5' );
	$oo6 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'oo6' );
	$oo7 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'oo7' );
	$oo8 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'oo8' );
	$oo9 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'oo9' );
	$oo10 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'oo10' );
	$busid1 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'businessid' );
	$busid2 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid2' );
	$busid3 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid3' );
	$busid4 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid4' );
	$busid5 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid5' );
	$busid6 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid6' );
	$busid7 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid7' );
	$busid8 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid8' );
	$busid9 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid9' );
	$busid10 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid10' );
	$bid2 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid2' );
	$bid3 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid3' );
	$bid4 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid4' );
	$bid5 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid5' );
	$bid6 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid6' );
	$bid7 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid7' );
	$bid8 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid8' );
	$bid9 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid9' );
	$bid10 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid10' );
	
	$pr1 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'payroll' );
	$pr2 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr2' );
	$pr3 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr3' );
	$pr4 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr4' );
	$pr5 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr5' );
	$pr6 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr6' );
	$pr7 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr7' );
	$pr8 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr8' );
	$pr9 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr9' );
	$pr10 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr10' );
}

if (
	$num != 1
	|| (
		$security_level==1
		&& ( $bid != $businessid || $cid != $companyid )
	)
	|| ( $security_level > 1 && $cid != $companyid )
	|| ( $user == '' && $pass == '' )
) {
	echo '<center><h3>Login Failed</h3>Use your browser\'s back button to try again.</center>';
}

else
{
	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( 'Unable to select database');
	$query = "SELECT * FROM company WHERE companyid = '$companyid'";
	$result = Treat_DB_ProxyOld::query( $query );
	//mysql_close();

	$companyname = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'companyname' );

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( 'Unable to select database');
	$query = "SELECT * FROM security WHERE securityid = '$mysecurity'";
	$result = Treat_DB_ProxyOld::query( $query );
	//mysql_close();

	$sec_bus_sales = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_sales' );
	$sec_bus_invoice = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_invoice' );
	$sec_bus_order = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_order' );
	$sec_bus_payable = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_payable' );
	$sec_bus_inventor1 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_inventor1' );
	$sec_bus_control = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_control' );
	$sec_bus_menu = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_menu' );
	$sec_bus_order_setup = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_order_setup' );
	$sec_bus_request = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_request' );
	$sec_route_collection = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'route_collection' );
	$sec_route_labor = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'route_labor' );
	$sec_route_detail = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'route_detail' );
	$sec_bus_labor = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_labor' );
	$sec_bus_budget = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_budget' );

	if ( $curcust != '' && $curcust != 0 ) {
		$editcust = "<a href=/ta/busedit_account.php?cid=$companyid&bid=$businessid&aid=$curcust><img src=/ta/edit.gif width=16 height=16 border=0 alt='Edit Account Info'></a>";
	}
	else {
		$editcust = '';
	}

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( 'Unable to select database');
	$query = "SELECT * FROM business WHERE companyid = '$companyid' AND businessid = '$businessid'";
	$result = Treat_DB_ProxyOld::query( $query );
	//mysql_close();

	$businessname = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'businessname' );
	$businessid = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'businessid' );
	$street = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'street' );
	$city = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'city' );
	$state = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'state' );
	$zip = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'zip' );
	$phone = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'phone' );
	$contract = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'contract' );
	$default_menu = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'default_menu' );
	$bus_unit = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'unit' );
	if ( $curmenu == '' ) {
		$curmenu = $default_menu;
	}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
	<head>
		<title>Essential Elements :: Treat America :: busordertrack.php </title>
		<link rel="stylesheet" type="text/css" href="/assets/css/style.css" />
		<link rel="stylesheet" type="text/css" href="/assets/css/ta/generic.css" />
		<script type="text/javascript" src="/assets/js/dynamicdrive-disableform.js"></script>
		<script type="text/javascript" src="/assets/js/calendarPopup.js"></script>
		<script type="text/javascript">document.write(getCalendarStyles());</script>
		<link rel="stylesheet" type="text/css" href="/assets/css/calendarPopup.css" />
		<script type="text/javascript" src="/assets/js/changePage.js"></script>
	</head>

	<body>
		<div id="page_wrapper" class="page_wrapper">
			<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
					<td colspan="2">
						<div>
							<a href="/ta/businesstrack.php"
								><img src="/ta/logo.jpg" border="0" height="43" width="205"
							/></a>
							<p>
						</div>
					</td>
				</tr>
<?php

	//echo '<tr bgcolor=black><td colspan=3 height=1></td></tr>';
?>
				<tr bgcolor="#ccccff">
					<td width="1%"
						><img src="/ta/weblogo.jpg" width="80" height="75"
					/></td>
					<td>
						<font size="4">
							<b><?php echo $businessname; ?> #<?php echo $bus_unit; ?></b>
						</font>
						<br/>
						<?php echo $street; ?>
						<br/>
						<?php echo $city, ', ', $state, ' ', $zip; ?>
						<br/>
						<?php echo $phone; ?>
					</td>
					<td align="right" valign="top">
						<br/>
						<form action="/ta/editbus.php" method="post">
							<input type="hidden" name="username" value="<?php echo $user; ?>" />
							<input type="hidden" name="password" value="<?php echo $pass; ?>" />
							<input type="hidden" name="businessid" value="<?php echo $businessid; ?>" />
							<input type="hidden" name="companyid" value="<?php echo $companyid; ?>" />
							<input type="submit" value=" Store Setup " />
						</form>
					</td>
				</tr>
<?php
	//echo '<tr bgcolor=black><td colspan=3 height=1></td></tr>';
	
?>
				<tr>
					<td colspan="3">
						<font color="#999999">
<?php
	if ( $sec_bus_sales > 0 ) {
		echo "<a href=busdetail.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Sales</font></a>";
	}
	if ( $sec_bus_invoice > 0 ) {
		echo " | <a href=businvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Invoicing</font></a>";
	}
	if ( $sec_bus_order > 0 ) {
		echo " | <a href=buscatertrack.php?bid=$businessid&cid=$companyid><font color=red onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='red' style='text-decoration:none'>Orders</font></a>";
	}
	if ( $sec_bus_payable > 0 ) {
		echo " | <a href=busapinvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Payables</font></a>";
	}
	if ( $sec_bus_inventor1 > 0 ) {
		echo " | <a href=businventor.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Inventory</font></a>";
	}
	if ( $sec_bus_control > 0 ) {
		echo " | <a href=buscontrol.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Control Sheet</font></a>";
	}
	if ( $sec_bus_menu > 0 ) {
		echo " | <a href=buscatermenu.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Menus</font></a>";
	}
	if ( $sec_bus_order_setup > 0 ) {
		echo " | <a href=buscaterroom.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Order Setup</font></a>";
	}
	if ( $sec_bus_request > 0 ) {
		echo " | <a href=busrequest.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Requests</font></a>";
	}
	if ( $sec_route_collection > 0 || $sec_route_labor > 0 || $sec_route_detail > 0 ) {
		echo " | <a href=busroute_collect.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Route Collections</font></a>";
	}
	if ( $sec_bus_labor > 0 ) {
		echo " | <a href=buslabor.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Labor</font></a>";
	}
	if (
		$sec_bus_budget > 0
		|| ( $businessid == $bid && $pr1 == 1 )
		|| ( $businessid == $bid2 && $pr2 == 1 )
		|| ( $businessid == $bid3 && $pr3 == 1 )
		|| ( $businessid == $bid4 && $pr4 == 1 )
		|| ( $businessid == $bid5 && $pr5 == 1 )
		|| ( $businessid == $bid6 && $pr6 == 1 )
		|| ( $businessid == $bid7 && $pr7 == 1 )
		|| ( $businessid == $bid8 && $pr8 == 1 )
		|| ( $businessid == $bid9 && $pr9 == 1 )
		|| ( $businessid == $bid10 && $pr10 == 1 )
	) {
		echo " | <a href=busbudget.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Budget</font></a>";
	}
?>

						</font>
					</td>
				</tr>
			</table>
			<p>

<?php
	////////////////////////////////////////////////
	if ( $security_level > 0 ) {
?>
			<table width="100%">
				<tr>
					<td width="50%">
						<form action="/ta/businvoice.php" method="post">
							<select name="gobus" onChange="changePage(this.form.gobus)">
<?php

		$districtquery='';
		if ( $security_level > 2 && $security_level < 7 ) {
			////mysql_connect($dbhost,$username,$password);
			////@mysql_select_db($database) or die( 'Unable to select database');
			$query = "SELECT * FROM login_district WHERE loginid = '$loginid'";
			$result = Treat_DB_ProxyOld::query( $query );
			$num = Treat_DB_ProxyOld::mysql_numrows( $result );
			////mysql_close();

			$num--;
			while ( $num >= 0 ) {
				$districtid = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'districtid' );
				if ( $districtquery == '' ) {
					$districtquery = "districtid = '$districtid'";
				}
				else {
					$districtquery = "$districtquery OR districtid = '$districtid'";
				}
				$num--;
			}
		}

		////mysql_connect($dbhost,$username,$password);
		////@mysql_select_db($database) or die( 'Unable to select database');
		if ( $security_level > 6 ) {
			$query = "SELECT * FROM business WHERE companyid = '$companyid' ORDER BY businessname DESC";
		}
		elseif ( $security_level == 2 || $security_level == 1 ) {
			$query = "SELECT * FROM business WHERE companyid = '$companyid' AND (businessid = '$bid' OR businessid = '$bid2' OR businessid = '$bid3' OR businessid = '$bid4' OR businessid = '$bid5' OR businessid = '$bid6' OR businessid = '$bid7' OR businessid = '$bid8' OR businessid = '$bid9' OR businessid = '$bid10') ORDER BY businessname DESC";
		}
		elseif ( $security_level > 2 && $security_level < 7 ) {
			$query = "SELECT * FROM business WHERE companyid = '$companyid' AND ($districtquery) ORDER BY businessname DESC";
		}
		$result = Treat_DB_ProxyOld::query( $query );
		$num = Treat_DB_ProxyOld::mysql_numrows( $result );
		////mysql_close();

		$num--;
		while ( $num >= 0 ) {
			$businessname = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'businessname' );
			$busid = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'businessid' );

			if ( $busid == $businessid ) {
				$show = ' selected="selected"';
			}
			else {
				$show = '';
			}
?>

								<option value="/ta/buscatertrack.php?bid=<?php
									echo $busid;
								?>&amp;cid=<?php
									echo $companyid;
								?>" <?php
									echo $show;
								?>><?php
									echo $businessname;
								?></option>
<?php
			$num--;
		}

?>
							</select>
						</form>
					</td>
					<td></td>
					<td width="50%" align="right"></td>
				</tr>
			</table>
			<p>
			<div>&nbsp;</div>
<?php
	}
	////////////////////////////////////////////////

	$bid = $caterbus;
	$cid = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'companyid' );
	$security_level = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'security_level' );

	if ( $newmonth == 1 ) {
		$month = 'January';
	}
	elseif ( $newmonth == 2 ) {
		$month = 'February';
	}
	elseif ( $newmonth == 3 ) {
		$month = 'March';
	}
	elseif ( $newmonth == 4 ) {
		$month = 'April';
	}
	elseif ( $newmonth == 5 ) {
		$month = 'May';
	}
	elseif ( $newmonth == 6 ) {
		$month = 'June';
	}
	elseif ( $newmonth == 7 ) {
		$month = 'July';
	}
	elseif ( $newmonth == 8 ) {
		$month = 'August';
	}
	elseif ( $newmonth == 9 ) {
		$month = 'September';
	}
	elseif ( $newmonth == 10 ) {
		$month = 'October';
	}
	elseif ( $newmonth == 11 ) {
		$month = 'November';
	}
	elseif ( $newmonth == 12 ) {
		$month = 'December';
	}

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( 'Unable to select database');
	$query33 = "SELECT * FROM accounts WHERE businessid = '$businessid' AND is_deleted = '0' ORDER BY status DESC,name DESC";
	$result33 = Treat_DB_ProxyOld::query( $query33 );
	$num33 = Treat_DB_ProxyOld::mysql_numrows( $result33 );
	//mysql_close();

	$laststatus = -1;

?>
			<table cellspacing="0" cellpadding="0" border="0" width="100%" style="border:2px solid #ccc;">
				<tr bgcolor="#e8e7e7">
					<td colspan="2">
						<center>
							<div>&nbsp;</div>
							<form action="/ta/setcustomer.php" method="post">
								<br/>
<?php
// TODO see if this form can be replaced with a get action against this page
/*
 *	based on the $location url generated below (from setcustomer.php) it would
 *	appear that we should be able to, but we need to check & see what setcustomer.php
 *	is doing with the database.
 *
		$location="/ta/busordertrack.php?
			date=$date
			newmonth=$newmonth
			newyear=$newyear
			newstart=$newstart
			bid=$businessid
			cid=$companyid
 */
?>
								<h3 style="display: inline;"> #<?php echo $curcust_num, ' ', $editcust; ?> </h3>
								<input type="hidden" name="username" value="<?php echo $user; ?>" />
								<input type="hidden" name="password" value="<?php echo $pass; ?>" />
								<input type="hidden" name="newstart" value="<?php echo $newstart; ?>" />
								<input type="hidden" name="newmonth" value="<?php echo $newmonth; ?>" />
								<input type="hidden" name="newyear" value="<?php echo $newyear; ?>" />
								<input type="hidden" name="bid" value="<?php echo $businessid; ?>" />
								<input type="hidden" name="cid" value="<?php echo $companyid; ?>" />
								<select name="customerid">
									<option value="-1">=========Please Choose=========</option>
<?php
	$num33--;
	while ( $num33 >= 0 ) {
		$accountid = @Treat_DB_ProxyOld::mysql_result( $result33, $num33, 'accountid' );
		$accountname = @Treat_DB_ProxyOld::mysql_result( $result33, $num33, 'name' );
		$acct_status = @Treat_DB_ProxyOld::mysql_result( $result33, $num33, 'status' );
		if ( $curcust == $accountid ) {
			$showcust = ' selected="selected"';
		}
		else {
			$showcust = '';
		}
		if ( $laststatus != $acct_status && $acct_status == 0 ) {
			echo '<option value="-1">==========================Active</option>';
		}
		elseif ( $laststatus != $acct_status && $acct_status == 1 ) {
			echo '<option value="-1">==========================Inactive</option>';
		}
		elseif ( $laststatus != $acct_status && $acct_status == 2 ) {
			echo '<option value="-1">==========================Suspended</option>';
		}
		elseif ( $laststatus != $acct_status && $acct_status == 3 ) {
			echo '<option value="-1">==========================COD</option>';
		}
		echo "<option value=$accountid $showcust>$accountname</option>";
		$num33--;
		$laststatus = $acct_status;
	}

?>

								</select>
								<input type="text" size="6" name="cust_num" />
								<input type="submit" value="GO">
								<h3 style="display: inline;"> - <?php echo $month, ' ', $year; ?></h3>
							</form>
							<p>
						</center>
					</td>
				</tr>
				<tr bgcolor="#e8e7e7">
					<td colspan="2">
<?php

	$leap = isLeapYear( $year );
	if ( $leap == 29 ) {
		$leap = 1;
	}
	else {
		$leap = 0;
	}

	if ( $curcust != '' ) {
?>

						<div style="text-align: center;">
							<table border="1" cellpadding="0" cellspacing="0" style="margin: 0 auto;">
								<tr bgcolor="#ffff99">
									<td style="text-align: center;">
										Sunday
									</td>
									<td style="text-align: center;">
										Monday
									</td>
									<td style="text-align: center;">
										Tuesday
									</td>
									<td style="text-align: center;">
										Wednesday
									</td>
									<td style="text-align: center;">
										Thursday
									</td>
									<td style="text-align: center;">
										Friday
									</td>
									<td style="text-align: center;">
										Saturday
									</td>
								</tr>
								<tr height="110">
<?php
		$weekend = 'F';
		while ( $long < 8 ) {
			if ( $long >= $start ) {
				if ( $long == 1 || $long == 7 ) {
					$weekend = 'T';
				}

				if ( $day < 10 && $newmonth < 10 ) {
					$date = "$newyear-0$newmonth-0$day";
				}
				elseif ( $day < 10 && $newmonth > 10 ) {
					$date = "$newyear-$newmonth-0$day";
				}
				elseif ( $day > 10 && $newmonth < 10 ) {
					$date = "$newyear-0$newmonth-$day";
				}
				else {
					$date = "$newyear-$newmonth-$day";
				}

				if ( $newstart == '' ) {
					$newstart = $start;
				}
				$correcturl = "orderdatecust.php?date=$date&newmonth=$newmonth&newyear=$newyear&newstart=$newstart&day=$day&weekend=$weekend&bid=$businessid&cid=$companyid";

				if ( $day == $todaynum2 && $monthnum == $newmonth && $year == $newyear ) {
					$color = 'red';
				}
				else {
					$color = 'black';
				}

?>

								<a style="<?php echo $style; ?>" href="<?php echo $correcturl; ?>">
									<td
										bgcolor="<?php echo $back; ?>"
										width="120" valign="top"
										onMouseOver="this.bgColor='#999999'"
										onMouseOut="this.bgColor='<?php echo $back; ?>'"
									>
										<a style="<?php echo $style; ?>" href="<?php echo $correcturl; ?>">
											<b>
												<font color="<?php
													echo $color;
												?>"><?php
													echo $day;
												?></font>
											</b>
										</a>
										<br/>
<?php

				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( 'Unable to select database');
				$query14 = "SELECT * FROM caterclose WHERE companyid = '$companyid' AND businessid = '$businessid' AND date = '$date'";
				$result14 = Treat_DB_ProxyOld::query( $query14 );
				$num14 = Treat_DB_ProxyOld::mysql_numrows( $result14 );
				//mysql_close();

				if ( $num14 == 0 ) {
	/////////////////////////////////////////////////////////////

					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( 'Unable to select database');
					$query = "SELECT * FROM menu_daypart WHERE menu_typeid = '$curmenu' ORDER BY orderid DESC";
					$result = Treat_DB_ProxyOld::query( $query );
					$num = Treat_DB_ProxyOld::mysql_numrows( $result );
					//mysql_close();

					$num--;
					while ( $num >= 0 ) {
						$menu_daypartid = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'menu_daypartid' );
						$menu_daypartname = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'menu_daypartname' );

						//mysql_connect($dbhost,$username,$password);
						//@mysql_select_db($database) or die( 'Unable to select database');
						$query10 = "SELECT * FROM menu_daypartdetail WHERE daypartid = '$menu_daypartid' AND accountid = '$curcust'";
						$result10 = Treat_DB_ProxyOld::query( $query10 );
						$num10 = Treat_DB_ProxyOld::mysql_numrows( $result10 );
						//mysql_close();

						///START IF
						if ( $num10 != 0 ) {

?>

										<b>
											<font size="1" color="blue">
												<u><?php echo $menu_daypartname; ?></u>
											</font>
										</b>
										<br/>
<?php

							//mysql_connect($dbhost,$username,$password);
							//@mysql_select_db($database) or die( 'Unable to select database');
							$query2 = "SELECT * FROM order_item WHERE date = '$date' AND daypartid = '$menu_daypartid' AND businessid = '$businessid' ORDER BY order_itemid DESC";
							$result2 = Treat_DB_ProxyOld::query( $query2 );
							$num2 = Treat_DB_ProxyOld::mysql_numrows( $result2 );
							//mysql_close();

							$num2--;
							while ( $num2 >= 0 ) {
								$order_itemid = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'order_itemid' );
								$menu_itemid = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'menu_itemid' );

								//mysql_connect($dbhost,$username,$password);
								//@mysql_select_db($database) or die( 'Unable to select database');
								$query3 = "SELECT * FROM menu_items WHERE menu_item_id = '$menu_itemid'";
								$result3 = Treat_DB_ProxyOld::query( $query3 );
								//mysql_close();

								$item_name = @Treat_DB_ProxyOld::mysql_result( $result3, 0, 'item_name' );

?>

										<font size="1"><?php echo $item_name; ?></font>
										<br/>
<?php

								$num2--;
							}
						}///END IF
						$num--;
					}

	/////////////////////////////////////////////////////////////
				}
				else {
?>

										<font size="2" color="red">CLOSED</font>
<?php
				}

				$counter=0;

?>

									</td>
<?php
				$back = 'white';

				if ( $day == 1 && $long == 1 ) {
					$backstart = 7;
				}
				elseif ( $day == 1 && $long != 1 ) {
					$backstart = $long - 1;
				}

				$day++;
			}
			else {
?>

									<td width="110" bgcolor="#dcdcdc"></td>
<?php
			}
			$weekend = 'F';
			$long++;
		}
?>

								</a>
								</tr>
<?php

		while ( $high > 1 ) {
			$long = 1;
?>

								<tr height="110">
<?php
			$weekend = 'F';
			while ( $long < 8 ) {
				if ( $long == 1 || $long == 7 ) {
					$weekend = 'T';
				}

				if ( $day < 10 && $newmonth < 10 ) {
					$date = "$newyear-0$newmonth-0$day";
				}
				elseif ( $day < 10 && $newmonth > 10 ) {
					$date = "$newyear-$newmonth-0$day";
				}
				elseif ( $day >= 10 && $newmonth < 10 ) {
					$date = "$newyear-0$newmonth-$day";
				}
				else {
					$date = "$newyear-$newmonth-$day";
				}

				if ( $newstart == '' ) {
					$newstart = $start;
				}
				$correcturl = "orderdatecust.php?date=$date&newmonth=$newmonth&newyear=$newyear&newstart=$newstart&day=$day&weekend=$weekend&bid=$businessid&cid=$companyid";

				$show = $day;
				if ( $day > 28 && $newmonth == 2 && $leap == 0 ) {
					$show = '';
				}
				elseif ( $day > 29 && $newmonth == 2 and $leap == 1 ) {
					$show = '';
				}
				elseif ( $day > 30 && ( $newmonth == 4 || $newmonth == 6 || $newmonth == 9 || $newmonth == 11 ) ) {
					$show = '';
				}
				elseif ( $day > 31 ) {
					$show = '';
				}
				else {
				}

				if ( $day > 28 && $newmonth == 2 && $leap == 0 ) {
					$back = '#DCDCDC';
				}
				elseif ( $day > 29 && $newmonth == 2 and $leap == 1 ) {
					$back = '#DCDCDC';
				}
				elseif ( $day > 30 && ( $newmonth == 4 || $newmonth == 6 || $newmonth == 9 || $newmonth == 11 ) ) {
					$back = '#DCDCDC';
				}
				elseif ( $day > 31 ) {
					$back = '#DCDCDC';
				}
				else {
					$back = 'white';
				}

				if ( $day == 29 && $newmonth == 2 && $leap == 0 ) {
					$newstart = $long;
				}
				if ( $day == 30 && $newmonth == 2 && $leap == 1 ) {
					$newstart = $long;
				}
				if ( $day == 31 && ( $newmonth == 4 || $newmonth == 6 || $newmonth == 9 || $newmonth == 11 ) ) {
					$newstart = $long;
				}
				if (
					$day == 32
					&& (
						$newmonth == 1
						|| $newmonth == 3
						|| $newmonth == 5
						|| $newmonth == 7
						|| $newmonth == 8
						|| $newmonth == 10
						|| $newmonth == 12
					)
				) {
					$newstart = $long;
				}

				if ( $day == $todaynum2 && $monthnum == $newmonth && $year == $newyear ) {
					$color = 'red';
				}
				else {
					$color = 'black';
				}

?>

								<a style="<?php echo $style; ?>" href="<?php echo $correcturl; ?>">
									<td
										bgcolor="<?php echo $back; ?>"
										width="120" valign="top"
										onMouseOver="this.bgColor='#999999'"
										onMouseOut="this.bgColor='<?php echo $back; ?>'"
									>
										<a style="<?php echo $style; ?>" href="<?php echo $correcturl; ?>">
											<b>
												<font color="<?php
													echo $color;
												?>"><?php
													echo $show;
												?></font>
											</b>
										</a>
										<br/>
<?php

				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( 'Unable to select database');
				$query14 = "SELECT * FROM caterclose WHERE companyid = '$companyid' AND businessid = '$businessid' AND date = '$date'";
				$result14 = Treat_DB_ProxyOld::query( $query14 );
				$num14 = Treat_DB_ProxyOld::mysql_numrows( $result14 );
				//mysql_close();

				if ( $num14 == 0 ) {
				/////////////////////////////////////////////////////////////
					if ( $back != '#DCDCDC' ) {
						//mysql_connect($dbhost,$username,$password);
						//@mysql_select_db($database) or die( 'Unable to select database');
						$query = "SELECT * FROM menu_daypart WHERE menu_typeid = '$curmenu' ORDER BY orderid DESC";
						$result = Treat_DB_ProxyOld::query( $query );
						$num = Treat_DB_ProxyOld::mysql_numrows( $result );
						//mysql_close();

						$num--;
						while ( $num >= 0 ) {
							$menu_daypartid = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'menu_daypartid' );
							$menu_daypartname = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'menu_daypartname' );

							//mysql_connect($dbhost,$username,$password);
							//@mysql_select_db($database) or die( 'Unable to select database');
							$query10 = "SELECT * FROM menu_daypartdetail WHERE daypartid = '$menu_daypartid' AND accountid = '$curcust'";
							$result10 = Treat_DB_ProxyOld::query( $query10 );
							$num10 = Treat_DB_ProxyOld::mysql_numrows( $result10 );
							//mysql_close();

							///START IF
							if ( $num10 != 0 ) {

?>

										<b>
											<font size="1" color="blue">
												<u><?php echo $menu_daypartname; ?></u>
											</font>
										</b>
										<br/>
<?php

								//mysql_connect($dbhost,$username,$password);
								//@mysql_select_db($database) or die( 'Unable to select database');
								$query2 = "SELECT * FROM order_item WHERE date = '$date' AND daypartid = '$menu_daypartid' AND businessid = '$businessid' ORDER BY order_itemid DESC";
								$result2 = Treat_DB_ProxyOld::query( $query2 );
								$num2 = Treat_DB_ProxyOld::mysql_numrows( $result2 );
								//mysql_close();

								$num2--;
								while ( $num2 >= 0 ) {
									$order_itemid = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'order_itemid' );
									$menu_itemid = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'menu_itemid' );

									//mysql_connect($dbhost,$username,$password);
									//@mysql_select_db($database) or die( 'Unable to select database');
									$query3 = "SELECT * FROM menu_items WHERE menu_item_id = '$menu_itemid'";
									$result3 = Treat_DB_ProxyOld::query( $query3 );
									//mysql_close();

									$item_name = @Treat_DB_ProxyOld::mysql_result( $result3, 0, 'item_name' );

?>

										<font size="1"><?php echo $item_name; ?></font>
										<br/>
<?php

									$num2--;
								}
							}///END IF
							$num--;
						}
					}
					/////////////////////////////////////////////////////////////
				}
				else {
?>

										<font size="2" color="red">CLOSED</font>
<?php
				}
				$counter = 0;

?>

									</td>
								</a>
<?php
				$back = 'white';

				$day++;
				$weekend = 'F';
				$long++;
			}
?>

								</tr>
<?php
			$high--;
		}
?>

							</table>
<?php

		if ( $newmonth == 5 || $newmonth == 7 || $newmonth == 10 || $newmonth == 12 ) {
			$count = 30;
		}
		elseif ( $newmonth == 1 || $newmonth == 2 || $newmonth == 4 || $newmonth == 6 || $newmonth == 8 || $newmonth == 9 || $newmonth == 11 ) {
			$count = 31;
		}
		elseif ( $newmonth == 3 && $leap == 0 ) {
			$count = 28;
		}
		elseif ( $newmonth == 3 && $leap == 1 ) {
			$count = 29;
		}

		$count2 = $backstart;
		while ( $count > 1 )
		{
			if ( $count2 == 1 ) {
				$count2 = 8;
			}
			$count--;
			$count2--;
		}
		$backstart = $count2;

		$prevmonth = $newmonth - 1;
		$prevyear = $newyear;
		if ( $prevmonth == 0 ) {
			$prevmonth = 12;
			$prevyear = $year - 1;
		}
		$nextmonth = $newmonth + 1;
		$nextyear = $newyear;
		if ( $nextmonth == 13 ) {
			$nextmonth = 1;
			$nextyear = $year + 1;
		}
?>

							<a href="/ta/busordertrack.php?newmonth=<?php
								echo $prevmonth;
							?>&amp;newyear=<?php
								echo $prevyear;
							?>&amp;newstart=<?php
								echo $backstart;
							?>&amp;bid=<?php
								echo $businessid;
							?>&amp;cid=<?php
								echo $companyid;
							?>">Previous Month</a> 
							:: 
							<a href="/ta/busordertrack.php?newmonth=<?php
							echo $nextmonth;
							?>&amp;newyear=<?php
							echo $nextyear;
							?>&amp;newstart=<?php
							echo $newstart;
							?>&amp;bid=<?php
							echo $businessid;
							?>&amp;cid=<?php
							echo $companyid;
							?>">Next Month</a>
						</div>
						<p>
<?php
	}
?>
					</td>
				</tr>
			</table>
			<p>
<?php
	////////////////////////////////////////////REPORTS
	$day = date('d');
	$year = date('Y');
	$month = date('m');
	$today="$year-$month-$day";

?>

			<a name="report"></a>
			<table width="100%" cellspacing="0" cellpadding="0" style="border:2px solid #cccccc;">
				<tr bgcolor="#e8e7e7">
					<td>
						<form action="/ta/contractreport.php" method="post" target="_blank">
							<input type="hidden" name="username" value="<?php echo $user; ?>" />
							<input type="hidden" name="password" value="<?php echo $pass; ?>" />
							<input type="hidden" name="businessid" value="<?php echo $businessid; ?>" />
							<input type="hidden" name="companyid" value="<?php echo $companyid; ?>" />
							Reports 
							<script language='JavaScript' id='js20'>
								var cal20 = new CalendarPopup('testdiv1');
								cal20.setCssPrefix('TEST');
							</script>
							<input type="text" name="date1" value="<?php echo $today; ?>" size="8" />
							<a href="#report"
									onClick="cal20.select(document.forms[3].date1,'anchor20','yyyy-MM-dd'); return false;"
									title="cal20.select(document.forms[3].date1,'anchor1x','yyyy-MM-dd'); return false;"
									name="anchor20"
									id="anchor20"
								><img src="/ta/calendar.gif" border="0" height="15" width="16" alt="Choose a Date"
							/></a>  for 
							<select name="account[]" multiple="multiple" size="8">
								<option value="-1" selected="selected">All Accounts</option>
<?php

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( 'Unable to select database');
	$query3 = "SELECT * FROM accounts WHERE businessid = '$businessid' ORDER BY name DESC";
	$result3 = Treat_DB_ProxyOld::query( $query3 );
	$num3 = Treat_DB_ProxyOld::mysql_numrows( $result3 );
	//mysql_close();

	$num3--;

	while ( $num3 >= 0 ) {
		$acctname = @Treat_DB_ProxyOld::mysql_result( $result3, $num3, 'name' );
		$accountid = @Treat_DB_ProxyOld::mysql_result( $result3, $num3, 'accountid' );
?>

								<option value="<?php echo $accountid; ?>"><?php echo $acctname; ?></option>
<?php
		$num3--;
	}
?>

							</select>
							<input type="submit" value="View Report" />
						</form>
					</td>
					<td></td>
				</tr>
			</table>
			<p>
<?php

/////////////////////////////////////////////////////
?>
			<a name="label"></a>
			<table width="100%" cellspacing="0" cellpadding="0" style="border:2px solid #ccc;">
				<tr bgcolor="#e8e7e7">
					<td>
						<form action="/ta/contractlabel.php" method="post" target="_blank">
							<input type="hidden" name="username" value="<?php echo $user; ?>" />
							<input type="hidden" name="password" value="<?php echo $pass; ?>" />
							<input type="hidden" name="businessid" value="<?php echo $businessid; ?>" />
							<input type="hidden" name="companyid" value="<?php echo $companyid; ?>" />
							Labels 
							<script type="text/javascript" id="js90">
								var cal90 = new CalendarPopup('testdiv1');
								cal90.setCssPrefix('TEST');
							</script>
							<input type="text" name="date1" value="<?php echo $today; ?>" size="8" />
							<a href="#label"
								onClick="cal90.select(document.forms[4].date1,'anchor90','yyyy-MM-dd'); return false;"
								title="cal90.select(document.forms[4].date1,'anchor1x','yyyy-MM-dd'); return false;"
								name="anchor90" id="anchor90"
							><img src="/ta/calendar.gif" border="0" height="15" width="16" alt="Choose a Date" /></a>
							for 
							<select name="account[]" multiple="multiple" size="8">
								<option value="-1" selected="selected">All Accounts</option>
<?php

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( 'Unable to select database');
	$query3 = "SELECT * FROM accounts WHERE businessid = '$businessid' ORDER BY name DESC";
	$result3 = Treat_DB_ProxyOld::query( $query3 );
	$num3 = Treat_DB_ProxyOld::mysql_numrows( $result3 );
	//mysql_close();

	$num3--;

	while ( $num3 >= 0 ) {
		$acctname = @Treat_DB_ProxyOld::mysql_result( $result3, $num3, 'name' );
		$accountid = @Treat_DB_ProxyOld::mysql_result( $result3, $num3, 'accountid' );
?>

								<option value="<?php echo $accountid; ?>"><?php echo $acctname; ?></option>
<?php
		$num3--;
	}
?>

							</select>
							<select name="label_type">
								<option value="1">Inline</option>
								<option value="2">Sheet</option>
							</select>
							<select name="item_only">
								<option value="-1">All Items</option>
<?php

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( 'Unable to select database');
	$query3 = "SELECT * FROM menu_items WHERE businessid = '$businessid' AND label = '1' ORDER BY item_name DESC";
	$result3 = Treat_DB_ProxyOld::query( $query3 );
	$num3 = Treat_DB_ProxyOld::mysql_numrows( $result3 );
	//mysql_close();

	$num3--;
	while ( $num3 >= 0 ) {
		$item_name = @Treat_DB_ProxyOld::mysql_result( $result3, $num3, 'item_name' );
		$itemid = @Treat_DB_ProxyOld::mysql_result( $result3, $num3, 'menu_item_id' );

?>

								<option value="<?php echo $itemid; ?>"><?php echo $item_name; ?></option>
<?php

		$num3--;
	}
?>

							</select>
							<input type="submit" value="Create" />
						</form>
					</td>
					<td></td>
				</tr>
			</table>
			<p>
<?php
////////////////////////////////////////////COPY ORDERS
	$day = date('d');
	$year = date('Y');
	$month = date('m');
	$today = "$year-$month-$day";
	$tomorrow = nextday( $today );

?>

			<a name="copy"></a>
			<table width="100%" cellspacing="0" cellpadding="0" style="border:2px solid #ccc;">
				<tr bgcolor="#e8e7e7">
					<td>
						<form action="/ta/copyorder.php" method="post" onSubmit="return disableForm(this);">
							<input type="hidden" name="username" value="<?php echo $user; ?>" />
							<input type="hidden" name="password" value="<?php echo $pass; ?>" />
							<input type="hidden" name="businessid" value="<?php echo $businessid; ?>" />
							<input type="hidden" name="companyid" value="<?php echo $companyid; ?>" />
							Copy Orders from
							<script type="text/javascript" id="js30">
								var cal30 = new CalendarPopup('testdiv1');
								cal30.setCssPrefix('TEST');
							</script>
							<input type="text" name="date1" value="<?php echo $today; ?>" size="8" />
							<a href="#copy"
								onClick="cal30.select(document.forms[5].date1,'anchor30','yyyy-MM-dd'); return false;"
								title="cal30.select(document.forms[5].date1,'anchor1x','yyyy-MM-dd'); return false;"
								name="anchor30" id="anchor30"
							><img src="/ta/calendar.gif" border="0" height="15" width="16" alt="Choose a Date"></a>
							to
							<script type="text/javascript" id="js31">
								var cal31 = new CalendarPopup('testdiv1');
								cal31.setCssPrefix('TEST');
							</script>
							<input type="text" name="date2" value="<?php echo $tomorrow; ?>" size="8" />
							<a href="#copy"
								onClick="cal31.select(document.forms[5].date2,'anchor31','yyyy-MM-dd'); return false;"
								title="cal31.select(document.forms[5].date2,'anchor1x','yyyy-MM-dd'); return false;"
								name="anchor31" id="anchor31"
							><img src="/ta/calendar.gif" border="0" height="15" width="16" alt="Choose a Date"></a>
							for
							<select name="account">
								<option value="-1">All Accounts</option>
<?php

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( 'Unable to select database');
	$query3 = "SELECT * FROM accounts WHERE businessid = '$businessid' ORDER BY name DESC";
	$result3 = Treat_DB_ProxyOld::query( $query3 );
	$num3 = Treat_DB_ProxyOld::mysql_numrows( $result3 );
	//mysql_close();

	$num3--;

	while ( $num3 >= 0 ) {
		$acctname = @Treat_DB_ProxyOld::mysql_result( $result3, $num3, 'name' );
		$accountid = @Treat_DB_ProxyOld::mysql_result( $result3, $num3, 'accountid' );
?>

								<option value="<?php echo $accountid; ?>"><?php echo $acctname; ?></option>
<?php
		$num3--;
	}
?>

							</select>
							<input type="submit" value="Copy" />
						</form>
					</td>
					<td></td>
				</tr>
			</table>
			<p>
<?php

/////////////////////////////////////////////////////
////////////////////////////////////////////FINANCIALS
	$day = date('d');
	$year = date('Y');
	$month = date('m');
	$today="$year-$month-$day";
	$tomorrow=nextday($today);

?>
			<a name="copy"></a>
			<table width="100%" cellspacing="0" cellpadding="0" style="border:2px solid #ccc;">
				<tr bgcolor="#e8e7e7">
					<td>
						<form action="/ta/contractfinance.php" method="post" target="_blank">
							<input type="hidden" name="username" value="<?php echo $user; ?>" />
							<input type="hidden" name="password" value="<?php echo $pass; ?>" />
							<input type="hidden" name="businessid" value="<?php echo $businessid; ?>" />
							<input type="hidden" name="companyid" value="<?php echo $companyid; ?>" />
							Financials from 
							<script type="text/javascript" id="js33">
								var cal33 = new CalendarPopup('testdiv1');
								cal33.setCssPrefix('TEST');
							</script>
							<input type="text" name="date1" value="<?php echo $today; ?>" size="8">
							<a href="#copy"
								onClick="cal33.select(document.forms[6].date1,'anchor33','yyyy-MM-dd'); return false;"
								title="cal33.select(document.forms[6].date1,'anchor1x','yyyy-MM-dd'); return false;"
								name="anchor33" id="anchor33"
							><img src="/ta/calendar.gif" border="0" height="15" width="16" alt="Choose a Date"></a>
							to
							<script type="text/javascript" id="js34">
								var cal34 = new CalendarPopup('testdiv1');
								cal34.setCssPrefix('TEST');
							</script>
							<input type="text" name="date2" value="<?php echo $tomorrow; ?>" size="8" />
							<a href="#copy"
								onClick="cal34.select(document.forms[6].date2,'anchor34','yyyy-MM-dd'); return false;"
								title="cal34.select(document.forms[6].date2,'anchor1x','yyyy-MM-dd'); return false;"
								name="anchor34" id="anchor34"
							><img src="/ta/calendar.gif" border="0" height="15" width="16" alt="Choose a Date"></a>
							for
							<select name="account">
								<option value="-1">All Accounts</option>
<?php

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( 'Unable to select database');
	$query3 = "SELECT * FROM accounts WHERE businessid = '$businessid' ORDER BY name DESC";
	$result3 = Treat_DB_ProxyOld::query( $query3 );
	$num3 = Treat_DB_ProxyOld::mysql_numrows( $result3 );
	//mysql_close();

	$num3--;

	while ( $num3 >= 0 ) {
		$acctname = @Treat_DB_ProxyOld::mysql_result( $result3, $num3, 'name' );
		$accountid = @Treat_DB_ProxyOld::mysql_result( $result3, $num3, 'accountid' );
?>
								<option value="<?php echo $accountid; ?>"><?php echo $acctname; ?></option>
<?php
		$num3--;
	}
?>

							</select>
							<input type="submit" value="View Report" />
						</form>
					</td>
					<td></td>
				</tr>
			</table>
			<p>
<?php

#	mysql_close();
/////////////////////////////////////////////////////
?>

			<form action="/ta/businesstrack.php" method="post">
				<div class="button">
					<input type="hidden" name="username" value="<?php echo $user; ?>" />
					<input type="hidden" name="password" value="<?php echo $pass; ?>" />
					<input type="submit" value=" Return to Main Page" />
				</div>
			</form>
		
			<div
				id="testdiv1"
				style="position:absolute;visibility:hidden;background-color:white;layer-background-color:white;"
			></div>
		</div>
<?php
	
	google_page_track();
?>

	</body>
</html>
<?php
}

