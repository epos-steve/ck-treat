<?php
if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
require_once( DOC_ROOT . '/bootstrap.php' );
include_once( 'db.php' );

use EE\Model\Inventory\Item as InventoryItem;
use EE\Model\Nutrition\RecipeItem;

$pageVariables = new \EE\ArrayObject();
$pageVariables->user = \EE\Controller\Base::getSessionCookieVariable( 'usercook' );
$pageVariables->pass = \EE\Controller\Base::getSessionCookieVariable( 'passcook' );
$pageVariables->businessId = \EE\Controller\Base::getPostOrGetVariable( array( 'businessid', 'bid' ) );
$pageVariables->companyId = \EE\Controller\Base::getPostOrGetVariable( array( 'companyid', 'cid' ) );
$pageVariables->editId = \EE\Controller\Base::getPostOrGetVariable( 'editid' );
$pageVariables->minId = \EE\Controller\Base::getPostOrGetVariable( 'minid' );
$pageVariables->edit = \EE\Controller\Base::getPostOrGetVariable( 'edit' );
$pageVariables->rec_num = \EE\Controller\Base::getPostOrGetVariable( 'rec_num' );
$pageVariables->inv_itemid = \EE\Controller\Base::getPostOrGetVariable( 'inv_itemid' );
$pageVariables->srv_num = \EE\Controller\Base::getPostOrGetVariable( 'srv_num' );
$pageVariables->rec_units = \EE\Controller\Base::getPostOrGetVariable( 'rec_units' );
$pageVariables->rec_order = \EE\Controller\Base::getPostOrGetVariable( 'rec_order' );
$pageVariables->batch = \EE\Controller\Base::getPostOrGetVariable( 'batch' );
$pageVariables->showvendor = \EE\Controller\Base::getPostOrGetVariable( 'showvendor' );
$pageVariables->recipeid = \EE\Controller\Base::getPostOrGetVariable( 'recipeid' );

$user = \EE\Model\User\Login::login( $pageVariables->user, $pageVariables->pass, true );

if ( !$user || !$pageVariables->user || !$pageVariables->pass ) {
	echo "<center><h3>Failed</h3></center>";
}

else {
	if ( $pageVariables->edit == 0 && ( $pageVariables->editId != 0 || $pageVariables->minId ) && $pageVariables->rec_num != 0 ) {
		$inventoryItem = InventoryItem::getById( intval( $pageVariables->inv_itemid ) );

		if ( $pageVariables->rec_order == 1 ) {
			$rec_size = $inventoryItem->rec_size;
		}
		elseif ( $pageVariables->rec_order == 2 ) {
			$rec_size = $inventoryItem->rec_size2;
		}

		$query = 'SELECT * FROM conversion WHERE sizeid = :rec_units AND sizeidto = :rec_size';
		$params = array(
			'rec_size' => $rec_size,
			'rec_units' => $pageVariables->rec_units,
		);
		$treatPdo = InventoryItem::db();
		$statement = $treatPdo->prepare( $query );
		if ( $statement->execute( $params ) ) {
			$conversion = $statement->fetchObject( '\EE\ArrayObject' );
		}
		if ( !$conversion ) {
			$conversion = new \EE\ArrayObject();
		}

		if ( $pageVariables->batch == 1 ) {
			$pageVariables->editId = $pageVariables->editId * -1;
		}

		$recipeItem = new RecipeItem();
		if ( $pageVariables->minId ) {
			$recipeItem->menu_item_new_id = intval( $pageVariables->minId );
		}
		else {
			$recipeItem->menu_itemid = intval( $pageVariables->editId );
		}
		$recipeItem->inv_itemid = intval( $pageVariables->inv_itemid );
		$recipeItem->rec_num = floatval( $pageVariables->rec_num );
		$recipeItem->rec_size = intval( $rec_size );
		$recipeItem->srv_num = floatval( $conversion->convert );
		$recipeItem->rec_order = intval( $pageVariables->rec_order );
		$result = $recipeItem->save();
		if ( $result ) {
			$_SESSION->addFlashMessage( sprintf(
				'Added <strong>"%s"</strong> to the recipe.'
				,$inventoryItem->item_name
			) );
		}
		else {
			$_SESSION->addFlashError( sprintf(
				'Encountered an error while attempting to add <strong>"%s"</strong> to the recipe.'
				,$inventoryItem->item_name
			) );
		}
	}
	elseif ( $pageVariables->edit == 2 ) {
		$recipeItem = RecipeItem::getById( intval( $pageVariables->recipeid ) );
		$recipeItem->delete();
	}

	$queryString = array(
		'cid' => $pageVariables->companyId,
		'bid' => $pageVariables->businessId,
		'editid' => abs( $pageVariables->editId ),
		'minid' => $pageVariables->minId,
		'item_price' => $pageVariables->price,
		'showvendor' => $pageVariables->showvendor,
		'batch' => $pageVariables->batch,
	);
	$location = '/ta/businventor4.php?' . http_build_query( $queryString );
	header( 'Location: ' . $location );
}

