<?php

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+3);
        if ($diff > 0 && $diff < '0.01'){$diff="0.01";}
        elseif($diff < 0 && $diff > '-0.01'){$diff="-0.01";}
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      if ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='32')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='08' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      $day=$day-31;  
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";} 
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

include("db.php");
$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");
$bbdayname=date("l");
$bbmonth=date("F");
$bbday=date("j");

$user=$_POST['username'];
$pass=$_POST['password'];
$reserveid=$_POST['reserveid'];
$accountid=$_POST['accountid'];
if ($user==""&&$pass==""){
   $user=$_COOKIE["usercook"];
   $pass=$_COOKIE["passcook"];
}
if ($reserveid==""){$reserveid=$_GET['reserveid'];}

mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM customer WHERE username = '$user' AND password = '$pass'";
$result = mysql_query($query);
$num=mysql_numrows($result);
mysql_close();

mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");
$query2 = "SELECT * FROM reserve WHERE reserveid = '$reserveid'";
$result2 = mysql_query($query2);
$num2=mysql_numrows($result2);
mysql_close();

if ($num != 1 || $user == "" || $pass == "")
{
    echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
    $user=mysql_result($result,0,"username");
    $customerid=mysql_result($result,0,"customerid");
    $accountid=mysql_result($result,0,"accountid");
    $businessid=mysql_result($result,0,"businessid");

    $accountid2=mysql_result($result2,0,"accountid");

    setcookie("usercook",$user);
    setcookie("passcook",$pass);

    echo "<body>";
    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%><tr><td colspan=4><img src=logo.jpg><p></td></tr>";

    mysql_connect($dbhost,$username,$password);
    @mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM company WHERE companyid = '1'";
    $result = mysql_query($query);
    mysql_close();

    $companyname=mysql_result($result,0,"companyname");

    mysql_connect($dbhost,$username,$password);
    @mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM business WHERE businessid = '$businessid'";
    $result = mysql_query($query);
    mysql_close();

    $taxrate=mysql_result($result,0,"tax");

    mysql_connect($dbhost,$username,$password);
    @mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM accounts WHERE accountid = '$accountid'";
    $result = mysql_query($query);
    mysql_close();

    $accountname=mysql_result($result,0,"name");
    $accountnum=mysql_result($result,0,"accountnum");
    $taxid=mysql_result($result,0,"taxid");

    mysql_connect($dbhost,$username,$password);
    @mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM reserve WHERE reserveid = '$reserveid'";
    $result = mysql_query($query);
    $num=mysql_numrows($result);
    mysql_close();

    $date=mysql_result($result,0,"date");
    $room=mysql_result($result,0,"roomid");
    $start_hour=mysql_result($result,0,"start_hour");
    $end_hour=mysql_result($result,0,"end_hour");
    $status=mysql_result($result,0,"status");
    $comment=mysql_result($result,0,"comment");
    $roomcomment=mysql_result($result,0,"roomcomment");
    $peoplenum=mysql_result($result,0,"peoplenum");

    echo "<tr bgcolor=black><td height=1 colspan=4></td></tr>";
    echo "<tr bgcolor=#CCCCFF><td width=10%><img src=weblogo.jpg height=75 width=80></td><td><font size=4><b>$companyname</b></font><br><b>Account: $accountname<br>Account #: $accountnum</td><td colspan=2 align=right valign=top><b>$bbdayname, $bbmonth $bbday, $year</b></td></tr>";
    echo "<tr bgcolor=black><td height=1 colspan=4></td></tr>";
    echo "</table></center><p>";

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    mysql_connect($dbhost,$username,$password);
    @mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM menu_groups ORDER BY groupname DESC";
    $result = mysql_query($query);
    $num=mysql_numrows($result);
    mysql_close();

    $num--;
    while ($num>=0){
       $groupid=mysql_result($result,$num,"menu_group_id");
       $groupname=mysql_result($result,$num,"groupname");
       $description=mysql_result($result,$num,"description");
       $image=mysql_result($result,$num,"image");
       $img_height=mysql_result($result,$num,"img_height");
       $img_width=mysql_result($result,$num,"img_width");

       echo "<center><table width=90% bgcolor='#E8E7E7' cellspacing=0 cellpadding=0 border=0>";
       echo "<tr height=1 bgcolor=black><td colspan=3></td></tr>";
       echo "<tr><td width=5 rowspan=2 height=100></td><td width=100 rowspan=2><a href=menuitems.php?reserveid=$reserveid&account=$accountid&groupid=$groupid><img src='pictures/$image' height=$img_height width=$img_width border=0></a></td><td><font size=4><b><i>$groupname</i></b></font></td></tr>";
       echo "<tr><td>$description</td></tr>";
       echo "<tr height=1 bgcolor=black><td colspan=3></td></tr>";

       mysql_connect($dbhost,$username,$password);
       @mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM menu_items WHERE businessid = '$businessid' AND groupid = '$groupid'";
       $result2 = mysql_query($query2);
       $num2=mysql_numrows($result2);
       mysql_close();

       echo "</table></center><p>";
       $num--;
    }

    echo "<center><a style=$style href=caterdetails.php?reserveid=$reserveid&account=$accountid><font color=blue>Return</font></a></center>";
    echo "</body>";

}
?>