<head>

<SCRIPT LANGUAGE="JavaScript" SRC="CalendarPopup.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">document.write(getCalendarStyles());</SCRIPT>

<STYLE>
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation
			{
			background-color:#6677DD;
			text-align:center;
			vertical-align:center;
			text-decoration:none;
			color:#FFFFFF;
			font-weight:bold;
			}
	.TESTcpDayColumnHeader,
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation,
	.TESTcpCurrentMonthDate,
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDate,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDate,
	.TESTcpCurrentDateDisabled,
	.TESTcpTodayText,
	.TESTcpTodayTextDisabled,
	.TESTcpText
			{
			font-family:arial;
			font-size:8pt;
			}
	TD.TESTcpDayColumnHeader
			{
			text-align:right;
			border:solid thin #6677DD;
			border-width:0 0 1 0;
			}
	.TESTcpCurrentMonthDate,
	.TESTcpOtherMonthDate,
	.TESTcpCurrentDate
			{
			text-align:right;
			text-decoration:none;
			}
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDateDisabled
			{
			color:#D0D0D0;
			text-align:right;
			text-decoration:line-through;
			}
	.TESTcpCurrentMonthDate
			{
			color:#6677DD;
			font-weight:bold;
			}
	.TESTcpCurrentDate
			{
			color: #FFFFFF;
			font-weight:bold;
			}
	.TESTcpOtherMonthDate
			{
			color:#808080;
			}
	TD.TESTcpCurrentDate
			{
			color:#FFFFFF;
			background-color: #6677DD;
			border-width:1;
			border:solid thin #000000;
			}
	TD.TESTcpCurrentDateDisabled
			{
			border-width:1;
			border:solid thin #FFAAAA;
			}
	TD.TESTcpTodayText,
	TD.TESTcpTodayTextDisabled
			{
			border:solid thin #6677DD;
			border-width:1 0 0 0;
			}
	A.TESTcpTodayText,
	SPAN.TESTcpTodayTextDisabled
			{
			height:20px;
			}
	A.TESTcpTodayText
			{
			color:#6677DD;
			font-weight:bold;
			}
	SPAN.TESTcpTodayTextDisabled
			{
			color:#D0D0D0;
			}
	.TESTcpBorder
			{
			border:solid thin #6677DD;
			}
</STYLE>

<SCRIPT LANGUAGE="JavaScript">
<!-- Web Site:  http://dynamicdrive.com -->

<!-- This script and many more are available free online at -->
<!-- The JavaScript Source!! http://javascript.internet.com -->

<!-- Begin
function disableForm(theform) {
if (document.all || document.getElementById) {
for (i = 0; i < theform.length; i++) {
var tempobj = theform.elements[i];
if (tempobj.type.toLowerCase() == "submit" || tempobj.type.toLowerCase() == "reset")
tempobj.disabled = true;
}

}
else {
alert("The form has been submitted.  But, since you're not using IE 4+ or NS 6, the submit button was not disabled on form submission.");
return false;
   }
}
//  End -->
</script>

<script type="text/javascript" src="javascripts/prototype.js"></script>

<script language="JavaScript" 
   type="text/JavaScript">
window.onload = function(){
   new headerScroll('test',{head: 'header'});
} 
</script>

</head>

<?php

if ( !defined('DOC_ROOT'))
{
	define('DOC_ROOT', dirname(dirname(__FILE__)));
}
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');

$style = "text-decoration:none";

function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}

function nextday($nextd,$day_format=''){ //Function returns next day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monn = substr($nextd, 5, 2);
   $dayn = substr($nextd, 8, 2);
   $yearn = substr($nextd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monn, $dayn+1, $yearn));
   return $tempdate;
}

function prevday($prevd,$day_format=''){ //Function returns previous day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monp = substr($prevd, 5, 2);
   $dayp = substr($prevd, 8, 2);
   $yearp = substr($prevd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monp, $dayp-1, $yearp));
   return $tempdate;
}

$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";

$user = isset($_COOKIE["usercook"])?$_COOKIE["usercook"]:'';
$pass = isset($_COOKIE["passcook"])?$_COOKIE["passcook"]:'';
$loginid = isset($_GET['lid'])?$_GET['lid']:'';

if($loginid==""){
   $loginid = isset($_POST['loginid'])?$_POST['loginid']:'';

   $lunch_sun = isset($_POST['lunch_sun'])?$_POST['lunch_sun']:'';
   $lunch_sun_time = isset($_POST['lunch_sun_time'])?$_POST['lunch_sun_time']:'';
   $lunch_mon = isset($_POST['lunch_mon'])?$_POST['lunch_mon']:'';
   $lunch_mon_time = isset($_POST['lunch_mon_time'])?$_POST['lunch_mon_time']:'';
   $lunch_tue = isset($_POST['lunch_tue'])?$_POST['lunch_tue']:'';
   $lunch_tue_time = isset($_POST['lunch_tue_time'])?$_POST['lunch_tue_time']:'';
   $lunch_wed = isset($_POST['lunch_wed'])?$_POST['lunch_wed']:'';
   $lunch_wed_time = isset($_POST['lunch_wed_time'])?$_POST['lunch_wed_time']:'';
   $lunch_thu = isset($_POST['lunch_thu'])?$_POST['lunch_thu']:'';
   $lunch_thu_time = isset($_POST['lunch_thu_time'])?$_POST['lunch_thu_time']:'';
   $lunch_fri = isset($_POST['lunch_fri'])?$_POST['lunch_fri']:'';
   $lunch_fri_time = isset($_POST['lunch_fri_time'])?$_POST['lunch_fri_time']:'';
   $lunch_sat = isset($_POST['lunch_sat'])?$_POST['lunch_sat']:'';
   $lunch_sat_time = isset($_POST['lunch_sat_time'])?$_POST['lunch_sat_time']:'';
   $clockout = isset($_POST['clockout'])?$_POST['clockout']:'';
   $clockout_time = isset($_POST['clockout_time'])?$_POST['clockout_time']:'';
   $auto_punch = isset($_POST['auto_punch'])?$_POST['auto_punch']:'';
   $auto_punch_hours = isset($_POST['auto_punch_hours'])?$_POST['auto_punch_hours']:'';

   $saveme=1;
}

mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = mysql_query($query);
$num=mysql_numrows($result);

if ($num != 1)
{
    echo "<center><h3>Failed</h3></center>";
}

else
{
    if($saveme==1){
       $query = "SELECT * FROM labor_clockin_options WHERE loginid = '$loginid'";
       $result = mysql_query($query);
       $num=mysql_numrows($result);

       if($num==1){
          $query = "UPDATE labor_clockin_options SET lunch_sun = '$lunch_sun', lunch_mon = '$lunch_mon', lunch_tue = '$lunch_tue', lunch_wed = '$lunch_wed', lunch_thu = '$lunch_thu', lunch_fri = '$lunch_fri', lunch_sat = '$lunch_sat', lunch_sun_time = '$lunch_sun_time', lunch_mon_time = '$lunch_mon_time', lunch_tue_time = '$lunch_tue_time', lunch_wed_time = '$lunch_wed_time', lunch_thu_time = '$lunch_thu_time', lunch_fri_time = '$lunch_fri_time', lunch_sat_time = '$lunch_sat_time', clockout = '$clockout', clockout_time = '$clockout_time', auto_punch = '$auto_punch', auto_punch_hours = '$auto_punch_hours' WHERE loginid = '$loginid'";
          $result = mysql_query($query);
       }
       else{
          $query = "INSERT INTO labor_clockin_options (loginid,lunch_sun,lunch_mon,lunch_tue,lunch_wed,lunch_thu,lunch_fri,lunch_sat,lunch_sun_time,lunch_mon_time,lunch_tue_time,lunch_wed_time,lunch_thu_time,lunch_fri_time,lunch_sat_time,clockout,clockout_time,auto_punch,auto_punch_hours) VALUES ('$loginid','$lunch_sun','$lunch_mon','$lunch_tue','$lunch_wed','$lunch_thu','$lunch_fri','$lunch_sat','$lunch_sun_time','$lunch_mon_time','$lunch_tue_time','$lunch_wed_time','$lunch_thu_time','$lunch_fri_time','$lunch_sat_time','$clockout','$clockout_time','$auto_punch','$auto_punch_hours')";
          $result = mysql_query($query);
       }
    }

    $query = "SELECT * FROM labor_clockin_options WHERE loginid = '$loginid'";
    $result = mysql_query($query);
    $num=mysql_numrows($result);

    $lunch_sun=@mysql_result($result,0,"lunch_sun");
    $lunch_sun_time=@mysql_result($result,0,"lunch_sun_time");
    $lunch_mon=@mysql_result($result,0,"lunch_mon");
    $lunch_mon_time=@mysql_result($result,0,"lunch_mon_time");
    $lunch_tue=@mysql_result($result,0,"lunch_tue");
    $lunch_tue_time=@mysql_result($result,0,"lunch_tue_time");
    $lunch_wed=@mysql_result($result,0,"lunch_wed");
    $lunch_wed_time=@mysql_result($result,0,"lunch_wed_time");
    $lunch_thu=@mysql_result($result,0,"lunch_thu");
    $lunch_thu_time=@mysql_result($result,0,"lunch_thu_time");
    $lunch_fri=@mysql_result($result,0,"lunch_fri");
    $lunch_fri_time=@mysql_result($result,0,"lunch_fri_time");
    $lunch_sat=@mysql_result($result,0,"lunch_sat");
    $lunch_sat_time=@mysql_result($result,0,"lunch_sat_time");
    $clockout=@mysql_result($result,0,"clockout");
    $clockout_time=@mysql_result($result,0,"clockout_time");
	$auto_punch=@mysql_result($result,0,"auto_punch");
	$auto_punch_hours=@mysql_result($result,0,"auto_punch_hours");

    $sun_color="white";
    $mon_color="white";
    $tue_color="white";
    $wed_color="white";
    $thu_color="white";
    $fri_color="white";
    $sat_color="white";
    $clock_color="white";
	$punch_color="white";

    if($lunch_sun==1){$lunch_sun="CHECKED";$sun_color="#FFFF99";}
    if($lunch_mon==1){$lunch_mon="CHECKED";$mon_color="#FFFF99";}
    if($lunch_tue==1){$lunch_tue="CHECKED";$tue_color="#FFFF99";}
    if($lunch_wed==1){$lunch_wed="CHECKED";$wed_color="#FFFF99";}
    if($lunch_thu==1){$lunch_thu="CHECKED";$thu_color="#FFFF99";}
    if($lunch_fri==1){$lunch_fri="CHECKED";$fri_color="#FFFF99";}
    if($lunch_sat==1){$lunch_sat="CHECKED";$sat_color="#FFFF99";}

    if($clockout==1){$clockout="CHECKED";$clock_color="#FFFF99";}
    if($clockout_time==""){$clockout_time="12:00a";}
	
	if($auto_punch==1){$auto_punch="CHECKED";$punch_color="#FFFF99";}

    $query = "SELECT firstname,lastname,empl_no FROM login WHERE loginid = '$loginid'";
    $result = mysql_query($query);

    $firstname=@mysql_result($result,0,"firstname");
    $lastname=@mysql_result($result,0,"lastname");
    $empl_no=@mysql_result($result,0,"empl_no");

    echo "<form action=employee_options.php method=post onSubmit='return disableForm(this);'><input type=hidden name=loginid value=$loginid>";

    echo "<table cellspacing=0 cellpadding=0 width=100% style=\"border:1px solid #E8E7E7;\" bgcolor=#E8E7E7>";
    echo "<tr><td style=\"border:1px solid #E8E7E7;\">&nbsp;<b>TimeClock Options for #$empl_no $firstname $lastname</td></tr>";
    echo "</table><p>";

    echo "<table cellspacing=0 cellpadding=0 width=100% style=\"border:1px solid #E8E7E7;\">";
    echo "<tr bgcolor=#E8E7E7><td style=\"border:1px solid #E8E7E7;\" colspan=3>&nbsp;<b>Automatic TimeOut</td></tr>";
    echo "<tr bgcolor=$clock_color><td style=\"border:1px solid #E8E7E7;\" width=33%>&nbsp;TimeOut</td><td style=\"border:1px solid #E8E7E7;\" width=33%>&nbsp;<input type=checkbox name=clockout value=1 $clockout> Active</td><td style=\"border:1px solid #E8E7E7;\">&nbsp;<input type=text size=5 name=clockout_time value='$clockout_time'> Time</td></tr>";
    echo "</table><font size=2>*Enter time in 12:00a format.<p>";
	
	echo "<table cellspacing=0 cellpadding=0 width=100% style=\"border:1px solid #E8E7E7;\">";
    echo "<tr bgcolor=#E8E7E7><td style=\"border:1px solid #E8E7E7;\" colspan=3>&nbsp;<b>Automatic Punches</td></tr>";
    echo "<tr bgcolor=$punch_color><td colspan=3 style=\"border:1px solid #E8E7E7;\" width=33%>&nbsp;<input type=checkbox name=auto_punch value=1 $auto_punch> Active, for <input type=text name=auto_punch_hours size=3 value='$auto_punch_hours'> hours</td></tr>";
    echo "</table><font size=2>*This will automatically add a punch on week days for # of hours. The purpose is to add records (for editing) for employees who are unable to clockin or out.<p>";

    echo "<table cellspacing=0 cellpadding=0 width=100% style=\"border:1px solid #E8E7E7;\">";
    echo "<tr bgcolor=#E8E7E7><td style=\"border:1px solid #E8E7E7;\" colspan=3>&nbsp;<b>Automatic Lunches</td></tr>";
    echo "<tr bgcolor=$sun_color><td style=\"border:1px solid #E8E7E7;\" width=33%>&nbsp;Sunday</td><td style=\"border:1px solid #E8E7E7;\" width=33%>&nbsp;<input type=checkbox name=lunch_sun value=1 $lunch_sun> Active</td><td style=\"border:1px solid #E8E7E7;\">&nbsp;<input type=text size=5 name=lunch_sun_time value='$lunch_sun_time'> Hours</td></tr>";
    echo "<tr bgcolor=$mon_color><td style=\"border:1px solid #E8E7E7;\" width=33%>&nbsp;Monday</td><td style=\"border:1px solid #E8E7E7;\" width=33%>&nbsp;<input type=checkbox name=lunch_mon value=1 $lunch_mon> Active</td><td style=\"border:1px solid #E8E7E7;\">&nbsp;<input type=text size=5 name=lunch_mon_time value='$lunch_mon_time'> Hours</td></tr>";
    echo "<tr bgcolor=$tue_color><td style=\"border:1px solid #E8E7E7;\" width=33%>&nbsp;Tuesday</td><td style=\"border:1px solid #E8E7E7;\" width=33%>&nbsp;<input type=checkbox name=lunch_tue value=1 $lunch_tue> Active</td><td style=\"border:1px solid #E8E7E7;\">&nbsp;<input type=text size=5 name=lunch_tue_time value='$lunch_tue_time'> Hours</td></tr>";
    echo "<tr bgcolor=$wed_color><td style=\"border:1px solid #E8E7E7;\" width=33%>&nbsp;Wednesday</td><td style=\"border:1px solid #E8E7E7;\" width=33%>&nbsp;<input type=checkbox name=lunch_wed value=1 $lunch_wed> Active</td><td style=\"border:1px solid #E8E7E7;\">&nbsp;<input type=text size=5 name=lunch_wed_time value='$lunch_wed_time'> Hours</td></tr>";
    echo "<tr bgcolor=$thu_color><td style=\"border:1px solid #E8E7E7;\" width=33%>&nbsp;Thursday</td><td style=\"border:1px solid #E8E7E7;\" width=33%>&nbsp;<input type=checkbox name=lunch_thu value=1 $lunch_thu> Active</td><td style=\"border:1px solid #E8E7E7;\">&nbsp;<input type=text size=5 name=lunch_thu_time value='$lunch_thu_time'> Hours</td></tr>";
    echo "<tr bgcolor=$fri_color><td style=\"border:1px solid #E8E7E7;\" width=33%>&nbsp;Friday</td><td style=\"border:1px solid #E8E7E7;\" width=33%>&nbsp;<input type=checkbox name=lunch_fri value=1 $lunch_fri> Active</td><td style=\"border:1px solid #E8E7E7;\">&nbsp;<input type=text size=5 name=lunch_fri_time value='$lunch_fri_time'> Hours</td></tr>";
    echo "<tr bgcolor=$sat_color><td style=\"border:1px solid #E8E7E7;\" width=33%>&nbsp;Saturday</td><td style=\"border:1px solid #E8E7E7;\" width=33%>&nbsp;<input type=checkbox name=lunch_sat value=1 $lunch_sat> Active</td><td style=\"border:1px solid #E8E7E7;\">&nbsp;<input type=text size=5 name=lunch_sat_time value='$lunch_sat_time'> Hours</td></tr>";
    echo "</table><font size=2>*Enter the hours in a decimal format (ie. .5 for half an hour).  Currently lunches will be figured for employees with 1 clockin/out per day.<p>";

    echo "<input type=submit value='Save'></form>";
}
mysql_close();
?>