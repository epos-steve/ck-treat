<?php
setlocale( LC_ALL, 'en_US.utf8' );

//include_once( 'db.php' );
if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
require_once( dirname(__FILE__) . '/../../application/bootstrap.php' );
require_once( DOC_ROOT . '/bootstrap.php' );

define( 'QUICK_SERVICE_MENU', -1 );

$view = null; # Notice: Undefined variable
$date1 = null; # Notice: Undefined variable
$date2 = null; # Notice: Undefined variable
$rec_sizename = null; # Notice: Undefined variable
$item_price = null; # Notice: Undefined variable

//Treat_DB_ProxyOld::$debug = true;
$style = "text-decoration:none";

$passed_cookie_login = require_once( realpath( DOC_ROOT . '/../lib/inc/cookie-login.php' ) );
$pageBusiness = 
$page_business = \EE\Model\Business\Singleton::getSingleton();

$pageUser = $_SESSION->getUser();
if ( !$pageUser ) {
	$pageUser = new \EE\Null();
}

///security check for recipes 2/28/13
if ( $passed_cookie_login && 1 > $pageUser->bus_inventor4 ) {
	$location = '/ta/businesstrack.php';
	if ( isset( $_SERVER['HTTP_REFERER'] ) ) {
		$location = $_SERVER['HTTP_REFERER'];
	}
	header( 'Location: ' . $location );
	exit();
}

if (
	(
		$passed_cookie_login
		&& (
			(
				$GLOBALS['security_level'] == 1
				&& $GLOBALS['bid'] == $pageBusiness->getBusinessId()
				&& $GLOBALS['cid'] == $pageBusiness->getCompanyId()
			)
			|| (
				$GLOBALS['security_level'] > 1
				&& $GLOBALS['cid'] == $pageBusiness->getCompanyId()
			)
		)
	)
) {
	$curmenu = \EE\Controller\Base::getPostGetSessionOrCookieVariable( 'curmenu' );
	$businessid = $pageBusiness->getBusinessId();
	$companyid = $pageBusiness->getCompanyId();
	$showvendor = \EE\Controller\Base::getPostOrGetVariable( 'showvendor' );
	$hl = \EE\Controller\Base::getPostOrGetVariable( 'hl' );
	$batch = \EE\Controller\Base::getPostOrGetVariable( 'batch' );
	$editid = \EE\Controller\Base::getPostOrGetVariable( 'menu_itemid' );
	if ( $editid == '' ) {
		$editid = \EE\Controller\Base::getPostOrGetVariable( 'editid' );
		$item_price = \EE\Controller\Base::getPostOrGetVariable( 'item_price' );
	}
	if ( $curmenu == '' ) {
		$curmenu = $page_business->cafe_menu;
	}
	\EE\Controller\Base::setCookie( 'curmenu', $curmenu );
	$editid = abs( $editid );
	
	$pageVariables = new \EE\ArrayObject();
	$pageVariables->businessId = $pageBusiness->getBusinessId();
	$pageVariables->companyId = $pageBusiness->getCompanyId();
	$pageVariables->editId = $editid;
	$pageVariables->menuItemNewId = \EE\Controller\Base::getPostOrGetVariable( 'minid' );
	$pageVariables->batch = $batch;
	$pageVariables->itemPrice = $item_price;
	$pageVariables->currentMenu = $curmenu;
	
	$pageVariables->editBusCaterMenu = false;
	$pageVariables->editBusInventor3 = false;
	
	\EE\Controller\Base::setCookie( 'viewcook', $view );
	\EE\Controller\Base::setCookie( 'date1cook', $date1 );
	\EE\Controller\Base::setCookie( 'date2cook', $date2 );
	
	$menuTypes = \EE\Model\Menu\Type::getAllByBusinessId( $pageBusiness->getBusinessId(), $class = '\EE\ArrayObject' );
	// @TODO clean this up - temp for presentation
	if ( !$menuTypes ) {
		$quickServiceMenu = new \EE\ArrayObject( array(
			'menu_typeid' => QUICK_SERVICE_MENU,
			'menu_typename' => 'Quick Service Menu',
			'companyid' => $pageBusiness->getCompanyId(),
			'businessid' => 0,
			'deleted' => 0,
			'type' => 0,
			'parent' => 0,
			'static_menu' => 0,
		) );
		$menuTypes = array( $quickServiceMenu );
	}
#	echo '$menuTypes = ';
#	var_dump( $menuTypes );
	$serveryStations = \EE\Model\Menu\ServeryStation::getAll( 'station_name', false, 'EE\ArrayObject' );
	$copyFromMenuItems = \EE\Model\Menu\Item::getByBusinessId(
		$pageBusiness->getBusinessId()
		,'\EE\ArrayObject'
		,$orderBy = array(
			'mi.menu_typeid DESC',
			'mi.item_name',
		)
		,$arrayGroupWith = 'menu_typeid'
	);
	if ( !$copyFromMenuItems ) {
		$copyFromMenuItems = array();
	}
	
	$genericLinkQuery = array(
		'cid' => $pageVariables->companyId,
		'bid' => $pageVariables->businessId,
		'curmenu' => null,
	);
	$busInventor4Query = array(
		'cid' => $pageVariables->companyId,
		'bid' => $pageVariables->businessId,
		'curmenu' => $pageVariables->currentMenu,
		'editid' => $pageVariables->editId,
		'minid' => $pageVariables->menuItemNewId,
		'item_price' => $pageVariables->itemPrice,
		'batch' => null,
	);
	
	///get logo
/*
 * @TODO WTF are we doing yet another query against the company table when we've already done the query & the result is cached??
 * @TODO why is $query indented with a tab, but $result is indented with spaces?
	$query = "SELECT * FROM company WHERE companyid = $companyid";
    $result = Treat_DB_ProxyOld::query($query);

	$com_logo=@Treat_DB_ProxyOld::mysql_result($result,0,"logo");

	if($com_logo == ""){$com_logo = "talogo.gif";}
*/
	if ( !$pageBusiness->logo ) {
		$pageBusiness->logo = "talogo.gif";
	}
	

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
	<head>
		<title>Essential Elements :: Recipes </title>
		<link rel="stylesheet" type="text/css" href="/assets/css/style.css" />
		<link rel="stylesheet" type="text/css" href="/assets/css/ta/generic.css" />
		<link rel="stylesheet" type="text/css" href="/assets/css/ta/inventory.css" />
		<link rel="stylesheet" type="text/css" href="/assets/css/ta/businventor4.css" />

<script language="JavaScript" type="text/javascript" src="/ta/source/org/tool-man/core.js"></script>
<script language="JavaScript" type="text/javascript" src="/ta/source/org/tool-man/events.js"></script>
<script language="JavaScript" type="text/javascript" src="/ta/source/org/tool-man/css.js"></script>
<script language="JavaScript" type="text/javascript" src="/ta/source/org/tool-man/coordinates.js"></script>
<script language="JavaScript" type="text/javascript" src="/ta/source/org/tool-man/drag.js"></script>
<script language="JavaScript" type="text/javascript" src="/ta/source/org/tool-man/dragsort.js"></script>
<script language="JavaScript" type="text/javascript" src="/ta/source/org/tool-man/cookies.js"></script>

		<script type="text/javascript" src="/assets/js/prototype.js"></script>
		<script type="text/javascript" src="/assets/js/scriptaculous.js"></script>
		<script type="text/javascript" src="/assets/js/prototype-eef.js"></script>
		<script type="text/javascript" src="/assets/js/ta/businventor4.js"></script>
	</head>

	<body>
	
		<center id="page_wrapper">
			<table cellspacing="0" cellpadding="0" border="0" width="90%">
				<tr>
					<td colspan="2">
						<a href="/ta/businesstrack.php"
							><img src="/assets/images/logo.jpg" border="0" height="43" width="205" alt="logo"
						/></a>
						<p>
					</td>
				</tr>
				<tr bgcolor="#ccccff">
					<td width="1%">
						<img src="/assets/images/logo/<?php echo $pageBusiness->logo; ?>"/>
					</td>
					<td>
						<font size="4">
							<b><?php echo $page_business->businessname, ' #', $page_business->unit; ?></b>
						</font>
						<br/>
						<?php echo $page_business->street; ?>
						<br/>
						<?php echo $page_business->city, ', ', $page_business->state, ' ', $page_business->zip; ?>
						<br/>
						<?php echo $page_business->phone; ?>
					</td>
					<td align="right" valign="top">
						<br/>
						<form action="/ta/editbus.php" method="post" name="store">
							<input type="hidden" name="username" value="<?php echo $GLOBALS['user']; ?>" />
							<input type="hidden" name="password" value="<?php echo $GLOBALS['pass']; ?>" />
							<input type="hidden" name="businessid" value="<?php echo $pageBusiness->getBusinessId(); ?>" />
							<input type="hidden" name="companyid" value="<?php echo $pageBusiness->getCompanyId(); ?>" />
							<input type="submit" value=" Store Setup " />
						</form>
					</td>
				</tr>

				<tr>
					<td colspan="3" class="header_nav">
						<div>
<?php
	echo $pageUser->getBusinessMenu(
		$pageBusiness->getBusinessId()
		,$pageBusiness->getCompanyId()
		,'businventor'
	);
?>

						</div>
					</td>
				</tr>

			</table>
			<p>
<?php
	if ( 1 == $pageUser->bus_inventor4 ) {
		$viewonly = 'DISABLED';
	}
	else {
		$viewonly = '';
	}
	
	if ( $GLOBALS['security_level'] > 0 ) {
?>

			<table width="90%">
				<tr>
					<td>
						<form action="/ta/businventor4.php" method="post">
							<select name="gobus" onChange="changePage(this.form.gobus)">
<?php

		$business_list = \EE\Model\Business::getListBySecurityLevel(
			$pageUser->security_level
			,$pageUser->loginid
			,$pageBusiness->getCompanyId()
			,$pageUser->getBusinessIds()
			,$pageBusiness->getBusinessId()
		);
		
		foreach ( $business_list as $business ) {
?>

								<option value="/ta/businventor4.php?<?php
									echo http_build_query( array_merge( $genericLinkQuery, array(
										'bid' => $business->businessid,
									) ), null, '&amp;' );
								?>"<?php
									echo $business->selected;
								?>><?php
									echo htmlentities( $business->businessname );
								?></option>
<?php
		}
?>

							</select>
						</form>
					</td>
					<td></td>
				</tr>
			</table>
			<p>
<?php
	}

?>
<?php
if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
	echo $_SESSION->getFlashDebug( "\t\t\t<pre class=\"flash_debug\">%s</pre>\n" );
}
echo $_SESSION->getFlashError(   "\t\t\t<div class=\"flash_error\">%s</div>\n"   );
echo $_SESSION->getFlashMessage( "\t\t\t<div class=\"flash_message\">%s</div>\n" );
?>
<?php


$menu_item = null;
if ( !$batch ) {
	if ( $pageVariables->editId ) {
		$menu_item = \EE\Model\Menu\Item::getMenuItemById( $pageVariables->editId );
	}
	elseif ( $pageVariables->menuItemNewId ) {
		$menu_item = \EE\Model\Menu\ItemNew::getById(
			$pageVariables->menuItemNewId
#			,'\EE\Model\Nutrition\MenuItem\NewObject'
			,'\EE\Model\Nutrition\MenuItem\Object'
		);
	}
}
else {
	$menu_item = \EE\Model\Inventory\Item::getById( $editid, '\EE\Model\Nutrition\RecipeItemObject' );
	$rec_num = $menu_item->rec_num ? $menu_item->rec_num : 1;
	$menu_item->setServingSize( $rec_num );
	$menu_item->serving_size = $rec_num;
}
if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
	echo '<pre>';
	echo '$editid = ';
	var_dump( $pageVariables->editId );
	echo '$minid = ';
	var_dump( $pageVariables->menuItemNewId );
	echo '$menu_item = ';
	var_dump( $menu_item );
	echo '</pre>';
}
if ( !$menu_item ) {
	$menu_item = new \EE\Model\Nutrition\MenuItem\Object();
}
$menu_item->setNumberOfServings( $menu_item->serving_size );

$recipe_items = $menu_item->getRecipeItems();
$nutrition = $menu_item->getNutritionValues();


	/////////////////////////RECIPES////////////////
/*
 * IE has issues with the vertical-align CSS property if there are any spaces
 * within the table cell - that is the reason for the somewhat odd structure.
 */
		//<editor-fold defaultstate="collapsed" desc="inventory_tabs table">
?>

			<table width="90%" cellspacing="0" cellpadding="0" class="inventory_tabs">
				<tr>
					<td width="1%" class="left_corner"
						><img src="/assets/images/corners/leftcrn2.jpg" height="6" width="6" alt=""
					/></td>
					<td width="15%" class="label">
						<a href="/ta/businventor.php?<?php
							echo http_build_query( $genericLinkQuery, null, '&amp;' );
						?>">Inventory $'s</a>
					</td>
					<td width="1%" class="right_corner"
						><img src="/assets/images/corners/rightcrn2.jpg" height="6" width="6" alt=""
					/></td>
					
					<td width="1%"></td>
					
					<td width="1%" class="left_corner"
						><img src="/assets/images/corners/leftcrn2.jpg" height="6" width="6" alt=""
					/></td>
					<td width="15%" class="label">
						<a href="/ta/businventor2.php?<?php
							echo http_build_query( $genericLinkQuery, null, '&amp;' );
						?>">Inventory Count</a>
					</td>
					<td width="1%" class="right_corner"
						><img src="/assets/images/corners/rightcrn2.jpg" height="6" width="6" alt=""
					/></td>
					
					<td width="1%"></td>
					
					<td width="1%" class="left_corner"
						><img src="/assets/images/corners/leftcrn2.jpg" height="6" width="6" alt=""
					/></td>
					<td width="15%" class="label">
						<a href="/ta/businventor3.php?<?php
							echo http_build_query( $genericLinkQuery, null, '&amp;' );
						?>">Inventory Setup</a>
					</td>
					<td width="1%" class="right_corner"
						><img src="/assets/images/corners/rightcrn2.jpg" height="6" width="6" alt=""
					/></td>
					
					<td width="1%"></td>
					
					<td width="1%" class="current_tab left_corner"
						><img src="/assets/images/corners/leftcrn.jpg" height="6" width="6" alt=""
					/></td>
					<td width="15%" class="current_tab label">
						<a href="/ta/businventor4.php?<?php
							echo http_build_query( array_merge( $genericLinkQuery, array(
								'curmenu' => $pageVariables->currentMenu,
							) ), null, '&amp;' );
						?>">Recipes</a>
					</td>
					<td width="1%" class="current_tab right_corner"
						><img src="/assets/images/corners/rightcrn.jpg" height="6" width="6" alt=""
					/></td>
					
					<td width="1%"></td>
					
					<td width="1%" class="left_corner"
						><img src="/assets/images/corners/leftcrn2.jpg" height="6" width="6" alt=""
					/></td>
					<td width="15%" class="label">
						<a href="/ta/businventor5.php?<?php
							echo http_build_query( $genericLinkQuery, null, '&amp;' );
						?>">Vendors/PO's</a>
					</td>
					<td width="1%" class="right_corner"
						><img src="/assets/images/corners/rightcrn2.jpg" height="6" width="6" alt=""
					/></td>
					
					<td width="1%"></td>
					<td width="25%"></td>
				</tr>


				<tr height="2">
					<td colspan="4"></td>
					<td colspan="4"></td>
					<td colspan="4"></td>
					<td colspan="3" bgcolor="#E8E7E7"></td>
					<td></td>
					<td colspan="4"></td>
					<td colspan="1"></td>
				</tr>

			</table>
<?php
		//</editor-fold>
?>
			
			
			<table width="90%" cellspacing="0" cellpadding="0">

				<tr>
					<td colspan="2" bgcolor="#E8E7E7">
						<form action="" method="get">
<?php
/*
						<form action="/ta/placemenu.php?<?php
							echo http_build_query( array(
								'cid' => $pageBusiness->getCompanyId(),
								'bid' => $pageBusiness->getBusinessId(),
							), null, '&amp;' );
						?>" method="post">
*/
?>
							<input type="hidden" name="cid" value="<?php echo $pageBusiness->getCompanyId(); ?>" />
							<input type="hidden" name="bid" value="<?php echo $pageBusiness->getBusinessId(); ?>" />
							<input type="hidden" name="direct" value="4" />
							<b>Recipes for</b>
							<select name="curmenu">
								<option></option>
<?php
	foreach ( $menuTypes as $menuType ) {
?>
								<option value="<?php
									echo $menuType->menu_typeid;
								?>"<?php
									if ( $menuType->menu_typeid == $pageVariables->currentMenu ) {
										echo ' selected="selected"';
									}
								?>><?php echo $menuType->menu_typename; ?></option>
<?php
	}
?>
							</select>
							<input type="submit" value="GO" />
						</form>
					</td>
					<td class="recipe_extras">
						<div class="recipe_extras">
<?php
	if ( $page_business->use_nutrition ) {
?>

							<a href="/ta/nutrition/recipe/conversion?bid=<?php
								echo $pageBusiness->getBusinessId();
							?>"
								target="_blank"
							>Recipe Conversion</a>
<?php
	}
	else {
?>

							<a href="/ta/recipecheck.php?curmenu=<?php
								echo $curmenu;
							?>&amp;cid=<?php
								echo $pageBusiness->getCompanyId();
							?>&amp;bid=<?php
								echo $pageBusiness->getBusinessId();
							?>">Recipe Checker</a>
<?php
	}
?>
							:: 
							<a href="/ta/menuproduct.php?curmenu=<?php
								echo $curmenu;
							?>&amp;bid=<?php
								echo $pageBusiness->getBusinessId();
							?>"
								target="_blank"
							>Production</a>
							:: 
							<a href="/ta/menuprofit.php?curmenu=<?php
								echo $curmenu;
							?>&amp;bid=<?php
								echo $pageBusiness->getBusinessId();
							?>"
								target="_blank"
							>Menu Profitability Report</a>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="3" height="1" bgcolor="black"></td>
				</tr>

				<tr>
					<td colspan="3" bgcolor="#E8E7E7">
						<form action="/ta/businventor4.php?<?php
							echo http_build_query( $genericLinkQuery, null, '&amp;' );
						?>" method="post">
							<input type="hidden" name="companyid" value="<?php echo $pageBusiness->getCompanyId(); ?>" />
							<input type="hidden" name="businessid" value="<?php echo $pageBusiness->getBusinessId(); ?>" />
<?php
	if ( QUICK_SERVICE_MENU == $pageVariables->currentMenu ) {
		$menu_items_list = \EE\Model\Menu\ItemNew::getMenuItemListWithGroupName(
			$pageBusiness->getBusinessId()
			,$page_business->contract
			,$curmenu
		);
	}
	else {
		$menu_items_list = \EE\Model\Menu\Item::getMenuItemListWithGroupName(
			$pageBusiness->getBusinessId()
			,$page_business->contract
			,$curmenu
		);
	}
	$lastgroupid = -1;
	$selected_set = false;
	$previtem = $nextitem = 0;
	$prevMitn = $nextMitn = 0;
?>
							<select name="menu_itemid" onChange="changePage(this.form.menu_itemid)">
<?php
	foreach ( $menu_items_list as $item ) {
		if ( $item->groupid != $lastgroupid ) {
?>

								<option value="/ta/businventor4.php?<?php
									echo http_build_query( $genericLinkQuery, null, '&amp;' );
								?>"
									style="background-color:yellow;"
								>===============================<?php
									echo htmlentities( $item->groupname ); ?></option>
<?php
		}
		if ( $item->menu_item_id && $pageVariables->editId == $item->menu_item_id ) {
			$pageVariables->editBusCaterMenu = true;
			$item->selected = ' selected="selected"';
			# @TODO I think this should be pulled from the page product, not this loop
			$rec_sizename = "<i>Serving Size:</i> {$item->sizename}";
			$selected_set = true;
		}
		elseif ( $item->min_id && $pageVariables->menuItemNewId == $item->min_id ) {
			$pageVariables->editBusCaterMenu = true;
			$item->selected = ' selected="selected"';
			$selected_set = true;
		}
		else {
			$item->selected = '';
			if ( !$selected_set && !( $nextitem || $nextMitn ) ) {
				$previtem = $item->menu_item_id;
				$prevMitn = $item->min_id;
			}
			if ( $selected_set && !( $nextitem || $nextMitn ) ) {
				$nextitem = $item->menu_item_id;
				$nextMitn = $item->min_id;
			}
		}
?>

								<option value="/ta/businventor4.php?<?php
									echo http_build_query( array_merge( $busInventor4Query, array(
										'editid' => $item->menu_item_id,
										'minid' => $item->min_id,
										'item_price' => $item->price,
									) ), null, '&amp;' );
								?>"<?php
									echo $item->selected;
								?>><?php echo htmlentities( $item->item_name ); ?></option>
<?php
		$lastgroupid = $item->groupid;
	}

	////////////////////////////
	////////BATCH RECIPES///////
	////////////////////////////

?>

								<option value="/ta/businventor4.php?<?php
									echo http_build_query( $genericLinkQuery, null, '&amp;' );
								?>"
									style="background-color:yellow;"
								>===============================Batch Recipes</option>
<?php
	$batch_items = \EE\Model\Inventory\Item::getBatchItemsByBusinessId( $businessid );
	foreach( $batch_items as $item ) {
		if ( $pageVariables->editId == $item->inv_itemid && 1 == $pageVariables->batch ) {
			$pageVariables->editBusInventor3 = true;
			$item->selected = ' selected="selected"';
		}
		else {
			$item->selected = '';
		}
?>

								<option value="/ta/businventor4.php?<?php
									echo http_build_query( array_merge( $busInventor4Query, array(
										'editid' => $item->inv_itemid,
										'item_price' => $item->price,
										'batch' => 1,
									) ), null, '&amp;' );
								?>"<?php
									echo $item->selected;
								?>><?php
									echo $item->item_name;
								?></option>
<?php
	}

	////////////////////////////
	////////END BATCHES/////////
	////////////////////////////

?>

							</select>


<?php
		if ( 1 != $pageUser->bus_inventor4 ) {
			if ( $pageVariables->editBusCaterMenu ) {
?>
							<a href="/ta/buscatermenu.php?<?php
								echo http_build_query( array_merge( $genericLinkQuery, array(
									'view1' => 'edit',
									'mid' => $pageVariables->editId,
								) ), null, '&amp;' );
							?>#item">
								<img src="/assets/images/edit.gif" height="16" width="16" border="0" alt="Edit Menu Item" />
							</a>
<?php
			}
			elseif ( $pageVariables->editBusInventor3 ) {
?>
							<a href="/ta/businventor3.php?<?php
								echo http_build_query( array_merge( $genericLinkQuery, array(
									'view2' => 'editinv',
									'editid' => $pageVariables->editId,
								) ), null, '&amp;' );
							?>#invitem">
								<img src="/assets/images/edit.gif" height="16" width="16" border="0" alt="Edit Batch Item" />
							</a>
<?php
			}
		}

?>
<?php	if ( $selected_set && ( $previtem > 0 || $prevMitn > 0 ) ) { ?>
							<font size="1">[</font>
							<a href="/ta/businventor4.php?<?php
								echo http_build_query( array_merge( $busInventor4Query, array(
									'editid' => $previtem,
									'minid' => $prevMitn,
									'item_price' => null,
								) ), null, '&amp;' );
							?>" style="<?php echo $style; ?>">
								<font size="1" color="blue">PREV</font>
							</a>
							<font size="1">]</font>
<?php	} ?> 
<?php	if ( $selected_set && ( $nextitem > 0 || $nextMitn > 0 ) ) { ?>
							<font size="1">
								[<a href="/ta/businventor4.php?<?php
									echo http_build_query( array_merge( $busInventor4Query, array(
										'editid' => $nextitem,
										'minid' => $nextMitn,
										'item_price' => null,
									) ), null, '&amp;' );
								?>" style="<?php echo $style; ?>"
									><font size="1" color="blue">NEXT</font
								></a>]
							</font>
<?php	} ?> 
							<br>
<?php	if ( $rec_sizename == "<i>Serving Size:</i> " && $page_business->contract == 1 ): ?>
							<i>Serving Size:</i> 
							<img src="/ta/error.gif" height="16" width="20" alt="NO SERVING SIZE DEFINED" />
<?php	elseif ( $editid != '' || !$pageVariables->menuItemNewId ): ?>
							<i>Serves: <?php echo $menu_item->serving_size; ?></i>
<?php	endif; ?>

						</form>
					</td>
				</tr>
<?php

$recipe_items = $menu_item->getRecipeItems();
?>
				<tr>
					<td colspan="3" bgcolor="#E8E7E7">
						<table id="recipe" cellspacing="0" cellpadding="0" border="0" width="95%">
							<thead>
<?php

	if ( count( $recipe_items ) ) {
?>
								<tr>
									<td class="counter">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td class="quantity_number" id="rec_num<?php echo $viewonly; ?>"></td>
									<td class="quantity_size" id="rec_size<?php echo $viewonly; ?>">&nbsp; <i>Qty</i></td>
									<td class="item_name"><i>Item</i></td>
									<td class="show_size"></td>
									<td class="show_size">&nbsp;</td>
									<td class="new_price"></td>
									<td></td>
								</tr>
<?php
	}
	else {
?>
								<tr>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td width="15%"></td>
									<td>&nbsp; </td>
									<td></td>
									<td></td>
									<td>&nbsp;</td>
									<td align="right"></td>
									<td></td>
								</tr>
<?php
	}
?>
							</thead>
							<tbody>
<?php
	$counter = 1;
	foreach ( $recipe_items as $item ) {
		if ( $hl == $item->inv_itemid ) {
			$class = 'highlight';
			$mycolor = "#00ffff";
		}
		else {
			$class = '';
			$mycolor = "white";
		}
?>

								<tr id="id_<?php echo $item->recipeid; ?>" class="<?php echo $class; ?>" itemID="<?php echo $counter; ?>">
									<td class="counter">&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $counter; ?>.</td>
									<td class="quantity_number<?php echo $viewonly; ?>">
										<?php echo $item->r_rec_num; ?> 
									</td>
									<td class="quantity_size<?php echo $viewonly; ?>">
										<?php echo \EE\Model\Nutrition\Sizes::getCachedSize( $item->calc_inv_rec_size ); ?> 
									</td>
									<td class="item_name">
										<?php echo $item->item_name; ?> 
										<sup>
											<font size="2">
												<?php echo $item->item_code, $item->getShowNutr(); ?> 
											</font>
										</sup>
									</td>
									<td class="show_size">&nbsp;</td>
									<td class="show_size">&nbsp;</td>
									<td class="new_price"><?php
										echo money_format( '%.2n', $item->getNewPrice() );
									?></td>
									<td class="show_edit_delete">
<?php
//	TODO change this form to post to a confirm delete page
//	TODO have jquery provide layered functionality to skip confirm delete page & post directly to delete page
?>

										<form id="delete_item"
											class="delete_item"
											action="/nutrition/recipe/delete"
											method="post"
											onsubmit="return delconfirm2()"
										>
											<input type="hidden" name="cid" value="<?php echo $pageBusiness->getCompanyId(); ?>" />
											<input type="hidden" name="bid" value="<?php echo $pageBusiness->getBusinessId(); ?>" />
											<input type="hidden" name="edit" value="2" />
											<input type="hidden" name="editid" value="<?php echo $editid; ?>" />
											<input type="hidden" name="minid" value="<?php echo $pageVariables->menuItemNewId; ?>" />
											<input type="hidden" name="recipeid" value="<?php echo $item->recipeid; ?>" />
											<input type="hidden" name="showvendor" value="<?php echo $showvendor; ?>" />
											<input type="hidden" name="batch" value="<?php echo $batch; ?>" />
											<input type="hidden" name="item_price" value="<?php echo getPostOrGetVariable( 'item_price' ); ?>" />
											<button class="delete_item" type="submit" <?php echo $viewonly; ?>>
												<img src="/ta/delete.gif" height="16" width="16" border="0"
													alt="DELETE ITEM"
												/>
											</button>
										</form>
										<a href="/ta/businventor3.php?cid=<?php
													echo $pageBusiness->getCompanyId();
												?>&amp;bid=<?php
													echo $pageBusiness->getBusinessId();
												?>&amp;view2=editinv&editid=<?php
													echo $item->inv_itemid;
												?>&amp;batch=<?php
													echo $batch
												?>#invitems"
											><img src="/ta/edit.gif" height="16" width="16" border="0"
												alt="Edit Inventory Item"
										/></a>
									</td>
								</tr>
<?php
		$counter++;
	}
	if ( $counter > 1 ) {
?>

								<tr class="total_recipe_cost">
									<td colspan="5"></td>
									<td class="recipe_label">
										Cost:
									</td>
									<td class="recipe_cost" id="ttlCost"><?php
										echo money_format( '%.2n', $menu_item->getCost() );
									?></td>
									<td class="recipe_rest" id="ttlRest"
										>&nbsp;&nbsp;<span class="dispVal"
										>Price:</span>&nbsp;<?php
											echo money_format( '%.2n', $menu_item->getRetailPriceForServingSize() );
										?>&nbsp;&nbsp;<span class="dispVal"
										>Profit:</span>&nbsp;<?php
											echo money_format( '%.2n', $menu_item->getRetailProfit() );
										?>&nbsp;&nbsp;<span class="dispVal"
										>FC%:</span>&nbsp;<?php
											echo $menu_item->getFcPercent();
									?>%</td>
								</tr>
<?php
	}

	if ( $counter >= 2 && $menu_item->serving_size > 1 ) {
?>

								<tr class="total_perserv_cost">
									<td colspan="5"></td>
									<td class="perserv_label">
										Per Serv:
									</td>
									<td class="perserv_cost" id="sCost"><?php
										echo money_format( '%.2n', $menu_item->getTotalPricePerServing() );
									?></td>
									<td class="perserv_rest" id="sRest"
										>&nbsp;&nbsp;<span class="dispVal"
										>Price:</span>&nbsp;<?php
											echo money_format( '%.2n', $menu_item->getUnitPrice() );
										?>&nbsp;&nbsp;<span class="dispVal"
										>Profit:</span>&nbsp;<?php
											echo money_format( '%.2n', $menu_item->getUnitProfit() );
									?>&nbsp;&nbsp;</td>
								</tr>
<?php
	}
?>

							</tbody>
						</table>



						<br/>
						<table cellspacing="0" cellpadding="0" border="0">
<?php
	if ( $editid > 0 || $pageVariables->menuItemNewId > 0 ) {
?>
							<tr bgcolor="yellow">
								<td bgcolor="#E8E7E7"></td>
								<td colspan="6">&nbsp;</td>
							</tr>

							<tr bgcolor="yellow">
								<td bgcolor="#E8E7E7" width="5%">
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								</td>
								<td colspan="5">
									<form
										id="addItemForm"
										action="/ta/editrecipe.php"
										method="post"
										onSubmit="return disableForm(this);"
										style="margin: 0; padding: 0;"
									>
										<input type="hidden" name="showvendor" value="<?php echo $showvendor; ?>" />
										<input type="hidden" name="editid" value="<?php echo $editid; ?>" />
										<input type="hidden" name="minid" value="<?php echo $pageVariables->menuItemNewId; ?>" />
										<input type="hidden" name="companyid" value="<?php echo $pageBusiness->getCompanyId(); ?>" />
										<input type="hidden" name="businessid" value="<?php echo $pageBusiness->getBusinessId(); ?>" />
										<input type="hidden" name="edit" value="0" />
										<input type="hidden" name="rec_units" value="<?php echo $menu_item->true_unit; ?>" />
										<input type="text"   name="inv_itemid" id="autoBox" autocomplete="off" size="50" /> 
										<input type="text" name="rec_num" value="[Qty]"
											onfocus="if(this.value == '[Qty]') this.value = '1'; this.select();"
											size="3"
										/>&nbsp;<select style="display: none;"
											id="recSizeSelect" name="rec_order"
										></select>
<?php
		if ( $page_business->contract == 1 ) {
?>
										= <input type="text" name="srv_num" size="3" /> 
										<?php echo $showsize; ?> 
<?php
		}
		else {
?>
										<input type="hidden" name="srv_num" value="1">
<?php
		}

?>
										<input type="hidden" name="batch" value="<?php echo $batch; ?>" />
										<input type="submit" value="Add" <?php echo $viewonly; ?> />
									</form>
								</td>
								<td></td>
							</tr>
							<tr height="10">
								<td colspan="7" bgcolor="#E8E7E7"></td>
							</tr>
							<tr bgcolor="yellow">
								<td bgcolor="#E8E7E7"></td>
								<td colspan="6">
									<form action="/ta/copyrecipe.php" method="post" onSubmit="return disableForm(this);">
										<input type="hidden" name="batch" value="<?php echo $batch; ?>" />
										<input type="hidden" name="businessid" value="<?php echo $pageBusiness->getBusinessId(); ?>" />
										<input type="hidden" name="companyid" value="<?php echo $pageBusiness->getCompanyId(); ?>" />
										<input type="hidden" name="copyto" value="<?php echo $editid; ?>" />
										Copy Recipe From: 
										<select name="copyfrom">
											<option></option>
<?php
		foreach ( $copyFromMenuItems as $menuTypes ) {
			$newMenuType = true;
			foreach ( $menuTypes as $copyFromMenuItem ) {
				if ( $newMenuType ) {
?>
											<optgroup label="<?php echo $copyFromMenuItem->menu_typename; ?>">
<?php
					$newMenuType = false;
				}
?>
												<option value="<?php
													echo $copyFromMenuItem->menu_item_id;
												?>"><?php
													echo $copyFromMenuItem->item_name;
												?></option>
<?php
			}
?>
											</optgroup>
<?php
		}
?>
										</select>
										<input type="submit" value="Copy" <?php echo $viewonly; ?> onclick="return confirmcopy()" />
									</form>
								</td>
								<td colspan="3" bgcolor="#E8E7E7"></td>
							</tr>
							<tr>
								<td colspan="5">&nbsp;</td>
							</tr>
<?php
	}
?>
						</table>
					</td>
				</tr>
<?php
	if ( ( $editid != "" && !$pageVariables->menuItemNewId ) && $batch != 1 ) {
		if ( $menu_item->recipe_active == 1 ) {
			$showrecact = ' checked="checked"';
		}
		else {
			$showrecact = '';
		}
		
		if ( $menu_item->recipe_active_cust == 1 ) {
			$showrecact2 = ' checked="checked"';
		}
		else {
			$showrecact2 = '';
		}

		if ( $menu_item->keeper > 0 ) {
			$showkeeper = ' checked="checked"';
		}
		else {
			$showkeeper = '';
		}

?>
				<tr bgcolor="#E8E7E7">
					<td colspan="3">
						<form action="/nutrition/recipe/save_notes" method="post" onSubmit="return disableForm(this);">
							<input type="hidden" name="editid" value="<?php echo $pageVariables->editId; ?>" />
							<input type="hidden" name="companyid" value="<?php echo $pageBusiness->getCompanyId(); ?>" />
							<input type="hidden" name="businessid" value="<?php echo $pageBusiness->getBusinessId(); ?>" />
							<table>
								<tr>
									<td>
										&nbsp;&nbsp;&nbsp;<i>Instructions:</i>
										<br/>
										&nbsp;&nbsp;&nbsp;<textarea name="recipenotes" cols="70" rows="15"
										><?php echo $menu_item->recipe; ?></textarea>
									</td>
								</tr>
<?php

		///////////////////////HEART HEALTHY CHECK
		$showWhyHeartHealthy = $menu_item->doHeartHealthyCheck();
		if ( !$showWhyHeartHealthy ) {
			$heartHealthyClass = 'isHeartHealthy';
			$showIsHeartHealthy = 'Does';
		}
		else {
			$heartHealthyClass = 'isNotHeartHealthy';
			$showIsHeartHealthy = 'Does Not';
		}
		$showWhyHealthyLiving = $menu_item->doHealthyLivingCheck();
		if ( !$showWhyHealthyLiving ) {
			$healthyLivingClass = 'isHealthyLiving';
			$showIsHealthyLiving = 'Does';
		}
		else {
			$healthyLivingClass = 'isNotHealthyLiving';
			$showIsHealthyLiving = 'Does Not';
		}
?>
								<tr bgcolor="#E8E7E7">
									<td colspan="3">
										&nbsp;&nbsp;&nbsp;Heart Healthy Item<font color="blue">*</font>: 
										<input type="checkbox" name="heart_healthy" value="1"<?php
											if ( $menu_item->heart_healthy ) {
												echo ' checked="checked"';
											}
										?> />
									</td>
								</tr>

								<tr bgcolor="#E8E7E7">
									<td colspan="3">
										&nbsp;&nbsp;&nbsp;Healthy Living Program Item<font color="blue">**</font>:
										<input type="checkbox" name="healthy_living" value="1"<?php
											if ( $menu_item->healthy_living ) {
												echo ' checked="checked"';
											}
										?> />
									</td>
								</tr>

								<tr bgcolor="#E8E7E7">
									<td colspan="3">
										&nbsp;&nbsp;&nbsp;<input type="text" name="serving_size" value="<?php echo $menu_item->serving_size; ?>" size="3" /> 
										Servings @ 
										<input type="text" name="serving_size2" value="<?php echo $menu_item->serving_size2; ?>" size="3" /> 
										per serving, serving units: 
										<input type="text" name="unit" value="<?php echo $menu_item->unit; ?>" size="10" />
									</td>
								</tr>
								<tr bgcolor="#E8E7E7">
									<td colspan="3">
										&nbsp;&nbsp;&nbsp;Category:
										<select name="recipe_station">
											<option value="0">(None)</option>
<?php
		if ( $serveryStations ) {
			foreach ( $serveryStations as $serveryStation ) {
?>
											<option value="<?php
												echo $serveryStation->stationid;
											?>"<?php
												if ( $menu_item->recipe_station == $serveryStation->stationid ) {
													echo ' selected="selected"';
												}
											?>><?php
												echo $serveryStation->station_name;
											?></option>
<?php
			}
			
		}
?>
										</select>
									</td>
								</tr>
								<tr bgcolor="#E8E7E7">
									<td colspan="3">
											&nbsp;&nbsp;&nbsp;<input type="checkbox" name="keeper" value="1"<?php echo $showkeeper; ?> />
												<b>Request this be a Corporate Recipe</b>
									</td>
								</tr>
								<tr bgcolor="#E8E7E7">
									<td colspan="3">
											&nbsp;&nbsp;&nbsp;<input type="checkbox" name="recipe_active" value="1"<?php echo $showrecact; ?> /> 
											Disable Internal Viewing of Recipe
									</td>
								</tr>
								<tr bgcolor="#E8E7E7">
									<td colspan="3">
											&nbsp;&nbsp;&nbsp;<input type="checkbox" name="recipe_active_cust" value="1"<?php echo $showrecact2; ?> /> 
											Disable Customer Viewing of Recipe
											<br/>
											&nbsp;&nbsp;&nbsp;<input type="submit" value="Save" <?php echo $viewonly; ?>>
									</td>
								</tr>
							</table>
						</form>
					</td>
				</tr>
				<tr bgcolor="#E8E7E7">
					<td colspan="3">
						<div class="heartHealthy">
							&nbsp;&nbsp;
							*This recipe
							<span class="<?php echo $heartHealthyClass; ?>"><?php echo $showIsHeartHealthy; ?></span>
							Qualify as a Heart Healthy Item. 
							<span><?php
								echo implode( ', ', $showWhyHeartHealthy );
							?></span>
						</div>
						<div class="healthyLiving">
							&nbsp;&nbsp;
							**This recipe
							<span class="<?php echo $healthyLivingClass; ?>"><?php echo $showIsHealthyLiving; ?></span>
							Qualify as a Healthy Living Program Item. 
							<span><?php
								echo implode( ', ', $showWhyHealthyLiving );
							?></span>
						</div>
					</td>
				</tr>
				<tr bgcolor="#E8E7E7">
					<td colspan="3">
						&nbsp;&nbsp;
						<font size="2">
							(Nutrition) Asterisks on Ingredients: 
							<font color="red">*</font>1:1 conversion. 
							<font color="green">*</font>Able to convert. 
							<font color="blue">*</font>Could not convert, used default values.
						</font>
					</td>
				</tr>

				<tr bgcolor="#E8E7E7">
					<td colspan="3">
						<p>
							<br/>
							<center>
								<form action="/ta/printrecipe.php" target="_blank" method="get">
									<input type="hidden" name="editid" value="<?php echo $editid; ?>" />
									<input type="hidden" name="companyid" value="<?php echo $pageBusiness->getCompanyId(); ?>" />
									<input type="hidden" name="businessid" value="<?php echo $pageBusiness->getBusinessId(); ?>" />
									# of Servings: 
									<input type="text" name="numserv" value="<?php echo $menu_item->serving_size; ?>" size="3" /> 
									<input type="checkbox" value="1" name="show_cost" checked="checked" />Show Costs 
									<input type="checkbox" name="rollup" value="1" checked="checked" />Convert to Count Units 
									<input type="submit" value="Print Recipe">
								</form>
							</center>
					</td>
				</tr>
<?php
	}
?>
			</table>
		</center>

		<br/>
		<form action="/ta/businesstrack.php" method="get">
			<center>
				<input type="hidden" name="cid" value="<?php echo $pageBusiness->getCompanyId(); ?>" />
				<input type="submit" value="Return to Main Page">
			</center>
		</form>
<?php
	
	google_page_track();
?>
	</body>
</html>
<?php
}
else {
	if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
		echo 'failed login';
	}
}
?>