<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');

require("../lib/class.dashboard.php");
require("../lib/class.labor.php");
require("../lib/class.graph.php");
require("../lib/class.dates.php");
?>
<html>
    <head>
		<style type="text/css">
#sortable {}

#sortable div.item {
				margin: 5px 1px;
				float: left;
				width: 49.75%;
}

#sortable div.item.full {
	width: 100%;
}

#sortable div.item div.graph {
	height: 270px;
}

#sortable div.item div.graph.edit {
	height: 380px;
}
		</style>
        <script type="text/javascript" src="../javascripts/jquery-1.4.2.min.js"></script>
		<script type="text/javascript" src="../javascripts/jquery-ui-1.7.2.custom.min.js"></script>
        <script type="text/javascript">
function disableForm(theform) {
	if (document.all || document.getElementById) {
		for (i = 0; i < theform.length; i++) {
			var tempobj = theform.elements[i];
			if (tempobj.type.toLowerCase() == "submit" || tempobj.type.toLowerCase() == "reset")
			tempobj.disabled = true;
		}

	}
	else {
		alert("The form has been submitted.  But, since you're not using IE 4+ or NS 6, the submit button was not disabled on form submission.");
		return false;
	}
}
//  End -->

$(function () {

	// Load the graphs first
	$('#sortable .item').each(function() {
		loadGraph($(this).attr('id'));
	});

	// Now make them sortable
	$('#sortable').sortable({
		update: function(event, ui) {
			$.ajax({
				complete: function(req, status) {
					if (req.responseText == 0) alert('Failed to save layout of gauges');
				},
				data: {graphs: $('#sortable').sortable('toArray')},
				url: 'updateOrder.php'
			});
		},
		handle: "tr.ui-widget-header",
		revert: true
	});
	$('#sortable').disableSelection();
});

function deletegraph(viewid){
	$("#" + viewid).hide();
	$.post("ajax.graph.php", {viewid: viewid});
}

function editGraph(viewid) {
	$('#'+viewid+' div.graph').addClass('edit').html('<iframe src="editgraph.php?viewid='+viewid+'" width="100%" height="365" frameborder="0"></iframe>').append($('#close').html());
}

function loadGraph(graphId,show) {
	$('#'+graphId+' div.graph').html('<table width=100%><tr height=110><td>&nbsp;</td></tr></table><img src="../images/ajax-loader.gif" />');
	$.get('display_graph.php', {'viewid': graphId, 'show': show}, function(data) {
		$('#'+graphId+' div.graph').html(data);
	});
}
		</script>
    </head>

    <body style="margin: 0px; font-family: arial; font-size: 10px; font-weight: bold;">
        
<?php
$user = isset($_COOKIE["usercook"])?$_COOKIE["usercook"]:'';
$pass = isset($_COOKIE["passcook"])?$_COOKIE["passcook"]:'';

$query = "SELECT loginid FROM login WHERE username = '$user' AND password = '$pass'";
$result = DB_Proxy::query($query);
$num = mysql_numrows($result);

    if($num==1){
        $loginid = mysql_result($result,0,"loginid");

        $today=date("Y-m-d");

        $query = "SELECT * FROM db_view WHERE loginid = '$loginid' ORDER BY db_order";
        $result = DB_Proxy::query($query);

		echo '<div id="sortable">';
        while($r=mysql_fetch_array($result)){
            $viewid = $r["viewid"];
            $graph_name = $r["name"];
            $graph_type = $r["graph_type"];
            $size = $r["size"];
            $row = $r["db_row"];
            $column = $r["db_column"];

            $period = $r["period"];
            $period_num = $r["period_num"];
            $period_group = $r["period_group"];
            $current = $r["current"];

?>
	<div id="<?php echo $viewid; ?>" class="ui-state-default item<?php if ($size == 2) echo ' full'; ?>">
		<table width="100%" cellspacing="0" cellpadding="0" style="border: 2px solid #E8E7E7; font-size: 14px; font-weight: bold;" id="graph<?php echo $viewid; ?>" bgcolor="white">
			<tr class="ui-widget-header" bgcolor="#E8E7E7" valign="middle" style="cursor: move;">
				<td>&nbsp;<?php echo $graph_name; ?> </td>
				<td align="right">
					<a href="display_graph.php?viewid=<?php echo $viewid; ?>&ex=1" target="_blank"><img src="expand.gif" border=0 height=10 width=10 title="Expand"></a>
					<a href="display_graph.php?viewid=<?php echo $viewid; ?>&ex=-1"><img src="download.gif" border="0" height="10" width="10" title="Export"></a>
					<a href="javascript:void();" onclick="editGraph(<?php echo $viewid; ?>);"><img src="edit.gif" border="0" height="10" width="10" title="Settings"></a>
					<a href="javascript:void();" onclick="if(confirm('Are you sure you want to remove this?')) deletegraph(<?php echo $viewid; ?>);"><img src=close2.gif border=0 height=10 width=10 title='Delete'></a>
				</td>
			</tr>
			<tr height="125">
				<td colspan="2" width="100%">
					<div width="100%" class="graph" style="overflow:auto; text-align:center; vertical-align:middle;"></div>
				</td>
			</tr>
		</table>
	</div>
<?php
        }

        echo "</div>";
        ?>

        <a name="editme">
        <center>
            <table width="100%" cellspacing="0" cellpadding="0" style="border: 2px solid #E8E7E7;">
                <tr style="background-color:#E8E7E7;">
                    <td colspan="2" align="right">
                        &nbsp;<a href='editgraph.php' target='editgraph' onclick="$('#new_graph').show();$('#close_graph').show();" style="text-decoration: none;"> <font color="blue">New Graph</font></a>&nbsp;
                    </td>
                </tr>
                <tr id='new_graph' style="display:none;" valign="bottom">
                    <td>
                        <iframe src="editgraph.php" height="300" width="100%" frameborder="0" name="editgraph"></iframe>
                    </td>
                </tr>
                <tr id='close_graph' style="display:none;">
                    <td align="right">
                        <a href="index.php" onclick="$('#new_graph').hide();$('#close_graph').hide();" style="text-decoration: none;"> <font color="blue">Close</font></a>&nbsp;
                    </td>
                </tr>
            </table>
        </center>
        </a>
        <?php
    }
    ?>
		<div id="close" style="display: none;">
			<a href="javascript: void();" onclick="window.location.reload();">Close</a>
		</div>
    </body>
</html>
