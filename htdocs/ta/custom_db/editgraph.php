<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');

$viewid = Treat_Filter::inputGet('viewid');
$user = isset($_COOKIE["usercook"])?$_COOKIE["usercook"]:'';
$pass = isset($_COOKIE["passcook"])?$_COOKIE["passcook"]:'';

$query = "SELECT loginid FROM login WHERE username = '$user' AND password = '$pass'";
$result = DB_Proxy::query($query);
$num = mysql_numrows($result);

if($num==1){
?>

<html>
    <head>
        <?php
        //<script type="text/javascript" src="../javascripts/prototype.js"></script>
        //<script type="text/javascript" src="../javascripts/scriptaculous.js"></script>
        ?>
        <script type="text/javascript" src="../javascripts/jquery-1.4.1.min.js"></script>
        <script type="text/javascript" src="../javascripts/jquery.ui.core.js"></script>
        <script type="text/javascript" src="../javascripts/jquery.ui.widget.js"></script>
        <script type="text/javascript" src="../javascripts/jquery.ui.mouse.js"></script>
        <script type="text/javascript" src="../javascripts/jquery.ui.draggable.min.js"></script>
        <script type="text/javascript" src="../javascripts/jquery.ui.droppable.min.js"></script>

        <SCRIPT LANGUAGE="JavaScript">

        function disableForm(theform) {
        if (document.all || document.getElementById) {
            for (i = 0; i < theform.length; i++) {
                var tempobj = theform.elements[i];
                if (tempobj.type.toLowerCase() == "submit" || tempobj.type.toLowerCase() == "reset")
                tempobj.disabled = true;
            }

        }
        else {
            alert("The form has been submitted.  But, since you're not using IE 4+ or NS 6, the submit button was not disabled on form submission.");
            return false;
        }
        }
        //  End -->

        function changePage(newLoc)
        {
            nextPage = newLoc.options[newLoc.selectedIndex].value

            if (nextPage != "")
            {
                document.location.href = nextPage
            }
        }

        </script>

	<script type="text/javascript">
	$(function() {
		$("#draggable").draggable();
	});
	</script>
    </head>

    <?
        if ($viewid>0){
            $showsave="Save";

            $query = "SELECT * FROM db_view WHERE viewid = '$viewid'";
            $result = DB_Proxy::query($query);

            $name = mysql_result($result,0,"name");
            $graph_type = mysql_result($result,0,"graph_type");
            $size = mysql_result($result,0,"size");
            $db_order = mysql_result($result,0,"db_order");
            $period = mysql_result($result,0,"period");
            $period_num = mysql_result($result,0,"period_num");
            $period_group = mysql_result($result,0,"period_group");
            $current = mysql_result($result,0,"current");
            $week_ends = mysql_result($result,0,"week_ends");

            if($graph_type==2){$graph_type2="SELECTED";}
            elseif($graph_type==3){$graph_type3="SELECTED";}
            elseif($graph_type==4){$graph_type4="SELECTED";}
            elseif($graph_type==5){$graph_type5="SELECTED";}
            elseif($graph_type==6){$graph_type6="SELECTED";}

            if($size==2){$size_sel="SELECTED";}

            if($current==2){$cur_sel="SELECTED";}

            if($period==2){$period_sel2="SELECTED";}
            elseif($period==3){$period_sel3="SELECTED";}
            elseif($period==4){$period_sel4="SELECTED";}

            if($period_group==2){$period_group_sel2="SELECTED";}
            elseif($period_group==3){$period_group_sel3="SELECTED";}
            elseif($period_group==4){$period_group_sel4="SELECTED";}
        }
        else{$showsave="Add";}
    ?>

    <body style="margin: 0px; font-size: 8px; font-family: arial;"><center>
                    <p><br><table width=95% cellspacing=0 style="font-size: 12px;"><tr><td style="border: 2px solid #777777;" bgcolor="#777777">&nbsp;<font color="white"><b>Graph Details</b></font></td></tr><tr><td style="border: 2px solid #777777;">
                        <form action="add_graph.php" method="post" style="margin:0;padding:0;display:inline;" onSubmit="return disableForm(this);"><input type="hidden" name="viewid" value="<? echo $viewid ?>">
                            &nbsp;
                            Name: <input type="text" style="font-size: 12px;" size="25" name="graph_name" value="<? echo $name ?>">
                            <br>&nbsp;&nbsp;Style: <select name="graph_type" style="font-size: 12px;">
                                <option value="1">Line Graph</option>
                                <option value="2" <? echo $graph_type2 ?>>Aggregate Line Graph</option>
                                <option value="3" <? echo $graph_type3 ?>>Bar Graph</option>
                                <option value="4" <? echo $graph_type4 ?>>Spread Sheet</option>
                                <option value="5" <? echo $graph_type5 ?>>Pie Chart</option>
                            </select>
                            Size: <select name="size" style="font-size: 12px;">
                                <option value="1">Normal</option>
                                <option value="2" <? echo $size_sel ?>>Large</option>
                            </select><br>
                            &nbsp;
                            Date Range:
                            <select name="current" style="font-size: 12px;">
                                <option value="1">Current</option>
                                <option value="2" <? echo $cur_sel ?>>Last</option>
                            </select>
                            <input type="text" style="font-size: 12px;" size="5" name="period_num" value="<? echo $period_num ?>">
                            <select name="period" style="font-size: 12px;">
                                <option value="1">Day(s)</option>
                                <option value="2" <? echo $period_sel2 ?>>Week(s)</option>
                                <option value="3" <? echo $period_sel3 ?>>Month(s)</option>
                                <option value="4" <? echo $period_sel4 ?>>Year(s)</option>
                            </select>
                            Grouped By
                            <select name="period_group" style="font-size: 12px;">
                                <option value="1">Day</option>
                                <option value="2" <? echo $period_group_sel2 ?>>Week</option>
                                <option value="3" <? echo $period_group_sel3 ?>>Month</option>
                                <option value="4" <? echo $period_group_sel4 ?>>Year</option>
                            </select>

                            <?
                            if($period_group == 2){
                                if($week_ends == "Monday"){$selmon="SELECTED";}
                                elseif($week_ends == "Tuesday"){$seltue="SELECTED";}
                                elseif($week_ends == "Wednesday"){$selwed="SELECTED";}
                                elseif($week_ends == "Thursday"){$selthu="SELECTED";}
                                elseif($week_ends == "Friday"){$selfri="SELECTED";}
                                elseif($week_ends == "Saturday"){$selsat="SELECTED";}
                                elseif($week_ends == "Sunday"){$selsun="SELECTED";}

                                echo " <select name=week_ends><option value=\"Thursday\">Week Ends...</option>";
                                echo "<option value=\"Monday\" $selmon>Monday</option>";
                                echo "<option value=\"Tuesday\" $seltue>Tuesday</option>";
                                echo "<option value=\"Wednesday\" $selwed>Wednesday</option>";
                                echo "<option value=\"Thursday\" $selthu>Thursday</option>";
                                echo "<option value=\"Friday\" $selfri>Friday</option>";
                                echo "<option value=\"Saturday\" $selsat>Saturday</option>";
                                echo "<option value=\"Sunday\" $selsun>Sunday</option>";
                                echo "</select> ";
                            }
                            ?>
                           
                            <input type="submit" style="font-size: 12px;" value=" <?echo $showsave?> ">
                        </form>
                            </td>
                        </tr>
                    </table>
                        <?php
                        if($viewid>0){
                            echo "<p><table width=95% cellspacing=0 style=\"font-size: 12px;\"><tr><td style=\"border:2px solid #777777;\" bgcolor=#777777>&nbsp;<font color=white><b>What</b></font></td></tr><tr valign=top>";

                            /////add data
                            echo "<td style=\"border:2px solid #777777;\">";

                            echo "<center><table width=100% cellspacing=0 cellpadding=0 style=\"font-size: 12px;\"><tr valign=top><td width=50%>";
                            ////list what
                            $lasttype=0;
                            $query = "SELECT * FROM db_what WHERE viewid = '$viewid'";
                            $result = DB_Proxy::query($query);
                            $num=mysql_numrows($result);

                            if($num==0){echo "&nbsp;<i>Nothing to Display</i><br>";}
                            else{
                                echo "<ul>";
                                $count=1;
                                while($r=mysql_fetch_array($result)){
                                    $id = $r["id"];
                                    $type = $r["type"];
                                    $what = $r["what"];

                                    $showdelete = "<a href=display_what.php?viewid=$viewid&id=$id&del=1 onclick=\"return confirm('Are you sure you want to delete?');\"><img src=close2.gif height=10 width=10 border=0></a>";

                                    if($type==4){
                                        $query2 = "SELECT category_name FROM po_category WHERE categoryid = '$what'";
                                        $result2 = DB_Proxy::query($query2);

                                        $what = mysql_result($result2,0,"category_name");
                                    }
                                    elseif($type==5){
                                        $query2 = "SELECT category_name FROM po_category WHERE budget_type = '$what'";
                                        $result2 = DB_Proxy::query($query2);

                                        $what = mysql_result($result2,0,"category_name");
                                        $what .= " BGT";
                                    }
                                    elseif($type==6){
                                        if($what==1){$what="Item Sales";}
										elseif($what==2){$what="Hourly Sales";}
                                    }
                                    elseif($what=="sales"){$what="Sales";}
                                    elseif($what=="cos"){$what="COGS";}
                                    elseif($what=="labor"){$what="Labor";}
                                    elseif($what=="toe"){$what="TOE";}
                                    elseif($what=="pl"){$what="P&L";}
                                    elseif($what=="sales_bgt"){$what="Sales BGT";}
                                    elseif($what=="cos_bgt"){$what="COGS BGT";}
                                    elseif($what=="labor_bgt"){$what="Labor BGT";}
                                    elseif($what=="toe_bgt"){$what="TOE BGT";}
                                    elseif($what=="pl_bgt"){$what="P&L BGT";}
                                    elseif($what=="est_sales"){$what="EST: Sales";}
                                    elseif($what=="est_sales_bgt"){$what="EST: Sales BGT";}
                                    elseif($what=="est_lost"){$what="EST: Lost";}
                                    elseif($what=="est_lost_bgt"){$what="EST: Lost BGT";}
                                    elseif($what=="est_redlist"){$what="EST: Redlist";}

                                    echo "<li> [$count] $what $showdelete</li>";
                                    $count++;
                                    $lasttype=$type;
                                }
                                echo "</ul>";
                            }

                            echo "<form action=display_what.php method=post style=\"margin:0;padding:0;display:inline;font-size: 12px;font-family: arial;\" onSubmit=\"return disableForm(this);\"><input type=hidden name=viewid value=$viewid>&nbsp;Options:";
                            echo " <select name=display_what style=\"font-size: 12px;\">
                                    <option value=''>Please Choose...</option>";
                                    if($lasttype==1||$lasttype==0){
                                    echo "
                                    <optgroup label='Accounting'>
                                    <option value=1:sales>Sales</option>
                                    <option value=1:cos>Cost of Sales</option>
                                    <option value=1:labor>Labor</option>
                                    <option value=1:toe>TOE</option>
                                    <option value=1:pl>P&L</option>
                                    <option value=1:sales_bgt>Sales Budget</option>
                                    <option value=1:cos_bgt>Cost of Sales Budget</option>
                                    <option value=1:labor_bgt>Labor Budget</option>
                                    <option value=1:toe_bgt>TOE Budget</option>
                                    <option value=1:pl_bgt>P&L Budget</option>
                                    </optgroup>";
                                    }
                                    if($lasttype==3||$lasttype==0){
                                    echo "
                                    <optgroup label='Vending'>
                                    </optgroup>";
                                    }
                                    if($lasttype==2||$lasttype==0){
                                    echo "
                                    <optgroup label='ESalesTrack'>
                                    <option value=2:est_sales>Sales</option>
                                    <option value=2:est_sales_bgt>Sales Budget</option>
                                    <option value=2:est_lost>Lost</option>
                                    <option value=2:est_lost_bgt>Lost Budget</option>
                                    <option value=2:est_redlist>Red List</option>
                                    </optgroup>";
                                    }
                                    if($lasttype==4||$lasttype==5||$lasttype==0){
                                    echo "<optgroup label='CapEx'>";

                                    $query = "SELECT * FROM po_category ORDER BY category_name";
                                    $result = DB_Proxy::query($query);

                                    while($r=mysql_fetch_array($result)){
                                        $categoryid = $r["categoryid"];
                                        $category_name = $r["category_name"];
                                        $budget_type = $r["budget_type"];

                                        echo "<option value='4:$categoryid'>$category_name</option>";
                                        echo "<option value='5:$budget_type'>$category_name BGT</option>";
                                    }

                                    echo "</optgroup>";
                                    }
                                    if($lasttype==6||$lasttype==0){
                                    echo "
                                    <optgroup label='POS Menu'>
                                    <option value=6:1>Item Sales</option>
									<option value=6:2>Hourly Sales</option>
                                    </optgroup>";
                                    }

                                    echo "</select> <input type=submit style=\"font-size: 12px;\" value='Add'>";
                            echo "</form>";

                            echo "</td><td width=50%>";

                            /////custom fields
                            $query = "SELECT * FROM db_custom WHERE viewid = '$viewid'";
                            $result = DB_Proxy::query($query);
                            $num1=mysql_numrows($result);

                            if($num1==0){echo "&nbsp;<i>Nothing to Display</i><br>";}
                            else{
                                echo "<ul>";
                                $count=1;
                                while($r=mysql_fetch_array($result)){
                                    $id = $r["id"];
                                    $name = $r["name"];
                                    $custom_field = $r["custom_field"];
                                    $future_dates = $r["future_dates"];

                                    if($future_dates==1){$f_color="red";}
                                    else{$f_color="black";}

                                    $showdelete = "<a href=display_custom.php?viewid=$viewid&id=$id&del=1 onclick=\"return confirm('Are you sure you want to delete?');\"><img src=close2.gif height=10 width=10 border=0></a>";

                                    echo "<li><font color=$f_color>$name: $custom_field</font> $showdelete";

                                    $count++;
                                }
                                echo "</ul>";
                            }

                            echo "<form action=display_custom.php method=post style=\"margin:0;padding:0;display:inline;font-size: 12px;font-family: arial;\" onSubmit=\"return disableForm(this);\"><input type=hidden name=viewid value=$viewid>&nbsp;<input type=text name=custom_name size=15 value='Name'><br>&nbsp;<input type=text name=custom_field value='Formula' size=15><br>&nbsp;<input type=checkbox name=future_dates value=1>Disable Future Dates <input type=submit value='Add'></form>";

                            echo "</td></tr></table>";

                            echo "</td></tr></table><p>";

                            if($num>0){
                            ////add who
                            echo "<a name=bottom><table width=95% cellspacing=0 style=\"font-size: 12px;\"><tr><td style=\"border:2px solid #777777;\" bgcolor=#777777>&nbsp;<font color=white><b>Who</b></font></td></tr><tr valign=top><td style=\"border:2px solid #777777;\">";

                            ////list what
                            $query = "SELECT * FROM db_who WHERE viewid = '$viewid'";
                            $result = DB_Proxy::query($query);
                            $num=mysql_numrows($result);

                            if($num==0){echo "&nbsp;<i>Nothing to Display</i><br>";}
                            else{
                                echo "<ul>";
                                while($r=mysql_fetch_array($result)){
                                    $id = $r["id"];
                                    $type = $r["type"];
                                    $viewtype = $r["viewtype"];
                                    $viewbus = $r["viewbus"];
                                    $viewbus2 = $r["viewbus2"];

                                    $showdelete = "<a href=display_who.php?viewid=$viewid&id=$id&del=1 onclick=\"return confirm('Are you sure you want to delete?');\"><img src=close2.gif height=10 width=10 border=0></a>";
                                    
                                    ///get names
                                    if($type==1||$type==4||$type==5||$type==6){
                                        if($viewtype==1){$showwho="Treat America";}
                                        elseif($viewtype==2){
                                            $showwho="Type";

                                            $query3 = "SELECT type_name FROM company_type WHERE typeid = '$viewbus'";
                                            $result3 = DB_Proxy::query($query3);

                                            $viewname=mysql_result($result3,0,"type_name");
                                        }
                                        elseif($viewtype==3){
                                            $showwho="Company";

                                            $query3 = "SELECT companyname FROM company WHERE companyid = '$viewbus'";
                                            $result3 = DB_Proxy::query($query3);

                                            $viewname=mysql_result($result3,0,"companyname");
                                        }
                                        elseif($viewtype==4){
                                            $showwho="Region";

                                            $query3 = "SELECT division_name FROM division WHERE divisionid = '$viewbus'";
                                            $result3 = DB_Proxy::query($query3);

                                            $viewname=mysql_result($result3,0,"division_name");
                                        }
                                        elseif($viewtype==5){
                                            $showwho="District";

                                            $query3 = "SELECT districtname FROM district WHERE districtid = '$viewbus'";
                                            $result3 = DB_Proxy::query($query3);

                                            $viewname=mysql_result($result3,0,"districtname");
                                        }
                                        elseif($viewtype==6){
                                            $showwho="Business";

                                            $query3 = "SELECT businessname FROM business WHERE businessid = '$viewbus'";
                                            $result3 = DB_Proxy::query($query3);

                                            $viewname=mysql_result($result3,0,"businessname");
                                        }
                                    }
                                    elseif($type==2){
                                        if($viewtype==1){$showwho="Treat America";$viewname="";$viewname2="";}
                                        elseif($viewtype==2||$viewtype==3){
                                            $showwho="Type";

                                            $query3 = "SELECT name FROM est_accountowner_type WHERE id = '$viewbus'";
                                            $result3 = DB_Proxy::query($query3);

                                            $viewname=mysql_result($result3,0,"name");

                                            if($viewbus2==0){
                                                $viewname2="All Locations";
                                            }
                                            else{
                                                $query3 = "SELECT location FROM est_locations WHERE id = '$viewbus2'";
                                                $result3 = DB_Proxy::query($query3);

                                                $viewname2=mysql_result($result3,0,"location");
                                            }
                                        }
                                        elseif($viewtype==4){
                                            $showwho="Salesman";

                                            $query3 = "SELECT name FROM est_accountowner WHERE accountowner = '$viewbus'";
                                            $result3 = DB_Proxy::query($query3);

                                            $viewname=mysql_result($result3,0,"name");
                                        }
                                    }
                                    elseif($type==3){

                                    }
                                    
                                    ///display
                                    echo "<li>$showwho: $viewname $viewname2 $showdelete</li>";
                                }
                                echo "</ul>";
                            }

                            ////ACCOUNTING WHO
                            if($lasttype==1||$lasttype==4||$lasttype==5||$lasttype==6){
                                $display_viewtype=$_GET["display_viewtype"];
                                if($display_viewtype==1){$vtype1="SELECTED";}
                                elseif($display_viewtype==2){$vtype2="SELECTED";}
                                elseif($display_viewtype==3){$vtype3="SELECTED";}
                                elseif($display_viewtype==4){$vtype4="SELECTED";}
                                elseif($display_viewtype==5){$vtype5="SELECTED";}
                                elseif($display_viewtype==6){$vtype6="SELECTED";}

                                echo "<form action=display_who.php method=post style=\"margin:0;padding:0;display:inline;font-size: 12px;font-family: arial;\" onSubmit=\"return disableForm(this);\"><input type=hidden name=viewid value=$viewid>&nbsp;Options:";
                                echo " <select name=display_viewtype onChange='changePage(this.form.display_viewtype)' style=\"font-size: 12px;\">
                                   <option>Choose A Level...</option>
                                   <option value='editgraph.php?viewid=$viewid&display_viewtype=1' $vtype1>Treat America</option>
                                   <option value='editgraph.php?viewid=$viewid&display_viewtype=2' $vtype2>&nbsp;Type</option>
                                   <option value='editgraph.php?viewid=$viewid&display_viewtype=3' $vtype3>&nbsp;&nbsp;Company</option>
                                   <option value='editgraph.php?viewid=$viewid&display_viewtype=4' $vtype4>&nbsp;&nbsp;&nbsp;Region</option>
                                   <option value='editgraph.php?viewid=$viewid&display_viewtype=5' $vtype5>&nbsp;&nbsp;&nbsp;&nbsp;District</option>
                                   <option value='editgraph.php?viewid=$viewid&display_viewtype=6' $vtype6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Business</option>
                                    </select>";

                                if($display_viewtype==1){
                                    echo "<input type=hidden name=viewtype value=$display_viewtype>";
                                }
                                elseif($display_viewtype==2){
                                    echo "<input type=hidden name=viewtype value=$display_viewtype>";
                                    echo "<select name=display_viewbus style=\"font-size: 12px;\">";

                                    $query = "SELECT * FROM company_type ORDER BY type_name";
                                    $result = DB_Proxy::query($query);

                                    while($r=mysql_fetch_array($result)){
                                        $viewbus=$r["typeid"];
                                        $viewname=$r["type_name"];

                                        echo "<option value=$viewbus>$viewname</option>";
                                    }
                                    echo "</select>";
                                }
                                elseif($display_viewtype==3){
                                    echo "<input type=hidden name=viewtype value=$display_viewtype>";
                                    echo "<select name=display_viewbus style=\"font-size: 12px;\">";

                                    $query = "SELECT * FROM company ORDER BY companyname";
                                    $result = DB_Proxy::query($query);

                                    while($r=mysql_fetch_array($result)){
                                        $viewbus=$r["companyid"];
                                        $viewname=$r["companyname"];

                                        echo "<option value=$viewbus>$viewname</option>";
                                    }
                                    echo "</select>";
                                }
                                elseif($display_viewtype==4){
                                    echo "<input type=hidden name=viewtype value=$display_viewtype>";
                                    echo "<select name=display_viewbus style=\"font-size: 12px;\">";

                                    $query = "SELECT * FROM division ORDER BY division_name";
                                    $result = DB_Proxy::query($query);

                                    while($r=mysql_fetch_array($result)){
                                        $viewbus=$r["divisionid"];
                                        $viewname=$r["division_name"];

                                        echo "<option value=$viewbus>$viewname</option>";
                                    }
                                    echo "</select>";
                                }
                                elseif($display_viewtype==5){
                                    echo "<input type=hidden name=viewtype value=$display_viewtype>";
                                    echo "<select name=display_viewbus style=\"font-size: 12px;\">";

                                    $query = "SELECT * FROM district ORDER BY districtname";
                                    $result = DB_Proxy::query($query);

                                    while($r=mysql_fetch_array($result)){
                                        $viewbus=$r["districtid"];
                                        $viewname=$r["districtname"];

                                        echo "<option value=$viewbus>$viewname</option>";
                                    }
                                    echo "</select>";
                                }
                                elseif($display_viewtype==6){
                                    echo "<input type=hidden name=viewtype value=$display_viewtype>";
                                    echo "<select name=display_viewbus style=\"font-size: 12px;\">";

                                    $query = "SELECT * FROM business WHERE companyid != '2' ORDER BY businessname";
                                    $result = DB_Proxy::query($query);

                                    while($r=mysql_fetch_array($result)){
                                        $viewbus=$r["businessid"];
                                        $viewname=$r["businessname"];

                                        echo "<option value=$viewbus>$viewname</option>";
                                    }
                                    echo "</select>";
                                }

                                if($display_viewtype>0){echo " <input type=submit style=\"font-size: 12px;\" value='Add'>";}
                                echo "</form>";
                            }
                            ////ESalesTrack WHO
                            elseif($lasttype==2){
                                echo "<form action=display_who.php method=post style=\"margin:0;padding:0;display:inline;font-size: 12px;font-family: arial;\" onSubmit=\"return disableForm(this);\"><input type=hidden name=viewid value=$viewid>&nbsp;Options:";
                                echo "<select name=display_viewbus><option value=2:1:0>Treat America</option>";

                                $query3 = "SELECT * FROM est_accountowner_type";
                                $result3 = DB_Proxy::query($query3);

                                while($r3=mysql_fetch_array($result3)){
                                    $id = $r3["id"];
                                    $name = $r3["name"];

                                    echo "<option value=2:2:$id:0>&nbsp;&nbsp;&nbsp;$name</option>";

                                    $query4 = "SELECT * FROM est_locations";
                                    $result4 = DB_Proxy::query($query4);

                                    while($r4=mysql_fetch_array($result4)){
                                        $lid=$r4["id"];
                                        $location=$r4["location"];

                                        echo "<option value=2:3:$id:$lid>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$location</option>";

                                        $query5 = "SELECT * FROM est_accountowner WHERE type = '$id' ANd location = '$lid'";
                                        $result5 = DB_Proxy::query($query5);

                                        while($r5=mysql_fetch_array($result5)){
                                            $accountowner=$r5["accountowner"];
                                            $name=$r5["name"];

                                            echo "<option value=2:4:$accountowner>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$name</option>";
                                        }
                                    }
                                }

                                echo "</select> <input type=submit style=\"font-size: 12px;\" value='Add'>";
                                echo "</form>";
                            }

                            echo "</td>";

                            echo "</tr></table></a>";
                            }
                        }
                        ?>
        </center></body>
</html>
<?php
}
?>