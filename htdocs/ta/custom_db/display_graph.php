<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');
//DB_Proxy::$debug = true;

require("../lib/class.dashboard.php");
require("../lib/class.labor.php");
require("../lib/class.graph.php");
require("../lib/class.dates.php");
require("../lib/class.menu_items.php");

$user = isset($_COOKIE["usercook"])?$_COOKIE["usercook"]:'';
$pass = isset($_COOKIE["passcook"])?$_COOKIE["passcook"]:'';

$viewid = Treat_Filter::inputGet('viewid');
$ex = Treat_Filter::inputGet('ex');
$today = date("Y-m-d");

$show=$_GET["show"];
if($show==""){$show=0;}

//if($ex<1){$ex=0;}

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = DB_Proxy::query($query);
$num=mysql_numrows($result);

////export
if($ex==-1){
	header("Content-type: application/x-msdownload");
	header("Content-Disposition: attachment; filename=report.xls");
	header("Pragma: no-cache");
	header("Expires: 100");
}
 else{
	header("Content-Type: text/html; charset=utf-8");
}

if ($num != 1)
{
    echo "<center><h3>Failed</h3></center>";
}
else{
        $query = "SELECT * FROM db_view WHERE viewid = '$viewid'";
        $result = DB_Proxy::query($query);

        $r=mysql_fetch_array($result);

            $viewid = $r["viewid"];
            $graph_name = $r["name"];
            $graph_type = $r["graph_type"];
            $size = $r["size"];
            $row = $r["db_row"];
            $column = $r["db_column"];

            $period = $r["period"];
            $period_num = $r["period_num"];
            $period_group = $r["period_group"];
            $current = $r["current"];
            $week_ends = $r["week_ends"];

            /////export
            if($ex==-1){$graph_type=4;}

                    ////////////////////////////
                    /////dashboard specifics////
                    ////////////////////////////

                    //dates
                    $dates = Dates::getDates($period,$period_num,$period_group,$current,$week_ends,$show);

                    ////query for what
                    $what = array();
                    $what_type = array();

                    $query2 = "SELECT * FROM db_what WHERE viewid = '$viewid' ORDER BY id";
                    $result2 = DB_Proxy::query($query2);

                    $counter=1;
                    while($r2=mysql_fetch_array($result2)){
                        $what[$counter]=$r2["what"];
                        $what_type[$counter]=$r2["type"];

                        $counter++;
                    }

                    //build business array
                    $viewtype = array();
                    $viewbus = array();
                    $viewbus2 = array();
                    $viewname = array();
                    $counter=1;

                    $query2 = "SELECT * FROM db_who WHERE viewid = '$viewid' ORDER BY id";
                    $result2 = DB_Proxy::query($query2);

                    while($r2=mysql_fetch_array($result2)){
                        $type = $r2["type"];
                        $viewtype[$counter]=$r2["viewtype"];
                        $viewbus[$counter]=$r2["viewbus"];
                        $viewbus2[$counter]=$r2["viewbus2"];

                        if($type==1||$type==4||$type==5){
                            if($viewtype[$counter]==1){
                                $viewname[$counter]="Treat America";
                            }
                            elseif($viewtype[$counter]==2){
                                $query3 = "SELECT type_name FROM company_type WHERE typeid = '$viewbus[$counter]'";
                                $result3 = DB_Proxy::query($query3);

                                $viewname[$counter]=mysql_result($result3,0,"type_name");
                            }
                            elseif($viewtype[$counter]==3){
                                $query3 = "SELECT companyname FROM company WHERE companyid = '$viewbus[$counter]'";
                                $result3 = DB_Proxy::query($query3);

                                $viewname[$counter]=mysql_result($result3,0,"companyname");
                            }
                            elseif($viewtype[$counter]==4){
                                $query3 = "SELECT division_name FROM division WHERE divisionid = '$viewbus[$counter]'";
                                $result3 = DB_Proxy::query($query3);

                                $viewname[$counter]=mysql_result($result3,0,"division_name");
                            }
                            elseif($viewtype[$counter]==5){
                                $query3 = "SELECT districtname FROM district WHERE districtid = '$viewbus[$counter]'";
                                $result3 = DB_Proxy::query($query3);

                                $viewname[$counter]=mysql_result($result3,0,"districtname");
                            }
                            elseif($viewtype[$counter]==6){
                                $query3 = "SELECT businessname FROM business WHERE businessid = '$viewbus[$counter]'";
                                $result3 = DB_Proxy::query($query3);

                                $viewname[$counter]=mysql_result($result3,0,"businessname");
                            }
                        }
                        elseif($type==2){
                            if($viewtype[$counter]==1){
                                $viewname[$counter] = "Treat America";
                            }
                            elseif($viewtype[$counter]==2||$viewtype[$counter]==3){
                                $query3 = "SELECT name FROM est_accountowner_type WHERE id = '$viewbus[$counter]'";
                                $result3 = DB_Proxy::query($query3);

                                $viewname[$counter]=mysql_result($result3,0,"name");

                                if($viewbus2[$counter]>0){
                                    $query3 = "SELECT location FROM est_locations WHERE id = '$viewbus2[$counter]'";
                                    $result3 = DB_Proxy::query($query3);

                                    $viewname2 = mysql_result($result3,0,"location");
                                    $viewname[$counter].= " $viewname2";
                                }
                            }
                            elseif($viewtype[$counter]==4){
                                $query3 = "SELECT name FROM est_accountowner WHERE accountowner = '$viewbus[$counter]'";
                                $result3 = DB_Proxy::query($query3);

                                $viewname[$counter]=mysql_result($result3,0,"name");
                            }
                        }

                        $counter++;
                    }

                    ////build legend
                    $counter=1;
                    $viewname2 = array();

                    foreach($viewname AS $key1 => $value1){
                        foreach($what AS $key2 => $value){
                            if($value=="sales"){$value="Sales";}
                            elseif($value=="cos"){$value="COGS";}
                            elseif($value=="labor"){$value="Labor";}
                            elseif($value=="toe"){$value="TOE";}
                            elseif($value=="pl"){$value="P&L";}
                            elseif($value=="sales_bgt"){$value="Sales BGT";}
                            elseif($value=="cos_bgt"){$value="COGS BGT";}
                            elseif($value=="labor_bgt"){$value="Labor BGT";}
                            elseif($value=="toe_bgt"){$value="TOE BGT";}
                            elseif($value=="pl_bgt"){$value="P&L BGT";}
                            elseif($value=="est_sales"){$value="EST: Sales";}
                            elseif($value=="est_sales_bgt"){$value="EST: Sales BGT";}
                            elseif($value=="est_lost"){$value="EST: Lost";}
                            elseif($value=="est_lost_bgt"){$value="EST: Lost BGT";}
                            elseif($value=="est_redlist"){$value="EST: Redlist";}
                            /////capex ap
                            elseif($what_type[$key2]==4){
                                $query3 = "SELECT category_name FROM po_category WHERE categoryid = '$value'";
                                $result3 = DB_Proxy::query($query3);

                                $value = mysql_result($result3,0,"category_name");
                            }
                            /////capex budget
                            elseif($what_type[$key2]==5){
                                $query3 = "SELECT category_name FROM po_category WHERE budget_type = '$value'";
                                $result3 = DB_Proxy::query($query3);

                                $value = mysql_result($result3,0,"category_name");
                                $value .= " BGT";
                            }

                            $viewname2[$counter]= $viewname[$key1] . " $value";

                            $counter++;
                        }
                    }
                    $legend_counter=$counter;

                    //////////////////////////////
                    ///get menu items//
                    //////////////////////////////
                    if($what_type[1] == 6 && $what[1] == 1){
                        $item_list = menu_items::list_active_menu($viewbus[1]);

                        $counter=1;
                        foreach($item_list AS $key => $value){
                            $what[$counter] = $key;
                            $what_type[$counter] = 6;
                            $viewname[$counter] = $value;
                            $viewname2[$counter] = $value;

                            $counter++;
                        }
                    }
					elseif($what_type[1] == 6 && $what[1] == 2){
						$counter=1;
                        for($counter2 = 0; $counter2 <= 23; $counter2++){
                            $what[$counter] = $counter2;
                            $what_type[$counter] = 6.2;
                            $viewname[$counter] = $counter2;
                            $viewname2[$counter] = $counter2;

							$counter++;
                        }
					}

                    /////get data
                    $arr_length=count($dates);

                    $data = array();
                    $data_counter=1;
                    foreach($viewbus AS $key1 => $value1){
                        foreach($what AS $key2 => $value2){
                            $aggregate=0;
                            for($counter=1;$counter<$arr_length;$counter++){

                                $date = $dates[$counter];

                                ///dates
                                if($period_group==1){
                                    $startdate=$dates[$counter+1];
                                    $enddate=$dates[$counter+1];
                                }
                                else{
                                    $startdate=nextday($date);
                                    $enddate=$dates[$counter+1];
                                }

                                /////dashboard class
                                if($what_type[$key2] == 1){
                                    $data[$data_counter][$counter] = DashBoard::get_data($startdate,$enddate,$value2,$viewtype[$key1],$value1);
                                }
                                ////esalestrack class
                                elseif($what_type[$key2] == 2){
                                    $data[$data_counter][$counter] = DashBoard::get_esalestrack_data($startdate,$enddate,$value2,$viewtype[$key1],$value1,$viewbus2[$key1]);
                                }
                                ////CapEx class
                                elseif($what_type[$key2] == 4){
                                    $data[$data_counter][$counter] = DashBoard::get_ap_data($startdate,$enddate,$value2,$viewtype[$key1],$value1);
                                }
                                ////CapEx BGT class
                                elseif($what_type[$key2] == 5){
                                    $data[$data_counter][$counter] = DashBoard::get_ap_bgt_data($startdate,$enddate,$value2,$viewtype[$key1],$value1);
                                }
                                ////POS Menu Items
                                elseif($what_type[$key2] == 6){
                                    $data[$data_counter][$counter] = menu_items::items_sold($value2,$viewbus[1],$startdate,$enddate);
                                }
								////POS Hourly Sales
                                elseif($what_type[$key2] == 6.2){
                                    $data[$data_counter][$counter] = DashBoard::hourly_sales($value2,$viewbus[1],$startdate,$enddate);
                                }

                                ////aggregate line graph
                                if($graph_type==2){
                                    $aggregate+=$data[$data_counter][$counter];
                                    $data[$data_counter][$counter]=$aggregate;
                                }

                                ///make zero if null
                                if($data[$data_counter][$counter]=="" && $enddate<=$today){$data[$data_counter][$counter]=0;}

                                ///make null if non-budget and in the future and viewing by day
                                if($graph_type <= 4 && $startdate > $today && ($period_group == 1 || $period_group == 2 || $period_group == 3) && strpos($value2, "bgt") == false){
                                    $data[$data_counter][$counter]="";
                                }
                            }
                            $data_counter++;
                        }
                    }

                    ////////////////////////////////////////
                    /////replace with custom fields if exist
                    ////////////////////////////////////////
                    $query2 = "SELECT * FROM db_custom WHERE viewid = '$viewid' ORDER BY id";
                    $result2 = DB_Proxy::query($query2);
                    $num2 = mysql_numrows($result2);

                    if($num2>0){
                        $today = date("Y-m-d");
                        $temp_array = array();
                        $viewname2 = array();

                        $counter=1;
                        while($r2 = mysql_fetch_array($result2)){
                            $id = $r2["id"];
                            $custom_name = $r2["name"];
                            $custom_field = $r2["custom_field"];
                            $future_dates = $r2["future_dates"];

                            ////redo legend
                            $viewname2[$counter] = $custom_name;

                            ////explode custom
                            $custom = explode(" ",$custom_field);
                            $arr_length = count($custom);

                            $pointer = 0;

                            while($pointer < $arr_length){
                                if($pointer==0){
                                    foreach($data[$custom[0]] AS $key => $value){
                                        $temp_array[$counter][$key] = $value;

										////hard coded number
										if(substring($value,0,1) == "["){
											$value = str_replace("[","",$value);
											$value = str_replace("]","",$value);
											$temp_array[$counter][$key] = $value;
										}
                                    }
                                }
                                else{
                                    foreach($data[$custom[$pointer]] AS $key => $value){
                                        if($custom[$pointer-1] == "+"){
                                            $temp_array[$counter][$key] += $value;
                                        }
                                        elseif($custom[$pointer-1] == "-"){
                                            $temp_array[$counter][$key] -= $value;
                                        }
                                        elseif($custom[$pointer-1] == "*"){
                                            $temp_array[$counter][$key] = $temp_array[$counter][$key] * $value;
                                        }
                                        elseif($custom[$pointer-1] == "/"){
                                            $temp_array[$counter][$key] = $temp_array[$counter][$key] / $value;
                                        }

                                        ////make null
                                        if($future_dates == 1 && $dates[$key+1] > $today){$temp_array[$counter][$key] = "";}
                                        elseif($temp_array[$counter][$key] == 0){$temp_array[$counter][$key] = "";}
                                    }
                                }

                                $pointer+=2;
                            }

                            $counter++;
                        }
                        
                        ////reassign data array
                        $data = $temp_array;
                    }

					//////////////////////////
					///reverse array
					//////////////////////////
					/*
					if($what_type[$key2] == 6.2){
						$temp_array = array();
						$temp_array = $data;
						$data = $dates;
						$dates = $temp_array;
						$viewname2 = $dates;
					}*/
                    
                    /////what kind of graph....
                    array_shift($dates);
                    if($graph_type==1||$graph_type==2){
                        ///linegraph or aggregate line graph
                        $mygraph = LineGraph::drawGraph($data,$dates,$viewname2,$ex);
                    }
                    elseif($graph_type==3){
                        ///bar graph
                        $mygraph = LineGraph::drawBarGraph($data,$dates,$viewname2,$ex);
                    }
                    elseif($graph_type==4){
                        ///spread sheet
                        $mygraph = LineGraph::drawSpreadSheet($data,$dates,$viewname2,$ex);
                    }
                    elseif($graph_type==5){
                        ///pie chart
                        $mygraph = LineGraph::drawPieChart($data,$viewname2,$ex);
                    }

					////display
                    if($ex==1){echo "<center><b>$graph_name</b></center>";}

                    echo $mygraph;

                    if($ex==1){
                        $next = $show + 1;
                        $prev = $show - 1;
                        echo "<table width=100%><tr><td align=right><a href=display_graph.php?viewid=$viewid&show=$prev&ex=$ex><img src=\"prev.gif\" height=\"10\" width=\"10\" border=\"0\" title=\"Previous Period\"></a> <a href=display_graph.php?viewid=$viewid&show=0&ex=$ex><img src=\"home.gif\" height=\"10\" width=\"10\" border=\"0\" title=\"Default View\"></a> <a href=display_graph.php?viewid=$viewid&show=$next&ex=$ex><img src=\"next.gif\" height=\"10\" width=\"10\" border=\"0\" title=\"Next Period\"></a>&nbsp;</td></tr></table>";
                    }
                    elseif($ex == -1){
						
                    }
                    else{
                        $next = $show + 1;
                        $prev = $show - 1;
                        echo "<table width=100% cellspacing=0 cellpadding=0><tr><td align=right><a onclick=\"loadGraph($viewid,$prev);\"><img src=\"prev.gif\" height=\"10\" width=\"10\" border=\"0\" title=\"Previous Period\"></a> <a onclick=\"loadGraph($viewid,0);\"><img src=\"home.gif\" height=\"10\" width=\"10\" border=\"0\" title=\"Default View\"></a> <a onclick=\"loadGraph($viewid,$next);\"><img src=\"next.gif\" height=\"10\" width=\"10\" border=\"0\" title=\"Next Period\"></a>&nbsp;</td></tr></table>";
                    }
}
?>
