<?php
/**
 * This file takes a JSON encoded array of IDs for graphs and updates the order
 * value in the database for them.
 */

define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');

if (is_array($_GET['graphs'])) {
	foreach ($_GET['graphs'] as $k => $graph) {
		$k += 1;
		$graph = mysql_escape_string($graph);
		$sql = "UPDATE db_view SET db_order=$k WHERE viewid=$graph LIMIT 1";
		DB_Proxy::query($sql);
	}

	echo '1';
	exit;
}

echo '0';