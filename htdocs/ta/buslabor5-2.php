<?php

function fixDate($date){
	$exp = explode(' ', $date);
	$d = $exp[0];
	$t = $exp[1];

	$exp = explode(':', $t);
	$h = $exp[0];
	$m = strval($exp[1]);

	$ap = 'a';
   
	if($h >= 12){
		$ap = 'p';
		if($h != 12) $h -= 12;
	}
	else if($h < 1) $h = 12;

	$t = "$h:$m$ap";
	$n_date = "$t $d";

	return $n_date;
}
function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+4);
        $diff=round($diff, 2);
        if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
        elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
        $diff = substr($diff, 0, $dot+3);
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      elseif ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      elseif ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '1')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='31')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;
$leap = date("L");

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   
   elseif ($leap==1&&$month=='03')
   {
      $day=$day+29;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      $day=$day-31;  
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";} 
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}

//include("db.php");
define('DOC_ROOT', dirname(dirname(__FILE__)));
require_once(DOC_ROOT.'/bootstrap.php');

$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";

/*$user=$_COOKIE["usercook"];
$pass=$_COOKIE["passcook"];
$businessid=$_GET["bid"];
$companyid=$_GET["cid"];
$newdate=$_GET["newdate"];
$date1=$_GET["date1"];
$date2=$_GET["date2"];
$success=$_GET["success"];
$addclock=$_GET["addclock"];*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook');
$businessid = \EE\Controller\Base::getGetVariable('bid');
$companyid = \EE\Controller\Base::getGetVariable('cid');
$newdate = \EE\Controller\Base::getGetVariable('newdate');
$date1 = \EE\Controller\Base::getGetVariable('date1');
$date2 = \EE\Controller\Base::getGetVariable('date2');
$success = \EE\Controller\Base::getGetVariable('success');
$addclock = \EE\Controller\Base::getGetVariable('addclock');

if ($businessid==""&&$companyid==""){
   /*$businessid=$_POST["businessid"];
   $companyid=$_POST["companyid"];
   $date1=$_POST["date1"];
   $date2=$_POST["date2"];
   $showdept_id=$_POST["showdept_id"];*/
	
	$businessid = \EE\Controller\Base::getPostVariable('businessid');
	$companyid = \EE\Controller\Base::getPostVariable('companyid');
	$date1 = \EE\Controller\Base::getPostVariable('date1');
	$date2 = \EE\Controller\Base::getPostVariable('date2');
	$showdept_id = \EE\Controller\Base::getPostVariable('showdept_id');
}

if($date1==""&&$date2==""){
   /*$date1=$_COOKIE["date8"];
   $date2=$_COOKIE["date9"];*/
	
	$date1 = \EE\Controller\Base::getSessionCookieVariable('date8');
	$date2 = \EE\Controller\Base::getSessionCookieVariable('date9');
}

if($showdept_id==""){
   //$showdept_id=$_COOKIE["showdept_id"];
	$showdept_id = \EE\Controller\Base::getSessionCookieVariable('showdept_id');
}

setcookie("date8",$date1);
setcookie("date9",$date2);
setcookie("showdept_id",$showdept_id);

$pieces=explode(":",$showdept_id);
$showdept_id=$pieces[1];
$showwhich=$pieces[0];

if($showwhich==3){$lbrgrp=$pieces[1];}

if($newdate!=""){$today=$newdate;}
$todayname = dayofweek($today);

/*mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");*/

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=Treat_DB_ProxyOld::mysql_numrows($result);

$loginid=Treat_DB_ProxyOld::mysql_result($result,0,"loginid");
$security_level=Treat_DB_ProxyOld::mysql_result($result,0,"security_level");
$mysecurity=Treat_DB_ProxyOld::mysql_result($result,0,"security");
$bid=Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
$bid2=Treat_DB_ProxyOld::mysql_result($result,0,"busid2");
$bid3=Treat_DB_ProxyOld::mysql_result($result,0,"busid3");
$bid4=Treat_DB_ProxyOld::mysql_result($result,0,"busid4");
$bid5=Treat_DB_ProxyOld::mysql_result($result,0,"busid5");
$bid6=Treat_DB_ProxyOld::mysql_result($result,0,"busid6");
$bid7=Treat_DB_ProxyOld::mysql_result($result,0,"busid7");
$bid8=Treat_DB_ProxyOld::mysql_result($result,0,"busid8");
$bid9=Treat_DB_ProxyOld::mysql_result($result,0,"busid9");
$bid10=Treat_DB_ProxyOld::mysql_result($result,0,"busid10");
$cid=Treat_DB_ProxyOld::mysql_result($result,0,"companyid");

if ($num != 1 || ($security_level==1 && ($bid != $businessid || $cid != $companyid)) || $user == "" || $pass == "")
{
    echo "<center><h3>Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM company WHERE companyid = '$companyid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $companyname=Treat_DB_ProxyOld::mysql_result($result,0,"companyname");
    $week_start=Treat_DB_ProxyOld::mysql_result($result,0,"week_start");
    $week_end=Treat_DB_ProxyOld::mysql_result($result,0,"week_end");

    $day = date("d");
    $year = date("Y");
    $month = date("m");
    $today2="$year-$month-$day";

    if ($date1==""){$date1=$today2;}
    if ($date2==""){$date2=$today2;}

    $temp_date=$today2;
    while(dayofweek($temp_date)!=$week_end){$temp_date=prevday($temp_date);}
    $week_end_date=$temp_date;
    $is_start=0;
    while($is_start!=2){
       $temp_date=prevday($temp_date);
       if (dayofweek($temp_date)==$week_start){$is_start++;}
    }
    $week_start_date=$temp_date;

    ////////////////////
    ////SUBMIT DATES////
    ////////////////////
    $submit_end=date("Y-m-d");
    while(dayofweek($submit_end)!=$week_end){$submit_end=prevday($submit_end);}
    $submit_start=$submit_end;
    for($counter=1;$counter<=6;$counter++){$submit_start=prevday($submit_start);}

    $showlastperiod="<font size=2>[<a href=buslabor5.php?bid=$businessid&cid=$companyid&date1=$week_start_date&date2=$week_end_date><font color=blue>Last 2 Weeks</font></a>]</font>";

    $query = "SELECT * FROM security WHERE securityid = '$mysecurity'";
    $result = Treat_DB_ProxyOld::query($query);

    $sec_emp_labor=Treat_DB_ProxyOld::mysql_result($result,0,"emp_labor");
    $sec_bus_sales=Treat_DB_ProxyOld::mysql_result($result,0,"bus_sales");
    $sec_bus_invoice=Treat_DB_ProxyOld::mysql_result($result,0,"bus_invoice");
    $sec_bus_order=Treat_DB_ProxyOld::mysql_result($result,0,"bus_order");
    $sec_bus_payable=Treat_DB_ProxyOld::mysql_result($result,0,"bus_payable");
    $sec_bus_inventor1=Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor1");
    $sec_bus_inventor2=Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor2");
    $sec_bus_inventor3=Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor3");
    $sec_bus_inventor4=Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor4");
    $sec_bus_inventor5=Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor5");
    $sec_bus_control=Treat_DB_ProxyOld::mysql_result($result,0,"bus_control");
    $sec_bus_menu=Treat_DB_ProxyOld::mysql_result($result,0,"bus_menu");
    $sec_bus_order_setup=Treat_DB_ProxyOld::mysql_result($result,0,"bus_order_setup");
    $sec_bus_request=Treat_DB_ProxyOld::mysql_result($result,0,"bus_request");
    $sec_route_collection=Treat_DB_ProxyOld::mysql_result($result,0,"route_collection");
    $sec_route_labor=Treat_DB_ProxyOld::mysql_result($result,0,"route_labor");
    $sec_route_detail=Treat_DB_ProxyOld::mysql_result($result,0,"route_detail");
    $sec_bus_labor=Treat_DB_ProxyOld::mysql_result($result,0,"bus_labor");
    $sec_bus_timeclock=Treat_DB_ProxyOld::mysql_result($result,0,"bus_timeclock");

    ////SECURITY LEVELS
    if($sec_bus_timeclock<1||$sec_bus_timeclock==1&&$bid!=$businessid){
       $location="businesstrack.php";
       header('Location: ./' . $location);
    }
    else{}

    //if ($sec_emp_labor>0){$showallunits="<font size=2>[<a href=emp_labor.php?bid=$businessid&cid=$companyid><font color=blue>Employee Labor</font></a>]";}

?>

<html>
<head>
<script language="JavaScript" 
   type="text/JavaScript">
function changePage(newLoc)
 {
   nextPage = newLoc.options[newLoc.selectedIndex].value
		
   if (nextPage != "")
   {
      document.location.href = nextPage
   }
 }
</script>

<SCRIPT LANGUAGE="JavaScript" SRC="CalendarPopup.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">document.write(getCalendarStyles());</SCRIPT>

<STYLE>
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation
			{
			background-color:#6677DD;
			text-align:center;
			vertical-align:center;
			text-decoration:none;
			color:#FFFFFF;
			font-weight:bold;
			}
	.TESTcpDayColumnHeader,
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation,
	.TESTcpCurrentMonthDate,
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDate,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDate,
	.TESTcpCurrentDateDisabled,
	.TESTcpTodayText,
	.TESTcpTodayTextDisabled,
	.TESTcpText
			{
			font-family:arial;
			font-size:8pt;
			}
	TD.TESTcpDayColumnHeader
			{
			text-align:right;
			border:solid thin #6677DD;
			border-width:0 0 1 0;
			}
	.TESTcpCurrentMonthDate,
	.TESTcpOtherMonthDate,
	.TESTcpCurrentDate
			{
			text-align:right;
			text-decoration:none;
			}
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDateDisabled
			{
			color:#D0D0D0;
			text-align:right;
			text-decoration:line-through;
			}
	.TESTcpCurrentMonthDate
			{
			color:#6677DD;
			font-weight:bold;
			}
	.TESTcpCurrentDate
			{
			color: #FFFFFF;
			font-weight:bold;
			}
	.TESTcpOtherMonthDate
			{
			color:#808080;
			}
	TD.TESTcpCurrentDate
			{
			color:#FFFFFF;
			background-color: #6677DD;
			border-width:1;
			border:solid thin #000000;
			}
	TD.TESTcpCurrentDateDisabled
			{
			border-width:1;
			border:solid thin #FFAAAA;
			}
	TD.TESTcpTodayText,
	TD.TESTcpTodayTextDisabled
			{
			border:solid thin #6677DD;
			border-width:1 0 0 0;
			}
	A.TESTcpTodayText,
	SPAN.TESTcpTodayTextDisabled
			{
			height:20px;
			}
	A.TESTcpTodayText
			{
			color:#6677DD;
			font-weight:bold;
			}
	SPAN.TESTcpTodayTextDisabled
			{
			color:#D0D0D0;
			}
	.TESTcpBorder
			{
			border:solid thin #6677DD;
			}
</STYLE>

<SCRIPT LANGUAGE=javascript><!--
function caution(){return confirm('THERE ARE UNSUBMITTED DAYS WITHIN THIS DATE RANGE!');}
// --></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
<!-- Web Site:  http://dynamicdrive.com -->

<!-- This script and many more are available free online at -->
<!-- The JavaScript Source!! http://javascript.internet.com -->
<!-- Begin
function disableForm(theform) {
if (document.all || document.getElementById) {
for (i = 0; i < theform.length; i++) {
var tempobj = theform.elements[i];
if (tempobj.type.toLowerCase() == "submit" || tempobj.type.toLowerCase() == "reset")
tempobj.disabled = true;
}

}
else {
alert("The form has been submitted.  But, since you're not using IE 4+ or NS 6, the submit button was not disabled on form submission.");
return false;
   }
}
//  End -->
</script>

<SCRIPT LANGUAGE=javascript SRC="javascripts/prototype.js"></SCRIPT>
<script type="text/javascript" src="javascripts/scriptaculous.js"></script>

<SCRIPT LANGUAGE=javascript>
/* TEST */
var procEdited = function(t){
	var cell = $(uObj.table.rows[uObj.curRow].cells[uObj.table.rows[uObj.curRow].cells.length-1]);
	cell.update(t.responseText);
	cell.highlight({endcolor: '#e8e7e7'});
}
var uObj;
var updateEdited = function(obj){
	uObj = obj;
	var parameters = [];
	parameters['method'] = 'updatePunchTime';
	parameters['row'] = obj.curRow;
	parameters['table'] = obj.table.id;
	parameters['id'] = obj.table.rows[obj.curRow].id;
	parameters['eid'] = obj.table.rows[obj.curRow].cells[0].id;
	parameters['job'] = obj.table.rows[obj.curRow].cells[1].id;
	parameters['from'] = '<?php echo $date1; ?>';
	parameters['to'] = '<?php echo $date2; ?>';
	obj.table.rows[obj.curRow].cells[obj.table.rows[obj.curRow].cells.length-1].innerHTML = 'Calculating...';

	new Ajax.Request('ajaxreq.php', {
			parameters: parameters
		});
}

var timeSwap = function(trigger, obj){
	var val = trigger.innerHTML;
	var exp = val.split(/ /);
	var t = exp[0];
	var d = exp[1];
	
	var input = $(document.createElement('input'));
	input.value = t;
	input.style.textAlign = 'right';
	$(trigger).update('');
	trigger.appendChild(input);
	input.select();

	input.onkeydown = function(e){
		var ev = e || window.event;
		var k = ev.keyCode || ev.charCode;

		if(k == 9){
			if($(trigger).cellIndex == 3){
				
				setTimeout(function(){ obj.tdSwap(obj.table.rows[obj.curRow].cells[5]); }, 100);
			}
		}
	}

	input.onblur = function(){
		var val = def = this.value;

		if(val.search(/a|p/) > -1){
			var os = false;
			if(val.search(/a/) > -1){
				var ap = 'a';
			}
			else{
				os = true;
				var ap = 'p';
			}
			val = val.replace(/a|p/, '');

			var exp = val.split(/:/);

			if(exp.length > 1){
				var h = new Number(exp[0]);
				var m = exp[1];
			}
			else{
				if(val.length > 2){
					var i = val.length - 2;
					var h = new Number(val.substring(0, i));
					var m = new Number(val.substring(i));
				}
				else{
					var h = val;
					var m = '00';
				}
			}
			var def = h+':'+m+ap;

			if(os){
				if(h != 12) h += 12;
			}
			else{
				if(h == 12) h = 0;
			}

			var time = h+":"+m+":00";
		}
		else{
			var tmp = time = this.value;
			var exp = time.split(/:/);
			if(exp.length > 1){
				var h = exp[0];
				var m = exp[1] || '00';
			}
			else{
				if(val.length > 2){
					var i = val.length - 2;
					var h = new Number(val.substring(0, i));
					var m = new Number(val.substring(i));
				}
				else{
					var h = val;
					var m = '00';
				}
				time = h+':'+m+':00';
			}
			var ap = 'a';
			if(h >= 12){
				ap = 'p';
				if(h != 12) h -= 12;
			}
			else if(h < 1) h = 12;

			def = h+':'+m+ap;
		}

		var s = d+' '+time;
		var disp = def+' '+d;
		obj.inputSwap(trigger, s, disp);
	}
}

window.onload=function(){
	var custoSwap = [];
	custoSwap['clockin'] = timeSwap;
	custoSwap['clockout'] = timeSwap;

	new Form.EditInPlace('labor_clockin', {
			primaryKey: 'clockinid',
			specialSwaps: custoSwap,
			onRowEdit: updateEdited
		});

}
</SCRIPT>

<style type="text/css">
.highlight_word{
    background-color: yellow;
}
</style> 

<SCRIPT LANGUAGE=javascript><!--
function starttimeclock(){return confirm('1. THE TIMECLOCK WILL START IN A NEW WINDOW\n2. MAKE SURE YOU CLOSE THIS WINDOW');}
// --></SCRIPT>

<SCRIPT LANGUAGE=javascript><!--
function deleteconfirm(){return confirm('ARE YOU SURE YOU WANT TO DELETE THIS RECORD?');}
// --></SCRIPT>

<SCRIPT LANGUAGE=javascript><!--
function fixconfirm(){return confirm('ARE YOU SURE YOU WANT TO REMOVE THE CLOCK OUT?');}
// --></SCRIPT>

<SCRIPT LANGUAGE=javascript><!--
function submit2(){return confirm('ARE YOU SURE YOU WANT TO SUBMIT?');}
// --></SCRIPT>

<script language="JavaScript"
   type="text/JavaScript">
function popup2(url)
  {
     popwin=window.open(url,"VendMachines","location=no,scrollbars=yes,menubar=no,titlebar=no,resizeable=no,height=600,width=600");   
  }
</script>

</head>

<?
   
    $printdate1=$date1;
    $printdate2=$date2;

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM business WHERE companyid = '$companyid' AND businessid = '$businessid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $businessname=Treat_DB_ProxyOld::mysql_result($result,0,"businessname");
    $businessid=Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
    $street=Treat_DB_ProxyOld::mysql_result($result,0,"street");
    $city=Treat_DB_ProxyOld::mysql_result($result,0,"city");
    $state=Treat_DB_ProxyOld::mysql_result($result,0,"state");
    $zip=Treat_DB_ProxyOld::mysql_result($result,0,"zip");
    $phone=Treat_DB_ProxyOld::mysql_result($result,0,"phone");
    $districtid=Treat_DB_ProxyOld::mysql_result($result,0,"districtid");
    $operate=Treat_DB_ProxyOld::mysql_result($result,0,"operate");
    $labor_notify_emailTreat_DB_ProxyOld::mysql_result($result,0,"labor_notify_email");
    $bus_unit=Treat_DB_ProxyOld::mysql_result($result,0,"unit");
    $auto_submit_labor=Treat_DB_ProxyOld::mysql_result($result,0,"auto_submit_labor");

    echo "<body>";  
    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%><tr><td colspan=2>";
    echo "<a href=businesstrack.php><img src=logo.jpg border=0 height=43 width=205></a><p></td></tr>";

    //echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";
    echo "<tr bgcolor=#CCCCFF><td width=1%><img src=weblogo.jpg width=80 height=75></td><td><font size=4><b>$businessname #$bus_unit</b></font><br>$street<br>$city, $state $zip<br>$phone</td>";
    echo "<td align=right valign=top><br>";
    echo "<FORM ACTION='editbus.php' method=post name='store'>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<input type=hidden name=businessid value=$businessid>";
    echo "<input type=hidden name=companyid value=$companyid>";
    echo "<INPUT TYPE=submit VALUE=' Store Setup '> ";
    echo "</form></td></tr>";
    //echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";
    
    echo "<tr><td colspan=3><font color=#999999>";
    if ($sec_bus_sales>0){echo "<a href=busdetail.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Sales</font></a>";}
    if ($sec_bus_invoice>0){echo " | <a href=businvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Invoicing</font></a>";}
    if ($sec_bus_order>0){echo " | <a href=buscatertrack.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Orders</font></a>";}
    if ($sec_bus_payable>0){echo " | <a href=busapinvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Payables</font></a>";}
    if ($sec_bus_inventor1>0){echo " | <a href=businventor.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Inventory</font></a>";}
    if ($sec_bus_control>0){echo " | <a href=buscontrol.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Control Sheet</font></a>";}
    if ($sec_bus_menu>0){echo " | <a href=buscatermenu.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Menus</font></a>";}
    if ($sec_bus_order_setup>0){echo " | <a href=buscaterroom.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Order Setup</font></a>";}
    if ($sec_bus_request>0){echo " | <a href=busrequest.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Requests</font></a>";}
    if ($sec_route_collection>0||$sec_route_labor>0||$sec_route_detail>0){echo " | <a href=busroute_collect.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Route Collections</font></a>";}
    if ($sec_bus_labor>0||$sec_bus_timeclock>0){echo " | <a href=buslabor.php?bid=$businessid&cid=$companyid><font color=red onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='red' style='text-decoration:none'>Labor</font></a>";}
    echo "</td></tr>";

    echo "</table></center><p>";

if ($security_level>0){
    echo "<center><table width=90%><tr><td><form action=buslabor3.php method=post>";
    echo "<select name=gobus onChange='changePage(this.form.gobus)'>";

    $districtquery="";
    if ($security_level>2&&$security_level<7){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM login_district WHERE loginid = '$loginid'";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();

       $num--;
       while($num>=0){
          $districtid=Treat_DB_ProxyOld::mysql_result($result,$num,"districtid");
          if($districtquery==""){$districtquery="districtid = '$districtid'";}
          else{$districtquery="$districtquery OR districtid = '$districtid'";}
          $num--;
       }
    }

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    if($security_level>6){$query = "SELECT * FROM business WHERE companyid = '$companyid' ORDER BY businessname DESC";}
    elseif($security_level==2||$security_level==1){$query = "SELECT * FROM business WHERE companyid = '$companyid' AND (businessid = '$bid' OR businessid = '$bid2' OR businessid = '$bid3' OR businessid = '$bid4' OR businessid = '$bid5' OR businessid = '$bid6' OR businessid = '$bid7' OR businessid = '$bid8' OR businessid = '$bid9' OR businessid = '$bid10') ORDER BY businessname DESC";}
    elseif($security_level>2&&$security_level<7){$query = "SELECT * FROM business WHERE companyid = '$companyid' AND ($districtquery) ORDER BY businessname DESC";}
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    $num--;
    while($num>=0){
       $businessname=Treat_DB_ProxyOld::mysql_result($result,$num,"businessname");
       $busid=Treat_DB_ProxyOld::mysql_result($result,$num,"businessid");

       if ($busid==$businessid){$show="SELECTED";}
       else{$show="";}

       echo "<option value=buslabor5.php?bid=$busid&cid=$companyid&date1=$date1&date2=$date2 $show>$businessname</option>";
       $num--;
    }
 
    echo "</select></td><td></form></td>";

    echo "<td width=70% align=right><form action=buslabor5.php method=post onSubmit='return disableForm(this);'><input type=hidden name=businessid value=$businessid><input type=hidden name=companyid value=$companyid>$showallunits $showlastperiod <select name=showdept_id>";

    echo "<option value=0>Show All</option>";
	
	////////////////////////////////
	/////////LABOR GROUPS///////////
	////////////////////////////////
	
	echo "<optgroup label='Or Show Group...'>";
	
	$query37 = "SELECT labor_groups.labor_group_id FROM labor_groups,labor_group_security WHERE labor_group_security.loginid = '$loginid' AND labor_group_security.labor_group_id = labor_groups.labor_group_id AND labor_groups.businessid = '$businessid'";
    $result37 = Treat_DB_ProxyOld::query($query37);
    $num37=Treat_DB_ProxyOld::mysql_numrows($result37);

    if($lbrgrp<1&&$num37>0&&$showwhich==3){$lbrgrp=Treat_DB_ProxyOld::mysql_result($result37,0,"labor_group_id");}
    
    if($num37==0){$query35 = "SELECT * FROM labor_groups WHERE businessid = '$businessid' ORDER BY labor_group_name DESC";}
    else{$query35 = "SELECT labor_groups.labor_group_name,labor_groups.labor_group_id FROM labor_groups,labor_group_security WHERE labor_groups.businessid = '$businessid' AND labor_groups.labor_group_id = labor_group_security.labor_group_id AND labor_group_security.loginid = '$loginid' ORDER BY labor_groups.labor_group_name DESC";}
    $result35 = Treat_DB_ProxyOld::query($query35);
    $num35=Treat_DB_ProxyOld::mysql_numrows($result35);

    $num35--;

    while($num35>=0){

       $labor_group_id=Treat_DB_ProxyOld::mysql_result($result35,$num35,"labor_group_id");
       $labor_group_nameTreat_DB_ProxyOld::mysql_result($result35,$num35,"labor_group_name");

       if($labor_group_id==$lbrgrp){$showsel="SELECTED";}
       else{$showsel="";}

       echo "<option value='3:$labor_group_id'>$labor_group_name</option>";

       $num35--;
    }
	echo "</optgroup>";
	/////////END LABOR GROUPS///////
	
	echo "<optgroup label='Or Show Department...'>";

    $query6 = "SELECT * FROM security_departments WHERE securityid = '$mysecurity'";
    $result6 = Treat_DB_ProxyOld::query($query6);
    $num6=Treat_DB_ProxyOld::mysql_numrows($result6);

    $num6--;
    while($num6>=0){
       $dept_id=Treat_DB_ProxyOld::mysql_result($result6,$num6,"dept_id");

       $query7 = "SELECT dept_name FROM payroll_department WHERE dept_id = '$dept_id'";
       $result7 = Treat_DB_ProxyOld::query($query7);

       $dept_name=Treat_DB_ProxyOld::mysql_result($result7,0,"dept_name");

       if($showdept_id==$dept_id){$showsel="SELECTED";}
       else{$showsel="";}

       echo "<option value='1:$dept_id' $showsel>$dept_name</option>";

       $num6--;
    }

    echo "</optgroup><optgroup label='Or Show Employee...'>";

    $query6 = "SELECT login.firstname,login.lastname,login.loginid FROM login,security_departments,payroll_departmentdetail WHERE login.oo2 = '$businessid' AND (login.loginid = payroll_departmentdetail.loginid AND payroll_departmentdetail.dept_id = security_departments.dept_id AND security_departments.securityid = '$mysecurity' AND login.is_deleted = '0') ORDER BY login.lastname DESC";
    $result6 = Treat_DB_ProxyOld::query($query6);
    $num6=Treat_DB_ProxyOld::mysql_numrows($result6);

    $num6--;
    while($num6>=0){
       $loginid=Treat_DB_ProxyOld::mysql_result($result6,$num6,"login.loginid");
       $firstname=Treat_DB_ProxyOld::mysql_result($result6,$num6,"login.firstname");
       $lastname=Treat_DB_ProxyOld::mysql_result($result6,$num6,"login.lastname");

       if($showdept_id==$loginid){$showsel="SELECTED";}
       else{$showsel="";}

       echo "<option value='2:$loginid' $showsel>$lastname, $firstname</option>";

       $num6--;
    }

    echo "</optgroup></select> <SCRIPT LANGUAGE='JavaScript' ID='js18'> var cal18 = new CalendarPopup('testdiv1'); cal18.setCssPrefix('TEST');</SCRIPT> <A HREF='javascript:void();' onClick=cal18.select(document.forms[2].date1,'anchor18','yyyy-MM-dd'); return false; TITLE=cal18.select(document.forms[2].date1,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor18' ID='anchor18'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A> <input type=text name=date1 value='$date1' size=8> <b><i>to</i></b> <SCRIPT LANGUAGE='JavaScript' ID='js19'> var cal19 = new CalendarPopup('testdiv1'); cal19.setCssPrefix('TEST');</SCRIPT> <A HREF='javascript:void();' onClick=cal19.select(document.forms[2].date2,'anchor19','yyyy-MM-dd'); return false; TITLE=cal19.select(document.forms[2].date2,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor19' ID='anchor19'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A> <input type=text name=date2 value='$date2' size=8> <input type=submit value='GO'></td><td></form></td></tr>";

    if($auto_submit_labor==1){echo "<tr valign=top><td colspan=2 width=50%><iframe src='dept_submit.php?cid=$companyid&bid=$businessid&date=$submit_end&st=1' WIDTH=99% HEIGHT=100 frameborder=0></iframe></td><td align=right colspan=1><iframe src='showtime.php' WIDTH=100% HEIGHT=24 frameborder=0></iframe></td></tr>";}

    echo "</table></center><p>";
}
    $security_level=$sec_bus_labor;

if($sec_bus_labor>0){
    echo "<center><table width=90% cellspacing=0 cellpadding=0>";

    echo "<tr bgcolor=#CCCCCC valign=top><td width=1%><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=15%><center><a href=buslabor.php?cid=$companyid&bid=$businessid style=$style><font color=blue>Hours</a></center></td><td width=1% align=right><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1% ><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=15%><center><a href=buslabor4.php?cid=$companyid&bid=$businessid&newdate=$date1 style=$style style=$style><font color=blue>Tips</a></center></td><td width=1% align=right><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1%><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=15%><center><a href=buslabor2.php?cid=$companyid&bid=$businessid&newdate=$date1 style=$style style=$style><font color=blue>Commissions</a></center></td><td width=1% align=right><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1%><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=15%><center><a href=buslabor3.php?cid=$companyid&bid=$businessid&newdate=$date1 style=$style><font color=blue>Summary</a></center></td><td width=1% align=right><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1% bgcolor=#E8E7E7><img src=leftcrn.jpg height=6 width=6 align=top></td><td width=15% bgcolor=#E8E7E7><center><a href=buslabor5.php?cid=$companyid&bid=$businessid&newdate=$date1 style=$style style=$style><font color=black>Time Clock</a></center></td><td width=1% align=right bgcolor=#E8E7E7><img src=rightcrn.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td bgcolor=white width=25%></td></tr>";

    echo "<tr height=2><td colspan=4></td><td colspan=4></td><td colspan=4></td><td colspan=4></td><td colspan=3 bgcolor=#E8E7E7></td><td></td><td colspan=1></td></tr>";

    echo "</table></center>";
}
///////////////////DISPLAY
    $query = "SELECT * FROM submit_labor WHERE date >= '$date1' AND date <= '$date2' AND businessid = '$businessid'";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);

    $tablename="labor_clockin";
    if($num>0&&$security_level<9){$is_disabled="DISABLED"; $showdisable="<font color=red>There are submitted days within this date range.</font>"; $tablename="labor_clockin";}

    echo "<center><div style=\"width: 90%; background: #e8e7e7;\" align=\"right\"><a href=redirect.php?bid=$businessid&cid=$companyid style=$style target='_blank' onclick='javascript:starttimeclock();'><font color=blue onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='blue'>Single Unit Time Clock</font></a> | <a href=redirect.php?bid=$businessid&cid=$companyid&multi=1 style=$style target='_blank' onclick='javascript:starttimeclock();'><font color=blue onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='blue'>Multi Unit Time Clock</font></a></div>";
    echo "<table width=90% cellspacing=0 cellpadding=0 bgcolor=#E8E7E7 id='$tablename'>";

    $today=date("Y-m-d");
    $today="$today 00:00:00";
    $date1="$date1 00:00:00";
    $date2="$date2 23:59:59";

	if($showwhich==3){$query = "SELECT distinct labor_clockin.clockinid,labor_clockin.loginid,labor_clockin.clockin,labor_clockin.clockout,labor_clockin.jobtype,labor_clockin.is_deleted,login.firstname,login.lastname,login.empl_no FROM labor_clockin,login,labor_groups,labor_group_detail WHERE labor_clockin.businessid = '$businessid' AND labor_clockin.clockin >= '$date1' AND labor_clockin.clockin <= '$date2' AND labor_clockin.loginid = login.loginid AND login.loginid = labor_group_detail.loginid AND labor_group_detail.labor_group_id = labor_groups.labor_group_id AND labor_groups.labor_group_id = '$lbrgrp' ORDER BY login.lastname DESC,login.firstname DESC,labor_clockin.jobtype,labor_clockin.clockin DESC";}
    elseif($showwhich==1){$query = "SELECT distinct labor_clockin.clockinid,labor_clockin.loginid,labor_clockin.clockin,labor_clockin.clockout,labor_clockin.jobtype,labor_clockin.is_deleted,login.firstname,login.lastname,login.empl_no FROM labor_clockin,login,payroll_departmentdetail,security_departments WHERE labor_clockin.businessid = '$businessid' AND labor_clockin.clockin >= '$date1' AND labor_clockin.clockin <= '$date2' AND labor_clockin.loginid = login.loginid AND (login.loginid = payroll_departmentdetail.loginid AND payroll_departmentdetail.dept_id = security_departments.dept_id AND security_departments.securityid = '$mysecurity' AND payroll_departmentdetail.dept_id = '$showdept_id') ORDER BY login.lastname DESC,login.firstname DESC,labor_clockin.jobtype,labor_clockin.clockin DESC";}
    elseif($showwhich==2){$query = "SELECT distinct labor_clockin.clockinid,labor_clockin.loginid,labor_clockin.clockin,labor_clockin.clockout,labor_clockin.jobtype,labor_clockin.is_deleted,login.firstname,login.lastname,login.empl_no FROM labor_clockin,login,payroll_departmentdetail,security_departments WHERE labor_clockin.businessid = '$businessid' AND labor_clockin.clockin >= '$date1' AND labor_clockin.clockin <= '$date2' AND labor_clockin.loginid = login.loginid AND (login.loginid = payroll_departmentdetail.loginid AND payroll_departmentdetail.dept_id = security_departments.dept_id AND security_departments.securityid = '$mysecurity' AND login.loginid = '$showdept_id') ORDER BY login.lastname DESC,login.firstname DESC,labor_clockin.jobtype,labor_clockin.clockin DESC";}
    else{$query = "SELECT distinct labor_clockin.clockinid,labor_clockin.loginid,labor_clockin.clockin,labor_clockin.clockout,labor_clockin.jobtype,labor_clockin.is_deleted,login.firstname,login.lastname,login.empl_no FROM labor_clockin,login,payroll_departmentdetail,security_departments WHERE labor_clockin.businessid = '$businessid' AND labor_clockin.clockin >= '$date1' AND labor_clockin.clockin <= '$date2' AND labor_clockin.loginid = login.loginid AND (login.loginid = payroll_departmentdetail.loginid AND payroll_departmentdetail.dept_id = security_departments.dept_id AND security_departments.securityid = '$mysecurity') ORDER BY login.lastname DESC,login.firstname DESC,labor_clockin.jobtype,labor_clockin.clockin DESC";}
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);

    if ($num==0){echo "<tr><td colspan=8><i>Nothing to Display</i></td></tr>";}
    else{echo "<tr bgcolor=#CCCC99 style=\"font-weight: bold;\"><td width=10%><font size=2><b>Empl#</b></td><td width=20%><font size=2><b>Employee</b></td><td width=20%><font size=2><b>Jobtype</b></td><td id='clockin' width=20% align=right>Clockin</td><td width=1></td><td id='clockout' width=20% align=right>Clockout</td><td></td><td align=right width=10%><b><font size=2>Total</td></tr>";}

    $lastloginid=-1;
    $lastjobtype=-1;
    $hours=0;
    $mins=0;
    $counter=0;

    $num--;
    while($num>=-1){
       $clockinid=Treat_DB_ProxyOld::mysql_result($result,$num,"labor_clockin.clockinid");
       $loginid=Treat_DB_ProxyOld::mysql_result($result,$num,"labor_clockin.loginid");
       $clockin=Treat_DB_ProxyOld::mysql_result($result,$num,"labor_clockin.clockin");
       $clockout=Treat_DB_ProxyOld::mysql_result($result,$num,"labor_clockin.clockout");
       $jobtype=Treat_DB_ProxyOld::mysql_result($result,$num,"labor_clockin.jobtype");
       $firstname=Treat_DB_ProxyOld::mysql_result($result,$num,"login.firstname");
       $lastname=Treat_DB_ProxyOld::mysql_result($result,$num,"login.lastname");
       $empl_no=Treat_DB_ProxyOld::mysql_result($result,$num,"login.empl_no");
       $is_deleted=Treat_DB_ProxyOld::mysql_result($result,$num,"labor_clockin.is_deleted");

       $query2="SELECT name FROM jobtype WHERE jobtypeid = '$jobtype'";
       $result2 = Treat_DB_ProxyOld::query($query2);

       $jobname=Treat_DB_ProxyOld::mysql_result($result2,0,"name");

       if($clockinid==$addclock){$rowcolor="yellow";}
       else{$rowcolor="";}

       if (($lastloginid!=$loginid||$lastjobtype!=$jobtype)&&$lastloginid!=-1&&$lastjobtype!=-1){
          $hours=$hours*60;
          $totmins=round(($hours+$mins)/60,2);

          echo "<tr bgcolor=#CCCCCC><td colspan=5><a name=$loginid></a>$counter Record(s) <font size=2>[<a href='printlabor2.php?eid=$lastloginid&bid=$businessid&cid=$companyid&date2=$printdate2&date1=$printdate1' style=$style target='_blank'><font color=blue>Print</font></a>]</font></td><td colspan=\"2\"></td><td id=\"et_$lastloginid:$lastjobtype\" align=right>$totmins</td></tr>";

          $counter=0;
          $hours=0;
          $mins=0;
       }

       $thisday=date("Y-m-d");

       $fixdate2="<table cellspacing=0 cellpadding=0><tr><td><a href=fixdate.php?bid=$businessid&cid=$companyid&clockinid=$clockinid&date1=$printdate1&date2=$printdate2&loginid=$loginid&chdate=3 onmouseover=up1$clockinid.src='arrowup2.gif' onmouseout=up1$clockinid.src='arrowup.gif'><img src=arrowup.gif height=10 width=10 border=0 name=up1$clockinid alt='Change Date'></a></td><td rowspan=2></td></tr><tr><td><a href=fixdate.php?bid=$businessid&cid=$companyid&clockinid=$clockinid&date1=$printdate1&date2=$printdate2&loginid=$loginid&chdate=4 onmouseover=down1$clockinid.src='arrowdown2.gif' onmouseout=down1$clockinid.src='arrowdown.gif'><img src=arrowdown.gif name=down1$clockinid height=10 width=10 border=0 alt='Change Date'></a></td></tr></table>";

       if($clockout!="0000-00-00 00:00:00"){
         if($clockout!="0000-00-00 00:00:00"&&substr($clockin,0,10)==$thisday){
             $removepunch="<a href=fixdate.php?bid=$businessid&cid=$companyid&clockinid=$clockinid&date1=$printdate1&date2=$printdate2&clear=1&loginid=$loginid onclick='return fixconfirm()'><img src=cleardate.gif height=16 width=16 border=0 alt='Remove Clockout'></a>";
         }   
         else{$removepunch="";} 

         $fixdate="<table cellspacing=0 cellpadding=0><tr><td><a href=fixdate.php?bid=$businessid&cid=$companyid&clockinid=$clockinid&date1=$printdate1&date2=$printdate2&loginid=$loginid&chdate=1 onmouseover=up$clockinid.src='arrowup2.gif' onmouseout=up$clockinid.src='arrowup.gif'><img src=arrowup.gif height=10 width=10 border=0 name=up$clockinid alt='Change Date'></a></td><td rowspan=2>$removepunch</td></tr><tr><td><a href=fixdate.php?bid=$businessid&cid=$companyid&clockinid=$clockinid&date1=$printdate1&date2=$printdate2&loginid=$loginid&chdate=2 onmouseover=down$clockinid.src='arrowdown2.gif' onmouseout=down$clockinid.src='arrowdown.gif'><img src=arrowdown.gif name=down$clockinid height=10 width=10 border=0 alt='Change Date'></a></td></tr></table>";
       }
       else{$fixdate="";}
   

       if($clockin<$today&&$clockout=="0000-00-00 00:00:00"){$showcolor="red";$timestyle="color:#FF0000";}
       elseif($clockout=="0000-00-00 00:00:00"){$showcolor="#444499";$timestyle="color:#444499";}
       else{$showcolor="black";$timestyle="color:black";}

       if($is_deleted==1){$showdelete="<a href=fixdate.php?bid=$businessid&cid=$companyid&clockinid=$clockinid&date1=$printdate1&date2=$printdate2&del=1&loginid=$loginid&delete=0><img src=undelete.gif height=16 width=16 border=0 alt='Undo'></a> ";}
       else{$showdelete="<a href=fixdate.php?bid=$businessid&cid=$companyid&clockinid=$clockinid&date1=$printdate1&date2=$printdate2&del=1&loginid=$loginid&delete=1 onclick='return deleteconfirm()'><img src=delete.gif height=16 width=16 border=0 alt='Delete Record'></a> ";}

       if($clockout=="0000-00-00 00:00:00"){$co_date=substr($clockin,0,10); $co_time=date("G:i:s"); $clockout="$co_date $co_time";}

       $clockin=fixDate($clockin);
       if($clockout!=""){$clockout=fixDate($clockout);}

       $counter++;

       $query2="SELECT TimeDiff(clockout,clockin) AS totalamt FROM labor_clockin WHERE clockinid = '$clockinid'";
       $result2 = Treat_DB_ProxyOld::query($query2);

       $totalamt=Treat_DB_ProxyOld::mysql_result($result2,0,"totalamt");

       if(substr($totalamt,0,2)>=14){$showcolor="orange";$timesyle="color:orange";}

       if($is_deleted==1){$showis_deleted="<strike>";}
       else{$showis_deleted="";}

       if($num!=-1){echo "<tr id='$clockinid' height=24 bgcolor='$rowcolor' onMouseOver=this.bgColor='#999999' onMouseOut=this.bgColor='$rowcolor'><td id=\"$loginid\"><font size=3 color=$showcolor>$showis_deleted$showdelete$empl_no</font></td><td id=\"$jobtype\"><FONT size=3 COLOR=$showcolor onMouseOver=this.style.color='#FF9900';this.style.cursor='hand' onMouseOut=this.style.color='$showcolor'><a onclick=popup2('employee_options.php?lid=$loginid') title='TimeClock Options'>$showis_deleted$lastname, $firstname</font></td><td><font size=3 color=$showcolor>$showis_deleted$jobname</font></td><td align=right>$clockin</td><td width=1>$fixdate2</td><td align=right style=$timestyle>$clockout</td><td width=1>$fixdate</td><td align=right><font size=3 color=$showcolor>$showis_deleted$totalamt</td></tr>";}

       if($is_deleted==0){
          $pieces=explode(":",$totalamt);
          $hours+=$pieces[0];
          $mins+=$pieces[1];
       }

       $lastloginid=$loginid;
       $lastjobtype=$jobtype;

       $num--;
    }

///////////////////END
    echo "<tr><td colspan=3><form action=update_hours.php method=post onSubmit='return disableForm(this);'><input type=hidden name=businessid value=$businessid><input type=hidden name=companyid value=$companyid><input type=hidden name=date1 value=$printdate1><input type=hidden name=date2 value=$printdate2><input type=submit value='Update Hours' $is_disabled> $showdisable</td><td></form></td>";
    if($auto_submit_labor==1){echo "<td align=right colspan=4><form action=submit_labor_dept.php style=\"margin:0;padding:0;display:inline;\" method=post onSubmit='return disableForm(this);'><input type=hidden name=companyid value=$companyid><input type=hidden name=businessid value=$businessid><input type=hidden name=date value=$submit_start><input type=hidden name=goto value=2><input type=submit value='Submit Week of $submit_end' onclick='return submit2()' $is_disabled></form></td>";}
    else{ echo "<td colspan=4></td>";}
    echo "</tr>";
    echo "</table></center>";
//////////////////ADD EMPLOYEE PUNCH

    $date3=date("Y-m-d");

    $addlogin=$_GET["addlogin"];

    echo "<p><a name=punch></a><center><table width=90% cellspacing=0 cellpadding=0><tr bgcolor=#E8E7E7><td width=50% align=left><form action=add_punch.php method=post onSubmit='return disableForm(this);'><input type=hidden name=date1 value=$printdate1><input type=hidden name=date2 value=$printdate2><input type=hidden name=businessid value=$businessid><input type=hidden name=companyid value=$companyid>Add Punch: <select name=loginid>";

    if($showwhich==1){$query = "SELECT login.loginid,login.firstname,login.lastname FROM login,payroll_departmentdetail,security_departments WHERE login.oo2 = '$businessid' AND login.is_deleted = '0' AND (login.loginid = payroll_departmentdetail.loginid AND payroll_departmentdetail.dept_id = security_departments.dept_id AND security_departments.securityid = '$mysecurity' AND payroll_departmentdetail.dept_id = '$showdept_id') ORDER BY login.lastname DESC";}
    elseif($showwhich==2){$query = "SELECT login.loginid,login.firstname,login.lastname FROM login,payroll_departmentdetail,security_departments WHERE login.oo2 = '$businessid' AND login.is_deleted = '0' AND (login.loginid = payroll_departmentdetail.loginid AND payroll_departmentdetail.dept_id = security_departments.dept_id AND security_departments.securityid = '$mysecurity' AND login.loginid = '$showdept_id') ORDER BY login.lastname DESC";}
    else{$query = "SELECT login.loginid,login.firstname,login.lastname FROM login,payroll_departmentdetail,security_departments WHERE login.oo2 = '$businessid' AND login.is_deleted = '0' AND (login.loginid = payroll_departmentdetail.loginid AND payroll_departmentdetail.dept_id = security_departments.dept_id AND security_departments.securityid = '$mysecurity') ORDER BY login.lastname DESC";}
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);

    $num--;
    echo "<option value=></option>";
    while($num>=0){
       $loginid=Treat_DB_ProxyOld::mysql_result($result,$num,"loginid");
       $firstname=Treat_DB_ProxyOld::mysql_result($result,$num,"firstname");
       $lastname=Treat_DB_ProxyOld::mysql_result($result,$num,"lastname");

       $query2 = "SELECT * FROM jobtypedetail WHERE loginid = '$loginid' AND is_deleted = '0'";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=Treat_DB_ProxyOld::mysql_numrows($result2);

       $num2--;
       while($num2>=0){
          $job=Treat_DB_ProxyOld::mysql_result($result2,$num2,"jobtype");

          $query3 = "SELECT * FROM jobtype WHERE jobtypeid = '$job'";
          $result3 = Treat_DB_ProxyOld::query($query3);

          $jobname=Treat_DB_ProxyOld::mysql_result($result3,0,"name");

          if($loginid==$addlogin){$showsel="SELECTED";}
          else{$showsel="";}

          echo "<option value='$loginid:$job' $showsel>$lastname, $firstname: $jobname</option>";

          $num2--;
       }

       $num--;
    }

    echo "</select> <SCRIPT LANGUAGE='JavaScript' ID='js14'> var cal14 = new CalendarPopup('testdiv1'); cal14.setCssPrefix('TEST');</SCRIPT> <A HREF='javascript:void();' onClick=cal14.select(document.forms[4].date3,'anchor14','yyyy-MM-dd'); return false; TITLE=cal14.select(document.forms[4].date1,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor14' ID='anchor14'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A> <input type=text name=date3 value='$date3' size=8> <input type=submit value='Add'> $showsuccess</td><td></form></td></tr></table></center><p>";

    /*
    if($labor_notify_email==""){$labor_notify_email="<i>Noone</i>";}
    echo "<center><table width=90% cellspacing=0 cellpadding=0 bgcolor=#E8E7E7><tr><td>Labor notifications sent to: <font color=#444455>$labor_notify_email</font></td></tr></table></center><p>";
    */

    //mysql_close(); 

    echo "<br><FORM ACTION=businesstrack.php method=post>";
    echo "<center><INPUT TYPE=submit VALUE=' Return to Main Page'></FORM></center><DIV ID=testdiv1 STYLE=position:absolute;visibility:hidden;background-color:white;layer-background-color:white;></DIV></body>";
	
	google_page_track();
}
?>
