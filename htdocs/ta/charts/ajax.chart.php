<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');

// ajax fix for ie
mb_internal_encoding("UTF-8");
mb_http_output("UTF-8");
ob_start("mb_output_handler");

function error_exit( $errorMessage )
{
	if( stristr( $_SERVER['HTTP_ACCEPT'], 'application/json' ) )
		$errorOut = '{"error":"'.$errorMessage.'"}';
	else $errorOut = '<p>'.$errorMessage.'</p>';

	echo $errorOut;
	exit( 1 );
}

$rqFile = $_SERVER['PHP_SELF'];

// initialize db connection so mysql_real_escape_string can be used
$query = 'SELECT 1';
Treat_DB_ProxyOld::query( $query );

$user = isset($_COOKIE["usercook"])?mysql_real_escape_string($_COOKIE["usercook"]):'';
$pass = isset($_COOKIE["passcook"])?mysql_real_escape_string($_COOKIE["passcook"]):'';
$query = "
	SELECT
		login.*,
		security.dashboard AS sec_dashboard
	FROM login
	JOIN security ON login.security = security.securityid
	WHERE username = '$user'
	AND password = '$pass'
";
$result = Treat_DB_ProxyOld::query($query);
$num=mysql_num_rows($result);

$loginid = @mysql_result( $result, 0, 'loginid' );
$sec_dashboard = $bypassSec ? 4 : @mysql_result( $result, 0, 'sec_dashboard' );

$companyid = $_COOKIE['compcook'];

$fn = Treat_Filter::inputGet( 'fn' );
$viewid = intval( Treat_Filter::inputGet( 'viewid' ) );
$groupid = intval( Treat_Filter::inputGet( 'groupid' ) );
$isPublished = $groupid == 'custom' ? false : true;

if( $groupid > 0 ) {
	$query = "
		SELECT
			1
		FROM db_public
		JOIN db_group USING( groupname )
		WHERE groupid = $groupid
	";
} elseif( !empty( $_GET['groupname'] ) ) {
	$groupname = mysql_real_escape_string( $_GET['groupname'] );
	$query = "
		SELECT
			1
		FROM db_public
		WHERE groupname = '$groupname'
	";
}

$result = !empty( $query ) ? Treat_DB_ProxyOld::query( $query ) : false;
$bypass = @mysql_num_rows( $result ) == 1;

$bypassSec = $_GET['bypass'] == 'essent1al';

if( !$bypass && $num != 1 ){
	error_exit( 'Not logged in' );
}

if( !$bypass && $sec_dashboard < 2 ){
	error_exit( 'Access denied' );
}

require_once('charts.php');

$fnArray = array(
	'getChart' => array( 'security' => 2, 'bypass' => true ),
	'getJson' => array( 'security' => 2, 'bypass' => true ),
	'getTable' => array( 'security' => 2, 'bypass' => true ),
	'getDefaultGroup' => array( 'security' => 2 ),
	'getDashboard' => array( 'security' => 2, 'bypass' => true ),
	'addChartToDb' => array( 'security' => 4 ),
	'getDbList' => array( 'security' => 2 ),
	'insertChart' => array( 'security' => 4 ),
	'removeChart' => array( 'security' => 4 ),
	'discardChart' => array( 'security' => 4 ),
	'publishChart' => array( 'security' => 4 ),
	'getGroupList' => array( 'security' => 2 ),
	'getGroupMenu' => array( 'security' => 2 ),
	'newGroup' => array( 'security' => 4 ),
	'removeDb' => array( 'security' => 4 ),
	'deleteDb' => array( 'security' => 4 ),
	'shareDb' => array( 'security' => 4 ),
	'shareAddUser' => array( 'security' => 4 ),
	'shareRemoveUser' => array( 'security' => 4 ),
	'getUserList' => array( 'security' => 4 ),
	'getShareList' => array( 'security' => 4 ),
	'addGroup' => array( 'security' => 4 ),
	'editChart' => array( 'security' => 4 ),
	'addGraph' => array( 'security' => 4 ),
	'displayWhat' => array( 'security' => 4 ),
	'displayCustom' => array( 'security' => 4 ),
	'displayWho' => array( 'security' => 4 ),
	'deleteChart' => array( 'security' => 4 ),
	'updateOrder' => array( 'security' => 4 ),
	'getTrendlines' => array( 'security' => 4 ),
	'editTrendline' => array( 'security' => 4 ),
	'deleteTrendline' => array( 'security' => 4 ),
	'exportData' => array( 'security' => 2, 'bypass' => true ),
	'clearCache' => array( 'security' => 2, 'bypass' => true ),
	'getGroupId' => array( 'security' => 0 )
);

if( ( $fnS = $fnArray[$fn] ) ){
	if( $fnS['security'] <= $sec_dashboard
		|| ( $fnS['bypass'] == true && $bypass ) ){
		$fn();
	} else error_exit( 'Access denied' );
} else error_exit( 'Bad function' );





//// ajax functions ////


function getChart( )
{
	global $rqFile, $loginid, $sec_dashboard, $fn, $viewid, $groupid, $isPublished;
	$chart = new eeChart( $viewid, $isPublished );
	header( 'Content-type: text/xml' );

	$dashboardWho = $_GET['who'];
	if( !empty( $dashboardWho ) )
		$chart->setWho( $dashboardWho );
	echo $chart->getChart( );
}

function getJson( )
{
	global $rqFile, $loginid, $sec_dashboard, $fn, $viewid, $groupid, $isPublished;
	$chart = new eeChart( $viewid, $isPublished );
	echo $chart->getJson( );
}

function getTable( )
{
	global $rqFile, $loginid, $sec_dashboard, $fn, $viewid, $groupid, $isPublished;
	$chart = new eeChart( $viewid, $isPublished );

	$dashboardWho = intval( $_GET['who'] );
	if( $dashboardWho > 0 )
		$chart->setWho( $dashboardWho );
	echo $chart->getTable( );
}

function getDefaultGroup( )
{
	global $rqFile, $loginid, $sec_dashboard, $fn, $viewid, $groupid, $isPublished, $companyid;
	
	$order = $sec_dashboard < 4 ? "ORDER BY dbDefault DESC" : '';
	
	$query = "SELECT groupid FROM vDbGroupList WHERE dbDefault=true OR loginid=$loginid $order LIMIT 1";
	$result = Treat_DB_ProxyOld::query( $query );
	$groupid = @mysql_result( $result, 0, 'groupid' );
	$groupid = $groupid ?: 'custom';
	
	echo json_encode( array( 'o' => 'o', 'groupid' => $groupid ) );
}

function getGroupId( ) {
	global $rqFile, $loginid, $sec_dashboard, $fn, $viewid, $groupid, $isPublished;

	$groupname = mysql_real_escape_string( $_GET['groupname'] );
	$query = "SELECT groupid FROM vDbGroupList WHERE groupname='$groupname' LIMIT 1";
	$result = Treat_DB_ProxyOld::query( $query );
	$groupid = @mysql_result( $result, 0, 'groupid' );
	$groupid = $groupid ?: 'custom';
	echo json_encode( array( 'o' => 'o', 'groupid' => $groupid ) );
}

function getDashboard( )
{
	global $rqFile, $loginid, $sec_dashboard, $fn, $viewid, $groupid, $isPublished, $companyid;
	$queryStandard = "
			SELECT
				*
			FROM vDbGroup
			WHERE (
				dbPublic = 1
				OR
				$loginid IN (
					SELECT loginid FROM vDbGroupList WHERE groupid='$groupid'
				)
			) AND vGroupId='$groupid'
			ORDER BY dbOrder
	";
	$queryCustom = "
			SELECT
				*
			FROM vDbCustom
			WHERE loginid=$loginid
			ORDER BY dbOrder
	";
	$queryGroup = "
			SELECT
				groupowner,
				db_public.dbDefault
			FROM db_group
			JOIN vDbGroupMembersOwners USING( groupid )
			LEFT JOIN db_public USING( groupname )
			WHERE groupid=$groupid
			AND (
				db_public.id IS NOT NULL
				OR
				loginid=$loginid
			)
	";

	$isOwner = 0;
	$dashboardWho = '';

	if( $groupid == 'custom' ) {
		if( $sec_dashboard < 4 ){
			error_exit( 'Access denied' );
			return;
		}
		$query = $queryCustom;
		$buttonStr = array( 'addNewChart' );
		$isOwner = 1;
	} else {
		$query = $queryStandard;
		$buttonStr = array( 'shareDashboard', 'removeDashboard', 'addChartToDb' );

		$result = Treat_DB_ProxyOld::query( $queryGroup );

		if( mysql_num_rows( $result ) == 0 ){
			error_exit( 'Access denied' );
			return;
		}

		$groupOwner = @mysql_result( $result, 0, 'groupowner' );
		if( $groupOwner == $loginid )
			$isOwner = 1;
		
		$dbDefault = @mysql_result( $result, 0, 'dbDefault' );
		if( $dbDefault && !$isOwner ) {
			if( !( $companyid > 0 ) ) {
				echo json_encode( array( 'error' => 'Could not determine company id' ) );
				return;
			}
		
			$queryCompany_type = "SELECT company_type FROM company WHERE companyid=$companyid";
			$result = Treat_DB_ProxyOld::query( $queryCompany_type );
			$company_type = @mysql_result( $result, 0, 'company_type' );
		
			$dbWho = array(
						"company_type-$company_type",
						"companyid-$companyid"
			);
			$dashboardWho = implode( '+', $dbWho );
		}
	}

	Treat_Model_Dashboard_Cache::clean( );

	$result = Treat_DB_ProxyOld::query( $query );

	$charts = array( );

	$i = 0;
	while( $r = mysql_fetch_array( $result ) ) {
		$charts[$i]['id'] = $r["viewid"];
		$charts[$i]['title'] = $r["name"];
		$charts[$i]['ctype'] = eeChart::$ChartType[$r["graph_type"]]['type'];
		$charts[$i]['size'] = $r['dbSide'];
		$charts[$i]['height'] = $r['dbHeight'];

		if( $groupid == 'custom' )
		{
			$charts[$i]['mtime'] = $r['mtime'];
			$charts[$i]['ptime'] = $r['ptime'];
		}

		$charts[$i]['cacheDate'] = $r['cacheDate'];
		$charts[$i]['cacheId'] = $r['cacheId'];

		$i++;
	}

	$buttonList = array(
		'addNewChart' => array(
			'name' => 'Add new chart',
			'icon' => 'ui-icon-plusthick',
			'dialog' => 'editChartDialog',
			'fn' => 'editChart'
		),
		'shareDashboard' => array(
			'name' => 'Share Dashboard',
			'icon' => 'ui-icon-star',
			'dialog' => 'editDbDialog',
			'fn' => 'shareDb'
		),
		'removeDashboard' => array(
			'name' => 'Remove Dashboard',
			'icon' => 'ui-icon-trash',
			'dialog' => 'editDbDialog',
			'fn' => 'removeDb'
		),
		'addChartToDb' => array(
			'name' => 'Insert Chart',
			'icon' => 'ui-icon-plusthick',
			'dialog' => 'editChartDialog',
			'fn' => 'addChartToDb'
		)
	);

	foreach( $buttonStr as $button )
		$buttons[] = array_merge( array( 'id' => $button ), $buttonList[$button] );
	
	echo json_encode( array( 'charts' => $charts, 'buttons' => $buttons, 'owner' => $isOwner, 'dbWho' => $dashboardWho ) );
}

function addChartToDb( )
{
	global $rqFile, $loginid, $sec_dashboard, $fn, $viewid, $groupid, $isPublished;
?>
		<script language="javascript" type="text/javascript">
			jQuery(function(){
				jQuery('.ajaxForm').ajaxForm({
					dataType: 'json',
					beforeSerialize: function( f, o ){
						jQuery('#viewid').val( jQuery('#viewlist').val() );
					},
					success: function( data, status, f ){
						if( data.viewid > 0 )
						{
							var d = f.parent();
							d.html( '<p>Chart added to dashboard.</p>' );
							d.data( 'dirty', true );
							d.data( 'viewid', data.viewid );
							window.setTimeout( function(){ d.dialog( 'close' ); }, 3000 );
						} else {
							var msg = data.error && data.error.length > 0
										? data.error
										: 'Error adding chart to dashboard';
							alert( msg );
						}
					}
				});

				jQuery('#viewlist')
					.load('<?=$rqFile?>?fn=getDbList&groupid='+getDashboardGroup());
				jQuery('#groupid').val( getDashboardGroup() );
			});
		</script>
		<form class="ajaxForm" action="<?=$rqFile?>?fn=insertChart" method="post">
			<label for="viewlist">Choose a chart:</label>
			<input id="viewid" type="hidden" name="viewid" value="0" />
			<input id="groupid" type="hidden" name="groupid" value="" />
			<select id="viewlist" name="viewlist"></select>
			<input type="submit" value="Add" />
		</form>
<?php
}

function getDbList( )
{
	global $rqFile, $loginid, $sec_dashboard, $fn, $viewid, $groupid, $isPublished;
	$query = "
		SELECT
			g.*
		FROM pub_db_view g
		LEFT JOIN db_group_graphs db
		ON db.viewid = g.viewid
		AND db.groupid=$groupid
		WHERE loginid=$loginid
		AND db.viewid IS NULL
		ORDER BY name
	";

	$result = Treat_DB_ProxyOld::query( $query );

	while( $r = mysql_fetch_assoc( $result ) ){
		echo '<option value="'.$r['viewid'].'">'.$r['name'].'</option>';
	}
}

function insertChart( )
{
	global $rqFile, $loginid, $sec_dashboard, $fn, $viewid, $groupid, $isPublished;
	$viewid = intval($_POST['viewid']);
	$groupid = intval($_POST['groupid']);
	$return = array( 'viewid' => $viewid );
	$query = "
		INSERT INTO db_group_graphs( viewid, groupid )
			SELECT
				$viewid AS viewid,
				$groupid AS groupid
			FROM db_group
			WHERE groupid=$groupid
			AND groupowner=$loginid
	";
	Treat_DB_ProxyOld::query( $query );

	if( !(mysql_affected_rows() > 0) ) {
		$return = array( 'error' => mysql_errno() ? mysql_error() : 'There was an error processing your request' );
	}

	echo json_encode( $return );
}

function removeChart( )
{
	global $rqFile, $loginid, $sec_dashboard, $fn, $viewid, $groupid, $isPublished;
	$query = "
		DELETE FROM db_group_graphs
		WHERE viewid=$viewid
		AND groupid=$groupid
		AND groupid IN (
			SELECT
				groupid
			FROM db_group
			WHERE groupowner=$loginid
		)
	";
	Treat_DB_ProxyOld::query( $query );

	if( !(mysql_affected_rows() > 0) ) {
		$return = array( 'error' => mysql_errno() ? mysql_error() : 'There was an error processing your request' );
		echo json_encode( $return );
	}
}

function discardChart( )
{
	global $rqFile, $loginid, $sec_dashboard, $fn, $viewid, $groupid, $isPublished;

	$loginCheck = " AND loginid=$loginid";
	foreach( eeChart::$PublishTables as $t ) {
		$l = $t == 'db_view' ? $loginCheck : '';
		$query = "DELETE FROM $t WHERE viewid=$viewid$l";
		Treat_DB_ProxyOld::query( $query );

		$query = "INSERT INTO $t SELECT * FROM pub_$t WHERE viewid=$viewid$l";
		Treat_DB_ProxyOld::query( $query );

		if( mysql_errno() ) {
			echo json_encode( array( 'error' => 'Error inserting to '.$t.' ('.mysql_error().')' ) );
			return;
		}
	}
}

function publishChart( )
{
	global $rqFile, $loginid, $sec_dashboard, $fn, $viewid, $groupid, $isPublished;

	$loginCheck = " AND loginid=$loginid";
	foreach( eeChart::$PublishTables as $t ) {
		$l = $t == 'db_view' ? $loginCheck : '';

		$query = "DELETE FROM pub_$t WHERE viewid=$viewid$l";
		Treat_DB_ProxyOld::query( $query );

		$query = "INSERT INTO pub_$t SELECT * FROM $t WHERE viewid=$viewid$l";
		Treat_DB_ProxyOld::query( $query );

		if( mysql_errno() ) {
			echo json_encode( array( 'error' => 'Error inserting to '.$t.' ('.mysql_error().')' ) );
			return;
		}
	}
}

function getGroupList( )
{
	global $rqFile, $loginid, $sec_dashboard, $fn, $viewid, $groupid, $isPublished;
	
	$order = $sec_dashboard < 4 ? 'dbDefault DESC,' : '';
	
	$query = "
		SELECT DISTINCT
			groupid,
			groupname,
			CASE
				WHEN groupowner='custom' THEN '2'
				WHEN groupowner=$loginid THEN '1'
				ELSE '0'
			END AS isOwner
		FROM vDbGroupList
		WHERE loginid=$loginid OR dbDefault=true
		ORDER BY $order isOwner DESC, groupname
	";
	$result = Treat_DB_ProxyOld::query( $query );

	$groups = array( );

	$ia = array( 0, 0, 0 );
	while( $r = mysql_fetch_array( $result ) )
	{
		$isOwner = $r['isOwner'];
		$i = ++$ia[$isOwner];
		$groups[$isOwner][$i]['groupid'] = $r['groupid'];
		$groups[$isOwner][$i]['groupname'] = $r['groupname'];
		$groups[$isOwner][$i]['isOwner'] = $isOwner;
	}

	if( $sec_dashboard == 4 ){
		$i = ++$ia[1];
		$groups[1][$i]['groupid'] = 'add';
		$groups[1][$i]['groupname'] = 'Add new group...';
		$groups[1][$i]['isOwner'] = -1;
	}

	echo json_encode( array( 'groups' => $groups ) );
}

function deprecated_getGroupMenu( )
{
	global $rqFile, $loginid, $sec_dashboard, $fn, $viewid, $groupid, $isPublished;
	$query = "select groupid, groupname from db_group";
	$result = Treat_DB_ProxyOld::query( $query );

	$groupid = $_GET['groupid'] ?: -1;

	echo '<option value="custom" '.($groupid == -1 ? 'selected="selected"' : '').'>My Charts</option>';
	while( $dbGroup = mysql_fetch_assoc( $result ) )
	{
		$c = ( $dbGroup['groupid'] == $groupid ) ? 'selected="selected"' : '';
		echo "<option value='{$dbGroup['groupid']}' $c>{$dbGroup['groupname']}</option>";
	}
	echo '<option value="add">Add new group...</option>';
}

function newGroup( )
{
	global $rqFile, $loginid, $sec_dashboard, $fn, $viewid, $groupid, $isPublished;
?>
		<script language="javascript" type="text/javascript">
			jQuery(function(){
				jQuery('.ajaxForm').ajaxForm({
					dataType: 'json',
					success: function( data, status, f ){
						if( data.groupid > 0 )
						{
							var d = f.parent();
							d.html( '<p>Dashboard created.</p>' );
							d.data( 'dirty', true );
							window.setTimeout( function(){ d.dialog( 'close' ); }, 3000 );
						} else {
							var msg = data.error && data.error.length > 0
										? data.error
										: 'Error creating new dashboard group';
							alert( msg );
						}
					}
				});
			});
		</script>
		<form class="ajaxForm" action="<?=$rqFile?>?fn=addGroup" method="post">
			<label for="groupname">Group Name:</label>
			<input type="text" name="groupname" value="new group" />
			<input type="submit" value="Add" />
		</form>
<?php
}

function removeDb( )
{
	global $rqFile, $loginid, $sec_dashboard, $fn, $viewid, $groupid, $isPublished;
?>
		<script language="javascript" type="text/javascript">
			jQuery(function(){
				jQuery('.ajaxForm').ajaxForm({
					dataType: 'json',
					beforeSerialize: function( f, o ){
						jQuery('#groupid').val( getDashboardGroup() );
					},
					success: function( data, status, f ){
						if( data.groupid > 0 || data.groupid == 'custom' )
						{
							var d = f.parent();
							d.html( '<p>Dashboard removed.</p>' );
							window.setTimeout( function(){
								d.dialog( 'close' );
								setDashboardGroup( data.groupid );
								createDbMenu( );
								createDashboard( );
							}, 3000 );
						} else {
							var msg = data.error && data.error.length > 0
								? data.error
								: 'Error deleting dashboard group';
							alert( msg );
						}
					}
				});
			});
		</script>
		<form class="ajaxForm" action="<?=$rqFile?>?fn=deleteDb" method="post">
			<p>Are you sure you want to remove this dashboard?
			This action cannot be undone.</p>
			<input type="hidden" id="groupid" name="groupid" value="" />
			<input type="submit" value="Remove dashboard permanently" />
		</form>
<?php
}

function deleteDb( )
{
	global $rqFile, $loginid, $sec_dashboard, $fn, $viewid, $groupid, $isPublished;
	$groupid = intval($_POST['groupid']);

	$query = "
		SELECT
			groupid
		FROM vDbGroupList
		WHERE loginid=$loginid
	";
	$result = Treat_DB_ProxyOld::query( $query );
	$newgroupid = 0;
	while( $r = mysql_fetch_assoc( $result ) ){
		if( $r['groupid'] == $groupid ){
			$nextrow = mysql_fetch_assoc( $result );
			if( is_array( $nextrow ) && !empty( $nextrow['groupid'] ) )
				$newgroupid = $nextrow['groupid'];
			elseif( is_array( $lastrow ) && !empty( $lastrow['groupid'] ) )
				$newgroupid = $lastrow['groupid'];
			else $newgroupid = 'custom';
			break;
		}
		$lastrow = $r;
	}

	$query = "DELETE FROM db_group WHERE groupid=$groupid AND groupowner=$loginid";
	$result = Treat_DB_ProxyOld::query( $query );

	if( mysql_affected_rows( ) > 0 ){
		$query = "DELETE FROM db_group_members WHERE groupid=$groupid";
		Treat_DB_ProxyOld::query( $query );
		$query = "DELETE FROM db_group_graphs WHERE groupid=$groupid";
		Treat_DB_ProxyOld::query( $query );
		echo '{"groupid":"'.$newgroupid.'"}';
	} else echo '{"groupid":"-1"}';
}

function shareDb( )
{
	global $rqFile, $loginid, $sec_dashboard, $fn, $viewid, $groupid, $isPublished;
?>
		<script language="javascript" type="text/javascript">
			jQuery(function(){
				jQuery('.ajaxForm').ajaxForm({
					dataType: 'json',
					beforeSerialize: function( f, o ){
						jQuery('#userid').val( jQuery('#loginlist').val() );
						jQuery('#groupid').val( getDashboardGroup() );
					},
					success: loadLists
				});

				jQuery('#sharelist .ajaxButton').live('click',function(){
					jQuery.ajax({
						url: jQuery(this).attr('href')+getDashboardGroup(),
						dataType: 'json',
						success: loadLists
					});
					return false;
				});

				function loadLists( ){
					jQuery('#loginlist')
						.load('<?=$rqFile?>?fn=getUserList&groupid='+getDashboardGroup());
					jQuery('#sharelist')
						.load('<?=$rqFile?>?fn=getShareList&groupid='+getDashboardGroup());
				}

				loadLists( );
			});
		</script>
		<div class="shareList">
			<ul id="sharelist">
			</ul>
		</div>
		<div class="userList">
			<form class="ajaxForm" action="<?=$rqFile?>?fn=shareAddUser" method="post">
				<input type="hidden" id="userid" name="userid" value="" />
				<input type="hidden" id="groupid" name="groupid" value="" />
				<select id="loginlist"></select><br />
				<input type="submit" value="Add" />
			</form>
		</div>
<?php
}

function shareAddUser( )
{
	global $rqFile, $loginid, $sec_dashboard, $fn, $viewid, $groupid, $isPublished;
	$groupid = intval($_POST['groupid']);
	$userid = intval($_POST['userid']);
	$query = "
		INSERT INTO db_group_members( groupid, loginid )
			SELECT
				$groupid AS groupid,
				$userid AS loginid
			FROM db_group
			WHERE groupowner=$loginid
			AND groupid=$groupid
	";
	$result = Treat_DB_ProxyOld::query( $query );
	echo json_encode( array( 'groupid' => $groupid, 'userid' => $userid ) );
}

function shareRemoveUser( )
{
	global $rqFile, $loginid, $sec_dashboard, $fn, $viewid, $groupid, $isPublished;
	$userid = intval($_GET['userid']);
	$query = "
		DELETE FROM db_group_members
		WHERE groupid=$groupid
		AND loginid=$userid
		AND $loginid IN (
			SELECT groupowner FROM db_group WHERE groupid=$groupid
		)
	";
	$result = Treat_DB_ProxyOld::query( $query );
	echo json_encode( array( 'groupid' => $groupid, 'userid' => $userid ) );
}

function getUserList( )
{
	global $rqFile, $loginid, $sec_dashboard, $fn, $viewid, $groupid, $isPublished;
	$query = '
		SELECT
			l.loginid,
			CONCAT(lastname,", ",firstname) as name
		FROM login l
		INNER JOIN security s
			ON s.securityid = l.security
			AND s.dashboard >= 2
		LEFT JOIN db_group_members db
			ON l.loginid = db.loginid
			AND db.groupid = '.$groupid.'
		LEFT JOIN db_group g
			ON g.groupid = '.$groupid.'
			AND g.groupowner = l.loginid
		WHERE db.groupid IS NULL
		AND g.groupid IS NULL
		ORDER BY name
	';
	$result = Treat_DB_ProxyOld::query( $query );
	while( $r = mysql_fetch_assoc( $result ) ){
		echo '<option value="'.$r['loginid'].'">'.$r['name'].'</option>';
	}
}

function getShareList( )
{
	global $rqFile, $loginid, $sec_dashboard, $fn, $viewid, $groupid, $isPublished;
	$query = '
		SELECT
			loginid,
			CONCAT(lastname,", ",firstname) as name
		FROM login
		JOIN db_group_members USING( loginid )
		WHERE groupid='.$groupid.'
		ORDER BY name
	';
	$result = Treat_DB_ProxyOld::query( $query );
	while( $r = mysql_fetch_assoc( $result ) ){
		echo '<li id="'.$r['loginid'].'">'.$r['name'].' ';
		echo '<a class="ajaxButton"';
		echo 'href="'.$rqFile.'?fn=shareRemoveUser&userid='.$r['loginid'].'&groupid="';
		echo '><img src="/assets/images/close2.gif" height=10 width=10 border=0></a>';
		echo '</li>';
	}
}

function addGroup( )
{
	global $rqFile, $loginid, $sec_dashboard, $fn, $viewid, $groupid, $isPublished;
	$groupname = mysql_real_escape_string($_POST['groupname']);
	$query = "INSERT INTO db_group SET groupname='$groupname', groupowner=$loginid";
	$result = Treat_DB_ProxyOld::query( $query );
	$groupid = mysql_insert_id( );

	echo '{ "groupid" : "'.$groupid.'" }';
}

function editChart( )
{
	global $rqFile, $loginid, $sec_dashboard, $fn, $viewid, $groupid, $isPublished;
		if ($viewid>0){
						$showsave="Save";

						$query = "SELECT * FROM db_view WHERE viewid = '$viewid'";
						$result = Treat_DB_ProxyOld::query($query);

						$name = mysql_result($result,0,"name");
						$graph_type = mysql_result($result,0,"graph_type");
						$size = mysql_result($result,0,"size");
						$xaxis = mysql_result($result,0,"xaxis");
						$db_order = mysql_result($result,0,"db_order");
						$period = mysql_result($result,0,"period");
						$period_num = mysql_result($result,0,"period_num");
						$period_group = mysql_result($result,0,"period_group");
						$current = mysql_result($result,0,"current");
						$week_ends = mysql_result($result,0,"week_ends");

						$trendEnabled = 'false';

						if($size==2){$size_sel="SELECTED";}

						$xaxis_sel = $xaxis == 2 ? "SELECTED" : '';

						if($current==2){$cur_sel="SELECTED";}

						if($period==2){$period_sel2="SELECTED";}
						elseif($period==3){$period_sel3="SELECTED";}
						elseif($period==4){$period_sel4="SELECTED";}

						if($period_group==2){$period_group_sel2="SELECTED";}
						elseif($period_group==3){$period_group_sel3="SELECTED";}
						elseif($period_group==4){$period_group_sel4="SELECTED";}
				}
				else{$showsave="Add";}
		?>
		<script language="javascript" type="text/javascript">
			function reloadDialog( viewid, extraParams )
			{
				if( viewid > 0 )
				{
					jQuery('.ui-dialog-content').data( 'dirty', true );
					jQuery('.ui-dialog-content').data( 'viewid', viewid );
					jQuery('.ui-dialog-content').load('<?=$rqFile?>?fn=editChart&viewid='+viewid+extraParams);
				}
			}

			function reloadDialogJSON( data )
			{
				if( data.error )
					alert( data.error );
				else reloadDialog( data.viewid, '' );
			}

			jQuery(function(){
				jQuery('.ajaxForm').ajaxForm({
					dataType: 'json',
					success: reloadDialogJSON
				});

				jQuery('.ajaxButton').click(function(){
					jQuery.ajax({
						url: jQuery(this).attr('href'),
						dataType: 'json',
						success: reloadDialogJSON
					});
					return false;
				});
			});
		</script>
		<form class="ajaxForm" action="<?=$rqFile?>?fn=addGraph" method="post" style="margin:0;padding:0;display:inline;">
		<table width=100% cellspacing=0 style="float: left; font-size: 12px;">
		<tr>
		<td colspan='2' style="border: 2px solid #777777;" bgcolor="#777777">&nbsp;
			<font color="white"><b>Graph Details</b></font>
			<input type="submit" style="float: right; font-size: 12px;" value=" <? echo $showsave ?> ">
		</td>
		</tr>
		<tr><td width='50%' style="border-left: 2px solid #777777; border-bottom: 2px solid #777777;">
			<input id="viewid" type="hidden" name="viewid" value="<? echo $viewid ?>">
			Name: <input type="text" style="font-size: 12px;" size="25" name="graph_name" value="<? echo $name ?>">
			<br>Style: <select name="graph_type" style="font-size: 12px;">
				<?php
					foreach( eeChart::$ChartType as $typeId => $typeValues )
						echo "<option value='$typeId'", $typeId == $graph_type ? " selected='selected'" : '',">{$typeValues['name']}</option>";
				?>
			</select><br />
			X-Axis: <select name="xaxis" style="font-size: 12px;">
				<option value="1">Dates</option>
				<option value="2" <?=$xaxis_sel?>>Units</option>
			</select>
			<!-- Location: <select name="size" style="font-size: 12px;">
				<option value="1">Left</option>
				<option value="2" <? echo $size_sel ?>>Right</option>
			</select> -->
		</td><td style="border-right: 2px solid #777777; border-bottom: 2px solid #777777;">
			Date Range:
			<select name="current" style="font-size: 12px;">
				<option value="1">Current</option>
				<option value="2" <? echo $cur_sel ?>>Last</option>
			</select>
			<input type="text" style="font-size: 12px;" size="5" name="period_num" value="<? echo $period_num ?>">
			<select name="period" style="font-size: 12px;">
				<option value="1">Day(s)</option>
				<option value="2" <? echo $period_sel2 ?>>Week(s)</option>
				<option value="3" <? echo $period_sel3 ?>>Month(s)</option>
				<option value="4" <? echo $period_sel4 ?>>Year(s)</option>
			</select><br />
			Dates Grouped By:
			<select name="period_group" style="font-size: 12px;">
				<option value="1">Day</option>
				<option value="2" <? echo $period_group_sel2 ?>>Week</option>
				<option value="3" <? echo $period_group_sel3 ?>>Month</option>
				<option value="4" <? echo $period_group_sel4 ?>>Year</option>
			</select>

			<?php
			if( $period_group == 2 ) {
				if( $week_ends == "Monday" ) {
					$selmon = "SELECTED";
				} elseif( $week_ends == "Tuesday" ) {
					$seltue = "SELECTED";
				} elseif( $week_ends == "Wednesday" ) {
					$selwed = "SELECTED";
				} elseif( $week_ends == "Thursday" ) {
					$selthu = "SELECTED";
				} elseif( $week_ends == "Friday" ) {
					$selfri = "SELECTED";
				} elseif( $week_ends == "Saturday" ) {
					$selsat = "SELECTED";
				} elseif( $week_ends == "Sunday" ) {
					$selsun = "SELECTED";
				}

				echo " <select name=week_ends><option value=\"Thursday\">Week Ends...</option>";
				echo "<option value=\"Monday\" $selmon>Monday</option>";
				echo "<option value=\"Tuesday\" $seltue>Tuesday</option>";
				echo "<option value=\"Wednesday\" $selwed>Wednesday</option>";
				echo "<option value=\"Thursday\" $selthu>Thursday</option>";
				echo "<option value=\"Friday\" $selfri>Friday</option>";
				echo "<option value=\"Saturday\" $selsat>Saturday</option>";
				echo "<option value=\"Sunday\" $selsun>Sunday</option>";
				echo "</select> ";
			}
			?>
		</td></tr></table>
		</form>
		<?php
		if( $viewid > 0 ) {
			/**********************
			 * DASHBOARD ADVANCED *
			 **********************/
		?>
		<script type="text/javascript" language="javascript">
		jQuery(function(){
			initializeAdvancedSettings( <?=$viewid?> );
		});
		</script>
		<table width="100%" cellspacing="0" style="float: left; font-size: 12px;">
			<tr><td bgcolor="#777777" class="valignMiddle" style="border: 2px solid #777777; color: white; font-weight: bold;">
				<span class="advancedHeader" id="advancedButton">show</span>
				<span class="advancedHeader">Advanced Settings</span>
			</td></tr>
			<tr style="margin-top: 2px"><td id="advancedSettings" style="border: 2px solid #777777;">
				<div style="float: left;">
					<label style="font-weight: bold;">Trendlines</label><br />
					<select id="trendlines" size="7">
					</select>
				</div>
				<form id="trendForm" class="ajaxForm" style="margin: 0; padding: 0; display: inline;" method="post"
					action="/ta/charts/ajax.chart.php?fn=editTrendline">
				<fieldset style="float: left;">
					<input type="hidden" name="trendid" value="new" />
					<input type="hidden" name="viewid" value="<?=$viewid?>" />
					<label for="lineStart">Start:</label>
					<input class="trendOption" type="text" name="lineStart" size="5" value="0" />
					<label for="lineEnd">End:</label>
					<input class="trendOption" type="text" name="lineEnd" size="5" value="0" /><br />
					<label for="trendCaption">Caption:</label>
					<input class="trendOption" type="text" name="trendCaption" size="15" value="" /><br />
					<label for="lineColor">Color:</label>
					<input class="trendOption" type="hidden" name="lineColor" value="ff0000" />
					<label for="lineWidth">Width:</label>
					<input class="trendOption" type="text" name="lineWidth" size="2" value="1" /><br />
					<!-- <label for="lineType">Type:</label>
					<select class="trendOption" name="lineType">
						<option value="0">Line</option>
						<option value="1">Area</option>
					</select> -->
					<label for="linePosition">Position:</label>
					<select class="trendOption" name="linePosition">
						<option value="0">Under data</option>
						<option value="1">Over data</option>
					</select><br />
					<input class="trendOption" type="submit" value="Save" />
					<button class="trendOption" id="deleteTrendline">Delete</button>
				</fieldset>
				</form>
			</td></tr></tbody>
		</table>
		<?php


			/******************
			 * DASHBOARD WHAT *
			 ******************/
			echo "<table width=50% cellspacing=0 style=\"float: left; font-size: 12px;\"><tr><td style=\"border:2px solid #777777;\" bgcolor=#777777>&nbsp;<font color=white><b>What</b></font></td></tr><tr valign=top>";

			/////add data
			echo "<td style=\"border:2px solid #777777;\">";

			echo "<center><table width=100% cellspacing=0 cellpadding=0 style=\"font-size: 12px;\"><tr valign=top><td width=50%>";
			////list what
			$lasttype = 0;
			$query = "SELECT * FROM db_what WHERE viewid = '$viewid'";
			$result = Treat_DB_ProxyOld::query( $query );
			$num = mysql_numrows( $result );

			if( $num == 0 ) {
				echo "&nbsp;<i>Nothing to Display</i><br>";
			} else {
				echo "<ul>";
				$count = 1;
				while( $r = mysql_fetch_array( $result ) ) {
					$id = $r["id"];
					$type = $r["type"];
					$what = $r["what"];

					$showdelete = "<a class='ajaxButton' href='$rqFile?fn=displayWhat&viewid=$viewid&id=$id&del=1' onclick=\"return confirm('Are you sure you want to delete?');\"><img src='/assets/images/close2.gif' height=10 width=10 border=0></a>";

					if( $type == 4 ) {
						$query2 = "SELECT category_name FROM po_category WHERE categoryid = '$what'";
						$result2 = Treat_DB_ProxyOld::query( $query2 );

						$what = mysql_result( $result2, 0, "category_name" );
					} elseif( $type == 5 ) {
						$query2 = "SELECT category_name FROM po_category WHERE budget_type = '$what'";
						$result2 = Treat_DB_ProxyOld::query( $query2 );

						$what = mysql_result( $result2, 0, "category_name" );
						$what .= " BGT";
					} elseif( $type == 6 ) {
						if( $what == 1 ) {
							$what = "Item Sales";
						} elseif( $what == 2 ) {
							$what = "Hourly Sales";
						} elseif( $what == 3 ) {
							$what = 'Ticket Count';
						} elseif( $what == 4 ) {
							$what = 'Item Sales (New)';
						} elseif( $what == 5 ) {
							$what = 'Unique Card Count';
						}
					} elseif( $what == "sales" ) {
						$what = "Sales";
					} elseif( $what == "cos" ) {
						$what = "COGS";
					} elseif( $what == "labor" ) {
						$what = "Labor";
					} elseif( $what == "toe" ) {
						$what = "TOE";
					} elseif( $what == "pl" ) {
						$what = "P&L";
					} elseif( $what == "sales_bgt" ) {
						$what = "Sales BGT";
					} elseif( $what == "cos_bgt" ) {
						$what = "COGS BGT";
					} elseif( $what == "labor_bgt" ) {
						$what = "Labor BGT";
					} elseif( $what == "toe_bgt" ) {
						$what = "TOE BGT";
					} elseif( $what == "pl_bgt" ) {
						$what = "P&L BGT";
					} elseif( $what == "customers" ) {
						$what = "Customers";
					} elseif( $what == "waste" ) {
						$what = "Waste";
					} elseif( $what == "shrink" ) {
						$what = "Shrink";
					} elseif( $type == 8 || $type == 10 ) {
						$query2 = "SELECT category FROM reporting_category WHERE id = $what";
						$result2 = Treat_DB_ProxyOld::query( $query2 );

						$cat_name = mysql_result($result2,0,"category");

						$what = "Category: $cat_name";
					} elseif( $what == "est_sales" ) {
						$what = "EST: Sales";
					} elseif( $what == "est_sales_bgt" ) {
						$what = "EST: Sales BGT";
					} elseif( $what == "est_lost" ) {
						$what = "EST: Lost";
					} elseif( $what == "est_lost_bgt" ) {
						$what = "EST: Lost BGT";
					} elseif( $what == "est_redlist" ) {
						$what = "EST: Redlist";
					} elseif( $what == "route_sales" ) {
						$what = "Route Collections";
					} elseif( $what == "avg_sales" ) {
						$what = "Average Collections";
					}

					echo "<li> [$count] $what $showdelete</li>";
					$count++;
					$lasttype = $type;
				}
				echo "</ul>";
			}

			echo "<form class='ajaxForm' action='$rqFile?fn=displayWhat' method=post style=\"margin:0;padding:0;display:inline;font-size: 12px;font-family: arial;\"><input type=hidden name=viewid value=$viewid>&nbsp;Options:";
			echo " <select name=display_what style=\"font-size: 12px;\">
																		<option value=''>Please Choose...</option>";
			if( $lasttype == 1 || $lasttype == 0 || $lasttype == 8 || $lasttype == 10) {
				echo "
																		<optgroup label='Accounting'>
																		<option value=1:sales>Sales</option>
																		<option value=1:cos>Cost of Sales</option>
																		<option value=1:labor>Labor</option>
																		<option value=1:toe>TOE</option>
																		<option value=1:pl>P&L</option>
																		<option value=1:sales_bgt>Sales Budget</option>
																		<option value=1:cos_bgt>Cost of Sales Budget</option>
																		<option value=1:labor_bgt>Labor Budget</option>
																		<option value=1:toe_bgt>TOE Budget</option>
																		<option value=1:pl_bgt>P&L Budget</option>
																		<option value=1:customers>Customers</option>
																		<option value=1:waste>Waste</option>
																		<option value=1:shrink>Shrink</option>";

																		////reporting categories (debits)
																		$query2 = "SELECT * FROM reporting_category WHERE type LIKE 'debits' ORDER BY category";
																		$result2 = Treat_DB_ProxyOld::query( $query2 );

																		while($r2 = mysql_fetch_array($result2)){
																			$new_catid = $r2["id"];
																			$new_catname = $r2["category"];

																			echo "<option value=8:$new_catid>$new_catname</option>";
																		}
																		
																		////reporting categories (debits)
																		$query2 = "SELECT * FROM reporting_category WHERE type LIKE 'credits' ORDER BY category";
																		$result2 = Treat_DB_ProxyOld::query( $query2 );

																		while($r2 = mysql_fetch_array($result2)){
																			$new_catid = $r2["id"];
																			$new_catname = $r2["category"];

																			echo "<option value=10:$new_catid>$new_catname</option>";
																		}

																		echo "</optgroup>";
			}
			if( $lasttype == 3 || $lasttype == 0 ) {
				echo "
																		<optgroup label='Vending'>
																		<option value='3:route_sales'>Route Collections</option>
																		<option value='3:avg_sales'>Average Collections</option>
																		</optgroup>";
			}
			if( $lasttype == 2 || $lasttype == 0 ) {
				echo "
																		<optgroup label='ESalesTrack'>
																		<option value=2:est_sales>Sales</option>
																		<option value=2:est_sales_bgt>Sales Budget</option>
																		<option value=2:est_lost>Lost</option>
																		<option value=2:est_lost_bgt>Lost Budget</option>
																		<option value=2:est_redlist>Red List</option>
																		</optgroup>";
			}
			if( $lasttype == 4 || $lasttype == 5 || $lasttype == 0 ) {
				echo "<optgroup label='CapEx'>";

				$query = "SELECT * FROM po_category ORDER BY category_name";
				$result = Treat_DB_ProxyOld::query( $query );

				while( $r = mysql_fetch_array( $result ) ) {
					$categoryid = $r["categoryid"];
					$category_name = $r["category_name"];
					$budget_type = $r["budget_type"];

					echo "<option value='4:$categoryid'>$category_name</option>";
					echo "<option value='5:$budget_type'>$category_name BGT</option>";
				}

				echo "</optgroup>";
			}
			if( $lasttype == 6 || $lasttype == 0 ) {
				echo "
																		<optgroup label='POS Menu'>
																		<option value=6:1>Item Sales</option>
																		<option value=6:2>Hourly Sales</option>
																		<option value=6:3>Ticket Count</option>
																		<option value=6:4>Item Sales (New)</option>
																		<option value=6:5>Unique Card Count</option>
																		</optgroup>";
			}

			echo "</select> <input type=submit style=\"font-size: 12px;\" value='Add'>";
			echo "</form>";

			echo "</td><td width=50%>";

			/////custom fields
			$query = "SELECT * FROM db_custom WHERE viewid = '$viewid'";
			$result = Treat_DB_ProxyOld::query( $query );
			$num1 = mysql_numrows( $result );

			if( $num1 == 0 ) {
				echo "&nbsp;<i>Nothing to Display</i><br>";
			} else {
				echo "<ul>";
				$count = 1;
				while( $r = mysql_fetch_array( $result ) ) {
					$id = $r["id"];
					$name = $r["name"];
					$custom_field = $r["custom_field"];
					$future_dates = $r["future_dates"];

					if( $future_dates == 1 ) {
						$f_color = "red";
					} else {
						$f_color = "black";
					}

					$showdelete = "<a class='ajaxButton' href='$rqFile?fn=displayCustom&viewid=$viewid&id=$id&del=1' onclick=\"return confirm('Are you sure you want to delete?');\"><img src='/assets/images/close2.gif' height=10 width=10 border=0></a>";

					echo "<li><font color=$f_color>$name: $custom_field</font> $showdelete";

					$count++;
				}
				echo "</ul>";
			}

			echo "<form class='ajaxForm' action='$rqFile?fn=displayCustom' method=post style=\"margin:0;padding:0;display:inline;font-size: 12px;font-family: arial;\"><input type=hidden name=viewid value=$viewid>&nbsp;<input type=text name=custom_name size=15 value='Name'><br>&nbsp;<input type=text name=custom_field value='Formula' size=15><br>&nbsp;<input type=checkbox name=future_dates value=1>Disable Future Dates <input type=submit value='Add'></form>";

			echo "</td></tr></table>";

			echo "</td></tr></table>";

			if( $num > 0 ) {
				////add who
				echo "<a name=bottom><table width=50% cellspacing=0 style=\"float: left; font-size: 12px;\"><tr><td style=\"border:2px solid #777777;\" bgcolor=#777777>&nbsp;<font color=white><b>Who</b></font></td></tr><tr valign=top><td style=\"border:2px solid #777777;\">";

				////list what
				$query = "SELECT * FROM db_who WHERE viewid = '$viewid'";
				$result = Treat_DB_ProxyOld::query( $query );
				$num = mysql_numrows( $result );

				if( $num == 0 ) {
					echo "&nbsp;<i>Nothing to Display</i><br>";
				} else {
					echo "<ul>";
					while( $r = mysql_fetch_array( $result ) ) {
						$id = $r["id"];
						$type = $r["type"];
						$viewtype = $r["viewtype"];
						$viewbus = $r["viewbus"];
						$viewbus2 = $r["viewbus2"];
						$expand = $r['expand'] == 1 ? ' expanded' : '';

						$showdelete = "<a class='ajaxButton' href='$rqFile?fn=displayWho&viewid=$viewid&id=$id&del=1' onclick=\"return confirm('Are you sure you want to delete?');\"><img src='/assets/images/close2.gif' height=10 width=10 border=0></a>";

						///get names
						if( $type == 1 || $type == 4 || $type == 5 || $type == 6 || $type == 8) {
							if( $viewtype == 1 ) {
								$showwho = "All Units";
							} elseif( $viewtype == 2 ) {
								$showwho = "Type";

								$query3 = "SELECT type_name FROM company_type WHERE typeid = '$viewbus'";
								$result3 = Treat_DB_ProxyOld::query( $query3 );

								$viewname = mysql_result( $result3, 0, "type_name" );
							} elseif( $viewtype == 3 ) {
								$showwho = "Company";

								$query3 = "SELECT companyname FROM company WHERE companyid = '$viewbus'";
								$result3 = Treat_DB_ProxyOld::query( $query3 );

								$viewname = mysql_result( $result3, 0, "companyname" );
							} elseif( $viewtype == 4 ) {
								$showwho = "Region";

								$query3 = "SELECT division_name FROM division WHERE divisionid = '$viewbus'";
								$result3 = Treat_DB_ProxyOld::query( $query3 );

								$viewname = mysql_result( $result3, 0, "division_name" );
							} elseif( $viewtype == 5 ) {
								$showwho = "District";

								$query3 = "SELECT districtname FROM district WHERE districtid = '$viewbus'";
								$result3 = Treat_DB_ProxyOld::query( $query3 );

								$viewname = mysql_result( $result3, 0, "districtname" );
							} elseif( $viewtype == 6 ) {
								$showwho = "Business";

								$query3 = "SELECT businessname FROM business WHERE businessid = '$viewbus'";
								$result3 = Treat_DB_ProxyOld::query( $query3 );

								$viewname = mysql_result( $result3, 0, "businessname" );
							}
						} elseif( $type == 2 ) {
							if( $viewtype == 1 ) {
								$showwho = "All Units";
								$viewname = "";
								$viewname2 = "";
							} elseif( $viewtype == 2 || $viewtype == 3 ) {
								$showwho = "Type";

								$query3 = "SELECT name FROM est_accountowner_type WHERE id = '$viewbus'";
								$result3 = Treat_DB_ProxyOld::query( $query3 );

								$viewname = mysql_result( $result3, 0, "name" );

								if( $viewbus2 == 0 ) {
									$viewname2 = "All Locations";
								} else {
									$query3 = "SELECT location FROM est_locations WHERE id = '$viewbus2'";
									$result3 = Treat_DB_ProxyOld::query( $query3 );

									$viewname2 = mysql_result( $result3, 0, "location" );
								}
							} elseif( $viewtype == 4 ) {
								$showwho = "Salesman";

								$query3 = "SELECT name FROM est_accountowner WHERE accountowner = '$viewbus'";
								$result3 = Treat_DB_ProxyOld::query( $query3 );

								$viewname = mysql_result( $result3, 0, "name" );
							}
						} elseif( $type == 3 ) {
							switch( $viewtype ) {
								case 1:
									$showwho = 'All Units';
									$viewname = '';
									break;

								case 2:
									$showwho = 'Business';

									$query3 = "SELECT businessname FROM business WHERE businessid = '$viewbus'";
									$result3 = Treat_DB_ProxyOld::query( $query3 );

									$viewname = @mysql_result ( $result3, 0, "businessname" );
									break;

								case 3:
									$showwho = 'Supervisor';

									$query3 = "SELECT supervisor_name FROM vend_supervisor WHERE supervisorid = '$viewbus'";
									$result3 = Treat_DB_ProxyOld::query( $query3 );

									$viewname = @mysql_result ( $result3, 0, "supervisor_name" );
									break;

								case 4:
									$showwho = 'Route';

									$query3 = "SELECT route FROM login_route WHERE login_routeid = '$viewbus'";
									$result3 = Treat_DB_ProxyOld::query( $query3 );

									$viewname = @mysql_result ( $result3, 0, "route" );
									break;
							}
						}

						///display
						echo "<li>$showwho: $viewname $viewname2$expand $showdelete</li>";
					}
					echo "</ul>";
				}

				////ACCOUNTING WHO
				if( $lasttype == 1 || $lasttype == 4 || $lasttype == 5 || $lasttype == 6 || $lasttype == 8) {
					$display_viewtype = intval($_GET["display_viewtype"]);
					if( $display_viewtype == 1 ) {
						$vtype1 = "SELECTED";
					} elseif( $display_viewtype == 2 ) {
						$vtype2 = "SELECTED";
					} elseif( $display_viewtype == 3 ) {
						$vtype3 = "SELECTED";
					} elseif( $display_viewtype == 4 ) {
						$vtype4 = "SELECTED";
					} elseif( $display_viewtype == 5 ) {
						$vtype5 = "SELECTED";
					} elseif( $display_viewtype == 6 ) {
						$vtype6 = "SELECTED";
					}

					echo "<form class='ajaxForm' action='$rqFile?fn=displayWho' method=post style=\"margin:0;padding:0;display:inline;font-size: 12px;font-family: arial;\"><input type=hidden name=viewid value=$viewid>&nbsp;Options:";
					echo " <select name=display_viewtype onChange='var s=this.form.display_viewtype; reloadDialog($viewid,\"&display_viewtype=\"+s.options[s.selectedIndex].value)' style=\"font-size: 12px;\">
																	<option>Choose A Level...</option>
																	<option value='1' $vtype1>All Units</option>
																	<option value='2' $vtype2>&nbsp;Type</option>
																	<option value='3' $vtype3>&nbsp;&nbsp;Company</option>
																	<option value='4' $vtype4>&nbsp;&nbsp;&nbsp;Region</option>
																	<option value='5' $vtype5>&nbsp;&nbsp;&nbsp;&nbsp;District</option>
																	<option value='6' $vtype6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Business</option>
																		</select>";

					if( $display_viewtype == 1 ) {
						echo "<input type=hidden name=viewtype value=$display_viewtype>";
					} elseif( $display_viewtype == 2 ) {
						echo "<input type=hidden name=viewtype value=$display_viewtype>";
						echo "<select name=display_viewbus style=\"font-size: 12px;\">";

						$query = "SELECT * FROM company_type ORDER BY type_name";
						$result = Treat_DB_ProxyOld::query( $query );

						while( $r = mysql_fetch_array( $result ) ) {
							$viewbus = $r["typeid"];
							$viewname = $r["type_name"];

							echo "<option value=$viewbus>$viewname</option>";
						}
						echo "</select>";
					} elseif( $display_viewtype == 3 ) {
						echo "<input type=hidden name=viewtype value=$display_viewtype>";
						echo "<select name=display_viewbus style=\"font-size: 12px;\">";

						$query = "SELECT * FROM company ORDER BY companyname";
						$result = Treat_DB_ProxyOld::query( $query );

						while( $r = mysql_fetch_array( $result ) ) {
							$viewbus = $r["companyid"];
							$viewname = $r["companyname"];

							echo "<option value=$viewbus>$viewname</option>";
						}
						echo "</select>";
					} elseif( $display_viewtype == 4 ) {
						echo "<input type=hidden name=viewtype value=$display_viewtype>";
						echo "<select name=display_viewbus style=\"font-size: 12px;\">";

						$query = "SELECT * FROM division ORDER BY division_name";
						$result = Treat_DB_ProxyOld::query( $query );

						while( $r = mysql_fetch_array( $result ) ) {
							$viewbus = $r["divisionid"];
							$viewname = $r["division_name"];

							echo "<option value=$viewbus>$viewname</option>";
						}
						echo "</select>";
					} elseif( $display_viewtype == 5 ) {
						echo "<input type=hidden name=viewtype value=$display_viewtype>";
						echo "<select name=display_viewbus style=\"font-size: 12px;\">";

						$query = "SELECT * FROM district ORDER BY districtname";
						$result = Treat_DB_ProxyOld::query( $query );

						while( $r = mysql_fetch_array( $result ) ) {
							$viewbus = $r["districtid"];
							$viewname = $r["districtname"];

							echo "<option value=$viewbus>$viewname</option>";
						}
						echo "</select>";
					} elseif( $display_viewtype == 6 ) {
						echo "<input type=hidden name=viewtype value=$display_viewtype>";
						echo "<select name=display_viewbus style=\"font-size: 12px;\">";

						$query = "SELECT * FROM business WHERE companyid != '2' ORDER BY businessname";
						$result = Treat_DB_ProxyOld::query( $query );

						while( $r = mysql_fetch_array( $result ) ) {
							$viewbus = $r["businessid"];
							$viewname = $r["businessname"];

							echo "<option value=$viewbus>$viewname</option>";
						}
						echo "</select>";
					}

					if( $display_viewtype > 0 ) {
						echo " <input type='checkbox' name='expand' id='expand' value='1' /> <label for='expand'>Expand</label>";
						echo " <input type=submit style=\"font-size: 12px;\" value='Add'>";
					}
					echo "</form>";
				}
				////ESalesTrack WHO
				elseif( $lasttype == 2 ) {
					echo "<form class='ajaxForm' action='$rqFile?fn=displayWho' method=post style=\"margin:0;padding:0;display:inline;font-size: 12px;font-family: arial;\"><input type=hidden name=viewid value=$viewid>&nbsp;Options:";
					echo "<select name=display_viewbus><option value=2:1:0>All Units</option>";

					$query3 = "SELECT * FROM est_accountowner_type";
					$result3 = Treat_DB_ProxyOld::query( $query3 );

					while( $r3 = mysql_fetch_array( $result3 ) ) {
						$id = $r3["id"];
						$name = $r3["name"];

						echo "<option value=2:2:$id:0>&nbsp;&nbsp;&nbsp;$name</option>";

						$query4 = "SELECT * FROM est_locations";
						$result4 = Treat_DB_ProxyOld::query( $query4 );

						while( $r4 = mysql_fetch_array( $result4 ) ) {
							$lid = $r4["id"];
							$location = $r4["location"];

							echo "<option value=2:3:$id:$lid>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$location</option>";

							$query5 = "SELECT * FROM est_accountowner WHERE type = '$id' ANd location = '$lid'";
							$result5 = Treat_DB_ProxyOld::query( $query5 );

							while( $r5 = mysql_fetch_array( $result5 ) ) {
								$accountowner = $r5["accountowner"];
								$name = $r5["name"];

								echo "<option value=2:4:$accountowner>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$name</option>";
							}
						}
					}

					echo "</select> <input type=submit style=\"font-size: 12px;\" value='Add'>";
					echo "</form>";
				} elseif( $lasttype == 3 ) {
					$display_viewtype = intval($_GET["display_viewtype"]);
					if( $display_viewtype == 1 ) {
						$vtype1 = "SELECTED";
					} elseif( $display_viewtype == 2 ) {
						$vtype2 = "SELECTED";
					} elseif( $display_viewtype == 3 ) {
						$vtype3 = "SELECTED";
					} elseif( $display_viewtype == 4 ) {
						$vtype4 = "SELECTED";
					}

					echo "<form class='ajaxForm' action='$rqFile?fn=displayWho' method=post style=\"margin:0;padding:0;display:inline;font-size: 12px;font-family: arial;\"><input type=hidden name=viewid value=$viewid>&nbsp;Options:";
					echo " <select name=display_viewtype onChange='var s=this.form.display_viewtype; reloadDialog($viewid,( s.options[s.selectedIndex].value > 0 ) ? \"&display_viewtype=\"+s.options[s.selectedIndex].value : \"\")' style=\"font-size: 12px;\">
																	<option>Choose A Level...</option>
																	<option value='1' $vtype1>All Units</option>
																	<option value='2' $vtype2>&nbsp;Business</option>
																	<option value='3' $vtype3>&nbsp;&nbsp;Supervisor</option>
																	<option value='4' $vtype4>&nbsp;&nbsp;&nbsp;Route</option>
																		</select>";

					if( $display_viewtype == 1 ) {
						echo "<input type=hidden name=viewtype value=$display_viewtype />";
						echo "<input type=hidden name=display_viewbus value='$lasttype:$display_viewtype:0:0' />";
					} elseif( $display_viewtype == 2 ) {
						echo "<input type=hidden name=viewtype value=$display_viewtype>";

						$query = "
							SELECT
								bz.businessid AS businessid,
								bz.businessname AS businessname
							FROM business bz
							INNER JOIN vend_locations vl
								ON vl.unitid = bz.businessid
							LEFT JOIN db_who
								ON db_who.viewbus=bz.businessid
								AND db_who.viewid=$viewid
								AND db_who.expand=0
							WHERE db_who.viewid IS NULL
							GROUP BY bz.businessid
							ORDER BY bz.businessname
						";
						$result = Treat_DB_ProxyOld::query( $query );

						if( mysql_num_rows( $result ) == 0 ) {
							echo 'None available';
							$display_viewtype = 0;
						} else {
							echo "<select name=display_viewbus style=\"font-size: 12px;\">";
							while( $r = mysql_fetch_array( $result ) ) {
								$viewbus = $r["businessid"];
								$viewname = $r["businessname"];

								echo "<option value='$lasttype:$display_viewtype:$viewbus:0'>$viewname</option>";
							}
							echo "</select>";
						}
					} elseif( $display_viewtype == 3 ) {
						echo "<input type=hidden name=viewtype value=$display_viewtype>";

						$query = "
							SELECT
								supervisorid, supervisor_name, districtname
							FROM vend_supervisor
							JOIN business USING( businessid )
							JOIN district USING( districtid )
							LEFT JOIN db_who
								ON db_who.viewbus=vend_supervisor.supervisorid
								AND db_who.viewid=$viewid
								AND db_who.expand=0
							WHERE db_who.viewid IS NULL
							ORDER BY districtname, supervisor_name
						";
						$result = Treat_DB_ProxyOld::query( $query );

						if( mysql_num_rows( $result ) == 0 ) {
							echo 'None available';
							$display_viewtype = 0;
						} else {
							echo "<select name=display_viewbus style=\"font-size: 12px;\">";
							$lastGroup = NULL;
							while( $r = mysql_fetch_array( $result ) ) {
								$viewbus = $r["supervisorid"];
								$viewname = $r["supervisor_name"];
								$viewGroup = $r['districtname'];

								if( $lastGroup != $viewGroup ) {
									if( $lastGroup != NULL )
										echo '</optgroup>';
									$lastGroup = $viewGroup;
									echo "<optgroup label='$viewGroup'>";
								}

								echo "<option value='$lasttype:$display_viewtype:$viewbus:0'>$viewname</option>";
							}
							echo "</optgroup></select>";
						}
					} elseif( $display_viewtype == 4 ) {
						echo "<input type=hidden name=viewtype value=$display_viewtype>";

						$query = "
							SELECT
								login_routeid, route, districtname
							FROM login_route lr
							JOIN vend_supervisor s ON lr.supervisor = s.supervisorid
							JOIN business b ON s.businessid = b.businessid
							JOIN district USING( districtid )
							LEFT JOIN db_who
								ON db_who.viewbus=lr.login_routeid
								AND db_who.viewid=$viewid
								AND db_who.expand=0
							WHERE db_who.viewid IS NULL
							ORDER BY districtname, route
						";
						$result = Treat_DB_ProxyOld::query( $query );

						if( mysql_num_rows( $result ) == 0 ) {
							echo 'None available';
							$display_viewtype = 0;
						} else {
							echo "<select name=display_viewbus style=\"font-size: 12px;\">";
							$lastGroup = NULL;
							while( $r = mysql_fetch_array( $result ) ) {
								$viewbus = $r["login_routeid"];
								$viewname = $r["route"];
								$viewGroup = $r['districtname'];

								if( $lastGroup != $viewGroup ) {
									if( $lastGroup != NULL )
										echo '</optgroup>';
									$lastGroup = $viewGroup;
									echo "<optgroup label='$viewGroup'>";
								}

								echo "<option value='$lasttype:$display_viewtype:$viewbus:0'>$viewname</option>";
							}
							echo "</optgroup></select>";
						}
					}

					if( $display_viewtype > 0 ) {
						echo " <input type='checkbox' name='expand' id='expand' value='1' /> <label for='expand'>Expand</label>";
						echo " <input type=submit style=\"font-size: 12px;\" value='Add'>";
					}
					echo "</form>";
				}

				echo "</td>";

				echo "</tr></table></a>";
			}
		}
}

function addGraph( )
{
	global $rqFile, $loginid, $sec_dashboard, $fn, $viewid, $groupid, $isPublished;

	$return = array( 'error' => 'There was an error processing your request' );

	$graph_name = mysql_real_escape_string($_POST["graph_name"]);
	$graph_type = intval($_POST["graph_type"]);
	$xaxis = intval($_POST["xaxis"]);
	$height = intval($_POST['height']);
	$period = intval($_POST["period"]);
	$period_num = intval($_POST["period_num"]);
	$period_group = intval($_POST["period_group"]);
	$current = intval($_POST["current"]);
	$week_ends = mysql_real_escape_string($_POST["week_ends"]);
	$viewid = intval($_POST["viewid"]);
	if($viewid>0){
		$query = "
			UPDATE db_view SET
				name = '$graph_name',
				graph_type = '$graph_type',
				db_height = '$height',
				xaxis = '$xaxis',
				period = '$period',
				period_num = '$period_num',
				period_group = '$period_group',
				current = '$current',
				week_ends = '$week_ends'
			WHERE viewid = '$viewid'
			AND loginid = '$loginid'
		";
		$result = Treat_DB_ProxyOld::query($query);

		if( mysql_errno() ) {
			$return['error'] = mysql_error( );
		} else $return = array( 'viewid' => $viewid );
	}
	else
	{
		$query = "INSERT INTO db_view (name,loginid,graph_type,db_height,xaxis,period,period_num,period_group,current) VALUES ('$graph_name','$loginid','$graph_type','$db_height','$xaxis','$period','$period_num','$period_group','$current')";
		$result = Treat_DB_ProxyOld::query($query);
		$viewid = mysql_insert_id();

		if( mysql_errno() ) {
			$return['error'] = mysql_error( );
		} else $return = array( 'viewid' => $viewid );
	}

	echo json_encode( $return );
}

function displayWhat( )
{
	global $rqFile, $loginid, $sec_dashboard, $fn, $viewid, $groupid, $isPublished;

	$return = array( 'error' => 'Error updating chart settings' );

	$display_what = $_POST["display_what"];
	$viewid = intval($_POST["viewid"]);

	if( $viewid == "" ) {
		$del = intval($_GET["del"]);
		$viewid = intval($_GET["viewid"]);
		$id = intval($_GET["id"]);
	}

	$query = "
			SELECT
				loginid
			FROM db_view
			WHERE viewid=$viewid
	";
	$result = Treat_DB_ProxyOld::query($query);
	$ownerid = @mysql_result( $result, 0, 'loginid' );
	if( $ownerid != $loginid ) {
		$return['error'] = 'Access denied';
	} else {
		if( $del == 1 && $viewid > 0 && $id > 0) {
			$query = "
				DELETE FROM db_what
				WHERE id = '$id'
			";
			$result = Treat_DB_ProxyOld::query($query);

			if( mysql_affected_rows() > 0 )
				$return = array( 'viewid' => $viewid );
		} elseif($display_what!="") {
			$display_what=explode(":",$display_what);
			$i = 0;
			for( $i = 0; $i < count($display_what); $i++ )
				$display_what[$i] = mysql_real_escape_string( $display_what[$i] );

			$query = "
				INSERT INTO db_what (viewid,type,what)
					VALUES($viewid,'$display_what[0]','$display_what[1]')
			";
			$result = Treat_DB_ProxyOld::query($query);

			if( mysql_affected_rows() > 0 )
				$return = array( 'viewid' => $viewid );
		}
	}

	echo json_encode( $return );
}

function displayCustom( )
{
	global $rqFile, $loginid, $sec_dashboard, $fn, $viewid, $groupid, $isPublished;
	$custom_name = mysql_real_escape_string($_POST["custom_name"]);
	$custom_field = mysql_real_escape_string($_POST["custom_field"]);
	$future_dates = mysql_real_escape_string($_POST["future_dates"]);
	$viewid = intval($_POST["viewid"]);

	if($viewid==""){
		$del = intval($_GET["del"]);
		$viewid=intval($_GET["viewid"]);
		$id=intval($_GET["id"]);
	}

	$return = array( 'error' => 'Error updating chart settings' );
	$query = "
			SELECT
				loginid
			FROM db_view
			WHERE viewid=$viewid
	";
	$result = Treat_DB_ProxyOld::query($query);
	$ownerid = @mysql_result( $result, 0, 'loginid' );
	if( $ownerid != $loginid ) {
		$return['error'] = 'Access denied';
	} else {
		if($del==1&&$viewid>0&&$id>0){
			$query = "
				DELETE FROM db_custom
				WHERE id = '$id'
			";
			$result = Treat_DB_ProxyOld::query($query);

			if( mysql_affected_rows() > 0 )
				$return = array( 'viewid' => $viewid );
		}
		elseif($custom_name!="")
		{
			$query = "
				INSERT INTO db_custom (viewid,name,custom_field,future_dates)
					VALUES( $viewid, '$custom_name', '$custom_field', '$future_dates' )
			";
			$result = Treat_DB_ProxyOld::query($query);

			if( mysql_affected_rows() > 0 )
				$return = array( 'viewid' => $viewid );
		}
	}

	echo json_encode( $return );
}

function displayWho( )
{
	global $rqFile, $loginid, $sec_dashboard, $fn, $viewid, $groupid, $isPublished;
	$viewtype = intval($_POST["viewtype"]);
	$viewbus = $_POST["display_viewbus"];
	$viewid = intval($_POST["viewid"]);
	$expand = $_POST['expand'] == 1 ? 1 : 0;

	if($viewid==""){
		$del = intval($_GET["del"]);
		$viewid=intval($_GET["viewid"]);
		$id=intval($_GET["id"]);
	}

	$return = array( 'error' => 'Error updating chart settings' );
	$query = "
			SELECT
				loginid
			FROM db_view
			WHERE viewid=$viewid
	";
	$result = Treat_DB_ProxyOld::query($query);
	$ownerid = @mysql_result( $result, 0, 'loginid' );
	if( $ownerid != $loginid ) {
		$return['error'] = 'Access denied';
	} else {
		////explode
		if(strpos($viewbus,":")>0){
			$data=explode(":",$viewbus);
			$type = intval($data[0]);
			$viewtype = intval($data[1]);
			$viewbus = intval($data[2]);
			$viewbus2 = intval($data[3]);
		}
		else{$type=1;}

		if($del==1&&$viewid>0&&$id>0){
			$query = "
				DELETE FROM db_who
				WHERE id = '$id'
			";
			$result = Treat_DB_ProxyOld::query($query);

			if( mysql_affected_rows() > 0 )
				$return = array( 'viewid' => $viewid );
		}
		elseif($viewtype>0)
		{
			$query = "
				INSERT INTO db_who (viewid,type,viewtype,viewbus,viewbus2,expand)
					VALUES( $viewid, '$type', '$viewtype', '$viewbus', '$viewbus2', '$expand' )
			";
			$result = Treat_DB_ProxyOld::query($query);

			if( mysql_affected_rows() > 0 )
				$return = array( 'viewid' => $viewid );
		}
	}

	echo json_encode( $return );
}

function deleteChart( )
{
	global $rqFile, $loginid, $sec_dashboard, $fn, $viewid, $groupid, $isPublished;
	$queryView = "
		DELETE FROM %s
		WHERE viewid = '$viewid'
		AND loginid = '$loginid'
	";
	$querySettings = "
		DELETE FROM db_view_order, db_who, pub_db_who, db_what, pub_db_what,
					db_custom, pub_db_custom, db_trendlines, pub_db_trendlines,
					db_group_graphs
		WHERE viewid = '$viewid'
	";
	Treat_DB_ProxyOld::query( sprintf( $queryView, 'db_view' ) );

	if( mysql_affected_rows( ) > 0 ){
		Treat_DB_ProxyOld::query( sprintf( $queryView, 'pub_db_view' ) );
		$tableList = array( 'db_view_order', 'db_who', 'pub_db_who', 'db_what', 'pub_db_what', 'db_custom',
							'pub_db_custom', 'db_trendlines', 'pub_db_trendlines', 'db_group_graphs' );
		foreach( $tableList as $t ) {
			Treat_DB_ProxyOld::query( sprintf( $querySettings, $t ) );
		}
	} else {
		$error = mysql_errno() ? mysql_error() : 'There was an error processing your request';
		$return = array( 'error', $error );
		echo json_encode( $return );
	}
}

function updateOrder( )
{
	global $rqFile, $loginid, $sec_dashboard, $fn, $viewid, $groupid, $isPublished;
	if (is_array($_GET['graphs'])) {
		if( $groupid == 'custom' )
			$query = '
				REPLACE db_view_order SET
					dbOrder=%d,
					dbSide=%d,
					dbHeight=%d,
					viewid=%d,
					loginid='.$loginid;
		else $query = '
			UPDATE db_group_graphs SET
				dbOrder=%d,
				dbSide=%d,
				dbHeight=%d
			WHERE viewid=%d
			AND '.$loginid.' IN (
				SELECT
					groupowner
				FROM db_group
				WHERE groupid='.$groupid.'
			)
			AND groupid='.$groupid;

		foreach ($_GET['graphs'] as $viewid => $o) {
			$gquery = sprintf( $query, intval($o['order']), intval($o['side']), intval($o['height']), $viewid );
			Treat_DB_ProxyOld::query( $gquery );
		}
	}
}

function getTrendlines( )
{
	global $rqFile, $loginid, $sec_dashboard, $fn, $viewid, $groupid, $isPublished;
	$query = "
		SELECT
			*
		FROM db_trendlines
		WHERE viewid=$viewid
		ORDER BY lineStart, lineEnd, trendCaption
	";
	$result = Treat_DB_ProxyOld::query( $query );

	$trends = array( );
	$i = 0;
	while( $r = mysql_fetch_assoc( $result ) ) {
		$trends[$i] = $r;
		$trendname = $r['lineStart'];
		if( $r['lineEnd'] != $r['lineStart'] )
			$trendname .= '-'.$r['lineEnd'];
		if( !empty( $r['trendCaption'] ) )
			$trendname = $r['trendCaption'].' ('.$trendname.')';
		$trends[$i]['name'] = $trendname;
		$i++;
	}

	echo json_encode( array( 'trends' => $trends ) );
}

function editTrendline( )
{
	global $rqFile, $loginid, $sec_dashboard, $fn, $viewid, $groupid, $isPublished;

	$return = array( 'error' => 'Error updating trendline' );
	$viewid = intval($_POST['viewid']);

	$query = "
		SELECT loginid
		FROM db_view
		WHERE viewid=$viewid
	";
	$result = Treat_DB_ProxyOld::query( $query );
	$ownerid = @mysql_result( $result, 0, 'loginid' );
	if( $ownerid == $loginid ) {

		$trendid = intval($_POST['trendid']);
		$lineStart = floatval($_POST['lineStart']);
		$lineEnd = floatval($_POST['lineEnd']);
		$trendCaption = mysql_real_escape_string($_POST['trendCaption']);
		$_lineColor = $_POST['lineColor'];
		$_lineColor_length = strlen( $_lineColor );
		$lineColor = mysql_real_escape_string( substr( $_lineColor, $_lineColor_length - 6, $_lineColor_length ) );
		$lineWidth = intval($_POST['lineWidth']);
		$linePosition = intval($_POST['linePosition']);

		$queryValues = "SET
			viewid=$viewid,
			lineStart=$lineStart,
			lineEnd=$lineEnd,
			trendCaption='$trendCaption',
			lineColor='$lineColor',
			lineWidth=$lineWidth,
			linePosition=$linePosition
		";

		if( $trendid == 'new' ) {
			$query = "
				INSERT INTO db_trendlines $queryValues
			";
		} else {
			$query = "
				UPDATE db_trendlines $queryValues
				WHERE trendid=$trendid
				AND viewid=$viewid
			";
		}

		$result = Treat_DB_ProxyOld::query( $query );
		if( !mysql_errno() )
			$return = array( 'viewid' => $viewid );
	}

	echo json_encode( $return );
}

function deleteTrendline( )
{
	global $rqFile, $loginid, $sec_dashboard, $fn, $viewid, $groupid, $isPublished;
	$trendid = intval($_GET['trendid']);

	$query = "
		DELETE FROM db_trendlines
		WHERE trendid=$trendid
		AND viewid=$viewid
		AND $loginid = (
			SELECT loginid
			FROM db_view
			WHERE viewid=$viewid
		)
	";
	$result = Treat_DB_ProxyOld::query( $query );

	if( mysql_affected_rows() > 0 )
		$return = array( 'viewid' => $viewid );
	else $return = array( 'error' => 'Error deleting trendline' );

	echo json_encode( $return );
}

function exportData( )
{
	global $rqFile, $loginid, $sec_dashboard, $fn, $viewid, $groupid, $isPublished;
	$chart = new eeChart( $viewid, $isPublished );
	header("Content-type: application/x-msdownload");
	header("Content-Disposition: attachment; filename=report.xls");
	header("Pragma: no-cache");
	header("Expires: 100");
	echo $chart->getTable( );
}

function clearCache( ) {
	global $rqFile, $loginid, $sec_dashboard, $fn, $viewid, $groupid, $isPublished;

	Treat_Model_Dashboard_Cache::clear( $viewid );
}

//php>
