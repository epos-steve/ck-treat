<?php
trigger_error(
	sprintf(
		'The file you are viewing required or included [%s], most likely via an absolute path. Please fix your code.'
		,__FILE__
	), E_USER_WARNING
);
if ( !defined( 'SITECH_LIB_PATH' ) ) {
	require_once dirname( __FILE__ ) . '/../../../application/bootstrap.php';
}


require(DOC_ROOT."/ta/lib/class.dashboard.php");
require(DOC_ROOT."/ta/lib/class.labor.php");
require(DOC_ROOT."/ta/lib/class.graph.php");
require(DOC_ROOT."/ta/lib/class.dates.php");
require(DOC_ROOT."/ta/lib/class.menu_items.php");

class eeDataSource {
	public $data = null;
	public $dates = null;
	public $legend = null;
	public $trends = null;
	public $dataMin = 0;
	public $dataMax = 0;
	public $multiSeries = true;
	public $labelCount = 0;

	protected $xAxis = 1;
	protected $chartSettings = array( );
	protected $viewid = NULL;
	protected $isPublished = NULL;
	protected $today = '';
	protected $show = 0;
	protected $prefix = '';

	protected $cacheId = NULL;
	protected $cacheData = NULL;

	protected $what, $real_what, $what_type;
	protected $viewtype, $viewbus, $viewbus2, $viewname;

	protected $dateField, $startDate, $endDate, $startDateValue, $endDateValue;

	protected $whoId = null;

	protected $dataMethod = 'legacy';

	static $WeekDays = array(
		'Monday' => 0,
		'Tuesday' => 1,
		'Wednesday' => 2,
		'Thursday' => 3,
		'Friday' => 4,
		'Saturday' => 5,
		'Sunday' => 6
	);

	protected $viewTypeList = array(
		1 => 'ap',
		2 => 'est',
		3 => 'vend',
		4 => 'ap',
		5 => 'ap'
	);

	protected $expandWho = array(
		'std1' => array( 'whoType' => 2, 'query' => 'SELECT typeid AS id FROM company_type' ),
		'std2' => array( 'whoType' => 3, 'query' => 'SELECT companyid AS id FROM company WHERE' ),
		'std3' => array( 'whoType' => 4, 'query' => 'SELECT divisionid AS id FROM division JOIN company USING( companyid ) WHERE' ),
		'std4' => array( 'whoType' => 5, 'query' => 'SELECT districtid AS id FROM district WHERE' ),
		'std5' => array( 'whoType' => 6, 'query' => 'SELECT businessid AS id FROM business WHERE business.companyid != 2 AND' ),
		'vnd1' => array( 'whoType' => 2, 'query' => 'SELECT businessid AS id FROM business INNER JOIN vend_locations vl ON vl.unitid = business.businessid' ),
		'vnd2' => array( 'whoType' => 3, 'query' => 'SELECT supervisorid AS id FROM vend_supervisor vs WHERE ' ),
		'vnd3' => array( 'whoType' => 4, 'query' => 'SELECT login_routeid AS id FROM login_route lr WHERE' )
	);

	protected $viewTypeWhatWho = array(
		1 => array(
			'what' => array(
				'fromTables' => 'dashboard dt ',
				'whatValues' => 'SUM(dt.%s)'
			),
			'who' => array(
				1 => array(
					'fromTables' => 'JOIN business ON dt.businessid = business.businessid AND business.db_restrict = "0" AND business.companyid != "2" ',
					'expandWho' => 'std1'
				),
				2 => array(
					'fromTables' => 'JOIN business ON dt.businessid = business.businessid AND business.db_restrict = "0" JOIN company ON business.companyid = company.companyid ',
					'whoClause' => 'company.company_type=',
					'expandWho' => 'std2'
				),
				3 => array(
					'fromTables' => 'JOIN business ON dt.businessid = business.businessid AND business.db_restrict = "0" JOIN company ON business.companyid = company.companyid ',
					'whoClause' => 'company.companyid=',
					'expandWho' => 'std3'
				),
				4 => array(
					'fromTables' => 'JOIN business ON dt.businessid = business.businessid AND business.db_restrict = "0" JOIN district ON business.districtid = district.districtid ',
					'whoClause' => 'district.divisionid=',
					'expandWho' => 'std4'
				),
				5 => array(
					'fromTables' => 'JOIN business ON dt.businessid = business.businessid AND business.db_restrict = "0" ',
					'whoClause' => 'business.districtid=',
					'expandWho' => 'std5'
				),
				6 => array(
					'whoClause' => 'dt.businessid='
				),
			)
		),
		2 => array(
			'what' => array(
				'fromTables' => 'est_accountowner',
				'est_sales' => array(
					'firstTable' => 'est_opportunity',
					'whatValues' => 'SUM(est_opportunity.income1)',
					'fromTables' => 'JOIN est_accountowner ON est_opportunity.accountowner = est_accountowner.accountowner AND est_accountowner.type >= 0 JOIN est_accountowner_budget ON est_accountowner.accountowner = est_accountowner_budget.accountowner AND EXTRACT(YEAR_MONTH FROM est_accountowner_budget.date) = EXTRACT(YEAR_MONTH FROM est_opportunity.expecteddate) AND est_accountowner_budget.gained > 0',
					'whereClause' => array(
						'est_opportunity.status IN (14, 11, 5, 7) '
					),
					'dateField' => 'est_opportunity.expecteddate'
				),
				'est_sales_bgt' => array(
					'firstTable' => 'est_accountowner_budget',
					'whatValues' => 'SUM(est_accountowner_budget.gained)',
					'fromTables' => 'JOIN est_accountowner ON est_accountowner_budget.accountowner = est_accountowner.accountowner',
					'dateField' => 'est_accountowner_budget.date'
				),
				'est_lost' => array(
					'firstTable' => 'est_accounts',
					'whatValuesType' => array(
						2 => 'est_accounts.cafeannualrevenue',
						'default' => 'est_accounts.yrlyrevenue+est_accounts.cafeannualrevenue+est_accounts.ocsrevenue'
					),
					'fromTables' => 'JOIN est_accountowner ON est_accounts.accountowner = est_accountowner.accountowner JOIN est_accountowner_budget ON est_accountowner.accountowner = est_accountowner_budget.accountowner AND EXTRACT(YEAR_MONTH FROM est_accountowner_budget.date) = EXTRACT(YEAR_MONTH FROM est_accounts.datelost) AND est_accountowner_budget.lost > 0',
					'whereClause' => array(
						'est_accounts.status = 2 AND (est_accounts.customertype != 3 AND est_accounts.customertype != 7)'
					),
					'dateField' => 'est_accounts.datelost'
				),
				'est_lost_bgt' => array(
					'firstTable' => 'est_accountowner_budget',
					'whatValues' => 'SUM(est_accountowner_budget.lost)',
					'fromTables' => 'JOIN est_accountowner ON est_accountowner_budget.accountowner = est_accountowner.accountowner',
					'dateField' => 'est_accountowner_budget.date'
				),
				'est_redlist' => array(
					'firstTable' => 'est_accounts',
					'whatValues' => 'SUM(est_accounts.yrlyrevenue+est_accounts.cafeannualrevenue+est_accounts.ocsrevenue)',
					'fromTables' => 'JOIN est_accountowner ON est_accounts.accountowner = est_accountowner.accountowner',
					'whereClause' => array(
						'est_accounts.customertype = 6 AND est_accounts.status = 2'
					),
					'dateField' => 'est_accounts.redlistdate'
				)
			),
			'who' => array(
				2 => array(
					'whoClause' => 'est_accountowner.type='
				),
				3 => array(
					'whoClause' => 'est_accountowner.type=',
					'whoClause2' => 'est_accountowner.location='
				),
				4 => array(
					'whoClause' => 'est_accountowner.accountowner='
				)
			)
		),
		3 => array(
			'what' => array(
				'fromTables' => 'vend_route_collection dt',
				'route_sales' => array(
					'whatValues' => 'SUM(dt.route_total)'
				),
				'avg_sales' => array(
					'whatValues' => 'AVG(dt.route_total)',
					'whereClause' => array( 'dt.route_total > 0 ' ),
					'sumDays' => true
				)
			),
			'who' => array(
				1 => array(
					'expandWho' => 'vnd1'
				),
				2 => array(
					'fromTables' => 'JOIN login_route lr USING( login_routeid ) JOIN vend_supervisor vs ON lr.supervisor=vs.supervisorid ',
					'whoClause' => 'vs.businessid=',
					'expandWho' => 'vnd2'
				),
				3 => array(
					'fromTables' => 'JOIN login_route lr USING( login_routeid ) ',
					'whoClause' => 'lr.supervisor=',
					'expandWho' => 'vnd3'
				),
				4 => array(
					'whoClause' => 'dt.login_routeid='
				)
			)
		),
		4 => array(
			'what' => array(
				'fromTables' => 'apinvoicedetail',
				'whatValues' => 'SUM(apinvoicedetail.amount)',
				'dateField' => 'apinvoice.date'
			),
			'who' => array(
				'fromTablesWhat' => 'JOIN apinvoice ON apinvoice.transfer = 4 AND apinvoice.posted = 1 AND apinvoice.apinvoiceid = apinvoicedetail.apinvoiceid AND apinvoicedetail.categoryid = "%s"',
				1 => array(
					'fromTables' => 'JOIN business ON apinvoice.businessid = business.businessid AND business.companyid != 2',
					'expandWho' => 'std1'
				),
				2 => array(
					'fromTables' => 'JOIN business ON apinvoice.businessid = business.businessid JOIN company ON business.companyid = company.companyid',
					'whoClause' => 'company.company_type=',
					'expandWho' => 'std2'
				),
				3 => array(
					'fromTables' => 'JOIN business ON apinvoice.businessid = business.businessid JOIN company ON business.companyid = company.companyid',
					'whoClause' => 'company.companyid=',
					'expandWho' => 'std3'
				),
				4 => array(
					'fromTables' => 'JOIN business ON apinvoice.businessid = business.businessid JOIN district ON business.districtid = district.districtid',
					'whoClause' => 'district.divisionid=',
					'expandWho' => 'std4'
				),
				5 => array(
					'fromTables' => 'JOIN business ON apinvoice.businessid = business.businessid',
					'whoClause' => 'business.districtid=',
					'expandWho' => 'std5'
				),
				6 => array(
					'whoClause' => 'apinvoice.businessid='
				)
			)
		),
		5 => array(
			'what' => array(
				'fromTables' => 'budget',
				'whatValues' => '
					CASE cal.dateMonth
						WHEN 1 THEN SUM(DISTINCT `01`)
						WHEN 2 THEN SUM(DISTINCT `02`)
						WHEN 3 THEN SUM(DISTINCT `03`)
						WHEN 4 THEN SUM(DISTINCT `04`)
						WHEN 5 THEN SUM(DISTINCT `05`)
						WHEN 6 THEN SUM(DISTINCT `06`)
						WHEN 7 THEN SUM(DISTINCT `07`)
						WHEN 8 THEN SUM(DISTINCT `08`)
						WHEN 9 THEN SUM(DISTINCT `09`)
						WHEN 10 THEN SUM(DISTINCT `10`)
						WHEN 11 THEN SUM(DISTINCT `11`)
						WHEN 12 THEN SUM(DISTINCT `12`)
					END
				',
				'dateField' => 'cal.calendarDate',
				'dateJoin' => 'budget.year = cal.dateYear'
			),
			'who' => array(
				'whereClauseWhat' => 'budget.type = "%s"',
				1 => array(
					'fromTables' => 'JOIN business ON budget.businessid = business.unit AND business.companyid != 2',
					'expandWho' => 'std1'
				),
				2 => array(
					'fromTables' => 'JOIN business ON budget.businessid = business.unit JOIN company ON business.companyid = company.companyid',
					'whoClause' => 'company.company_type=',
					'expandWho' => 'std2'
				),
				3 => array(
					'fromTables' => 'JOIN business ON budget.businessid = business.unit JOIN company ON business.companyid = company.companyid',
					'whoClause' => 'company.companyid=',
					'expandWho' => 'std3'
				),
				4 => array(
					'fromTables' => 'JOIN business ON budget.businessid = business.unit JOIN district ON business.districtid = district.districtid',
					'whoClause' => 'district.divisionid=',
					'expandWho' => 'std4'
				),
				5 => array(
					'fromTables' => 'JOIN business ON budget.businessid = business.unit',
					'whoClause' => 'business.districtid=',
					'expandWho' => 'std5'
				),
				6 => array(
					'fromTables' => 'JOIN business ON budget.businessid = business.unit',
					'whoClause' => 'business.businessid='
				)
			)
		),
		6 => array(
			'what' => array(
				'fromTables' => 'checkdetail',
				'dateField' => 'DATE(checks.bill_datetime)',
				1 => array(
					'whatValues' => 'SUM(checkdetail.quantity)'
				),
				3 => array(
					'whatValues' => 'COUNT(checks.id)',
					'whereClause' => array( 'checkdetail.quantity > 0' )
				)
			),
			'who' => array(
				'fromTablesWhat' => 'JOIN checks ON checks.businessid = checkdetail.businessid AND checks.check_number = checkdetail.bill_number',
				'whereClauseWhat' => 'checkdetail.item_number=%s',
				1 => array(
					'fromTables' => 'JOIN business ON checks.businessid = business.businessid AND business.db_restrict = "0" AND business.companyid != "2" '
				),
				2 => array(
					'fromTables' => 'JOIN business ON checks.businessid = business.businessid AND business.db_restrict = "0" JOIN company ON business.companyid = company.companyid ',
					'whoClause' => 'company.company_type='
				),
				3 => array(
					'fromTables' => 'JOIN business ON checks.businessid = business.businessid AND business.db_restrict = "0" JOIN company ON business.companyid = company.companyid ',
					'whoClause' => 'company.companyid='
				),
				4 => array(
					'fromTables' => 'JOIN business ON checks.businessid = business.businessid AND business.db_restrict = "0" JOIN district ON business.districtid = district.districtid ',
					'whoClause' => 'district.divisionid='
				),
				5 => array(
					'fromTables' => 'JOIN business ON checks.businessid = business.businessid AND business.db_restrict = "0" ',
					'whoClause' => 'business.districtid=',
					'expandWho' => 'std5'
				),
				6 => array(
					'whoClause' => 'checks.businessid='
				),
			)
		),
		7 => array(
			'what' => array(
				'fromTables' => 'checkdetail',
				'dateField' => 'DATE(checks.bill_datetime)',
				'whatValues' => 'SUM(checkdetail.quantity * checkdetail.price)'
			),
			'who' => array(
				'fromTablesWhat' => 'JOIN checks ON checks.businessid = checkdetail.businessid AND checks.check_number = checkdetail.bill_number',
				'whereClauseWhat' => 'HOUR(checks.bill_datetime)=%s',
				1 => array(
					'fromTables' => 'JOIN business ON checks.businessid = business.businessid AND business.db_restrict = "0" AND business.companyid != "2" '
				),
				2 => array(
					'fromTables' => 'JOIN business ON checks.businessid = business.businessid AND business.db_restrict = "0" JOIN company ON business.companyid = company.companyid ',
					'whoClause' => 'company.company_type='
				),
				3 => array(
					'fromTables' => 'JOIN business ON checks.businessid = business.businessid AND business.db_restrict = "0" JOIN company ON business.companyid = company.companyid ',
					'whoClause' => 'company.companyid='
				),
				4 => array(
					'fromTables' => 'JOIN business ON checks.businessid = business.businessid AND business.db_restrict = "0" JOIN district ON business.districtid = district.districtid ',
					'whoClause' => 'district.divisionid='
				),
				5 => array(
					'fromTables' => 'JOIN business ON checks.businessid = business.businessid AND business.db_restrict = "0" ',
					'whoClause' => 'business.districtid=',
					'expandWho' => 'std5'
				),
				6 => array(
					'whoClause' => 'checks.businessid='
				),
			)
		),
		8 => array(
			'what' => array(
				'fromTablesWhat' => 'debitdetail JOIN debits dt ON debitdetail.debitid = dt.debitid AND dt.category = %s ',
				'whatValues' => 'ROUND(SUM(debitdetail.amount), 2)',
				'dateField' => 'debitdetail.date'
			),
			'who' => array(
				1 => array(
					'fromTables' => 'JOIN business ON dt.businessid = business.businessid AND business.db_restrict = "0" AND business.companyid != "2" ',
					'expandWho' => 'std1'
				),
				2 => array(
					'fromTables' => 'JOIN business ON dt.businessid = business.businessid AND business.db_restrict = "0" JOIN company ON business.companyid = company.companyid ',
					'whoClause' => 'company.company_type=',
					'expandWho' => 'std2'
				),
				3 => array(
					'fromTables' => 'JOIN business ON dt.businessid = business.businessid AND business.db_restrict = "0" JOIN company ON business.companyid = company.companyid ',
					'whoClause' => 'company.companyid=',
					'expandWho' => 'std3'
				),
				4 => array(
					'fromTables' => 'JOIN business ON dt.businessid = business.businessid AND business.db_restrict = "0" JOIN district ON business.districtid = district.districtid ',
					'whoClause' => 'district.divisionid=',
					'expandWho' => 'std4'
				),
				5 => array(
					'fromTables' => 'JOIN business ON dt.businessid = business.businessid AND business.db_restrict = "0" ',
					'whoClause' => 'business.districtid=',
					'expandWho' => 'std5'
				),
				6 => array(
					'whoClause' => 'dt.businessid='
				),
			)
		),
		9 => array(
			'what' => array(
				'fromTables' => 'payment_detail',
				'dateField' => 'DATE(checks.bill_datetime)',
				5 => array(
					'whatValues' => 'COUNT(DISTINCT payment_detail.scancode)',
					'whereClause' => array( 'payment_detail.scancode LIKE "780%"' )
				)
			),
			'who' => array(
				'fromTablesWhat' => 'JOIN checks ON checks.businessid = payment_detail.businessid AND checks.check_number = payment_detail.check_number',
				1 => array(
					'fromTables' => 'JOIN business ON checks.businessid = business.businessid AND business.db_restrict = "0" AND business.companyid != "2" ',
					'expandWho' => 'std1'
				),
				2 => array(
					'fromTables' => 'JOIN business ON checks.businessid = business.businessid AND business.db_restrict = "0" JOIN company ON business.companyid = company.companyid ',
					'whoClause' => 'company.company_type=',
					'expandWho' => 'std2'
				),
				3 => array(
					'fromTables' => 'JOIN business ON checks.businessid = business.businessid AND business.db_restrict = "0" JOIN company ON business.companyid = company.companyid ',
					'whoClause' => 'company.companyid=',
					'expandWho' => 'std3'
				),
				4 => array(
					'fromTables' => 'JOIN business ON checks.businessid = business.businessid AND business.db_restrict = "0" JOIN district ON business.districtid = district.districtid ',
					'whoClause' => 'district.divisionid=',
					'expandWho' => 'std4'
				),
				5 => array(
					'fromTables' => 'JOIN business ON checks.businessid = business.businessid AND business.db_restrict = "0" ',
					'whoClause' => 'business.districtid=',
					'expandWho' => 'std5'
				),
				6 => array(
					'whoClause' => 'checks.businessid='
				),
			)
		),
		10 => array(
			'what' => array(
				'fromTablesWhat' => 'creditdetail JOIN credits ct ON creditdetail.creditid = ct.creditid AND ct.category = %s ',
				'whatValues' => 'ROUND(SUM(creditdetail.amount), 2)',
				'dateField' => 'creditdetail.date'
			),
			'who' => array(
				1 => array(
					'fromTables' => 'JOIN business ON ct.businessid = business.businessid AND business.db_restrict = "0" AND business.companyid != "2" ',
					'expandWho' => 'std1'
				),
				2 => array(
					'fromTables' => 'JOIN business ON ct.businessid = business.businessid AND business.db_restrict = "0" JOIN company ON business.companyid = company.companyid ',
					'whoClause' => 'company.company_type=',
					'expandWho' => 'std2'
				),
				3 => array(
					'fromTables' => 'JOIN business ON ct.businessid = business.businessid AND business.db_restrict = "0" JOIN company ON business.companyid = company.companyid ',
					'whoClause' => 'company.companyid=',
					'expandWho' => 'std3'
				),
				4 => array(
					'fromTables' => 'JOIN business ON ct.businessid = business.businessid AND business.db_restrict = "0" JOIN district ON business.districtid = district.districtid ',
					'whoClause' => 'district.divisionid=',
					'expandWho' => 'std4'
				),
				5 => array(
					'fromTables' => 'JOIN business ON ct.businessid = business.businessid AND business.db_restrict = "0" ',
					'whoClause' => 'business.districtid=',
					'expandWho' => 'std5'
				),
				6 => array(
					'whoClause' => 'ct.businessid='
				),
			)
		),
	);


	/*
	 *
	 *
	 */

	protected $viewTypeInfo = array(
		'ap' => array(
			1 => array( 'viewbus' => 'All Units' ),
			2 => array(
				'viewbusQuery' => 'SELECT type_name AS viewname FROM company_type WHERE typeid = \'%s\''
			),
			3 => array(
				'viewbusQuery' => 'SELECT companyname AS viewname FROM company WHERE companyid = \'%s\''
			),
			4 => array(
				'viewbusQuery' => 'SELECT division_name AS viewname FROM division WHERE divisionid = \'%s\''
			),
			5 => array(
				'viewbusQuery' => 'SELECT districtname AS viewname FROM district WHERE districtid = \'%s\''
			),
			6 => array(
				'viewbusQuery' => 'SELECT businessname AS viewname FROM business WHERE businessid = \'%s\''
			)
		),
		'est' => array(
			1 => array( 'viewbus' => 'All Units' ),
			2 => array(
				'viewbusQuery' => 'SELECT name AS viewname FROM est_accountowner_type WHERE id = \'%s\'',
				'viewbus2Query' => 'SELECT location AS viewname FROM est_locations WHERE id = \'%s\''
			),
			3 => array(
				'viewbusQuery' => 'SELECT name AS viewname FROM est_accountowner_type WHERE id = \'%s\'',
				'viewbus2Query' => 'SELECT location AS viewname FROM est_locations WHERE id = \'%s\''
			),
			4 => array(
				'viewbusQuery' => 'SELECT name AS viewname FROM est_accountowner WHERE accountowner = \'%s\''
			)
		),
		'vend' => array(
			1 => array( 'viewbus' => 'All Units' ),
			2 => array(
				'viewbusQuery' => 'SELECT businessname AS viewname FROM business WHERE businessid = \'%s\''
			),
			3 => array(
				'viewbusQuery' => 'SELECT supervisor_name AS viewname FROM vend_supervisor WHERE supervisorid = \'%s\''
			),
			4 => array(
				'viewbusQuery' => 'SELECT route AS viewname FROM login_route WHERE login_routeid = \'%s\''
			)
		)
	);

	public function __construct( $viewid = NULL, $isPublished = NULL ) {
		if( $viewid != NULL && $isPublished != NULL ) {
			$this->setChart( $viewid, $isPublished );
		}
	}

	public function getAxis( ) {
		return $this->xAxis;
	}

	protected function _buildCacheId( ) {
		$idStr = 'eeDataSource:v1:';
		$idArray = array( );

		$idArray[] = 'startDate=' . $this->startDate;
		$idArray[] = 'endDate=' . $this->endDate;

		foreach( $this->chartSettings as $csk => $cs ) {
			$idArray[] = "$csk=$cs";
		}

		foreach( $this->what as $csk => $cs ) {
			$idArray[] = "$csk=$cs";
		}

		foreach( $this->what_type as $csk => $cs ) {
			$idArray[] = "$csk=$cs";
		}

		foreach( $this->viewbus as $csk => $cs ) {
			$idArray[] = "$csk=$cs";
		}

		foreach( $this->viewbus2 as $csk => $cs ) {
			$idArray[] = "$csk=$cs";
		}

		foreach( $this->viewtype as $csk => $cs ) {
			$idArray[] = "$csk=$cs";
		}

		foreach( $this->trends as $csk => $cs ) {
			$idArray[] = "$csk=$cs";
		}

		$idStr .= implode( ':', $idArray );
		$cacheId = crc32( $idStr );

		$this->cacheId = $cacheId;
	}

	protected function _checkCache( ) {
		$this->_buildCacheId( );

		//$deleteQuery = 'DELETE FROM db_cache WHERE cacheDate < DATE_SUB( NOW(), INTERVAL 1 MONTH )';
		//Treat_DB_ProxyOld::query( $deleteQuery );
		Treat_Model_Dashboard_Cache::clean( );

		//$query = 'SELECT dataXml FROM db_cache WHERE cacheId = ' . $this->cacheId;
		//$result = Treat_DB_ProxyOld::query( $query );
		$cache = Treat_Model_Dashboard_Cache::getById( $this->cacheId );

		if( $cache && !empty( $cache->dataXml ) ) {
			$cacheJson = $cache->dataXml;
			$this->cacheData = json_decode( $cacheJson, true );
			$this->cacheData['cacheDate'] = $cache->cacheDate;
			return true;
		}

		return false;
	}

	protected function _cache( $viewid, $array ) {
		$this->_buildCacheId( );

		//$query = 'INSERT INTO dataXml( cacheId, dataXml ) VALUES( ?, ? )';
		$dataJson = json_encode( $array );
		Treat_Model_Dashboard_Cache::add( $this->cacheId, $viewid, $dataJson );
	}

	protected function _getDates( ) {
		$datePeriodNum = $this->chartSettings['period_num'];
		$datePeriodGroup = $this->chartSettings['period_group'];
		$dateCurrent = ( $this->chartSettings['current'] == 1 );
		$datePeriod = $this->chartSettings['period'];

		$datePeriodInterval = array(
			1 => 'DAY',
			2 => 'WEEK',
			3 => 'MONTH',
			4 => 'YEAR'
		);

		$weekEnds = in_array( $this->chartSettings['week_ends'], array_keys( static::$WeekDays ) )
					? $this->chartSettings['week_ends']
					: 'Thursday';
		$weekEndsNum = static::$WeekDays[$weekEnds];

		switch( $datePeriodGroup ) {
			case 1:
				$dateField = 'calendarDate';
				break;

			case 2:
				$dateField = 'week' . $weekEnds;
				break;

			case 3:
				$dateField = 'monthEnd';
				break;

			case 4:
				$dateField = 'yearEnd';
				break;
		}

		switch( $datePeriod ) {
			case 1:
				$endDateWhere = $dateCurrent
							? "'{$this->today}'"
							: "DATE_SUB( '{$this->today}', INTERVAL 1 DAY )";
				$endDateQuery = "SELECT $endDateWhere AS endDate";
				break;

			case 2:
				$endDateWhere = $dateCurrent
								? "calendarDate = '{$this->today}'"
								: "week$weekEnds < '{$this->today}' ORDER BY week$weekEnds DESC LIMIT 1";
				$endDateQuery = "SELECT week$weekEnds AS endDate FROM calendarTable WHERE $endDateWhere";
				break;

			case 3:
				$endDateWhere = $dateCurrent
								? "calendarDate = '{$this->today}'"
								: "monthEnd < '{$this->today}' ORDER BY monthEnd DESC LIMIT 1";
				$endDateQuery = "SELECT monthEnd AS endDate FROM calendarTable WHERE $endDateWhere";
				break;

			case 4:
				$endDateWhere = $dateCurrent
								? "calendarDate = '{$this->today}'"
								: "yearEnd < '{$this->today}' ORDER BY yearEnd DESC LIMIT 1";
				$endDateQuery = "SELECT yearEnd AS endDate FROM calendarTable WHERE $endDateWhere";
				break;
		}

		$result = Treat_DB_ProxyOldProcessHost::query( $endDateQuery );
		$endDateValue = @mysql_result( $result, 0, 'endDate' );
		$startDateWhere = "DATE_SUB( '$endDateValue', INTERVAL $datePeriodNum {$datePeriodInterval[$datePeriod]} )";
		$startDateWhere = "DATE_ADD( $startDateWhere, INTERVAL 1 DAY )";
		$startDateQuery = "SELECT $startDateWhere AS startDate";
		$result = Treat_DB_ProxyOldProcessHost::query( $startDateQuery );
		$startDateValue = @mysql_result( $result, 0, 'startDate' );

		if( $endDateValue > $this->today  && ( $periodGroup == 1 || $periodGroup == 2 || $periodGroup == 3 ) && strpos( $whatType, "bgt" ) == false )
			$endDateValue = $this->today;

		$endDate = "'$endDateValue'";
		$startDate = "'$startDateValue'";

		$this->dateField = $dateField;
		$this->endDate = $endDate;
		$this->endDateValue = $endDateValue;
		$this->startDate = $startDate;
		$this->startDateValue = $startDateValue;
		return;
	}

	protected function _getWhat( ) {
		$what = array( );
		$what_type = array( );

		$query = "SELECT * FROM {$this->prefix}db_what WHERE viewid = '$this->viewid' ORDER BY id";
		$result = Treat_DB_ProxyOldProcessHost::query( $query );

		$counter = 1;
		while( $r = mysql_fetch_array( $result ) ) {
			$what[$counter] = $r["what"];
			$what_type[$counter] = $r["type"];

			$counter++;
		}

		$this->what = $what;
		$this->what_type = $what_type;
	}

	protected function _getWho() {
		$this->viewtype = array( );
		$this->viewbus = array( );
		$this->viewbus2 = array( );
		$this->viewname = array( );
		$counter = 1;

		$query = "SELECT * FROM {$this->prefix}db_who WHERE viewid = '$this->viewid' ORDER BY id";
		$result = Treat_DB_ProxyOldProcessHost::query( $query );

		while( $r = mysql_fetch_assoc( $result ) ) {
			if( !empty( $this->whoId ) ) {
				$whoIds = explode( ' ', $this->whoId );
				$whoIdArray = array( );
				foreach( $whoIds as $w ) {
					$ww = explode( '-', $w );
					$whoIdArray[$ww[0]] = $ww[1];
				}

				$whoField = $this->viewTypeWhatWho[$r['type']]['who'][$r['viewtype']]['whoClause'];
				$whoFieldArray = explode( '.', $whoField );
				$whoField = $whoFieldArray[count($whoFieldArray) - 1];
				$whoField = substr( $whoField, 0, strlen( $whoField ) - 1 );

				if( !empty( $whoIdArray[$whoField] ) )
					$r['viewbus'] = $whoIdArray[$whoField];
			}

			if( $r['expand'] == 1 ) {
				$viewWho = $this->viewTypeWhatWho[$r['type']]['who'][$r['viewtype']];

				if( ( $expand = $viewWho['expandWho'] ) ) {
					if( !is_array( $expand ) )
						$expand = $this->expandWho[$expand];

					$query = $expand['query'];
					if( $r['viewbus'] > 0 )
						$query .= ' ' . $viewWho['whoClause'] . $r['viewbus'];
					$resultWho = Treat_DB_ProxyOldProcessHost::query( $query );

					while( $r2 = mysql_fetch_assoc( $resultWho ) ) {
						$whoArray = array(
							'type' => $r['type'],
							'viewtype' => $expand['whoType'],
							'viewbus' => $r2['id'],
							'viewbus2' => 0
						);
						$this->_addWho( $whoArray, $counter++ );
					}
					continue;
				}
			}

			$this->_addWho( $r, $counter++ );
		}
	}

	protected function _addWho( $r, $counter ) {
		$type = $r["type"];
		$this->viewtype[$counter] = $r["viewtype"];
		$this->viewbus[$counter] = $r["viewbus"];
		$this->viewbus2[$counter] = $r["viewbus2"];

		$viewTypeInfo = $this->viewTypeInfo[$this->viewTypeList[$type]][$this->viewtype[$counter]];

		if( isset( $viewTypeInfo['viewbusQuery'] ) ) {
			$result2 = Treat_DB_ProxyOldProcessHost::query(
						sprintf( $viewTypeInfo['viewbusQuery'], $this->viewbus[$counter] ) );
			$this->viewname[$counter] = @mysql_result( $result2, 0, 'viewname' );

			if( isset( $viewTypeInfo['viewbus2Query'] ) && $this->viewbus2['counter'] > 0 ) {
				$result2 = Treat_DB_ProxyOldProcessHost::query(
							sprintf( $viewTypeInfo['viewbus2Query'], $this->viewbus2[$counter] ) );
				$this->viewname[$counter] .= ' ' . @mysql_result( $result2, 0, 'viewname' );
			}
		} else $this->viewname[$counter] = $viewTypeInfo['viewbus'];
	}

	protected function _getTrendlines( ) {
		$query = "
			SELECT
				*
			FROM {$this->prefix}db_trendlines
			WHERE viewid=$this->viewid
		";
		$result = Treat_DB_ProxyOldProcessHost::query( $query );

		$i = 0;
		$this->trends = array( );
		while( $r = mysql_fetch_assoc( $result ) ) {
			$this->trends[$i] = $r;
			$i++;
		}
	}

	protected function _buildLegend( ) {
		if( !empty( $this->legend ) )
			return;

		$counter = 1;
		$this->legend = array( );

		foreach( $this->viewname AS $key1 => $value1 ) {
			foreach( $this->what AS $key2 => $value ) {
				if( $value == "sales" ) {
					$value = "Sales";
				} elseif( $value == "cos" ) {
					$value = "COGS";
				} elseif( $value == "labor" ) {
					$value = "Labor";
				} elseif( $value == "toe" ) {
					$value = "TOE";
				} elseif( $value == "pl" ) {
					$value = "P&L";
				} elseif( $value == "sales_bgt" ) {
					$value = "Sales BGT";
				} elseif( $value == "cos_bgt" ) {
					$value = "COGS BGT";
				} elseif( $value == "labor_bgt" ) {
					$value = "Labor BGT";
				} elseif( $value == "toe_bgt" ) {
					$value = "TOE BGT";
				} elseif( $value == "pl_bgt" ) {
					$value = "P&L BGT";
				} elseif( $value == "est_sales" ) {
					$value = "EST: Sales";
				} elseif( $value == "est_sales_bgt" ) {
					$value = "EST: Sales BGT";
				} elseif( $value == "est_lost" ) {
					$value = "EST: Lost";
				} elseif( $value == "est_lost_bgt" ) {
					$value = "EST: Lost BGT";
				} elseif( $value == "est_redlist" ) {
					$value = "EST: Redlist";
				} elseif( $value == "route_sales" ) {
					$value = "Route Collections";
				} elseif( $value == "avg_sales" ) {
					$value = "Average Collections";
				} elseif( $this->what_type[$key2] == 6 && $value == 1 ) {
					$value = "Item Sales";
				} elseif( $this->what_type[$key2] == 6 && $value == 2 ) {
					$value = "Hourly Sales";
				} elseif( $this->what_type[$key2] == 6 && $value == 3 ) {
					$value = 'Ticket Count';
				} elseif( $this->what_type[$key2] == 9 && $value == 5 ) {
					$value = 'Unique Cards';
				}
				/////capex ap
				elseif( $this->what_type[$key2] == 4 ) {
					$query3 = "SELECT category_name FROM po_category WHERE categoryid = '$value'";
					$result3 = Treat_DB_ProxyOldProcessHost::query( $query3 );

					$value = mysql_result( $result3, 0, "category_name" );
				}
				/////capex budget
				elseif( $this->what_type[$key2] == 5 ) {
					$query3 = "SELECT category_name FROM po_category WHERE budget_type = '$value'";
					$result3 = Treat_DB_ProxyOldProcessHost::query( $query3 );

					$value = mysql_result( $result3, 0, "category_name" );
					$value .= " BGT";
				}
				/////reporting categories
				elseif( $this->what_type[$key2] == 8 || $this->what_type[$key2] == 10 ) {
					$query3 = "SELECT category FROM reporting_category WHERE id = $value";
					$result3 = Treat_DB_ProxyOldProcessHost::query( $query3 );

					$value = mysql_result( $result3, 0, "category" );
				}

				$this->legend[$counter] = $this->viewname[$key1]." $value";

				$counter++;
			}
		}
	}

	protected function _getMenuItems( ) {
		$newWhat = array( );
		$newWhatType = array( );
		$newLegend = array( );
		$realWhat = array( );

		$counter = 1;
		for( $i = 1; $i <= count( $this->what_type ); $i++ ) {
			if( $this->what_type[$i] == 6 && $this->what[$i] == 1 ) {
				$item_list = menu_items::list_active_menu( $this->viewbus[1], 0, $this->startDateValue, $this->endDateValue );

				//$counter = 1;
				foreach( $item_list AS $key => $value ) {
					//$newWhat[] = array( $key, $value );
					$newWhat[$counter] = $key;
					$newWhatType[$counter] = 6;
					$newLegend[$counter] = $value;
					$realWhat[$counter] = 1;

					$counter++;
				}
			} elseif( $this->what_type[$i] == 6 && $this->what[$i] == 2 ) {
				//$counter = 1;
				for( $counter2 = 0; $counter2 <= 23; $counter2++ ) {
					//$newWhat[] = array( $counter2, $counter2 );
					$newWhat[$counter] = $counter2;
					$newWhatType[$counter] = 7;
					$newLegend[$counter] = $counter2;
					$realWhat[$counter] = 2;

					$counter++;
				}
			} elseif( $this->what_type[$i] == 6 && $this->what[$i] == 4 ) {
				$tempData = menu_items::list_consolidated_sales( $this->viewbus[$i], $this->viewtype[$i], $this->startDate, $this->endDate );
				//$this->data[1] = $tempData[1];
				$counter = 1;
				foreach($tempData[1] AS $key => $value){
					$this->data[$counter][1] = $value;
					$counter++;
				}
				//array_unshift($this->data[1],1);
				//unset($this->data[1][0]);
				$this->legend = $tempData[0];
				array_unshift($this->legend,1);
				unset($this->legend[0]);
				return;
			} elseif( $this->what_type[$i] == 6 && $this->what[$i] == 5 ) {
				$newWhat[$counter] = $this->what[$i];
				$newWhatType[$counter] = 9;

				$counter ++;
			} else {
				$newWhat[$counter] = $this->what[$i];
				$newWhatType[$counter] = $this->what_type[$i];

				$counter ++;
			}
		}

		$this->what = $newWhat;
		$this->what_type = $newWhatType;
		if( !empty( $newLegend ) )
			$this->legend = $newLegend;
		if( !empty( $realWhat ) )
			$this->real_what = $realWhat;
	}

	protected function _getEachData( $data_counter, $key1, $value1, $key2, $value2 ) {
		$data = &$this->data;
		$arr_length = count( $this->dates ) - 1;

		for( $counter = 1; $counter <= $arr_length; $counter++ ) {
			$period_group = $this->chartSettings['period_group'];

			///dates
			if( $period_group == 1 ) {
				$startdate = $this->dates[$counter];
				$enddate = $this->dates[$counter];
			} else {
				$startdate = nextday( $this->dates[$counter - 1] );
				$enddate = $this->dates[$counter];
			}

			/////dashboard class
			if( $this->what_type[$key2] == 1 ) {
				$data[$data_counter][$counter] = DashBoard::get_data( $startdate, $enddate, $value2, $this->viewtype[$key1], $value1 );
			}
			////esalestrack class
			elseif( $this->what_type[$key2] == 2 ) {
				$data[$data_counter][$counter] = DashBoard::get_esalestrack_data( $startdate, $enddate, $value2, $this->viewtype[$key1], $value1, $this->viewbus2[$key1] );
			}
			////CapEx class
			elseif( $this->what_type[$key2] == 4 ) {
				$data[$data_counter][$counter] = DashBoard::get_ap_data( $startdate, $enddate, $value2, $this->viewtype[$key1], $value1 );
			}
			////CapEx BGT class
			elseif( $this->what_type[$key2] == 5 ) {
				$data[$data_counter][$counter] = DashBoard::get_ap_bgt_data( $startdate, $enddate, $value2, $this->viewtype[$key1], $value1 );
			}
			////POS Menu Items
			elseif( $this->what_type[$key2] == 6 ) {
				$data[$data_counter][$counter] = menu_items::items_sold( $value2, $this->viewbus[1], $startdate, $enddate );
			}
			////POS Hourly Sales
			elseif( $this->what_type[$key2] == 6.2 ) {
				$data[$data_counter][$counter] = DashBoard::hourly_sales( $value2, $this->viewbus[1], $startdate, $enddate );
			}
			////POS Item Sales New
			elseif( $this->what_type[$key2] == 6.4 ) {
				
			}
			////Unique Card Count
			elseif( $this->what_type[$key2] == 6.5 ) {
				$data[$data_counter][$counter] = Dashboard::unique_card_count( $value2, $this->viewbus[1], $startdate, $enddate );
			}
			////Vending Route Sales
			elseif( $this->what_type[$key2] == 3 ) {
				$data[$data_counter][$counter] = eeDataSource::OLDgetVendingData( $startdate, $enddate, $value2, $this->viewtype[$key1], $value1 );
			}

			////aggregate line graph
			if( $this->graph_type == 2 ) {
				$aggregate+=$data[$data_counter][$counter];
				$data[$data_counter][$counter] = $aggregate;
			}

			///make zero if null
			if( $data[$data_counter][$counter] == "" && $enddate <= $this->today ) {
				$data[$data_counter][$counter] = 0;
			}

			///make null if non-budget and in the future and viewing by day
			if( $startdate > $this->today
				&& ($period_group == 1 || $period_group == 2 || $period_group == 3)
				&& strpos( $value2, "bgt" ) == false ) {
				$data[$data_counter][$counter] = "";
			}

			if( $data[$data_counter][$counter] < $this->dataMin )
				$this->dataMin = $data[$data_counter][$counter];

			if( $data[$data_counter][$counter] > $this->dataMax )
				$this->dataMax = $data[$data_counter][$counter];
		}
	}

	protected function _getAllData( $data_counter, $viewBusIndex, $viewBus, $whatIndex, $_whatType, $realWhat ) {
		$whoType = $this->viewtype[$viewBusIndex];
		$whoId = $viewBus;
		$whoId2 = $this->viewbus2[$viewBusIndex];

		$viewIndex = $this->what_type[$whatIndex];
		$viewTypeWhatWho = $this->viewTypeWhatWho[$viewIndex];

		$whatValue = $_whatType;
		$whatType = $realWhat ?: $_whatType;

		$dateField = $this->dateField;
		$startDate = $this->startDate;
		$endDate = $this->endDate;

		$whereClause = array( );
		$whereSql = '';
		$fromTables = '';
		$whatValues = '';
		$groupClause = '';

		$joinDatesClause = "cal.calendarDate BETWEEN $startDate AND $endDate";

		if( isset( $viewTypeWhatWho['what']['dateJoin'] ) ) {
			$joinDateField = $viewTypeWhatWho['what']['dateJoin'];
			$joinDates = " ON $joinDateField AND $joinDatesClause";
		} else {
			$joinDateField = $viewTypeWhatWho['what'][$whatType]['dateField']
						?: $viewTypeWhatWho['what']['dateField']
						?: 'dt.date';
			$joinDates = " ON cal.calendarDate = $joinDateField AND $joinDatesClause";
		}

		// get What settings
		if( isset( $viewTypeWhatWho['what']['fromTablesWhat'] ) ) {
			$fromTables = sprintf( $viewTypeWhatWho['what']['fromTablesWhat'], $whatValue );
		} else {
			$fromTables = $viewTypeWhatWho['what'][$whatType]['firstTable']
						?: $viewTypeWhatWho['what']['fromTables'];
			$fromTables .= ' ' . $viewTypeWhatWho['what'][$whatType]['fromTables'];
		}

		if( isset( $viewTypeWhatWho['what'][$whatType]['whatValuesType'] ) ) {
			$whatValuesType = $viewTypeWhatWho['what'][$whatType]['whatValuesType'][$whoType]
							?: $viewTypeWhatWho['what'][$whatType]['whatValuesType']['default'];
			$whatValues = 'SUM(' . $whatValuesType . ')';
		} else {
			$whatValues = $viewTypeWhatWho['what'][$whatType]['whatValues']
						?: sprintf( $viewTypeWhatWho['what']['whatValues'], $whatValue );
		}
		$whatValues .= ' AS val';
		if( !empty( $viewTypeWhatWho['what'][$whatType]['whereClause'] ) )
			$whereClause = array_merge( $whereClause, $viewTypeWhatWho['what'][$whatType]['whereClause'] );

		// get Who settings
		if( $viewTypeWhatWho['who']['fromTablesWhat'] )
			$fromTables .= ' ' . sprintf( $viewTypeWhatWho['who']['fromTablesWhat'], $whatValue );
		$fromTables .= ' ' . $viewTypeWhatWho['who'][$whoType]['fromTables'];
		if( !empty( $viewTypeWhatWho['who'][$whoType]['whereClause'] ) )
			$whereClause = array_merge( $whereClause, $viewTypeWhatWho['who'][$whoType]['whereClause'] );
		if( ( $whereClauseWhat = $viewTypeWhatWho['who']['whereClauseWhat'] )
			|| ( $whereClauseWhat = $viewTypeWhatWho['who'][$whoType]['whereClauseWhat'] ) )
			$whereClause = array_merge( $whereClause, array(
				sprintf( $whereClauseWhat, $whatValue )
			) );
		if( !empty( $viewTypeWhatWho['who'][$whoType]['whoClause'] ) )
			$whoClause = ' ' . $viewTypeWhatWho['who'][$whoType]['whoClause'] . $whoId;
		if( !empty( $viewTypeWhatWho['who'][$whoType]['whoClause2'] ) )
			$whoClause .= ' AND ' . $viewTypeWhatWho['who'][$whoType]['whoClause2'] . $whoId2;
		if( is_array( $viewTypeWhatWho['who']['whoClauseFromWhat'] ) )
			$whoClause .= ' ' . $viewTypeWhatWho['who']['whoClauseFromWhat'][$whatType] . $whoId;
		if( !empty( $viewTypeWhatWho['who']['whoClause'] ) )
			$whoClause .= ' ' . $viewTypeWhatWho['who']['whoClause'] . $whoId;

		if( !empty( $whoClause ) )
			$whereClause = array_merge( $whereClause, array( $whoClause ) );

		$groupClause = 'GROUP BY cal.';
		$groupClause .= $viewTypeWhatWho['what'][$whatType]['sumDays'] ? 'calendarDate' : $dateField;
		$whereSql = implode( ' AND ', $whereClause );
		$whereSql = $whereSql ? 'AND ' . $whereSql : '';

		$joinClause = $joinDates . ' ' . $whereSql . ' WHERE ' . $joinDatesClause;

		//$query = "SELECT cal.$dateField, $whatValues FROM $fromTables $joinDates $whereSql $groupClause";
		$query = "SELECT cal.$dateField, $whatValues FROM calendarTable cal LEFT JOIN ($fromTables) $joinClause $groupClause";

		if( $viewTypeWhatWho['what'][$whatType]['sumDays'] ) {
			$query = "SELECT $dateField, SUM( val ) AS val FROM ( $query ) d GROUP BY $dateField";
		}

		$query = str_replace( array( "\t", "\n", "\r" ), ' ', $query );

		$result = Treat_DB_ProxyOldProcessHost::query( $query );

		$data = array( 0 => 0 );
		while( $r = mysql_fetch_assoc( $result ) ) {
			$data[$r[$dateField]] = $r['val'] ?: 0;
		}

		$this->dates = array_keys( $data );
		$this->data[$data_counter] = array_values( $data );
		unset( $this->dates[0] );
		unset( $this->data[$data_counter][0] );
	}

	protected function _getData( ) {
		if( $this->what_type[1] == 6 && $this->what[1] == 4 ){return;}
		$this->data = array( );
		$data_counter = 1;
		foreach( $this->viewbus AS $key1 => $value1 ) {
			foreach( $this->what AS $key2 => $value2 ) {
				//if( is_array( $value2 ) ) {
				//	$what = $value2[0];
				//	foreach( $value2[1] as $nv2 ) {
				//		$this->_getAllData( $data_counter++, $key, $value1, $key2, $what, $nv2[0] );
				//	}
				//} else {
				$this->_getAllData( $data_counter++, $key1, $value1, $key2, $value2, $this->real_what[$key2] ?: NULL );
			}
		}
	}

	protected function _processCustom( ) {
		$query = "SELECT * FROM {$this->prefix}db_custom WHERE viewid = '$this->viewid' ORDER BY id";
		$result = Treat_DB_ProxyOldProcessHost::query( $query );
		$num = mysql_numrows( $result );

		if( $num > 0 ) {
			$temp_array = array( );
			$temp_legend = array( );

			$counter = 1;
			while( $r = mysql_fetch_array( $result ) ) {
				$id = $r["id"];
				$custom_name = $r["name"];
				$custom_field = $r["custom_field"];
				$future_dates = $r["future_dates"];

				////redo legend
				$temp_legend[$counter] = $custom_name;

				if( preg_match( '#^\s*(sum|avg)\([^)]*\)\s*$#i', $custom_field ) ) {
					$fieldList = explode( '|', preg_replace( '#^\s*(sum|avg)\(([^)]*)\)\s*$#i', '$1|$2', $custom_field ) );
					$customFn = $fieldList[0];
					$customFields = explode( ',', $fieldList[1] );

					$count = 0;
					foreach( $customFields as $field ) {
						if( strstr( $field, ':' ) ) {
							$moreFields = explode( ':', $field );
							for( $ci = $moreFields[0]; $ci <= $moreFields[1]; $ci++ ) {
								$count++;
								foreach( $this->data[$ci] as $key => $value ) {
									if( $this->chartSettings['future_dates'] == 1 && $this->dates[$key + 1] > $this->today )
										break;
									$temp_array[$counter][$key] += $value;
								}
							}
						} else {
							$count++;
							foreach( $this->data[$field] as $key => $value ) {
								if( $this->chartSettings['future_dates'] == 1 && $this->dates[$key + 1] > $this->today )
									break;
								$temp_array[$counter][$key] += $value;
							}
						}
					}

					if( $customFn == 'avg' ) {
						foreach( $temp_array[$counter] as $key => $value ) {
							if( $this->chartSettings['future_dates'] == 1 && $this->dates[$key + 1] > $this->today )
								break;
							$temp_array[$counter][$key] = $value / $count;
						}
					}
				} else {
					////explode custom
					$custom = explode( " ", $custom_field );
					$arr_length = count( $custom );

					$pointer = 0;
					$scalar = false;

					while( $pointer < $arr_length ) {
						$scalar = preg_filter( '#\[(\d*)\]#', '$1', $custom[$pointer] );
						$dataIndex = $scalar ? 1 : $custom[$pointer];

						if( $pointer == 0 ) {
							foreach( $this->data[$dataIndex] AS $key => $value ) {
								$temp_array[$counter][$key] = ( $scalar ?: $value );
							}
						} else {
							foreach( $this->data[$dataIndex] AS $key => $value ) {

								if( $custom[$pointer - 1] == "+" ) {
									$temp_array[$counter][$key] += ( $scalar ?: $value );
								} elseif( $custom[$pointer - 1] == "-" ) {
									$temp_array[$counter][$key] -= ( $scalar ?: $value );
								} elseif( $custom[$pointer - 1] == "*" ) {
									$temp_array[$counter][$key] = $temp_array[$counter][$key] * ( $scalar ?: $value );
								} elseif( $custom[$pointer - 1] == "/" ) {
									$temp_array[$counter][$key] = $temp_array[$counter][$key] / ( $scalar ?: $value );
								}

								////make null
								if( $this->chartSettings['future_dates'] == 1 && $this->dates[$key + 1] > $this->today ) {
									$temp_array[$counter][$key] = "";
								} elseif( $temp_array[$counter][$key] == 0 ) {
									$temp_array[$counter][$key] = "";
								}
							}
						}

						$pointer+=2;
					}
				}

				$counter++;
			}

			////reassign data array
			$this->data = $temp_array;
			$this->legend = $temp_legend;
		}
	}

	protected function _fixLegend( ) {
		if( count( $this->legend ) != count( $this->data ) )
			$this->legend = array( );
	}

	public function setChart( $viewid, $isPublished, $whoId ) {
		$this->viewid = $viewid;
		$this->isPublished = $isPublished;
		$this->today = date("Y-m-d");
		$this->whoId = $whoId;

		$this->show= $_GET["show"] ?: 0;

		if( $isPublished )
			$this->prefix = 'pub_';
		else $this->prefix = '';

		$query = "SELECT * FROM {$this->prefix}db_view WHERE viewid = '$this->viewid'";
		$result = Treat_DB_ProxyOldProcessHost::query( $query );

		$this->chartSettings = mysql_fetch_assoc( $result );
		$this->chartSettings['graph_name'] = $this->chartSettings['name'];
		unset( $this->chartSettings['name'] );

		if( stristr( $this->chartSettings['graph_name'], 'vtest' ) )
			$this->dataMethod = 'new';

		if( !in_array( $this->chartSettings['week_ends'], array(
				'Monday',
				'Tuesday',
				'Wednesday',
				'Thursday',
				'Friday',
				'Saturday',
				'Sunday'
			) ) )
			$this->chartSettings['week_ends'] = "Friday";

		$this->multiSeries = eeChart::$ChartType[$this->chartSettings['graph_type']]['multiSeries'];
		$this->xAxis = $this->chartSettings['xaxis'];
	}

	public function loadDataArray( $viewid, $isPublished, $whoId ) {
		$this->setChart( $viewid, $isPublished, $whoId );

		$this->_getDates( );

		////query for what
		$this->_getWhat( );

		//build business array
		$this->_getWho( );

		// get trendlines
		$this->_getTrendlines( );

		if( $this->_checkCache( ) )
			return;

		///get menu items
		$this->_getMenuItems( );

		////build legend
		$this->_buildLegend( );

		/////get data
		$this->_getData( );

		/////replace with custom fields if exist
		$this->_processCustom( );

		$this->_fixLegend( );
	}

	public function getXmlArray( )
	{
		if( $this->cacheData !== NULL ) {
			$returnArray = $this->cacheData;
			return $returnArray;
		}

		if( $this->data === NULL )
			return false;

		$returnArray = array(
			'category' => '',
			'dataseries' => array( ),
			'trends' => '',
			'min' => $this->dataMin,
			'max' => $this->dataMax
		);

		//$counter = 0;
		$catArray = $this->xAxis == 1 ? $this->dates : $this->legend;
		$datArray = $this->xAxis == 1 ? $this->legend : $this->dates;

		if( $this->xAxis == 1 ) {
			$catCount = count( $this->data[1] );
		} else {
			$catCount = count( $this->data );
		}
		$this->labelCount = $catCount;

		//foreach( $catArray as $cat )
		//	$returnArray['category'] .= "<category name='$cat' />"; // showName='".( ++$counter % 7 == 1 ? '1' : '0' )."' />";

		for( $counter = 1; $counter <= $catCount; $counter++ )
			$returnArray['category'] .= "<category name='".htmlspecialchars( $catArray[$counter] ?: $counter )."' />";

		foreach( $this->trends as $trend ) {
			$returnArray['trends'] .= '<line '
				.'startValue="'.$trend['lineStart'].'" '
				.'endValue="'.$trend['lineEnd'].'" '
				.( !empty($trend['trendCaption']) ? 'displayValue="'.htmlspecialchars( $trend['trendCaption'] ).'" ' : '' )
				.'color="'.$trend['lineColor'].'" '
				.'thickness="'.$trend['lineWidth'].'" '
				.'showOnTop="'.$trend['linePosition'].'" '
				.'alpha="'.( $trend['linePosition'] && $trend['lineWidth'] > 1 ? '65' : '100').'" '
				.'valueOnRight="1" />';
		}

		if( $this->multiSeries )
		{
			if( $this->xAxis == 1 )
				foreach( $this->data as $dataId => $dataSeries ) {
					$dataName = htmlspecialchars( $datArray[$dataId] ) ?: $dataId;
					foreach( $dataSeries as $data ) {
						$data = number_format( $data, 2, '.','' );
						$returnArray['dataseries'][$dataName] .= "<set value='$data' />";
					}
				}
			else {
				$maxcount = count( $this->data[1] );
				for( $counter = 1; $counter <= $maxcount; $counter++ )
				{
					$dataName = htmlspecialchars( $datArray[$counter] ) ?: $counter;
					foreach( $this->data as $dataSeries ) {
						$data = number_format( $dataSeries[$counter], 2, '.','' );
						$returnArray['dataseries'][$dataName] .= "<set value='$data' />";
					}
				}
			}
		} elseif( count( $this->data ) > 1 ) {
			if( $this->xAxis == 1 )
			{
				$maxcount = count( $this->data[1] );
				for( $counter = 1; $counter <= $maxcount; $counter++ )
				{
					$data = 0;
					foreach( $this->data as $dataSeries )
						$data += $dataSeries[$counter];
					$dataName = htmlspecialchars( $catArray[$counter] );
					$data = number_format( $data, 2, '.','' );
					$returnArray['dataseries'][0] .= "<set value='$data' name='$dataName' color='".eeChart::getColor($counter)."' />";
				}
			} else {
				$counter = 1;
				foreach( $this->data as $dataId => $dataSeries ) {
					$dataName = htmlspecialchars( $catArray[$dataId] ) ?: $dataId;
					$data = array_sum( $dataSeries );
					$data = number_format( $data, 2, '.','' );
					$returnArray['dataseries'][0] .= "<set value='$data' name='$dataName' color='".eeChart::getColor($counter++)."' />";
				}
			}
		} else {
			if( $this->xAxis == 1 )
			{
				$counter = 1;
				$dataSeries = $this->data[1];
				foreach( $dataSeries as $dataId => $data ) {
					$dataName = htmlspecialchars( $catArray[$dataId] );
					$data = number_format( $data, 2 , '.','' );
					$returnArray['dataseries'][0] .= "<set value='$data' name='$dataName' color='".eeChart::getColor($counter++)."' />";
				}
			} else {
				$data = array_sum( $this->data[1] );
				$dataName = htmlspecialchars( $catArray[1] ) ?: 0;
				$data = number_format( $data, 2, '.','' );
				$returnArray['dataseries'][0] .= "<set value='$data' name='$dataName' color='".eeChart::getColor(1)."' />";
			}
		}

		$this->_cache( $this->viewid, $returnArray );

		return $returnArray;
	}
}
?>
