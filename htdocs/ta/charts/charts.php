<?php
trigger_error(
	sprintf(
		'The file you are viewing required or included [%s], most likely via an absolute path. Please fix your code.'
		,__FILE__
	), E_USER_WARNING
);
require_once( 'class.datasource.php' );
require_once( 'class.charts.php' );
?>
