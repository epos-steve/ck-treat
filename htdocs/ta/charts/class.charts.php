<?php

class eeChart {
	public static $ChartType = array(
		1 => array(
			'name' => 'Line Graph',
			'type' => 'MSLine2D',
			'multiSeries' => true
		),
		2 => array(
			'name' => 'Aggregate Line Graph',
			'type' => 'MSLine2D',
			'multiSeries' => true
		),
		3 => array(
			'name' => 'Column Chart',
			'type' => 'MSColumn2D',
			'multiSeries' => true
		),
		4 => array(
			'name' => 'Spread Sheet',
			'type' => 'table',
			'multiSeries' => true
		),
		5 => array(
			'name' => 'Pie Chart',
			'type' => 'Pie2D',
			'multiSeries' => false,
			'showValues' => 1,
			'xmlGraphOptions' => array( 'showNames' => 1 )
		),
		6 => array(
			'name' => 'Doughnut',
			'type' => 'Doughnut2D',
			'multiSeries' => false
		),
		7 => array(
			'name' => 'Stacked Line Graph',
			'type' => 'StackedArea2D',
			'multiSeries' => true
		),
		9 => array(
			'name' => 'Bar Chart',
			'type' => 'MSBar2D',
			'multiSeries' => true
		),
		11 => array(
			'name' => 'Stacked Column Chart',
			'type' => 'StackedColumn2D',
			'multiSeries' => true
		),
		12 => array(
			'name' => 'Stacked Bar Chart',
			'type' => 'StackedBar2D',
			'multiSeries' => true
		),
		/* These are editable charts - cool but not useful for dashboard
		13 => array(
			'name' => 'Drag Column Chart',
			'type' => 'DragColumn2D',
			'multiSeries' => true
		),
		14 => array(
			'name' => 'Drag Area Chart',
			'type' => 'DragArea',
			'multiSeries' => true
		),
		15 => array(
			'name' => 'Drag Line Chart',
			'type' => 'DragLine',
			'multiSeries' => true
		),
		16 => array(
			'name' => 'Drag Node Chart',
			'type' => 'DragNode',
			'multiSeries' => true
		),*/
		17 => array(
			'name' => 'Kagi Chart',
			'type' => 'Kagi',
			'multiSeries' => false
		),
		18 => array(
			'name' => 'Logarithmic Column Chart',
			'type' => 'LogColumn2D',
			'multiSeries' => true
		),
		19 => array(
			'name' => 'Logarithmic Line Graph',
			'type' => 'LogLine2D',
			'multiSeries' => true
		),
		/* Not displaying data - probably needs different XML data format
		20 => array(
			'name' => 'Multi-level Pie Chart',
			'type' => 'MultilevelPie',
			'multiSeries' => true
		),*/
		21 => array(
			'name' => 'Radar Graph',
			'type' => 'Radar',
			'multiSeries' => true
		),
		/* Freezes Flash in Linux/Firefox
		22 => array(
			'name' => 'Funnel Chart',
			'type' => 'Funnel',
			'multiSeries' => false
		),*/
		23 => array(
			'name' => 'Candlestick Chart',
			'type' => 'Candlestick',
			'multiSeries' => false
		),
		24 => array(
			'name' => 'Spline Graph',
			'type' => 'MSSpline2D',
			'multiSeries' => true
		),
		25 => array(
			'name' => 'Spline Area Graph',
			'type' => 'MSSplineArea2D',
			'multiSeries' => true
		),
		26 => array(
			'name' => 'Inverse Area Graph',
			'type' => 'InverseArea2D',
			'multiSeries' => true
		),
		27 => array(
			'name' => 'Inverse Column Chart',
			'type' => 'InverseColumn2D',
			'multiSeries' => true
		),
		28 => array(
			'name' => 'Inverse Line Graph',
			'type' => 'InverseLine2D',
			'multiSeries' => true
		),
		29 => array(
			'name' => 'Waterfall Graph',
			'type' => 'Waterfall',
			'multiSeries' => false
		),
		30 => array(
			'name' => 'Scrolling Area Graph',
			'type' => 'ScrollArea2D',
			'multiSeries' => true
		),
		31 => array(
			'name' => 'Scrolling Line Graph',
			'type' => 'ScrollLine2D',
			'multiSeries' => true
		),
		32 => array(
			'name' => 'Scrolling Column Chart',
			'type' => 'ScrollColumn2D',
			'multiSeries' => true
		),
		33 => array(
			'name' => 'Scrolling Stacked Column Chart',
			'type' => 'ScrollStackedColumn2D',
			'multiSeries' => true
		),
		34 => array(
			'name' => 'Spark Line Graph',
			'type' => 'SparkLine',
			'multiSeries' => true
		),
		35 => array(
			'name' => 'Spark Column Chart',
			'type' => 'SparkColumn',
			'multiSeries' => true
		),
		/* Needs different data to be useful
		36 => array(
			'name' => 'Spark Win/Loss Chart',
			'type' => 'SparkWinLoss',
			'multiSeries' => true
		),*/
		37 => array(
			'name' => 'Pyramid Chart',
			'type' => 'Pyramid',
			'multiSeries' => false
		)
	);

	public static $ChartColors = array(
		'cc0033',
		'3300cc',
		'00cc00',
		'ff6600',
		'0066ff',
		'006600',
		'cc9933',
		'990099',
		'3366cc',
		'66cc00',
		'993300',
		'993399',
		'3399ff',
		'ccff99',
		'cc9999',
		'ff99ff',
		'ccffcc'
	);

	public static $HideCaptionXml = '
		<styles>
			<definition>
				<style name="hideCaption" type="font" size="0" color="FFFFFF" />
			</definition>
			<application>
				<apply toObject="Caption" styles="hideCaption" />
			</application>
		</styles>';

	public static $PublishTables = array(
		'db_custom',
		'db_what',
		'db_who',
		'db_trendlines',
		'db_view' // db_view needs to be the last table to ensure mtime is updated properly
	);

	static function getColor( $counter )
	{
		$numColors = count( eeChart::$ChartColors );
		return eeChart::$ChartColors[$counter % $numColors];
	}

	protected $chartTables = array( true => 'vDbGroup', false => 'vDbCustom' );

	protected $chartId = null;
	protected $isPublished = false;
	protected $dataSource = null;
	protected $chartOptions = null;

	protected $dashboardWho = null;

	function __construct( $chartId, $isPublished = false ) {
		Treat_Model_Dashboard_Cache::clean( );
		$this->chartId = $chartId;
		$this->isPublished = $isPublished;
		$query = 'SELECT * FROM '.$this->chartTables[$isPublished].' WHERE viewid='.$chartId;
		$r = Treat_DB_ProxyOld::query( $query );
		if( mysql_num_rows( $r ) >= 1 )
			$this->chartOptions = mysql_fetch_array( $r );
	}

	function setWho( $who ) {
		$this->dashboardWho = $who;
	}

	function getJson( )
	{
		//$cache = Treat_Model_Dashboard_Cache::getByViewId( $this->chartId );

		$returnArray = array(
			'id' => $this->chartId,
			'title' => $this->chartOptions['name'],
			'size' => $this->chartOptions['dbSide'],
			'height' => $this->chartOptions['dbHeight'],
			'ctype_num' => $this->chartOptions['graph_type'],
			'ctype' => eeChart::$ChartType[$this->chartOptions['graph_type']]['type'],
			'mtime' => $this->chartOptions['mtime'],
			'ptime' => $this->chartOptions['ptime'],
			'cacheId' => '', //$cache->cacheId,
			'cacheDate' => '' //$cache->cacheDate
		);

		return arrayToJson( $returnArray );
	}

	function getTable( )
	{
		$this->dataSource = new eeDataSource();
		$this->dataSource->loadDataArray( $this->chartId, $this->isPublished, $this->dashboardWho );

		$returnHtml = '<table width="100%" cellspacing="0" cellpadding="1" style="border: 1px solid black;">';
		$returnHtml .= '<thead><tr bgcolor="#e8e7e7"><td bgcolor="#cccccc" align="right" style="border: 1px solid black;">&nbsp;</td>';

		$xAxis = $this->dataSource->getAxis( );
		$catArray = $xAxis == 1 ? $this->dataSource->legend : $this->dataSource->dates;
		$datArray = $xAxis == 1 ? $this->dataSource->dates : $this->dataSource->legend;

		foreach( $catArray as $title )
			$returnHtml .= '<td align="right" style="border: 1px solid black;">'.$title.'</td>';

		$returnHtml .= '</tr></thead><tbody>';

		if( $xAxis == 1 )
		{
			$maxcount = count( $this->dataSource->data[1] );
			for( $counter = 1; $counter <= $maxcount; $counter++ )
			{
				$returnHtml .= '<tr><td bgcolor="#e8e7e7" align="right" style="border: 1px solid black;">';
				$returnHtml .= $datArray[$counter];
				$returnHtml .= '</td>';

				foreach( $this->dataSource->data as $data )
					$returnHtml .= '<td align="right" style="border: 1px solid black;">'.$data[$counter].'</td>';

				$returnHtml .= '</tr>';
			}
		} else {
			$counter = 1;
			foreach( $this->dataSource->data as $dataSeries )
			{
				$returnHtml .= '<tr><td bgcolor="#e8e7e7" align="right" style="border: 1px solid black;">';
				$returnHtml .= $datArray[$counter++];
				$returnHtml .= '</td>';

				foreach( $dataSeries as $data )
					$returnHtml .= '<td align="right" style="border: 1px solid black;">'.$data.'</td>';

				$returnHtml .= '</tr>';
			}
		}

		$returnHtml .= '</tbody></table>';

		return $returnHtml;
	}

	function getChart( )
	{
		$this->dataSource = new eeDataSource();
		$this->dataSource->loadDataArray( $this->chartId, $this->isPublished, $this->dashboardWho );
		$xmlArray = $this->dataSource->getXmlArray( );
		$categoryXml = $xmlArray['category'];
		$trendXml = $xmlArray['trends'];
		$dataArray = $xmlArray['dataseries'];
		$dataMin = $xmlArray['min'];
		$dataMax = $xmlArray['max'] > 0 ? $xmlArray['max'] : 1;

		$chartDefaults = eeChart::$ChartType[$this->chartOptions['graph_type']];
		$multiSeries = $chartDefaults['multiSeries'];

		$showValues = isset( $chartDefaults['showValues'] ) ? $chartDefaults['showValues'] : 0;

		if( isset( $chartDefaults['xmlGraphOptions'] ) )
			foreach( $chartDefaults['xmlGraphOptions'] as $attrName => $attrValue )
				$xmlGraphOptions .= "$attrName='$attrValue'";

		$graphYLimits = $dataMax == 0 ? "yAxisMinValue='$dataMin' yAxisMaxValue='$dataMax'" : '';
		$graphExport = "exportEnabled='1' exportAtClient='1' exportHandler='fcExporter1'";

		$labelStep = 1 + floor( $this->dataSource->labelCount / 30 );

		$graphOptions = "caption='{$this->chartOptions['name']}' labelStep='$labelStep' animation='0' showValues='$showValues' rotateLabels='1' $graphYLimits $graphExport $xmlGraphOptions";

		$returnXml = "<graph $graphOptions>";
		$counter = 0;

		if( $multiSeries ) {
			$returnXml .= "<categories>$categoryXml</categories>";
			foreach( $dataArray as $dataName => $dataXml ) {
				$dataColor = eeChart::getColor( $counter++ );
				$returnXml .= "<dataset seriesname='$dataName' color='$dataColor'>$dataXml</dataset>";
			}
		} else {
			$returnXml .= $dataArray[0];
		}

		$returnXml .= '<trendLines>'.$trendXml.'</trendLines>';

		$returnXml .= eeChart::$HideCaptionXml;

		$returnXml .= '</graph>';

		return $returnXml;
	}
}
?>
