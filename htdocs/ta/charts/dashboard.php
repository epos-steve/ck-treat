<?php
require_once( 'charts.php' );

if( empty( $user ) || empty( $pass ) )
{
	$user = isset( $_COOKIE["usercook"] ) ? $_COOKIE["usercook"] : '';
	$pass = isset( $_COOKIE["passcook"] ) ? $_COOKIE["passcook"] : '';
}

$query = "SELECT loginid FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query( $query );
$num = mysql_numrows( $result );

if( $num != 1 ) {
	echo 'Error: Not logged in';
	exit( 0 );
}

$loginid = mysql_result( $result, 0, "loginid" );

$today = date( "Y-m-d" );

$query = "SELECT * FROM db_view WHERE loginid = '$loginid' ORDER BY db_order";
$result = Treat_DB_ProxyOld::query( $query );

$charts = array( );

$i = 0;
while( $r = mysql_fetch_array( $result ) ) {
	$i++;
	$charts[$i]['viewid'] = $r["viewid"];
	$charts[$i]['graph_name'] = $r["name"];
	$charts[$i]['graph_type'] = eeChart::$ChartType[$r["graph_type"]]['type'];
	$charts[$i]['graph_size'] = $r['size'];
	$charts[$i]['graph_height'] = $r['db_height'];
}

?>
<link rel="stylesheet" type="text/css" href="/assets/css/customDashboard.css" />
<link rel="stylesheet" type="text/css" href="/assets/css/jquery.ui.selectmenu.css" />
<link rel="stylesheet" type="text/css" href="/assets/css/jquery.ui.tooltip.css" />
<link rel="stylesheet" type="text/css" href="/assets/css/jquery.miniColors.css" />
<script type="text/javascript" language="javascript" src="/assets/js/jquery.fusioncharts.debug.js"></script>
<script type="text/javascript" language="javascript" src="/assets/js/defaults.fusioncharts.js"></script>
<script type="text/javascript" language="javascript" src="/assets/js/FusionCharts.js"></script>
<script type="text/javascript" language="javascript" src="/assets/js/FusionChartsExportComponent.js"></script>
<script type="text/javascript" language="javascript" src="/assets/js/jquery.form.js"></script>
<script type="text/javascript" language="javascript" src="/assets/js/jquery.ui.selectmenu.js"></script>
<script type="text/javascript" language="javascript" src="/assets/js/jquery.ui.tooltip.js"></script>
<script type="text/javascript" language="javascript" src="/assets/js/jquery.miniColors.min.js"></script>
<script type="text/javascript" language="javascript" src="/assets/js/customDashboard.js"></script>
<script type="text/javascript" language="javascript">
jQuery(function(){
	createFCExport( );

	initializeDashboard( );
});
</script>
<div id="dashboard_header" class="ui-widget ui-widget-header ui-corner-all">
	<div id="dashboardTitle">
		<select id="dashboardMenu" name="dashboardMenu"></select>
	</div>
	<div id="dashboardButtons"></div>
</div>
<input type='hidden' id='dashboardGroup' value='' />
<input type='hidden' id='dashboardOwner' value='' />
<div id="editChartDialog" class="ui-hidden"></div>
<div id="editDbDialog" class="ui-hidden"></div>
<div id="saveChartFrame">&nbsp;</div>
<div id="dashboard_charts_container"><div id="dashboard_charts">
	<div id="dashboard_message"></div>
	<div id="dashboard_left" class="connectedDashboard">&nbsp;</div>
	<div id="dashboard_right" class="connectedDashboard">&nbsp;</div>
	<div id="dashboard_spacer">&nbsp;</div>
</div></div>
