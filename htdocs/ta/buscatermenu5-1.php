<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once(DOC_ROOT.'/bootstrap.php');

ini_set('display_erros', false);

$output = array("status" => "DEFAULT", "message" => "An unknown action has occured");
//switch(@$_POST['step']):
switch(@\EE\Controller\Base::getPostVariable('step')):

	case "D":
		//$id = intval(@$_POST['item_inv']);
		$id = intval(@\EE\Controller\Base::getPostVariable('item_inv'));
		$query = "DELETE FROM inv_count_vend WHERE id = $id";
		Treat_DB_ProxyOld::query($query);
		$affected_rows = mysql_affected_rows();
		//if(Treat_DB_ProxyOld::mysql_affected_rows() > 0) {
		if($affected_rows > 0) {
			$output["status"] = "SUCCESS";
			$output["message"] = "Your request was successful";
		}
		else {
			$output["status"] = "ERROR";
			$output["message"] = "There was an error deleting the record";
		}

		break;

	case "A":
		$post_fields = array("month", "day", "year", "hour", "minute", "ampm", "type", "amount");
		$values = array_fill_keys(array("machine_num", "date_time", "item_code", "onhand", "unit_cost", "pulled", "fills", "waste", "is_inv"), "");
		
		//$values['machine_num'] = $_POST['machine_num'];
		$values['machine_num']= \EE\Controller\Base::getPostVariable('machine_num');
		
		$bid= \EE\Controller\Base::getPostVariable('bid');
		
		//$ts = strtotime($_POST['year'] . "-" . $_POST['month'] . "-" . $_POST['day'] . " " . $_POST['hour'] . ":" . $_POST['minute'] . " " . $_POST['ampm']);
		$ts = strtotime(\EE\Controller\Base::getPostVariable('year'). "-" 
				.\EE\Controller\Base::getPostVariable('month'). "-" 
				.\EE\Controller\Base::getPostVariable('day'). " " 
				.\EE\Controller\Base::getPostVariable('hour'). ":" 
				.\EE\Controller\Base::getPostVariable('minute'). " " 
				. $_POST['ampm']);
				
		$minDate = date("Y-m-d",mktime(0, 0, 0, date("m") , date("d")-13, date("Y")));
		$invYear = @$_POST['year'];
		$invMonth = @$_POST['month'];
		if(strlen($invMonth) < 2){$invMonth = "0$invMonth";}
		$invDay = @$_POST['day'];
		if(strlen($invDay) < 2){$invDay = "0$invDay";}
		$invDate = "$invYear-$invMonth-$invDay";
		
		$values['date_time'] = date("Y-m-d H:i:00", $ts);
		//$values['item_code'] = $_POST['itemcode'];
		$values['item_code'] = \EE\Controller\Base::getPostVariable('itemcode');
		$values['onhand'] = strtoupper(@$_POST['type']) == "COUNT" ? floatval(@$_POST['amount']) : "0";
		$values['fills'] = strtoupper(@$_POST['type']) == "FILL" ? floatval(@$_POST['amount']) : "0";
		$values['waste'] = strtoupper(@$_POST['type']) == "WASTE" ? floatval(@$_POST['amount']) : "0";
		$values['unit_cost'] = "0";
		
		///get cost
		$itemCode = $values["item_code"];
		$query2 = "SELECT mip.cost
			FROM menu_items_price mip
			JOIN menu_items_new min ON min.id = mip.menu_item_id
			WHERE min.businessid =$bid
			AND min.item_code = '$itemCode'";
		$result2 = Treat_DB_ProxyOld::query($query2); 
		
		$r2 = mysql_fetch_array($result2);
		$values['unit_cost'] = $r2['cost'];
		
		$values['pulled'] = "0";
		$values['is_inv'] = strtoupper(@$_POST['type']) == "COUNT" ? "1" : "0";

		if(@$_POST['amount'] == "" || preg_match('/[^0-9\.-]/', @$_POST['amount']))
		{
			$output["status"] = "ERROR";
			$output["message"] = "An invalid amount was given";
		}
		elseif(!checkdate(intval(@$_POST['month']), intval(@$_POST['day']), intval(@$_POST['year'])) || $invDate < $minDate) {
			$output["status"] = "ERROR";
			$output["message"] = "An invalid date was given";
		}
		elseif($ts > time()) {
			$output["status"] = "ERROR";
			$output["message"] = "The time supplied is in the future";
		}
		else
		{
			$values_str = "'" . implode("','", $values) . "'";
			$query = "INSERT INTO inv_count_vend(machine_num, date_time, item_code, onhand, unit_cost, pulled, fills, waste, is_inv)
				VALUES($values_str)";
			$result = Treat_DB_ProxyOld::query($query);
			$insert_id = Treat_DB_ProxyOld::mysql_insert_id();
			if($insert_id > 0) {
				$output["status"] = "SUCCESS";
				$output["message"] = "Your request was successful";
				
				///update shrink queue table for recount
				$date = substr($values['date_time'], 0, 10);
				
				///audit record
				$loginid = @$_COOKIE["logincook"];
				$companyid = @$_COOKIE["compcook"];
				$query = "INSERT INTO audit_inv_count_vend (inv_count_vend_id,loginid,companyid) VALUES ($insert_id, $loginid, $companyid)";
				$result = Treat_DB_ProxyOld::query($query);
				
				$query = "SELECT businessid FROM machine_bus_link WHERE machine_num = '" . $values['machine_num'] . "'";
				$result = Treat_DB_ProxyOld::query($query);
				
				$businessid = mysql_result($result,0,"businessid");
				
				$query = "INSERT INTO dashboard_shrink_recalculate (businessid,date_posted) VALUES ($businessid,'$date')
					ON DUPLICATE KEY UPDATE date_posted = IF('$date' < date_posted, '$date', date_posted);";
				$result = Treat_DB_ProxyOld::query($query);
			}
			else {
				$output["status"] = "ERROR";
				$output["message"] = "There was an error adding that item (" . mysql_errno() . ")";
			}
		}

		break;
		
	case "E":
		$item_id = $_POST["item"];
		$amt = $_POST["amt"];
		$type = $_POST["type"];
		$bid = $_POST["bid"];
		
		if($amt == ""){$amt = 0;}
		
		$query = "SELECT date_time FROM inv_count_vend WHERE id = $item_id";
		$result = Treat_DB_ProxyOld::query($query);
		
		$r = mysql_fetch_array($result);
		
		$date_time = $r["date_time"];
		
		if($type == "COUNT"){
			$query = "UPDATE inv_count_vend SET onhand = '$amt', pulled = 0, fills = 0, waste = 0, is_inv = 1 WHERE id = $item_id";
		}
		elseif($type == "FILL"){
			$query = "UPDATE inv_count_vend SET onhand = 0, pulled = 0, fills = '$amt', waste = 0, is_inv = 0 WHERE id = $item_id";
		}
		elseif($type == "WASTE"){
			$query = "UPDATE inv_count_vend SET onhand = 0, pulled = 0, fills = 0, waste = '$amt', is_inv = 0 WHERE id = $item_id";
		}
		else{
		
		}
		$result = Treat_DB_ProxyOld::query($query);
		$affected_rows = mysql_affected_rows();
	
		if($affected_rows > 0) {
			$loginid = $_COOKIE["logincook"];
			$companyid = $_COOKIE["compcook"];
		
			$query = "INSERT INTO dashboard_shrink_recalculate (businessid,date_posted) VALUES ($bid,'$date_time')
							ON DUPLICATE KEY UPDATE date_posted = IF('$date_time' < date_posted, '$date_time', date_posted)";
			$result = Treat_DB_ProxyOld::query($query);
			
			$query = "INSERT INTO audit_inv_count_vend(inv_count_vend_id,loginid,companyid) VALUES ($item_id,$loginid,$companyid)";
			$result = Treat_DB_ProxyOld::query($query);
		
			$output["status"] = "SUCCESS";
			$output["message"] = "Your request was successful";
		}
		else {
			$output["status"] = "ERROR";
			$output["message"] = "There was an error updating that record.";
		}
		
		sleep(2);
	
		break;

	case "V":

		$minDate = date("Y-m-d",mktime(0, 0, 0, date("m") , date("d")-13, date("Y")));
		$item = $_POST['item'];
		$bid = $_POST['bid'];
		$cur_term = intval($_POST['cur_term']);

		$query = "SELECT * FROM menu_items_new mi JOIN machine_bus_link mb ON (mi.item_code = '$item' AND mi.businessid = '$bid' AND mi.businessid = mb.businessid AND mi.ordertype = mb.ordertype $ex)";
		if($cur_term > 0)$query .= " WHERE mb.client_programid = $cur_term";

		$result = Treat_DB_ProxyOld::query($query);
		$row = Treat_DB_ProxyOld::mysql_fetch_array($result);
		$mnum = $row['machine_num'];
		if($mnum){
			echo "<input type='hidden' name='machine_num' value='$mnum'>";
		}
		else
		{
			die("Error retrieving machine number");
		}


		$query = "SELECT * FROM inv_count_vend WHERE item_code = '$item' AND machine_num = '$mnum' ORDER BY date_time DESC";
		$result = Treat_DB_ProxyOld::query($query);
		if(Treat_DB_ProxyOld::mysql_num_rows($result) == 0){
			echo "No results returned";
			die();
		}
?>

<script type="text/javascript">

jQuery(document).ready( function() {

	jQuery(".deleteLinks")
	.css("color", "blue")
	.mouseover( function() {
		jQuery(this).css("color", "#FF9900");
	})
	.mouseleave( function() {
		jQuery(this).css("color", "blue");
	})
	.click( function() {
		item_id = jQuery(this).attr("id");
		//if(confirm("Are you sure you want to edit this item?")){
		//jQuery('#editForm' + item_id).removeClass('deleteLinks');
		jQuery('#editForm' + item_id).html('<select id=\"newType' + item_id +'\"><option value=\"COUNT\">COUNT</option><option value=\"FILL\">FILL</option><option value=\"WASTE\">WASTE</option></select><input type=text size=5 name=amt id=\"newAmt' + item_id + '\"><button onclick=\"updateItem(' + item_id + ');\">Save</button>');
			/*
			jQuery("#dialog-status").html("Sending data").removeClass().addClass("ui-state-default");
			jQuery.ajax({
				type: 'POST',
				cache: false,
				url: "/ta/buscatermenu5-1.php",
				data: {item_inv:item_id,step:"E"},
				dataType: "html",
				success: function(data){
					data = jQuery.parseJSON(data);
					response = jQuery.extend({}. dataResponse, data);
					if(response.status == "SUCCESS"){
						jQuery("#dialog-status").html(response.message).removeClass().addClass("ui-state-default");
						jQuery("#"+item_id).parent().parent().remove();
					}
					else if(response.status == "ERROR") {
						jQuery("#dialog-status").html(response.message).removeClass().addClass("ui-state-error");
					}
					else {
						jQuery("#dialog-status").html("There was an error deleting the item").removeClass().addClass("ui-state-error");
					}
				},
				error: function() {
					jQuery("#dialog-status").html("There was an error calling the script").removeClass().addClass("ui-state-error");
				}
			});
			*/
		//}
	});
});
</script>

<table border=1>
	<tr>
		<th>Date</th>
		<th>Type</th>
		<th>Amount</th>
		<th></th>
	</tr>

<?php
		$minDate = date("Y-m-d",mktime(0, 0, 0, date("m") , date("d")-13, date("Y")));

		while($row = Treat_DB_ProxyOld::mysql_fetch_array($result)){
			echo "<tr align='center'>";
			echo "<td>" . date("m/d/Y, g:i a", strtotime($row["date_time"])) . "</td>";
			$cnts = array("COUNT" => $row["onhand"], "FILL" => $row["fills"], "WASTE" =>$row["waste"]);
			$tds = "<td>N/A</td><td>0</td>";
			foreach($cnts as $key=>$value){
				if($value != 0 || ($row["is_inv"] == 1)){
					$tds = "<td>$key</td><td>$value</td>";
					break;
				}
			}

			echo $tds;
			if($row["date_time"] < $minDate){
				echo "<td>&nbsp;</td>";
			}
			else{
				//echo "<td><a id='{$row['id']}' href='#' class='deleteLinks'>Edit</a></td>";
				echo "<td><div id='{$row['id']}' class='deleteLinks'>Edit</div></td>";
			}
			echo "<td id=\"editForm{$row['id']}\"></td>";
			echo "</tr>";
		}
?>

</table>

<?php
		die();

	case "U":
		$minDate = date("Y-m-d",mktime(0, 0, 0, date("m") , date("d")-13, date("Y")));
		$minDate .= " 00:00:00";
		$post_date = date("Y-m-d");
	
		$allowed_types = array("text/comma-separated-values", "text/csv", "application/csv", "application/excel",
			"application/vnd.ms-excel", "application/vnd.msexcel", "text/anytext,", "text/plain");
		if(count($_FILES) == 0) {
			$output["status"] = "ERROR";
			$output["message"] = "There was no file uploaded";
		}
		elseif(count($_FILES) > 1) {
			$output["status"] = "ERROR";
			$output["message"] = "Only one file is allowed to be uploaded at a time";
		}
		else {
			$uploaded_file = array_shift($_FILES);
			if($uploaded_file["size"] == 0) {
				$output["status"] = "ERROR";
				$output["message"] = "The uploaded file is empty";
			}
			elseif(!in_array($uploaded_file["type"], $allowed_types)) {
				$output["status"] = "ERROR";
				$output["message"] = "Uploaded file has invalid format";
			}
			elseif($uploaded_file["error"] != 0) {
				$output["status"] = "ERROR";
				$output["message"] = "There was an error with the file upload";
			}
			elseif(($bid = intval(@$_POST['bid'])) == 0) {
				$output["status"] = "ERROR";
				$output["message"] = "Invalid business supplied";
			}
			else {
				$row = 1;
				$required_number_fields = 6;
				$inserts = array("success" => array(), "errors" => array());
				if (($handle = fopen($uploaded_file["tmp_name"], "r")) !== FALSE) {

					$good_machines = array();
					$query = "SELECT machine_num FROM machine_bus_link where businessid = $bid";
					$result = Treat_DB_ProxyOld::query($query);
					while($r = Treat_DB_ProxyOld::mysql_fetch_array($result))$good_machines[] = strtoupper(trim($r['machine_num']));

					$queries = array();
					while (($line = fgets($handle)) !== FALSE) {
						$data = explode(",", $line);
						$num = count($data);
						if($num != $required_number_fields) {
							$output["status"] = "ERROR";
							$output["message"] = "Incorrect number of fields in row $row";
							break;
						}

						foreach($data as $key=>$val)$data[$key] = trim(strtoupper(trim($val, chr(34))));

						$values = array_fill_keys(array("machine_num", "date_time", "item_code", "onhand", "unit_cost", "pulled", "fills", "waste", "is_inv"), "");
						$values['item_code'] = $data[0];

						if(!preg_match('/[0-9\.]/', $data[2])) {
							$output["status"] = "ERROR";
							$output["message"] = "Invalid amount in row $row, amount must be numeric";
						}
						elseif(!preg_match("/^([0-9]{1,2})\/([0-9]{1,2})\/([0-9]{4})\$/", $data[3], $date_parts)) {
							$output["status"] = "ERROR";
							$output["message"] = "Invalid date format in row $row, date must be in MM/DD/YYYY format";
						}
						elseif(!checkdate($date_parts[1], $date_parts[2], $date_parts[3])) {
							$output["status"] = "ERROR";
							$output["message"] = "Invalid date in row $row, you must supply a valid date";
						}
						elseif(!preg_match("/^(0?[1-9]|1[012]):([0-5][0-9]) ([apAP][mM])\$/", $data[4]. $time_parts)) {
							$output["status"] = "ERROR";
							$output["message"] = "Invalid time format in row $row, time must be in HH&#58;MM AM/PM format";
						}
						elseif(!in_array($data[5], array("COUNT", "FILL", "WASTE"), true)) {
							$output["status"] = "ERROR";
							$output["message"] = "Invalid type format in row $row, type must be COUNT, FILL, or WASTE";
						}
						elseif(!in_array(strtoupper($data[1]), $good_machines, true)) {
							$output["status"] = "ERROR";
							$output["message"] = "Invalid machine in row $row, you must enter a machine that exists in this business";
						}
						else {
							$values['machine_num'] = strtoupper($data[1]);
							$date = $date_parts[3] . "-" . $date_parts[1] . "-" . $date_parts[2];
							$time = strtoupper($data[4]);
							$ts = strtotime($date . " " . $time);
							$values['date_time'] = date("Y-m-d H:i:00", $ts);
							
							///check date
							if($values['date_time'] < $minDate){
								$output["status"] = "ERROR";
								$output["message"] = "Invalid Date in File";
								continue;
							}
							else{
								if(substr($values['date_time'],0,10) < $post_date){
									$post_date = substr($values['date_time'],0,10);
								}
							}
							
							$values['onhand'] = strtoupper($data[5]) == "COUNT" ? floatval($data[2]) : "0";
							$values['fills'] = strtoupper($data[5]) == "FILL" ? floatval($data[2]) : "0";
							$values['waste'] = strtoupper($data[5]) == "WASTE" ? floatval($data[2]) : "0";
							$values['unit_cost'] = "0";
							
							///get cost
							$itemCode = $values["item_code"];
							$query2 = "SELECT mip.cost
								FROM menu_items_price mip
								JOIN menu_items_new min ON min.id = mip.menu_item_id
								WHERE min.businessid =$bid
								AND min.item_code =  '$itemCode'";
							$result2 = Treat_DB_ProxyOld::query($query2); 
							
							$r2 = mysql_fetch_array($result2);
							$values['unit_cost'] = $r2['cost'];
							
							$values['pulled'] = "0";
							$values['is_inv'] = strtoupper($data[5]) == "COUNT" ? "1" : "0";

							foreach($values as $key=>$val)$values[$key] = trim(strtoupper($val));

							$values_str = "'" . implode("','", $values) . "'";
							$query = "INSERT INTO inv_count_vend(machine_num, date_time, item_code, onhand, unit_cost, pulled, fills, waste, is_inv)
								VALUES($values_str)";

							$queries[] = $query;
						}

						if($output["status"] == "ERROR") {
							$queries = array();
							break;
						}

						$row++;
					}

					fclose($handle);

					foreach($queries as $query) {
						$result = Treat_DB_ProxyOld::query($query);
						$insert_id = Treat_DB_ProxyOld::mysql_insert_id();

						if($insert_id > 0) {
							$inserts["success"][] = $row;
						}
						else {
						$output["status"] = "ERROR";
						$output["message"] = mysql_error();
							$inserts["errors"][] = $row;
						}
					}
					
					///update shrink queue table for recount
					if($output["status"] != "ERROR"){
						$query = "INSERT INTO dashboard_shrink_recalculate (businessid,date_posted) VALUES ($businessid,'$post_date')
							ON DUPLICATE KEY UPDATE date_posted = IF('$post_date' < date_posted, '$post_date', date_posted);";
						$result = DB_Proxy::query($query);
					}
				}
				else {
					$output["status"] = "ERROR";
					$output["message"] = "The CSV file could not be opened and/or read";

				}

				if($output["status"] != "ERROR") {
					$output["status"] = "SUCCESS";
					$output["message"] = "Upload is complete - " . count($inserts["success"]) . " added, " . count($inserts["errors"]) . " errored";
				}
			}
		}

		break;

	default:
		$output["status"] = "ERROR";
		$output["message"] = "Invalid action";

		break;


endswitch;


header("Content-type: text/html; charset=UTF-8", true);
echo json_encode($output);

?>
