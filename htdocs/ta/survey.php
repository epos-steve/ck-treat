<?php

define( 'DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once( DOC_ROOT . '/bootstrap.php' );

if( require_once( realpath( DOC_ROOT . '/../lib/inc/cookie-login.php' ) ) ) {
	
		$user = Treat_Controller_Abstract::getSessionCookieVariable( 'usercook', '' );
		$companyid = Treat_Controller_Abstract::getSessionCookieVariable( array('companyid','compcook'), '' );
		
		$search = $_POST["search"];
		$date1 = $_POST["date1"];
		$date2 = $_POST["date2"];
		$rating = $_POST["rating"];
		$expand = $_POST["expand"];
		$today = date("Y-m-d");
		if($search == ""){$search = "3:$companyid";}
		if($date1 == ""){
			$date1 = date("Y-m-d");
			$date2 = date("Y-m-d");
		}

		?>
		<html>
			<head>				
				<script type="text/javascript" src="javascripts/jquery-1.4.2.min.js"></script>
				<script type="text/javascript" src="javascripts/jquery-ui-1.7.2.custom.min.js"></script>
				<script type="text/javascript" src="../assets/js/jquery.tablesorter.js"></script>
				<script type="text/javascript">$(document).ready(function (){$('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' });});</script>
				<link rel="stylesheet" href="css/jq_themes/redmond/jquery-ui-1.7.2.custom.css">
				
				<script type="text/javascript">
					$(document).ready(function() 
					{ 
						$("#survey_list").tablesorter(); 
					} 
					); 
				</script>
			</head>
			<body>
			<center>
				<table cellspacing=0 cellpadding=0 border=0 width=90%>
					<tr>
						<td colspan=2>
							<a href="/ta/businesstrack.php"><img src="/ta/logo.jpg" border=0 height=43 width=205 alt="logo" /></a>
							<p></p>
						</td>
					</tr>
					<tr bgcolor=#CCCCFF>
						<td colspan=2>
							<font size=4>
								<b>Site Survey</b>
							</font>
						</td>
					</tr>
					<tr bgcolor=black>
						<td colspan=2 height=1></td>
					</tr>
		<?php
	
		$query = "SELECT company.companyname,
					company.companyid,
					district.districtname,
					district.districtid,
					business.businessname,
					business.businessid
					FROM business
					JOIN district ON district.districtid = business.districtid
					JOIN company ON company.companyid = business.companyid
					WHERE business.companyid = $companyid AND business.categoryid = 4
					ORDER BY company.companyname,district.districtname,business.businessname";
		$result = Treat_DB_ProxyOld::query( $query );
	
		echo "<tr bgcolor=#E8E7E7 valign=top><td><form action=survey.php method=post><center><select name=search size=20>";
	
		while($r = mysql_fetch_array($result)){
			$businessid = $r["businessid"];
			$businessname = $r["businessname"];
			$districtid = $r["districtid"];
			$districtname = $r["districtname"];
			$companyid = $r["companyid"];
			$companyname = $r["companyname"];
		
			if($companyid != $lastcompanyid){
				if($search == "3:$companyid"){$sel="SELECTED";}
				else{$sel = "";}
				echo "<option value=3:$companyid $sel>$companyname</option>";
			}
			if($districtid != $lastdistrictid){
				if($search == "2:$districtid"){$sel="SELECTED";}
				else{$sel = "";}
				echo "<option value=2:$districtid $sel>&nbsp;&nbsp;&nbsp;&nbsp;$districtname</option>";
			}
		
			if($search == "1:$businessid"){$sel="SELECTED";$showbusname = $businessname;$busid=$businessid;}
			else{$sel = "";}
			echo "<option value=1:$businessid $sel>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$businessname</option>";
		
			$lastdistrictid = $districtid;
			$lastcompanyid = $companyid;
		}
		echo "</select><br>
			<input type=submit value='Search' style=\"border:1px solid #777777;background-color:#E8E7E7;font-size:12px;\">
			</center>
			<input type=hidden name=date1 value=$date1>
			<input type=hidden name=date2 value=$date2>
			<input type=hidden name=expand value=$expand>
			<input type=hidden name=rating value=$rating>
			</form></td><td width=70%>";
		
		////filter
		if($expand == 1){$ex_check = "CHECKED";}
		
		if($rating == 1){$rat_sel = "SELECTED";}
		elseif($rating == 2){$rat_sel2 = "SELECTED";}
		elseif($rating == 3){$rat_sel3 = "SELECTED";}
		?>
					<center>
						<form action="survey.php" method="post">
							<input type="hidden" name="search" value="<?php echo $search; ?>">
							Filter: <input type="text" name="date1" value="<?php echo $date1; ?>" size="8" class="datepicker">
							to <input type="text" name="date2" value=<?php echo $date2; ?> size="8" class="datepicker"> 
							<select name="rating"><option value="0"></option><option value="1" <?php echo $rat_sel; ?>>Good</option><option value="2" <?php echo $rat_sel2; ?>>Average</option><option value="3" <?php echo $rat_sel3; ?>>Poor</option></select> 
							<input type="checkbox" value="1" name="expand" <?php echo $ex_check; ?>>Expand 
							<input type="submit" value="GO" style="border:1px solid #777777;background-color:#E8E7E7;font-size:12px;">
						</form>
					</center>
		<?php

		////surveys
		?>
		
			<center>
				<table width="95%" cellspacing="0" cellpadding="0" style="border:1px solid #777777;background:white;">
					<thead><tr style="background:#777777;">
						<th style="color:white;border:1px solid #777777;width:40%;">&nbsp;Business</th>
						<th style="color:white;border:1px solid #777777;width:20%;">&nbsp;Date</th>
						<th style="color:white;border:1px solid #777777;width:20%;">&nbsp;Survey Rating</th>
						<th style="color:white;border:1px solid #777777;width:20%;">&nbsp;Audit</th>
					</tr></thead><tbody>
					
		<?php
		$original_search = $search;
		$search = explode(":", $search);
		
		$download_data = "Business,Date,Survey Rating, Audit\r\n";
		
		if($rating > 0){$rate_query = "AND survey.rating = $rating";}
		
		if($search[0] == 1){
			$query = "SELECT survey.*, business.businessid, business.businessname FROM survey 
						JOIN business ON survey.businessid = business.businessid 
						WHERE survey.businessid = $search[1] 
						AND survey.date BETWEEN '$date1' AND '$date2' 
						$rate_query
						ORDER BY survey.date";
		}
		elseif($search[0] == 2){
			$query = "SELECT survey.*, business.businessid, business.businessname FROM survey 
						JOIN business ON survey.businessid = business.businessid 
						AND business.districtid = $search[1]
						WHERE survey.date BETWEEN '$date1' AND '$date2'
						$rate_query
						ORDER BY survey.date";
		}
		elseif($search[0] == 3){
			$query = "SELECT survey.*, business.businessid, business.businessname FROM survey 
						JOIN business ON survey.businessid = business.businessid 
						AND business.companyid = $search[1]
						WHERE survey.date BETWEEN '$date1' AND '$date2' 
						$rate_query
						ORDER BY survey.date";
		}
		$result = Treat_DB_ProxyOld::query( $query );
		
		$rates = array(0, 0, 0, 0);
		
		while($r = mysql_fetch_array($result)){
			$survey_id = $r["survey_id"];
			$businessid = $r["businessid"];
			$businessname = $r["businessname"];
			$s_date = $r["date"];
			$s_rating = $r["rating"];
			$s_audit = $r["added_by"];
			$s_audit_time = $r["added_when"];
			
			//total surveys
			$surveys++;
			
			//total businesses
			$businesses[$businessid] = $businessname;
			
			if($s_rating == 1){$d_rating = "Good";}
			elseif($s_rating == 2){$d_rating = "Average";}
			elseif($s_rating == 3){$d_rating = "Poor";}
			
			$download_data .= "$businessname,$s_date,$d_rating,$s_audit\r\n";
			
			///current status
			$query2 = "SELECT SUM(rating) AS tot_rating, COUNT(survey_id) AS tot_srvs FROM survey WHERE businessid = $businessid";
			$result2 = Treat_DB_ProxyOld::query($query2);
			
			$tot_rating = mysql_result($result2, 0 , "tot_rating");
			$tot_srvs = mysql_result($result2, 0 , "tot_srvs");
			
			$current_rating = round($tot_rating / $tot_srvs, 0);
			
			if($current_rating == 1){$current_rating = "<img src=greendot.gif title='Current Status: Good'>";}
			elseif($current_rating == 2){$current_rating = "<img src=bluedot.gif title='Current Status: Average'>";}
			elseif($current_rating == 3){$current_rating = "<img src=reddot.gif title='Current Status: Poor'>";}
			else{$current_rating = "<img src=bluedot.gif title='N/A'>";}
			
			///survey rating
			if($s_rating == 1){$srv_rating = "<img src=greendot.gif title='Good'> Good";$rates[1]++;}
			elseif($s_rating == 2){$srv_rating = "<img src=bluedot.gif title='Average'> Average";$rates[2]++;}
			elseif($s_rating == 3){$srv_rating = "<img src=reddot.gif title='Poor'> Poor";$rates[3]++;}
			
			$day = substr(dayofweek($s_date),0,3);
			
			echo "<tr onclick=\"$('#srv$survey_id').show();$('#main-srv$survey_id').css('background-color','#FF8040');$('#main-srv$survey_id').animate({backgroundColor:'#FFFFFF'}, 1000);\" onMouseOver=this.bgColor='yellow' onMouseOut=this.bgColor='white' id='main-srv$survey_id'>
				<td style=\"border:1px solid #777777;\">&nbsp;$current_rating $businessname</td>
				<td style=\"border:1px solid #777777;\">&nbsp;$s_date $day</td>
				<td style=\"border:1px solid #777777;\">&nbsp; $srv_rating</td>
				<td style=\"border:1px solid #777777;\" title='$s_audit_time'>&nbsp;$s_audit</td>
				</tr>";
			
			////details
			if($expand == 1){$display = "";}
			else{$display = "none";}
			
			echo "<tr id='srv$survey_id' style=\"display:$display;\" onclick=\"$('#srv$survey_id').hide();$('#main-srv$survey_id').css('background-color','#FF8040');$('#main-srv$survey_id').animate({backgroundColor:'#E9E9E9'}, 1000);\"><td colspan=4 style=\"border:1px solid #777777;\"><table width=100% style=\"font-size:12px;background:#E8E7E7;\">";
				
			echo "<tr valign=top style=\"font-weight:bold;\"><td width=33%>Category</td><td width=33%>Notes</td><td width=33%>Actions Needed/Taken</td></tr>";
			
			$query2 = "SELECT survey_category.name,survey_detail.notes,survey_detail.action
						FROM survey_detail
						JOIN survey_category ON survey_category.category_id = survey_detail.category_id
						WHERE survey_detail.survey_id = $survey_id";
			$result2 = Treat_DB_ProxyOld::query($query2);
			
			while($r2 = mysql_fetch_array($result2)){
				$cat_name = $r2["name"];
				$notes = $r2["notes"];
				$action = $r2["action"];
				
				$notes = str_replace(",",";",$notes);
				$action = str_replace(",",";",$action);
				
				$notes = str_replace("'","`",$notes);
				$action = str_replace("'","`",$action);
				
				$download_data .= ",\"$cat_name\",\"$notes\",\"$action\"\r\n";
				
				$notes = str_replace("\n", "<br>", $notes);
				$action = str_replace("\n", "<br>", $action);
				
				echo "<tr valign=top onMouseOver=this.bgColor='yellow' onMouseOut=this.bgColor='#E8E7E7'><td width=33% style=\"color:blue;\">$cat_name</td><td width=33%>$notes</td><td width=33%>$action</td></tr>";	
			}
			
			echo "</table></td></tr>";
		}
		echo "</tbody></table><p>";
		
		//totals
		$tot_bus = count($businesses);
		$rate1 = round($rates[1] / $surveys * 100, 1);
		$rate2 = round($rates[2] / $surveys * 100, 1);
		$rate3 = round($rates[3] / $surveys * 100, 1);
		?>
			<center>
				<table width="95%" cellspacing="0" cellpadding="0" style="border:1px solid #777777;background:white;">
					<thead><tr style="background:#777777;">
						<th style="color:white;border:1px solid #777777;width:20%;">&nbsp;Total Surveys</th>
						<th style="color:white;border:1px solid #777777;width:20%;">&nbsp;Total Businesses</th>
						<th style="color:white;border:1px solid #777777;width:60%;" colspan="3">&nbsp;Ratings</th>
						</tr>
					</thead>
					<tbody>
						<tr valign="top">
						<td style="border:1px solid #777777;">&nbsp;<?php echo $surveys; ?></td>
						<td style="border:1px solid #777777;">&nbsp;<?php echo $tot_bus; ?></td>
						<td style="border:1px solid #777777;width:20%;">&nbsp;<img src="greendot.gif"> <?php echo $rates[1]; ?> Good (<?php echo $rate1; ?>%)</td> 
						<td style="border:1px solid #777777;width:20%;">&nbsp;<img src="bluedot.gif"> <?php echo $rates[2]; ?> Average (<?php echo $rate2; ?>%)</td> 
						<td style="border:1px solid #777777;width:20%;">&nbsp;<img src="reddot.gif"> <?php echo $rates[3]; ?> Poor (<?php echo $rate3; ?>%)</td>
						</tr>
					</tbody>
				</table>
			</center><p>
						
		<?php
		
		////new survey
		if($search[0] == 1){
			?>
				<div id="new_link" style="color:blue;" onclick="$('#new_survey').show();$('#new_link').hide();">Add New Survey</div>
				
				<center>
					<form action="survey_save.php" method="post">
						<input type="hidden" name="search" value="<?php echo $original_search; ?>">
						<input type="hidden" name="date1" value="<?php echo $date1; ?>">
						<input type="hidden" name="date2" value="<?php echo $date2; ?>">
						<input type="hidden" name="businessid" value="<?php echo $busid; ?>">
						<table width="95%" cellspacing="0" cellpadding="0" style="border:1px solid #777777;background:white;display:none;" id="new_survey">
							<tr bgcolor="#777777"><td style="color:white;border:1px solid #777777;width:33%;">&nbsp;<?php echo $showbusname; ?> Survey</td><td style="color:white;border:1px solid #777777;width:33%;">Notes</td><td style="color:white;border:1px solid #777777;width:33%;">Action Taken/Needed</td></tr>
							<?php
							
							$query = "SELECT * FROM survey_category ORDER BY category_id";
							$result = Treat_DB_ProxyOld::query( $query );
							
							while($r = mysql_fetch_array($result)){
								$category_id = $r["category_id"];
								$cat_name = $r["name"];
								
								echo "<tr valign=top><td style=\"border:1px solid #777777;\">&nbsp;$cat_name</td>";
								echo "<td style=\"border:1px solid #777777;\"><textarea name='notes$category_id' cols=30 rows=6></textarea></td>";
								echo "<td style=\"border:1px solid #777777;\"><textarea name='action$category_id' cols=30 rows=6></textarea></td></tr>";
							}
							?>
							<tr style="background:#777777;">
								<td colspan="3" align="right" style="border:1px solid #777777;">
									Overall Status: <select name="rating"><option value="1">Good</option><option value="2" SELECTED>Average</option><option value="3">Poor</option></select> 
									Survey Done on:<input type="text" name="survey_date" value=<?php echo $today; ?> size="8" class="datepicker">
									<input type="submit" value="Add">
								</td>
							</tr>
						</table>
					</form>
				</center>	
			<?php
		}
		
		///download
		?>
			<center>
			<form action='posreport.php' method='post' style="margin:0;padding:0;display:inline;">
				<input type=hidden name=download_data value='<? echo $download_data ?>' />
				<input type=submit value='Download' style="border: 1px solid #999999; font-size: 10px; background-color:#CCCCCC;" />	
			</form>
			</center>
				
		<?
		
		echo "</td></tr></table></center><p>";
		?>
		<FORM ACTION=businesstrack.php method=post>
			<center>
				<INPUT TYPE=submit VALUE=' Return to Main Page ' />
			</center>
		</FORM>
		</body>
		</html>
		<?php
}
?>
