<?php

define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once(DOC_ROOT.'/bootstrap.php');

if ( require_once( realpath( DOC_ROOT . '/../lib/inc/cookie-login.php' ) ) ) {
	if (isset($_POST['convert']) && $_POST['convert'] == 1 && $_POST['businessid'] > 0) {
		if (\EE\Model\Business::convertToKiosk(intval($_POST['businessid']))) {
			header( 'Location: /ta/setup_mgr.php?cid=0&popup_bid='.intval($_POST['businessid']));
			exit();
		}
	} else if (isset($_POST['companyid'])) {
		\EE\Model\Business::setupKiosk(intval($_POST['companyid']));
	}

	header( 'Location: ' . $_SERVER[ 'HTTP_REFERER' ]);
}