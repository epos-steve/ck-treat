<?php
//ini_set("display_errors","true");

if ( !defined('DOC_ROOT'))
{
	define('DOC_ROOT', dirname(dirname(__FILE__)));
}
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');
 $style = "text-decoration:none";

require("lib/class.labor.php");
require("lib/class.graph.php");

////who
$business = array(151 => "Indiana Vending Operations", 97 => "KC Vending Operations", 106 => "Omaha Vending Operations");

////number of weeks
$numweeks=25;

///build date array
$today = date("Y-m-d");
while(dayofweek($today)!="Tuesday"){$today=prevday($today);}
while(dayofweek($today)!="Friday"){$today=prevday($today);}
$counter=$numweeks;
$labels[$numweeks+1]=$today;
while($counter>=1){
    for($count=1;$count<=7;$count++){
        $today=prevday($today);
    }
    $labels[$counter]=$today;
    $counter--;
}
asort($labels);

////get data
foreach($business AS $key => $value){
    for ($counter=1;$counter<=$numweeks;$counter++){
        $values[$key][$counter] = Labor::get_labor(nextday($labels[$counter]), $labels[$counter+1], $key, 3, 1);
    }
}

////display
array_shift($labels);
$mygraph = LineGraph::drawGraph($values,$labels,$business);
echo $mygraph;

?>