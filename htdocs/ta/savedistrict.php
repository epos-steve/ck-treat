<?php
require_once( __DIR__ . '/../../application/bootstrap.php' );

$style = "text-decoration:none";

$user = \EE\Controller\Base::getPostVariable('username');
$pass = \EE\Controller\Base::getPostVariable('password');
$businessid = \EE\Controller\Base::getPostVariable('businessid');
$companyid = \EE\Controller\Base::getPostVariable('companyid');
$districtid = \EE\Controller\Base::getPostVariable('districtid');
$districtname = \EE\Controller\Base::getPostVariable('districtname');
$divisionid = \EE\Controller\Base::getPostVariable('divisionid');
$arrequest = \EE\Controller\Base::getPostVariable('arrequest');
$aprequest = \EE\Controller\Base::getPostVariable('aprequest');
$labor_request = \EE\Controller\Base::getPostVariable('labor_request');
$dm = \EE\Controller\Base::getPostVariable('dm');
$submit = \EE\Controller\Base::getPostVariable('submit');
$email_submit = \EE\Controller\Base::getPostVariable('email_submit');
$labor_submit = \EE\Controller\Base::getPostVariable('labor_submit');
$labor_email = \EE\Controller\Base::getPostVariable('labor_email');
$bad_ar = \EE\Controller\Base::getPostVariable('bad_ar');
$bad_ar_email = \EE\Controller\Base::getPostVariable('bad_ar_email');
$email_orders = \EE\Controller\Base::getPostVariable('email_orders');
$deactivate_items = \EE\Controller\Base::getPostVariable('deactivate_items');
$from_bid = \EE\Controller\Base::getPostVariable('from_bid');
$from_cid = \EE\Controller\Base::getPostVariable('from_cid');

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num = Treat_DB_ProxyOld::mysql_num_rows($result);


if ($num != 1 || $districtname == "")
{
	echo "<center><h3>Failed</h3></center>";
}
else
{
	///production
	$production = 0;
	if (isset($_POST['production']) && is_array($_POST['production'])) {
		$production = array_sum($_POST['production']);
	}

	if ($districtid > 0) {
		$districtObject = \EE\Model\District::get("districtid = {$districtid}", true);
	} else {
		$districtObject = new \EE\Model\District;
	}

	$districtObject->districtname = $districtname;
	$districtObject->divisionid = $divisionid;
	$districtObject->arrequest = $arrequest;
	$districtObject->aprequest = $aprequest;
	$districtObject->labor_request = $labor_request;
	$districtObject->dm = $dm;
	$districtObject->submit = $submit;
	$districtObject->email_submit = $email_submit;
	$districtObject->labor_submit = $labor_submit;
	$districtObject->labor_email = $labor_email;
	$districtObject->bad_ar = $bad_ar;
	$districtObject->bad_ar_email = $bad_ar_email;
	$districtObject->email_orders = $email_orders;
	$districtObject->production = $production;
	$districtObject->deactivate_items = $deactivate_items;
	$districtObject->kiosk_activation_emails = $_POST['kiosk_activation_emails'];
	$districtObject->save();

	///kiosk orders
	$query2 = "
		SELECT
			kiosk_order_type.id,
			kiosk_order_type.ordertype,
			kiosk_order_sched.active,
			kiosk_order_sched.datetime,
			kiosk_order_sched.weekend
		FROM kiosk_order_type
		LEFT JOIN kiosk_order_sched ON
			kiosk_order_sched.ordertype = kiosk_order_type.id
			AND kiosk_order_sched.districtid = $districtid
		ORDER BY kiosk_order_type.id
	";
	$result2 = Treat_DB_ProxyOld::query($query2);

	while($r2 = mysql_fetch_array($result2)){
		$id = $r2["id"];

		$active = @$_POST["active$id"];
		$hour = @$_POST["hour$id"];
		$minute = @$_POST["minute$id"];
		$weekend = @$_POST["weekend$id"];
		$email = @$_POST["email$id"];
		$route_total = @$_POST["route_total$id"];

		$datetime = "$hour$minute";

		$query3 = "
			INSERT INTO kiosk_order_sched
			(
				districtid,
				ordertype,
				active,
				datetime,
				weekend,
				email,
				route_total
			) VALUES (
				$districtid,
				$id,
				'$active',
				'$datetime',
				'$weekend',
				'$email',
				'$route_total'
			)
			ON DUPLICATE KEY UPDATE
				active = '$active',
				datetime = '$datetime',
				weekend = '$weekend',
				email = '$email',
				route_total = '$route_total'
		";
		Treat_DB_ProxyOld::query($query3);
	}
}

$location = "editdistrict.php?districtid=$districtid&from_cid=$from_cid&from_bid=$from_bid";
header('Location: ./' . $location);
