<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once(DOC_ROOT.'/bootstrap.php');

require("lib/class.dashboard.php");
require("lib/class.labor.php");
require("lib/class.graph.php");
require("lib/class.dates.php");
?>
<html>
    <head>
		
    </head>

    <body style="margin: 0px; font-family: arial; font-size: 10px; font-weight: bold;">
        
<?php
/*$user = isset($_COOKIE["usercook"])?$_COOKIE["usercook"]:'';
$pass = isset($_COOKIE["passcook"])?$_COOKIE["passcook"]:'';*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook','');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook','');

$query = "SELECT loginid FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num = Treat_DB_ProxyOld::mysql_numrows($result);

    if($num==1){
        $loginid = Treat_DB_ProxyOld::mysql_result($result,0,"loginid");

        $today=date("Y-m-d");

        $query = "SELECT * FROM db_view WHERE loginid = '4662' ORDER BY db_order";
        $result = Treat_DB_ProxyOld::query($query);

		echo '<div id="sortable">';
        while($r=Treat_DB_ProxyOld::mysql_fetch_array($result)){
            $viewid = $r["viewid"];
            $graph_name = $r["name"];
            $graph_type = $r["graph_type"];
            $size = $r["size"];
            $row = $r["db_row"];
            $column = $r["db_column"];

            $period = $r["period"];
            $period_num = $r["period_num"];
            $period_group = $r["period_group"];
            $current = $r["current"];

?>
	<div id="<?php echo $viewid; ?>" class="ui-state-default item<?php if ($size == 2) echo ' full'; ?>">
		<table width="100%" cellspacing="0" cellpadding="0" style="border: 2px solid #E8E7E7; font-size: 14px; font-weight: bold;" id="graph<?php echo $viewid; ?>" bgcolor="white">
			<tr class="ui-widget-header" bgcolor="#E8E7E7" valign="middle" style="cursor: move;">
				<td>&nbsp;<?php echo $graph_name; ?> </td>
				<td align="right">
					<a href="custom_db/display_graph.php?viewid=<?php echo $viewid; ?>&ex=1" target="_blank"><img src="expand.gif" border=0 height=10 width=10 title="Expand"></a>
					<a href="custom_db/display_graph.php?viewid=<?php echo $viewid; ?>&ex=-1"><img src="download.gif" border="0" height="10" width="10" title="Export"></a>
				</td>
			</tr>
			<tr height="125">
				<td colspan="2" width="100%">
					<div width="100%" class="graph" style="overflow:auto; text-align:center; vertical-align:middle;"></div>
				</td>
			</tr>
		</table>
	</div>
    <?php
        }

        echo "</div>";

    }
    ?>
    </body>
</html>
