<?php

define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once(DOC_ROOT.'/bootstrap.php');

if ( require_once( realpath( DOC_ROOT . '/../lib/inc/cookie-login.php' ) ) ) {
	$businessid = $_POST["businessid"];
	$companyid = $_POST["companyid"];
	$showall = isset($_COOKIE["showall"])?$_COOKIE["showall"]:'';

	if($showall==1){
		$query = "SELECT * FROM menu_items_new WHERE businessid = $businessid ORDER BY ordertype,name";
	}
	else{
		$query = "SELECT * FROM menu_items_new WHERE businessid = $businessid AND active = 0 ORDER BY ordertype,name";
	}
	$result = DB_Proxy::query($query);

	while($r = mysql_fetch_array($result)){
		$id = $r["id"];

		$ordertype = $_POST["ordertype$id"];
		$manufacturer = $_POST["manufacturer$id"];
		$item_code = $_POST["item_code$id"];
		$active = $_POST["active$id"];
		$upc = $_POST["upc$id"];
		$max = $_POST["max$id"];
		$reorder_point = $_POST["reorder_point$id"];
		$reorder_amount = $_POST["reorder_amount$id"];
		$new_price = $_POST["new_price$id"];
		$cost = $_POST["cost$id"];

		$query2 = "UPDATE menu_items_new SET
					ordertype = $ordertype,
					manufacturer = $manufacturer,
					pos_processed = 0,
					item_code = '$item_code',
					active = '$active',
					upc = '$upc',
					max = '$max',
					reorder_point = '$reorder_point',
					reorder_amount = '$reorder_amount'
				WHERE id = $id";
		$result2 = DB_Proxy::query($query2);
		if (mysql_affected_rows ($result2) > 0) {
			Treat_Model_KioskSync::addSync($businessid, $id, Treat_Model_KioskSync::TYPE_ITEM);
			Treat_Model_KioskSync::addSync($businessid, $id, Treat_Model_KioskSync::TYPE_ITEM_TAX);
		}

		if($new_price != ""){
			$query2 = "UPDATE menu_items_price SET
					new_price = '$new_price'
				WHERE menu_item_id = $id";
			$result2 = DB_Proxy::query($query2);
			Treat_Model_KioskSync::addSync($businessid, $id, Treat_Model_KioskSync::TYPE_ITEM);
			Treat_Model_KioskSync::addSync($businessid, $id, Treat_Model_KioskSync::TYPE_ITEM_TAX);
		}

		///cost
		$query2 = "UPDATE menu_items_price SET
					cost = '$cost'
				WHERE menu_item_id = $id";
		$result2 = DB_Proxy::query($query2);

	}
	
	header( 'Location: ' . $_SERVER[ 'HTTP_REFERER' ] );
}
