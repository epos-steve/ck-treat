<?php

function money($diff){
	$findme='.';
	$double='.00';
	$single='0';
	$double2='00';
	$pos1 = strpos($diff, $findme);
	$pos2 = strlen($diff);
	if ($pos1==""){$diff="$diff$double";}
	elseif ($pos2-$pos1==2){$diff="$diff$single";}
	elseif ($pos2-$pos1==1){$diff="$diff$double2";}
	else{}
	$dot = strpos($diff, $findme);
	$diff = substr($diff, 0, $dot+3);
	if ($diff > 0 && $diff < '0.01'){$diff="0.01";}
	elseif($diff < 0 && $diff > '-0.01'){$diff="-0.01";}
	return $diff;
}

function nextday($date2)
{
	$day=substr($date2,8,2);
	$month=substr($date2,5,2);
	$year=substr($date2,0,4);
	$leap = date("L");

	if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
	{
		if ($month == "01"){$month="02";}
		if ($month == "03"){$month="04";}
		if ($month == "05"){$month="06";}
		if ($month == "07"){$month="08";}
		if ($month == "08"){$month="09";}
		if ($month == "10"){$month="11";}
		$day='01';
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	} else if ($month=='02' && $day == '29' && $leap == '0'){
		$month='03';
		$day='01';
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	} else if ($month=='02' && $day == '28'){
		$month='03';
		$day='01';
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	} else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11')){
		if ($month == "04"){$month="05";}
		if ($month == "06"){$month="07";}
		if ($month == "09"){$month="10";}
		if ($month == "11"){$month="12";}
		$day='01';
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	} else if ($month=='12' && $day=='32'){
		$day='01';
		$month='01';
		$year++;
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	} else {
		$day=$day+1;
		if ($day<10){
			$day="0$day";
		}
		$tomorrow="$year-$month-$day";
		return $tomorrow;
	}
}

function prevday($date2){
	$day=substr($date2,8,2);
	$month=substr($date2,5,2);
	$year=substr($date2,0,4);
	$day=$day-1;

	if ($day <= 0){
		if ($month == 01){
			$month = '12';
			$year--;
			$day=$day+31;
			$yesterday = "$year-$month-$day";
			return $yesterday;
		} else if ($month=='02' || $month=='04' || $month=='06' || $month=='09' || $month=='11'){
			$month--;
			if ($month < 10){
				$month="0$month";
			}
			$day=$day+31;
			$yesterday="$year-$month-$day";
			return $yesterday;
		} else if ($month=='05' || $month=='07' || $month=='08' || $month=='10' || $month=='12'){
		$month--;
		if ($month < 10){
			$month="0$month";
		}
		$day=$day+30;
		$yesterday="$year-$month-$day";
		return $yesterday;
		} else {
			$day=$day+28;
			$month='02';
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
	} else {
		if ($day < 10){
			$day="0$day";
		}
		$yesterday="$year-$month-$day";
		return $yesterday;
	}
}

function futureday($num){
	$day = date("d");
	$year = date("Y");
	$month = date("m");
	$leap = date("L");
	$day=$day+$num;

	if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32'){
		if ($month == "03"){$month="04";}
		if ($month == "05"){$month="06";}
		if ($month == "07"){$month="08";}
		if ($month == "08"){$month="09";}
		$day=$day-31;
		if ($day<10){$day="0$day";}
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	} else if ($month=='02' && $leap == '1' && $day >= '29'){
		$month='03';
		$day=$day-29;
		if ($day<10){$day="0$day";}
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	} else if ($month=='02' && $leap == '0' && $day >= '28'){
		$month='03';
		$day=$day-28;
		if ($day<10){$day="0$day";}
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	} else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31'){
		if ($month == "04"){$month="05";}
		if ($month == "06"){$month="07";}
		if ($month == "09"){$month="10";}
		if ($month == "11"){$month="12";}
		$day=$day-30;
		if ($day<10){$day="0$day";}
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	} else if ($month==12 && $day>=32){
		$day=$day-31;
		if ($day<10){$day="0$day";}
		$month='01';
		$year++;
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	} else {
	if ($day<10){$day="0$day";}
		$tomorrow="$year-$month-$day";
		return $tomorrow;
	}
}

function pastday($num){
	$day = date("d");
	$day=$day-$num;
	if ($day <= 0){
		$year = date("Y");
		$month = date("m");
		if ($month == 01){
			$month = '12';
			$year--;
			$day=$day+31;
			$yesterday = "$year-$month-$day";
			return $yesterday;
		} else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11'){
			$month--;
			if ($month < 10){
				$month="0$month";
			}
			$day=$day+31;
			$yesterday="$year-$month-$day";
			return $yesterday;
		} else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12'){
			$month--;
			if ($month < 10)
			{
			$month="0$month";
			}
			$day=$day+30;
			$yesterday="$year-$month-$day";
			return $yesterday;
		} else {
			$day=$day+28;
			$month='02';
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
	} else {
		if ($day < 10){
			$day="0$day";
		}
		$year=date("Y");
		$month=date("m");
		$yesterday="$year-$month-$day";
		return $yesterday;
	}
}

define('DOC_ROOT', dirname(dirname(__FILE__)));
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');

$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");
$creditamount="creditamount";
$creditidnty="creditidnty";
$creditdate="creditdate";
$debitamount="debitamount";
$debitidnty="debitidnty";
$debitdate="debitdate";
$quatertotal=0;
$monthtotal=0;
$total=0;
$counter=0;
$findme='.';
$double='.00';
$single='0';
$double2='00';

/*$user = isset($_COOKIE["usercook"])?$_COOKIE["usercook"]:'';
$pass = isset($_COOKIE["passcook"])?$_COOKIE["passcook"]:'';
$view = isset($_COOKIE["viewcook"])?$_COOKIE["viewcook"]:'';
$date1 = isset($_COOKIE["date1cook"])?$_COOKIE["date1cook"]:'';
$date2 = isset($_COOKIE["date2cook"])?$_COOKIE["date2cook"]:'';
$businessid = isset($_GET["bid"])?$_GET["bid"]:'';*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook','');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook','');
$view = \EE\Controller\Base::getSessionCookieVariable('viewcook','');
$date1 = \EE\Controller\Base::getSessionCookieVariable('date1cook','');
$date2 = \EE\Controller\Base::getSessionCookieVariable('date2cook','');
$businessid = \EE\Controller\Base::getGetVariable('bid','');

setcookie("caterbus",$businessid);

/*$companyid = isset($_GET["cid"])?$_GET["cid"]:'';
$view2 = isset($_GET["view2"])?$_GET["view2"]:'';
$roomeditid = isset($_GET["rid"])?$_GET["rid"]:'';*/

$companyid = \EE\Controller\Base::getGetVariable('cid','');
$view2 = \EE\Controller\Base::getGetVariable('view2','');
$roomeditid = \EE\Controller\Base::getGetVariable('rid','');

if ($businessid==""&&$companyid==""){
	/*$businessid = isset($_POST["businessid"])?$_POST["businessid"]:'';
	$companyid = isset($_POST["companyid"])?$_POST["companyid"]:'';
	$view2 = isset($_POST["view2"])?$_POST["view2"]:'';
	$date1 = isset($_POST["date1"])?$_POST["date1"]:'';
	$date2 = isset($_POST["date2"])?$_POST["date2"]:'';
	$findinv = isset($_POST["findinv"])?$_POST["findinv"]:'';*/
	
	$businessid = \EE\Controller\Base::getPostVariable('businessid','');
	$companyid = \EE\Controller\Base::getPostVariable('companyid','');
	$view2 = \EE\Controller\Base::getPostVariable('view2','');
	$date1 = \EE\Controller\Base::getPostVariable('date1','');
	$date2 = \EE\Controller\Base::getPostVariable('date2','');
	$findinv = \EE\Controller\Base::getPostVariable('findinv','');
}

if ($date1==""&&$date2==""){
	$previous=$today;
	for ($count=1;$count<=31;$count++){
		$previous=prevday($previous);
	}
	$date1=$previous;$date2=$today;$view='All';
}


//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=Treat_DB_ProxyOld::mysql_numrows($result);

$security_level=@Treat_DB_ProxyOld::mysql_result($result,0,"security_level");
$mysecurity=@Treat_DB_ProxyOld::mysql_result($result,0,"security");
$bid=@Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
$cid=@Treat_DB_ProxyOld::mysql_result($result,0,"companyid");
$bid2=@Treat_DB_ProxyOld::mysql_result($result,0,"busid2");
$bid3=@Treat_DB_ProxyOld::mysql_result($result,0,"busid3");
$bid4=@Treat_DB_ProxyOld::mysql_result($result,0,"busid4");
$bid5=@Treat_DB_ProxyOld::mysql_result($result,0,"busid5");
$bid6=@Treat_DB_ProxyOld::mysql_result($result,0,"busid6");
$bid7=@Treat_DB_ProxyOld::mysql_result($result,0,"busid7");
$bid8=@Treat_DB_ProxyOld::mysql_result($result,0,"busid8");
$bid9=@Treat_DB_ProxyOld::mysql_result($result,0,"busid9");
$bid10=@Treat_DB_ProxyOld::mysql_result($result,0,"busid10");
$loginid=@Treat_DB_ProxyOld::mysql_result($result,0,"loginid");

$pr1=@Treat_DB_ProxyOld::mysql_result($result,0,"payroll");
$pr2=@Treat_DB_ProxyOld::mysql_result($result,0,"pr2");
$pr3=@Treat_DB_ProxyOld::mysql_result($result,0,"pr3");
$pr4=@Treat_DB_ProxyOld::mysql_result($result,0,"pr4");
$pr5=@Treat_DB_ProxyOld::mysql_result($result,0,"pr5");
$pr6=@Treat_DB_ProxyOld::mysql_result($result,0,"pr6");
$pr7=@Treat_DB_ProxyOld::mysql_result($result,0,"pr7");
$pr8=@Treat_DB_ProxyOld::mysql_result($result,0,"pr8");
$pr9=@Treat_DB_ProxyOld::mysql_result($result,0,"pr9");
$pr10=@Treat_DB_ProxyOld::mysql_result($result,0,"pr10");

if ($num != 1 || ($security_level==1 && ($bid != $businessid || $cid != $companyid)) || ($security_level > 1 AND $cid != $companyid) || $user == "" || $pass == ""){
	echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
} else {
	?>
	<head>
		<style>
			#add-cater-policy { width: 225px; }
			.add-cater-policy-line { width: 225px; float: left; margin: 5px; }
		</style>
	<script language="JavaScript" type="text/JavaScript">
	function changePage(newLoc){
		nextPage = newLoc.options[newLoc.selectedIndex].value
		if (nextPage != ""){
			document.location.href = nextPage
		}
	}
	</script>
	</head>
	
	<center><table cellspacing="0" cellpadding="0" border="0" width="90%"><tr><td colspan="2">
	<a href="businesstrack.php"><img src="logo.jpg" border="0" height="43" width="205"></a><p></td></tr>
	<?php
	
	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM company WHERE companyid = '$companyid'";
	$result = Treat_DB_ProxyOld::query($query);
	//mysql_close();

	$companyname=@Treat_DB_ProxyOld::mysql_result($result,0,"companyname");
	$com_logo=@Treat_DB_ProxyOld::mysql_result($result,0,"logo");

	if($com_logo == ""){$com_logo = "talogo.gif";}

	$caterpolicyquery = "SELECT policy FROM mburris_businesstrack.caterpolicy WHERE businessid = '$businessid' LIMIT 1";
	$caterpolicyresult = Treat_DB_ProxyOld::query($caterpolicyquery);
	$caterpolicytext = @Treat_DB_ProxyOld::mysql_result($caterpolicyresult,0,"policy");	

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM security WHERE securityid = '$mysecurity'";
	$result = Treat_DB_ProxyOld::query($query);
	//mysql_close();

	$sec_bus_sales=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_sales");
	$sec_bus_invoice=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_invoice");
	$sec_bus_order=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_order");
	$sec_bus_payable=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_payable");
	$sec_bus_inventor1=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor1");
	$sec_bus_control=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_control");
	$sec_bus_menu=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_menu");
	$sec_bus_order_setup=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_order_setup");
	$sec_bus_request=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_request");
	$sec_route_collection=@Treat_DB_ProxyOld::mysql_result($result,0,"route_collection");
	$sec_route_labor=@Treat_DB_ProxyOld::mysql_result($result,0,"route_labor");
	$sec_route_detail=@Treat_DB_ProxyOld::mysql_result($result,0,"route_detail");
	$sec_bus_labor=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_labor");
	$sec_bus_budget=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_budget");

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM business WHERE companyid = '$companyid' AND businessid = '$businessid'";
	$result = Treat_DB_ProxyOld::query($query);
	//mysql_close();

	$businessname=@Treat_DB_ProxyOld::mysql_result($result,0,"businessname");
	$businessid=@Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
	$street=@Treat_DB_ProxyOld::mysql_result($result,0,"street");
	$city=@Treat_DB_ProxyOld::mysql_result($result,0,"city");
	$state=@Treat_DB_ProxyOld::mysql_result($result,0,"state");
	$zip=@Treat_DB_ProxyOld::mysql_result($result,0,"zip");
	$phone=@Treat_DB_ProxyOld::mysql_result($result,0,"phone");
	$caternum=@Treat_DB_ProxyOld::mysql_result($result,0,"caternum");
	$caterdays=@Treat_DB_ProxyOld::mysql_result($result,0,"caterdays");
	$srvchrg=@Treat_DB_ProxyOld::mysql_result($result,0,"srvchrg");
	$srvchrgpcnt=@Treat_DB_ProxyOld::mysql_result($result,0,"srvchrgpcnt");
	$srvchrg_name=@Treat_DB_ProxyOld::mysql_result($result,0,"srvchrg_name");
	$weekend=@Treat_DB_ProxyOld::mysql_result($result,0,"weekend");
	$unit_add=@Treat_DB_ProxyOld::mysql_result($result,0,"unit_add");
	$inv_note=@Treat_DB_ProxyOld::mysql_result($result,0,"inv_note");
	$cater_min=@Treat_DB_ProxyOld::mysql_result($result,0,"cater_min");
	$cater_spread=@Treat_DB_ProxyOld::mysql_result($result,0,"cater_spread");
	$cater_costcenter=@Treat_DB_ProxyOld::mysql_result($result,0,"cater_costcenter");
	$bus_unit=@Treat_DB_ProxyOld::mysql_result($result,0,"unit");

	if($cater_spread==0){
		$catspr1="SELECTED";
	} elseif($cater_spread==15){
		$catspr15="SELECTED";
	} elseif($cater_spread==30){
		$catspr30="SELECTED";
	} elseif($cater_spread==45){
		$catspr45="SELECTED";
	} elseif($cater_spread==60){
		$catspr60="SELECTED";
	}

	if($cater_costcenter==1){
		$check_costcenter="CHECKED";
	}
	
	?>

	<!--tr bgcolor=black><td colspan=3 height=1></td></tr-->
	<tr bgcolor="#CCCCFF"><td width="1%"><img src="logo/<?php echo $com_logo; ?>"></td><td><font size="4"><b><?php echo $businessname; ?> #<?php echo $bus_unit; ?></b></font><br><?php echo $street; ?><br><?php echo $city . "," . $state . $zip; ?><br><?php echo $phone; ?></td>
	<td align="right" valign="top">
	<br><FORM ACTION=editbus.php method="post">
	<input type="hidden" name="username" value="<?php echo $user; ?>">
	<input type="hidden" name="password" value="<?php echo $pass; ?>">
	<input type="hidden" name="businessid" value="<?php echo $businessid; ?>">
	<input type="hidden" name="companyid" value="<?php echo $companyid; ?>">
	<input type="submit" VALUE=' Store Setup '></FORM></td></tr>
	<!--tr bgcolor=black><td colspan=3 height=1></td></tr-->

	<tr><td colspan="3"><font color="#999999">
	
	<?php

	if ($sec_bus_sales>0){
		echo "<a href=busdetail.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Sales</font></a>";
	}

	if ($sec_bus_invoice>0){
		echo " | <a href=businvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Invoicing</font></a>";
	}

	if ($sec_bus_order>0){
		echo " | <a href=buscatertrack.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Orders</font></a>";
	}

	if ($sec_bus_payable>0){
		echo " | <a href=busapinvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Payables</font></a>";
	}

	if ($sec_bus_inventor1>0){
		echo " | <a href=businventor.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Inventory</font></a>";
	}

	if ($sec_bus_control>0){
		echo " | <a href=buscontrol.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Control Sheet</font></a>";
	}

	if ($sec_bus_menu>0){
		echo " | <a href=buscatermenu.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Menus</font></a>";
	}

	if ($sec_bus_order_setup>0){
		echo " | <a href=buscaterroom.php?bid=$businessid&cid=$companyid><font color=red onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='red' style='text-decoration:none'>Order Setup</font></a>";
	}

	if ($sec_bus_request>0){
		echo " | <a href=busrequest.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Requests</font></a>";
	}

	if ($sec_route_collection>0||$sec_route_labor>0||$sec_route_detail>0){
		echo " | <a href=busroute_collect.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Route Collections</font></a>";
	}

	if ($sec_bus_labor>0){
		echo " | <a href=buslabor.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Labor</font></a>";
	}

	if ($sec_bus_budget>0||($businessid == $bid && $pr1 == 1)||($businessid == $bid2 && $pr2 == 1)||($businessid == $bid3 && $pr3 == 1)||($businessid == $bid4 && $pr4 == 1)||($businessid == $bid5 && $pr5 == 1)||($businessid == $bid6 && $pr6 == 1)||($businessid == $bid7 && $pr7 == 1)||($businessid == $bid8 && $pr8 == 1)||($businessid == $bid9 && $pr9 == 1)||($businessid == $bid10 && $pr10 == 1)){
		echo " | <a href=busbudget.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Budget</font></a>";
	}

	echo "</td></tr>";

	echo "</table></center><p>";

	if ($security_level>0){
		echo "<p><center><table width=90%><tr><td width=50%><form action=businvoice.php method=post>";
		echo "<select name=gobus onChange='changePage(this.form.gobus)'>";

		$districtquery="";
		if ($security_level>2&&$security_level<7){
			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query = "SELECT * FROM login_district WHERE loginid = '$loginid'";
			$result = Treat_DB_ProxyOld::query($query);
			$num=Treat_DB_ProxyOld::mysql_numrows($result);
			//mysql_close();

			$num--;
			while($num>=0){
				$districtid=@Treat_DB_ProxyOld::mysql_result($result,$num,"districtid");
				if($districtquery==""){
					$districtquery="districtid = '$districtid'";
				} else {
					$districtquery="$districtquery OR districtid = '$districtid'";
				}
				$num--;
			}
		}

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		if($security_level>6){
			$query = "SELECT * FROM business WHERE companyid = '$companyid' ORDER BY businessname DESC";
		} elseif($security_level==2||$security_level==1){
			$query = "SELECT * FROM business WHERE companyid = '$companyid' AND (businessid = '$bid' OR businessid = '$bid2' OR businessid = '$bid3' OR businessid = '$bid4' OR businessid = '$bid5' OR businessid = '$bid6' OR businessid = '$bid7' OR businessid = '$bid8' OR businessid = '$bid9' OR businessid = '$bid10') ORDER BY businessname DESC";
		} elseif($security_level>2&&$security_level<7){
			$query = "SELECT * FROM business WHERE companyid = '$companyid' AND ($districtquery) ORDER BY businessname DESC";
		}

		$result = Treat_DB_ProxyOld::query($query);
		$num=Treat_DB_ProxyOld::mysql_numrows($result);
		//mysql_close();

		$num--;
		while($num>=0){
			$businessname=@Treat_DB_ProxyOld::mysql_result($result,$num,"businessname");
			$busid=@Treat_DB_ProxyOld::mysql_result($result,$num,"businessid");
			if ($busid==$businessid){
				$show="SELECTED";
			} else {
				$show="";
			}
			echo "<option value=buscaterroom.php?bid=$busid&cid=$companyid $show>$businessname</option>";
			$num--;
		}
		echo "</select></td><td></form></td><td width=50% align=right></td></tr></table></center><p>";
	}

	?>
	
	<p>
	<center>
	<table width="90%" cellspacing=0 cellpadding=0>
		<tr bgcolor="#E8E7E7" valign="top">
			<td width="1%"><img src="leftcrn.jpg" height="6" width="6" align="top"></td>
			<td width="15%"><center><a href=buscaterroom.php?cid=<?php echo $companyid; ?>&bid=<?php echo $businessid; ?> style=<?php echo $style; ?>><font color="black">Rooms/Settings</a></center></td>
			<td width="1%" align="right"><img src="rightcrn.jpg" height="6" width="6" align="top"></td>
			<td width="1%" bgcolor="white"></td>
			<td width="1%" bgcolor="#CCCCCC"><img src="leftcrn2.jpg" height="6" width="6" align="top"></td>
			<td width="15%" bgcolor="#CCCCCC"><center><a href=buscaterroom2.php?cid=<?php echo $companyid; ?>&bid=<?php echo $businessid; ?> style=<?php echo $style; ?> style=<?php echo $style; ?>><font color="blue">Custom Fields</a></center></td>
			<td width="1%" align="right" bgcolor="#CCCCCC"><img src="rightcrn2.jpg" height="6" width="6" align="top"></td>
			<td width="1%" bgcolor="white"></td>
			<td width="1%" bgcolor="#CCCCCC"><img src="leftcrn2.jpg" height="6" width="6" align="top"></td>
		<td width="15%" bgcolor="#CCCCCC">
		<center>
			<a href=buscaterroom3.php?cid=<?php echo $companyid; ?>&bid=<?php echo $businessid; ?> style=<?php echo $style; ?> style=<?php echo $style; ?>><font color="blue">Inactive Groups</a>
		</center>
			</td>
			<td width="1%" align="right" bgcolor="#CCCCCC"><img src="rightcrn2.jpg" height="6" width="6" align="top"></td>
			<td width="1%" bgcolor="white"></td>
			<td bgcolor="white" width="60%"></td>
		</tr>
		<tr height="2">
			<td colspan="3" bgcolor="#E8E7E7"></td>
			<td colspan="4"></td>
			<td colspan="4"></td>
			<td></td>
			<td colspan="1"></td>
		</tr>
	</table>

	<center>
		<table cellspacing=0 cellpadding=0 border=0 width="90%">
			<tr bgcolor="#E8E7E7">
				<td height="1" colspan="3"><b>Room</b></td>
				<td><b>Building</b></td>
				<td><b>Floor</td>
				<td><b>Room#</td>
			</tr>
			<tr bgcolor="black">
				<td height="1" colspan="6"></td>
			</tr>
			<tr bgcolor="#CCCCCC">
				<td height="1" colspan="6"></td>
			</tr>

	<?php
	
	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM rooms WHERE companyid = '$companyid' AND businessid = '$businessid' ORDER BY roomname DESC";
	$result = Treat_DB_ProxyOld::query($query);
	$num=Treat_DB_ProxyOld::mysql_numrows($result);
	//mysql_close();

	$num--;
	while ($num>=0){
		$roomid=@Treat_DB_ProxyOld::mysql_result($result,$num,"roomid");
		$roomname=@Treat_DB_ProxyOld::mysql_result($result,$num,"roomname");
		$buildingnum=@Treat_DB_ProxyOld::mysql_result($result,$num,"buildingnum");
		$floornum=@Treat_DB_ProxyOld::mysql_result($result,$num,"floornum");
		$roomnum=@Treat_DB_ProxyOld::mysql_result($result,$num,"roomnum");
		$deleted=@Treat_DB_ProxyOld::mysql_result($result,$num,"deleted");

		$showedit="<a href=buscaterroom.php?cid=$companyid&bid=$businessid&view2=edit&rid=$roomid#item><img border=0 src=edit.gif height=16 width=16 alt='EDIT ROOM'></a>";
		if ($deleted==0){$showdelete="<a href=editcaterroom.php?cid=$companyid&bid=$businessid&edit=2&editid=$roomid><img border=0 src=on.gif height=16 width=16 alt='ACTIVE'></a>";}
		else {$showdelete="<a href=editcaterroom.php?cid=$companyid&bid=$businessid&edit=2&editid=$roomid&on=on><img border=0 src=off.gif height=16 width=16 alt='INACTIVE'></a>";}

		?>
			
		<tr bgcolor="white" onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor='white'>
			<td width="25%"><?php echo $roomname; ?></td>
			<td width="4%"><?php echo $showdelete; ?></td>
			<td width="4%"><?php echo $showedit; ?></td>
			<td><?php echo $buildingnum; ?></td>
			<td><?php echo $floornum; ?></td>
			<td><?php echo $roomnum; ?></td>
		</tr>
		<tr bgcolor="#CCCCCC">
			<td height="1" colspan="6"></td>
		</tr>
		
		<?php

		$num--;
	}

	?>
		<tr bgcolor="black">
			<td height="1" colspan="6"></td>
		</tr>
		</table>
	</center>
	<?php

	if ($view2=="edit"){
		echo "<a name='item'>";
		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query2 = "SELECT * FROM rooms WHERE roomid = '$roomeditid'";
		$result2 = Treat_DB_ProxyOld::query($query2);
		//mysql_close();

		$editname=@Treat_DB_ProxyOld::mysql_result($result2,0,"roomname");
		$editbuilding=@Treat_DB_ProxyOld::mysql_result($result2,0,"buildingnum");
		$editfloor=@Treat_DB_ProxyOld::mysql_result($result2,0,"floornum");
		$editroom=@Treat_DB_ProxyOld::mysql_result($result2,0,"roomnum");

		?>
		<center>
		<table width="90%" bgcolor="#E8E7E7">
			<tr valign="top">
				<td colspan="2"><a name="item"></a><form action="editcaterroom.php" method="post"></td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="hidden" name="companyid" value="<?php echo $companyid; ?>">
					<input type="hidden" name="businessid" value="<?php echo $businessid; ?>">
					<input type="hidden" name="editid" value="<?php echo $roomeditid; ?>">
					<input type="hidden" name="edit" value="1"><b><font color="red">*</font><u>Edit Location</u></b>
				</td>
			</tr>
			<tr>
				<td align="right" width="10%">Room Name:</td>
				<td><input type="text" name="roomname" value='<?php echo $editname; ?>' size="20"></td>
			</tr>
			<tr>
				<td align="right">Building:</td>
				<td><input type="text" name="buildingnum" value="<?php echo $editbuilding; ?>" size="10"></td>
			</tr>
			<tr>
				<td align="right">Floor:</td>
				<td><input type="text" name="floornum" value="<?php echo $editfloor; ?>" size="10"></td>
			</tr>
			<tr>
				<td align="right">Room#:</td>
				<td><input type="text" name="roomnum" value="<?php echo $editroom; ?>" size="10"></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" value="Save"></form></td>
			</tr>
		</table>
		</center>
		<?php
	} else {
		?>
		<a name="pref"></a>
		<center>
		<table width="90%" bgcolor="#E8E7E7">
		<tr valign="top">
		<td>
		<table>
		<tr>
			<td colspan="2"><a name="item"></a><form action="editcaterroom.php" method="post"></td>
		</tr>
		<tr>
			<td colspan="2">
				<input type="hidden" name="companyid" value="<?php echo $companyid; ?>">
				<input type="hidden" name="businessid" value="<?php echo $businessid; ?>"><b><u>Add Location</u></b>
			</td>
		</tr>
		<tr>
			<td align="right" width="30%">Room Name:</td>
			<td>
				<input type="text" name="roomname" value="<?php echo $editname; ?>" size="20">
			</td>
		</tr>
		<tr>
			<td align="right">Building:</td>
			<td>
				<input type="text" name="buildingnum" value="<?php echo $editbuilding; ?>" size="10">
			</td>
		</tr>
		<tr>
			<td align="right">Floor:</td>
			<td>
				<input type="text" name="floornum" value="<?php echo $editfloor; ?>" size="10">
			</td>
		</tr>
		<tr>
			<td align="right">Room#:</td>
			<td>
				<input type="text" name="roomnum" value="<?php echo $editroom; ?>" size="10">
			</td>
		</tr>
		<tr>
			<td></td>
			<td>
				<input type="submit" value="Add">
				</form>
			</td>
		</tr>
		<!-- ADDING ADDITIONAL FORM TO ENTER THE CATERING POLICY -->
		<center>
		<table width="90%" bgcolor="#E8E7E7">
		<tr valign="top">
		<td>
		<table>
		<tr>
			<td colspan="2">
				<a name="item"></a>
				<form action="addcaterpolicy.php" method="post">
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<input type="hidden" name="businessid" value="<?php echo $businessid; ?>">
				<input type="hidden" name="companyid" value="<?php echo $cid; ?>"><b><u>Add Catering Policy</u></b>
			</td>
		</tr>
		<tr>
			<td>
				<textarea name="policy" rows="5" cols="35"><?php echo $caterpolicytext; ?></textarea>
			</td>
		</tr>
		<tr>
			<td>
				<input type="submit" value="Add" Policy'>
				</form>
			</td>
		</tr>
		</table>
		</td>
		<!-- END OF ADDITIONAL FORM FOR CATERING POLICY -->
		</table>
		</td>
		<td valign="top">
			<table>
				<tr>
					<td colspan="2">
						<form action="savecaterpref.php" method="post">
							<input type="hidden" name="companyid" value="<?php echo $companyid; ?>">
							<input type="hidden" name="businessid" value="<?php echo $businessid; ?>">
					</td>
				</tr>
		<tr>
			<td colspan="2"><b><u>Preferences</u><b></td>
		</tr>
		<tr>
			<td align="right">Max Orders per Day:</td>
			<td><input type="text" name="caternum" value="<?php echo $caternum; ?>" size="3"></td>
		</tr>
		<tr>
			<td align="right">Max days prior to Submit/Change:</td>
			<td>
				<input type="text" name="caterdays" value="<?php echo $caterdays; ?>" size="3">
			</td>
		</tr>
		<tr>
			<td align="right">Minutes btw Scheduled Rooms:</td>
			<td>
				<select name="cater_spread">
					<option value="0" <?php echo $catspr1; ?>>0</option>
					<option value="15" <?php echo $catspr15; ?>>15</option>
					<option value="30" <?php echo $catspr30; ?>>30</option>
					<option value="45" <?php echo $catspr45; ?>>45</option>
					<option value="60" <?php echo $catspr60; ?>>60</option>
				</select>
			</td>
		</tr>
		<tr>
			<td align="right">Minimum Check Value:</td>
			<td>$<input type="text" name="cater_min" value="<?php echo $cater_min; ?>" size="3"></td>
		</tr>
		<?php

		if ($srvchrg==1){
			$check="CHECKED";
		} else {
			$check="";
		}

		if ($weekend==1){
			$check2="CHECKED";
		} else {
			$check2="";
		}

		if ($unit_add==1){
			$check3="CHECKED";
		} else {
			$check3="";
		}

		?>
		<tr>
			<td align="right">Service Charge Displayed:</td>
			<td><input type="checkbox" name="srvchrg" value="1" <?php echo $check; ?>> <input type="text" name="srvchrgpcnt" value='<?php echo $srvchrgpcnt; ?>' size="3"><b>%</b></td>
		</tr>
		<tr>
			<td align="right">Service Charge Display Name:</td>
			<td><input type="text" name="srvchrg_name" value="<?php echo $srvchrg_name; ?>" size="25"> <font size="2">(Applies only to printed or emailed invoices)</td>
		</tr>
		<tr>
			<td align="right">Require Cost Center:</td>
			<td><input type="checkbox" name="cater_costcenter" value="1" <?php echo $check_costcenter; ?>> </td>
		</tr>
		<tr>
			<td align="right">Weekends allowed:</td>
			<td><input type="checkbox" name="weekend" value="1" <?php echo $check2; ?>></td>
		</tr>
		<!--tr><td align="right">Use Unit Address for Payments:</td><td><input type=checkbox name=unit_add value=1 $check3> (Instead of Corporate Address)</td></tr-->
		<tr>
			<td align="right" valign="top">Invoice Message*:</td>
			<td><textarea name="inv_note" cols="30" rows="3"><?php echo $inv_note; ?></textarea><br>*<i>This will appear on ALL invoices (in the notes section)<br> that are printed or viewed.</i></td>
		</tr>
		<tr>
			<td align="right"><a href='calender.php?bid=<?php echo "$businessid"; ?>&cid=<?php echo $companyid; ?>' style=<?php echo $style; ?> ><font color="blue">Click here to close specific dates</font></a></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td><input type="submit" value="Save"></form></td>
		</tr>
	</table>

		</td>
	</tr>
	</table>
	</center>
		<?php
	}
	//////END TABLE////////////////////////////////////////////////////////////////////////////////////

	?>
	<br>
	<FORM ACTION=businesstrack.php method="post">
	<input type="hidden" name="username" value="<?php echo $user; ?>">
	<input type="hidden" name="password" value="<?php echo $pass; ?>">
	<center><input type="submit" value="Return to Main Page"></center>
	</FORM>
	</body>
	<?php

	google_page_track();
}
//mysql_close();
?>