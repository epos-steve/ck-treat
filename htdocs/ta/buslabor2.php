<?php

function money($diff){
	$findme='.';
	$double='.00';
	$single='0';
	$double2='00';
	$pos1 = strpos($diff, $findme);
	$pos2 = strlen($diff);
	if ($pos1==""){$diff="$diff$double";}
	elseif ($pos2-$pos1==2){$diff="$diff$single";}
	elseif ($pos2-$pos1==1){$diff="$diff$double2";}
	else{}
	$dot = strpos($diff, $findme);
	$diff = substr($diff, 0, $dot+4);
	$diff=round($diff, 2);
	if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
	elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
	$diff = substr($diff, 0, $dot+3);
	$pos1 = strpos($diff, $findme);
	$pos2 = strlen($diff);
	if ($pos1==""){$diff="$diff$double";}
	elseif ($pos2-$pos1==2){$diff="$diff$single";}
	elseif ($pos2-$pos1==1){$diff="$diff$double2";}
	else{}
	return $diff;
}

function nextday($date2)
{
	$day=substr($date2,8,2);
	$month=substr($date2,5,2);
	$year=substr($date2,0,4);
	$leap = date("L");

	if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
	{
		if ($month == "01"){$month="02";}
		elseif ($month == "03"){$month="04";}
		elseif ($month == "05"){$month="06";}
		elseif ($month == "07"){$month="08";}
		elseif ($month == "08"){$month="09";}
		elseif ($month == "10"){$month="11";}
		$day='01';
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($month=='02' && $day == '29' && $leap == '1')
	{
		$month='03';
		$day='01';
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($month=='02' && $day == '28' && $leap == '0')
	{
		$month='03';
		$day='01';
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
	{
		if ($month == "04"){$month="05";}
		if ($month == "06"){$month="07";}
		if ($month == "09"){$month="10";}
		if ($month == "11"){$month="12";}
		$day='01';
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($month=='12' && $day=='31')
	{
		$day='01';
		$month='01';
		$year++;
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else
	{
		$day=$day+1;
		if ($day<10){$day="0$day";}
		$tomorrow="$year-$month-$day";
		return $tomorrow;
	}
}

function prevday($date2) {
	$day=substr($date2,8,2);
	$month=substr($date2,5,2);
	$year=substr($date2,0,4);
	$day=$day-1;
	$leap = date("L");

	if ($day <= 0)
	{
		if ($month == 01)
		{
			$month = '12';
			$year--;
			$day=$day+31;
			$yesterday = "$year-$month-$day";
			return $yesterday;
		}
		else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
		{
			$month--;
			if ($month < 10)
			{
				$month="0$month";
			}
			$day=$day+31;
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
		else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
		{
			$month--;
			if ($month < 10)
			{
				$month="0$month";
			}
			$day=$day+30;
			$yesterday="$year-$month-$day";
			return $yesterday;
		}

		elseif ($leap==1&&$month=='03')
		{
			$day=$day+29;
			$month='02';
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
		else
		{
			$day=$day+28;
			$month='02';
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
	}
	else
	{
		if ($day < 10)
		{
			$day="0$day";
		}
		$yesterday="$year-$month-$day";
		return $yesterday;
	}
}

function futureday($num) {
	$day = date("d");
	$year = date("Y");
	$month = date("m");
	$leap = date("L");
	$day=$day+$num;

	if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
	{
		if ($month == "03"){$month="04";}
		elseif ($month == "05"){$month="06";}
		elseif ($month == "07"){$month="08";}
		elseif ($month == "08"){$month="09";}
		$day=$day-31;
		if ($day<10){$day="0$day";}
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($month=='02' && $leap == '1' && $day >= '29')
	{
		$month='03';
		$day=$day-29;
		if ($day<10){$day="0$day";}
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($month=='02' && $leap == '0' && $day >= '28')
	{
		$month='03';
		$day=$day-28;
		if ($day<10){$day="0$day";}
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
	{
		if ($month == "04"){$month="05";}
		if ($month == "06"){$month="07";}
		if ($month == "09"){$month="10";}
		if ($month == "11"){$month="12";}
		$day=$day-30;
		if ($day<10){$day="0$day";}
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($month==12 && $day>=32)
	{
		$day=$day-31;
		if ($day<10){$day="0$day";}
		$month='01';
		$year++;
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else
	{
		if ($day<10){$day="0$day";}
		$tomorrow="$year-$month-$day";
		return $tomorrow;
	}
}

function pastday($num) {
	$day = date("d");
	$day=$day-$num;
	if ($day <= 0)
	{
		$year = date("Y");
		$month = date("m");
		if ($month == 01)
		{
			$month = '12';
			$year--;
			$day=$day+31;
			$yesterday = "$year-$month-$day";
			return $yesterday;
		}
		else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
		{
			$month--;
			if ($month < 10)
			{
				$month="0$month";
			}
			$day=$day+31;
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
		else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
		{
			$month--;
			if ($month < 10)
			{
				$month="0$month";
			}
			$day=$day+30;
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
		else
		{
			$day=$day+28;
			$month='02';
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
	}
	else
	{
		if ($day < 10)
		{
			$day="0$day";
		}
		$year=date("Y");
		$month=date("m");
		$yesterday="$year-$month-$day";
		return $yesterday;
	}
}

function dayofweek($date1)
{
	$day=substr($date1,8,2);
	$month=substr($date1,5,2);
	$year=substr($date1,0,4);
	$dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
	return $dayofweek;
}


if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
require_once( DOC_ROOT . '/bootstrap.php' );

$date2 = null; # Notice: Undefined variable
$is_disabled = null; # Notice: Undefined variable
$save_disable = null; # Notice: Undefined variable
$showpend = null; # Notice: Undefined variable
$temp_date = null; # Notice: Undefined variable
$audit = null; # Notice: Undefined variable

$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today = "$year-$month-$day";

/*$user = Treat_Controller_Abstract::getSessionCookieVariable( 'usercook' );
$pass = Treat_Controller_Abstract::getSessionCookieVariable( 'passcook' );
$businessid = Treat_Controller_Abstract::getPostOrGetVariable( 'bid' );
$companyid = Treat_Controller_Abstract::getPostOrGetVariable( 'cid' );
$newdate = Treat_Controller_Abstract::getPostOrGetVariable( 'newdate' );
$date1 = Treat_Controller_Abstract::getPostOrGetVariable( 'date1' );*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook');
$businessid = \EE\Controller\Base::getPostOrGetVariable('bid');
$companyid = \EE\Controller\Base::getPostOrGetVariable('cid');
$newdate = \EE\Controller\Base::getPostOrGetVariable('newdate');
$date1 = \EE\Controller\Base::getPostOrGetVariable('date1');

if ( $businessid == "" && $companyid == "" ) {
	/*$businessid = Treat_Controller_Abstract::getPostOrGetVariable( 'businessid' );
	$companyid = Treat_Controller_Abstract::getPostOrGetVariable( 'companyid' );
	$date1 = Treat_Controller_Abstract::getPostOrGetVariable( 'date1' );*/
	
	$businessid = \EE\Controller\Base::getPostOrGetVariable('businessid');
	$companyid = \EE\Controller\Base::getPostOrGetVariable('companyid');
	$date1 = \EE\Controller\Base::getPostOrGetVariable('date1');
}

if ( $date1 == "" ) {
	$date1 = $newdate;
}

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query( $query );
$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );
//mysql_close();

$loginid = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'loginid' );
$security_level = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'security_level' );
$mysecurity = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'security' );
$bid = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'businessid' );
$bid2 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid2' );
$bid3 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid3' );
$bid4 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid4' );
$bid5 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid5' );
$bid6 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid6' );
$bid7 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid7' );
$bid8 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid8' );
$bid9 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid9' );
$bid10 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid10' );
$cid = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'companyid' );

$pr1 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'payroll' );
$pr2 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr2' );
$pr3 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr3' );
$pr4 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr4' );
$pr5 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr5' );
$pr6 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr6' );
$pr7 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr7' );
$pr8 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr8' );
$pr9 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr9' );
$pr10 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr10' );

if ( $num != 1 || ( $security_level == 1 && ( $bid != $businessid || $cid != $companyid ) ) || $user == "" || $pass == "" )
{
	echo "<center><h3>Failed</h3>Use your browser's back button to try again.</center>";
}

else
{

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM company WHERE companyid = '$companyid'";
	$result = Treat_DB_ProxyOld::query( $query );
	//mysql_close();

	$companyname = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'companyname' );
	$week_end = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'week_end' );
	$com_logo=@Treat_DB_ProxyOld::mysql_result($result,0,"logo");

	if($com_logo == ""){$com_logo = "talogo.gif";}

	if ( $date1 != "" ) {
		$today = $date1;
	}

	while ( dayofweek( $today ) != $week_end ) {
		$today = nextday( $today );
	}
	$date1=$today;

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM security WHERE securityid = '$mysecurity'";
	$result = Treat_DB_ProxyOld::query( $query );
	//mysql_close();

	$sec_bus_sales = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_sales' );
	$sec_bus_invoice = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_invoice' );
	$sec_bus_order = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_order' );
	$sec_bus_payable = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_payable' );
	$sec_bus_inventor1 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_inventor1' );
	$sec_bus_inventor2 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_inventor2' );
	$sec_bus_inventor3 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_inventor3' );
	$sec_bus_inventor4 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_inventor4' );
	$sec_bus_inventor5 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_inventor5' );
	$sec_bus_control = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_control' );
	$sec_bus_menu = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_menu' );
	$sec_bus_order_setup = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_order_setup' );
	$sec_bus_request = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_request' );
	$sec_route_collection = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'route_collection' );
	$sec_route_labor = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'route_labor' );
	$sec_route_detail = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'route_detail' );
	$sec_bus_labor = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_labor' );
	$sec_bus_timeclock = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_timeclock' );
	$sec_bus_budget = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_budget' );

	////SECURITY LEVELS
	if ( $sec_bus_timeclock != 0 && $sec_bus_labor == 0 ) {
		$location = "/ta/buslabor5.php?bid=$businessid&cid=$companyid";
		header( 'Location: ' . $location );
		exit();
	}
	elseif ( $sec_bus_labor < .5 ) {
		$location = "/ta/businesstrack.php";
		header( 'Location: ' . $location );
		exit();
	}
	/*
	elseif ( $sec_bus_labor <= 2 && $bid != $businessid ) {
		$location="buslabor.php?bid=$businessid&cid=$companyid";
		header('Location: ./' . $location);
	}
	else{}
	*/

?>

<html>
<head>
		<title>Treat America :: buslabor2.php </title>
<script language="JavaScript" type="text/javascript">
function changePage(newLoc)
{
	nextPage = newLoc.options[newLoc.selectedIndex].value

	if (nextPage != "")
	{
		document.location.href = nextPage
	}
}
</script>

<script language="JavaScript" type="text/javascript" src="/assets/js/calendarPopup.js"></script>

<script language="JavaScript" type="text/javascript">document.write(getCalendarStyles());</script>

<style type="text/css">
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation
			{
			background-color:#6677DD;
			text-align:center;
			vertical-align:center;
			text-decoration:none;
			color:#FFFFFF;
			font-weight:bold;
			}
	.TESTcpDayColumnHeader,
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation,
	.TESTcpCurrentMonthDate,
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDate,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDate,
	.TESTcpCurrentDateDisabled,
	.TESTcpTodayText,
	.TESTcpTodayTextDisabled,
	.TESTcpText
			{
			font-family:arial;
			font-size:8pt;
			}
	TD.TESTcpDayColumnHeader
			{
			text-align:right;
			border:solid thin #6677DD;
			border-width:0 0 1 0;
			}
	.TESTcpCurrentMonthDate,
	.TESTcpOtherMonthDate,
	.TESTcpCurrentDate
			{
			text-align:right;
			text-decoration:none;
			}
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDateDisabled
			{
			color:#D0D0D0;
			text-align:right;
			text-decoration:line-through;
			}
	.TESTcpCurrentMonthDate
			{
			color:#6677DD;
			font-weight:bold;
			}
	.TESTcpCurrentDate
			{
			color: #FFFFFF;
			font-weight:bold;
			}
	.TESTcpOtherMonthDate
			{
			color:#808080;
			}
	TD.TESTcpCurrentDate
			{
			color:#FFFFFF;
			background-color: #6677DD;
			border-width:1;
			border:solid thin #000000;
			}
	TD.TESTcpCurrentDateDisabled
			{
			border-width:1;
			border:solid thin #FFAAAA;
			}
	TD.TESTcpTodayText,
	TD.TESTcpTodayTextDisabled
			{
			border:solid thin #6677DD;
			border-width:1 0 0 0;
			}
	A.TESTcpTodayText,
	SPAN.TESTcpTodayTextDisabled
			{
			height:20px;
			}
	A.TESTcpTodayText
			{
			color:#6677DD;
			font-weight:bold;
			}
	SPAN.TESTcpTodayTextDisabled
			{
			color:#D0D0D0;
			}
	.TESTcpBorder
			{
			border:solid thin #6677DD;
			}
</style>

<script language="javascript" type="text/javascript"><!--
function submit2(){return confirm('ARE YOU SURE YOU WANT TO SAVE/SUBMIT END OF MONTH INVENTORY?');}
// --></script>

<script language="JavaScript" type="text/javascript">
<!-- Web Site:  http://dynamicdrive.com -->

<!-- This script and many more are available free online at -->
<!-- The JavaScript Source!! http://javascript.internet.com -->
<!-- Begin
function disableForm(theform) {
if (document.all || document.getElementById) {
for (i = 0; i < theform.length; i++) {
var tempobj = theform.elements[i];
if (tempobj.type.toLowerCase() == "submit" || tempobj.type.toLowerCase() == "reset")
tempobj.disabled = true;
}

}
else {
alert("The form has been submitted.  But, since you're not using IE 4+ or NS 6, the submit button was not disabled on form submission.");
return false;
	}
}
//  End -->
</script>

<script type="text/javascript" src="javascripts/prototype.js"></script>

<script language="JavaScript" type="text/javascript">
window.onload = function(){
	new ArrowGrid('test');
	new headerScroll('test',{head: 'header'});
}
</script>

</head>

<?php
	//if ($date2==$date1){for($counter=1;$counter<7;$counter++){$date2=nextday($date2);}}

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM business WHERE companyid = '$companyid' AND businessid = '$businessid'";
	$result = Treat_DB_ProxyOld::query( $query );
	//mysql_close();

	$businessname = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'businessname' );
	$businessid = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'businessid' );
	$street = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'street' );
	$city = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'city' );
	$state = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'state' );
	$zip = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'zip' );
	$phone = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'phone' );
	$districtid = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'districtid' );
	$operate = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'operate' );
	$bus_unit = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'unit' );

	echo "<body>";
	echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%><tr><td colspan=2>";
	echo "<a href=businesstrack.php><img src=logo.jpg border=0 height=43 width=205></a><p></td></tr>";

	//echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";
	echo "<tr bgcolor=#CCCCFF><td width=1%><img src=logo/$com_logo></td><td><font size=4><b>$businessname #$bus_unit</b></font><br>$street<br>$city, $state $zip<br>$phone</td>";
	echo "<td align=right valign=top><br>";
	echo "<FORM ACTION='editbus.php' method=post name='store'>";
	echo "<input type=hidden name=username value=$user>";
	echo "<input type=hidden name=password value=$pass>";
	echo "<input type=hidden name=businessid value=$businessid>";
	echo "<input type=hidden name=companyid value=$companyid>";
	echo "<INPUT TYPE=submit VALUE=' Store Setup '> ";
	echo "</form></td></tr>";
	//echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";
	
	echo "<tr><td colspan=3><font color=#999999>";
	if ($sec_bus_sales>0){echo "<a href=busdetail.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Sales</font></a>";}
	if ($sec_bus_invoice>0){echo " | <a href=businvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Invoicing</font></a>";}
	if ($sec_bus_order>0){echo " | <a href=buscatertrack.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Orders</font></a>";}
	if ($sec_bus_payable>0){echo " | <a href=busapinvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Payables</font></a>";}
	if ($sec_bus_inventor1>0){echo " | <a href=businventor.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Inventory</font></a>";}
	if ($sec_bus_control>0){echo " | <a href=buscontrol.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Control Sheet</font></a>";}
	if ($sec_bus_menu>0){echo " | <a href=buscatermenu.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Menus</font></a>";}
	if ($sec_bus_order_setup>0){echo " | <a href=buscaterroom.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Order Setup</font></a>";}
	if ($sec_bus_request>0){echo " | <a href=busrequest.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Requests</font></a>";}
	if ($sec_route_collection>0||$sec_route_labor>0||$sec_route_detail>0){echo " | <a href=busroute_labor.php?bid=$businessid&cid=$companyid&date1=$newdate><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Route Collections</font></a>";}
	if ($sec_bus_labor>0){echo " | <a href=buslabor.php?bid=$businessid&cid=$companyid><font color=red onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='red' style='text-decoration:none'>Labor</font></a>";}
	if ($sec_bus_budget>0||($businessid == $bid && $pr1 == 1)||($businessid == $bid2 && $pr2 == 1)||($businessid == $bid3 && $pr3 == 1)||($businessid == $bid4 && $pr4 == 1)||($businessid == $bid5 && $pr5 == 1)||($businessid == $bid6 && $pr6 == 1)||($businessid == $bid7 && $pr7 == 1)||($businessid == $bid8 && $pr8 == 1)||($businessid == $bid9 && $pr9 == 1)||($businessid == $bid10 && $pr10 == 1)){echo " | <a href=busbudget.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Budget</font></a>";}
	echo "</td></tr>";

	echo "</table></center><p>";

	if ( $security_level > 0 ) {
		echo "<center><table width=90%><tr><td><form action=buslabor3.php method=post>";
		echo "<select name=gobus onChange='changePage(this.form.gobus)'>";

		$districtquery = "";
		if ( $security_level > 2 && $security_level < 7 ) {
			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query = "SELECT * FROM login_district WHERE loginid = '$loginid'";
			$result = Treat_DB_ProxyOld::query( $query );
			$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );
			//mysql_close();

			$num--;
			while ( $num >= 0 ) {
				$districtid = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'districtid' );
				if ( $districtquery == "" ) {
					$districtquery = "districtid = '$districtid'";
				}
				else {
					$districtquery = "$districtquery OR districtid = '$districtid'";
				}
				$num--;
			}
		}

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		if ( $security_level > 6 ) {
			$query = "SELECT * FROM business WHERE (companyid = '$companyid' AND exp_payroll = '0') OR (businessid = '$businessid') ORDER BY businessname DESC";
		}
		elseif ( $security_level == 2 || $security_level == 1 ) {
			$query = "SELECT * FROM business WHERE (companyid = '$companyid' AND (businessid = '$bid' OR businessid = '$bid2' OR businessid = '$bid3' OR businessid = '$bid4' OR businessid = '$bid5' OR businessid = '$bid6' OR businessid = '$bid7' OR businessid = '$bid8' OR businessid = '$bid9' OR businessid = '$bid10') AND exp_payroll = '0') OR (businessid = '$businessid') ORDER BY businessname DESC";
		}
		elseif ( $security_level > 2 && $security_level < 7 ) {
			$query = "SELECT * FROM business WHERE (companyid = '$companyid' AND ($districtquery) AND exp_payroll = '0') OR (businessid = '$businessid') ORDER BY businessname DESC";
		}
		$result = Treat_DB_ProxyOld::query( $query );
		$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );
		//mysql_close();

		$num--;
		while ( $num >= 0 ) {
			$businessname = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'businessname' );
			$busid = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'businessid' );

			if ( $busid == $businessid ) {
				$show = 'SELECTED';
			}
			else {
				$show = '';
			}

			echo "<option value=buslabor2.php?bid=$busid&cid=$companyid&date1=$date1&date2=$date2 $show>$businessname</option>";
			$num--;
		}

		echo "</select></td><td></form></td><td width=50% align=right><form action=buslabor2.php method=get><b>Week ending:</b><input type=hidden name=businessid value=$businessid><input type=hidden name=companyid value=$companyid><SCRIPT LANGUAGE='JavaScript' ID='js18'> var cal18 = new CalendarPopup('testdiv1'); cal18.setCssPrefix('TEST');</SCRIPT> <A HREF=# onClick=cal18.select(document.forms[2].date1,'anchor18','yyyy-MM-dd'); return false; TITLE=cal18.select(document.forms[2].date1,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor18' ID='anchor18'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A> <input type=text name=date1 value='$date1' size=8> <input type=submit value='GO'></td><td></form></td></tr></table></center><p>";
	}
	$security_level = $sec_bus_labor;

	echo "<center><table width=90% cellspacing=0 cellpadding=0>";

	echo "<tr bgcolor=#CCCCCC valign=top><td width=1%><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=15%><center><a href=buslabor.php?cid=$companyid&bid=$businessid style=$style><font color=blue>Hours</a></center></td><td width=1% align=right><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1%><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=15%><center><a href=buslabor4.php?cid=$companyid&bid=$businessid&newdate=$date1 style=$style><font color=blue>Tips</a></center></td><td width=1% align=right><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1% bgcolor=#E8E7E7><img src=leftcrn.jpg height=6 width=6 align=top></td><td bgcolor=#E8E7E7 width=15%><center><a href=buslabor2.php?cid=$companyid&bid=$businessid&newdate=$date1 style=$style style=$style><font color=black>Commissions</a></center></td><td bgcolor=#E8E7E7 width=1% align=right><img src=rightcrn.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1%><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=15%><center><a href=buslabor3.php?cid=$companyid&bid=$businessid&newdate=$date1 style=$style><font color=blue>Summary</a></center></td><td width=1% align=right><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1%><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=15%><center><a href=buslabor5.php?cid=$companyid&bid=$businessid&newdate=$date1 style=$style style=$style><font color=blue>Time Clock</a></center></td><td width=1% align=right><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td bgcolor=white width=25%></td></tr>";

	echo "<tr height=2><td colspan=4></td><td colspan=4></td><td colspan=3 bgcolor=#E8E7E7></td><td></td><td colspan=4></td><td colspan=1></td></tr>";

	echo "</table></center><center><table width=90% cellspacing=0 cellpadding=0 id='test'>";

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query36 = "SELECT * FROM submit_labor WHERE businessid = '$businessid' AND date = '$today'";
	$result36 = Treat_DB_ProxyOld::query( $query36 );
	$num36 = Treat_DB_ProxyOld::mysql_numrows( $result36, 0 );
	//mysql_close();

	if ( ( $num36 > 0 && $security_level < 9 ) || $security_level < 1 ) {
		$is_disabled = 'DISABLED';
	}

	echo "<tr><td colspan=10>";
	echo "<a name=labor></a><form action=save_comm.php method=post onSubmit='return disableForm(this);'><input type=hidden name=companyid value=$companyid><input type=hidden name=businessid value=$businessid><input type=hidden name=date value=$today><center><table width=100% border=0 bgcolor=#E8E7E7>";

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query34 = "SELECT * FROM payroll_code_comm WHERE companyid = '$companyid' ORDER BY orderid DESC";
	$result34 = Treat_DB_ProxyOld::query( $query34 );
	$num34 = Treat_DB_ProxyOld::mysql_numrows( $result34, 0 );
	//mysql_close();

	///////////////HEADER BEGIN DISPLAY
	$showspan = $num34 + 4;
	echo "<tr bgcolor=#CCCC99 id='header'><td width=10%><b><font size=2>Empl#</td><td width=15%><b><font size=2>Employee</td><td width=5% align=right><b><font size=2>Rate</td>";

	$temp_num = $num34 - 1;
	$temp_width = 65 / ( $temp_num + 1 );
	while ( $temp_num >= 0 ) {
		$codeid = @Treat_DB_ProxyOld::mysql_result( $result34, $temp_num, 'codeid' );
		$payroll_code = @Treat_DB_ProxyOld::mysql_result( $result34, $temp_num, 'code_name' );

		echo "<td align=right width=$temp_width%><b><font size=2>$payroll_code</font></td>";

		$temp_num--;
	}
	echo "<td width=5%></td></tr>";
	/////////////END HEADER

	$totalpayroll=0;

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM jobtype WHERE companyid = '$companyid' AND commission = '1' ORDER BY name DESC";
	$result = Treat_DB_ProxyOld::query( $query );
	$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );
	//mysql_close();

	$highlight=0;

	$num--;
	while ( $num >= 0 ) {
		$jobtypeid = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'jobtypeid' );
		$jobtypename = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'name' );
		$hourly = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'hourly' );

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query2 = "
			SELECT
				ltc.*
				,sd.*
				,pd.*
				,l.firstname
				,l.lastname
				,l.empl_no
			FROM labor_temp_comm ltc
				JOIN payroll_departmentdetail pd
					ON ltc.loginid = pd.loginid
				JOIN security_departments sd
					ON pd.dept_id = sd.dept_id
					AND sd.securityid = '$mysecurity'
				LEFT JOIN login l
					ON ltc.loginid = l.loginid
			WHERE ltc.jobtypeid = '$jobtypeid'
				AND ltc.businessid = '$businessid'
				AND ltc.date = '$today'
			ORDER BY
				l.lastname DESC
				,l.firstname DESC
				,l.empl_no DESC
		";
		$result2 = Treat_DB_ProxyOld::query( $query2 );
		$num2 = Treat_DB_ProxyOld::mysql_numrows( $result2, 0 );
		//mysql_close();

		$num2--;
		while ( $num2 >= 0 ) {
			$tempid = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'ltc.tempid' );
			$loginid = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'ltc.loginid' );
			$rate = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'ltc.rate' );

			$rate = money( $rate );

			$firstname = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'firstname' );
			$lastname = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'lastname' );
			$empl_no = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'empl_no' );

			if ( $hourly == 1 ) {
				$showrate = "$$rate/hr";
			}
			else {
				$showrate = "$rate/Base";
			}

			echo "<tr id='row$highlight'><td><font size=2>$empl_no</td><td><font size=2>$lastname,$firstname</td><td align=right><font size=2>$showrate</td>";

			$rowtotal = 0;
			$temp_num = $num34 - 1;
			while ( $temp_num >= 0 ) {
				$codeid = @Treat_DB_ProxyOld::mysql_result( $result34, $temp_num, 'codeid' );
				$nocharge = @Treat_DB_ProxyOld::mysql_result( $result34, $temp_num, 'nocharge' );
				$noedit = @Treat_DB_ProxyOld::mysql_result( $result34, $temp_num, 'noedit' );

				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( "Unable to select database");
				$query3 = "SELECT * FROM labor_comm WHERE temp_commid = '$tempid' AND coded = '$codeid'";
				$result3 = Treat_DB_ProxyOld::query( $query3 );
				//mysql_close();

				$value = @Treat_DB_ProxyOld::mysql_result( $result3, 0, 'amount' );

				$myname = "$tempid:$codeid";

				if ( ( $noedit == 1 && $security_level < 9 ) || $security_level < 1 ) {
					echo "<td align=right><input type=text size=5 name=name value='$value' STYLE='font-size: 10px;' DISABLED><input type=hidden name='$myname' value='$value'></td>";
				}
				else {
					echo "<td align=right><input type=text size=5 name='$myname' value='$value' STYLE='font-size: 10px;' onfocus=\"row$highlight.style.backgroundColor='yellow'\" onblur=\"row$highlight.style.backgroundColor=''\"></td>";
				}

				$temp_num--;
				if ( $nocharge != 1 ) {
					$rowtotal += $value;
				}
			}
			$totalpayroll += $rowtotal;
			$rowtotal = number_format( $rowtotal, 2 );
			echo "<td align=right><font size=2><b>$$rowtotal</td></tr>";
			$highlight++;

			$num2--;
		}

		echo "<tr bgcolor=#CCCCCC><td colspan=$showspan><b><font size=2>$jobtypename</font></td></tr>";
		$num--;
	}
	echo "<tr><td colspan=2><input type=submit value='Save' $is_disabled></td><td></form></td><td colspan=$num34 align=right><form action=comm_records.php method=post onSubmit='return disableForm(this);'><input type=hidden name=businessid value=$businessid><input type=hidden name=companyid value=$companyid><input type=hidden name=date value='$today'><input type=submit value='Add Records' $is_disabled></td></tr>";
	echo "<tr bgcolor=black height=1><td colspan=$showspan></form></td></tr>";
	
	///////////////////END

	/////////// total payroll
	$query2 = "SELECT * FROM stats WHERE businessid = '$businessid' AND date = '$today'";
	$result2 = Treat_DB_ProxyOld::query( $query2 );
	$num2 = Treat_DB_ProxyOld::mysql_numrows( $result2, 0 );

	if ( $num2 > 0 ) {
		$query2 = "UPDATE stats SET labor_vend = '$totalpayroll' WHERE businessid = '$businessid' AND date = '$today'";
		$result2 = Treat_DB_ProxyOld::query( $query2 );
	}
	else {
		$query2 = "INSERT INTO stats (businessid,companyid,date,labor_vend) VALUES ('$businessid','$companyid','$today','$totalpayroll')";
		$result2 = Treat_DB_ProxyOld::query( $query2 );
	}
	//////////END UPDATE PAYROLL
	
	echo "</table>";
	echo "</td></tr></table></center>";

	/////////////ADD TEMP EMPLOYEE
	echo "<p><center><table width=90% border=0><tr><td>";
	echo "<table bgcolor=#E8E7E7 width=75%>";

	echo "<tr><td><form action=add_emp_labor2.php method=post onSubmit='return disableForm(this);'><input type=hidden name=companyid value=$companyid><input type=hidden name=businessid value=$businessid><input type=hidden name=date value=$today><select name=loginid>";

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM login,jobtype,jobtypedetail WHERE login.companyid = '$companyid' AND login.security_level < '3' AND login.is_deleted = '0' AND (jobtypedetail.loginid = login.loginid AND jobtypedetail.jobtype = jobtype.jobtypeid AND jobtype.hourly = '1' AND jobtype.commission = '0') ORDER BY lastname DESC,firstname DESC";
	$result = Treat_DB_ProxyOld::query( $query );
	$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );
	//mysql_close();

	$num--;
	while ( $num >= 0 ) {
		$loginid = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'loginid' );
		$lastname = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'lastname' );
		$firstname = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'firstname' );
		$empl_no = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'empl_no' );
		$oo2 = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'oo2' );
		$jobtype = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'jobtype' );

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query2 = "SELECT name FROM jobtype WHERE jobtypeid = '$jobtype'";
		$result2 = Treat_DB_ProxyOld::query( $query2 );
		//mysql_close();

		$job_name = @Treat_DB_ProxyOld::mysql_result( $result2, 0, 'name' );

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query2 = "SELECT * FROM labor_temp WHERE businessid = '$businessid' AND date = '$date1' AND loginid = '$loginid'";
		$result2 = Treat_DB_ProxyOld::query( $query2 );
		$num2 = Treat_DB_ProxyOld::mysql_numrows( $result2, 0 );
		//mysql_close();

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query2 = "SELECT * FROM jobtypedetail WHERE loginid = '$loginid'";
		$result2 = Treat_DB_ProxyOld::query( $query2 );
		$num3 = Treat_DB_ProxyOld::mysql_numrows( $result2, 0 );
		//mysql_close();

		if ( $num2 == 0 && $num3 > 0 ) {
			echo "<option value=$loginid>$lastname, $firstname ($empl_no) - $job_name</option>";
		}

		$num--;
	}
	echo "</select> <input type=submit value='Add Employee' $save_disable onclick='return submit3()'></td><td></form></td></tr>";
	echo "</table>";
	echo "</td></tr></table></center>";

	/////////////////////////NEW EMPLOYEES
	echo "<center><table width=90%><tr><td>";

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query3 = "SELECT * FROM login_pend WHERE businessid = '$businessid'";
	$result3 = Treat_DB_ProxyOld::query( $query3 );
	$num3 = Treat_DB_ProxyOld::mysql_numrows( $result3, 0 );
	//mysql_close();

	if ( $num3 > 0 ) {
		$showpend = "<b> - <font color=blue>$num3 Request(s) Pending</font></b>";
	}

	echo "<table width=75% bgcolor=#E8E7E7>";

	echo "<tr><td colspan=2><b>Request New Employee</b> $showpend</td></tr>";
	echo "<tr><td><form action=labor_request.php method=post><input type=hidden name=newdate value=$date1><input type=hidden name=companyid value=$companyid><input type=hidden name=businessid value=$businessid><input type=text name=firstname value='firstname' size=20> <input type=text name=lastname value='lastname' size=20> <select name=jobtypeid>";

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query2 = "SELECT * FROM jobtype WHERE companyid = '$companyid' AND commission = '1' ORDER BY name DESC";
	$result2 = Treat_DB_ProxyOld::query( $query2 );
	$num2 = Treat_DB_ProxyOld::mysql_numrows( $result2, 0 );
	//mysql_close();

	$num2--;
	while ( $num2 >= 0 ) {
		$job_typename = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'name' );
		$jobtypeid = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'jobtypeid' );

		echo "<option value=$jobtypeid>$job_typename</option>";

		$num2--;
	}
	echo "</select> <b>$</b><input type=text size=5 name=rate> <input type=submit value='Request' onclick='return submit4()'></td><td></form></td></tr></table>";

	echo "</td></tr></table></center>";
	////////////////END NEW EMPLOYEES
	echo "<center><table width=90%><tr><td>";

	$temp_date=prevday($temp_date);
	echo "<table width=50% bgcolor=#E8E7E7>";

	if ( $audit == 1 ) {
		$showchange = "<a href=buslabor2.php?cid=$companyid&bid=$businessid&newdate=$date1 style=$style><font color=blue>HIDE</font></a>";
	}
	else {
		$showchange = "<a href=buslabor2.php?cid=$companyid&bid=$businessid&newdate=$date1&audit=1#audit style=$style><font color=blue>SHOW</font></a>";
	}

	echo "<tr><td colspan=3><font size=1><b><a name=audit>AUDIT TRAIL</a> </b> $showchange</td></tr>";
	if ( $audit == 1 ) {
		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query2 = "SELECT * FROM audit_comm WHERE businessid = '$businessid' AND date = '$date1' ORDER BY time";
		$result2 = Treat_DB_ProxyOld::query( $query2 );
		$num2 = Treat_DB_ProxyOld::mysql_numrows( $result2, 0 );
		//mysql_close();

		$num2--;
		while ( $num2 >= 0 ) {
			$time = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'time' );
			$comment = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'comment' );
			$who = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'user' );

			echo "<tr bgcolor=white><td><font size=1>$comment</td><td><font size=1>$who</td><td><font size=1>$time</td></tr>";

			$num2--;
		}
	}
	echo "</table>";

	echo "</td></tr></table></center>";

	//mysql_close();

	echo "<br><FORM ACTION=businesstrack.php method=post>";
	echo "<input type=hidden name=username value=$user>";
	echo "<input type=hidden name=password value=$pass>";
	echo "<center><INPUT TYPE=submit VALUE=' Return to Main Page'></FORM></center><DIV ID=testdiv1 STYLE=position:absolute;visibility:hidden;background-color:white;layer-background-color:white;></DIV></body>";
	
	google_page_track();
}
?>