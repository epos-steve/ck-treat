<?php

define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once(DOC_ROOT.'/bootstrap.php');

// ajax fix for ie
mb_internal_encoding("UTF-8");
mb_http_output("UTF-8");
ob_start("mb_output_handler");

if ( require_once( realpath( DOC_ROOT . '/../lib/inc/cookie-login.php' ) ) ) {
	$businessid = $_GET["bid"];
	$import_id = microtime( true );
	?>
	<div id="import_menu_dialog_content">
		<script language="javascript" type="text/javascript">
			function display_import_help(){
				jQuery('<div id="import_help">')
					.load('/ta/import_menu_help.html')
					.dialog({
						zIndex: 9999,
						width: 750,
						height: 480,
						title: 'Import Help'
					});
			}

			jQuery(document).ready(function(){
				jQuery('#import_form').ajaxForm({
					beforeSubmit: function(){
						jQuery('.ui-dialog').bind( 'dialogbeforeclose', function(){ return false; } );
						jQuery('#import_form_div').addClass('dialog-hidden');
						jQuery('#import_wait').removeClass('dialog-hidden');
						jQuery('#import_progressbar').progressbar({ value: 0 });
						window.progressIntervalId = window.setInterval( function(){
							jQuery.getJSON( '/ta/utility/import_menu_data.php?progressbar=<?=$import_id?>', function(data){
								jQuery('#import_progressbar').progressbar( 'option', 'value', data.progress );
								if( data.progress > 100 ) {
									jQuery('#import_wait').addClass('dialog-hidden');
									jQuery('#import_done').removeClass('dialog-hidden');
									jQuery('#import_error').html( data.error );
									window.clearInterval( window.progressIntervalId );
								}
							});
						}, 500 );
					},
					/*success: function(){
						jQuery('#import_wait').addClass('dialog-hidden');
						jQuery('#import_done').removeClass('dialog-hidden');
						window.clearInterval( window.progressIntervalId );
					},*/
					iframe: true,
					dataType: 'json',
					success: function(data){
						jQuery('#import_wait').addClass('dialog-hidden');
						jQuery('#import_done').removeClass('dialog-hidden');
						jQuery('#import_error').html( data.error );
						window.clearInterval( window.progressIntervalId );
					}
				});
			});
		</script>
		<div id="import_form_div">
			<form id="import_form" enctype="multipart/form-data" action="/ta/utility/import_menu_data.php" method="POST">
				<input type="hidden" name="import_id" value="<?=$import_id?>" />
				<input type="hidden" name="MAX_FILE_SIZE" value="10000000" />
				<input type="hidden" name="return_url" value="<?=$_GET['return_url']?>" />
				<input type="hidden" name="bid" value="<?=$_GET['bid']?>" />
				<p><label for="menu_data">Please select the file to import:</label></p>
				<p><input type="file" name="menu_data" id="menu_data" /></p>
				<p><input type="submit" value="Upload Data" /> <button onclick="display_import_help(); return false;">?</button></p>
			</form>
		</div>
		<div id="import_wait" class="dialog-hidden">
			<p>Please wait, menu import may take a few minutes...</p>
			<div id="import_progressbar"></div>
		</div>
		<div id="import_done" class="dialog-hidden">
			<div id="import_error"></div>
			<p><button onclick="location.reload(true);">Ok</button></p>
		</div>
	</div>
	<?
}
?>
