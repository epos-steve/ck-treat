<?php
define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
require_once( DOC_ROOT . '/bootstrap.php' );
ini_set( 'include_path', DOC_ROOT . '/lib' . PATH_SEPARATOR . ini_get( 'include_path' ) );

ini_set( 'display_errors', false );
ini_set( 'memory_limit','1000M' );
if ( function_exists ( 'memory_get_usage' ) )
{
	echo 'enable memory limit was set in the configuration line, you can set the memory_limit in your script!', PHP_EOL;
	echo ini_get( 'memory_limit' ), PHP_EOL;
	echo ini_get( 'display_errors' ), PHP_EOL;
}
else
{
	echo 'enable memory limit was not set in the configuration line, you can not set the memory_limit in your script!', PHP_EOL;
}

set_time_limit(3600);
error_reporting(E_ALL);
require_once( 'Zend/Exception.php' );
require_once( 'Zend/Soap/Client.php' );


$wsdl = 'https://est05.esalestrack.com/esalestrackapi/service1.asmx?wsdl';
$options = array(
	'UserIdentifier' => '5cd1b9e5-2845-4acd-aed1-72a94a1d18b6',
	'Module'         => 'Contracts',
);

$tmp_file_any = DIR_TMP_FILES . '/esalestrack.contracts.any.xml';

$xml_doc = '';
try {
	# if segment & file_put_contents is for testing only
	$filemtime = filemtime( $tmp_file_any );
	$too_old = strtotime( '-2 hours' );
	if ( file_exists( $tmp_file_any ) && $filemtime > $too_old ) {
		$xml_doc = file_get_contents( $tmp_file_any );
	}
	else {
		$client = new Zend_Soap_Client( $wsdl ); // Servers WSDL Location
		
		$result = $client->GetDataRecords( $options );
#		var_dump( $result );
		$xml_doc = $result->GetDataRecordsResult->any;
#		$schema_doc = $result->GetDataRecordsResult->schema;
		# for testing only
		file_put_contents( $tmp_file_any, $xml_doc );
	}
	
}
catch(Zend_Exception $e) {
	echo $e->getMessage();
        echo "failed";
	exit(__LINE__);
}
catch (SoapFault $e) {
	echo '$e->getMessage() = ', $e->getMessage(), PHP_EOL;
	echo '$client->getLastRequest() = ', PHP_EOL;
	echo $client->getLastRequest();
	exit(__LINE__);
}


$items = array();
if ( $xml_doc ) {
	$doc = new DOMDocument();
	$doc->loadXML( $xml_doc );
	
	$xpath = new DOMXpath( $doc );
	$xpath->registerNamespace( 'diffgr', 'urn:schemas-microsoft-com:xml-diffgram-v1' );
	
	$elements = $xpath->query( '/diffgr:diffgram/NewDataSet/Table' );
	if ( !is_null( $elements ) ) {
		foreach ($elements as $element) {
			$table = array();
			$nodes = $element->childNodes;
			foreach ($nodes as $node) {
				$table[ $node->nodeName ] = $node->nodeValue;
			}
			$items[] = $table;
		}
	}
	var_dump( $items );
}
else {
	exit(__LINE__);
}

////////////////////////////////////////////////

//echo "Done";

if( $items ){
		
		
		//print_r($items);
		
		foreach($items AS $key => $value)
		{
                        /////clear variables
			$systemid="";
                        $vendrev="";
                        $fsrev="";
                        $ocsrev="";
                                        
                                foreach($value AS $key3 => $value3){
				
					echo "$key3: $value3<br>";
					
					$value3=str_replace("'","`",$value3);
					
					//////FILL VARIABLES
					if($key3=="accountx"){$systemid = $value3;}
                                        elseif($key3=="vendannualrevenuex"){$vendrev = $value3;}
                                        elseif($key3=="fsannualrevenuex"){$fsrev = $value3;}
                                        elseif($key3=="ocsannualrevenuex"){$ocsrev = $value3;}
						
                                }
				//////UPDATE DB
                                ///build query
                                $subquery="";
                                if($vendrev!=0){$subquery = "yrlyrevenue = yrlyrevenue + '$vendrev'";}
                                if($fsrev!=0 && $subquery != ""){$subquery.= ", cafeannualrevenue = cafeannualrevenue + '$fsrev'";}
                                elseif($fsrev!=0){$subquery = "cafeannualrevenue = cafeannualrevenue + '$fsrev'";}
                                if($ocsrev!=0 && $subquery != ""){$subquery.= ", ocsrevenue = ocsrevenue + '$ocsrev'";}
                                elseif($ocsrev!=0){$subquery = "ocsrevenue = ocsrevenue + '$ocsrev'";}

                                if($subquery!=""){
                                    $query = "UPDATE est_accounts SET $subquery WHERE systemid = '$systemid'";
                                    $result = DB_Proxy::query($query);

                                    echo "<font size=2>$query<br></font><br>";
                                }
                                else{echo "<font color=red>Do Nothing</font><br>";}
		}

		echo 'Done';
}







