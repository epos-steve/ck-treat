<?php
$findinv = "";
function money($diff){
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+4);
        $diff=round($diff, 2);
        if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
        elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
        $diff = substr($diff, 0, $dot+3);
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      if ($month == "10"){$month="11";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='32')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='08' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      $day=$day-31;
      if ($day<10){$day="0$day";}
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";}
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";}
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";}
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";}
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}


define('DOC_ROOT', dirname(dirname(__FILE__)));
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');

$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");
$creditamount="creditamount";
$creditidnty="creditidnty";
$creditdate="creditdate";
$debitamount="debitamount";
$debitidnty="debitidnty";
$debitdate="debitdate";
$quatertotal=0;
$monthtotal=0;
$total=0;
$counter=0;
$findme='.';
$double='.00';
$single='0';
$double2='00';

/*$user = isset($_COOKIE["usercook"])?$_COOKIE["usercook"]:'';
$pass = isset($_COOKIE["passcook"])?$_COOKIE["passcook"]:'';
$view = isset($_COOKIE["viewcook"])?$_COOKIE["viewcook"]:'';
$sort_ap = isset($_COOKIE["sort_ap"])?$_COOKIE["sort_ap"]:'';
$subtotal = isset($_COOKIE["subtotal"])?$_COOKIE["subtotal"]:'';
$date1 = isset($_COOKIE["date1cook"])?$_COOKIE["date1cook"]:'';
$date2 = isset($_COOKIE["date2cook"])?$_COOKIE["date2cook"]:'';
$showvendor = isset($_COOKIE["showvendor"])?$_COOKIE["showvendor"]:'';
$businessid = isset($_GET["bid"])?$_GET["bid"]:'';
$companyid = isset($_GET["cid"])?$_GET["cid"]:'';
$interco = isset($_GET["interco"])?$_GET["interco"]:'';*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook','');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook','');
$view = \EE\Controller\Base::getSessionCookieVariable('viewcook','');
$sort_ap = \EE\Controller\Base::getSessionCookieVariable('sort_ap','');
$subtotal = \EE\Controller\Base::getSessionCookieVariable('subtotal','');
$date1 = \EE\Controller\Base::getSessionCookieVariable('date1cook','');
$date2 = \EE\Controller\Base::getSessionCookieVariable('date2cook','');
$showvendor = \EE\Controller\Base::getSessionCookieVariable('showvendor','');
$businessid = \EE\Controller\Base::getGetVariable('bid','');
$companyid = \EE\Controller\Base::getGetVariable('cid','');
$interco = \EE\Controller\Base::getGetVariable('interco','');

if ($businessid==""&&$companyid==""){
   /*$businessid = isset($_POST["businessid"])?$_POST["businessid"]:'';
   $companyid = isset($_POST["companyid"])?$_POST["companyid"]:'';
   $view = isset($_POST["view"])?$_POST["view"]:'';
   $date1 = isset($_POST["date1"])?$_POST["date1"]:'';
   $date2 = isset($_POST["date2"])?$_POST["date2"]:'';
   $findinv = isset($_POST["findinv"])?$_POST["findinv"]:'';
   $sort_ap = isset($_POST["sort_ap"])?$_POST["sort_ap"]:'';
   $subtotal = isset($_POST["subtotal"])?$_POST["subtotal"]:'';
   $showvendor = isset($_POST["showvendor"])?$_POST["showvendor"]:'';*/
	
	$businessid = \EE\Controller\Base::getPostVariable('businessid','');
	$companyid = \EE\Controller\Base::getPostVariable('companyid','');
	$view = \EE\Controller\Base::getPostVariable('view','');
	$date1 = \EE\Controller\Base::getPostVariable('date1','');
	$date2 = \EE\Controller\Base::getPostVariable('date2','');
	$findinv = \EE\Controller\Base::getPostVariable('findinv','');
	$sort_ap = \EE\Controller\Base::getPostVariable('sort_ap','');
	$subtotal = \EE\Controller\Base::getPostVariable('subtotal','');
	$showvendor = \EE\Controller\Base::getPostVariable('showvendor','');

   ////validate date1 and date2
   $date1 = strtotime($date1);
   $date2 = strtotime($date2);

   $date1 = date("Y-m-d", $date1);
   $date2 = date("Y-m-d", $date2);

   if($date1 < "2000-01-01"){$date1 = "2005-01-01";}
   if($date2 < "2000-01-01"){$date2 = "2005-01-01";}
}

if ($date2==""){
   $date2=$today;$view='All';
}

if ($sort_ap==""){$sort_ap="date";}
if($interco<1){$interco=0;}

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=Treat_DB_ProxyOld::mysql_numrows($result);
//mysql_close();

$loginid=@Treat_DB_ProxyOld::mysql_result($result,0,"loginid");
$security_level=@Treat_DB_ProxyOld::mysql_result($result,0,"security_level");
$mysecurity=@Treat_DB_ProxyOld::mysql_result($result,0,"security");
$bid=@Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
$cid=@Treat_DB_ProxyOld::mysql_result($result,0,"companyid");
$bid2=@Treat_DB_ProxyOld::mysql_result($result,0,"busid2");
$bid3=@Treat_DB_ProxyOld::mysql_result($result,0,"busid3");
$bid4=@Treat_DB_ProxyOld::mysql_result($result,0,"busid4");
$bid5=@Treat_DB_ProxyOld::mysql_result($result,0,"busid5");
$bid6=@Treat_DB_ProxyOld::mysql_result($result,0,"busid6");
$bid7=@Treat_DB_ProxyOld::mysql_result($result,0,"busid7");
$bid8=@Treat_DB_ProxyOld::mysql_result($result,0,"busid8");
$bid9=@Treat_DB_ProxyOld::mysql_result($result,0,"busid9");
$bid10=@Treat_DB_ProxyOld::mysql_result($result,0,"busid10");

$pr1=@Treat_DB_ProxyOld::mysql_result($result,0,"payroll");
$pr2=@Treat_DB_ProxyOld::mysql_result($result,0,"pr2");
$pr3=@Treat_DB_ProxyOld::mysql_result($result,0,"pr3");
$pr4=@Treat_DB_ProxyOld::mysql_result($result,0,"pr4");
$pr5=@Treat_DB_ProxyOld::mysql_result($result,0,"pr5");
$pr6=@Treat_DB_ProxyOld::mysql_result($result,0,"pr6");
$pr7=@Treat_DB_ProxyOld::mysql_result($result,0,"pr7");
$pr8=@Treat_DB_ProxyOld::mysql_result($result,0,"pr8");
$pr9=@Treat_DB_ProxyOld::mysql_result($result,0,"pr9");
$pr10=@Treat_DB_ProxyOld::mysql_result($result,0,"pr10");

if ($num != 1 || ($security_level==1 && ($bid != $businessid || $cid != $companyid)) || $user == "" || $pass == "")
{
    echo "<center><h3>Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
    setcookie("viewcook",$view);
    setcookie("date1cook",$date1);
    setcookie("date2cook",$date2);
    setcookie("sort_ap",$sort_ap);
    setcookie("subtotal",$subtotal);
    setcookie("showvendor",$showvendor);
?>

<html>
<head>
<SCRIPT LANGUAGE="JavaScript" SRC="CalendarPopup.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">document.write(getCalendarStyles());</SCRIPT>

<script language="JavaScript"
   type="text/JavaScript">
function changePage(newLoc)
 {
   nextPage = newLoc.options[newLoc.selectedIndex].value

   if (nextPage != "")
   {
      document.location.href = nextPage
   }
 }
</script>

<STYLE>
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation
			{
			background-color:#6677DD;
			text-align:center;
			vertical-align:center;
			text-decoration:none;
			color:#FFFFFF;
			font-weight:bold;
			}
	.TESTcpDayColumnHeader,
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation,
	.TESTcpCurrentMonthDate,
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDate,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDate,
	.TESTcpCurrentDateDisabled,
	.TESTcpTodayText,
	.TESTcpTodayTextDisabled,
	.TESTcpText
			{
			font-family:arial;
			font-size:8pt;
			}
	TD.TESTcpDayColumnHeader
			{
			text-align:right;
			border:solid thin #6677DD;
			border-width:0 0 1 0;
			}
	.TESTcpCurrentMonthDate,
	.TESTcpOtherMonthDate,
	.TESTcpCurrentDate
			{
			text-align:right;
			text-decoration:none;
			}
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDateDisabled
			{
			color:#D0D0D0;
			text-align:right;
			text-decoration:line-through;
			}
	.TESTcpCurrentMonthDate
			{
			color:#6677DD;
			font-weight:bold;
			}
	.TESTcpCurrentDate
			{
			color: #FFFFFF;
			font-weight:bold;
			}
	.TESTcpOtherMonthDate
			{
			color:#808080;
			}
	TD.TESTcpCurrentDate
			{
			color:#FFFFFF;
			background-color: #6677DD;
			border-width:1;
			border:solid thin #000000;
			}
	TD.TESTcpCurrentDateDisabled
			{
			border-width:1;
			border:solid thin #FFAAAA;
			}
	TD.TESTcpTodayText,
	TD.TESTcpTodayTextDisabled
			{
			border:solid thin #6677DD;
			border-width:1 0 0 0;
			}
	A.TESTcpTodayText,
	SPAN.TESTcpTodayTextDisabled
			{
			height:20px;
			}
	A.TESTcpTodayText
			{
			color:#6677DD;
			font-weight:bold;
			}
	SPAN.TESTcpTodayTextDisabled
			{
			color:#D0D0D0;
			}
	.TESTcpBorder
			{
			border:solid thin #6677DD;
			}
</STYLE>

<SCRIPT LANGUAGE="JavaScript">
<!-- Web Site:  http://dynamicdrive.com -->

<!-- This script and many more are available free online at -->
<!-- The JavaScript Source!! http://javascript.internet.com -->

<!-- Begin
function disableForm(theform) {
if (document.all || document.getElementById) {
for (i = 0; i < theform.length; i++) {
var tempobj = theform.elements[i];
if (tempobj.type.toLowerCase() == "submit" || tempobj.type.toLowerCase() == "reset")
tempobj.disabled = true;
}

}
else {
alert("The form has been submitted.  But, since you're not using IE 4+ or NS 6, the submit button was not disabled on form submission.");
return false;
   }
}
//  End -->
</script>

<SCRIPT LANGUAGE=javascript><!--
function confirmcreate(){return confirm('ARE YOU SURE YOU WANT TO CREATE A TRANSFER?');}
// --></SCRIPT>

<SCRIPT LANGUAGE=javascript><!--
function confirmcreate2(){return confirm('ARE YOU SURE YOU WANT TO CREATE A PURCHASE CARD RECEIPT?');}
// --></SCRIPT>

<script language="JavaScript"
   type="text/JavaScript">
function changePageb(newLoc)
 {
   nextPage = newLoc.value

   if (nextPage != "")
   {
      document.location.href = nextPage
   }
 }
</script>

</head>

<?php

    echo "<body>";
    echo "<div style=border:50px solid red;padding:10px;>";
    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%><tr><td colspan=2>";
    echo "<a href=businesstrack.php><img src=logo.jpg border=0 height=43 width=205></a><p></td></tr>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM company WHERE companyid = '$companyid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $companyname=@Treat_DB_ProxyOld::mysql_result($result,0,"companyname");
    $week_end=@Treat_DB_ProxyOld::mysql_result($result,0,"week_end");
	$com_logo=@Treat_DB_ProxyOld::mysql_result($result,0,"logo");

	if($com_logo == ""){$com_logo = "talogo.gif";}


    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM security WHERE securityid = '$mysecurity'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $sec_bus_sales=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_sales");
    $sec_bus_invoice=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_invoice");
    $sec_bus_order=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_order");
    $sec_bus_payable=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_payable");
    $sec_bus_payable2=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_payable2");
    $sec_bus_payable3=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_payable3");
    $sec_bus_inventor1=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor1");
    $sec_bus_control=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_control");
    $sec_bus_menu=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_menu");
    $sec_bus_order_setup=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_order_setup");
    $sec_bus_request=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_request");
    $sec_route_collection=@Treat_DB_ProxyOld::mysql_result($result,0,"route_collection");
    $sec_route_labor=@Treat_DB_ProxyOld::mysql_result($result,0,"route_labor");
    $sec_route_detail=@Treat_DB_ProxyOld::mysql_result($result,0,"route_detail");
    $sec_bus_labor=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_labor");
    $sec_bus_timeclock=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_timeclock");
	$sec_bus_budget=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_budget");

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM business WHERE companyid = '$companyid' AND businessid = '$businessid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $businessname=@Treat_DB_ProxyOld::mysql_result($result,0,"businessname");
    $businessid=@Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
    $street=@Treat_DB_ProxyOld::mysql_result($result,0,"street");
    $city=@Treat_DB_ProxyOld::mysql_result($result,0,"city");
    $state=@Treat_DB_ProxyOld::mysql_result($result,0,"state");
    $zip=@Treat_DB_ProxyOld::mysql_result($result,0,"zip");
    $phone=@Treat_DB_ProxyOld::mysql_result($result,0,"phone");
    $districtid=@Treat_DB_ProxyOld::mysql_result($result,0,"districtid");
    $bus_unit=@Treat_DB_ProxyOld::mysql_result($result,0,"unit");
    $inventor_goal=@Treat_DB_ProxyOld::mysql_result($result,0,"inventor_goal");
    $operate=@Treat_DB_ProxyOld::mysql_result($result,0,"operate");

    //echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";
    echo "<tr bgcolor=#CCCCFF><td width=1%><img src=logo/$com_logo></td><td><font size=4><b>$businessname #$bus_unit</b></font><br>$street<br>$city, $state $zip<br>$phone</td>";
    echo "<td align=right valign=top><br>";
    echo "<FORM ACTION='editbus.php' method=post name='store'>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<input type=hidden name=businessid value=$businessid>";
    echo "<input type=hidden name=companyid value=$companyid>";
    echo "<INPUT TYPE=submit VALUE=' Store Setup '> ";
    echo "</form></td></tr>";
    //echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";

    echo "<tr><td colspan=3><font color=#999999>";
    if ($sec_bus_sales>0){echo "<a href=busdetail.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Sales</font></a>";}
    if ($sec_bus_invoice>0){echo " | <a href=businvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Invoicing</font></a>";}
    if ($sec_bus_order>0){echo " | <a href=buscatertrack.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Orders</font></a>";}
    if ($sec_bus_payable>0){echo " | <a href=busapinvoice.php?bid=$businessid&cid=$companyid><font color=red onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='red' style='text-decoration:none'>Payables</font></a>";}
    if ($sec_bus_inventor1>0){echo " | <a href=businventor.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Inventory</font></a>";}
    if ($sec_bus_control>0){echo " | <a href=buscontrol.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Control Sheet</font></a>";}
    if ($sec_bus_menu>0){echo " | <a href=buscatermenu.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Menus</font></a>";}
    if ($sec_bus_order_setup>0){echo " | <a href=buscaterroom.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Order Setup</font></a>";}
    if ($sec_bus_request>0){echo " | <a href=busrequest.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Requests</font></a>";}
    if ($sec_route_collection>0||$sec_route_labor>0||$sec_route_detail>0){echo " | <a href=busroute_collect.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Route Collections</font></a>";}
    if ($sec_bus_labor>0||$sec_bus_timeclock){echo " | <a href=buslabor.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Labor</font></a>";}
	if ($sec_bus_budget>0||($businessid == $bid && $pr1 == 1)||($businessid == $bid2 && $pr2 == 1)||($businessid == $bid3 && $pr3 == 1)||($businessid == $bid4 && $pr4 == 1)||($businessid == $bid5 && $pr5 == 1)||($businessid == $bid6 && $pr6 == 1)||($businessid == $bid7 && $pr7 == 1)||($businessid == $bid8 && $pr8 == 1)||($businessid == $bid9 && $pr9 == 1)||($businessid == $bid10 && $pr10 == 1)){echo " | <a href=busbudget.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Budget</font></a>";}
    echo "</td></tr>";

    echo "</table></center><p>";

if ($security_level>0){
    echo "<p><center><table width=90%><tr><td width=50%><form action=busapinvoice.php method=post>";
    echo "<select name=gobus onChange='changePage(this.form.gobus)'>";

    $districtquery="";
    if ($security_level>2&&$security_level<7){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM login_district WHERE loginid = '$loginid'";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();

       $num--;
       while($num>=0){
          $districtid=@Treat_DB_ProxyOld::mysql_result($result,$num,"districtid");
          if($districtquery==""){$districtquery="districtid = '$districtid'";}
          else{$districtquery="$districtquery OR districtid = '$districtid'";}
          $num--;
       }
    }

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    if($security_level>6){$query = "SELECT * FROM business WHERE companyid = '$companyid' ORDER BY businessname DESC";}
    elseif($security_level==2||$security_level==1){$query = "SELECT * FROM business WHERE companyid = '$companyid' AND (businessid = '$bid' OR businessid = '$bid2' OR businessid = '$bid3' OR businessid = '$bid4' OR businessid = '$bid5' OR businessid = '$bid6' OR businessid = '$bid7' OR businessid = '$bid8' OR businessid = '$bid9' OR businessid = '$bid10') ORDER BY businessname DESC";}
    elseif($security_level>2&&$security_level<7){$query = "SELECT * FROM business WHERE companyid = '$companyid' AND ($districtquery) ORDER BY businessname DESC";}
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    $num--;
    while($num>=0){
       $businessname=@Treat_DB_ProxyOld::mysql_result($result,$num,"businessname");
       $busid=@Treat_DB_ProxyOld::mysql_result($result,$num,"businessid");

       if ($busid==$businessid){$show="SELECTED";}
       else{$show="";}

       echo "<option value=busapinvoice.php?bid=$busid&cid=$companyid $show>$businessname</option>";
       $num--;
    }

    echo "</select></td><td></form></td><td width=50% align=right></td></tr></table></center><p>";
}

    $security_level=$sec_bus_payable;

    if ($findinv!=""&&$findinv!="-1"){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM apinvoice WHERE (businessid = '$businessid' OR (vendor = '$businessid' AND transfer = '1')) AND invoicenum LIKE '$findinv' AND transfer != '3'";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();
    }
    elseif ($findinv=="-1"){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM apinvoice WHERE businessid = '$businessid' AND posted = '0' AND transfer != '3' AND transfer != '4' ORDER BY $sort_ap";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();

       ///OR (vendor = '$businessid' AND transfer = '1' AND posted != '1' AND date >= '$date1' AND date <= '$date2')
    }
    elseif($showvendor>0){
       $query = "SELECT * FROM apinvoice WHERE businessid = '$businessid' AND date >= '$date1' AND date <= '$date2' AND vendor = '$showvendor' AND transfer = '0' ORDER BY $sort_ap";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
    }
    else{
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM apinvoice WHERE (businessid = '$businessid' AND date >= '$date1' AND date <= '$date2' AND transfer != '3' AND transfer != '4') OR (vendor = '$businessid' AND transfer = '1' AND posted != '2' AND posted != '0' AND date >= '$date1' AND date <= '$date2' AND transfer != '3') ORDER BY $sort_ap";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();
    }
	
	$s1 = "";
	$s2 = "";
	$s3 = "";
	$s4 = "";
	$showsub="";

    if ($sort_ap=="date"){$s1="SELECTED";}
    elseif($sort_ap=="vendor"){$s2="SELECTED";}
    elseif($sort_ap=="invoicenum"){$s3="SELECTED";}
    elseif($sort_ap=="total"){$s4="SELECTED";}

    if ($subtotal==1){$showsub="CHECKED";}

    echo "<center><table width=90% border=0 cellspacing=0 cellpadding=0><tr valign=bottom><td colspan=4><FORM ACTION='busapinvoice.php' method='post'>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<input type=hidden name=businessid value=$businessid>";
    echo "<input type=hidden name=companyid value=$companyid>";
    echo "<table width=100%><tr><td>View ";
    echo "<SCRIPT LANGUAGE='JavaScript' ID='js18'> var cal18 = new CalendarPopup('testdiv1');cal18.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date1 VALUE='$date1' SIZE=8> <A HREF=# onClick=cal18.select(document.forms[2].date1,'anchor18','yyyy-MM-dd'); return false; TITLE=cal18.select(document.forms[2].date1,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor18' ID='anchor18'><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A> to ";
    echo "<SCRIPT LANGUAGE='JavaScript' ID='js19'> var cal19 = new CalendarPopup('testdiv1');cal19.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date2 VALUE='$date2' SIZE=8> <A HREF=# onClick=cal19.select(document.forms[2].date2,'anchor19','yyyy-MM-dd'); return false; TITLE=cal19.select(document.forms[2].date2,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor19' ID='anchor19'><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A> Sort by <select name=sort_ap><option value=date $s1>Date</option><option value=invoicenum $s3>Invoice #</option><option value=vendor $s2>Vendor</option><option value=total $s4>Total</option></select> <select name=showvendor><option value=>All Vendors</option>";

    $query2 = "SELECT * FROM vendors WHERE businessid = '$businessid' AND is_deleted = '0' ORDER BY name";
    $result2 = Treat_DB_ProxyOld::query($query2);

    while($r=Treat_DB_ProxyOld::mysql_fetch_array($result2)){
       $vendorid=$r["vendorid"];
       $vendor_name=$r["name"];
       $vendor_master=$r["master"];

       if($showvendor==$vendorid){$sel1="SELECTED";}
       else{$sel1="";}

       echo "<option value=$vendorid $sel1>$vendor_name</option>";
    }

    echo "</select> <input type=checkbox name=subtotal value=1 $showsub>Totals ";
    echo " <i><b><u> or</u></b></i> Find Invoice#: <INPUT TYPE=text name=findinv size=10> <INPUT TYPE=submit VALUE='GO'></FORM><FORM ACTION='busapinvoice.php' method='post'><input type=hidden name=username value=$user><input type=hidden name=password value=$pass><input type=hidden name=businessid value=$businessid><input type=hidden name=companyid value=$companyid><input type=hidden name=findinv value='-1'><input type=hidden name=date1 value='$date1'><input type=hidden name=date2 value='$date2'></td><td align=right><INPUT TYPE=submit VALUE='Unsubmitted'></FORM></td></tr></table></td></tr>";
    //echo "<tr bgcolor=black><td height=1 colspan=4></td></tr></table>";

    if($sec_bus_payable2>0||$sec_bus_payable3>0){
       echo "<p><center><table width=90% cellspacing=0 cellpadding=0>";
       echo "<tr bgcolor=#E8E7E7 valign=top><td width=1%><img src=leftcrn.jpg height=6 width=6 align=top></td><td width=15%><center><a href=busapinvoice.php?cid=$companyid&bid=$businessid style=$style><font color=black>Payables</a></center></td><td width=1% align=right><img src=rightcrn.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1% bgcolor=#CCCCCC><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=15% bgcolor=#CCCCCC><center><a href=busapinvoice2.php?cid=$companyid&bid=$businessid style=$style style=$style><font color=blue>Expense Reports</a></center></td><td width=1% align=right bgcolor=#CCCCCC><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1% bgcolor=#CCCCCC><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=15% bgcolor=#CCCCCC><center><a href=busapinvoice3.php?cid=$companyid&bid=$businessid style=$style style=$style><font color=blue>Purchase Orders</a></center></td><td width=1% align=right bgcolor=#CCCCCC><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td bgcolor=white width=60%></td></tr>";
       echo "<tr height=2><td colspan=3 bgcolor=#E8E7E7></td><td colspan=4></td><td colspan=4></td><td></td><td colspan=1></td></tr>";
       echo "</table>";
    }

    echo "<table width=90% cellspacing=0 cellpadding=0><tr bgcolor=#E8E7E7><td colspan=4>&nbsp;</td></tr>";
    echo "<tr bgcolor=black><td height=1 colspan=4></td></tr>";

    //////////////////////////
    /////budget graph/////////
    //////////////////////////
        ////include
        include("lib/class.credits.php");

        ////dates
        $graph_today = date("Y-m-d");
        $graph_date1 = substr($date1,0,8);
        $graph_date1 .= "01";
        $graph_date2 = date("Y-m-t",mktime(0, 0, 0, substr($date1,5,2) , substr($date1,8,2), substr($date1,0,4)));
        $graph_last_month = date("Y-m-d",mktime(0, 0, 0, substr($date1,5,2)-1 , "01", substr($date1,0,4)));

        $months_left = date("n", mktime(0, 0, 0, substr($date1,5,2) , "01", substr($date1,0,4)));
        $months_left = 13 - $months_left;

        ////opertaing days
        $temp_date = $graph_date1;
		$count_value = 0;
        $counter = 1;
        $total_op_days = array();
        $current_op_days = array();
        $actual_sales = array();
		$cur_days = "";
		$tot_days = "";
		$tot_sales_diff = "";
		$cur_cogs_bgt = "";
		$total_span = "";
		$totalamt = "";

        while($temp_date <= $graph_date2){
            if($operate == 5 && dayofweek($temp_date) != "Saturday" && dayofweek($temp_date) != "Sunday"){
                if ( !isset($total_op_days[$counter])){
					$total_op_days[$counter] = 0;
				} else {
					$total_op_days[$counter]++;
				}
                $sales = Credits::get_sales($temp_date,$temp_date,$businessid);
				if ( !isset($actual_sales[$counter])){
					$actual_sales[$counter] = 0;
				} else {
					$actual_sales[$counter]+=$sales;
				}
                
                if($sales != 0){$current_op_days[$counter]++;}
                elseif($temp_date < $graph_today){$total_op_days[$counter]--;}
            }
            elseif($operate == 6 && dayofweek($temp_date) != "Sunday"){
                $total_op_days[$counter]++;
                $sales = Credits::get_sales($temp_date,$temp_date,$businessid);
                $actual_sales[$counter]+=$sales;
                if($sales != 0){$current_op_days[$counter]++;}
                elseif($temp_date < $graph_today){$total_op_days[$counter]--;}
            }
            elseif($operate == 7){
                $total_op_days[$counter]++;
                $sales = Credits::get_sales($temp_date,$temp_date,$businessid);
                $actual_sales[$counter]+=$sales;
                if($sales != 0){$current_op_days[$counter]++;}
                elseif($temp_date < $graph_today){$total_op_days[$counter]--;}
            }

            if(dayofweek($temp_date) == $week_end || $temp_date == $graph_date2){$weeks[$counter] = $temp_date; $counter++;}

            $temp_date = nextday($temp_date);
        }

        $graph_month_name = date("F", mktime(0, 0, 0, substr($date1,5,2) , "01", substr($date1,0,4)));

        /////sales
        $graph_sales = Credits::get_sales($graph_date1,$graph_date2,$businessid);

        ////sales budget
        $graph_sales_bgt = Credits::get_budget($graph_date1,$companyid,$bus_unit,"10");

        ////get cogs
        $graph_cogs = Credits::get_cogs($graph_date1,$graph_date2,$businessid);

        ////get cogs budget
        $graph_cogs_bgt = Credits::get_budget($graph_date1,$companyid,$bus_unit,"20");

        ////last month inventory amount
        $query90 = "SELECT SUM(amount) AS total_inv FROM inventor WHERE businessid = $businessid AND date = '$graph_last_month'";
        $result90 = Treat_DB_ProxyOld::query($query90);

        $graph_last_inv = Treat_DB_ProxyOld::mysql_result($result90,0,"total_inv");

        ///actual cogs budget
        $act_cogs_bgt = number_format($graph_cogs_bgt,2);

        ///refigure goal
        if($inventor_goal == 0){
            $inv_reduction = 0;
        }
        else{
            $inv_reduction = round(($graph_last_inv - $inventor_goal)/$months_left/array_sum($total_op_days), 2);
        }

        ////show totals
        $show_cogs = number_format($graph_cogs,2);
        $show_cogs_bgt = number_format($graph_cogs_bgt,2);
        $show_sales = number_format($graph_sales,2);
        $show_sales_bgt = number_format($graph_sales_bgt,2);
        $show_inv_goal = number_format($inventor_goal,2);

        ////graph
        if($graph_cogs_bgt > 0){

        echo "<tr valign=top><td colspan=4 style=\"border-left:1px solid black;border-right:1px solid black;border-bottom:1px solid black;background:#E8E7E7;\">";
            echo "<table width=100%><tr valign=top><td width=20%>";
            echo "&nbsp;<b>$graph_month_name</b></td><td><br>";
            foreach($weeks AS $key => $value){
                if ( !isset($current_op_days[$key])){
					$cur_days += 0;
				} else {
					$cur_days += $current_op_days[$key];
				}
				
                $tot_days += $total_op_days[$key];
				//var_dump($current_op_days[$key]);
                if ( !isset($current_op_days[$key])){
					$current_op_days[$key] = 0;
					$sales_diff = round((($graph_sales_bgt / array_sum($total_op_days)) * $current_op_days[$key] - $actual_sales[$key])*($graph_cogs_bgt/$graph_sales_bgt), 2);
				} else {
					$sales_diff = round((($graph_sales_bgt / array_sum($total_op_days)) * $current_op_days[$key] - $actual_sales[$key])*($graph_cogs_bgt/$graph_sales_bgt), 2);
				}
                //$sales_diff = round((($graph_sales_bgt / array_sum($total_op_days)) * $current_op_days[$key] - $actual_sales[$key])*($graph_cogs_bgt/$graph_sales_bgt), 2);
                $tot_sales_diff += $sales_diff;
                $diff = $cur_cogs_bgt;
                $cur_cogs_bgt += round(($graph_cogs_bgt - ($tot_days * $inv_reduction)) / array_sum($total_op_days) * $tot_days, 2);
                $diff = round($cur_cogs_bgt - $diff, 0);
                $week_amt[$key] = $diff - $sales_diff;
            }

            ///size graph
            $graph_width = 650;
            $per_pixel = round(($graph_cogs_bgt * 1.1) / $graph_width, 0);
            $per_pixel2 = round(($graph_cogs) / $graph_width, 0);
            if($per_pixel2 > $per_pixel){$per_pixel = $per_pixel2;}

            ///purchases
            echo "<center><div style=\"width:95%\">";
                $cogs_value = round($graph_cogs/$per_pixel,0);
                $rest = $graph_width - $cogs_value;
                echo "<span style=\"display:inline-block;height:22px;width:" . $cogs_value . "px;text-align:center;font-size:10px;background:#00FF00;border-left:1px solid black;border-top:1px solid black;border-bottom:1px solid black;\">$show_cogs</span>";
                echo "<span style=\"display:inline-block;height:22px;width:" . $rest . "px;text-align:center;font-size:10px;background:#00CCFF;border-right:1px solid black;border-top:1px solid black;border-bottom:1px solid black;\">&nbsp;</span>";
            echo "</div>";

            ///legend
            echo "<div style=\"width:95%\">";
            $last_value = 0;
            foreach($week_amt AS $key => $value){
                $span = round(($value-$last_value)/$per_pixel,0);
                $total_span += $span;
                $show_value = number_format($value,0);
                echo "<span style=\"display:inline-block;height:22px;width:" . $span . "px;border-right:3px solid black;text-align:right;font-size:10px;\">Wk$key&nbsp;<br>$show_value&nbsp;</span>";

                $last_value = $value;
            }
            $span = $graph_width - $total_span;
            echo "<span style=\"display:inline-block;height:22px;width:" . $span . "px;text-align:right;font-size:10px;\">&nbsp;</span>";
            echo "</div><center></td>";

            $tot_sales_diff = number_format($tot_sales_diff * -1,2);
            echo "<td width=20% align=right><table>";
                echo "<tr><td align=right>COGS BGT:</td><td align=right> $show_cogs_bgt&nbsp;</td></tr>";
                echo "<tr><td align=right>INV Goal:</td><td align=right> $show_inv_goal&nbsp;</td></tr>";
                echo "<tr><td align=right>+/- Sales BGT:</td><td align=right> $tot_sales_diff&nbsp;</td></tr></table>";
            echo "</td></tr></table>";

        echo "</td></tr>";
        }
    ///////////////////////////
    /////end graph/////////////
    ///////////////////////////

    echo "<tr bgcolor=#E8E7E7><td><b>Invoice#</b></td><td><b>Vendor/Unit</b></td><td><b>Date</b></td><td align=right><b>Total</b></td></tr>";
    echo "<tr bgcolor=#CCCCCC><td height=1 colspan=4></td></tr>";

    $lastsort=-1;
    $grandtotal=0;
    $subtotal2=0;
    $subtot=0;
    $costotal=0;
    $results=$num;
    $num--;
    while ($num>=-1){
          $receive=0;
          $apinvoiceid=@Treat_DB_ProxyOld::mysql_result($result,$num,"apinvoiceid");
          $busid2=@Treat_DB_ProxyOld::mysql_result($result,$num,"businessid");
          $vendorid=@Treat_DB_ProxyOld::mysql_result($result,$num,"vendor");
          $apinvoicedate=@Treat_DB_ProxyOld::mysql_result($result,$num,"date");
          $invoicenum=@Treat_DB_ProxyOld::mysql_result($result,$num,"invoicenum");
          $total=@Treat_DB_ProxyOld::mysql_result($result,$num,"total");
          $inputtotal=@Treat_DB_ProxyOld::mysql_result($result,$num,"invtotal");
          $transfer=@Treat_DB_ProxyOld::mysql_result($result,$num,"transfer");
          $pc_type=@Treat_DB_ProxyOld::mysql_result($result,$num,"pc_type");
          $notes=@Treat_DB_ProxyOld::mysql_result($result,$num,"notes");
          if ($transfer==1&&$vendorid==$businessid){$total=$total*-1;$inputtotal=$inputtotal*-1;$receive=$vendorid;}
          $total=money($total);

          if ($lastsort!=-1&&$subtotal==1&&$sort_ap=="date"&&$lastsort!=$apinvoicedate){$subtotal2=number_format($subtotal2,2);echo "<tr bgcolor='#E8E7E7' onMouseOver=this.bgColor='#00FF00' onMouseOut=this.bgColor='#E8E7E7'><td colspan=2><b>Results: $subtot</b></td><td align=right colspan=2><b>$$subtotal2</b></td></tr>";echo "<tr bgcolor=#CCCCCC><td height=1 colspan=6></td></tr>";$subtotal2=0;$subtot=0;}
          elseif ($lastsort!=-1&&$subtotal==1&&$sort_ap=="vendor"&&$lastsort!=$vendorid){$subtotal2=number_format($subtotal2,2);echo "<tr bgcolor='#E8E7E7' onMouseOver=this.bgColor='#00FF00' onMouseOut=this.bgColor='#E8E7E7'><td colspan=2><b>Results: $subtot</b></td><td align=right colspan=2><b>$$subtotal2</b></td></tr>";echo "<tr bgcolor=#CCCCCC><td height=1 colspan=6></td></tr>";$subtotal2=0;$subtot=0;}
          elseif ($lastsort!=-1&&$subtotal==1&&$sort_ap=="invoicenum"&&$lastsort!=$invoicenum){$subtotal2=number_format($subtotal2,2);echo "<tr bgcolor='#E8E7E7' onMouseOver=this.bgColor='#00FF00' onMouseOut=this.bgColor='#E8E7E7'><td colspan=2><b>Results: $subtot</b></td><td align=right colspan=2><b>$$subtotal2</b></td></tr>";echo "<tr bgcolor=#CCCCCC><td height=1 colspan=6></td></tr>";$subtotal2=0;$subtot=0;}
          elseif ($lastsort!=-1&&$subtotal==1&&$sort_ap=="total"&&$lastsort!=$total){$subtotal2=number_format($subtotal2,2);echo "<tr bgcolor='#E8E7E7' onMouseOver=this.bgColor='#00FF00' onMouseOut=this.bgColor='#E8E7E7'><td colspan=2><b>Results: $subtot</b></td><td align=right colspan=2><b>$$subtotal2</b></td></tr>";echo "<tr bgcolor=#CCCCCC><td height=1 colspan=6></td></tr>";$subtotal2=0;$subtot=0;}

          $grandtotal=$grandtotal+$total;
          $subtotal2=$subtotal2+$total;
          $subtot++;

          if ($transfer==0){
             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query2 = "SELECT * FROM vendors WHERE vendorid = '$vendorid'";
             $result2 = Treat_DB_ProxyOld::query($query2);
             //mysql_close();
             $vendorname=@Treat_DB_ProxyOld::mysql_result($result2,0,"name");
          }
          elseif ($transfer==1&&$vendorid==$businessid){
             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query2 = "SELECT businessname FROM business WHERE businessid = '$busid2'";
             $result2 = Treat_DB_ProxyOld::query($query2);
             //mysql_close();
             $vendorname=@Treat_DB_ProxyOld::mysql_result($result2,0,"businessname");
          }
          elseif ($transfer==1){
             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query2 = "SELECT businessname FROM business WHERE businessid = '$vendorid'";
             $result2 = Treat_DB_ProxyOld::query($query2);
             //mysql_close();
             $vendorname=@Treat_DB_ProxyOld::mysql_result($result2,0,"businessname");
          }
          elseif ($transfer==2){
             if ($pc_type==0){
                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query4 = "SELECT firstname,lastname FROM login WHERE loginid = '$vendorid'";
                $result4 = Treat_DB_ProxyOld::query($query4);
                //mysql_close();
             }
             elseif($pc_type==1){
                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query4 = "SELECT firstname,lastname FROM login_route WHERE login_routeid = '$vendorid'";
                $result4 = Treat_DB_ProxyOld::query($query4);
                //mysql_close();
             }
             $firstname=@Treat_DB_ProxyOld::mysql_result($result4,0,"firstname");
             $lastname=@Treat_DB_ProxyOld::mysql_result($result4,0,"lastname");

             $vendorname="$lastname, $firstname";
          }

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query3 = "SELECT * FROM apinvoicedetail WHERE apinvoiceid = '$apinvoiceid'";
          $result3 = Treat_DB_ProxyOld::query($query3);
          $num3=Treat_DB_ProxyOld::mysql_numrows($result3);
          //mysql_close();

          $num3--;
          while ($num3>=0){
             $amount2=0;
             $amount2=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"amount");
             $apaccountid=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"apaccountid");
             $tfr_acct=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"tfr_acct");

             if($vendorid==$businessid&&$transfer==1){$apaccountid=$tfr_acct;}

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query4 = "SELECT cos FROM acct_payable WHERE acct_payableid = '$apaccountid'";
             $result4 = Treat_DB_ProxyOld::query($query4);
             //mysql_close();

             $costype=@Treat_DB_ProxyOld::mysql_result($result4,0,"cos");

             if ($costype==1&&$num!=-1){
                if($vendorid==$businessid&&$transfer==1){$amount2=$amount2*-1;}
                $costotal=$costotal+$amount2;
             }

             $num3--;
          }

          if (money($total)!=money($inputtotal)){$showerror="<font color=red>*</font>";}
          else {$showerror="";}
          if ($total<0){$invcolor="red";}
          else {$invcolor="blue";}
          if ($total<0){$total="<font color=red>$total</font>";}

          if($notes!=""){$notes = ": $notes";}

          if ($transfer==0){$showcolor="white";$showtitle="Invoice$notes";}
          elseif($transfer==1){$showcolor="yellow";$showtitle="Transfer$notes";}
          elseif($transfer==2){$showcolor="#00FFFF";$showtitle="Purchase Card$notes";}

          if($num!=-1){echo "<tr bgcolor='$showcolor' onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor='$showcolor'><td><a style=$style href=apinvoice.php?bid=$businessid&cid=$companyid&apinvoiceid=$apinvoiceid title='$showtitle'><font color='$invcolor'>$invoicenum</font></a></td><td>$vendorname</td><td>$apinvoicedate</td><td align=right>$$total$showerror</td></tr>";}
          if($num!=-1){echo "<tr bgcolor=#CCCCCC><td height=1 colspan=6></td></tr>";}

          $num--;

          if ($sort_ap=="date"){$lastsort=$apinvoicedate;}
          elseif ($sort_ap=="vendor"){$lastsort=$vendorid;}
          elseif ($sort_ap=="invoicenum"){$lastsort=$invoicenum;}
          elseif ($sort_ap=="total"){$lastsort=$total;}
    }

    echo "<tr bgcolor=black><td height=1 colspan=4></td></tr>";
    $grandtotal=number_format($grandtotal,2);
    echo "<tr><td><b>Results: $results</b></td><td colspan=2></td><td align=right><b>Total: $$grandtotal</b></td></tr>";
    $costotal=number_format($costotal,2);
    echo "<tr><td></td><td colspan=2></td><td align=right><b>COGS Total: $$costotal</b></td></tr>";
    echo "<tr bgcolor=white><td height=3 colspan=4></td></tr>";

    echo "<tr colspan=4><td><form action=submitap.php method=post onSubmit='return disableForm(this);'><input type=hidden name=username value=$user><input type=hidden name=password value=$pass><input type=hidden name=businessid value=$businessid><input type=hidden name=companyid value=$companyid><input type=submit value='Submit Payables Only'></form><p></td></tr>";

    echo "<tr><td colspan=4><a name=new></a> <form action='apinvoice.php#additems' method='post' onSubmit='return disableForm(this);'> <u>Invoice Date</u>: <SCRIPT LANGUAGE='JavaScript' ID='js20'> var cal20 = new CalendarPopup('testdiv1');cal20.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date3 VALUE='$today' SIZE=8> <A HREF=#new onClick=cal20.select(document.forms[5].date3,'anchor20','yyyy-MM-dd'); return false; TITLE=cal20.select(document.forms[5].date3,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor20' ID='anchor20'><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A> <u>Invoice#</u>: <input type=text maxlength=10 name=invoicenum size=10>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<input type=hidden name=businessid value=$businessid>";
    echo "<input type=hidden name=companyid value=$companyid>";
    echo "<input type=hidden name=date value='$today'>";
    echo "<input type=hidden name=new value='yes'>";
    echo "<input type=hidden name=transfer value='0'>";
    echo " <u>Vendor</u>: <select name=vendor>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query3 = "SELECT * FROM vendors WHERE businessid = '$businessid' AND is_deleted = '0' ORDER BY name DESC";
    $result3 = Treat_DB_ProxyOld::query($query3);
    $num3=Treat_DB_ProxyOld::mysql_numrows($result3);
    //mysql_close();

    //$num3--;

    while ($num3>=0){
       $vendorname=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"name");
       $vendorid=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"vendorid");
       echo "<option value=$vendorid>$vendorname</option>";
       $num3--;
    }

    echo "</select>";

    echo " <u>Total</u>: <b>$</b><input type=text name=inputtotal size=6> <input type=submit value='Create'></form></td></tr>";

    ////////////////////////////TRANSFER

    echo "<tr><td colspan=4><a name=new></a> <form action='apinvoice.php#additems' method='post' onSubmit='return disableForm(this);'> <u>Transfer Date</u>: <SCRIPT LANGUAGE='JavaScript' ID='js21'> var cal21 = new CalendarPopup('testdiv1');cal21.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date3 VALUE='$today' SIZE=8> <A HREF=#new onClick=cal21.select(document.forms[6].date3,'anchor21','yyyy-MM-dd'); return false; TITLE=cal21.select(document.forms[6].date3,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor21' ID='anchor21'><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A> ";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<input type=hidden name=businessid value=$businessid>";
    echo "<input type=hidden name=companyid value=$companyid>";
    echo "<input type=hidden name=date value='$today'>";
    echo "<input type=hidden name=new value='yes'>";
    echo "<input type=hidden name=transfer value='1'>";
    echo " <u>Unit</u>: <select name=vendor>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query3 = "SELECT * FROM business WHERE companyid = '$companyid' AND businessid != '$businessid' AND exp_trans = '0' AND restrict_transfer_to = '0' ORDER BY businessname DESC";
    $result3 = Treat_DB_ProxyOld::query($query3);
    $num3=Treat_DB_ProxyOld::mysql_numrows($result3);
    //mysql_close();

    $num3--;

    while ($num3>=0){
       $busname=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"businessname");
       $tfrid=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"businessid");
       echo "<option value=$tfrid>$busname</option>";
       $num3--;
    }

    echo "<optgroup label='Inter Company Transfer'>";

    $query3 = "SELECT * FROM business WHERE companyid != '$companyid' AND companyid != '2' AND exp_trans = '0' AND restrict_transfer_to = '0' ORDER BY businessname DESC";
    $result3 = Treat_DB_ProxyOld::query($query3);
    $num3=Treat_DB_ProxyOld::mysql_numrows($result3);

    $num3--;

    while ($num3>=0){
       $busname=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"businessname");
       $tfrid=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"businessid");
       echo "<option value=$tfrid>$busname</option>";
       $num3--;
    }

    echo "</optgroup></select>";

    echo " <a name=transfer></a> <input type=submit value='Create' onclick='return confirmcreate()'></form></td></tr>";

    /*////////////////////////////PURCHASE CARDS

    echo "<tr><td colspan=4><a name=new></a> <form action='apinvoice.php#additems' method='post' onSubmit='return disableForm(this);'> <u>Purchase Card Date</u>: <SCRIPT LANGUAGE='JavaScript' ID='js22'> var cal22 = new CalendarPopup('testdiv1');cal22.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date3 VALUE='$today' SIZE=8> <A HREF=#new onClick=cal22.select(document.forms[7].date3,'anchor22','yyyy-MM-dd'); return false; TITLE=cal22.select(document.forms[7].date3,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor22' ID='anchor22'><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A> ";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<input type=hidden name=businessid value=$businessid>";
    echo "<input type=hidden name=companyid value=$companyid>";
    echo "<input type=hidden name=date value='$today'>";
    echo "<input type=hidden name=new value='yes'>";
    echo "<input type=hidden name=transfer value='2'>";
    echo " <u>Purchase Card User</u>: <select name=vendor>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query3 = "SELECT * FROM purchase_card WHERE businessid = '$businessid' AND is_deleted = '0'";
    $result3 = Treat_DB_ProxyOld::query($query3);
    $num3=mysql_numrows($result3);
    //mysql_close();

    $num3--;

    while ($num3>=0){
       $loginid=@mysql_result($result3,$num3,"loginid");
       $type=@mysql_result($result3,$num3,"type");

       if ($type==0){
          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query4 = "SELECT firstname,lastname FROM login WHERE loginid = '$loginid'";
          $result4 = Treat_DB_ProxyOld::query($query4);
          //mysql_close();
       }
       elseif($type==1){
          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query4 = "SELECT firstname,lastname FROM login_route WHERE login_routeid = '$loginid'";
          $result4 = Treat_DB_ProxyOld::query($query4);
          //mysql_close();
       }
       $firstname=@mysql_result($result4,0,"firstname");
       $lastname=@mysql_result($result4,0,"lastname");

       $myvalue="$loginid;$type";

       echo "<option value=$myvalue>$lastname, $firstname</option>";
       $num3--;
    }

    echo "</select>";

    echo " <input type=submit value='Create' onclick='return confirmcreate2()'></form></td></tr>";*/


    echo "</table></center><DIV ID=testdiv1 STYLE=position:absolute;visibility:hidden;background-color:white;layer-background-color:white;></DIV>";

    //mysql_close();

    echo "<br><FORM ACTION=businesstrack.php method=post>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<center><INPUT TYPE=submit VALUE=' Return to Main Page'></FORM></center></body>";

	google_page_track();
}
?>
