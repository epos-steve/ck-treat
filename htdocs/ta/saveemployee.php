<?php

function nextday($date2)
{
	$day=substr($date2,8,2);
	$month=substr($date2,5,2);
	$year=substr($date2,0,4);
	$leap = date("L");

	if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
	{
		if ($month == "01"){$month="02";}
		elseif ($month == "03"){$month="04";}
		elseif ($month == "05"){$month="06";}
		elseif ($month == "07"){$month="08";}
		elseif ($month == "08"){$month="09";}
		elseif ($month == "10"){$month="11";}
		$day='01';
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($month=='02' && $day == '29' && $leap == '1')
	{
		$month='03';
		$day='01';
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($month=='02' && $day == '28' && $leap == '0')
	{
		$month='03';
		$day='01';
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
	{
		if ($month == "04"){$month="05";}
		if ($month == "06"){$month="07";}
		if ($month == "09"){$month="10";}
		if ($month == "11"){$month="12";}
		$day='01';
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($month=='12' && $day=='31')
	{
		$day='01';
		$month='01';
		$year++;
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else
	{
		$day=$day+1;
		if ($day<10){$day="0$day";}
		$tomorrow="$year-$month-$day";
		return $tomorrow;
	}
}


if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
require_once( dirname(__FILE__) . '/../../application/bootstrap.php' );

$tempuser = null; # Notice: Undefined variable
$order_only = null; # Notice: Undefined variable
$del_date = null; # Notice: Undefined variable

$style = "text-decoration:none";

$user = \EE\Controller\Base::getPostVariable( 'username', '' );
$pass = \EE\Controller\Base::getPostVariable( 'password', '' );
$businessid = \EE\Controller\Base::getPostVariable( 'businessid', '' );
$companyid = \EE\Controller\Base::getPostVariable( 'companyid', '' );
$employeeid = \EE\Controller\Base::getPostVariable( 'employeeid', '' );
$firstname = \EE\Controller\Base::getPostVariable( 'firstname', '' );
$lastname = \EE\Controller\Base::getPostVariable( 'lastname', '' );
$sec_level = \EE\Controller\Base::getPostVariable( 'sec_level', '' );
$busid = \EE\Controller\Base::getPostVariable( 'busid', '' );
$payroll = \EE\Controller\Base::getPostVariable( 'payroll', '' );
$busid2 = \EE\Controller\Base::getPostVariable( 'busid2', '' );
$busid3 = \EE\Controller\Base::getPostVariable( 'busid3', '' );
$busid4 = \EE\Controller\Base::getPostVariable( 'busid4', '' );
$busid5 = \EE\Controller\Base::getPostVariable( 'busid5', '' );
$busid6 = \EE\Controller\Base::getPostVariable( 'busid6', '' );
$busid7 = \EE\Controller\Base::getPostVariable( 'busid7', '' );
$busid8 = \EE\Controller\Base::getPostVariable( 'busid8', '' );
$busid9 = \EE\Controller\Base::getPostVariable( 'busid9', '' );
$busid10 = \EE\Controller\Base::getPostVariable( 'busid10', '' );
$empl_user = \EE\Controller\Base::getPostVariable( 'empl_user', '' );
$empl_pass = \EE\Controller\Base::getPostVariable( 'empl_pass', '' );
$empl_no = \EE\Controller\Base::getPostVariable( 'empl_no', '' );
$email = \EE\Controller\Base::getPostVariable( 'email', '' );
$new_empl = \EE\Controller\Base::getPostVariable( 'new_empl', '' );
$myseclvl = \EE\Controller\Base::getPostVariable( 'myseclvl', '' );
$oldseclvl = \EE\Controller\Base::getPostVariable( 'oldseclvl', '' );
$myid = \EE\Controller\Base::getPostVariable( 'myid', '' );
$expiredays = \EE\Controller\Base::getPostVariable( 'expiredays', '' );
$distnum = \EE\Controller\Base::getPostVariable( 'distnum', '' );
$co_num = \EE\Controller\Base::getPostVariable( 'co_num', '' );
$sendemail = \EE\Controller\Base::getPostVariable( 'sendemail', '' );
$security = \EE\Controller\Base::getPostVariable( 'security', '' );
$showcustom = \EE\Controller\Base::getPostVariable( 'showcustom', '' );
$is_deleted = \EE\Controller\Base::getPostVariable( 'is_deleted', '' );
$receive_kpi = \EE\Controller\Base::getPostVariable( 'receive_kpi', '' );

if ( $security == -1 ) {
	$showcustom = 1;
}

//////////EXTENDED FIELDS

$title = \EE\Controller\Base::getPostVariable( 'title', '' );
$street = \EE\Controller\Base::getPostVariable( 'street', '' );
$city = \EE\Controller\Base::getPostVariable( 'city', '' );
$state = \EE\Controller\Base::getPostVariable( 'state', '' );
$zip = \EE\Controller\Base::getPostVariable( 'zip', '' );
$phone = \EE\Controller\Base::getPostVariable( 'phone', '' );
$fax = \EE\Controller\Base::getPostVariable( 'fax', '' );

$day = date("d");
$year = date("Y");
$month = date("m");
$today = "$year-$month-$day";

if ( $expiredays != '' ) {
	$tempuser = 1;
	$tempexpiredate = $today;
	for ( $counter = 1; $counter <= $expiredays; $counter++ ) {
		$tempexpiredate = nextday( $tempexpiredate );
	}
}

for ( $counter = 1; $counter <= 30; $counter++ ) {
	$today = nextday( $today );
}

if ( $busid == '' ) {
	$busid = '0';
}
if ( $payroll == '' ) {
	$payroll = '0';
}

$passed_cookie_login = require_once( realpath( DOC_ROOT . '/../lib/inc/cookie-login.php' ) );
$pr2 = \EE\Controller\Base::getPostVariable( 'pr2', '0' );
$pr3 = \EE\Controller\Base::getPostVariable( 'pr3', '0' );
$pr4 = \EE\Controller\Base::getPostVariable( 'pr4', '0' );
$pr5 = \EE\Controller\Base::getPostVariable( 'pr5', '0' );
$pr6 = \EE\Controller\Base::getPostVariable( 'pr6', '0' );
$pr7 = \EE\Controller\Base::getPostVariable( 'pr7', '0' );
$pr8 = \EE\Controller\Base::getPostVariable( 'pr8', '0' );
$pr9 = \EE\Controller\Base::getPostVariable( 'pr9', '0' );
$pr10 = \EE\Controller\Base::getPostVariable( 'pr10', '0' );

if (
	!$passed_cookie_login
	|| !$firstname
	|| !$lastname
	|| !$sec_level
	|| ( $myseclvl <= $oldseclvl && $myid != $employeeid )
	|| ( strtoupper( $new_empl ) == 'NEW' && $myseclvl < $sec_level )
)
{
	include_once( 'inc/template-login-failed.php' );
/*
?>
<html>
	<head>
		<title>Login Failed</title>
	</head>
	<body>
		<center>
			<h3>Login Failed</h3>
			<p>
				Use your browser's back button to try again or 
				go back to the <a href="/">login page</a>.
			</p>
		</center>
	</body>
</html>
<?php
*/
}

else
{
	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM login WHERE username = '$empl_user' AND username != ''";
	$result = Treat_DB_ProxyOld::query( $query );
	$num = @mysql_numrows( $result );
	//mysql_close();

	if ( $num != 0 ) {
		$updateid = @mysql_result( $result, 0, 'loginid' );
	}

	if ( $num != 0 && $employeeid != $updateid ) {
?>
<?php
		echo "<center><font color=red><h3>Username already exists!</h3></font><br>Use your browser's back button to continue.</font></center>";
?>
<?php
	}
	else {

		///////////CUSTOM SECURITY
		if ( $security == -1 ) {
			$query = "INSERT INTO security (companyid,security_name,custom) VALUES ('$companyid','$firstname $lastname','1')";
			$result = Treat_DB_ProxyOld::query( $query );
			$security = mysql_insert_id();
		}

		if ( strtoupper( $new_empl ) == 'NEW' ) {
			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			if ( $tempuser == 1 ) {
				$query = "INSERT INTO login (lastname,firstname,security_level,security,username,password,businessid,payroll,busid2,busid3,busid4,busid5,busid6,busid7,busid8,busid9,busid10,empl_no,email,companyid,passexpire,pr2,pr3,pr4,pr5,pr6,pr7,pr8,pr9,pr10,temp_expire,order_only,oo2,oo3,oo4,oo5,oo6,oo7,oo8,oo9,oo10,move_date,receive_kpi) VALUES ('$lastname', '$firstname', '$sec_level', '$security', '$empl_user', '$empl_pass', '$busid', '$payroll', '$busid2', '$busid3', '$busid4', '$busid5', '$busid6', '$busid7', '$busid8', '$busid9', '$busid10', '$empl_no', '$email', '$companyid','$today','$pr2','$pr3','$pr4','$pr5','$pr6','$pr7','$pr8','$pr9','$pr10','$tempexpiredate','$order_only','$oo2','$oo3','$oo4','$oo5','$oo6','$oo7','$oo8','$oo9','$oo10','$today','$receive_kpi')";
			}
			else {
				$query = "INSERT INTO login (lastname,firstname,security_level,security,username,password,businessid,payroll,busid2,busid3,busid4,busid5,busid6,busid7,busid8,busid9,busid10,empl_no,email,companyid,passexpire,pr2,pr3,pr4,pr5,pr6,pr7,pr8,pr9,pr10,order_only,oo2,oo3,oo4,oo5,oo6,oo7,oo8,oo9,oo10,move_date,receive_kpi) VALUES ('$lastname', '$firstname', '$sec_level', '$security', '$empl_user', '$empl_pass', '$busid', '$payroll', '$busid2', '$busid3', '$busid4', '$busid5', '$busid6', '$busid7', '$busid8', '$busid9', '$busid10', '$empl_no', '$email', '$companyid','$today','$pr2','$pr3','$pr4','$pr5','$pr6','$pr7','$pr8','$pr9','$pr10','$order_only','$oo2','$oo3','$oo4','$oo5','$oo6','$oo7','$oo8','$oo9','$oo10','$today','$receive_kpi')";
			}
			$result = Treat_DB_ProxyOld::query( $query );
			$employeeid = mysql_insert_id();
			//mysql_close();
		}
		else {
			if ( $is_deleted == 1 ) {
				$del_date = $today;
				$empl_pass = "$empl_pass(is_deleted)";
				$sec_level = 0;
				$busid = 0;
				$busid2 = 0;
				$busid3 = 0;
				$busid4 = 0;
				$busid5 = 0;
				$busid6 = 0;
				$busid7 = 0;
				$busid8 = 0;
				$busid9 = 0;
				$busid10 = 0;
				$oo2 = 0;
			}

			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			if ( $tempuser == 1 ) {
				$query = "UPDATE login SET lastname = '$lastname', firstname = '$firstname', security_level = '$sec_level', security = '$security', username = '$empl_user', password = '$empl_pass', businessid = '$busid', payroll = '$payroll', busid2 = '$busid2', busid3 = '$busid3', busid4 = '$busid4', busid5 = '$busid5', busid6 = '$busid6', busid7 = '$busid7', busid8 = '$busid8', busid9 = '$busid9', busid10 = '$busid10', email = '$email'
				, pr2 = '$pr2', pr3 = '$pr3', pr4 = '$pr4', pr5 = '$pr5', pr6 = '$pr6', pr7 = '$pr7', pr8 = '$pr8', pr9 = '$pr9', pr10 = '$pr10', temp_expire = '$tempexpiredate', order_only = '$order_only', is_deleted = '$is_deleted', del_date = '$del_date', receive_kpi = '$receive_kpi', passexpire = date_add(now() , interval 60 day) WHERE loginid = '$employeeid'";
			}
			else {
				$query = "UPDATE login SET lastname = '$lastname', firstname = '$firstname', security_level = '$sec_level', security = '$security', username = '$empl_user', password = '$empl_pass', businessid = '$busid', payroll = '$payroll', busid2 = '$busid2', busid3 = '$busid3', busid4 = '$busid4', busid5 = '$busid5', busid6 = '$busid6', busid7 = '$busid7', busid8 = '$busid8', busid9 = '$busid9', busid10 = '$busid10', email = '$email'
				, pr2 = '$pr2', pr3 = '$pr3', pr4 = '$pr4', pr5 = '$pr5', pr6 = '$pr6', pr7 = '$pr7', pr8 = '$pr8', pr9 = '$pr9', pr10 = '$pr10', temp_expire = '0000-00-00', order_only = '$order_only', is_deleted = '$is_deleted', del_date = '$del_date', receive_kpi = '$receive_kpi', passexpire = date_add(now() , interval 60 day) WHERE loginid = '$employeeid'";
			}
			$result = Treat_DB_ProxyOld::query( $query );
			//mysql_close();
		}

/*
///////////////UPDATE DISTRICT KPI INFO

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query = "SELECT * FROM district ORDER BY companyid DESC, districtname DESC";
		$result = Treat_DB_ProxyOld::query( $query );
		$num = @mysql_numrows( $result );
		//mysql_close();

		$num--;
		while ( $num >= 0 ) {

			$districtid = @mysql_result( $result, $num, 'districtid' );
			$varname="dist$num";
			$getdist = Treat_Controller_Abstract::getPostVariable( $varname, '' );

			if ( $getdist == 0 || $getdist == '' ) {
				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( "Unable to select database");
				$query4 = "DELETE FROM login_district WHERE loginid = '$employeeid' AND districtid = '$districtid'";
				$result4 = Treat_DB_ProxyOld::query( $query4 );
				//mysql_close();
			}
			else {
				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( "Unable to select database");
				$query4 = "SELECT * FROM login_district WHERE loginid = '$employeeid' AND districtid = '$districtid'";
				$result4 = Treat_DB_ProxyOld::query( $query4 );
				$num4 = @mysql_numrows( $result4 );
				//mysql_close();

				if ( $num4 == 0 ) {
					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( "Unable to select database");
					$query4 = "INSERT INTO login_district (loginid,districtid) VALUES ('$employeeid','$districtid')";
					$result4 = Treat_DB_ProxyOld::query( $query4 );
					//mysql_close();
				}
			}

			$num--;

		}
*/
		///////////////UPDATE COMPANIES ALLOWED

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query = "SELECT * FROM company ORDER BY companyname DESC";
		$result = Treat_DB_ProxyOld::query( $query );
		$num = @mysql_numrows( $result );
		//mysql_close();

		$num--;
		while ( $num >= 0 ) {

			$co_id = @mysql_result( $result, $num, 'companyid' );
			$varname = "co$num";
			$getdist = \EE\Controller\Base::getPostVariable( $varname, '' );

			if ( $getdist == 0 || $getdist == '' ) {
				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( "Unable to select database");
				$query4 = "DELETE FROM login_company WHERE loginid = '$employeeid' AND companyid = '$co_id'";
				$result4 = Treat_DB_ProxyOld::query( $query4 );
				//mysql_close();
			}
			else {
				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( "Unable to select database");
				$query4 = "SELECT * FROM login_company WHERE loginid = '$employeeid' AND companyid = '$co_id'";
				$result4 = Treat_DB_ProxyOld::query( $query4 );
				$num4 = @mysql_numrows( $result4 );
				//mysql_close();

				if ( $num4 == 0 ) {
					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( "Unable to select database");
					$query4 = "INSERT INTO login_company (loginid,companyid) VALUES ('$employeeid','$co_id')";
					$result4 = Treat_DB_ProxyOld::query( $query4 );
					//mysql_close();
				}
			}

			$num--;

		}

		//////////////////////////////
		////////NEW LAYOUT////////////
		//////////////////////////////

		$login_all = \EE\Controller\Base::getPostVariable( "login_all", '' );

		$query45 = "SELECT * FROM login_all WHERE loginid = '$employeeid'";
		$result45 = Treat_DB_ProxyOld::query( $query45 );
		$num45 = @mysql_numrows( $result45 );

		if ( $login_all == 1 && $num45 == 0 ) {
			$query45 = "INSERT INTO login_all (loginid,login_all) VALUES ('$employeeid','1')";
			$result45 = Treat_DB_ProxyOld::query( $query45 );
		}
		elseif ( $login_all != 1 ) {
			$query45 = "DELETE FROM login_all WHERE loginid = '$employeeid'";
			$result45 = Treat_DB_ProxyOld::query( $query45 );
		}

		///////////COMPANY TYPE
		$query = "SELECT * FROM company_type ORDER BY type_name DESC";
		$result = Treat_DB_ProxyOld::query( $query );
		$num = @mysql_numrows( $result );

		$num--;
		while ( $num >= 0 ) {
			$typeid = @mysql_result( $result, $num, 'typeid' );
			$type_name = @mysql_result( $result, $num, 'type_name' );

			$query45 = "SELECT * FROM login_company_type WHERE loginid = '$employeeid' AND company_type = '$typeid'";
			$result45 = Treat_DB_ProxyOld::query( $query45 );
			$num45 = @mysql_numrows( $result45 );

			$varname = "co_type$typeid";

			$get_co_type = \EE\Controller\Base::getPostVariable( $varname, '' );

			if ( $get_co_type == 1 && $num45 == 0 ) {
				$query45 = "INSERT INTO login_company_type (loginid,company_type) VALUES ('$employeeid','$typeid')";
				$result45 = Treat_DB_ProxyOld::query( $query45 );
			}
			elseif ( $get_co_type != 1 ) {
				$query45 = "DELETE FROM login_company_type WHERE loginid = '$employeeid' AND company_type = '$typeid'";
				$result45 = Treat_DB_ProxyOld::query( $query45 );
			}

			/////////COMPANIES
			$query2 = "SELECT * FROM company WHERE company_type = '$typeid' ORDER BY companyname DESC";
			$result2 = Treat_DB_ProxyOld::query( $query2 );
			$num2 = @mysql_numrows( $result2 );

			$num2--;
			while ( $num2 >= 0 ) {
				$co_id = @mysql_result( $result2, $num2, 'companyid' );
				$companyname = @mysql_result( $result2, $num2, 'reference' );

				$query45 = "SELECT * FROM login_comp WHERE loginid = '$employeeid' AND companyid = '$co_id'";
				$result45 = Treat_DB_ProxyOld::query( $query45 );
				$num45 = @mysql_numrows( $result45 );

				$varname = "com$co_id";

				$get_comp = \EE\Controller\Base::getPostVariable( $varname, '' );

				if ( $get_comp == 1 && $num45 == 0 ) {
					$query45 = "INSERT INTO login_comp (loginid,companyid) VALUES ('$employeeid','$co_id')";
					$result45 = Treat_DB_ProxyOld::query( $query45 );
				}
				elseif ( $get_comp != 1 ) {
					$query45 = "DELETE FROM login_comp WHERE loginid = '$employeeid' AND companyid = '$co_id'";
					$result45 = Treat_DB_ProxyOld::query( $query45 );
				}

				/////////////DIVISIONS
				$query3 = "SELECT * FROM division WHERE companyid = '$co_id' ORDER BY division_name DESC";
				$result3 = Treat_DB_ProxyOld::query( $query3 );
				$num3 = @mysql_numrows( $result3 );

				$num3--;
				while ( $num3 >= 0 ) {
					$divisionid = @mysql_result( $result3, $num3, 'divisionid' );
					$division_name = @mysql_result( $result3, $num3, 'division_name' );

					$query45 = "SELECT * FROM login_division WHERE loginid = '$employeeid' AND divisionid = '$divisionid'";
					$result45 = Treat_DB_ProxyOld::query( $query45 );
					$num45 = @mysql_numrows( $result45 );

					$varname = "div$divisionid";

					$get_div = \EE\Controller\Base::getPostVariable( $varname, '' );

					if ( $get_div == 1 && $num45 == 0 ) {
						$query45 = "INSERT INTO login_division (loginid,divisionid) VALUES ('$employeeid','$divisionid')";
						$result45 = Treat_DB_ProxyOld::query( $query45 );
					}
					elseif ( $get_div != 1 ) {
						$query45 = "DELETE FROM login_division WHERE loginid = '$employeeid' AND divisionid = '$divisionid'";
						$result45 = Treat_DB_ProxyOld::query( $query45 );
					}

					/////////////DISTRICTS
					$query4 = "SELECT * FROM district WHERE divisionid = '$divisionid' ORDER BY districtname DESC";
					$result4 = Treat_DB_ProxyOld::query( $query4 );
					$num4 = @mysql_numrows( $result4 );

					$num4--;
					while ( $num4 >= 0 ) {
						$districtid = @mysql_result( $result4, $num4, 'districtid' );
						$districtname = @mysql_result( $result4, $num4, 'districtname' );

						$query45 = "SELECT * FROM login_district WHERE loginid = '$employeeid' AND districtid = '$districtid'";
						$result45 = Treat_DB_ProxyOld::query( $query45 );
						$num45 = @mysql_numrows( $result45 );

						$varname = "dist$districtid";

						$get_dist = \EE\Controller\Base::getPostVariable( $varname, '' );

						if ( $get_dist == 1 && $num45 == 0 ) {
							$query45 = "INSERT INTO login_district (loginid,districtid) VALUES ('$employeeid','$districtid')";
							$result45 = Treat_DB_ProxyOld::query( $query45 );
						}
						elseif ( $get_dist != 1 ) {
							$query45 = "DELETE FROM login_district WHERE loginid = '$employeeid' AND districtid = '$districtid'";
							$result45 = Treat_DB_ProxyOld::query( $query45 );
						}

						$num4--;
					}

					$num3--;
				}

				$num2--;
			}

			$num--;
		}

		//////////////////////////////
		/////////END NEW LAYOUT///////
		//////////////////////////////

		$visitormail = "support@treatamerica.com";
		$from = "From: $visitormail\r\n";
		$subject = "BusinessTrack Login Information";

		$message = "$firstname,\n\nYour login information:\nUsername: $empl_user\nPassword: $empl_pass\nLogin at: http://treatamerica.essentialpos.com\n\nSupport";

		if ( $sendemail == 'on' && $is_deleted != 1 ) {
			mail( $email, $subject, $message, $from );
		}

		//////////////EXTENDED FIEDLS

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query = "SELECT * FROM login_extended WHERE loginid = '$employeeid'";
		$result = Treat_DB_ProxyOld::query( $query );
		$num = @mysql_numrows( $result );
		//mysql_close();

		if ( $num == 0 ) {
			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query = "INSERT INTO login_extended (loginid,title,street,city,state,zip,phone,fax) VALUES ('$employeeid','$title','$street','$city','$state','$zip','$phone','$fax')";
			$result = Treat_DB_ProxyOld::query( $query );
			//mysql_close();
		}
		else {
			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query = "UPDATE login_extended SET title='$title',street='$street',city='$city',state='$state',zip='$zip',phone='$phone',fax='$fax' WHERE loginid = '$employeeid'";
			$result = Treat_DB_ProxyOld::query( $query );
			//mysql_close();
		}

		////////////////////////////////////
		/////////DEFINE CUSTOM SECURITY/////
		////////////////////////////////////

		if ( $showcustom == 1 ) {
?>
<html>
	<head>
		<title>Treat America :: saveemployee.php</title>
		<link type="text/css" rel="stylesheet" href="/assets/css/ta/user_security.css" />
	</head>
	<body class="user_security">
		<center>
			<table cellspacing="0" cellpadding="0" border="0" width="90%">
				<tr>
					<td>
						<img src="/assets/images/logo.jpg" alt="logo" />
						<p></p>
					</td>
				</tr>


<?php
/*
* /
?>
				<tr bgcolor="#e8e7e7">
					<td colspan="2">
						<br />
					</td>
				</tr>
<?php
/*
*/
?>
				<tr>
					<td align="center">
<?php
/*
*/
?>
<?php
	$is_custom_security = true;
	include( DOC_ROOT . '/../application/views/ta/user/security.php' );
?>
<?php
/*
*/
?>
					</td>
				</tr>
<?php
/*
*/
?>
			</table>
		</center>
	</body>
</html>
<?php
		}
		else {
			echo "Done";
		}

		$location = "/ta/user_setup.php?bid=$businessid&cid=$companyid";
		if ( $showcustom != 1 ) {
			header( 'Location: ' . $location );
			exit;
		}

		//mysql_close();
	}
}
?>
