<head>

<script language="JavaScript"
   type="text/JavaScript">
function changePage(newLoc)
 {
   nextPage = newLoc.options[newLoc.selectedIndex].value

   if (nextPage != "")
   {
      document.location.href = nextPage
   }
 }
</script>

<SCRIPT LANGUAGE=javascript><!--
function delconfirm(){return confirm('Are you sure you want to delete this item?');}
// --></SCRIPT>

<SCRIPT LANGUAGE=javascript><!--
function loadpic(){return confirm('There is already an image loaded for this invoice. Are you sure you want to load another?');}
// --></SCRIPT>

<SCRIPT LANGUAGE=javascript><!--
function confirmvoid(){return confirm('ARE YOU SURE YOU WANT TO VOID THIS INVOICE?');}
// --></SCRIPT>

<SCRIPT LANGUAGE=javascript><!--
function confirmaccept(){return confirm('ARE YOU SURE YOU WANT TO ACCEPT THIS TRANSFER?');}
// --></SCRIPT>

<SCRIPT LANGUAGE=javascript><!--
function confirmmultiunit(){return confirm('ARE YOU SURE YOU WANT TO SUBMIT THIS DISPERSAL?');}
// --></SCRIPT>

<SCRIPT LANGUAGE=javascript><!--
function confirmdm(){return confirm('Are you sure you want to approve this Purchase Card?');}
// --></SCRIPT>

<SCRIPT LANGUAGE=javascript><!--
function submitconfirm(){return confirm('Are you sure you want to submit?');}
// --></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
function js_date(theString)
{
	ex = theString.split('-');
	return new Date(ex[0],ex[1]-1,ex[2]);
}

function validate(theForm,transfer,val_day)
{
	if (transfer == 4){
		val_day = js_date(val_day);
		set_day = js_date(theForm.date31.value);

		if (theForm.itemname.value == "" || theForm.itemname.value == "Description")
		{
			alert("Please enter a description");
			theForm.itemname.focus();
			return (false);
		}
		if (theForm.item_price.value == "" || theForm.item_price.value == "Price")
		{
			alert("Please enter a price");
			theForm.item_price.focus();
			return (false);
		}
		if (theForm.qty.value == "" || theForm.qty.value == "Qty")
		{
			alert("Please enter a qty");
			theForm.qty.focus();
			return (false);
		}
		if (set_day.getTime() < val_day.getTime())
		{
			alert("Please enter date after " + val_day);
			theForm.date31.focus();
			return (false);
		}

	}
	else if (transfer == 3 || transfer == 1){

		if (theForm.itemname.value == "" || theForm.itemname.value == "Description")
		{
			alert("Please enter a description");
			theForm.itemname.focus();
			return (false);
		}
		if (theForm.amount.value == "")
		{
			alert("Please enter an amount");
			theForm.amount.focus();
			return (false);
		}
	}
	else if (transfer == -1){

		if (theForm.item_note.value == "")
		{
			alert("Please enter a description");
			theForm.item_note.focus();
			return (false);
		}
	}
}
</SCRIPT>


<SCRIPT LANGUAGE="JavaScript" SRC="CalendarPopup.js"></SCRIPT>

<!-- This prints out the default stylehseets used by the DIV style calendar.
     Only needed if you are using the DIV style popup -->
<SCRIPT LANGUAGE="JavaScript">document.write(getCalendarStyles());</SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
<!-- Web Site:  http://dynamicdrive.com -->

<!-- This script and many more are available free online at -->
<!-- The JavaScript Source!! http://javascript.internet.com -->

<!-- Begin
function disableForm(theform) {
if (document.all || document.getElementById) {
for (i = 0; i < theform.length; i++) {
var tempobj = theform.elements[i];
if (tempobj.type.toLowerCase() == "submit" || tempobj.type.toLowerCase() == "reset")
tempobj.disabled = true;
}

}
else {
alert("The form has been submitted.  But, since you're not using IE 4+ or NS 6, the submit button was not disabled on form submission.");
return false;
   }
}
//  End -->
</script>

<STYLE>
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation
			{
			background-color:#6677DD;
			text-align:center;
			vertical-align:center;
			text-decoration:none;
			color:#FFFFFF;
			font-weight:bold;
			}
	.TESTcpDayColumnHeader,
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation,
	.TESTcpCurrentMonthDate,
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDate,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDate,
	.TESTcpCurrentDateDisabled,
	.TESTcpTodayText,
	.TESTcpTodayTextDisabled,
	.TESTcpText
			{
			font-family:arial;
			font-size:8pt;
			}
	TD.TESTcpDayColumnHeader
			{
			text-align:right;
			border:solid thin #6677DD;
			border-width:0 0 1 0;
			}
	.TESTcpCurrentMonthDate,
	.TESTcpOtherMonthDate,
	.TESTcpCurrentDate
			{
			text-align:right;
			text-decoration:none;
			}
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDateDisabled
			{
			color:#D0D0D0;
			text-align:right;
			text-decoration:line-through;
			}
	.TESTcpCurrentMonthDate
			{
			color:#6677DD;
			font-weight:bold;
			}
	.TESTcpCurrentDate
			{
			color: #FFFFFF;
			font-weight:bold;
			}
	.TESTcpOtherMonthDate
			{
			color:#808080;
			}
	TD.TESTcpCurrentDate
			{
			color:#FFFFFF;
			background-color: #6677DD;
			border-width:1;
			border:solid thin #000000;
			}
	TD.TESTcpCurrentDateDisabled
			{
			border-width:1;
			border:solid thin #FFAAAA;
			}
	TD.TESTcpTodayText,
	TD.TESTcpTodayTextDisabled
			{
			border:solid thin #6677DD;
			border-width:1 0 0 0;
			}
	A.TESTcpTodayText,
	SPAN.TESTcpTodayTextDisabled
			{
			height:20px;
			}
	A.TESTcpTodayText
			{
			color:#6677DD;
			font-weight:bold;
			}
	SPAN.TESTcpTodayTextDisabled
			{
			color:#D0D0D0;
			}
	.TESTcpBorder
			{
			border:solid thin #6677DD;
			}
</STYLE>

<script language="JavaScript"
   type="text/JavaScript">
function changePageb(newLoc)
 {
   nextPage = newLoc.value

   if (nextPage != "")
   {
      document.location.href = nextPage
   }
 }
</script>

<script language="JavaScript" type="text/JavaScript" src="javascripts/prototype.js"></script>

<script language="JavaScript" type="text/JavaScript">
        var loadbus = function(){
		var val = $('sel_co').getValue();

		$('sel_bus').update('<option>Loading...</option>');
                $('sel_acct').update('<option></option>');
		new Ajax.Request('ajaxap.php', {parameters: {method: 'ajaxloadap', cid: $GET('cid'), bid: $GET('bid'), val: val}});

	}
        var loadap = function(){
		var val = $('sel_bus').getValue();

		$('sel_acct').update('<option>Loading...</option>');
		new Ajax.Request('ajaxap.php', {parameters: {method: 'ajaxloadap2', cid: $GET('cid'), bid: $GET('bid'), val: val}});

	}

        var showbox = function(select){
                if($F(select)  == 1) $("mult").show();
                else $("mult").hide();
        }

        var updateTotal = function(number){

           var multiplier = new Number(number);
           $('amt').value = new Number(multiplier * new Number($F('mult'))).decRound(2);
        }

</script>

</head>

<?php
$cat_name = "";
$tfr_acct_name = "";

function nextday($nextd,$day_format=''){ //Function returns next day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monn = substr($nextd, 5, 2);
   $dayn = substr($nextd, 8, 2);
   $yearn = substr($nextd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monn, $dayn+1, $yearn));
   return $tempdate;
}

function prevday($prevd,$day_format=''){ //Function returns previous day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monp = substr($prevd, 5, 2);
   $dayp = substr($prevd, 8, 2);
   $yearp = substr($prevd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monp, $dayp-1, $yearp));
   return $tempdate;
}

function money($diff){
		$diff = floatval($diff);
		$diff = number_format($diff, 2, '.', '');
        return $diff;
}

if ( !defined( 'DOC_ROOT' ) ) {
        define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
require_once( dirname(__FILE__) . '/../../application/bootstrap.php' );

$style = "text-decoration:none";

/*mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");*/

/*$user = isset($_COOKIE["usercook"])?$_COOKIE["usercook"]:'';
$pass = isset($_COOKIE["passcook"])?$_COOKIE["passcook"]:'';*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook','');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook','');

/*$businessid = isset($_POST["businessid"])?$_POST["businessid"]:'';
$companyid = isset($_POST["companyid"])?$_POST["companyid"]:'';
$vendorid = isset($_POST["vendor"])?$_POST["vendor"]:'';
$invoicenum = isset($_POST["invoicenum"])?$_POST["invoicenum"]:'';*/

$businessid = \EE\Controller\Base::getPostVariable('businessid','');
$companyid = \EE\Controller\Base::getPostVariable('companyid','');
$vendorid = \EE\Controller\Base::getPostVariable('vendor','');
$invoicenum = \EE\Controller\Base::getPostVariable('invoicenum','');

$invoicenum=strtoupper($invoicenum);
$invoicenum=str_replace(" ","",$invoicenum);
$invoicenum=str_replace(",",".",$invoicenum);

/*$date = isset($_POST["date3"])?$_POST["date3"]:'';
$new = isset($_POST["new"])?$_POST["new"]:'';
$allitems = isset($_POST["allitems"])?$_POST["allitems"]:'';
$notes = isset($_POST["notes"])?$_POST["notes"]:'';*/

$date = \EE\Controller\Base::getPostVariable('date3','');
$new = \EE\Controller\Base::getPostVariable('new','');
$allitems = \EE\Controller\Base::getPostVariable('allitems','');
$notes = \EE\Controller\Base::getPostVariable('notes','');

$notes=str_replace("'","`",$notes);

/*$inputtotal = isset($_POST["inputtotal"])?$_POST["inputtotal"]:'';
$apinvoiceid = isset($_POST["apinvoiceid"])?$_POST["apinvoiceid"]:'';
$transfer = isset($_POST["transfer"])?$_POST["transfer"]:'';
$interco = isset($_POST["interco2"])?$_POST["interco2"]:'';*/

$inputtotal = \EE\Controller\Base::getPostVariable('inputtotal','');
$apinvoiceid = \EE\Controller\Base::getPostVariable('apinvoiceid','');
$transfer = \EE\Controller\Base::getPostVariable('transfer','');
$interco = \EE\Controller\Base::getPostVariable('interco2','');

$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$pc_type="";
$goto = "";
$submitdate = "";
$amount_disable = "";
$val_day = "";
$taxtotal = "";
$freighttotal = "";
$showuser = "";

if ($transfer==2){
   $pieces = explode(";", $vendor);
   $vendorid=$pieces[0];
   $pc_type=$pieces[1];
}

$inventory_type = 0;

if ($companyid=="" && $businessid==""){
   /*$allitems = isset($_GET["allitems"])?$_GET["allitems"]:'';
   $businessid = isset($_GET["bid"])?$_GET["bid"]:'';
   $companyid = isset($_GET["cid"])?$_GET["cid"]:'';
   $apinvoiceid = isset($_GET["apinvoiceid"])?$_GET["apinvoiceid"]:'';
   $goto = isset($_GET["goto"])?$_GET["goto"]:'';
   $load = isset($_GET["load"])?$_GET["load"]:'';
   $newitemid = isset($_GET["inv_itemid"])?$_GET["inv_itemid"]:'';
   $interco_trans = isset($_GET["interco_trans"])?$_GET["interco_trans"]:'';
   $transco_id = isset($_GET["transco_id"])?$_GET["transco_id"]:'';
   $transco_bus1 = isset($_GET["transco_bus1"])?$_GET["transco_bus1"]:'';
   $transco_bus2 = isset($_GET["transco_bus2"])?$_GET["transco_bus2"]:'';
   $interco = isset($_GET["interco2"])?$_GET["interco2"]:'';*/
	
	$allitems = \EE\Controller\Base::getGetVariable('allitems','');
	$businessid = \EE\Controller\Base::getGetVariable('bid','');
	$companyid = \EE\Controller\Base::getGetVariable('cid','');
	$apinvoiceid = \EE\Controller\Base::getGetVariable('apinvoiceid','');
	$goto = \EE\Controller\Base::getGetVariable('goto','');
	$load = \EE\Controller\Base::getGetVariable('load','');
	$newitemid = \EE\Controller\Base::getGetVariable('inv_itemid','');
	$interco_trans = \EE\Controller\Base::getGetVariable('interco_trans','');
	$transco_id = \EE\Controller\Base::getGetVariable('transco_id','');
	$transco_bus1 = \EE\Controller\Base::getGetVariable('transco_bus1','');
	$transco_bus2 = \EE\Controller\Base::getGetVariable('transco_bus2','');
	$interco = \EE\Controller\Base::getGetVariable('interco2','');
}

$inventory_type_query = "select b1.businessid, b1.businessname, b2.inventory_type from business b1 left join business_setting b2 on b1.businessid = b2.business_id where b1.businessid = $businessid";
$inventory_type_result = Treat_DB_ProxyOld::query($inventory_type_query);
$inventory_type = Treat_DB_ProxyOld::mysql_result($inventory_type_result,0,"inventory_type");

if($interco_trans==1 && ($transco_id<1||$transco_bus1<1||$transco_bus2<1)){$showdisable="disabled";}
else{$showdisable="";}

if ($new=="yes" && $transfer == "1"){
   //mysql_connect($dbhost,$username,$password);
   //@mysql_select_db($database) or die( "Unable to select database");
   $query = "SELECT unit,transfer FROM business WHERE businessid = '$businessid'";
   $result = Treat_DB_ProxyOld::query($query);
   //mysql_close();

   $transfer_num=@Treat_DB_ProxyOld::mysql_result($result,0,"transfer");
   $bus_unit=@Treat_DB_ProxyOld::mysql_result($result,0,"unit");
   $newnum=$transfer_num+1;

   //mysql_connect($dbhost,$username,$password);
   //@mysql_select_db($database) or die( "Unable to select database");
   $query = "UPDATE business SET transfer = '$newnum' WHERE businessid = '$businessid'";
   $result = Treat_DB_ProxyOld::query($query);
   //mysql_close();

   //$invoicenum="TFR-$businessid$transfer_num";
   $invoicenum="TFR-".$businessid."-".$transfer_num;
}
elseif ($new=="yes" && $transfer == "2"){
   //mysql_connect($dbhost,$username,$password);
   //@mysql_select_db($database) or die( "Unable to select database");
   $query = "SELECT transfer FROM business WHERE businessid = '$businessid'";
   $result = Treat_DB_ProxyOld::query($query);
   //mysql_close();

   $transfer_num=@Treat_DB_ProxyOld::mysql_result($result,0,"transfer");
   $newnum=$transfer_num+1;

   //mysql_connect($dbhost,$username,$password);
   //@mysql_select_db($database) or die( "Unable to select database");
   $query = "UPDATE business SET transfer = '$newnum' WHERE businessid = '$businessid'";
   $result = Treat_DB_ProxyOld::query($query);
   //mysql_close();

   $invoicenum="PUR-$businessid$transfer_num";
}
elseif ($new=="yes" && $transfer == "3"){
   //mysql_connect($dbhost,$username,$password);
   //@mysql_select_db($database) or die( "Unable to select database");
   $query = "SELECT transfer FROM business WHERE businessid = '$businessid'";
   $result = Treat_DB_ProxyOld::query($query);
   //mysql_close();

   $transfer_num=@Treat_DB_ProxyOld::mysql_result($result,0,"transfer");
   $newnum=$transfer_num+1;

   //mysql_connect($dbhost,$username,$password);
   //@mysql_select_db($database) or die( "Unable to select database");
   $query = "UPDATE business SET transfer = '$newnum' WHERE businessid = '$businessid'";
   $result = Treat_DB_ProxyOld::query($query);
   //mysql_close();

   $invoicenum="EXP-$businessid$transfer_num";
}
elseif ($new=="yes" && $transfer == "4"){

   $query = "SELECT po_num,po_prefix,unit FROM business WHERE businessid = '$businessid'";
   $result = Treat_DB_ProxyOld::query($query);

   $bus_unit=@Treat_DB_ProxyOld::mysql_result($result,0,"unit");
   $po_prefix=@Treat_DB_ProxyOld::mysql_result($result,0,"po_prefix");
   $transfer_num=@Treat_DB_ProxyOld::mysql_result($result,0,"po_num");
   $newnum=$transfer_num+1;

   $query = "UPDATE business SET po_num = '$newnum' WHERE businessid = '$businessid'";
   $result = Treat_DB_ProxyOld::query($query);

   	$invoicenum="$po_prefix-$transfer_num";

   //$invoicenum=date("dHGs");
   //$invoicenum.="$businessid";
}


$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=Treat_DB_ProxyOld::mysql_numrows($result);

$security_level=@Treat_DB_ProxyOld::mysql_result($result,0,"security_level");
$mysecurity=@Treat_DB_ProxyOld::mysql_result($result,0,"security");

////NEW SECURITY
    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query56 = "SELECT * FROM security WHERE securityid = '$mysecurity'";
    $result56 = Treat_DB_ProxyOld::query($query56);
    //mysql_close();

    $security_level=@Treat_DB_ProxyOld::mysql_result($result56,0,"bus_payable");

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query4 = "SELECT master FROM vendors WHERE vendorid = '$vendorid'";
$result4 = Treat_DB_ProxyOld::query($query4);
$num4=Treat_DB_ProxyOld::mysql_numrows($result);
//mysql_close();
if($num4!=0){$master=@mysql_result($result,0,"master");}

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query2 = "SELECT * FROM apinvoice WHERE invoicenum = '$invoicenum' AND master_vendor_id='$master'";
$result2 = Treat_DB_ProxyOld::query($query2, true);
$num2=Treat_DB_ProxyOld::mysql_numrows($result2);
//mysql_close();

if ($num != 1 || $user == "" || $pass == "" || ($new == "yes" && $invoicenum == "" ))
{
    echo "<center><h3>Please enter an Invoice Number or Valid Vendor</h3>Use your browser's back button to try again.</center>";
}
else if ($num2!=0 AND $new == "yes")
{
    echo "<center><h3>That invoice number has already been used.</h3>Use your browser's back button to try again.</center>";
}
else
{

    $day9=substr($today,8,2);
    $month9=substr($today,5,2);
    $year9=substr($today,0,4);

    if ($day9<=5){
       $month9--;
       if ($month9==0){$month9=12;$year9--;}
       if ($month9<10){$month9="0$month9";}
    }
    $lastdate="$year9-$month9-01";


    if ($new=="yes" && $date <= $lastdate && $security_level != 9){$date=$lastdate;}

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query3 = "SELECT * FROM apinvoice WHERE invoicenum = '$invoicenum' AND vendor = '$vendorid' AND apinvoiceid != '$apinvoiceid'";
    $result3 = Treat_DB_ProxyOld::query($query3, true);
    $num3=Treat_DB_ProxyOld::mysql_numrows($result3);
    //mysql_close();

//////////////////////////////////////////////CREATE/UPDATE AP INVOICE

    if ($new=="yes" && $invoicenum != "" && ($date >= $lastdate || $security_level == 9) && $num3==0 && $vendorid > 0){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT master FROM vendors WHERE vendorid = '$vendorid'";
       $result = Treat_DB_ProxyOld::query($query);
       //mysql_close();
       $master=@Treat_DB_ProxyOld::mysql_result($result,0,"master");
       if ($transfer==1||$transfer==2||$transfer==3){$master=0;}

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM apinvoice WHERE invoicenum LIKE '$invoicenum' AND master_vendor_id = '$master'";
       $result = Treat_DB_ProxyOld::query($query, true);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();

       if ($num==0){
          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query = "INSERT INTO apinvoice (invoicenum,businessid,companyid,date,vendor,invtotal,master_vendor_id,transfer,submitby,pc_type,interco_trans) VALUES ('$invoicenum','$businessid','$companyid','$date','$vendorid','$inputtotal','$master','$transfer','$user','$pc_type','$interco')";
          $result = Treat_DB_ProxyOld::query($query);
          $apinvoiceid = Treat_DB_ProxyOld::mysql_insert_id();
          //mysql_close();
       }
    }
    elseif ($new=="update" && ($date >= $lastdate || $security_level == 9) && $invoicenum != "" && $num3 == 0){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT master FROM vendors WHERE vendorid = '$vendorid'";
       $result = Treat_DB_ProxyOld::query($query);
       //mysql_close();
       $master=@Treat_DB_ProxyOld::mysql_result($result,0,"master");

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM apinvoice WHERE invoicenum = '$invoicenum' AND master_vendor_id = '$master' AND apinvoiceid != '$apinvoiceid'";
       $result = Treat_DB_ProxyOld::query($query, true);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();

      if ($num==0){
       $query = "UPDATE apinvoice SET invoicenum = '$invoicenum', date = '$date', vendor = '$vendorid', invtotal = '$inputtotal', master_vendor_id = '$master', notes = '$notes', submitby = '$user' WHERE apinvoiceid = '$apinvoiceid'";
       $result = Treat_DB_ProxyOld::query($query);

       $query3 = "SELECT SUM(amount) AS totalamt FROM apinvoicedetail WHERE apinvoiceid = '$apinvoiceid'";
       $result3 = Treat_DB_ProxyOld::query($query3, true);
       $num3=Treat_DB_ProxyOld::mysql_numrows($result3);

	   $total=@Treat_DB_ProxyOld::mysql_result($result3,0,"totalamt");

       $query = "UPDATE apinvoice SET total = '$total' WHERE apinvoiceid = '$apinvoiceid'";
       $result = Treat_DB_ProxyOld::query($query);
      }
    }
    else{
    }

/////////////////////////////////////////////
    if ($new=="yes"){

       $query = "SELECT * FROM apinvoice WHERE apinvoiceid = '$apinvoiceid'";
       $result = Treat_DB_ProxyOld::query($query, true);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);

      if ($num!=0){
       //$apinvoiceid=@Treat_DB_ProxyOld::mysql_result($result,0,"apinvoiceid");
       $busid2=@Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
       $invoicenum=@Treat_DB_ProxyOld::mysql_result($result,0,"invoicenum");
       $date=@Treat_DB_ProxyOld::mysql_result($result,0,"date");
       $vendorid=@Treat_DB_ProxyOld::mysql_result($result,0,"vendor");
       $invtotal=@Treat_DB_ProxyOld::mysql_result($result,0,"total");
       $posted=@Treat_DB_ProxyOld::mysql_result($result,0,"posted");
       $export=@Treat_DB_ProxyOld::mysql_result($result,0,"export");
       $inputtotal=@Treat_DB_ProxyOld::mysql_result($result,0,"invtotal");
       $notes=@Treat_DB_ProxyOld::mysql_result($result,0,"notes");
       $vendor_master=@Treat_DB_ProxyOld::mysql_result($result,0,"master_vendor_id");
       $transfer=@Treat_DB_ProxyOld::mysql_result($result,0,"transfer");
       $pc_type=@Treat_DB_ProxyOld::mysql_result($result,0,"pc_type");
       $interco=@Treat_DB_ProxyOld::mysql_result($result,0,"interco_trans");
       $accept_by=@Treat_DB_ProxyOld::mysql_result($result,0,"accept_by");
       $dm_approve=@Treat_DB_ProxyOld::mysql_result($result,0,"dm");
       if ($interco==-1){$showinterco="CHECKED";$interco3=0;}
       else{$interco3=1;}
      }
    }
    else {
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM apinvoice WHERE apinvoiceid = '$apinvoiceid'";
       $result = Treat_DB_ProxyOld::query($query, true);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();

      if ($num!=0){
       $apinvoiceid=@Treat_DB_ProxyOld::mysql_result($result,0,"apinvoiceid");
       $busid2=@Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
       $invoicenum=@Treat_DB_ProxyOld::mysql_result($result,0,"invoicenum");
       $date=@Treat_DB_ProxyOld::mysql_result($result,0,"date");
       $vendorid=@Treat_DB_ProxyOld::mysql_result($result,0,"vendor");
       $invtotal=@Treat_DB_ProxyOld::mysql_result($result,0,"total");
       $posted=@Treat_DB_ProxyOld::mysql_result($result,0,"posted");
       $export=@Treat_DB_ProxyOld::mysql_result($result,0,"export");
       $image=@Treat_DB_ProxyOld::mysql_result($result,0,"image");
       $inputtotal=@Treat_DB_ProxyOld::mysql_result($result,0,"invtotal");
       $notes=@Treat_DB_ProxyOld::mysql_result($result,0,"notes");
       $vendor_master=@Treat_DB_ProxyOld::mysql_result($result,0,"master_vendor_id");
       $transfer=@Treat_DB_ProxyOld::mysql_result($result,0,"transfer");
       $submitdate=@Treat_DB_ProxyOld::mysql_result($result,0,"submitdate");
       $submitby=@Treat_DB_ProxyOld::mysql_result($result,0,"submitby");
       $showuser=$submitby;
       $pc_type=@Treat_DB_ProxyOld::mysql_result($result,0,"pc_type");
       $interco=@Treat_DB_ProxyOld::mysql_result($result,0,"interco_trans");
       $accept_by=@Treat_DB_ProxyOld::mysql_result($result,0,"accept_by");
       $dm_approve=@Treat_DB_ProxyOld::mysql_result($result,0,"dm");
       if ($interco==-1){$showinterco="CHECKED";$interco3=0;}
       else{$interco3=1;}
      }
    }

    if($interco==-2){echo "<body onLoad='setTimeout(function(){multiform.multi_co.focus();},50)'>";}
    else {echo "<body onLoad='newitem.apaccountid.focus()'>";}

    echo "<div style=border:50px solid red;padding:10px;>";
    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%><tr><td colspan=2>";
    echo "<a href=businesstrack.php><img src=logo.jpg border=0 height=43 width=205></a><p></td></tr></table>";

    echo "<center><table cellspacing=0 cellpadding=0 width=90% style=\"border:2px solid #CCCCCC;\"><tr><td>";

    if ($apinvoiceid=="" || $apinvoiceid==0){
      echo "<center><h3>Invalid Vendor or Invoice number already used.</h3></center>";
    }
    else{

    if ($vendorid==$businessid&&$transfer==1){$inputtotal=$inputtotal*-1;$invtotal=$invtotal*-1;}
    $inputtotal=money($inputtotal);

    $formnum=0;

    if ($transfer==0){echo "<center><table bgcolor=#E8E7E7 width=100% ><tr><td><center><h3>A/P INVOICE</h3></td></tr><tr><td>";}
    elseif ($transfer==1){echo "<center><table bgcolor=#E8E7E7 width=100% ><tr><td><center><h3>TRANSFER</h3></td></tr><tr><td>";}
    elseif ($transfer==2){
       echo "<center><table bgcolor=#E8E7E7 width=100%><tr><td><center><h3>PURCHASE CARD RECEIPT</h3></td></tr>";

       echo "<tr><td>";
    }
    elseif ($transfer==3){
	   echo "<center><table bgcolor=#E8E7E7 width=100% ><tr><td><center><h3>EXPENSE REPORT</h3></td></tr>";

       /////////////////DM APPROVAL
       if($transfer==3){
          if(($dm_approve==""||$dm_approve==-1) && $security_level>=9 && $transfer == 3){
             echo "<tr><td align=right><form action=dm_approve.php?apinvoiceid=$apinvoiceid&goto=$goto&bid=$businessid&cid=$companyid method=post style=\"margin:0;padding:0;display:inline;\"><input type=submit value='Approve' style=\"border: 1px solid #CCCCCC; font-size: 12px; background-color:#FFFF99;\" onclick='return confirmdm()' onMouseOver=this.style.color='#FF9900';this.style.cursor='hand' onMouseOut=this.style.color='#000000'></form></td></tr>";
             $formnum=1;
          }
          elseif($dm_approve!=""&&$dm_approve!=-1){
             echo "<tr><td align=right><form action=dm_approve.php?apinvoiceid=$apinvoiceid&goto=$goto method=post style=\"margin:0;padding:0;display:inline;\"><input type=button value='Approved by $dm_approve' style=\"border: 1px solid #CCCCCC; font-size: 12px; background-color:#00FF00;\"></form></td></tr>";
             $formnum=1;
          }
       }
       ////////////////////////////

       echo "<tr><td>";
    }
	elseif ($transfer==4){
       echo "<center><table bgcolor=#E8E7E7 width=100%><tr><td><center><h3>PURCHASE ORDER</h3></td></tr>";

	   if($dm_approve!=""&&$dm_approve!=-1){
             echo "<tr><td align=right><form action=dm_approve.php?apinvoiceid=$apinvoiceid&goto=$goto method=post style=\"margin:0;padding:0;display:inline;\"><input type=button value='Approved by $dm_approve' style=\"border: 1px solid #CCCCCC; font-size: 12px; background-color:#00FF00;\"></form></td></tr>";
             $formnum=1;
       }

       echo "<tr><td>";
    }

    echo "<table>";
    echo "<tr><td colspan=2>";
    echo "<form action=apinvoice.php method=post style=\"margin:0;padding:0;display:inline;\">";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<input type=hidden name=businessid value=$businessid>";
    echo "<input type=hidden name=companyid value=$companyid>";
    echo "<input type=hidden name=allitems value=$allitems>";
    echo "<input type=hidden name=new value='update'>";
    echo "<input type=hidden name=goto value=$goto>";
    echo "<input type=hidden name=apinvoiceid value='$apinvoiceid'>";
    echo "</td></tr>";

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query2 = "SELECT businessname FROM business WHERE businessid = '$busid2'";
             $result2 = Treat_DB_ProxyOld::query($query2);
             //mysql_close();
             $vendorname2=@Treat_DB_ProxyOld::mysql_result($result2,0,"businessname");

    $showfrom="<b>Sender:</b> $vendorname2";

    if ($submitdate!="0000-00-00"&&$submitdate!=""){$submitdate="<font color=red>Submitted on $submitdate $submitby</font>";}
    else{$submitdate="";}

    if ($invtotal!=$inputtotal){$fontcolor="red";}
    else{$fontcolor="black";}
    if ($invtotal!=$inputtotal){echo "<tr><td colspan=2><font color=red>Your Totals Don't Match</font></td></tr>";}
    if ($export==1){echo "<tr><td><font color=red>EXPORTED</font></td><td></td></tr>";}
	elseif ($posted==1&&$transfer==4){echo "<tr><td><font color=red>ACCEPTED</font></td><td></td></tr>";}
    elseif ($posted==1){echo "<tr><td><font color=red>SUBMITTED</font></td><td></td></tr>";}
    elseif ($posted==2){echo "<tr><td><font color=red>PENDING</font></td><td></td></tr>";}
	elseif ($posted==3&&$transfer==4){echo "<tr><td><font color=red>REJECTED</font></td><td></td></tr>";}
    if ($transfer==0){echo "<tr><td align=right><b>Invoice#: </td><td><input type=text name=invoicenum maxlength=10 value='$invoicenum' size=15></td></tr>";}
    elseif ($transfer==1){echo "<tr><td align=right><b>Transfer#: </td><td><input type=text name=invoicenum maxlength=10 value='$invoicenum' size=15></td></tr>";}
    elseif ($transfer==2){echo "<tr><td align=right><b>PURCHASE#: </td><td><input type=text name=invoicenum maxlength=10 value='$invoicenum' size=15></td></tr>";}
    elseif ($transfer==3){echo "<tr><td align=right><b>EXPENSE#: </td><td><input type=text name=invoicenum maxlength=10 value='$invoicenum' size=15></td></tr>";}
	elseif ($transfer==4){echo "<tr><td align=right><b>PURCHASE ORDER#: </td><td><input type=text name=invoicenum maxlength=10 value='$invoicenum' size=15></td></tr>";}
    echo "<tr><td align=right><b>Date: </td><td><SCRIPT LANGUAGE='JavaScript' ID='js19'> var cal19 = new CalendarPopup('testdiv1');cal19.setCssPrefix('TEST');</SCRIPT> <A HREF=# onClick=cal19.select(document.forms[$formnum].date3,'anchor19','yyyy-MM-dd'); return false; TITLE=cal19.select(document.forms[$formnum].date3,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor19' ID='anchor19'><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A> <INPUT TYPE=text NAME=date3 VALUE='$date' SIZE=8> $submitdate</td></tr>";

  if ($transfer==0||$transfer==4){
    echo "<tr><td align=right><b>Vendor: </td><td><select name=vendor>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query3 = "SELECT * FROM vendors WHERE businessid = '$businessid' AND ((is_deleted = '0') OR (is_deleted = '1' AND del_date >= '$date')) ORDER BY name DESC";
    $result3 = Treat_DB_ProxyOld::query($query3);
    $num3=Treat_DB_ProxyOld::mysql_numrows($result3);
    //mysql_close();

    $num3--;

    while ($num3>=0){
       $vendorname=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"name");
       $vendornum=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"vendorid");
       if ($vendornum==$vendorid){$c1="SELECTED";}
       else {$c1="";}
       echo "<option value=$vendornum $c1>$vendorname</option>";
       $num3--;
    }
    echo "</select></td></tr>";
  }
  elseif ($transfer==1){
    if($interco==-1){$findinterco=1;}
    else{$findinterco=0;}

    echo "<tr><td align=right><b>Receiver: </td><td><select name=vendor>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    if ($vendorid==$businessid){
       $query3 = "SELECT * FROM business WHERE companyid = '$companyid' AND businessid = '$businessid'";
    }
    else{
       $query3 = "SELECT * FROM business WHERE companyid != '2' AND businessid != '$businessid' AND exp_trans = '0' AND restrict_transfer_to = '0' ORDER BY businessname DESC";
    }
    $result3 = Treat_DB_ProxyOld::query($query3);
    $num3=Treat_DB_ProxyOld::mysql_numrows($result3);
    //mysql_close();

    //$num3--;

    $transerror="<img src=error.gif height=16 width=20 alt='ERROR - Please Choose a Unit and Save'>";
    while ($num3>=0){
       $busname=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"businessname");
       $vendornum=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"businessid");
       if ($vendornum==$vendorid&&$vendorid>0){$c1="SELECTED";$transerror="";}
       else {$c1="";}
       echo "<option value=$vendornum $c1>$busname</option>";
       $num3--;
    }
    echo "</select> $transerror $showfrom</td></tr>";
  }
  elseif ($transfer==2||$transfer==3){

    echo "<tr><td align=right><b>Employee: </td><td><select name=vendor>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query3 = "SELECT * FROM purchase_card WHERE businessid = '$businessid' AND ((is_deleted = '0') OR (is_deleted = '1' AND del_date >= '$date'))";
    $result3 = Treat_DB_ProxyOld::query($query3);
    $num3=Treat_DB_ProxyOld::mysql_numrows($result3);
    //mysql_close();

    $num3--;

    while ($num3>=0){

       $loginid=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"loginid");
       $type=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"type");
       $has_vendor=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"vendorid");
       $cash_only2=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"cash_only");

       if ($type==0){
          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query4 = "SELECT firstname,lastname FROM login WHERE loginid = '$loginid'";
          $result4 = Treat_DB_ProxyOld::query($query4);
          //mysql_close();
       }
       elseif($type==1){
          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query4 = "SELECT firstname,lastname FROM login_route WHERE login_routeid = '$loginid'";
          $result4 = Treat_DB_ProxyOld::query($query4);
          //mysql_close();
       }
       $firstname=@Treat_DB_ProxyOld::mysql_result($result4,0,"firstname");
       $lastname=@Treat_DB_ProxyOld::mysql_result($result4,0,"lastname");

       if ($loginid==$vendorid&&$type==$pc_type){$cash_only=$cash_only2; $c1="SELECTED";if($has_vendor>0){$my_vendor=1;}}
       else {$c1="";}
       echo "<option value='$loginid;$type' $c1>$lastname, $firstname</option>";
       $num3--;
    }
    echo "</select></td></tr>";
  }

    echo "<tr><td align=right><b><font color=$fontcolor>Total: </font></td><td><input type=text name=inputtotal value='$inputtotal' size=10></td></tr>";

    if (($transfer==1||$transfer==2||$transfer==3)&&$notes==""){$shownotes="<br><font size=2 color=red><i>*Details Required Before Submitting</i></font>";$showast="<font color=red>*</font>";}
    else{$shownotes="";$showast="";}

    echo "<tr><td align=right valign=top><b>Notes$showast:</b></td><td><textarea name=notes cols=60 rows=10>$notes</textarea>$shownotes</td></tr>";

    if (($posted!=1&&$posted!=2)||$security_level==9){echo "<tr><td align=right><input type=hidden name=goto value=$goto><input type=submit value='Save'></form></td>"; $formnum++;}
    else{echo "<tr><td align=right></form></td>"; $formnum++;}

    if ($transfer>0&&$transfer<3&&$notes==""){$noreturn=1;}
    else{$noreturn=0;}

    if ($invtotal==$inputtotal&&$goto==1&&$notreturn==0){echo "<td><form action=comapinvoice.php?cid=$companyid&bid=$businessid method=post style=\"margin:0;padding:0;display:inline;\"><input type=submit value='Return'></form></td>"; $formnum++;}
    elseif ($invtotal==$inputtotal&&$goto==2&&$noreturn==0){echo "<td><form action=comtransfer.php?cid=$companyid&bid=$businessid method=post style=\"margin:0;padding:0;display:inline;\"><input type=submit value='Return'></form></td>"; $formnum++;}
    elseif ($invtotal==$inputtotal&&$goto==3&&$noreturn==0){echo "<td><form action=busapinvoice2.php?cid=$companyid&bid=$businessid method=post style=\"margin:0;padding:0;display:inline;\"><input type=submit value='Return'></form></td>"; $formnum++;}
	elseif ($goto==4){echo "<td><form action=busapinvoice3.php?cid=$companyid&bid=$businessid method=post style=\"margin:0;padding:0;display:inline;\"><input type=submit value='Return'></form></td>"; $formnum++;}
    elseif ($invtotal==$inputtotal&&$noreturn==0){echo "<td><form action=busapinvoice.php?cid=$companyid&bid=$businessid method=post style=\"margin:0;padding:0;display:inline;\"><input type=submit value='Return'></form></td>"; $formnum++;}
    else {echo "<td></td>";}

    if ($posted==0||($posted==2&&$security_level>8)){echo "<td><form action=delapinv.php method=POST onclick='return confirmvoid()' style=\"margin:0;padding:0;display:inline;\"><input type=hidden name=companyid value='$companyid'><input type=hidden name=businessid value='$businessid'><input type=hidden name=apinvoiceid value='$apinvoiceid'><input type=hidden name=goto value='$goto'><input type=submit value='VOID'></form></td></tr>"; $formnum++;}
    else {echo "<td></td></tr>";}

    echo "</table></td></tr>";
///////////////////////List ITems/////////////////////
    if ($transfer==1){$allitems=1;}

    echo "<center><table bgcolor=#E8E7E7 width=100% cellspacing=0 cellpadding=0 style=\"font-size: 12px;\">";
    if ($transfer==0){echo "<tr><td colspan=7><center><h3>INVOICE ITEMS<a name='additems'></a></h3></td></tr>";}
    elseif ($transfer==1){echo "<tr><td colspan=7><center><h3>TRANSFER ITEMS<a name='additems'></a></h3></td></tr>";}
    elseif ($transfer==2){echo "<tr><td colspan=7><center><h3>PURCHASE CARD ITEMS<a name='additems'></a></h3></td></tr>";}
    elseif ($transfer==3){echo "<tr><td colspan=7><center><h3>EXPENSE REPORT ITEMS<a name='additems'></a></h3></td></tr>";}
	elseif ($transfer==4){echo "<tr><td colspan=9><center><h3>PURCHASE ORDER ITEMS<a name='additems'></a></h3></td></tr>";}

	//////show budget status for purchase orders
	if($transfer==4 && $posted!=1){

		///////get unit number
		$query72 = "SELECT unit FROM business WHERE businessid = '$businessid'";
		$result72 = Treat_DB_ProxyOld::query($query72);

		$bus_unit=@Treat_DB_ProxyOld::mysql_result($result72,0,"unit");

		///////budget
		$query72 = "SELECT date,categoryid FROM apinvoicedetail WHERE apinvoiceid = '$apinvoiceid' ORDER BY date";
		$result72 = Treat_DB_ProxyOld::query($query72, true);

		$lastdate="0000-00-00";
		while($r72=Treat_DB_ProxyOld::mysql_fetch_array($result72)){

		$finddate=$r72["date"];
		$findcat=$r72["category"];

		if(substr($lastdate,5,2) != substr($finddate,5,2)){

		$budget_year=substr($finddate,0,4);
		$budget_month=substr($finddate,5,2);
		$b_date1="$budget_year-$budget_month-01";
		$b_date2="$budget_year-$budget_month-31";

		if($budget_month==1){$monthname="January";}
		elseif($budget_month==2){$monthname="February";}
		elseif($budget_month==3){$monthname="March";}
		elseif($budget_month==4){$monthname="April";}
		elseif($budget_month==5){$monthname="May";}
		elseif($budget_month==6){$monthname="June";}
		elseif($budget_month==7){$monthname="July";}
		elseif($budget_month==8){$monthname="August";}
		elseif($budget_month==9){$monthname="September";}
		elseif($budget_month==10){$monthname="October";}
		elseif($budget_month==11){$monthname="November";}
		elseif($budget_month==12){$monthname="December";}

		$query1 = "SELECT * FROM po_category ORDER BY category_name";
		$result1 = Treat_DB_ProxyOld::query($query1);

		while($r=Treat_DB_ProxyOld::mysql_fetch_array($result1)){
			$categoryid = $r["categoryid"];
			$category_name = $r["category_name"];
			$budget_type = $r["budget_type"];

			/////////if category exists for this purchase order
			$query2 = "SELECT categoryid FROM apinvoicedetail WHERE apinvoiceid = '$apinvoiceid' AND categoryid = '$categoryid' AND date >= '$b_date1' AND date <= '$b_date2'";
			$result2 = Treat_DB_ProxyOld::query($query2, true);
			$num_cat=Treat_DB_ProxyOld::mysql_numrows($result2);

			if($num_cat>0){
			//////totals for month
			$b_date1="$budget_year-$budget_month-01";
			$b_date2="$budget_year-$budget_month-31";
			$query2 = "SELECT SUM(apinvoicedetail.amount) AS totalamt FROM apinvoice,apinvoicedetail WHERE apinvoice.businessid = '$businessid' AND apinvoice.transfer = '4' AND (apinvoice.posted = '1' OR apinvoice.apinvoiceid = '$apinvoiceid') AND apinvoice.apinvoiceid = apinvoicedetail.apinvoiceid AND apinvoicedetail.categoryid = '$categoryid' AND apinvoicedetail.date >= '$b_date1' AND apinvoicedetail.date <= '$b_date2'";
			$result2 = Treat_DB_ProxyOld::query($query2, true);

			$totalamt=@Treat_DB_ProxyOld::mysql_result($result2,0,"totalamt");

			if(strlen($default_month)<2){$default_month="0$default_month";}
			$query2 = "SELECT `$budget_month` AS budgetamt FROM budget WHERE businessid = '$bus_unit' AND companyid = '$companyid' AND type = '$budget_type' AND year = '$budget_year'";
			$result2 = Treat_DB_ProxyOld::query($query2);

			$budgetamt=@Treat_DB_ProxyOld::mysql_result($result2,0,"budgetamt");

			if($budgetamt<$totalamt){$shownumber=number_format($totalamt-$budgetamt,2); $shownumber="<font color=red>+$$shownumber</font>"; echo "<tr><td></td><td colspan=5 style=\"border:2px solid red;\" bgcolor=white>&nbsp;<img src=restrict.gif height=16 width=16> <b>This Purchase Order will exceed your $monthname $category_name budget! $shownumber</b></td><td></td></tr><tr height=10><td colspan=7></td></tr>";}
			elseif($totalamt==$budgetamt){echo "<tr><td></td><td colspan=5 style=\"border:2px solid red;\" bgcolor=white>&nbsp;<img src=error.gif height=16 width=20> <b>This Purchase Order will exhaust your $monthname $category_name budget!</b></td><td></td></tr><tr height=10><td colspan=7></td></tr>";}
			elseif($totalamt>=($budgetamt-($budgetamt*.05))){$shownumber=number_format($budgetamt-$totalamt,2); $shownumber="<font color=green>$$shownumber</font>"; echo "<tr><td></td><td colspan=5 style=\"border:2px solid yellow;\" bgcolor=white>&nbsp;<img src=error.gif height=16 width=20> <b>This Purchase Order will put you within 5% of your $monthname $category_name budget! $shownumber left.</b></td><td></td></tr><tr height=10><td colspan=7></td></tr>";}
			elseif($totalamt>=($budgetamt-($budgetamt*.1))){$shownumber=number_format($budgetamt-$totalamt,2); $shownumber="<font color=green>$$shownumber</font>"; echo "<tr><td></td><td colspan=5 style=\"border:2px solid yellow;\" bgcolor=white>&nbsp;<img src=error.gif height=16 width=20> <b>This Purchase Order will put you within 10% of your $monthname $category_name budget! $shownumber left.</b></td><td></td></tr><tr height=10><td colspan=7></td></tr>";}

			//////totals for year
			$b_date1="$budget_year-01-01";
			$b_date2="$budget_year-12-31";
			$query2 = "SELECT SUM(apinvoicedetail.amount) AS totalamt FROM apinvoice,apinvoicedetail WHERE apinvoice.businessid = '$businessid' AND apinvoice.transfer = '4' AND (apinvoice.posted = '1' OR apinvoice.apinvoiceid = '$apinvoiceid') AND apinvoice.apinvoiceid = apinvoicedetail.apinvoiceid AND apinvoicedetail.categoryid = '$categoryid' AND apinvoicedetail.date >= '$b_date1' AND apinvoicedetail.date <= '$b_date2'";
			$result2 = Treat_DB_ProxyOld::query($query2, true);

			$totalamt=@Treat_DB_ProxyOld::mysql_result($result2,0,"totalamt");

			$query2 = "SELECT (`01`+`02`+`03`+`04`+`05`+`06`+`07`+`08`+`09`+`10`+`11`+`12`) AS budgetamt FROM budget WHERE businessid = '$bus_unit' AND companyid = '$companyid' AND type = '$budget_type' AND year = '$budget_year'";
			$result2 = Treat_DB_ProxyOld::query($query2);

			$budgetamt=@Treat_DB_ProxyOld::mysql_result($result2,0,"budgetamt");

			if($budgetamt<$totalamt){$shownumber=number_format($totalamt-$budgetamt,2); $shownumber="<font color=red>+$$shownumber</font>"; echo "<tr><td></td><td colspan=5 style=\"border:2px solid red;\" bgcolor=#FFFF99>&nbsp;<img src=restrict.gif height=16 width=16> <b>This Purchase Order will exceed your $budget_year $category_name budget! $shownumber</b></td><td></td></tr><tr height=10><td colspan=7></td></tr>";}
			elseif($totalamt==$budgetamt){echo "<tr><td></td><td colspan=7 style=\"border:2px solid red;\" bgcolor=#FFFF99>&nbsp;<img src=error.gif height=16 width=20> <b>This Purchase Order will exhaust your $budget_year $category_name budget!</b></td><td></td></tr><tr height=10><td colspan=7></td></tr>";}
			elseif($totalamt>=($budgetamt-($budgetamt*.05))){$shownumber=number_format($budgetamt-$totalamt,2); $shownumber="<font color=green>$$shownumber</font>"; echo "<tr><td></td><td colspan=5 style=\"border:2px solid yellow;\" bgcolor=#FFFF99>&nbsp;<img src=error.gif height=16 width=20> <b>This Purchase Order will put you within 5% of your $budget_year $category_name budget! $shownumber left.</b></td><td></td></tr><tr height=10><td colspan=7></td></tr>";}
			elseif($totalamt>=($budgetamt-($budgetamt*.1))){$shownumber=number_format($budgetamt-$totalamt,2); $shownumber="<font color=green>$$shownumber</font>"; echo "<tr><td></td><td colspan=5 style=\"border:2px solid yellow;\" bgcolor=#FFFF99>&nbsp;<img src=error.gif height=16 width=20> <b>This Purchase Order will put you within 10% of your $budget_year $category_name budget! $shownumber left.</b></td><td></td></tr><tr height=10><td colspan=7></td></tr>";}
			}
		}
		}
		$totalamt=0;
		$budgetamt=0;
		$lastdate=$finddate;
		}
	}

    if($transfer==3){$showpcard="<b><u>PCard</u></b>";}
    else{$showpcard="";}

	//////header for table
	if($transfer==4){echo "<tr><td width=8%></td><td><b><u>Account</u></b></td><td></td><td></td><td></td><td align=right><b><u>Freight</u></b>&nbsp;</td><td align=right><b><u>Tax</u></b>&nbsp;</td><td align=right><b><u>$/Unit</u></b>&nbsp;</td><td align=right><b><u>Amount</u></b></td><td></td></tr>";}
    else{echo "<tr><td width=8%></td><td><b><u>Account</u></b></td><td></td><td></td><td></td><td align=right>$showpcard</td><td align=right><b><u>Amount</u></b></td><td></td></tr>";}

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query3 = "SELECT * FROM apinvoicedetail WHERE apinvoiceid = '$apinvoiceid' ORDER BY addon DESC, date, itemid DESC";
    $result3 = Treat_DB_ProxyOld::query($query3, true);
    $num3=Treat_DB_ProxyOld::mysql_numrows($result3);
    //mysql_close();

    $num3--;
	
    $total=0;
    while ($num3>=0){
       $itemid=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"itemid");
       $amount=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"amount");
	   $tax=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"tax");
	   $freight=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"freight");
       $apaccountid=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"apaccountid");
       $pcard=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"pcard");
       $item=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"item");
       $qty=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"qty");
       $unit=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"unit");
       $tfr_acct=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"tfr_acct");
       $item_price=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"item_price");
       $categoryid=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"categoryid");
       $date31=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"date");
	   $is_addon=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"addon");
	   $inv_item_id=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"inv_item_id");

	   /////totals
	   $taxtotal+=$tax;
	   $freighttotal+=$freight;

	   if($is_addon==1){$addon_color="#00FFFF";}
	   else{$addon_color="white";}

       if ($vendorid==$businessid&&$transfer==1){$qty=$qty*-1;$amount=$amount*-1;}

       $amount=money($amount);
		  if($transfer==3){
			$query4 = "SELECT name FROM expense_report_category WHERE categoryid = '$categoryid'";
			$result4 = Treat_DB_ProxyOld::query($query4);
			$cat_name=@Treat_DB_ProxyOld::mysql_result($result4,0,"name");

			if($cat_name!=""){$cat_name="($cat_name)";}
		  }
		  elseif($transfer==4){
			$query4 = "SELECT category_name FROM po_category WHERE categoryid = '$categoryid'";
			$result4 = Treat_DB_ProxyOld::query($query4);
			$cat_name=@Treat_DB_ProxyOld::mysql_result($result4,0,"category_name");

			if($cat_name!=""){$cat_name="($cat_name)";}
		  }

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          if ($vendorid==$businessid && $transfer==1){
             $query4 = "SELECT * FROM acct_payable WHERE acct_payableid = '$tfr_acct'";
          } else if ($inventory_type > 0) {
		$query4 = "select inv_itemid as accountnum, businessid, item_name as acct_name, price, price_date from inv_items where inv_itemid = '$inv_item_id'";
	  }
          else{
             $query4 = "SELECT * FROM acct_payable WHERE acct_payableid = '$apaccountid'";
          }
          $result4 = Treat_DB_ProxyOld::query($query4);
          //mysql_close();

          $acct_name=@Treat_DB_ProxyOld::mysql_result($result4,0,"acct_name");
          $acct_num=@Treat_DB_ProxyOld::mysql_result($result4,0,"accountnum");
          $acct_num=substr($acct_num,0,5);
          $total=$total+$amount;

          if (($posted!=1&&$posted!=2)||$security_level==9){$showdelete="&nbsp<a href=apitem.php?itemid=$itemid&apinvoiceid=$apinvoiceid&bid=$businessid&cid=$companyid&invtotal=$invtotal&itemamount=$amount&apaccountid=1&goto=$goto&transfer=$transfer&inventory_type=$inventory_type onclick='return delconfirm()'><img alt='Delete' border=0 src=delete.gif></a>";}
          else {$showdelete = "";}

          if ($qty==0){$qty="";}
          if ($item_price==0){$item_price="";}
          else{
			$item_price=number_format($item_price,2);
			$item_price="$$item_price";
		  }

          if ($posted==1 && $transfer==1 && $goto==2){
              //mysql_connect($dbhost,$username,$password);
              //@mysql_select_db($database) or die( "Unable to select database");
              $query5 = "SELECT * FROM acct_payable WHERE acct_payableid = '$tfr_acct'";
              $result5 = Treat_DB_ProxyOld::query($query5);
              //mysql_close();

              $tfr_acct_name=@Treat_DB_ProxyOld::mysql_result($result5,0,"acct_name");
              $tfr_acct_num=@Treat_DB_ProxyOld::mysql_result($result5,0,"accountnum");
              $tfr_acct_num=substr($tfr_acct_num,0,5);

              $tfr_acct_name="<font color=red>=></font> $tfr_acct_num $tfr_acct_name";
          }

          if($transfer==3&&$pcard==1){$showicon="<img src=credit.gif height=25 width=34 alt='PCard'>";}
          elseif($transfer==3&&$pcard==0){$showicon="<img src=cash.gif height=25 width=26 alt='Petty Cash'>";}
          else{$showicon="";}

          if($date31=="0000-00-00"){$date31="";}
          else{$date31="<font style=\"background: #FFFF99;\">&nbsp;$date31&nbsp;</font>";}

          if($qty!=0){
			if($unit!=""){$unit=" $unit";}
			if ($inventory_type > 0){
				$inv_item_qty = $qty;
				$qty = "";
			} else {
				$inv_item_qty = "";
				$qty="[$qty$unit]";
			}
//			$qty="[$qty$unit]";
		  }
          else{$qty=""; $inv_item_qty = "";}

		  /////display line items
		  if($transfer==4){
			$tax=money($tax);
			$freight=money($freight);
			echo "<tr><td width=8% align=right>$date31</td><td bgcolor=$addon_color>$acct_num $acct_name $cat_name $tfr_acct_name</td><td bgcolor=white>$inv_item_qty</td><td bgcolor=$addon_color>$qty $item</td><td bgcolor=$addon_color></td><td align=right bgcolor=$addon_color>$$freight&nbsp;</td><td align=right bgcolor=$addon_color>$$tax&nbsp;</td><td bgcolor=$addon_color align=right>$item_price&nbsp;</td><td align=right bgcolor=$addon_color>$$amount</td><td width=8%>$showdelete</td></tr>";
		    echo "<tr bgcolor=#E8E7E7 height=1><td colspan=9></td></tr>";
		  }
		  else{
			echo "<tr><td width=8% align=right>$date31</td><td bgcolor=white>$acct_num $acct_name $cat_name $tfr_acct_name</td><td bgcolor=white>$inv_item_qty</td><td bgcolor=white>$qty $item</td><td bgcolor=white></td><td bgcolor=white align=right>$showicon</td><td align=right bgcolor=white>$$amount</td><td width=8%>$showdelete</td></tr>";
		    echo "<tr bgcolor=#E8E7E7 height=1><td colspan=7></td></tr>";
		  }

       $num3--;
    }

    ////////////////////NEW WAY TO ADD ITEMS (7/7/09)
    if (($posted!=1&&$posted!=2)||$security_level==9||($transfer==4&&$posted==1)){
	   if($transfer==4&&$posted==1){$addon=1;}
	   else{$addon=0;}

if ($inventory_type > 0){
	$query3 = "select inv_itemid as acct_payableid, inv_itemid as accountnum, item_name as acct_name, 1 as cos, 2 as acct_payableid from inv_items WHERE businessid = '$businessid'";
} else {
	$query3 = "SELECT * FROM acct_payable WHERE businessid = '$businessid' AND (cs_type = '0' OR cs_type = '4') AND is_deleted = '0' ORDER BY cos,accountnum DESC";
}
//       $query3 = "SELECT * FROM acct_payable WHERE businessid = '$businessid' AND (cs_type = '0' OR cs_type = '4') AND is_deleted = '0' ORDER BY cos,accountnum DESC";
//	$query3 = "select inv_itemid as accountnum, item_name as acct_name, 1 as cos, 2 as acct_payableid from inv_items where businessid = 19";
       $result3 = Treat_DB_ProxyOld::query($query3);
       $num3=Treat_DB_ProxyOld::mysql_numrows($result3);

       echo "<tr height=0><td colspan=8>";
       echo "<form action=apitem.php method=post onSubmit='return disableForm(this);' name=newitem style=\"margin:0;padding:0;display:inline;\">";
       echo "<input type=hidden name=username value=$user>";
       echo "<input type=hidden name=password value=$pass>";
       echo "<input type=hidden name=businessid value=$businessid>";
       echo "<input type=hidden name=companyid value=$companyid>";
       echo "<input type=hidden name=apinvoiceid value='$apinvoiceid'>";
       echo "<input type=hidden name=goto value=$goto>";
       echo "<input type=hidden name=allitems value=$allitems>";
       echo "<input type=hidden name=new value='new'>";
       echo "<input type=hidden name=transfer value=$transfer>";
       echo "<input type=hidden name=invtotal value='$invtotal'>";
	   echo "<input type=hidden name=addon value='$addon'>";
	echo "<input type=hidden name=inventory_type value='$inventory_type'>";
       echo "</td></tr>";
       echo "<tr><td align=right>";

       ///////////////EXPENSE REPORT DATE
       if($transfer==3||$transfer==4){
          //$formnum++;
          echo "<INPUT TYPE=text NAME=date31 VALUE='$date' SIZE=8 STYLE=\"font-size: 10px;\"> <SCRIPT LANGUAGE='JavaScript' ID='js191'> var cal191 = new CalendarPopup('testdiv1');cal191.setCssPrefix('TEST');</SCRIPT> <A HREF='javascript:void();' onClick=cal191.select(document.forms[$formnum].date31,'anchor191','yyyy-MM-dd'); return false; TITLE=cal191.select(document.forms[$formnum].date31,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor191' ID='anchor191'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A>";
       }
       //////////////////////////////////

if ($inventory_type > 0){
//echo "</td><td bgcolor=white> <select name=inv_item_id STYLE=\"font-size: 10px;\">";
echo "</td><td bgcolor=white> <select name=apaccountid STYLE=\"font-size: 10px;\">";
} else {
echo "</td><td bgcolor=white> <select name=apaccountid STYLE=\"font-size: 10px;\">";
}

//       echo "</td><td bgcolor=white> <select name=apaccountid STYLE=\"font-size: 10px;\">";

       $num3--;
       $lastcos=1;
       while ($num3>=0){
          $acct_name=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"acct_name");
          $acct_payableid=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"acct_payableid");
          $accountnum=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"accountnum");
          $cos=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"cos");
          $accountnum=substr($accountnum,0,5);

          if ($lastcos!=$cos){echo "<option value=0>====================================</option>";}
          else{
             echo "<option value=$acct_payableid>$accountnum - $acct_name</option>";
             $num3--;
          }
          $lastcos=$cos;
       }
       echo "</select>";
echo "</td><td>";
if ($inventory_type > 0){
echo "<input type=text name=inventory_item_qty size=5 maxlength=5 value=qty onfocus=\"if (this.value=='qty') {this.value='';}\">";
} else {
echo "<input type=hidden name=inventory_item_qty value=0>";
}

       //////EXPENSE REPORT CATEGORY
       if($transfer==3){
          $query32 = "SELECT * FROM expense_report_category ORDER BY name";
          $result32 = Treat_DB_ProxyOld::query($query32);

          echo "<select name=categoryid onchange=\"showbox(this)\" STYLE=\"font-size: 10px;\">";

          while($r=Treat_DB_ProxyOld::mysql_fetch_array($result32)){
             $categoryid=$r["categoryid"];
             $cat_name=$r["name"];
             $multiplier1=$r["multiplier"];

             if($multiplier1!=0){$multiplier=$multiplier1;}

             echo "<option value=$categoryid>$cat_name</option>";
          }
          echo "</select> <input type=text size=3 name=mult style=\"display: none; font-size: 10px;\" id='mult' onkeyup=\"updateTotal('$multiplier')\">";
       }
	   //////PURCHASE ORDER CATEGORY
       if($transfer==4){
          $query32 = "SELECT * FROM po_category ORDER BY category_name";
          $result32 = Treat_DB_ProxyOld::query($query32);

          echo "<select name=categoryid onchange=\"showbox(this)\" STYLE=\"font-size: 10px;\">";

          while($r=Treat_DB_ProxyOld::mysql_fetch_array($result32)){
             $categoryid=$r["categoryid"];
             $cat_name=$r["category_name"];

             echo "<option value=$categoryid>$cat_name</option>";
          }
          echo "</select>";
       }
       ////////////////////////////

       echo "</td>";

       echo "<td bgcolor=white>";
	   if($transfer==4){echo "<input type=text name=qty size=4 value='Qty' onfocus=\"if (this.value=='Qty') {this.value='1';}\" STYLE=\"font-size: 10px;\">&nbsp;<input type=text name=units size=5 value='Units' STYLE=\"font-size: 10px;\" onfocus=\"if (this.value=='Units') {this.value='';}\">&nbsp;";}
	   echo "<input type=text name=itemname size=40 maxlength=40 value='Description' STYLE=\"font-size: 10px;\" onfocus=\"if (this.value=='Description') {this.value='';}\">";

	   if($transfer==4){echo "</td><td bgcolor=white></td><td align=right bgcolor=white>$<input type=text size=5 name=freight value='Freight' STYLE=\"font-size: 10px;\" onfocus=\"if (this.value=='Freight') {this.value='';}\">&nbsp;</td><td align=right bgcolor=white>$<input type=text size=5 name=tax value='Tax' STYLE=\"font-size: 10px;\" onfocus=\"if (this.value=='Tax') {this.value='';}\"></td><td align=right bgcolor=white>";}
	   else{echo "</td><td bgcolor=white></td><td bgcolor=white align=right>";}

	   if($transfer==4){echo "$<input type=text name=item_price size=5 value='Price' STYLE=\"font-size: 10px;\" onfocus=\"if (this.value=='Price') {this.value='';}\">"; $amount_disable="DISABLED";}
       elseif($transfer==3&&$cash_only==1){echo "<select name=pcard STYLE=\"font-size: 10px;\"><option value=0>Cash</option></select>";}
       elseif($transfer==3&&$my_vendor==1){echo "<select name=pcard STYLE=\"font-size: 10px;\"><option value=1>PCard</option><option value=0>Cash</option></select>";}
       elseif($transfer==3&&$my_vendor==0){echo "<select name=pcard STYLE=\"font-size: 10px;\"><option value=1>PCard</option></select>";}

	   /////total amount
	   if($transfer==4){echo "</td><td bgcolor=white align=right></td>";}
       else{echo "</td><td bgcolor=white align=right>$<input type=text name=amount size=5 id='amt' $amount_disable STYLE=\"font-size: 10px;\"></td>";}

	   ////last valid date
	   if($transfer==4&&$security_level<9){
			$val_day=date("d");
			if($val_day<=4){
				$val_month=date("m");
				$val_year=date("Y");
				$val_month--;
				if($val_month==0){$val_month="12";$val_year--;}
				if(strlen($val_month)<2){$val_month="0$val_month";}
				$val_day="$val_year-$val_month-01";
			}
			else{
				$val_month=date("m");
				$val_year=date("Y");
				$val_day="$val_year-$val_month-01";
			}
	   }

       echo "<td><input type=submit value='Add' STYLE=\"font-size: 10px;\" onclick=\"return validate(this.form,$transfer,'$val_day');\" ></form></td><td></td></tr>";

    }

    $total=money($total);
	$taxtotal=money($taxtotal);
	$freighttotal=money($freighttotal);
	if($transfer==4){
		echo "<tr><td></td><td></td><td></td><td align=right><b><font color=$fontcolor>TOTALS:</font></b></td><td align=right bgcolor=white><b>$$freighttotal</b>&nbsp;</td><td align=right bgcolor=white><b>$$taxtotal</b>&nbsp;</td><td bgcolor=white></td><td align=right bgcolor=white><b><font color=$fontcolor>$$total</font></b></td><td></td></tr>";
		echo "<tr bgcolor=#E8E7E7 height=1><td colspan=9></td></tr>";
	}
	else{
		echo "<tr><td colspan=5></td><td align=right><b><font color=$fontcolor>TOTAL:</font></b></td><td align=right bgcolor=white><b><font color=$fontcolor>$$total</font></b></td><td></td></tr>";
		echo "<tr bgcolor=#E8E7E7 height=1><td colspan=7></td></tr>";
	}
///////////////ACCEPT PURCHASE CARD///////////////////////////////////////
    if ($posted==2&&$security_level>7&&$transfer==2){
       echo "<tr bgcolor=red><td colspan=7><p><br><form action=acceptpcard.php method=post onSubmit='return disableForm(this);'><input type=hidden name=businessid value=$businessid><input type=hidden name=companyid value=$companyid><input type=hidden name=apinvoiceid value=$apinvoiceid><input type=hidden name=goto value=$goto><center><input type=submit value='APPROVE PURCHASE CARD'></form></td></tr>";
    }
    if ($posted==1&&$transfer==2){
       echo "<tr bgcolor=#00FF00><td colspan=7><p><br><center><b>Accepted by $accept_by</b></center><p></td></tr>";
    }
///////////////ACCEPT TRANSFER////////////////////////////////////////////
    if ($posted==2&&$security_level>7&&$transfer==1){
       echo "<tr><td colspan=7><p><br><form action=accepttransfer.php method=post onSubmit='return disableForm(this);'></td></tr>";
       if($interco_trans==1){echo "<tr bgcolor=#00FF00><td colspan=7></td></tr>";}
       else{echo "<tr bgcolor=#00FF00><td colspan=7></td></tr>";}

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query3 = "SELECT * FROM apinvoicedetail WHERE apinvoiceid = '$apinvoiceid'";
       $result3 = Treat_DB_ProxyOld::query($query3, true);
       $num3=Treat_DB_ProxyOld::mysql_numrows($result3);
       //mysql_close();

       $num3--;

       $total=0;
       while ($num3>=0){
       $itemid=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"itemid");
       $amount=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"amount");
       $apaccountid=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"apaccountid");
       $tfr_acct=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"tfr_acct");
       $item=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"item");
       $qty=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"qty");
       $unit=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"unit");
       $item_price=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"item_price");
       $amount=money($amount*-1);

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query4 = "SELECT * FROM acct_payable WHERE acct_payableid = '$apaccountid'";
          $result4 = Treat_DB_ProxyOld::query($query4);
          //mysql_close();

          $acct_name=@Treat_DB_ProxyOld::mysql_result($result4,0,"acct_name");
          $acct_num=@Treat_DB_ProxyOld::mysql_result($result4,0,"accountnum");
          $acct_num=substr($acct_num,0,5);
          $total=$total+$amount;

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query7 = "SELECT * FROM acct_payable WHERE businessid = '$vendorid' AND (cs_type = '0' OR cs_type = '4') AND accountnum LIKE '$acct_num%' AND is_deleted = '0'";
          $result7 = Treat_DB_ProxyOld::query($query7);
          $num7=Treat_DB_ProxyOld::mysql_numrows($result7);
          //mysql_close();

          if ($num7==0&&$tfr_acct==0){$showcolor="red";$showerror="<img src=error.gif height=16 width=20 alt='NO CORRESPONDING ACCOUNT'>";}else{$showcolor="#00FF00";$showerror="";}

          if ($qty==0){$qty="";}
          if ($item_price==0){$item_price="";}
          else{$item_price="$$item_price";}

          $newapid="apid$num3";

          echo "<tr><td width=20% bgcolor=$showcolor>$showerror</td><td bgcolor=$showcolor>$acct_num $acct_name</td><td bgcolor=$showcolor>$item</td><td bgcolor=$showcolor>$qty $unit</td><td bgcolor=$showcolor align=right>$item_price</td><td align=right bgcolor=$showcolor>$$amount&nbsp</td><td bgcolor=$showcolor><select name=$newapid $showdisable>";

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query8 = "SELECT * FROM acct_payable WHERE businessid = '$vendorid' AND (cs_type = '0' OR cs_type = '4') AND is_deleted = '0' ORDER BY cos,accountnum DESC";
          $result8 = Treat_DB_ProxyOld::query($query8);
          $num8=Treat_DB_ProxyOld::mysql_numrows($result8);
          //mysql_close();

          $is_dif=0;
          $num8--;
          while ($num8>=0){
             $acct_name=@Treat_DB_ProxyOld::mysql_result($result8,$num8,"acct_name");
             $acct_payableid=@Treat_DB_ProxyOld::mysql_result($result8,$num8,"acct_payableid");
             $accountnum=@Treat_DB_ProxyOld::mysql_result($result8,$num8,"accountnum");
             $cos=@Treat_DB_ProxyOld::mysql_result($result8,$num8,"cos");
             $accountnum=substr($accountnum,0,5);

             if ($acct_payableid==$tfr_acct){$showtransfer="SELECTED";$is_dif=1;}
             elseif ($accountnum==$acct_num && $is_dif == 0){$showtransfer="SELECTED";}
             else{$showtransfer="";}

             echo "<option value=$acct_payableid $showtransfer>$accountnum - $acct_name</option>";

             $num8--;
          }

          echo "</select></td></tr>";
          echo "<tr bgcolor=#E8E7E7 height=1><td colspan=7></td></tr>";

       $num3--;
       }
       if ($interco_trans==1){echo "<tr height=30 valign=bottom><td colspan=7></td></tr>";}

       /////////////INTER COMPANY TRANSFER///////////////////
       if ($interco_trans==1){

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query51 = "SELECT * FROM company WHERE companyid != '$companyid'";
          $result51 = Treat_DB_ProxyOld::query($query51);
          $num51=Treat_DB_ProxyOld::mysql_numrows($result51);
          //mysql_close();

          echo "<tr bgcolor=yellow><td colspan=7><center><b>Inter-Company Transfer Details</b></center></td></tr>";
          echo "<tr bgcolor=yellow><td colspan=7>Please Choose a Company: <select name=transco_id onChange='changePage(this.form.transco_id)'>";
          while($num51>=0){
             $transco_name=@Treat_DB_ProxyOld::mysql_result($result51,$num51,"companyname");
             $newtransco_id=@Treat_DB_ProxyOld::mysql_result($result51,$num51,"companyid");

             if($newtransco_id==$transco_id){$showsel2="SELECTED";}
             else{$showsel2="";}

             echo "<option value=apinvoice.php?bid=$businessid&cid=$companyid&apinvoiceid=$apinvoiceid&goto=$goto&interco_trans=1&transco_id=$newtransco_id#interco $showsel2>$transco_name</option>";

             $num51--;
          }
          echo "</select></td></tr>";
          echo "<tr bgcolor=#E8E7E7 height=1><td colspan=7></td></tr>";

          if ($transco_id>0){
             echo "<tr bgcolor=yellow><td colspan=7><select name=transco_bus1 onChange='changePage(this.form.transco_bus1)'>";

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query52 = "SELECT * FROM business WHERE companyid = '$transco_id' AND businessid != '$transco_bus2' AND interco = '1' ORDER BY businessname DESC";
             $result52 = Treat_DB_ProxyOld::query($query52);
             $num52=Treat_DB_ProxyOld::mysql_numrows($result52);
             //mysql_close();

             while($num52>=0){

                $newtransco_bus1_name=@Treat_DB_ProxyOld::mysql_result($result52,$num52,"businessname");
                $newtransco_bus1=@Treat_DB_ProxyOld::mysql_result($result52,$num52,"businessid");

                if($newtransco_bus1==$transco_bus1){$showsel3="SELECTED";}
                else{$showsel3="";}

                echo "<option value=apinvoice.php?bid=$businessid&cid=$companyid&apinvoiceid=$apinvoiceid&goto=$goto&interco_trans=1&transco_id=$newtransco_id&transco_bus1=$newtransco_bus1&transco_bus2=$transco_bus2#interco $showsel3>$newtransco_bus1_name</option>";

                $num52--;
             }
             echo "</select> <b>Credit</b></td></tr>";
             echo "<tr bgcolor=#E8E7E7 height=1><td colspan=7></td></tr>";

             /////////DETAILS FOR INTERCO TRANSFER
             if ($transco_bus1>0){

                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query3 = "SELECT * FROM apinvoicedetail WHERE apinvoiceid = '$apinvoiceid'";
                $result3 = Treat_DB_ProxyOld::query($query3, true);
                $num3=Treat_DB_ProxyOld::mysql_numrows($result3);
                //mysql_close();

                $num3--;

                $total=0;
                while ($num3>=0){
                $itemid=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"itemid");
                $amount=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"amount");
                $apaccountid=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"apaccountid");
                $tfr_acct=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"tfr_acct");
                $item=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"item");
                $qty=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"qty");
                $unit=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"unit");
                $item_price=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"item_price");
                $amount=money($amount);

                   //mysql_connect($dbhost,$username,$password);
                   //@mysql_select_db($database) or die( "Unable to select database");
                   $query4 = "SELECT * FROM acct_payable WHERE acct_payableid = '$apaccountid'";
                   $result4 = Treat_DB_ProxyOld::query($query4);
                   //mysql_close();

                   $acct_name=@Treat_DB_ProxyOld::mysql_result($result4,0,"acct_name");
                   $acct_num=@Treat_DB_ProxyOld::mysql_result($result4,0,"accountnum");
                   $acct_num=substr($acct_num,0,5);
                   $total=$total+$amount;

                   //mysql_connect($dbhost,$username,$password);
                   //@mysql_select_db($database) or die( "Unable to select database");
                   $query7 = "SELECT * FROM acct_payable WHERE businessid = '$transco_bus1' AND (cs_type = '0' OR cs_type = '4') AND accountnum LIKE '$acct_num%' AND is_deleted = '0'";
                   $result7 = Treat_DB_ProxyOld::query($query7);
                   $num7=Treat_DB_ProxyOld::mysql_numrows($result7);
                   //mysql_close();

                   if ($num7==0&&$tfr_acct==0){$showcolor="red";$showerror="<img src=error.gif height=16 width=20 alt='NO CORRESPONDING ACCOUNT'>";}else{$showcolor="#00FF00";$showerror="";}

                   if ($qty==0){$qty="";}
                   if ($item_price==0){$item_price="";}
                   else{$item_price="$$item_price";}

                   $newapid="transbus1_apid$num3";

                   echo "<tr><td width=20% bgcolor=yellow>$showerror</td><td bgcolor=yellow>$acct_num $acct_name</td><td bgcolor=yellow>$item</td><td bgcolor=yellow>$qty $unit</td><td bgcolor=yellow align=right>$item_price</td><td align=right bgcolor=yellow>$$amount&nbsp</td><td bgcolor=yellow><select name=$newapid $showdisable>";

                   //mysql_connect($dbhost,$username,$password);
                   //@mysql_select_db($database) or die( "Unable to select database");
                   $query8 = "SELECT * FROM acct_payable WHERE businessid = '$transco_bus1' AND (cs_type = '0' OR cs_type = '4') AND is_deleted = '0' ORDER BY cos,accountnum DESC";
                   $result8 = Treat_DB_ProxyOld::query($query8);
                   $num8=Treat_DB_ProxyOld::mysql_numrows($result8);
                   //mysql_close();

                   $num8--;
                   while ($num8>=0){
                      $acct_name=@Treat_DB_ProxyOld::mysql_result($result8,$num8,"acct_name");
                      $acct_payableid=@Treat_DB_ProxyOld::mysql_result($result8,$num8,"acct_payableid");
                      $accountnum=@Treat_DB_ProxyOld::mysql_result($result8,$num8,"accountnum");
                      $cos=@Treat_DB_ProxyOld::mysql_result($result8,$num8,"cos");
                      $accountnum=substr($accountnum,0,5);
                      if ($acct_payableid==$tfr_acct){$showtransfer="SELECTED";}
                      elseif ($accountnum==$acct_num){$showtransfer="SELECTED";}
                      else{$showtransfer="";}

                      echo "<option value=$acct_payableid $showtransfer>$accountnum - $acct_name</option>";

                      $num8--;
                   }

                   echo "</select></td></tr>";
                   echo "<tr bgcolor=#E8E7E7 height=1><td colspan=7></td></tr>";

                $num3--;
                }

             }
             /////////END DETAILS FOR BUS1

             echo "<tr bgcolor=yellow><td colspan=7><select name=transco_bus2 onChange='changePage(this.form.transco_bus2)'>";

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query52 = "SELECT * FROM business WHERE companyid = '$transco_id' AND businessid != '$transco_bus1' ORDER BY businessname DESC";
             $result52 = Treat_DB_ProxyOld::query($query52);
             $num52=Treat_DB_ProxyOld::mysql_numrows($result52);
             //mysql_close();

             while($num52>=0){

                $newtransco_bus2_name=@Treat_DB_ProxyOld::mysql_result($result52,$num52,"businessname");
                $newtransco_bus2=@Treat_DB_ProxyOld::mysql_result($result52,$num52,"businessid");

                if($newtransco_bus2==$transco_bus2){$showsel3="SELECTED";}
                else{$showsel3="";}

                echo "<option value=apinvoice.php?bid=$businessid&cid=$companyid&apinvoiceid=$apinvoiceid&goto=$goto&interco_trans=1&transco_id=$newtransco_id&transco_bus1=$transco_bus1&transco_bus2=$newtransco_bus2#interco $showsel3>$newtransco_bus2_name</option>";

                $num52--;
             }
             echo "</select> <b>Debit</b></td></tr>";
             echo "<tr bgcolor=#E8E7E7 height=1><td colspan=7></td></tr>";

             /////////DETAILS FOR INTERCO TRANSFER
             if ($transco_bus2>0){

                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query3 = "SELECT * FROM apinvoicedetail WHERE apinvoiceid = '$apinvoiceid'";
                $result3 = Treat_DB_ProxyOld::query($query3, true);
                $num3=Treat_DB_ProxyOld::mysql_numrows($result3);
                //mysql_close();

                $num3--;

                $total=0;
                while ($num3>=0){
                $itemid=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"itemid");
                $amount=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"amount");
                $apaccountid=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"apaccountid");
                $tfr_acct=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"tfr_acct");
                $item=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"item");
                $qty=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"qty");
                $unit=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"unit");
                $item_price=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"item_price");
                $amount=money($amount*-1);
                
                   //mysql_connect($dbhost,$username,$password);
                   //@mysql_select_db($database) or die( "Unable to select database");
                   $query4 = "SELECT * FROM acct_payable WHERE acct_payableid = '$apaccountid'";
                   $result4 = Treat_DB_ProxyOld::query($query4);
                   //mysql_close();

                   $acct_name=@Treat_DB_ProxyOld::mysql_result($result4,0,"acct_name");
                   $acct_num=@Treat_DB_ProxyOld::mysql_result($result4,0,"accountnum");
                   $acct_num=substr($acct_num,0,5);
                   $total=$total+$amount;

                   //mysql_connect($dbhost,$username,$password);
                   //@mysql_select_db($database) or die( "Unable to select database");
                   $query7 = "SELECT * FROM acct_payable WHERE businessid = '$transco_bus2' AND (cs_type = '0' OR cs_type = '4') AND accountnum LIKE '$acct_num%' AND is_deleted = '0'";
                   $result7 = Treat_DB_ProxyOld::query($query7);
                   $num7=Treat_DB_ProxyOld::mysql_numrows($result7);
                   //mysql_close();

                   if ($num7==0&&$tfr_acct == 0){$showcolor="red";$showerror="<img src=error.gif height=16 width=20 alt='NO CORRESPONDING ACCOUNT'>";}else{$showcolor="#00FF00";$showerror="";}

                   if ($qty==0){$qty="";}
                   if ($item_price==0){$item_price="";}
                   else{$item_price="$$item_price";}

                   $newapid="transbus2_apid$num3";

                   echo "<tr><td width=20% bgcolor=yellow>$showerror</td><td bgcolor=yellow>$acct_num $acct_name</td><td bgcolor=yellow>$item</td><td bgcolor=yellow>$qty $unit</td><td bgcolor=yellow align=right>$item_price</td><td align=right bgcolor=yellow>$$amount&nbsp</td><td bgcolor=yellow><select name=$newapid $showdisable>";

                   //mysql_connect($dbhost,$username,$password);
                   //@mysql_select_db($database) or die( "Unable to select database");
                   $query8 = "SELECT * FROM acct_payable WHERE businessid = '$transco_bus2' AND (cs_type = '0' OR cs_type = '4') AND is_deleted = '0' ORDER BY cos,accountnum DESC";
                   $result8 = Treat_DB_ProxyOld::query($query8);
                   $num8=Treat_DB_ProxyOld::mysql_numrows($result8);
                   //mysql_close();

                   $num8--;
                   while ($num8>=0){
                      $acct_name=@Treat_DB_ProxyOld::mysql_result($result8,$num8,"acct_name");
                      $acct_payableid=@Treat_DB_ProxyOld::mysql_result($result8,$num8,"acct_payableid");
                      $accountnum=@Treat_DB_ProxyOld::mysql_result($result8,$num8,"accountnum");
                      $cos=@Treat_DB_ProxyOld::mysql_result($result8,$num8,"cos");
                      $accountnum=substr($accountnum,0,5);

                      if ($tfr_acct==$acct_payableid){$showtransfer="SELECTED";}
                      elseif ($accountnum==$acct_num){$showtransfer="SELECTED";}
                      else{$showtransfer="";}

                      echo "<option value=$acct_payableid $showtransfer>$accountnum - $acct_name</option>";

                      $num8--;
                   }

                   echo "</select></td></tr>";
                   echo "<tr bgcolor=#E8E7E7 height=1><td colspan=7></td></tr>";

                $num3--;
                }

             }
             /////////END DETAILS FOR BUS2
          }
       }
       /////////////END INTER COMPANY TRANSFER//////////////
       echo "<tr bgcolor=#00FF00><td colspan=7><center><br><p><input type=hidden name=businessid value=$businessid><input type=hidden name=companyid value=$companyid><input type=hidden name=apinvoiceid value=$apinvoiceid><input type=hidden name=goto value=$goto><input type=hidden name=interco_trans value=$interco_trans><input type=hidden name=new_transco_id value=$transco_id><input type=hidden name=new_transco_bus1 value=$transco_bus1><input type=hidden name=new_transco_bus2 value=$transco_bus2><input type=hidden name=invoicenum value=$invoicenum><input type=hidden name=date value=$date><input type=hidden name=total value=$total><input type=hidden name=notes value='$notes'><input type=submit value='ACCEPT TRANSFER' onclick='return confirmaccept()' $showdisable></form></center></td></tr>";
    }

    if($transfer==3){$showprint="&nbsp;<a href=expense_print.php?id=$apinvoiceid style=$style target='_blank'><font color=blue>Print</font></a>";}
	elseif($transfer==4&&$posted==1){$showprint="&nbsp;<a href=po_print.php?id=$apinvoiceid style=$style target='_blank'><font color=blue>Print</font></a>";}
    else{$showprint="";}

	if($transfer==4){echo "<tr><td colspan=8 style=\"font-size: 18px;\">$showprint</td><td align=right><font size=1>$showuser</td></tr>";}
    else{echo "<tr><td colspan=6 style=\"font-size: 18px;\">$showprint</td><td align=right><font size=1>$showuser</td></tr>";}

	/////////////email purchase order
	if($transfer==4&&$posted==1){

		/////email
		$query59 = "SELECT email FROM vendors WHERE vendorid = '$vendorid'";
        $result59 = Treat_DB_ProxyOld::query($query59);

		$email=@Treat_DB_ProxyOld::result($result59,0,"email");

		echo "<tr><td colspan=6><form action=po_email.php method=post onSubmit='return disableForm(this);'><input type=hidden name=companyid value=$companyid><input type=hidden name=businessid value=$businessid><input type=hidden name=apinvoiceid value=$apinvoiceid>&nbsp;<textarea name=message rows=4 cols=26>Message</textarea><br>&nbsp;<input type=text name=email value='$email' size=25> <input type=submit value='Email' onclick='return confirm(\"Are you sure you want to email this Purchase Order?\")'></form></td></tr>";
	}

    echo "</table></center>";

/////////////////////ADD ITEMS////////////////////////
  if (($posted!=1&&$posted!=2)||$security_level>0){

    echo "<table bgcolor=#E8E7E7 width=100% >";


/*///////////////////////////INVENTORY ITEMS
    if ($allitems==1){$showitems="VENDOR'S ITEMS ONLY";$senditems=0;}
    else{$showitems="SHOW ALL ITEMS";$senditems=1;}

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    if ($allitems==1){
       $query3 = "SELECT * FROM inv_items WHERE businessid = '$businessid' ORDER BY apaccountid,item_name DESC";
    }
    else{
       $query3 = "SELECT * FROM inv_items WHERE businessid = '$businessid' AND vendor = '$vendor_master' ORDER BY apaccountid,item_name DESC";
    }
    $result3 = mysql_query($query3);
    $num3=mysql_numrows($result3);
    //mysql_close();

    echo "<tr><td colspan=2>";
    echo "<form action=apitem.php method=post name=invitem>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<input type=hidden name=businessid value=$businessid>";
    echo "<input type=hidden name=companyid value=$companyid>";
    echo "<input type=hidden name=apinvoiceid value='$apinvoiceid'>";
    echo "<input type=hidden name=goto value=$goto>";
    echo "<input type=hidden name=allitems value=$allitems>";
    echo "<input type=hidden name=new value='new'>";
    echo "<input type=hidden name=transfer value=$transfer>";
    echo "<input type=hidden name=invtotal value='$invtotal'>";
    echo "</td></tr>";
    echo "<tr><td colspan=2><ul><li>Quantity: &nbsp<input type=text name=qty size=5> <select name=invitem>";

    $num3--;
    $lastcos=1;
    while ($num3>=0){
       $inv_itemid=@mysql_result($result3,$num3,"inv_itemid");
       $item_name=@mysql_result($result3,$num3,"item_name");
       $units=@mysql_result($result3,$num3,"units");
       $price=@mysql_result($result3,$num3,"price");
       $apaccountid=@mysql_result($result3,$num3,"apaccountid");

       if ($lastapacct!=$apaccountid&&$lastapacct!=0){echo "<option value=0>====================================</option>";}
       else{
          $price=money($price);

          if ($newitemid==$inv_itemid){$showsel="SELECTED";}
          else{$showsel="";}

          echo "<option value=$inv_itemid $showsel>$item_name - $units $$price</option>";
          $num3--;
          $showspace="";
       }
       $lastapacct=$apaccountid;
    }
    echo "</select> <input type=text size=5 name=newprice value='new$'> <input type=submit value='Add'> </form><font size=1>[<a href=apinvoice.php?bid=$businessid&cid=$companyid&apinvoiceid=$apinvoiceid&goto=$goto&load=1&allitems=$senditems#additems><font color=blue>$showitems</font></a>] <font size=1>[<a href=businventor3.php?bid=$businessid&cid=$companyid&apinv=$apinvoiceid&newvendor=$vendor_master#invitem><font color=blue>NEW ITEM</font></a>]</td></tr>";

    echo "<tr><td colspan=2><p><br><p><br></td></tr>";*/

    if(($transfer==0||$transfer==3)&&$interco==0&&$security_level>0&&$posted==0){
       echo "<tr><td><a href=disperse.php?apinvoiceid=$apinvoiceid&bid=$businessid&cid=$companyid&dis=-2&goto=$goto style=$style><font color=blue>Disperse Funds</font></a></td>";
    }
    elseif(($transfer==0||$transfer==3)&&$interco==-2&&$security_level>0&&$posted==0){
       echo "<tr><td colspan=2><a href=disperse.php?apinvoiceid=$apinvoiceid&bid=$businessid&cid=$companyid&dis=0&goto=$goto style=$style><font color=blue>Cancel Dispersal</font></a></td></tr>";
    }

    if($transfer==3&&$interco==0&&$security_level>0&&$posted==0){if($noreturn!=0){$submit_disable="DISABLED";} echo "<td align=right><form action=submit_expense.php method=post style=\"margin:0;padding:0;display:inline;\"><input type=hidden name=apinvoiceid value=$apinvoiceid><input type=hidden name=goto value=$goto><input type=hidden name=businessid value=$businessid><input type=hidden name=companyid value=$companyid><input type=submit value='Submit' onclick='return submitconfirm()' $submit_disable></form></td></tr>";}
	elseif($transfer==4&&$security_level>0&&($posted==0||$posted==3)){
		/////////submit purchase order
		echo "<td align=right>";

		echo "<table cellspacing=0 cellpadding=1 style=\"border:1px solid #CCCCCC;width: 500px;background-color: white;\">";

		/////display history
		$query23 = "SELECT * FROM po_note WHERE apinvoiceid = '$apinvoiceid' ORDER BY id";
		$result23 = Treat_DB_ProxyOld::query($query23);

		while($r23=Treat_DB_ProxyOld::mysql_fetch_array($result23)){
			$message=$r23["message"];
			$message=str_replace("\n","<br>",$message);
			echo "<tr><td style=\"border:1px solid #CCCCCC;\">$message</td></tr>";
		}

		if($noreturn!=0){$submit_disable="DISABLED";}
		echo "<tr><td style=\"border:1px solid #CCCCCC;\"><form action=submit_po.php method=post style=\"margin:0;padding:0;display:inline;\"><input type=hidden name=apinvoiceid value=$apinvoiceid><input type=hidden name=goto value=$goto><input type=hidden name=businessid value=$businessid><input type=hidden name=companyid value=$companyid><i>Approval considerations:</i><br><textarea name=message rows=3 cols=45></textarea><input type=submit value='Submit' onclick='return submitconfirm()' $submit_disable></form></td></tr>";

		echo "</td></tr>";
	}
    else{echo "<td></td></tr>";}
/////////////////////////////////
//////////MULTI_TRANSFER/////////
/////////////////////////////////
    if($interco==-2){
       echo "<tr><td colspan=2><center><table width=100%>";
       echo "<tr><td colspan=7><center><h3><a name=#disperse>DISPERSE ITEMS</a></h3></center></td></tr>";

       echo "<tr bgcolor=#CCCCCC><td width=5%><b>Co</td><td width=20%><b>Unit</td><td width=20%><b>From Account...</td><td width=20%><b>To Account...</td><td width=20%><b>Description</td><td width=10% align=right><b>$</td><td width=5%></td></tr>";

       $query51 = "SELECT * FROM apinvoice_multi_trans WHERE apinvoiceid = '$apinvoiceid' ORDER BY id DESC";
       $result51 = Treat_DB_ProxyOld::query($query51, true);
       $num51=Treat_DB_ProxyOld::mysql_numrows($result51);

       $num51--;
       while($num51>=0){
          $item_id=@Treat_DB_ProxyOld::mysql_result($result51,$num51,"id");
          $item_comid=@Treat_DB_ProxyOld::mysql_result($result51,$num51,"companyid");
          $item_busid=@Treat_DB_ProxyOld::mysql_result($result51,$num51,"businessid");
          $item_apacct_from=@Treat_DB_ProxyOld::mysql_result($result51,$num51,"acct_payableid_from");
          $item_apacct=@Treat_DB_ProxyOld::mysql_result($result51,$num51,"acct_payableid");
          $item_note=@Treat_DB_ProxyOld::mysql_result($result51,$num51,"note");
          $item_amount=@Treat_DB_ProxyOld::mysql_result($result51,$num51,"amount");

          $query52 = "SELECT code FROM company WHERE companyid = '$item_comid'";
          $result52 = Treat_DB_ProxyOld::query($query52);

          $item_comid=@Treat_DB_ProxyOld::mysql_result($result52,0,"code");

          $query52 = "SELECT businessname FROM business WHERE businessid = '$item_busid'";
          $result52 = Treat_DB_ProxyOld::query($query52);

          $item_busid=@Treat_DB_ProxyOld::mysql_result($result52,0,"businessname");

          $query52 = "SELECT acct_name,accountnum FROM acct_payable WHERE acct_payableid = '$item_apacct_from'";
          $result52 = Treat_DB_ProxyOld::query($query52);

          $acct_name_from=@Treat_DB_ProxyOld::mysql_result($result52,0,"acct_name");
          $accountnum_from=@Treat_DB_ProxyOld::mysql_result($result52,0,"accountnum");

          $accountnum_from=substr($accountnum_from,0,5);

          $item_apacct_from="$accountnum_from $acct_name_from";

          $query52 = "SELECT acct_name,accountnum FROM acct_payable WHERE acct_payableid = '$item_apacct'";
          $result52 = Treat_DB_ProxyOld::query($query52);

          $acct_name=@Treat_DB_ProxyOld::mysql_result($result52,0,"acct_name");
          $accountnum=@Treat_DB_ProxyOld::mysql_result($result52,0,"accountnum");

          $accountnum=substr($accountnum,0,5);

          $item_apacct="$accountnum $acct_name";

          if($posted==0){$showdelete="<a href=disperse.php?bid=$businessid&cid=$companyid&apinvoiceid=$apinvoiceid&edit=2&id=$item_id&goto=$goto onclick='return delconfirm()'><img src=delete.gif height=16 width=16 border=0 alt='Delete'></a>";}
		  elseif($security_level==9){$showdelete="<a href=disperse.php?bid=$businessid&cid=$companyid&apinvoiceid=$apinvoiceid&edit=2&id=$item_id&goto=$goto onclick=\"if(delconfirm()){return confirm('Really Sure? This Expense Report is already submitted.')} else{return false;}\"><img src=delete.gif height=16 width=16 border=0 alt='Delete'></a>";}
		  else{$showdelete="";}

          $item_amount=money($item_amount);

          $disperse_total+=$item_amount;

          echo "<tr bgcolor=white><td width=5%>$item_comid</td><td width=20%>$item_busid</td><td width=20%>$item_apacct_from</td><td width=20%>$item_apacct</td><td width=20%>&nbsp;$item_note</td><td width=10% align=right>$item_amount</td><td width=5%>$showdelete</td></tr>";
          $num51--;
       }
       $disperse_total=money($disperse_total);

       if($disperse_total!=$total){$showtotal="<font color=red><b>$disperse_total</font>";}
       else{$showtotal="<font color=green><b>$disperse_total</font>";}

       echo "<tr bgcolor=white><td align=right colspan=6>$$showtotal</td><td></td></tr>";


       if($posted==0){

          echo "<tr bgcolor=white><td><form action=disperse.php method=post style=\"margin:0;padding:0;display:inline;\" onSubmit='return disableForm(this);' name='multiform'><input type=hidden name=apinvoiceid value=$apinvoiceid><input type=hidden name=businessid value=$businessid><input type=hidden name=companyid value=$companyid><input type=hidden name=edit value=1><select name=multi_co onchange=\"loadbus()\" id=\"sel_co\">";

          $query51 = "SELECT companyid,code FROM company WHERE companyid != '2' ORDER BY code DESC";
          $result51 = Treat_DB_ProxyOld::query($query51);
          $num51=Treat_DB_ProxyOld::mysql_numrows($result51);

          while($num51>=0){
             $multi_coid=@Treat_DB_ProxyOld::mysql_result($result51,$num51,"companyid");
             $multi_cocode=@Treat_DB_ProxyOld::mysql_result($result51,$num51,"code");

             echo "<option value=$multi_coid>$multi_cocode</option>";

             $num51--;
          }
          echo "</select></td><td><select name=multi_bus onchange=\"loadap()\" id=\"sel_bus\" style=\"width:250px;\"><option value=0></option></select></td><td><select name=from_acct style=\"width:250px;\">";

          //////FROM ACCOUNT
          $query51 = "SELECT distinct apaccountid FROM apinvoicedetail WHERE apinvoiceid = '$apinvoiceid'";
          $result51 = Treat_DB_ProxyOld::query($query51, true);
          $num51=Treat_DB_ProxyOld::mysql_numrows($result51);

          $num51--;
          while($num51>=0){
             $from_acct=@Treat_DB_ProxyOld::mysql_result($result51,$num51,"apaccountid");

             $query52 = "SELECT acct_name,accountnum FROM acct_payable WHERE acct_payableid = '$from_acct'";
             $result52 = Treat_DB_ProxyOld::query($query52);

             $from_acctname=@Treat_DB_ProxyOld::mysql_result($result52,0,"acct_name");
             $from_acctnum=@Treat_DB_ProxyOld::mysql_result($result52,0,"accountnum");

             $from_acctnum=substr($from_acctnum,0,5);
             $from_acctnum="$from_acctnum $from_acctname";

             echo "<option value=$from_acct>$from_acctnum</option>";

             $num51--;
          }

          echo "</select></td><td><select name=multi_acct id=\"sel_acct\" style=\"width:250px;\"><option value=0></option></select></td><td><input type=text size=30 name=item_note maxlength=40></td><td align=right><input type=text size=8 name=amount></td><td><input type=hidden name=goto value=$goto><input type=submit value='Add' onclick=\"return validate(this.form,-1,'$val_day');\" ></form></td></tr>";
       }
       if($posted==0){
          echo "<tr bgcolor=white><td colspan=7 align=right><form action=disperse.php method=post style=\"margin:0;padding:0;display:inline;\" onSubmit='return disableForm(this);'><input type=hidden name=apinvoiceid value=$apinvoiceid><input type=hidden name=businessid value=$businessid><input type=hidden name=companyid value=$companyid><input type=hidden name=edit value=3><input type=hidden name=goto value=$goto><input type=hidden name=is_transfer value=$transfer><input type=submit value='Submit' onclick='return confirmmultiunit()'></form></td></tr>";
       }
       echo "</table><p></center></td></tr>";
    }

////////////////////////////////
/*
    if ($image!=""){$showimage="<font color=red><b>IMAGE LOADED</b></font>";$showload="onclick='return loadpic()'";}
    else {$showimage="";$showload="";}

    echo "<tr><td colspan=2><form enctype='multipart/form-data' action=uploadinvoice.php method=post target='_blank' style=\"margin:0;padding:0;display:inline;\" $showload><input type=hidden name=username value='$user'><input type=hidden name=password value='$pass'><input type=hidden name=companyid value='$companyid'><input type=hidden name=businessid value='$businessid'><input type=hidden name=apinvoiceid value='$apinvoiceid'><input type=hidden name=MAX_FILE_SIZE value='500000' /></td></tr>";
    echo "<tr bgcolor=#CCCCCC><td align=right>Invoice Files:</td><td><input type=file name=uploadedfile size=50> <input type=submit value='Load Invoice' DISABLED></td></tr>";

    echo "<tr bgcolor=#CCCCCC><td bgcolor=#E7E8E8></form></td><td><form action=viewinvoice.php method=post target='_blank'><input type=hidden name=username value='$user'><input type=hidden name=password value='$pass'><input type=hidden name=companyid value='$companyid'><input type=hidden name=businessid value='$businessid'><input type=hidden name=apinvoiceid value='$apinvoiceid'><input type=submit value='View Invoice'> $showimage</form></td></tr>";
*/
    echo "</td></tr></table></center>";
  }
  else {
/*
    if ($image!=""){$showimage="<font color=red><b>IMAGE LOADED</b></font>";$showload="onclick='return loadpic()'";}
    else {$showimage="";$showload="";}

    echo "<tr bgcolor=#CCCCCC><td bgcolor=#E7E8E8></td><td><form action=viewinvoice.php method=post target='_blank' style=\"margin:0;padding:0;display:inline;\"><input type=hidden name=username value='$user'><input type=hidden name=password value='$pass'><input type=hidden name=companyid value='$companyid'><input type=hidden name=businessid value='$businessid'><input type=hidden name=apinvoiceid value='$apinvoiceid'><input type=submit value='View Invoice'> $showimage</td></tr>";
*/
    echo "</form></td></tr></table></center>";
  }

    echo "<DIV ID=testdiv1 STYLE=position:absolute;visibility:hidden;background-color:white;layer-background-color:white;></DIV>";
//////////////////////////////////////////////END////////////////////////////////////////////////////////////

    echo "</td></tr></table></center></body>";
  }

}
//mysql_close();
google_page_track();
?>

