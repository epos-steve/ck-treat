<?php

function dayofweek($date1)
{
	$day=substr($date1,8,2);
	$month=substr($date1,5,2);
	$year=substr($date1,0,4);
	$dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
	return $dayofweek;
}

function money($diff){
	$findme='.';
	$double='.00';
	$single='0';
	$double2='00';
	$pos1 = strpos($diff, $findme);
	$pos2 = strlen($diff);
	if ($pos1==""){$diff="$diff$double";}
	elseif ($pos2-$pos1==2){$diff="$diff$single";}
	elseif ($pos2-$pos1==1){$diff="$diff$double2";}
	else{}
	$dot = strpos($diff, $findme);
	$diff = substr($diff, 0, $dot+4);
	$diff=round($diff, 2);
	if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
	elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
	$diff = substr($diff, 0, $dot+3);
	$pos1 = strpos($diff, $findme);
	$pos2 = strlen($diff);
	if ($pos1==""){$diff="$diff$double";}
	elseif ($pos2-$pos1==2){$diff="$diff$single";}
	elseif ($pos2-$pos1==1){$diff="$diff$double2";}
	else{}
	return $diff;
}

function nextday($date2)
{
	$day=substr($date2,8,2);
	$month=substr($date2,5,2);
	$year=substr($date2,0,4);
	$leap = date("L");

	if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
	{
		if ($month == "01"){$month="02";}
		elseif ($month == "03"){$month="04";}
		elseif ($month == "05"){$month="06";}
		elseif ($month == "07"){$month="08";}
		elseif ($month == "08"){$month="09";}
		elseif ($month == "10"){$month="11";}
		$day='01';    
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($month=='02' && $day == '29' && $leap == '1')
	{
		$month='03';
		$day='01';
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($month=='02' && $day == '28' && $leap == '0')
	{
		$month='03';
		$day='01';
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
	{
		if ($month == "04"){$month="05";}
		if ($month == "06"){$month="07";}
		if ($month == "09"){$month="10";}
		if ($month == "11"){$month="12";}
		$day='01';
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($month=='12' && $day=='31')
	{
		$day='01';
		$month='01';
		$year++;
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else
	{
		$day=$day+1;
		if ($day<10){$day="0$day";}
		$tomorrow="$year-$month-$day";
		return $tomorrow;
	}
}

function prevday($date2) {
	$day=substr($date2,8,2);
	$month=substr($date2,5,2);
	$year=substr($date2,0,4);
	$day=$day-1;
	$leap = date("L");

	if ($day <= 0)
	{
		if ($month == 01)
		{
			$month = '12';
			$year--;
			$day=$day+31;
			$yesterday = "$year-$month-$day";
			return $yesterday;
		}
		else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
		{
			$month--;
			if ($month < 10)
			{
				$month="0$month";
			}
			$day=$day+31;
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
		else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
		{
			$month--;
			if ($month < 10)
			{
				$month="0$month";
			}
			$day=$day+30;
			$yesterday="$year-$month-$day";
			return $yesterday;
		}

		elseif ($leap==1&&$month=='03')
		{
			$day=$day+29;
			$month='02';
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
		else
		{
			$day=$day+28;
			$month='02';
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
	}
	else
	{
		if ($day < 10)
		{
			$day="0$day";
		}
		$yesterday="$year-$month-$day";
		return $yesterday;
	}
}

function futureday($num) {
	$day = date("d");
	$year = date("Y");
	$month = date("m");
	$leap = date("L");
	$day=$day+$num;

	if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
	{
		if ($month == "03"){$month="04";}
		if ($month == "05"){$month="06";}
		if ($month == "07"){$month="08";}
		if ($month == "08"){$month="09";}
		$day=$day-31;  
		if ($day<10){$day="0$day";} 
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($month=='02' && $leap == '1' && $day >= '29')
	{
		$month='03';
		$day=$day-29;
		if ($day<10){$day="0$day";} 
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($month=='02' && $leap == '0' && $day >= '28')
	{
		$month='03';
		$day=$day-28;
		if ($day<10){$day="0$day";} 
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
	{
		if ($month == "04"){$month="05";}
		if ($month == "06"){$month="07";}
		if ($month == "09"){$month="10";}
		if ($month == "11"){$month="12";}
		$day=$day-30;
		if ($day<10){$day="0$day";} 
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($month==12 && $day>=32)
	{
		$day=$day-31;
		if ($day<10){$day="0$day";} 
		$month='01';
		$year++;
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else
	{
		if ($day<10){$day="0$day";}
		$tomorrow="$year-$month-$day";
		return $tomorrow;
	}
}

function pastday($num) {
	$day = date("d");
	$day=$day-$num;
	if ($day <= 0)
	{
		$year = date("Y");
		$month = date("m");
		if ($month == 01)
		{
			$month = '12';
			$year--;
			$day=$day+31;
			$yesterday = "$year-$month-$day";
			return $yesterday;
		}
		else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
		{
			$month--;
			if ($month < 10)
			{
				$month="0$month";
			}
			$day=$day+31;
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
		else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
		{
			$month--;
			if ($month < 10)
			{
				$month="0$month";
			}
			$day=$day+30;
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
		else
		{
			$day=$day+28;
			$month='02';
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
	}
	else
	{
		if ($day < 10)
		{
			$day="0$day";
		}
		$year=date("Y");
		$month=date("m");
		$yesterday="$year-$month-$day";
		return $yesterday;
	}
}


if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
require_once( DOC_ROOT . '/bootstrap.php' );

$numdays = null; # Notice: Undefined variable
$eid = null; # Notice: Undefined variable
$editlogin = null; # Notice: Undefined variable
$showsubmit = null; # Notice: Undefined variable
$kiosktotal = null; # Notice: Undefined variable
$totalpayroll = null; # Notice: Undefined variable
$is_disabled = null; # Notice: Undefined variable
$nolastweek = null; # Notice: Undefined variable

#<editor-fold defaultstate="collapsed" desc="beginning variable declarations">
$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");

$user = Treat_Controller_Abstract::getSessionCookieVariable( 'usercook' );
$pass = Treat_Controller_Abstract::getSessionCookieVariable( 'passcook' );
$view = Treat_Controller_Abstract::getSessionCookieVariable( 'viewcook' );
$sort_ap = Treat_Controller_Abstract::getSessionCookieVariable( 'sort_ap' );
$subtotal = Treat_Controller_Abstract::getSessionCookieVariable( 'subtotal' );
$date6 = Treat_Controller_Abstract::getSessionCookieVariable( 'date6cook' );
$autosave = Treat_Controller_Abstract::getPostOrGetVariable( 'autosave' );
if ( $autosave == '' ) {
	$autosave = Treat_Controller_Abstract::getSessionCookieVariable( 'autosave' );

}
$businessid = Treat_Controller_Abstract::getPostOrGetVariable( 'bid' );
$companyid = Treat_Controller_Abstract::getPostOrGetVariable( 'cid' );
$date1 = Treat_Controller_Abstract::getPostOrGetVariable( 'date1' );
$error = Treat_Controller_Abstract::getPostOrGetVariable( 'error' );

if ( $businessid == "" || $companyid == "" ) {
	$businessid = Treat_Controller_Abstract::getPostOrGetVariable( 'businessid' );
	$companyid = Treat_Controller_Abstract::getPostOrGetVariable( 'companyid' );
	$numdays = Treat_Controller_Abstract::getPostOrGetVariable( 'numdays' );
	$date1 = Treat_Controller_Abstract::getPostOrGetVariable( 'date1' );
}

if ( $date6 != "" && $date1 == "" ) {
	$date1 = $date6;
}
if ( $numdays == "" ) {
	$numdays = 7;
}

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query( $query );
$num = @mysql_numrows( $result );
//mysql_close();

$security_level = @mysql_result( $result, 0, 'security_level' );
$mysecurity = @mysql_result( $result, 0, 'security' );
$bid = @mysql_result( $result, 0, 'businessid' );
$cid = @mysql_result( $result, 0, 'companyid' );
$bid2 = @mysql_result( $result, 0, 'busid2' );
$bid3 = @mysql_result( $result, 0, 'busid3' );
$bid4 = @mysql_result( $result, 0, 'busid4' );
$bid5 = @mysql_result( $result, 0, 'busid5' );
$bid6 = @mysql_result( $result, 0, 'busid6' );
$bid7 = @mysql_result( $result, 0, 'busid7' );
$bid8 = @mysql_result( $result, 0, 'busid8' );
$bid9 = @mysql_result( $result, 0, 'busid9' );
$bid10 = @mysql_result( $result, 0, 'busid10' );
$loginid = @mysql_result( $result, 0, 'loginid' );

$pr1 = @mysql_result( $result, 0, 'payroll' );
$pr2 = @mysql_result( $result, 0, 'pr2' );
$pr3 = @mysql_result( $result, 0, 'pr3' );
$pr4 = @mysql_result( $result, 0, 'pr4' );
$pr5 = @mysql_result( $result, 0, 'pr5' );
$pr6 = @mysql_result( $result, 0, 'pr6' );
$pr7 = @mysql_result( $result, 0, 'pr7' );
$pr8 = @mysql_result( $result, 0, 'pr8' );
$pr9 = @mysql_result( $result, 0, 'pr9' );
$pr10 = @mysql_result( $result, 0, 'pr10' );
#</editor-fold>

if ( $num != 1 || ( $security_level==1 && ( $bid != $businessid || $cid != $companyid ) ) || $user == "" || $pass == "")
{
	echo "<center><h3>Failed</h3>Use your browser's back button to try again.</center>";
}
else
{
	$dayname = dayofweek( $date6 );

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM company WHERE companyid = '$companyid'";
	$result = Treat_DB_ProxyOld::query( $query );
	//mysql_close();

	$companyname = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'companyname' );
	$week_end = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'week_end' );
	$com_logo=@Treat_DB_ProxyOld::mysql_result($result,0,"logo");

	if($com_logo == ""){$com_logo = "talogo.gif";}

	if ( $date1 != "" ) {
		$today = $date1;
	}

	while ( dayofweek( $today ) != $week_end ) {
		$today = nextday( $today );
	}
	$date1 = $today;

	$startdate = $date1;
	for ( $mycount1 = 1; $mycount1 <= 6; $mycount1++ ) {
		$startdate = prevday( $startdate );
	}

	$prevperiod = prevday( $startdate );

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM security WHERE securityid = '$mysecurity'";
	$result = Treat_DB_ProxyOld::query( $query );
	//mysql_close();

	$sec_bus_sales = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_sales' );
	$sec_bus_invoice = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_invoice' );
	$sec_bus_order = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_order' );
	$sec_bus_payable = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_payable' );
	$sec_bus_inventor1 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_inventor1' );
	$sec_bus_control = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_control' );
	$sec_bus_menu = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_menu' );
	$sec_bus_order_setup = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_order_setup' );
	$sec_bus_request = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_request' );
	$sec_route_collection = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'route_collection' );
	$sec_route_labor = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'route_labor' );
	$sec_route_detail = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'route_detail' );
	$sec_route_machine = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'route_machine' );
	$sec_bus_labor = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_labor' );
	$sec_bus_budget = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_budget' );

	if ( $sec_route_collection < 1 && $sec_route_labor < 1 && $sec_route_detail < 1 ) {
		$location = '/ta/businesstrack.php';
	}
	elseif ( $sec_route_collection > 0 && $sec_route_labor < 1 ) {
		$location = sprintf( '/ta/busroute_collect.php?bid=%d&cid=%d', $businessid, $companyid );
	}
	elseif ( $sec_route_labor < 1 && $sec_route_detail > 0 ) {
		$location = sprintf( '/ta/busroute_detail.php?bid=%d&cid=%d', $businessid, $companyid );
	}
	if ( isset( $location ) && $location && !is_array( $location ) ) {
		header( 'Location: ' . $location );
	}

/*
<!DOCTYPE html
	PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
*/
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title>Treat America :: busroute_labor.php</title>
<style type="text/css">
#loading {
	width: 200px;
	height: 48px;
	background-color: transparent;
	position: absolute;
	left: 50%;
	top: 50%;
	margin-top: -50px;
	margin-left: -100px;
	text-align: center;
}
</style>
		<script type="text/javascript" src="/assets/js/calendarPopup.js"></script>
		<link type="text/css" rel="stylesheet" href="/assets/css/calendarPopup.css" />

<script type="text/javascript">
<!-- Web Site:  http://dynamicdrive.com -->

<!-- This script and many more are available free online at -->
<!-- The JavaScript Source!! http://javascript.internet.com -->

<!-- Begin
function disableForm(theform) {
if (document.all || document.getElementById) {
for (i = 0; i < theform.length; i++) {
var tempobj = theform.elements[i];
if (tempobj.type.toLowerCase() == "submit" || tempobj.type.toLowerCase() == "reset")
tempobj.disabled = true;
}

}
else {
alert("The form has been submitted.  But, since you're not using IE 4+ or NS 6, the submit button was not disabled on form submission.");
return false;
	}
}
//  End -->
</script>

<script type="text/javascript" language=javascript><!--
function confirm1(){return confirm('ARE YOU SURE YOU WANT TO SUBMIT?');}
// --></script>

<script type="text/javascript">
function checkKey(){

var key = event.keyCode;
var DaIndexId = event.srcElement.IndexId-0
if(key==38) {
document.getElementByIndex(DaIndexId-1).focus();
return false;
}
else if(key==40) {
document.getElementByIndex(DaIndexId+1).focus();
return false;
}
else if(key==13) {
document.getElementByIndex(DaIndexId+1).focus();
return false;
}
else if(key==37) {
document.getElementByIndex(DaIndexId-1000).focus();
return false;
}
else if(key==39) {
document.getElementByIndex(DaIndexId+1000).focus();
return false;
}
return true
} // end checkKey function
document.getElementByIndex=function(){
for(var i=0;i<document.all.length;i++)
if(document.all[i].IndexId==arguments[0])
return document.all[i]
return null
}
</script>

<script type="text/javascript">
function checkKey2(){

var key = event.keyCode;
var DaIndexId = event.srcElement.IndexId-0
if(key==37) {
document.getElementByIndex(DaIndexId-1000).focus();
return false;
}
else if(key==39) {
document.getElementByIndex(DaIndexId+1000).focus();
return false;
}

return true
} // end checkKey function
document.getElementByIndex=function(){
for(var i=0;i<document.all.length;i++)
if(document.all[i].IndexId==arguments[0])
return document.all[i]
return null
}
</script>

<script type="text/javascript">
function changePage(newLoc)
 {
	nextPage = newLoc.options[newLoc.selectedIndex].value
		
	if (nextPage != "")
	{
	  document.location.href = nextPage
	}
 }
</script>

<script type="text/javascript" src="javascripts/prototype.js"></script>

<script type="text/javascript">
window.onload = function(){
	new headerScroll('test',{head: 'header'});
} 
</script>

<script type="text/javascript"><!--
function submit2(){return confirm('After you save, MAKE SURE you Update Payroll on the Route Detail Page.');}
// --></script>

		<link type="text/css" rel="stylesheet" href="/assets/css/style.css" />
	</head>

<?php
	$goprev = $date1;
	for ( $counter = 1; $counter <= 7; $counter++ ) {
		$goprev = prevday( $goprev );
	}
	$gonext = nextday( $date1 );

?>
	<body>
		<div style="padding: 0 5%">
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
					<tr>
						<td colspan="2">
							<a href="/ta/businesstrack.php"
								><img src="/ta/logo.jpg" border="0" height="43" width="205" alt=""
							/></a>
							<p></p>
						</td>
					</tr>
<?php

	$query = "SELECT * FROM business WHERE companyid = '$companyid' AND businessid = '$businessid'";
	$result = Treat_DB_ProxyOld::query( $query );

	$businessname = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'businessname' );
	$businessid = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'businessid' );
	$street = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'street' );
	$city = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'city' );
	$state = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'state' );
	$zip = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'zip' );
	$phone = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'phone' );
	$districtid = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'districtid' );
	$bus_unit = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'unit' );

	//echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";
?>


					<tr bgcolor="#ccccff">
						<td width="1%">
							<img src="logo/<?php echo $com_logo; ?>" />
						</td>
						<td>
							<font size="4">
								<b><?php echo $businessname; ?> #<?php echo $bus_unit; ?></b>
							</font>
							<br/><?php echo $street; ?> 
							<br/><?php echo $city; ?>, <?php echo $state; ?> <?php echo $zip; ?> 
							<br/><?php echo $phone; ?> 
						</td>
						<td align="right" valign="top">
							<br/>
							<form action="/ta/editbus.php" method="post" name="store">
								<div>
									<input type="hidden" name="username" value="<?php echo $user; ?>" />
									<input type="hidden" name="password" value="<?php echo $pass; ?>" />
									<input type="hidden" name="businessid" value="<?php echo $businessid; ?>" />
									<input type="hidden" name="companyid" value="<?php echo $companyid; ?>" />
									<input type="submit" value=" Store Setup " />
								</div>
							</form>
						</td>
					</tr>

<?php
	//echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";
?>

					<tr>
						<td colspan="3">
							<font color="#999999">

<?php
	if ( $sec_bus_sales > 0 ) {
		echo "<a href=busdetail.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Sales</font></a>";
	}
	if ( $sec_bus_invoice > 0 ) {
		echo " | <a href=businvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Invoicing</font></a>";
	}
	if ( $sec_bus_order > 0 ) {
		echo " | <a href=buscatertrack.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Orders</font></a>";
	}
	if ( $sec_bus_payable > 0 ) {
		echo " | <a href=busapinvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Payables</font></a>";
	}
	if ( $sec_bus_inventor1 > 0 ) {
		echo " | <a href=businventor.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Inventory</font></a>";
	}
	if ( $sec_bus_control > 0 ) {
		echo " | <a href=buscontrol.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Control Sheet</font></a>";
	}
	if ( $sec_bus_menu > 0 ) {
		echo " | <a href=buscatermenu.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Menus</font></a>";
	}
	if ( $sec_bus_order_setup > 0 ) {
		echo " | <a href=buscaterroom.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Order Setup</font></a>";
	}
	if ( $sec_bus_request > 0 ) {
		echo " | <a href=busrequest.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Requests</font></a>";
	}
	if ( $sec_route_collection > 0 || $sec_route_labor > 0 || $sec_route_detail > 0 ) {
		echo " | <a href=busroute_collect.php?bid=$businessid&cid=$companyid><font color=red onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='red' style='text-decoration:none'>Route Collections</font></a>";
	}
	if ( $sec_bus_labor > 0 ) {
		echo " | <a href=buslabor2.php?bid=$businessid&cid=$companyid&newdate=$date1><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Labor</font></a>";
	}
	if (
		$sec_bus_budget > 0
		|| ( $businessid == $bid  && $pr1 == 1 )
		|| ( $businessid == $bid2 && $pr2 == 1 )
		|| ( $businessid == $bid3 && $pr3 == 1 )
		|| ( $businessid == $bid4 && $pr4 == 1 )
		|| ( $businessid == $bid5 && $pr5 == 1 )
		|| ( $businessid == $bid6 && $pr6 == 1 )
		|| ( $businessid == $bid7 && $pr7 == 1 )
		|| ( $businessid == $bid8 && $pr8 == 1 )
		|| ( $businessid == $bid9 && $pr9 == 1 )
		|| ( $businessid == $bid10 && $pr10 == 1 )
	) {
		echo " | <a href=busbudget.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Budget</font></a>";
	}
?>

						</td>
					</tr>
				</table>
			<p></p>

<?php

	if ( $security_level > 0 ) {
?>


			<p></p>
				<table width="100%">
					<tr>
						<td width="50%">
							<form action="/ta/busrequest.php" method="post">
								<div>
								<select name="gobus" onChange="changePage(this.form.gobus)">

<?php

		$districtquery = "";
		if ( $security_level > 2 && $security_level < 7 ) {
			//////mysql_connect($dbhost,$username,$password);
			//////@mysql_select_db($database) or die( "Unable to select database");
			$query = sprintf( '
					SELECT *
					FROM login_district
					WHERE loginid = "%d"
				'
				,$loginid
			);
			$result = Treat_DB_ProxyOld::query( $query );
			$num = @Treat_DB_ProxyOld::mysql_numrows( $result );
			//////mysql_close();

			$num--;
			while ( $num >= 0 ) {
				$districtid = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'districtid' );
				if ( $districtquery == "" ) {
					$districtquery = "districtid = '$districtid'";
				}
				else {
					$districtquery = "$districtquery OR districtid = '$districtid'";
				}
				$num--;
			}
		}

		//////mysql_connect($dbhost,$username,$password);
		//////@mysql_select_db($database) or die( "Unable to select database");
		if ( $security_level > 6 ) {
			$query = sprintf( '
					SELECT *
					FROM business
					WHERE companyid = "%d"
					ORDER BY businessname DESC
				'
				,$companyid
			);
		}
		elseif ( $security_level == 2 || $security_level == 1 ) {
			$query = sprintf( '
					SELECT *
					FROM business
					WHERE companyid = "%d"
						AND (
							businessid = "%d"
							OR businessid = "%d"
							OR businessid = "%d"
							OR businessid = "%d"
							OR businessid = "%d"
							OR businessid = "%d"
							OR businessid = "%d"
							OR businessid = "%d"
							OR businessid = "%d"
							OR businessid = "%d"
						)
					ORDER BY businessname DESC
				'
				,$companyid
				,$bid
				,$bid2
				,$bid3
				,$bid4
				,$bid5
				,$bid6
				,$bid7
				,$bid8
				,$bid9
				,$bid10
			);
		}
		elseif ( $security_level > 2 && $security_level < 7 ) {
			$query = sprintf( '
					SELECT *
					FROM business
					WHERE companyid = "%d"
					AND (%s)
					ORDER BY businessname DESC
				'
				,$companyid
				,$districtquery
			);
		}
		$result = Treat_DB_ProxyOld::query( $query );
		$num = @Treat_DB_ProxyOld::mysql_numrows( $result );
		//////mysql_close();

		$num--;
		while ( $num >= 0 ) {
			$businessname = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'businessname' );
			$busid = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'businessid' );

			if ( $busid == $businessid ) {
				$show = ' selected="selected"';
			}
			else {
				$show = "";
			}

?>

									<option value="/ta/busroute_labor.php?bid=<?php
										echo $busid;
									?>&amp;cid=<?php
										echo $companyid;
									?>&amp;date1=<?php
										echo $date1;
									?>" <?php echo $show; ?>><?php
										echo $businessname;
									?></option>

<?php
			$num--;
		}

?>

								</select>
								</div>
							</form>
						</td>
						<td></td>
						<td width="50%" align="right">
							<iframe src="/ta/showtime.php" width="100%" height="24" frameborder="0"></iframe>
						</td>
					</tr>
				</table>
			<p></p>
<?php
	}
	
?>
				<table width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="6">
							<table cellspacing="0" cellpadding="0" width="100%">
								<tr bgcolor="#cccccc" valign="top">
									<td width="1%"><img src="/ta/leftcrn2.jpg" height="6" width="6" align="top" /></td>
									<td width="13%">
										<center>
											<a href="/ta/busroute_collect.php?cid=<?php
												echo $companyid;
											?>&amp;bid=<?php
												echo $businessid;
											?>" style="<?php
												echo $style;
											?>">
												<font color="blue">
													Collection Sheet
												</font>
											</a>
										</center>
									</td>
									<td width="1%" align="right"><img src="/ta/rightcrn2.jpg" height="6" width="6" align="top" /></td>
									<td width="1%" bgcolor="white"></td>
									<td width="1%"><img src="/ta/leftcrn2.jpg" height="6" width="6" align="top" /></td>
									<td width="13%">
										<center>
											<a href="/ta/busroute_balance.php?cid=<?php
												echo $companyid;
											?>&amp;bid=<?php
												echo $businessid;
											?>" style="<?php
												echo $style;
											?>">
												<font color="blue">
													Balance Sheet
												</font>
											</a>
										</center>
									</td>
									<td width="1%" align="right"><img src="/ta/rightcrn2.jpg" height="6" width="6" align="top" /></td>
									<td width="1%" bgcolor="white"></td>
									<td width="1%"><img src="/ta/leftcrn2.jpg" height="6" width="6" align="top" /></td>
									<td width="13%">
										<center>
											<a href="/ta/busroute_report.php?cid=<?php
												echo $companyid;
											?>&amp;bid=<?php
												echo $businessid;
											?>" style="<?php
												echo $style;
											?>">
												<font color="blue">
													Reporting
												</font>
											</a>
										</center>
									</td>
									<td width="1%" align="right"><img src="/ta/rightcrn2.jpg" height="6" width="6" align="top" /></td>
									<td width="1%" bgcolor="white"></td>
									<td width="1%" bgcolor="#e8e7e7"><img src="/ta/leftcrn.jpg" height="6" width="6" align="top" /></td>
									<td width="13%" bgcolor="#e8e7e7">
										<center>
											<a href="/ta/busroute_labor.php?cid=<?php
												echo $companyid;
											?>&amp;bid=<?php
												echo $businessid;
											?>" style="<?php
												echo $style;
											?>">
												<font color="black">
													Payroll/Route Linking
												</font>
											</a>
										</center>
									</td>
									<td width="1%" align="right" bgcolor="#e8e7e7"><img src="/ta/rightcrn.jpg" height="6" width="6" align="top" /></td>
									<td width="1%" bgcolor="white"></td>
									<td width="1%"><img src="/ta/leftcrn2.jpg" height="6" width="6" align="top" /></td>
									<td width="13%">
										<center>
											<a href="/ta/busroute_detail.php?cid=<?php
												echo $companyid;
											?>&amp;bid=<?php
												echo $businessid;
											?>&date1=<?php
												echo $date1;
											?>" style="<?php
												echo $style;
											?>">
												<font color="blue">
													Route Details
												</font>
											</a>
										</center>
									</td>
									<td width="1%" align="right"><img src="/ta/rightcrn2.jpg" height="6" width="6" align="top" /></td>
									<td width="1%" bgcolor="white"></td>
									<td width="1%"><img src="/ta/leftcrn2.jpg" height="6" width="6" align="top" /></td>
									<td width="13%">
										<center>
											<a href="/ta/busroute_machine.php?cid=<?php
												echo $companyid;
											?>&amp;bid=<?php
												echo $businessid;
											?>" style="<?php
												echo $style;
											?>">
												<font color="blue">
													Machine Mgmt
												</font>
											</a>
										</center>
									</td>
									<td width="1%" align="right"><img src="/ta/rightcrn2.jpg" height="6" width="6" align="top" /></td>
									<td bgcolor="white" width="25%"></td>
								</tr>
								<tr height="2">
									<td colspan="4"></td>
									<td colspan="4"></td>
									<td colspan="4"></td>
									<td colspan="3" bgcolor="#e8e7e7"></td>
									<td></td>
									<td colspan="4"></td>
									<td colspan="4"></td>
									<td colspan="1"></td>
								</tr>
							</table>
						</td>
					</tr>

<?php

	$query34 = sprintf( '
			SELECT *
			FROM submit_labor
			WHERE businessid = "%d"
				AND date = "%s"
		'
		,$businessid
		,$date1
	);
	$result34 = Treat_DB_ProxyOld::query( $query34 );
	$num34 = @Treat_DB_ProxyOld::mysql_numrows( $result34 );

	$is_exported = @Treat_DB_ProxyOld::mysql_result( $result34, 0, 'export' );

	if ( $num34 > 0 && $security_level < 9 && $is_exported == 1 ) {
		$is_disabled = ' disabled="disabled"';
	}
	if ( $num34 > 0 ) {
		$showsubmit = "<font color=red><b>SUBMITTED</b></font>";
	}

?>

					<tr bgcolor="#e8e7e7">
						<td colspan="6">
							<form action="/ta/busroute_labor.php" method="post">
								<div>
								<font size="1">
									[<a href="/ta/busroute_labor.php?cid=<?php
										echo $companyid;
									?>&amp;bid=<?php
										echo $businessid;
									?>&amp;<?php
										echo $eid; 
									?>=<?php
										echo $editlogin;
									?>&amp;date1=<?php
										echo $goprev;
									?>">
										<font color="blue">
											PREV
										</font>
									</a>]
								</font>
								<b>Week ending:</b>
								<input type="hidden" name="businessid" value="<?php echo $businessid; ?>" />
								<input type="hidden" name="companyid" value="<?php echo $companyid; ?>" />
								<script type="text/javascript" id="js18">
									var cal18 = new CalendarPopup('testdiv1');
									cal18.setCssPrefix('TEST');
								</script>
								<a href="#"
									onClick="cal18.select(document.forms[2].date1,'anchor18','yyyy-MM-dd'); return false;"
									title="cal18.select(document.forms[2].date1,'anchor1x','yyyy-MM-dd'); return false;"
									name="anchor18"
									id="anchor18"
									class="old_calendar_popup"
								>
									<img src="/ta/calendar.gif" border="0" height="16" width="16" alt="Choose a Date" />
								</a>
								<input type="text" name="date1" value="<?php echo $date1; ?>" size="8" />
								<input type="submit" value="GO" />
								<?php echo $showsubmit; ?> 
								<font size="1">
									[<a href="/ta/busroute_labor.php?cid=<?php
										echo $companyid;
									?>&amp;bid=<?php
										echo $businessid;
									?>&amp;<?php
										echo $eid;
									?>=<?php
										echo $editlogin;
									?>&amp;date1=<?php
										echo $gonext;
									?>">
										<font color="blue">
											NEXT
										</font>
									</a>]
								</font>
								</div>
							</form>
						</td>
						<td align="right"></td>
					</tr>
					<tr bgcolor="black" height="1">
						<td colspan="7"></td>
					</tr>
					<tr>
						<td colspan="7">
							<form action="/ta/labor_route_link.php" method="post"
								onSubmit="return disableForm(this);"
							>
								<div>
								<input type="hidden" name="date" value="<?php echo $date1; ?>" />
								<input type="hidden" name="businessid" value="<?php echo $businessid; ?>" />
								<input type="hidden" name="companyid" value="<?php echo $companyid; ?>" />
								<table width="100%" border="0" cellspacing="0" cellpadding="0" id="test">
<?php

	/////////////////////START TABLE

	$counter = 1;
	$freevendtotal = 0;
	$noncashtotal = 0;
	$csvtotal = 0;
	$weekendtotal = 0;
	$salestotal = 0;

	$query33 = sprintf( '
			SELECT l.firstname
				,l.lastname
				,l.empl_no
				,l.loginid
				,jtd.jobtype
				,jtd.rate
			FROM login l
				JOIN jobtypedetail jtd
					ON jtd.loginid = l.loginid
					AND l.oo2 = "%1$d"
					AND l.is_deleted = "0"
					# previously did not check the jobtypedetail is_deleted field
					AND jtd.is_deleted = "0"
				JOIN jobtype jt
					ON jt.jobtypeid = jtd.jobtype
					AND jt.commission = "1"
			WHERE l.oo2 = "%1$d"
			ORDER BY l.lastname DESC
		'
		,$businessid
	);
	$result33 = Treat_DB_ProxyOld::query( $query33 );
	$num33 = @Treat_DB_ProxyOld::mysql_numrows( $result33 );

	$query = sprintf( '
			SELECT *
			FROM vend_locations
			WHERE unitid = "%d"
		'
		,$businessid
	);
	$result = Treat_DB_ProxyOld::query( $query );
	$num = @Treat_DB_ProxyOld::mysql_numrows( $result );

	$arrow = 0;
	$num--;
	while ( $num >= 0 ) {
		$location_name = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'location_name' );
		$locationid = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'locationid' );

?>

									<tr bgcolor="#ccccff">
										<td colspan="9"><?php echo $location_name; ?></td>
									</tr>
									<tr bgcolor="#e8e7e7" id="header">
										<td width="6%">
											<font size="2">
												<b>
													Route
												</b>
											</font>&nbsp;
										</td>
										<td align="right" width="10%">
											<font size="2">
												<b>
													Sales
												</b>
											</font>
										</td>
										<td align="right" width="10%">
											<font size="2">
												<b>
													Cashless
												</b>
											</font>
										</td>
										<td align="right" width="10%">
											<font size="2">
												<b>
													Kiosk Sales
												</b>
											</font>
										</td>
										<td align="right" width="10%">
											<font size="2">
												<b>
													FV/Subsidy
												</b>
											</font>
										</td>
										<td align="right" width="10%">
											<font size="2">
												<b>
													CSV
												</b>
											</font>
										</td>
										<td align="right" width="10%">
											<font size="2">
												<b>
													Weekend Pay
												</b>
											</font>
										</td>
										<td align="right" width="10%">
											<font size="2">
												<b>
												</b>
											</font>
										</td>
										<td align="right">
											<font size="2">
												<b>
													Employee
												</b>
											</font>
										</td>
									</tr>
<?php
	
		$query2 = sprintf( '
				SELECT *
				FROM login_route
				WHERE locationid = "%d"
					AND active = "0"
				ORDER BY route DESC
			'
			,$locationid
		);
		$result2 = Treat_DB_ProxyOld::query( $query2 );
		$num2 = @Treat_DB_ProxyOld::mysql_numrows( $result2 );

		$num2--;
		while ( $num2 >= 0 ) {
			$login_routeid = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'login_routeid' );
			$route = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'route' );
			$commissionid = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'commissionid' );

			if ( $commissionid < 1 ) {
				$showerror = '<img src="/ta/error.gif" height="16" width="20" alt="No Commission Type" /> ';
			}
			else {
				$showerror = "";
			}

			$query12 = sprintf( '
					SELECT SUM(route_total) AS totalamt
					FROM vend_route_collection
					WHERE login_routeid = "%d"
						AND date >= "%s"
						AND date <= "%s"
				'
				,$login_routeid
				,$startdate
				,$date1
			);
			$result12 = Treat_DB_ProxyOld::query( $query12 );

			$totalamt = @Treat_DB_ProxyOld::mysql_result( $result12, 0, 'totalamt' );
			$totalamt = money( $totalamt );

			$salestotal += $totalamt;

			$query12 = sprintf( '
					SELECT *
					FROM labor_route_link
					WHERE login_routeid = "%d"
						AND date = "%s"
				'
				,$login_routeid
				,$date1
			);
			$result12 = Treat_DB_ProxyOld::query( $query12 );
			$num12 = @Treat_DB_ProxyOld::mysql_numrows( $result12 );

			if ( $num12 > 0 ) {
				$freevend = @Treat_DB_ProxyOld::mysql_result( $result12, 0, 'freevend' );
				$noncash = @Treat_DB_ProxyOld::mysql_result( $result12, 0, 'noncash' );
				$kiosk = @Treat_DB_ProxyOld::mysql_result( $result12, 0, 'kiosk' );
				$csv = @Treat_DB_ProxyOld::mysql_result( $result12, 0, 'csv' );
				$employee = @Treat_DB_ProxyOld::mysql_result( $result12, 0, 'loginid' );
				$weekend = @Treat_DB_ProxyOld::mysql_result( $result12, 0, 'weekend_pay' );
				$showprev = "";
				$nolastweek = 1;
			}
			else {
				$query12 = sprintf( '
						SELECT *
						FROM labor_route_link
						WHERE login_routeid = "%d"
							AND date = "%s"
					'
					,$login_routeid
					,$prevperiod
				);
				$result12 = Treat_DB_ProxyOld::query( $query12 );
				$num12 = @Treat_DB_ProxyOld::mysql_numrows( $result12 );

				if ( $num12 > 0 && $nolastweek != 1 ) {
					$freevend = "";
					$noncash = "";
					$kiosk = "";
					$csv = "";
					$weekend = "";
					$employee = @Treat_DB_ProxyOld::mysql_result( $result12, 0, 'loginid' );
					$showprev = "<font color=red><b>*</b></font>";
				}
				else {
					$freevend = "";
					$noncash = "";
					$kiosk = "";
					$csv = "";
					$weekend = "";
					$employee = "";
					$showprev = "";
				}
			}

			/////////////CALCULATE PAYROLL
			$payroll = 0;

			$query12 = sprintf( '
					SELECT name
						,base
					FROM vend_commission
					WHERE commissionid = "%d"
				'
				,$commissionid
			);
			$result12 = Treat_DB_ProxyOld::query( $query12 );

			$base = @Treat_DB_ProxyOld::mysql_result( $result12, 0, 'base' );
			$commission_name = @Treat_DB_ProxyOld::mysql_result( $result12, 0, 'name' );
/*
			////////SALES
			$query12 = "SELECT * FROM vend_commission_detail WHERE commissionid = '$commissionid' AND type = '1'";
			$result12 = Treat_DB_ProxyOld::query( $query12 );
			$num12 = @mysql_numrows( $result12 );
			$num12--;
			while($num12>=0){
				$lower = @mysql_result( $result12, $num12, 'lower' );
				$upper = @mysql_result( $result12, $num12, 'upper' );
				$calc_type = @mysql_result( $result12, $num12, 'calc_type' );
				$percent = @mysql_result( $result12, $num12, 'percent' );

				if($calc_type==1&&$totalamt>=$lower&&$totalamt<=$upper){$payroll=$totalamt*($percent/100);}
				elseif($calc_type==2&&$freevend>=$lower&&$freevend<=$upper){$payroll=$totalamt*($percent/100);}
				elseif($calc_type==3&&$csv>=$lower&&$csv<=$upper){$payroll=$totalamt*($percent/100);}
				elseif($calc_type==4&&($totalamt+$freevend)>=$lower&&($totalamt+$freevend)<=$upper){$payroll=$totalamt*($percent/100);}
				elseif($calc_type==5&&($freevend+$csv)>=$lower&&($freevend+$csv)<=$upper){$payroll=$totalamt*($percent/100);}
				elseif($calc_type==6&&($totalamt+$csv)>=$lower&&($totalamt+$csv)<=$upper){$payroll=$totalamt*($percent/100);}
				elseif($calc_type==7&&($totalamt+$freevend+$csv)>=$lower&&($totalamt+$freevend+$csv)<=$upper){$payroll=$totalamt*($percent/100);}

				$num12--;
			}
//echo "$payroll,";
			///////////////END SALES
			////////FREEVEND
			$query12 = "SELECT * FROM vend_commission_detail WHERE commissionid = '$commissionid' AND type = '2'";
			$result12 = Treat_DB_ProxyOld::query( $query12 );
			$num12 = @mysql_numrows( $result12 );
			$num12--;
			while($num12>=0){
				$lower = @mysql_result( $result12, $num12, 'lower' );
				$upper = @mysql_result( $result12, $num12, 'upper' );
				$calc_type = @mysql_result( $result12, $num12, 'calc_type' );
				$percent = @mysql_result( $result12, $num12, 'percent' );

				if($calc_type==1&&$totalamt>=$lower&&$totalamt<=$upper){$payroll+=$freevend*($percent/100);}
				elseif($calc_type==2&&$freevend>=$lower&&$freevend<=$upper){$payroll+=$freevend*($percent/100);}
				elseif($calc_type==3&&$csv>=$lower&&$csv<=$upper){$payroll+=$freevend*($percent/100);}
				elseif($calc_type==4&&($totalamt+$freevend)>=$lower&&($totalamt+$freevend)<=$upper){$payroll+=$freevend*($percent/100);}
				elseif($calc_type==5&&($freevend+$csv)>=$lower&&($freevend+$csv)<=$upper){$payroll+=$freevend*($percent/100);}
				elseif($calc_type==6&&($totalamt+$csv)>=$lower&&($totalamt+$csv)<=$upper){$payroll+=$freevend*($percent/100);}
				elseif($calc_type==7&&($totalamt+$freevend+$csv)>=$lower&&($totalamt+$freevend+$csv)<=$upper){$payroll+=$freevend*($percent/100);}

				$num12--;
			}
//echo "$payroll,";
			///////////////END FREEVEND
			////////CSV
			$query12 = "SELECT * FROM vend_commission_detail WHERE commissionid = '$commissionid' AND type = '3'";
			$result12 = Treat_DB_ProxyOld::query( $query12 );
			$num12 = @mysql_numrows( $result12 );
			$num12--;
			while($num12>=0){
				$lower = @mysql_result( $result12, $num12, 'lower' );
				$upper = @mysql_result( $result12, $num12, 'upper' );
				$calc_type = @mysql_result( $result12, $num12, 'calc_type' );
				$percent = @mysql_result( $result12, $num12, 'percent' );

				if($calc_type==1&&$totalamt>=$lower&&$totalamt<=$upper){$payroll+=$csv*($percent/100);}
				elseif($calc_type==2&&$freevend>=$lower&&$freevend<=$upper){$payroll+=$csv*($percent/100);}
				elseif($calc_type==3&&$csv>=$lower&&$csv<=$upper){$payroll+=$csv*($percent/100);}
				elseif($calc_type==4&&($totalamt+$freevend)>=$lower&&($totalamt+$freevend)<=$upper){$payroll+=$csv*($percent/100);}
				elseif($calc_type==5&&($freevend+$csv)>=$lower&&($freevend+$csv)<=$upper){$payroll+=$csv*($percent/100);}
				elseif($calc_type==6&&($totalamt+$csv)>=$lower&&($totalamt+$csv)<=$upper){$payroll+=$csv*($percent/100);}
				elseif($calc_type==7&&($totalamt+$freevend+$csv)>=$lower&&($totalamt+$freevend+$csv)<=$upper){$payroll+=$csv*($percent/100);}

				$num12--;
			}
//echo "$payroll<br>";
			////////////END PAYROLL
*/
			$payroll += $weekend;

			if ( $payroll > $base && $employee > 0 ) {
				$showtotal = number_format( $payroll, 2 );
				$totalpayroll += $payroll;
			}
			elseif ( $employee > 0 ) {
				$showtotal = number_format( $base, 2 );
				$totalpayroll += $base;
			}
			else {
				$showtotal = 0;
			}

			if ( $csv == 0 ) {
				$csv = "";
			}
			if ( $freevend == 0 ) {
				$freevend = "";
			}
			if ( $noncash == 0 ) {
				$noncash = "";
			}
			if ( $kiosk == 0 ) {
				$kiosk = "";
			}
			if ( $weekend == 0 ) {
				$weekend = "";
			}

			$arrow1 = $arrow;
			$arrow2 = $arrow1 + 1000;
			$arrow3 = $arrow1 + 2000;
			$arrow4 = $arrow1 + 3000;
			$arrow++;

			$freevendtotal += $freevend;
			$noncashtotal += $noncash;
			$kiosktotal += $kiosk;
			$csvtotal += $csv;
			$weekendtotal += $weekend;

?>

									<tr
										onMouseOver="this.bgColor='#e8e7e7'"
										onMouseOut="this.bgColor='white'"
										id="row<?php echo $counter; ?>"
									>
										<td title="Commission Type: <?php echo $commission_name; ?>" nowrap="nowrap">
											<?php echo $showerror; ?> 
											<font size="2">
												<?php echo $route; ?> 
											</font>
										</td>
										<td align="right">
											<font size="2">
												<?php echo $totalamt; ?> 
											</font>
										</td>
										<td align="right">
											<input type="text" size="8" name="noncash_<?php
												echo $counter;
											?>" value="<?php
												echo $noncash;
											?>"
												onfocus="<?php echo 'row', $counter; ?>.style.backgroundColor='yellow'; this.select();"
												onblur="<?php echo 'row', $counter; ?>.style.backgroundColor=''"
												onkeydown="return checkKey()"
												IndexId="<?php echo $arrow1; ?>"
											/>
										</td>
										<td align="right">
											<input type="text" size="8" name="kiosk_<?php echo $counter; ?>"
												value="<?php echo $kiosk; ?>"
												onfocus="<?php echo 'row', $counter; ?>.style.backgroundColor='yellow'; this.select();"
												onblur="<?php echo 'row', $counter; ?>.style.backgroundColor=''"
												onkeydown="return checkKey()"
												IndexId="<?php echo $arrow1; ?>"
											/>
										</td>
										<td align="right">
											<input type="text" size="8" name="freevend_<?php echo $counter; ?>"
												value="<?php echo $freevend; ?>"
												onfocus="<?php echo 'row', $counter; ?>.style.backgroundColor='yellow'; this.select();"
												onblur="<?php echo 'row', $counter; ?>.style.backgroundColor=''"
												onkeydown="return checkKey()"
												IndexId="<?php echo $arrow1; ?>"
											/>
										</td>
										<td align="right">
											<input type="text" size="8" name="csv_<?php echo $counter; ?>"
												value="<?php echo $csv; ?>"
												onfocus="<?php echo 'row', $counter; ?>.style.backgroundColor='yellow'; this.select();"
												onblur="<?php echo 'row', $counter; ?>.style.backgroundColor=''"
												onkeydown="return checkKey()"
												IndexId="<?php echo $arrow2; ?>"
											/>
										</td>
										<td align="right">
											<input type="text" size="8" name="weekend_<?php echo $counter; ?>"
												value="<?php echo $weekend; ?>"
												onfocus="<?php echo 'row', $counter; ?>.style.backgroundColor='yellow'; this.select();"
												onblur="<?php echo 'row', $counter; ?>.style.backgroundColor=''"
												onkeydown="return checkKey()"
												IndexId="<?php echo $arrow3; ?>"
											/>
										</td>
										<td align="right">
											<font size="2"></font>
										</td>
										<td align="right" nowrap="nowrap">
											<?php echo $showprev; ?> 
											<font size="2"><?php echo $route; ?></font>
											<select name="employee_<?php echo $counter; ?>"
												onfocus="<?php echo 'row', $counter; ?>.style.backgroundColor='yellow'"
												onblur="<?php echo 'row', $counter; ?>.style.backgroundColor=''"
												onkeydown="return checkKey2()"
												IndexId="<?php echo $arrow4; ?>"
											>
												<option value="0"></option>
<?php

			$temp_num = $num33 - 1;
			while ( $temp_num >= 0 ) {
				$loginid = @Treat_DB_ProxyOld::mysql_result( $result33, $temp_num, 'loginid' );
				$firstname = @Treat_DB_ProxyOld::mysql_result( $result33, $temp_num, 'firstname' );
				$lastname = @Treat_DB_ProxyOld::mysql_result( $result33, $temp_num, 'lastname' );
				$empl_no = @Treat_DB_ProxyOld::mysql_result( $result33, $temp_num, 'empl_no' );

				if ( $employee == $loginid ) {
					$showsel = ' selected="selected"';
				}
				else {
					$showsel = "";
				}

?>

												<option value="<?php
													echo $loginid;
												?>"<?php
													echo $showsel;
												?>><?php
													echo $lastname;
												?>, <?php
													echo $firstname;
												?> (<?php
													echo $empl_no;
												?>)</option>
<?php
?>
<?php

				$temp_num--;
			}

?>

											</select>
										</td>
									</tr>
									<tr bgcolor="#e8e7e7" height="1">
										<td colspan="9"></td>
									</tr>
<?php
			$num2--;
			$counter++;
		}

		$num--;
	}

	$salestotal = number_format( $salestotal, 2 );
	$freevendtotal = number_format( $freevendtotal, 2 );
	$noncashtotal = number_format( $noncashtotal, 2 );
	$kiosktotal = number_format( $kiosktotal, 2 );
	$csvtotal = number_format( $csvtotal, 2 );
	$weekendtotal = number_format( $weekendtotal, 2 );
	$totalpayroll = number_format( $totalpayroll, 2 );
?>

									<tr bgcolor="#ccccff">
										<td colspan="1">
											<input type="submit" value="Save"<?php
												echo $is_disabled;
											?> onclick="return submit2()" />
										</td>
										<td align="right">
											<font size="2">
												<b>
													$<?php echo $salestotal; ?> 
												</b>
											</font>
										</td>
										<td align="right">
											<font size="2">
												<b>
													$<?php echo $noncashtotal; ?> 
												</b>
											</font>
										</td>
										<td align="right">
											<font size="2">
												<b>
													$<?php echo $kiosktotal; ?> 
												</b>
											</font>
										</td>
										<td align="right">
											<font size="2">
												<b>
													$<?php echo $freevendtotal; ?> 
												</b>
											</font>
										</td>
										<td align="right">
											<font size="2">
												<b>
													$<?php echo $csvtotal; ?> 
												</b>
											</font>
										</td>
										<td align="right">
											<font size="2">
												<b>
													$<?php echo $weekendtotal; ?> 
												</b>
											</font>
										</td>
										<td align="right">
											<font size="2">
												<b>
												</b>
											</font>
										</td>
										<td></td>
									</tr>
<?php
	if ( $nolastweek != 1 ) {
?>

									<tr>
										<td colspan="9">
											<font color="red">*</font>Route Driver From Last Week
										</td>
									</tr>
<?php
	}
	/////////////////////END TABLE
?>

								</table>
								</div>
							</form>
						</td>
					</tr>
				</table>

			<div id="testdiv1" style="position:absolute;visibility:hidden;background-color:white;"></div>

			<br/>
			<form action="/ta/businesstrack.php" method="post">
				<div class="button">
					<input type="hidden" name="username" value="<?php echo $user; ?>" />
					<input type="hidden" name="password" value="<?php echo $pass; ?>" />
					<input type="submit" value=" Return to Main Page" />
				</div>
			</form>
		</div>
<?php
	
	google_page_track();
?>
	</body>
</html>
<?php
}
//mysql_close();

