<?php
function nextday($nextd, $day_format = 'Y-m-d' ){ //Function returns next day of passed date and formats accordingly.
	if($day_format==""){$day_format="Y-m-d";}
	$monn = substr($nextd, 5, 2);
	$dayn = substr($nextd, 8, 2);
	$yearn = substr($nextd, 0,4);
	$tempdate = date($day_format , mktime(0,0,0, $monn, $dayn+1, $yearn));
	return $tempdate;
}

function prevday($prevd,$day_format){ //Function returns previous day of passed date and formats accordingly.
	if($day_format==""){$day_format="Y-m-d";}
	$monp = substr($prevd, 5, 2);
	$dayp = substr($prevd, 8, 2);
	$yearp = substr($prevd, 0,4);
	$tempdate = date($day_format , mktime(0,0,0, $monp, $dayp-1, $yearp));
	return $tempdate;
}

function money($diff){
	$findme='.';
	$double='.00';
	$single='0';
	$double2='00';
	$pos1 = strpos($diff, $findme);
	$pos2 = strlen($diff);
	if ($pos1==""){$diff="$diff$double";}
	elseif ($pos2-$pos1==2){$diff="$diff$single";}
	elseif ($pos2-$pos1==1){$diff="$diff$double2";}
	else{}
	$dot = strpos($diff, $findme);
	$diff = substr($diff, 0, $dot+3);
	if ($diff > 0 && $diff < '0.01'){$diff="0.01";}
	elseif($diff < 0 && $diff > '-0.01'){$diff="-0.01";}
	return $diff;
}

function dayofweek($date1)
{
	$day=substr($date1,8,2);
	$month=substr($date1,5,2);
	$year=substr($date1,0,4);
	$dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
	return $dayofweek;
}

if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
/*include_once( 'db.php' );*/
require_once(DOC_ROOT.'/bootstrap.php');

/*$user           = getSessionCookieVariable( 'usercook' );
$companyid      = getSessionCookieVariable( 'compcook' );
$pass           = getSessionCookieVariable( 'passcook' );
$businessid     = getPostOrGetVariable( 'businessid' );
$reporttype     = getPostOrGetVariable( 'reporttype' );
$date1          = getPostOrGetVariable( 'date1' );
$date2          = getPostOrGetVariable( 'date2' );
$account        = getPostOrGetVariable( 'account' );
$label_type     = getPostOrGetVariable( 'label_type' );
$item_only      = getPostOrGetVariable( 'item_only' );*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook');
$companyid = \EE\Controller\Base::getSessionCookieVariable('compcook');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook');
$businessid = \EE\Controller\Base::getPostOrGetVariable('businessid');
$reporttype = \EE\Controller\Base::getPostOrGetVariable('reporttype');
$date1 = \EE\Controller\Base::getPostOrGetVariable('date1');
$date2 = \EE\Controller\Base::getPostOrGetVariable('date2');
$account = \EE\Controller\Base::getPostOrGetVariable('account');
$label_type = \EE\Controller\Base::getPostOrGetVariable('label_type');
$item_only = \EE\Controller\Base::getPostOrGetVariable('item_only');

$day = date( 'd' );
$year = date( 'Y' );
$month = date( 'm' );
$today = "$month/$day/$year";

$weekend = dayofweek( $date1 );

if ( $weekend == "Saturday" || $weekend == "Sunday" ) {
	$date2 = $date1;
}
else{
	$date2 = nextday( $date1 );
	$date2name = dayofweek( $date2 );
	if ( $date2name == "Saturday" ) {
		$date2 = nextday( $date2 );
		$date2 = nextday( $date2 );
	}
	elseif ( $date2name == "Sunday" ) {
		$date2 = nextday( $date2 );
	}
}


//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM caterclose WHERE businessid = '$businessid' AND date = '$date2'";
$result = Treat_DB_ProxyOld::query( $query );
$num = Treat_DB_ProxyOld::mysql_numrows( $result );
//mysql_close();

if ($num>0){
	$date2 = nextday( $date2 );
	$date2name = dayofweek( $date2);
	if ( $date2name == "Saturday" ) {
		$date2 = nextday( $date2 );
		$date2 = nextday( $date2 );
	}
	elseif ( $date2name == "Sunday" ) {
		$date2 = nextday( $date2 );
	}
}

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM caterclose WHERE businessid = '$businessid' AND date = '$date1'";
$result = Treat_DB_ProxyOld::query( $query );
$num = Treat_DB_ProxyOld::mysql_numrows( $result );
//mysql_close();

if ( $num > 0 ) {
	$date2 = $date1;
}

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT username,password,security_level FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query( $query );
$num = Treat_DB_ProxyOld::mysql_numrows( $result );
//mysql_close();
#echo '<pre>';
#var_dump( $_POST );
#echo '</pre>';

if ($num!=1)
{
	echo "<center><h4>Failed, Session Timed Out</h4></center>";
	exit();
}
elseif($label_type==2){
	$weekday=dayofweek($date1);
	
	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM company WHERE companyid = '$companyid'";
	$result = Treat_DB_ProxyOld::query( $query );
	//mysql_close();
	
	$companyname = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'companyname' );
	
	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM business WHERE businessid = '$businessid'";
	$result = Treat_DB_ProxyOld::query( $query );
	//mysql_close();
	
	$bustax = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'tax' );
	$default_menu = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'default_menu' );
	
	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	if ( $account[0] == -1 ) {
		$query1 = "SELECT * FROM accounts WHERE businessid = '$businessid' AND print_labels != 0 ORDER BY name DESC";
	}
	else {
		$myquery="";
		$first="";
		foreach ( $account as $key ) {
			if ( $first == "" ) {
				$myquery = "accountid = '$key'";
				$first = 1;
			}
			else {
				$myquery = "$myquery OR accountid = '$key'";
			}
		}
		$query1 = "SELECT * FROM accounts WHERE ( $myquery ) AND print_labels != 0";
	}
	$result1 = Treat_DB_ProxyOld::query( $query1 );
	$num1 = Treat_DB_ProxyOld::mysql_numrows( $result1 );
	//mysql_close();
	
	$num1--;
	while ( $num1 >= 0 ) {
		$accountid = @Treat_DB_ProxyOld::mysql_result( $result1, $num1, 'accountid' );
		$accountname = @Treat_DB_ProxyOld::mysql_result( $result1, $num1, 'name' );
		$acct_menu = @Treat_DB_ProxyOld::mysql_result( $result1, $num1, 'menu' );
		$assign = @Treat_DB_ProxyOld::mysql_result( $result1, $num1, 'assign' );
		$allow_sub = @Treat_DB_ProxyOld::mysql_result( $result1, $num1, 'allow_sub' );
		$contract_num = @Treat_DB_ProxyOld::mysql_result( $result1, $num1, 'contract_num' );
		if ( $acct_menu == -1 ) {
			$acct_menu = $default_menu;
		}
		
		$daypartname[]="";
		$daypartid[]="";
		$portionname[]="";
		$portionid[]="";
		$custqty[]="";
		
		////////////////CHECK FOR ORDERS
		
		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query10 = "SELECT order_detail.qty FROM order_detail,menu_daypart WHERE order_detail.accountid = '$accountid' AND ((order_detail.date = '$date1' AND order_detail.menu_daypartid = menu_daypart.menu_daypartid AND menu_daypart.breakfast = '0') OR (order_detail.date = '$date2' AND order_detail.menu_daypartid = menu_daypart.menu_daypartid AND menu_daypart.breakfast = '1'))";
		$result10 = Treat_DB_ProxyOld::query( $query10 );
		$num10 = Treat_DB_ProxyOld::mysql_numrows( $result10 );
		//mysql_close();
		
		$total_qty=0;
		$num10--;
		while ( $num10 >= 0 ) {
			$qty = @Treat_DB_ProxyOld::mysql_result( $result10, $num10, 'qty' );
			$total_qty=$total_qty+$qty;
			$num10--;
		}
		$custtotal=$total_qty;
		
		////////////////////////////////////
		
		if ($total_qty!=0){
			
			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query2 = "SELECT * FROM menu_type WHERE menu_typeid = '$acct_menu'";
			$result2 = Treat_DB_ProxyOld::query( $query2 );
			$num2 = Treat_DB_ProxyOld::mysql_numrows( $result2 );
			//mysql_close();
			
			$num2--;
			while ( $num2 >= 0 ) {
				
				$menu_typeid = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'menu_typeid' );
				
				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( "Unable to select database");
				$query3 = "SELECT * FROM menu_daypart WHERE menu_typeid = '$menu_typeid' ORDER BY orderid DESC";
				$result3 = Treat_DB_ProxyOld::query( $query3 );
				$num3 = Treat_DB_ProxyOld::mysql_numrows( $result3 );
				//mysql_close();
				
				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( "Unable to select database");
				$query4 = "SELECT * FROM menu_portion WHERE menu_typeid = '$menu_typeid' ORDER BY orderid DESC";
				$result4 = Treat_DB_ProxyOld::query( $query4 );
				$num4 = Treat_DB_ProxyOld::mysql_numrows( $result4 );
				//mysql_close();
				
				$colwidth=70/($num4*3);
				
				$num3--;
				$daypartcount=0;
				while ( $num3 >= 0 ) {
					
					$menu_daypartid = @Treat_DB_ProxyOld::mysql_result( $result3, $num3, 'menu_daypartid' );
					$menu_daypartname = @Treat_DB_ProxyOld::mysql_result( $result3, $num3, 'menu_daypartname' );
					$breakfast = @Treat_DB_ProxyOld::mysql_result( $result3, $num3, 'breakfast' );
					
					if ( $breakfast == 1 ) {
						$temp_date = $date2;
					}
					else {
						$temp_date = $date1;
					}
					
					$daypartname[$daypartcount]=$menu_portionname;
					$daypartid[$daypartcount]=$menu_portionid;
					
					////////////////CHECK FOR ORDERS FOR DAYPART
					
					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( "Unable to select database");
					$query10 = "SELECT qty FROM order_detail WHERE menu_daypartid = '$menu_daypartid' AND accountid = '$accountid' AND date = '$temp_date'";
					$result10 = Treat_DB_ProxyOld::query( $query10 );
					$num10 = Treat_DB_ProxyOld::mysql_numrows( $result10 );
					//mysql_close();
					
					$total_qty=0;
					$num10--;
					while ( $num10 >= 0 ) {
						$qty = @Treat_DB_ProxyOld::mysql_result( $result10, $num10, 'qty' );
						$total_qty=$total_qty+$qty;
						$num10--;
					}
					$daypartmenutotal = $total_qty;
					
					////////////////////////////////////
					if ( $total_qty != 0 ) {
						
						$num5=$num4-1;
						while ( $num5 >= 0 ) {
							$menu_portionid = @Treat_DB_ProxyOld::mysql_result( $result4, $num5, 'menu_portionid' );
							$menu_portionname = @Treat_DB_ProxyOld::mysql_result( $result4, $num5, 'menu_portionname' );
							
							//mysql_connect($dbhost,$username,$password);
							//@mysql_select_db($database) or die( "Unable to select database");
							$query6 = "SELECT qty FROM order_detail WHERE menu_daypartid = '$menu_daypartid' AND menu_portionid = '$menu_portionid' AND accountid = '$accountid' AND date = '$temp_date'";
							$result6 = Treat_DB_ProxyOld::query( $query6 );
							$num6 = Treat_DB_ProxyOld::mysql_numrows( $result6 );
							//mysql_close();
							
							$total_qty=0;
							$num6--;
							while ( $num6 >= 0 ) {
								$qty = @Treat_DB_ProxyOld::mysql_result( $result6, $num6, 'qty' );
								$total_qty=$total_qty+$qty;
								$num6--;
							}
							$orderqty[$num5]=$total_qty;
							
							$num5--;
						}
						
						//mysql_connect($dbhost,$username,$password);
						//@mysql_select_db($database) or die( "Unable to select database");
						$query8 = "SELECT * FROM order_item WHERE daypartid = '$menu_daypartid' AND businessid = '$businessid' AND date = '$temp_date' ORDER BY order_itemid DESC";
						$result8 = Treat_DB_ProxyOld::query( $query8 );
						$num8 = Treat_DB_ProxyOld::mysql_numrows( $result8 );
						//mysql_close();
						
						$num8--;
						while ( $num8 >= 0 ) {
							$menu_itemid = @Treat_DB_ProxyOld::mysql_result( $result8, $num8, 'menu_itemid' );
							$order_itemid = @Treat_DB_ProxyOld::mysql_result( $result8, $num8, 'order_itemid' );
							
							//mysql_connect($dbhost,$username,$password);
							//@mysql_select_db($database) or die( "Unable to select database");
							$query9 = "SELECT * FROM menu_items WHERE menu_item_id = '$menu_itemid'";
							$result9 = Treat_DB_ProxyOld::query( $query9 );
							//mysql_close();
							
							$menu_itemname = @Treat_DB_ProxyOld::mysql_result( $result9, 0, 'item_name' );
							$true_unit = @Treat_DB_ProxyOld::mysql_result( $result9, 0, 'true_unit' );
							$order_unit = @Treat_DB_ProxyOld::mysql_result( $result9, 0, 'order_unit' );
							$su_in_ou = @Treat_DB_ProxyOld::mysql_result( $result9, 0, 'su_in_ou' );
							$label = @Treat_DB_ProxyOld::mysql_result( $result9, 0, 'label' );
							$contract_option = @Treat_DB_ProxyOld::mysql_result( $result9, 0, 'contract_option' );
							
							//mysql_connect($dbhost,$username,$password);
							//@mysql_select_db($database) or die( "Unable to select database");
							$query9 = "SELECT * FROM size WHERE sizeid = '$true_unit'";
							$result9 = Treat_DB_ProxyOld::query( $query9 );
							//mysql_close();
							
							$sizename = @Treat_DB_ProxyOld::mysql_result( $result9, 0, 'sizename' );
							
							//mysql_connect($dbhost,$username,$password);
							//@mysql_select_db($database) or die( "Unable to select database");
							$query9 = "SELECT * FROM size WHERE sizeid = '$order_unit'";
							$result9 = Treat_DB_ProxyOld::query( $query9 );
							//mysql_close();
							
							$serving = @Treat_DB_ProxyOld::mysql_result( $result9, 0, 'sizename' );
							
							$totaldec=0;
							$num5=$num4-1;
							while ( $num5 >= 0 ) {
								$menu_portionid = @Treat_DB_ProxyOld::mysql_result( $result4, $num5, 'menu_portionid' );
								$menu_portionname = @Treat_DB_ProxyOld::mysql_result( $result4, $num5, 'menu_portionname' );
								
								//mysql_connect($dbhost,$username,$password);
								//@mysql_select_db($database) or die( "Unable to select database");
								$query9 = "SELECT * FROM menu_portiondetail WHERE menu_itemid = '$menu_itemid' AND menu_typeid = '$menu_typeid' AND menu_portionid = '$menu_portionid' AND menu_daypartid = '$menu_daypartid'";
								$result9 = Treat_DB_ProxyOld::query( $query9 );
								//mysql_close();
								
								$portiondetail = @Treat_DB_ProxyOld::mysql_result( $result9, 0, 'portion' );
								
								if ( $contract_option == 1 && $allow_sub == 1 ) {
									//mysql_connect($dbhost,$username,$password);
									//@mysql_select_db($database) or die( "Unable to select database");
									$query62 = "SELECT * FROM order_subs WHERE daypartid = '$menu_daypartid' AND portionid = '$menu_portionid' AND accountid = '$accountid' AND order_itemid = '$order_itemid'";
									$result62 = Treat_DB_ProxyOld::query( $query62 );
									$num62 = Treat_DB_ProxyOld::mysql_numrows( $result62 );
									//mysql_close();
									
									$subvalue = @Treat_DB_ProxyOld::mysql_result( $result62, 0, 'amount' );
								}
								
								$decimalequiv=0;
								if ( $contract_option == 1 && $allow_sub == 1 ) {
									$decimalequiv = ( $portiondetail * $subvalue ) / $su_in_ou * 10;
									$decimalequiv = ceil( $decimalequiv );
									$decimalequiv = $decimalequiv / 10;
								}
								else {
									$decimalequiv = ( $portiondetail * $orderqty[ $num5 ] ) / $su_in_ou * 10;
									$decimalequiv = ceil( $decimalequiv );
									$decimalequiv = $decimalequiv / 10;
								}
								$totaldec = $totaldec + $decimalequiv;
								
								$num5--;
							}
							/////////UPDATE TOTALS/////////////////////////////////////////////////////////////////
							if ( $label == 1 ) {
								//mysql_connect($dbhost,$username,$password);
								//@mysql_select_db($database) or die( "Unable to select database");
								$query31 = "INSERT INTO contract_temp2 (businessid,menu_itemid,accountid,menu_itemname,accountname,total,units,driver) VALUES ('$businessid','$menu_itemid','$accountid','$menu_itemname','$accountname','$totaldec','$serving','$assign')";
								$result31 = Treat_DB_ProxyOld::query( $query31 );
								//mysql_close();
							}
							/////////END TOTALS/////////////////////////////////////////////////////////////////////
							$num8--;
						}
					}//END IF STATEMENT
					
					$daypartcount++;
					$num3--;
				}
				$num2--;
			}
		}//END IF STATEMENT
		$num1--;
	}
	
	
	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query10 = "SELECT * FROM contract_temp2 WHERE businessid = '$businessid' ORDER BY menu_itemid, driver";
	$result10 = Treat_DB_ProxyOld::query( $query10 );
	$num10 = Treat_DB_ProxyOld::mysql_numrows( $result10 );
	//mysql_close();
	
	$lastid=0;
	$num10--;
	
	$pagecount=0;
	$rowcount=0;
	
	echo "<table width=100%>";
	
	while ( $num10 >= 0 ) {
		$driver = @Treat_DB_ProxyOld::mysql_result( $result10, $num10, 'driver' );
		$acct_name = @Treat_DB_ProxyOld::mysql_result( $result10, $num10, 'accountname' );
		$item_name = @Treat_DB_ProxyOld::mysql_result( $result10, $num10, 'menu_itemname' );
		$acct_name=substr($acct_name,0,30);
		$item_name=substr($item_name,0,30);
		$menu_itemid = @Treat_DB_ProxyOld::mysql_result( $result10, $num10, 'menu_itemid' );
		$total = @Treat_DB_ProxyOld::mysql_result( $result10, $num10, 'total' );
		$units = @Treat_DB_ProxyOld::mysql_result( $result10, $num10, 'units' );
		
		if ( $total > 0 ) {
			if ( $rowcount == 0 ) {
				echo "<tr height=95>";
			}
			echo "<td width=33%>";
			echo "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<table><tr valign=top><td><u><b><i>$driver</i></b></u></td><td><i>$acct_name</i></font></td></tr><tr><td></td><td>$item_name</td></tr><tr><td></td><td>$total $units</td></tr></table>";
			echo "</td>";
			$pagecount++;
			$rowcount++;
			if ( $rowcount == 3 ) {
				$rowcount = 0;
				echo "</tr>";
			}
			if ( $pagecount == 30 ) {
				$pagecount = 0;
				echo "</table> <p STYLE='page-break-before: always'></p> <table width=100%>";
			}
		}
		
		$num10--;
		$lastid=$menu_itemid;
	}
	echo "</table>";
	
	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query10 = "DELETE FROM contract_temp2 WHERE businessid = '$businessid'";
	$result10 = Treat_DB_ProxyOld::query( $query10 );
	//mysql_close();
	
	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query10 = "OPTIMIZE TABLE contract_temp2";
	$result10 = Treat_DB_ProxyOld::query( $query10 );
	//mysql_close();
}
else {
	$weekday = dayofweek( $date1 );

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM company WHERE companyid = '$companyid'";
	$result = Treat_DB_ProxyOld::query( $query );
	//mysql_close();

	$companyname = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'companyname' );

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM business WHERE businessid = '$businessid'";
	$result = Treat_DB_ProxyOld::query( $query );
	//mysql_close();

	$bustax = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'tax' );
	$default_menu = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'default_menu' );

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	if ( $account[0] == -1 ) {
		$query1 = "SELECT * FROM accounts WHERE businessid = '$businessid' AND print_labels != 0 ORDER BY name DESC";
	}
	else{
		$myquery="";
		$first="";
		foreach ($account as $key){
			if ( $first == "" ) {
				$myquery = "accountid = '$key'";
				$first = 1;
			}
			else {
				$myquery = "$myquery OR accountid = '$key'";
			}
		}
		$query1 = "SELECT * FROM accounts WHERE ( $myquery ) AND print_labels != 0";
	}
	$result1 = Treat_DB_ProxyOld::query( $query1 );
	$num1 = Treat_DB_ProxyOld::mysql_numrows( $result1 );
	//mysql_close();
	
	$num1--;
	while ( $num1 >= 0 ) {
		$accountid = @Treat_DB_ProxyOld::mysql_result( $result1, $num1, 'accountid' );
		$accountname = @Treat_DB_ProxyOld::mysql_result( $result1, $num1, 'name' );
		$acct_menu = @Treat_DB_ProxyOld::mysql_result( $result1, $num1, 'menu' );
		$assign = @Treat_DB_ProxyOld::mysql_result( $result1, $num1, 'assign' );
		$contract_num = @Treat_DB_ProxyOld::mysql_result( $result1, $num1, 'contract_num' );
		$allow_sub = @Treat_DB_ProxyOld::mysql_result( $result1, $num1, 'allow_sub' );
		if ( $acct_menu == -1 ) {
			$acct_menu = $default_menu;
		}
		
		$daypartname[]="";
		$daypartid[]="";
		$portionname[]="";
		$portionid[]="";
		$custqty[]="";
		
		////////////////CHECK FOR ORDERS
		
		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query10 = "SELECT order_detail.qty FROM order_detail,menu_daypart WHERE order_detail.accountid = '$accountid' AND ((order_detail.date = '$date1' AND order_detail.menu_daypartid = menu_daypart.menu_daypartid AND menu_daypart.breakfast = '0') OR (order_detail.date = '$date2' AND order_detail.menu_daypartid = menu_daypart.menu_daypartid AND menu_daypart.breakfast = '1'))";
		$result10 = Treat_DB_ProxyOld::query( $query10 );
		$num10 = Treat_DB_ProxyOld::mysql_numrows( $result10 );
		//mysql_close();
		
		$total_qty = 0;
		$num10--;
		while ( $num10 >= 0 ) {
			$qty = @Treat_DB_ProxyOld::mysql_result( $result10, $num10, 'qty' );
			$total_qty = $total_qty + $qty;
			$num10--;
		}
		$custtotal = $total_qty;
		
		////////////////////////////////////
		
		if ( $total_qty != 0 ) {
			
			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query2 = "SELECT * FROM menu_type WHERE menu_typeid = '$acct_menu'";
			$result2 = Treat_DB_ProxyOld::query( $query2 );
			$num2 = Treat_DB_ProxyOld::mysql_numrows( $result2 );
			//mysql_close();
			
			$num2--;
			while ( $num2 >= 0 ) {
				
				$menu_typeid = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'menu_typeid' );
				
				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( "Unable to select database");
				$query3 = "SELECT * FROM menu_daypart WHERE menu_typeid = '$menu_typeid' ORDER BY orderid DESC";
				$result3 = Treat_DB_ProxyOld::query( $query3 );
				$num3 = Treat_DB_ProxyOld::mysql_numrows( $result3 );
				//mysql_close();
				
				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( "Unable to select database");
				$query4 = "SELECT * FROM menu_portion WHERE menu_typeid = '$menu_typeid' ORDER BY orderid DESC";
				$result4 = Treat_DB_ProxyOld::query( $query4 );
				$num4 = Treat_DB_ProxyOld::mysql_numrows( $result4 );
				//mysql_close();
				
				$colwidth = 70 / ( $num4 * 3 );
				
				$num3--;
				$daypartcount=0;
				while ( $num3 >= 0 ) {
					
					$menu_daypartid = @Treat_DB_ProxyOld::mysql_result( $result3, $num3, 'menu_daypartid' );
					$menu_daypartname = @Treat_DB_ProxyOld::mysql_result( $result3, $num3, 'menu_daypartname' );
					$breakfast = @Treat_DB_ProxyOld::mysql_result( $result3, $num3, 'breakfast' );
					
					if ( $breakfast == 1 ) {
						$temp_date = $date2;
					}
					else {
						$temp_date = $date1;
					}
					
					$daypartname[$daypartcount]=$menu_portionname;
					$daypartid[$daypartcount]=$menu_portionid;
					
					////////////////CHECK FOR ORDERS FOR DAYPART
					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( "Unable to select database");
					$query10 = "SELECT qty FROM order_detail WHERE menu_daypartid = '$menu_daypartid' AND accountid = '$accountid' AND date = '$temp_date'";
					$result10 = Treat_DB_ProxyOld::query( $query10 );
					$num10 = Treat_DB_ProxyOld::mysql_numrows( $result10 );
					//mysql_close();
					
					$total_qty=0;
					$num10--;
					while ( $num10 >= 0 ) {
							$qty = @Treat_DB_ProxyOld::mysql_result( $result10, $num10, 'qty' );
							$total_qty=$total_qty+$qty;
							$num10--;
					}
					$daypartmenutotal=$total_qty;
					
					////////////////////////////////////
					if ( $total_qty != 0 ) {
						
						$num5 = $num4 - 1;
						while ( $num5 >= 0 ) {
							$menu_portionid = @Treat_DB_ProxyOld::mysql_result( $result4, $num5, 'menu_portionid' );
							$menu_portionname = @Treat_DB_ProxyOld::mysql_result( $result4, $num5, 'menu_portionname' );
							
							//mysql_connect($dbhost,$username,$password);
							//@mysql_select_db($database) or die( "Unable to select database");
							$query6 = "SELECT qty FROM order_detail WHERE menu_daypartid = '$menu_daypartid' AND menu_portionid = '$menu_portionid' AND accountid = '$accountid' AND date = '$temp_date'";
							$result6 = Treat_DB_ProxyOld::query( $query6 );
							$num6 = Treat_DB_ProxyOld::mysql_numrows( $result6 );
							//mysql_close();
							
							$total_qty=0;
							$num6--;
							while ( $num6 >= 0 ) {
								$qty = @Treat_DB_ProxyOld::mysql_result( $result6, $num6, 'qty' );
								$total_qty = $total_qty + $qty;
								$num6--;
							}
							$orderqty[ $num5 ] = $total_qty;
							
							$num5--;
						}
						
						//mysql_connect($dbhost,$username,$password);
						//@mysql_select_db($database) or die( "Unable to select database");
						$query8 = "SELECT * FROM order_item WHERE daypartid = '$menu_daypartid' AND businessid = '$businessid' AND date = '$temp_date' ORDER BY order_itemid DESC";
						$result8 = Treat_DB_ProxyOld::query( $query8 );
						$num8 = Treat_DB_ProxyOld::mysql_numrows( $result8 );
						//mysql_close();
						
						$num8--;
						while ( $num8 >= 0 ) {
							$menu_itemid = @Treat_DB_ProxyOld::mysql_result( $result8, $num8, 'menu_itemid' );
							$order_itemid = @Treat_DB_ProxyOld::mysql_result( $result8, $num8, 'order_itemid' );

							//mysql_connect($dbhost,$username,$password);
							//@mysql_select_db($database) or die( "Unable to select database");
							$query9 = "SELECT * FROM menu_items WHERE menu_item_id = '$menu_itemid'";
							$result9 = Treat_DB_ProxyOld::query( $query9 );
							//mysql_close();

							$menu_itemname = @Treat_DB_ProxyOld::mysql_result( $result9, 0, 'item_name' );
							$true_unit = @Treat_DB_ProxyOld::mysql_result( $result9, 0, 'true_unit' );
							$order_unit = @Treat_DB_ProxyOld::mysql_result( $result9, 0, 'order_unit' );
							$su_in_ou = @Treat_DB_ProxyOld::mysql_result( $result9, 0, 'su_in_ou' );
							$label = @Treat_DB_ProxyOld::mysql_result( $result9, 0, 'label' );
							$contract_option = @Treat_DB_ProxyOld::mysql_result( $result9, 0, 'contract_option' );

							//mysql_connect($dbhost,$username,$password);
							//@mysql_select_db($database) or die( "Unable to select database");
							$query9 = "SELECT * FROM size WHERE sizeid = '$true_unit'";
							$result9 = Treat_DB_ProxyOld::query( $query9 );
							//mysql_close();

							$sizename = @Treat_DB_ProxyOld::mysql_result( $result9, 0, 'sizename' );

							//mysql_connect($dbhost,$username,$password);
							//@mysql_select_db($database) or die( "Unable to select database");
							$query9 = "SELECT * FROM size WHERE sizeid = '$order_unit'";
							$result9 = Treat_DB_ProxyOld::query( $query9 );
							//mysql_close();

							$serving = @Treat_DB_ProxyOld::mysql_result( $result9, 0, 'sizename' );

							$totaldec=0;
							$num5=$num4-1;
							while ( $num5 >= 0 ) {
								$menu_portionid = @Treat_DB_ProxyOld::mysql_result( $result4, $num5, 'menu_portionid' );
								$menu_portionname = @Treat_DB_ProxyOld::mysql_result( $result4, $num5, 'menu_portionname' );
								
								//mysql_connect($dbhost,$username,$password);
								//@mysql_select_db($database) or die( "Unable to select database");
								$query9 = "SELECT * FROM menu_portiondetail WHERE menu_itemid = '$menu_itemid' AND menu_typeid = '$menu_typeid' AND menu_portionid = '$menu_portionid' AND menu_daypartid = '$menu_daypartid'";
								$result9 = Treat_DB_ProxyOld::query( $query9 );
								//mysql_close();
								
								$portiondetail = @Treat_DB_ProxyOld::mysql_result( $result9, 0, 'portion' );
								
								if ( $contract_option == 1 && $allow_sub == 1 ) {
									//mysql_connect($dbhost,$username,$password);
									//@mysql_select_db($database) or die( "Unable to select database");
									$query62 = "SELECT * FROM order_subs WHERE daypartid = '$menu_daypartid' AND portionid = '$menu_portionid' AND accountid = '$accountid' AND order_itemid = '$order_itemid'";
									$result62 = Treat_DB_ProxyOld::query( $query62 );
									$num62 = Treat_DB_ProxyOld::mysql_numrows( $result62 );
									//mysql_close();
									
									$subvalue = @Treat_DB_ProxyOld::mysql_result( $result62, 0, 'amount' );
								}
								
								$decimalequiv=0;
								if ( $contract_option == 1 && $allow_sub == 1 ) {
									$decimalequiv = ( $portiondetail * $subvalue ) / $su_in_ou * 10;
									$decimalequiv = ceil( $decimalequiv );
									$decimalequiv = $decimalequiv / 10;
								}
								else {
									$decimalequiv = ( $portiondetail * $orderqty[ $num5 ] ) / $su_in_ou * 10;
									$decimalequiv = ceil( $decimalequiv );
									$decimalequiv = $decimalequiv / 10;
								}
								$totaldec = $totaldec + $decimalequiv;
								
								$num5--;
							}
							/////////UPDATE TOTALS/////////////////////////////////////////////////////////////////
							if ( $label == 1 ) {
								//mysql_connect($dbhost,$username,$password);
								//@mysql_select_db($database) or die( "Unable to select database");
								$query31 = "INSERT INTO contract_temp2 (businessid,menu_itemid,accountid,menu_itemname,accountname,total,units,driver) VALUES ('$businessid','$menu_itemid','$accountid','$menu_itemname','$accountname','$totaldec','$serving','$assign')";
								$result31 = Treat_DB_ProxyOld::query( $query31 );
								//mysql_close();
							}
							/////////END TOTALS/////////////////////////////////////////////////////////////////////
							$num8--;
						}
					}//END IF STATEMENT
					
					$daypartcount++;
					$num3--;
				}
				$num2--;
			}
		}//END IF STATEMENT
		$num1--;
	}
	
	echo "<body leftmargin='0px' topmargin='0px' marginwidth='0px' marginheight='0px'>";
	
	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query10 = "SELECT * FROM contract_temp2 WHERE businessid = '$businessid' ORDER BY menu_itemid, driver";
	$result10 = Treat_DB_ProxyOld::query( $query10 );
	$num10 = Treat_DB_ProxyOld::mysql_numrows( $result10 );
	//mysql_close();
	
	$lastid=0;
	$num10--;
	while ( $num10 >= 0 ) {
		$driver = @Treat_DB_ProxyOld::mysql_result( $result10, $num10, 'driver' );
		$acct_name = @Treat_DB_ProxyOld::mysql_result( $result10, $num10, 'accountname' );
		if ( strlen( $acct_name ) > 24 ) {
			$acct_name = "<font size=1>$acct_name</font>";
		}
		$item_name = @Treat_DB_ProxyOld::mysql_result( $result10, $num10, 'menu_itemname' );
		$item_name=substr($item_name,0,28);
		if ( strlen( $item_name ) > 24 ) {
			$item_name = "<font size=1>$item_name</font>";
		}
		$menu_itemid = @Treat_DB_ProxyOld::mysql_result( $result10, $num10, 'menu_itemid' );
		$total = @Treat_DB_ProxyOld::mysql_result( $result10, $num10, 'total' );
		$units = @Treat_DB_ProxyOld::mysql_result( $result10, $num10, 'units' );
		
		if ( $total > 0 && ( $item_only == $menu_itemid || $item_only == -1 ) ) {
			echo "<table><tr valign=top><td rowspan=3><font size=5 face=arial><b><u>$driver</u></b></td><td><font size=2 face=arial><i>$acct_name</i></font></td></tr><tr><td><font size=2 face=arial>$item_name</td></tr><tr><td><font size=2 face=arial>$total $units</td></tr></table> <p STYLE='page-break-before: always'></p>";
		}
		
		$num10--;
		$lastid = $menu_itemid;
	}
	
	echo "</body>";
	
	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query10 = "DELETE FROM contract_temp2 WHERE businessid = '$businessid'";
	$result10 = Treat_DB_ProxyOld::query( $query10 );
	//mysql_close();
	
	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query10 = "OPTIMIZE TABLE contract_temp2";
	$result10 = Treat_DB_ProxyOld::query( $query10 );
	//mysql_close();
	
	//mysql_close();
	
}
?>