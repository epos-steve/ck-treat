<?php

function money($diff){   
        $diff=round($diff, 0);
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      elseif ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      elseif ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '1')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='31')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;
$leap = date("L");

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   
   elseif ($leap==1&&$month=='03')
   {
      $day=$day+29;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

//include("db.php");
define('DOC_ROOT', dirname(dirname(__FILE__)));
require_once(DOC_ROOT.'/bootstrap.php');
$style = "text-decoration:none";

$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-01";

/*$user=$_COOKIE["usercook"];
$pass=$_COOKIE["passcook"];
$compcook=$_COOKIE["compcook"];*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook');
$compcook = \EE\Controller\Base::getSessionCookieVariable('compcook');

/*mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");*/
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=Treat_DB_ProxyOld::mysql_numrows($result);
//mysql_close();

if ($num != 1 || $user == "" || $pass == "")
{
    echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
   /*mysql_connect($dbhost,$username,$password);
   @mysql_select_db($database) or die( "Unable to select database");*/
   $query = "SELECT * FROM company WHERE companyid = '$compcook'";
   $result = Treat_DB_ProxyOld::query($query);
   //mysql_close();
   $companyname=Treat_DB_ProxyOld::mysql_result($result,0,"companyname");

   echo "<center><b>$companyname Monthly Budget Comparison</b></center>";

   $month=$month-5;
   if ($month<1){$month=$month+12;$year--;}
   if ($month<10){$month="0$month";}
   $searchdate="$year-$month-01";

   if ($month==1){$monthname1="JAN";$monthname2="FEB";$monthname3="MAR";$monthname4="APR";$monthname5="MAY";$monthname6="JUN";}
   elseif ($month==2){$monthname1="FEB";$monthname2="MAR";$monthname3="APR";$monthname4="MAY";$monthname5="JUN";$monthname6="JUL";}
   elseif ($month==3){$monthname1="MAR";$monthname2="APR";$monthname3="MAY";$monthname4="JUN";$monthname5="JUL";$monthname6="AUG";}
   elseif ($month==4){$monthname1="APR";$monthname2="MAY";$monthname3="JUN";$monthname4="JUL";$monthname5="AUG";$monthname6="SEP";}
   elseif ($month==5){$monthname1="MAY";$monthname2="JUN";$monthname3="JUL";$monthname4="AUG";$monthname5="SEP";$monthname6="OCT";}
   elseif ($month==6){$monthname1="JUN";$monthname2="JUL";$monthname3="AUG";$monthname4="SEP";$monthname5="OCT";$monthname6="NOV";}
   elseif ($month==7){$monthname1="JUL";$monthname2="AUG";$monthname3="SEP";$monthname4="OCT";$monthname5="NOV";$monthname6="DEC";}
   elseif ($month==8){$monthname1="AUG";$monthname2="SEP";$monthname3="OCT";$monthname4="NOV";$monthname5="DEC";$monthname6="JAN";}
   elseif ($month==9){$monthname1="SEP";$monthname2="OCT";$monthname3="NOV";$monthname4="DEC";$monthname5="JAN";$monthname6="FEB";}
   elseif ($month==10){$monthname1="OCT";$monthname2="NOV";$monthname3="DEC";$monthname4="JAN";$monthname5="FEB";$monthname6="MAR";}
   elseif ($month==11){$monthname1="NOV";$monthname2="DEC";$monthname3="JAN";$monthname4="FEB";$monthname5="MAR";$monthname6="APR";}
   elseif ($month==12){$monthname1="DEC";$monthname2="JAN";$monthname3="FEB";$monthname4="MAR";$monthname5="APR";$monthname6="MAY";}

/////SALES////////////////////////////////////////////////////////////////////////

   /*mysql_connect($dbhost,$username,$password);
   @mysql_select_db($database) or die( "Unable to select database");*/
   $query = "SELECT sales FROM com_kpi WHERE companyid = '$compcook' AND date <= '$today' AND date >= '$searchdate' ORDER BY date DESC";
   $result = Treat_DB_ProxyOld::query($query);
   $num=Treat_DB_ProxyOld::mysql_numrows($result);
   //mysql_close();

/////////////////////JAVA GRAPH/////////////////////////////////////////
?>
<center><applet code="LineGraphApplet.class" archive="Linegraph.jar" width="600" height="175">

<!-- Start Up Parameters -->
<PARAM name="LOADINGMESSAGE" value="Creating Chart - Please Wait.">
<PARAM name="STEXTCOLOR"     value="#000060">
<PARAM name="STARTUPCOLOR"   value="#FFFFFF">

<!-- Line Data -->
<!--   value,URL,Target Frame -->
<?php
////////////////////////////////////////////////////////////////////////

   $num--;
   $counter=1;
   while ($num>=0){
      $data="data$counter";
      $series="series1";
      $sales=Treat_DB_ProxyOld::mysql_result($result,$num,"sales");
      echo "<PARAM name=$data$series value=$sales>";
      $counter++;
      $num--;
   }

   /*mysql_connect($dbhost,$username,$password);
   @mysql_select_db($database) or die( "Unable to select database");*/
   $query = "SELECT sales_bgt FROM com_kpi WHERE companyid = '$compcook' AND date <= '$today' AND date >= '$searchdate' ORDER BY date DESC";
   $result = Treat_DB_ProxyOld::query($query);
   $num=Treat_DB_ProxyOld::mysql_numrows($result);
   //mysql_close();


   $num--;
   $counter=1;
   while ($num>=0){
      $data="data$counter";
      $series="series2";
      $sales_bgt=Treat_DB_ProxyOld::mysql_result($result,$num,"sales_bgt");
      echo "<PARAM name=$data$series value=$sales_bgt>";
      $counter++;
      $num--;
   }

?>

<!-- Properties -->
<PARAM name="autoscale"            value="true">

<PARAM name="gridbgcolor"          value="#E8E7E7">

<PARAM name="series1"              value="green|5|7|true|Sales">
<PARAM name="series2"              value="black|3|5|true|Budget">

<PARAM name="ndecplaces"           value="0">
<PARAM name="legend"               value="true">
<PARAM name="LegendStyle"          value="Horizontal">
<PARAM name="legendfont"           value="Arial,N,10">
<PARAM name="labelOrientation"     value="Up Angle">

<PARAM name="ylabel_pre"           value="$">
<PARAM name="ylabel_font"          value="Arial,N,10">

<PARAM name="popup_pre"            value="$">

<PARAM name="xlabel_font"          value="Arial,N,10">

<?php

echo "<PARAM name=label1               value=$monthname1>";
echo "<PARAM name=label2               value=$monthname2>";
echo "<PARAM name=label3               value=$monthname3>";
echo "<PARAM name=label4               value=$monthname4>";
echo "<PARAM name=label5               value=$monthname5>";
echo "<PARAM name=label6               value=$monthname6>";

echo "</applet></center>";

   //echo "<center><img src='line-graph.php?data=com_sales_data.txt&config=com_sales_config.txt' width=600 height=175></center>";

/////COS////////////////////////////////////////////////////////////////////////
/////////////////////JAVA GRAPH/////////////////////////////////////////
?>
<center><applet code="LineGraphApplet.class" archive="Linegraph.jar" width="600" height="175">

<!-- Start Up Parameters -->
<PARAM name="LOADINGMESSAGE" value="Creating Chart - Please Wait.">
<PARAM name="STEXTCOLOR"     value="#000060">
<PARAM name="STARTUPCOLOR"   value="#FFFFFF">

<!-- Line Data -->
<!--   value,URL,Target Frame -->
<?php
////////////////////////////////////////////////////////////////////////

   /*mysql_connect($dbhost,$username,$password);
   @mysql_select_db($database) or die( "Unable to select database");*/
   $query = "SELECT cos FROM com_kpi WHERE companyid = '$compcook' AND date <= '$today' AND date >= '$searchdate' ORDER BY date DESC";
   $result = Treat_DB_ProxyOld::query($query);
   $num=Treat_DB_ProxyOld::mysql_numrows($result);
   //mysql_close();

   $num--;
   $counter=1;
   while ($num>=0){
      $data="data$counter";
      $series="series1";
      $cos=Treat_DB_ProxyOld::mysql_result($result,$num,"cos");
      echo "<PARAM name=$data$series value=$cos>";
      $counter++;
      $num--;
   }

   /*mysql_connect($dbhost,$username,$password);
   @mysql_select_db($database) or die( "Unable to select database");*/
   $query = "SELECT cos_bgt FROM com_kpi WHERE companyid = '$compcook' AND date <= '$today' AND date >= '$searchdate' ORDER BY date DESC";
   $result = Treat_DB_ProxyOld::query($query);
   $num=Treat_DB_ProxyOld::mysql_numrows($result);
   //mysql_close();


   $num--;
   $counter=1;
   while ($num>=0){
      $data="data$counter";
      $series="series2";
      $cos_bgt=Treat_DB_ProxyOld::mysql_result($result,$num,"cos_bgt");
      echo "<PARAM name=$data$series value=$cos_bgt>";
      $counter++;
      $num--;
   }

?>

<!-- Properties -->
<PARAM name="autoscale"            value="true">

<PARAM name="gridbgcolor"          value="#E8E7E7">

<PARAM name="series1"              value="green|5|7|true|COS">
<PARAM name="series2"              value="black|3|5|true|Budget">

<PARAM name="ndecplaces"           value="0">
<PARAM name="legend"               value="true">
<PARAM name="LegendStyle"          value="Horizontal">
<PARAM name="legendfont"           value="Arial,N,10">
<PARAM name="labelOrientation"     value="Up Angle">

<PARAM name="ylabel_pre"           value="$">
<PARAM name="ylabel_font"          value="Arial,N,10">

<PARAM name="popup_pre"            value="$">

<PARAM name="xlabel_font"          value="Arial,N,10">

<?php

echo "<PARAM name=label1               value=$monthname1>";
echo "<PARAM name=label2               value=$monthname2>";
echo "<PARAM name=label3               value=$monthname3>";
echo "<PARAM name=label4               value=$monthname4>";
echo "<PARAM name=label5               value=$monthname5>";
echo "<PARAM name=label6               value=$monthname6>";

echo "</applet></center>";

   //echo "<center><img src='line-graph.php?data=com_cos_data.txt&config=com_cos_config.txt' width=600 height=175></center>";

/////LABOR////////////////////////////////////////////////////////////////////////
/////////////////////JAVA GRAPH/////////////////////////////////////////
?>
<center><applet code="LineGraphApplet.class" archive="Linegraph.jar" width="600" height="175">

<!-- Start Up Parameters -->
<PARAM name="LOADINGMESSAGE" value="Creating Chart - Please Wait.">
<PARAM name="STEXTCOLOR"     value="#000060">
<PARAM name="STARTUPCOLOR"   value="#FFFFFF">

<!-- Line Data -->
<!--   value,URL,Target Frame -->
<?php
////////////////////////////////////////////////////////////////////////

   /*mysql_connect($dbhost,$username,$password);
   @mysql_select_db($database) or die( "Unable to select database");*/
   $query = "SELECT labor FROM com_kpi WHERE companyid = '$compcook' AND date <= '$today' AND date >= '$searchdate' ORDER BY date DESC";
   $result = Treat_DB_ProxyOld::query($query);
   $num=Treat_DB_ProxyOld::mysql_numrows($result);
   //mysql_close();

   $num--;
   $counter=1;
   while ($num>=0){
      $data="data$counter";
      $series="series1";
      $labor=Treat_DB_ProxyOld::mysql_result($result,$num,"labor");
      echo "<PARAM name=$data$series value=$labor>";
      $counter++;
      $num--;
   }

   /*mysql_connect($dbhost,$username,$password);
   @mysql_select_db($database) or die( "Unable to select database");*/
   $query = "SELECT lbr_bgt FROM com_kpi WHERE companyid = '$compcook' AND date <= '$today' AND date >= '$searchdate' ORDER BY date DESC";
   $result = Treat_DB_ProxyOld::query($query);
   $num=Treat_DB_ProxyOld::mysql_numrows($result);
   //mysql_close();

   $num--;
   $counter=1;
   while ($num>=0){
      $data="data$counter";
      $series="series2";
      $lbr_bgt=Treat_DB_ProxyOld::mysql_result($result,$num,"lbr_bgt");
      echo "<PARAM name=$data$series value=$lbr_bgt>";
      $counter++;
      $num--;
   }


?>

<!-- Properties -->
<PARAM name="autoscale"            value="true">

<PARAM name="gridbgcolor"          value="#E8E7E7">

<PARAM name="series1"              value="green|5|7|true|Labor">
<PARAM name="series2"              value="black|3|5|true|Budget">

<PARAM name="ndecplaces"           value="0">
<PARAM name="legend"               value="true">
<PARAM name="LegendStyle"          value="Horizontal">
<PARAM name="legendfont"           value="Arial,N,10">
<PARAM name="labelOrientation"     value="Up Angle">

<PARAM name="ylabel_pre"           value="$">
<PARAM name="ylabel_font"          value="Arial,N,10">

<PARAM name="popup_pre"            value="$">

<PARAM name="xlabel_font"          value="Arial,N,10">

<?php

echo "<PARAM name=label1               value=$monthname1>";
echo "<PARAM name=label2               value=$monthname2>";
echo "<PARAM name=label3               value=$monthname3>";
echo "<PARAM name=label4               value=$monthname4>";
echo "<PARAM name=label5               value=$monthname5>";
echo "<PARAM name=label6               value=$monthname6>";

echo "</applet></center>";


/////TOE////////////////////////////////////////////////////////////////////////
/////////////////////JAVA GRAPH/////////////////////////////////////////
?>
<center><applet code="LineGraphApplet.class" archive="Linegraph.jar" width="600" height="175">

<!-- Start Up Parameters -->
<PARAM name="LOADINGMESSAGE" value="Creating Chart - Please Wait.">
<PARAM name="STEXTCOLOR"     value="#000060">
<PARAM name="STARTUPCOLOR"   value="#FFFFFF">

<!-- Line Data -->
<!--   value,URL,Target Frame -->
<?php
////////////////////////////////////////////////////////////////////////

   /*mysql_connect($dbhost,$username,$password);
   @mysql_select_db($database) or die( "Unable to select database");*/
   $query = "SELECT toe FROM com_kpi WHERE companyid = '$compcook' AND date <= '$today' AND date >= '$searchdate' ORDER BY date DESC";
   $result = Treat_DB_ProxyOld::query($query);
   $num=Treat_DB_ProxyOld::mysql_numrows($result);
   //mysql_close();

   $num--;
   $counter=1;
   while ($num>=0){
      $data="data$counter";
      $series="series1";
      $toe=Treat_DB_ProxyOld::mysql_result($result,$num,"toe");
      echo "<PARAM name=$data$series value=$toe>";
      $counter++;
      $num--;
   }

   /*mysql_connect($dbhost,$username,$password);
   @mysql_select_db($database) or die( "Unable to select database");*/
   $query = "SELECT toe_bgt FROM com_kpi WHERE companyid = '$compcook' AND date <= '$today' AND date >= '$searchdate' ORDER BY date DESC";
   $result = Treat_DB_ProxyOld::query($query);
   $num=Treat_DB_ProxyOld::mysql_numrows($result);
   //mysql_close();

   $num--;
   $counter=1;
   while ($num>=0){
      $data="data$counter";
      $series="series2";
      $toe_bgt=Treat_DB_ProxyOld::mysql_result($result,$num,"toe_bgt");
      echo "<PARAM name=$data$series value=$toe_bgt>";
      $counter++;
      $num--;
   }

?>

<!-- Properties -->
<PARAM name="autoscale"            value="true">

<PARAM name="gridbgcolor"          value="#E8E7E7">

<PARAM name="series1"              value="green|5|7|true|TOE">
<PARAM name="series2"              value="black|3|5|true|Budget">

<PARAM name="ndecplaces"           value="0">
<PARAM name="legend"               value="true">
<PARAM name="LegendStyle"          value="Horizontal">
<PARAM name="legendfont"           value="Arial,N,10">
<PARAM name="labelOrientation"     value="Up Angle">

<PARAM name="ylabel_pre"           value="$">
<PARAM name="ylabel_font"          value="Arial,N,10">

<PARAM name="popup_pre"            value="$">

<PARAM name="xlabel_font"          value="Arial,N,10">

<?php

echo "<PARAM name=label1               value=$monthname1>";
echo "<PARAM name=label2               value=$monthname2>";
echo "<PARAM name=label3               value=$monthname3>";
echo "<PARAM name=label4               value=$monthname4>";
echo "<PARAM name=label5               value=$monthname5>";
echo "<PARAM name=label6               value=$monthname6>";

echo "</applet></center>";

   
/////P&L////////////////////////////////////////////////////////////////////////
/////////////////////JAVA GRAPH/////////////////////////////////////////
?>
<center><applet code="LineGraphApplet.class" archive="Linegraph.jar" width="600" height="175">

<!-- Start Up Parameters -->
<PARAM name="LOADINGMESSAGE" value="Creating Chart - Please Wait.">
<PARAM name="STEXTCOLOR"     value="#000060">
<PARAM name="STARTUPCOLOR"   value="#FFFFFF">

<!-- Line Data -->
<!--   value,URL,Target Frame -->
<?php
////////////////////////////////////////////////////////////////////////

   /*mysql_connect($dbhost,$username,$password);
   @mysql_select_db($database) or die( "Unable to select database");*/
   $query = "SELECT pl FROM com_kpi WHERE companyid = '$compcook' AND date <= '$today' AND date >= '$searchdate' ORDER BY date DESC";
   $result = Treat_DB_ProxyOld::query($query);
   $num=Treat_DB_ProxyOld::mysql_numrows($result);
   //mysql_close();

   $num--;
   $counter=1;
   while ($num>=0){
      $data="data$counter";
      $series="series1";
      $pl=Treat_DB_ProxyOld::mysql_result($result,$num,"pl");
      echo "<PARAM name=$data$series value=$pl>";
      $counter++;
      $num--;
   }

   /*mysql_connect($dbhost,$username,$password);
   @mysql_select_db($database) or die( "Unable to select database");*/
   $query = "SELECT pl_bgt FROM com_kpi WHERE companyid = '$compcook' AND date <= '$today' AND date >= '$searchdate' ORDER BY date DESC";
   $result = Treat_DB_ProxyOld::query($query);
   $num=Treat_DB_ProxyOld::mysql_numrows($result);
   //mysql_close();

   $num--;
   $counter=1;
   while ($num>=0){
      $data="data$counter";
      $series="series2";
      $pl_bgt=Treat_DB_ProxyOld::mysql_result($result,$num,"pl_bgt");
      echo "<PARAM name=$data$series value=$pl_bgt>";
      $counter++;
      $num--;
   }


?>

<!-- Properties -->
<PARAM name="autoscale"            value="true">

<PARAM name="gridbgcolor"          value="#E8E7E7">

<PARAM name="series1"              value="green|5|7|true|P&L">
<PARAM name="series2"              value="black|3|5|true|Budget">

<PARAM name="ndecplaces"           value="0">
<PARAM name="legend"               value="true">
<PARAM name="LegendStyle"          value="Horizontal">
<PARAM name="legendfont"           value="Arial,N,10">
<PARAM name="labelOrientation"     value="Up Angle">

<PARAM name="ylabel_pre"           value="$">
<PARAM name="ylabel_font"          value="Arial,N,10">

<PARAM name="popup_pre"            value="$">

<PARAM name="xlabel_font"          value="Arial,N,10">

<?php

echo "<PARAM name=label1               value=$monthname1>";
echo "<PARAM name=label2               value=$monthname2>";
echo "<PARAM name=label3               value=$monthname3>";
echo "<PARAM name=label4               value=$monthname4>";
echo "<PARAM name=label5               value=$monthname5>";
echo "<PARAM name=label6               value=$monthname6>";

echo "</applet></center><p>";

  
/////Districts////////////////////////////////////////////////////////////////////////

   /*mysql_connect($dbhost,$username,$password);
   @mysql_select_db($database) or die( "Unable to select database");*/
   $query = "SELECT * FROM district WHERE companyid = '$compcook' ORDER BY districtid DESC";
   $result = Treat_DB_ProxyOld::query($query);
   $num=Treat_DB_ProxyOld::mysql_numrows($result);
   //mysql_close();

   $num--;
   while ($num>=0){

      $districtid=Treat_DB_ProxyOld::mysql_result($result,$num,"districtid");
      $districtname=Treat_DB_ProxyOld::mysql_result($result,$num,"districtname");
      $counter=1;
      $counter2=1;
      $data="data$counter";
      $series="series1";
if ($districtid!=5){
      echo "<center><b>$districtname Budget Variance</b></center>";
/////////////////////JAVA GRAPH/////////////////////////////////////////
?>
<center><applet code="LineGraphApplet.class" archive="Linegraph.jar" width="600" height="200">

<!-- Start Up Parameters -->
<PARAM name="LOADINGMESSAGE" value="Creating Chart - Please Wait.">
<PARAM name="STEXTCOLOR"     value="#000060">
<PARAM name="STARTUPCOLOR"   value="#FFFFFF">

<!-- Line Data -->
<!--   value,URL,Target Frame -->
<?php
////////////////////////////////////////////////////////////////////////
      

      /*mysql_connect($dbhost,$username,$password);
      @mysql_select_db($database) or die( "Unable to select database");*/
      $query2 = "SELECT sales,sales_bgt FROM dist_kpi WHERE companyid = '$compcook' AND districtid = '$districtid' AND date <= '$today' AND date >= '$searchdate' ORDER BY date DESC";
      $result2 = Treat_DB_ProxyOld::query($query2);
      $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
      //mysql_close();
    
      $counter=1;
      $num2--;
      while ($num2>=0){
         $data="data$counter";
         $series="series$counter2";

         $sales=Treat_DB_ProxyOld::mysql_result($result2,$num2,"sales");
         $sales_bgt=Treat_DB_ProxyOld::mysql_result($result2,$num2,"sales_bgt");

         $sales=$sales-$sales_bgt;

         echo "<PARAM name=$data$series value=$sales>";

         $counter++;
         $num2--;
      }
      $counter2++;

      /*mysql_connect($dbhost,$username,$password);
      @mysql_select_db($database) or die( "Unable to select database");*/
      $query2 = "SELECT cos,cos_bgt FROM dist_kpi WHERE companyid = '$compcook' AND districtid = '$districtid' AND date <= '$today' AND date >= '$searchdate' ORDER BY date DESC";
      $result2 = Treat_DB_ProxyOld::query($query2);
      $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
      //mysql_close();
      
      $counter=1;
      $num2--;
      while ($num2>=0){
         $data="data$counter";
         $series="series$counter2";

         $cos=Treat_DB_ProxyOld::mysql_result($result2,$num2,"cos");
         $cos_bgt=Treat_DB_ProxyOld::mysql_result($result2,$num2,"cos_bgt");

         $cos=$cos-$cos_bgt;

         echo "<PARAM name=$data$series value=$cos>";

         $counter++;
         $num2--;
      }
      $counter2++;

      /*mysql_connect($dbhost,$username,$password);
      @mysql_select_db($database) or die( "Unable to select database");*/
      $query2 = "SELECT labor,lbr_bgt FROM dist_kpi WHERE companyid = '$compcook' AND districtid = '$districtid' AND date <= '$today' AND date >= '$searchdate' ORDER BY date DESC";
      $result2 = Treat_DB_ProxyOld::query($query2);
      $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
      //mysql_close();
      
      $counter=1;
      $num2--;
      while ($num2>=0){
         $data="data$counter";
         $series="series$counter2";

         $labor=Treat_DB_ProxyOld::mysql_result($result2,$num2,"labor");
         $lbr_bgt=Treat_DB_ProxyOld::mysql_result($result2,$num2,"lbr_bgt");

         $labor=$labor-$lbr_bgt;

         echo "<PARAM name=$data$series value=$labor>";

         $counter++;
         $num2--;
      }
      $counter2++;

      /*mysql_connect($dbhost,$username,$password);
      @mysql_select_db($database) or die( "Unable to select database");*/
      $query2 = "SELECT toe,toe_bgt FROM dist_kpi WHERE companyid = '$compcook' AND districtid = '$districtid' AND date <= '$today' AND date >= '$searchdate' ORDER BY date DESC";
      $result2 = Treat_DB_ProxyOld::query($query2);
      $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
      //mysql_close();
      
      $counter=1;
      $num2--;
      while ($num2>=0){
         $data="data$counter";
         $series="series$counter2";

         $toe=Treat_DB_ProxyOld::mysql_result($result2,$num2,"toe");
         $toe_bgt=Treat_DB_ProxyOld::mysql_result($result2,$num2,"toe_bgt");

         $toe=$toe-$toe_bgt;

         echo "<PARAM name=$data$series value=$toe>";

         $counter++;
         $num2--;
      }
      $counter2++;

      /*mysql_connect($dbhost,$username,$password);
      @mysql_select_db($database) or die( "Unable to select database");*/
      $query2 = "SELECT pl,pl_bgt FROM dist_kpi WHERE companyid = '$compcook' AND districtid = '$districtid' AND date <= '$today' AND date >= '$searchdate' ORDER BY date DESC";
      $result2 = Treat_DB_ProxyOld::query($query2);
      $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
      //mysql_close();
      
      $counter=1;
      $num2--;
      while ($num2>=0){
         $data="data$counter";
         $series="series$counter2";

         $pl=Treat_DB_ProxyOld::mysql_result($result2,$num2,"pl");
         $pl_bgt=Treat_DB_ProxyOld::mysql_result($result2,$num2,"pl_bgt");

         $pl=$pl-$pl_bgt;

         echo "<PARAM name=$data$series value=$pl>";

         $counter++;
         $num2--;
      }

?>

<!-- Properties -->
<PARAM name="autoscale"            value="true">

<PARAM name="gridbgcolor"          value="#E8E7E7">

<PARAM name="series1"              value="#00FF00|3|5|true|Sales">
<PARAM name="series2"              value="#FF0000|3|5|true|COS">
<PARAM name="series3"              value="#0000FF|3|5|true|Labor">
<PARAM name="series4"              value="#FF8040|3|5|true|TOE">
<PARAM name="series5"              value="#000000|3|5|true|P&L">


<PARAM name="ndecplaces"           value="0">
<PARAM name="legend"               value="true">
<PARAM name="LegendStyle"          value="Horizontal">
<PARAM name="legendfont"           value="Arial,N,10">
<PARAM name="labelOrientation"     value="Up Angle">

<PARAM name="ylabel_pre"           value="$">
<PARAM name="ylabel_font"          value="Arial,N,10">

<PARAM name="popup_pre"            value="$">

<PARAM name="xlabel_font"          value="Arial,N,10">

<?php

echo "<PARAM name=label1               value=$monthname1>";
echo "<PARAM name=label2               value=$monthname2>";
echo "<PARAM name=label3               value=$monthname3>";
echo "<PARAM name=label4               value=$monthname4>";
echo "<PARAM name=label5               value=$monthname5>";
echo "<PARAM name=label6               value=$monthname6>";

echo "</applet></center>";
}
      $num--;
   }
}
?>