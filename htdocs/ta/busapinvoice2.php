<?php

function money($diff){
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+4);
        $diff=round($diff, 2);
        if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
        elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
        $diff = substr($diff, 0, $dot+3);
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      if ($month == "10"){$month="11";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='32')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='08' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      $day=$day-31;
      if ($day<10){$day="0$day";}
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";}
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";}
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";}
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";}
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

define('DOC_ROOT', dirname(dirname(__FILE__)));
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');

$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");
$creditamount="creditamount";
$creditidnty="creditidnty";
$creditdate="creditdate";
$debitamount="debitamount";
$debitidnty="debitidnty";
$debitdate="debitdate";
$quatertotal=0;
$monthtotal=0;
$total=0;
$counter=0;
$findme='.';
$double='.00';
$single='0';
$double2='00';

/*$user=$_COOKIE["usercook"];
$pass=$_COOKIE["passcook"];
$view=$_COOKIE["viewcook"];
$sort_ap=$_COOKIE["sort_ap"];
$subtotal=$_COOKIE["subtotal"];
$date1=$_COOKIE["date1cook"];
$date2=$_COOKIE["date2cook"];
$businessid=$_GET["bid"];
$companyid=$_GET["cid"];
$interco=$_GET["interco"];*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook');
$view = \EE\Controller\Base::getSessionCookieVariable('viewcook');
$sort_ap = \EE\Controller\Base::getSessionCookieVariable('sort_ap');
$subtotal = \EE\Controller\Base::getSessionCookieVariable('subtotal');
$date1 = \EE\Controller\Base::getSessionCookieVariable('date1cook');
$date2 = \EE\Controller\Base::getSessionCookieVariable('date2cook');
$businessid = \EE\Controller\Base::getGetVariable('bid');
$companyid = \EE\Controller\Base::getGetVariable('cid');
$interco = \EE\Controller\Base::getGetVariable('interco');

if ($businessid==""&&$companyid==""){
   /*$businessid=$_POST["businessid"];
   $companyid=$_POST["companyid"];
   $view=$_POST["view"];
   $date1=$_POST["date1"];
   $date2=$_POST["date2"];
   $findinv=$_POST["findinv"];
   $sort_ap=$_POST["sort_ap"];
   $subtotal=$_POST["subtotal"];*/
	
	$businessid = \EE\Controller\Base::getPostVariable('businessid');
	$companyid = \EE\Controller\Base::getPostVariable('companyid');
	$view = \EE\Controller\Base::getPostVariable('view');
	$date1 = \EE\Controller\Base::getPostVariable('date1');
	$date2 = \EE\Controller\Base::getPostVariable('date2');
	$findinv = \EE\Controller\Base::getPostVariable('findinv');
	$sort_ap = \EE\Controller\Base::getPostVariable('sort_ap');
	$subtotal = \EE\Controller\Base::getPostVariable('subtotal');
}

if ($date2==""){
   $date2=$today;$view='All';
}

if ($sort_ap==""){$sort_ap="date";}
if($interco<1){$interco=0;}

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=Treat_DB_ProxyOld::mysql_numrows($result);
//mysql_close();

$loginid=Treat_DB_ProxyOld::mysql_result($result,0,"loginid");
$security_level=Treat_DB_ProxyOld::mysql_result($result,0,"security_level");
$mysecurity=Treat_DB_ProxyOld::mysql_result($result,0,"security");
$bid=Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
$cid=Treat_DB_ProxyOld::mysql_result($result,0,"companyid");
$bid2=Treat_DB_ProxyOld::mysql_result($result,0,"busid2");
$bid3=Treat_DB_ProxyOld::mysql_result($result,0,"busid3");
$bid4=Treat_DB_ProxyOld::mysql_result($result,0,"busid4");
$bid5=Treat_DB_ProxyOld::mysql_result($result,0,"busid5");
$bid6=Treat_DB_ProxyOld::mysql_result($result,0,"busid6");
$bid7=Treat_DB_ProxyOld::mysql_result($result,0,"busid7");
$bid8=Treat_DB_ProxyOld::mysql_result($result,0,"busid8");
$bid9=Treat_DB_ProxyOld::mysql_result($result,0,"busid9");
$bid10=Treat_DB_ProxyOld::mysql_result($result,0,"busid10");

$pr1=Treat_DB_ProxyOld::mysql_result($result,0,"payroll");
$pr2=Treat_DB_ProxyOld::mysql_result($result,0,"pr2");
$pr3=Treat_DB_ProxyOld::mysql_result($result,0,"pr3");
$pr4=Treat_DB_ProxyOld::mysql_result($result,0,"pr4");
$pr5=Treat_DB_ProxyOld::mysql_result($result,0,"pr5");
$pr6=Treat_DB_ProxyOld::mysql_result($result,0,"pr6");
$pr7=Treat_DB_ProxyOld::mysql_result($result,0,"pr7");
$pr8=Treat_DB_ProxyOld::mysql_result($result,0,"pr8");
$pr9=Treat_DB_ProxyOld::mysql_result($result,0,"pr9");
$pr10=Treat_DB_ProxyOld::mysql_result($result,0,"pr10");

if ($num != 1 || ($security_level==1 && ($bid != $businessid || $cid != $companyid)) || $user == "" || $pass == "")
{
    echo "<center><h3>Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
    setcookie("viewcook",$view);
    setcookie("date1cook",$date1);
    setcookie("date2cook",$date2);
    setcookie("sort_ap",$sort_ap);
    setcookie("subtotal",$subtotal);
?>

<html>
<head>
<SCRIPT LANGUAGE="JavaScript" SRC="CalendarPopup.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">document.write(getCalendarStyles());</SCRIPT>

<script language="JavaScript"
   type="text/JavaScript">
function changePage(newLoc)
 {
   nextPage = newLoc.options[newLoc.selectedIndex].value

   if (nextPage != "")
   {
      document.location.href = nextPage
   }
 }
</script>

<STYLE>
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation
			{
			background-color:#6677DD;
			text-align:center;
			vertical-align:center;
			text-decoration:none;
			color:#FFFFFF;
			font-weight:bold;
			}
	.TESTcpDayColumnHeader,
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation,
	.TESTcpCurrentMonthDate,
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDate,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDate,
	.TESTcpCurrentDateDisabled,
	.TESTcpTodayText,
	.TESTcpTodayTextDisabled,
	.TESTcpText
			{
			font-family:arial;
			font-size:8pt;
			}
	TD.TESTcpDayColumnHeader
			{
			text-align:right;
			border:solid thin #6677DD;
			border-width:0 0 1 0;
			}
	.TESTcpCurrentMonthDate,
	.TESTcpOtherMonthDate,
	.TESTcpCurrentDate
			{
			text-align:right;
			text-decoration:none;
			}
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDateDisabled
			{
			color:#D0D0D0;
			text-align:right;
			text-decoration:line-through;
			}
	.TESTcpCurrentMonthDate
			{
			color:#6677DD;
			font-weight:bold;
			}
	.TESTcpCurrentDate
			{
			color: #FFFFFF;
			font-weight:bold;
			}
	.TESTcpOtherMonthDate
			{
			color:#808080;
			}
	TD.TESTcpCurrentDate
			{
			color:#FFFFFF;
			background-color: #6677DD;
			border-width:1;
			border:solid thin #000000;
			}
	TD.TESTcpCurrentDateDisabled
			{
			border-width:1;
			border:solid thin #FFAAAA;
			}
	TD.TESTcpTodayText,
	TD.TESTcpTodayTextDisabled
			{
			border:solid thin #6677DD;
			border-width:1 0 0 0;
			}
	A.TESTcpTodayText,
	SPAN.TESTcpTodayTextDisabled
			{
			height:20px;
			}
	A.TESTcpTodayText
			{
			color:#6677DD;
			font-weight:bold;
			}
	SPAN.TESTcpTodayTextDisabled
			{
			color:#D0D0D0;
			}
	.TESTcpBorder
			{
			border:solid thin #6677DD;
			}
</STYLE>

<SCRIPT LANGUAGE="JavaScript">
<!-- Web Site:  http://dynamicdrive.com -->

<!-- This script and many more are available free online at -->
<!-- The JavaScript Source!! http://javascript.internet.com -->

<!-- Begin
function disableForm(theform) {
if (document.all || document.getElementById) {
for (i = 0; i < theform.length; i++) {
var tempobj = theform.elements[i];
if (tempobj.type.toLowerCase() == "submit" || tempobj.type.toLowerCase() == "reset")
tempobj.disabled = true;
}

}
else {
alert("The form has been submitted.  But, since you're not using IE 4+ or NS 6, the submit button was not disabled on form submission.");
return false;
   }
}
//  End -->
</script>

<SCRIPT LANGUAGE=javascript><!--
function confirmcreate(){return confirm('ARE YOU SURE YOU WANT TO CREATE A TRANSFER?');}
// --></SCRIPT>

<SCRIPT LANGUAGE=javascript><!--
function confirmcreate2(){return confirm('ARE YOU SURE YOU WANT TO CREATE AN EXPENSE REPORT?');}
// --></SCRIPT>

<script language="JavaScript"
   type="text/JavaScript">
function changePageb(newLoc)
 {
   nextPage = newLoc.value

   if (nextPage != "")
   {
      document.location.href = nextPage
   }
 }
</script>

</head>

<?

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM company WHERE companyid = '$companyid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $companyname=Treat_DB_ProxyOld::mysql_result($result,0,"companyname");
	$com_logo=@Treat_DB_ProxyOld::mysql_result($result,0,"logo");

	if($com_logo == ""){$com_logo = "talogo.gif";}

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM security WHERE securityid = '$mysecurity'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $sec_bus_sales=Treat_DB_ProxyOld::mysql_result($result,0,"bus_sales");
    $sec_bus_invoice=Treat_DB_ProxyOld::mysql_result($result,0,"bus_invoice");
    $sec_bus_order=Treat_DB_ProxyOld::mysql_result($result,0,"bus_order");
    $sec_bus_payable=Treat_DB_ProxyOld::mysql_result($result,0,"bus_payable");
    $sec_bus_payable2=Treat_DB_ProxyOld::mysql_result($result,0,"bus_payable2");
    $sec_bus_payable3=Treat_DB_ProxyOld::mysql_result($result,0,"bus_payable3");
    $sec_bus_inventor1=Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor1");
    $sec_bus_control=Treat_DB_ProxyOld::mysql_result($result,0,"bus_control");
    $sec_bus_menu=Treat_DB_ProxyOld::mysql_result($result,0,"bus_menu");
    $sec_bus_order_setup=Treat_DB_ProxyOld::mysql_result($result,0,"bus_order_setup");
    $sec_bus_request=Treat_DB_ProxyOld::mysql_result($result,0,"bus_request");
    $sec_route_collection=Treat_DB_ProxyOld::mysql_result($result,0,"route_collection");
    $sec_route_labor=Treat_DB_ProxyOld::mysql_result($result,0,"route_labor");
    $sec_route_detail=Treat_DB_ProxyOld::mysql_result($result,0,"route_detail");
    $sec_bus_labor=Treat_DB_ProxyOld::mysql_result($result,0,"bus_labor");
    $sec_bus_timeclock=Treat_DB_ProxyOld::mysql_result($result,0,"bus_timeclock");
	$sec_bus_budget=Treat_DB_ProxyOld::mysql_result($result,0,"bus_budget");

    echo "<body>";
    echo "<div style=border:50px solid red;padding:10px;>";
    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%><tr><td colspan=2>";
    if(1==2){echo "<a href=../><img src=logo.jpg border=0 height=43 width=205></a><p></td></tr>";}
    else{echo "<a href=businesstrack.php><img src=logo.jpg border=0 height=43 width=205></a><p></td></tr>";}

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM business WHERE companyid = '$companyid' AND businessid = '$businessid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $businessname=Treat_DB_ProxyOld::mysql_result($result,0,"businessname");
    $businessid=Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
    $street=Treat_DB_ProxyOld::mysql_result($result,0,"street");
    $city=Treat_DB_ProxyOld::mysql_result($result,0,"city");
    $state=Treat_DB_ProxyOld::mysql_result($result,0,"state");
    $zip=Treat_DB_ProxyOld::mysql_result($result,0,"zip");
    $phone=Treat_DB_ProxyOld::mysql_result($result,0,"phone");
    $districtid=Treat_DB_ProxyOld::mysql_result($result,0,"districtid");
    $bus_unit=Treat_DB_ProxyOld::mysql_result($result,0,"unit");

    //echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";
    echo "<tr bgcolor=#CCCCFF><td width=1%><img src=logo/$com_logo></td><td><font size=4><b>$businessname #$bus_unit</b></font><br>$street<br>$city, $state $zip<br>$phone</td>";
    echo "<td align=right valign=top><br>";

    if($security_level<2){$storedisable="DISABLED";}

    echo "<FORM ACTION='editbus.php' method=post name='store'>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<input type=hidden name=businessid value=$businessid>";
    echo "<input type=hidden name=companyid value=$companyid>";
    echo "<INPUT TYPE=submit VALUE=' Store Setup ' $storedisable> ";
    echo "</form></td></tr>";

    //echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";

    echo "<tr><td colspan=3><font color=#999999>";
    if ($sec_bus_sales>0){echo "<a href=busdetail.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Sales</font></a>";}
    if ($sec_bus_invoice>0){echo " | <a href=businvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Invoicing</font></a>";}
    if ($sec_bus_order>0){echo " | <a href=buscatertrack.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Orders</font></a>";}
    if ($sec_bus_payable>0){echo " | <a href=busapinvoice.php?bid=$businessid&cid=$companyid><font color=red onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='red' style='text-decoration:none'>Payables</font></a>";}
    if ($sec_bus_inventor1>0){echo " | <a href=businventor.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Inventory</font></a>";}
    if ($sec_bus_control>0){echo " | <a href=buscontrol.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Control Sheet</font></a>";}
    if ($sec_bus_menu>0){echo " | <a href=buscatermenu.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Menus</font></a>";}
    if ($sec_bus_order_setup>0){echo " | <a href=buscaterroom.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Order Setup</font></a>";}
    if ($sec_bus_request>0){echo " | <a href=busrequest.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Requests</font></a>";}
    if ($sec_route_collection>0||$sec_route_labor>0||$sec_route_detail>0){echo " | <a href=busroute_collect.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Route Collections</font></a>";}
    if ($sec_bus_labor>0||$sec_bus_timeclock){echo " | <a href=buslabor.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Labor</font></a>";}
    if ($sec_bus_budget>0||($businessid == $bid && $pr1 == 1)||($businessid == $bid2 && $pr2 == 1)||($businessid == $bid3 && $pr3 == 1)||($businessid == $bid4 && $pr4 == 1)||($businessid == $bid5 && $pr5 == 1)||($businessid == $bid6 && $pr6 == 1)||($businessid == $bid7 && $pr7 == 1)||($businessid == $bid8 && $pr8 == 1)||($businessid == $bid9 && $pr9 == 1)||($businessid == $bid10 && $pr10 == 1)){echo " | <a href=busbudget.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Budget</font></a>";}
	echo "</td></tr>";

    echo "</table></center><p>";

if ($security_level>1){
    echo "<p><center><table width=90%><tr><td width=50%><form action=busapinvoice.php method=post>";
    echo "<select name=gobus onChange='changePage(this.form.gobus)'>";

    $districtquery="";
    if ($security_level>2&&$security_level<7){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM login_district WHERE loginid = '$loginid'";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();

       $num--;
       while($num>=0){
          $districtid=Treat_DB_ProxyOld::mysql_result($result,$num,"districtid");
          if($districtquery==""){$districtquery="districtid = '$districtid'";}
          else{$districtquery="$districtquery OR districtid = '$districtid'";}
          $num--;
       }
    }

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    if($security_level>6){$query = "SELECT * FROM business WHERE companyid = '$companyid' ORDER BY businessname DESC";}
    elseif($security_level==2||$security_level==1){$query = "SELECT * FROM business WHERE companyid = '$companyid' AND (businessid = '$bid' OR businessid = '$bid2' OR businessid = '$bid3' OR businessid = '$bid4' OR businessid = '$bid5' OR businessid = '$bid6' OR businessid = '$bid7' OR businessid = '$bid8' OR businessid = '$bid9' OR businessid = '$bid10') ORDER BY businessname DESC";}
    elseif($security_level>2&&$security_level<7){$query = "SELECT * FROM business WHERE companyid = '$companyid' AND ($districtquery) ORDER BY businessname DESC";}
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    $num--;
    while($num>=0){
       $businessname=Treat_DB_ProxyOld::mysql_result($result,$num,"businessname");
       $busid=Treat_DB_ProxyOld::mysql_result($result,$num,"businessid");

       if ($busid==$businessid){$show="SELECTED";}
       else{$show="";}

       echo "<option value=busapinvoice2.php?bid=$busid&cid=$companyid $show>$businessname</option>";
       $num--;
    }

    echo "</select></td><td></form></td><td width=50% align=right></td></tr></table></center><p>";
}
else{echo "<form action=nothing.php method=post style=\"margin:0;padding:0;display:inline;\"></form>";}

    $security_level=$sec_bus_payable2;

    $myloginid=$loginid;

    if($security_level<2){$query = "SELECT * FROM apinvoice WHERE businessid = '$businessid' AND transfer = '3' AND date >= '$date1' AND date <= '$date2' AND vendor = '$loginid' AND pc_type = '0' ORDER BY date";}
    else{$query = "SELECT * FROM apinvoice WHERE businessid = '$businessid' AND transfer = '3' AND date >= '$date1' AND date <= '$date2' ORDER BY date";}
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);

    echo "<center><table width=90% border=0 cellspacing=0 cellpadding=0><tr valign=bottom><td colspan=4><FORM ACTION='busapinvoice2.php' method='post'>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<input type=hidden name=businessid value=$businessid>";
    echo "<input type=hidden name=companyid value=$companyid>";
    echo "<table width=100%><tr><td>View ";
    echo "<SCRIPT LANGUAGE='JavaScript' ID='js18'> var cal18 = new CalendarPopup('testdiv1');cal18.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date1 VALUE='$date1' SIZE=8> <A HREF=# onClick=cal18.select(document.forms[2].date1,'anchor18','yyyy-MM-dd'); return false; TITLE=cal18.select(document.forms[2].date1,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor18' ID='anchor18'><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A> to ";
    echo "<SCRIPT LANGUAGE='JavaScript' ID='js19'> var cal19 = new CalendarPopup('testdiv1');cal19.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date2 VALUE='$date2' SIZE=8> <A HREF=# onClick=cal19.select(document.forms[2].date2,'anchor19','yyyy-MM-dd'); return false; TITLE=cal19.select(document.forms[2].date2,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor19' ID='anchor19'><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A> ";
    echo " <INPUT TYPE=submit VALUE='GO'></FORM></td></tr></table></td></tr>";
    //echo "<tr bgcolor=black><td height=1 colspan=4></td></tr></table>";

    if($sec_bus_payable>0||$sec_bus_payable3>0){
       echo "<p><center><table width=90% cellspacing=0 cellpadding=0>";
       echo "<tr bgcolor=#E8E7E7 valign=top><td width=1% bgcolor=#CCCCCC><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=15% bgcolor=#CCCCCC><center><a href=busapinvoice.php?cid=$companyid&bid=$businessid style=$style><font color=blue>Payables</a></center></td><td width=1% align=right bgcolor=#CCCCCC><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1%><img src=leftcrn.jpg height=6 width=6 align=top></td><td width=15%><center><a href=busapinvoice2.php?cid=$companyid&bid=$businessid style=$style style=$style><font color=black>Expense Reports</a></center></td><td width=1% align=right><img src=rightcrn.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1% bgcolor=#CCCCCC><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=15% bgcolor=#CCCCCC><center><a href=busapinvoice3.php?cid=$companyid&bid=$businessid style=$style style=$style><font color=blue>Purchase Orders</a></center></td><td width=1% align=right bgcolor=#CCCCCC><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td bgcolor=white width=60%></td></tr>";
       echo "<tr height=2><td colspan=4></td><td colspan=3 bgcolor=#E8E7E7></td><td></td><td colspan=4></td><td colspan=1></td></tr>";
       echo "</table>";
    }

    echo "<table width=90% cellspacing=0 cellpadding=0><tr bgcolor=#E8E7E7><td><b>Employee</b></td><td><b>Date</b></td><td><b>Status</b></td><td><b>Approved By</b></td><td align=right><b>Total</b></td></tr>";
    echo "<tr bgcolor=black><td height=1 colspan=5></td></tr>";

    $num--;
    while ($num>=-1){
          $receive=0;
          $apinvoiceid=Treat_DB_ProxyOld::mysql_result($result,$num,"apinvoiceid");
          $busid2=Treat_DB_ProxyOld::mysql_result($result,$num,"businessid");
          $vendorid=Treat_DB_ProxyOld::mysql_result($result,$num,"vendor");
          $apinvoicedate=Treat_DB_ProxyOld::mysql_result($result,$num,"date");
          $invoicenum=Treat_DB_ProxyOld::mysql_result($result,$num,"invoicenum");
          $total=Treat_DB_ProxyOld::mysql_result($result,$num,"total");
          $inputtotal=Treat_DB_ProxyOld::mysql_result($result,$num,"invtotal");
          $transfer=Treat_DB_ProxyOld::mysql_result($result,$num,"transfer");
          $pc_type=Treat_DB_ProxyOld::mysql_result($result,$num,"pc_type");
          $posted=Treat_DB_ProxyOld::mysql_result($result,$num,"posted");
          $dm=Treat_DB_ProxyOld::mysql_result($result,$num,"dm");

          if($dm==-1){$dm="";}

          if($posted==0){$showstatus="<font color=red>Pending</font>";}
          else{$showstatus="Submitted";}

          if ($transfer==1&&$vendorid==$businessid){$total=$total*-1;$inputtotal=$inputtotal*-1;$receive=$vendorid;}
          $total=money($total);

          if ($lastsort!=-1&&$subtotal==1&&$sort_ap=="date"&&$lastsort!=$apinvoicedate){$subtotal2=money($subtotal2);echo "<tr bgcolor='#E8E7E7' onMouseOver=this.bgColor='#00FF00' onMouseOut=this.bgColor='#E8E7E7'><td colspan=3><b>Results: $subtot</b></td><td align=right colspan=2><b>$$subtotal2</b></td></tr>";echo "<tr bgcolor=#CCCCCC><td height=1 colspan=6></td></tr>";$subtotal2=0;$subtot=0;}
          elseif ($lastsort!=-1&&$subtotal==1&&$sort_ap=="vendor"&&$lastsort!=$vendorid){$subtotal2=money($subtotal2);echo "<tr bgcolor='#E8E7E7' onMouseOver=this.bgColor='#00FF00' onMouseOut=this.bgColor='#E8E7E7'><td colspan=3><b>Results: $subtot</b></td><td align=right colspan=2><b>$$subtotal2</b></td></tr>";echo "<tr bgcolor=#CCCCCC><td height=1 colspan=6></td></tr>";$subtotal2=0;$subtot=0;}
          elseif ($lastsort!=-1&&$subtotal==1&&$sort_ap=="invoicenum"&&$lastsort!=$invoicenum){$subtotal2=money($subtotal2);echo "<tr bgcolor='#E8E7E7' onMouseOver=this.bgColor='#00FF00' onMouseOut=this.bgColor='#E8E7E7'><td colspan=3><b>Results: $subtot</b></td><td align=right colspan=2><b>$$subtotal2</b></td></tr>";echo "<tr bgcolor=#CCCCCC><td height=1 colspan=6></td></tr>";$subtotal2=0;$subtot=0;}
          elseif ($lastsort!=-1&&$subtotal==1&&$sort_ap=="total"&&$lastsort!=$total){$subtotal2=money($subtotal2);echo "<tr bgcolor='#E8E7E7' onMouseOver=this.bgColor='#00FF00' onMouseOut=this.bgColor='#E8E7E7'><td colspan=3><b>Results: $subtot</b></td><td align=right colspan=2><b>$$subtotal2</b></td></tr>";echo "<tr bgcolor=#CCCCCC><td height=1 colspan=6></td></tr>";$subtotal2=0;$subtot=0;}

          $grandtotal=$grandtotal+$total;
          $subtotal2=$subtotal2+$total;
          $subtot++;

          if ($transfer==0){
             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query2 = "SELECT * FROM vendors WHERE vendorid = '$vendorid'";
             $result2 = Treat_DB_ProxyOld::query($query2);
             //mysql_close();
             $vendorname=Treat_DB_ProxyOld::mysql_result($result2,0,"name");
          }
          elseif ($transfer==1&&$vendorid==$businessid){
             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query2 = "SELECT businessname FROM business WHERE businessid = '$busid2'";
             $result2 = Treat_DB_ProxyOld::query($query2);
             //mysql_close();
             $vendorname=Treat_DB_ProxyOld::mysql_result($result2,0,"businessname");
          }
          elseif ($transfer==1){
             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query2 = "SELECT businessname FROM business WHERE businessid = '$vendorid'";
             $result2 = Treat_DB_ProxyOld::query($query2);
             //mysql_close();
             $vendorname=Treat_DB_ProxyOld::mysql_result($result2,0,"businessname");
          }
          elseif ($transfer==2||$transfer==3){
             if ($pc_type==0){
                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query4 = "SELECT firstname,lastname FROM login WHERE loginid = '$vendorid'";
                $result4 = Treat_DB_ProxyOld::query($query4);
                //mysql_close();
             }
             elseif($pc_type==1){
                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query4 = "SELECT firstname,lastname FROM login_route WHERE login_routeid = '$vendorid'";
                $result4 = Treat_DB_ProxyOld::query($query4);
                //mysql_close();
             }
             $firstname=Treat_DB_ProxyOld::mysql_result($result4,0,"firstname");
             $lastname=Treat_DB_ProxyOld::mysql_result($result4,0,"lastname");

             $vendorname="$lastname, $firstname";
          }

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query3 = "SELECT * FROM apinvoicedetail WHERE apinvoiceid = '$apinvoiceid'";
          $result3 = Treat_DB_ProxyOld::query($query3);
          $num3=Treat_DB_ProxyOld::mysql_numrows($result3);
          //mysql_close();

          $num3--;
          while ($num3>=0){
             $amount2=0;
             $amount2=Treat_DB_ProxyOld::mysql_result($result3,$num3,"amount");
             $apaccountid=Treat_DB_ProxyOld::mysql_result($result3,$num3,"apaccountid");
             $tfr_acct=Treat_DB_ProxyOld::mysql_result($result3,$num3,"tfr_acct");

             if($vendorid==$businessid&&$transfer==1){$apaccountid=$tfr_acct;}

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query4 = "SELECT cos FROM acct_payable WHERE acct_payableid = '$apaccountid'";
             $result4 = Treat_DB_ProxyOld::query($query4);
             //mysql_close();

             $costype=Treat_DB_ProxyOld::mysql_result($result4,0,"cos");

             if ($costype==1&&$num!=-1){
                if($vendorid==$businessid&&$transfer==1){$amount2=$amount2*-1;}
                $costotal=$costotal+$amount2;
             }

             $num3--;
          }

          if (money($total)!=money($inputtotal)){$showerror="<font color=red>*</font>";}
          else {$showerror="";}
          if ($total<0){$invcolor="red";}
          else {$invcolor="blue";}
          if ($total<0){$total="<font color=red>$total</font>";}
          if ($transfer==0){$showcolor="white";$showtitle="Invoice";}
          elseif($transfer==1){$showcolor="yellow";$showtitle="Transfer";}
          elseif($transfer==2){$showcolor="#00FFFF";$showtitle="Purchase Card";}

          if($num!=-1){echo "<tr bgcolor='$showcolor' onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor='$showcolor'><td><a style=$style href=apinvoice.php?bid=$businessid&cid=$companyid&apinvoiceid=$apinvoiceid&goto=3 title='$showtitle'><font color='$invcolor'>$vendorname</font></a></td><td>$apinvoicedate</td><td>$showstatus</td><td>$dm</td><td align=right>$$total$showerror</td></tr>";}
          if($num!=-1){echo "<tr bgcolor=#CCCCCC><td height=1 colspan=7></td></tr>";}

          $num--;

          if ($sort_ap=="date"){$lastsort=$apinvoicedate;}
          elseif ($sort_ap=="vendor"){$lastsort=$vendorid;}
          elseif ($sort_ap=="invoicenum"){$lastsort=$invoicenum;}
          elseif ($sort_ap=="total"){$lastsort=$total;}
    }

    echo "<tr bgcolor=black><td height=1 colspan=5></td></tr>";
    $grandtotal=money($grandtotal);
    echo "<tr><td><b>Results: $results</b></td><td colspan=3></td><td align=right><b>Total: $$grandtotal</b></td></tr>";

    ////////////////////////////NEW EXPENSE REPORT

    echo "<tr><td colspan=5><a name=new></a> <form action='apinvoice.php' method='post' onSubmit='return disableForm(this);'> <u>Expense Report Date</u>: <SCRIPT LANGUAGE='JavaScript' ID='js22'> var cal22 = new CalendarPopup('testdiv1');cal22.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date3 VALUE='$today' SIZE=8> <A HREF=#new onClick=cal22.select(document.forms[3].date3,'anchor22','yyyy-MM-dd'); return false; TITLE=cal22.select(document.forms[3].date3,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor22' ID='anchor22'><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A> ";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<input type=hidden name=businessid value=$businessid>";
    echo "<input type=hidden name=companyid value=$companyid>";
    echo "<input type=hidden name=date value='$today'>";
    echo "<input type=hidden name=new value='yes'>";
    echo "<input type=hidden name=transfer value='3'>";
    echo "<input type=hidden name=goto value='3'>";
    echo " <u>Employee</u>: <select name=vendor>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    if($security_level<2){$query3 = "SELECT * FROM purchase_card WHERE businessid = '$businessid' AND is_deleted = '0' AND loginid = '$loginid' AND type = '0'";}
    else{$query3 = "SELECT * FROM purchase_card WHERE businessid = '$businessid' AND is_deleted = '0'";}
    $result3 = Treat_DB_ProxyOld::query($query3);
    $num3=Treat_DB_ProxyOld::mysql_numrows($result3);
    //mysql_close();

    $num3--;

    while ($num3>=0){
       $loginid=Treat_DB_ProxyOld::mysql_result($result3,$num3,"loginid");
       $type=Treat_DB_ProxyOld::mysql_result($result3,$num3,"type");

       if ($type==0){
          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query4 = "SELECT firstname,lastname FROM login WHERE loginid = '$loginid'";
          $result4 = Treat_DB_ProxyOld::query($query4);
          //mysql_close();
       }
       elseif($type==1){
          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query4 = "SELECT firstname,lastname FROM login_route WHERE login_routeid = '$loginid'";
          $result4 = Treat_DB_ProxyOld::query($query4);
          //mysql_close();
       }
       $firstname=Treat_DB_ProxyOld::mysql_result($result4,0,"firstname");
       $lastname=Treat_DB_ProxyOld::mysql_result($result4,0,"lastname");

       $myvalue="$loginid;$type";

       if($loginid==$myloginid&&$type==0){$sel="SELECTED";}
       else{$sel="";}

       echo "<option value=$myvalue $sel>$lastname, $firstname</option>";
       $num3--;
    }

    echo "</select>";

    echo " <input type=submit value='Create' onclick='return confirmcreate2()'></form></td></tr>";

    echo "</table></center><DIV ID=testdiv1 STYLE=position:absolute;visibility:hidden;background-color:white;layer-background-color:white;></DIV>";

    //mysql_close();

    if(1==2){echo "<br><FORM ACTION=../ method=post>";}
    else{echo "<br><FORM ACTION=businesstrack.php method=post>";}
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<center><INPUT TYPE=submit VALUE=' Return to Main Page'></FORM></center></body>";

	google_page_track();
}
?>