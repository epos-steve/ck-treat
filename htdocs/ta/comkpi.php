<?php

//ini_set('memory_limit','512M');

function money($diff) {
	$diff=round($diff, 0);
	return $diff;
}

function dayofweek($date1)
{
	$day=substr($date1,8,2);
	$month=substr($date1,5,2);
	$year=substr($date1,0,4);
	$dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
	return $dayofweek;
}

function nextday($nextd,$day_format=''){ //Function returns next day of passed date and formats accordingly.
	if($day_format==""){$day_format="Y-m-d";}
	$monn = substr($nextd, 5, 2);
	$dayn = substr($nextd, 8, 2);
	$yearn = substr($nextd, 0,4);
	$tempdate = date($day_format , mktime(0,0,0, $monn, $dayn+1, $yearn));
	return $tempdate;
}

function prevday($prevd,$day_format=''){ //Function returns previous day of passed date and formats accordingly.
	if($day_format==""){$day_format="Y-m-d";}
	$monp = substr($prevd, 5, 2);
	$dayp = substr($prevd, 8, 2);
	$yearp = substr($prevd, 0,4);
	$tempdate = date($day_format , mktime(0,0,0, $monp, $dayp-1, $yearp));
	return $tempdate;
}


if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
require_once( DOC_ROOT . '/bootstrap.php' );

$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");
$creditamount="creditamount";
$creditidnty="creditidnty";
$creditdate="creditdate";
$debitamount="debitamount";
$debitidnty="debitidnty";
$debitdate="debitdate";
$quatertotal=0;
$monthtotal=0;
$total=0;
$counter=0;
$findme='.';
$double='.00';
$single='0';
$double2='00';
$overallpcnt=0;
$message2="";

/*$user = isset($_COOKIE["usercook"])?$_COOKIE["usercook"]:'';
$pass = isset($_COOKIE["passcook"])?$_COOKIE["passcook"]:'';
$businessid = isset($_GET["bid"])?$_GET["bid"]:'';
$companyid = isset($_GET["cid"])?$_GET["cid"]:'';
$prevperiod = isset($_GET["prevperiod"])?$_GET["prevperiod"]:'';
$nextperiod = isset($_GET["nextperiod"])?$_GET["nextperiod"]:'';
$viewmonth = isset($_GET["viewmonth"])?$_GET["viewmonth"]:'';
$update = isset($_GET["update"])?$_GET["update"]:'';
$audit = isset($_GET["audit"])?$_GET["audit"]:'';
$nouser = isset($_GET["nouser"])?$_GET["nouser"]:'';*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook','');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook','');
$businessid = \EE\Controller\Base::getGetVariable('bid','');
$companyid = \EE\Controller\Base::getGetVariable('cid','');
$prevperiod = \EE\Controller\Base::getGetVariable('prevperiod','');
$nextperiod = \EE\Controller\Base::getGetVariable('nextperiod','');
$viewmonth = \EE\Controller\Base::getGetVariable('viewmonth','');
$update = \EE\Controller\Base::getGetVariable('update','');
$audit = \EE\Controller\Base::getGetVariable('audit','');
$nouser = \EE\Controller\Base::getGetVariable('nouser','');

//$update=1;

if ($nouser!="taauto") 
{
	echo "<center><h2>Failed</h2></center>";
}

else
{

	$companyid = 1;

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( mysql_error());
	$query = "SELECT * FROM company WHERE companyid = '$companyid'";
	$result = Treat_DB_ProxyOld::query( $query );
	//mysql_close();

	$companyname = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'companyname' );

	echo "Starting $companyname KPI's...<br>";
	echo "Please wait until the page finishes loading (Bottom of page says finished)...<p>";

	/////////MAIN LOOP///////////////////////////////////////////////////////////////////////////////////////////
	$comsales=0;
	$commtdsales=0;
	$comcos=0;
	$commtdcos=0;
	$comlbr=0;
	$commtdlbr=0;
	$comtoe=0;
	$commtdtoe=0;
	$compl=0;
	$commtdpl=0;
	$comsalesbgt=0;
	$commtdsalesbgt=0;
	$comtotsalesbgt=0;
	$comcosbgt=0;
	$commtdcosbgt=0;
	$comtotcosbgt=0;
	$comlbrbgt=0;
	$commtdlbrbgt=0;
	$comtotlbrbgt=0;
	$comtoebgt=0;
	$commtdtoebgt=0;
	$comtottoebgt=0;
	$complbgt=0;
	$commtdplbgt=0;
	$comtotplbgt=0;

	$myFile = DIR_COMKPI . '/comkpi.htm';
	$fh = fopen($myFile, 'w') or die("can't open file");
	$myFile2 = DIR_COMKPI . '/comkpi1.htm';
	$fh2 = fopen($myFile2, 'w') or die("can't open file");
	fwrite($fh, "<html><table>");
	fwrite($fh2, "<html><table>");
	$myFile4 = DIR_COMKPI . '/comkpi4.htm';
	$fh4 = fopen($myFile4, 'w') or die("can't open file");
	fwrite($fh4, "<html><table>");

	/////NEW BUDGET VARIANCE
	$myFile9 = DIR_COMKPI . '/comkpi9.xls';
	$fh9 = fopen($myFile9, 'w') or die("can't open file");
	fwrite($fh9, "<html><table border=1><tr bgcolor=#FFFF99><td><b>Unit#</b></td><td><b>Unit</b></td><td><b>Comp.</b></td><td><b>Sales</b></td><td><b>Sales BGT</b></td><td bgcolor=#CCCCCC><b>VAR</b></td><td><b>COS</b></td><td><b>COS BGT</b></td><td bgcolor=#CCCCCC><b>VAR</b></td><td><b>Labor</b></td><td><b>Labor BGT</b></td><td bgcolor=#CCCCCC><b>VAR</b></td><td><b>TOE</b></td><td><b>TOE BGT</b></td><td bgcolor=#CCCCCC><b>VAR</b></td><td><b>AMT DUE</b></td><td><b>AMT DUE BGT</b></td><td bgcolor=#CCCCCC><b>VAR</b></td><td><b>P&L</b></td><td><b>P&L BGT</b></td><td bgcolor=#CCCCCC><b>VAR</b></td><td><b>Category</b></td><td><b>Association</b></td></tr>");
	$newbudget="";

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( mysql_error());
	$query99 = "SELECT * FROM business WHERE companyid != '2' AND skpi = '0' ORDER BY districtid DESC,unit DESC";
	$result99 = Treat_DB_ProxyOld::query( $query99 );
	$num99 = Treat_DB_ProxyOld::mysql_numrows( $result99 );
	//mysql_close();

	if ($viewmonth!=""){$startdate=$viewmonth;$temp_date=$today;}
	else {$startdate=$today;$temp_date=$today;}

	/////temp override for date
	//$startdate = "2010-12-10";
	//$temp_date = "2010-12-10";

	//////////////////WEEK GROUPS
	$whichweek=1;
	$cs_week_start[$whichweek]=$startdate;
	$cs_enddate_year=substr($startdate,0,4);
	$cs_enddate_month=substr($startdate,5,2);
	$cs_enddate_month++;
	if($cs_enddate_month==13){$cs_enddate_month=1;$cs_enddate_year++;}
	if($cs_enddate_month<10){$cs_enddate_month="0$cs_enddate_month";}
	$cs_enddate="$cs_enddate_year-$cs_enddate_month-01";
	$cs_enddate=prevday($cs_enddate);

	$tempdate=$startdate;
	while($tempdate<=$cs_enddate){
		if(dayofweek($tempdate)==$week_end||$tempdate==$cs_enddate){
			$cs_week_end[$whichweek]=$tempdate;
			$whichweek++;
			$tempdate=nextday($tempdate);
			if($tempdate<=$cs_enddate){$cs_week_start[$whichweek]=$tempdate;}
		}
		else{$tempdate=nextday($tempdate);}
	}

	//////////////////END WEEK GROUPS

	$lastdistrictid=0;
	$num99--;
	while ($num99>=0){

		//////set time limit
		ini_set('max_execution_time','100');
		$b = ini_get(max_execution_time);
		echo "time limit reset to $b<br>";

		$message="";
		$newbudget="";
		$curbusinessid = @Treat_DB_ProxyOld::mysql_result( $result99, $num99, 'businessid' );

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( mysql_error());
		$query = "SELECT * FROM business WHERE businessid = '$curbusinessid'";
		$result = Treat_DB_ProxyOld::query( $query );
		//mysql_close();

		$businessname = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'businessname' );
		$businessid = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'businessid' );
		$companyid = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'companyid' );
		$categoryid = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'categoryid' );
		$street = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'street' );
		$city = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'city' );
		$state = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'state' );
		$zip = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'zip' );
		$phone = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'phone' );
		$over_short_acct = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'over_short_acct' );
		$count1 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'count1' );
		$count2 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'count2' );
		$count3 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'count3' );
		$benefitprcnt = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'benefit' );
		$operate = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'operate' );
		$unit = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'unit' );
		$districtid = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'districtid' );
		$cs_amounttype = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'cs_amounttype' );
		$cs_amount = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'cs_amount' );
		$cs_special = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'cs_special' );
		$sort = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'sort' );

		echo "<font color=blue>$unit - $businessname</font><br>";

		$query = "SELECT * FROM business_category WHERE categoryid = '$categoryid'";
		$result = Treat_DB_ProxyOld::query( $query );

		$category_name = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'category_name' );

		$query = "SELECT code FROM company WHERE companyid = '$companyid'";
		$result = Treat_DB_ProxyOld::query( $query );

		$com_code = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'code' );

		if ($lastdistrictid!=$districtid){
			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( mysql_error());
			$query98 = "SELECT * FROM district WHERE districtid = '$districtid'";
			$result98 = Treat_DB_ProxyOld::query( $query98 );
			//mysql_close();

			$message3="<table>$message3</table>";

			if ($districtname!=""){

				fwrite($fh4, "</table></html>");
				fclose($fh4);

				$fileatt = DIR_COMKPI . '/comkpi4.htm'; // Path to the file 
				$fileatt_type = "application/octet-stream"; // File Type 
				$fileatt_name = "KPI.htm"; // Filename that will be used for the file as the attachment 

				$email_from = "support@treatamerica.com"; // Who the email is from 
				$email_subject = "$districtname Weekly KPI Exceptions"; // The Subject of the email 
				$email_txt = "Weekly KPI's. Please view attachment."; // Message that the email has in it 

				$headers = "From: ".$email_from; 

				$file = fopen($fileatt,'rb'); 
				$data = fread($file,filesize($fileatt)); 
				fclose($file); 

				$semi_rand = md5(time()); 
				$mime_boundary = "==Multipart_Boundary_x{$semi_rand}x"; 

				$headers .= "\nMIME-Version: 1.0\n" . 
				"Content-Type: multipart/mixed;\n" . 
				" boundary=\"{$mime_boundary}\""; 

				$email_message .= "This is a multi-part message in MIME format.\n\n" . 
				"--{$mime_boundary}\n" . 
				"Content-Type:text/html; charset=\"iso-8859-1\"\n" . 
				"Content-Transfer-Encoding: 7bit\n\n" . 
				$email_message . "\n\n"; 

				$data = chunk_split(base64_encode($data)); 

				$email_message .= "--{$mime_boundary}\n" . 
				"Content-Type: {$fileatt_type};\n" . 
				" name=\"{$fileatt_name}\"\n" . 
				//"Content-Disposition: attachment;\n" . 
				//" filename=\"{$fileatt_name}\"\n" . 
				"Content-Transfer-Encoding: base64\n\n" . 
				$data . "\n\n" . 
				"--{$mime_boundary}--\n"; 

				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( mysql_error());
				$query56 = "SELECT * FROM login_district WHERE districtid = '$lastdistrictid'";
				$result56 = Treat_DB_ProxyOld::query( $query56 );
				$num56 = Treat_DB_ProxyOld::mysql_numrows( $result56 );
				//mysql_close();

				$num56--;
				while ($num56>=0){
					$empl_loginid = @Treat_DB_ProxyOld::mysql_result( $result56, $num56, 'loginid' );

					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( mysql_error());
					$query76 = "SELECT email FROM login WHERE loginid = '$empl_loginid' AND receive_kpi = '0' AND is_deleted = '0'";
					$result76 = Treat_DB_ProxyOld::query( $query76 );
					//mysql_close();

					$to = @Treat_DB_ProxyOld::mysql_result( $result76, 0, 'email' );

					if ($update==1){}
					else{$ok = @mail($to, $email_subject, $email_message, $headers);} 

					if($ok) { 
						echo "<font color=red>$to emailed...</font><br>"; 
					} else { 
						echo "<font color=red>Sorry but the email could not be sent.</font><br>";}

					$num56--;
				}

				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( mysql_error());
				$query92 = "SELECT date FROM dist_kpi WHERE companyid = '$companyid' AND districtid = '$lastdistrictid' AND date = '$startdate'";
				$result92 = Treat_DB_ProxyOld::query( $query92 );
				$num92 = Treat_DB_ProxyOld::mysql_numrows( $result92 );
				//mysql_close();

				if ($num92==0){
					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( mysql_error());
					$query95 = "INSERT INTO dist_kpi (companyid,districtid,sales,cos,labor,toe,pl,date,sales_bgt,cos_bgt,lbr_bgt,toe_bgt,pl_bgt) VALUES ('$companyid','$lastdistrictid','$distmtdsales','$distmtdcos','$distmtdlbr','$distmtdtoe','$distmtdpl','$startdate','$distmtdsalesbgt','$distmtdcosbgt','$distmtdlbrbgt','$distmtdtoebgt','$distmtdplbgt')";
					$result95 = Treat_DB_ProxyOld::query( $query95 );
					//mysql_close();
				}
				else{
					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( mysql_error());
					$query95 = "UPDATE dist_kpi SET sales = '$distmtdsales',cos = '$distmtdcos',labor = '$distmtdlbr',toe = '$distmtdtoe',pl = '$distmtdpl',sales_bgt = '$distmtdsalesbgt',cos_bgt = '$distmtdcosbgt',lbr_bgt = '$distmtdlbrbgt',toe_bgt = '$distmtdtoebgt',pl_bgt = '$distmtdplbgt' WHERE date = '$startdate' AND companyid = '$companyid' AND districtid = '$lastdistrictid'";
					$result95 = Treat_DB_ProxyOld::query( $query95 );
					//mysql_close();
				}

				echo "<font color=red>Updating District KPI's...</font><br>";

				$distcos=round($distcos/$distsales*100,1);
				$distlbr=round($distlbr/$distsales*100,1);
				$disttoe=round($disttoe/$distsales*100,1);
				$distcosbgt=round($distcosbgt/$distsalesbgt*100,1);
				$distlbrbgt=round($distlbrbgt/$distsalesbgt*100,1);
				$disttoebgt=round($disttoebgt/$distsalesbgt*100,1);
				$distmtdcos=round($distmtdcos/$distmtdsales*100,1);
				$distmtdlbr=round($distmtdlbr/$distmtdsales*100,1);
				$distmtdtoe=round($distmtdtoe/$distmtdsales*100,1);
				$distmtdcosbgt=round($distmtdcosbgt/$distmtdsalesbgt*100,1);
				$distmtdlbrbgt=round($distmtdlbrbgt/$distmtdsalesbgt*100,1);
				$distmtdtoebgt=round($distmtdtoebgt/$distmtdsalesbgt*100,1);
				$distbgtcosprcnt=round($disttotcosbgt/$disttotsalesbgt*100,1);
				$distbgtlbrprcnt=round($disttotlbrbgt/$disttotsalesbgt*100,1);
				$distbgttoeprcnt=round($disttottoebgt/$disttotsalesbgt*100,1);
				$distbgtplprcnt=round($disttotplbgt/$disttotsalesbgt*100,1);

				$distsales=number_format($distsales);
				$distsalesbgt=number_format($distsalesbgt);
				$distmtdsales=number_format($distmtdsales);
				$distmtdsalesbgt=number_format($distmtdsalesbgt);
				$distpl=number_format($distpl);
				$distplbgt=number_format($distplbgt);
				$distmtdpl=number_format($distmtdpl);
				$distmtdplbgt=number_format($distmtdplbgt);
				$disttotplbgt=number_format($disttotplbgt);
				$disttotsalesbgt=number_format($disttotsalesbgt);
				$disttotcosbgt=number_format($disttotcosbgt);
				$disttotlbrbgt=number_format($disttotlbrbgt);
				$disttottoebgt=number_format($disttottoebgt);

				fwrite($fh, "<tr><td colspan=11 bgcolor=#CCCCFF><b>$districtname Totals</b></td></tr>");
				fwrite($fh, "<tr><td colspan=2 bgcolor=#E8E7E7><center><font color=black>Last Week</td><td colspan=2 bgcolor=#E8E7E7><center><font color=black>LW Bdgt</td><td colspan=2 bgcolor=#E8E7E7><center><font color=black>Month to Date</td><td colspan=2 bgcolor=#E8E7E7><center><font color=black>MTD Bdgt</td><td bgcolor=#E8E7E7 colspan=3><center>Total Month Bdgt</center></td></tr>");
				fwrite($fh, "<tr><td align=right>SALES:</td><td align=right>$$distsales</td><td align=right></td><td align=right>$$distsalesbgt</td><td align=right></td><td align=right>$$distmtdsales</td><td align=right></td><td align=right>$$distmtdsalesbgt</td><td align=right></td><td align=right>$$disttotsalesbgt</td><td align=right></td></tr>");
				fwrite($fh, "<tr><td align=right>COS:</td><td align=right>$distcos%</td><td align=right></td><td align=right>$distbgtcosprcnt%</td><td align=right></td><td align=right>$distmtdcos%</td><td align=right></td><td align=right>$distbgtcosprcnt%</td><td align=right></td><td align=right>$$disttotcosbgt</td><td align=right>$distbgtcosprcnt%</td></tr>");
				fwrite($fh, "<tr><td align=right>LABOR:</td><td align=right>$distlbr%</td><td align=right></td><td align=right>$distbgtlbrprcnt%</td><td align=right></td><td align=right>$distmtdlbr%</td><td align=right></td><td align=right>$distbgtlbrprcnt%</td><td align=right></td><td align=right>$$disttotlbrbgt</td><td align=right>$distbgtlbrprcnt%</td></tr>");
				fwrite($fh, "<tr><td align=right>TOE:</td><td align=right>$disttoe%</td><td align=right></td><td align=right>$distbgttoeprcnt%</td><td align=right></td><td align=right>$distmtdtoe%</td><td align=right></td><td align=right>$distbgttoeprcnt%</td><td align=right></td><td align=right>$$disttottoebgt</td><td align=right>$distbgttoeprcnt%</td></tr>");
				fwrite($fh, "<tr><td align=right>P&L:</td><td align=right>$$distpl</td><td align=right></td><td align=right>$$distplbgt</td><td align=right></td><td align=right>$$distmtdpl</td><td align=right></td><td align=right>$$distmtdplbgt</td><td align=right></td><td align=right>$$disttotplbgt</td><td align=right>$distbgtplprcnt%</td></tr>");

				$myFile4 = DIR_COMKPI . '/comkpi4.htm';
				$fh4 = fopen($myFile4, 'w') or die("can't open file");
				fwrite($fh4, "<html><table>");
			}

			//if ($message3!=""){mail($to, $subject, $message3, $from);}
			$message3="";

			$districtname = @Treat_DB_ProxyOld::mysql_result( $result98, 0, 'districtname' );
			//$dist_id = @mysql_result( $result98, 0, 'districtid' );
			fwrite($fh, "<tr bgcolor=#CCCCFF><td colspan=11><center><font size=5><b>$districtname</center></td></tr>");
			fwrite($fh2, "<tr bgcolor=#CCCCFF><td colspan=11><center><font size=5><b>$districtname</center></td></tr>");
			fwrite($fh4, "<tr bgcolor=#CCCCFF><td colspan=11><center><font size=5><b>$districtname</center></td></tr>");
			$message="";
			$newbudget="";

			$distsales=0;
			$distmtdsales=0;
			$distcos=0;
			$distmtdcos=0;
			$distlbr=0;
			$distmtdlbr=0;
			$disttoe=0;
			$distmtdtoe=0;
			$distpl=0;
			$distmtdpl=0;
			$distsalesbgt=0;
			$distmtdsalesbgt=0;
			$disttotsalesbgt=0;
			$distcosbgt=0;
			$distmtdcosbgt=0;
			$disttotcosbgt=0;
			$distlbrbgt=0;
			$distmtdlbrbgt=0;
			$disttotlbrbgt=0;
			$disttoebgt=0;
			$distmtdtoebgt=0;
			$disttottoebgt=0;
			$distplbgt=0;
			$distmtdplbgt=0;
			$disttotplbgt=0;
		}
		
		$message = "$message<tr><td colspan=11 bgcolor=#E8E7E7><b>$unit - $businessname</b></td></tr>";
		$message = "$message<tr><td colspan=2 bgcolor=#E8E7E7><center><font color=black>Last Week</td><td colspan=2 bgcolor=#E8E7E7><center><font color=black>LW Bdgt</td><td colspan=2 bgcolor=#E8E7E7><center><font color=black>Month to Date</td><td colspan=2 bgcolor=#E8E7E7><center><font color=black>MTD Bdgt</td><td bgcolor=#E8E7E7 colspan=3><center>Total Month Bdgt</center></td></tr>";

		$newbudget = "$newbudget<tr><td>$unit</td><td>$businessname</td><td>$com_code</td>";

		$lastdistrictid = $districtid;
		$showyear = $year;
		$gomonth = substr( $viewmonth, 5, 2 );
		$goyear = substr( $viewmonth, 0, 4 );

		if ($gomonth=="01" || ($viewmonth==""&&$month=="01")){$showm1="SELECTED";}
		if ($gomonth=="02" || ($viewmonth==""&&$month=="02")){$showm2="SELECTED";}
		if ($gomonth=="03" || ($viewmonth==""&&$month=="03")){$showm3="SELECTED";}
		if ($gomonth=="04" || ($viewmonth==""&&$month=="04")){$showm4="SELECTED";}
		if ($gomonth=="05" || ($viewmonth==""&&$month=="05")){$showm5="SELECTED";}
		if ($gomonth=="06" || ($viewmonth==""&&$month=="06")){$showm6="SELECTED";}
		if ($gomonth=="07" || ($viewmonth==""&&$month=="07")){$showm7="SELECTED";}
		if ($gomonth=="08" || ($viewmonth==""&&$month=="08")){$showm8="SELECTED";}
		if ($gomonth=="09" || ($viewmonth==""&&$month=="09")){$showm9="SELECTED";}
		if ($gomonth=="10" || ($viewmonth==""&&$month=="10")){$showm10="SELECTED";}
		if ($gomonth=="11" || ($viewmonth==""&&$month=="11")){$showm11="SELECTED";}
		if ($gomonth=="12" || ($viewmonth==""&&$month=="12")){$showm12="SELECTED";}

		//////START TABLE//////////////////////////////////////////////////////////////////////////////////

		if ($viewmonth!=""){$startdate=$viewmonth;$temp_date=$today;}
		else {$startdate=$today;$temp_date=$today;}

		$totaloperate=0;
		$operate1=0;
		$operate2=0;
		$operate3=0;
		$operate4=0;
		$operate5=0;
		$operate6=0;

		$startmonth=substr($startdate,5,2);
		$startyear=substr($startdate,0,4);
		$startmonth++;
		if ($startmonth==13){$startmonth="01";$startyear++;$viewnext="$startyear-$startmonth-01";}
		else {
			if ($startmonth<10){$startmonth="0$startmonth";}
			$viewnext="$startyear-$startmonth-01";
		}
		
		$startmonth=substr($startdate,5,2);
		$startyear=substr($startdate,0,4);
		$startmonth--;
		if ($startmonth==0){$startmonth="12";$startyear--;$viewprev="$startyear-$startmonth-01";}
		else {
			if ($startmonth<10){$startmonth="0$startmonth";}
			$viewprev="$startyear-$startmonth-01";
		}
		
		$currentmonth = date("m");
		$weekday = date("l");
		$startmonth=substr($startdate,5,2);
		$startyear=substr($startdate,0,4);
		$startdate="$startyear-$startmonth-01";

		$leap = date("L");
		if ($startmonth==1 || $startmonth==3 || $startmonth==5 || $startmonth==7 || $startmonth==8 || $startmonth==10 || $startmonth==12){$endingdate=31;}
		elseif ($startmonth==4 || $startmonth==6 || $startmonth==9 || $startmonth==11){$endingdate=30;}
		elseif ($startmonth==2 && $leap==1){$endingdate=29;}
		elseif ($startmonth==2 && $leap==0){$endingdate=28;}
		$enddate="$startyear-$startmonth-$endingdate";

		if ($startmonth=="01"){$showmonth="January";}
		elseif ($startmonth=="02"){$showmonth="February";}
		elseif ($startmonth=="03"){$showmonth="March";}
		elseif ($startmonth=="04"){$showmonth="April";}
		elseif ($startmonth=="05"){$showmonth="May";}
		elseif ($startmonth=="06"){$showmonth="June";}
		elseif ($startmonth=="07"){$showmonth="July";}
		elseif ($startmonth=="08"){$showmonth="August";}
		elseif ($startmonth=="09"){$showmonth="September";}
		elseif ($startmonth=="10"){$showmonth="October";}
		elseif ($startmonth=="11"){$showmonth="November";}
		elseif ($startmonth=="12"){$showmonth="December";}

		$weekday = date("l");
		$temp_date=$today;
		while ($temp_date>$startdate){
			$temp_date=prevday($temp_date);
			if ($weekday=="Sunday"){$weekday="Saturday";}
			elseif ($weekday=="Monday"){$weekday="Sunday";}
			elseif ($weekday=="Tuesday"){$weekday="Monday";}
			elseif ($weekday=="Wednesday"){$weekday="Tuesday";}
			elseif ($weekday=="Thursday"){$weekday="Wednesday";}
			elseif ($weekday=="Friday"){$weekday="Thursday";}
			elseif ($weekday=="Saturday"){$weekday="Friday";}
		}
		$startweekday = $weekday;

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( mysql_error());
		$query = "SELECT * FROM credits WHERE (credittype = '1' OR credittype = '5') AND businessid = '$businessid' AND ((is_deleted = '0') OR (is_deleted = '1' AND del_date >= '$startdate')) ORDER BY credittype DESC, creditname DESC";
		$result = Treat_DB_ProxyOld::query( $query );
		$num = Treat_DB_ProxyOld::mysql_numrows( $result );
		//mysql_close();

		$weekcount=1;
		$goweek=0;
		$weekdayname=$weekday;
		$startweekdayname=$weekday;
		$day=$startdate;

		while ($day<=$enddate){
			if ($weekdayname=="Sunday"){$weekdayname="Monday";}
			elseif ($weekdayname=="Saturday"){$weekdayname="Sunday";}
			elseif ($weekdayname=="Friday"){$weekdayname="Saturday";}
			elseif ($weekdayname=="Thursday"&&$day!=$enddate){$weekdayname="Friday";$weekcount++;}
			elseif ($weekdayname=="Thursday"&&$day==$enddate){$weekdayname="Friday";}
			elseif ($weekdayname=="Wednesday"){$weekdayname="Thursday";}
			elseif ($weekdayname=="Tuesday"){$weekdayname="Wednesday";}
			elseif ($weekdayname=="Monday"){$weekdayname="Tuesday";}
			if ($today==$day){$goweek=$weekcount-1;}
			$day=nextday($day);
		}
		if ($goweek==0){$goweek=$weekcount;}

		if ($weekcount==4){$colnum=14;}
		elseif($weekcount==5){$colnum=16;}
		elseif($weekcount==6){$colnum=18;}

		///////Operating DAys and Budget////////////////////////////////////////////////////////////////////////////////////////////////
		$wk1laborhour=0;
		$wk2laborhour=0;
		$wk3laborhour=0;
		$wk4laborhour=0;
		$wk5laborhour=0;
		$wk6laborhour=0;

		$cashwk1=0;
		$cashwk2=0;
		$cashwk3=0;
		$cashwk4=0;
		$cashwk5=0;
		$cashwk6=0;

		$day=$startdate;
		$weekdayname2=dayofweek($startdate);
		$currentweek=1;
		while ($day<=$enddate){  
			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( mysql_error());
			$query5 = "SELECT * FROM stats WHERE businessid = '$businessid' AND date = '$day'";
			$result5 = Treat_DB_ProxyOld::query( $query5 );
			//mysql_close();

			$closed = @Treat_DB_ProxyOld::mysql_result( $result5, 0, 'closed' );
			$labor_hour = @Treat_DB_ProxyOld::mysql_result( $result5, 0, 'labor_hour' );

			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( mysql_error());
			$query6 = "SELECT * FROM cash WHERE businessid = '$businessid' AND date = '$day'";
			$result6 = Treat_DB_ProxyOld::query( $query6 );
			//mysql_close();
			$cashamount = @Treat_DB_ProxyOld::mysql_result( $result6, 0, 'amount' );
			$cashtype = @Treat_DB_ProxyOld::mysql_result( $result6, 0, 'type' );
 
			if ($currentweek==1){if (($weekdayname2!="Sunday"&&$weekdayname2!="Saturday"&&$operate==5&&$closed!=1)||($weekdayname2!="Sunday"&&$operate==6&&$closed!=1)||($operate==7&&$closed!=1)){$operate1++;$totaloperate++;}$wk1laborhour=$wk1laborhour+$labor_hour;}
			elseif ($currentweek==2){if (($weekdayname2!="Sunday"&&$weekdayname2!="Saturday"&&$operate==5&&$closed!=1)||($weekdayname2!="Sunday"&&$operate==6&&$closed!=1)||($operate==7&&$closed!=1)){$operate2++;$totaloperate++;}$wk2laborhour=$wk2laborhour+$labor_hour;}
			elseif ($currentweek==3){if (($weekdayname2!="Sunday"&&$weekdayname2!="Saturday"&&$operate==5&&$closed!=1)||($weekdayname2!="Sunday"&&$operate==6&&$closed!=1)||($operate==7&&$closed!=1)){$operate3++;$totaloperate++;}$wk3laborhour=$wk3laborhour+$labor_hour;}
			elseif ($currentweek==4){if (($weekdayname2!="Sunday"&&$weekdayname2!="Saturday"&&$operate==5&&$closed!=1)||($weekdayname2!="Sunday"&&$operate==6&&$closed!=1)||($operate==7&&$closed!=1)){$operate4++;$totaloperate++;}$wk4laborhour=$wk4laborhour+$labor_hour;}
			elseif ($currentweek==5){if (($weekdayname2!="Sunday"&&$weekdayname2!="Saturday"&&$operate==5&&$closed!=1)||($weekdayname2!="Sunday"&&$operate==6&&$closed!=1)||($operate==7&&$closed!=1)){$operate5++;$totaloperate++;}$wk5laborhour=$wk5laborhour+$labor_hour;}
			elseif ($currentweek==6){if (($weekdayname2!="Sunday"&&$weekdayname2!="Saturday"&&$operate==5&&$closed!=1)||($weekdayname2!="Sunday"&&$operate==6&&$closed!=1)||($operate==7&&$closed!=1)){$operate6++;$totaloperate++;}$wk6laborhour=$wk6laborhour+$labor_hour;}
			
			if ($currentweek==1){if($cashtype==0){$cashwk1=$cashwk1+$cashamount;}else{$cashwk1=$cashwk1-$cashamount;}}
			elseif ($currentweek==2){if($cashtype==0){$cashwk2=$cashwk2+$cashamount;}else{$cashwk2=$cashwk2-$cashamount;}}
			elseif ($currentweek==3){if($cashtype==0){$cashwk3=$cashwk3+$cashamount;}else{$cashwk3=$cashwk3-$cashamount;}}
			elseif ($currentweek==4){if($cashtype==0){$cashwk4=$cashwk4+$cashamount;}else{$cashwk4=$cashwk4-$cashamount;}}
			elseif ($currentweek==5){if($cashtype==0){$cashwk5=$cashwk5+$cashamount;}else{$cashwk5=$cashwk5-$cashamount;}}
			elseif ($currentweek==6){if($cashtype==0){$cashwk6=$cashwk6+$cashamount;}else{$cashwk6=$cashwk6-$cashamount;}}

			if ($weekdayname2=="Sunday"){$weekdayname2="Monday";}
			elseif ($weekdayname2=="Saturday"){$weekdayname2="Sunday";}
			elseif ($weekdayname2=="Friday"){$weekdayname2="Saturday";}
			elseif ($weekdayname2=="Thursday"){$weekdayname2="Friday";$currentweek++;}
			elseif ($weekdayname2=="Wednesday"){$weekdayname2="Thursday";}
			elseif ($weekdayname2=="Tuesday"){$weekdayname2="Wednesday";}
			elseif ($weekdayname2=="Monday"){$weekdayname2="Tuesday";}
			$day = nextday( $day );
		}

		echo "$wk1laborhour,$wk2laborhour,$wk3laborhour,$wk4laborhour,$wk5laborhour,$wk6laborhour<br>";

		$year5=substr($startdate,0,4);
		$month5=substr($startdate,5,2);

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( mysql_error());
		$query2 = "SELECT * FROM budget WHERE businessid = '$unit' and type = '10' and year = '$year5'";
		$result2 = Treat_DB_ProxyOld::query( $query2 );
		//mysql_close();
		$bgtotsales=@Treat_DB_ProxyOld::mysql_result($result2,0,$month5);

		$comtotsalesbgt=$comtotsalesbgt+$bgtotsales;
		$disttotsalesbgt=$disttotsalesbgt+$bgtotsales;

		if ($goweek==1){$wksalesbgt=$bgtotsales/$totaloperate*$operate1;$mtdsalesbgt=$wksalesbgt;}
		elseif ($goweek==2){$wksalesbgt=$bgtotsales/$totaloperate*$operate2;$mtdsalesbgt=$bgtotsales/$totaloperate*($operate1+$operate2);}
		elseif ($goweek==3){$wksalesbgt=$bgtotsales/$totaloperate*$operate3;$mtdsalesbgt=$bgtotsales/$totaloperate*($operate1+$operate2+$operate3);}
		elseif ($goweek==4){$wksalesbgt=$bgtotsales/$totaloperate*$operate4;$mtdsalesbgt=$bgtotsales/$totaloperate*($operate1+$operate2+$operate3+$operate4);}
		elseif ($goweek==5){$wksalesbgt=$bgtotsales/$totaloperate*$operate5;$mtdsalesbgt=$bgtotsales/$totaloperate*($operate1+$operate2+$operate3+$operate4+$operate5);}
		elseif ($goweek==6){$wksalesbgt=$bgtotsales/$totaloperate*$operate6;$mtdsalesbgt=$bgtotsales/$totaloperate*($operate1+$operate2+$operate3+$operate4+$operate5+$operate6);}

		$comsalesbgt=$wksalesbgt+$comsalesbgt;
		$commtdsalesbgt=$mtdsalesbgt+$commtdsalesbgt;
		$distsalesbgt=$wksalesbgt+$distsalesbgt;
		$distmtdsalesbgt=$mtdsalesbgt+$distmtdsalesbgt;

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( mysql_error());
		$query2 = "SELECT * FROM budget WHERE businessid = '$unit' and type = '20' and year = '$year5'";
		$result2 = Treat_DB_ProxyOld::query( $query2 );
		//mysql_close();
		$bgtcos=@Treat_DB_ProxyOld::mysql_result($result2,0,$month5);

		$comtotcosbgt=$comtotcosbgt+$bgtcos;
		$disttotcosbgt=$disttotcosbgt+$bgtcos;

		if ($goweek==1){$wkcosbgt=$bgtcos/$totaloperate*$operate1;$mtdcosbgt=$wkcosbgt;}
		elseif ($goweek==2){$wkcosbgt=$bgtcos/$totaloperate*$operate2;$mtdcosbgt=$bgtcos/$totaloperate*($operate1+$operate2);}
		elseif ($goweek==3){$wkcosbgt=$bgtcos/$totaloperate*$operate3;$mtdcosbgt=$bgtcos/$totaloperate*($operate1+$operate2+$operate3);}
		elseif ($goweek==4){$wkcosbgt=$bgtcos/$totaloperate*$operate4;$mtdcosbgt=$bgtcos/$totaloperate*($operate1+$operate2+$operate3+$operate4);}
		elseif ($goweek==5){$wkcosbgt=$bgtcos/$totaloperate*$operate5;$mtdcosbgt=$bgtcos/$totaloperate*($operate1+$operate2+$operate3+$operate4+$operate5);}
		elseif ($goweek==6){$wkcosbgt=$bgtcos/$totaloperate*$operate6;$mtdcosbgt=$bgtcos/$totaloperate*($operate1+$operate2+$operate3+$operate4+$operate5+$operate6);}

		$comcosbgt=$comcosbgt+$wkcosbgt;
		$commtdcosbgt=$commtdcosbgt+$mtdcosbgt;
		$distcosbgt=$distcosbgt+$wkcosbgt;
		$distmtdcosbgt=$distmtdcosbgt+$mtdcosbgt;

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( mysql_error());
		$query2 = "SELECT * FROM budget WHERE businessid = '$unit' and type = '30' and year = '$year5'";
		$result2 = Treat_DB_ProxyOld::query( $query2 );
		//mysql_close();
		$bgtlabor=@Treat_DB_ProxyOld::mysql_result($result2,0,$month5);

		$comtotlbrbgt=$comtotlbrbgt+$bgtlabor;
		$disttotlbrbgt=$disttotlbrbgt+$bgtlabor;

		if ($goweek==1){$wklaborbgt=$bgtlabor/$totaloperate*$operate1;$mtdlaborbgt=$wklaborbgt;}
		elseif ($goweek==2){$wklaborbgt=$bgtlabor/$totaloperate*$operate2;$mtdlaborbgt=$bgtlabor/$totaloperate*($operate1+$operate2);}
		elseif ($goweek==3){$wklaborbgt=$bgtlabor/$totaloperate*$operate3;$mtdlaborbgt=$bgtlabor/$totaloperate*($operate1+$operate2+$operate3);}
		elseif ($goweek==4){$wklaborbgt=$bgtlabor/$totaloperate*$operate4;$mtdlaborbgt=$bgtlabor/$totaloperate*($operate1+$operate2+$operate3+$operate4);}
		elseif ($goweek==5){$wklaborbgt=$bgtlabor/$totaloperate*$operate5;$mtdlaborbgt=$bgtlabor/$totaloperate*($operate1+$operate2+$operate3+$operate4+$operate5);}
		elseif ($goweek==6){$wklaborbgt=$bgtlabor/$totaloperate*$operate6;$mtdlaborbgt=$bgtlabor/$totaloperate*($operate1+$operate2+$operate3+$operate4+$operate5+$operate6);}

		$comlbrbgt=$comlbrbgt+$wklaborbgt;
		$commtdlbrbgt=$commtdlbrbgt+$mtdlaborbgt;
		$distlbrbgt=$distlbrbgt+$wklaborbgt;
		$distmtdlbrbgt=$distmtdlbrbgt+$mtdlaborbgt;

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( mysql_error());
		$query2 = "SELECT * FROM budget WHERE businessid = '$unit' and type = '40' and year = '$year5'";
		$result2 = Treat_DB_ProxyOld::query( $query2 );
		//mysql_close();
		$bgttoe=@Treat_DB_ProxyOld::mysql_result($result2,0,$month5);

		$comtottoebgt=$comtottoebgt+$bgttoe;
		$disttottoebgt=$disttottoebgt+$bgttoe;

		if ($goweek==1){$wktoebgt=$bgttoe/$totaloperate*$operate1;$mtdtoebgt=$wktoebgt;}
		elseif ($goweek==2){$wktoebgt=$bgttoe/$totaloperate*$operate2;$mtdtoebgt=$bgttoe/$totaloperate*($operate1+$operate2);}
		elseif ($goweek==3){$wktoebgt=$bgttoe/$totaloperate*$operate3;$mtdtoebgt=$bgttoe/$totaloperate*($operate1+$operate2+$operate3);}
		elseif ($goweek==4){$wktoebgt=$bgttoe/$totaloperate*$operate4;$mtdtoebgt=$bgttoe/$totaloperate*($operate1+$operate2+$operate3+$operate4);}
		elseif ($goweek==5){$wktoebgt=$bgttoe/$totaloperate*$operate5;$mtdtoebgt=$bgttoe/$totaloperate*($operate1+$operate2+$operate3+$operate4+$operate5);}
		elseif ($goweek==6){$wktoebgt=$bgttoe/$totaloperate*$operate6;$mtdtoebgt=$bgttoe/$totaloperate*($operate1+$operate2+$operate3+$operate4+$operate5+$operate6);}

		$comtoebgt=$comtoebgt+$wktoebgt;
		$commtdtoebgt=$commtdtoebgt+$mtdtoebgt;
		$disttoebgt=$disttoebgt+$wktoebgt;
		$distmtdtoebgt=$distmtdtoebgt+$mtdtoebgt;

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( mysql_error());
		$query2 = "SELECT * FROM budget WHERE businessid = '$unit' and type = '50' and year = '$year5'";
		$result2 = Treat_DB_ProxyOld::query( $query2 );
		//mysql_close();
		$bgtnetpft=@Treat_DB_ProxyOld::mysql_result($result2,0,$month5);

		$comtotplbgt=$comtotplbgt+$bgtnetpft;
		$disttotplbgt=$disttotplbgt+$bgtnetpft;

		if ($goweek==1){$wknetpftbgt=$bgtnetpft/$totaloperate*$operate1;$mtdnetpftbgt=$wknetpftbgt;}
		elseif ($goweek==2){$wknetpftbgt=$bgtnetpft/$totaloperate*$operate2;$mtdnetpftbgt=$bgtnetpft/$totaloperate*($operate1+$operate2);}
		elseif ($goweek==3){$wknetpftbgt=$bgtnetpft/$totaloperate*$operate3;$mtdnetpftbgt=$bgtnetpft/$totaloperate*($operate1+$operate2+$operate3);}
		elseif ($goweek==4){$wknetpftbgt=$bgtnetpft/$totaloperate*$operate4;$mtdnetpftbgt=$bgtnetpft/$totaloperate*($operate1+$operate2+$operate3+$operate4);}
		elseif ($goweek==5){$wknetpftbgt=$bgtnetpft/$totaloperate*$operate5;$mtdnetpftbgt=$bgtnetpft/$totaloperate*($operate1+$operate2+$operate3+$operate4+$operate5);}
		elseif ($goweek==6){$wknetpftbgt=$bgtnetpft/$totaloperate*$operate6;$mtdnetpftbgt=$bgtnetpft/$totaloperate*($operate1+$operate2+$operate3+$operate4+$operate5+$operate6);}

		$complbgt=$complbgt+$wknetpftbgt;
		$commtdplbgt=$commtdplbgt+$mtdnetpftbgt;
		$distplbgt=$distplbgt+$wknetpftbgt;
		$distmtdplbgt=$distmtdplbgt+$mtdnetpftbgt;

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( mysql_error());
		$query2 = "SELECT * FROM budget WHERE businessid = '$unit' and type = '60' and year = '$year5'";
		$result2 = Treat_DB_ProxyOld::query( $query2 );
		//mysql_close();
		$bgtamtdue=@Treat_DB_ProxyOld::mysql_result($result2,0,$month5);

		if ($goweek==1){$wkamtduebgt=$bgtamtdue/$totaloperate*$operate1;$mtdamtduebgt=$wkamtduebgt;}
		elseif ($goweek==2){$wkamtduebgt=$bgtamtdue/$totaloperate*$operate2;$mtdamtduebgt=$bgtamtdue/$totaloperate*($operate1+$operate2);}
		elseif ($goweek==3){$wkamtduebgt=$bgtamtdue/$totaloperate*$operate3;$mtdamtduebgt=$bgtamtdue/$totaloperate*($operate1+$operate2+$operate3);}
		elseif ($goweek==4){$wkamtduebgt=$bgtamtdue/$totaloperate*$operate4;$mtdamtduebgt=$bgtamtdue/$totaloperate*($operate1+$operate2+$operate3+$operate4);}
		elseif ($goweek==5){$wkamtduebgt=$bgtamtdue/$totaloperate*$operate5;$mtdamtduebgt=$bgtamtdue/$totaloperate*($operate1+$operate2+$operate3+$operate4+$operate5);}
		elseif ($goweek==6){$wkamtduebgt=$bgtamtdue/$totaloperate*$operate6;$mtdamtduebgt=$bgtamtdue/$totaloperate*($operate1+$operate2+$operate3+$operate4+$operate5+$operate6);}


		/////////////BUDGETS//////////////////////////////////////////////////////////////////////////////////////////
		$cosprcnt=round($bgtcos/$bgtotsales*100,1);
		$lbrprcnt=round($bgtlabor/$bgtotsales*100,1);
		$toeprcnt=round($bgttoe/$bgtotsales*100,1);
		$netpftprcnt=round($bgtnetpft/$bgtotsales*100,1);
		$bgtotsales = number_format($bgtotsales);
		$bgtcos = number_format($bgtcos);
		$bgtlabor = number_format($bgtlabor);
		$bgttoe = number_format($bgttoe);
		$bgtnetpft = number_format($bgtnetpft);

		echo "Figured Operating Days & Budget...<br>";

		///////////////////////END OPERATING DAYS//////////////////////////////////////////////////////////////////
		$weekday=$startweekday;

		$wk1=0;
		$wk2=0;
		$wk3=0;
		$wk4=0;
		$wk5=0;
		$wk6=0;
		$currentweek=1;

		$num--;
		while ($num>=0){

			$day = $startdate;

			$creditname = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'creditname' );
			$creditid = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'creditid' );
			$account = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'account' );
			$invoicesales = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'invoicesales' );
			$invoicesales_notax = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'invoicesales_notax' );
			$invoicetax = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'invoicetax' );
			$servchrg = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'servchrg' );
			$salescode = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'salescode' );
			$prepayment = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'heldcheck' );
			$showaccount=substr($account,0,3);

			$prevamount=0;
			$credittotal=0;
			$monthtotal=0;
			while ($day<=$enddate){

				$prevamount=0;

				///MANUAL
				if ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0&&$prepayment==0&&($salescode==0||$salescode=="")){
					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( mysql_error());
					$query5 = "SELECT amount FROM creditdetail WHERE creditid = '$creditid' AND date = '$day'";
					$result5 = Treat_DB_ProxyOld::query( $query5 );
					$num5 = Treat_DB_ProxyOld::mysql_numrows( $result5 );
					//mysql_close();

					if ($num5!=0){$prevamount = @Treat_DB_ProxyOld::mysql_result( $result5, 0, 'amount' );}
					$prevamount = money($prevamount);

				}
				///INVOICE
				elseif ($invoicesales==1&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0&&$prepayment==0&&($salescode=="0" || $salescode=="")){
					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( mysql_error());
					$query5 = "SELECT total,taxtotal,servamt FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND taxable = 'on' AND salescode = '0' AND (status = '4' OR status = '2' OR status = '6')";
					$result5 = Treat_DB_ProxyOld::query( $query5 );
					$num6 = Treat_DB_ProxyOld::mysql_numrows( $result5 );
					//mysql_close();
					$num6--;
					$prevamount=0;
					while ($num6>=0){
						$invtotal = @Treat_DB_ProxyOld::mysql_result( $result5, $num6, 'total' );
						$invtax = @Treat_DB_ProxyOld::mysql_result( $result5, $num6, 'taxtotal' );
						$servamt = @Treat_DB_ProxyOld::mysql_result( $result5, $num6, 'servamt' );
						$prevamount=$prevamount+($invtotal-$invtax-$servamt);
						$num6--;
					}
					$prevamount=money($prevamount);
				}
				///INVOICE TAX EXEMPT
				elseif ($invoicesales==0&&$invoicesales_notax==1&&$invoicetax==0&&$servchrg==0&&$prepayment==0&&($salescode=="0" || $salescode=="")){
					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( mysql_error());
					$query5 = "SELECT total,taxtotal,servamt FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND taxable = 'off' AND salescode = '0' AND (status = '4' OR status = '2' OR status = '6')";
					$result5 = Treat_DB_ProxyOld::query( $query5 );
					$num6 = Treat_DB_ProxyOld::mysql_numrows( $result5 );
					//mysql_close();
					$num6--;
					$prevamount=0;
					while ($num6>=0){
						$invtotal = @Treat_DB_ProxyOld::mysql_result( $result5, $num6, 'total' );
						$servamt = @Treat_DB_ProxyOld::mysql_result( $result5, $num6, 'servamt' );
						$prevamount=$prevamount+$invtotal-$servamt;
						$num6--;
					}
					$prevamount=money($prevamount);
				}
				///INVOICE TAX
				elseif ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==1&&$servchrg==0&&$prepayment==0&&($salescode=="0" || $salescode=="")){
					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( mysql_error());
					$query5 = "SELECT taxtotal FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND taxable = 'on' AND salescode = '0' AND (status = '4' OR status = '2' OR status = '6')";
					$result5 = Treat_DB_ProxyOld::query( $query5 );
					$num6 = Treat_DB_ProxyOld::mysql_numrows( $result5 );
					//mysql_close();
					$num6--;
					$prevamount=0;
					while ($num6>=0){
						$taxtotal = @Treat_DB_ProxyOld::mysql_result( $result5, $num6, 'taxtotal' );
						$prevamount=$prevamount+$taxtotal;
						$num6--;
					}
					$prevamount=money($prevamount);
				}
				///INVOICE SERVICE CHARGE
				elseif ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==1&&$prepayment==0&&($salescode=="0" || $salescode=="")){
					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( mysql_error());
					$query5 = "SELECT servamt FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND salescode = '0' AND (status = '4' OR status = '2' OR status = '6')";
					$result5 = Treat_DB_ProxyOld::query( $query5 );
					$num6 = Treat_DB_ProxyOld::mysql_numrows( $result5 );
					//mysql_close();
					$num6--;
					$prevamount=0;
					while ($num6>=0){
						$servamt = @Treat_DB_ProxyOld::mysql_result( $result5, $num6, 'servamt' );
						$prevamount=$prevamount+$servamt;
						$num6--;
					}
					$prevamount=money($prevamount);
				}
				///INVOICE (SALESCODE)////////////////////////////////////////////////////////////////////////////////////////////////////
				elseif ($invoicesales==1&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0&&$prepayment==0&&$salescode!="0" &&$salescode!=""){
					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( mysql_error());
					$query5 = "SELECT total,taxtotal,servamt FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND taxable = 'on' AND salescode = '$salescode' AND (status = '4' OR status = '2' OR status = '6')";
					$result5 = Treat_DB_ProxyOld::query( $query5 );
					$num6 = Treat_DB_ProxyOld::mysql_numrows( $result5 );
					//mysql_close();
					$num6--;
					$prevamount=0;
					while ($num6>=0){
						$invtotal = @Treat_DB_ProxyOld::mysql_result( $result5, $num6, 'total' );
						$invtax = @Treat_DB_ProxyOld::mysql_result( $result5, $num6, 'taxtotal' );
						$servamt = @Treat_DB_ProxyOld::mysql_result( $result5, $num6, 'servamt' );
						$prevamount=$prevamount+($invtotal-$invtax-$servamt);
						$num6--;
					}
					$prevamount=money($prevamount);
				}
				///INVOICE TAX EXEMPT (SALESCODE)
				elseif ($invoicesales==0&&$invoicesales_notax==1&&$invoicetax==0&&$servchrg==0 && $salescode!="0" && $prepayment==0){
					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( mysql_error());
					$query5 = "SELECT total,taxtotal,servamt FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND taxable = 'off' AND salescode = '$salescode' AND (status = '4' OR status = '2' OR status = '6')";
					$result5 = Treat_DB_ProxyOld::query( $query5 );
					$num6 = Treat_DB_ProxyOld::mysql_numrows( $result5 );
					//mysql_close();
					$num6--;
					$prevamount=0;
					while ($num6>=0){
						$invtotal = @Treat_DB_ProxyOld::mysql_result( $result5, $num6, 'total' );
						$servamt = @Treat_DB_ProxyOld::mysql_result( $result5, $num6, 'servamt' );
						$prevamount=$prevamount+$invtotal-$servamt;
						$num6--;
					}
					$prevamount=money($prevamount);
				}
				///INVOICE TAX (SALESCODE)
				elseif ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==1&&$servchrg==0&&$salescode!="0"&&$salescode!=""&&$prepayment==0){
					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( mysql_error());
					$query5 = "SELECT taxtotal FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND taxable = 'on' AND salescode = '$salescode' AND (status = '4' OR status = '2' OR status = '6')";
					$result5 = Treat_DB_ProxyOld::query( $query5 );
					$num6 = Treat_DB_ProxyOld::mysql_numrows( $result5 );
					//mysql_close();
					$num6--;
					$prevamount=0;
					while ($num6>=0){
						$taxtotal = @Treat_DB_ProxyOld::mysql_result( $result5, $num6, 'taxtotal' );
						$prevamount=$prevamount+$taxtotal;
						$num6--;
					}
					$prevamount=money($prevamount);
				}
				///INVOICE SERVICE CHARGE (SALESCODE)
				elseif ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==1&&$salescode!="0"&&$salescode!=""&&$prepayment==0){
					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( mysql_error());
					$query5 = "SELECT servamt FROM invoice WHERE type = '1' AND businessid = '$businessid' AND salescode = '$salescode' AND date = '$day' AND (status = '4' OR status = '2' OR status = '6')";
					$result5 = Treat_DB_ProxyOld::query( $query5 );
					$num6 = Treat_DB_ProxyOld::mysql_numrows( $result5 );
					//mysql_close();
					$num6--;
					$prevamount=0;
					while ($num6>=0){
						$servamt = @Treat_DB_ProxyOld::mysql_result( $result5, $num6, 'servamt' );
						$prevamount=$prevamount+$servamt;
						$num6--;
					}
					$prevamount=money($prevamount);
				}
				///PREPAYMENT
				elseif ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0&&$prepayment==1){
					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( mysql_error());
					$query5 = "SELECT amount FROM invtender WHERE businessid = '$businessid' AND date = '$day'";
					$result5 = Treat_DB_ProxyOld::query( $query5 );
					$num6 = Treat_DB_ProxyOld::mysql_numrows( $result5 );
					//mysql_close();
					$num6--;
					$prevamount=0;
					while ($num6>=0){
						$amount = @Treat_DB_ProxyOld::mysql_result( $result5, $num6, 'amount' );
						$prevamount=$prevamount+$amount;
						$num6--;  
					}
					$prevamount=money($prevamount);
					
				}

				$credittotal=$prevamount+$credittotal;

				if ($currentweek==1){$wk1=$wk1+$prevamount;}
				elseif ($currentweek==2){$wk2=$wk2+$prevamount;}
				elseif ($currentweek==3){$wk3=$wk3+$prevamount;}
				elseif ($currentweek==4){$wk4=$wk4+$prevamount;}
				elseif ($currentweek==5){$wk5=$wk5+$prevamount;}
				elseif ($currentweek==6){$wk6=$wk6+$prevamount;}

				if ($weekday=="Thursday" || $day == $enddate){
					if ($credittotal!=0){$credittotal=money($credittotal);}
					$monthtotal=$credittotal+$monthtotal;

					$credittotal=0;
					$currentweek++;
				}

				$day=nextday($day);

				if ($weekday=="Sunday"){$weekday="Monday";}
				elseif ($weekday=="Saturday"){$weekday="Sunday";}
				elseif ($weekday=="Friday"){$weekday="Saturday";}
				elseif ($weekday=="Thursday"){$weekday="Friday";}
				elseif ($weekday=="Wednesday"){$weekday="Thursday";}
				elseif ($weekday=="Tuesday"){$weekday="Wednesday";}
				elseif ($weekday=="Monday"){$weekday="Tuesday";}
			}
			$day=$startdate;
			$weekday=$startweekday;

			$monthtotal=money($monthtotal);

			$num--;
			$currentweek=1;
			echo "$num<br>";
		}

		$weektotal=$wk1+$wk2+$wk3+$wk4+$wk5+$wk6;
		$wk1=money($wk1);$saleswk1=$wk1;
		$wk2=money($wk2);$saleswk2=$wk2;
		$wk3=money($wk3);$saleswk3=$wk3;
		$wk4=money($wk4);$saleswk4=$wk4;
		$wk5=money($wk5);$saleswk5=$wk5;
		$wk6=money($wk6);$saleswk6=$wk6;
		$weektotal=money($weektotal);
		$salestotal=$weektotal;
		
		$wk2mtd=money($wk1+$wk2);
		$sales2mtd=$wk2mtd;
		$wk3mtd=money($wk2mtd+$wk3);
		$sales3mtd=$wk3mtd;
		$wk4mtd=money($wk3mtd+$wk4);
		$sales4mtd=$wk4mtd;
		$wk5mtd=money($wk4mtd+$wk5);
		$sales5mtd=$wk5mtd;
		$wk6mtd=money($wk5mtd+$wk6);
		$sales6mtd=$wk6mtd;

		$wksalesbgt=money($wksalesbgt);$wksalesbgt1 = number_format($wksalesbgt);
		$mtdsalesbgt=money($mtdsalesbgt);$mtdsalesbgt1 = number_format($mtdsalesbgt);
		
		$wkcosbgt=round($wkcosbgt/$wksalesbgt*100,1);
		$mtdcosbgt9=money($mtdcosbgt);
		$mtdcosbgt=round($mtdcosbgt/$mtdsalesbgt*100,1);
		
		$wklaborbgt=round($wklaborbgt/$wksalesbgt*100,1);
		$mtdlaborbgt9=money($mtdlaborbgt);
		$mtdlaborbgt=round($mtdlaborbgt/$mtdsalesbgt*100,1);
		
		$wktoebgt=round($wktoebgt/$wksalesbgt*100,1);
		$mtdtoebgt9=money($mtdtoebgt);
		$mtdtoebgt=round($mtdtoebgt/$mtdsalesbgt*100,1);
		
		$wknetpftbgt=money($wknetpftbgt);$wknetpftbgt = number_format($wknetpftbgt);
		$mtdnetpftbgt9=money($mtdnetpftbgt);
		$mtdnetpftbgt=money($mtdnetpftbgt);$mtdnetpftbgt = number_format($mtdnetpftbgt);

		if ($goweek==1){$with_commas = number_format($saleswk1);$message="$message <tr><td align=right>SALES:</td><td align=right>$$with_commas</td><td align=right></td><td align=right>$$wksalesbgt1</td><td align=right></td><td align=right>$$with_commas</td><td align=right></td><td align=right>$$mtdsalesbgt1</td><td align=right></td><td align=right>$$bgtotsales</td><td align=right></td></tr>";$comsales=$saleswk1+$comsales;$commtdsales=$commtdsales+$saleswk1;$distsales=$saleswk1+$distsales;$distmtdsales=$distmtdsales+$saleswk1;$variance=money($wk1-$mtdsalesbgt);}
		elseif ($goweek==2){$with_commas = number_format($saleswk2);$with_commas2 = number_format($sales2mtd);$message="$message<tr><td align=right>SALES:</td><td align=right>$$with_commas</td><td align=right></td><td align=right>$$wksalesbgt1</td><td align=right></td><td align=right>$$with_commas2</td><td align=right></td><td align=right>$$mtdsalesbgt1</td><td align=right></td><td align=right>$$bgtotsales</td><td align=right></td></tr>";$comsales=$saleswk2+$comsales;$commtdsales=$commtdsales+$sales2mtd;$distsales=$saleswk2+$distsales;$distmtdsales=$distmtdsales+$sales2mtd;$variance=money($sales2mtd-$mtdsalesbgt);}
		elseif ($goweek==3){$with_commas = number_format($saleswk3);$with_commas2 = number_format($sales3mtd);$message="$message<tr><td align=right>SALES:</td><td align=right>$$with_commas</td><td align=right></td><td align=right>$$wksalesbgt1</td><td align=right></td><td align=right>$$with_commas2</td><td align=right></td><td align=right>$$mtdsalesbgt1</td><td align=right></td><td align=right>$$bgtotsales</td><td align=right></td></tr>";$comsales=$saleswk3+$comsales;$commtdsales=$commtdsales+$sales3mtd;$distsales=$saleswk3+$distsales;$distmtdsales=$distmtdsales+$sales3mtd;$variance=money($sales3mtd-$mtdsalesbgt);}
		elseif ($goweek==4){$with_commas = number_format($saleswk4);$with_commas2 = number_format($sales4mtd);$message="$message<tr><td align=right>SALES:</td><td align=right>$$with_commas</td><td align=right></td><td align=right>$$wksalesbgt1</td><td align=right></td><td align=right>$$with_commas2</td><td align=right></td><td align=right>$$mtdsalesbgt1</td><td align=right></td><td align=right>$$bgtotsales</td><td align=right></td></tr>";$comsales=$saleswk4+$comsales;$commtdsales=$commtdsales+$sales4mtd;$distsales=$saleswk4+$distsales;$distmtdsales=$distmtdsales+$sales4mtd;$variance=money($sales4mtd-$mtdsalesbgt);}
		elseif ($goweek==5){$with_commas = number_format($saleswk5);$with_commas2 = number_format($sales5mtd);$message="$message<tr><td align=right>SALES:</td><td align=right>$$with_commas</td><td align=right></td><td align=right>$$wksalesbgt1</td><td align=right></td><td align=right>$$with_commas2</td><td align=right></td><td align=right>$$mtdsalesbgt1</td><td align=right></td><td align=right>$$bgtotsales</td><td align=right></td></tr>";$comsales=$saleswk5+$comsales;$commtdsales=$commtdsales+$sales5mtd;$distsales=$saleswk5+$distsales;$distmtdsales=$distmtdsales+$sales5mtd;$variance=money($sales5mtd-$mtdsalesbgt);}
		elseif ($goweek==6){$with_commas = number_format($saleswk6);$with_commas2 = number_format($sales6mtd);$message="$message<tr><td align=right>SALES:</td><td align=right>$$with_commas</td><td align=right></td><td align=right>$$wksalesbgt1</td><td align=right></td><td align=right>$$with_commas2</td><td align=right></td><td align=right>$$mtdsalesbgt1</td><td align=right></td><td align=right>$$bgtotsales</td><td align=right></td></tr>";$comsales=$saleswk6+$comsales;$commtdsales=$commtdsales+$sales6mtd;$distsales=$saleswk6+$distsales;$distmtdsales=$distmtdsales+$sales6mtd;$variance=money($sales6mtd-$mtdsalesbgt);}

		if($goweek==1){$with_commas2=$with_commas;}
		$newbudget="$newbudget<td>$with_commas2</td><td>$mtdsalesbgt1</td><td bgcolor=#FFFF99>$variance</td>";
		$variance=0;

		echo "Figured Sales... $with_commas<br>";
		//////END SALES/START COS//////////////////////////////////////////////////////////////////////////////////////////////////////////
		$totalinv=0;

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( mysql_error());
		$query = "SELECT * FROM acct_payable WHERE businessid = '$businessid' AND cos = '1' AND cs = '0' AND ((is_deleted = '0' AND created <= '$cs_enddate') OR (is_deleted = '1' AND del_date >= '$startdate')) ORDER BY accountnum DESC";
		$result = Treat_DB_ProxyOld::query( $query );
		$num = Treat_DB_ProxyOld::mysql_numrows( $result );
		//mysql_close();

		$wk1=0;
		$wk2=0;
		$wk3=0;
		$wk4=0;
		$wk5=0;
		$wk6=0;
		$currentweek=1;
		$num--;

		while ($num>=0){

			$acct_payableid = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'acct_payableid' );
			$accountnum = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'accountnum' );
			$acct_name = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'acct_name' );
			$cs_type = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'cs_type' );
			$showaccount=substr($accountnum,0,3);

			$day=$startdate;  

			$prevamount=0;
			$aptotal=0;
			$monthtotal=0;

			if ($cs_type==0){
				while ($day<=$enddate){
					$prevamount=0;

					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( mysql_error());
					$query5 = "SELECT * FROM apinvoice WHERE businessid = $businessid AND date = '$day' AND transfer < 3
							UNION 
							SELECT * FROM apinvoice WHERE vendor = $businessid AND date = '$day' AND transfer = 1";
					$result5 = Treat_DB_ProxyOld::query( $query5 );
					$num5 = Treat_DB_ProxyOld::mysql_numrows( $result5 );
					//mysql_close();

					$prevamount=0;
					$num5--;
					while ($num5>=0){

						$apinvoiceid = @Treat_DB_ProxyOld::mysql_result( $result5, $num5, 'apinvoiceid' );
						$transfer = @Treat_DB_ProxyOld::mysql_result( $result5, $num5, 'transfer' );
						$vendor = @Treat_DB_ProxyOld::mysql_result( $result5, $num5, 'vendor' );

						//mysql_connect($dbhost,$username,$password);
						//@mysql_select_db($database) or die( mysql_error());
						if($transfer==1&&$vendor==$businessid){
							$query6 = "SELECT amount FROM apinvoicedetail WHERE apinvoiceid = '$apinvoiceid' AND tfr_acct = '$acct_payableid'";
						}
						else{
							$query6 = "SELECT amount FROM apinvoicedetail WHERE apinvoiceid = '$apinvoiceid' AND apaccountid = '$acct_payableid'";
						}
						$result6 = Treat_DB_ProxyOld::query( $query6 );
						$num6 = Treat_DB_ProxyOld::mysql_numrows( $result6 );
						//mysql_close();

						$num6--;
						while ($num6>=0){
							$apamount = @Treat_DB_ProxyOld::mysql_result( $result6, $num6, 'amount' );
							if ($transfer==1&&$vendor==$businessid){$apamount=$apamount*-1;}
							$prevamount=$prevamount+$apamount;
							$num6--;
						}

						$num5--;
					}

					$aptotal=$prevamount+$aptotal;

					if ($weekday=="Thursday" || $day == $enddate){
						//mysql_connect($dbhost,$username,$password);
						//@mysql_select_db($database) or die( mysql_error());
						$query65 = "SELECT amount FROM inventor2 WHERE acct_payableid = '$acct_payableid' AND date < '$day'";
						$result65 = Treat_DB_ProxyOld::query( $query65 );
						$num65 = Treat_DB_ProxyOld::mysql_numrows( $result65 );
						//mysql_close();
						$beginamt = @Treat_DB_ProxyOld::mysql_result( $result65, $num65-1, 'amount' );

						//mysql_connect($dbhost,$username,$password);
						//@mysql_select_db($database) or die( mysql_error());
						$query65 = "SELECT amount FROM inventor2 WHERE acct_payableid = '$acct_payableid' AND date = '$day' ORDER by date DESC";
						$result65 = Treat_DB_ProxyOld::query( $query65 );
						$num65 = Treat_DB_ProxyOld::mysql_numrows( $result65 );
						//mysql_close();
						$endamt = @Treat_DB_ProxyOld::mysql_result( $result65, $num65-1, 'amount' );

						if ($beginamt!=0 && $endamt != 0){$aptotal=$beginamt+$aptotal-$endamt;}

						if ($currentweek==1){$wk1=$wk1+$aptotal;}
						elseif ($currentweek==2){$wk2=$wk2+$aptotal;}
						elseif ($currentweek==3){$wk3=$wk3+$aptotal;}
						elseif ($currentweek==4){$wk4=$wk4+$aptotal;}
						elseif ($currentweek==5){$wk5=$wk5+$aptotal;}
						elseif ($currentweek==6){$wk6=$wk6+$aptotal;}

						$prcnt=0;
						if ($currentweek==1&&$aptotal!=0&&$saleswk1!=0){$prcnt=($aptotal/$saleswk1)*100;$prcnt=round($prcnt,1);}
						elseif ($currentweek==2&&$aptotal!=0&&$saleswk2!=0){$prcnt=($aptotal/$saleswk2)*100;$prcnt=round($prcnt,1);}
						elseif ($currentweek==3&&$aptotal!=0&&$saleswk3!=0){$prcnt=($aptotal/$saleswk3)*100;$prcnt=round($prcnt,1);}
						elseif ($currentweek==4&&$aptotal!=0&&$saleswk4!=0){$prcnt=($aptotal/$saleswk4)*100;$prcnt=round($prcnt,1);}
						elseif ($currentweek==5&&$aptotal!=0&&$saleswk5!=0){$prcnt=($aptotal/$saleswk5)*100;$prcnt=round($prcnt,1);}
						elseif ($currentweek==6&&$aptotal!=0&&$saleswk6!=0){$prcnt=($aptotal/$saleswk6)*100;$prcnt=round($prcnt,1);}

						if ($aptotal!=0){$aptotal=money($aptotal);}
						$monthtotal=$aptotal+$monthtotal;
						$aptotal=0;
						$currentweek++;
					}

					$day=nextday($day);
					if ($weekday=="Sunday"){$weekday="Monday";}
					elseif ($weekday=="Saturday"){$weekday="Sunday";}
					elseif ($weekday=="Friday"){$weekday="Saturday";}
					elseif ($weekday=="Thursday"){$weekday="Friday";}
					elseif ($weekday=="Wednesday"){$weekday="Thursday";}
					elseif ($weekday=="Tuesday"){$weekday="Wednesday";}
					elseif ($weekday=="Monday"){$weekday="Tuesday";}
				}
			}
			elseif ($cs_type==9){
				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( mysql_error());
				$query7 = "SELECT * FROM controlsheet WHERE businessid = '$businessid' AND date = '$startdate' AND acct_payableid = '$acct_payableid'";
				$result7 = Treat_DB_ProxyOld::query( $query7 );
				$num7 = Treat_DB_ProxyOld::mysql_numrows( $result7 );
				//mysql_close();

				if ($num7!=0){
					$csid = @Treat_DB_ProxyOld::mysql_result( $result7, 0, 'csid' );
					$amount1 = money( @Treat_DB_ProxyOld::mysql_result( $result7, 0, 'amount1' ) );
					$amount2 = money( @Treat_DB_ProxyOld::mysql_result( $result7, 0, 'amount2' ) );
					$amount3 = money( @Treat_DB_ProxyOld::mysql_result( $result7, 0, 'amount3' ) );
					$amount4 = money( @Treat_DB_ProxyOld::mysql_result( $result7, 0, 'amount4' ) );
					$amount5 = money( @Treat_DB_ProxyOld::mysql_result( $result7, 0, 'amount5' ) );
					$amount6 = money( @Treat_DB_ProxyOld::mysql_result( $result7, 0, 'amount6' ) );
				}
				else{
					$amount1=0;
					$amount2=0;
					$amount3=0;
					$amount4=0;
					$amount5=0;
					$amount6=0;
				}

				if ($weekcount==5){$monthtotal=$amount1+$amount2+$amount3+$amount4+$amount5;}
				elseif ($weekcount==6){$monthtotal=$amount1+$amount2+$amount3+$amount4+$amount5+$amount6;}
				else{$monthtotal=$amount1+$amount2+$amount3+$amount4;}
				$wk1=$wk1+$amount1;
				$wk2=$wk2+$amount2;
				$wk3=$wk3+$amount3;
				$wk4=$wk4+$amount4;
				if ($weekcount==5){$wk5=$wk5+$amount5;}
				if ($weekcount==6){$wk5=$wk5+$amount5;$wk6=$wk6+$amount6;}

				$amount1prcnt=0;
				$amount2prcnt=0;
				$amount3prcnt=0;
				$amount4prcnt=0;
				$amount5prcnt=0;
				$amount6prcnt=0;
				if ($amount1!=0&&$saleswk1!=0){$amount1prcnt=round(($amount1/$saleswk1)*100,1);}
				if ($amount2!=0&&$saleswk2!=0){$amount2prcnt=round(($amount2/$saleswk2)*100,1);}
				if ($amount3!=0&&$saleswk3!=0){$amount3prcnt=round(($amount3/$saleswk3)*100,1);}
				if ($amount4!=0&&$saleswk4!=0){$amount4prcnt=round(($amount4/$saleswk4)*100,1);}
				if ($amount5!=0&&$saleswk5!=0){$amount5prcnt=round(($amount5/$saleswk5)*100,1);}
				if ($amount6!=0&&$saleswk6!=0){$amount6prcnt=round(($amount6/$saleswk6)*100,1);}

				if ($amount1==0){$amount1=0;}
				if ($amount2==0){$amount2=0;}
				if ($amount3==0){$amount3=0;}
				if ($amount4==0){$amount4=0;}
				if ($amount5==0){$amount5=0;}
				if ($amount6==0){$amount6=0;}

			}
			elseif($cs_type==10){
				$day=$startdate;  
				$month=substr($day,5,2);
				$recuryear=substr($day,0,4);
				$leap = date("L");
				if ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10' || $month == '12'){$monthcount=31;}
				elseif($month=='04' || $month=='06' || $month=='09' || $month=='11'){$monthcount=30;}
				elseif($month=='02' && $leap == 0){$monthcount=28;}
				elseif($month=='02' && $leap == 1){$monthcount=29;}

				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( mysql_error());
				$query12 = "SELECT `$month` FROM recur WHERE year = '$recuryear' AND apaccountid = '$acct_payableid'";
				$result12 = Treat_DB_ProxyOld::query( $query12 );
				//mysql_close();

				$recuramount = @Treat_DB_ProxyOld::mysql_result( $result12, 0, $month );
				$recuramount=$recuramount/$monthcount;

				$prevamount=0;
				$aptotal=0;
				$monthtotal=0;
				while ($day<=$enddate){

					$prevamount=$prevamount+$recuramount;

					if ($currentweek==1){$wk1=$wk1+$recuramount;}
					elseif ($currentweek==2){$wk2=$wk2+$recuramount;}
					elseif ($currentweek==3){$wk3=$wk3+$recuramount;}
					elseif ($currentweek==4){$wk4=$wk4+$recuramount;}
					elseif ($currentweek==5){$wk5=$wk5+$recuramount;}
					elseif ($currentweek==6){$wk6=$wk6+$recuramount;}

					$prcnt=0;
					if ($currentweek==1&&$prevamount!=0&&$saleswk1!=0){$prcnt=($prevamount/$saleswk1)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==2&&$prevamount!=0&&$saleswk2!=0){$prcnt=($prevamount/$saleswk2)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==3&&$prevamount!=0&&$saleswk3!=0){$prcnt=($prevamount/$saleswk3)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==4&&$prevamount!=0&&$saleswk4!=0){$prcnt=($prevamount/$saleswk4)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==5&&$prevamount!=0&&$saleswk5!=0){$prcnt=($prevamount/$saleswk5)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==6&&$prevamount!=0&&$saleswk6!=0){$prcnt=($prevamount/$saleswk6)*100;$prcnt=round($prcnt,1);}

					if ($weekday=="Thursday" || $day == $enddate){
						$showamount=$prevamount;
						if ($showamount!=0){$showamount=money($showamount);}
						//echo "<td  align=right><font size=2>$$showamount</td><td align=right><font size=1 color=blue>$prcnt%</td>";
						$monthtotal=$prevamount+$monthtotal;
						$currentweek++;
						$prevamount=0;
					}

					$day=nextday($day);
					if ($weekday=="Sunday"){$weekday="Monday";}
					elseif ($weekday=="Saturday"){$weekday="Sunday";}
					elseif ($weekday=="Friday"){$weekday="Saturday";}
					elseif ($weekday=="Thursday"){$weekday="Friday";}
					elseif ($weekday=="Wednesday"){$weekday="Thursday";}
					elseif ($weekday=="Tuesday"){$weekday="Wednesday";}
					elseif ($weekday=="Monday"){$weekday="Tuesday";}
				}

			}
			
			$day=$startdate;
			$weekday=$startweekday;
			////////////Inventory Adjustment Was Here///////////////////////////////////////
			

			$monthprcnt=0;
			$monthtotal=money($monthtotal);
			if ($monthtotal!=0&&$salestotal!=0){$monthprcnt=round(($monthtotal/$salestotal)*100,1);}
			
			$num--;
			$currentweek=1;
			echo "$num<br>";
		}

		$weektotal=$wk1+$wk2+$wk3+$wk4+$wk5+$wk6;
		$wk1=money($wk1);
		$wk2=money($wk2);
		$wk3=money($wk3);
		$wk4=money($wk4);
		$wk5=money($wk5);
		$wk6=money($wk6);
		$weektotal=money($weektotal);
		
		if ($wk1!=0&&$saleswk1!=0){$coswk1prcnt=round($wk1/$saleswk1*100,1);} else{$coswk1prcnt=0;}
		if ($wk2!=0&&$saleswk2!=0){$coswk2prcnt=round($wk2/$saleswk2*100,1);} else{$coswk2prcnt=0;}
		if ($wk3!=0&&$saleswk3!=0){$coswk3prcnt=round($wk3/$saleswk3*100,1);} else{$coswk3prcnt=0;}
		if ($wk4!=0&&$saleswk4!=0){$coswk4prcnt=round($wk4/$saleswk4*100,1);} else{$coswk4prcnt=0;}
		if ($wk5!=0&&$saleswk5!=0){$coswk5prcnt=round($wk5/$saleswk5*100,1);} else{$coswk5prcnt=0;}
		if ($wk6!=0&&$saleswk6!=0){$coswk6prcnt=round($wk6/$saleswk6*100,1);} else{$coswk6prcnt=0;}
		if ($salestotal!=0&&$weektotal!=0){$monthprcnt=round($weektotal/$salestotal*100,1);} else{$monthprcnt=0;}

		$overallpcnt=$overallpcnt+$monthprcnt;
		
		$apwk1=$wk1;
		$apwk2=$wk2;
		$apwk3=$wk3;
		$apwk4=$wk4;
		$apwk5=$wk5;
		$apwk6=$wk6;
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		if ($weektotal!=0&&$salestotal!=0){$wkprcnt=round($weektotal/$salestotal*100,1);}
		
		$wk2mtd=money($wk1+$wk2);
		$wk3mtd=money($wk2mtd+$wk3);
		$wk4mtd=money($wk3mtd+$wk4);
		$wk5mtd=money($wk4mtd+$wk5);
		$wk6mtd=money($wk5mtd+$wk6);
		$commtdcos=$commtdcos+$wk1;

		if ($sales2mtd!=0){$coswk2prcntmtd=round($wk2mtd/$sales2mtd*100,1);}else{$coswk2prcntmtd=0;}
		if ($sales3mtd!=0){$coswk3prcntmtd=round($wk3mtd/$sales3mtd*100,1);}else{$coswk3prcntmtd=0;}
		if ($sales4mtd!=0){$coswk4prcntmtd=round($wk4mtd/$sales4mtd*100,1);}else{$coswk4prcntmtd=0;}
		if ($sales5mtd!=0){$coswk5prcntmtd=round($wk5mtd/$sales5mtd*100,1);}else{$coswk5prcntmtd=0;}
		if ($sales6mtd!=0){$coswk6prcntmtd=round($wk6mtd/$sales6mtd*100,1);}else{$coswk6prcntmtd=0;}

		$showtab="";
		if ($goweek==1&&strlen($coswk1prcnt)<5){$showtab="\t";}
		elseif ($goweek==2&&strlen($coswk2prcnt)<5){$showtab="\t";}
		elseif ($goweek==3&&strlen($coswk3prcnt)<5){$showtab="\t";}
		elseif ($goweek==4&&strlen($coswk4prcnt)<5){$showtab="\t";}
		elseif ($goweek==5&&strlen($coswk5prcnt)<5){$showtab="\t";}
		elseif ($goweek==6&&strlen($coswk6prcnt)<5){$showtab="\t";}
		else{$showtab="";}

		if ($goweek==1){$message="$message<tr><td align=right>COS:</td><td align=right>$coswk1prcnt%</td><td align=right></td><td align=right>$wkcosbgt%</td><td align=right></td><td align=right>$coswk1prcnt%</td><td align=right></td><td align=right>$mtdcosbgt%</td><td align=right></td><td align=right>$$bgtcos</td><td align=right>$cosprcnt%</td></tr>";$comcos=$comcos+$wk1;$commtdcos=$commtdcos+$wk1;$distcos=$distcos+$wk1;$distmtdcos=$distmtdcos+$wk1;$variance=money($wk1mtd-$mtdcosbgt9);$newbudget="$newbudget<td>$wk1</td><td>$mtdcosbgt9</td><td bgcolor=#FFFF99>$variance</td>";}
		elseif ($goweek==2){$message="$message<tr><td align=right>COS:</td><td align=right>$coswk2prcnt%</td><td align=right></td><td align=right>$wkcosbgt%</td><td align=right></td><td align=right>$coswk2prcntmtd%</td><td align=right></td><td align=right>$mtdcosbgt%</td><td align=right></td><td align=right>$$bgtcos</td><td align=right>$cosprcnt%</td></tr>";$comcos=$comcos+$wk2;$commtdcos=$commtdcos+$wk2mtd;$distcos=$distcos+$wk2;$distmtdcos=$distmtdcos+$wk2mtd;$variance=money($wk2mtd-$mtdcosbgt9);$newbudget="$newbudget<td>$wk2mtd</td><td>$mtdcosbgt9</td><td bgcolor=#FFFF99>$variance</td>";}
		elseif ($goweek==3){$message="$message<tr><td align=right>COS:</td><td align=right>$coswk3prcnt%</td><td align=right></td><td align=right>$wkcosbgt%</td><td align=right></td><td align=right>$coswk3prcntmtd%</td><td align=right></td><td align=right>$mtdcosbgt%</td><td align=right></td><td align=right>$$bgtcos</td><td align=right>$cosprcnt%</td></tr>";$comcos=$comcos+$wk3;$commtdcos=$commtdcos+$wk3mtd;$distcos=$distcos+$wk3;$distmtdcos=$distmtdcos+$wk3mtd;$variance=money($wk3mtd-$mtdcosbgt9);$newbudget="$newbudget<td>$wk3mtd</td><td>$mtdcosbgt9</td><td bgcolor=#FFFF99>$variance</td>";}
		elseif ($goweek==4){$message="$message<tr><td align=right>COS:</td><td align=right>$coswk4prcnt%</td><td align=right></td><td align=right>$wkcosbgt%</td><td align=right></td><td align=right>$coswk4prcntmtd%</td><td align=right></td><td align=right>$mtdcosbgt%</td><td align=right></td><td align=right>$$bgtcos</td><td align=right>$cosprcnt%</td></tr>";$comcos=$comcos+$wk4;$commtdcos=$commtdcos+$wk4mtd;$distcos=$distcos+$wk4;$distmtdcos=$distmtdcos+$wk4mtd;$variance=money($wk4mtd-$mtdcosbgt9);$newbudget="$newbudget<td>$wk4mtd</td><td>$mtdcosbgt9</td><td bgcolor=#FFFF99>$variance</td>";}
		elseif ($goweek==5){$message="$message<tr><td align=right>COS:</td><td align=right>$coswk5prcnt%</td><td align=right></td><td align=right>$wkcosbgt%</td><td align=right></td><td align=right>$coswk5prcntmtd%</td><td align=right></td><td align=right>$mtdcosbgt%</td><td align=right></td><td align=right>$$bgtcos</td><td align=right>$cosprcnt%</td></tr>";$comcos=$comcos+$wk5;$commtdcos=$commtdcos+$wk5mtd;$distcos=$distcos+$wk5;$distmtdcos=$distmtdcos+$wk5mtd;$variance=money($wk5mtd-$mtdcosbgt9);$newbudget="$newbudget<td>$wk5mtd</td><td>$mtdcosbgt9</td><td bgcolor=#FFFF99>$variance</td>";}
		elseif ($goweek==6){$message="$message<tr><td align=right>COS:</td><td align=right>$coswk6prcnt%</td><td align=right></td><td align=right>$wkcosbgt%</td><td align=right></td><td align=right>$coswk6prcntmtd%</td><td align=right></td><td align=right>$mtdcosbgt%</td><td align=right></td><td align=right>$$bgtcos</td><td align=right>$cosprcnt%</td></tr>";$comcos=$comcos+$wk6;$commtdcos=$commtdcos+$wk6mtd;$distcos=$distcos+$wk6;$distmtdcos=$distmtdcos+$wk6mtd;$variance=money($wk6mtd-$mtdcosbgt9);$newbudget="$newbudget<td>$wk6mtd</td><td>$mtdcosbgt9</td><td bgcolor=#FFFF99>$variance</td>";}
		
		echo "Figured Cost of Sales...$wk5<br>";
		//////END COS/START LABOR//////////////////////////////////////////////////////////////////////////////////////////////////////////

		$benwk1=0;
		$benwk2=0;
		$benwk3=0;
		$benwk4=0;
		$benwk5=0;
		$benwk6=0;
		$wk1=0;
		$wk2=0;
		$wk3=0;
		$wk4=0;
		$wk5=0;
		$wk6=0;
		$currentweek=1;

		$day=$startdate;  

		$prevamount=0;
		$aptotal=0;
		$monthtotal=0;
		while ($day<=$enddate){

			$prevamount=0;

			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( mysql_error());
			$query5 = "SELECT * FROM stats WHERE businessid = '$businessid' AND date = '$day'";
			$result5 = Treat_DB_ProxyOld::query( $query5 );
			$num5 = Treat_DB_ProxyOld::mysql_numrows( $result5 );
			//mysql_close();

			$prevamount=0;
			$num5--;
			while ($num5>=0){

				$labor = @Treat_DB_ProxyOld::mysql_result( $result5, $num5, 'labor' );

				$prevamount=$prevamount+$labor;

				$num5--;
			}

			$aptotal=$prevamount+$aptotal;

			if ($currentweek==1){$wk1=$wk1+$prevamount;$benwk1=$benwk1+$prevamount;}
			elseif ($currentweek==2){$wk2=$wk2+$prevamount;$benwk2=$benwk2+$prevamount;}
			elseif ($currentweek==3){$wk3=$wk3+$prevamount;$benwk3=$benwk3+$prevamount;}
			elseif ($currentweek==4){$wk4=$wk4+$prevamount;$benwk4=$benwk4+$prevamount;}
			elseif ($currentweek==5){$wk5=$wk5+$prevamount;$benwk5=$benwk5+$prevamount;}
			elseif ($currentweek==6){$wk6=$wk6+$prevamount;$benwk6=$benwk6+$prevamount;}

			$prcnt=0;
			if ($currentweek==1&&$aptotal!=0&&$saleswk1!=0){$prcnt=($aptotal/$saleswk1)*100;$prcnt=round($prcnt,1);}
			elseif ($currentweek==2&&$aptotal!=0&&$saleswk2!=0){$prcnt=($aptotal/$saleswk2)*100;$prcnt=round($prcnt,1);}
			elseif ($currentweek==3&&$aptotal!=0&&$saleswk3!=0){$prcnt=($aptotal/$saleswk3)*100;$prcnt=round($prcnt,1);}
			elseif ($currentweek==4&&$aptotal!=0&&$saleswk4!=0){$prcnt=($aptotal/$saleswk4)*100;$prcnt=round($prcnt,1);}
			elseif ($currentweek==5&&$aptotal!=0&&$saleswk5!=0){$prcnt=($aptotal/$saleswk5)*100;$prcnt=round($prcnt,1);}
			elseif ($currentweek==6&&$aptotal!=0&&$saleswk6!=0){$prcnt=($aptotal/$saleswk6)*100;$prcnt=round($prcnt,1);}

			if ($weekday=="Thursday" || $day == $enddate){
				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( mysql_error());
				$query55 = "SELECT * FROM payroll_date WHERE date = '$day' AND companyid = '$companyid'";
				$result55 = Treat_DB_ProxyOld::query( $query55 );
				$num55 = Treat_DB_ProxyOld::mysql_numrows( $result55 );
				//mysql_close();

				if ($num55==1){
					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( mysql_error());
					$query56 = "SELECT reg2,ot2 FROM payroll WHERE date = '$day' AND unit_num = '$unit'";
					$result56 = Treat_DB_ProxyOld::query( $query56 );
					$num56 = Treat_DB_ProxyOld::mysql_numrows( $result56 );
					//mysql_close();

					$num56--;
					$totalgross=0;
					while($num56>=0){
						$reg2=0;$ot2=0;
						$reg2 = @Treat_DB_ProxyOld::mysql_result( $result56, $num56, 'reg2' );
						$ot2 = @Treat_DB_ProxyOld::mysql_result( $result56, $num56, 'ot2' );
						$totalgross=$totalgross+$reg2+$ot2;
						$num56--;
					}

					$labortemp=$day;
					for($counter=1;$counter<14;$counter++){$labortemp=prevday($labortemp);}

					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( mysql_error());
					$query56 = "SELECT labor FROM stats WHERE businessid = '$businessid' AND date >= '$labortemp' AND date <= '$day'";
					$result56 = Treat_DB_ProxyOld::query( $query56 );
					$num56 = Treat_DB_ProxyOld::mysql_numrows( $result56 );
					//mysql_close();

					$num56--;
					$totallabor=0;
					while($num56>=0){
						$labor = @Treat_DB_ProxyOld::mysql_result( $result56, $num56, 'labor' );
						$totallabor=$totallabor+$labor;
						$num56--;
					}

					$payroll_adj=money($totalgross-$totallabor);
					$monthtotal=$monthtotal+$payroll_adj;

					if ($currentweek==1){$wk1=$wk1+$payroll_adj;$benwk1=$benwk1+$payroll_adj;}
					elseif ($currentweek==2){$wk2=$wk2+$payroll_adj;$benwk2=$benwk2+$payroll_adj;}
					elseif ($currentweek==3){$wk3=$wk3+$payroll_adj;$benwk3=$benwk3+$payroll_adj;}
					elseif ($currentweek==4){$wk4=$wk4+$payroll_adj;$benwk4=$benwk4+$payroll_adj;}
					elseif ($currentweek==5){$wk5=$wk5+$payroll_adj;$benwk5=$benwk5+$payroll_adj;}
					elseif ($currentweek==6){$wk6=$wk6+$payroll_adj;$benwk6=$benwk6+$payroll_adj;}
				}
				else{$payroll_adj=0;}

				if ($aptotal!=0){$aptotal=money($aptotal);}
				$monthtotal=$aptotal+$monthtotal;
				$aptotal=0;
				$currentweek++;
			}

			$day=nextday($day);
			if ($weekday=="Sunday"){$weekday="Monday";}
			elseif ($weekday=="Saturday"){$weekday="Sunday";}
			elseif ($weekday=="Friday"){$weekday="Saturday";}
			elseif ($weekday=="Thursday"){$weekday="Friday";}
			elseif ($weekday=="Wednesday"){$weekday="Thursday";}
			elseif ($weekday=="Tuesday"){$weekday="Wednesday";}
			elseif ($weekday=="Monday"){$weekday="Tuesday";}

		}

		$day=$startdate;
		$weekday=$startweekday;

		$prcnt=0;
		$monthtotal=money($monthtotal);
		if ($salestotal!=0){$prcnt=round($monthtotal/$salestotal*100,1);}
		else{$prcnt=0;}

		$currentweek=1;

		echo "Labor Week: $wk1,$wk2,$wk3,$wk4,$wk5,$wk6<br>";

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( mysql_error());
		$query = "SELECT * FROM acct_payable WHERE businessid = '$businessid' AND cos = '0' AND cs = '0' AND ((cs_type > 3 AND cs_type < 6) OR cs_type = '8' OR cs_type = '11' OR cs_type = '13') AND ((is_deleted = '0' AND created <= '$cs_enddate') OR (is_deleted = '1' AND del_date >= '$startdate')) ORDER BY accountnum DESC";
		$result = Treat_DB_ProxyOld::query( $query );
		$num = Treat_DB_ProxyOld::mysql_numrows( $result );
		//mysql_close();

		$num--;
		while ($num>=0){

			$acct_payableid = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'acct_payableid' );
			$accountnum = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'accountnum' );
			$acct_name = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'acct_name' );
			$cs_type = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'cs_type' );
			$daily_fixed = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'daily_fixed' );
			$daily_labor = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'daily_labor' );
			$showaccount=substr($accountnum,0,5);

			
			if ($cs_type==5){$acct_name="<a style=$style target='_blank' href='edit_daily_amount.php?acct_payableid=$acct_payableid'><font size=2 color=gray>$acct_name</font></a>";}

			
			if ($cs_type==4){
				$day=$startdate;  

				$prevamount=0;
				$aptotal=0;
				$monthtotal=0;
				while ($day<=$enddate){

					$prevamount=0;

					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( mysql_error());
					$query5 = "SELECT * FROM apinvoice WHERE businessid = $businessid AND date = '$day' AND transfer < 3
							UNION
							SELECT * FROM apinvoice WHERE vendor = $businessid AND date = '$day' AND transfer = 1";
					$result5 = Treat_DB_ProxyOld::query( $query5 );
					$num5 = Treat_DB_ProxyOld::mysql_numrows( $result5 );
					//mysql_close();

					$prevamount=0;
					$num5--;
					while ($num5>=0){

						$apinvoiceid = @Treat_DB_ProxyOld::mysql_result( $result5, $num5, 'apinvoiceid' );
						$transfer = @Treat_DB_ProxyOld::mysql_result( $result5, $num5, 'transfer' );
						$vendor = @Treat_DB_ProxyOld::mysql_result( $result5, $num5, 'vendor' );

						//mysql_connect($dbhost,$username,$password);
						//@mysql_select_db($database) or die( mysql_error());
						if($transfer==1&&$vendor==$businessid){
							$query6 = "SELECT amount FROM apinvoicedetail WHERE apinvoiceid = '$apinvoiceid' AND tfr_acct = '$acct_payableid'";
						}
						else{
							$query6 = "SELECT amount FROM apinvoicedetail WHERE apinvoiceid = '$apinvoiceid' AND apaccountid = '$acct_payableid'";
						}
						$result6 = Treat_DB_ProxyOld::query( $query6 );
						$num6 = Treat_DB_ProxyOld::mysql_numrows( $result6 );
						//mysql_close();

						$num6--;
						while ($num6>=0){
							$apamount = @Treat_DB_ProxyOld::mysql_result( $result6, $num6, 'amount' );
							if ($transfer==1&&$vendor==$businessid){$apamount=$apamount*-1;}
							$prevamount=$prevamount+$apamount;
							$num6--;
						}

						$num5--;
					}

					$aptotal=$prevamount+$aptotal;

					if ($currentweek==1){$wk1=$wk1+$prevamount;if($showaccount!="50900"){$benwk1+=$prevamount;}}
					elseif ($currentweek==2){$wk2=$wk2+$prevamount;if($showaccount!="50900"){$benwk2+=$prevamount;}}
					elseif ($currentweek==3){$wk3=$wk3+$prevamount;if($showaccount!="50900"){$benwk3+=$prevamount;}}
					elseif ($currentweek==4){$wk4=$wk4+$prevamount;if($showaccount!="50900"){$benwk4+=$prevamount;}}
					elseif ($currentweek==5){$wk5=$wk5+$prevamount;if($showaccount!="50900"){$benwk5+=$prevamount;}}
					elseif ($currentweek==6){$wk6=$wk6+$prevamount;if($showaccount!="50900"){$benwk6+=$prevamount;}}

					$prcnt=0;
					if ($currentweek==1&&$aptotal!=0&&$saleswk1!=0){$prcnt=($aptotal/$saleswk1)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==2&&$aptotal!=0&&$saleswk2!=0){$prcnt=($aptotal/$saleswk2)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==3&&$aptotal!=0&&$saleswk3!=0){$prcnt=($aptotal/$saleswk3)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==4&&$aptotal!=0&&$saleswk4!=0){$prcnt=($aptotal/$saleswk4)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==5&&$aptotal!=0&&$saleswk5!=0){$prcnt=($aptotal/$saleswk5)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==6&&$aptotal!=0&&$saleswk6!=0){$prcnt=($aptotal/$saleswk6)*100;$prcnt=round($prcnt,1);}

					if ($weekday=="Thursday" || $day == $enddate){
						if ($aptotal!=0){$aptotal=money($aptotal);}
						$monthtotal=$aptotal+$monthtotal;
						$aptotal=0;
						$currentweek++;
					}

					$day=nextday($day);
					if ($weekday=="Sunday"){$weekday="Monday";}
					elseif ($weekday=="Saturday"){$weekday="Sunday";}
					elseif ($weekday=="Friday"){$weekday="Saturday";}
					elseif ($weekday=="Thursday"){$weekday="Friday";}
					elseif ($weekday=="Wednesday"){$weekday="Thursday";}
					elseif ($weekday=="Tuesday"){$weekday="Wednesday";}
					elseif ($weekday=="Monday"){$weekday="Tuesday";}
				}
			}
			elseif($cs_type==5){
				$day=$startdate;  

				$day=$startdate;  

				$prevamount=0;
				$aptotal=0;
				$monthtotal=0;
				while ($day<=$enddate){

					$prevamount=$prevamount+$daily_labor;

					if ($currentweek==1){$wk1=$wk1+$daily_labor;$benwk1=$benwk1+$daily_labor;}
					elseif ($currentweek==2){$wk2=$wk2+$daily_labor;$benwk2=$benwk2+$daily_labor;}
					elseif ($currentweek==3){$wk3=$wk3+$daily_labor;$benwk3=$benwk3+$daily_labor;}
					elseif ($currentweek==4){$wk4=$wk4+$daily_labor;$benwk4=$benwk4+$daily_labor;}
					elseif ($currentweek==5){$wk5=$wk5+$daily_labor;$benwk5=$benwk5+$daily_labor;}
					elseif ($currentweek==6){$wk6=$wk6+$daily_labor;$benwk6=$benwk6+$daily_labor;}

					$prcnt=0;
					if ($currentweek==1&&$prevamount!=0&&$saleswk1!=0){$prcnt=($prevamount/$saleswk1)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==2&&$prevamount!=0&&$saleswk2!=0){$prcnt=($prevamount/$saleswk2)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==3&&$prevamount!=0&&$saleswk3!=0){$prcnt=($prevamount/$saleswk3)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==4&&$prevamount!=0&&$saleswk4!=0){$prcnt=($prevamount/$saleswk4)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==5&&$prevamount!=0&&$saleswk5!=0){$prcnt=($prevamount/$saleswk5)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==6&&$prevamount!=0&&$saleswk6!=0){$prcnt=($prevamount/$saleswk6)*100;$prcnt=round($prcnt,1);}

					if ($weekday=="Thursday" || $day == $enddate){
						if ($prevamount!=0){$prevamount=money($prevamount);}
						$monthtotal=$prevamount+$monthtotal;
						$currentweek++;
						$prevamount=0;
					}

					$day=nextday($day);
					if ($weekday=="Sunday"){$weekday="Monday";}
					elseif ($weekday=="Saturday"){$weekday="Sunday";}
					elseif ($weekday=="Friday"){$weekday="Saturday";}
					elseif ($weekday=="Thursday"){$weekday="Friday";}
					elseif ($weekday=="Wednesday"){$weekday="Thursday";}
					elseif ($weekday=="Tuesday"){$weekday="Wednesday";}
					elseif ($weekday=="Monday"){$weekday="Tuesday";}
				}
			}
			elseif($cs_type==8){
				$day=$startdate;  
				$month=substr($day,5,2);
				$leap = date("L");
				if ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10' || $month == '12'){$monthcount=31;}
				elseif($month=='04' || $month=='06' || $month=='09' || $month=='11'){$monthcount=30;}
				elseif($month=='02' && $leap == 0){$monthcount=28;}
				elseif($month=='02' && $leap == 1){$monthcount=29;}

				$daily_fixed=$daily_fixed/$monthcount;

				$prevamount=0;
				$aptotal=0;
				$monthtotal=0;
				while ($day<=$enddate){

					$prevamount=$prevamount+$daily_fixed;

					if ($currentweek==1){$wk1=$wk1+$daily_fixed;$benwk1=$benwk1+$daily_fixed;}
					elseif ($currentweek==2){$wk2=$wk2+$daily_fixed;$benwk2=$benwk2+$daily_fixed;}
					elseif ($currentweek==3){$wk3=$wk3+$daily_fixed;$benwk3=$benwk3+$daily_fixed;}
					elseif ($currentweek==4){$wk4=$wk4+$daily_fixed;$benwk4=$benwk4+$daily_fixed;}
					elseif ($currentweek==5){$wk5=$wk5+$daily_fixed;$benwk5=$benwk5+$daily_fixed;}
					elseif ($currentweek==6){$wk6=$wk6+$daily_fixed;$benwk6=$benwk6+$daily_fixed;}

					$prcnt=0;
					if ($currentweek==1&&$prevamount!=0&&$saleswk1!=0){$prcnt=($prevamount/$saleswk1)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==2&&$prevamount!=0&&$saleswk2!=0){$prcnt=($prevamount/$saleswk2)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==3&&$prevamount!=0&&$saleswk3!=0){$prcnt=($prevamount/$saleswk3)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==4&&$prevamount!=0&&$saleswk4!=0){$prcnt=($prevamount/$saleswk4)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==5&&$prevamount!=0&&$saleswk5!=0){$prcnt=($prevamount/$saleswk5)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==6&&$prevamount!=0&&$saleswk6!=0){$prcnt=($prevamount/$saleswk6)*100;$prcnt=round($prcnt,1);}

					if ($weekday=="Thursday" || $day == $enddate){
						$showamount=$prevamount;
						if ($showamount!=0){$showamount=money($showamount);}
						$monthtotal=$prevamount+$monthtotal;
						$currentweek++;
						$prevamount=0;
					}

					$day=nextday($day);
					if ($weekday=="Sunday"){$weekday="Monday";}
					elseif ($weekday=="Saturday"){$weekday="Sunday";}
					elseif ($weekday=="Friday"){$weekday="Saturday";}
					elseif ($weekday=="Thursday"){$weekday="Friday";}
					elseif ($weekday=="Wednesday"){$weekday="Thursday";}
					elseif ($weekday=="Tuesday"){$weekday="Wednesday";}
					elseif ($weekday=="Monday"){$weekday="Tuesday";}
				}

			}
			elseif($cs_type==11){
				$day=$startdate;  
				$month=substr($day,5,2);
				$recuryear=substr($day,0,4);
				$leap = date("L");
				if ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10' || $month == '12'){$monthcount=31;}
				elseif($month=='04' || $month=='06' || $month=='09' || $month=='11'){$monthcount=30;}
				elseif($month=='02' && $leap == 0){$monthcount=28;}
				elseif($month=='02' && $leap == 1){$monthcount=29;}

				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( mysql_error());
				$query12 = "SELECT `$month` FROM recur WHERE year = '$recuryear' AND apaccountid = '$acct_payableid'";
				$result12 = Treat_DB_ProxyOld::query( $query12 );
				//mysql_close();

				$recuramount = @Treat_DB_ProxyOld::mysql_result( $result12, 0, $month );
				$recuramount=$recuramount/$monthcount;

				$prevamount=0;
				$aptotal=0;
				$monthtotal=0;
				while ($day<=$enddate){

					$prevamount=$prevamount+$recuramount;

					if ($currentweek==1){$wk1=$wk1+$recuramount;}
					elseif ($currentweek==2){$wk2=$wk2+$recuramount;}
					elseif ($currentweek==3){$wk3=$wk3+$recuramount;}
					elseif ($currentweek==4){$wk4=$wk4+$recuramount;}
					elseif ($currentweek==5){$wk5=$wk5+$recuramount;}
					elseif ($currentweek==6){$wk6=$wk6+$recuramount;}

					$prcnt=0;
					if ($currentweek==1&&$prevamount!=0&&$saleswk1!=0){$prcnt=($prevamount/$saleswk1)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==2&&$prevamount!=0&&$saleswk2!=0){$prcnt=($prevamount/$saleswk2)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==3&&$prevamount!=0&&$saleswk3!=0){$prcnt=($prevamount/$saleswk3)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==4&&$prevamount!=0&&$saleswk4!=0){$prcnt=($prevamount/$saleswk4)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==5&&$prevamount!=0&&$saleswk5!=0){$prcnt=($prevamount/$saleswk5)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==6&&$prevamount!=0&&$saleswk6!=0){$prcnt=($prevamount/$saleswk6)*100;$prcnt=round($prcnt,1);}

					if ($weekday=="Thursday" || $day == $enddate){
						$showamount=$prevamount;
						if ($showamount!=0){$showamount=money($showamount);}
						//echo "<td  align=right><font size=2>$$showamount</td><td align=right><font size=1 color=blue>$prcnt%</td>";
						$monthtotal=$prevamount+$monthtotal;
						$currentweek++;
						$prevamount=0;
					}

					$day=nextday($day);
					if ($weekday=="Sunday"){$weekday="Monday";}
					elseif ($weekday=="Saturday"){$weekday="Sunday";}
					elseif ($weekday=="Friday"){$weekday="Saturday";}
					elseif ($weekday=="Thursday"){$weekday="Friday";}
					elseif ($weekday=="Wednesday"){$weekday="Thursday";}
					elseif ($weekday=="Tuesday"){$weekday="Wednesday";}
					elseif ($weekday=="Monday"){$weekday="Tuesday";}
				}

			}
			elseif($cs_type==13){
				$day=$startdate;  
				$month=substr($day,5,2);
				$recuryear=substr($day,0,4);
				$leap = date("L");
				if ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10' || $month == '12'){$monthcount=31;}
				elseif($month=='04' || $month=='06' || $month=='09' || $month=='11'){$monthcount=30;}
				elseif($month=='02' && $leap == 0){$monthcount=28;}
				elseif($month=='02' && $leap == 1){$monthcount=29;}

				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( mysql_error());
				$query12 = "SELECT `$month` FROM recur WHERE year = '$recuryear' AND apaccountid = '$acct_payableid'";
				$result12 = Treat_DB_ProxyOld::query( $query12 );
				//mysql_close();

				$recuramount = @Treat_DB_ProxyOld::mysql_result( $result12, 0, $month );
				$recuramount=$recuramount/$monthcount;

				$prevamount=0;
				$aptotal=0;
				$monthtotal=0;
				while ($day<=$enddate){

					$prevamount=$prevamount+$recuramount;

					if ($currentweek==1){$wk1=$wk1+$recuramount;$benwk1=$benwk1+$recuramount;}
					elseif ($currentweek==2){$wk2=$wk2+$recuramount;$benwk2=$benwk2+$recuramount;}
					elseif ($currentweek==3){$wk3=$wk3+$recuramount;$benwk3=$benwk3+$recuramount;}
					elseif ($currentweek==4){$wk4=$wk4+$recuramount;$benwk4=$benwk4+$recuramount;}
					elseif ($currentweek==5){$wk5=$wk5+$recuramount;$benwk5=$benwk5+$recuramount;}
					elseif ($currentweek==6){$wk6=$wk6+$recuramount;$benwk6=$benwk6+$recuramount;}

					$prcnt=0;
					if ($currentweek==1&&$prevamount!=0&&$saleswk1!=0){$prcnt=($prevamount/$saleswk1)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==2&&$prevamount!=0&&$saleswk2!=0){$prcnt=($prevamount/$saleswk2)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==3&&$prevamount!=0&&$saleswk3!=0){$prcnt=($prevamount/$saleswk3)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==4&&$prevamount!=0&&$saleswk4!=0){$prcnt=($prevamount/$saleswk4)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==5&&$prevamount!=0&&$saleswk5!=0){$prcnt=($prevamount/$saleswk5)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==6&&$prevamount!=0&&$saleswk6!=0){$prcnt=($prevamount/$saleswk6)*100;$prcnt=round($prcnt,1);}

					if ($weekday=="Thursday" || $day == $enddate){
						$showamount=$prevamount;
						if ($showamount!=0){$showamount=money($showamount);}
						//echo "<td  align=right><font size=2>$$showamount</td><td align=right><font size=1 color=blue>$prcnt%</td>";
						$monthtotal=$prevamount+$monthtotal;
						$currentweek++;
						$prevamount=0;
					}

					$day=nextday($day);
					if ($weekday=="Sunday"){$weekday="Monday";}
					elseif ($weekday=="Saturday"){$weekday="Sunday";}
					elseif ($weekday=="Friday"){$weekday="Saturday";}
					elseif ($weekday=="Thursday"){$weekday="Friday";}
					elseif ($weekday=="Wednesday"){$weekday="Thursday";}
					elseif ($weekday=="Tuesday"){$weekday="Wednesday";}
					elseif ($weekday=="Monday"){$weekday="Tuesday";}
				}

			}
			$day = $startdate;
			$weekday = $startweekday;

			$monthprcnt = 0;
			$monthtotal = money( $monthtotal );
			if ($monthtotal!=0&&$salestotal!=0){$monthprcnt=round(($monthtotal/$salestotal)*100,1);}

			$num--;
			$currentweek=1;
			echo "Labor Week: $wk1,$wk2,$wk3,$wk4,$wk5,$wk6<br>";
		}
		/////////////////////TRANSFER LABOR/////////////////////////////////
		

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( mysql_error());
		$query7 = "SELECT * FROM controlsheet WHERE businessid = '$businessid' AND date = '$startdate' AND type = '1'";
		$result7 = Treat_DB_ProxyOld::query( $query7 );
		$num7 = Treat_DB_ProxyOld::mysql_numrows( $result7 );
		//mysql_close();

		if ($num7!=0){
			$csid = @Treat_DB_ProxyOld::mysql_result( $result7, 0, 'csid' );
			$amount1 = money( @Treat_DB_ProxyOld::mysql_result( $result7, 0, 'amount1' ) );
			$amount2 = money( @Treat_DB_ProxyOld::mysql_result( $result7, 0, 'amount2' ) );
			$amount3 = money( @Treat_DB_ProxyOld::mysql_result( $result7, 0, 'amount3' ) );
			$amount4 = money( @Treat_DB_ProxyOld::mysql_result( $result7, 0, 'amount4' ) );
			$amount5 = money( @Treat_DB_ProxyOld::mysql_result( $result7, 0, 'amount5' ) );
			$amount6 = money( @Treat_DB_ProxyOld::mysql_result( $result7, 0, 'amount6' ) );
		}
		else{
			$amount1=0;
			$amount2=0;
			$amount3=0;
			$amount4=0;
			$amount5=0;
			$amount6=0;
		}


		if ($weekcount==5){$monthtotal=money($amount1+$amount2+$amount3+$amount4+$amount5);}
		elseif ($weekcount==6){$monthtotal=money($amount1+$amount2+$amount3+$amount4+$amount5+$amount6);}
		else{$monthtotal=money($amount1+$amount2+$amount3+$amount4);}
		$wk1=$wk1+$amount1;
		$wk2=$wk2+$amount2;
		$wk3=$wk3+$amount3;
		$wk4=$wk4+$amount4;
		if ($weekcount==5){$wk5=$wk5+$amount5;}
		if ($weekcount==6){$wk5=$wk5+$amount5;$wk6=$wk6+$amount6;}

		$amount1prcnt=0;
		$amount2prcnt=0;
		$amount3prcnt=0;
		$amount4prcnt=0;
		$amount5prcnt=0;
		$amount6prcnt=0;
		if ($amount1!=0&&$saleswk1!=0){$amount1prcnt=round(($amount1/$saleswk1)*100,1);}
		if ($amount2!=0&&$saleswk2!=0){$amount2prcnt=round(($amount2/$saleswk2)*100,1);}
		if ($amount3!=0&&$saleswk3!=0){$amount3prcnt=round(($amount3/$saleswk3)*100,1);}
		if ($amount4!=0&&$saleswk4!=0){$amount4prcnt=round(($amount4/$saleswk4)*100,1);}
		if ($amount5!=0&&$saleswk5!=0){$amount5prcnt=round(($amount5/$saleswk5)*100,1);}
		if ($amount6!=0&&$saleswk6!=0){$amount6prcnt=round(($amount6/$saleswk6)*100,1);}
		if ($monthtotal!=0&&$salestotal!=0){$monthprcnt=round(($monthtotal/$salestotal)*100,1);}

		//////////////////BENEFITS//////////////////////////////////////
		$benwk1=money($benefitprcnt/100*$benwk1);$wk1=$wk1+$benwk1;
		$benwk2=money($benefitprcnt/100*$benwk2);$wk2=$wk2+$benwk2;
		$benwk3=money($benefitprcnt/100*$benwk3);$wk3=$wk3+$benwk3;
		$benwk4=money($benefitprcnt/100*$benwk4);$wk4=$wk4+$benwk4;
		$benwk5=money($benefitprcnt/100*$benwk5);$wk5=$wk5+$benwk5;
		$benwk6=money($benefitprcnt/100*$benwk6);$wk6=$wk6+$benwk6;
		if ($benwk1=="0.00"){$benwk1=0;}
		if ($benwk2=="0.00"){$benwk2=0;}
		if ($benwk3=="0.00"){$benwk3=0;}
		if ($benwk4=="0.00"){$benwk4=0;}
		if ($benwk5=="0.00"){$benwk5=0;}
		if ($benwk6=="0.00"){$benwk6=0;}
		$totalben=money($benwk1+$benwk2+$benwk3+$benwk4+$benwk5+$benwk6);

		echo "Labor w/ benefit: $wk1,$wk2,$wk3,$wk4,$wk5,$wk6<br>";
		//////////////////END BENEFITS/////////////////////////////////

		$apwk1=$apwk1+$wk1;
		$apwk2=$apwk2+$wk2;
		$apwk3=$apwk3+$wk3;
		$apwk4=$apwk4+$wk4;
		$apwk5=$apwk5+$wk5;
		$apwk6=$apwk6+$wk6;

		$weektotal=$wk1+$wk2+$wk3+$wk4+$wk5+$wk6;
		$wk1=money($wk1);
		$wk2=money($wk2);
		$wk3=money($wk3);
		$wk4=money($wk4);
		$wk5=money($wk5);
		$wk6=money($wk6);
		$weektotal=money($weektotal);
		
		if ($wk1!=0&&$saleswk1!=0){$lbrwk1prcnt=round($wk1/$saleswk1*100,1);} else{$lbrwk1prcnt=0;}
		if ($wk2!=0&&$saleswk2!=0){$lbrwk2prcnt=round($wk2/$saleswk2*100,1);} else{$lbrwk2prcnt=0;}
		if ($wk3!=0&&$saleswk3!=0){$lbrwk3prcnt=round($wk3/$saleswk3*100,1);} else{$lbrwk3prcnt=0;}
		if ($wk4!=0&&$saleswk4!=0){$lbrwk4prcnt=round($wk4/$saleswk4*100,1);} else{$lbrwk4prcnt=0;}
		if ($wk5!=0&&$saleswk5!=0){$lbrwk5prcnt=round($wk5/$saleswk5*100,1);} else{$lbrwk5prcnt=0;}
		if ($wk6!=0&&$saleswk6!=0){$lbrwk6prcnt=round($wk6/$saleswk6*100,1);} else{$lbrwk6prcnt=0;}
		if ($salestotal!=0){$monthprcnt=round($weektotal/$salestotal*100,1);} else{$monthprcnt=0;}

		$overallpcnt=$overallpcnt+$monthprcnt;

		$wk2mtd=money($wk1+$wk2);
		$wk3mtd=money($wk2mtd+$wk3);
		$wk4mtd=money($wk3mtd+$wk4);
		$wk5mtd=money($wk4mtd+$wk5);
		$wk6mtd=money($wk5mtd+$wk6);

		if ($sales2mtd!=0){$lbrwk2prcntmtd=round($wk2mtd/$sales2mtd*100,1);}else{$lbrwk2prcntmtd=0;}	
		if ($sales3mtd!=0){$lbrwk3prcntmtd=round($wk3mtd/$sales3mtd*100,1);}else{$lbrwk3prcntmtd=0;}
		if ($sales4mtd!=0){$lbrwk4prcntmtd=round($wk4mtd/$sales4mtd*100,1);}else{$lbrwk4prcntmtd=0;}
		if ($sales5mtd!=0){$lbrwk5prcntmtd=round($wk5mtd/$sales5mtd*100,1);}else{$lbrwk5prcntmtd=0;}
		if ($sales6mtd!=0){$lbrwk6prcntmtd=round($wk6mtd/$sales6mtd*100,1);}else{$lbrwk6prcntmtd=0;}

		$showtab="";
		if ($goweek==1&&strlen($lbrwk1prcnt)<5){$showtab="\t";}
		elseif ($goweek==2&&strlen($lbrwk2prcnt)<5){$showtab="\t";}
		elseif ($goweek==3&&strlen($lbrwk3prcnt)<5){$showtab="\t";}
		elseif ($goweek==4&&strlen($lbrwk4prcnt)<5){$showtab="\t";}
		elseif ($goweek==5&&strlen($lbrwk5prcnt)<5){$showtab="\t";}
		elseif ($goweek==6&&strlen($lbrwk6prcnt)<5){$showtab="\t";}
		else{$showtab="";}

		if ($goweek==1){$message="$message<tr><td align=right>LABOR:</td><td align=right>$lbrwk1prcnt%</td><td align=right></td><td align=right>$wklaborbgt%</td><td align=right></td><td align=right>$lbrwk1prcnt%</td><td align=right></td><td align=right>$mtdlaborbgt%</td><td align=right></td><td align=right>$$bgtlabor</td><td align=right>$lbrprcnt%</td></tr>";$comlbr=$comlbr+$wk1;$commtdlbr=$commtdlbr+$wk1;$distlbr=$distlbr+$wk1;$distmtdlbr=$distmtdlbr+$wk1;$variance=money($wk1mtd-$mtdlaborbgt9);$newbudget="$newbudget<td>$wk1</td><td>$mtdlaborbgt9</td><td bgcolor=#FFFF99>$variance</td>";}
		elseif ($goweek==2){$message="$message<tr><td align=right>LABOR:</td><td align=right>$lbrwk2prcnt%</td><td align=right></td><td align=right>$wklaborbgt%</td><td align=right></td><td align=right>$lbrwk2prcntmtd%</td><td align=right></td><td align=right>$mtdlaborbgt%</td><td align=right></td><td align=right>$$bgtlabor</td><td align=right>$lbrprcnt%</td></tr>";$comlbr=$comlbr+$wk2;$commtdlbr=$commtdlbr+$wk2mtd;$distlbr=$distlbr+$wk2;$distmtdlbr=$distmtdlbr+$wk2mtd;$variance=money($wk2mtd-$mtdlaborbgt9);$newbudget="$newbudget<td>$wk2mtd</td><td>$mtdlaborbgt9</td><td bgcolor=#FFFF99>$variance</td>";}
		elseif ($goweek==3){$message="$message<tr><td align=right>LABOR:</td><td align=right>$lbrwk3prcnt%</td><td align=right></td><td align=right>$wklaborbgt%</td><td align=right></td><td align=right>$lbrwk3prcntmtd%</td><td align=right></td><td align=right>$mtdlaborbgt%</td><td align=right></td><td align=right>$$bgtlabor</td><td align=right>$lbrprcnt%</td></tr>";$comlbr=$comlbr+$wk3;$commtdlbr=$commtdlbr+$wk3mtd;$distlbr=$distlbr+$wk2;$distmtdlbr=$distmtdlbr+$wk3mtd;$variance=money($wk3mtd-$mtdlaborbgt9);$newbudget="$newbudget<td>$wk3mtd</td><td>$mtdlaborbgt9</td><td bgcolor=#FFFF99>$variance</td>";}
		elseif ($goweek==4){$message="$message<tr><td align=right>LABOR:</td><td align=right>$lbrwk4prcnt%</td><td align=right></td><td align=right>$wklaborbgt%</td><td align=right></td><td align=right>$lbrwk4prcntmtd%</td><td align=right></td><td align=right>$mtdlaborbgt%</td><td align=right></td><td align=right>$$bgtlabor</td><td align=right>$lbrprcnt%</td></tr>";$comlbr=$comlbr+$wk4;$commtdlbr=$commtdlbr+$wk4mtd;$distlbr=$distlbr+$wk2;$distmtdlbr=$distmtdlbr+$wk4mtd;$variance=money($wk4mtd-$mtdlaborbgt9);$newbudget="$newbudget<td>$wk4mtd</td><td>$mtdlaborbgt9</td><td bgcolor=#FFFF99>$variance</td>";}
		elseif ($goweek==5){$message="$message<tr><td align=right>LABOR:</td><td align=right>$lbrwk5prcnt%</td><td align=right></td><td align=right>$wklaborbgt%</td><td align=right></td><td align=right>$lbrwk5prcntmtd%</td><td align=right></td><td align=right>$mtdlaborbgt%</td><td align=right></td><td align=right>$$bgtlabor</td><td align=right>$lbrprcnt%</td></tr>";$comlbr=$comlbr+$wk5;$commtdlbr=$commtdlbr+$wk5mtd;$distlbr=$distlbr+$wk2;$distmtdlbr=$distmtdlbr+$wk5mtd;$variance=money($wk5mtd-$mtdlaborbgt9);$newbudget="$newbudget<td>$wk5mtd</td><td>$mtdlaborbgt9</td><td bgcolor=#FFFF99>$variance</td>";}
		elseif ($goweek==6){$message="$message<tr><td align=right>LABOR:</td><td align=right>$lbrwk6prcnt%</td><td align=right></td><td align=right>$wklaborbgt%</td><td align=right></td><td align=right>$lbrwk6prcntmtd%</td><td align=right></td><td align=right>$mtdlaborbgt%</td><td align=right></td><td align=right>$$bgtlabor</td><td align=right>$lbrprcnt%</td></tr>";$comlbr=$comlbr+$wk6;$commtdlbr=$commtdlbr+$wk6mtd;$distlbr=$distlbr+$wk2;$distmtdlbr=$distmtdlbr+$wk6mtd;$variance=money($wk6mtd-$mtdlaborbgt9);$newbudget="$newbudget<td>$wk6mtd</td><td>$mtdlaborbgt9</td><td bgcolor=#FFFF99>$variance</td>";}

		echo "Figured Labor...$wk2<br>";
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		///START TOE///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( mysql_error());
		$query = "SELECT * FROM acct_payable WHERE businessid = '$businessid' AND cos = '0' AND cs = '0' AND (cs_type < 4 OR cs_type = 6 OR cs_type = 7 OR cs_type = 12) AND ((is_deleted = '0' AND created <= '$cs_enddate') OR (is_deleted = '1' AND del_date >= '$startdate')) ORDER BY accountnum DESC";
		$result = Treat_DB_ProxyOld::query( $query );
		$num = Treat_DB_ProxyOld::mysql_numrows( $result );
		//mysql_close();

		$wk1=0;
		$wk2=0;
		$wk3=0;
		$wk4=0;
		$wk5=0;
		$wk6=0;
		$currentweek=1;
		$num--;
		while ($num>=0){

			$acct_payableid = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'acct_payableid' );
			$accountnum = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'accountnum' );
			$acct_name = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'acct_name' );
			$cs_type = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'cs_type' );
			$daily_fixed = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'daily_fixed' );
			$cs_glacct = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'cs_glacct' );
			$cs_prcnt = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'cs_prcnt' );
			$showaccount=substr($accountnum,0,3);

			if ($cs_type==0){
				$day=$startdate;  

				$prevamount=0;
				$aptotal=0;
				$monthtotal=0;
				while ($day<=$enddate){

					$prevamount=0;

					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( mysql_error());
					$query5 = "SELECT * FROM apinvoice WHERE businessid = $businessid AND date = '$day' AND transfer != 3
							UNION
							SELECT * FROM apinvoice WHERE vendor = $businessid AND date = '$day' AND transfer = 1";
					$result5 = Treat_DB_ProxyOld::query( $query5 );
					$num5 = Treat_DB_ProxyOld::mysql_numrows( $result5 );
					//mysql_close();

					$prevamount=0;
					$num5--;
					while ($num5>=0){

						$apinvoiceid = @Treat_DB_ProxyOld::mysql_result( $result5, $num5, 'apinvoiceid' );
						$transfer = @Treat_DB_ProxyOld::mysql_result( $result5, $num5, 'transfer' );
						$vendor = @Treat_DB_ProxyOld::mysql_result( $result5, $num5, 'vendor' );

						//mysql_connect($dbhost,$username,$password);
						//@mysql_select_db($database) or die( mysql_error());
						if($transfer==1&&$vendor==$businessid){
							$query6 = "SELECT amount FROM apinvoicedetail WHERE apinvoiceid = '$apinvoiceid' AND tfr_acct = '$acct_payableid'";
						}
						else{
							$query6 = "SELECT amount FROM apinvoicedetail WHERE apinvoiceid = '$apinvoiceid' AND apaccountid = '$acct_payableid'";
						}
						$result6 = Treat_DB_ProxyOld::query( $query6 );
						$num6 = Treat_DB_ProxyOld::mysql_numrows( $result6 );
						//mysql_close();

						$num6--;
						while ($num6>=0){
							$apamount = @Treat_DB_ProxyOld::mysql_result( $result6, $num6, 'amount' );
							if ($transfer==1&&$vendor==$businessid){$apamount=$apamount*-1;}
							$prevamount=$prevamount+$apamount;
							$num6--;
						}

						$num5--;
					}

					$aptotal=$prevamount+$aptotal;

					if ($currentweek==1){$wk1=$wk1+$prevamount;}
					elseif ($currentweek==2){$wk2=$wk2+$prevamount;}
					elseif ($currentweek==3){$wk3=$wk3+$prevamount;}
					elseif ($currentweek==4){$wk4=$wk4+$prevamount;}
					elseif ($currentweek==5){$wk5=$wk5+$prevamount;}
					elseif ($currentweek==6){$wk6=$wk6+$prevamount;}

					$prcnt=0;
					if ($currentweek==1&&$aptotal!=0&&$saleswk1!=0){$prcnt=($aptotal/$saleswk1)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==2&&$aptotal!=0&&$saleswk2!=0){$prcnt=($aptotal/$saleswk2)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==3&&$aptotal!=0&&$saleswk3!=0){$prcnt=($aptotal/$saleswk3)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==4&&$aptotal!=0&&$saleswk4!=0){$prcnt=($aptotal/$saleswk4)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==5&&$aptotal!=0&&$saleswk5!=0){$prcnt=($aptotal/$saleswk5)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==6&&$aptotal!=0&&$saleswk6!=0){$prcnt=($aptotal/$saleswk6)*100;$prcnt=round($prcnt,1);}

					if ($weekday=="Thursday" || $day == $enddate){
						if ($aptotal!=0){$aptotal=money($aptotal);}
						$monthtotal=$aptotal+$monthtotal;
						$aptotal=0;
						$currentweek++;
					}

					$day=nextday($day);
					if ($weekday=="Sunday"){$weekday="Monday";}
					elseif ($weekday=="Saturday"){$weekday="Sunday";}
					elseif ($weekday=="Friday"){$weekday="Saturday";}
					elseif ($weekday=="Thursday"){$weekday="Friday";}
					elseif ($weekday=="Wednesday"){$weekday="Thursday";}
					elseif ($weekday=="Tuesday"){$weekday="Wednesday";}
					elseif ($weekday=="Monday"){$weekday="Tuesday";}
				}
			}
			elseif($cs_type==1){
				$day=$startdate;  

				$prevamount=0;
				$aptotal=0;
				$monthtotal=0;

				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( mysql_error());
				$query5 = "SELECT * FROM debits WHERE businessid = '$businessid' AND debittype = '1'";
				$result5 = Treat_DB_ProxyOld::query( $query5 );
				$num5 = Treat_DB_ProxyOld::mysql_numrows( $result5 );
				//mysql_close();
				$debitnum=$num5;

				while ($day<=$enddate){
					$num5=$debitnum;
					$num5--;
					while ($num5>=0){
						$debitid = @Treat_DB_ProxyOld::mysql_result( $result5, $num5, 'debitid' );

						//mysql_connect($dbhost,$username,$password);
						//@mysql_select_db($database) or die( mysql_error());
						$query6 = "SELECT amount FROM debitdetail WHERE debitid = '$debitid' AND date = '$day'";
						$result6 = Treat_DB_ProxyOld::query( $query6 );
						$num6 = Treat_DB_ProxyOld::mysql_numrows( $result6 );
						//mysql_close();

						$num6--;
						while ($num6>=0){
							$amount = @Treat_DB_ProxyOld::mysql_result( $result6, $num6, 'amount' );
							$prevamount=$prevamount+$amount;
							$num6--;
						}
						$num5--;
					}

					if ($weekday=="Thursday" || $day == $enddate){
						if ($prevamount!=0){$prevamount=money($prevamount*.0375);}
						$prcnt=0;
						if ($currentweek==1&&$prevamount!=0&&$saleswk1!=0){$prcnt=($prevamount/$saleswk1)*100;$prcnt=round($prcnt,1);}
						elseif ($currentweek==2&&$prevamount!=0&&$saleswk2!=0){$prcnt=($prevamount/$saleswk2)*100;$prcnt=round($prcnt,1);}
						elseif ($currentweek==3&&$prevamount!=0&&$saleswk3!=0){$prcnt=($prevamount/$saleswk3)*100;$prcnt=round($prcnt,1);}
						elseif ($currentweek==4&&$prevamount!=0&&$saleswk4!=0){$prcnt=($prevamount/$saleswk4)*100;$prcnt=round($prcnt,1);}
						elseif ($currentweek==5&&$prevamount!=0&&$saleswk5!=0){$prcnt=($prevamount/$saleswk5)*100;$prcnt=round($prcnt,1);}
						elseif ($currentweek==6&&$prevamount!=0&&$saleswk6!=0){$prcnt=($prevamount/$saleswk6)*100;$prcnt=round($prcnt,1);}

						if ($currentweek==1){$wk1=$wk1+$prevamount;}
						elseif ($currentweek==2){$wk2=$wk2+$prevamount;}
						elseif ($currentweek==3){$wk3=$wk3+$prevamount;}
						elseif ($currentweek==4){$wk4=$wk4+$prevamount;}
						elseif ($currentweek==5){$wk5=$wk5+$prevamount;}
						elseif ($currentweek==6){$wk6=$wk6+$prevamount;}
						$monthtotal=$prevamount+$monthtotal;
						$currentweek++;
						$prevamount=0;
					}

					$day=nextday($day);
					if ($weekday=="Sunday"){$weekday="Monday";}
					elseif ($weekday=="Saturday"){$weekday="Sunday";}
					elseif ($weekday=="Friday"){$weekday="Saturday";}
					elseif ($weekday=="Thursday"){$weekday="Friday";}
					elseif ($weekday=="Wednesday"){$weekday="Thursday";}
					elseif ($weekday=="Tuesday"){$weekday="Wednesday";}
					elseif ($weekday=="Monday"){$weekday="Tuesday";}
				}
			}
			elseif($cs_type==2){
				$day=$startdate;  

				$prevamount=0;
				$aptotal=0;
				$monthtotal=0;
				while ($day<=$enddate){

					$prevamount=$prevamount+$daily_fixed;

					if ($currentweek==1){$wk1=$wk1+$daily_fixed;}
					elseif ($currentweek==2){$wk2=$wk2+$daily_fixed;}
					elseif ($currentweek==3){$wk3=$wk3+$daily_fixed;}
					elseif ($currentweek==4){$wk4=$wk4+$daily_fixed;}
					elseif ($currentweek==5){$wk5=$wk5+$daily_fixed;}
					elseif ($currentweek==6){$wk6=$wk6+$daily_fixed;}

					$prcnt=0;
					if ($currentweek==1&&$prevamount!=0&&$saleswk1!=0){$prcnt=($prevamount/$saleswk1)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==2&&$prevamount!=0&&$saleswk2!=0){$prcnt=($prevamount/$saleswk2)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==3&&$prevamount!=0&&$saleswk3!=0){$prcnt=($prevamount/$saleswk3)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==4&&$prevamount!=0&&$saleswk4!=0){$prcnt=($prevamount/$saleswk4)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==5&&$prevamount!=0&&$saleswk5!=0){$prcnt=($prevamount/$saleswk5)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==6&&$prevamount!=0&&$saleswk6!=0){$prcnt=($prevamount/$saleswk6)*100;$prcnt=round($prcnt,1);}

					if ($weekday=="Thursday" || $day == $enddate){
						if ($prevamount!=0){$prevamount=money($prevamount);}
						$monthtotal=$prevamount+$monthtotal;
						$currentweek++;
						$prevamount=0;
					}

					$day=nextday($day);
					if ($weekday=="Sunday"){$weekday="Monday";}
					elseif ($weekday=="Saturday"){$weekday="Sunday";}
					elseif ($weekday=="Friday"){$weekday="Saturday";}
					elseif ($weekday=="Thursday"){$weekday="Friday";}
					elseif ($weekday=="Wednesday"){$weekday="Thursday";}
					elseif ($weekday=="Tuesday"){$weekday="Wednesday";}
					elseif ($weekday=="Monday"){$weekday="Tuesday";}
				}

			}
			elseif($cs_type==3){
				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( mysql_error());
				$query7 = "SELECT * FROM controlsheet WHERE businessid = '$businessid' AND date = '$startdate' AND acct_payableid = '$acct_payableid'";
				$result7 = Treat_DB_ProxyOld::query( $query7 );
				$num7 = Treat_DB_ProxyOld::mysql_numrows( $result7 );
				//mysql_close();

				if ($num7!=0){
					$csid = @Treat_DB_ProxyOld::mysql_result( $result7, 0, 'csid' );
					$amount1 = money( @Treat_DB_ProxyOld::mysql_result( $result7, 0, 'amount1' ) );
					$amount2 = money( @Treat_DB_ProxyOld::mysql_result( $result7, 0, 'amount2' ) );
					$amount3 = money( @Treat_DB_ProxyOld::mysql_result( $result7, 0, 'amount3' ) );
					$amount4 = money( @Treat_DB_ProxyOld::mysql_result( $result7, 0, 'amount4' ) );
					$amount5 = money( @Treat_DB_ProxyOld::mysql_result( $result7, 0, 'amount5' ) );
					$amount6 = money( @Treat_DB_ProxyOld::mysql_result( $result7, 0, 'amount6' ) );
				}
				else{
					$amount1=0;
					$amount2=0;
					$amount3=0;
					$amount4=0;
					$amount5=0;
					$amount6=0;
				}

				if ($weekcount==5){$monthtotal=$amount1+$amount2+$amount3+$amount4+$amount5;}
				elseif ($weekcount==6){$monthtotal=$amount1+$amount2+$amount3+$amount4+$amount5+$amount6;}
				else{$monthtotal=$amount1+$amount2+$amount3+$amount4;}
				$wk1=$wk1+$amount1;
				$wk2=$wk2+$amount2;
				$wk3=$wk3+$amount3;
				$wk4=$wk4+$amount4;
				if ($weekcount==5){$wk5=$wk5+$amount5;}
				if ($weekcount==6){$wk5=$wk5+$amount5;$wk6=$wk6+$amount6;}

				$amount1prcnt=0;
				$amount2prcnt=0;
				$amount3prcnt=0;
				$amount4prcnt=0;
				$amount5prcnt=0;
				$amount6prcnt=0;
				if ($amount1!=0&&$saleswk1!=0){$amount1prcnt=round(($amount1/$saleswk1)*100,1);}
				if ($amount2!=0&&$saleswk2!=0){$amount2prcnt=round(($amount2/$saleswk2)*100,1);}
				if ($amount3!=0&&$saleswk3!=0){$amount3prcnt=round(($amount3/$saleswk3)*100,1);}
				if ($amount4!=0&&$saleswk4!=0){$amount4prcnt=round(($amount4/$saleswk4)*100,1);}
				if ($amount5!=0&&$saleswk5!=0){$amount5prcnt=round(($amount5/$saleswk5)*100,1);}
				if ($amount6!=0&&$saleswk6!=0){$amount6prcnt=round(($amount6/$saleswk6)*100,1);}

			}

			elseif($cs_type==6){
				$day=$startdate;  

				$prevamount=0;
				$aptotal=0;
				$monthtotal=0;

				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( mysql_error());
				$query5 = "SELECT * FROM credits WHERE businessid = '$businessid' AND (credittype = '1' OR credittype = '5') AND ((is_deleted = '0') OR (is_deleted = '1' AND del_date >= '$startdate'))";
				$result5 = Treat_DB_ProxyOld::query( $query5 );
				$num5 = Treat_DB_ProxyOld::mysql_numrows( $result5 );
				//mysql_close();
				$debitnum=$num5;

				while ($day<=$enddate){
					$num5=$debitnum;
					$num5--;
					while ($num5>=0){
						$creditid = @Treat_DB_ProxyOld::mysql_result( $result5, $num5, 'creditid' );
						$invoicesales = @Treat_DB_ProxyOld::mysql_result( $result5, $num5, 'invoicesales' );
						$invoicesales_notax = @Treat_DB_ProxyOld::mysql_result( $result5, $num5, 'invoicesales_notax' );
						$servchrg = @Treat_DB_ProxyOld::mysql_result( $result5, $num5, 'servchrg' );

						if ($invoicesales==0&&$invoicesales_notax==0&&$servchrg==0){
							//mysql_connect($dbhost,$username,$password);
							//@mysql_select_db($database) or die( mysql_error());
							$query6 = "SELECT amount FROM creditdetail WHERE creditid = '$creditid' AND date = '$day'";
							$result6 = Treat_DB_ProxyOld::query( $query6 );
							$num6 = Treat_DB_ProxyOld::mysql_numrows( $result6 );
							//mysql_close();

							$num6--;
							while ($num6>=0){
								$amount = @Treat_DB_ProxyOld::mysql_result( $result6, $num6, 'amount' );
								if ($cs_glacct==-1||$cs_glacct==$creditid){$prevamount=$prevamount+$amount;}
								$num6--;
							}
						}
						elseif ($invoicesales==1){
							//mysql_connect($dbhost,$username,$password);
							//@mysql_select_db($database) or die( mysql_error());
							$query7 = "SELECT total,taxtotal,servamt FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND taxable = 'on' AND salescode = '0' AND (status = '4' OR status = '2' OR status = '6')";
							$result7 = Treat_DB_ProxyOld::query( $query7 );
							$num7 = Treat_DB_ProxyOld::mysql_numrows( $result7 );
							//mysql_close();
							$num7--;
							while ($num7>=0){
								$invtotal = @Treat_DB_ProxyOld::mysql_result( $result7, $num7, 'total' );
								$invtax = @Treat_DB_ProxyOld::mysql_result( $result7, $num7, 'taxtotal' );
								$servamt = @Treat_DB_ProxyOld::mysql_result( $result7, $num7, 'servamt' );
								$prevamount=$prevamount+($invtotal-$invtax-$servamt);
								$num7--;
							}
						}
						elseif ($invoicesales_notax==1){
							//mysql_connect($dbhost,$username,$password);
							//@mysql_select_db($database) or die( mysql_error());
							$query7 = "SELECT total,taxtotal,servamt FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND taxable = 'off' AND salescode = '0' AND (status = '4' OR status = '2' OR status = '6')";
							$result7 = Treat_DB_ProxyOld::query( $query7 );
							$num7 = Treat_DB_ProxyOld::mysql_numrows( $result7 );
							//mysql_close();
							$num7--;
							while ($num7>=0){
								$invtotal = @Treat_DB_ProxyOld::mysql_result( $result7, $num7, 'total' );
								$servamt = @Treat_DB_ProxyOld::mysql_result( $result7, $num7, 'servamt' );
								$prevamount=$prevamount+$invtotal-$servamt;
								$num7--;
							}
						}
						elseif ($servchrg==1){
							//mysql_connect($dbhost,$username,$password);
							//@mysql_select_db($database) or die( mysql_error());
							$query7 = "SELECT servamt FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND (status = '4' OR status = '2' OR status = '6')";
							$result7 = Treat_DB_ProxyOld::query( $query7 );
							$num7 = Treat_DB_ProxyOld::mysql_numrows( $result7 );
							//mysql_close();
							$num7--;
							while ($num7>=0){
								$servamt = @Treat_DB_ProxyOld::mysql_result( $result7, $num7, 'servamt' );
								$prevamount=$prevamount+$servamt;
								$num7--;
							}
						}
						$num5--;
					}

					if ($weekday=="Thursday" || $day == $enddate){
						if ($prevamount!=0){$prevamount=money($prevamount*$cs_prcnt/100);}
						$prcnt=0;
						if ($currentweek==1&&$prevamount!=0&&$saleswk1!=0){$prcnt=($prevamount/$saleswk1)*100;$prcnt=round($prcnt,1);}
						elseif ($currentweek==2&&$prevamount!=0&&$saleswk2!=0){$prcnt=($prevamount/$saleswk2)*100;$prcnt=round($prcnt,1);}
						elseif ($currentweek==3&&$prevamount!=0&&$saleswk3!=0){$prcnt=($prevamount/$saleswk3)*100;$prcnt=round($prcnt,1);}
						elseif ($currentweek==4&&$prevamount!=0&&$saleswk4!=0){$prcnt=($prevamount/$saleswk4)*100;$prcnt=round($prcnt,1);}
						elseif ($currentweek==5&&$prevamount!=0&&$saleswk5!=0){$prcnt=($prevamount/$saleswk5)*100;$prcnt=round($prcnt,1);}
						elseif ($currentweek==6&&$prevamount!=0&&$saleswk6!=0){$prcnt=($prevamount/$saleswk6)*100;$prcnt=round($prcnt,1);}

						if ($currentweek==1){$wk1=$wk1+$prevamount;}
						elseif ($currentweek==2){$wk2=$wk2+$prevamount;}
						elseif ($currentweek==3){$wk3=$wk3+$prevamount;}
						elseif ($currentweek==4){$wk4=$wk4+$prevamount;}
						elseif ($currentweek==5){$wk5=$wk5+$prevamount;}
						elseif ($currentweek==6){$wk6=$wk6+$prevamount;}

						$monthtotal=$prevamount+$monthtotal;
						$currentweek++;
						$prevamount=0;
					}

					$day=nextday($day);
					if ($weekday=="Sunday"){$weekday="Monday";}
					elseif ($weekday=="Saturday"){$weekday="Sunday";}
					elseif ($weekday=="Friday"){$weekday="Saturday";}
					elseif ($weekday=="Thursday"){$weekday="Friday";}
					elseif ($weekday=="Wednesday"){$weekday="Thursday";}
					elseif ($weekday=="Tuesday"){$weekday="Wednesday";}
					elseif ($weekday=="Monday"){$weekday="Tuesday";}
				}
			}
			elseif($cs_type==7){
				$day=$startdate;  
				$month=substr($day,5,2);
				$leap = date("L");
				if ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10' || $month == '12'){$monthcount=31;}
				elseif($month=='04' || $month=='06' || $month=='09' || $month=='11'){$monthcount=30;}
				elseif($month=='02' && $leap == 0){$monthcount=28;}
				elseif($month=='02' && $leap == 1){$monthcount=29;}

				$daily_fixed=$daily_fixed/$monthcount;

				$prevamount=0;
				$aptotal=0;
				$monthtotal=0;
				while ($day<=$enddate){

					$prevamount=$prevamount+$daily_fixed;

					if ($currentweek==1){$wk1=$wk1+$daily_fixed;}
					elseif ($currentweek==2){$wk2=$wk2+$daily_fixed;}
					elseif ($currentweek==3){$wk3=$wk3+$daily_fixed;}
					elseif ($currentweek==4){$wk4=$wk4+$daily_fixed;}
					elseif ($currentweek==5){$wk5=$wk5+$daily_fixed;}
					elseif ($currentweek==6){$wk6=$wk6+$daily_fixed;}

					$prcnt=0;
					if ($currentweek==1&&$prevamount!=0&&$saleswk1!=0){$prcnt=($prevamount/$saleswk1)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==2&&$prevamount!=0&&$saleswk2!=0){$prcnt=($prevamount/$saleswk2)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==3&&$prevamount!=0&&$saleswk3!=0){$prcnt=($prevamount/$saleswk3)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==4&&$prevamount!=0&&$saleswk4!=0){$prcnt=($prevamount/$saleswk4)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==5&&$prevamount!=0&&$saleswk5!=0){$prcnt=($prevamount/$saleswk5)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==6&&$prevamount!=0&&$saleswk6!=0){$prcnt=($prevamount/$saleswk6)*100;$prcnt=round($prcnt,1);}

					if ($weekday=="Thursday" || $day == $enddate){
						$showamount=$prevamount;
						if ($showamount!=0){$showamount=money($showamount);}
						$monthtotal=$prevamount+$monthtotal;
						$currentweek++;
						$prevamount=0;
					}

					$day=nextday($day);
					if ($weekday=="Sunday"){$weekday="Monday";}
					elseif ($weekday=="Saturday"){$weekday="Sunday";}
					elseif ($weekday=="Friday"){$weekday="Saturday";}
					elseif ($weekday=="Thursday"){$weekday="Friday";}
					elseif ($weekday=="Wednesday"){$weekday="Thursday";}
					elseif ($weekday=="Tuesday"){$weekday="Wednesday";}
					elseif ($weekday=="Monday"){$weekday="Tuesday";}
				}

			}
			elseif($cs_type==12){
				$day=$startdate;  
				$month=substr($day,5,2);
				$recuryear=substr($day,0,4);
				$leap = date("L");
				if ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10' || $month == '12'){$monthcount=31;}
				elseif($month=='04' || $month=='06' || $month=='09' || $month=='11'){$monthcount=30;}
				elseif($month=='02' && $leap == 0){$monthcount=28;}
				elseif($month=='02' && $leap == 1){$monthcount=29;}

				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( mysql_error());
				$query12 = "SELECT `$month` FROM recur WHERE year = '$recuryear' AND apaccountid = '$acct_payableid'";
				$result12 = Treat_DB_ProxyOld::query( $query12 );
				//mysql_close();

				$recuramount = @Treat_DB_ProxyOld::mysql_result( $result12, 0, $month );
				$recuramount=$recuramount/$monthcount;

				$prevamount=0;
				$aptotal=0;
				$monthtotal=0;
				while ($day<=$enddate){

					$prevamount=$prevamount+$recuramount;

					if ($currentweek==1){$wk1=$wk1+$recuramount;}
					elseif ($currentweek==2){$wk2=$wk2+$recuramount;}
					elseif ($currentweek==3){$wk3=$wk3+$recuramount;}
					elseif ($currentweek==4){$wk4=$wk4+$recuramount;}
					elseif ($currentweek==5){$wk5=$wk5+$recuramount;}
					elseif ($currentweek==6){$wk6=$wk6+$recuramount;}

					$prcnt=0;
					if ($currentweek==1&&$prevamount!=0&&$saleswk1!=0){$prcnt=($prevamount/$saleswk1)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==2&&$prevamount!=0&&$saleswk2!=0){$prcnt=($prevamount/$saleswk2)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==3&&$prevamount!=0&&$saleswk3!=0){$prcnt=($prevamount/$saleswk3)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==4&&$prevamount!=0&&$saleswk4!=0){$prcnt=($prevamount/$saleswk4)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==5&&$prevamount!=0&&$saleswk5!=0){$prcnt=($prevamount/$saleswk5)*100;$prcnt=round($prcnt,1);}
					elseif ($currentweek==6&&$prevamount!=0&&$saleswk6!=0){$prcnt=($prevamount/$saleswk6)*100;$prcnt=round($prcnt,1);}

					if ($weekday=="Thursday" || $day == $enddate){
						$showamount=$prevamount;
						if ($showamount!=0){$showamount=money($showamount);}
						//echo "<td  align=right><font size=2>$$showamount</td><td align=right><font size=1 color=blue>$prcnt%</td>";
						$monthtotal=$prevamount+$monthtotal;
						$currentweek++;
						$prevamount=0;
					}

					$day=nextday($day);
					if ($weekday=="Sunday"){$weekday="Monday";}
					elseif ($weekday=="Saturday"){$weekday="Sunday";}
					elseif ($weekday=="Friday"){$weekday="Saturday";}
					elseif ($weekday=="Thursday"){$weekday="Friday";}
					elseif ($weekday=="Wednesday"){$weekday="Thursday";}
					elseif ($weekday=="Tuesday"){$weekday="Wednesday";}
					elseif ($weekday=="Monday"){$weekday="Tuesday";}
				}

			}

			$day=$startdate;
			$weekday=$startweekday;

			$monthprcnt=0;
			$monthtotal=money($monthtotal);
			if ($monthtotal!=0&&$salestotal!=0){$monthprcnt=round(($monthtotal/$salestotal)*100,1);}
			
			$num--;
			$currentweek=1;
			echo "$num<br>";
		}

		$cashtotal=money($cashwk1+$cashwk2+$cashwk3+$cashwk4+$cashwk5+$cashwk6);
		$cashwk1=money($cashwk1);
		$cashwk2=money($cashwk2);
		$cashwk3=money($cashwk3);
		$cashwk4=money($cashwk4);
		$cashwk5=money($cashwk5);
		$cashwk6=money($cashwk6);

		$apwk1=$apwk1+$wk1+$cashwk1;
		$apwk2=$apwk2+$wk2+$cashwk2;
		$apwk3=$apwk3+$wk3+$cashwk3;
		$apwk4=$apwk4+$wk4+$cashwk4;
		$apwk5=$apwk5+$wk5+$cashwk5;
		$apwk6=$apwk6+$wk6+$cashwk6;

		$weektotal=$wk1+$wk2+$wk3+$wk4+$wk5+$wk6+$cashwk1+$cashwk2+$cashwk3+$cashwk4+$cashwk5+$cashwk6;
		$wk1=money($wk1+$cashwk1);
		$wk2=money($wk2+$cashwk2);
		$wk3=money($wk3+$cashwk3);
		$wk4=money($wk4+$cashwk4);
		$wk5=money($wk5+$cashwk5);
		$wk6=money($wk6+$cashwk6);
		$weektotal=money($weektotal);
		
		if ($wk1!=0&&$saleswk1!=0){$toewk1prcnt=round($wk1/$saleswk1*100,1);} else{$toewk1prcnt=0;}
		if ($wk2!=0&&$saleswk2!=0){$toewk2prcnt=round($wk2/$saleswk2*100,1);} else{$toewk2prcnt=0;}
		if ($wk3!=0&&$saleswk3!=0){$toewk3prcnt=round($wk3/$saleswk3*100,1);} else{$toewk3prcnt=0;}
		if ($wk4!=0&&$saleswk4!=0){$toewk4prcnt=round($wk4/$saleswk4*100,1);} else{$toewk4prcnt=0;}
		if ($wk5!=0&&$saleswk5!=0){$toewk5prcnt=round($wk5/$saleswk5*100,1);} else{$toewk5prcnt=0;}
		if ($wk6!=0&&$saleswk6!=0){$toewk6prcnt=round($wk6/$saleswk6*100,1);} else{$toewk6prcnt=0;}
		if ($salestotal!=0){$monthprcnt=round($weektotal/$salestotal*100,1);} else{$monthprcnt=0;}

		$overallpcnt=$overallpcnt+$monthprcnt;

		$wk2mtd=money($wk1+$wk2);
		$wk3mtd=money($wk2mtd+$wk3);
		$wk4mtd=money($wk3mtd+$wk4);
		$wk5mtd=money($wk4mtd+$wk5);
		$wk6mtd=money($wk5mtd+$wk6);

		if ($sales2mtd!=0){$toewk2prcntmtd=round($wk2mtd/$sales2mtd*100,1);}else{$toewk2prcntmtd=0;}
		if ($sales3mtd!=0){$toewk3prcntmtd=round($wk3mtd/$sales3mtd*100,1);}else{$toewk3prcntmtd=0;}
		if ($sales4mtd!=0){$toewk4prcntmtd=round($wk4mtd/$sales4mtd*100,1);}else{$toewk4prcntmtd=0;}
		if ($sales5mtd!=0){$toewk5prcntmtd=round($wk5mtd/$sales5mtd*100,1);}else{$toewk5prcntmtd=0;}
		if ($sales6mtd!=0){$toewk6prcntmtd=round($wk6mtd/$sales6mtd*100,1);}else{$toewk6prcntmtd=0;}

		$showtab="";
		if ($goweek==1&&strlen($toewk1prcnt)<5){$showtab="\t";}
		elseif ($goweek==2&&strlen($toewk2prcnt)<5){$showtab="\t";}
		elseif ($goweek==3&&strlen($toewk3prcnt)<5){$showtab="\t";}
		elseif ($goweek==4&&strlen($toewk4prcnt)<5){$showtab="\t";}
		elseif ($goweek==5&&strlen($toewk5prcnt)<5){$showtab="\t";}
		elseif ($goweek==6&&strlen($toewk6prcnt)<5){$showtab="\t";}
		else{$showtab="";}

		if ($goweek==1){$message="$message<tr><td align=right>TOE:</td><td align=right>$toewk1prcnt%</td><td align=right></td><td align=right>$wktoebgt%</td><td align=right></td><td align=right>$toewk1prcnt%</td><td align=right></td><td align=right>$mtdtoebgt%</td><td align=right></td><td align=right>$$bgttoe</td><td align=right>$toeprcnt%</td></tr>";$comtoe=$comtoe+$wk1;$commtdtoe=$commtdtoe+$wk1;$disttoe=$disttoe+$wk1;$distmtdtoe=$distmtdtoe+$wk1;$variance=money($wk1mtd-$mtdtoebgt9);$newbudget="$newbudget<td>$wk1</td><td>$mtdtoebgt9</td><td bgcolor=#FFFF99>$variance</td>";}
		elseif ($goweek==2){$message="$message<tr><td align=right>TOE:</td><td align=right>$toewk2prcnt%</td><td align=right></td><td align=right>$wktoebgt%</td><td align=right></td><td align=right>$toewk2prcntmtd%</td><td align=right></td><td align=right>$mtdtoebgt%</td><td align=right></td><td align=right>$$bgttoe</td><td align=right>$toeprcnt%</td></tr>";$comtoe=$comtoe+$wk2;$commtdtoe=$commtdtoe+$wk2mtd;$disttoe=$disttoe+$wk2;$distmtdtoe=$distmtdtoe+$wk2mtd;$variance=money($wk2mtd-$mtdtoebgt9);$newbudget="$newbudget<td>$wk2mtd</td><td>$mtdtoebgt9</td><td bgcolor=#FFFF99>$variance</td>";}
		elseif ($goweek==3){$message="$message<tr><td align=right>TOE:</td><td align=right>$toewk3prcnt%</td><td align=right></td><td align=right>$wktoebgt%</td><td align=right></td><td align=right>$toewk3prcntmtd%</td><td align=right></td><td align=right>$mtdtoebgt%</td><td align=right></td><td align=right>$$bgttoe</td><td align=right>$toeprcnt%</td></tr>";$comtoe=$comtoe+$wk3;$commtdtoe=$commtdtoe+$wk3mtd;$disttoe=$disttoe+$wk3;$distmtdtoe=$distmtdtoe+$wk3mtd;$variance=money($wk3mtd-$mtdtoebgt9);$newbudget="$newbudget<td>$wk3mtd</td><td>$mtdtoebgt9</td><td bgcolor=#FFFF99>$variance</td>";}
		elseif ($goweek==4){$message="$message<tr><td align=right>TOE:</td><td align=right>$toewk4prcnt%</td><td align=right></td><td align=right>$wktoebgt%</td><td align=right></td><td align=right>$toewk4prcntmtd%</td><td align=right></td><td align=right>$mtdtoebgt%</td><td align=right></td><td align=right>$$bgttoe</td><td align=right>$toeprcnt%</td></tr>";$comtoe=$comtoe+$wk4;$commtdtoe=$commtdtoe+$wk4mtd;$disttoe=$disttoe+$wk4;$distmtdtoe=$distmtdtoe+$wk4mtd;$variance=money($wk4mtd-$mtdtoebgt9);$newbudget="$newbudget<td>$wk4mtd</td><td>$mtdtoebgt9</td><td bgcolor=#FFFF99>$variance</td>";}
		elseif ($goweek==5){$message="$message<tr><td align=right>TOE:</td><td align=right>$toewk5prcnt%</td><td align=right></td><td align=right>$wktoebgt%</td><td align=right></td><td align=right>$toewk5prcntmtd%</td><td align=right></td><td align=right>$mtdtoebgt%</td><td align=right></td><td align=right>$$bgttoe</td><td align=right>$toeprcnt%</td></tr>";$comtoe=$comtoe+$wk5;$commtdtoe=$commtdtoe+$wk5mtd;$disttoe=$disttoe+$wk5;$distmtdtoe=$distmtdtoe+$wk5mtd;$variance=money($wk5mtd-$mtdtoebgt9);$newbudget="$newbudget<td>$wk5mtd</td><td>$mtdtoebgt9</td><td bgcolor=#FFFF99>$variance</td>";}
		elseif ($goweek==6){$message="$message<tr><td align=right>TOE:</td><td align=right>$toewk6prcnt%</td><td align=right></td><td align=right>$wktoebgt%</td><td align=right></td><td align=right>$toewk6prcntmtd%</td><td align=right></td><td align=right>$mtdtoebgt%</td><td align=right></td><td align=right>$$bgttoe</td><td align=right>$toeprcnt%</td></tr>";$comtoe=$comtoe+$wk6;$commtdtoe=$commtdtoe+$wk6mtd;$disttoe=$disttoe+$wk6;$distmtdtoe=$distmtdtoe+$wk6mtd;$variance=money($wk6mtd-$mtdtoebgt9);$newbudget="$newbudget<td>$wk6mtd</td><td>$mtdtoebgt9</td><td bgcolor=#FFFF99>$variance</td>";}

		echo "Figured TOE...$wk2<br>";

		/////////////////////BILL TO CLIENT/////////////////////////////////
	
		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( mysql_error());
		$query7 = "SELECT * FROM controlsheet WHERE businessid = '$businessid' AND date = '$startdate' AND type = '2'";
		$result7 = Treat_DB_ProxyOld::query( $query7 );
		$num7 = Treat_DB_ProxyOld::mysql_numrows( $result7 );
		//mysql_close();

		if ($num7!=0){
			$csid = @Treat_DB_ProxyOld::mysql_result( $result7, 0, 'csid' );
			$amount1 = money( @Treat_DB_ProxyOld::mysql_result( $result7, 0, 'amount1' ) );
			$amount2 = money( @Treat_DB_ProxyOld::mysql_result( $result7, 0, 'amount2' ) );
			$amount3 = money( @Treat_DB_ProxyOld::mysql_result( $result7, 0, 'amount3' ) );
			$amount4 = money( @Treat_DB_ProxyOld::mysql_result( $result7, 0, 'amount4' ) );
			$amount5 = money( @Treat_DB_ProxyOld::mysql_result( $result7, 0, 'amount5' ) );
			$amount6 = money( @Treat_DB_ProxyOld::mysql_result( $result7, 0, 'amount6' ) );
		}
		else{
			$amount1=0;
			$amount2=0;
			$amount3=0;
			$amount4=0;
			$amount5=0;
			$amount6=0;
		}

		if($cs_amounttype==1){
			$amount1=round(($saleswk1-$apwk1+($cs_special/$totaloperate*$operate1))*($cs_amount/100),2);
			$amount2=round(($saleswk2-$apwk2+($cs_special/$totaloperate*$operate2))*($cs_amount/100),2);
			$amount3=round(($saleswk3-$apwk3+($cs_special/$totaloperate*$operate3))*($cs_amount/100),2);
			$amount4=round(($saleswk4-$apwk4+($cs_special/$totaloperate*$operate4))*($cs_amount/100),2);
			$amount5=round(($saleswk5-$apwk5+($cs_special/$totaloperate*$operate5))*($cs_amount/100),2);
			$amount6=round(($saleswk6-$apwk6+($cs_special/$totaloperate*$operate6))*($cs_amount/100),2);

			/*
			if($cs_amount==100&&$amount1<0){$amount1=$amount1*-1;}
			//elseif($cs_amount==100&&$amount1>0){$amount1=0;}
			if($cs_amount==100&&$amount2<0){$amount2=$amount2*-1;}
			//elseif($cs_amount==100&&$amount2>0){$amount2=0;}
			if($cs_amount==100&&$amount3<0){$amount3=$amount3*-1;}
			//elseif($cs_amount==100&&$amount3>0){$amount3=0;}
			if($cs_amount==100&&$amount4<0){$amount4=$amount4*-1;}
			//elseif($cs_amount==100&&$amount4>0){$amount4=0;}
			if($cs_amount==100&&$amount5<0){$amount5=$amount5*-1;}
			//elseif($cs_amount==100&&$amount5>0){$amount5=0;}
			if($cs_amount==100&&$amount6<0){$amount6=$amount6*-1;}
			//elseif($cs_amount==100&&$amount6>0){$amount6=0;}
			*/

			if($num7!=0){
				$query7 = "UPDATE controlsheet SET amount1 = '$amount1', amount2 = '$amount2', amount3 = '$amount3', amount4 = '$amount4', amount5 = '$amount5', amount6 = '$amount6' WHERE businessid = '$businessid' AND date = '$startdate' AND type = '2'";
				$result7 = Treat_DB_ProxyOld::query( $query7 );
			}
			else{
				$query7 = "INSERT INTO controlsheet (companyid,businessid,date,amount1,amount2,amount3,amount4,amount5,amount6,type) VALUES ('$companyid','$businessid','$startdate','$amount1','$amount2','$amount3','$amount4','$amount5','$amount6','2')";
				$result7 = Treat_DB_ProxyOld::query( $query7 );
			}
		}
		elseif($cs_amounttype==2){
			$amount1=money($cs_amount/$totaloperate*$operate1);
			$amount2=money($cs_amount/$totaloperate*$operate2);
			$amount3=money($cs_amount/$totaloperate*$operate3);
			$amount4=money($cs_amount/$totaloperate*$operate4);
			$amount5=money($cs_amount/$totaloperate*$operate5);
			$amount6=money($cs_amount/$totaloperate*$operate6);

			if($num7!=0){
				$query7 = "UPDATE controlsheet SET amount1 = '$amount1', amount2 = '$amount2', amount3 = '$amount3', amount4 = '$amount4', amount5 = '$amount5', amount6 = '$amount6' WHERE businessid = '$businessid' AND date = '$startdate' AND type = '2'";
				$result7 = Treat_DB_ProxyOld::query( $query7 );
			}
			else{
				$query7 = "INSERT INTO controlsheet (companyid,businessid,date,amount1,amount2,amount3,amount4,amount5,amount6,type) VALUES ('$companyid','$businessid','$startdate','$amount1','$amount2','$amount3','$amount4','$amount5','$amount6','2')";
				$result7 = Treat_DB_ProxyOld::query( $query7 );
			}
		}

		if ($goweek==1){$showamtdue=money($amount1);$showamtduemtd=money($amount1);}
		elseif ($goweek==2){$showamtdue=money($amount2);$showamtduemtd=money($amount1+$amount2);}
		elseif ($goweek==3){$showamtdue=money($amount3);$showamtduemtd=money($amount1+$amount2+$amount3);}
		elseif ($goweek==4){$showamtdue=money($amount4);$showamtduemtd=money($amount1+$amount2+$amount3+$amount4);}
		elseif ($goweek==5){$showamtdue=money($amount5);$showamtduemtd=money($amount1+$amount2+$amount3+$amount4+$amount5);}
		elseif ($goweek==6){$showamtdue=money($amount6);$showamtduemtd=money($amount1+$amount2+$amount3+$amount4+$amount5+$amount6);}
		
		//$showamtduemtd=money($amount1+$amount2+$amount3+$amount4+$amount5+$amount6);

		//////////////////////////AMOUNT DUE
		$variance=money($showamtduemtd-$mtdamtduebgt);
		$wkamtduebgt=number_format($wkamtduebgt);
		$mtdamtduebgt=number_format($mtdamtduebgt);
		$bgtamtdue=number_format($bgtamtdue);

		$message="$message<tr><td align=right>AMT DUE:</td><td align=right>$$showamtdue</td><td align=right></td><td align=right>$$wkamtduebgt</td><td align=right></td><td align=right>$$showamtduemtd</td><td></td><td align=right>$$mtdamtduebgt</td><td align=right></td><td align=right>$$bgtamtdue</td><td></td></tr>";$newbudget="$newbudget<td>$showamtduemtd</td><td>$mtdamtduebgt</td><td bgcolor=#FFFF99>$variance</td>";

		//////////////////BENEFITS//////////////////////////////////////

		///////P&L////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		

		$apmonthtotal=money($salestotal-($apwk1+$apwk2+$apwk3+$apwk4+$apwk5+$apwk6-$invadj)+$monthtotal);
		$apwk1=money($saleswk1-$apwk1+$amount1);
		$apwk1m=$apwk1;
		if ($apwk1==0){$apwk1=0;}
		$apwk2=money($saleswk2-$apwk2+$amount2);
		$apwk2m=money($apwk1m+$apwk2);
		if ($apwk2==0){$apwk2=0;}
		$apwk3=money($saleswk3-$apwk3+$amount3);
		$apwk3m=money($apwk2m+$apwk3);
		if ($apwk3==0){$apwk3=0;}
		$apwk4=money($saleswk4-$apwk4+$amount4);
		$apwk4m=money($apwk3m+$apwk4);
		if ($apwk4==0){$apwk4=0;}
		$apwk5=money($saleswk5-$apwk5+$amount5);
		$apwk5m=money($apwk4m+$apwk5);
		if ($apwk5==0){$apwk5=0;}
		$apwk6=money($saleswk6-$apwk6+$amount6);
		$apwk6m=money($apwk5m+$apwk6);
		if ($apwk6==0){$apwk6=0;}

		if ($goweek==1){$with_commas = number_format($apwk1);$message="$message<tr><td align=right>P&L:</td><td align=right>$$with_commas</td><td align=right></td><td align=right>$$wknetpftbgt</td><td align=right></td><td align=right>$$with_commas</td><td align=right></td><td align=right>$$mtdnetpftbgt</td><td align=right></td><td align=right>$$bgtnetpft</td><td align=right>$netpftprcnt%</td></tr>";$compl=$compl+$apwk1;$commtdpl=$commtdpl+$apwk1;$distpl=$distpl+$apwk1;$distmtdpl=$distmtdpl+$apwk1;$variance=money($apwk1-$mtdnetpftbgt9);$newbudget="$newbudget<td>$with_commas</td><td>$mtdnetpftbgt</td><td bgcolor=#FFFF99>$variance</td><td>$category_name</td></tr>";}
		elseif ($goweek==2){$with_commas = number_format($apwk2);$with_commas2 = number_format($apwk2m);$message="$message<tr><td align=right>P&L:</td><td align=right>$$with_commas</td><td align=right></td><td align=right>$$wknetpftbgt</td><td align=right></td><td align=right>$$with_commas2</td><td align=right></td><td align=right>$$mtdnetpftbgt</td><td align=right></td><td align=right>$$bgtnetpft</td><td align=right>$netpftprcnt%</td></tr>";$compl=$compl+$apwk2;$commtdpl=$commtdpl+$apwk2m;$distpl=$distpl+$apwk2;$distmtdpl=$distmtdpl+$apwk2m;$variance=money($apwk2m-$mtdnetpftbgt9);$newbudget="$newbudget<td>$with_commas2</td><td>$mtdnetpftbgt</td><td bgcolor=#FFFF99>$variance</td><td>$category_name</td></tr>";}
		elseif ($goweek==3){$with_commas = number_format($apwk3);$with_commas2 = number_format($apwk3m);$message="$message<tr><td align=right>P&L:</td><td align=right>$$with_commas</td><td align=right></td><td align=right>$$wknetpftbgt</td><td align=right></td><td align=right>$$with_commas2</td><td align=right></td><td align=right>$$mtdnetpftbgt</td><td align=right></td><td align=right>$$bgtnetpft</td><td align=right>$netpftprcnt%</td></tr>";$compl=$compl+$apwk3;$commtdpl=$commtdpl+$apwk3m;$distpl=$distpl+$apwk3;$distmtdpl=$distmtdpl+$apwk3m;$variance=money($apwk3m-$mtdnetpftbgt9);$newbudget="$newbudget<td>$with_commas2</td><td>$mtdnetpftbgt</td><td bgcolor=#FFFF99>$variance</td><td>$category_name</td></tr>";}
		elseif ($goweek==4){$with_commas = number_format($apwk4);$with_commas2 = number_format($apwk4m);$message="$message<tr><td align=right>P&L:</td><td align=right>$$with_commas</td><td align=right></td><td align=right>$$wknetpftbgt</td><td align=right></td><td align=right>$$with_commas2</td><td align=right></td><td align=right>$$mtdnetpftbgt</td><td align=right></td><td align=right>$$bgtnetpft</td><td align=right>$netpftprcnt%</td></tr>";$compl=$compl+$apwk4;$commtdpl=$commtdpl+$apwk4m;$distpl=$distpl+$apwk4;$distmtdpl=$distmtdpl+$apwk4m;$variance=money($apwk4m-$mtdnetpftbgt9);$newbudget="$newbudget<td>$with_commas2</td><td>$mtdnetpftbgt</td><td bgcolor=#FFFF99>$variance</td><td>$category_name</td></tr>";}
		elseif ($goweek==5){$with_commas = number_format($apwk5);$with_commas2 = number_format($apwk5m);$message="$message<tr><td align=right>P&L:</td><td align=right>$$with_commas</td><td align=right></td><td align=right>$$wknetpftbgt</td><td align=right></td><td align=right>$$with_commas2</td><td align=right></td><td align=right>$$mtdnetpftbgt</td><td align=right></td><td align=right>$$bgtnetpft</td><td align=right>$netpftprcnt%</td></tr>";$compl=$compl+$apwk5;$commtdpl=$commtdpl+$apwk5m;$distpl=$distpl+$apwk5;$distmtdpl=$distmtdpl+$apwk5m;$variance=money($apwk5m-$mtdnetpftbgt9);$newbudget="$newbudget<td>$with_commas2</td><td>$mtdnetpftbgt</td><td bgcolor=#FFFF99>$variance</td><td>$category_name</td></tr>";}
		elseif ($goweek==6){$with_commas = number_format($apwk6);$with_commas2 = number_format($apwk6m);$message="$message<tr><td align=right>P&L:</td><td align=right>$$with_commas</td><td align=right></td><td align=right>$$wknetpftbgt</td><td align=right></td><td align=right>$$with_commas2</td><td align=right></td><td align=right>$$mtdnetpftbgt</td><td align=right></td><td align=right>$$bgtnetpft</td><td align=right>$netpftprcnt%</td></tr>";$compl=$compl+$apwk6;$commtdpl=$commtdpl+$apwk6m;$distpl=$distpl+$apwk6;$distmtdpl=$distmtdpl+$apwk6m;$variance=money($apwk6m-$mtdnetpftbgt9);$newbudget="$newbudget<td>$with_commas2</td><td>$mtdnetpftbgt</td><td bgcolor=#FFFF99>$variance</td><td>$category_name</td><td>$sort</td></tr>";}


		/////////FIND EXCEPTIONS/////////////////////////////////////////////////////////////////////////////////////////

		if ($goweek==1){$error1=$coswk1prcnt-$wkcosbgt;$error2=$lbrwk1prcnt-$wklaborbgt;$error3=$toewk1prcnt-$wktoebgt;$error4=$coswk1prcnt-$mtdcosbgt;$error5=$lbrwk1prcnt-$mtdlaborbgt;$error6=$toewk1prcnt-$mtdtoebgt;}
		elseif ($goweek==2){$error1=$coswk2prcnt-$wkcosbgt;$error2=$lbrwk2prcnt-$wklaborbgt;$error3=$toewk2prcnt-$wktoebgt;$error4=$coswk2prcntmtd-$mtdcosbgt;$error5=$lbrwk2prcntmtd-$mtdlaborbgt;$error6=$toewk2prcntmtd-$mtdtoebgt;}
		elseif ($goweek==3){$error1=$coswk3prcnt-$wkcosbgt;$error2=$lbrwk3prcnt-$wklaborbgt;$error3=$toewk3prcnt-$wktoebgt;$error4=$coswk3prcntmtd-$mtdcosbgt;$error5=$lbrwk3prcntmtd-$mtdlaborbgt;$error6=$toewk3prcntmtd-$mtdtoebgt;}
		elseif ($goweek==4){$error1=$coswk4prcnt-$wkcosbgt;$error2=$lbrwk4prcnt-$wklaborbgt;$error3=$toewk4prcnt-$wktoebgt;$error4=$coswk4prcntmtd-$mtdcosbgt;$error5=$lbrwk4prcntmtd-$mtdlaborbgt;$error6=$toewk4prcntmtd-$mtdtoebgt;}
		elseif ($goweek==5){$error1=$coswk5prcnt-$wkcosbgt;$error2=$lbrwk5prcnt-$wklaborbgt;$error3=$toewk5prcnt-$wktoebgt;$error4=$coswk5prcntmtd-$mtdcosbgt;$error5=$lbrwk5prcntmtd-$mtdlaborbgt;$error6=$toewk5prcntmtd-$mtdtoebgt;}
		elseif ($goweek==6){$error1=$coswk6prcnt-$wkcosbgt;$error2=$lbrwk6prcnt-$wklaborbgt;$error3=$toewk6prcnt-$wktoebgt;$error4=$coswk6prcntmtd-$mtdcosbgt;$error5=$lbrwk6prcntmtd-$mtdlaborbgt;$error6=$toewk6prcntmtd-$mtdtoebgt;}

		//if ($error1 > 5 || $error1 < -5 || $error2 > 5 || $error2 < -5 || $error3 > 5 || $error3 < -5 || $error4 > 5 || $error4 < -5 || $error5 > 5 || $error5 < -5 || $error6 > 5 || $error6 < -5){
		if(1==1){

			$myFile3 = DIR_COMKPI . "/comkpi.$businessid.htm";
			$ourFileHandle = fopen($myFile3, 'w') or die("can't open file");
			fclose($ourFileHandle);
			$fh3 = fopen($myFile3, 'w') or die("can't open file");
			fwrite($fh3, "<html><table>");
			fwrite($fh2, "$message");
			fwrite($fh4, "$message");
			fwrite($fh3, "$message");
			$message7="<tr><td colspan=11> *WTD: ";
			if ($error1 > 5){$message7="$message7 <font color=red>COS $error1 pts over bdgt</font> - ";}
			elseif ($error1 < -5){$error1=$error1*-1;$message7="$message7 <font color=green>COS $error1 pts under bdgt</font> - ";}
			if ($error2 > 5){$message7="$message7 <font color=red>LABOR $error2 pts over bdgt</font> - ";}
			elseif ($error2 < -5){$error2=$error2*-1;$message7="$message7 <font color=green>LABOR $error2 pts under bdgt</font> - ";}
			if ($error3 > 5){$message7="$message7 <font color=red>TOE $error3 pts over bdgt</font>";}
			elseif ($error3 < -5){$error3=$error3*-1;$message7="$message7 <font color=green>TOE $error3 pts under bdgt</font>";}
			fwrite($fh3, "$message7");
			$message7="$message7 </td></tr>";
			
			$message7="$message7 <tr><td colspan=11> *MTD: ";
			fwrite($fh3, "<tr><td colspan=11> *MTD: ");
			if ($error4 > 5){$message7="$message7 <font color=red>COS $error4 pts over bdgt</font> - ";fwrite($fh3, " <font color=red>COS $error4 pts over bdgt</font> - ");}
			elseif ($error4 < -5){$error4=$error4*-1;$message7="$message7 <font color=green>COS $error4 pts under bdgt</font> - ";fwrite($fh3, "<font color=green>COS $error4 pts under bdgt</font> - ");}
			if ($error5 > 5){$message7="$message7 <font color=red>LABOR $error5 pts over bdgt</font> - ";fwrite($fh3, " <font color=red>LABOR $error5 pts over bdgt</font> - ");}
			elseif ($error5 < -5){$error5=$error5*-1;$message7="$message7 <font color=green>LABOR $error5 pts under bdgt</font> - ";fwrite($fh3, " <font color=green>LABOR $error5 pts under bdgt</font> - ");}
			if ($error6 > 5){$message7="$message7 <font color=red>TOE $error6 pts over bdgt</font>";fwrite($fh3, " <font color=red>TOE $error6 pts over bdgt</font>");}
			elseif ($error6 < -5){$error6=$error6*-1;$message7="$message7 <font color=green>TOE $error6 pts under bdgt</font>";fwrite($fh3, " <font color=green>TOE $error6 pts under bdgt</font>");}
			$message7="$message7 </td></tr>";

			fwrite($fh2, "$message7");
			fwrite($fh4, "$message7");

			if($goweek==1){$showlaborhour=round($saleswk1/$wk1laborhour,0);$show_labor=$wk1laborhour;}
			elseif($goweek==2){$showlaborhour=round($saleswk2/$wk2laborhour,0);$show_labor=$wk2laborhour;}
			elseif($goweek==3){$showlaborhour=round($saleswk3/$wk3laborhour,0);$show_labor=$wk3laborhour;}
			elseif($goweek==4){$showlaborhour=round($saleswk4/$wk4laborhour,0);$show_labor=$wk4laborhour;}
			elseif($goweek==5){$showlaborhour=round($saleswk5/$wk5laborhour,0);$show_labor=$wk5laborhour;}
			elseif($goweek==6){$showlaborhour=round($saleswk6/$wk6laborhour,0);$show_labor=$wk6laborhour;}

			$showlaborhourtotal=round($salestotal/($wk1laborhour+$wk2laborhour+$wk3laborhour+$wk4laborhour+$wk5laborhour+$wk6laborhour),0);

			fwrite($fh2, "<tr><td colspan=11>Labor Hours: $show_labor, Sales/Labor Hour: $$showlaborhour MTD: $$showlaborhourtotal</td></tr>");
			fwrite($fh4, "<tr><td colspan=11>Labor Hours: $show_labor,Sales/Labor Hour: $$showlaborhour MTD: $$showlaborhourtotal</td></tr>");

			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( mysql_error());
			$query56 = "SELECT firstname,lastname,email FROM login WHERE security_level > 0 AND is_deleted = 0 AND (businessid = '$businessid') || (busid2 = '$businessid') || (busid3 = '$businessid') || (busid4 = '$businessid') || (busid5 = '$businessid') || (busid6 = '$businessid') || (busid7 = '$businessid') || (busid8 = '$businessid') || (busid9 = '$businessid') || (busid10 = '$businessid')";
			$result56 = Treat_DB_ProxyOld::query( $query56 );
			$num56 = Treat_DB_ProxyOld::mysql_numrows( $result56 );
			//mysql_close();

			fwrite($fh2, "<tr><td colspan=11>");
			fwrite($fh4, "<tr><td colspan=11>");
			$num56--;
			while($num56>=0){
				$firstname = @Treat_DB_ProxyOld::mysql_result( $result56, $num56, 'firstname' );
				$lastname = @Treat_DB_ProxyOld::mysql_result( $result56, $num56, 'lastname' );
				$email = @Treat_DB_ProxyOld::mysql_result( $result56, $num56, 'email' );
				$num56--;
				fwrite($fh2, "$firstname $lastname <a href=\"mailto:$email\">$email</a>, ");
				fwrite($fh4, "$firstname $lastname <a href=\"mailto:$email\">$email</a>, ");
			}
			fwrite($fh2, "$phone</td></tr>");
			fwrite($fh4, "$phone</td></tr>");
			///////COMMENTS////////////////////////////
			$cs_year = date("Y");
			$cs_month = date("m");

			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( mysql_error());
			$query56 = "SELECT * FROM cs_memo WHERE year = '$cs_year' AND month = '$cs_month' AND week = '$goweek' AND businessid = '$businessid'";
			$result56 = Treat_DB_ProxyOld::query( $query56 );
			$num56 = Treat_DB_ProxyOld::mysql_numrows( $result56 );
			//mysql_close();

			$memo = @Treat_DB_ProxyOld::mysql_result( $result56, 0, 'memo' );

			fwrite($fh2, "<tr><td colspan=11><font color=blue>$memo</font></td></tr>");
			fwrite($fh4, "<tr><td colspan=11><font color=blue>$memo</font></td></tr>");
			////////////////////////////////////////////////

			fwrite($fh3, "</td></tr><tr height=3><td colspan=11 bgcolor=#E8E7E7></td></tr>");
			fwrite($fh3, "</table></html>");
			$message7="";
			fclose($fh3);

			//////UNIT EMAIL///////////////////////////////////////////////
			echo "Unit Email...<br>";  
			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( mysql_error());
			$query5 = "SELECT email FROM login WHERE (businessid = '$businessid' && receive_kpi < '2') || (busid2 = '$businessid' && receive_kpi < '1') || (busid3 = '$businessid' && receive_kpi < '1') || (busid4 = '$businessid' && receive_kpi < '1') || (busid5 = '$businessid' && receive_kpi < '1') || (busid6 = '$businessid' && receive_kpi < '1') || (busid7 = '$businessid' && receive_kpi < '1') || (busid8 = '$businessid' && receive_kpi < '1') || (busid9 = '$businessid' && receive_kpi < '1') || (busid10 = '$businessid' && receive_kpi < '1')";
			$result5 = Treat_DB_ProxyOld::query( $query5 );
			$num5 = Treat_DB_ProxyOld::mysql_numrows( $result5 );
			//mysql_close();

			$fileatt = "kpi/comkpi.$businessid.htm"; // Path to the file 
			$fileatt_type = "application/octet-stream"; // File Type 
			$fileatt_name = "KPI.htm"; // Filename that will be used for the file as the attachment 

			$email_from = "support@treatamerica.com"; // Who the email is from 
			$email_subject = "Weekly KPI Exceptions"; // The Subject of the email 
			$email_txt = "Weekly KPI's. Please view attachment."; // Message that the email has in it 

			$headers = "From: ".$email_from; 

			$file = fopen($fileatt,'rb'); 
			$data = fread($file,filesize($fileatt)); 
			fclose($file); 

			$semi_rand = md5(time()); 
			$mime_boundary = "==Multipart_Boundary_x{$semi_rand}x"; 

			$headers .= "\nMIME-Version: 1.0\n" . 
			"Content-Type: multipart/mixed;\n" . 
			" boundary=\"{$mime_boundary}\""; 

			$email_message .= "This is a multi-part message in MIME format.\n\n" . 
			"--{$mime_boundary}\n" . 
			"Content-Type:text/html; charset=\"iso-8859-1\"\n" . 
			"Content-Transfer-Encoding: 7bit\n\n" . 
			$email_message . "\n\n"; 

			$data = chunk_split(base64_encode($data)); 

			$email_message .= "--{$mime_boundary}\n" . 
			"Content-Type: {$fileatt_type};\n" . 
			" name=\"{$fileatt_name}\"\n" . 
			//"Content-Disposition: attachment;\n" . 
			//" filename=\"{$fileatt_name}\"\n" . 
			"Content-Transfer-Encoding: base64\n\n" . 
			$data . "\n\n" . 
			"--{$mime_boundary}--\n"; 

			$num5--;
			while ($num5>=0){
				echo "Sending email...<br>";

				$to = @Treat_DB_ProxyOld::mysql_result( $result5, $num5, 'email' );

				if ($update==1){}
				else{$ok = @mail($to, $email_subject, $email_message, $headers);} 

				if($ok) { 
					echo "$to emailed...<br>"; 
				} else { 
					echo "Sorry but the email could not be sent.<br>";}

				$num5--;
			}
			$email_message="";
			$myFile3 = DIR_COMKPI . '/comkpi3.htm';
			$fh3 = fopen($myFile3, 'w') or die("can't open file");
			fwrite($fh3, "");
			fclose($fh3);

			///////////////////////////////////////////////////////////////
		}

		$error1=0;$error2=0;$error3=0;$error4=0;$error5=0;$error6=0;

		echo "Figured Exceptions & Sent Emails...<br>";
		/////////WRITE TO FILE///////////////////////////////////////////////////////////////////////////////////////////

		fwrite($fh, $message);
		fwrite($fh9, $newbudget);

		$num99--;   
	}

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( mysql_error());
	$query99 = "SELECT date FROM com_kpi WHERE companyid = '$companyid' AND date = '$startdate'";
	$result99 = Treat_DB_ProxyOld::query( $query99 );
	$num99 = Treat_DB_ProxyOld::mysql_numrows( $result99 );
	//mysql_close();

	if ($num99==0){
		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( mysql_error());
		$query99 = "INSERT INTO com_kpi (companyid,sales,cos,labor,toe,pl,date,sales_bgt,cos_bgt,lbr_bgt,toe_bgt,pl_bgt) VALUES ('$companyid','$commtdsales','$commtdcos','$commtdlbr','$commtdtoe','$commtdpl','$startdate','$commtdsalesbgt','$commtdcosbgt','$commtdlbrbgt','$commtdtoebgt','$commtdplbgt')";
		$result99 = Treat_DB_ProxyOld::query( $query99 );
		//mysql_close();
	}
	else {
		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( mysql_error());
		$query99 = "UPDATE com_kpi SET sales = '$commtdsales',cos = '$commtdcos',labor = '$commtdlbr',toe = '$commtdtoe',pl = '$commtdpl',sales_bgt = '$commtdsalesbgt',cos_bgt = '$commtdcosbgt',lbr_bgt = '$commtdlbrbgt',toe_bgt = '$commtdtoebgt',pl_bgt = '$commtdplbgt' WHERE date = '$startdate' AND companyid = '$companyid'";
		$result99 = Treat_DB_ProxyOld::query( $query99 );
		//mysql_close();
	}

	$comcos=round($comcos/$comsales*100,1);
	$comlbr=round($comlbr/$comsales*100,1);
	$comtoe=round($comtoe/$comsales*100,1);
	$comcosbgt=round($comcosbgt/$comsalesbgt*100,1);
	$comlbrbgt=round($comlbrbgt/$comsalesbgt*100,1);
	$comtoebgt=round($comtoebgt/$comsalesbgt*100,1);
	$commtdcos=round($commtdcos/$commtdsales*100,1);
	$commtdlbr=round($commtdlbr/$commtdsales*100,1);
	$commtdtoe=round($commtdtoe/$commtdsales*100,1);
	$commtdcosbgt=round($commtdcosbgt/$commtdsalesbgt*100,1);
	$commtdlbrbgt=round($commtdlbrbgt/$commtdsalesbgt*100,1);
	$commtdtoebgt=round($commtdtoebgt/$commtdsalesbgt*100,1);
	$combgtcosprcnt=round($comtotcosbgt/$comtotsalesbgt*100,1);
	$combgtlbrprcnt=round($comtotlbrbgt/$comtotsalesbgt*100,1);
	$combgttoeprcnt=round($comtottoebgt/$comtotsalesbgt*100,1);
	$combgtplprcnt=round($comtotplbgt/$comtotsalesbgt*100,1);

	$comsales=number_format($comsales);
	$comsalesbgt=number_format($comsalesbgt);
	$commtdsales=number_format($commtdsales);
	$commtdsalesbgt=number_format($commtdsalesbgt);
	$compl=number_format($compl);
	$complbgt=number_format($complbgt);
	$commtdpl=number_format($commtdpl);
	$commtdplbgt=number_format($commtdplbgt);
	$comtotplbgt=number_format($comtotplbgt);
	$comtotsalesbgt=number_format($comtotsalesbgt);
	$comtotcosbgt=number_format($comtotcosbgt);
	$comtotlbrbgt=number_format($comtotlbrbgt);
	$comtottoebgt=number_format($comtottoebgt);

	fwrite($fh, "<tr><td colspan=11 bgcolor=#CCCCFF><center><b>$companyname</b></td></tr>");
	fwrite($fh, "<tr><td colspan=2 bgcolor=#E8E7E7><center><font color=black>Last Week</td><td colspan=2 bgcolor=#E8E7E7><center><font color=black>LW Bdgt</td><td colspan=2 bgcolor=#E8E7E7><center><font color=black>Month to Date</td><td colspan=2 bgcolor=#E8E7E7><center><font color=black>MTD Bdgt</td><td bgcolor=#E8E7E7 colspan=3><center>Total Month Bdgt</center></td></tr>");
	fwrite($fh, "<tr><td align=right>SALES:</td><td align=right>$$comsales</td><td align=right></td><td align=right>$$comsalesbgt</td><td align=right></td><td align=right>$$commtdsales</td><td align=right></td><td align=right>$$commtdsalesbgt</td><td align=right></td><td align=right>$$comtotsalesbgt</td><td align=right></td></tr>");
	fwrite($fh, "<tr><td align=right>COS:</td><td align=right>$comcos%</td><td align=right></td><td align=right>$combgtcosprcnt%</td><td align=right></td><td align=right>$commtdcos%</td><td align=right></td><td align=right>$combgtcosprcnt%</td><td align=right></td><td align=right>$$comtotcosbgt</td><td align=right>$combgtcosprcnt%</td></tr>");
	fwrite($fh, "<tr><td align=right>LABOR:</td><td align=right>$comlbr%</td><td align=right></td><td align=right>$combgtlbrprcnt%</td><td align=right></td><td align=right>$commtdlbr%</td><td align=right></td><td align=right>$combgtlbrprcnt%</td><td align=right></td><td align=right>$$comtotlbrbgt</td><td align=right>$combgtlbrprcnt%</td></tr>");
	fwrite($fh, "<tr><td align=right>TOE:</td><td align=right>$comtoe%</td><td align=right></td><td align=right>$combgttoeprcnt%</td><td align=right></td><td align=right>$commtdtoe%</td><td align=right></td><td align=right>$combgttoeprcnt%</td><td align=right></td><td align=right>$$comtottoebgt</td><td align=right>$combgttoeprcnt%</td></tr>");
	fwrite($fh, "<tr><td align=right>P&L:</td><td align=right>$$compl</td><td align=right></td><td align=right>$$complbgt</td><td align=right></td><td align=right>$$commtdpl</td><td align=right></td><td align=right>$$commtdplbgt</td><td align=right></td><td align=right>$$comtotplbgt</td><td align=right>$combgtplprcnt%</td></tr>");
	fwrite($fh, "</table></html>");
	fwrite($fh9, "</table></html>");
	fwrite($fh2, "</table></html>");
	fclose($fh);
	fclose($fh2);

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( mysql_error());
	$query5 = "SELECT email FROM login WHERE security_level > '7' && receive_kpi < '1'";
	$result5 = Treat_DB_ProxyOld::query( $query5 );
	$num5 = Treat_DB_ProxyOld::mysql_numrows( $result5 );
	//mysql_close();

	$message2="<table>$message2</table>";
	$to_email="";

	$email_message="";
	$fileatt = DIR_COMKPI . '/comkpi1.htm'; // Path to the file 
	$fileatt_type = "application/octet-stream"; // File Type 
	$fileatt_name = "KPI.htm"; // Filename that will be used for the file as the attachment 

	$email_from = "support@treatamerica.com"; // Who the email is from 
	$email_subject = "Weekly KPI Exceptions"; // The Subject of the email 
	$email_txt = "Weekly KPI's. Please view attachment..."; // Message that the email has in it 

	$headers = "From: ".$email_from; 

	$file = fopen($fileatt,'rb'); 
	$data = fread($file,filesize($fileatt)); 
	fclose($file); 

	$semi_rand = md5(time()); 
	$mime_boundary = "==Multipart_Boundary_x{$semi_rand}x"; 

	$headers .= "\nMIME-Version: 1.0\n" . 
	"Content-Type: multipart/mixed;\n" . 
	" boundary=\"{$mime_boundary}\""; 

	$email_message .= "This is a multi-part message in MIME format.\n\n" . 
	"--{$mime_boundary}\n" . 
	"Content-Type:text/html; charset=\"iso-8859-1\"\n" . 
	"Content-Transfer-Encoding: 7bit\n\n" . 
	$email_message . "\n\n"; 

	$data = chunk_split(base64_encode($data)); 

	$email_message .= "--{$mime_boundary}\n" . 
	"Content-Type: {$fileatt_type};\n" . 
	" name=\"{$fileatt_name}\"\n" . 
	//"Content-Disposition: attachment;\n" . 
	//" filename=\"{$fileatt_name}\"\n" . 
	"Content-Transfer-Encoding: base64\n\n" . 
	$data . "\n\n" . 
	"--{$mime_boundary}--\n"; 

	$num5--;
	while ($num5>=0){
		echo "Sending email...<br>";

		$to = @Treat_DB_ProxyOld::mysql_result( $result5, $num5, 'email' );

		if ($update==1){}
		else {$ok = @mail($to, $email_subject, $email_message, $headers);}

		if($ok) { 
			echo "$to emailed...<br>"; 
		} else { 
			echo "Sorry but the email could not be sent.<br>";}

		$num5--;
	}

	echo "<br><b><font color=green>Emails Sent...</font></b><p>";
	echo "<b><font color=red>KPI's Finished, You may close this window.</font></b>";

	for ($counter=1;$counter<1000;$counter++){
		$file_delete="kpi/comkpi.$counter.htm";
		unlink($file_delete); 
	}
	//mysql_close();
	//////END TABLE////////////////////////////////////////////////////////////////////////////////////

}
?>
