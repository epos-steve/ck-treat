<?php

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+3);
        if ($diff > 0 && $diff < '0.01'){$diff="0.01";}
        elseif($diff < 0 && $diff > '-0.01'){$diff="-0.01";}
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      if ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='32')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='08' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      $day=$day-31;  
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";} 
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}


if ( !defined('DOC_ROOT'))
{
	define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
}
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');
$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");
$creditamount="creditamount";
$creditidnty="creditidnty";
$creditdate="creditdate";
$debitamount="debitamount";
$debitidnty="debitidnty";
$debitdate="debitdate";
$quatertotal=0;
$monthtotal=0;
$total=0;
$counter=0;
$findme='.';
$double='.00';
$single='0';
$double2='00';

/*$user = isset($_COOKIE["usercook"])?$_COOKIE["usercook"]:'';
$pass = isset($_COOKIE["passcook"])?$_COOKIE["passcook"]:'';
$view = isset($_COOKIE["viewcook"])?$_COOKIE["viewcook"]:'';
$date1 = isset($_COOKIE["date1cook"])?$_COOKIE["date1cook"]:'';
$date2 = isset($_COOKIE["date2cook"])?$_COOKIE["date2cook"]:'';
$businessid = isset($_GET["bid"])?$_GET["bid"]:'';
$companyid = isset($_GET["cid"])?$_GET["cid"]:'';
$view1 = isset($_GET["view1"])?$_GET["view1"]:'';
$itemeditid = isset($_GET["mid"])?$_GET["mid"]:'';*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook','');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook','');
$view = \EE\Controller\Base::getSessionCookieVariable('viewcook','');
$date1 = \EE\Controller\Base::getSessionCookieVariable('date1cook','');
$date2 = \EE\Controller\Base::getSessionCookieVariable('date2cook','');
$businessid = \EE\Controller\Base::getGetVariable('bid','');
$companyid = \EE\Controller\Base::getGetVariable('cid','');
$view1 = \EE\Controller\Base::getGetVariable('view1','');
$itemeditid = \EE\Controller\Base::getGetVariable('mid','');

if ($businessid==""&&$companyid==""){
   /*$businessid = isset($_POST["businessid"])?$_POST["businessid"]:'';
   $companyid = isset($_POST["companyid"])?$_POST["companyid"]:'';
   $view1 = isset($_POST["view1"])?$_POST["view1"]:'';
   $date1 = isset($_POST["date1"])?$_POST["date1"]:'';
   $date2 = isset($_POST["date2"])?$_POST["date2"]:'';
   $findinv = isset($_POST["findinv"])?$_POST["findinv"]:'';*/
	
	$businessid = \EE\Controller\Base::getPostVariable('businessid','');
	$companyid = \EE\Controller\Base::getPostVariable('companyid','');
	$view1 = \EE\Controller\Base::getPostVariable('view1','');
	$date1 = \EE\Controller\Base::getPostVariable('date1','');
	$date2 = \EE\Controller\Base::getPostVariable('date2','');
	$findinv = \EE\Controller\Base::getPostVariable('findinv','');
}

//$curmenu = isset($_COOKIE["curmenu"])?$_COOKIE["curmenu"]:'';
$curmenu = \EE\Controller\Base::getSessionCookieVariable('curmenu','');

if ($curmenu==""){
   $curmenu=1;
}

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=Treat_DB_ProxyOld::mysql_numrows($result);
//mysql_close();

$security_level=@Treat_DB_ProxyOld::mysql_result($result,0,"security_level");
$security=@Treat_DB_ProxyOld::mysql_result($result,0,"security");
$bid=@Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
$cid=@Treat_DB_ProxyOld::mysql_result($result,0,"companyid");

if ($num != 1 || ($security_level==1 && ($bid != $businessid || $cid != $companyid)) || ($security_level > 1 AND $cid != $companyid) || $user == "" || $pass == "")
{
    echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
	$query = "SELECT * FROM security WHERE securityid = '$security'";
	$result = Treat_DB_ProxyOld::query($query);
	
	$sec_item_list=@Treat_DB_ProxyOld::mysql_result($result,0,"item_list");
	$sec_nutrition=@Treat_DB_ProxyOld::mysql_result($result,0,"nutrition");
	
	if($sec_item_list<1&&$sec_nutrition<1){
		$location="businesstrack.php";
		header('Location: ./' . $location);
	}
	elseif($sec_item_list<1&&$sec_nutrition>0){
		$location="caternutrition.php?cid=$companyid";
		header('Location: ./' . $location);
	}
	
?>
<head>
<SCRIPT LANGUAGE=javascript><!--
function delconfirm(){return confirm('Are you sure you want to delete this group? Any items assigned to this group will not be viewed by online customers until they are assigned a new group.');}
// --></SCRIPT>

<SCRIPT LANGUAGE=javascript><!--
function delconfirm2(){return confirm('Are you sure you want to delete this?');}
// --></SCRIPT>
</head>
<?

    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%><tr><td colspan=2>";
    echo "<a href=businesstrack.php><img src=logo.jpg border=0 height=43 width=205></a><p></td></tr>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM company WHERE companyid = '$companyid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $companyname=@Treat_DB_ProxyOld::mysql_result($result,0,"companyname");

    //echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";
    echo "<tr bgcolor=#CCCCFF><td colspan=2><font size=4><b>Menu Setup - $companyname</b></font></td>";
    echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";
    echo "<tr><td colspan=2><a href=catermenu.php?bid=$bid&cid=$cid><font color=blue>Menu Items</font></a> :: Menu Groups :: <a href=catertype.php?cid=$companyid><font color=blue>Menu Types</font></a> :: <a href=caternutrition.php?cid=$companyid><font color=blue>Nutrition</font></a></td></tr>";
    echo "</table></center><p>";

/////////////////////////////////////////////////////////////////////////////////
    echo "<a name='item'></a>";
    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%>";
    //echo "<tr bgcolor=black><td height=1 colspan=4></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td height=1 colspan=4><form action=placemenu.php?cid=$companyid method=post><input type=hidden name=direct value=1><b>Menu Groups for</b> <select name=curmenu>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM menu_type WHERE companyid = '$companyid' ORDER BY menu_typeid DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    $num--;
    $curtype=0;
    while ($num>=0){
       $menu_typename=@Treat_DB_ProxyOld::mysql_result($result,$num,"menu_typename");
       $menu_typeid=@Treat_DB_ProxyOld::mysql_result($result,$num,"menu_typeid");
       if ($curmenu==$menu_typeid){$showmenu="SELECTED"; $curtype=@Treat_DB_ProxyOld::mysql_result($result,$num,"type");}
       else{$showmenu="";}
       echo "<option value=$menu_typeid $showmenu>$menu_typename</option>";
       $num--;
    }

    echo "</select><input type=submit value='GO'></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td colspan=4></form></td></tr>";
    echo "<tr bgcolor=black><td height=1 colspan=4></td></tr>";
    echo "<tr bgcolor=#CCCCCC><td height=1 colspan=4></td></tr>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM menu_groups WHERE companyid = '$companyid' AND menu_typeid = '$curmenu' ORDER BY orderid DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    $lastgroup="";
    $num--;
    while ($num>=0){
       $menu_group_id=@Treat_DB_ProxyOld::mysql_result($result,$num,"menu_group_id");
       $groupname=@Treat_DB_ProxyOld::mysql_result($result,$num,"groupname");
       $deleted=@Treat_DB_ProxyOld::mysql_result($result,$num,"deleted");

       $showedit="<a href=catergroup.php?cid=$companyid&bid=$businessid&view1=edit&mid=$menu_group_id#item><img border=0 src=edit.gif height=16 width=16 alt='EDIT GROUP'></a>";
       $deletegroup="<a href=catergroupedit.php?cid=$companyid&bid=$businessid&edit=3&editid=$menu_group_id onclick='return delconfirm()'><img border=0 src=delete.gif height=16 width=16 alt='DELETE GROUP'></a>";
       if ($deleted==0){$showdelete="<a href=catergroupedit.php?cid=$companyid&bid=$businessid&edit=2&editid=$menu_group_id><img border=0 src=on.gif height=15 width=15 alt='ACTIVE'></a>";}
       else{$showdelete="<a href=catergroupedit.php?cid=$companyid&bid=$businessid&edit=2&editid=$menu_group_id&on=on><img border=0 src=off.gif height=15 width=15 alt='INACTIVE'></a>";}

       echo "<tr bgcolor='white' onMouseOver=this.bgColor='#999999' onMouseOut=this.bgColor='white'><td width=25%>$groupname</td><td width=4%>$showdelete</td><td width=4%>$showedit</td><td width=4%>$deletegroup</td></tr>";
       echo "<tr bgcolor=#CCCCCC><td height=1 colspan=4></td></tr>";

       $num--;
    }
  
    echo "<tr bgcolor=black><td height=1 colspan=4></td></tr><tr><td colspan=4>";

    if ($view1=="edit"){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM menu_groups WHERE menu_group_id = '$itemeditid'";
       $result2 = Treat_DB_ProxyOld::query($query2);
       //mysql_close();

       $editname=@Treat_DB_ProxyOld::mysql_result($result2,0,"groupname");
       $description=@Treat_DB_ProxyOld::mysql_result($result2,0,"description");
       $grouptypeid=@Treat_DB_ProxyOld::mysql_result($result2,0,"menu_group_id");
       $orderid=@Treat_DB_ProxyOld::mysql_result($result2,0,"orderid");

       echo "<center><table width=100% bgcolor='#E8E7E7'><tr><td colspan=2><a name='item'></a><form action=catergroupedit.php method=post></td></tr>";
       echo "<tr><td colspan=2><INPUT TYPE=hidden name=companyid value=$companyid><INPUT TYPE=hidden name=businessid value=$businessid><INPUT TYPE=hidden name=editid value=$itemeditid><INPUT TYPE=hidden name=edit value=1><b><font color=red>*</font><u>Edit Menu Group</u></b></td></tr>";
       echo "<tr><td align=right width=10%>Group Name:</td><td><INPUT TYPE=text name=item_name value='$editname' size=20> Order: <input type=text size=2 name=orderid value='$orderid'></td></tr>";
       echo "<tr><td align=right>Description:</td><td><textarea name=description rows=3 cols=50>$description</textarea></td></tr>";
       echo "<tr><td></td><td><INPUT TYPE=submit value='Save'></form></td></tr></table></center>";
    }
    else {
       echo "<center><table width=100% bgcolor='#E8E7E7'><tr><td colspan=2><form action=catergroupedit.php method=post></td></tr>";
       echo "<tr><td colspan=2><INPUT TYPE=hidden name=companyid value=$companyid><INPUT TYPE=hidden name=businessid value=$businessid><b><u>Add Menu Group</u></b></td></tr>";
       echo "<tr><td align=right width=10%>Group Name:</td><td><INPUT TYPE=text name=item_name size=20> Order: <input type=text size=2 name=orderid></td></tr>";
       echo "<tr><td align=right>Description:</td><td><textarea name=description rows=3 cols=50></textarea></td></tr>";
       //echo "<tr><td align=right><input type=checkbox name=loadpic></td><td>Upload Image</td></tr>";
       //echo "<tr><td align=right>Picture File:</td><td><input type=file name=image size=40></td></tr>";
       echo "<tr><td></td><td><INPUT TYPE=submit value='Add Group'></form></td></tr></table></center>";
    }

    echo "</td></tr>";
//////START PORTIONS////////////////////////////////////////////////////////////////////////////////////
if($curtype==1){
    echo "<tr bgcolor=black><td height=1 colspan=4></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td colspan=4><a name='item2'></a><b>PORTIONS</b></td></tr>";
    echo "<tr bgcolor=black><td height=1 colspan=4></td></tr>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM menu_portion WHERE menu_typeid = '$curmenu' ORDER BY orderid DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    $lastgroup="";
    $num--;
    while ($num>=0){
       $menu_portionname=@Treat_DB_ProxyOld::mysql_result($result,$num,"menu_portionname");
       $menu_portionid=@Treat_DB_ProxyOld::mysql_result($result,$num,"menu_portionid");

       $showedit="<a href=catergroup.php?cid=$companyid&bid=$businessid&view1=edit2&mid=$menu_portionid#item2><img border=0 src=edit.gif height=16 width=16 alt='EDIT PORTION'></a>";
       $deletegroup="<a href=caterportionedit.php?cid=$companyid&bid=$businessid&edit=3&editid=$menu_portionid onclick='return delconfirm2()'><img border=0 src=delete.gif height=16 width=16 alt='DELETE PORTION'></a>";

       echo "<tr bgcolor='white' onMouseOver=this.bgColor='#999999' onMouseOut=this.bgColor='white'><td width=25%>$menu_portionname</td><td width=4%></td><td width=4%>$showedit</td><td width=4%>$deletegroup</td></tr>";
       echo "<tr bgcolor=#CCCCCC><td height=1 colspan=4></td></tr>";

       $num--;
    }
  
    echo "<tr bgcolor=black><td height=1 colspan=4></td></tr><tr><td colspan=4>";

    if ($view1=="edit2"){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM menu_portion WHERE menu_portionid = '$itemeditid'";
       $result2 = Treat_DB_ProxyOld::query($query2);
       //mysql_close();

       $editname=@Treat_DB_ProxyOld::mysql_result($result2,0,"menu_portionname");
       $grouptypeid=@Treat_DB_ProxyOld::mysql_result($result2,0,"menu_group_id");
       $orderid=@Treat_DB_ProxyOld::mysql_result($result2,0,"orderid");

       echo "<center><table width=100% bgcolor='#E8E7E7'><tr><td colspan=2><a name='item2'></a><form action=caterportionedit.php method=post></td></tr>";
       echo "<tr><td colspan=2><INPUT TYPE=hidden name=companyid value=$companyid><INPUT TYPE=hidden name=businessid value=$businessid><INPUT TYPE=hidden name=editid value=$itemeditid><INPUT TYPE=hidden name=edit value=1><b><font color=red>*</font><u>Edit Portion</u></b></td></tr>";
       echo "<tr><td align=right width=20%>Portion Name:</td><td><INPUT TYPE=text name=item_name value='$editname' size=20> Order: <input type=text size=3 name=order value='$orderid'></td></tr>";
       echo "<tr><td></td><td><INPUT TYPE=submit value='Save'></form></td></tr></table></center>";
    }
    else {
       echo "<center><table width=100% bgcolor='#E8E7E7'><tr><td colspan=2><form action=caterportionedit.php method=post></td></tr>";
       echo "<tr><td colspan=2><INPUT TYPE=hidden name=companyid value=$companyid><INPUT TYPE=hidden name=businessid value=$businessid><b><u>Add Portion</u></b></td></tr>";
       echo "<tr><td align=right width=20%>Portion Name:</td><td><INPUT TYPE=text name=item_name size=20> Order: <input type=text size=3 name=order> <INPUT TYPE=submit value='Add'></td><td></form></td></tr>";
       echo "</table></center>";
    }

    echo "</td></tr>";
//////START DAYPARTS////////////////////////////////////////////////////////////////////////////////////

    echo "<tr bgcolor=black><td height=1 colspan=4></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td colspan=4><a name='item3'></a><b>DAYPARTS</b></td></tr>";
    echo "<tr bgcolor=black><td height=1 colspan=4></td></tr>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM menu_daypart WHERE menu_typeid = '$curmenu' ORDER BY orderid DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    $lastgroup="";
    $num--;
    while ($num>=0){
       $menu_daypartname=@Treat_DB_ProxyOld::mysql_result($result,$num,"menu_daypartname");
       $menu_daypartid=@Treat_DB_ProxyOld::mysql_result($result,$num,"menu_daypartid");
       $menu_deliver=@Treat_DB_ProxyOld::mysql_result($result,$num,"deliver_charge");

       if ($menu_deliver==1){$showdeliver="<font color=red>*</font>";}
       else{$showdeliver="";}

       $showedit="<a href=catergroup.php?cid=$companyid&bid=$businessid&view1=edit3&mid=$menu_daypartid#item2><img border=0 src=edit.gif height=16 width=16 alt='EDIT DAYPART'></a>";
       $deletegroup="<a href=caterdaypartedit.php?cid=$companyid&bid=$businessid&edit=3&editid=$menu_daypartid onclick='return delconfirm2()'><img border=0 src=delete.gif height=16 width=16 alt='DELETE DAYPART'></a>";

       echo "<tr bgcolor='white' onMouseOver=this.bgColor='#999999' onMouseOut=this.bgColor='white'><td width=25%>$menu_daypartname$showdeliver</td><td width=4%></td><td width=4%>$showedit</td><td width=4%>$deletegroup</td></tr>";
       echo "<tr bgcolor=#CCCCCC><td height=1 colspan=4></td></tr>";

       $num--;
    }
  
    echo "<tr bgcolor=black><td height=1 colspan=4></td></tr><tr><td colspan=4>";

    if ($view1=="edit3"){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM menu_daypart WHERE menu_daypartid = '$itemeditid'";
       $result2 = Treat_DB_ProxyOld::query($query2);
       //mysql_close();

       $editname=@Treat_DB_ProxyOld::mysql_result($result2,0,"menu_daypartname");
       $grouptypeid=@Treat_DB_ProxyOld::mysql_result($result2,0,"menu_daypartid");
       $orderid=@Treat_DB_ProxyOld::mysql_result($result2,0,"orderid");
       $deliver_charge=@Treat_DB_ProxyOld::mysql_result($result2,0,"deliver_charge");
       $breakfast=@Treat_DB_ProxyOld::mysql_result($result2,0,"breakfast");

       if ($deliver_charge==1){$showcheck="CHECKED";}
       else{$showcheck="";}
       if ($breakfast==1){$showcheck2="CHECKED";}
       else{$showcheck="";}

       echo "<center><table width=100% bgcolor='#E8E7E7'><tr><td colspan=2><a name='item3'></a><form action=caterdaypartedit.php method=post></td></tr>";
       echo "<tr><td colspan=2><INPUT TYPE=hidden name=companyid value=$companyid><INPUT TYPE=hidden name=businessid value=$businessid><INPUT TYPE=hidden name=editid value=$itemeditid><INPUT TYPE=hidden name=edit value=1><b><font color=red>*</font><u>Edit Daypart</u></b></td></tr>";
       echo "<tr><td align=right width=20%>Daypart Name:</td><td><INPUT TYPE=text name=item_name value='$editname' size=20> Order: <input type=text size=3 name=order value='$orderid'> <input type=checkbox name=deliver_charge value=1 $showcheck> Counts toward Delivery Charge <input type=checkbox name=breakfast value=1 $showcheck2> Breakfast</td></tr>";
       echo "<tr><td></td><td><INPUT TYPE=submit value='Save'></form></td></tr></table></center>";
    }
    else {
       echo "<center><table width=100% bgcolor='#E8E7E7'><tr><td colspan=2><form action=caterdaypartedit.php method=post></td></tr>";
       echo "<tr><td colspan=2><INPUT TYPE=hidden name=companyid value=$companyid><INPUT TYPE=hidden name=businessid value=$businessid><b><u>Add Daypart</u></b></td></tr>";
       echo "<tr><td align=right width=20%>Daypart Name:</td><td><INPUT TYPE=text name=item_name size=20> Order: <input type=text size=3 name=order> <input type=checkbox name=deliver_charge value=1> Counts toward Delivery Charge <input type=checkbox name=breakfast value=1 $showcheck2> Breakfast <INPUT TYPE=submit value='Add'></td><td></form></td></tr>";
       echo "</table></center>";
    }

    echo "</td></tr>";
}
//////START PRICE GROUPS////////////////////////////////////////////////////////////////////////////////////
if($curtype==2){
    echo "<tr bgcolor=black><td height=1 colspan=4></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td colspan=4><a name='item4'></a><b>PRICE GROUP</b></td></tr>";
    echo "<tr bgcolor=black><td height=1 colspan=4></td></tr>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM menu_pricegroup WHERE menu_typeid = '$curmenu' ORDER BY menu_groupid,orderid DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    $lastgroup="";
    $num--;
    while ($num>=0){
       $menu_pricegroupname=@Treat_DB_ProxyOld::mysql_result($result,$num,"menu_pricegroupname");
       $menu_pricegroupid=@Treat_DB_ProxyOld::mysql_result($result,$num,"menu_pricegroupid");
       $menu_groupid=@Treat_DB_ProxyOld::mysql_result($result,$num,"menu_groupid");
       $account=@Treat_DB_ProxyOld::mysql_result($result,$num,"account");
       $price=@Treat_DB_ProxyOld::mysql_result($result,$num,"price");
       $percent=@Treat_DB_ProxyOld::mysql_result($result,$num,"percent");

       $showedit="<a href=catergroup.php?cid=$companyid&bid=$businessid&view1=edit4&mid=$menu_pricegroupid#item4><img border=0 src=edit.gif height=16 width=16 alt='EDIT PRICE GROUP'></a>";
       $deletegroup="<a href=caterpricegroupedit.php?cid=$companyid&bid=$businessid&edit=3&editid=$menu_pricegroupid onclick='return delconfirm2()'><img border=0 src=delete.gif height=16 width=16 alt='DELETE PRICE GROUP'></a>";

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT groupname FROM menu_groups WHERE menu_group_id = '$menu_groupid'";
       $result2 = Treat_DB_ProxyOld::query($query2);
       //mysql_close();

       $groupname=@Treat_DB_ProxyOld::mysql_result($result2,0,"groupname");
       if($percent==1){
           $price="$price%";
       }
       else{
            $price=money($price);
            $price="$$price";
       }

       echo "<tr bgcolor='white' onMouseOver=this.bgColor='#999999' onMouseOut=this.bgColor='white'><td width=25%>$menu_pricegroupname - <i>$price</i></td><td width=10%>$account - $groupname</td><td width=4%>$showedit</td><td width=4%>$deletegroup</td></tr>";
       echo "<tr bgcolor=#CCCCCC><td height=1 colspan=4></td></tr>";

       $num--;
    }
  
    echo "<tr bgcolor=black><td height=1 colspan=4></td></tr><tr><td colspan=4>";

    if ($view1=="edit4"){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM menu_pricegroup WHERE menu_pricegroupid = '$itemeditid'";
       $result2 = Treat_DB_ProxyOld::query($query2);
       //mysql_close();

       $editname=@Treat_DB_ProxyOld::mysql_result($result2,0,"menu_pricegroupname");
       $grouptypeid=@Treat_DB_ProxyOld::mysql_result($result2,0,"menu_pricegroupid");
       $orderid=@Treat_DB_ProxyOld::mysql_result($result2,0,"orderid");
       $thisgroupid=@Treat_DB_ProxyOld::mysql_result($result2,0,"menu_groupid");
       $price=@Treat_DB_ProxyOld::mysql_result($result2,0,"price");
       $percent=@Treat_DB_ProxyOld::mysql_result($result2,0,"percent");
       $account=@Treat_DB_ProxyOld::mysql_result($result2,0,"account");

       if($percent==1){$selme="SELECTED";}

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM menu_groups WHERE menu_typeid = '$curmenu' ORDER BY groupname DESC";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
       //mysql_close();

       echo "<center><table width=100% bgcolor='#E8E7E7'><tr><td colspan=2><a name='item4'></a><form action=caterpricegroupedit.php method=post></td></tr>";
       echo "<tr><td colspan=2><INPUT TYPE=hidden name=companyid value=$companyid><INPUT TYPE=hidden name=businessid value=$businessid><INPUT TYPE=hidden name=editid value=$itemeditid><INPUT TYPE=hidden name=edit value=1><b><font color=red>*</font><u>Edit Price Group</u></b></td></tr>";
       echo "<tr><td align=right width=20%>Price Group Name:</td><td><INPUT TYPE=text name=item_name value='$editname' size=20> <select name=groupid>";

       $num2--;
       while($num2>=0){
          $menu_groupid=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"menu_group_id");
          $menu_groupname=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"groupname");
          if ($thisgroupid==$menu_groupid){$sel="SELECTED";}
          else{$sel="";}

          echo "<option value=$menu_groupid $sel>$menu_groupname</option>";

          $num2--;
       }

       echo "</select> Price: <input type=text size=3 name=price value='$price'><select name=percent><option value=0>$</option><option value=1 $selme>%</option></select> Order: <input type=text size=3 name=order value='$orderid'> Acct: <input type=text size=3 name=account value='$account'> </td></tr>";
       echo "<tr><td></td><td><INPUT TYPE=submit value='Save'></form></td></tr></table></center>";
    }
    else {
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM menu_groups WHERE menu_typeid = '$curmenu' ORDER BY groupname DESC";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
       //mysql_close();

       echo "<center><table width=100% bgcolor='#E8E7E7'><tr><td colspan=2><form action=caterpricegroupedit.php method=post></td></tr>";
       echo "<tr><td colspan=2><INPUT TYPE=hidden name=companyid value=$companyid><INPUT TYPE=hidden name=businessid value=$businessid><b><u>Add Price Group</u></b></td></tr>";
       echo "<tr><td align=right width=20%>Price Group Name:</td><td><INPUT TYPE=text name=item_name size=20> <select name=groupid>";

       $num2--;
       while($num2>=0){
          $menu_groupid=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"menu_group_id");
          $menu_groupname=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"groupname");

          echo "<option value=$menu_groupid>$menu_groupname</option>";

          $num2--;
       }

       echo "</select> Price: <input type=text size=3 name=price><select name=percent><option value=0>$</option><option value=1>%</option></select> Order: <input type=text size=3 name=order> Acct: <input type=text size=3 name=account> <INPUT TYPE=submit value='Add'></td><td></form></td></tr>";
       echo "</table></center>";
    }
    echo "</td></tr>";
    }
    echo "</table></center>";
//////END TABLE////////////////////////////////////////////////////////////////////////////////////

    echo "<br><FORM ACTION=businesstrack.php method=post>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<center><INPUT TYPE=submit VALUE=' Return to Main Page'></FORM></center>";
}
google_page_track();
?>