<?php

function money( $diff ) {
	$findme = '.';
	$double = '.00';
	$single = '0';
	$double2 = '00';
	$pos1 = strpos( $diff, $findme );
	$pos2 = strlen( $diff );
	if( $pos1 == "" ) {
		$diff = "$diff$double";
	}
	elseif( $pos2 - $pos1 == 2 ) {
		$diff = "$diff$single";
	}
	elseif( $pos2 - $pos1 == 1 ) {
		$diff = "$diff$double2";
	}
	else {}
	$dot = strpos( $diff, $findme );
	$diff = substr( $diff, 0, $dot + 4 );
	$diff = round( $diff, 2 );
	if( $diff > 0 && $diff <= 0.01 ) {
		$diff = "0.01";
	}
	elseif( $diff < 0 && $diff >= -0.01 ) {
		$diff = "-0.01";
	}
	$diff = substr( $diff, 0, $dot + 3 );
	$pos1 = strpos( $diff, $findme );
	$pos2 = strlen( $diff );
	if( $pos1 == "" ) {
		$diff = "$diff$double";
	}
	elseif( $pos2 - $pos1 == 2 ) {
		$diff = "$diff$single";
	}
	elseif( $pos2 - $pos1 == 1 ) {
		$diff = "$diff$double2";
	}
	else {}
	return $diff;
}

function writePaycorLine( $type, $data ) {
	$paycorFormat = array(
		'H' => array(
			array(
				'const' => 'PAYCOR01'
			),
			array(
				'var' => 'month', 'format' => '%02d/', 'len' => 3
			),
			array(
				'var' => 'day', 'format' => '%02d/', 'len' => 3
			),
			array(
				'var' => 'year', 'format' => '%04d', 'len' => 4
			),
			array( // this is not in Paycor's format, but was in our previous exports
				'const' => ' '
			),
			array(
				'var' => 'manuf', 'format' => '%-37s', 'len' => 37
			),
			array(
				'const' => 'N'
			),
			array(
				'var' => 'endMonth', 'format' => '%02d/', 'len' => 3
			),
			array(
				'var' => 'endDay', 'format' => '%02d/', 'len' => 3
			),
			array(
				'var' => 'endYear', 'format' => '%04d', 'len' => 4
			)
		),
		'D' => array(
			array(
				'var' => 'employee', 'format' => '%06d', 'len' => 6
			),
			array(
				'var' => 'rateCode', 'format' => '%1s', 'len' => 1
			),
			array(
				'var' => 'regular', 'format' => '%08d', 'len' => 8, 'mult' => 10000
			),
			array(
				'var' => 'overtime', 'format' => '%07d', 'len' => 7, 'mult' => 10000
			),
			array(
				'var' => 'coded', 'format' => '%09d', 'len' => 9, 'mult' => 10000
			),
			array(
				'var' => 'specialCode', 'format' => '%-10s', 'len' => 10
			),
			array(
				'var' => 'earnings1', 'format' => '%08d', 'len' => 8, 'mult' => 100
			),
			array(
				'var' => 'earnCode1', 'format' => '%-10s', 'len' => 10
			),
			array(
				'var' => 'earnings2', 'format' => '%08d', 'len' => 8, 'mult' => 100
			),
			array(
				'var' => 'earnCode2', 'format' => '%-10s', 'len' => 10
			),
			array(
				'var' => 'exceptionRate', 'format' => '%08d', 'len' => 8, 'mult' => 100000
			),
			array(
				'var' => 'exceptionDept', 'format' => '%014d', 'len' => 14
			),
			array(
				'var' => 'shift', 'format' => '%1d', 'len' => 1
			),
			array(
				'var' => 'frequency', 'format' => '%-2s', 'len' => 2
			),
			array(
				'var' => 'checkNo', 'format' => '%1d', 'len' => 1
			),
			array(
				'var' => 'grossReceipt', 'format' => '%08d', 'len' => 8, 'mult' => 100
			)
		),
		'T' => array(
			array(
				'var' => 'regular', 'format' => '%013.4f', 'len' => 13
			),
			array(
				'var' => 'overtime', 'format' => '%013.4f', 'len' => 13
			),
			array(
				'var' => 'coded', 'format' => '%013.4f', 'len' => 13
			),
			array(
				'var' => 'earnings', 'format' => '%011.2f', 'len' => 11
			)
		)
	);
	
	$format = $paycorFormat[$type];
	$line = $type;
	
	foreach( $format as $f ) {
		if( isset( $f['const'] ) )
			$line .= $f['const'];
		else {
			$var = $f['var'];
			
			if( isset( $data[$var] ) ) {
				$d = isset( $f['mult'] ) ? $data[$var] * $f['mult'] : $data[$var];
				$line .= sprintf( $f['format'], $d );
			}
			else {
				$line .= sprintf( '%' . $f['len'] . 's', ' ' );
			}
		}
	}
	
	$line .= "\r\n";
	return $line;
}


if ( !defined('DOC_ROOT'))
{
	define('DOC_ROOT', dirname(dirname(__FILE__)));
}
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');
$style = "text-decoration:none";
$day = date( "d" );
$year = date( "Y" );
$month = date( "m" );
$today = "$year-$month-$day";
if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
	header( 'Content-type: text/plain' );
}

$user = isset( $_COOKIE["usercook"] ) ? $_COOKIE["usercook"] : '';
$pass = isset( $_COOKIE["passcook"] ) ? $_COOKIE["passcook"] : '';
$businessid = isset( $_GET["bid"] ) ? $_GET["bid"] : '';
$companyid = isset( $_GET["cid"] ) ? $_GET["cid"] : '';
$newdate = isset( $_GET["newdate"] ) ? $_GET["newdate"] : '';
$date1 = isset( $_GET["date1"] ) ? $_GET["date1"] : '';
$date2 = isset( $_GET["date2"] ) ? $_GET["date2"] : '';

if( $businessid == "" && $companyid == "" ) {
	$businessid = isset( $_POST["businessid"] ) ? $_POST["businessid"] : '';
	$companyid = isset( $_POST["companyid"] ) ? $_POST["companyid"] : '';
	$date1 = isset( $_POST["date1"] ) ? $_POST["date1"] : '';
	$date2 = isset( $_POST["date2"] ) ? $_POST["date2"] : '';
}

if( $newdate != "" ) {
	$today = $newdate;
}
$todayname = dayofweek( $today );


//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query( $query );
$num = mysql_numrows( $result );
//mysql_close();

$loginid = @mysql_result( $result, 0, "loginid" );
$security_level = @mysql_result( $result, 0, "security_level" );
$mysecurity = @mysql_result( $result, 0, "security" );
$bid = @mysql_result( $result, 0, "businessid" );
$bid2 = @mysql_result( $result, 0, "busid2" );
$bid3 = @mysql_result( $result, 0, "busid3" );
$bid4 = @mysql_result( $result, 0, "busid4" );
$bid5 = @mysql_result( $result, 0, "busid5" );
$bid6 = @mysql_result( $result, 0, "busid6" );
$bid7 = @mysql_result( $result, 0, "busid7" );
$bid8 = @mysql_result( $result, 0, "busid8" );
$bid9 = @mysql_result( $result, 0, "busid9" );
$bid10 = @mysql_result( $result, 0, "busid10" );
$cid = @mysql_result( $result, 0, "companyid" );

if( $num != 1 || ( $security_level == 1 && ( $bid != $businessid || $cid != $companyid ) )
		|| $user == "" || $pass == "" ) {
	echo "<center><h3>Failed</h3></center>";
}
else {
	$data = '';
	if( $date1 == "" ) {
		$date1 = $today;
	}
	if( $date2 == "" ) {
		$date2 = $today;
	}
	
	$num_date = $date1;
	$counter = 1;
	while( $num_date < $date2 ) {
		$num_date = nextday( $num_date );
		$counter++;
		if( $counter == 8 ) {
			$week_end_date = $num_date;
		}
	}
	
	$salary_hours = 80;
	if( $counter <= 7 ) {
		$export_week = "W ";
	}
	else {
		$export_week = "BW";
	}
	
	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "UPDATE submit_labor SET export = '1' WHERE businessid = '$businessid' AND date >= '$date1' AND date <= '$date2'";
	if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
		echo $query, PHP_EOL;
	}
	$result = Treat_DB_ProxyOld::query( $query );
	//mysql_close();
	
	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM company WHERE companyid = '$companyid'";
	if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
		echo $query, PHP_EOL;
	}
	$result = Treat_DB_ProxyOld::query( $query );
	//mysql_close();
	
	$companyname = @mysql_result( $result, 0, "companyname" );
	
	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM business WHERE companyid = '$companyid' AND businessid = '$businessid'";
	if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
		echo $query, PHP_EOL;
	}
	$result = Treat_DB_ProxyOld::query( $query );
	//mysql_close();
	
	$businessname = @mysql_result( $result, 0, "businessname" );
	$businessid = @mysql_result( $result, 0, "businessid" );
	$unit = @mysql_result( $result, 0, "unit" );
	//$unit=substr($unit,0,3);
	$street = @mysql_result( $result, 0, "street" );
	$city = @mysql_result( $result, 0, "city" );
	$state = @mysql_result( $result, 0, "state" );
	$zip = @mysql_result( $result, 0, "zip" );
	$phone = @mysql_result( $result, 0, "phone" );
	$districtid = @mysql_result( $result, 0, "districtid" );
	$operate = @mysql_result( $result, 0, "operate" );
	$over_time_type = @mysql_result( $result, 0, "over_time_type" );
	
	///Unit (14)
	$exceptionDept = $unit;
	$len = strlen( $unit ); //remove
	while( $len < 14 ) {
		$unit = " $unit";
		$len++;
	} //remove
	
	$sh_totpay = 0;
	$sh_tothour = 0;
	$sh_codehour = array( );
	$sh_totreg = 0;
	$sh_totot = 0;
	
	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query34 = "SELECT * FROM payroll_code WHERE companyid = '$companyid' ORDER BY orderid DESC";
	if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
		echo $query, PHP_EOL;
	}
	$result34 = Treat_DB_ProxyOld::query( $query34 );
	$num34 = mysql_numrows( $result34 );
	//mysql_close();
	
	$temp_num = $num34 - 1;
	
	$endday = substr( $date2, 8, 2 );
	$endmonth = substr( $date2, 5, 2 );
	$endyear = substr( $date2, 0, 4 );
	
	//$data = "HPAYCOR01$month/$day/$year Essential Elements                   N$endmonth/$endday/$endyear\r\n";
	$tmp = writePaycorLine( 'H',
			array(
				'month' => $month,
				'day' => $day,
				'year' => $year,
				'manuf' => 'Essential Elements',
				'endMonth' => $endmonth,
				'endDay' => $endday,
				'endYear' => $endyear
			) );
	if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
		echo $tmp, PHP_EOL;
	}
	$data .= $tmp;
	
	
	/////////////REGULAR LABOR
	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM login WHERE oo2 = '$businessid' AND move_date <= '$date2' AND (is_deleted = '0' OR (is_deleted = '1' AND del_date >= '$date1')) AND empl_no != '' ORDER BY security_level,lastname DESC";
	if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
		echo $query, PHP_EOL;
	}
	$result = Treat_DB_ProxyOld::query( $query );
	$num = mysql_numrows( $result );
	//mysql_close();
	
	$num--;
	
	$period_hours = 0;
	$period_ot = 0;
	
	$totalreghours = 0;
	$totalothours = 0;
	$totalcodehours = 0;
	$totalnewhours = 0;
	
	while( $num >= 0 ) {
		$firstname = @mysql_result( $result, $num, "firstname" );
		$lastname = @mysql_result( $result, $num, "lastname" );
		$empl_no = @mysql_result( $result, $num, "empl_no" );
		$loginid = @mysql_result( $result, $num, "loginid" );
		
		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query2 = "SELECT * FROM labor WHERE loginid = '$loginid' AND businessid = '$businessid' AND date >= '$date1' AND date <= '$date2' AND tempid = '0' ORDER BY date DESC,rate,coded";
		if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
			echo $query2, PHP_EOL;
		}
		$result2 = Treat_DB_ProxyOld::query( $query2 );
		$num2 = mysql_numrows( $result2 );
		//mysql_close();
		
		$salary_reg = 0;
		$salary_amt = 0;
		
		$week_hours = 0;
		$week_ot = 0;
		
		$period_code_reg = array( );
		$period_code_ot = array( );
		
		$newweek = 0;
		
		$week_code_reg = array( );
		$week_code_ot = array( );
		$week_code_pto_override = array( );
		
		$rate_codes = array( );
		
		$num2--;
		while( $num2 >= 0 ) {
			$hours = @mysql_result( $result2, $num2, "hours" );
			$tips = @mysql_result( $result2, $num2, "tips" );
			$rate = @mysql_result( $result2, $num2, "rate" );
			$coded = @mysql_result( $result2, $num2, "coded" );
			$jobtype = @mysql_result( $result2, $num2, "jobtypeid" );
			$date = @mysql_result( $result2, $num2, "date" );
			
			$payRate = $rate;
			$rate = $rate * 100; //remove
			
			if( $date >= $week_end_date && $newweek == 0 ) {
				$week_hours = 0;
				$week_ot = 0;
				$newweek = 1;
			}
			
			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query3 = "SELECT * FROM payroll_code WHERE codeid = '$coded'";
			if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
				echo $query3, PHP_EOL;
			}
			$result3 = Treat_DB_ProxyOld::query( $query3 );
			//mysql_close();
			
			$special = @mysql_result( $result3, 0, "special" );
			
			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query3 = "
				SELECT
					*
				FROM jobtype
				LEFT JOIN jobtypedetail
					ON jobtype.jobtypeid = jobtypedetail.jobtype
					AND jobtypedetail.loginid = $loginid
				WHERE jobtypeid = '$jobtype'
			";
			if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
				echo $query3, PHP_EOL;
			}
			$result3 = Treat_DB_ProxyOld::query( $query3 );
			//mysql_close();
			
			$hourly = @mysql_result( $result3, 0, "hourly" );
			$is_tiped = @mysql_result( $result3, 0, "tips" );
			$pto_override = @mysql_result( $result3, 0, "pto_override" );
			$rateCode = @mysql_result( $result3, 0, 'rateCode' );
			if( empty( $rateCode ) )
				unset( $rateCode );
			else $rate_codes[$rateCode] = $rate;
			
			$rateIndex = $rateCode ? 'R' . $rateCode : $rate;
			
			if( $pto_override > 0 ) {
				$payRate = $pto_override;
				$rate = $pto_override * 100;
				$week_code_pto_override["$rateIndex.$coded"] = 1;
				if( $rateCode )
					$rate_codes[$rateCode] = $rate;
			}
			
			$overtime = 0;
			if( $special == 0 && $hourly == 0 ) {
				$salary_reg = $salary_reg + $hours;
				$salary_amt = $rate;
			}
			elseif( 1 == 1 ) {
				//////////////////CALIFORNIA OVERTIME
				$newhours = 0;
				if( $over_time_type == 1 ) {
					if( $hours > 8 ) {
						
						$newhours = $hours - 8;
						$hours = 8;
						$week_code_ot["$rateIndex.$coded"] += $newhours;
					}
				}
				
				if( $special == 1 ) {}
				elseif( $week_hours == 40 ) {
					$week_ot = $week_ot + $hours;
					$overtime = 1;
				}
				elseif( ( $week_hours + $hours ) > 40 ) {
					$overtime_hours = round( $week_hours + $hours - 40, 2 );
					$undertime_hours = round( $hours - $overtime_hours, 2 );
					$week_hours = 40;
					$week_ot = $week_hours + $hours - 40;
					$overtime = 2;
				}
				else {
					$week_hours = $week_hours + $hours;
				}
				
				if( $overtime == 0 ) {
					$week_code_reg["$rateIndex.$coded"] = $week_code_reg["$rateIndex.$coded"] + $hours;
				}
				elseif( $overtime == 1 ) {
					$week_code_ot["$rateIndex.$coded"] = $week_code_ot["$rateIndex.$coded"] + $hours;
				}
				elseif( $overtime == 2 ) {
					$week_code_reg["$rateIndex.$coded"] = $week_code_reg["$rateIndex.$coded"]
							+ $undertime_hours;
					$week_code_ot["$rateIndex.$coded"] = $week_code_ot["$rateIndex.$coded"] + $overtime_hours;
					
					$undertime_hours = 0;
					$overtime_hours = 0;
				}
			}
			else {}
			
			$num2--;
		}
		
		///////////////SALARY REGULAR HOURS
		if( $salary_reg > 0 ) {
			
			$salary_amt = $salary_amt / 100;
			$earnings = ( ( $salary_amt / $salary_hours ) * $salary_reg );
			$payEarnings = $earnings;
			$earnings = money( $earnings ); //remove
			
			///earnings (8)
			$earnings = $earnings * 100;
			$len = strlen( $earnings );
			while( $len < 8 ) {
				$earnings = "0$earnings";
				$len++;
			} //remove
			
			///empl number (6)
			$employeeNo2 = $empl_no;
			$len = strlen( $empl_no ); //remove
			$empl_no2 = $empl_no;
			while( $len < 6 ) {
				$empl_no2 = "0$empl_no2";
				$len++;
			} //remove
			
			///salary hours (9)
			$empSalary = $salary_reg;
			$salary_reg = $salary_reg * 10000; //remove
			$len = strlen( $salary_reg );
			while( $len < 9 ) {
				$salary_reg = "0$salary_reg";
				$len++;
			} //remove
			
			//$data.="D$empl_no2                                   $earnings                                                   $export_week \r\n";
		}
		
		foreach( $week_code_reg as $key => $value ) {
			
			$pieces = explode( ".", $key );
			if( $pieces[0][0] == 'R' ) {
				$rateCode = substr( $pieces[0], 1 );
				$pieces[0] = $rate_codes[$rateCode];
			}
			$pieces[0] = money( $pieces[0] / 100 );
			
			///rate (8)
			$payRate = $pieces[0];
			$rate = $pieces[0] * 100000; //remove
			$len = strlen( $rate );
			while( $len < 8 ) {
				$rate = "0$rate";
				$len++;
			} //remove
			
			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query3 = "SELECT * FROM payroll_code WHERE codeid = '$pieces[1]'";
			if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
				echo $query3, PHP_EOL;
			}
			$result3 = Treat_DB_ProxyOld::query( $query3 );
			//mysql_close();
			
			$export_name = @mysql_result( $result3, 0, "export_name" );
			$special = @mysql_result( $result3, 0, "special" );
			
			///empl number (6)
			$employeeNo = $empl_no;
			$len = strlen( $empl_no ); //remove
			while( $len < 6 ) {
				$empl_no = "0$empl_no";
				$len++;
			} //remove
			
			///REG HOURS(8)
			$value = round( $value, 2 );
			$payValue = $value;
			if( $special == 1 ) {
				$totalcodehours = $totalcodehours + $value;
			}
			else {
				$totalreghours = $totalreghours + $value;
			}
			$value = $value * 10000; //remove
			$len = strlen( $value );
			while( $len < 8 ) {
				$value = "0$value";
				$len++;
			} //remove
			
			///OT HOURS (7)
			$ot = $week_code_ot[$key];
			$ot = round( $ot, 2 );
			$payOvertime = $ot;
			if( $special == 1 ) {
				$totalcodehours = $totalcodehours + $ot;
			}
			else {
				$totalothours = $totalothours + $ot;
			}
			$ot = $ot * 10000; //remove
			$len = strlen( $ot );
			while( $len < 7 ) {
				$ot = "0$ot";
				$len++;
			} //remove
			
			if( $special == 1 ) {
				$exportName = $export_name;
				$len = strlen( $export_name ); //remove
				while( $len < 10 ) {
					$export_name = "$export_name ";
					$len++;
				} //remove
				$value = "0$value";
				
				if( $salary_reg > 0 ) {
					$exportName2 = $export_name;
					$export_name2 = $export_name;
					
					$earnings = ( $salary_amt / $salary_hours ) * ( $value / 10000 );
					$earnings = money( $earnings );
					
					///earnings (8)
					$payEarnings = $earnings;
					$earnings = $earnings * 100; //remove
					$len = strlen( $earnings );
					while( $len < 8 ) {
						$earnings = "0$earnings";
						$len++;
					} //remove
					
				}
				else {
					$earnings = "        ";
					$export_name2 = "          ";
					unset( $payEarnings );
					unset( $exportName2 );
				}
				
				if( $week_code_pto_override[$key] > 0 ) {
					$showrate = substr( $rate, 2, 6 );
					$showRate = $payRate;
					$showrate .= "00"; //remove
				}
				else {
					$showrate = "        ";
					unset( $showRate );
				}
				
				//$data.="D$empl_no                $value$export_name$earnings$export_name2                  $showrate$unit $export_week \r\n";
				//$data .= "X$empl_no                $value$export_name$earnings$export_name2                  $showrate               $export_week \r\n";
				$tmp = writePaycorLine( 'D',
						array(
							'employee' => $employeeNo,
							'rateCode' => $rateCode,
							'coded' => $payValue,
							'specialCode' => $exportName,
							'earnings1' => $payEarnings,
							'earnCode1' => $exportName2,
							'exceptionRate' => $showRate,
							'frequency' => $export_week
						) );
				if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
					echo $tmp, PHP_EOL;
				}
				$data .= $tmp;
			}
			else {
				//$data.="D$empl_no $value$ot                                                               $unit $export_week \r\n";
				//$data .= "X$empl_no $value$ot                                                                              $export_week \r\n";
				if( empty( $rateCode ) )
					unset( $rateCode );
				
				$tmp = writePaycorLine( 'D',
						array(
							'employee' => $employeeNo,
							'rateCode' => $rateCode,
							'regular' => $payValue,
							'overtime' => $payOvertime,
							'frequency' => $export_week
						) );
				if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
					echo $tmp, PHP_EOL;
				}
				$data .= $tmp;
			}
		}
		//////////////TIPS
		////////////////REPORTED TIPS
		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query4 = "SELECT SUM(cash_tips+charge_tips+cash_out+charge_out) AS tip_amt FROM labor_tips WHERE loginid = '$loginid' AND businessid = '$businessid' AND date >= '$date1' AND date <= '$date2'";
		if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
			echo $query4, PHP_EOL;
		}
		$result4 = Treat_DB_ProxyOld::query( $query4 );
		//mysql_close();
		
		$tip_amt = @mysql_result( $result4, 0, "tip_amt" );
		$show_tip = "REPTIPS   ";
		
		if( $tip_amt != 0 ) {
			$employeeNo = $empl_no;
			$tipAmount = $tip_amt;
			$tip_amt = money( $tip_amt ); //remove
			$tip_amt = $tip_amt * 100;
			$len = strlen( $tip_amt );
			while( $len < 8 ) {
				$tip_amt = "0$tip_amt";
				$len++;
			} //remove
			$payValue = $value;
			$value = "0$value"; //remove
			//$data .= "X$empl_no                                   $tip_amt$show_tip                                         $export_week \r\n";
			$tmp = writePaycorLine( 'D',
					array(
						'employee' => $employeeNo,
						'earnings1' => $tipAmount,
						'earnCode1' => $show_tip,
						'frequency' => $export_week
					) ); //untested!!!!!
			if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
				echo $tmp, PHP_EOL;
			}
			$data .= $tmp;
		}
		
		////////////////REIMBURSED TIPS
		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query4 = "SELECT SUM(charge_tips+charge_out) AS tip_amt FROM labor_tips WHERE loginid = '$loginid' AND businessid = '$businessid' AND date >= '$date1' AND date <= '$date2'";
		if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
			echo $query4, PHP_EOL;
		}
		$result4 = Treat_DB_ProxyOld::query( $query4 );
		//mysql_close();
		
		$tip_amt = @mysql_result( $result4, 0, "tip_amt" );
		$show_tip = "Reimb     ";
		
		if( $tip_amt != 0 ) {
			$employeeNo = $empl_no;
			$tipAmount = $tip_amt;
			$tip_amt = money( $tip_amt ); //remove
			$tip_amt = $tip_amt * 100;
			$len = strlen( $tip_amt );
			while( $len < 8 ) {
				$tip_amt = "0$tip_amt";
				$len++;
			} //remove
			$payValue = $value;
			$value = "0$value"; //remove
			//$data .= "X$empl_no                                   $tip_amt$show_tip                                         $export_week \r\n";
			$tmp = writePaycorLine( 'D',
					array(
						'employee' => $employeeNo,
						'earnings1' => $tipAmount,
						'earnCode1' => $show_tip,
						'frequency' => $export_week
					) ); //untested!!!!!
			if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
				echo $tmp, PHP_EOL;
			}
			$data .= $tmp;
		}
		///////////////END TIPS
		
		$num--;
	}
	
	/////////////PAST LABOR
	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT distinct labor.loginid FROM login,labor WHERE labor.businessid = '$businessid' AND labor.date >= '$date1' AND labor.date <= '$date2' AND login.oo2 != '$businessid' AND labor.loginid = login.loginid AND labor.tempid = '0'";
	if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
		echo $query, PHP_EOL;
	}
	$result = Treat_DB_ProxyOld::query( $query );
	$num = mysql_numrows( $result );
	//mysql_close();
	
	$num--;
	
	$period_hours = 0;
	$period_ot = 0;
	
	while( $num >= 0 ) {
		$loginid = @mysql_result( $result, $num, "loginid" );
		
		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query73 = "SELECT * FROM login WHERE loginid = '$loginid'";
		if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
			echo $query73, PHP_EOL;
		}
		$result73 = Treat_DB_ProxyOld::query( $query73 );
		//mysql_close();
		
		$firstname = @mysql_result( $result73, 0, "firstname" );
		$lastname = @mysql_result( $result73, 0, "lastname" );
		$empl_no = @mysql_result( $result73, 0, "empl_no" );
		
		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query2 = "SELECT * FROM labor WHERE loginid = '$loginid' AND businessid = '$businessid' AND date >= '$date1' AND date <= '$date2' AND tempid = '0' ORDER BY date DESC,rate,coded";
		if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
			echo $query2, PHP_EOL;
		}
		$result2 = Treat_DB_ProxyOld::query( $query2 );
		$num2 = mysql_numrows( $result2 );
		//mysql_close();
		
		$salary_reg = 0;
		$salary_amt = 0;
		
		$week_hours = 0;
		$week_ot = 0;
		$totalnewhours = 0;
		
		$period_code_reg = array( );
		$period_code_ot = array( );
		
		$newweek = 0;
		
		$week_code_reg = array( );
		$week_code_ot = array( );
		
		$rate_codes = array( );
		
		$num2--;
		while( $num2 >= 0 ) {
			$hours = @mysql_result( $result2, $num2, "hours" );
			$tips = @mysql_result( $result2, $num2, "tips" );
			$rate = @mysql_result( $result2, $num2, "rate" );
			$coded = @mysql_result( $result2, $num2, "coded" );
			$jobtype = @mysql_result( $result2, $num2, "jobtypeid" );
			$date = @mysql_result( $result2, $num2, "date" );
			
			$payRate = $rate;
			$rate = $rate * 100; //remove
			
			if( $date >= $week_end_date && $newweek == 0 ) {
				$week_hours = 0;
				$week_ot = 0;
				$newweek = 1;
			}
			
			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query3 = "
				SELECT
					*
				FROM jobtype
				LEFT JOIN jobtypedetail
					ON jobtype.jobtypeid = jobtypedetail.jobtype
					AND jobtypedetail.loginid = $loginid
				WHERE jobtypeid = '$jobtype'
			";
			if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
				echo $query3, PHP_EOL;
			}
			$result3 = Treat_DB_ProxyOld::query( $query3 );
			//mysql_close();
			
			$hourly = @mysql_result( $result3, 0, "hourly" );
			$is_tiped = @mysql_result( $result3, 0, "tips" );
			$pto_override = @mysql_result( $result3, 0, "pto_override" );
			$rateCode = @mysql_result( $result3, 0, 'rateCode' );
			if( empty( $rateCode ) )
				unset( $rateCode );
			else $rate_codes[$rateCode] = $rate;
			
			$rateIndex = $rateCode ? 'R' . $rateCode : $rate;
			
			if( $pto_override > 0 ) {
				$payRate = $pto_override;
				$rate = $pto_override;
				if( $rateCode )
					$rate_codes[$rateCode] = $rate;
			}
			
			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query3 = "SELECT * FROM payroll_code WHERE codeid = '$coded'";
			if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
				echo $query3, PHP_EOL;
			}
			$result3 = Treat_DB_ProxyOld::query( $query3 );
			//mysql_close();
			
			$special = @mysql_result( $result3, 0, "special" );
			
			$overtime = 0;
			if( $special == 0 && $hourly == 0 ) {
				$salary_reg = $salary_reg + $hours;
				$salary_amt = $rate;
			}
			elseif( 1 == 1 ) {
				//////////////////CALIFORNIA OVERTIME
				if( $over_time_type == 1 ) {
					if( $hours > 8 ) {
						$newhours = $hours - 8;
						$hours = 8;
						$week_code_ot["$rateIndex.$coded"] += $newhours;
					}
				}
				
				if( $special == 1 ) {}
				elseif( $week_hours == 40 ) {
					$week_ot = $week_ot + $hours;
					$overtime = 1;
				}
				elseif( ( $week_hours + $hours ) > 40 ) {
					$overtime_hours = round( $week_hours + $hours - 40, 2 );
					$undertime_hours = round( $hours - $overtime_hours, 2 );
					$week_hours = 40;
					$week_ot = $week_hours + $hours - 40;
					$overtime = 2;
				}
				else {
					$week_hours = $week_hours + $hours;
				}
				
				if( $overtime == 0 ) {
					$week_code_reg["$rateIndex.$coded"] = $week_code_reg["$rateIndex.$coded"] + $hours;
				}
				elseif( $overtime == 1 ) {
					$week_code_ot["$rateIndex.$coded"] = $week_code_ot["$rateIndex.$coded"] + $hours;
				}
				elseif( $overtime == 2 ) {
					$week_code_reg["$rateIndex.$coded"] = $week_code_reg["$rateIndex.$coded"]
							+ $undertime_hours;
					$week_code_ot["$rateIndex.$coded"] = $week_code_ot["$rateIndex.$coded"] + $overtime_hours;
					
					$undertime_hours = 0;
					$overtime_hours = 0;
				}
			}
			else {}
			
			$num2--;
		}
		
		///////////////SALARY REGULAR HOURS
		if( $salary_reg > 0 ) {
			
			$salary_amt = $salary_amt / 100;
			$earnings = ( ( $salary_amt / $salary_hours ) * $salary_reg );
			$payEarnings = $earnings;
			$earnings = money( $earnings ); //remove
			
			///earnings (8)
			$earnings = $earnings * 100; //remove
			$len = strlen( $earnings );
			while( $len < 8 ) {
				$earnings = "0$earnings";
				$len++;
			} //remove
			
			///empl number (6)
			$employeeNo2 = $empl_no;
			$len = strlen( $empl_no ); //remove
			$empl_no2 = $empl_no;
			while( $len < 6 ) {
				$empl_no2 = "0$empl_no2";
				$len++;
			} //remove
			
			///salary hours (9)
			$salaryReg = $salary_reg;
			$salary_reg = $salary_reg * 10000; //remove
			$len = strlen( $salary_reg );
			while( $len < 9 ) {
				$salary_reg = "0$salary_reg";
				$len++;
			} //remove
			
			//$data.="D$empl_no2                                   $earnings                                                   $export_week \r\n";
		}
		
		foreach( $week_code_reg as $key => $value ) {
			
			$pieces = explode( ".", $key );
			if( $pieces[0][0] == 'R' ) {
				$rateCode = substr( $pieces[0], 1 );
				$pieces[0] = $rate_codes[$rateCode];
			}
			$payRate = $pieces[0] / 100;
			$pieces[0] = money( $pieces[0] / 100 ); //remove
			
			///rate (8)
			$rate = $pieces[0] * 100000;
			$len = strlen( $rate );
			while( $len < 8 ) {
				$rate = "0$rate";
				$len++;
			} //remove
			
			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query3 = "SELECT * FROM payroll_code WHERE codeid = '$pieces[1]'";
			if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
				echo $query3, PHP_EOL;
			}
			$result3 = Treat_DB_ProxyOld::query( $query3 );
			//mysql_close();
			
			$export_name = @mysql_result( $result3, 0, "export_name" );
			$special = @mysql_result( $result3, 0, "special" );
			
			///empl number (6)
			$employeeNo = $empl_no;
			$len = strlen( $empl_no ); //remove
			while( $len < 6 ) {
				$empl_no = "0$empl_no";
				$len++;
			} //remove
			
			///REG HOURS(8)
			$value = round( $value, 2 );
			$payValue = $value;
			if( $special == 1 ) {
				$totalcodehours = $totalcodehours + $value;
			}
			else {
				$totalreghours = $totalreghours + $value;
			}
			$value = $value * 10000; //remove
			$len = strlen( $value );
			while( $len < 8 ) {
				$value = "0$value";
				$len++;
			} //remove
			
			///OT HOURS (7)
			$ot = $week_code_ot[$key];
			$ot = round( $ot, 2 );
			$payOvertime = $ot;
			if( $special == 1 ) {
				$totalcodehours = $totalcodehours + $ot;
			}
			else {
				$totalothours = $totalothours + $ot;
			}
			$ot = $ot * 10000; //remove
			$len = strlen( $ot );
			while( $len < 7 ) {
				$ot = "0$ot";
				$len++;
			} //remove
			
			if( $special == 1 ) {
				$exportName = $export_name;
				$len = strlen( $export_name ); //remove
				while( $len < 10 ) {
					$export_name = "$export_name ";
					$len++;
				} //remove
				$value = "0$value"; //remove
				
				if( $salary_reg > 0 ) {
					$export_name2 = $export_name;
					$exportName2 = $exportName;
					
					$earnings = ( $salary_amt / $salary_hours ) * ( $value / 10000 );
					$payEarnings = $earnings;
					$earnings = money( $earnings ); //remove
					
					///earnings (8)
					$earnings = $earnings * 100; //remove
					$len = strlen( $earnings );
					while( $len < 8 ) {
						$earnings = "0$earnings";
						$len++;
					} //remove
				}
				else {
					$earnings = "        ";
					$export_name2 = "          ";
					unset( $payEarnings );
					unset( $exportName2 );
				}
				
				//$data .= "X$empl_no                $value$export_name$earnings$export_name2                                         $export_week \r\n";
				$tmp = writePaycorLine( 'D',
						array(
							'employee' => $employeeNo,
							'coded' => $payValue,
							'specialCode' => $exportName,
							'earnings1' => $payEarnings,
							'earnCode1' => $exportName2,
							'frequency' => $export_week
						) ); //untested
				if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
					echo $tmp, PHP_EOL;
				}
				$data .= $tmp;
			}
			else {
				//$data .= "X$empl_no $value$ot                                                                              $export_week \r\n";
				if( empty( $rateCode ) )
					unset( $rateCode );
				
				$tmp = writePaycorLine( 'D',
						array(
							'employee' => $employeeNo,
							'rateCode' => $rateCode,
							'regular' => $payValue,
							'overtime' => $payOvertime,
							'frequency' => $export_week
						) ); //untested
				if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
					echo $tmp, PHP_EOL;
				}
				$data .= $tmp;
			}
		}
		//////////////TIPS
		////////////////REPORTED TIPS
		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query4 = "SELECT SUM(cash_tips+charge_tips+cash_out+charge_out) AS tip_amt FROM labor_tips WHERE loginid = '$loginid' AND businessid = '$businessid' AND date >= '$date1' AND date <= '$date2'";
		if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
			echo $query4, PHP_EOL;
		}
		$result4 = Treat_DB_ProxyOld::query( $query4 );
		//mysql_close();
		
		$tip_amt = @mysql_result( $result4, 0, "tip_amt" );
		$show_tip = "REPTIPS   ";
		
		if( $tip_amt != 0 ) {
			$employeeNo = $empl_no;
			$tipAmount = $tip_amt;
			$tip_amt = money( $tip_amt ); //remove
			$tip_amt = $tip_amt * 100;
			$len = strlen( $tip_amt );
			while( $len < 8 ) {
				$tip_amt = "0$tip_amt";
				$len++;
			} //remove
			$value = "0$value";
			//$data .= "X$empl_no                                   $tip_amt$show_tip                          $unit $export_week \r\n";
			$tmp = writePaycorLine( 'D',
					array(
						'employee' => $employeeNo,
						'earnings1' => $tipAmount,
						'earnCode1' => $show_tip,
						'exceptionDept' => $exceptionDept,
						'frequency' => $export_week
					) ); //untested
			if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
				echo $tmp, PHP_EOL;
			}
			$data .= $tmp;
		}
		
		////////////////REIMBURSED TIPS
		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query4 = "SELECT SUM(charge_tips+charge_out) AS tip_amt FROM labor_tips WHERE loginid = '$loginid' AND businessid = '$businessid' AND date >= '$date1' AND date <= '$date2'";
		if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
			echo $query4, PHP_EOL;
		}
		$result4 = Treat_DB_ProxyOld::query( $query4 );
		//mysql_close();
		
		$tip_amt = @mysql_result( $result4, 0, "tip_amt" );
		$show_tip = "Reimb     ";
		
		if( $tip_amt != 0 ) {
			$employeeNo = $empl_no;
			$tipAmount = $tip_amt;
			$tip_amt = money( $tip_amt ); //remove
			$tip_amt = $tip_amt * 100;
			$len = strlen( $tip_amt );
			while( $len < 8 ) {
				$tip_amt = "0$tip_amt";
				$len++;
			}
			$value = "0$value"; //remove
			//$data .= "D$empl_no                                   $tip_amt$show_tip                          $unit $export_week \r\n";
			$tmp = writePaycorLine( 'D',
					array(
						'employee' => $employeeNo,
						'earnings1' => $tipAmount,
						'earnCode1' => $show_tip,
						'exceptionDept' => $exceptionDept,
						'frequency' => $export_week
					) ); //untested
			if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
				echo $tmp, PHP_EOL;
			}
			$data .= $tmp;
		}
		///////////////END TIPS
		
		$num--;
	}
	///////////////TEMP LABOR
	$lastloginid = 0;
	$showRate = "";
	$payEarnings = "";
	$exportName2 = "";
	
	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM labor_temp WHERE businessid = '$businessid' AND date >= '$date1' AND date <= '$date2' ORDER BY loginid";
	if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
		echo $query, PHP_EOL;
	}
	$result = Treat_DB_ProxyOld::query( $query );
	$num = mysql_numrows( $result );
	//mysql_close();
	
	$num--;
	while( $num >= 0 ) {
		$loginid = @mysql_result( $result, $num, "loginid" );
		$tempid = @mysql_result( $result, $num, "tempid" );
		
		///////CHECK FOR DOUBLES
		if( $lastloginid != $loginid ) {
			
			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query73 = "SELECT * FROM login WHERE loginid = '$loginid'";
			if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
				echo $query73, PHP_EOL;
			}
			$result73 = Treat_DB_ProxyOld::query( $query73 );
			//mysql_close();
			
			$firstname = @mysql_result( $result73, 0, "firstname" );
			$lastname = @mysql_result( $result73, 0, "lastname" );
			$empl_no = @mysql_result( $result73, 0, "empl_no" );
			
			$hourtotal = 0;
			$othourtotal = 0;
			$payroll_code_hours = array( );
			$temp_reg_hours = array( );
			$temp_ot_hours = array( );
			$total_hours = 0;
			$rowtotal = 0;
			
			//////FIND HOURS IN HOME UNIT
			$temp_date = $date1;
			$temp_hours = 0;
			while( $temp_date <= $date2 ) {
				for( $counter = 1; $counter <= 7; $counter++ ) {
					
					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( "Unable to select database");
					$query2 = "SELECT oo2 FROM login WHERE loginid = '$loginid'";
					if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
						echo $query2, PHP_EOL;
					}
					$result2 = Treat_DB_ProxyOld::query( $query2 );
					//mysql_close();
					
					$temp_bid = @mysql_result( $result2, 0, "oo2" );
					
					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( "Unable to select database");
					$query2 = "SELECT * FROM labor WHERE loginid = '$loginid' AND businessid = '$temp_bid' AND date = '$temp_date' AND tempid = '0'";
					if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
						echo $query2, PHP_EOL;
					}
					$result2 = Treat_DB_ProxyOld::query( $query2 );
					$num2 = mysql_numrows( $result2 );
					//mysql_close();
					
					$num2--;
					while( $num2 >= 0 ) {
						$coded = @mysql_result( $result2, $num2, "coded" );
						$hours = @mysql_result( $result2, $num2, "hours" );
						$rate = @mysql_result( $result2, $num2, "rate" );
						$jobtype = @mysql_result( $result2, $num2, "jobtypeid" );
						$date = @mysql_result( $result2, $num2, "date" );
						
						//mysql_connect($dbhost,$username,$password);
						//@mysql_select_db($database) or die( "Unable to select database");
						$query3 = "SELECT * FROM payroll_code WHERE codeid = '$coded'";
						if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
							echo $query3, PHP_EOL;
						}
						$result3 = Treat_DB_ProxyOld::query( $query3 );
						//mysql_close();
						
						$special = @mysql_result( $result3, 0, "special" );
						$pto_override = @mysql_result( $result3, 0, "pto_override" );
						
						if( $pto_override > 0 ) {
							$payRate = $pto_override;
							$rate = $pto_override;
						}
						
						//mysql_connect($dbhost,$username,$password);
						//@mysql_select_db($database) or die( "Unable to select database");
						$query3 = "SELECT * FROM jobtype WHERE jobtypeid = '$jobtype'";
						if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
							echo $query3, PHP_EOL;
						}
						$result3 = Treat_DB_ProxyOld::query( $query3 );
						//mysql_close();
						
						$hourly = @mysql_result( $result3, 0, "hourly" );
						
						if( $date < $week_end_date && $special == 0 && $hourly == 1 ) {
							$temp_reg_hours[1] = $temp_reg_hours[1] + $hours;
						}
						elseif( $special == 0 && $hourly == 1 ) {
							$temp_reg_hours[2] = $temp_reg_hours[2] + $hours;
						}
						
						$num2--;
					}
					$temp_date = nextday( $temp_date );
				}
			}
			
			//////FIND HOURS IN OTHER UNITS
			//////WHERE tempid is less than this tempid
			
			$temp_date = $date1;
			while( $temp_date <= $date2 ) {
				for( $counter = 1; $counter <= 7; $counter++ ) {
					
					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( "Unable to select database");
					$query2 = "SELECT oo2 FROM login WHERE loginid = '$loginid'";
					if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
						echo $query2, PHP_EOL;
					}
					$result2 = Treat_DB_ProxyOld::query( $query2 );
					//mysql_close();
					
					$temp_bid = @mysql_result( $result2, 0, "oo2" );
					
					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( "Unable to select database");
					$query2 = "SELECT * FROM labor WHERE labor.loginid = '$loginid' AND labor.businessid != '$temp_bid' AND labor.businessid != '$businessid' AND labor.date = '$temp_date' AND labor.tempid < '$tempid' AND labor.tempid != '$tempid' AND labor.tempid > '0'";
					if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
						echo $query2, PHP_EOL;
					}
					$result2 = Treat_DB_ProxyOld::query( $query2 );
					$num2 = mysql_numrows( $result2 );
					//mysql_close();
					
					$num2--;
					while( $num2 >= 0 ) {
						$coded = @mysql_result( $result2, $num2, "coded" );
						$hours = @mysql_result( $result2, $num2, "hours" );
						$rate = @mysql_result( $result2, $num2, "rate" );
						$jobtype = @mysql_result( $result2, $num2, "jobtypeid" );
						$date = @mysql_result( $result2, $num2, "date" );
						
						//mysql_connect($dbhost,$username,$password);
						//@mysql_select_db($database) or die( "Unable to select database");
						$query3 = "SELECT * FROM payroll_code WHERE codeid = '$coded'";
						if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
							echo $query3, PHP_EOL;
						}
						$result3 = Treat_DB_ProxyOld::query( $query3 );
						//mysql_close();
						
						$special = @mysql_result( $result3, 0, "special" );
						
						//mysql_connect($dbhost,$username,$password);
						//@mysql_select_db($database) or die( "Unable to select database");
						$query3 = "SELECT * FROM jobtype WHERE jobtypeid = '$jobtype'";
						if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
							echo $query3, PHP_EOL;
						}
						$result3 = Treat_DB_ProxyOld::query( $query3 );
						//mysql_close();
						
						$hourly = @mysql_result( $result3, 0, "hourly" );
						
						if( $date < $week_end_date && $special == 0 && $hourly == 1 ) {
							$temp_reg_hours[1] = $temp_reg_hours[1] + $hours;
						}
						elseif( $special == 0 && $hourly == 1 ) {
							$temp_reg_hours[2] = $temp_reg_hours[2] + $hours;
						}
						
						$num2--;
					}
					$temp_date = nextday( $temp_date );
				}
			}
			
			//////CALCULATE TRUE TIME
			if( $temp_reg_hours[1] > 40 ) {
				$temp_ot_hours[1] = $temp_reg_hours[1] - 40;
				$temp_reg_hours[1] = 40;
			}
			if( $temp_reg_hours[2] > 40 ) {
				$temp_ot_hours[2] = $temp_reg_hours[2] - 40;
				$temp_reg_hours[2] = 40;
			}
			
			//$data.="$empl_no,$temp_reg_hours[1],$temp_ot_hours[1],$temp_reg_hours[2],$temp_ot_hours[2]\r\n";
			
			$week_code_reg = array( );
			$week_code_ot = array( );
			
			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query2 = "SELECT * FROM labor WHERE loginid = '$loginid' AND businessid = '$businessid' AND date >= '$date1' AND date <= '$date2' AND tempid > '0' ORDER BY date DESC,rate,coded";
			if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
				echo $query2, PHP_EOL;
			}
			$result2 = Treat_DB_ProxyOld::query( $query2 );
			$num2 = mysql_numrows( $result2 );
			//mysql_close();
			
			$week_hours = $temp_reg_hours[1];
			$week_ot = $temp_ot_hours[1];
			
			$period_code_reg = array( );
			$period_code_ot = array( );
			
			$newweek = 0;
			
			$week_code_reg = array( );
			$week_code_ot = array( );
			
			$rate_codes = array( );
			
			$is_reg = 0;
			$is_reg1 = 0;
			$is_reg2 = 0;
			
			$num2--;
			while( $num2 >= 0 ) {
				$hours = @mysql_result( $result2, $num2, "hours" );
				$tips = @mysql_result( $result2, $num2, "tips" );
				$rate = @mysql_result( $result2, $num2, "rate" );
				$coded = @mysql_result( $result2, $num2, "coded" );
				$jobtype = @mysql_result( $result2, $num2, "jobtypeid" );
				$date = @mysql_result( $result2, $num2, "date" );
				
				$payRate = $rate;
				$rate = $rate * 100; //remove
				
				if( $date >= $week_end_date && $newweek == 0 ) {
					$week_hours = $temp_reg_hours[2];
					$week_ot = $temp_ot_hours[2];
					$newweek = 1;
				}
				
				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( "Unable to select database");
				$query3 = "
					SELECT
						*
					FROM jobtype
					LEFT JOIN jobtypedetail
						ON jobtype.jobtypeid = jobtypedetail.jobtype
						AND jobtypedetail.loginid = $loginid
					WHERE jobtypeid = '$jobtype'
				";
				if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
					echo $query3, PHP_EOL;
				}
				$result3 = Treat_DB_ProxyOld::query( $query3 );
				//mysql_close();
				
				$hourly = @mysql_result( $result3, 0, "hourly" );
				$is_tiped = @mysql_result( $result3, 0, "tips" );
				$pto_override = @mysql_result( $result3, 0, "pto_override" );
				$rateCode = @mysql_result( $result3, 0, 'rateCode' );
				if( empty( $rateCode ) )
					unset( $rateCode );
				else $rate_codes[$rateCode] = $rate;
				
				$rateIndex = $rateCode ? 'R' . $rateCode : $rate;
				
				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( "Unable to select database");
				$query3 = "SELECT * FROM payroll_code WHERE codeid = '$coded'";
				if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
					echo $query3, PHP_EOL;
				}
				$result3 = Treat_DB_ProxyOld::query( $query3 );
				//mysql_close();
				
				$special = @mysql_result( $result3, 0, "special" );
				$code_name2 = @mysql_result( $result3, 0, "code" );
				
				if( $code_name2 == "PTO" && $pto_override > 0 ) {
					$payRate = $pto_override;
					$rate = $pto_override * 100;
					if( $rateCode )
						$rate_codes[$rateCode] = $rate;
				}
				
				$overtime = 0;
				if( $hourly == 0 && $special == 0 ) {
					$salary_reg = $salary_reg + $hours;
				}
				elseif( 1 == 1 ) {
					//////////////////CALIFORNIA OVERTIME
					if( $over_time_type == 1 ) {
						if( $hours > 8 ) {
							$newhours = $hours - 8;
							$hours = 8;
							$week_code_ot["$rateIndex.$coded"] += $newhours;
						}
					}
					
					if( $special == 1 ) {}
					elseif( $week_hours == 40 ) {
						$week_ot = $week_ot + $hours;
						$overtime = 1;
					}
					elseif( ( $week_hours + $hours ) > 40 ) {
						$overtime_hours = round( $week_hours + $hours - 40, 2 );
						$undertime_hours = round( $hours - $overtime_hours, 2 );
						
						$week_hours = 40;
						$week_ot = $week_hours + $hours - 40;
						$overtime = 2;
					}
					else {
						$week_hours = $week_hours + $hours;
					}
					
					if( $overtime == 0 ) {
						$week_code_reg["$rateIndex.$coded"] = $week_code_reg["$rateIndex.$coded"] + $hours;
					}
					elseif( $overtime == 1 ) {
						$week_code_ot["$rateIndex.$coded"] = $week_code_ot["$rateIndex.$coded"] + $hours;
						
						if( $is_reg == 0 ) {
							$week_code_reg["$rateIndex.$coded"] = 0;
						}
					}
					elseif( $overtime == 2 ) {
						$week_code_reg["$rateIndex.$coded"] = $week_code_reg["$rateIndex.$coded"]
								+ $undertime_hours;
						$week_code_ot["$rateIndex.$coded"] = $week_code_ot["$rateIndex.$coded"]
								+ $overtime_hours;
						
						$undertime_hours = 0;
						$overtime_hours = 0;
						
						$is_reg = 1;
					}
					
				}
				else {}
				
				$num2--;
			}
			
			foreach( $week_code_reg as $key => $value ) {
				
				$pieces = explode( ".", $key );
				if( $pieces[0][0] == 'R' ) {
					$rateCode = substr( $pieces[0], 1 );
					$pieces[0] = $rate_codes[$rateCode];
				}
				$pieces[0] = money( $pieces[0] / 100 );
				
				///rate (8)
				$payRate = $pieces[0];
				$rate = $pieces[0] * 100000; //remove
				$len = strlen( $rate );
				while( $len < 8 ) {
					$rate = "0$rate";
					$len++;
				} //remove
				
				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( "Unable to select database");
				$query3 = "SELECT * FROM payroll_code WHERE codeid = '$pieces[1]'";
				if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
					echo $query3, PHP_EOL;
				}
				$result3 = Treat_DB_ProxyOld::query( $query3 );
				//mysql_close();
				
				$export_name = @mysql_result( $result3, 0, "export_name" );
				$special = @mysql_result( $result3, 0, "special" );
				
				///empl number (6)
				$employeeNo = $empl_no;
				$len = strlen( $empl_no ); //remove
				while( $len < 6 ) {
					$empl_no = "0$empl_no";
					$len++;
				} //remove
				
				///REG HOURS(8)
				$value = round( $value, 2 );
				if( $special == 1 ) {
					$totalcodehours = $totalcodehours + $value;
				}
				else {
					$totalreghours = $totalreghours + $value;
				}
				$payValue = $value;
				$value = $value * 10000; //remove
				$len = strlen( $value );
				while( $len < 8 ) {
					$value = "0$value";
					$len++;
				} //remove
				
				///OT HOURS (7)
				$ot = $week_code_ot[$key];
				$ot = round( $ot, 2 );
				if( $special == 1 ) {
					$totalcodehours = $totalcodehours + $ot;
				}
				else {
					$totalothours = $totalothours + $ot;
				}
				$payOvertime = $ot;
				$ot = $ot * 10000; //remove
				$len = strlen( $ot );
				while( $len < 7 ) {
					$ot = "0$ot";
					$len++;
				} //remove
				
				if( $special == 1 ) {
					$exportName = $export_name;
					$len = strlen( $export_name ); //remove
					while( $len < 10 ) {
						$export_name = "$export_name ";
						$len++;
					}
					$value = "0$value"; //remove
					
					//$data .= "X$empl_no                $value$export_name                                            $unit $export_week \r\n";
					/*$data .= writePaycorLine( 'D',
							array(
								'employee' => $employeeNo,
								'earnings1' => $payValue,
								'earnCode1' => $exportName,
								'exceptionDept' => $exceptionDept,
								'frequency' => $export_week
							) ); //untested*/
					$tmp = writePaycorLine( 'D',
						array(
							'employee' => $employeeNo,
							'coded' => $payValue,
							'specialCode' => $exportName,
							'earnings1' => $payEarnings,
							'earnCode1' => $exportName2,
							'exceptionRate' => $showRate,
							'frequency' => $export_week
						) );
					if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
						echo $tmp, PHP_EOL;
					}
					$data .= $tmp;
				}
				else {
					//$data .= "X$empl_no $value$ot                                                       $rate$unit $export_week \r\n";
					if( empty( $rateCode ) )
						unset( $rateCode );
					
					$tmp = writePaycorLine( 'D',
							array(
								'employee' => $employeeNo,
								'rateCode' => $rateCode,
								'regular' => $payValue,
								'overtime' => $payOvertime,
								'exceptionRate' => $payRate,
								'exceptionDept' => $exceptionDept,
								'frequency' => $export_week
							) ); //untested
					if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
						echo $tmp, PHP_EOL;
					}
					$data .= $tmp;
				}
				
			}
		}
		
		//////////////TIPS
		////////////////REPORTED TIPS
		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query4 = "SELECT SUM(cash_tips+charge_tips+cash_out+charge_out) AS tip_amt FROM labor_tips WHERE loginid = '$loginid' AND businessid = '$businessid' AND date >= '$date1' AND date <= '$date2'";
		if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
			echo $query4, PHP_EOL;
		}
		$result4 = Treat_DB_ProxyOld::query( $query4 );
		//mysql_close();
		
		$tip_amt = @mysql_result( $result4, 0, "tip_amt" );
		$show_tip = "REPTIPS   ";
		
		if( $tip_amt != 0 ) {
			$employeeNo = $empl_no;
			$tipAmount = $tip_amt;
			$tip_amt = money( $tip_amt ); //remove
			$tip_amt = $tip_amt * 100;
			$len = strlen( $tip_amt );
			while( $len < 8 ) {
				$tip_amt = "0$tip_amt";
				$len++;
			}
			$value = "0$value"; //remove
			//
			$tmp = "X$empl_no                                   $tip_amt$show_tip                          $unit $export_week \r\n";
			$data .= $tmp;
			$tmp = writePaycorLine( 'D',
					array(
						'employee' => $employeeNo,
						'earnings1' => $tipAmount,
						'earnCode1' => $show_tip,
						'exceptionDept' => $exceptionDept,
						'frequency' => $export_week
					) ); //untested/
			if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
				echo $tmp, PHP_EOL;
			}
			$data .= $tmp;
		}
		
		////////////////REIMBURSED TIPS
		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query4 = "SELECT SUM(charge_tips+charge_out) AS tip_amt FROM labor_tips WHERE loginid = '$loginid' AND businessid = '$businessid' AND date >= '$date1' AND date <= '$date2'";
		if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
			echo $query4, PHP_EOL;
		}
		$result4 = Treat_DB_ProxyOld::query( $query4 );
		//mysql_close();
		
		$tip_amt = @mysql_result( $result4, 0, "tip_amt" );
		$show_tip = "Reimb     ";
		
		if( $tip_amt != 0 ) {
			$employeeNo = $empl_no;
			$tipAmount = $tip_amt;
			$tip_amt = money( $tip_amt ); //remove
			$tip_amt = $tip_amt * 100;
			$len = strlen( $tip_amt );
			while( $len < 8 ) {
				$tip_amt = "0$tip_amt";
				$len++;
			}
			$value = "0$value"; //remove
			//$data .= "X$empl_no                                   $tip_amt$show_tip                          $unit $export_week \r\n";
			$tmp = writePaycorLine( 'D',
					array(
						'employee' => $employeeNo,
						'earnings1' => $tipAmount,
						'earnCode1' => $show_tip,
						'exceptionDept' => $exceptionDept,
						'frequency' => $export_week
					) ); //untested
			if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
				echo $tmp, PHP_EOL;
			}
			$data .= $tmp;
		}
		///////////////END TIPS
		$lastloginid = $loginid;
		$num--;
	}
	///////////END EMPLOYEE LABOR
	///////////////////COMMISSIONS
	
	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query34 = "SELECT * FROM payroll_code_comm WHERE companyid = '$companyid' AND no_export = '0' ORDER BY orderid DESC";
	if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
		echo $query34, PHP_EOL;
	}
	$result34 = Treat_DB_ProxyOld::query( $query34 );
	$num34 = mysql_numrows( $result34 );
	//mysql_close();
	
	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM jobtype WHERE companyid = '$companyid' AND commission = '1'";
	if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
		echo $query, PHP_EOL;
	}
	$result = Treat_DB_ProxyOld::query( $query );
	$num = mysql_numrows( $result );
	//mysql_close();
	
	$num--;
	while( $num >= 0 ) {
		$jobtypeid = @mysql_result( $result, $num, "jobtypeid" );
		$jobtypename = @mysql_result( $result, $num, "name" );
		
		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query2 = "
			SELECT
				*
			FROM labor_temp_comm
			JOIN jobtypedetail
				ON labor_temp_comm.jobtypeid = jobtypedetail.jobtype
				AND labor_temp_comm.loginid = jobtypedetail.loginid
				AND jobtypedetail.is_deleted = 0
			WHERE jobtypeid = '$jobtypeid'
			AND businessid = '$businessid'
			AND date BETWEEN '$date1' AND '$date2'
			ORDER BY labor_temp_comm.loginid DESC
		";
		if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
			echo $query2, PHP_EOL;
		}
		$result2 = Treat_DB_ProxyOld::query( $query2 );
		$num2 = mysql_numrows( $result2 );
		//mysql_close();
		
		$codevalue = array( );
		
		$ot = 0;
		$totalpay = 0;
		$lastloginid = 0;
		$num2--;
		while( $num2 >= -1 ) {
			$tempid = @mysql_result( $result2, $num2, "tempid" );
			$loginid = @mysql_result( $result2, $num2, "loginid" );
			
			if( $loginid != $lastloginid && $lastloginid != 0 ) {
				
				$temp_num = $num34 - 1;
				while( $temp_num >= 0 ) {
					$codeid = @mysql_result( $result34, $temp_num, "codeid" );
					$code_name = @mysql_result( $result34, $temp_num, "code_name" );
					$export_name = @mysql_result( $result34, $temp_num, "export_name" );
					$hours = @mysql_result( $result34, $temp_num, "hours" );
					
					////////EMPLOYEE NUMBER (6)
					$employeeNo = $empl_no;
					$len = strlen( $empl_no ); //remove
					while( $len < 6 ) {
						$empl_no = "0$empl_no";
						$len++;
					} //remove
					
					if( $code_name != "REG" && $codevalue[$codeid] != 0 ) {
						if( $hours == 1 ) {
							$totalcodehours = $totalcodehours + $codevalue[$codeid];
						}
						
						$value = $codevalue[$codeid];
						$payValue = $value;
						$value = $value * 10000; //remove
						$len = strlen( $value );
						while( $len < 8 ) {
							$value = "0$value";
							$len++;
						} //remove
						
						$exportName = $export_name;
						$len = strlen( $export_name ); //remove
						while( $len < 10 ) {
							$export_name = "$export_name ";
							$len++;
						}
						$value = "0$value"; //remove
						
						if( $hours == 1 ) {
							//$data .= "X$empl_no                " . $value
							//		. "$export_name                                                           $export_week \r\n";
							$tmp = writePaycorLine( 'D',
									array(
										'employee' => $employeeNo,
										'coded' => $payValue,
										'specialCode' => $exportName,
										'frequency' => $export_week
									) ); //untested
							if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
								echo $tmp, PHP_EOL;
							}
							$data .= $tmp;
						}
						else {
							$value = $value / 100; //remove
							$len = strlen( $value );
							while( $len < 8 ) {
								$value = "0$value";
								$len++;
							} //remove
							//$data .= "Z$empl_no                                   $value$export_name                                         $export_week \r\n";
							$tmp = writePaycorLine( 'D',
									array(
										'employee' => $employeeNo,
										'earnings1' => $payValue,
										'earnCode1' => $exportName,
										'frequency' => $export_week
									) );
							if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
								echo $tmp, PHP_EOL;
							}
							$data .= $tmp;
						}
					}
					elseif( $codevalue[$codeid] != 0 ) {
						$totalreghours = $totalreghours + $codevalue[$codeid];
						$totalothours = $totalothours + $ot;
						
						$value = $codevalue[$codeid];
						$payValue = $value;
						$value = $value * 10000; //remove
						$len = strlen( $value );
						while( $len < 8 ) {
							$value = "0$value";
							$len++;
						} //remove
						
						$payOvertime = $ot;
						$ot = $ot * 10000; //remove
						$len = strlen( $ot );
						while( $len < 7 ) {
							$ot = "0$ot";
							$len++;
						} //remove
						
						if( empty( $rateCode ) )
							unset( $rateCode );
						
						//$data .= "X$empl_no $value$ot                                                                              $export_week \r\n";
						$tmp = writePaycorLine( 'D',
								array(
									'employee' => $employeeNo,
									'regular' => $payValue,
									'overtime' => $payOvertime,
									'frequency' => $export_week
								) ); //untested
						if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
							echo $tmp, PHP_EOL;
						}
						$data .= $tmp;
					}
					
					$temp_num--;
				}
				$totalpay = money( $totalpay );
				//echo "<td align=right><font size=2>$totalpay</td></tr>";
				
				$codevalue = array( );
				$ot = 0;
				$totalpay = 0;
			}
			
			$rate = @mysql_result( $result2, $num2, "rate" );
			
			$rate = money( $rate );
			
			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query3 = "SELECT * FROM login WHERE loginid = '$loginid'";
			if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
				echo $query3, PHP_EOL;
			}
			$result3 = Treat_DB_ProxyOld::query( $query3 );
			//mysql_close();
			
			$firstname = @mysql_result( $result3, 0, "firstname" );
			$lastname = @mysql_result( $result3, 0, "lastname" );
			$empl_no = @mysql_result( $result3, 0, "empl_no" );
			
			$temp_num = $num34 - 1;
			while( $temp_num >= 0 ) {
				$codeid = @mysql_result( $result34, $temp_num, "codeid" );
				$code_name = @mysql_result( $result34, $temp_num, "code_name" );
				$hours = @mysql_result( $result34, $temp_num, "hours" );
				
				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( "Unable to select database");
				$query3 = "SELECT * FROM labor_comm WHERE temp_commid = '$tempid' AND coded = '$codeid'";
				if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
					echo $query3, PHP_EOL;
				}
				$result3 = Treat_DB_ProxyOld::query( $query3 );
				//mysql_close();
				
				$value = @mysql_result( $result3, 0, "amount" );
				
				if( $code_name == "REG" ) {
					if( $value > 40 ) {
						$totalpay = $totalpay + ( 40 * $rate );
						$totalpay = $totalpay + ( ( $value - 40 ) * ( $rate * 1.5 ) );
						
						$codevalue[$codeid] = $codevalue[$codeid] + 40;
						$ot = $ot + ( $value - 40 );
					}
					else {
						$totalpay = $totalpay + ( $value * $rate );
						
						$codevalue[$codeid] = $codevalue[$codeid] + $value;
					}
				}
				elseif( $hours == 1 ) {
					$totalpay = $totalpay + ( $value * $rate );
					
					$codevalue[$codeid] = $codevalue[$codeid] + $value;
				}
				else {
					$totalpay = $totalpay + $value;
					
					$codevalue[$codeid] = $codevalue[$codeid] + $value;
				}
				
				$temp_num--;
			}
			
			$lastloginid = $loginid;
			$num2--;
		}
		
		$num--;
	}
	
	//mysql_close();
	
	///////////////////END COMMISSIONS
	
	$totalRegHours = $totalreghours;
	$totalreghours = money( $totalreghours ); //remove
	$totalreghours .= "00";
	while( strlen( $totalreghours ) != 13 ) {
		$totalreghours = "0$totalreghours";
	} //remove
	$totalOtHours = $totalothours;
	$totalothours = money( $totalothours ); //remove
	$totalothours .= "00";
	while( strlen( $totalothours ) != 13 ) {
		$totalothours = "0$totalothours";
	} //remove
	$totalCodeHours = $totalcodehours;
	$totalcodehours = money( $totalcodehours ); //remove
	$totalcodehours .= "00";
	while( strlen( $totalcodehours ) != 13 ) {
		$totalcodehours = "0$totalcodehours";
	} //remove
	
	//$data .= "X$totalreghours$totalothours$totalcodehours           \r\n";
	$tmp = writePaycorLine( 'T',
			array(
				'regular' => $totalRegHours,
				'overtime' => $totalOtHours,
				'coded' => $totalCodeHours
			) );
	if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
		echo $tmp, PHP_EOL;
	}
	$data .= $tmp;
	
	
	$file = "$businessname-$date1-$date2.txt";
	// Output the headers to download the file
	if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
	}
	else {
		header( "Content-type: application/x-msdownload" );
		header( "Content-Disposition: attachment; filename=\"$file\"" );
	}
	header( "Cache-Control: no-cache" );
	header( "Pragma: no-cache" );
	header( "Expires: -1" );
	echo $data;
}
?>
