<?php

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+3);
        if ($diff > 0 && $diff < '0.01'){$diff="0.01";}
        elseif($diff < 0 && $diff > '-0.01'){$diff="-0.01";}
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      if ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='32')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='08' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      $day=$day-31;  
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";} 
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

include("db.php");
$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");
$bbdayname=date("l");
$bbmonth=date("F");
$bbday=date("j");

$user=$_POST['username'];
$pass=$_POST['password'];
$reserveid=$_GET['reserveid'];
$businessid=$_GET['bid'];
if ($user==""&&$pass==""){
   $user=$_COOKIE["usercook"];
   $pass=$_COOKIE["passcook"];
}

mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = mysql_query($query);
$num=mysql_numrows($result);
mysql_close();

if ($num != 1 || $user == "" || $pass == "")
{
    echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
    $user=mysql_result($result,0,"username");
    $companyid=mysql_result($result,0,"companyid");

    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=100%><tr><td colspan=4><img src=logo.jpg><p></td></tr>";

    mysql_connect($dbhost,$username,$password);
    @mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM company WHERE companyid = '$companyid'";
    $result = mysql_query($query);
    mysql_close();

    $companyname=mysql_result($result,0,"companyname");

    mysql_connect($dbhost,$username,$password);
    @mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM business WHERE businessid = '$businessid'";
    $result = mysql_query($query);
    mysql_close();

    $taxrate=mysql_result($result,0,"tax");

    mysql_connect($dbhost,$username,$password);
    @mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM reserve WHERE reserveid = '$reserveid'";
    $result = mysql_query($query);
    $num=mysql_numrows($result);
    mysql_close();

    $date=mysql_result($result,0,"date");
    $room=mysql_result($result,0,"roomid");
    $start_hour=mysql_result($result,0,"start_hour");
    $end_hour=mysql_result($result,0,"end_hour");
    $status=mysql_result($result,0,"status");
    $comment=mysql_result($result,0,"comment");
    $roomcomment=mysql_result($result,0,"roomcomment");
    $peoplenum=mysql_result($result,0,"peoplenum");
    $accountid=mysql_result($result,0,"accountid");

    mysql_connect($dbhost,$username,$password);
    @mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM accounts WHERE accountid = '$accountid'";
    $result = mysql_query($query);
    mysql_close();

    $accountname=mysql_result($result,0,"name");
    $phone=mysql_result($result,0,"phone");
    $taxid=mysql_result($result,0,"taxid");

    echo "<tr bgcolor=black><td height=1 colspan=4></td></tr>";
    echo "<tr><td width=10%><img src=login.jpg height=75 width=80></td><td><font size=4><b>$companyname</b></font><br><b>Account: $accountname<br>Phone #: $phone</td><td colspan=2 align=right valign=top><b>$bbdayname, $bbmonth $bbday, $year</b></td></tr>";
    echo "<tr bgcolor=black><td height=1 colspan=4></td></tr>";
    echo "</table></center>";

    echo "<center><table width=100% cellspacing=0 cellpadding=0>";

    echo "<p><center><table width=100%>";
    echo "<tr><td colspan=2><b><u>Catering Details</u></b></a></td></tr>";     

    echo "<tr><td align=right width=20%><b>Date: </b></td><td><b>$date</b></td></tr>"; 
    echo "<tr><td align=right><b>Reference:</b> </td><td><b>$comment</b></td></tr>";
    echo "<tr><td align=right><b># of People:</b> </td><td><b>$peoplenum</b></td></tr>";
    
    mysql_connect($dbhost,$username,$password);
    @mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM rooms WHERE roomid = '$room'";
    $result = mysql_query($query);
    mysql_close();

    if ($room!=0){$roomname=mysql_result($result,0,"roomname");}
    else {$roomname="N/A";}
   
    echo "<tr><td align=right><b>Location: </b></td><td><b>$roomname</b></td></tr>"; 

    echo "<tr><td align=right valign=top><b>Details:</b> </td><td>$roomcomment</td></tr>";

    if (strlen($start_hour)==3){
          $showstarthour=substr($start_hour,0,1);
          $showstartmin=substr($start_hour,1,3);
          $showstartam2="SELECTED";
    }
    elseif (strlen($start_hour)==1){
          $showstarthour="00";
          $showstartmin="0$start_hour";
          $showstartam2="SELECTED";
    }
    elseif (strlen($start_hour)==2){
          $showstarthour="00";
          $showstartmin=$start_hour;
          $showstartam2="SELECTED";
    }
    else {
          $showstarthour=substr($start_hour,0,2);
          $showstartmin=substr($start_hour,2,4);
          if ($showstarthour>12){$showstarthour=$showstarthour-12;$showstartam="SELECTED";}
          elseif ($showstarthour==12){$showstartam="SELECTED";}
          else {$showstartam2="SELECTED";}
    }

    if (strlen($end_hour)==3){
          $showendhour=substr($end_hour,0,1);
          $showendmin=substr($end_hour,1,3);
          $showendam2="SELECTED";
    }
    elseif (strlen($end_hour)==1){
          $showendhour="00";
          $showendmin="0$end_hour";
          $showendam2="SELECTED";
    }
    elseif (strlen($end_hour)==2){
          $showendhour="00";
          $showendmin=$end_hour;
          $showendam2="SELECTED";
    }
    else {
          $showendhour=substr($end_hour,0,2);
          $showendmin=substr($end_hour,2,4);
          if ($showendhour>12){$showendhour=$showendhour-12;$showendam="SELECTED";}
          elseif ($showendhour==12){$showendam="SELECTED";}
          else {$showendam2="SELECTED";}
    }

    if ($showstarthour==1){$c1="SELECTED";}
    elseif ($showstarthour==2){$c2="SELECTED";}
    elseif ($showstarthour==3){$c3="SELECTED";}
    elseif ($showstarthour==4){$c4="SELECTED";}
    elseif ($showstarthour==5){$c5="SELECTED";}
    elseif ($showstarthour==6){$c6="SELECTED";}
    elseif ($showstarthour==7){$c7="SELECTED";}
    elseif ($showstarthour==8){$c8="SELECTED";}
    elseif ($showstarthour==9){$c9="SELECTED";}
    elseif ($showstarthour==10){$c10="SELECTED";}
    elseif ($showstarthour==11){$c11="SELECTED";}
    elseif ($showstarthour==12||$showstarthour==0){$c12="SELECTED";}

    if ($showendhour==1){$c13="SELECTED";}
    elseif ($showendhour==2){$c14="SELECTED";}
    elseif ($showendhour==3){$c15="SELECTED";}
    elseif ($showendhour==4){$c16="SELECTED";}
    elseif ($showendhour==5){$c17="SELECTED";}
    elseif ($showendhour==6){$c18="SELECTED";}
    elseif ($showendhour==7){$c19="SELECTED";}
    elseif ($showendhour==8){$c20="SELECTED";}
    elseif ($showendhour==9){$c21="SELECTED";}
    elseif ($showendhour==10){$c22="SELECTED";}
    elseif ($showendhour==11){$c23="SELECTED";}
    elseif ($showendhour==12 || $showendhour==0){$c24="SELECTED";}

    if ($showstartmin=="00" || $showstartmin=="0"){$c25="SELECTED";}
    elseif ($showstartmin=="05" || $showstartmin=="5"){$c26="SELECTED";}
    elseif ($showstartmin=="10"){$c27="SELECTED";}
    elseif ($showstartmin==15){$c28="SELECTED";}
    elseif ($showstartmin==20){$c29="SELECTED";}
    elseif ($showstartmin==25){$c30="SELECTED";}
    elseif ($showstartmin==30){$c31="SELECTED";}
    elseif ($showstartmin==35){$c32="SELECTED";}
    elseif ($showstartmin==40){$c33="SELECTED";}
    elseif ($showstartmin==45){$c34="SELECTED";}
    elseif ($showstartmin==50){$c35="SELECTED";}
    elseif ($showstartmin==55){$c36="SELECTED";}

    if ($showendmin=="00" || $showendmin=="0"){$c37="SELECTED";}
    elseif ($showendmin=="05" || $showendmin=="5"){$c38="SELECTED";}
    elseif ($showendmin==10){$c39="SELECTED";}
    elseif ($showendmin==15){$c40="SELECTED";}
    elseif ($showendmin==20){$c41="SELECTED";}
    elseif ($showendmin==25){$c42="SELECTED";}
    elseif ($showendmin==30){$c43="SELECTED";}
    elseif ($showendmin==35){$c44="SELECTED";}
    elseif ($showendmin==40){$c45="SELECTED";}
    elseif ($showendmin==45){$c46="SELECTED";}
    elseif ($showendmin==50){$c47="SELECTED";}
    elseif ($showendmin==55){$c48="SELECTED";}
 
    echo "<tr><td align=right><b>Start Time:</b></td><td><select NAME=start_hour><OPTION VALUE='01' $c1>1</OPTION><OPTION VALUE='02' $c2>2</OPTION><OPTION VALUE='03' $c3>3</OPTION><OPTION VALUE='04' $c4>4</OPTION><OPTION VALUE='05' $c5>5</OPTION><OPTION VALUE='06' $c6>6</OPTION><OPTION VALUE='07' $c7>7</OPTION><OPTION VALUE='08' $c8>8</OPTION><OPTION VALUE='09' $c9>9</OPTION><OPTION VALUE='10' $c10>10</OPTION><OPTION VALUE='11' $c11>11</OPTION><OPTION VALUE='12' $c12>12</OPTION></SELECT><b>:</b><select NAME=start_min><OPTION VALUE='00' $c25>00</OPTION><OPTION VALUE='05' $c26>05</OPTION><OPTION VALUE='10' $c27>10</OPTION><OPTION VALUE='15' $c28>15</OPTION><OPTION VALUE='20' $c29>20</OPTION><OPTION VALUE='25' $c30>25</OPTION><OPTION VALUE='30' $c31>30</OPTION><OPTION VALUE='35' $c32>35</OPTION><OPTION VALUE='40' $c33>40</OPTION><OPTION VALUE='45' $c34>45</OPTION><OPTION VALUE='50' $c35>50</OPTION><OPTION VALUE='55' $c36>55</OPTION></SELECT> <select NAME=start_am><OPTION VALUE='AM' $showam2>AM</OPTION><OPTION VALUE='PM' $showstartam>PM</OPTION></SELECT></td></tr>";
    echo "<tr><td align=right><b>End Time:</b></td><td><select NAME=end_hour><OPTION VALUE='01' $c13>1</OPTION><OPTION VALUE='02' $c14>2</OPTION><OPTION VALUE='03' $c15>3</OPTION><OPTION VALUE='04' $c16>4</OPTION><OPTION VALUE='05' $c17>5</OPTION><OPTION VALUE='06' $c18>6</OPTION><OPTION VALUE='07' $c19>7</OPTION><OPTION VALUE='08' $c20>8</OPTION><OPTION VALUE='09' $c21>9</OPTION><OPTION VALUE='10' $c22>10</OPTION><OPTION VALUE='11' $c23>11</OPTION><OPTION VALUE='12' $c24>12</OPTION></SELECT><b>:</b><select NAME=end_min><OPTION VALUE='00' $c37>00</OPTION><OPTION VALUE='05' $c38>05</OPTION><OPTION VALUE='10' $c39>10</OPTION><OPTION VALUE='15' $c40>15</OPTION><OPTION VALUE='20' $c41>20</OPTION><OPTION VALUE='25' $c42>25</OPTION><OPTION VALUE='30' $c43>30</OPTION><OPTION VALUE='35' $c44>35</OPTION><OPTION VALUE='40' $c45>40</OPTION><OPTION VALUE='45' $c46>45</OPTION><OPTION VALUE='50' $c47>50</OPTION><OPTION VALUE='55' $c48>55</OPTION></SELECT> <select NAME=end_am><OPTION VALUE='AM' $showendam2>AM</OPTION><OPTION VALUE='PM' $showendam>PM</OPTION></SELECT></td></tr>";

    echo "<tr><td colspan=2><p><br><p><center><table border=0 bgcolor=white width=100%>";
    echo "<tr><td colspan=4><a name='invoice'></a><center><h2><u>CATERING ITEMS</u></h2></td></tr>";
    echo "<tr><td align=right><h5><u>QTY</u></h5></td><td><h5><u>ITEM DESCRIPTION</u></h5></td><td align=right><h5><u>UNIT PRICE</u></h5></td><td align=right><h5><u>TOTAL</u></h5></td></tr>";

       mysql_connect($dbhost,$username,$password);
       @mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM invoicedetail WHERE reserveid = '$reserveid'";
       $result = mysql_query($query);
       $num=mysql_numrows($result);
       mysql_close();

       $num--;
       $total2=0;
       $subtotal=0;
       $taxtotal=0;
       while ($num>=0){
          $itemid=mysql_result($result,$num,"itemid");
          $qty=mysql_result($result,$num,"qty");
          $item=mysql_result($result,$num,"item");
          $price=mysql_result($result,$num,"price");
          $taxed=mysql_result($result,$num,"taxed");
          $detail=mysql_result($result,$num,"detail");
          $total=money($price*$qty);
          $price=money($price);
          if ($taxed==""){$item="$item <font color=red>(Non-Taxed)</font>";}
          echo "<tr><td align=right>$qty</td><td>$item</td><td align=right>$$price</td><td align=right>$$total</td></tr>";
          if ($detail!=""){echo "<tr><td></td><td><li>$detail</td><td></td><td></td></tr>";}
          $num--;
          $total2=$total2+$total;
          $subtotal=$subtotal+($qty*$price);
       }
       $subtotal=money($subtotal);
       echo "<tr><td></td><td></td><td><b>SUBTOTAL:</b></td><td align=right><b>$$subtotal</b></td></tr>";
       if ($taxid==""){
          $taxtotal=$subtotal*($taxrate/100); 
          $taxtotal=money($taxtotal); 
          echo "<tr><td></td><td></td><td><b>TAXES:</b></td><td align=right><b>$$taxtotal</b></td></tr>";
       }
       $srvcharge=$subtotal*(15/100);
       $srvcharge=money($srvcharge);
       echo "<tr><td></td><td></td><td><b>SRV CHARGE:</b></td><td align=right><b>$$srvcharge</b></td></tr>";
       $grandtotal=money($subtotal+$taxtotal+$srvcharge);
       echo "<tr><td></td><td></td><td><b>TOTAL:</b></td><td align=right><b>$$grandtotal</b></td></tr>";

    mysql_connect($dbhost,$username,$password);
    @mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM menu_items WHERE businessid = '$businessid' ORDER BY item_name DESC";
    $result = mysql_query($query);
    $num=mysql_numrows($result);
    mysql_close();
       
    echo "</table></td></tr>";

    echo "</table></center>";

}
?>