<?php

define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once(DOC_ROOT.'/bootstrap.php');

if ( require_once( realpath( DOC_ROOT . '/../lib/inc/cookie-login.php' ) ) ) {
		$loginid = $GLOBALS["loginid"];

		$companyname = $_POST["companyname"];
		$reference = $_POST["reference"];
		$street = $_POST["street"];
		$city = $_POST["city"];
		$state = $_POST["state"];
		$zip = $_POST["zip"];
		$phone = $_POST["phone"];
		$operatorid = $_POST["operatorid"];
		$vendornum = $_POST["vendornum"];
		$week_end = $_POST["week_end"];

		if($week_end == "Monday"){$week_start = "Tuesday";}
		elseif($week_end == "Tuesday"){$week_start = "Wednesday";}
		elseif($week_end == "Wednesday"){$week_start = "Thursday";}
		elseif($week_end == "Thursday"){$week_start = "Friday";}
		elseif($week_end == "Friday"){$week_start = "Saturday";}
		elseif($week_end == "Saturday"){$week_start = "Sunday";}
		elseif($week_end == "Sunday"){$week_start = "Monday";}
		
		////create vendor
		$query = "INSERT INTO vendor_master (companyid,vendor_name,vendor_code,street,city,state,zip,phone)
					VALUES (7,'$companyname','$vendornum','$street','$city','$state','$zip','$phone')";
		$result = DB_Proxy::query($query);
		$master_vendor_id = mysql_insert_id();

		////insert company
        $query = "INSERT INTO company (company_type,companyname,reference,street,city,state,zip,phone,week_end,week_start,invoice_name,invoice_remit,logo,backdrop,master_vendor_id,company_code) VALUES
									(4,'$companyname','$reference','$street','$city','$state','$zip','$phone','$week_end','$week_start','$companyname','$companyname','ck.png','ckback.gif',$master_vendor_id,'$operatorid')";
        $result = DB_Proxy::query($query);
		$companyid = mysql_insert_id();

		////insert division
		$query = "INSERT INTO division (division_name,companyid) VALUES ('Division',$companyid)";
		$result = DB_Proxy::query($query);
		$divisionid = mysql_insert_id();

		////insert district
		$query = "INSERT INTO district (companyid,divisionid,districtname) VALUES ($companyid,$divisionid,'District')";
		$result = DB_Proxy::query($query);
		$districtid = mysql_insert_id();

		////insert vending operations/master unit
		$query = "INSERT INTO business (companyid,districtid,businessname,companyname,street,city,state,zip,phone,unit,categoryid)
					VALUES
					($companyid,$districtid,'$companyname Operations','$companyname','$street','$city','$state','$zip','$phone','$operatorid',4)";
		$result = DB_Proxy::query($query);
		$businessid = mysql_insert_id();

		/////default GL for operations/master unit
		$query = "INSERT INTO credits (businessid,companyid,creditname,credittype,account,category)
					VALUES
					($businessid,$companyid,'Beverage',1,'40000',8),
					($businessid,$companyid,'Snack',1,'40000',8),
					($businessid,$companyid,'Cold Food',1,'40000',8),
					($businessid,$companyid,'Tax',2,'21400',7),
					($businessid,$companyid,'CK Card Online CC Payments',6,'12830',21),
					($businessid,$companyid,'CK Card Refunds',6,'12830',24),
					($businessid,$companyid,'CK Card Promos',6,'12830',23),
					($businessid,$companyid,'CK Card Kiosk Cash Payments',6,'12830',25),
					($businessid,$companyid,'CK Card Kiosk CC Payments',6,'12830',22)";
		$result = DB_Proxy::query($query);

		$query = "INSERT INTO debits (businessid,companyid,debitname,debittype,account,category)
					VALUES
					($businessid,$companyid,'Kiosk Credit Card Purchase',1,'12830',1),
					($businessid,$companyid,'CK Online CC Payments',1,'12830',12),
					($businessid,$companyid,'CK Kiosk CC Payments',1,'12830',11),
					($businessid,$companyid,'Cash',3,'11000',10),
					($businessid,$companyid,'CK Card Purchases',5,'12830',2),
					($businessid,$companyid,'Payroll Deduct',5,'12830',6),
					($businessid,$companyid,'CK Card Refunds',5,'41800',4),
					($businessid,$companyid,'CK Card Promos',5,'52300',3),
					($businessid,$companyid,'Promotions - Operators',5,'52300',3),
					($businessid,$companyid,'Promotions - Manufacturers',5,'12830',9)";
		$result = DB_Proxy::query($query);

		////insert vend location
		$query = "INSERT INTO vend_locations (location_name,businessid,companyid,unitid,op_unitid)
					VALUES
					('$companyname',$businessid,$companyid,$businessid,$businessid)";
		$result = DB_Proxy::query($query);
		$locationid = mysql_insert_id();

		////insert default route
		$operatorid .= "000";
		$query = "INSERT INTO login_route (username,password,firstname,lastname,route,truck,businessid,companyid,locationid,sec_level,order_by_machine)
					VALUES
					('$operatorid','2011','NA','NA','$operatorid','NA',$businessid,$companyid,$locationid,1,1)";
		$result = DB_Proxy::query($query);

		////company security
		$query = "INSERT INTO login_company (loginid,companyid) VALUES (53,$companyid),
																		(1,$companyid),
																		(149,$companyid),
																		(4830,$companyid),
																		(3822,$companyid),
																		(3155,$companyid),
																		(4903,$companyid),
																		(3352,$companyid),
																		(5222,$companyid),
																		(3895,$companyid),
																		(4776,$companyid),
																		(5688,$companyid),
																		(192,$companyid),
																		(4542,$companyid),
																		(5871,$companyid),
																		(3833,$companyid),
																		(5232,$companyid),
																		(5868,$companyid),
																		(5970,$companyid),
																		(7266,$companyid),
																		(8230,$companyid),
																		(8561,$companyid),
																		(8580,$companyid),
																		(8581,$companyid),
																		(8582,$companyid),
																		(8677,$companyid),
																		(8143,$companyid),
																		(7857,$companyid)
		";
		$result = DB_Proxy::query($query);

		////district security
		$query = "INSERT INTO login_district (loginid,districtid) VALUES (53,$districtid),
																		(1,$districtid),
																		(149,$districtid),
																		(4830,$districtid),
																		(3822,$districtid),
																		(3155,$districtid),
																		(4903,$districtid),
																		(3352,$districtid),
																		(5222,$districtid),
																		(3895,$districtid),
																		(4776,$districtid),
																		(4542,$districtid)
		";
		$result = DB_Proxy::query($query);

		$sanitizedCompanyName = mysql_real_escape_string($companyname);
		$clientProgram = \EE\Model\ClientPrograms::get("`name` = '{$sanitizedCompanyName}'", true);

		if( ! $clientProgram )
		{
			$clientProgram = new \EE\Model\ClientPrograms;
			$clientProgram->name = $sanitizedCompanyName;
			$clientProgram->businessid = $businessid;
			$clientProgram->pos_type = 0;
			$clientProgram->save();
		}

		////send email
				$email = "smartin@essentialpos.com";
				$email_subject = "New Operator Setup";
				$email_message = "Operator $companyname has been setup.<p>$user_cook";

				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				$headers .= "From: support@companykitchen.com\r\n";
				mail($email, $email_subject, $email_message, $headers);

        header( 'Location: ' . $_SERVER[ 'HTTP_REFERER' ]);
}
?>