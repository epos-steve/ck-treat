<?php

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+3);
        if ($diff > 0 && $diff < '0.01'){$diff="0.01";}
        elseif($diff < 0 && $diff > '-0.01'){$diff="-0.01";}
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      if ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='32')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='08' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      $day=$day-31;  
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";} 
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}


if ( !defined('DOC_ROOT'))
{
	define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
}
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');
$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");
$creditamount="creditamount";
$creditidnty="creditidnty";
$creditdate="creditdate";
$debitamount="debitamount";
$debitidnty="debitidnty";
$debitdate="debitdate";
$quatertotal=0;
$monthtotal=0;
$total=0;
$counter=0;
$findme='.';
$double='.00';
$single='0';
$double2='00';

/*$user = isset($_COOKIE["usercook"])?$_COOKIE["usercook"]:'';
$pass = isset($_COOKIE["passcook"])?$_COOKIE["passcook"]:'';
$view = isset($_COOKIE["viewcook"])?$_COOKIE["viewcook"]:'';
$date1 = isset($_COOKIE["date1cook"])?$_COOKIE["date1cook"]:'';
$date2 = isset($_COOKIE["date2cook"])?$_COOKIE["date2cook"]:'';
$businessid = isset($_GET["bid"])?$_GET["bid"]:'';
$companyid = isset($_GET["cid"])?$_GET["cid"]:'';
$view1 = isset($_GET["view1"])?$_GET["view1"]:'';
$itemeditid = isset($_GET["mid"])?$_GET["mid"]:'';*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook','');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook','');
$view = \EE\Controller\Base::getSessionCookieVariable('viewcook','');
$date1 = \EE\Controller\Base::getSessionCookieVariable('date1cook','');
$date2 = \EE\Controller\Base::getSessionCookieVariable('date2cook','');
$businessid = \EE\Controller\Base::getGetVariable('bid','');
$companyid = \EE\Controller\Base::getGetVariable('cid','');
$view1 = \EE\Controller\Base::getGetVariable('view1','');
$itemeditid = \EE\Controller\Base::getGetVariable('mid','');

if ($businessid==""&&$companyid==""){
   /*$businessid = isset($_POST["businessid"])?$_POST["businessid"]:'';
   $companyid = isset($_POST["companyid"])?$_POST["companyid"]:'';
   $view1 = isset($_POST["view1"])?$_POST["view1"]:'';
   $date1 = isset($_POST["date1"])?$_POST["date1"]:'';
   $date2 = isset($_POST["date2"])?$_POST["date2"]:'';
   $findinv = isset($_POST["findinv"])?$_POST["findinv"]:'';*/
	
	$businessid = \EE\Controller\Base::getPostVariable('businessid','');
	$companyid = \EE\Controller\Base::getPostVariable('companyid','');
	$view1 = \EE\Controller\Base::getPostVariable('view1','');
	$date1 = \EE\Controller\Base::getPostVariable('date1','');
	$date2 = \EE\Controller\Base::getPostVariable('date2','');
	$findinv = \EE\Controller\Base::getPostVariable('findinv','');
}


/*mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");*/

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=Treat_DB_ProxyOld::mysql_numrows($result);
//mysql_close();

$security_level=@Treat_DB_ProxyOld::mysql_result($result,0,"security_level");
$security=@Treat_DB_ProxyOld::mysql_result($result,0,"security");
$bid=@Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
$cid=@Treat_DB_ProxyOld::mysql_result($result,0,"companyid");

if ($num != 1 || ($security_level==1 && ($bid != $businessid || $cid != $companyid)) || ($security_level > 1 AND $cid != $companyid) || $user == "" || $pass == "")
{
    echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
	$query = "SELECT * FROM security WHERE securityid = '$security'";
	$result = Treat_DB_ProxyOld::query($query);
	
	$sec_item_list=@Treat_DB_ProxyOld::mysql_result($result,0,"item_list");
	$sec_nutrition=@Treat_DB_ProxyOld::mysql_result($result,0,"nutrition");
	
	if($sec_item_list<1&&$sec_nutrition<1){
		$location="businesstrack.php";
		header('Location: ./' . $location);
	}
	elseif($sec_item_list<1&&$sec_nutrition>0){
		$location="caternutrition.php?cid=$companyid";
		header('Location: ./' . $location);
	}
	
?>
<head>
<SCRIPT LANGUAGE=javascript><!--
function delconfirm(){return confirm('Are you sure you want to delete this menu?');}
// --></SCRIPT>
</head>
<?

    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%><tr><td colspan=2>";
    echo "<a href=businesstrack.php><img src=logo.jpg border=0 height=43 width=205></a><p></td></tr>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM company WHERE companyid = '$companyid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $companyname=@Treat_DB_ProxyOld::mysql_result($result,0,"companyname");

    //echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";
    echo "<tr bgcolor=#CCCCFF><td colspan=2><font size=4><b>Menu Setup - $companyname</b></font></td>";
    echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";
    echo "<tr><td colspan=2><a href=catermenu.php?bid=$bid&cid=$cid><font color=blue>Menu Items</font></a> :: <a href=catergroup.php?cid=$companyid><font color=blue>Menu Groups</font></a> :: Menu Types :: <a href=caternutrition.php?cid=$companyid><font color=blue>Nutrition</font></a></td></tr>";
    echo "</table></center><p>";

/////////////////////////////////////////////////////////////////////////////////
    echo "<a name='item'></a>";
    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%>";
    //echo "<tr bgcolor=black><td height=1 colspan=4></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td height=1 colspan=4><b>Menu Types</b></td></tr>";
    echo "<tr bgcolor=black><td height=1 colspan=4></td></tr>";
    echo "<tr bgcolor=#CCCCCC><td height=1 colspan=4></td></tr>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM menu_type WHERE companyid = '$companyid' ORDER BY type DESC, menu_typename DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    $lastgroup="";
    $num--;
    while ($num>=0){
       $menu_typeid=@Treat_DB_ProxyOld::mysql_result($result,$num,"menu_typeid");
       $menu_typename=@Treat_DB_ProxyOld::mysql_result($result,$num,"menu_typename");
       $deleted=@Treat_DB_ProxyOld::mysql_result($result,$num,"deleted");
       $menutype=@Treat_DB_ProxyOld::mysql_result($result,$num,"type");
       $parent=@Treat_DB_ProxyOld::mysql_result($result,$num,"parent");
       $static_menu=@Treat_DB_ProxyOld::mysql_result($result,$num,"static_menu");

       if ($menutype==1){$showmenu="<i> - CONTRACT</i>";}
       elseif ($menutype==2){$showmenu="<i> - VENDING</i>";}
       else{$showmenu="";}

       if($static_menu==1){$showmenu .= " <i>STATIC</i>";}
       else{}

       if($parent>0){
          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query23 = "SELECT menu_typename FROM menu_type WHERE menu_typeid = '$parent'";
          $result23 = Treat_DB_ProxyOld::query($query23);
          //mysql_close();

          $showmenu2=@Treat_DB_ProxyOld::mysql_result($result23,0,"menu_typename");

          $showmenu2="<i>(Linked to $showmenu2)</i>";
       }
       else{$showmenu2="";}

       $showedit="<a href=catertype.php?cid=$companyid&bid=$businessid&view1=edit&mid=$menu_typeid#item><img border=0 src=edit.gif height=16 width=16 alt='EDIT GROUP'></a>";
       $deletegroup="<a href=catertypeedit.php?cid=$companyid&bid=$businessid&edit=3&editid=$menu_typeid onclick='return delconfirm()'><img border=0 src=delete.gif height=16 width=16 alt='DELETE MENU'></a>";
       if ($deleted==0){$showdelete="<a href=catertypeedit.php?cid=$companyid&bid=$businessid&edit=2&editid=$menu_typeid><img border=0 src=on.gif height=15 width=15 alt='ACTIVE'></a>";}
       else{$showdelete="<a href=catertypeedit.php?cid=$companyid&bid=$businessid&edit=2&editid=$menu_typeid&on=on><img border=0 src=off.gif height=15 width=15 alt='INACTIVE'></a>";}

       echo "<tr bgcolor='white' onMouseOver=this.bgColor='#999999' onMouseOut=this.bgColor='white'><td width=25%>$menu_typename $showmenu $showmenu2</td><td width=4%>$showdelete</td><td width=4%>$showedit</td><td width=4%>$deletegroup</td></tr>";
       echo "<tr bgcolor=#CCCCCC><td height=1 colspan=4></td></tr>";

       $num--;
    }
  
    echo "<tr bgcolor=black><td height=1 colspan=4></td></tr></table></center>";

    if ($view1=="edit"){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM menu_type WHERE menu_typeid = '$itemeditid'";
       $result2 = Treat_DB_ProxyOld::query($query2);
       //mysql_close();

       $editname=@Treat_DB_ProxyOld::mysql_result($result2,0,"menu_typename");
       $grouptypeid=@Treat_DB_ProxyOld::mysql_result($result2,0,"menu_typeid");
       $edittype=@Treat_DB_ProxyOld::mysql_result($result2,0,"type");
       $busid=@Treat_DB_ProxyOld::mysql_result($result2,0,"businessid");
       $menu_parent=@Treat_DB_ProxyOld::mysql_result($result2,0,"parent");
       $static_menu=@Treat_DB_ProxyOld::mysql_result($result2,0,"static_menu");

       if ($edittype==1){$opt1="SELECTED";}
       elseif ($edittype==2){$opt2="SELECTED";}

       if($static_menu==1){$sel_static="CHECKED";}

       echo "<center><table width=90% bgcolor='#E8E7E7'><tr><td colspan=2><a name='item'></a><form action=catertypeedit.php method=post></td></tr>";
       echo "<tr><td colspan=2><INPUT TYPE=hidden name=companyid value=$companyid><INPUT TYPE=hidden name=businessid value=$businessid><INPUT TYPE=hidden name=editid value=$itemeditid><INPUT TYPE=hidden name=edit value=1><b><font color=red>*</font><u>Edit Menu</u></b></td></tr>";
       echo "<tr><td align=right width=10%>Menu Name:</td><td><INPUT TYPE=text name=item_name value='$editname' size=30> <select name=menutype><option value=0 $opt0>Catering</option><option value=1 $opt1>Contract</option><option value=2 $opt2>Vending</option></select> <select name=businessid><option value=0>All Units</option>";
      
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM business WHERE companyid = '$companyid' ORDER BY businessname DESC";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
       //mysql_close();

       $num2--;
       while($num2>=0){
          $businessname=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"businessname");
          $businessid=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"businessid");

          if ($busid==$businessid){$thissel="SELECTED";}
          else{$thissel="";}

          echo "<option value=$businessid $thissel>$businessname</option>";

          $num2--;
       }

       echo "</select> Link to: <select name=parent>";

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM menu_type ORDER BY menu_typename DESC";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
       //mysql_close();

       while($num2>=0){
          $menu_typeid=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"menu_typeid");
          $menu_typename=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"menu_typename");

          if($menu_parent==$menu_typeid){$sel1="SELECTED";}
          else{$sel1="";}

          echo "<option value=$menu_typeid $sel1>$menu_typename</option>";

          $num2--;
       }

       echo "</select> <input type=checkbox name=static_menu value=1 $sel_static>Static Menu";

       echo "</td></tr>";
       echo "<tr><td></td><td><INPUT TYPE=submit value='Save'></form></td></tr></table></center>";
    }
    else {
       echo "<center><table width=90% bgcolor='#E8E7E7'><tr><td colspan=2><form action=catertypeedit.php method=post></td></tr>";
       echo "<tr><td colspan=2><INPUT TYPE=hidden name=companyid value=$companyid><INPUT TYPE=hidden name=businessid value=$businessid><b><u>Add Menu</u></b></td></tr>";
       echo "<tr><td align=right width=10%>Menu Name:</td><td><INPUT TYPE=text name=item_name size=30> <select name=menutype><option value=0>Catering</option><option value=1>Contract</option><option value=2>Vending</option></select> <select name=businessid><option value=0>All Units</option>";

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM business WHERE companyid = '$companyid' ORDER BY businessname DESC";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
       //mysql_close();

       $num2--;
       while($num2>=0){
          $businessname=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"businessname");
          $businessid=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"businessid");

          echo "<option value=$businessid>$businessname</option>";

          $num2--;
       }

       echo "</select> Link to: <select name=parent>";

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM menu_type ORDER BY menu_typename DESC";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
       //mysql_close();

       while($num2>=0){
          $menu_typeid=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"menu_typeid");
          $menu_typename=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"menu_typename");

          echo "<option value=$menu_typeid>$menu_typename</option>";

          $num2--;
       }

       echo "</select> <input type=checkbox name=static_menu value=1>Static Menu";

       echo "</td></tr>";
       echo "<tr><td></td><td><INPUT TYPE=submit value='Add Menu'></form></td></tr></table></center>";
    }
//////END TABLE////////////////////////////////////////////////////////////////////////////////////

    echo "<br><FORM ACTION=businesstrack.php method=post>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<center><INPUT TYPE=submit VALUE=' Return to Main Page'></FORM></center>";
	
	google_page_track();
}
//mysql_close();
?>