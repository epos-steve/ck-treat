<?php

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+3);
        if ($diff > 0 && $diff < '0.01'){$diff="0.01";}
        elseif($diff < 0 && $diff > '-0.01'){$diff="-0.01";}
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      elseif ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      elseif ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='32')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      $day=$day-31;  
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";} 
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

include("db.php");
$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");
$creditamount="creditamount";
$creditidnty="creditidnty";
$creditdate="creditdate";
$debitamount="debitamount";
$debitidnty="debitidnty";
$debitdate="debitdate";
$quatertotal=0;
$monthtotal=0;
$total=0;
$counter=0;
$findme='.';
$double='.00';
$single='0';
$double2='00';

$user=$_COOKIE["usercook"];
$pass=$_COOKIE["passcook"];
$view=$_COOKIE["viewcook"];
$sorting=$_COOKIE["sortcook"];
$date1=$_COOKIE["date1cook"];
$date2=$_COOKIE["date2cook"];
$findaccount=$_COOKIE["acctcook"];
$businessid=$_GET["bid"];
$companyid=$_GET["cid"];
$error=$_GET["error"];
$help=$_GET["help"];

$depositid=$_COOKIE["depositid"];

if ($businessid==""&&$companyid==""){
   $businessid=$_POST["businessid"];
   $companyid=$_POST["companyid"];
   $view=$_POST["view"];
   $date1=$_POST["date1"];
   $date2=$_POST["date2"];
   $findinv=$_POST["findinv"];
   $findamount=$_POST["findamount"];
   $findaccount=$_POST["findaccount"];
   $sorting=$_POST["sorting"];
}

if ($date2==""||$date1==""){
   $date2=$today;$date1=$today;$view="All";$sorting="invoiceid";
}

if ($sorting=="submit"||$sorting=="unsubmit"||$sorting=="invoicenum"||$sorting=="master_vendor_id"){$sorting="invoiceid";}

mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = mysql_query($query);
$num=mysql_numrows($result);

$security_level=mysql_result($result,0,"security_level");
$bid=mysql_result($result,0,"businessid");
$cid=mysql_result($result,0,"companyid");

if ($num != 1 || ($security_level==1 && ($bid != $businessid || $cid != $companyid)) || ($security_level > 1 AND $cid != $companyid) || ($user == "" && $pass == ""))
{
    echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
    setcookie("date1cook",$date1);
    setcookie("date2cook",$date2);
?>

<html>
<head>

<STYLE>
#loading {
 	width: 200px;
 	height: 48px;
 	background-color: ;
 	position: absolute;
 	left: 50%;
 	top: 50%;
 	margin-top: -50px;
 	margin-left: -100px;
 	text-align: center;
}
</STYLE>

<script type="text/javascript" src="preLoadingMessage.js"></script>

<script type="text/javascript" src="public_smo_scripts.js"></script>

<script type="text/javascript">    
var szColorTo = '#00FF00';    
var szColorOriginal = '';        
function change_row_colour(pTarget)    {       
  var pTR = pTarget.parentNode;               
  if(pTR.nodeName.toLowerCase() != 'td')        {            return;        } 
     if(pTarget.checked == true)        {            pTR.style.backgroundColor = szColorTo;        }   
     else        {            pTR.style.backgroundColor = szColorOriginal;        }    
}

</script>

<SCRIPT LANGUAGE="JavaScript" SRC="CalendarPopup.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">document.write(getCalendarStyles());</SCRIPT>

<STYLE>
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation
			{
			background-color:#6677DD;
			text-align:center;
			vertical-align:center;
			text-decoration:none;
			color:#FFFFFF;
			font-weight:bold;
			}
	.TESTcpDayColumnHeader,
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation,
	.TESTcpCurrentMonthDate,
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDate,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDate,
	.TESTcpCurrentDateDisabled,
	.TESTcpTodayText,
	.TESTcpTodayTextDisabled,
	.TESTcpText
			{
			font-family:arial;
			font-size:8pt;
			}
	TD.TESTcpDayColumnHeader
			{
			text-align:right;
			border:solid thin #6677DD;
			border-width:0 0 1 0;
			}
	.TESTcpCurrentMonthDate,
	.TESTcpOtherMonthDate,
	.TESTcpCurrentDate
			{
			text-align:right;
			text-decoration:none;
			}
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDateDisabled
			{
			color:#D0D0D0;
			text-align:right;
			text-decoration:line-through;
			}
	.TESTcpCurrentMonthDate
			{
			color:#6677DD;
			font-weight:bold;
			}
	.TESTcpCurrentDate
			{
			color: #FFFFFF;
			font-weight:bold;
			}
	.TESTcpOtherMonthDate
			{
			color:#808080;
			}
	TD.TESTcpCurrentDate
			{
			color:#FFFFFF;
			background-color: #6677DD;
			border-width:1;
			border:solid thin #000000;
			}
	TD.TESTcpCurrentDateDisabled
			{
			border-width:1;
			border:solid thin #FFAAAA;
			}
	TD.TESTcpTodayText,
	TD.TESTcpTodayTextDisabled
			{
			border:solid thin #6677DD;
			border-width:1 0 0 0;
			}
	A.TESTcpTodayText,
	SPAN.TESTcpTodayTextDisabled
			{
			height:20px;
			}
	A.TESTcpTodayText
			{
			color:#6677DD;
			font-weight:bold;
			}
	SPAN.TESTcpTodayTextDisabled
			{
			color:#D0D0D0;
			}
	.TESTcpBorder
			{
			border:solid thin #6677DD;
			}
</STYLE>

<SCRIPT LANGUAGE=javascript><!--
function delconfirm(){return confirm('Are you sure you want to delete this check?');}
// --></SCRIPT>

<SCRIPT LANGUAGE=javascript><!--
function delconfirm2(){return confirm('Are you sure you want to delete this deposit?');}
// --></SCRIPT>

<SCRIPT LANGUAGE=javascript><!--
function submitconfirm(){
   return confirm('Are you sure you want to Submit this deposit?');
   
}
// --></SCRIPT>

</head>

<?
    echo "<body>";
    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%><tr><td colspan=2>";
    echo "<a href=businesstrack.php><img src=logo.jpg border=0 height=43 width=205></a><p></td></tr>";

    $query = "SELECT * FROM company WHERE companyid = '$companyid'";
    $result = mysql_query($query);

    $companyname=mysql_result($result,0,"companyname");

    //echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";
    echo "<tr bgcolor=#CCCCFF><td colspan=2><font size=4><b>Invoice Payments - $companyname</b></font></td>";
    echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";
    echo "<tr><td colspan=2><a href=invoice.php?cid=$companyid><font color=blue>Invoice List</font></a> :: <a href=invoice2.php?cid=$companyid><font color=blue>Aging</font></a> :: Payment Detail</td></tr>";
    echo "</table></center><p>";

    if($help==1){
       echo "<center><table width=90% cellspacing=0 cellpadding=0><tr><td align=right><a href=invoice3.php?cid=$companyid&help=0 style=$style><font color=blue size=2>Hide</font></a></td></tr></table></center>";
       echo "<center><table width=90% cellspacing=0 cellpadding=0 style=\"border:1px solid black;\" bgcolor=#E8E7E7><tr><td><font size=2>1. Start a new deposit by typing in a check number, invoice number or numbers and a total<br>2.  Add invoices to an existing check by entering only the check number and invoice number.<br>3. Add an invoice to the last check displayed by typing in only an invoice number.<br>4. Add more checks to the deposit by typing in a check number, invoice number or numbers and a total.<br>5. Submit the deposit for the correct day when finished.</td></tr></table></center><p>";
    }
    else{
       echo "<center><table width=90% cellspacing=0 cellpadding=0><tr><td align=right><a href=invoice3.php?cid=$companyid&help=1 style=$style><font color=blue size=2>Help</font></a></td></tr></table></center>";
    }
///////////////////////////////////////////////////////////////////////////////////////////////////

    if($depositid>0){
       if($error>0){
          if($error==1){$showmyerror="<font color=red><b>Invoice Not Found</b></font>";}
          elseif($error==2){$showmyerror="<font color=red><b>Check Not Found</b></font>";}
          echo "<center><table width=90%><tr><td>$showmyerror</td></tr></table></center>";
       }   

       echo "<center><table width=90% border=0 cellspacing=0 cellpadding=0 style=\"border:1px solid black;\">";
       echo "<tr bgcolor=#E8E7E7><td><font size=2><b>Check#</td><td><font size=2><b>Invoice#</td><td><font size=2><b>Account</td><td><font size=2><b>Business</td><td><font size=2><b>Date</td><td align=right><font size=2><b>Invoice$</td><td align=right><font size=2><b>Total</td><td align=right></td></tr>";
       echo "<tr bgcolor=black height=1><td colspan=8></td></tr>";

       $query = "SELECT * FROM deposit WHERE depositid = '$depositid'";
       $result = mysql_query($query);

       $date=mysql_result($result,0,"date");
       $bywho=mysql_result($result,0,"user");
       $export=mysql_result($result,0,"export");
       $acctnum=mysql_result($result,0,"account");

       if($export==1){$showsubmit="<font color=white><b>SUBMITTED</b></font>";}
       elseif($export==2){$showsubmit="<font color=white><b>EXPORTED</b></font>";}

       if($export>0&&$security_level<9){$submit_disable="DISABLED";}

       if($export==0||$security_level>8){$showdeletedeposit="<a href=deposit.php?depositid=$depositid&cid=$companyid&del2=1 onclick='return delconfirm2()'><img src=delete.gif height=16 width=16 border=0 alt='DELETE THIS DEPOSIT'></a>";}

       $query = "SELECT * FROM depositdetail WHERE depositid = '$depositid' ORDER BY detailid DESC";
       $result = mysql_query($query);
       $num=mysql_numrows($result);

       $showcolor="white";

       $deposittotal=0;

       $num--;
       while($num>=0){
          $counter=1;
          $errormessage="";
          $checktotal=0;

          $detailid=mysql_result($result,$num,"detailid");
          $checknum=mysql_result($result,$num,"checknum");
          $total=mysql_result($result,$num,"total");

          $lastcheck=$detailid;

          $deposittotal=money($deposittotal+$total);

          $query2 = "SELECT * FROM depositinvoice WHERE detailid = '$detailid'";
          $result2 = mysql_query($query2);
          $num2=mysql_numrows($result2);

          $num2--;
          while($num2>=0){
             $invoiceid=mysql_result($result2,$num2,"invoiceid");
             $type=mysql_result($result2,$num2,"type");

             if($type==1){
                $query3 = "SELECT * FROM invoice WHERE invoiceid = '$invoiceid'";
                $result3 = mysql_query($query3);

                $accountid=mysql_result($result3,0,"accountid");
                $businessid=mysql_result($result3,0,"businessid");
                $invdate=mysql_result($result3,0,"date");
                $cleared=mysql_result($result3,0,"cleared");
                $invtotal=mysql_result($result3,0,"total");
                $invtotal=money($invtotal);

                $query3 = "SELECT name,accountnum FROM accounts WHERE accountid = '$accountid'";
                $result3 = mysql_query($query3);

                $acct_name=mysql_result($result3,0,"name");
                $accountnum=mysql_result($result3,0,"accountnum");

                $query3 = "SELECT businessname FROM business WHERE businessid = '$businessid'";
                $result3 = mysql_query($query3);

                $businessname=mysql_result($result3,0,"businessname");

                if($cleared==1&&$export==0){$errormessage="Invoice Not Outstanding";$invcolor="red";}
                else{$invcolor="black";}
             }
             elseif($type==2){
                $query3 = "SELECT * FROM invoice2 WHERE invoiceid2 = '$invoiceid'";
                $result3 = mysql_query($query3);

                $invoice_num=mysql_result($result3,0,"invoice_num");
                $businessid=mysql_result($result3,0,"businessid");
                $invdate=mysql_result($result3,0,"date");
                $acct_name=mysql_result($result3,0,"cust_name");
                $accountnum=mysql_result($result3,0,"cust_num");
                $status=mysql_result($result3,0,"status");
                $invtotal=mysql_result($result3,0,"total");
                $invtotal=money($invtotal);

                $query3 = "SELECT businessname FROM business WHERE businessid = '$businessid'";
                $result3 = mysql_query($query3);

                $businessname=mysql_result($result3,0,"businessname");

                $invoiceid=$invoice_num;

                if($status!=1&&$export==0){$errormessage="Invoice Not Outstanding";$invcolor="red";}
                else{$invcolor="black";}
             }
             else{$invoiceid="";$acct_name="";$accountnum="<i><font color=red>No Account</font></i>";$businessname="";}

             $checktotal=money($checktotal+$invtotal);

             if($export==0||$security_level>8){$showdelete="<a href=deposit.php?detailid=$detailid&cid=$companyid&del=1 onclick='return delconfirm()'><img src=delete.gif height=16 width=16 border=0 alt='DELETE'></a>";}

             $total=money($total);
             if($counter==1){echo "<tr bgcolor=$showcolor><td>$checknum</td><td><font size=2 color=$invcolor>$invoiceid</td><td><font size=2>($accountnum) $acct_name</td><td><font size=2>$businessname</td><td><font size=2>$invdate</td><td align=right><font size=2>$$invtotal</td><td align=right>$$total</td><td align=right>$showdelete</td></tr>";}
             else{echo "<tr bgcolor=$showcolor><td></td><td><font size=2 color=$invcolor>$invoiceid</td><td><font size=2>($accountnum) $acct_name</td><td><font size=2>$businessname</td><td><font size=2>$invdate</td><td align=right><font size=2>$$invtotal</td><td align=right></td><td></td></tr>";}

             if($num2==0&&$checktotal!=$total){
                if($errormessage!=""){$errormessage="$errormessage, Check Total Does Not Match Invoice Total";}
                else{$errormessage="Check Total Does Not Match Invoice Total";}
             }

             if($num2==0&&$errormessage!=""){echo "<tr bgcolor=$showcolor><td colspan=7 align=right><font color=red>$errormessage</td><td></td></tr>";}

             $counter++;
             $num2--;
          }

          if($showcolor=="white"){$showcolor="#E8E7E7";}
          else{$showcolor="white";}

          $num--;
       }

       echo "<tr valign=center bgcolor=#E8E7E7><td colspan=6 bgcolor=orange><form action=deposit.php method=post><input type=hidden name=lastcheck value=$lastcheck><input type=hidden name=add value=1><input type=hidden name=companyid value=$companyid>Check#:<input type=text name=checknum size=10> Invoice(s):<input type=text size=40 name=invoiceid> Total $:<input type=text size=8 name=total> <input type=submit value='GO' $submit_disable></td><td align=right bgcolor=orange><b>$$deposittotal</td><td bgcolor=red align=right>$showdeletedeposit</td></tr>";
       echo "<tr><td colspan=8></form></td></tr></table></center><p>";
       echo "<center><table width=90% border=0 cellspacing=0 cellpadding=0 style=\"border:1px solid black;\" bgcolor=#E8E7E7>";
       echo "<tr valign=center bgcolor=#E8E7E7><td colspan=7 bgcolor=orange><form action=submitdeposit.php method=post><input type=hidden name=companyid value=$companyid> <SCRIPT LANGUAGE='JavaScript' ID='js18'> var cal18 = new CalendarPopup('testdiv1');cal18.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date1 VALUE='$date' SIZE=8> <A HREF=# onClick=cal18.select(document.forms[1].date1,'anchor18','yyyy-MM-dd'); return false; TITLE=cal18.select(document.forms[1].date1,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor18' ID='anchor18'><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A> <input type=submit value='Submit' onclick='return submitconfirm()' $submit_disable> $showsubmit</td><td bgcolor=orange align=right><a href=deposit.php?exit=1&cid=$companyid style=$style><font color=white><b>Exit</b></font></a></td></tr>";
       echo "<tr bgcolor=orange><td colspan=8></form></td></tr>";
    }
    else{
       echo "<center><table width=90% border=0 cellspacing=0 cellpadding=0>";

       echo "<tr valign=bottom><td colspan=7 style=\"border:1px solid black;\" bgcolor=orange><form action=deposit.php method=post><input type=hidden name=new value=1><input type=hidden name=add value=1><input type=hidden name=companyid value=$companyid><b>NEW DEPOSIT</b>  Check#:<input type=text name=checknum size=10> Invoice(s):<input type=text size=40 name=invoiceid> Total $:<input type=text size=8 name=total> <input type=submit value='GO'></td></tr>";
       echo "<tr height=16><td colspan=17></form></td></tr>";

       echo "<tr valign=bottom><td colspan=7><FORM ACTION='invoice3.php' method='post'>";
       echo "<input type=hidden name=businessid value=$businessid>";
       echo "<input type=hidden name=companyid value=$companyid>";
       echo "Display Deposits from <SCRIPT LANGUAGE='JavaScript' ID='js18'> var cal18 = new CalendarPopup('testdiv1');cal18.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date1 VALUE='$date1' SIZE=8> <A HREF=# onClick=cal18.select(document.forms[1].date1,'anchor18','yyyy-MM-dd'); return false; TITLE=cal18.select(document.forms[1].date1,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor18' ID='anchor18'><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A> to ";
       echo "<SCRIPT LANGUAGE='JavaScript' ID='js19'> var cal19 = new CalendarPopup('testdiv1');cal19.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date2 VALUE='$date2' SIZE=8> <A HREF=# onClick=cal19.select(document.forms[1].date2,'anchor19','yyyy-MM-dd'); return false; TITLE=cal19.select(document.forms[1].date2,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor19' ID='anchor19'><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A>";
       echo " <INPUT TYPE=submit VALUE='GO'></FORM></td></tr>";

       echo "<tr bgcolor=#E8E7E7><td colspan=2 width=15%><font size=2><b>Desposit</td><td><font size=2><b>Date</td><td><font size=2><b>User</td><td><font size=2><b>Account</td><td align=right><font size=2><b>Total</td><td align=right><font size=2><b>Status</td></tr>";

       $query = "SELECT * FROM deposit WHERE companyid = '$companyid' AND date >= '$date1' AND date <= '$date2' ORDER BY date";
       $result = mysql_query($query);
       $num=mysql_numrows($result);

       $deposittotal=0;

       $counter=0;
       $num--;
       while($num>=0){
          $depositid=mysql_result($result,$num,"depositid");
          $date=mysql_result($result,$num,"date");
          $bywho=mysql_result($result,$num,"user");
          $export=mysql_result($result,$num,"export");
          $acctnum=mysql_result($result,$num,"account");

          if($export==2){$showstatus="Exported";}
          elseif($export==1){$showstatus="<font color=red>Submitted</font>";}
          else{$showstatus="<font color=green>Pending</font>";}

          $query2 = "SELECT SUM(total) AS totalamt FROM depositdetail WHERE depositid = '$depositid'";
          $result2 = mysql_query($query2);

          $totalamt=mysql_result($result2,0,"totalamt");
          $totalamt=money($totalamt);

          $deposittotal=money($deposittotal+$totalamt);

          echo "<tr><td colspan=2><a href=deposit.php?cid=$companyid&start=1&depositid=$depositid style=$style><font color=blue>$depositid</a></td><td>$date</td><td>$bywho</td><td>$acctnum</td><td align=right>$totalamt</td><td align=right>$showstatus</td></tr>";
          echo "<tr bgcolor=#E8E7E7 height=1><td colspan=7></td></tr>";

          $num--;
          $counter++;
       }
       echo "<tr><td colspan=5><i>Results: $counter</td><td align=right><b>$$deposittotal</td><td></td></tr>";
    }
    echo "</table></center>";
    
    mysql_close();

//////END TABLE////////////////////////////////////////////////////////////////////////////////////

    echo "<br><FORM ACTION=businesstrack.php method=post>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<center><INPUT TYPE=submit VALUE=' Return to Main Page'></FORM></center>";
    echo "<DIV ID=testdiv1 STYLE=position:absolute;visibility:hidden;background-color:white;layer-background-color:white;></DIV></body>";
}
?>