<?php
if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
require_once( DOC_ROOT . '/bootstrap.php' );

require_once( DOC_ROOT . '/../application/controllers/ta/route.php' );
$path_to_this = Ta_RouteController::BUSROUTE_DETAIL;
#$path_to_this = '/ta/route/detail';

$num133 = null; # Notice: Undefined variable
$update_disable = null; # Notice: Undefined variable
$showempl = null; # Notice: Undefined variable
$showpend = null; # Notice: Undefined variable
$sendmessage = null; # Notice: Undefined variable

$style = 'text-decoration:none';
$day = date('d');
$year = date('Y');
$month = date('m');
$today = "$year-$month-$day";
$todayname = date('l');

$user = \EE\Controller\Base::getSessionCookieVariable('usercook','');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook','');
$view = \EE\Controller\Base::getSessionCookieVariable('viewcook','');

$sort_ap = \EE\Controller\Base::getSessionCookieVariable('sort_ap','');
$subtotal = \EE\Controller\Base::getSessionCookieVariable('subtotal','');
$date6 = \EE\Controller\Base::getSessionCookieVariable('date6cook','');

$businessid = \EE\Controller\Base::getGetVariable(array('bid','businessid'),'');
$companyid = \EE\Controller\Base::getGetVariable(array('cid','companyid'),'');
$date1 = \EE\Controller\Base::getGetVariable('date1','');
$error = \EE\Controller\Base::getGetVariable('error','');
$editlogin = \EE\Controller\Base::getGetVariable('eid','');
$audit = \EE\Controller\Base::getGetVariable('audit','');
$lbrgrp = \EE\Controller\Base::getGetVariable('lbrgrp','');

$mei = \EE\Controller\Base::getPostGetSessionOrCookieVariable('mei', 0);
$com_calc_type = \EE\Controller\Base::getPostGetSessionOrCookieVariable('com_calc_type', 0);

if ( $businessid == '' || $companyid == '' ) {
	$businessid = \EE\Controller\Base::getPostVariable(array('bid','businessid'),'');
	$companyid = \EE\Controller\Base::getPostVariable(array('cid','companyid'),'');
	$numdays = \EE\Controller\Base::getPostVariable('numdays','');
	$date1 = \EE\Controller\Base::getPostVariable('date1','');
	$editlogin = \EE\Controller\Base::getPostVariable('loginid','');
}

$_SESSION['mei'] = $mei;
$_SESSION['com_calc_type'] = $com_calc_type;
setcookie( 'mei', $mei );
setcookie( 'com_calc_type', $com_calc_type );

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");

//if ($date6!=""&&$date1==""){$date1=$date6;}
//if ($numdays==""){$numdays=7;}

if ( !$date1 ) {
	$query = "SELECT * FROM submit_labor WHERE businessid = '$businessid' AND export = '1' ORDER BY date DESC LIMIT 0,1";
	$result = Treat_DB_ProxyOld::query( $query );

	$lastexport = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'date' );
	if ( false !== $lastexport ) {
		$date1 = $lastexport;
		for ( $counter = 1; $counter <= 14; $counter++ ) {
			$date1 = nextday( $date1 );
		}
	}
	else {
		$date1 = null;
	}
}

$passed_cookie_login = require_once( realpath( DOC_ROOT . '/../lib/inc/cookie-login.php' ) );
$page_business = Treat_Model_Business_Singleton::getSingleton();

if (
	(
		$passed_cookie_login
		&& (
			(
				$GLOBALS['security_level'] == 1
				&& $GLOBALS['bid'] == $page_business->getBusinessId()
				&& $GLOBALS['cid'] == $page_business->getCompanyId()
			)
			|| (
				$GLOBALS['security_level'] > 1
				&& $GLOBALS['cid'] == $page_business->getCompanyId()
			)
		)
	)
) {
	# this stuff is being generated here so that I know it does not depend on
	# variables generated farther down the page. Otherwise it should come after
	# the security/location check (currently between lines 201-222
	$page_company = Treat_Model_Company::getById( $companyid );
	
	$route_detail = new Treat_Model_Labor_Route_Detail();
	$route_detail->setUserId( $_SESSION->getUser()->loginid );
	$route_detail->setUserRouteDetailSecurityLevel( $_SESSION->getUser()->route_detail );
	$route_detail->setBusiness( $page_business );
	$route_detail->setDate( $date1 );
	$route_detail->setComCalcType( $com_calc_type );
	$route_detail->setMei( $mei );
	$route_detail->setLaborGroup( $lbrgrp );
	
	
	$route_detail->init();
	$routes = $route_detail->getRoutes();
	
	# variables defined after this point may be deletable as long as they're not needed anywhere
	
#	$security_level = $_SESSION->getUser()->security_level;
	$dayname = dayofweek( $date6 );

#	$companyname = $page_business->companyname;
	$week_end = $page_business->week_end;

	if ( $date1 != '' ) {
		$today = $date1;
	}

	while ( dayofweek( $today ) != $week_end ) {
		$today = nextday( $today );
	}
	$date1 = $today;

	$startdate = $date1;
	for ( $mycount1 = 1; $mycount1 <= 6; $mycount1++ ) {
		$startdate = prevday( $startdate );
	}

	//////////////5 WK AVG
	$prevperiod = prevday( $startdate );

	$tempdate = $prevperiod;
	for ( $counter = 1; $counter <= 27; $counter++ ) {
		$tempdate = prevday( $tempdate );
	}
	$date1_5wk = $tempdate;
	for ( $counter = 1; $counter <= 7; $counter++ ) {
		$tempdate = prevday( $tempdate );
	}
	$prevperiod_5wk = $tempdate;

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( 'Unable to select database');
#	$query = "SELECT * FROM security WHERE securityid = '{$GLOBALS['mysecurity']}'";
#	$result = Treat_DB_ProxyOld::query( $query );
	// user login pulls in security.*
	$security = $_SESSION->getUser();
	//mysql_close();

/*
	$sec_bus_sales = $security->bus_sales;
	$sec_bus_invoice = $security->bus_invoice;
	$sec_bus_order = $security->bus_order;
	$sec_bus_payable = $security->bus_payable;
	$sec_bus_inventor1 = $security->bus_inventor1;
	$sec_bus_control = $security->bus_control;
	$sec_bus_menu = $security->bus_menu;
	$sec_bus_order_setup = $security->bus_order_setup;
	$sec_bus_request = $security->bus_request;
	$sec_route_collection = $security->route_collection;
	$sec_route_labor = $security->route_labor;
	$sec_route_detail = $security->route_detail;
	$sec_route_machine = $security->route_machine;
	$sec_bus_labor = $security->bus_labor;
	$sec_bus_timeclock = $security->bus_timeclock;
	$sec_bus_budget = $security->bus_budget;
*/

	$location = '';
	if ( $security->route_collection < 1 && $security->route_labor < 1 && $security->route_detail < .5 ) {
		$location = '/ta/businesstrack.php';
	}
	elseif ( $security->route_collection > 0 && $security->route_detail < .5 ) {
		$location = sprintf(
			'/ta/busroute_collect.php?bid=%d&cid=%d'
			,$businessid
			,$companyid
		);
	}
	elseif ( $security->route_labor > 0 && $security->route_detail < .5 ) {
		$location = sprintf(
			'/ta/busroute_labor.php?bid=%d&cid=%d'
			,$businessid
			,$companyid
		);
	}
	if ( $location ) {
		header('Location: ' . $location);
		exit();
	}

?>
<!DOCTYPE html
	PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title>Treat America :: busroute_detail.php</title>
<?php
/*
* /
?>
		<script type="text/javascript" src="/assets/js/prototype.js"></script>
		<script type="text/javascript" src="/assets/js/prototype-eef.js"></script>
<?php
/*
*/
?>
		<script type="text/javascript" src="/assets/js/calendarPopup.js"></script>
<?php
/*
* /
?>
		<script type="text/javascript">document.write(getCalendarStyles());</script>
<?php
/*
*/
?>
		<link rel="stylesheet" type="text/css" href="/assets/css/calendarPopup.css" />
		<script type="text/javascript" src="/assets/js/dynamicdrive-disableform.js"></script>
		<script type="text/javascript" src="/assets/js/jquery-1.5.1.min.js"></script>

<script type="text/javascript"><!--
function confirm1(){return confirm('ARE YOU SURE YOU WANT TO SUBMIT?');}
// --></script>

<script type="text/javascript">
//window.onload = function(){
//	new ArrowGrid('test');
//	new headerScroll('test',{head: 'header'});
//}
</script>
<script type="text/javascript">
jQuery(document).ready(function() {
	jQuery('table.labor_table tr.employee td input').focus(function() {
		jQuery( jQuery( this ).parents( 'tr' ).get(0) ).addClass( 'highlighted' );
		this.select();
	}).blur(function() {
		jQuery( jQuery( this ).parents( 'tr' ).get(0) ).removeClass( 'highlighted' );
	});
	
	jQuery('table.labor_table tr.employee td select').focus(function() {
		jQuery( jQuery( this ).parents( 'tr' ).get(0) ).addClass( 'highlighted' );
	}).blur(function() {
		jQuery( jQuery( this ).parents( 'tr' ).get(0) ).removeClass( 'highlighted' );
	});
});
</script>

		<script type="text/javascript" src="/assets/js/common.js"></script>
		<link rel="stylesheet" type="text/css" href="/assets/css/style.css" />
		<link rel="stylesheet" type="text/css" href="/assets/css/ta/generic.css" />
		<link rel="stylesheet" type="text/css" href="/assets/css/ta/inventory.css" />
		<link rel="stylesheet" type="text/css" href="/assets/css/ta/busroute_detail.css" />
	</head>

<?php
	$myFile2 = DIR_EXPORTS . sprintf( '/report%d.csv', $_SESSION->getUser()->loginid );
	$fh = fopen( $myFile2, 'w' );
	fwrite(
		$fh,
		'Empl#,Lastname,Firstname,Week,Rating,Route,Sales,Freevend,CSV,Holiday,SNP,PTO,Base,COMM,WkEnd Pay,AVG,Total'
			. PHP_EOL
	);


#	$query = "SELECT * FROM company WHERE companyid = '$companyid'";
#	$result = Treat_DB_ProxyOld::query( $query );
#
#	$companyname = @mysql_result( $result, 0, 'companyname' );
#	$week_end = @mysql_result( $result, 0, 'week_end' );
#	$week_start = @mysql_result( $result, 0, 'week_start' );

#	$query = "SELECT * FROM business WHERE companyid = '$companyid' AND businessid = '$businessid'";
#	$result = Treat_DB_ProxyOld::query( $query );
#	$page_business = Treat_Model_Business_Singleton::getSingleton();

	$businessname = $page_business->businessname;
	$businessid = $page_business->getBusinessId();
	$street = $page_business->street;
	$city = $page_business->city;
	$state = $page_business->state;
	$zip = $page_business->zip;
	$phone = $page_business->phone;
	$districtid = $page_business->districtid;
	$bus_unit = $page_business->unit;
	$use_mei_sales = $page_business->use_mei_sales;

	if ( $use_mei_sales == 0 ) {
		$mei = 0;
	}

	$dateform = 2;
	
	///get logo
	$query = "SELECT * FROM company WHERE companyid = $companyid";
    $result = Treat_DB_ProxyOld::query($query);

	$com_logo=@Treat_DB_ProxyOld::mysql_result($result,0,"logo");

	if($com_logo == ""){$com_logo = "talogo.gif";}

	//echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";
?>
	<body>
		<div id="page_wrapper" class="page_wrapper">
			<table cellspacing="0" cellpadding="0" border="0" width="100%" class="logo">
				<tbody>
<?php
		#<editor-fold defaultstate="collapsed" desc="header">
?>
					<tr>
						<td colspan="3">
							<a href="/ta/businesstrack.php"
								><img src="/ta/logo.jpg" height="43" width="205" alt="logo"
							/></a>
							<p>
						</td>
					</tr>
					<tr bgcolor="#ccccff">
						<td width="1%">
							<img src="logo/<?php echo $com_logo; ?>"/>
						</td>
						<td>
							<div style="font-size: 1.13em; font-weight: bold;">
								<?php echo $page_business->businessname; ?> #<?php echo $page_business->unit; ?> 
							</div>
							<div>
								<?php echo $page_business->street; ?> 
							</div>
							<div>
								<?php echo $page_business->city, ', ', $page_business->state, ' ', $page_business->zip; ?> 
							</div>
							<div>
								<?php echo $page_business->phone; ?> 
							</div>
						</td>
						<td align="right" valign="top">
							<br/>
							<form action="/ta/editbus.php" method="post" name="store">
								<input type="hidden" name="username" value="<?php echo $user; ?>" />
								<input type="hidden" name="password" value="<?php echo $pass; ?>" />
								<input type="hidden" name="businessid" value="<?php echo $businessid; ?>" />
								<input type="hidden" name="companyid" value="<?php echo $companyid; ?>" />
								<input type="submit" value=" Store Setup " />
							</form>
						</td>
					</tr>
<?php
		#</editor-fold>
	
		#<editor-fold defaultstate="collapsed" desc="business menu">
?>

					<tr>
						<td colspan="3">
							<div class="unit_menu">
								<?php echo $_SESSION->getUser()->getBusinessMenu(
										$page_business->getBusinessId()
										,$page_business->getCompanyId()
										,Treat_Model_User_Object_Abstract::SECTION_ROUTE_COLLECTIONS
									);
								?> 
							</div>
						</td>
					</tr>
<?php
		#</editor-fold>
?>
				</tbody>
			</table>
			<p></p>

<?php

	if ( $_SESSION->getUser()->security_level > 0 ) {
		#<editor-fold defaultstate="collapsed" desc="output gobus business selector, labor group selector, & showtime">
?>
			<p></p>
			<table width="100%">
				<tbody>
					<tr>
						<td width="50%">
							<form action="/ta/busrequest.php" method="post">
<?php
		$businesses = Treat_Model_Business::getListBySecurityLevel(
			$_SESSION->getUser()->security_level
			,$_SESSION->getUser()->loginid
			,$page_business->getCompanyId()
			,$_SESSION->getUser()->getBusinessIds()
			,$page_business->getBusinessId()
		);
?>
								<select name="gobus" onChange="changePage(this.form.gobus)">
<?php
								foreach ( $businesses as $business ) {
									$query = array(
										'cid' => $business->getCompanyId(),
										'bid' => $business->getBusinessId(),
									);
?>
									<option value="<?php echo $path_to_this; ?>?<?php
										echo http_build_query( $query, null, '&amp;' );
									?>"<?php
										echo $business->selected
									?>><?php
										echo $business->businessname
									?></option>
<?php
								}
?>
								</select>
							</form>
						</td>
						<td></td>
						<td width="50%" align="right" colspan="2">
<?php

		//////////////////LABOR GROUPS
#		$security_level = $security->route_detail;

		if ( $_SESSION->getUser()->route_detail > 8 ) {
?>
<?php
#							<font size="2" color="blue"
#								onMouseOver="this.style.color='#FF9900';this.style.cursor='hand'"
#								onMouseOut="this.style.color='blue'"
#							>
?>
								<a
									href="/ta/manage_lbr_grps.php?bid=<?php
										echo $businessid;
									?>&amp;lbrgrp=<?php
										echo $lbrgrp;
									?>"
									onclick="popup_labor('/ta/manage_lbr_grps.php?bid=<?php
										echo $businessid;
									?>&amp;lbrgrp=<?php
										echo $lbrgrp;
									?>'); return false;"
									title="Manage Labor Groups"
									class="manage_labor_groups"
								>Manage</a>
<?php
#							</font>
?>
<?php
		}

		$query37 = sprintf( '
				SELECT
					lg.labor_group_id
				FROM labor_groups lg
					JOIN labor_group_security lgs
						ON lgs.labor_group_id = lg.labor_group_id
				WHERE lg.businessid = "%1$d"
					AND lgs.loginid = "%2$d"
			'
			,$businessid
			,$loginid
		);
		$result37 = Treat_DB_ProxyOld::query( $query37 );
		$num37 = mysql_numrows( $result37 );

		$test_lbrgrp = $lbrgrp;
		if ( $lbrgrp < 1 && $num37 > 0 ) {
			$lbrgrp = @mysql_result( $result37, 0, 'labor_group_id' );
		}
		
?>

							<form action="<?php echo $path_to_this; ?>?bid=<?php
								echo $businessid;
							?>&amp;cid=<?php
								echo $companyid;
							?>" method="post" style="margin:0; padding:0; display:inline;">
<?php
$test_lbrgrp = $route_detail->getLaborGroup();
$labor_group_list = $route_detail->getLaborGroupList();
if ( !$labor_group_list ) {
	$labor_group_list = array();
}
?>
								<select name="golbrgroup" onChange="changePage(this.form.golbrgroup)">
<?php
								$tmp_bid = $page_business->getBusinessId();
								$tmp_cid = $page_business->getCompanyId();
								foreach ( $labor_group_list as $labor_group ) {
?>
									<option value="<?php echo $path_to_this; ?>?<?php
										$query = array(
											'bid' => $tmp_bid,
											'cid' => $tmp_cid,
											'date1' => $date1,
											'lbrgrp' => $labor_group->labor_group_id
										);
										echo http_build_query( $query, null, '&amp;' );
									?>"<?php
										if ( $test_lbrgrp && $labor_group->labor_group_id == $test_lbrgrp ) {
											echo ' selected="selected"';
										}
									?>><?php
										echo $labor_group->labor_group_name;
									?></option>
<?php
								}
?>
								</select>
<?php
/*
								<select name="golbrgroup" onChange="changePage(this.form.golbrgroup)">
<?php
		if ( $num37 == 0 ) {
?>

									<option value="<?php echo $path_to_this; ?>?bid=<?php
										echo $businessid;
									?>&amp;cid=<?php
										echo $companyid;
									?>&amp;date1=<?php
										echo $date1;
									?>&amp;lbrgrp=0">All Groups</option>
<?php
		}

		if ( $num37 == 0 ) {
			$query35 = sprintf( '
					SELECT
						lg.labor_group_name
						,lg.labor_group_id
					FROM labor_groups lg
					WHERE businessid = "%1$d"
					ORDER BY labor_group_name DESC
				'
				,$businessid
			);
		}
		else {
			$query35 = sprintf( '
					SELECT
						lg.labor_group_name
						,lg.labor_group_id
					FROM labor_groups lg
						JOIN labor_group_security lgs
							ON lg.labor_group_id = lgs.labor_group_id
							AND lgs.loginid = "%2$d"
					WHERE lg.businessid = "%1$d"
					ORDER BY lg.labor_group_name DESC
				'
				,$businessid
				,$loginid
			);
		}
		$result35 = Treat_DB_ProxyOld::query( $query35 );
		$num35 = @mysql_numrows( $result35 );

#		$num35--;
#		while ( $num35 >= 0 ) {
		while ( $row = @mysql_fetch_object( $result35 ) ) {
?>

									<option value="<?php echo $path_to_this; ?>?bid=<?php
										echo $businessid;
									?>&amp;cid=<?php
										echo $companyid;
									?>&amp;date1=<?php
										echo $date1;
									?>&amp;lbrgrp=<?php
										echo $row->labor_group_id;
									?>"<?php
			if ( $row->labor_group_id == $lbrgrp ) {
				$showsel = ' selected="selected"';
			}
			else {
				$showsel = '';
			}
										echo $showsel;
									?>><?php echo $row->labor_group_name; ?></option>
<?php
#			$num35--;
		}
?>

								</select>
*/
?>
							</form>
<?php
		$dateform++;
?>
						</td>
					</tr>
				</tbody>
			</table>
<?php
		#</editor-fold>
	}
?>

<?php
if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
	echo $_SESSION->getFlashDebug( "\t\t\t<pre class=\"flash_debug\">%s</pre>\n" );
}
echo $_SESSION->getFlashError(   "\t\t\t<div class=\"flash_error\">%s</div>\n"   );
echo $_SESSION->getFlashMessage( "\t\t\t<div class=\"flash_message\">%s</div>\n" );
?>


<?php
	# this was separated from the above segment with the same if statement in order
	# to put in the flash messages. Might be a better way to do this.
	if ( $_SESSION->getUser()->security_level > 0 ) {
		#<editor-fold defaultstate="collapsed" desc="output gobus business selector, labor group selector, & showtime">
?>
			<table width="100%">
				<tbody>
					<tr valign="top">
						<td colspan="2">
							<iframe src="/ta/dept_submit.php?<?php
								$query = array(
									'cid' => $companyid,
									'bid' => $businessid,
									'date' => $date1,
									'st' => 1,
									'showp' => 1,
								);
								echo http_build_query( $query, null, '&amp;' );
							?>" width="99%" height="100" frameborder="0"></iframe>
						</td>
						<td colspan="2" align="right">
							<iframe src="/ta/showtime.php" width="100%" height="100" frameborder="0"></iframe>
						</td>
					</tr>
				</tbody>
			</table>
			<p></p>
<?php
		#</editor-fold>
	}
#	$security_level = $security->route_detail;


?>

<?php
	if ( $security->route_collection > 0 || $security->route_labor > 0 || $security->route_machine > 0 ) {
		#<editor-fold defaultstate="collapsed" desc="output page tabs">
?>

			<table cellspacing="0" cellpadding="0" width="100%" class="tabbed">
				<tbody>
					<tr valign="top">
						<td width="1%" class="left_corner"
							><img src="/assets/images/corners/leftcrn2.jpg" height="6" width="6" alt=""
						/></td>
						<td width="13%" class="label">
							<a href="/ta/busroute_collect.php?cid=<?php
								echo $page_business->getCompanyId();
							?>&amp;bid=<?php
								echo $page_business->getBusinessId();
							?>" style="<?php echo $style; ?>">
								Collection Sheet
							</a>
						</td>
						<td width="1%" class="right_corner"
							><img src="/assets/images/corners/rightcrn2.jpg" height="6" width="6" alt=""
						/></td>
						
						<td width="1%"></td>
						
						<td width="1%" class="left_corner"
							><img src="/assets/images/corners/leftcrn2.jpg" height="6" width="6" alt=""
						/></td>
						<td width="13%" class="label">
							<a href="/ta/busroute_balance.php?cid=<?php
								echo $page_business->getCompanyId();
							?>&amp;bid=<?php
								echo $page_business->getBusinessId();
							?>" style="<?php
								echo $style;
							?>">
								Balance Sheet
							</a>
						</td>
						<td width="1%" class="right_corner"
							><img src="/assets/images/corners/rightcrn2.jpg" height="6" width="6" alt=""
						/></td>
						
						<td width="1%"></td>
						
						<td width="1%" class="left_corner"
							><img src="/assets/images/corners/leftcrn2.jpg" height="6" width="6" alt=""
						/></td>
						<td width="13%" class="label">
							<a href="/ta/busroute_report.php?cid=<?php
								echo $page_business->getCompanyId();
							?>&amp;bid=<?php
								echo $page_business->getBusinessId();
							?>" style="<?php
								echo $style;
							?>">
								Reporting
							</a>
						</td>
						<td width="1%" class="right_corner"
							><img src="/assets/images/corners/rightcrn2.jpg" height="6" width="6" alt=""
						/></td>
						
						<td width="1%"></td>
						
						<td width="1%" class="left_corner"
							><img src="/assets/images/corners/leftcrn2.jpg" height="6" width="6" alt=""
						/></td>
						<td width="13%" class="label">
							<a href="/ta/busroute_labor.php?cid=<?php
								echo $page_business->getCompanyId();
							?>&amp;bid=<?php
								echo $page_business->getBusinessId();
							?>" style="<?php
								echo $style;
							?>">
								Payroll/Route Linking
							</a>
						</td>
						<td width="1%" class="right_corner"
							><img src="/assets/images/corners/rightcrn2.jpg" height="6" width="6" alt=""
						/></td>
						
						<td width="1%"></td>
						
						<td width="1%" class="current_tab left_corner"
							><img src="/assets/images/corners/leftcrn.jpg" height="6" width="6" alt=""
						/></td>
						<td width="13%" class="current_tab label">
							<a href="<?php echo $path_to_this; ?>?cid=<?php
								echo $page_business->getCompanyId();
							?>&amp;bid=<?php
								echo $page_business->getBusinessId();
							?>" style="<?php
								echo $style;
							?>">
								Route Details
							</a>
						</td>
						<td width="1%" class="current_tab right_corner"
							><img src="/assets/images/corners/rightcrn.jpg" height="6" width="6" alt=""
						/></td>
						
						<td width="1%"></td>
						
						<td width="1%" class="left_corner"
							><img src="/assets/images/corners/leftcrn2.jpg" height="6" width="6" alt=""
						/></td>
						<td width="13%" class="label">
							<a href="/ta/busroute_machine.php?cid=<?php
								echo $page_business->getCompanyId();
							?>&amp;bid=<?php
								echo $page_business->getBusinessId();
							?>" style="<?php
								echo $style;
							?>">
								Machine Mgmt
							</a>
						</td>
						<td width="1%" class="right_corner"
							><img src="/assets/images/corners/rightcrn2.jpg" height="6" width="6" alt=""
						/></td>
						
						<td bgcolor="white" width="25%"></td>
					</tr>
					<tr height="2">
						<td colspan="4"></td>
						<td colspan="4"></td>
						<td colspan="4"></td>
						<td colspan="4"></td>
						<td colspan="3" bgcolor="#e8e7e7"></td>
						<td></td>
						<td colspan="4"></td>
						<td colspan="1"></td>
					</tr>
				</tbody>
			</table>
<?php
		#</editor-fold>
	}
?>

<?php
/*
	$query34 = sprintf( '
			SELECT
				sl.*
				,sld.*
			FROM (
				SELECT
					businessid
					,date
				FROM submit
				WHERE
					businessid = %d
					AND date = "%s"
				GROUP BY
					businessid
					,date
			) b
				LEFT JOIN submit_labor sl
					ON b.businessid = sl.businessid
					AND b.date = sl.date
				LEFT JOIN submit_labor_dept sld
					ON b.businessid = sld.businessid
					AND b.date = sld.date
					AND sld.dept_id = 5
		'
		,$page_business->getBusinessId()
		,$date
	);
*/

	$query34 = sprintf( '
			SELECT export
			FROM submit_labor
			WHERE businessid = %d
				AND date = "%s"
		'
		,$businessid
		,$date1
	);
	$result34 = Treat_DB_ProxyOld::query( $query34 );
	$num34 = Treat_DB_ProxyOld::mysql_numrows( $result34 );

	$export1 = @Treat_DB_ProxyOld::mysql_result( $result34, 0, 'export' );

	$query34b = sprintf( '
			SELECT dept_id
			FROM submit_labor_dept
			WHERE businessid = %d
				AND date = "%s"
				AND dept_id = 5
		'
		,$businessid
		,$date1
	);
#	echo '<pre>';
#	echo '$query34b = ';
#	echo $query34b;
#	echo '</pre>';
	$result34b = Treat_DB_ProxyOld::query( $query34b );
	$num34b = Treat_DB_ProxyOld::mysql_numrows( $result34b );

	if (
		( $num34 > 0 && $export1 == 0 )
		|| $num34b > 0
		|| $_SESSION->getUser()->route_detail < 1
	) {
		$is_disabled1 = ' disabled="disabled"';
/*
* /
		echo '<pre>		( $num34 [', var_export( $num34, true ), '] > 0 && $export1 [', var_export( $export1, true ), '] == 0 )
		|| $num34b ['
			, var_export( $num34b, true )
			, '|'
			, var_export( @mysql_result( $result34b, 0, 'dept_id' ), true )
			, '] > 0
		|| $_SESSION->getUser()->route_detail [', var_export( $_SESSION->getUser()->route_detail, true ), '] < 1
</pre>';
/*
*/
	}

	$query35 = sprintf( '
			SELECT export
			FROM submit_labor
			WHERE businessid = %d
				AND date = "%s"
		'
		,$businessid
		,$prevperiod
	);
	$result35 = Treat_DB_ProxyOld::query( $query35 );
	$num35 = Treat_DB_ProxyOld::mysql_numrows( $result35 );

	$export2 = @Treat_DB_ProxyOld::mysql_result( $result35, 0, 'export' );

	$query35b = sprintf( '
			SELECT dept_id
			FROM submit_labor_dept
			WHERE businessid = %d
			AND date = "%s"
			AND dept_id = 5
		'
		,$businessid
		,$prevperiod
	);
#	echo '<pre>';
#	echo '$query35b = ';
#	echo $query35b;
#	echo '</pre>';
	$result35b = Treat_DB_ProxyOld::query( $query35b );
	$num35b = Treat_DB_ProxyOld::mysql_numrows( $result35b );

	if (
		( $num35 > 0 && $export2 == 0 )
		|| $num35b > 0
		|| $_SESSION->getUser()->route_detail < 1
	) {
		$is_disabled2 = ' disabled="disabled"';
/*
* /
		echo '<pre>		( $num35 [', var_export( $num35, true ), '] > 0 && $export2 [', var_export( $export2, true ), '] == 0 )
		|| $num35b ['
			, var_export( $num35b, true )
			, '|'
			, var_export( @mysql_result( $result35b, 0, 'dept_id' ), true )
			, '] > 0
		|| $_SESSION->getUser()->route_detail [', var_export( $_SESSION->getUser()->route_detail, true ), '] < 1
</pre>';
/*
*/
	}

	if (
		$num34 == 1 && $export1 == 1
		&& $num35 == 1 && $export2 == 1
	) {
		$is_disabled = ' disabled="disabled"';
		$showsubmit = '<b><font color="red">EXPORTED</font></b>';
	}
	elseif (
		$num34 == 1 && $export1 == 0
		&& $num35 == 1 && $export2 == 0
	) {
		$is_disabled = ' disabled="disabled"';
		$showsubmit = '<b><font color="red">SUBMITTED</font></b>';
	}
	elseif (
		$num34 == 1 && $export1 == -1
		&& $num35 == 1 && $export2 == 0
	) {
		$showsubmit = '<b><font color="orange">SUBMITTED/PENDING</font></b>';
	}
	elseif (
		$num34 == 1 && $export1 == -1
		&& $num35 == 1 && $export2 == -1
	) {
		$showsubmit = '<b><font color="orange">PENDING</font></b>';
		if ( $_SESSION->getUser()->route_detail < 2 ) {
			$is_disabled = ' disabled="disabled"';
/*
* /
			echo '<pre>		$num34 [', var_export( $num34, true ), '] == 1 && $export1 [', var_export( $export1, true ), '] == -1
		&& $num35 [', var_export( $num35, true ), '] == 1 && $export2 [', var_export( $export2, true ), '] == -1
</pre>';
/*
*/
		}
	}
	elseif (
		$num34 == 0
		&& $num35 == 1 && $export2 == -1
	) {
		$showsubmit = '<b><font color="orange">1ST WK PENDING</font></b>';
	}
	elseif (
		$num34 == 0
		&& $num35 == 1 && $export2 == 0
	) {
		$showsubmit = '<b><font color="orange">1ST WK SUBMITTED</font></b>';
	}
	else{}

	//if($_SESSION->getUser()->route_detail>1&&$export1==-1){$is_disabled1='';}
	//if($_SESSION->getUser()->route_detail>1&&$export2==-1){$is_disabled2='';}
	if ( $export1 == 1 || $export2 == 1 ) {
		$is_disabled1 = ' disabled="disabled"';
		$is_disabled2 = ' disabled="disabled"';
#		echo '<pre>$export1 [', $export1, '] == 1 || $export2 [', $export2, '] == 1</pre>';
	}
	if (
		( $export1 == 1 && $export2 != 1 )
		|| ( $export2 == 1 && $export1 != 1 )
	) {
		$errormessage = '<b><font color="red">NOT A VALID PAY PERIOD.</font></b> ';
	}

	if ( $_SESSION->getUser()->route_detail < 1 ) {
		$is_disabled = ' disabled="disabled"';
		$is_disabled1 = ' disabled="disabled"';
		$is_disabled2 = ' disabled="disabled"';
#		echo '<pre>$_SESSION->getUser()->route_detail < 1</pre>';
	}
	
#	$query37 = "SELECT * FROM vend_commission WHERE companyid = '$companyid' AND route_emp = '1'";
#	$result37 = Treat_DB_ProxyOld::query( $query37 );
#	$num37 = mysql_numrows( $result37 );
#	$vend_commissions = array();
#	$vend_commissions[] = null;
#	while ( $row = mysql_fetch_object( $result37 ) ) {
#		$vend_commissions[ $row->commissionid ] = $row;
#	}
	$vend_commissions = $route_detail->getVendingCommissions();

/*
* /
	if ( $lbrgrp > 0 ) {
		$query133 = sprintf( '
				SELECT
					l.firstname
					,l.lastname
					,l.empl_no
					,l.loginid
					,jtd.jobtype
					,jtd.rate
				FROM login l
					JOIN jobtypedetail jtd
						ON l.loginid = jtd.loginid
						AND jtd.is_deleted = "0"
					JOIN jobtype jt
						ON jtd.jobtype = jt.jobtypeid
						AND jt.commission = "1"
					JOIN labor_group_detail lgd
						ON l.loginid = lgd.loginid
						AND lgd.labor_group_id = "%3$s"
				WHERE l.oo2 = "%1$d"
					AND (
						l.is_deleted = "0"
						OR (
							l.is_deleted = "1"
							AND l.del_date >= "%2$s"
						)
					)
				ORDER BY l.lastname DESC
			'
			,$businessid
			,$date1
			,$lbrgrp
		);
	}
	else {
		$query133 = sprintf( '
				SELECT
					l.firstname
					,l.lastname
					,l.empl_no
					,l.loginid
					,jtd.jobtype
					,jtd.rate
				FROM login l
					JOIN jobtypedetail jtd
						ON l.loginid = jtd.loginid
						AND jtd.is_deleted = "0"
					JOIN jobtype jt
						ON jtd.jobtype = jt.jobtypeid
						AND jt.commission = "1"
				WHERE l.oo2 = "%1$d"
					AND (
						l.is_deleted = "0"
						OR (
							l.is_deleted = "1"
							AND l.del_date >= "%2$s"
						)
					)
				ORDER BY l.lastname
					,l.firstname
			'
			,$businessid
			,$date1
		);
	}
#	echo '<pre>';
#	echo '$query133 = ';
#	echo $query133;
#	echo '</pre>';
	$result133 = Treat_DB_ProxyOld::query( $query133 );
	$num133 = @mysql_numrows( $result133 );
/*
*/
#	$jobtypedetail_employees = Treat_Model_User_Login::getRouteDetailLaborEmployees( $businessid, $date1, $lbrgrp );
	$jobtypedetail_employees = $route_detail->getEmployees();

	///////////////HOW TO CALCULATE
	if ( $_SESSION->getUser()->route_detail < 9 ) {
		$com_calc_type = 0;
	}

/*
* /
	if ( $com_calc_type == 1 ) {
		$calc1 = ' selected="selected"';
	}
	elseif ( $com_calc_type == 2 ) {
		$calc2 = ' selected="selected"';
	}
	elseif ( $com_calc_type == 3 ) {
		$calc3 = ' selected="selected"';
	}
	elseif ( $com_calc_type == 4 ) {
		$calc4 = ' selected="selected"';
	}
	elseif ( $com_calc_type == 5 ) {
		$calc5 = ' selected="selected"';
	}
/*
*/

	if ( $lbrgrp > 0 ) {
		$lbrgrp_disable = ' disabled="disabled"';
	}

	#<editor-fold defaultstate="collapsed" desc="PREV/NEXT & Use MEI/BT code block">
?>

			<table width="100%" cellspacing="0" cellpadding="0">
				<tbody>
					<tr bgcolor="#e8e7e7">
						<td colspan="3" nowrap="nowrap">
							<font size="1">[<a href="<?php echo $path_to_this; ?>?<?php
								$query = array(
									'cid' => $page_business->getCompanyId(),
									'bid' => $page_business->getBusinessId(),
									'eid' => $editlogin,
									'date1' => date( 'Y-m-d', $route_detail->getGoPreviousDate() ),
									'lbrgrp' => $lbrgrp,
								);
								echo http_build_query( $query, null, '&amp;' );
							?>"><font color="blue">PREV</font></a>]</font>
							<form action="<?php
								echo $path_to_this;
							?>" method="get" style="margin:0; padding:0; display:inline;">
								<b>2 Weeks ending:</b>
								<input type="hidden" name="bid" value="<?php echo $page_business->getBusinessId(); ?>" />
								<input type="hidden" name="cid" value="<?php echo $page_business->getCompanyId(); ?>" />
								<input type="hidden" name="mei" value="<?php echo $mei; ?>" />
								<input type="hidden" name="lbrgrp" value="<?php echo $lbrgrp; ?>" />
<?php
/*
*/
?>
								<script type="text/javascript" id="js18">
									var cal18 = new CalendarPopup('testdiv1');
									cal18.setCssPrefix('TEST');
								</script>
<?php
/*
*/
?>
								<a href="javascript:void();"
<?php
/*
* /
?>
									onClick="cal18.select(document.forms[<?php echo $dateform; ?>].date1,'anchor18','yyyy-MM-dd'); return false;"
<?php
/*
*/
?>
									onClick="cal18.select( document.forms[<?php echo $dateform; ?>].date1, 'anchor18', 'yyyy-MM-dd' ); return false;"
									title="cal18.select(document.forms[<?php echo $dateform; ?>].date1,'anchor1x','yyyy-MM-dd'); return false;"
									name="anchor18" id="anchor18"
								>
									<img src="/ta/calendar.gif" border="0" height="16" width="16" alt="Choose a Date"
								/></a>
								<input type="text" name="date1" id="date1" value="<?php echo $date1; ?>" size="8" />
								<select name="com_calc_type">
<?php
								$options = $route_detail->getComCalcTypes();
								foreach ( $options as $option ) {
?>
									<option value="<?php
										echo $option->id
									?>"<?php
										if ( $com_calc_type == $option->id ) {
											echo ' selected="selected"';
										}
										if ( $option->disabled ) {
											echo ' disabled="disabled"';
										}
									?>><?php
										echo $option->name
									?></option>
<?php
								}
?>
								</select>
								<input type="submit" value="GO" />
							</form>
							<?php
								echo $route_detail->getExportDisabledMessages();
							?> 
							<font size="1">[<a href="<?php echo $path_to_this; ?>?<?php
								$query = array(
									'cid' => $page_business->getCompanyId(),
									'bid' => $page_business->getBusinessId(),
									'eid' => $editlogin,
									'date1' => date( 'Y-m-d', $route_detail->getGoNextDate() ),
									'lbrgrp' => $lbrgrp,
								);
								echo http_build_query( $query, null, '&amp;' );
							?>"><font color="blue">NEXT</font></a>]</font>
						</td>
						<td align="right" colspan="4">
<?php
	///////////////USE WHICH SALES////////////////
	if ( $page_business->use_mei_sales ) {
?>
							<span class="which_sales">
								<a href="<?php echo $path_to_this; ?>?<?php
									$query = array(
										'cid' => $page_business->getCompanyId(),
										'bid' => $page_business->getBusinessId(),
										'mei' => intval( !$route_detail->getMei() ),
										'date1' => date( 'Y-m-d', $route_detail->getDate() ),
										'lbrgrp' => $route_detail->getLaborGroup(),
									);
									echo http_build_query( $query, null, '&amp;' );
								?>"><span><?php
										if ( $route_detail->getMei() ) {
											echo 'Use BT Sales';
										}
										else {
											echo 'Use MEI Sales';
										}
								?></span></a>
							</span>
<?php
	}
?>
							<form action="/ta/submitroutelabor.php" method="post"
								class="week_form"
							>
								<input type="hidden" name="week" value="1" />
								<input type="hidden" name="date1" value="<?php echo $date1; ?>" />
								<input type="hidden" name="prevperiod" value="<?php echo $prevperiod; ?>" />
								<input type="hidden" name="businessid" value="<?php echo $businessid; ?>" />
								<input type="hidden" name="companyid" value="<?php echo $companyid; ?>" />
								<input type="hidden" name="sec_level" value="<?php echo $_SESSION->getUser()->route_detail; ?>" />
								<input type="submit" value="Week1" onclick="return confirm1()" <?php
									if (
										true & (
											# if 1 of these is true it disables the button
											$route_detail->isExportDisabledWeek1()
											| $route_detail->isDisabledViaLaborGroup()
										)
									) {
										echo ' disabled="disabled"';
									}
								?> />
							</form>
							<form action="/ta/submitroutelabor.php" method="post"
								class="week_form"
							>
								<input type="hidden" name="week" value="2" />
								<input type="hidden" name="date1" value="<?php echo $date1; ?>" />
								<input type="hidden" name="prevperiod" value="<?php echo $prevperiod; ?>" />
								<input type="hidden" name="businessid" value="<?php echo $businessid; ?>" />
								<input type="hidden" name="companyid" value="<?php echo $companyid; ?>" />
								<input type="hidden" name="sec_level" value="<?php echo $_SESSION->getUser()->route_detail; ?>" />
								<input type="submit" value="Week2" onclick="return confirm1()" <?php
									if (
										true & (
											# if 1 of these is true it disables the button
											$route_detail->isExportDisabledWeek2()
											| $route_detail->isDisabledViaLaborGroup()
										)
									) {
										echo ' disabled="disabled"';
									}
								?> />
							</form>
						</td>
					</tr>
					<tr bgcolor="black" height="1">
						<td colspan="7"></td>
					</tr>
				</tbody>
			</table>


<?php
	#</editor-fold>
	$num133--;
	$arrow = 0;
	$sheettotal = 0;
	$sheet_sales = 0;
	$sheet_cashless = 0;
	$sheet_freevend = 0;
	$sheet_csv = 0;
	$sheet_comm = 0;
/*
* /
		echo '<pre>';
		echo '$prevperiod = ';
		var_dump( $prevperiod );
		echo '$prevperiod_5wk = ';
		var_dump( $prevperiod_5wk );
		echo '$date1 = ';
		var_dump( $date1 );
		echo '$date1_5wk = ';
		var_dump( $date1_5wk );
		echo '</pre>';
/*
*/

	/////////////////////START TABLE
#			<form action="/ta/labor_route_link2.php" method="post"
?>


			<form action="/ta/route/labor_link" method="post"
				onSubmit="return disableForm(this);"
				style="margin: 0; padding: 0;"
			>
				<input type="hidden" name="lbrgrp" value="<?php echo $lbrgrp; ?>" />
				<input type="hidden" name="date1" value="<?php echo $date1; ?>" />
				<input type="hidden" name="prevperiod" value="<?php echo $prevperiod; ?>" />
				<input type="hidden" name="editlogin" value="<?php echo $editlogin; ?>" />
				<input type="hidden" name="businessid" value="<?php echo $businessid; ?>" />
				<input type="hidden" name="companyid" value="<?php echo $companyid; ?>" />
				<table id="test_" class="labor_table" cellspacing="0" cellpadding="1">
					<thead>
						<tr id="header">
							<th scope="col" class="employee_number">Empl#&nbsp;</th>
							<th scope="col" class="employee">Employee</th>
							<th scope="col" class="status">Status</th>
							<th scope="col" class="route">Route#</th>
							<th scope="col" class="sales">Sales</th>
							<th scope="col" class="cashless">Cashless</th>
							<th scope="col" class="kiosk_sales">Kiosk Sales</th>
							<th scope="col" class="fv_sub">FV/Sub</th>
							<th scope="col" class="csv">CSV</th>
							<th scope="col" class="holiday">Holiday</th>
							<th scope="col" class="snp">SNP</th>
							<th scope="col" class="days">Days</th>
							<th scope="col" class="pto">PTO</th>
							<th scope="col" class="pto_total">PTO/Day</th>
							<th scope="col" class="base">Base</th>
							<th scope="col" class="commission">Comm.</th>
							<th scope="col" class="pay_week">Wk Pay</th>
							<th scope="col" class="pay_average">PTO Tot</th>
							<th scope="col" class="pay_total">Total</th>
						</tr>
					</thead>
					<tbody>
<?php
#		'Empl#,Lastname,Firstname,Week,Rating,Route,Sales,Freevend,CSV,Holiday,SNP,PTO,Base,COMM,WkEnd Pay,AVG,Total'
	foreach ( $jobtypedetail_employees as $jobtypedetail ) {
/*
* /
		$job_weeks = $jobtypedetail->getWeeksCalculated();
	}
	$jobtypedetail_employees1 = array_slice( $jobtypedetail_employees, 0, 4 );
	$page_total = array_pop( $jobtypedetail_employees );
	$jobtypedetail_employees1[] = $page_total;
	foreach ( $jobtypedetail_employees1 as $jobtypedetail ) {
/*
*/
		$week_counter = 0;
#		$job_weeks = $jobtypedetail->getWeeks();
		$job_weeks = $jobtypedetail->getWeeksCalculated();
		foreach ( $job_weeks as $week => $week_obj ) {
#			$week_obj = $jobtypedetail->getWeek( $week );
			if ( !$week_obj ) {
				$week_obj = new Treat_Null();
			}
			$arrow = sprintf( '_%d_%s', $jobtypedetail->loginid, $week );
			$form_names = '';
			if ( !( $week_obj instanceof \EE\Model\Labor\Route\Employee\WeekTotal ) ) {
#				Treat_Model_Labor_Route_Employee_Week::$DEBUG = true;
				$form_names = sprintf(
					'e[%d][%s][%%s]'
					,$jobtypedetail->loginid
#					,'asdf'
					,date( 'Y-m-d', $week_obj->getDate() )
				);
#				Treat_Model_Labor_Route_Employee_Week::$DEBUG = false;
			}
?>
						<tr
							class="<?php
								$classes = array(
									'white',
									'employee',
									'week_' . $week,
								);
								if ( $week_obj instanceof Treat_Model_Labor_Route_Employee_PageTotal ) {
									$classes[] = 'pagetotal';
								}
								# pagetotal doesn't need the :hover background that 'total' gets
								# even though pagetotal extends week total
								elseif ( $week_obj instanceof \EE\Model\Labor\Route\Employee\WeekTotal ) {
									$classes[] = 'total';
								}
								echo implode( ' ', $classes );
							?>"
							id="row<?php echo $arrow; ?>"
						>
							<td class="employee_number"><?php
								echo htmlentities( $week_obj->getEmployeeNumber() );
							?></td>
							<td class="employee"><?php
								echo htmlentities( $week_obj->getEmployeeName() );
							?></td>
							<td class="status">
<?php
							if ( !( $week_obj instanceof \EE\Model\Labor\Route\Employee\WeekTotal ) ) {
/*
* /
								<select name="<?php echo $jobtypedetail->loginid; ?>:emp_com<?php echo $week; ?>]"
/*
*/
?>
								<select name="<?php
										echo sprintf(
											$form_names
											,Treat_Model_Labor_Route_Detail::FIELD_INPUT_VENDING_COMMISSION_ID
										);
									?>"
									style="font-size: 10px;"
<?php
/*
* /
?>
									onfocus="<?php echo 'row', $arrow; ?>.style.backgroundColor='yellow'"
									onblur="<?php echo 'row', $arrow; ?>.style.backgroundColor=''"<?php
/*
*/
										if ( $week_obj->getIsDisabled() ) {
											echo ' disabled="disabled"';
										}
									?> 
								>
<?php
			$showempcomname = '';
			foreach ( $vend_commissions as $vend_commission ) {
				if ( $vend_commission ) {
?>
									<option value="<?php echo $vend_commission->commissionid; ?>"<?php
										if ( $vend_commission->commissionid == $week_obj->commissionid ) {
											$showempcomname = $vend_commission->name;
											echo ' selected="selected"';
										}
									?>><?php echo htmlentities( $vend_commission->name ); ?></option>
<?php
				}
			}
?>
								</select>
<?php
							}
?>
							</td>
							<td class="route">
<?php
			if ( !( $week_obj instanceof \EE\Model\Labor\Route\Employee\WeekTotal ) ) {
?>
								Wk<?php echo $week; ?> <?php echo $week_obj->getShowNot(); ?> 
<?php
/*
* /
								<select name="<?php echo $jobtypedetail->loginid; ?>:route<?php echo $week; ?>"
/*
*/
?>
								<select name="<?php
										echo sprintf(
											$form_names
											,Treat_Model_Labor_Route_Detail::FIELD_INPUT_LOGIN_ROUTEID
										);
									?>"
									style="font-size: 10px;"
<?php
/*
* /
?>
									onfocus="<?php echo 'row', $arrow; ?>.style.backgroundColor='yellow';"
									onblur="<?php echo 'row', $arrow; ?>.style.backgroundColor='';"<?php
/*
*/
										if ( $week_obj->getIsDisabled() ) {
											echo ' disabled="disabled"';
										}
									?> 
								>
									<option></option>
<?php
				$debug = '';
				foreach ( $routes as $route ) {
?>

									<option value="<?php
										echo $route->login_routeid;
									?>"<?php
										if ( $route->login_routeid == $week_obj->{Treat_Model_Labor_Route_Detail::FIELD_LOGIN_ROUTEID} ) {
											echo ' selected="selected"';
										}
									?>><?php
										echo htmlentities( $route->route );
									?></option>
<?php
				}
?>

									<option value="-1"<?php
										if ( -1 == $week_obj->{Treat_Model_Labor_Route_Detail::FIELD_LOGIN_ROUTEID} ) {
											echo ' selected="selected"';
											$showroute = 'TRNG';
										}
									?>>TRNG</option>
									<option value="-2"<?php
										if ( -2 == $week_obj->{Treat_Model_Labor_Route_Detail::FIELD_LOGIN_ROUTEID} ) {
											echo ' selected="selected"';
											$showroute = 'TERM/WC';
										}
									?>>TERM/WC</option>
								</select>
<?php
			}
?>
							</td>
							<td class="sales"><?php
								echo number_format( $week_obj->{Treat_Model_Labor_Route_Detail::FIELD_SHOW_SALES}, 2 );
							?></td>
							<td class="cashless"><?php
								echo number_format( $week_obj->{Treat_Model_Labor_Route_Detail::FIELD_SHOW_NONCASH}, 2 );
							?></td>
							<td class="kiosk_sales"><?php
								echo number_format( $week_obj->{Treat_Model_Labor_Route_Detail::FIELD_SHOW_KIOSK}, 2 );
							?></td>
							<td class="fv_sub"><?php
								echo number_format( $week_obj->{Treat_Model_Labor_Route_Detail::FIELD_SHOW_FREEVEND}, 2 );
							?></td>
							<td class="csv"><?php
								echo number_format( $week_obj->{Treat_Model_Labor_Route_Detail::FIELD_SHOW_CSV}, 2 );
							?></td>
							<td class="holiday">
<?php
							if ( 'total' != $week ) {
?>
<?php
/*
* /
									name="<?php echo $jobtypedetail->loginid; ?>:pay_days<?php echo $week; ?>"
/*
*/
?>
								<input type="text"
									name="<?php
										echo sprintf(
											$form_names
											,Treat_Model_Labor_Route_Detail::FIELD_INPUT_PAY_DAYS_VALUE
										);
									?>"
									size="3"
									value="<?php
										echo $week_obj->{Treat_Model_Labor_Route_Detail::FIELD_SHOW_PAY_DAYS};
									?>"
									style="text-align:right; font-size: 10px;"<?php
										if ( $week_obj->getIsDisabled() ) {
											echo ' disabled="disabled"';
										}
									?>
<?php
/*
* /
?>
									onfocus="<?php echo 'row', $arrow; ?>.style.backgroundColor='yellow'; this.select();"
									onblur="<?php echo 'row', $arrow; ?>.style.backgroundColor=''"
<?php
/*
*/
?>
								/>
<?php
							}
							else {
								echo $week_obj->{Treat_Model_Labor_Route_Detail::FIELD_SHOW_PAY_DAYS};
							}
?>
							</td>
							<td class="snp">
<?php
							if ( 'total' != $week ) {
?>
<?php
/*
* /
									name="<?php echo $jobtypedetail->loginid; ?>:nopay_days<?php echo $week; ?>"
/*
*/
?>
								<input type="text"
									name="<?php
										echo sprintf(
											$form_names
											,Treat_Model_Labor_Route_Detail::FIELD_INPUT_NOPAY_DAYS_VALUE
										);
									?>"
									size="3"
									value="<?php
										echo $week_obj->{Treat_Model_Labor_Route_Detail::FIELD_SHOW_NOPAY_DAYS};
									?>"<?php
										if ( $week_obj->getIsDisabled() ) {
											echo ' disabled="disabled"';
										}
									?> 
									style="text-align:right; font-size: 10px;"
<?php
/*
* /
?>
									onfocus="<?php echo 'row', $arrow; ?>.style.backgroundColor='yellow'; this.select();"
									onblur="<?php echo 'row', $arrow; ?>.style.backgroundColor=''"
<?php
/*
*/
?>
								/>
<?php
							}
							else {
								echo $week_obj->{Treat_Model_Labor_Route_Detail::FIELD_SHOW_NOPAY_DAYS};
							}
?>
							</td>
							<td class="days"><?php
								echo $week_obj->{Treat_Model_Labor_Route_Detail::FIELD_SHOW_WEEKDAYS};
							?></td>
							<td class="pto">
<?php
							if ( 'total' != $week ) {
?>
<?php
/*
* /
									name="<?php echo $jobtypedetail->loginid; ?>:pto<?php echo $week; ?>"
/*
*/
?>
								<input type="text"
									name="<?php
										echo sprintf(
											$form_names
											,Treat_Model_Labor_Route_Detail::FIELD_INPUT_PTO_VALUE
										);
									?>"
									size="3"
									value="<?php
										echo $week_obj->{Treat_Model_Labor_Route_Detail::FIELD_SHOW_PTO};
									?>"<?php
										if ( $week_obj->getIsDisabled() ) {
											echo ' disabled="disabled"';
										}
									?> 
									style="text-align:right; font-size: 10px;"
<?php
/*
* /
?>
									onfocus="<?php echo 'row', $arrow; ?>.style.backgroundColor='yellow'; this.select();"
									onblur="<?php echo 'row', $arrow; ?>.style.backgroundColor=''"
<?php
/*
*/
?>
								/>
<?php
							}
							else {
								echo $week_obj->{Treat_Model_Labor_Route_Detail::FIELD_SHOW_PTO};
							}
?>
							</td>
							<td class="pto_total"><?php
								if ( $week_obj->{Treat_Model_Labor_Route_Detail::FIELD_SHOW_PTO_5WEEK_DAILY} ) {
									echo number_format( $week_obj->{Treat_Model_Labor_Route_Detail::FIELD_SHOW_PTO_5WEEK_DAILY}, 2 );
								}
							?></td>
							<td class="base">
								<input type="hidden" name="<?php
										echo sprintf(
											$form_names
											,Treat_Model_Labor_Route_Detail::FIELD_INPUT_BASE_VALUE
										);
									?>" value="<?php
									if ( $week_obj->{Treat_Model_Labor_Route_Detail::FIELD_BASE} ) {
										echo $week_obj->{Treat_Model_Labor_Route_Detail::FIELD_BASE};
									}
								?>" />
								<?php
									echo $week_obj->{Treat_Model_Labor_Route_Detail::FIELD_SHOW_BASE};
								?>
							</td>
							<td class="commission">
								<input type="hidden" name="<?php
										echo sprintf(
											$form_names
											,'base_commission'
										);
									?>" value="<?php
										if ( $week_obj->base_commission ) {
											echo $week_obj->base_commission;
										}
								?>" />
								<input type="hidden" name="<?php
										echo sprintf(
											$form_names
											,Treat_Model_Labor_Route_Detail::FIELD_INPUT_COMMISSION_VALUE
										);
									?>" value="<?php
									if ( $week_obj->{Treat_Model_Labor_Route_Detail::FIELD_COMMISSION} ) {
										echo $week_obj->{Treat_Model_Labor_Route_Detail::FIELD_COMMISSION};
									}
								?>" />
								<?php
									echo number_format( $week_obj->{Treat_Model_Labor_Route_Detail::FIELD_SHOW_COMMISSION}, 2 );
								?> 
							</td>
							<td class="pay_week">
<?php
							if ( 'total' != $week ) {
?>
<?php
/*
* /
									name="<?php echo $jobtypedetail->loginid; ?>:weekend<?php echo $week; ?>"
/*
*/
?>
								<input type="text"
									name="<?php
										echo sprintf(
											$form_names
											,Treat_Model_Labor_Route_Detail::FIELD_INPUT_WEEKEND_VALUE
										);
									?>"
									size="4"
									value="<?php
										echo $week_obj->{Treat_Model_Labor_Route_Detail::FIELD_SHOW_WEEKEND};
									?>"<?php
										if ( $week_obj->getIsDisabled() ) {
											echo ' disabled="disabled"';
										}
									?> 
									style="text-align:right; font-size: 10px;"
<?php
/*
* /
?>
									onfocus="<?php echo 'row', $arrow; ?>.style.backgroundColor='yellow'; this.select();"
									onblur="<?php echo 'row', $arrow; ?>.style.backgroundColor=''"
<?php
/*
*/
?>
								/>
<?php
							}
							else {
								echo $week_obj->{Treat_Model_Labor_Route_Detail::FIELD_SHOW_WEEKEND};
							}
?>
							</td>
							<td class="pay_average">
<?php
							if ( 'total' != $week ) {
?>
								<input type="hidden" name="<?php
										echo sprintf(
											$form_names
											,Treat_Model_Labor_Route_Detail::FIELD_INPUT_PTO_5WEEK_TOTAL
										);
									?>" value="<?php
									echo $week_obj->{Treat_Model_Labor_Route_Detail::FIELD_SHOW_PTO_5WEEK_TOTAL};
								?>" />
<?php
							}
?>
								<?php
									if ( $week_obj->{Treat_Model_Labor_Route_Detail::FIELD_SHOW_PTO_5WEEK_TOTAL} ) {
										echo number_format( $week_obj->{Treat_Model_Labor_Route_Detail::FIELD_SHOW_PTO_5WEEK_TOTAL}, 2 );
									}
								?> 
							</td>
							<td class="pay_total"><?php
								echo number_format( $week_obj->{Treat_Model_Labor_Route_Detail::FIELD_SHOW_TOTAL}, 2 );
							?></td>
						</tr>
<?php
			$csv_array = (array)$week_obj->toCsvArray();
			if ( !$csv_array ) {
				$csv_array = array();
			}
			fputcsv( $fh, $csv_array, ',', '"' );
			$week_counter++;
		}
	}
?>
						<tr bgcolor="#ccccff">
							<td colspan="19">
								<input type="hidden" name="sec_level" value="<?php echo $_SESSION->getUser()->route_detail; ?>" />
								<input type="hidden" name="prevperiod_5wk" value="<?php echo $prevperiod_5wk; ?>" />
								<input type="hidden" name="date1_5wk" value="<?php echo $date1_5wk; ?>" />
								<input type="hidden" name="is_disabled1" value="<?php
									echo intval(
										true & (
											$route_detail->isExportDisabledWeek1()
#											| $route_detail->isDisabledViaLaborGroup()
										)
									);
								?>" />
								<input type="hidden" name="is_disabled2" value="<?php
									echo intval(
										true & (
											$route_detail->isExportDisabledWeek2()
#											| $route_detail->isDisabledViaLaborGroup()
										)
									);
								?>" />
								<input type="checkbox" name="update_payroll" value="1" /> Update Payroll
								<input type="submit" value="Save"<?php
									if (
										$route_detail->isExportDisabledWeek1()   	# isExportDisabledPreviousWeek()
										&& $route_detail->isExportDisabledWeek2()	# isExportDisabledCurrentWeek()
									) {
										echo ' disabled="disabled"';
									}
								?> />
								<a href="<?php
									echo HTTP_EXPORTS;
								?>/report<?php
									echo $_SESSION->getUser()->loginid;
								?>.csv" style="<?php
									echo $style;
								?>">
									<font color="blue" size="2">Excel File</font>
								</a>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
				<table cellspacing="0" cellpadding="1" width="100%">
					<tbody>
						<tr bgcolor="#ccccff">
							<td align="right">
								<form action="/ta/route/print_detail" target="_blank" method="get"
									style="margin: 0; padding: 0; display: inline;"
								>
									<input type="hidden" name="date1" value="<?php echo $date1; ?>" />
									<input type="hidden" name="prevperiod" value="<?php echo $prevperiod; ?>" />
									<input type="hidden" name="businessid" value="<?php echo $businessid; ?>" />
									<input type="hidden" name="companyid" value="<?php echo $companyid; ?>" />
									<input type="hidden" name="lbrgrp" value="<?php echo $lbrgrp; ?>" />
									<input type="submit" value="Print"<?php echo $update_disable; ?> />
								</form>
							</td>
						</tr>
<?php
	if ( $showempl == 1 ) {
?>

						<tr bgcolor="white">
							<td colspan="7">
								<font color="red" size="2">*</font>
								<font size="2">Route from last period.</font>
							</td>
						</tr>
<?php
	}

?>
					</tbody>
				</table>

<?php

	fclose( $fh );

	/////////////////////////////////////////////
	///////LABOR GROUPS (HOURLY EMPLOYEES)///////
	/////////////////////////////////////////////
	if ( $lbrgrp > 0 ) {

		for ( $counter = 1; $counter <= 13; $counter++ ) {
			$date1 = prevday( $date1 );
		}

		$labordollar = array();
		//////////////////////////////

?>

			<p></p>
			<table width="100%" cellspacing="0" cellpadding="0">
				<tbody>
					<tr>
						<td colspan="10">
							<center>
								<form action="/ta/savelabor2.php" method="post"
									onSubmit="return disableForm(this);"
								>
									<input type="hidden" name="lbrgrp" value="<?php echo $lbrgrp; ?>" />
									<input type="hidden" name="goto" value="10" />
									<input type="hidden" name="companyid" value="<?php echo $companyid; ?>" />
									<input type="hidden" name="businessid" value="<?php echo $businessid; ?>" />
									<input type="hidden" name="date" value="<?php echo $date1; ?>" />
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<tbody>
											<tr height="0" bgcolor="#e8e7e7">
												<td colspan="18"></td>
											</tr>
											<tr bgcolor="#e8e7e7">
												<td colspan="3">
													<b>Hourly Employees<a name="hours"></a></b>
												</td>
<?php
		$temp_date = $date1;
		for ( $counter = 1; $counter <= 7; $counter++ ) {

			$query36 = "SELECT * FROM submit_labor WHERE businessid = '$businessid' AND date = '$temp_date'";
			$result36 = Treat_DB_ProxyOld::query( $query36 );
			$num36 = Treat_DB_ProxyOld::mysql_numrows( $result36 );

			$export = @Treat_DB_ProxyOld::mysql_result( $result36, 0, 'export' );

			$showfont = 'black';
			if ( $export == 1 ) {
				$showcolor = '#444455';
				$showfont = 'white';
			}
			elseif ( $export == -1 ) {
				$showcolor = 'orange';
				$showfont = 'black';
			}
			elseif ( $num36 > 0 ) {
				$showcolor = '#00FF00';
			}
			else {
				$showcolor = '#00CCFF';
			}

			if ( $counter == 7 ) {
				$end_date = $temp_date;
			}

?>

												<td align="right" bgcolor="<?php echo $showcolor; ?>" width="11%" colspan="2">
													<font color="<?php echo $showfont; ?>"><?php echo $temp_date; ?></font>
												</td>
<?php
			$temp_date = nextday( $temp_date );
		}
?>

												<td></td>
											</tr>
											<tr bgcolor="#e8e7e7">
												<td colspan="3" align="right"></td>
<?php
		for ( $counter = 1; $counter <= 7; $counter++ ) {

			$query36 = "SELECT * FROM submit_labor WHERE businessid = '$businessid' AND date = '$temp_date'";
			$result36 = Treat_DB_ProxyOld::query( $query36 );
			$num36 = Treat_DB_ProxyOld::mysql_numrows( $result36 );

			$export = @Treat_DB_ProxyOld::mysql_result( $result36, 0, 'export' );

			$showfont = 'black';
			if ( $export == 1 ) {
				$showcolor = '#444455';
				$showfont = 'white';
			}
			elseif ( $export == -1 ) {
				$showcolor = 'orange';
				$showfont = 'black';
			}
			elseif ( $num36 > 0 ) {
				$showcolor = '#00FF00';
			}
			else {
				$showcolor = '#00CCFF';
			}

			if ( $counter == 7 ) {
				$end_date = $temp_date;
			}

?>

												<td align="right" bgcolor="<?php echo $showcolor; ?>" width="11%" colspan="2">
													<font color="<?php echo $showfont; ?>"><?php echo $temp_date; ?></font>
												</td>
<?php
			$temp_date = nextday( $temp_date );
		}
?>

												<td></td>
											</tr>
											<tr bgcolor="#ccccff">
												<td width="4%">Empl#</td>
												<td width="11%">Name</td>
												<td align="right">Rate</td>
<?php
		$temp_date = $date1;
		$todayname = dayofweek( $temp_date );
		for ( $counter = 1; $counter <= 7; $counter++ ) {
			$todayname = substr( $todayname, 0, 3 );
			if ( $todayname=='Sat' || $todayname == 'Sun' ) {
				$showcolor = 'yellow';
			}
			else {
				$showcolor = '';
			}
?>

												<td align="right" bgcolor="<?php echo $showcolor; ?>">Coded</td>
												<td align="right" bgcolor="<?php echo $showcolor; ?>"><?php echo $todayname; ?></td>
<?php
			if ( $counter == 7 ) {
				$date2 = $temp_date;
			}
			$temp_date = nextday( $temp_date );
			$todayname = dayofweek( $temp_date );
		}
?>

												<td align="right">
													<font size="2" width="4%">
														<b></b>
													</font>
												</td>
											</tr>
<?php

		/////////////////LIST EMPLOYEES
		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( 'Unable to select database');
		$query34 = "SELECT * FROM payroll_code WHERE companyid = '$companyid' ORDER BY orderid DESC";
		$result34 = Treat_DB_ProxyOld::query( $query34 );
		$num34 = Treat_DB_ProxyOld::mysql_numrows( $result34 );
		//mysql_close();

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( 'Unable to select database');
		$query = "SELECT * FROM jobtype WHERE companyid = '$companyid' AND commission = '0' ORDER BY hourly DESC";
		$result = Treat_DB_ProxyOld::query( $query );
		$num = Treat_DB_ProxyOld::mysql_numrows( $result );
		//mysql_close();

		$num--;
		$mycounter = 0;
		$coltotal = array();
		$coldollar = array();
		$sheettotal = 0;

		while ( $num >= 0 ) {
			$jobtypeid = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'jobtypeid' );
			$jobtypename = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'name' );
			$hourly = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'hourly' );

			/////////////NORMAL LABOR
			//REMOVED 8/31/09: AND (login.loginid = payroll_departmentdetail.loginid AND payroll_departmentdetail.dept_id = security_departments.dept_id AND security_departments.securityid = '$mysecurity')
			
			$query2 = "SELECT distinct login.loginid,login.firstname,login.lastname,login.empl_no,jobtypedetail.rate FROM login,jobtypedetail,labor_groups,labor_group_detail WHERE login.oo2 = '$businessid' AND login.move_date <= '$end_date' AND (login.is_deleted = '0' OR (login.is_deleted = '1' AND login.del_date >= '$date1')) AND jobtypedetail.loginid = login.loginid AND jobtypedetail.jobtype = '$jobtypeid' AND (jobtypedetail.is_deleted = '0' OR (jobtypedetail.is_deleted = '1' AND jobtypedetail.del_date >= '$date1')) AND login.loginid = labor_group_detail.loginid AND labor_group_detail.labor_group_id = labor_groups.labor_group_id AND labor_groups.labor_group_id = '$lbrgrp' ORDER BY login.lastname DESC, login.firstname DESC";
			$result2 = Treat_DB_ProxyOld::query( $query2 );
			$num2 = Treat_DB_ProxyOld::mysql_numrows( $result2 );
			//mysql_close();
			//echo "<font size=1>$query2</font><br>";
			$num2--;
			$people = 0;
			$job_coltotal = array();
			$job_coldollar = array();
			$job_rowtotal = 0;

			while ( $num2 >= 0 ) {
				$firstname = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'firstname' );
				$lastname = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'lastname' );
				$empl_no = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'empl_no' );
				$loginid = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'loginid' );
				$rate = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'rate' );
				$people++;
				$rate = money( $rate );

				if ( $empl_no < 1 && $_SESSION->getUser()->route_detail < 9 ) {
					$submit_disable = ' disabled="disabled"';
					$showwhy = "<font color=red>Pending Employee Number</font>";
				}
				elseif ( $empl_no < 1 ) {
					$showwhy = "<font color=red>Pending Employee Number</font>";
				}

				if ( $hourly == 0 || $_SESSION->getUser()->route_detail < 1 ) {
					$showrate = 'xxx';
				}
				elseif ( $viewrate != 1 ) {
					$showrate = 'xxx';
				}
				else {
					$showrate = $rate;
				}

				$rowtotal = 0;

?>

											<tr valign="top">
												<td>
													<font size="2"><?php echo $empl_no; ?></font>
												</td>
												<td>
<?php
				if ( $_SESSION->getUser()->route_detail == 9 ) {
?>

													<font size="2">
														<a href="/ta/edit_emp_labor.php?cid=<?php
															echo $companyid;
														?>&amp;bid=<?php
															echo $businessid;
														?>&amp;eid=<?php
															echo $loginid;
														?>" style="<?php
															echo $style;
														?>">
															<font color="blue"><?php echo $lastname, ',', $firstname; ?></font>
														</a>
													</font>
<?php
				}
				else {
?>

													<font size="2"><?php echo $lastname, ',', $firstname; ?></font>
<?php
				}
?>

												</td>
												<td align="right">
													<font size="2">$<?php echo $showrate; ?></font>
												</td>
<?php

				$temp_date = $date1;
				$temp_date2 = $temp_date;
				for ( $counter = 1; $counter <= 7; $counter++ ) {
					$temp_date2 = nextday( $temp_date2 );
				}

				$rowhours = 0;
				$rowhours2 = 0;
				for ( $counter = 1; $counter <= 7; $counter++ ) {

					///////////////////////////
					/////FIRST WEEK////////////
					///////////////////////////
					$query3 = "SELECT * FROM labor WHERE date = '$temp_date' AND loginid = '$loginid' AND jobtypeid = '$jobtypeid' AND businessid = '$businessid' AND tempid = '0'";
					$result3 = Treat_DB_ProxyOld::query( $query3 );
					$num3 = Treat_DB_ProxyOld::mysql_numrows( $result3 );

					$rate = @Treat_DB_ProxyOld::mysql_result( $result3, 0, 'rate' );
					$hours = @Treat_DB_ProxyOld::mysql_result( $result3, 0, 'hours' );
					$coded = @Treat_DB_ProxyOld::mysql_result( $result3, 0, 'coded' );

					$rowhours = $rowhours + $hours;

					if ( $hourly == 1 ) {
						$rowtotal = money( $rowtotal + ( $rate * $hours ) );
					}
					elseif ( $hourly == 0 && $hours != 0 ) {
						$rowtotal = money( $rowtotal + ( $rate / ( $operate * 2 ) ) );
					}

					$job_coltotal[ $counter ] = $job_coltotal[ $counter ] + $hours;

					if ( $hourly == 1 ) {
						$job_coldollar[ $counter ] = money( $job_coldollar[ $counter ] + ( $rate * $hours ) );
					}
					elseif ( $hourly == 0 && $hours != 0 ) {
						$job_coldollar[ $counter ] = money( $job_coldollar[ $counter ] + ( $rate / ( $operate * 2 ) ) );
					}

					$coltotal[ $counter ] = $coltotal[ $counter ] + $hours;

					if ( $hourly == 1 ) {
						$coldollar[ $counter ] = money( $coldollar[ $counter ] + ( $rate * $hours ) );
					}
					elseif ( $hourly == 0 && $hours != 0 ) {
						$coldollar[ $counter ] = money( $coldollar[ $counter ] + ( $rate / ( $operate * 2 ) ) );
					}

					///////////////////////////
					/////SECOND WEEK///////////
					///////////////////////////
					$query3 = "SELECT * FROM labor WHERE date = '$temp_date2' AND loginid = '$loginid' AND jobtypeid = '$jobtypeid' AND businessid = '$businessid' AND tempid = '0'";
					$result3 = Treat_DB_ProxyOld::query( $query3 );
					$num3 = Treat_DB_ProxyOld::mysql_numrows( $result3 );

					$rate2 = @Treat_DB_ProxyOld::mysql_result( $result3, 0, 'rate' );
					$hours2 = @Treat_DB_ProxyOld::mysql_result( $result3, 0, 'hours' );
					$coded2 = @Treat_DB_ProxyOld::mysql_result( $result3, 0, 'coded' );

					$rowhours2 = $rowhours2 + $hours2;

					$job_coltotal[ $counter ] = $job_coltotal[ $counter ] + $hours2;

					$coltotal[ $counter ] = $coltotal[ $counter ] + $hours2;

					/////FIRST WEEK CODED
?>

												<td align="right">
													<select name="code:<?php echo $mycounter; ?>"
														tabindex="<?php echo $counter; ?>"
														style="font-size: 10px;"
														onChange="return submit5()"
													>
<?php
					$temp_num = $num34 - 1;
					while ( $temp_num >= 0 ) {
						$codeid = @Treat_DB_ProxyOld::mysql_result( $result34, $temp_num, 'codeid' );
						$code = @Treat_DB_ProxyOld::mysql_result( $result34, $temp_num, 'code' );

						if ( $codeid == $coded ) {
							$showsel = ' selected="selected"';
						}
						else {
							$showsel = '';
						}


?>

														<option value="<?php
															echo $codeid;
														?>"<?php
															echo $showsel;
														?>><?php
															echo $code;
														?></option>";
<?php
						$temp_num--;
					}
?>

													</select>
													<br/>
<?php

					///////SECOND WEEK CODED
?>

													<select name="code2:<?php echo $mycounter; ?>"
														tabindex="<?php echo $counter; ?>"
														style="font-size: 10px;"
														onChange="return submit5()"
													>
<?php
					$temp_num = $num34 - 1;
					while ( $temp_num >= 0 ) {
						$codeid = @Treat_DB_ProxyOld::mysql_result( $result34, $temp_num, 'codeid' );
						$code = @Treat_DB_ProxyOld::mysql_result( $result34, $temp_num, 'code' );

						if ( $codeid == $coded2 ) {
							$showsel = ' selected="selected"';
						}
						else {
							$showsel = '';
						}

?>

														<option value="<?php
															echo $codeid;
														?>"<?php
															echo $showsel;
														?>><?php
															echo $code;
														?></option>
<?php
						$temp_num--;
					}
?>

													</select>
												</td>
<?php

					///////HOURS
					if ( $hourly == 1 ) {
?>

												<td align="right">
													<input type="text" size="3"
														name="hour:<?php echo $mycounter; ?>"
														value="<?php echo $hours; ?>"
														tabindex="<?php echo $counter; ?>"
														style="font-size: 10px;"
													/>
													<br/>
													<input type="text" size="3"
														name="hour2:<?php echo $mycounter; ?>"
														value="<?php echo $hours2; ?>"
														tabindex="<?php echo $counter; ?>"
														style="font-size: 10px;"
													/>
												</td>
<?php
					}
					else {
?>

												<td align="right">
													<input type="text" size="3"
														name="hour:<?php echo $mycounter; ?>"
														value="<?php echo $hours; ?>"
														tabindex="<?php echo $counter; ?>"
														style="font-size: 10px;"
													/>
													<br/>
													<input type="text" size="3"
														name="hour2:<?php echo $mycounter; ?>"
														value="<?php echo $hours2; ?>"
														tabindex="<?php echo $counter; ?>"
														style="font-size: 10px;"
													/>
												</td>
<?php
					}

					$temp_date = nextday( $temp_date );
					$temp_date2 = nextday( $temp_date2 );
					$mycounter++;
				}

?>

												<td align="right" width="5%">
													<font size="2">
														<b>&nbsp;<?php echo $rowhours; ?></b>
														<br/>
														<b>&nbsp;<?php echo $rowhours2; ?></b>
													</font>
												</td>
											</tr>
											<tr height="4" bgcolor="#e8e7e7">
												<td colspan="18"></td>
											</tr>
<?php
				$sheettotal = money( $sheettotal + $rowtotal );
				$job_rowtotal = money( $job_rowtotal + $rowtotal );

				if ( $rowhours > 40 && $hourly == 0 ) {
					$submit_disable = ' disabled="disabled"';
					$showwhy = "<font color = red>Salaried employee can't have more than 40 hours.</font>";
				}

				$num2--;
			}
			/////////////PAST LABOR
/*
			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( 'Unable to select database');
			$query2 = "SELECT distinct labor.loginid,labor.rate FROM login,labor WHERE labor.businessid = '$businessid' AND labor.date >= '$date1' AND labor.date<= '$enddate' AND labor.jobtypeid = '$jobtypeid' AND login.oo2 != '$businessid' AND labor.loginid = login.loginid AND labor.tempid = '0'";
			$result2 = Treat_DB_ProxyOld::query( $query2 );
			$num2 = mysql_numrows( $result2 );
			//mysql_close();

			$num2--;

			while ( $num2 >= 0 ) {
				$loginid = @mysql_result( $result2, $num2, 'loginid' );
				$rate = @mysql_result( $result2, $num2, 'rate' );

				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( 'Unable to select database');
				$query99 = "SELECT lastname,firstname,empl_no FROM login WHERE loginid = '$loginid'";
				$result99 = Treat_DB_ProxyOld::query( $query99 );
				//mysql_close();

				$firstname = @mysql_result( $result99, $num99, 'firstname' );
				$lastname = @mysql_result( $result99, $num99, 'lastname' );
				$empl_no = @mysql_result( $result99, $num99, 'empl_no' );

				$people++;
				$rate=money($rate);

				if ( $hourly==0||$_SESSION->getUser()->route_detail < 1 ) {
					$showrate = 'xxx';
				}
				elseif ( $viewrate != 1 ) {
					$showrate = 'xxx';
				}
				else {
					$showrate=$rate;
				}

				$rowtotal=0;

				if ($_SESSION->getUser()->route_detail==9){echo "<tr valign=top bgcolor=red><td><font size=2>$empl_no</td><td><font size=2><a href=edit_emp_labor.php?cid=$companyid&bid=$businessid&eid=$loginid style=$style><font color=blue>$lastname,$firstname</font></a></td><td align=right><font size=2>$$showrate</td>";}
				else {
					echo "<tr valign=top bgcolor=red><td><font size=2>$empl_no</td><td><font size=2>$lastname,$firstname</td><td align=right><font size=2>$$showrate</td>";
				}

				$temp_date=$date1;
				$rowhours=0;
				for($counter=1;$counter<=7;$counter++){

					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( 'Unable to select database');
					$query3 = "SELECT * FROM labor WHERE date = '$temp_date' AND loginid = '$loginid' AND jobtypeid = '$jobtypeid' AND businessid = '$businessid' AND tempid = '0'";
					$result3 = Treat_DB_ProxyOld::query( $query3 );
					$num3 = mysql_numrows( $result3 );
					//mysql_close();

					$rate = @mysql_result( $result3, 0, 'rate' );
					$hours = @mysql_result( $result3, 0, 'hours' );
					$coded = @mysql_result( $result3, 0, 'coded' );

					$rowhours=$rowhours+$hours;

					if ( $hourly == 1 ) {
						$rowtotal = money($rowtotal+($rate*$hours));
					}
					elseif ( $hourly==0&&$hours != 0 ) {
						$rowtotal = money($rowtotal+($rate/($operate*2)));
					}

					$job_coltotal[$counter]=$job_coltotal[$counter]+$hours;

					if ( $hourly == 1 ) {
						$job_coldollar[$counter] = money($job_coldollar[$counter]+($rate*$hours));
					}
					elseif ( $hourly==0&&$hours != 0 ) {
						$job_coldollar[$counter] = money($job_coldollar[$counter]+($rate/($operate*2)));
					}

					$coltotal[$counter]=$coltotal[$counter]+$hours;

					if ( $hourly == 1 ) {
						$coldollar[$counter] = money($coldollar[$counter]+($rate*$hours));
					}
					elseif ( $hourly==0&&$hours != 0 ) {
						$coldollar[$counter] = money($coldollar[$counter]+($rate/($operate*2)));
					}

					echo "<td align=right><select name=code$mycounter tabindex=$counter STYLE='font-size: 10px;'>";
					$temp_num=$num34-1;
					while ( $temp_num >= 0 ) {
						$codeid = @mysql_result( $result34, $temp_num, 'codeid' );
						$code = @mysql_result( $result34, $temp_num, 'code' );

						if ( $codeid == $coded ) {
							$showsel = ' selected="selected"';
						}
						else {
							$showsel = '';
						}


						echo "<option value=$codeid $showsel>$code</option>";
						$temp_num--;
					}
					echo "</select></td><td align=right><input type=text size=3 name=hour$mycounter value='$hours' tabindex=$counter STYLE='font-size: 10px;'></td>";

					$temp_date=nextday($temp_date);
					$mycounter++;
				}

				echo "<td align=right><font size=2><b>$rowhours</font></td></tr>";
				$sheettotal=money($sheettotal+$rowtotal);
				$job_rowtotal=money($job_rowtotal+$rowtotal);

				$num2--;
			}
			/////////////ADDED LABOR
			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( 'Unable to select database');
			$query2 = "SELECT * FROM labor_temp WHERE businessid = '$businessid' AND date >= '$date1' AND date <= '$date2' AND jobtypeid = '$jobtypeid'";
			$result2 = Treat_DB_ProxyOld::query( $query2 );
			$num2 = mysql_numrows( $result2 );
			//mysql_close();

			$num2--;

			while ( $num2 >= 0 ) {
				$mytempid = @mysql_result( $result2, $num2, 'tempid' );
				$loginid = @mysql_result( $result2, $num2, 'loginid' );
				$rate = @mysql_result( $result2, $num2, 'rate' );

				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( 'Unable to select database');
				$query99 = "SELECT lastname,firstname,empl_no FROM login WHERE loginid = '$loginid'";
				$result99 = Treat_DB_ProxyOld::query( $query99 );
				//mysql_close();

				$firstname = @mysql_result( $result99, $num99, 'firstname' );
				$lastname = @mysql_result( $result99, $num99, 'lastname' );
				$empl_no = @mysql_result( $result99, $num99, 'empl_no' );

				$people++;
				$rate=money($rate);

				if ( $hourly==0||$_SESSION->getUser()->route_detail < 1 ) {
					$showrate = 'xxx';
				}
				elseif ( $viewrate != 1 ) {
					$showrate = 'xxx';
				}
				else {
					$showrate=$rate;
				}

				$rowtotal=0;
				
				$showdelete="<a href=deltempempl.php?bid=$businessid&cid=$companyid&newdate=$date1&tempid=$mytempid onclick='return deletetemp()'><img src=delete.gif height=16 width=16 border=0 alt='Delete Temp'></a>";

				//if ($_SESSION->getUser()->route_detail==9){echo "<tr valign=top bgcolor=#FFFF99><td><font size=2>$empl_no</td><td>$showdelete<font size=2><a href=edit_emp_labor.php?cid=$companyid&bid=$businessid&eid=$loginid style=$style><font color=blue>$lastname,$firstname</font></a></td><td align=right><font size=2>$$showrate</td>";}
				#else{echo "<tr valign=top bgcolor=#FFFF99><td><font size=2>$empl_no</td><td><font size=2>$lastname,$firstname</td><td align=right><font size=2>$$showrate</td>";}
				//echo "<tr bgcolor=#FFFF99 valign=top><td><font size=2>$empl_no</td><td><font size=2>$lastname,$firstname</td><td align=right><font size=2>$$showrate</td>";

				$temp_date=$date1;
				$rowhours=0;
				for($counter=1;$counter<=7;$counter++){

					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( 'Unable to select database');
					$query3 = "SELECT * FROM labor WHERE date = '$temp_date' AND loginid = '$loginid' AND jobtypeid = '$jobtypeid' AND businessid = '$businessid'";
					$result3 = Treat_DB_ProxyOld::query( $query3 );
					$num3 = mysql_numrows( $result3 );
					//mysql_close();

					$rate = @mysql_result( $result3, 0, 'rate' );
					$hours = @mysql_result( $result3, 0, 'hours' );
					$coded = @mysql_result( $result3, 0, 'coded' );

					$rowhours=$rowhours+$hours;

					if ( $hourly == 1 ) {
						$rowtotal = money($rowtotal+($rate*$hours));
					}
					elseif ( $hourly==0&&$hours != 0 ) {
						$rowtotal = money($rowtotal+($rate/($operate*2)));
					}

					$job_coltotal[$counter]=$job_coltotal[$counter]+$hours;

					if ( $hourly == 1 ) {
						$job_coldollar[$counter] = money($job_coldollar[$counter]+($rate*$hours));
					}
					elseif ( $hourly==0&&$hours != 0 ) {
						$job_coldollar[$counter] = money($job_coldollar[$counter]+($rate/($operate*2)));
					}

					$coltotal[$counter]=$coltotal[$counter]+$hours;

					if ( $hourly == 1 ) {
						$coldollar[$counter] = money($coldollar[$counter]+($rate*$hours));
					}
					elseif ( $hourly==0&&$hours != 0 ) {
						$coldollar[$counter] = money($coldollar[$counter]+($rate/($operate*2)));
					}

					echo "<td align=right><select name=code$mycounter tabindex=$counter STYLE='font-size: 10px;'>";
					$temp_num=$num34-1;
					while ( $temp_num >= 0 ) {
						$codeid = @mysql_result( $result34, $temp_num, 'codeid' );
						$code = @mysql_result( $result34, $temp_num, 'code' );

						if ( $codeid == $coded ) {
							$showsel = ' selected="selected"';
						}
						else {
							$showsel='';
						}

						echo "<option value=$codeid $showsel>$code</option>";
						$temp_num--;
					}
					echo "</select></td><td align=right><input type=text size=3 name=hour$mycounter value='$hours' tabindex=$counter STYLE='font-size: 10px;'></td>";

					$temp_date=nextday($temp_date);
					$mycounter++;
				}

				echo "<td align=right><font size=2><b>$rowhours</font></td></tr>";
				$sheettotal=money($sheettotal+$rowtotal);
				$job_rowtotal=money($job_rowtotal+$rowtotal);

				$num2--;
			}
*/
			/////////END ADDED LABOR
			$num--;
			if ( $people > 0 ) {
?>

											<tr bgcolor="#cccccc">
												<td colspan="3">
													<font size="2">
														<b><?php echo $jobtypename; ?> Totals</b>
													</font>
												</td>
												<td align="right" colspan="2">
													<font size="2"><?php echo $job_coltotal[1]; ?> hrs</font>
												</td>
												<td align="right" colspan="2">
													<font size="2"><?php echo $job_coltotal[2]; ?> hrs</font>
												</td>
												<td align="right" colspan="2">
													<font size="2"><?php echo $job_coltotal[3]; ?> hrs</font>
												</td>
												<td align="right" colspan="2">
													<font size="2"><?php echo $job_coltotal[4]; ?> hrs</font>
												</td>
												<td align="right" colspan="2">
													<font size="2"><?php echo $job_coltotal[5]; ?> hrs</font>
												</td>
												<td align="right" colspan="2">
													<font size="2"><?php echo $job_coltotal[6]; ?> hrs</font>
												</td>
												<td align="right" colspan="2">
													<font size="2"><?php echo $job_coltotal[7]; ?> hrs</font>
												</td>
												<td align="right">
													<font size="2">
														<b></b>
													</font>
												</td>
											</tr>
<?php
			}
		}
?>

											<tr bgcolor="#e8e7e7">
												<td colspan="3">
													<font size="2">
														<b>Totals</b>
													</font>
												</td>
												<td align="right" colspan="2">
													<font size="2">
														<b><?php echo $coltotal[1]; ?> hrs</b>
													</font>
												</td>
												<td align="right" colspan="2">
													<font size="2">
														<b><?php echo $coltotal[2]; ?> hrs</b>
													</font>
												</td>
												<td align="right" colspan="2">
													<font size="2">
														<b><?php echo $coltotal[3]; ?> hrs</b>
													</font>
												</td>
												<td align="right" colspan="2">
													<font size="2">
														<b><?php echo $coltotal[4]; ?> hrs</b>
													</font>
												</td>
												<td align="right" colspan="2">
													<font size="2">
														<b><?php echo $coltotal[5]; ?> hrs</b>
													</font>
												</td>
												<td align="right" colspan="2">
													<font size="2">
														<b><?php echo $coltotal[6]; ?> hrs</b>
													</font>
												</td>
												<td align="right" colspan="2">
													<font size="2">
														<b><?php echo $coltotal[7]; ?> hrs</b>
													</font>
												</td>
												<td align="right">
													<font size="2">
														<b></b>
													</font>
												</td>
											</tr>
<?php

		if ( $_SESSION->getUser()->route_detail > 10 ) {
?>

											<tr bgcolor="#cccc99">
												<td colspan="3">
													<font size="2">
														<b>Dollars</b>
													</font>
												</td>
<?php
			for ( $counter = 1; $counter <= 7; $counter++ ) {
				$labordollar[ $counter ] = money( $labordollar[ $counter ] );
?>

												<td align="right" colspan="2">
													<font size="2">
														<b>$<?php echo $labordollar[$counter]; ?></b>
													</font>
												</td>
<?php
			}
?>

												<td></td>
											</tr>
<?php
		}
		//////////////Removed by bd $submit_disable
		if ( $auto_submit_labor == 1 ) {
			$myform = '/ta/submit_labor_dept.php';
		}
		else {
			$myform = '/ta/submit_labor.php';
		}

?>

											<tr bgcolor="#ccccff">
												<td colspan="3">
													<input type="submit" value="Save" tabindex="8"<?php echo $is_disabled1; ?> />
												</td>
												<td>
												</td>
												<td align="right" colspan="14"></td>
											</tr>
											<tr>
												<td colspan="18"></td>
											</tr>
										</tbody>
									</table>
								</form>
							</center>
						</td>
					</tr>
				</tbody>
			</table>
<?php
	}
	/////////////////////////////////////////////
	/////////END HOURLY//////////////////////////
	/////////////////////////////////////////////

	/////////////////////////NEW EMPLOYEES
?>

			<p></p>
			<table width="100%">
				<tbody>
					<tr>
						<td>
<?php

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( 'Unable to select database');
	$query3 = "SELECT * FROM login_pend WHERE businessid = '$businessid'";
	$result3 = Treat_DB_ProxyOld::query( $query3 );
	$num3 = Treat_DB_ProxyOld::mysql_numrows( $result3 );
	//mysql_close();

	if ( $num3 > 0 ) {
		$showpend = "<b> - <font color=blue>$num3 Request(s) Pending</font></b>";
	}

?>

							<table width="100%" bgcolor="#e8e7e7">
								<tbody>
									<tr>
										<td colspan="2">
											<b>Request New Employee</b>
											<?php echo $showpend; ?> 
										</td>
									</tr>
									<tr>
										<td>
											<form action="/ta/labor_request.php" method="post">
												<input type="hidden" name="newdate" value="<?php echo $date1; ?>" />
												<input type="hidden" name="companyid" value="<?php echo $companyid; ?>" />
												<input type="hidden" name="businessid" value="<?php echo $businessid; ?>" />
												<input type="hidden" name="goto" value="1" />
												<input type="text" name="firstname" value="firstname" size="20" />
												<input type="text" name="lastname" value="lastname" size="20" />
												<select name="jobtypeid">
<?php

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( 'Unable to select database');
	$query2 = "SELECT * FROM jobtype WHERE companyid = '$companyid' AND commission = '1' ORDER BY name DESC";
	$result2 = Treat_DB_ProxyOld::query( $query2 );
	$num2 = Treat_DB_ProxyOld::mysql_numrows( $result2 );
	//mysql_close();

	$num2--;
	while ( $num2 >= 0 ) {
		$job_typename = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'name' );
		$jobtypeid = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'jobtypeid' );
?>

													<option value="<?php
														echo $jobtypeid;
													?>"><?php
														echo $job_typename;
													?></option>
<?php
		$num2--;
	}
?>

												</select>
												<b>$</b
												><input type="text" size="5" name="rate" />
												<input type="submit" value="Request"
													onclick="return submit4()"
												/>
											</form>
										</td>
										<td width="1"></td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
<?php
	////////////////END NEW EMPLOYEES
	////////////////EXISTING EMPLOYEES
?>

			<p></p>
			<table width="100%">
				<tbody>
					<tr>
						<td>
							<table width="100%" bgcolor="#e8e7e7">
								<tbody>
									<tr>
										<td colspan="2">
											<b>Request Existing Employee</b>
										</td>
									</tr>
									<tr>
										<td>
											<form action="/ta/labor_request_comm.php" method="post">
												<input type="hidden" name="newdate" value="<?php echo $date1; ?>" />
												<input type="hidden" name="companyid" value="<?php echo $companyid; ?>" />
												<input type="hidden" name="businessid" value="<?php echo $businessid; ?>" />
												<input type="hidden" name="goto" value="1" />
												<select name="loginid">
<?php

	$query2 = "SELECT login.* FROM login,jobtype,jobtypedetail WHERE login.companyid = '$companyid' AND login.is_deleted = '0' AND login.security_level < '1' AND login.loginid = jobtypedetail.loginid AND jobtypedetail.is_deleted = '0' AND jobtypedetail.jobtype = jobtype.jobtypeid AND jobtype.commission != '1' ORDER BY login.lastname DESC, login.firstname DESC";
	$result2 = Treat_DB_ProxyOld::query( $query2 );
	$num2 = Treat_DB_ProxyOld::mysql_numrows( $result2 );
	//mysql_close();

	$num2--;
	while ( $num2 >= 0 ) {
		$loginid = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'loginid' );
		$firstname = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'firstname' );
		$lastname = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'lastname' );
		$empl_no = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'empl_no' );

?>

													<option value="<?php
														echo $loginid;
													?>"><?php
														echo $lastname, ', ', $firstname, ' (', $empl_no, ')'
													?></option>
<?php

		$num2--;
	}

?>

												</select>
												<select name="jobtypeid">
<?php

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( 'Unable to select database');
	$query2 = "SELECT * FROM jobtype WHERE companyid = '$companyid' AND commission = '1' ORDER BY name DESC";
	$result2 = Treat_DB_ProxyOld::query( $query2 );
	$num2 = Treat_DB_ProxyOld::mysql_numrows( $result2 );
	//mysql_close();

	$num2--;
	while ( $num2 >= 0 ) {
		$job_typename = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'name' );
		$jobtypeid = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'jobtypeid' );

?>

													<option value="<?php
														echo $jobtypeid;
													?>"><?php
														echo $job_typename;
													?></option>
<?php

		$num2--;
	}
?>

												</select>
												<b>$</b
												><input type="text" size="5" name="rate" />
												<input type="submit" value="Request"
													onclick="return submit4()"
												/>
											</form>
										</td>
										<td width="1"></td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
<?php
	////////////////END EXISTING EMPLOYEES
	/////////////////////////REQUEST TERM/TRANSFER
	$is_sent = isset( $_GET['send'] ) ? $_GET['send'] : '';

	if ( $is_sent == 1 ) {
		$sendmessage = "<font color=green> - Message Sent</font>";
	}
	elseif ( $is_sent == 2 ) {
		$sendmessage = "<font color=red> - Please enter a Message</font>";
	}

?>

			<table width="100%">
				<tbody>
					<tr>
						<td>
							<table width="100%" bgcolor="#e8e7e7">
								<tbody>
									<tr>
										<td colspan="2">
											<a name="message"></a>
											<b>Request Termination/Transfer/Other <?php echo $sendmessage; ?></b>
										</td>
									</tr>
									<tr>
										<td>
											<form action="/ta/email_labor_request.php" method="post">
												<input type="hidden" name="goto" value="1" />
												<input type="hidden" name="districtid" value="<?php echo $districtid; ?>" />
												<input type="hidden" name="companyid" value="<?php echo $companyid; ?>" />
												<input type="hidden" name="businessid" value="<?php echo $businessid; ?>" />
												<select name="eid">
<?php

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( 'Unable to select database');
	$query2 = "SELECT * FROM login WHERE oo2 = '$businessid' AND is_deleted = '0' ORDER BY lastname DESC,firstname DESC";
	$result2 = Treat_DB_ProxyOld::query( $query2 );
	$num2 = Treat_DB_ProxyOld::mysql_numrows( $result2 );
	//mysql_close();

	$num2--;
	while ( $num2 >= 0 ) {
		$loginid = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'loginid' );
		$lastname = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'lastname' );
		$firstname = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'firstname' );
		$empl_no = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'empl_no' );
?>

													<option value="<?php
														echo $loginid;
													?>"><?php
														echo $lastname;
													?>, <?php
														echo $firstname;
													?> (<?php
														echo $empl_no;
													?>)</option>
<?php
		$num2--;
	}
?>

												</select>
												Message: 
												<input type="text" size="40" name="request" />
												<input type="submit" value="Send" />
											</form>
										</td>
										<td></td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>


			<table width="100%">
				<tbody>
<?php

	if ( $audit == 1 ) {
?>

					<tr bgcolor="white">
						<td colspan="4">
							<font size="2">
								<b>Audit Trail</b>
							</font>
						</td>
					</tr>
					<tr bgcolor="white">
						<td colspan="4">
							<table cellspacing="0" style="border: 1px solid #cccccc;">
								<tbody>
									<tr bgcolor="#cccccc">
										<td style="border: 1px solid #cccccc;" width="25%">
											<font size="2">
												<b>Date</b>
											</font>
										</td>
										<td style="border: 1px solid #cccccc;">
											<font size="2">
												<b>Date</b>
											</font>
										</td>
										<td style="border: 1px solid #cccccc;" width="25%">
											<font size="2">
												<b>Time</b>
											</font>
										</td>
										<td style="border: 1px solid #cccccc;" width="25%">
											<font size="2">
												<b>User</b>
											</font>
										</td>
									</tr>
<?php

		$query12 = "SELECT * FROM audit_route WHERE loginid = '$editlogin' AND (date1 = '$date1' OR date2 = '$date1' OR date1 = '$prevperiod' OR date2 = '$prevperiod') ORDER BY id DESC";
		$result12 = Treat_DB_ProxyOld::query( $query12 );
		$num12 = Treat_DB_ProxyOld::mysql_numrows( $result12 );

		$num12--;
		while ( $num12 >= 0 ) {
			$a_date1 = @Treat_DB_ProxyOld::mysql_result( $result12, $num12, 'date1' );
			$a_date2 = @Treat_DB_ProxyOld::mysql_result( $result12, $num12, 'date2' );
			$a_route1 = @Treat_DB_ProxyOld::mysql_result( $result12, $num12, 'route' );
			$a_route2 = @Treat_DB_ProxyOld::mysql_result( $result12, $num12, 'route2' );
			$a_time = @Treat_DB_ProxyOld::mysql_result( $result12, $num12, 'time' );
			$a_user = @Treat_DB_ProxyOld::mysql_result( $result12, $num12, 'user' );
?>

									<tr>
										<td style="border: 1px solid #cccccc;" width="25%">
											<font size="2"><?php echo $a_date1; ?></font>
										</td>
										<td style="border: 1px solid #cccccc;">
											<font size="2"><?php echo $a_date2; ?></font>
										</td>
										<td style="border: 1px solid #cccccc;" width="25%">
											<font size="2"><?php echo $a_time; ?></font>
										</td>
										<td style="border: 1px solid #cccccc;" width="25%">
											<font size="2"><?php echo $a_user; ?></font>
										</td>
									</tr>
<?php
			$num12--;
		}

?>

								</tbody>
							</table>
						</td>
					</tr>
<?php
	}
	else {
?>

					<tr bgcolor="white">
						<td colspan="4">
							<font size="2">
								<a href="<?php echo $path_to_this; ?>?cid=<?php
									echo $companyid;
								?>&amp;bid=<?php
									echo $businessid;
								?>&amp;eid=<?php
									echo $editlogin;
								?>&amp;date1=<?php
									echo $date1;
								?>&amp;audit=1#save" style="<?php
									echo $style;
								?>">
									<font color="blue">
										<b>Audit Trail</b>
									</font>
								</a>
							</font>
						</td>
					</tr>
<?php
	}


?>

				</tbody>
			</table>
		</div>
		<div id="testdiv1"
			style="position: absolute; visibility: hidden; background-color: white;"
		></div>

		<a name="save"></a>
		<br/>
		<form action="/ta/businesstrack.php" method="post">
			<center>
				<input type="hidden" name="username" value="<?php echo $user; ?>" />
				<input type="hidden" name="password" value="<?php echo $pass; ?>" />
				<input type="submit" value=" Return to Main Page" />
			</center>
		</form>
<?php
	
	google_page_track();
?>
	</body>
</html>
<?php
}
else {
	require_once( 'inc/avoid-whitescreen.php' );
}
//mysql_close();
?>