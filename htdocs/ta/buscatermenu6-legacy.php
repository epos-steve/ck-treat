<?php

define( 'DOC_ROOT', realpath( dirname( __FILE__ ) . '/../' ) );
require_once( DOC_ROOT . '/bootstrap.php' );
require_once( DOC_ROOT . '/ta/lib/class.menu_items.php' );
require_once( DOC_ROOT . '/ta/lib/class.Kiosk.php' );
//DB_Proxy::$debug = true;
//ini_set('display_errors','true');

$style = "text-decoration:none";

//$showall = $_GET["showall"];
$showall = \EE\Controller\Base::getGetVariable('showall');

if( $showall == "" ) {
	//$showall = isset( $_COOKIE["showall"] ) ? $_COOKIE["showall"] : '';
	$showall = \EE\Controller\Base::getSessionCookieVariable('showall','');
}
setcookie( showall, "$showall" );

if( require_once( realpath( DOC_ROOT . '/../lib/inc/cookie-login.php' ) ) ) {
	
	//$steve = isset( $_COOKIE["usercook"] ) ? $_COOKIE["usercook"] : '';
	$steve = \EE\Controller\Base::getSessionCookieVariable('usercook','');
	
	$query = "SELECT * FROM security WHERE securityid = '$mysecurity'";
	$result = Treat_DB_ProxyOld::query( $query );
	
	$sec_bus_sales = Treat_DB_ProxyOld::mysql_result( $result, 0, "bus_sales" );
	$sec_bus_invoice = Treat_DB_ProxyOld::mysql_result( $result, 0, "bus_invoice" );
	$sec_bus_order = Treat_DB_ProxyOld::mysql_result( $result, 0, "bus_order" );
	$sec_bus_payable = Treat_DB_ProxyOld::mysql_result( $result, 0, "bus_payable" );
	$sec_bus_inventor1 = Treat_DB_ProxyOld::mysql_result( $result, 0, "bus_inventor1" );
	$sec_bus_control = Treat_DB_ProxyOld::mysql_result( $result, 0, "bus_control" );
	$sec_bus_menu = Treat_DB_ProxyOld::mysql_result( $result, 0, "bus_menu" );
	$sec_bus_order_setup = Treat_DB_ProxyOld::mysql_result( $result, 0, "bus_order_setup" );
	$sec_bus_request = Treat_DB_ProxyOld::mysql_result( $result, 0, "bus_request" );
	$sec_route_collection = Treat_DB_ProxyOld::mysql_result( $result, 0, "route_collection" );
	$sec_route_labor = Treat_DB_ProxyOld::mysql_result( $result, 0, "route_labor" );
	$sec_route_detail = Treat_DB_ProxyOld::mysql_result( $result, 0, "route_detail" );
	$sec_route_machine = @Treat_DB_ProxyOld::mysql_result( $result, 0, "route_machine" );
	$sec_bus_labor = Treat_DB_ProxyOld::mysql_result( $result, 0, "bus_labor" );
	$sec_bus_budget = Treat_DB_ProxyOld::mysql_result( $result, 0, "bus_budget" );
	$sec_bus_menu_pos_report = Treat_DB_ProxyOld::mysql_result( $result, 0, "bus_menu_pos_report" );
	$sec_bus_menu_pos_mgmt = Treat_DB_ProxyOld::mysql_result( $result, 0, "bus_menu_pos_mgmt" );
	
	if( $sec_bus_menu_pos_mgmt < 1 ) {
		$location = "buscatermenu.php?bid=$businessid&cid=$companyid";
		header( 'Location: ./' . $location );
	}
?>
<!DOCTYPE html
	PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=8" />
		
		<title>POS Mgmt :: Treat America</title>
	
		<link rel="stylesheet" type="text/css" href="/assets/css/style.css" />
		<link rel="stylesheet" type="text/css" href="/assets/css/ta/generic.css" />
		<link rel="stylesheet" type="text/css" href="/assets/css/jq_themes/redmond/jqueryui-current.css" />
		
		<script type="text/javascript" src="/assets/js/jquery-current.min.js"></script>
		<script type="text/javascript" src="/assets/js/jqueryui-current.min.js"></script>
		<script type="text/javascript" src="/assets/js/dynamicdrive-disableform.js"></script>
		<script type="text/javascript" src="/assets/js/common.js"></script>
		<script type="text/javascript" src="/assets/js/ta/buscatermenu4-functions.js"></script>
	
		<script type="text/javascript" src="/assets/js/ta/buscatermenu6.js"></script>
	</head>
	<body>
	<div id="page_wrapper" class="page_wrapper">
	<table cellspacing='0' cellpadding='0' border='0' width='100%'>
		<tr>
			<td colspan='3'>
				<div id='headerMessage'></div>
				<a href='/ta/businesstrack.php'>
					<img src='/assets/images/logo.jpg' border='0' height='43' width='205' />
				</a>
				<br />
			</td>
		</tr>
<?php
	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM company WHERE companyid = '$companyid'";
	$result = Treat_DB_ProxyOld::query( $query );
	//mysql_close();
	
	$companyname = Treat_DB_ProxyOld::mysql_result( $result, 0, "companyname" );
	$com_logo = @Treat_DB_ProxyOld::mysql_result( $result, 0, "logo" );
	
	if( $com_logo == "" ) {
		$com_logo = "/ta/weblogo.gif";
	}
	
	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM security WHERE securityid = '$mysecurity'";
	$result = Treat_DB_ProxyOld::query( $query );
	//mysql_close();
	
	$sec_bus_sales = Treat_DB_ProxyOld::mysql_result( $result, 0, "bus_sales" );
	$sec_bus_invoice = Treat_DB_ProxyOld::mysql_result( $result, 0, "bus_invoice" );
	$sec_bus_order = Treat_DB_ProxyOld::mysql_result( $result, 0, "bus_order" );
	$sec_bus_payable = Treat_DB_ProxyOld::mysql_result( $result, 0, "bus_payable" );
	$sec_bus_inventor1 = Treat_DB_ProxyOld::mysql_result( $result, 0, "bus_inventor1" );
	$sec_bus_control = Treat_DB_ProxyOld::mysql_result( $result, 0, "bus_control" );
	$sec_bus_menu = Treat_DB_ProxyOld::mysql_result( $result, 0, "bus_menu" );
	$sec_bus_order_setup = Treat_DB_ProxyOld::mysql_result( $result, 0, "bus_order_setup" );
	$sec_bus_request = Treat_DB_ProxyOld::mysql_result( $result, 0, "bus_request" );
	$sec_route_collection = Treat_DB_ProxyOld::mysql_result( $result, 0, "route_collection" );
	$sec_route_labor = Treat_DB_ProxyOld::mysql_result( $result, 0, "route_labor" );
	$sec_route_detail = Treat_DB_ProxyOld::mysql_result( $result, 0, "route_detail" );
	$sec_bus_labor = Treat_DB_ProxyOld::mysql_result( $result, 0, "bus_labor" );
	$sec_bus_budget = Treat_DB_ProxyOld::mysql_result( $result, 0, "bus_budget" );
?>
		<tr bgcolor='#CCCCFF'>
			<td width='1%'>
				<img width="80" height="75" alt="" src='<?=$com_logo?>' />
			</td>
			<td>
				<font size=4><b><?=$businessname?> #<?=$bus_unit?></b></font><br />
				<?=$street?><br />
				<?=$city?>, <?=$state?> <?=$zip?><br />
				<?=$phone?>
			</td>
			<td align='right' valign='top'>
				<br />
				<form action='editbus.php' method='post'>
				<input type='hidden' name='username' value='<?=$user?>' />
				<input type='hidden' name='password' value='<?=$pass?>' />
				<input type='hidden' name='businessid' value='<?=$businessid?>' />
				<input type='hidden' name='companyid' value='<?=$companyid?>' />
				<input type='submit' value=' Store Setup ' />
				</form>
			</td>
		</tr>
			
		<tr>
			<td colspan='3'>
				<font color='#999999'>
<?php
	$menuLinks = array( );
	
	if( $sec_bus_sales > 0 ) {
		$menuLinks[] = array( 'busdetail.php', 'Sales' );
		//echo "<a href=busdetail.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Sales</font></a>";
	}
	if( $sec_bus_invoice > 0 ) {
		$menuLinks[] = array( 'businvoice.php', 'Invoicing' );
		//echo " | <a href=businvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Invoicing</font></a>";
	}
	if( $sec_bus_order > 0 ) {
		$menuLinks[] = array( 'buscatertrack.php', 'Orders' );
		//echo " | <a href=buscatertrack.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Orders</font></a>";
	}
	if( $sec_bus_payable > 0 ) {
		$menuLinks[] = array( 'busapinvoice.php', 'Payables' );
		//echo " | <a href=busapinvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Payables</font></a>";
	}
	if( $sec_bus_inventor1 > 0 ) {
		$menuLinks[] = array( 'businventory.php', 'Inventory' );
		//echo " | <a href=businventor.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Inventory</font></a>";
	}
	if( $sec_bus_control > 0 ) {
		$menuLinks[] = array( 'buscontrol.php', 'Control Sheet' );
		//echo " | <a href=buscontrol.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Control Sheet</font></a>";
	}
	if( $sec_bus_menu > 0 ) {
		$menuLinks[] = array( 'buscatermenu.php', 'Menus', 'red' );
		//echo " | <a href=buscatermenu.php?bid=$businessid&cid=$companyid><font color=red onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='red' style='text-decoration:none'>Menus</font></a>";
	}
	if( $sec_bus_order_setup > 0 ) {
		$menuLinks[] = array( 'buscaterroom.php', 'Order Setup' );
		//echo " | <a href=buscaterroom.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Order Setup</font></a>";
	}
	if( $sec_bus_request > 0 ) {
		$menuLinks[] = array( 'busrequest.php', 'Requests' );
		//echo " | <a href=busrequest.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Requests</font></a>";
	}
	if( $sec_route_collection > 0 || $sec_route_labor > 0 || $sec_route_detail > 0
			|| $sec_route_machine > 0 ) {
		$menuLinks[] = array( 'busroute_collect.php', 'Route Collections' );
		//echo " | <a href=busroute_collect.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Route Collections</font></a>";
	}
	if( $sec_bus_labor > 0 ) {
		$menuLinks[] = array( 'buslabor.php', 'Labor' );
		//echo " | <a href=buslabor.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Labor</font></a>";
	}
	if( $sec_bus_budget > 0 || ( $businessid == $bid && $pr1 == 1 )
			|| ( $businessid == $bid2 && $pr2 == 1 ) || ( $businessid == $bid3 && $pr3 == 1 )
			|| ( $businessid == $bid4 && $pr4 == 1 ) || ( $businessid == $bid5 && $pr5 == 1 )
			|| ( $businessid == $bid6 && $pr6 == 1 ) || ( $businessid == $bid7 && $pr7 == 1 )
			|| ( $businessid == $bid8 && $pr8 == 1 ) || ( $businessid == $bid9 && $pr9 == 1 )
			|| ( $businessid == $bid10 && $pr10 == 1 ) ) {
		$menuLinks[] = array( 'busbudget.php', 'Budget' );
		//echo " | <a href=busbudget.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Budget</font></a>";
	}
	
	$menuHtml = array( );
	foreach( $menuLinks as $link ) {
		if( empty( $link[2] ) )
			$link[2] = '#999999';
		$menuHtml[] = "<a href='$link[0]?bid=$businessid&cid=$companyid'><font color='$link[2]' onmouseover='this.style.color=\"#FF9900\"' onmouseout='this.style.color=\"$link[2]\"' style='text-decoration: none;'>$link[1]</font></a>";
	}
	
	echo implode( ' | ', $menuHtml );
?>
				</font>
			</td>
		</tr>
	</table>
	<p></p>
<?php
	if( $security_level > 0 ) {
?>
	<form action='businvoice.php' method='post'>
	<select name='gobus' onchange='changePage(this.form.gobus)'>
<?php
		$districtquery = "";
		if( $security_level > 2 && $security_level < 7 ) {
			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query = "SELECT * FROM login_district WHERE loginid = '$loginid'";
			$result = Treat_DB_ProxyOld::query( $query );
			$num = Treat_DB_ProxyOld::mysql_numrows( $result );
			//mysql_close();
			
			$num--;
			while( $num >= 0 ) {
				$districtid = Treat_DB_ProxyOld::mysql_result( $result, $num, "districtid" );
				if( $districtquery == "" ) {
					$districtquery = "districtid = '$districtid'";
				}
				else {
					$districtquery = "$districtquery OR districtid = '$districtid'";
				}
				$num--;
			}
		}
		
		if( $security_level > 6 ) {
			$query = "SELECT * FROM business WHERE companyid = '$companyid' ORDER BY businessname DESC";
		}
		elseif( $security_level == 2 || $security_level == 1 ) {
			$query = "SELECT * FROM business WHERE companyid = '$companyid' AND (businessid = '$bid' OR businessid = '$bid2' OR businessid = '$bid3' OR businessid = '$bid4' OR businessid = '$bid5' OR businessid = '$bid6' OR businessid = '$bid7' OR businessid = '$bid8' OR businessid = '$bid9' OR businessid = '$bid10') ORDER BY businessname DESC";
		}
		elseif( $security_level > 2 && $security_level < 7 ) {
			$query = "SELECT * FROM business WHERE companyid = '$companyid' AND ($districtquery) ORDER BY businessname DESC";
		}
		$result = Treat_DB_ProxyOld::query( $query );
		$num = Treat_DB_ProxyOld::mysql_numrows( $result );
		
		$num--;
		while( $num >= 0 ) {
			$businessname = Treat_DB_ProxyOld::mysql_result( $result, $num, "businessname" );
			$busid = Treat_DB_ProxyOld::mysql_result( $result, $num, "businessid" );
			
			if( $busid == $businessid ) {
				$show = "selected='selected'";
			}
			else {
				$show = "";
			}
?>
		<option value='buscatermenu6.php?bid=<?=$busid?>&cid=<?=$companyid?>' <?=$show?>><?=$businessname?></option>
<?php
			$num--;
		}
?>
	</select>
	</form>
	<p></p>
<?php
	}
	
	///// TABS
?>
			<table cellspacing="0" cellpadding="0" width="100%" class="tabbed">
				<tbody>
					<tr valign="top">
<?php
	
	$tabLinks = array( 
		array( 'buscatermenu.php', 'Menus' ),
		array( 'buscatermenu3.php', 'POS Menu Linking' ),
		array( 'buscatermenu4.php', 'POS Menu' ),
		array( 'buscatermenu5.php', 'POS Reporting' ),
		array( 'buscatermenu6.php', 'POS Mgmt', true )
	);
	
	foreach( $tabLinks as $tab ) {
		$currentTab = $tab[2] ? 'current_tab' : '';
		$img = $tab[2] ? '' : '2';
?>
						<td width="1%" class="<?=$currentTab?> left_corner"
							><img src="/assets/images/corners/leftcrn<?=$img?>.jpg" height="6" width="6" alt=""
						/></td>
						<td width="13%" class="<?=$currentTab?> label">
							<a href="/ta/<?=$tab[0]?>?cid=<?=$companyid?>&amp;bid=<?=$businessid?>" 
								style="<?=$style?>"><?=$tab[1]?></a>
						</td>
						<td width="1%" class="<?=$currentTab?> right_corner"
							><img src="/assets/images/corners/rightcrn<?=$img?>.jpg" height="6" width="6" alt=""
						/></td>
						
						<td width="1%"></td>
<?php
	}
	
?>
					</tr>
					<tr height="2">
<?php
	
	foreach( $tabLinks as $tab ) {
		if( $tab[2] ) {
?>
						<td colspan="3" bgcolor="#e8e7e7"></td>
						<td></td>
<?php
		} else {
?>
						<td colspan="4"></td>
<?php
		}
	}
	
?>
					</tr>
				</tbody>
			</table>
	
			<table cellspacing='0' cellpadding='0' border='0' width='100%'>
				<tr bgcolor='#E8E7E7'>
					<td colspan='2'>&nbsp;</td>
				</tr>
				<tr height='1' bgcolor='black' valign='top'>
					<td colspan='2'>
						<img src='spacer.gif' alt=''>
					</td>
				</tr>
				<tr bgcolor='#E8E7E7'>
					<td colspan='2'>
						<br />
						<table width='99%' cellspacing='0' cellpadding='0' style='margin-left: auto; margin-right: auto;'>
<?php
	//$goto = $_GET["goto"];
	$goto = \EE\Controller\Base::getGetVariable('goto');
	if( $goto == 1 ) {
		$showcust = "none";
		$showcfo = "block";
	}
	else {
		$showcust = "block";
		$showcfo = "none";
	}

	///first column (nav)
?>
							<tr bgcolor='#E8E7E7' valign='top'>
								<td width='20%'>
									<div style="border: 2px solid #CCCCCC; background: white; width: 98%;">
										<div style="background: #CCCCCC; width: 100%; text-align: center;">
											<b>Category</b>
										</div>
<?php 
	$navLinks = array(
		array( 'customer', 'Customers' ),
		array( 'cards', 'Card Numbers' ),
		array( 'groups', 'Menu Groups' ),
		array( 'taxes', 'Taxes' ),
		array( 'promos', 'Promotions' ),
		array( 'commissary', 'Cold Food Orders' )
	);
	
	foreach( $navLinks as $link ) {
?>
										&nbsp;
										<a onclick="$('.navPanel').hide( ); $('#<?=$link[0]?>').show( );" 
											style="text-decoration: none; color: blue; cursor: pointer;"
											onmouseover='this.style.color="#FF9900"' 
											onmouseout='this.style.color="blue"'><?=$link[1]?></a>
										<br />
<?php 
	}
?>
									</div>
	
									<p style='text-align: center;'><input type='button' value='Sync All' onclick="sync_all(<?php echo $businessid; ?>);" /></p>
	
								</td>
								<td width=2%>&nbsp;</td>
								<td>
<?php
	//////////////////////
	/////CUSTOMERS////////
	//////////////////////
?>

									<div id='customer' class='navPanel' style="display: <?=$showcust?>;">
										<div class='navPanelTitle'>Customers</div>
										<table width='100%'>
											<tr valign='top'>
												<td width='25%'>
													<div id='customer_list' 
														style="height:275px; width:100%; overflow:auto;">
<?php
	$query = "SELECT * FROM customer WHERE businessid = $businessid ORDER BY last_name,first_name";
	$result = Treat_DB_ProxyOld::query( $query );
	
	while( $r = Treat_DB_ProxyOld::mysql_fetch_array( $result ) ) {
		$customerid = $r["customerid"];
		$last_name = $r["last_name"];
		$first_name = $r["first_name"];
		$email = $r["username"];
		$user_pass = $r["password"];
		$scancode = $r["scancode"];
		$balance = $r["balance"];
		
		$show_scancode = substr( $scancode, 6, 7 );
		
		echo "<a href=\"javascript:void(0);\" id='cust$customerid' onclick=\"set_customer($customerid,'$last_name','$first_name','$email','$scancode','$balance');\" style=\"text-decoration:none;\"><font color=black>[$show_scancode] $last_name, $first_name</font></a><br>";
	}
?>
													</div>
												</td>
												<td>
													<form action="#" method='post'>
														<input type='hidden' name='businessid' value='<?=$businessid?>' id='cust_bid' />
														<input type='hidden' name='customerid' value='0' id='customerid' />
														<table width='95%' style='margin-left: auto; margin-right: auto;'>
															<tr>
																<td style='text-align: right;'>
																	Email:
																</td>
																<td>
																	<input type='text' size='40' id='email' value='' />
																</td>
																<td rowspan='5' width='15%' align='right'>
																	<div id='balance' 
																		style="text-align: center; border: 2px solid black; background:#FFFF99;">
																			Balance<br />
																			<b>$0.00</b>
																	</div>
																</td>
															</tr>
															<tr>
																<td style='text-align: right;'>
																	Password:
																</td>
																<td>
																	<input type='text' size='40' id='user_pass' value='' />
																	<font size=2>*Leave blank if not changing</font>
																</td>
															</tr>
															<tr>
																<td style='text-align: right;'>
																	First/Last Name:
																</td>
																<td>
																	<input type='text' size='20' id='first_name' value='' />
																	<input type='text' size='20' id='last_name' value='' />
																</td>
															</tr>
															<tr>
																<td style='text-align: right;'>
																	Card#:
																</td>
																<td>
																	<input type='text' size='20' id='card_num' value='' />
																</td>
															</tr>
															<tr>
																<td></td>
																<td>
																	<input type='button' value='Submit' id='cust_submit' onclick="submit_customer();" /> 
																	<input type='button' value='Clear' onclick="set_customer(0,'','','','');" />
																</td>
															</tr>
														</table>
													</form>

													<hr width='95%'>

													<div style='text-align: center;'>
														<form action="#" method='post'>
															<input type='hidden' name='cust_id' id='cust_id' value='0' />
															Add value to account: 
															$<input type='text' size='3' name='dol_value' id='dol_value' value='' /> 
															Reason:<select name='transtype' id='transtype'>
																<option value='3'>Refund</option>
																<option value='4'>Promo</option>
															</select>
															<input type='submit' value='GO' id='trans_submit' onclick="submit_trans();">
														</form>
													</div>
													<div id='cust_trans' style="width:95%"></div>

												</td>
											</tr>
										</table>
	
									</div>
<?php
	//////////////////////
	////CARD NUMBERS//////
	//////////////////////
	echo "<div id='cards' class='navPanel' style=\"display: none;\">";
	echo "<div class='navPanelTitle'>Card Numbers</div>";
	
	echo "<ul id='card_list'>";
	$query = "SELECT * FROM customer_scancodes WHERE businessid = $businessid ORDER BY scan_start";
	$result = Treat_DB_ProxyOld::query( $query );
	
	while( $r = Treat_DB_ProxyOld::mysql_fetch_array( $result ) ) {
		$id = $r["id"];
		$scan_start = $r["scan_start"];
		$scan_end = $r["scan_end"];
		$scan_value = $r["value"];
		
		$scan_value = number_format( $scan_value, 2 );
		
		echo "<li id='card$id' onmouseover=\"this.style.cursor='pointer';\" onclick=\"update_cards($id,'$scan_start','$scan_end','$scan_value');\">$scan_start - $scan_end [$$scan_value]";
	}
	echo "</ul>";
	
	////add scan range
	echo "<ul><li><form action=\"javascript:void(0);\" method=post style=\"margin:0;padding:0;display:inline;\"><input type=hidden name=scan_bid id='scan_bid' value=$businessid><input type=hidden name=scan_id id='scan_id' value=0><input type=text size=11 name=scan_start id='scan_start' value=''> - <input type=text size=11 name=scan_end id='scan_end' value=''> $<input type=text size=4 name=scan_value id='scan_value' value=''> <input type=submit value='Submit' id='scan_submit' onclick=\"submit_cards();\"></form><input type=button value='Clear' onclick=\"update_cards(0,'','','');\"";
	echo "</ul>";
	echo "</div>";
	//////////////////////
	////MENU GROUPS///////
	//////////////////////
	echo "<div id='groups' class='navPanel' style=\"display: none;\">";
	echo "<div class='navPanelTitle'>Menu Groups</div>";
	
	echo "<ul id='group_list'>";
	$query = "SELECT * FROM menu_groups_new WHERE businessid = $businessid ORDER BY pos_id";
	$result = Treat_DB_ProxyOld::query( $query );
	
	while( $r = Treat_DB_ProxyOld::mysql_fetch_array( $result ) ) {
		$id = $r["id"];
		$pos_id = $r["pos_id"];
		$name = $r["name"];
		
		echo "<li id='group$id' onmouseover=\"this.style.cursor='pointer';\"  onclick=\"update_group($id,'$name');\">$name";
	}
	echo "</ul>";
	
	////add group
	echo "<ul><li><form action=\"javascript:void(0);\" method=post style=\"margin:0;padding:0;display:inline;\"><input type=hidden name=bid id='grpbid' value=$businessid><input type=hidden name=group_id id='group_id' value=0><input type=text size=15 name=group_name id='group_name' value=''><input type=submit value='Submit' id='group_submit' onclick=\"submit_group();\"></form><input type=button value='Clear' onclick=\"update_group(0,'');\"";
	
	echo "</ul><p>";
	
	///tax for menu group
	echo "<div id='group_tax' style=\"display:none;border:2px solid #CCCCCC;width:400px;\">";
	echo "<div style=\"background: #CCCCCC;\"><center><b>Update Item Tax</b></center></div>";
	echo "&nbsp;<form action=\"javascript:void(0);\" method=post><input type=hidden name=group_tax_id id='group_tax_id' value=0>";
	
	$query = "SELECT * FROM menu_tax WHERE businessid = $businessid ORDER BY pos_id LIMIT 3";
	$result = Treat_DB_ProxyOld::query( $query );
	
	while( $r = Treat_DB_ProxyOld::mysql_fetch_array( $result ) ) {
		$id = $r["id"];
		$pos_id = $r["pos_id"];
		$name = $r["name"];
		$percent = $r["percent"];
		
		echo "&nbsp;&nbsp;<a href=\"javascript:void(0);\" onclick=\"update_group_taxes(1,$id);\" title='Add tax to all items in this group'><img src=\"../projects/plus.gif\" border=0></a> <a href=\"javascript:void(0);\" onclick=\"update_group_taxes(2,$id);\" title='Remove tax from items in this group'><img src=delete.gif border=0></a> $name ($percent%)<br>";
	}
	
	echo "</form>";
	
	echo "</div>";
	
	echo "</div>";
	//////////////////////////
	///////TAXES//////////////
	//////////////////////////
	echo "<div id='taxes' class='navPanel' style=\"display: none;\">";
	echo "<div class='navPanelTitle'>Taxes</div>";
	
	echo "<ul id='tax_list'>";
	$query = "SELECT * FROM menu_tax WHERE businessid = $businessid ORDER BY pos_id";
	$result = Treat_DB_ProxyOld::query( $query );
	
	while( $r = Treat_DB_ProxyOld::mysql_fetch_array( $result ) ) {
		$id = $r["id"];
		$pos_id = $r["pos_id"];
		$name = $r["name"];
		$percent = $r["percent"];
		
		echo "<li id='tax$id' onmouseover=\"this.style.cursor='pointer';\"  onclick=\"update_tax($id,'$name','$percent');\">$name ($percent%)";
	}
	
	echo "</ul>";
	
	////add tax
	echo "<ul><li><form action=\"javascript:void(0);\" id='tax_form' method=post style=\"margin:0;padding:0;display:inline;\"><input type=hidden id='bid' name=businessid value=$businessid><input type=hidden name=tax_id id='tax_id' value=0><input type=text size=15 name=tax_name id='new_tax_name' value=''> <input type=text size=5 name=percent id='new_percent' value=''>% <input type=submit value='Submit' id='tax_submit' onclick=\"submit_tax();\"></form><input type=button value='Clear' onclick=\"update_tax(0,'','');\"</ul>";
	
	echo "</div>";
	//////////////////////////
	///////PROMOS/////////////
	//////////////////////////
	echo "<div id='promos' class='navPanel' style=\"display: none;\">";
	echo "<div class='navPanelTitle'>Promotions</div>";
	echo '<script language="javascript" type="text/javascript" src="/assets/js/kioskPromotions.js"></script>';
	echo '<link rel="stylesheet" type="text/css" href="/assets/css/settingsForm.css" />';
	echo '<div class="divTable"><div class="divTableRow">';
	echo "<div class='divTableCell' style='width: 200px;'>";
	echo "<select style='width: 100%;' size='16' id='promoList'></select>";
	echo "<button id='movePromoUp' class='promoMove'>Move Up</button>";
	echo "<button id='movePromoDown' class='promoMove'>Move Down</button>";
	echo "</div>";
	
	////add promo
	echo "<div class='divTableCell'>";
	echo "<div class='settingsContainer'>";
	
	echo "<div class='settingsHeader ui-state-default'>Basic Settings</div>";
	
	echo "<form class='settingsForm' id='promoForm' onsubmit='return false;' method='post'><input type=hidden name=bid id='promobid' value=$businessid><input type=hidden name=promo_id id='promo_id' value=0>";
	echo "<input style='float: right;' class='promoOption' type='submit' id='promo_submit' value='Save' />";
	
	echo "Name: <input class='promoOption' type=text size=15 name=promo_name id='promo_name' value=''>";
	
	echo "Tax Rate: <select class='promoOption' name='promo_tax' id='promo_tax'>";
	echo '<option value="">None</option>';
	
	$query = "SELECT * FROM menu_tax WHERE businessid=$businessid ORDER BY id";
	$result = Treat_DB_ProxyOld::query( $query );
	
	while( $r = Treat_DB_ProxyOld::mysql_fetch_array( $result ) ) {
		$targetid = $r["id"];
		$target_name = $r["name"];
		
		echo "<option value=$targetid selected=false>$target_name</option>";
	}
	
	echo "</select>";
	
	echo "<br />";
	echo "<button style='float: right; clear: both;' class='promoOption' id='deletePromo'>Deactivate</button>";
	
	echo "Trigger: <select class='promoOption' name=promo_trigger id='promo_trigger'>";
	
	$query = "SELECT * FROM promo_trigger ORDER BY id";
	$result = Treat_DB_ProxyOld::query( $query );
	
	while( $r = Treat_DB_ProxyOld::mysql_fetch_array( $result ) ) {
		$triggerid = $r["id"];
		$trigger_name = $r["name"];
		
		echo "<option value=$triggerid selected=false>$trigger_name</option>";
	}
	
	echo "</select>";
	
	echo "<span class='promoAmount'>";
	echo "<select class='promoOption' name='promo_trigger_amount_limit' id='promo_trigger_amount_limit'>";
	echo "<option value='single'>Once per transaction</option>";
	echo "<option value='multiple'>Unlimited</option>";
	echo "</select>";
	echo "</span>";
	
	echo "<span class='promoAmount'>";
	echo "<br />";
	echo "</span>";
	
	echo "<span class='promoAmount'>";
	echo "Trigger On: <select class='promoOption' name='promo_trigger_amount_type' id='promo_trigger_amount_type'>";
	echo "<option value='by_qty'>Quantity</option>";
	echo "<option value='by_subtotal'>Subtotal</option>";
	echo "</select>";
	echo "</span>";
	
	echo "<span class='promoAmount'>";
	echo "Value: <input type='text' name='promo_trigger_amount' id='promo_trigger_amount' size='5' />";
	echo "</span>";
	
	echo "<span class='promoAmount2'>";
	echo "Value 2: <input type='text' name='promo_trigger_amount_2' id='promo_trigger_amount_2' size='5' />";
	echo "</span>";
	
	echo "<br />";
	
	echo "Promotion Type: <select class='promoOption' name=promo_type id='promo_type'>";
	
	$query = "SELECT * FROM promo_type ORDER BY id";
	$result = Treat_DB_ProxyOld::query( $query );
	
	while( $r = Treat_DB_ProxyOld::mysql_fetch_array( $result ) ) {
		$typeid = $r["id"];
		$type_name = $r["name"];
		
		echo "<option value=$typeid selected=false>$type_name</option>";
	}
	echo "</select>";
	
	echo "<span class='promoDiscount promo_amount_off promo_fixed_value promo_cheapest_one_free promo_discount_after_n promo_percentage_off'>";
	echo "Value: <input class='promoOption' type=text name=promo_value id='promo_value' value='' size=6>";
	echo "</span>";
	
	echo "<span class='promoDiscount promo_amount_off promo_fixed_value'>";
	echo "Discount: <select class='promoOption' id='promo_discount' name='promo_option_discount'>";
	echo "<option value='by_qty'>Each Item</option>";
	echo "<option value='by_subtotal'>Item Subtotal</option>";
	echo "</select>";
	echo "</span>";
	
	echo "<span class='promoDiscount promo_cheapest_one_free promo_discount_after_n'>";
	echo "<br />";
	echo "Discount Type: <select class='promoOption' id='promo_discount_type' name='promo_option_discount_type'>";
	echo "<option value='by_fixed_price'>Fixed Price</option>";
	echo "<option value='by_amount_off'>Amount Off</option>";
	echo "<option value='by_percentage_off'>Percent</option>";
	echo "</select>";
	echo "</span>";
	
	echo "<span class='promoDiscount promo_cheapest_one_free promo_discount_after_n'>";
	echo "Limit: <input type='text' class='promoOption' id='promo_discount_limit' name='promo_option_discount_limit' size='5' />";
	echo "</span>";
	
	echo "<span class='promoDiscount promo_discount_after_n'>";
	echo "Discount N: <input type='text' class='promoOption' id='promo_discount_n' name='promo_option_discount_n' size='5' />";
	echo "</span>";
	
	echo "</form></div>";
	
	echo "<div id='promo_schedule'></div>";
	
	echo "<div id='promo_detail'></div>";
	
	echo "</div></div></div></div>";
	
	echo "</div>";
	//////////////////////////
	///////ORDERS/////////////
	//////////////////////////
	echo "<div id='orders' class='navPanel' style=\"display: none;\">";
	echo "<div class='navPanelTitle'>Order Setup</div>";
	
	$order_types = array(
		1 => "Commissary", 2 => "Pick to Light", 3 => "Email"
	);
	
	$query = "SELECT machine_bus_link.*,vend_machine.name,vend_machine.login_routeid FROM machine_bus_link
				JOIN vend_machine ON vend_machine.machine_num = machine_bus_link.machine_num
				WHERE machine_bus_link.businessid = $businessid ORDER BY machine_bus_link.client_programid,machine_bus_link.machine_num";
	$result = Treat_DB_ProxyOld::query( $query );
	
	echo "<form action=\"javascript:void(0);\" id='tax_form' method=post style=\"margin:0;padding:0;display:inline;\"><input type=hidden id='bid' name=businessid value=$businessid> <ul>";
	while( $r = Treat_DB_ProxyOld::mysql_fetch_array( $result ) ) {
		$machine_num = $r["machine_num"];
		$name = $r["name"];
		$ordertype = $r["ordertype"];
		$id = $r["id"];
		$leadtime = $r["leadtime"];
		
		echo "<li><span style=\"width:25%;\">[$machine_num] $name</span> <span style=\"width:25%;\">Type:<select name='ot$id'>";
		
		foreach( $order_types AS $key => $value ) {
			if( $ordertype == $key ) {
				$sel = "SELECTED";
			}
			else {
				$sel = "";
			}
			
			echo "<option value=$key $sel>$value</option>";
		}
		echo "</select></span><span style=\"width:25%;\"> Lead Time in Days:<input type=text size=5 name=lt$id value=$leadtime></span>";
	}
	echo "<span style=\"width=75%;text-align:right;\"><input type=submit value='Save' id='ot_submit' onclick=\"submit_ordertype($businessid);\">";
	echo "</ul></form>";
	echo "</div>";
	//////////////////////////
	///////COMMISSARY/////////
	//////////////////////////
	echo "<div id='commissary' class='navPanel' style=\"display: $showcfo;\">";
	echo "<div class='navPanelTitle'>Cold Food Orders</div>";
	
	$query = "SELECT 
							machine_bus_link.machine_num
							,vend_machine.name
							,vend_machine.login_routeid
							,vend_machine.menu_typeid
							,vend_machine.machineid
							,login_route.route
						FROM machine_bus_link
						JOIN vend_machine ON vend_machine.machine_num = machine_bus_link.machine_num AND vend_machine.is_deleted = 0
						JOIN login_route ON login_route.login_routeid = vend_machine.login_routeid
						WHERE machine_bus_link.businessid = $businessid AND machine_bus_link.ordertype = 1";
	$result = Treat_DB_ProxyOld::query( $query );
	
	$machine_num = @Treat_DB_ProxyOld::mysql_result( $result, 0, "machine_num" );
	$name = @Treat_DB_ProxyOld::mysql_result( $result, 0, "name" );
	$menu_typeid = @Treat_DB_ProxyOld::mysql_result( $result, 0, "menu_typeid" );
	$login_routeid = @Treat_DB_ProxyOld::mysql_result( $result, 0, "login_routeid" );
	$machineid = @Treat_DB_ProxyOld::mysql_result( $result, 0, "machineid" );
	$route = @Treat_DB_ProxyOld::mysql_result( $result, 0, "route" );
	
	if( $machine_num != '' ) {
		$date2 = date( "Y-m-d" );
		$yesterday = date( "Y-m-d", mktime( 0, 0, 0, date( "m" ), date( "d" ) - 1, date( "Y" ) ) );
		
		$ship_dates = Kiosk::comm_inTransit_dates( $machine_num );
		
		$machine_num[0] = $machine_num;
		
		/////get ordering businessid
		$query = "SELECT businessid FROM menu_type WHERE menu_typeid = $menu_typeid";
		$result = Treat_DB_ProxyOld::query( $query );
		
		$order_bus = Treat_DB_ProxyOld::mysql_result( $result, 0, "businessid" );
		
		////correct order date
		$hour = date( "H" );
		$order_date = $date2;
		
		/////move to Monday if weekend
		if( dayofweek( $order_date ) == "Saturday" ) {
			$order_date = nextday( $order_date );
			$order_date = nextday( $order_date );
		}
		if( dayofweek( $order_date ) == "Sunday" ) {
			$order_date = nextday( $order_date );
		}
		
		///always 2 days in advance
		$order_date = nextday( $order_date );
		$order_date = nextday( $order_date );
		
		////if date lands on weekend
		if( dayofweek( $order_date ) == "Saturday" || dayofweek( $order_date ) == "Sunday" ) {
			$order_date = nextday( $order_date );
			$order_date = nextday( $order_date );
		}
		
		///if after 10
		if( $hour >= 10 ) {
			$order_date = nextday( $order_date );
		}
		if( dayofweek( $order_date ) == "Saturday" ) {
			$order_date = nextday( $order_date );
			$order_date = nextday( $order_date );
		}
		
		////scheduled?
		if( dayofweek( $order_date ) == "Monday" ) {
			$day = 1;
		}
		elseif( dayofweek( $order_date ) == "Tuesday" ) {
			$day = 2;
		}
		elseif( dayofweek( $order_date ) == "Wednesday" ) {
			$day = 3;
		}
		elseif( dayofweek( $order_date ) == "Thursday" ) {
			$day = 4;
		}
		elseif( dayofweek( $order_date ) == "Friday" ) {
			$day = 5;
		}
		
		$query = "SELECT * FROM vend_machine_schedule WHERE machineid = $machineid AND day = $day";
		$result = Treat_DB_ProxyOld::query( $query );
		
		$scheduleid = Treat_DB_ProxyOld::mysql_result( $result, 0, "id" );
		
		///next order
		$query = "SELECT * FROM vend_machine_schedule WHERE machineid = $machineid ORDER BY day";
		$result = Treat_DB_ProxyOld::query( $query );
		
		while( $r = Treat_DB_ProxyOld::mysql_fetch_array( $result ) ) {
			$mach_schedule[$r["day"]] = $r["id"];
		}
		
		$next_order_date = nextday( $order_date );
		for( $date_count = 1; $date_count <= 7; $date_count++ ) {
			////scheduled?
			if( dayofweek( $next_order_date ) == "Monday" ) {
				$next_order_day = 1;
			}
			elseif( dayofweek( $next_order_date ) == "Tuesday" ) {
				$next_order_day = 2;
			}
			elseif( dayofweek( $next_order_date ) == "Wednesday" ) {
				$next_order_day = 3;
			}
			elseif( dayofweek( $next_order_date ) == "Thursday" ) {
				$next_order_day = 4;
			}
			elseif( dayofweek( $next_order_date ) == "Friday" ) {
				$next_order_day = 5;
			}
			elseif( dayofweek( $next_order_date ) == "Saturday" ) {
				$next_order_day = 6;
			}
			elseif( dayofweek( $next_order_date ) == "Sunday" ) {
				$next_order_day = 7;
			}
			
			////if schedule exists
			if( array_key_exists( $next_order_day, $mach_schedule ) ) {
				break;
			}
			
			$next_order_date = nextday( $next_order_date );
		}
		
		///remove weekends if not 24 operation
		$temp_date = $order_date;
		$temp_days = $date_count;
		while( $temp_date <= $next_order_date ) {
			if( $operating_days <= 6 && dayofweek( $temp_date ) == "Sunday" ) {
				$temp_days--;
			}
			elseif( $operating_days <= 5 && dayofweek( $temp_date ) == "Saturday" ) {
				$temp_days--;
			}
			$temp_date = nextday( $temp_date );
		}
		
		///operating days until order arrives
		$temp_date2 = nextday( $date2 );
		$temp_days2 = 0;
		while( $temp_date2 <= $order_date ) {
			$temp_days2++;
			if( $operating_days < 7 && dayofweek( $temp_date2 ) == "Sunday" ) {
				$temp_days2--;
			}
			elseif( $operating_days < 6 && dayofweek( $temp_date2 ) == "Saturday" ) {
				$temp_days2--;
			}
			$temp_date2 = nextday( $temp_date2 );
		}
		
		///display order
		$showday = dayofweek( $order_date );
		$shownextday = dayofweek( $next_order_date );
		
		echo "<div style=\"background-color:orange;width:100%;border:1px solid black;\">&nbsp;$showday ($order_date) order for Route# $route [" . $machine_num . "]&nbsp;|&nbsp;<i>Next Order for $shownextday ($next_order_date) in $date_count Day(s)</i>&nbsp;</div>";
		
		if( $scheduleid > 0 ) {
			$query = "SELECT operate FROM business WHERE businessid = $businessid";
			$result = Treat_DB_ProxyOld::query( $query );
			
			$operating_days = Treat_DB_ProxyOld::mysql_result( $result, 0, "operate" );
			
			$fourteen_days = date( "Y-m-d", mktime( 0, 0, 0, date( "m" ), date( "d" ) - 14, date( "Y" ) ) );
			
			$query = "SELECT
								menu_groups.groupname
								,menu_pricegroup.menu_pricegroupname
								,menu_items.item_name
								,menu_items.menu_item_id
								,menu_items.item_code
								,menu_items_new.id AS myid
								,menu_items_new.pos_id
								,menu_items_new.rollover
								,vend_item.vend_itemid
								,vend_order.amount
							FROM menu_items
							JOIN menu_pricegroup ON menu_pricegroup.menu_pricegroupid = menu_items.groupid
							JOIN menu_groups ON menu_groups.menu_group_id = menu_pricegroup.menu_groupid
							JOIN vend_item ON vend_item.menu_itemid = menu_items.menu_item_id AND vend_item.date = '$order_date'
							LEFT JOIN vend_order ON vend_order.vend_itemid = vend_item.vend_itemid AND vend_order.login_routeid = $login_routeid AND vend_order.date = '$order_date' AND vend_order.machineid = $machineid
							LEFT JOIN menu_items_new ON menu_items_new.item_code = menu_items.item_code AND menu_items_new.businessid = $businessid
							WHERE menu_items.businessid = $order_bus AND menu_items.menu_typeid = $menu_typeid
							ORDER BY menu_groups.orderid,menu_pricegroup.orderid,menu_items.item_name";
			$result = Treat_DB_ProxyOld::query( $query );
			
			///main table
			echo "<form action=savekioskorder.php method=post style=\"margin:0;padding:0;display:inline;\">
							<input type=hidden name=businessid value=$businessid>
							<input type=hidden name=companyid value=$companyid>
							<input type=hidden name=order_date value='$order_date'>
							<input type=hidden name=order_bus value=$order_bus>
							<input type=hidden name=login_routeid value=$login_routeid>
							<input type=hidden name=menu_typeid value=$menu_typeid>
							<input type=hidden name=machineid value=$machineid>";
			
			echo "<table width=100%><tr valign=top><td width=100%>";
			
			echo "<table cellspacing=0 style=\"border:1px solid black;font-size:12px;width:100%;\">";
			$lastgroup = "";
			$lastpgroup = "";
			while( $r = Treat_DB_ProxyOld::mysql_fetch_array( $result ) ) {
				$menu_itemid = $r["menu_item_id"];
				$item_name = $r["item_name"];
				$item_code = $r["item_code"];
				$pgroup_name = $r["menu_pricegroupname"];
				$group_name = $r["groupname"];
				$myid = $r["myid"];
				$rollover = $r["rollover"];
				$pos_id = $r["pos_id"];
				$vend_itemid = $r["vend_itemid"];
				$amount = $r["amount"];
				
				if( $lastgroup != $group_name ) {
					if( $lastgroup != "" ) {
						echo "</table><p><table cellspacing=0 style=\"border:1px solid black;font-size:12px;width:100%;\">";
					}
					echo "<tr bgcolor=black><td colspan=10><font color=white><b>&nbsp;$group_name</b></font></td></tr>";
				}
				if( $lastpgroup != $pgroup_name ) {
					echo "<tr bgcolor=#FFFF99><td colspan=4><i>&nbsp;$pgroup_name</i></td><td title='Roll Over'><i>RO</i></td><td title='2 Week Daily Average'><i>AVG</i></td><td title='Current OnHand'><i>OHD</i></td><td title='In Transit'><i>TRN</i></td><td title='Suggested'><i>SGT</i></td><td><i>Order</i></td></tr>";
				}
				
				/////////////////////
				////onhand amounts///
				/////////////////////
				////get dates
				$dates = Kiosk::last_inv_dates( $machine_num, $item_code );
				$date = $dates[1];
				if( $date == "" ) {
					$date = $dates[0];
				}
				
				////last count
				$onhand2 = Kiosk::inv_onhand( $machine_num, $item_code, $date );
				
				////fills
				$fills = Kiosk::fills( $machine_num, $item_code, $date, $date2 );
				
				////waste
				$waste = Kiosk::waste( $machine_num, $item_code, $date, $date2 );
				
				////get check detail
				$items_sold = menu_items::items_sold( $pos_id, $businessid, $date, $date2, $current_terminal );
				
				////in transit
				if( count( $ship_dates ) > 0 ) {
					$inTransit = Kiosk::comm_inTransit_amt( $ship_dates, $item_code, $menu_typeid,
							$order_bus, $machineid );
				}
				else {
					$inTransit = 0;
				}
				
				if( $inTransit == 0 ) {
					$inTransit = 0;
				}
				
				////average daily sales for last 2 weeks
				$items_sold14 = menu_items::items_sold( $pos_id, $businessid, $fourteen_days, $yesterday, $current_terminal );
				
				$avg_daily_sales = round( $items_sold14 / ( $operating_days * 2 ), 1 );
				
				////onhand amount
				$onhand = $onhand2 + $fills - $waste - $items_sold;
				
				////suggested
				if($onhand < 0){$newonhand = 0;}
				else{$newonhand = $onhand;}

				$suggested = round( round( $rollover * $avg_daily_sales * $date_count ) - ($newonhand + $inTransit) );
				$show_suggested = "round( round( $rollover * $avg_daily_sales * $date_count ) - ($newonhand + $inTransit ) )";

				if( $myid < 1 ) {
					$item_code = "<strike>$item_code</strike>";
					$item_name = "<strike>$item_name</strike>";
					$amt_disable = "DISABLED";
				}
				else {
					$amt_disable = "";
				}

				if($suggested <= 0){$fcolor="#999999";}
				else{$fcolor="black";}
				
				echo "<tr onMouseOver=this.bgColor='#FFFF00' onMouseOut=this.bgColor='#FFFFFF'><td>&nbsp;</td><td>&nbsp;</td><td>$item_code</td><td>$item_name</td><td><input type=text name=rollover$myid size=5 value='$rollover' style=\"font-size:10px;\" $amt_disable></td><td>$avg_daily_sales</td><td>$onhand</td><td>$inTransit</td><td title='$show_suggested'><font color=$fcolor>$suggested</font></td><td><input type=text name=amount$vend_itemid size=5 value='$amount' style=\"font-size:10px;\" $amt_disable></td></tr>";
				$lastgroup = $group_name;
				$lastpgroup = $pgroup_name;
			}
			echo "</table></td></tr><tr><td align=right colspan=2><input type=submit value='Save'></td></tr></table>";
			echo "</form>";
		}
		else {
			echo "<p><center><b><font color=red>This date is not a scheduled stop.</font></b></center><p>";
		}
	}
	else {
		echo "No machine setup for Cold Food&nbsp;";
	}
	
	echo "</div>";
	echo "</td></tr>";
	
	echo "</table></center><p>";
	echo "</td></tr></table>";
	
	echo "<DIV ID=testdiv1 STYLE=position:absolute;visibility:hidden;background-color:white;layer-background-color:white;></DIV>";
	
	echo "<form action='businesstrack.php' method='post'>";
	echo "<input type='hidden' name='username' value='$user'>";
	echo "<input type='hidden' name='password' value='$pass'>";
	echo "<p style='text-align: center;'><input type='submit' value='Return to Main Page' /></p></form>";
	
	google_page_track( );
}
?>
	</div>
	</body>
</html>
