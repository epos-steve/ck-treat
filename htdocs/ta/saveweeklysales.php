<?php

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      $day=$day-31;  
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";} 
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function nextday($nextd,$day_format){ //Function returns next day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monn = substr($nextd, 5, 2);
   $dayn = substr($nextd, 8, 2);
   $yearn = substr($nextd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monn, $dayn+1, $yearn));
   return $tempdate;
}

function prevday($prevd,$day_format){ //Function returns previous day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monp = substr($prevd, 5, 2);
   $dayp = substr($prevd, 8, 2);
   $yearp = substr($prevd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monp, $dayp-1, $yearp));
   return $tempdate;
}


function money($diff){   
        $diff = round($diff,2);
        return $diff;
}

define('DOC_ROOT', dirname(dirname(__FILE__)));
require_once(DOC_ROOT.'/bootstrap.php');

$style = "text-decoration:none";
$creditamount="creditamount";
$creditidnty="creditidnty";
$creditdate="creditdate";
$debitamount="debitamount";
$debitidnty="debitidnty";
$debitdate="debitdate";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";

$user=$_POST['username'];
$pass=$_POST['password'];
$bid=$_POST['businessid'];
$cid=$_POST['companyid'];
$prevperiod=$_POST['prevperiod'];
$nextperiod=$_POST['nextperiod'];
$day1=$_POST['day1'];
$startday=$day1;
$can_edit=$_POST['can_edit'];
$startdate=$day1;

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=mysql_numrows($result);

$security_level=mysql_result($result,0,"security_level");

if ($num != 1)
{
    echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
    $creditcount=$_POST['creditcount'];
    if ($prevperiod!=""){$location="busdetail.php?bid=$bid&cid=$cid&prevperiod=$day1";}
    else {$location="busdetail.php?bid=$bid&cid=$cid&nextperiod=$day1";}
    
	header('Location: ./' . $location);

    $count=0;
    while ($count<$creditcount){
       for ($counter=0;$counter<=6;$counter++){
          $arrayname="$count$counter$creditamount";
          $creditamountarray[$counter] = $_POST[$arrayname];
          $arrayname="$count$counter$creditidnty";
          $creditidarray[$counter] = $_POST[$arrayname];
          $arrayname="$count$counter$creditdate";
          $creditdatearray[$counter] = $_POST[$arrayname];
		  
		  $query = "SELECT map.id
						FROM mburris_manage.client_programs cp
						JOIN mburris_manage.mapping map ON map.client_programid = cp.client_programid 
							AND map.tbl = 1 AND map.localid = $creditidarray[$counter]
						WHERE cp.businessid = $bid AND cp.db_name = '" . Treat_Config::singleton()->get('db', 'database') . "'";
		  $result = Treat_DB_ProxyOld::query($query);
	  
		  $map_id = @mysql_result($result,0,"id");
		  
		  ///check for credit debit linking
		  if($map_id < 1){
				$query = "select mct.creditid from mburris_manage.client_programs cp
					join mburris_manage.mapping_cc_transactions mct ON mct.client_programid = cp.client_programid
						AND mct.creditid = $creditidarray[$counter]
					where cp.businessid = $bid and cp.db_name = '". Treat_Config::singleton()->get('db', 'database') . "'";
				$result = Treat_DB_ProxyOld::query($query);
	  
				$map_id = @mysql_result($result,0,"creditid");
		  }
		  
		  if($map_id < 1 || $security_level >= 9){

				$query = "SELECT * FROM creditdetail WHERE creditid = '$creditidarray[$counter]' AND date = '$creditdatearray[$counter]'";
				$result = Treat_DB_ProxyOld::query($query);
				$num=mysql_numrows($result);

				$creditamountarray[$counter]=money($creditamountarray[$counter]);

				if ($num!=0){
				   $creditdetailid = mysql_result($result,0,"creditdetailid");
				   $query="UPDATE creditdetail SET amount = '$creditamountarray[$counter]' WHERE creditdetailid = '$creditdetailid'";
				}
				elseif($creditamountarray[$counter]!=0){
				   $query = "INSERT INTO creditdetail (companyid,businessid,amount,date,creditid) VALUES ('$cid','$bid','$creditamountarray[$counter]','$creditdatearray[$counter]','$creditidarray[$counter]')";
				}
				else{
				   $query="";
				}

				if($query!=""){$result = Treat_DB_ProxyOld::query($query);}
		  }
       }
       $count++;
    }

    $debitcount=$_POST['debitcount'];

    $count=0;
    while ($count<$debitcount){
       for ($counter=0;$counter<=6;$counter++){
          $arrayname="$count$counter$debitamount";
          $debitamountarray[$counter] = $_POST[$arrayname];
          $arrayname="$count$counter$debitidnty";
          $debitidarray[$counter] = $_POST[$arrayname];
          $arrayname="$count$counter$debitdate";
          $debitdatearray[$counter] = $_POST[$arrayname];
		  
		  $query = "SELECT map.id, map.name
						FROM mburris_manage.client_programs cp
						JOIN mburris_manage.mapping map ON map.client_programid = cp.client_programid 
							AND map.tbl = 2 AND map.localid = $debitidarray[$counter]
						WHERE cp.businessid = $bid AND cp.db_name = '". Treat_Config::singleton()->get('db', 'database') . "'";
		  $result = Treat_DB_ProxyOld::query($query);
		  
		  $map_id = @mysql_result($result,0,"id");
		  $map_name = @mysql_result($result,0,"name");
		  
		  ///check for credit debit linking
		  if($map_id < 1){
				$query = "select mct.debitid from mburris_manage.client_programs cp
					join mburris_manage.mapping_cc_transactions mct ON mct.client_programid = cp.client_programid
						AND mct.creditid = $debitidarray[$counter]
					where cp.businessid = $bid and cp.db_name = '". Treat_Config::singleton()->get('db', 'database') . "'";
				$result = Treat_DB_ProxyOld::query($query);
	  
				$map_id = @mysql_result($result,0,"id");
		  }
		  
		  if($map_id < 1 || $security_level >= 9){

				$query = "SELECT * FROM debitdetail WHERE debitid = '$debitidarray[$counter]' AND date = '$debitdatearray[$counter]'";
				$result = Treat_DB_ProxyOld::query($query);
				$num=mysql_numrows($result);

				if ($num==1){
				   $debitdetailid = mysql_result($result,0,"debitdetailid");
				   $query="UPDATE debitdetail SET amount = '$debitamountarray[$counter]' WHERE debitdetailid = '$debitdetailid'";
				}
				elseif($debitamountarray[$counter]!=0){
				   $query = "INSERT INTO debitdetail (companyid,businessid,amount,date,debitid) VALUES ('$cid','$bid','$debitamountarray[$counter]','$debitdatearray[$counter]','$debitidarray[$counter]')";
				}
				else{
				   $query="";
				}

				if($query!=""){$result = Treat_DB_ProxyOld::query($query);}
		  }
       }
       $count++;
    }

    for ($counter=0;$counter<=6;$counter++){
       $arrayname="cc$day1";
       $customercount = $_POST[$arrayname];
       $arrayname="lunch$day1";
       $lunch = $_POST[$arrayname];
       $arrayname="dinner$day1";
       $dinner = $_POST[$arrayname];
       $arrayname="labor$day1";
       $labor = $_POST[$arrayname];
       $arrayname="labor_hour$day1";
       $labor_hour = $_POST[$arrayname];
       $arrayname="void$day1";
       $void = $_POST[$arrayname];
       $arrayname="closed$day1";
       $closed = $_POST[$arrayname];

          $query = "SELECT * FROM stats WHERE businessid = '$bid' AND date = '$day1'";
          $result = Treat_DB_ProxyOld::query($query);
          $num=mysql_numrows($result);

          if ($closed=="on"){$closed="1";}

          if ($num==1){
			 $statid = mysql_result($result,0,"statid");
             $query="UPDATE stats SET number = '$customercount', lunch = '$lunch', dinner = '$dinner', void = '$void', closed = '$closed' WHERE statid = '$statid'";
          }
          else{
             $query = "INSERT INTO stats (companyid,businessid,date,number,lunch,dinner,void,closed) VALUES ('$cid','$bid','$day1','$customercount','$lunch','$dinner','$void','$closed')";
          }

          $result = Treat_DB_ProxyOld::query($query);

       $day1=nextday($day1);
    }
   
    if ($can_edit==1 && $security_level==1){}
    else{
       $query = "INSERT INTO audit_sales (companyid,businessid,date,user) VALUES ('$cid','$bid','$startdate','$user')";
       $result = Treat_DB_ProxyOld::query($query);
    }
///INSERT UPDATE CASH OVER/SHORT////////////////////////////////////////////////////////////////////


//////CREDITS/////////////////////////////////////////////////////////////////////////////////////////////////////
    $day1=$startday;
    $day2=nextday($day1);
    $day3=nextday($day2);
    $day4=nextday($day3);
    $day5=nextday($day4);
    $day6=nextday($day5);
    $day7=nextday($day6);

    $query = "SELECT * FROM credittype ORDER BY credittypename DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=mysql_numrows($result);

    $hc1=0;
    $hc2=0;
    $hc3=0;
    $hc4=0;
    $hc5=0;
    $hc6=0;
    $hc7=0;
    $pp1=0;
    $pp2=0;
    $pp3=0;
    $pp4=0;
    $pp5=0;
    $pp6=0;
    $pp7=0;
    $num--;
    $count=0;
    $value1="0";
    $heldcheck=0;
    $day1column=0;
    $day2column=0;
    $day3column=0;
    $day4column=0;
    $day5column=0;
    $day6column=0;
    $day7column=0;
    while ($num>=0){
       $credittypename=mysql_result($result,$num,"credittypename");
       $credittypeid=mysql_result($result,$num,"credittypeid");
       
       $query2 = "SELECT * FROM credits WHERE businessid = '$bid' AND credittype = '$credittypeid' AND ((is_deleted = '0') || (is_deleted = '1' && del_date >= '$day1')) ORDER BY credittype,creditname DESC";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=mysql_numrows($result2);

       $num2--;
       $day1coltot=0;
       $day2coltot=0;
       $day3coltot=0;
       $day4coltot=0;
       $day5coltot=0;
       $day6coltot=0;
       $day7coltot=0;
       while ($num2>=0){
          $creditrowtotal=0;
          $creditname=mysql_result($result2,$num2,"creditname");
          $creditid=mysql_result($result2,$num2,"creditid");
          $account=mysql_result($result2,$num2,"account");
          $invoicesales=mysql_result($result2,$num2,"invoicesales");
          $invoicesales_notax=mysql_result($result2,$num2,"invoicesales_notax");
          $invoicetax=mysql_result($result2,$num2,"invoicetax");
          $servchrg=mysql_result($result2,$num2,"servchrg");
          $prepayment=mysql_result($result2,$num2,"heldcheck");
          $salescode=mysql_result($result2,$num2,"salescode");
          for ($counter=0;$counter<=6;$counter++){
             if ($counter==0){$day=$day1;}
             elseif ($counter==1){$day=$day2;}
             elseif ($counter==2){$day=$day3;}
             elseif ($counter==3){$day=$day4;}
             elseif ($counter==4){$day=$day5;}
             elseif ($counter==5){$day=$day6;}
             elseif ($counter==6){$day=$day7;}
///MANUAL ENTRY
             if ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0&&$salescode==0&&$prepayment!=1){
                  $query5 = "SELECT amount FROM creditdetail WHERE creditid = '$creditid' AND date = '$day'";
                  $result5 = Treat_DB_ProxyOld::query($query5);
                  $num5=mysql_numrows($result5);

                  if ($num5==1&&$invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0){
                     $prevamount=mysql_result($result5,0,"amount");
                     $prevamount = money($prevamount);
                  }
                  else{$prevamount="0";}

             }
///INVOICE SALES
             elseif ($invoicesales==1&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0&&$salescode==0&&$prepayment!=1){
                  $query5 = "SELECT total,taxtotal,servamt FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND taxable = 'on' AND salescode = '0' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = Treat_DB_ProxyOld::query($query5);
                  $num6=mysql_numrows($result5);
                  $num6--;
                  $prevamount=0;
                  while ($num6>=0){
                     $invtotal=mysql_result($result5,$num6,"total");
                     $invtax=mysql_result($result5,$num6,"taxtotal");
                     $servamt=mysql_result($result5,$num6,"servamt");
                     $prevamount=$prevamount+($invtotal-$invtax-$servamt);
                     $num6--;
                  }
                  $prevamount=money($prevamount);
                  if ($counter==0){$hc1=$hc1+$prevamount;}
                  elseif ($counter==1){$hc2=$hc2+$prevamount;}
                  elseif ($counter==2){$hc3=$hc3+$prevamount;}
                  elseif ($counter==3){$hc4=$hc4+$prevamount;}
                  elseif ($counter==4){$hc5=$hc5+$prevamount;}
                  elseif ($counter==5){$hc6=$hc6+$prevamount;}
                  elseif ($counter==6){$hc7=$hc7+$prevamount;}
                  
             }
///INVOICE TAX EXEMPT
             elseif ($invoicesales==0&&$invoicesales_notax==1&&$invoicetax==0&&$servchrg==0&&$salescode==0&&$prepayment!=1){
                  $query5 = "SELECT total,taxtotal,servamt FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND taxable = 'off' AND salescode = '0' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = Treat_DB_ProxyOld::query($query5);
                  $num6=mysql_numrows($result5);
                  $num6--;
                  $prevamount=0;
                  while ($num6>=0){
                     $invtotal=mysql_result($result5,$num6,"total");
                     $servamt=mysql_result($result5,$num6,"servamt");
                     $prevamount=$prevamount+$invtotal-$servamt;
                     $num6--;
                  }
                  $prevamount=money($prevamount);
                  if ($counter==0){$hc1=$hc1+$prevamount;}
                  elseif ($counter==1){$hc2=$hc2+$prevamount;}
                  elseif ($counter==2){$hc3=$hc3+$prevamount;}
                  elseif ($counter==3){$hc4=$hc4+$prevamount;}
                  elseif ($counter==4){$hc5=$hc5+$prevamount;}
                  elseif ($counter==5){$hc6=$hc6+$prevamount;}
                  elseif ($counter==6){$hc7=$hc7+$prevamount;}
             }
///INVOICE TAX
             elseif ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==1&&$servchrg==0&&$salescode==0&&$prepayment!=1){
                  $query5 = "SELECT taxtotal FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND taxable = 'on' AND salescode = '0' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = Treat_DB_ProxyOld::query($query5);
                  $num6=mysql_numrows($result5);
                  $num6--;
                  $prevamount=0;
                  while ($num6>=0){
                     $taxtotal=mysql_result($result5,$num6,"taxtotal");
                     $prevamount=$prevamount+$taxtotal;
                     $num6--;
                  }
                  $prevamount=money($prevamount);
                  if ($counter==0){$hc1=$hc1+$prevamount;}
                  elseif ($counter==1){$hc2=$hc2+$prevamount;}
                  elseif ($counter==2){$hc3=$hc3+$prevamount;}
                  elseif ($counter==3){$hc4=$hc4+$prevamount;}
                  elseif ($counter==4){$hc5=$hc5+$prevamount;}
                  elseif ($counter==5){$hc6=$hc6+$prevamount;}
                  elseif ($counter==6){$hc7=$hc7+$prevamount;}
             }
///INVOICE SERVICE CHARGE
             elseif ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==1&&$salescode==0&&$prepayment!=1){
                  $query5 = "SELECT servamt FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND salescode = '0' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = Treat_DB_ProxyOld::query($query5);
                  $num6=mysql_numrows($result5);
                  $num6--;
                  $prevamount=0;
                  while ($num6>=0){
                     $servamt=mysql_result($result5,$num6,"servamt");
                     $prevamount=$prevamount+$servamt;
                     $num6--;
                  }
                  $prevamount=money($prevamount);
                  if ($counter==0){$hc1=$hc1+$prevamount;}
                  elseif ($counter==1){$hc2=$hc2+$prevamount;}
                  elseif ($counter==2){$hc3=$hc3+$prevamount;}
                  elseif ($counter==3){$hc4=$hc4+$prevamount;}
                  elseif ($counter==4){$hc5=$hc5+$prevamount;}
                  elseif ($counter==5){$hc6=$hc6+$prevamount;}
                  elseif ($counter==6){$hc7=$hc7+$prevamount;}
             }
///INVOICE SALES (SALESCODE)/////////////////////////////////////////////////////////////////
             elseif ($invoicesales==1&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0&&$salescode!=0&&$prepayment!=1){
                  $query5 = "SELECT total,taxtotal,servamt FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND taxable = 'on' AND salescode = '$salescode' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = Treat_DB_ProxyOld::query($query5);
                  $num6=mysql_numrows($result5);
                  $num6--;
                  $prevamount=0;
                  while ($num6>=0){
                     $invtotal=mysql_result($result5,$num6,"total");
                     $invtax=mysql_result($result5,$num6,"taxtotal");
                     $servamt=mysql_result($result5,$num6,"servamt");
                     $prevamount=$prevamount+($invtotal-$invtax-$servamt);
                     $num6--;
                  }
                  $prevamount=money($prevamount);
                  if ($counter==0){$hc1=$hc1+$prevamount;}
                  elseif ($counter==1){$hc2=$hc2+$prevamount;}
                  elseif ($counter==2){$hc3=$hc3+$prevamount;}
                  elseif ($counter==3){$hc4=$hc4+$prevamount;}
                  elseif ($counter==4){$hc5=$hc5+$prevamount;}
                  elseif ($counter==5){$hc6=$hc6+$prevamount;}
                  elseif ($counter==6){$hc7=$hc7+$prevamount;}
                  
             }
///INVOICE TAX EXEMPT (SALESCODE)
             elseif ($invoicesales==0&&$invoicesales_notax==1&&$invoicetax==0&&$servchrg==0&&$salescode!=0&&$prepayment!=1){
                  $query5 = "SELECT total,taxtotal,servamt FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND taxable = 'off' AND salescode = '$salescode' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = Treat_DB_ProxyOld::query($query5);
                  $num6=mysql_numrows($result5);
                  $num6--;
                  $prevamount=0;
                  while ($num6>=0){
                     $invtotal=mysql_result($result5,$num6,"total");
                     $servamt=mysql_result($result5,$num6,"servamt");
                     $prevamount=$prevamount+$invtotal-$servamt;
                     $num6--;
                  }
                  $prevamount=money($prevamount);
                  if ($counter==0){$hc1=$hc1+$prevamount;}
                  elseif ($counter==1){$hc2=$hc2+$prevamount;}
                  elseif ($counter==2){$hc3=$hc3+$prevamount;}
                  elseif ($counter==3){$hc4=$hc4+$prevamount;}
                  elseif ($counter==4){$hc5=$hc5+$prevamount;}
                  elseif ($counter==5){$hc6=$hc6+$prevamount;}
                  elseif ($counter==6){$hc7=$hc7+$prevamount;}
             }
///INVOICE TAX (SALESCODE)
             elseif ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==1&&$servchrg==0&&$salescode!=0&&$prepayment!=1){
                  $query5 = "SELECT taxtotal FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND taxable = 'on' AND salescode = '$salescode' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = Treat_DB_ProxyOld::query($query5);
                  $num6=mysql_numrows($result5);
                  $num6--;
                  $prevamount=0;
                  while ($num6>=0){
                     $taxtotal=mysql_result($result5,$num6,"taxtotal");
                     $prevamount=$prevamount+$taxtotal;
                     $num6--;
                  }
                  $prevamount=money($prevamount);
                  if ($counter==0){$hc1=$hc1+$prevamount;}
                  elseif ($counter==1){$hc2=$hc2+$prevamount;}
                  elseif ($counter==2){$hc3=$hc3+$prevamount;}
                  elseif ($counter==3){$hc4=$hc4+$prevamount;}
                  elseif ($counter==4){$hc5=$hc5+$prevamount;}
                  elseif ($counter==5){$hc6=$hc6+$prevamount;}
                  elseif ($counter==6){$hc7=$hc7+$prevamount;}
             }
///INVOICE SERVICE CHARGE (SALESCODE)
             elseif ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==1&&$salescode!=0&&$prepayment!=1){
                  $query5 = "SELECT servamt FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND salescode = '$salescode' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = Treat_DB_ProxyOld::query($query5);
                  $num6=mysql_numrows($result5);
                  $num6--;
                  $prevamount=0;
                  while ($num6>=0){
                     $servamt=mysql_result($result5,$num6,"servamt");
                     $prevamount=$prevamount+$servamt;
                     $num6--;
                  }
                  $prevamount=money($prevamount);
                  if ($counter==0){$hc1=$hc1+$prevamount;}
                  elseif ($counter==1){$hc2=$hc2+$prevamount;}
                  elseif ($counter==2){$hc3=$hc3+$prevamount;}
                  elseif ($counter==3){$hc4=$hc4+$prevamount;}
                  elseif ($counter==4){$hc5=$hc5+$prevamount;}
                  elseif ($counter==5){$hc6=$hc6+$prevamount;}
                  elseif ($counter==6){$hc7=$hc7+$prevamount;}
             }
///PREPAYMENT
             elseif ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0&&$prepayment==1){
                  $query5 = "SELECT amount FROM invtender WHERE businessid = '$businessid' AND date = '$day'";
                  $result5 = Treat_DB_ProxyOld::query($query5);
                  $num6=mysql_numrows($result5);
                  $num6--;
                  $prevamount=0;
                  while ($num6>=0){
                     $amount=mysql_result($result5,$num6,"amount");
                     $prevamount=$prevamount+$amount;
                     $num6--;  
                  }
                  $prevamount=money($prevamount);
                  if ($counter==0){$pp1=$pp1+$prevamount;}
                  elseif ($counter==1){$pp2=$pp2+$prevamount;}
                  elseif ($counter==2){$pp3=$pp3+$prevamount;}
                  elseif ($counter==3){$pp4=$pp4+$prevamount;}
                  elseif ($counter==4){$pp5=$pp5+$prevamount;}
                  elseif ($counter==5){$pp6=$pp6+$prevamount;}
                  elseif ($counter==6){$pp7=$pp7+$prevamount;}
             }  

             $creditrowtotal=money($creditrowtotal+$prevamount);

             if ($day==$day1){$day1coltot=money($day1coltot+$prevamount);}
             elseif ($day==$day2){$day2coltot=money($day2coltot+$prevamount);}
             elseif ($day==$day3){$day3coltot=money($day3coltot+$prevamount);}
             elseif ($day==$day4){$day4coltot=money($day4coltot+$prevamount);}
             elseif ($day==$day5){$day5coltot=money($day5coltot+$prevamount);}
             elseif ($day==$day6){$day6coltot=money($day6coltot+$prevamount);}
             elseif ($day==$day7){$day7coltot=money($day7coltot+$prevamount);}
          }
          $num2--;
          $count++;
       }
       $rowtotal=money($day1coltot+$day2coltot+$day3coltot+$day4coltot+$day5coltot+$day6coltot+$day7coltot);      

       $day1column=money($day1column+$day1coltot);
       $day2column=money($day2column+$day2coltot);
       $day3column=money($day3column+$day3coltot);
       $day4column=money($day4column+$day4coltot);
       $day5column=money($day5column+$day5coltot);
       $day6column=money($day6column+$day6coltot);
       $day7column=money($day7column+$day7coltot);
       $num--;
    }    

//////DEBITS///////////////////////////////////////////////////////////////////////////////////////

    $query = "SELECT * FROM debittype ORDER BY debittypename DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=mysql_numrows($result);

    $num--;
    $count=0;
    $value1="0";
    $day1column1=0;
    $day2column1=0;
    $day3column1=0;
    $day4column1=0;
    $day5column1=0;
    $day6column1=0;
    $day7column1=0;
    while ($num>=0){
       $debittypename=mysql_result($result,$num,"debittypename");
       $debittypeid=mysql_result($result,$num,"debittypeid");
       
       $query2 = "SELECT * FROM debits WHERE businessid = '$bid' AND debittype = '$debittypeid' AND ((is_deleted = '0') || (is_deleted = '1' && del_date >= '$day1')) ORDER BY debittype,debitname DESC";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=mysql_numrows($result2);

       $num2--;
       $day1coltot=0;
       $day2coltot=0;
       $day3coltot=0;
       $day4coltot=0;
       $day5coltot=0;
       $day6coltot=0;
       $day7coltot=0;
       while ($num2>=0){
          $debitrowtotal=0;
          $debitname=mysql_result($result2,$num2,"debitname");
          $debitid=mysql_result($result2,$num2,"debitid");
          $account=mysql_result($result2,$num2,"account");
          $prepayment=mysql_result($result2,$num2,"heldcheck");
          $showaccount=substr($account,0,3);
          for ($counter=0;$counter<=6;$counter++){
             if ($counter==0){$day=$day1;}
             elseif ($counter==1){$day=$day2;}
             elseif ($counter==2){$day=$day3;}
             elseif ($counter==3){$day=$day4;}
             elseif ($counter==4){$day=$day5;}
             elseif ($counter==5){$day=$day6;}
             elseif ($counter==6){$day=$day7;}

             if ($prepayment!=1){
                $query5 = "SELECT amount FROM debitdetail WHERE debitid = '$debitid' AND date = '$day'";
                $result5 = Treat_DB_ProxyOld::query($query5);
                $num5=mysql_numrows($result5);

                if ($num5==1){$prevamount=mysql_result($result5,0,"amount");
                   $prevamount = money($prevamount);
                }
                else{$prevamount="0";}

             }
             elseif ($prepayment==1){
                if ($day==$day1){
                   $prevamount=$hc1;
                   if ($prevamount<=0){$prevamount=0;}
                }
                elseif ($day==$day2){
                   $prevamount=$hc2;
                   if ($prevamount<=0){$prevamount=0;}
                }
                elseif ($day==$day3){
                   $prevamount=$hc3;
                   if ($prevamount<=0){$prevamount=0;}
                }
                elseif ($day==$day4){
                   $prevamount=$hc4;
                   if ($prevamount<=0){$prevamount=0;}
                }
                elseif ($day==$day5){
                   $prevamount=$hc5;
                   if ($prevamount<=0){$prevamount=0;}
                }
                elseif ($day==$day6){
                   $prevamount=$hc6;
                   if ($prevamount<=0){$prevamount=0;}
                }
                elseif ($day==$day7){
                   $prevamount=$hc7;
                   if ($prevamount<=0){$prevamount=0;}
                }
                $prevamount = money($prevamount);
             }

             $debitrowtotal=money($debitrowtotal+$prevamount);

             if ($day==$day1){$day1coltot=money($day1coltot+$prevamount);}
             elseif ($day==$day2){$day2coltot=money($day2coltot+$prevamount);}
             elseif ($day==$day3){$day3coltot=money($day3coltot+$prevamount);}
             elseif ($day==$day4){$day4coltot=money($day4coltot+$prevamount);}
             elseif ($day==$day5){$day5coltot=money($day5coltot+$prevamount);}
             elseif ($day==$day6){$day6coltot=money($day6coltot+$prevamount);}
             elseif ($day==$day7){$day7coltot=money($day7coltot+$prevamount);}
          }
          $num2--;
          $count++;
       }
       $rowtotal=money($day1coltot+$day2coltot+$day3coltot+$day4coltot+$day5coltot+$day6coltot+$day7coltot);      

       $day1column1=money($day1column1+$day1coltot);
       $day2column1=money($day2column1+$day2coltot);
       $day3column1=money($day3column1+$day3coltot);
       $day4column1=money($day4column1+$day4coltot);
       $day5column1=money($day5column1+$day5coltot);
       $day6column1=money($day6column1+$day6coltot);
       $day7column1=money($day7column1+$day7coltot);
       $num--;
    }

///CASH SHORT//////////////////

    $day1=$startday;

    for ($counter=0;$counter<=6;$counter++){

       if ($counter==0){$cashtotal=money($day1column-$day1column1);}
       elseif ($counter==1){$cashtotal=money($day2column-$day2column1);}
       elseif ($counter==2){$cashtotal=money($day3column-$day3column1);}
       elseif ($counter==3){$cashtotal=money($day4column-$day4column1);}
       elseif ($counter==4){$cashtotal=money($day5column-$day5column1);}
       elseif ($counter==5){$cashtotal=money($day6column-$day6column1);}
       elseif ($counter==6){$cashtotal=money($day7column-$day7column1);}

          $query = "SELECT * FROM cash WHERE businessid = '$bid' AND date = '$day1'";
          $result = Treat_DB_ProxyOld::query($query);
          $num=mysql_numrows($result);

          if ($cashtotal>=0){$cashtype=0;}
          else {$cashtype=1;$cashtotal=$cashtotal*-1;}

          if ($num==1){
			 $cashid = mysql_result($result,0,"cashid");
             $query="UPDATE cash SET amount = '$cashtotal', type = '$cashtype' WHERE cashid = '$cashid'";
          }
          else{
             $query = "INSERT INTO cash (companyid,businessid,date,amount,type) VALUES ('$cid','$bid','$day1','$cashtotal','$cashtype')";
          }

          $result = Treat_DB_ProxyOld::query($query);

       $day1=nextday($day1);
    }
}
?>