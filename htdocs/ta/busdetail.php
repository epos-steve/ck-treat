<?php

/////////////////////////////////////////REVISION HISTORY////////////////////////////////////////////
//2/21/07 - Changed Check Average to include Sales Only -SM
//2/27/07 - Removed Catering count from check average -SM
/////////////////////////////////////////REVISION HISTORY////////////////////////////////////////////

// removed all of the functions because they're an exact match for the shit functions
// in lib/inc/functions-old.php which have been rewritten to correctly use strtotime
// or DateTime object instead of doing string manipulation for calculating dates.

if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
require_once( DOC_ROOT . '/bootstrap.php' );
include('lib/class.Kiosk.php');

$newdate = null; # Notice: Undefined variable
$day1 = null; # Notice: Undefined variable
$showsubmit = null; # Notice: Undefined variable
$plsincrement = null; # Notice: Undefined variable
$tabrow = null; # Notice: Undefined variable
$problem = null; # Notice: Undefined variable
$showproblem = null; # Notice: Undefined variable
$problem_submit = null; # Notice: Undefined variable

$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");
$creditamount="creditamount";
$creditidnty="creditidnty";
$creditdate="creditdate";
$debitamount="debitamount";
$debitidnty="debitidnty";
$debitdate="debitdate";
$quatertotal=0;
$monthtotal=0;
$total=0;
$counter=0;
$findme='.';
$double='.00';
$single='0';
$double2='00';
$curarrow=0;

$user = \EE\Controller\Base::getSessionCookieVariable('usercook','');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook','');
$businessid = \EE\Controller\Base::getGetVariable('bid','');
$companyid = \EE\Controller\Base::getGetVariable('cid','');
$prevperiod = \EE\Controller\Base::getGetVariable('prevperiod','');
$nextperiod = \EE\Controller\Base::getGetVariable('nextperiod','');
$nowperiod = \EE\Controller\Base::getGetVariable('nowperiod','');
$audit = \EE\Controller\Base::getGetVariable('audit','');
$showmgr = \EE\Controller\Base::getGetVariable('showmgr','');

if($showmgr!=""){
	$expire = 60 * 60 * 24 * 60 + time();
	setcookie("showmgr",$showmgr,$expire);
}
//else{$showmgr = isset($_COOKIE["showmgr"])?$_COOKIE["showmgr"]:'';}
else{$showmgr = \EE\Controller\Base::getSessionCookieVariable('showmgr','');}

if ($businessid==""&&$companyid==""){
	$businessid = isset($_POST["businessid"])?$_POST["businessid"]:'';
	$companyid = isset($_POST["companyid"])?$_POST["companyid"]:'';
	$newdate = isset($_POST["newdate"])?$_POST["newdate"]:'';
}

if ($newdate!=""){
	$today=$newdate;
	$todayname=dayofweek($today);
}

	$company = Treat_Model_Company::getById($companyid);
	list($companyname, $com_logo) = Array(
		$company->companyname,$company->logo,
	);
	
?>
<html>
<head>
		<title><?php echo $company->companyname; ?></title>

<script language="JavaScript"
	type="text/JavaScript">
function changePage(newLoc)
{
	nextPage = newLoc.options[newLoc.selectedIndex].value

	if (nextPage != "")
	{
		document.location.href = nextPage
	}
}
</script>

<SCRIPT LANGUAGE=javascript><!--
function submit2(){return confirm('MAKE SURE YOU SAVE BEFORE YOU SUBMIT!! If you have entered sales without saving, they will not submit!!!');}
// --></SCRIPT>

<script type="text/javascript" language="javascript">
function checkKey(){

var key = event.keyCode;
var DaIndexId = event.srcElement.IndexId-0
if(key==38) {
document.getElementByIndex(DaIndexId-1).focus();
return false;
}
else if(key==40) {
document.getElementByIndex(DaIndexId+1).focus();
return false;
}
else if(key==13) {
document.getElementByIndex(DaIndexId+1).focus();
return false;
}
else if(key==37) {
document.getElementByIndex(DaIndexId-1000).focus();
return false;
}
else if(key==39) {
document.getElementByIndex(DaIndexId+1000).focus();
return false;
}
return true
} // end checkKey function
document.getElementByIndex=function(){
for(var i=0;i<document.all.length;i++)
if(document.all[i].IndexId==arguments[0])
return document.all[i]
return null
}
</script>

<SCRIPT LANGUAGE="JavaScript" SRC="CalendarPopup.js" type="text/javascript"></SCRIPT>

<!-- This prints out the default stylehseets used by the DIV style calendar.
	Only needed if you are using the DIV style popup -->
<SCRIPT LANGUAGE="JavaScript" type="text/javascript">document.write(getCalendarStyles());</SCRIPT>

<script language="JavaScript" type="text/javascript">
<!-- Web Site:  http://dynamicdrive.com -->

<!-- This script and many more are available free online at -->
<!-- The JavaScript Source!! http://javascript.internet.com -->

<!-- Begin
function disableForm(theform) {
if (document.all || document.getElementById) {
for (i = 0; i < theform.length; i++) {
var tempobj = theform.elements[i];
if (tempobj.type.toLowerCase() == "submit" || tempobj.type.toLowerCase() == "reset")
tempobj.disabled = true;
}

}
else {
alert("The form has been submitted.  But, since you're not using IE 4+ or NS 6, the submit button was not disabled on form submission.");
return false;
}
}
//  End -->
</script>

<style type="text/css">
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation
			{
			background-color:#6677DD;
			text-align:center;
			vertical-align:center;
			text-decoration:none;
			color:#FFFFFF;
			font-weight:bold;
			}
	.TESTcpDayColumnHeader,
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation,
	.TESTcpCurrentMonthDate,
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDate,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDate,
	.TESTcpCurrentDateDisabled,
	.TESTcpTodayText,
	.TESTcpTodayTextDisabled,
	.TESTcpText
			{
			font-family:arial;
			font-size:8pt;
			}
	TD.TESTcpDayColumnHeader
			{
			text-align:right;
			border:solid thin #6677DD;
			border-width:0 0 1 0;
			}
	.TESTcpCurrentMonthDate,
	.TESTcpOtherMonthDate,
	.TESTcpCurrentDate
			{
			text-align:right;
			text-decoration:none;
			}
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDateDisabled
			{
			color:#D0D0D0;
			text-align:right;
			text-decoration:line-through;
			}
	.TESTcpCurrentMonthDate
			{
			color:#6677DD;
			font-weight:bold;
			}
	.TESTcpCurrentDate
			{
			color: #FFFFFF;
			font-weight:bold;
			}
	.TESTcpOtherMonthDate
			{
			color:#808080;
			}
	TD.TESTcpCurrentDate
			{
			color:#FFFFFF;
			background-color: #6677DD;
			border-width:1;
			border:solid thin #000000;
			}
	TD.TESTcpCurrentDateDisabled
			{
			border-width:1;
			border:solid thin #FFAAAA;
			}
	TD.TESTcpTodayText,
	TD.TESTcpTodayTextDisabled
			{
			border:solid thin #6677DD;
			border-width:1 0 0 0;
			}
	A.TESTcpTodayText,
	SPAN.TESTcpTodayTextDisabled
			{
			height:20px;
			}
	A.TESTcpTodayText
			{
			color:#6677DD;
			font-weight:bold;
			}
	SPAN.TESTcpTodayTextDisabled
			{
			color:#D0D0D0;
			}
	.TESTcpBorder
			{
			border:solid thin #6677DD;
			}
</style>

<script language="JavaScript"
	type="text/JavaScript">
function calc_popup(url)
{
	popwin=window.open(url,"Calculator","location=no,menubar=no,titlebar=no,resizeable=no,height=50,width=100");
}
</script>

<script type="text/javascript" src="javascripts/prototype.js"></script>

<script language="JavaScript"
	type="text/JavaScript">
window.onload = function(){
	new headerScroll('test',{head: 'header'});
}
</script>

</head>
<?php

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( mysql_error());

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query( $query );
$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );
//mysql_close();

if ($num!=0){
	$loginid = Treat_DB_ProxyOld::mysql_result( $result, 0, 'loginid', '' );
	$security_level = Treat_DB_ProxyOld::mysql_result( $result, 0, 'security_level', '' );
	$mysecurity = Treat_DB_ProxyOld::mysql_result( $result, 0, 'security', '' );
	$bid = Treat_DB_ProxyOld::mysql_result( $result, 0, 'businessid', '' );
	$bid2 = Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid2', '' );
	$bid3 = Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid3', '' );
	$bid4 = Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid4', '' );
	$bid5 = Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid5', '' );
	$bid6 = Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid6', '' );
	$bid7 = Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid7', '' );
	$bid8 = Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid8', '' );
	$bid9 = Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid9', '' );
	$bid10 = Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid10', '' );
	$cid = Treat_DB_ProxyOld::mysql_result( $result, 0, 'companyid', '' );

	$pr1 = Treat_DB_ProxyOld::mysql_result( $result, 0, 'payroll', '' );
	$pr2 = Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr2', '' );
	$pr3 = Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr3', '' );
	$pr4 = Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr4', '' );
	$pr5 = Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr5', '' );
	$pr6 = Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr6', '' );
	$pr7 = Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr7', '' );
	$pr8 = Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr8', '' );
	$pr9 = Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr9', '' );
	$pr10 = Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr10', '' );
}

if ($num != 1 || ($user == "" && $pass == ""))
{
	echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}

elseif (($security_level==1 && $bid == $businessid && $cid == $companyid) || ($security_level > 1 AND $cid == $companyid))
{
	echo "<body><center><table cellspacing=0 cellpadding=0 border=0 width=90%><tr><td colspan=2>";
	echo "<a href=businesstrack.php><img src=logo.jpg border=0 height=43 width=205></a><p></td></tr>";


	$company = Treat_Model_Company::getById($companyid);
	list($companyname, $week_end, $week_start, $com_logo) = Array(
		$company->companyname,$company->week_end,$company->week_start,$company->logo,
	);
/*

	$query = "SELECT * FROM company WHERE companyid = '$companyid'";
	$result = Treat_DB_ProxyOld::query( $query );

	$companyname = Treat_DB_ProxyOld::mysql_result( $result, 0, 'companyname', '' );
	$week_end = Treat_DB_ProxyOld::mysql_result( $result, 0, 'week_end', '' );
	$week_start = Treat_DB_ProxyOld::mysql_result( $result, 0, 'week_start', '' );
	$com_logo = Treat_DB_ProxyOld::mysql_result( $result, 0, 'logo', '' );
	 */
	$query = "SELECT * FROM security WHERE securityid = '$mysecurity'";
	$result = Treat_DB_ProxyOld::query( $query );
	//mysql_close();

	$sec_bus_sales = Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_sales', '' );
	$sec_bus_invoice = Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_invoice', '' );
	$sec_bus_order = Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_order', '' );
	$sec_bus_payable = Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_payable', '' );
	$sec_bus_inventor1 = Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_inventor1', '' );
	$sec_bus_control = Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_control', '' );
	$sec_bus_menu = Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_menu', '' );
	$sec_bus_order_setup = Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_order_setup', '' );
	$sec_bus_request = Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_request', '' );
	$sec_route_collection = Treat_DB_ProxyOld::mysql_result( $result, 0, 'route_collection', '' );
	$sec_route_labor = Treat_DB_ProxyOld::mysql_result( $result, 0, 'route_labor', '' );
	$sec_route_detail = Treat_DB_ProxyOld::mysql_result( $result, 0, 'route_detail', '' );
	$sec_route_machine = Treat_DB_ProxyOld::mysql_result( $result, 0, 'route_machine', '' );
	$sec_bus_labor = Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_labor', '' );
	$sec_bus_timeclock = Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_timeclock', '' );
	$sec_bus_budget = Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_budget', '' );
	$sec_pos_settings = Treat_DB_ProxyOld::mysql_result( $result, 0, 'pos_settings', '' );

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM business WHERE companyid = '$companyid' AND businessid = '$businessid'";
	$result = Treat_DB_ProxyOld::query( $query );
	//mysql_close();

	$businessname = Treat_DB_ProxyOld::mysql_result( $result, 0, 'businessname', '' );
	$businessid = Treat_DB_ProxyOld::mysql_result( $result, 0, 'businessid', '' );
	$street = Treat_DB_ProxyOld::mysql_result( $result, 0, 'street', '' );
	$city = Treat_DB_ProxyOld::mysql_result( $result, 0, 'city', '' );
	$state = Treat_DB_ProxyOld::mysql_result( $result, 0, 'state', '' );
	$zip = Treat_DB_ProxyOld::mysql_result( $result, 0, 'zip', '' );
	$phone = Treat_DB_ProxyOld::mysql_result( $result, 0, 'phone', '' );
	$over_short_acct = Treat_DB_ProxyOld::mysql_result( $result, 0, 'over_short_acct', '' );
	$count1 = Treat_DB_ProxyOld::mysql_result( $result, 0, 'count1', '' );
	$count2 = Treat_DB_ProxyOld::mysql_result( $result, 0, 'count2', '' );
	$count3 = Treat_DB_ProxyOld::mysql_result( $result, 0, 'count3', '' );
	$bus_unit = Treat_DB_ProxyOld::mysql_result( $result, 0, 'unit', '' );
	$operate = Treat_DB_ProxyOld::mysql_result( $result, 0, 'operate', '' );
	$contract = Treat_DB_ProxyOld::mysql_result( $result, 0, 'contract', '' );
	$default_menu = Treat_DB_ProxyOld::mysql_result( $result, 0, 'default_menu', '' );
	$categoryid = Treat_DB_ProxyOld::mysql_result( $result, 0, 'categoryid', '' );

	if($showmgr==""){$showmgr=1;}
	///////////////////Manager(s) on header
	if($showmgr==1){
		$showmgr2="<a href=busdetail.php?bid=$businessid&cid=$companyid&prevperiod=$day1&showmgr=0 style=$style><font size=2 color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>[Hide Managers]</font></a>";
		$query91 = "SELECT firstname,lastname,email FROM login WHERE (businessid = '$businessid' OR busid2 = '$businessid' OR busid3 = '$businessid' OR busid4 = '$businessid' OR busid5 = '$businessid' OR busid6 = '$businessid' OR busid7 = '$businessid' OR busid8 = '$businessid' OR busid9 = '$businessid' OR busid10 = '$businessid') AND security_level > '0' AND is_deleted = '0'";
		$result91 = Treat_DB_ProxyOld::query( $query91 );
		$num91 = Treat_DB_ProxyOld::mysql_numrows( $result91, 0 );

		$showmanager="";
		$num91--;
		while($num91>=0){
			$firstname = Treat_DB_ProxyOld::mysql_result( $result91, $num91, 'firstname', '' );
			$lastname = Treat_DB_ProxyOld::mysql_result( $result91, $num91, 'lastname', '' );
			$email = Treat_DB_ProxyOld::mysql_result( $result91, $num91, 'email', '' );

			$showmanager.="<br> <a href=mailto:$email style=$style><font color=blue onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='blue' style='text-decoration:none'>$firstname $lastname, $email</font></a>";

			$num91--;
		}
	}
	else{$showmgr2="<a href=busdetail.php?bid=$businessid&cid=$companyid&prevperiod=$day1&showmgr=1 style=$style><font size=2 color=blue onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='blue' style='text-decoration:none'>[Show Managers]</font></a>";$showmanager="";}
	///////////////////END

	if($com_logo == ""){$com_logo = "talogo.gif";}

	//echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";
	echo "<tr bgcolor=#CCCCFF valign=top><td width=1%><img src=logo/$com_logo></td><td><font size=4><b>$businessname #$bus_unit</b></font><br>$street<br>$city, $state $zip<br>$phone &nbsp;&nbsp;&nbsp;$showmgr2$showmanager</td>";
	echo "<td align=right valign=top>";
	echo "<br><FORM ACTION=editbus.php method=post>";
	echo "<input type=hidden name=username value=$user>";
	echo "<input type=hidden name=password value=$pass>";
	echo "<input type=hidden name=businessid value=$businessid>";
	echo "<input type=hidden name=companyid value=$companyid>";
	echo "<INPUT TYPE=submit VALUE=' Store Setup '></FORM></td></tr>";
	//echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";

	echo "<tr><td colspan=3><font color=#999999>";
	if ($sec_bus_sales>0){echo "<a href=busdetail.php?bid=$businessid&cid=$companyid><font color=red onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='red' style='text-decoration:none'>Sales</font></a>";}
	if ($sec_bus_invoice>0){echo " | <a href=businvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Invoicing</font></a>";}
	if ($sec_bus_order>0){echo " | <a href=buscatertrack.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Orders</font></a>";}
	if ($sec_bus_payable>0){echo " | <a href=busapinvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Payables</font></a>";}
	if ($sec_bus_inventor1>0){echo " | <a href=businventor.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Inventory</font></a>";}
	if ($sec_bus_control>0){echo " | <a href=buscontrol.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Control Sheet</font></a>";}
	if ($sec_bus_menu>0){echo " | <a href=buscatermenu.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Menus</font></a>";}
	if ($sec_bus_order_setup>0){echo " | <a href=buscaterroom.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Order Setup</font></a>";}
	if ($sec_bus_request>0){echo " | <a href=busrequest.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Requests</font></a>";}
	if ($sec_route_collection>0||$sec_route_labor>0||$sec_route_detail>0||$sec_route_machine>0){echo " | <a href=busroute_collect.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Route Collections</font></a>";}
	if ($sec_bus_labor>0||$sec_bus_timeclock){echo " | <a href=buslabor.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Labor</font></a>";}
	if ($sec_bus_budget>0||($businessid == $bid && $pr1 == 1)||($businessid == $bid2 && $pr2 == 1)||($businessid == $bid3 && $pr3 == 1)||($businessid == $bid4 && $pr4 == 1)||($businessid == $bid5 && $pr5 == 1)||($businessid == $bid6 && $pr6 == 1)||($businessid == $bid7 && $pr7 == 1)||($businessid == $bid8 && $pr8 == 1)||($businessid == $bid9 && $pr9 == 1)||($businessid == $bid10 && $pr10 == 1)){echo " | <a href=busbudget.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Budget</font></a>";}
	if ($sec_pos_settings>0){echo " | <a href=/ta/possettings/?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>POS Settings</font></a>";}
	echo "</td></tr>";

	echo "</table></center>";

	////////////////////NEW WAY
	if ($prevperiod!=""){$temp_date=$prevperiod;}
	elseif ($nextperiod!=""){$temp_date=$nextperiod;}
	elseif ($nowperiod!=""){$temp_date=$nowperiod;}
	elseif ($newdate!=""){$temp_date=$newdate;}
	else {$temp_date=date("Y-m-d");}

	while(dayofweek($temp_date)!=$week_end){$temp_date=nextday($temp_date);}

	$day7=$temp_date;
	$day7name=dayofweek($day7);

	$day6=prevday($day7);
	$day6name=dayofweek($day6);

	$day5=prevday($day6);
	$day5name=dayofweek($day5);

	$day4=prevday($day5);
	$day4name=dayofweek($day4);

	$day3=prevday($day4);
	$day3name=dayofweek($day3);

	$day2=prevday($day3);
	$day2name=dayofweek($day2);

	$day1=prevday($day2);
	$day1name=dayofweek($day1);

	$prevperiod=prevday($day1);
	$nextperiod=nextday($day7);

	if ($security_level>0){
		echo "<p><center><table width=90%><tr><td width=50%><form action=busdetail.php method=post>";
		echo "<select name=gobus onChange='changePage(this.form.gobus)'>";

		$districtquery="";
		if ($security_level>2&&$security_level<7){
			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query = "SELECT * FROM login_district WHERE loginid = '$loginid'";
			$result = Treat_DB_ProxyOld::query( $query );
			$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );
			//mysql_close();

			$num--;
			while($num>=0){
				$districtid = Treat_DB_ProxyOld::mysql_result( $result, $num, 'districtid', '' );
				if($districtquery==""){$districtquery="districtid = '$districtid'";}
				else{$districtquery="$districtquery OR districtid = '$districtid'";}
				$num--;
			}
		}

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		if($security_level>6){$query = "SELECT * FROM business WHERE companyid = '$companyid' ORDER BY businessname DESC";}
		elseif($security_level==2||$security_level==1){$query = "SELECT * FROM business WHERE companyid = '$companyid' AND (businessid = '$bid' OR businessid = '$bid2' OR businessid = '$bid3' OR businessid = '$bid4' OR businessid = '$bid5' OR businessid = '$bid6' OR businessid = '$bid7' OR businessid = '$bid8' OR businessid = '$bid9' OR businessid = '$bid10') ORDER BY businessname DESC";}
		elseif($security_level>2&&$security_level<7){$query = "SELECT * FROM business WHERE companyid = '$companyid' AND ($districtquery) ORDER BY businessname DESC";}
		$result = Treat_DB_ProxyOld::query( $query );
		$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );
		//mysql_close();

		$num--;
		while($num>=0){
			$businessname = Treat_DB_ProxyOld::mysql_result( $result, $num, 'businessname', '' );
			$busid = Treat_DB_ProxyOld::mysql_result( $result, $num, 'businessid', '' );

			if ($busid==$businessid){$show="SELECTED";}
			else{$show="";}

			echo "<option value=busdetail.php?bid=$busid&cid=$companyid&prevperiod=$day1 $show>$businessname</option>";
			$num--;
		}

		echo "</select></td><td></form></td><td width=50% align=right><form action=busdetail.php method=post><input type=hidden name=businessid value=$businessid><input type=hidden name=companyid value=$companyid><SCRIPT LANGUAGE='JavaScript' ID='js19'> var cal19 = new CalendarPopup('testdiv1');cal19.setCssPrefix('TEST');</SCRIPT> <A HREF=# onClick=cal19.select(document.forms[2].newdate,'anchor19','yyyy-MM-dd'); return false; TITLE=cal19.select(document.forms[2].newdate,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor19' ID='anchor19'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A> <input type=text name=newdate value='$today' size=8> <input type=submit value='GO'></td><td></form></td></tr></table></center>";
	}

	$security_level=$sec_bus_sales;

	/* OLD WAY OF FIGURING DATES
	if ($prevperiod!=""){
		$day7=prevday($prevperiod);
		$day6=prevday($day7);
		$day5=prevday($day6);
		$day4=prevday($day5);
		$day3=prevday($day4);
		$day2=prevday($day3);
		$day1=prevday($day2);
	}
	elseif ($nextperiod!=""){
		$day1=nextday($nextperiod);
		$day2=nextday($day1);
		$day3=nextday($day2);
		$day4=nextday($day3);
		$day5=nextday($day4);
		$day6=nextday($day5);
		$day7=nextday($day6);
	}
	elseif ($nowperiod!=""){
		$day1=$nowperiod;
		$day2=nextday($day1);
		$day3=nextday($day2);
		$day4=nextday($day3);
		$day5=nextday($day4);
		$day6=nextday($day5);
		$day7=nextday($day6);
	}
	elseif ($todayname=="Friday"){
		$day1=$today;
		$day2=nextday($day1);
		$day3=nextday($day2);
		$day4=nextday($day3);
		$day5=nextday($day4);
		$day6=nextday($day5);
		$day7=nextday($day6);
	}
	elseif ($todayname=="Saturday"){
		$day1=prevday($today);
		$day2=$today;
		$day3=nextday($day2);
		$day4=nextday($day3);
		$day5=nextday($day4);
		$day6=nextday($day5);
		$day7=nextday($day6);
	}
	elseif ($todayname=="Sunday"){
		$day3=$today;
		$day4=nextday($day3);
		$day5=nextday($day4);
		$day6=nextday($day5);
		$day7=nextday($day6);
		$day2=prevday($day3);
		$day1=prevday($day2);
	}
	elseif ($todayname=="Monday"){
		$day4=$today;
		$day5=nextday($day4);
		$day6=nextday($day5);
		$day7=nextday($day6);
		$day3=prevday($day4);
		$day2=prevday($day3);
		$day1=prevday($day2);
	}
	elseif ($todayname=="Tuesday"){
		$day5=$today;
		$day6=nextday($day5);
		$day7=nextday($day6);
		$day4=prevday($day5);
		$day3=prevday($day4);
		$day2=prevday($day3);
		$day1=prevday($day2);
	}
	elseif ($todayname=="Wednesday"){
		$day6=$today;
		$day7=nextday($day6);
		$day5=prevday($day6);
		$day4=prevday($day5);
		$day3=prevday($day4);
		$day2=prevday($day3);
		$day1=prevday($day2);
	}
	elseif ($todayname=="Thursday"){
		$day7=$today;
		$day6=prevday($day7);
		$day5=prevday($day6);
		$day4=prevday($day5);
		$day3=prevday($day4);
		$day2=prevday($day3);
		$day1=prevday($day2);
	}
	*/


	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM submit WHERE businessid = '$businessid' AND date = '$day1'";
	$result = Treat_DB_ProxyOld::query( $query, true );
	$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );
	//mysql_close();

	$wordfont1="black";
	$wordfont2="black";
	$wordfont3="black";
	$wordfont4="black";
	$wordfont5="black";
	$wordfont6="black";
	$wordfont7="black";

	if ($num!=0){$export = Treat_DB_ProxyOld::mysql_result( $result, 0, 'export', '' );if ($export==1){$day1color="#444455";$wordfont1="white";}else{$day1color="#00CC00";}}else{$day1color="#00CCFF";}

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM submit WHERE businessid = '$businessid' AND date = '$day2'";
	$result = Treat_DB_ProxyOld::query( $query, true );
	$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );
	//mysql_close();

	if ($num!=0){$export = Treat_DB_ProxyOld::mysql_result( $result, 0, 'export', '' );if ($export==1){$day2color="#444455";$wordfont2="white";}else{$day2color="#00CC00";}}else{$day2color="#00CCFF";}

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM submit WHERE businessid = '$businessid' AND date = '$day3'";
	$result = Treat_DB_ProxyOld::query( $query, true );
	$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );
	//mysql_close();

	if ($num!=0){$export = Treat_DB_ProxyOld::mysql_result( $result, 0, 'export', '' );if ($export==1){$day3color="#444455";$wordfont3="white";}else{$day3color="#00CC00";}}else{$day3color="#00CCFF";}

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM submit WHERE businessid = '$businessid' AND date = '$day4'";
	$result = Treat_DB_ProxyOld::query( $query, true );
	$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );
	//mysql_close();

	if ($num!=0){$export = Treat_DB_ProxyOld::mysql_result( $result, 0, 'export', '' );if ($export==1){$day4color="#444455";$wordfont4="white";}else{$day4color="#00CC00";}}else{$day4color="#00CCFF";}

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM submit WHERE businessid = '$businessid' AND date = '$day5'";
	$result = Treat_DB_ProxyOld::query( $query, true );
	$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );
	//mysql_close();

	if ($num!=0){$export = Treat_DB_ProxyOld::mysql_result( $result, 0, 'export', '' );if ($export==1){$day5color="#444455";$wordfont5="white";}else{$day5color="#00CC00";}}else{$day5color="#00CCFF";}

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM submit WHERE businessid = '$businessid' AND date = '$day6'";
	$result = Treat_DB_ProxyOld::query( $query, true );
	$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );
	//mysql_close();

	if ($num!=0){$export = Treat_DB_ProxyOld::mysql_result( $result, 0, 'export', '' );if ($export==1){$day6color="#444455";$wordfont6="white";}else{$day6color="#00CC00";}}else{$day6color="#00CCFF";}

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM submit WHERE businessid = '$businessid' AND date = '$day7'";
	$result = Treat_DB_ProxyOld::query( $query, true );
	$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );
	//mysql_close();

	if ($num!=0){$export = Treat_DB_ProxyOld::mysql_result( $result, 0, 'export', '' );if ($export==1){$day7color="#444455";$wordfont7="white";}else{$day7color="#00CC00";}}else{$day7color="#00CCFF";}

	echo "<FORM ACTION=saveweeklysales.php method=post onSubmit='return disableForm(this);'>";
	echo "<input type=hidden name=username value=$user>";
	echo "<input type=hidden name=password value=$pass>";
	echo "<input type=hidden name=businessid value=$businessid>";
	echo "<input type=hidden name=companyid value=$companyid>";

	echo "<p><center><table width=90% cellspacing=0 cellpadding=0>";
	echo "<tr bgcolor=#E8E7E7 valign=top><td width=1%><img src=leftcrn.jpg height=6 width=6 align=top></td><td width=15%><center><a href=busdetail.php?cid=$companyid&bid=$businessid&prevperiod=$day1 style=$style><font color=black>Daily Sales</a></center></td><td width=1% align=right><img src=rightcrn.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1% bgcolor=#CCCCCC><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=15% bgcolor=#CCCCCC><center><a href=busdetail2.php?cid=$companyid&bid=$businessid&prevperiod=$day1 style=$style style=$style><font color=blue>Daily Imprest Fund</a></center></td><td width=1% align=right bgcolor=#CCCCCC><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td bgcolor=white width=60%></td></tr>";
	echo "<tr height=2><td colspan=3 bgcolor=#E8E7E7></td><td colspan=4></td><td></td><td colspan=1></td></tr>";
	echo "</table>";

	echo "<center><table border=0 width=90% bgcolor=#E8E7E7 cellspacing=1 cellpadding=0 id='test'>";
	echo "<tr bgcolor=#E8E7E7 id='header'><td bgcolor=#E8E7E7>$showsubmit</td><td bgcolor=#E8E7E7 align=right><font size=1>[<a href=busdetail.php?bid=$businessid&cid=$companyid&prevperiod=$prevperiod>PREV</a>]</td><td align=right bgcolor=$day1color><font color=$wordfont1>$day1</td><td align=right bgcolor=$day2color><font color=$wordfont2>$day2</td><td align=right bgcolor=$day3color><font color=$wordfont3>$day3</td><td align=right bgcolor=$day4color><font color=$wordfont4>$day4</td><td align=right bgcolor=$day5color><font color=$wordfont5>$day5</td><td align=right bgcolor=$day6color><font color=$wordfont6>$day6</td><td align=right bgcolor=$day7color><font color=$wordfont7>$day7</td><td bgcolor=#E8E7E7> <font size=1>[<a href=busdetail.php?bid=$businessid&cid=$companyid&nextperiod=$nextperiod>NEXT</a>]</td></tr>";
	echo "<tr bgcolor=#CCCC99><td><b>CREDITS</b></td><td align=right>ACCT#</td><td align=right width=10%>$day1name</td><td align=right width=10%>$day2name</td><td align=right width=10%>$day3name</td><td align=right width=10%>$day4name</td><td align=right width=10%>$day5name</td><td align=right width=10%>$day6name</td><td align=right width=10%>$day7name</td><td align=right><b>TOTAL</b></td></tr>";

	//////CREDITS/////////////////////////////////////////////////////////////////////////////////////////////////////
	$day1sales=0;
	$day2sales=0;
	$day3sales=0;
	$day4sales=0;
	$day5sales=0;
	$day6sales=0;
	$day7sales=0;

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM credittype ORDER BY ordered DESC";
	$result = Treat_DB_ProxyOld::query( $query, true );
	$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );
	//mysql_close();

	$hc1=0;
	$hc2=0;
	$hc3=0;
	$hc4=0;
	$hc5=0;
	$hc6=0;
	$hc7=0;
	$pp1=0;
	$pp2=0;
	$pp3=0;
	$pp4=0;
	$pp5=0;
	$pp6=0;
	$pp7=0;
	$num--;
	$count=0;
	$value1="0";
	$heldcheck=0;
	$day1column=0;
	$day2column=0;
	$day3column=0;
	$day4column=0;
	$day5column=0;
	$day6column=0;
	$day7column=0;
	$rowtab=1;
	
	////disable mapped fields
	$arrayMap = array();
	
	$queryMap = "SELECT map.id, map.localid, map.name
			  FROM mburris_manage.client_programs cp
			  JOIN mburris_manage.mapping map ON map.client_programid = cp.client_programid 
				  AND map.tbl = 1
			  WHERE cp.businessid = $businessid AND cp.db_name = '". Treat_Config::singleton()->get('db', 'database') . "'";
	$resultMap = Treat_DB_ProxyOld::query( $queryMap );
	
	while($r = mysql_fetch_array($resultMap)){
		$arrayMap[$r["localid"]] = $r["name"];
	}
	
	$queryMap = "select mct.creditid from mburris_manage.client_programs cp
		join mburris_manage.mapping_cc_transactions mct ON mct.client_programid = cp.client_programid
		where cp.businessid = $businessid and cp.db_name = '". Treat_Config::singleton()->get('db', 'database') . "'";
	$resultMap = Treat_DB_ProxyOld::query( $queryMap );
	
	while($r = mysql_fetch_array($resultMap)){
		$arrayMap[$r["creditid"]] = "na";
	}
	
	while ($num>=0){
		$credittypename = Treat_DB_ProxyOld::mysql_result( $result, $num, 'credittypename', '' );
		$credittypeid = Treat_DB_ProxyOld::mysql_result( $result, $num, 'credittypeid', '' );

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query2 = "SELECT * FROM credits WHERE businessid = '$businessid' AND credittype = '$credittypeid' AND ((is_deleted = '0') || (is_deleted = '1' && del_date >= '$day1')) AND parent >= '0' ORDER BY credittype,creditname DESC";
		$result2 = Treat_DB_ProxyOld::query( $query2, true );
		$num2 = Treat_DB_ProxyOld::mysql_numrows( $result2, 0 );
		//mysql_close();

		$num2--;
		$day1coltot=0;
		$day2coltot=0;
		$day3coltot=0;
		$day4coltot=0;
		$day5coltot=0;
		$day6coltot=0;
		$day7coltot=0;

		while ($num2>=0){
			$tabcount=10;
			$creditrowtotal=0;
			$creditname = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'creditname', '' );
			$creditid = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'creditid', '' );
			$account = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'account', '' );
			$invoicesales = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'invoicesales', '' );
			$invoicesales_notax = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'invoicesales_notax', '' );
			$invoicetax = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'invoicetax', '' );
			$servchrg = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'servchrg', '' );
			$salescode = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'salescode', '' );
			$prepayment = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'heldcheck', '' );
			$showaccount=substr($account,0,5);

			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query27 = "SELECT * FROM sales_calc WHERE creditid = '$creditid'";
			$result27 = Treat_DB_ProxyOld::query( $query27, true );
			$num27 = Treat_DB_ProxyOld::mysql_numrows( $result27, 0 );
			//mysql_close();

			if ($plsincrement==1){$curarrow=$curarrow-7000+1;$plsincrement=0;}

			echo "<tr><td>$creditname</td><td align=right>$showaccount</td>";
			for ($counter=0;$counter<=6;$counter++){
				if ($counter==0){$day=$day1;}
				elseif ($counter==1){$day=$day2;}
				elseif ($counter==2){$day=$day3;}
				elseif ($counter==3){$day=$day4;}
				elseif ($counter==4){$day=$day5;}
				elseif ($counter==5){$day=$day6;}
				elseif ($counter==6){$day=$day7;}

				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( "Unable to select database");
				$query77 = "SELECT * FROM submit WHERE businessid = '$businessid' AND date = '$day'";
				$result77 = Treat_DB_ProxyOld::query( $query77, true );
				$num77 = Treat_DB_ProxyOld::mysql_numrows( $result77, 0 );
				//mysql_close();

				if ($num77!=0 || array_key_exists($creditid, $arrayMap)){$can_edit=1;}
				else {$can_edit=0;}

				//////MANUAL ENTER///////////////////////
				if ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0&&$prepayment==0&&($salescode==0||$salescode=="")){
					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( "Unable to select database");
					$query5 = "SELECT amount FROM creditdetail WHERE creditid = '$creditid' AND date = '$day'";
					$result5 = Treat_DB_ProxyOld::query( $query5, true );
					$num5 = Treat_DB_ProxyOld::mysql_numrows( $result5, 0 );
					//mysql_close();

					if ($num5==1&&$invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0&&$prepayment==0){
						$prevamount = Treat_DB_ProxyOld::mysql_result( $result5, 0, 'amount', '' );
						$prevamount = money($prevamount);
					}
					else{$prevamount="0";}

					echo "<td align=right>";
					$arrayname = "$count$counter$creditamount";
					if ($can_edit==1 && $security_level<8){
						echo "$$prevamount";
						echo "<INPUT TYPE=hidden name='$arrayname' value='$prevamount'>";
					}
					else {
						if ($prevamount==0){$prevamount="";}
						if ($num27>0){
							$calc_id = Treat_DB_ProxyOld::mysql_result( $result27, 0, 'calc_id', '' );
							echo "<INPUT TYPE=text name='$arrayname' value='$prevamount' SIZE=5 tabindex='$tabcount$tabrow' onkeydown='return checkKey()' IndexId=$curarrow onclick=calc_popup('sales_calc.php?calc_id=$calc_id&date=$day&bid=$businessid&cid=$companyid')>";
						}
						else{echo "<INPUT TYPE=text name='$arrayname' value='$prevamount' SIZE=5 tabindex='$tabcount$tabrow' onkeydown='return checkKey()' IndexId=$curarrow>";}
						$curarrow=$curarrow+1000;
						$plsincrement=1;
					}
					$arrayname = "$count$counter$creditidnty";
					echo "<INPUT TYPE=hidden name='$arrayname' value=$creditid>";
					$arrayname = "$count$counter$creditdate";
					echo "<INPUT TYPE=hidden name='$arrayname' value=$day></td>";
				}
				///INVOICE
				elseif ($invoicesales==1&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0&&$prepayment==0&&($salescode=="0" || $salescode=="")){
					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( "Unable to select database");
					$query5 = "SELECT total,taxtotal,servamt FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND taxable = 'on' AND salescode = '0' AND (status = '4' OR status = '2' OR status = '6')";
					$result5 = Treat_DB_ProxyOld::query( $query5, true );
					$num6 = Treat_DB_ProxyOld::mysql_numrows( $result5, 0 );
					//mysql_close();
					$num6--;
					$prevamount=0;
					while ($num6>=0){
						$invtotal = Treat_DB_ProxyOld::mysql_result( $result5, $num6, 'total', '' );
						$invtax = Treat_DB_ProxyOld::mysql_result( $result5, $num6, 'taxtotal', '' );
						$servamt = Treat_DB_ProxyOld::mysql_result( $result5, $num6, 'servamt', '' );
						$prevamount=$prevamount+($invtotal-$invtax-$servamt);
						$num6--;
					}
					$prevamount=money($prevamount);

					echo "<td align=right>$$prevamount</td>";
					if ($counter==0){$hc1=$hc1+$prevamount;}
					elseif ($counter==1){$hc2=$hc2+$prevamount;}
					elseif ($counter==2){$hc3=$hc3+$prevamount;}
					elseif ($counter==3){$hc4=$hc4+$prevamount;}
					elseif ($counter==4){$hc5=$hc5+$prevamount;}
					elseif ($counter==5){$hc6=$hc6+$prevamount;}
					elseif ($counter==6){$hc7=$hc7+$prevamount;}
				}
				///INVOICE TAX EXEMPT
				elseif ($invoicesales==0&&$invoicesales_notax==1&&$invoicetax==0&&$servchrg==0&&$prepayment==0&&($salescode=="0" || $salescode=="")){
					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( "Unable to select database");
					$query5 = "SELECT total,taxtotal,servamt FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND taxable = 'off' AND salescode = '0' AND (status = '4' OR status = '2' OR status = '6')";
					$result5 = Treat_DB_ProxyOld::query( $query5, true );
					$num6 = Treat_DB_ProxyOld::mysql_numrows( $result5, 0 );
					//mysql_close();
					$num6--;
					$prevamount=0;
					while ($num6>=0){
						$invtotal = Treat_DB_ProxyOld::mysql_result( $result5, $num6, 'total', '' );
						$servamt = Treat_DB_ProxyOld::mysql_result( $result5, $num6, 'servamt', '' );
						$prevamount=$prevamount+$invtotal-$servamt;
						$num6--;
					}
					$prevamount=money($prevamount);
					echo "<td align=right>$$prevamount</td>";
					if ($counter==0){$hc1=$hc1+$prevamount;}
					elseif ($counter==1){$hc2=$hc2+$prevamount;}
					elseif ($counter==2){$hc3=$hc3+$prevamount;}
					elseif ($counter==3){$hc4=$hc4+$prevamount;}
					elseif ($counter==4){$hc5=$hc5+$prevamount;}
					elseif ($counter==5){$hc6=$hc6+$prevamount;}
					elseif ($counter==6){$hc7=$hc7+$prevamount;}
				}
				///INVOICE TAX
				elseif ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==1&&$servchrg==0&&$prepayment==0&&($salescode=="0" || $salescode=="")){
					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( "Unable to select database");
					$query5 = "SELECT taxtotal FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND taxable = 'on' AND salescode = '0' AND (status = '4' OR status = '2' OR status = '6')";
					$result5 = Treat_DB_ProxyOld::query( $query5, true );
					$num6 = Treat_DB_ProxyOld::mysql_numrows( $result5, 0 );
					//mysql_close();
					$num6--;
					$prevamount=0;
					while ($num6>=0){
						$taxtotal = Treat_DB_ProxyOld::mysql_result( $result5, $num6, 'taxtotal', '' );
						$prevamount=$prevamount+$taxtotal;
						$num6--;
					}
					$prevamount=money($prevamount);
					echo "<td align=right>$$prevamount</td>";
					if ($counter==0){$hc1=$hc1+$prevamount;}
					elseif ($counter==1){$hc2=$hc2+$prevamount;}
					elseif ($counter==2){$hc3=$hc3+$prevamount;}
					elseif ($counter==3){$hc4=$hc4+$prevamount;}
					elseif ($counter==4){$hc5=$hc5+$prevamount;}
					elseif ($counter==5){$hc6=$hc6+$prevamount;}
					elseif ($counter==6){$hc7=$hc7+$prevamount;}
				}
				///INVOICE SERVICE CHARGE
				elseif ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==1&&$prepayment==0&&($salescode=="0" || $salescode=="")){
					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( "Unable to select database");
					$query5 = "SELECT servamt FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND salescode = '0' AND (status = '4' OR status = '2' OR status = '6')";
					$result5 = Treat_DB_ProxyOld::query( $query5, true );
					$num6 = Treat_DB_ProxyOld::mysql_numrows( $result5, 0 );
					//mysql_close();
					$num6--;
					$prevamount=0;
					while ($num6>=0){
						$servamt = Treat_DB_ProxyOld::mysql_result( $result5, $num6, 'servamt', '' );
						$prevamount=$prevamount+$servamt;
						$num6--;
					}
					$prevamount=money($prevamount);
					echo "<td align=right>$$prevamount</td>";
					if ($counter==0){$hc1=$hc1+$prevamount;}
					elseif ($counter==1){$hc2=$hc2+$prevamount;}
					elseif ($counter==2){$hc3=$hc3+$prevamount;}
					elseif ($counter==3){$hc4=$hc4+$prevamount;}
					elseif ($counter==4){$hc5=$hc5+$prevamount;}
					elseif ($counter==5){$hc6=$hc6+$prevamount;}
					elseif ($counter==6){$hc7=$hc7+$prevamount;}
				}
				///INVOICE (SALESCODE)////////////////////////////////////////////////////////////////////////////////////////////////////
				elseif ($invoicesales==1&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0&&$prepayment==0&&$salescode!="0" &&$salescode!=""){
					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( "Unable to select database");
					$query5 = "SELECT total,taxtotal,servamt FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND taxable = 'on' AND salescode = '$salescode' AND (status = '4' OR status = '2' OR status = '6')";
					$result5 = Treat_DB_ProxyOld::query( $query5, true );
					$num6 = Treat_DB_ProxyOld::mysql_numrows( $result5, 0 );
					//mysql_close();
					$num6--;
					$prevamount=0;
					while ($num6>=0){
						$invtotal = Treat_DB_ProxyOld::mysql_result( $result5, $num6, 'total', '' );
						$invtax = Treat_DB_ProxyOld::mysql_result( $result5, $num6, 'taxtotal', '' );
						$servamt = Treat_DB_ProxyOld::mysql_result( $result5, $num6, 'servamt', '' );
						$prevamount=$prevamount+($invtotal-$invtax-$servamt);
						$num6--;
					}
					$prevamount=money($prevamount);

					echo "<td align=right>$$prevamount</td>";
					if ($counter==0){$hc1=$hc1+$prevamount;}
					elseif ($counter==1){$hc2=$hc2+$prevamount;}
					elseif ($counter==2){$hc3=$hc3+$prevamount;}
					elseif ($counter==3){$hc4=$hc4+$prevamount;}
					elseif ($counter==4){$hc5=$hc5+$prevamount;}
					elseif ($counter==5){$hc6=$hc6+$prevamount;}
					elseif ($counter==6){$hc7=$hc7+$prevamount;}
				}
				///INVOICE TAX EXEMPT (SALESCODE)
				elseif ($invoicesales==0&&$invoicesales_notax==1&&$invoicetax==0&&$servchrg==0 && $salescode!="0" && $prepayment==0){
					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( "Unable to select database");
					$query5 = "SELECT total,taxtotal,servamt FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND taxable = 'off' AND salescode = '$salescode' AND (status = '4' OR status = '2' OR status = '6')";
					$result5 = Treat_DB_ProxyOld::query( $query5, true );
					$num6 = Treat_DB_ProxyOld::mysql_numrows( $result5, 0 );
					//mysql_close();
					$num6--;
					$prevamount=0;
					while ($num6>=0){
						$invtotal = Treat_DB_ProxyOld::mysql_result( $result5, $num6, 'total', '' );
						$servamt = Treat_DB_ProxyOld::mysql_result( $result5, $num6, 'servamt', '' );
						$prevamount=$prevamount+$invtotal-$servamt;
						$num6--;
					}
					$prevamount=money($prevamount);
					echo "<td align=right>$$prevamount</td>";
					if ($counter==0){$hc1=$hc1+$prevamount;}
					elseif ($counter==1){$hc2=$hc2+$prevamount;}
					elseif ($counter==2){$hc3=$hc3+$prevamount;}
					elseif ($counter==3){$hc4=$hc4+$prevamount;}
					elseif ($counter==4){$hc5=$hc5+$prevamount;}
					elseif ($counter==5){$hc6=$hc6+$prevamount;}
					elseif ($counter==6){$hc7=$hc7+$prevamount;}
				}
				///INVOICE TAX (SALESCODE)
				elseif ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==1&&$servchrg==0&&$salescode!="0"&&$salescode!=""&&$prepayment==0){
					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( "Unable to select database");
					$query5 = "SELECT taxtotal FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND taxable = 'on' AND salescode = '$salescode' AND (status = '4' OR status = '2' OR status = '6')";
					$result5 = Treat_DB_ProxyOld::query( $query5, true );
					$num6 = Treat_DB_ProxyOld::mysql_numrows( $result5, 0 );
					//mysql_close();
					$num6--;
					$prevamount=0;
					while ($num6>=0){
						$taxtotal = Treat_DB_ProxyOld::mysql_result( $result5, $num6, 'taxtotal', '' );
						$prevamount=$prevamount+$taxtotal;
						$num6--;
					}
					$prevamount=money($prevamount);
					echo "<td align=right>$$prevamount</td>";
					if ($counter==0){$hc1=$hc1+$prevamount;}
					elseif ($counter==1){$hc2=$hc2+$prevamount;}
					elseif ($counter==2){$hc3=$hc3+$prevamount;}
					elseif ($counter==3){$hc4=$hc4+$prevamount;}
					elseif ($counter==4){$hc5=$hc5+$prevamount;}
					elseif ($counter==5){$hc6=$hc6+$prevamount;}
					elseif ($counter==6){$hc7=$hc7+$prevamount;}
				}
				///INVOICE SERVICE CHARGE (SALESCODE)
				elseif ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==1&&$salescode!="0"&&$salescode!=""&&$prepayment==0){
					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( "Unable to select database");
					$query5 = "SELECT servamt FROM invoice WHERE type = '1' AND businessid = '$businessid' AND salescode = '$salescode' AND date = '$day' AND (status = '4' OR status = '2' OR status = '6')";
					$result5 = Treat_DB_ProxyOld::query( $query5, true );
					$num6 = Treat_DB_ProxyOld::mysql_numrows( $result5, 0 );
					//mysql_close();
					$num6--;
					$prevamount=0;
					while ($num6>=0){
						$servamt = Treat_DB_ProxyOld::mysql_result( $result5, $num6, 'servamt', '' );
						$prevamount=$prevamount+$servamt;
						$num6--;
					}
					$prevamount=money($prevamount);
					echo "<td align=right>$$prevamount</td>";
					if ($counter==0){$hc1=$hc1+$prevamount;}
					elseif ($counter==1){$hc2=$hc2+$prevamount;}
					elseif ($counter==2){$hc3=$hc3+$prevamount;}
					elseif ($counter==3){$hc4=$hc4+$prevamount;}
					elseif ($counter==4){$hc5=$hc5+$prevamount;}
					elseif ($counter==5){$hc6=$hc6+$prevamount;}
					elseif ($counter==6){$hc7=$hc7+$prevamount;}
				}
				///PREPAYMENT
				elseif ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0&&$prepayment==1){
					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( "Unable to select database");
					$query5 = "SELECT amount FROM invtender WHERE businessid = '$businessid' AND date = '$day' AND sameday = '1'";
					$result5 = Treat_DB_ProxyOld::query( $query5, true );
					$num6 = Treat_DB_ProxyOld::mysql_numrows( $result5, 0 );
					//mysql_close();
					$num6--;
					$prevamount=0;
					while ($num6>=0){
						$amount = Treat_DB_ProxyOld::mysql_result( $result5, $num6, 'amount', '' );
						$prevamount=$prevamount+$amount;
						$num6--;
					}
					$prevamount=money($prevamount);
					echo "<td align=right>$$prevamount</td>";
					if ($counter==0){$pp1=$pp1+$prevamount;}
					elseif ($counter==1){$pp2=$pp2+$prevamount;}
					elseif ($counter==2){$pp3=$pp3+$prevamount;}
					elseif ($counter==3){$pp4=$pp4+$prevamount;}
					elseif ($counter==4){$pp5=$pp5+$prevamount;}
					elseif ($counter==5){$pp6=$pp6+$prevamount;}
					elseif ($counter==6){$pp7=$pp7+$prevamount;}

				}
				$creditrowtotal=money($creditrowtotal+$prevamount);


				if ($day==$day1){$day1coltot=money($day1coltot+$prevamount);}
				elseif ($day==$day2){$day2coltot=money($day2coltot+$prevamount);}
				elseif ($day==$day3){$day3coltot=money($day3coltot+$prevamount);}
				elseif ($day==$day4){$day4coltot=money($day4coltot+$prevamount);}
				elseif ($day==$day5){$day5coltot=money($day5coltot+$prevamount);}
				elseif ($day==$day6){$day6coltot=money($day6coltot+$prevamount);}
				elseif ($day==$day7){$day7coltot=money($day7coltot+$prevamount);}

				if ($day==$day1&&$credittypeid==1&&$invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0&&$prepayment==0&&($salescode==0||$salescode=="")){$day1sales=$day1sales+$prevamount;}
				elseif ($day==$day2&&$credittypeid==1&&$invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0&&$prepayment==0&&($salescode==0||$salescode=="")){$day2sales=$day2sales+$prevamount;}
				elseif ($day==$day3&&$credittypeid==1&&$invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0&&$prepayment==0&&($salescode==0||$salescode=="")){$day3sales=$day3sales+$prevamount;}
				elseif ($day==$day4&&$credittypeid==1&&$invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0&&$prepayment==0&&($salescode==0||$salescode=="")){$day4sales=$day4sales+$prevamount;}
				elseif ($day==$day5&&$credittypeid==1&&$invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0&&$prepayment==0&&($salescode==0||$salescode=="")){$day5sales=$day5sales+$prevamount;}
				elseif ($day==$day6&&$credittypeid==1&&$invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0&&$prepayment==0&&($salescode==0||$salescode=="")){$day6sales=$day6sales+$prevamount;}
				elseif ($day==$day7&&$credittypeid==1&&$invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0&&$prepayment==0&&($salescode==0||$salescode=="")){$day7sales=$day7sales+$prevamount;}

				$tabcount++;
			}
			echo "<td bgcolor=#CCCC99 align=right>$$creditrowtotal</td></tr>";
			$tabcount++;
			$num2--;
			if ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0&&$prepayment==0){$count++;}
		}
		$rowtotal=money($day1coltot+$day2coltot+$day3coltot+$day4coltot+$day5coltot+$day6coltot+$day7coltot);
		$rowtab++;
		echo "<tr bgcolor=#CCCCCC><td><b>$credittypename Total</b></td><td></td><td align=right>$$day1coltot</td><td align=right>$$day2coltot</td><td align=right>$$day3coltot</td><td align=right>$$day4coltot</td><td align=right>$$day5coltot</td><td align=right>$$day6coltot</td><td align=right>$$day7coltot</td><td align=right><b>$$rowtotal</b></td></tr>";

		$day1column=money($day1column+$day1coltot);
		$day2column=money($day2column+$day2coltot);
		$day3column=money($day3column+$day3coltot);
		$day4column=money($day4column+$day4coltot);
		$day5column=money($day5column+$day5coltot);
		$day6column=money($day6column+$day6coltot);
		$day7column=money($day7column+$day7coltot);
		$num--;
	}

	///CASH OVER//////////////////

	echo "<tr><td colspan=2>Cash Over</td>";

	$cashtotal=0;
	for ($counter=0;$counter<=6;$counter++){
		if ($counter==0){$day=$day1;}
		elseif ($counter==1){$day=$day2;}
		elseif ($counter==2){$day=$day3;}
		elseif ($counter==3){$day=$day4;}
		elseif ($counter==4){$day=$day5;}
		elseif ($counter==5){$day=$day6;}
		elseif ($counter==6){$day=$day7;}

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query2 = "SELECT * FROM cash WHERE companyid = '$companyid' AND businessid = '$businessid' AND date = '$day' AND type = '1'";
		$result2 = Treat_DB_ProxyOld::query( $query2, true );
		$num2 = Treat_DB_ProxyOld::mysql_numrows( $result2, 0 );
		//mysql_close();

		if ($num2!=0){$cashamount = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'amount', '' );}
		else {$cashamount="0.00";}

		$cashamount=money($cashamount);

		echo "<td align=right>$$cashamount</td>";
		$cashtotal=$cashtotal+$cashamount;

		if ($counter==0){$day1column2=money($day1column+$cashamount);$day1cashover=$cashamount;}
		elseif ($counter==1){$day2column2=money($day2column+$cashamount);$day2cashover=$cashamount;}
		elseif ($counter==2){$day3column2=money($day3column+$cashamount);$day3cashover=$cashamount;}
		elseif ($counter==3){$day4column2=money($day4column+$cashamount);$day4cashover=$cashamount;}
		elseif ($counter==4){$day5column2=money($day5column+$cashamount);$day5cashover=$cashamount;}
		elseif ($counter==5){$day6column2=money($day6column+$cashamount);$day6cashover=$cashamount;}
		elseif ($counter==6){$day7column2=money($day7column+$cashamount);$day7cashover=$cashamount;}

	}
	$cashtotal=money($cashtotal);
	echo "<td align=right><b>$$cashtotal</b></td></tr>";
	///////////////////////
	$credittotal=money($day1column2+$day2column2+$day3column2+$day4column2+$day5column2+$day6column2+$day7column2);
	echo "<tr bgcolor=#CCCC99><td><b>CREDIT TOTAL</b><INPUT TYPE=hidden name='creditcount' value=$count></td><td></td><td align=right>$$day1column2</td><td align=right>$$day2column2</td><td align=right>$$day3column2</td><td align=right>$$day4column2</td><td align=right>$$day5column2</td><td align=right>$$day6column2</td><td align=right>$$day7column2</td><td align=right bgcolor=yellow><b>$$credittotal</b></td></tr>";
	$credittotal=money($day1column+$day2column+$day3column+$day4column+$day5column+$day6column+$day7column);
	//////DEBITS///////////////////////////////////////////////////////////////////////////////////////

	echo "<tr><td colspan=10 height=10></td></tr>";
	echo "<tr bgcolor=#CCCC99><td colspan=10 height=5><b>DEBITS</b></td></tr>";

	////disable mapped fields
	$arrayMap = array();
	
	$queryMap = "SELECT map.id, map.localid, map.name
			  FROM mburris_manage.client_programs cp
			  JOIN mburris_manage.mapping map ON map.client_programid = cp.client_programid 
				  AND map.tbl = 2
			  WHERE cp.businessid = $businessid AND cp.db_name = '". Treat_Config::singleton()->get('db', 'database') . "'";
	$resultMap = Treat_DB_ProxyOld::query( $queryMap );
	
	while($r = mysql_fetch_array($resultMap)){
		$arrayMap[$r["localid"]] = $r["name"];
	}
	
	$queryMap = "select mct.debitid from mburris_manage.client_programs cp
		join mburris_manage.mapping_cc_transactions mct ON mct.client_programid = cp.client_programid
		where cp.businessid = $businessid and cp.db_name = '". Treat_Config::singleton()->get('db', 'database') . "'";
	$resultMap = Treat_DB_ProxyOld::query( $queryMap );
	
	while($r = mysql_fetch_array($resultMap)){
		$arrayMap[$r["debitid"]] = "na";
	}
	
	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM debittype ORDER BY debittypename DESC";
	$result = Treat_DB_ProxyOld::query( $query, true );
	$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );
	//mysql_close();

	$num--;
	$count=0;
	$value1="0";
	$day1column1=0;
	$day2column1=0;
	$day3column1=0;
	$day4column1=0;
	$day5column1=0;
	$day6column1=0;
	$day7column1=0;
	$rowtab=1;

	while ($num>=0){
		$debittypename = Treat_DB_ProxyOld::mysql_result( $result, $num, 'debittypename', '' );
		$debittypeid = Treat_DB_ProxyOld::mysql_result( $result, $num, 'debittypeid', '' );

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query2 = "SELECT * FROM debits WHERE businessid = '$businessid' AND debittype = '$debittypeid' AND ((is_deleted = '0') || (is_deleted = '1' && del_date >= '$day1')) ORDER BY debittype,debitname DESC";
		$result2 = Treat_DB_ProxyOld::query( $query2, true );
		$num2 = Treat_DB_ProxyOld::mysql_numrows( $result2, 0 );
		//mysql_close();

		$num2--;
		$day1coltot=0;
		$day2coltot=0;
		$day3coltot=0;
		$day4coltot=0;
		$day5coltot=0;
		$day6coltot=0;
		$day7coltot=0;
		while ($num2>=0){
			$tabcount=10;
			$debitrowtotal=0;
			$debitname = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'debitname', '' );
			$debitid = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'debitid', '' );
			$account = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'account', '' );
			$prepayment = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'heldcheck', '' );
			$showaccount=substr($account,0,5);

			if ($plsincrement==1){$curarrow=$curarrow-7000+1;$plsincrement=0;}

			echo "<tr><td>$debitname</td><td align=right>$showaccount</td>";
			for ($counter=0;$counter<=6;$counter++){
				if ($counter==0){$day=$day1;}
				elseif ($counter==1){$day=$day2;}
				elseif ($counter==2){$day=$day3;}
				elseif ($counter==3){$day=$day4;}
				elseif ($counter==4){$day=$day5;}
				elseif ($counter==5){$day=$day6;}
				elseif ($counter==6){$day=$day7;}

				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( "Unable to select database");
				$query77 = "SELECT * FROM submit WHERE businessid = '$businessid' AND date = '$day'";
				$result77 = Treat_DB_ProxyOld::query( $query77, true );
				$num77 = Treat_DB_ProxyOld::mysql_numrows( $result77, 0 );
				//mysql_close();

				if ($num77!=0 || (array_key_exists($debitid, $arrayMap))){$can_edit=1;}
				else {$can_edit=0;}

				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( "Unable to select database");
				$query5 = "SELECT amount FROM debitdetail WHERE debitid = '$debitid' AND date = '$day'";
				$result5 = Treat_DB_ProxyOld::query( $query5, true );
				$num5 = Treat_DB_ProxyOld::mysql_numrows( $result5, 0 );
				//mysql_close();

				if ($num5==1){$prevamount = Treat_DB_ProxyOld::mysql_result( $result5, 0, 'amount', '' );
					$prevamount = money($prevamount);
				}
				else{$prevamount="0";}

				echo "<td align=right>";
				$arrayname = "$count$counter$debitamount";
				if ($prevamount==0){$prevamount="";}
				if ($can_edit==1 && $security_level<8&&$prepayment!=1){echo "$$prevamount<INPUT TYPE=hidden name='$arrayname' value='$prevamount'>";}
				elseif ($prepayment==1){
					if ($counter==0){$hcheck=$hc1;}
					elseif ($counter==1){$hcheck=$hc2;}
					elseif ($counter==2){$hcheck=$hc3;}
					elseif ($counter==3){$hcheck=$hc4;}
					elseif ($counter==4){$hcheck=$hc5;}
					elseif ($counter==5){$hcheck=$hc6;}
					elseif ($counter==6){$hcheck=$hc7;}
					if ($hcheck<=0){$hcheck=0;}
					$prevamount=money($hcheck);
					echo "$$prevamount";
				}
				else {echo "<INPUT TYPE=text name='$arrayname' value='$prevamount' SIZE=5 tabindex='$tabcount$tabrow' onkeydown='return checkKey()' IndexId=$curarrow>";
						$curarrow=$curarrow+1000;
						$plsincrement=1;
				}

				$arrayname = "$count$counter$debitidnty";
				echo "<INPUT TYPE=hidden name='$arrayname' value=$debitid>";
				$arrayname = "$count$counter$debitdate";

				echo "<INPUT TYPE=hidden name='$arrayname' value=$day></td>";
				$debitrowtotal=money($debitrowtotal+$prevamount);

				if ($day==$day1){$day1coltot=money($day1coltot+$prevamount);}
				elseif ($day==$day2){$day2coltot=money($day2coltot+$prevamount);}
				elseif ($day==$day3){$day3coltot=money($day3coltot+$prevamount);}
				elseif ($day==$day4){$day4coltot=money($day4coltot+$prevamount);}
				elseif ($day==$day5){$day5coltot=money($day5coltot+$prevamount);}
				elseif ($day==$day6){$day6coltot=money($day6coltot+$prevamount);}
				elseif ($day==$day7){$day7coltot=money($day7coltot+$prevamount);}

				$tabcount++;
			}
			echo "<td bgcolor=#CCCC99 align=right>$$debitrowtotal</td></tr>";
			$num2--;
			$count++;
		}
		$rowtotal=money($day1coltot+$day2coltot+$day3coltot+$day4coltot+$day5coltot+$day6coltot+$day7coltot);
		$rowtab++;
		echo "<tr bgcolor=#CCCCCC><td><b>$debittypename Total</b></td><td></td><td align=right>$$day1coltot</td><td align=right>$$day2coltot</td><td align=right>$$day3coltot</td><td align=right>$$day4coltot</td><td align=right>$$day5coltot</td><td align=right>$$day6coltot</td><td align=right>$$day7coltot</td><td align=right><b>$$rowtotal</b></td></tr>";
		$day1column1=money($day1column1+$day1coltot);
		$day2column1=money($day2column1+$day2coltot);
		$day3column1=money($day3column1+$day3coltot);
		$day4column1=money($day4column1+$day4coltot);
		$day5column1=money($day5column1+$day5coltot);
		$day6column1=money($day6column1+$day6coltot);
		$day7column1=money($day7column1+$day7coltot);
		$num--;
	}

	///CASH SHORT//////////////////

	echo "<tr><td colspan=2>Cash Short</td>";

	$cashtotal=0;
	for ($counter=0;$counter<=6;$counter++){
		if ($counter==0){$day=$day1;}
		elseif ($counter==1){$day=$day2;}
		elseif ($counter==2){$day=$day3;}
		elseif ($counter==3){$day=$day4;}
		elseif ($counter==4){$day=$day5;}
		elseif ($counter==5){$day=$day6;}
		elseif ($counter==6){$day=$day7;}

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query2 = "SELECT * FROM cash WHERE businessid = '$businessid' AND date = '$day' AND type = '0'";
		$result2 = Treat_DB_ProxyOld::query( $query2, true );
		$num2 = Treat_DB_ProxyOld::mysql_numrows( $result2, 0 );
		//mysql_close();

		if ($num2!=0){$cashamount = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'amount', '' );}
		else {$cashamount="0.00";}

		$cashamount=money($cashamount);

		echo "<td align=right>$$cashamount</td>";
		$cashtotal=$cashtotal+$cashamount;

		if ($counter==0){$day1column2=money($day1column1+$cashamount);$day1cashshort=$cashamount;}
		elseif ($counter==1){$day2column2=money($day2column1+$cashamount);$day2cashshort=$cashamount;}
		elseif ($counter==2){$day3column2=money($day3column1+$cashamount);$day3cashshort=$cashamount;}
		elseif ($counter==3){$day4column2=money($day4column1+$cashamount);$day4cashshort=$cashamount;}
		elseif ($counter==4){$day5column2=money($day5column1+$cashamount);$day5cashshort=$cashamount;}
		elseif ($counter==5){$day6column2=money($day6column1+$cashamount);$day6cashshort=$cashamount;}
		elseif ($counter==6){$day7column2=money($day7column1+$cashamount);$day7cashshort=$cashamount;}

	}
	$cashtotal=money($cashtotal);
	echo "<td align=right><b>$$cashtotal</b></td></tr>";
	///////////////////////

	$debittotal=money($day1column2+$day2column2+$day3column2+$day4column2+$day5column2+$day6column2+$day7column2);
	echo "<tr bgcolor=#CCCC99><td><b>DEBIT TOTAL</b><INPUT TYPE=hidden name='debitcount' value=$count></td><td></td><td align=right>$$day1column2</td><td align=right>$$day2column2</td><td align=right>$$day3column2</td><td align=right>$$day4column2</td><td align=right>$$day5column2</td><td align=right>$$day6column2</td><td align=right>$$day7column2</td><td align=right bgcolor=yellow><b>$$debittotal</b></td></tr>";
	$debittotal=money($day1column1+$day2column1+$day3column1+$day4column1+$day5column1+$day6column1+$day7column1);
	///////////////////////
	echo "<tr><td colspan=10 height=10></td></tr>";
	if ($credittotal==$debittotal){$color="#00FF00";}
	else {$color="red";}
	$diff=$debittotal-$credittotal;
	$diff=money($diff);

	$day1column1=$day1column1-$day1column;
	$day2column1=$day2column1-$day2column;
	$day3column1=$day3column1-$day3column;
	$day4column1=$day4column1-$day4column;
	$day5column1=$day5column1-$day5column;
	$day6column1=$day6column1-$day6column;
	$day7column1=$day7column1-$day7column;

	if(round(abs($day1column1),2) != $day1cashover && round(abs($day1column1),2) != $day1cashshort){$problem=1;$problem_date=$day1;$problem_submit="DISABLED";}
	if(round(abs($day2column1),2) != $day2cashover && round(abs($day2column1),2) != $day2cashshort){$problem=1;$problem_date=$day2;$problem_submit="DISABLED";}
	if(round(abs($day3column1),2) != $day3cashover && round(abs($day3column1),2) != $day3cashshort){$problem=1;$problem_date=$day3;$problem_submit="DISABLED";}
	if(round(abs($day4column1),2) != $day4cashover && round(abs($day4column1),2) != $day4cashshort){$problem=1;$problem_date=$day4;$problem_submit="DISABLED";}
	if(round(abs($day5column1),2) != $day5cashover && round(abs($day5column1),2) != $day5cashshort){$problem=1;$problem_date=$day5;$problem_submit="DISABLED";}
	if(round(abs($day6column1),2) != $day6cashover && round(abs($day6column1),2) != $day6cashshort){$problem=1;$problem_date=$day6;$problem_submit="DISABLED";}
	if(round(abs($day7column1),2) != $day7cashover && round(abs($day7column1),2) != $day7cashshort){$problem=1;$problem_date=$day7;$problem_submit="DISABLED";}

	$showcash=substr($over_short_acct,0,5);
	echo "<tr bgcolor=$color><td>Cash Over/Short</td><td align=right>$showcash</td>";
	if ($day1column1==0){$color="#00FF00";}
	else {$color="red";}
	$day1column1=money($day1column1);
	if ($day1column1=="0.00" && $color=="red"){$day1column1="0.01";}
	elseif ($day1column1=="-0.00" && $color=="red"){$day1column1="-0.01";}
	echo "<td align=right bgcolor=$color>$$day1column1</td>";
	if ($day2column1==0){$color="#00FF00";}
	else {$color="red";}
	$day2column1=money($day2column1);
	if ($day2column1=="0.00" && $color=="red"){$day2column1="0.01";}
	elseif ($day2column1=="-0.00" && $color=="red"){$day2column1="-0.01";}
	echo "<td align=right bgcolor=$color>$$day2column1</td>";
	if ($day3column1==0){$color="#00FF00";}
	else {$color="red";}
	$day3column1=money($day3column1);
	if ($day3column1=="0.00" && $color=="red"){$day3column1="0.01";}
	elseif ($day3column1=="-0.00" && $color=="red"){$day3column1="-0.01";}
	echo "<td align=right bgcolor=$color>$$day3column1</td>";
	if ($day4column1==0){$color="#00FF00";}
	else {$color="red";}
	$day4column1=money($day4column1);
	if ($day4column1=="0.00" && $color=="red"){$day4column1="0.01";}
	elseif ($day4column1=="-0.00" && $color=="red"){$day4column1="-0.01";}
	echo "<td align=right bgcolor=$color>$$day4column1</td>";
	if ($day5column1==0){$color="#00FF00";}
	else {$color="red";}
	$day5column1=money($day5column1);
	if ($day5column1=="0.00" && $color=="red"){$day5column1="0.01";}
	elseif ($day5column1=="-0.00" && $color=="red"){$day5column1="-0.01";}
	echo "<td align=right bgcolor=$color>$$day5column1</td>";
	if ($day6column1==0){$color="#00FF00";}
	else {$color="red";}
	$day6column1=money($day6column1);
	if ($day6column1=="0.00" && $color=="red"){$day6column1="0.01";}
	elseif ($day6column1=="-0.00" && $color=="red"){$day6column1="-0.01";}
	echo "<td align=right bgcolor=$color>$$day6column1</td>";
	if ($day7column1==0){$color="#00FF00";}
	else {$color="red";}
	$day7column1=money($day7column1);
	if ($day7column1=="0.00" && $color=="red"){$day7column1="0.01";}
	elseif ($day7column1=="-0.00" && $color=="red"){$day7column1="-0.01";}
	echo "<td align=right bgcolor=$color>$$day7column1</td>";

	echo "<td align=right><b>$$diff</b></td></tr>";

	/////////////////////START STATS////////////////////////////////////////////////////////
	////////////////////////////////
	/////////kiosk?/////////////////
	////////////////////////////////
	$query77 = "SELECT COUNT(businessid) AS is_kiosk FROM machine_bus_link WHERE businessid = $businessid";
	$result77 = Treat_DB_ProxyOld::query( $query77 );

	$is_kiosk = Treat_DB_ProxyOld::mysql_result( $result77, 0, 'is_kiosk', '' );

	$row1=0;
	$row2=0;
	$row3=0;
	$row4=0;
	$row5=0;
	$row6=0;
	$row7=0;
	echo "<tr><td colspan=10 height=10></td></tr>";

	echo "<tr bgcolor=#CCCC99><td colspan=10><b>KPI</b></td></tr>";

	echo "<tr bgcolor='#CCCCCC'><td colspan=10><b>Customer Count</b></td></tr>";
	echo "<tr><td colspan=2>$count1</td>";

	$curarrow=$curarrow-7000+1;

	$void = 0;
	$totalcc=0;
	for ($counter=0;$counter<=6;$counter++){
		if ($counter==0){$day=$day1;$tabcount=100;}
		elseif ($counter==1){$day=$day2;$tabcount=111;}
		elseif ($counter==2){$day=$day3;$tabcount=122;}
		elseif ($counter==3){$day=$day4;$tabcount=133;}
		elseif ($counter==4){$day=$day5;$tabcount=144;}
		elseif ($counter==5){$day=$day6;$tabcount=155;}
		elseif ($counter==6){$day=$day7;$tabcount=166;}

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query77 = "SELECT * FROM submit WHERE businessid = '$businessid' AND date = '$day'";
		$result77 = Treat_DB_ProxyOld::query( $query77, true );
		$num77 = Treat_DB_ProxyOld::mysql_numrows( $result77, 0 );
		//mysql_close();

		if ($num77!=0){$can_edit=1;}
		else {$can_edit=0;}

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query2 = "SELECT * FROM stats WHERE businessid = '$businessid' AND date = '$day'";
		$result2 = Treat_DB_ProxyOld::query( $query2, true );
		$num2 = Treat_DB_ProxyOld::mysql_numrows( $result2, 0 );
		//mysql_close();

		if ($num2!=0){
			$cccurent = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'number', '' );
			$lunch = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'lunch', '' );
			$dinner = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'dinner', '' );
			$labor = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'labor', '' );
			$void = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'void', '' );
		}

		////kiosk
		if($is_kiosk > 0){
			///breakfast
			$query3 = "SELECT COUNT(check_number) AS cccurent FROM checks WHERE businessid = '$businessid' AND bill_datetime >= '$day 00:00:00' AND checks.bill_datetime <= '$day 10:00:00' AND is_void = 0 AND total > 0";
			$result3 = Treat_DB_ProxyOld::query( $query3, true );
			$cccurent = Treat_DB_ProxyOld::mysql_result( $result3, 0, "cccurent", '' );
		}

		if ($can_edit==1 && $security_level<8){echo "<td align=right>$cccurent<INPUT TYPE=hidden name='cc$day' value='$cccurent'></td>";}
		else {
			if ($cccurent==0){$cccurent="";}
			echo "<td align=right><INPUT TYPE=text SIZE=5 name='cc$day' value='$cccurent' tabindex='$tabcount' onkeydown='return checkKey()' IndexId=$curarrow></td>";
			$curarrow=$curarrow+1000;
		}
		if ($counter==0){$day1cc=$cccurent;$row1=$row1+$cccurent;}
		elseif ($counter==1){$day2cc=$cccurent;$row2=$row2+$cccurent;}
		elseif ($counter==2){$day3cc=$cccurent;$row3=$row3+$cccurent;}
		elseif ($counter==3){$day4cc=$cccurent;$row4=$row4+$cccurent;}
		elseif ($counter==4){$day5cc=$cccurent;$row5=$row5+$cccurent;}
		elseif ($counter==5){$day6cc=$cccurent;$row6=$row6+$cccurent;}
		elseif ($counter==6){$day7cc=$cccurent;$row7=$row7+$cccurent;}
		$totalcc=$totalcc+$cccurent;
	}
	echo "<td align=right><b>$totalcc</b></td></tr>";

	$curarrow=$curarrow-7000+1;

	echo "<tr><td colspan=2>$count2</td>";
	$totalcc=0;
	for ($counter=0;$counter<=6;$counter++){
		if ($counter==0){$day=$day1;$tabcount=100;}
		elseif ($counter==1){$day=$day2;$tabcount=111;}
		elseif ($counter==2){$day=$day3;$tabcount=122;}
		elseif ($counter==3){$day=$day4;$tabcount=133;}
		elseif ($counter==4){$day=$day5;$tabcount=144;}
		elseif ($counter==5){$day=$day6;$tabcount=155;}
		elseif ($counter==6){$day=$day7;$tabcount=166;}

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query77 = "SELECT * FROM submit WHERE businessid = '$businessid' AND date = '$day'";
		$result77 = Treat_DB_ProxyOld::query( $query77, true );
		$num77 = Treat_DB_ProxyOld::mysql_numrows( $result77, 0 );
		//mysql_close();

		if ($num77!=0){$can_edit=1;}
		else {$can_edit=0;}

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query2 = "SELECT * FROM stats WHERE businessid = '$businessid' AND date = '$day'";
		$result2 = Treat_DB_ProxyOld::query( $query2, true );
		$num2 = Treat_DB_ProxyOld::mysql_numrows( $result2, 0 );
		//mysql_close();

		if ($num2!=0){
			$cccurent = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'number', '' );
			$lunch = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'lunch', '' );
			$dinner = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'dinner', '' );
			$labor = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'labor', '' );
			$void = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'void', '' );
		}

		////kiosk
		if($is_kiosk > 0){
			///lunch
			$query3 = "SELECT COUNT(check_number) AS lunch FROM checks WHERE businessid = '$businessid' AND bill_datetime >= '$day 10:00:01' AND checks.bill_datetime <= '$day 15:00:00' AND is_void = 0 AND total > 0";
			$result3 = Treat_DB_ProxyOld::query( $query3, true );
			$lunch = Treat_DB_ProxyOld::mysql_result( $result3, 0, "lunch", '' );
		}

		if ($can_edit==1 && $security_level<8){echo "<td align=right>$lunch<INPUT TYPE=hidden name='lunch$day' value='$lunch'></td>";}
		else {
			if ($lunch==0){$lunch="";}
			echo "<td align=right><INPUT TYPE=text SIZE=5 name='lunch$day' value='$lunch' tabindex='$tabcount' onkeydown='return checkKey()' IndexId=$curarrow></td>";
			$curarrow=$curarrow+1000;
		}
		if ($counter==0){$day1cc=$lunch;$row1=$row1+$lunch;}
		elseif ($counter==1){$day2cc=$lunch;$row2=$row2+$lunch;}
		elseif ($counter==2){$day3cc=$lunch;$row3=$row3+$lunch;}
		elseif ($counter==3){$day4cc=$lunch;$row4=$row4+$lunch;}
		elseif ($counter==4){$day5cc=$lunch;$row5=$row5+$lunch;}
		elseif ($counter==5){$day6cc=$lunch;$row6=$row6+$lunch;}
		elseif ($counter==6){$day7cc=$lunch;$row7=$row7+$lunch;}
		$totalcc=$totalcc+$lunch;
	}
	echo "<td align=right><b>$totalcc</b></td></tr>";

	$curarrow=$curarrow-7000+1;

	echo "<tr><td colspan=2>$count3</td>";
	$totalcc=0;
	for ($counter=0;$counter<=6;$counter++){
		if ($counter==0){$day=$day1;$tabcount=100;}
		elseif ($counter==1){$day=$day2;$tabcount=111;}
		elseif ($counter==2){$day=$day3;$tabcount=122;}
		elseif ($counter==3){$day=$day4;$tabcount=133;}
		elseif ($counter==4){$day=$day5;$tabcount=144;}
		elseif ($counter==5){$day=$day6;$tabcount=155;}
		elseif ($counter==6){$day=$day7;$tabcount=166;}

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query77 = "SELECT * FROM submit WHERE businessid = '$businessid' AND date = '$day'";
		$result77 = Treat_DB_ProxyOld::query( $query77, true );
		$num77 = Treat_DB_ProxyOld::mysql_numrows( $result77, 0 );
		//mysql_close();

		if ($num77!=0){$can_edit=1;}
		else {$can_edit=0;}

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query2 = "SELECT * FROM stats WHERE businessid = '$businessid' AND date = '$day'";
		$result2 = Treat_DB_ProxyOld::query( $query2, true );
		$num2 = Treat_DB_ProxyOld::mysql_numrows( $result2, 0 );
		//mysql_close();

		if ($num2!=0){
			$cccurent = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'number', '' );
			$lunch = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'lunch', '' );
			$dinner = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'dinner', '' );
			$labor = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'labor', '' );
			$void = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'void', '' );
		}

		////kiosk
		if($is_kiosk > 0){
			///dinner
			$query3 = "SELECT COUNT(check_number) AS dinner FROM checks WHERE businessid = '$businessid' AND bill_datetime >= '$day 15:00:01' AND checks.bill_datetime <= '$day 23:59:59' AND is_void = 0 AND total > 0";
			$result3 = Treat_DB_ProxyOld::query( $query3, true );
			$dinner = Treat_DB_ProxyOld::mysql_result( $result3, 0, "dinner", '' );
		}

		if ($can_edit==1 && $security_level<8){echo "<td align=right>$dinner<INPUT TYPE=hidden name='dinner$day' value='$dinner'></td>";}
		else {
			if ($dinner==0){$dinner="";}
			echo "<td align=right><INPUT TYPE=text SIZE=5 name='dinner$day' value='$dinner' tabindex='$tabcount' onkeydown='return checkKey()' IndexId=$curarrow></td>";
			$curarrow=$curarrow+1000;
		}
		if ($counter==0){$day1cc=$dinner;$row1=$row1+$dinner;}
		elseif ($counter==1){$day2cc=$dinner;$row2=$row2+$dinner;}
		elseif ($counter==2){$day3cc=$dinner;$row3=$row3+$dinner;}
		elseif ($counter==3){$day4cc=$dinner;$row4=$row4+$dinner;}
		elseif ($counter==4){$day5cc=$dinner;$row5=$row5+$dinner;}
		elseif ($counter==5){$day6cc=$dinner;$row6=$row6+$dinner;}
		elseif ($counter==6){$day7cc=$dinner;$row7=$row7+$dinner;}
		$totalcc=$totalcc+$dinner;
	}
	echo "<td align=right><b>$totalcc</b></td></tr>";

	echo "<tr bgcolor='#CCCCCC'><td><b>Total</b></td><td></td>";
	$checkaverage=0;
	$totalcheckavg=0;
	for ($counter=0;$counter<=6;$counter++){
		if ($counter==0){echo "<td align=right>$row1</td>";$day1cc=$row1;}
		elseif ($counter==1){echo "<td align=right>$row2</td>";$day2cc=$row2;}
		elseif ($counter==2){echo "<td align=right>$row3</td>";$day3cc=$row3;}
		elseif ($counter==3){echo "<td align=right>$row4</td>";$day4cc=$row4;}
		elseif ($counter==4){echo "<td align=right>$row5</td>";$day5cc=$row5;}
		elseif ($counter==5){echo "<td align=right>$row6</td>";$day6cc=$row6;}
		elseif ($counter==6){echo "<td align=right>$row7</td>";$day7cc=$row7;}
	}
	$rowtotal=$row1+$row2+$row3+$row4+$row5+$row6+$row7;
	echo "<td align=right><b>$rowtotal</b></td></tr>";

	echo "<tr bgcolor='#CCCCCC'><td><b>Check Average</b></td><td></td>";
	$daycount = 0;
	$checkaverage=0;
	$totalcheckavg=0;
	for ($counter=0;$counter<=6;$counter++){
		if ($counter==0){if ($day1sales!=0 && $row1!=0){$checkaverage=$day1sales/$row1;} else {$checkaverage=0;}}
		elseif ($counter==1){if ($day2sales!=0 && $row2!=0){$checkaverage=$day2sales/$row2;} else {$checkaverage=0;}}
		elseif ($counter==2){if ($day3sales!=0 && $row3!=0){$checkaverage=$day3sales/$row3;} else {$checkaverage=0;}}
		elseif ($counter==3){if ($day4sales!=0 && $row4!=0){$checkaverage=$day4sales/$row4;} else {$checkaverage=0;}}
		elseif ($counter==4){if ($day5sales!=0 && $row5!=0){$checkaverage=$day5sales/$row5;} else {$checkaverage=0;}}
		elseif ($counter==5){if ($day6sales!=0 && $row6!=0){$checkaverage=$day6sales/$row6;} else {$checkaverage=0;}}
		elseif ($counter==6){if ($day7sales!=0 && $row7!=0){$checkaverage=$day7sales/$row7;} else {$checkaverage=0;}}
		$checkaverage=money($checkaverage);
		echo "<td align=right>$$checkaverage</td>";
		$totalcheckavg=$totalcheckavg+$checkaverage;
		if ($checkaverage!=0){$daycount++;}
	}
	if ($totalcheckavg!=0) {
		//$totalcheckavg=money($totalcheckavg/$daycount);
		$totalcheckavg = number_format(($day1sales
										+ $day2sales
										+ $day3sales
										+ $day4sales
										+ $day5sales
										+ $day6sales
										+ $day7sales)
										/
										($row1
										+ $row2
										+ $row3
										+ $row4
										+ $row5
										+ $row6
										+$row7), 2);
	}
	else {$totalcheckavg=0;}
	echo "<td align=right><b>$$totalcheckavg</b></td></tr>";

	/////collect reconciliation
	if($categoryid == 4){
		echo "<tr bgcolor='#CCCCCC'><td><b>Cash Collections</b></td><td></td>";
		$tempdate = $day1;
		$total_collects = 0;
		while($tempdate <= $day7){
			$collects = Kiosk::collect_amount($businessid,$tempdate);
			if($collects != 0){
				echo "<td align=right>$$collects</td>";
				$total_collects+=$collects;
			}
			else{echo "<td>&nbsp;</td>";}
			$tempdate = nextday($tempdate);
		}
		//$total_collects = number_format($total_collects,2);
		//echo "<td align=right><b>$$total_collects</b></td></tr>";
		////cash meter
		$tempdate = date("Y-m-d");
		$tempdate2 = date("Y-m-d H:i:s");
		$collects = Kiosk::collect_amount($businessid,$tempdate,$tempdate2);
		echo "<td align=right style=\"border:1px solid #00FF00;text-align:center;font-weight:bold;\" title='Current Cash Meter'><font color=#555555>$$collects</font>&nbsp;</td></tr>";
	}
	////end collects

	echo "<tr><td>Catering Count</td><td></td>";
	$totalcatering=0;
	$daycount=0;
	for ($counter=0;$counter<=6;$counter++){
		if ($counter==0){$day=$day1;}
		elseif ($counter==1){$day=$day2;}
		elseif ($counter==2){$day=$day3;}
		elseif ($counter==3){$day=$day4;}
		elseif ($counter==4){$day=$day5;}
		elseif ($counter==5){$day=$day6;}
		elseif ($counter==6){$day=$day7;}

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query2 = "SELECT people FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND (status = '2' OR status = '4' OR status = '6')";
		$result2 = Treat_DB_ProxyOld::query( $query2, true );
		$num2 = Treat_DB_ProxyOld::mysql_numrows( $result2, 0 );
		//mysql_close();

		$num2--;
		$cateringcount=0;
		while ($num2>=0){
			$people = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'people', '' );
			$cateringcount=$cateringcount+$people;
			$num2--;
		}

		echo "<td align=right>$cateringcount</td>";
		if ($counter==0){$day1ctc=$cateringcount;}
		elseif ($counter==1){$day2ctc=$cateringcount;}
		elseif ($counter==2){$day3ctc=$cateringcount;}
		elseif ($counter==3){$day4ctc=$cateringcount;}
		elseif ($counter==4){$day5ctc=$cateringcount;}
		elseif ($counter==5){$day6ctc=$cateringcount;}
		elseif ($counter==6){$day7ctc=$cateringcount;}
		$totalcatering=$totalcatering+$cateringcount;
	}
	echo "<td align=right><b>$totalcatering</b></td></tr>";

	//$curarrow=$curarrow-7000+1;

	echo "<tr bgcolor=yellow><td colspan=2>Labor Hours</td>";
	$totalcc=0;
	for ($counter=0;$counter<=6;$counter++){
		if ($counter==0){$day=$day1;$tabcount=100;}
		elseif ($counter==1){$day=$day2;$tabcount=111;}
		elseif ($counter==2){$day=$day3;$tabcount=122;}
		elseif ($counter==3){$day=$day4;$tabcount=133;}
		elseif ($counter==4){$day=$day5;$tabcount=144;}
		elseif ($counter==5){$day=$day6;$tabcount=155;}
		elseif ($counter==6){$day=$day7;$tabcount=166;}

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query77 = "SELECT * FROM submit WHERE businessid = '$businessid' AND date = '$day'";
		$result77 = Treat_DB_ProxyOld::query( $query77, true );
		$num77 = Treat_DB_ProxyOld::mysql_numrows( $result77, 0 );
		//mysql_close();

		if ($num77!=0){$can_edit=1;}
		else {$can_edit=0;}

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query2 = "SELECT * FROM stats WHERE businessid = '$businessid' AND date = '$day'";
		$result2 = Treat_DB_ProxyOld::query( $query2, true );
		$num2 = Treat_DB_ProxyOld::mysql_numrows( $result2, 0 );
		//mysql_close();

		$labor_hour = '';
		if ($num2!=0){
			$labor_hour = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'labor_hour', '' );
		}
		if ($can_edit==1 && $security_level<8){echo "<td align=right>$labor_hour<INPUT TYPE=hidden name='labor_hour$day' value='$labor_hour'></td>";}
		else {
			if ($labor_hour==0){$labor_hour="";}
			echo "<td align=right><INPUT TYPE=text SIZE=5 name='labor_hour$day' value='$labor_hour' tabindex='$tabcount' DISABLED></td>";
			//$curarrow=$curarrow+1000;
		}
		$totalcc=money($totalcc+$labor_hour);
	}
	echo "<td align=right><b>$totalcc</b></td></tr>";

	$curarrow=$curarrow-7000+1;
/*
	echo "<tr bgcolor=yellow><td colspan=2>Daily Labor $</td>";
	$totalcc=0;
	for ($counter=0;$counter<=6;$counter++){
		if ($counter==0){$day=$day1;$tabcount=100;}
		elseif ($counter==1){$day=$day2;$tabcount=111;}
		elseif ($counter==2){$day=$day3;$tabcount=122;}
		elseif ($counter==3){$day=$day4;$tabcount=133;}
		elseif ($counter==4){$day=$day5;$tabcount=144;}
		elseif ($counter==5){$day=$day6;$tabcount=155;}
		elseif ($counter==6){$day=$day7;$tabcount=166;}

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query77 = "SELECT * FROM submit WHERE businessid = '$businessid' AND date = '$day'";
		$result77 = Treat_DB_ProxyOld::query( $query77, true );
		$num77 = Treat_DB_ProxyOld::mysql_numrows( $result77, 0 );
		//mysql_close();

		if ($num77!=0){$can_edit=1;}
		else {$can_edit=0;}

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query2 = "SELECT * FROM stats WHERE companyid = '$companyid' AND businessid = '$businessid' AND date = '$day'";
		$result2 = Treat_DB_ProxyOld::query( $query2, true );
		$num2 = Treat_DB_ProxyOld::mysql_numrows( $result2, 0 );
		//mysql_close();

		if ($num2!=0){
			$cccurent = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'number', '' );
			$lunch = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'lunch', '' );
			$dinner = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'dinner', '' );
			$labor = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'labor', '' );
			$void = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'void', '' );
		}
		$labor=money($labor);
		if ($can_edit==1 && $security_level<8){echo "<td align=right>$$labor<INPUT TYPE=hidden name='labor$day' value='$labor'></td>";}
		else {
			if ($labor==0){$labor="";}
			echo "<td align=right><INPUT TYPE=text SIZE=5 name='labor$day' value='$labor' tabindex='$tabcount' onkeydown='return checkKey()' IndexId=$curarrow></td>";
			$curarrow=$curarrow+1000;
		}
		$totalcc=money($totalcc+$labor);
	}
	echo "<td align=right><b>$$totalcc</b></td></tr>";

	$curarrow=$curarrow-7000+1;
*/
	echo "<tr><td colspan=2>Voids/Overrings</td>";
	$totalcc=0;
	for ($counter=0;$counter<=6;$counter++){
		if ($counter==0){$day=$day1;$tabcount=100;}
		elseif ($counter==1){$day=$day2;$tabcount=111;}
		elseif ($counter==2){$day=$day3;$tabcount=122;}
		elseif ($counter==3){$day=$day4;$tabcount=133;}
		elseif ($counter==4){$day=$day5;$tabcount=144;}
		elseif ($counter==5){$day=$day6;$tabcount=155;}
		elseif ($counter==6){$day=$day7;$tabcount=166;}

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query77 = "SELECT * FROM submit WHERE businessid = '$businessid' AND date = '$day'";
		$result77 = Treat_DB_ProxyOld::query( $query77, true );
		$num77 = Treat_DB_ProxyOld::mysql_numrows( $result77, 0 );
		//mysql_close();

		if ($num77!=0){$can_edit=1;}
		else {$can_edit=0;}

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query2 = "SELECT * FROM stats WHERE businessid = '$businessid' AND date = '$day'";
		$result2 = Treat_DB_ProxyOld::query( $query2, true );
		$num2 = Treat_DB_ProxyOld::mysql_numrows( $result2, 0 );
		//mysql_close();

		if ($num2!=0){
			$cccurent = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'number', '' );
			$lunch = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'lunch', '' );
			$dinner = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'dinner', '' );
			$dailylabor[$day] = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'labor', '' );
			$void = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'void', '' );
		}
		$void=money($void);
		if ($can_edit==1 && $security_level<8){echo "<td align=right>$$void<INPUT TYPE=hidden name='void$day' value='$void'></td>";}
		else {
			if ($void==0){$void="";}
			echo "<td align=right><INPUT TYPE=text SIZE=5 name='void$day' value='$void' tabindex='$tabcount' onkeydown='return checkKey()' IndexId=$curarrow></td>";
			$curarrow=$curarrow+1000;
		}
		$totalcc=money($totalcc+$void);
	}
	echo "<td align=right><b>$$totalcc</b></td></tr>";

	/////////////////////////////////
	/////////////////COMMISSARY STATS
	/////////////////////////////////
	if($contract==2){
		echo "<tr><td colspan=10 bgcolor=#CCCCCC><b>Commissary Stats</b></td></tr>";

		$query2 = "SELECT * FROM menu_groups WHERE menu_typeid = '$default_menu' ORDER BY orderid";
		$result2 = Treat_DB_ProxyOld::query( $query2 );

		while($r=Treat_DB_ProxyOld::mysql_fetch_array($result2)){
			$menu_group_id=$r["menu_group_id"];
			$groupname=$r["groupname"];

			echo "<tr><td colspan=2>$groupname</td>";

			$day=$day1;
			$weektotal=0;
			while($day<=$day7){

				$query3 = "SELECT SUM(vend_order.amount) AS totalamt FROM vend_order,vend_item,menu_pricegroup WHERE vend_order.vend_itemid = vend_item.vend_itemid AND vend_item.date = '$day' AND vend_item.groupid = menu_pricegroup.menu_pricegroupid AND menu_pricegroup.menu_groupid = '$menu_group_id'";
				$result3 = Treat_DB_ProxyOld::query( $query3 );

				$totalamt = round(Treat_DB_ProxyOld::mysql_result( $result3, 0, 'totalamt' ), 0);
				$weektotal+=$totalamt;
				$grouptotal[$day]+=$totalamt;

				echo "<td align=right>$totalamt</td>";

				$day=nextday($day);
			}
			echo "<td align=right><b>$weektotal</b></td></tr>";
		}

		echo "<tr bgcolor=yellow><td colspan=2>Labor $'s/Total Items</td>";
		$day=$day1;
		while($day<=$day7){
			$weektotaldollars+=$dailylabor[$day];
			$weektotalgroup+=$grouptotal[$day];
			$showdollars=number_format($dailylabor[$day]/$grouptotal[$day],2);
			echo "<td align=right>$showdollars</td>";
			$day=nextday($day);
		}
		$showdollars=number_format($weektotaldollars/$weektotalgroup,2);
		echo "<td align=right><b>$showdollars</b></td></tr>";
	}
	///////////////////////
	//////kiosk stats//////
	///////////////////////
	$query2 = "SELECT kiosk_cash,kiosk_onlinecc,kiosk_giftcard FROM vend_settings WHERE businessid = $businessid";
	$result2 = Treat_DB_ProxyOld::query( $query2 );

	$kiosk_cash = Treat_DB_ProxyOld::mysql_result( $result2, 0, "kiosk_cash", '' );
	$kiosk_onlinecc = Treat_DB_ProxyOld::mysql_result( $result2, 0, "kiosk_onlinecc", '' );
	$kiosk_giftcard = Treat_DB_ProxyOld::mysql_result( $result2, 0, "kiosk_giftcard", '' );

	if($kiosk_cash > 0){
		echo "<tr><td colspan=2>Prepaid Balance</td>";

		$k_today = date("Y-m-d");
		$day=$day1;
		while($day<=$day7){
			$query2 = "SELECT SUM(amount) AS tot_cash FROM debitdetail WHERE debitid = $kiosk_cash AND date <= '$day'";
			$result2 = Treat_DB_ProxyOld::query( $query2, true );

			$tot_cash = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'tot_cash', '' );

			$query2 = "SELECT SUM(amount) AS tot_onlinecc FROM debitdetail WHERE debitid = $kiosk_onlinecc AND date <= '$day'";
			$result2 = Treat_DB_ProxyOld::query( $query2, true );

			$tot_onlinecc = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'tot_onlinecc', '' );

			$query2 = "SELECT SUM(amount) AS tot_giftcard FROM debitdetail WHERE debitid = $kiosk_giftcard AND date <= '$day'";
			$result2 = Treat_DB_ProxyOld::query( $query2, true );

			$tot_giftcard = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'tot_giftcard', '' );

			if($day<=$k_today){
				$balance = number_format($tot_cash + $tot_onlinecc - $tot_giftcard,2);
			}
			else{
				$balance = "&nbsp;";
			}

			echo "<td align=right>$balance</td>";

			////week balance
			if($day==$day1 && $day <= $k_today){$start_balance  = $tot_cash + $tot_onlinecc - $tot_giftcard;}
			if($day == $k_today || ($day == $day7 && $day7 <= $k_today)){$end_balance  = $tot_cash + $tot_onlinecc - $tot_giftcard;}

			$day = nextday($day);
		}

		///balance change
		$show_balance = $end_balance - $start_balance;
		if($show_balance > 0){$show_balance = number_format($show_balance,2);$show_balance = "<font color=green><b>+$show_balance</b></font>";}
		elseif($show_balance < 0){$show_balance = number_format($show_balance,2);$show_balance = "<font color=red><b>-$show_balance</b></font>";}
		else{$show_balance = number_format($show_balance,2);}

		echo "<td align=right>$show_balance</td></tr>";
	}

	/////////////////////  END STATS/Start Closings////////////////////////////////////////////////////////

	echo "<tr><td colspan=10 height=10></td></tr>";
	echo "<tr bgcolor='#CCCC99'><td colspan=2 title='Check to close for holidays and other days your unit is closed during normal operating days'><b>Closed Operating Days</b></td>";
	$totalcc=0;
	for ($counter=0;$counter<=6;$counter++){
		if ($counter==0){$day=$day1;}
		elseif ($counter==1){$day=$day2;}
		elseif ($counter==2){$day=$day3;}
		elseif ($counter==3){$day=$day4;}
		elseif ($counter==4){$day=$day5;}
		elseif ($counter==5){$day=$day6;}
		elseif ($counter==6){$day=$day7;}

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query77 = "SELECT * FROM submit WHERE businessid = '$businessid' AND date = '$day'";
		$result77 = Treat_DB_ProxyOld::query( $query77, true );
		$num77 = Treat_DB_ProxyOld::mysql_numrows( $result77, 0 );
		//mysql_close();

		if ($num77!=0){$can_edit=1;}
		else {$can_edit=0;}

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query2 = "SELECT * FROM stats WHERE businessid = '$businessid' AND date = '$day'";
		$result2 = Treat_DB_ProxyOld::query( $query2, true );
		$num2 = Treat_DB_ProxyOld::mysql_numrows( $result2, 0 );
		//mysql_close();

		$closed = 0;
		$on = '';
		if ($num2!=0) {
			$closed = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'closed', '' );
			if ( $closed == 1 )
			{
				$on="on";
			}
		}

		if ($can_edit==1 && $security_level<8 && $closed==1){echo "<td><a href=reason.php?bid=$businessid&cid=$companyid&date=$day target = '_blank' style=$style width=300 height=200><img src=off.gif height=15 width=15 border=0></a><INPUT TYPE=hidden name='closed$day' value='$on'></td>";}
		elseif ($can_edit==1 && $security_level<8 && $closed==0){echo "<td><a href=reason.php?bid=$businessid&cid=$companyid&date=$day target = '_blank' style=$style width=300 height=200><img src=on.gif height=15 width=15 border=0></a><INPUT TYPE=hidden name='closed$day' value='$on'></td>";}
		else {
			if ($closed==1){$showcheck="CHECKED";}
			else {$showcheck="";}
			if($operate==5&&$security_level<9&&(dayofweek($day)=="Saturday"||dayofweek($day)=="Sunday")){$is_disabled="DISABLED";}
			elseif($operate==6&&dayofweek($day)=="Sunday"){$is_disabled="DISABLED";}
			else{$is_disabled="";}
			echo "<td><INPUT TYPE=checkbox name='closed$day' $showcheck $is_disabled> <a href=reason.php?bid=$businessid&cid=$companyid&date=$day target = '_blank' style=$style width=300 height=200><font size=2>[Memo]</font></a></td>";
		}
	}
	echo "<td align=right></td></tr>";

	///////////////////////END Closing/////////////////////////////////////////////////////////////////////
	$day8=prevday($day1);
	if ($audit==1){$showaudit=0;}
	else {$showaudit=1;}
?>
	
				<tr height="2">
					<td colspan=10 bgcolor=black>
						
					</td>
				</tr>
				<tr>
					<td colspan=3>
						<input type="hidden" name="nextperiod" value="<?php echo $nextperiod; ?>">
						<input type="hidden" name="day1" value="<?php echo $day1; ?>">
						<input type="hidden" name="can_edit" value="<?php echo $can_edit; ?>">
						<input type="hidden" name="prevperiod" value="<?php echo $prevperiod; ?>">
	<?php if ( $sec_bus_sales >= 2 ) : ?>
						<input type=submit value=" Save ">
	<?php endif; ?>
						<a name='audit'></a><a href="busdetail.php?bid=<?php echo $businessid; ?>&cid=<?php echo $companyid; ?>&prevperiod=<?php echo $day1; ?>&audit=<?php echo $showaudit; ?>#audit">
							<img src='details.gif' border=0 alt='Show/Hide'>
							<font color=blue>Audit Trail</font>
						</a>
					</form>
					</td>
					<td align=right colspan=7>
		<?php if ( $sec_bus_sales >= 2 ) : ?>
						<form action="submit.php" method="POST" onclick="return submit2()">
							<input type="hidden" name=username value="<?php echo $user; ?>">
							<input type="hidden" name=password value="<?php echo $pass; ?>">
							<input type="hidden" name=businessid value="<?php echo $businessid; ?>">
							<input type="hidden" name=companyid value="<?php echo $companyid; ?>">
							<input type="hidden" name=diff value="<?php echo $diff; ?>">
							<input type="hidden" name=day1 value="<?php echo $day1; ?>">
							<input type="hidden" name=day7 value="<?php echo $day7; ?>">
							<input type="hidden" name=prevperiod value="<?php echo $prevperiod; ?>">
							<input type="hidden" name=nextperiod value="<?php echo $nextperiod; ?>">
							<input type="hidden" name=day1cc value="<?php echo $day1cc; ?>">
							<input type="hidden" name=day2cc value="<?php echo $day2cc; ?>">
							<input type="hidden" name=day3cc value="<?php echo $day3cc; ?>">
							<input type="hidden" name=day4cc value="<?php echo $day4cc; ?>">
							<input type="hidden" name=day5cc value="<?php echo $day5cc; ?>">
							<input type="hidden" name=day6cc value="<?php echo $day6cc; ?>">
							<input type="hidden" name=day7cc value="<?php echo $day7cc; ?>">
							<input type="hidden" name=day1column value="<?php echo $day1column; ?>">
							<input type="hidden" name=day2column value="<?php echo $day2column; ?>">
							<input type="hidden" name=day3column value="<?php echo $day3column; ?>">
							<input type="hidden" name=day4column value="<?php echo $day4column; ?>">
							<input type="hidden" name=day5column value="<?php echo $day5column; ?>">
							<input type="hidden" name=day6column value="<?php echo $day6column; ?>">
							<input type="hidden" name=day7column value="<?php echo $day7column; ?>">
		<?php endif; ?>
<?php
	if ( $problem == 1 )
	{
		$showproblem="<b><font color=red>$problem_date OUT OF BALANCE, PLEASE SAVE TO FIX</font></b>";
	}

	if ( $can_edit == 1 )
	{
		echo $showexport;
	}
	else
	{
		echo $showproblem;
		if ( $sec_bus_sales >= 2 )
		{
			echo "<INPUT TYPE=submit VALUE=' Submit Current Sales ' $problem_submit>";
		}
	}
?>
		<?php if ( $sec_bus_sales >= 2 ) : ?>
						</form>
		<?php endif; ?>
					</td>
				</tr>
<?php
	//echo "<tr height=2><td colspan=10 bgcolor=black></td></tr>";

	if ($audit==1){
		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query2 = "SELECT * FROM audit_sales WHERE  businessid = '$businessid' AND date = '$day1'";
		$result2 = Treat_DB_ProxyOld::query( $query2, true );
		$num2 = Treat_DB_ProxyOld::mysql_numrows( $result2, 0 );
		//mysql_close();

		$num2--;
		echo "<tr><td colspan=10><b>Audit Trail</b></td></tr>";
		while ($num2>=0){

			$showuser = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'user', '' );
			$showtime = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'time', '' );
			$wassubmit = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'submit', '' );
			if ($wassubmit==1){echo "<tr><td colspan=10><font color=red>Submitted by $showuser on $showtime.</font></td></tr>";}
			else {echo "<tr><td colspan=10>Saved by $showuser on $showtime.</td></tr>";}
			$num2--;
		}
		//echo "<tr height=2><td colspan=10 bgcolor=black></td></tr>";
	}

	echo "</table></center>";
	//mysql_close();
	//////END TABLE////////////////////////////////////////////////////////////////////////////////////

	echo "<br><FORM ACTION=businesstrack.php method=post>";
	echo "<input type=hidden name=username value=$user>";
	echo "<input type=hidden name=password value=$pass>";
	echo "<center><INPUT TYPE=submit VALUE=' Return to Main Page'></FORM></center><DIV ID=testdiv1 STYLE=position:absolute;visibility:hidden;background-color:white;layer-background-color:white;></DIV></body>";

	//google_page_track();
}
?>
