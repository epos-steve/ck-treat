<?php

////////Import Vending KPI's
ini_set('max_execution_time','1800');

define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');
$style = "text-decoration:none";

/////Indy
$companyid=3;
$businessid=97;

$myFile2 = "1101KPI-KC-071010-090310.csv";

if(file_exists($myFile2)){

$handle = fopen($myFile2, 'r');

echo "Open $myFile2<br>";

mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");

while (!feof($handle))
{
   $pieces=array();

   ////////////////////////////////////
   /////////PARSE FILE/////////////////
   ////////////////////////////////////
   $pieces = fgets($handle,512);

   $expr="/,(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))/";
   $pieces = preg_split($expr,trim($pieces));
   $pieces = preg_replace("/^\"(.*)\"$/","$1",$pieces);

   ////////////////////////////////////
   ////////////FORMAT DATE (YYYY-MM-DD)
   ////////////////////////////////////
   $date=$pieces[14];

   if($date==""){$date=$lastdate;}
   else{

      if(strlen($date)<6){$date="0$date";}
      $day=substr($date,2,2);
      $month=substr($date,0,2);
      $year=substr($date,4,2);

      $newdate="20";
      $newdate.="$year-$month-$day";
   }

   $lastdate=$date;
   ////////////////////////////////////
   ////////////FORMAT INSTALL DATE (YYYY-MM-DD)
   ////////////////////////////////////
   $install_date=$pieces[3];

   if(strlen($install_date)<6){$install_date="0$install_date";}
   $day=substr($install_date,2,2);
   $month=substr($install_date,0,2);
   $year=substr($install_date,4,2);

   if($year>=80){$new_install_date="19";}
   else{$new_install_date="20";}
   $new_install_date.="$year-$month-$day";

   ////////////////////////////////////
   ////////////FIND/CREATE VGROUP
   ////////////////////////////////////
   $query = "SELECT * FROM vend_machine_vgroup WHERE vgroup_num LIKE '$pieces[17]' AND businessid = '$businessid'";
   $result = mysql_query($query);
   $num=mysql_numrows($result);

   if($num>0){
      $vgroup=@mysql_result($result,0,"vgroupid");

	  echo "<font size=1>Found VGroup</font><br>";
   }
   else{
      $query = "INSERT INTO vend_machine_vgroup (vgroup_num,vgroup_name,businessid) VALUES ('$pieces[17]','$pieces[18]','$businessid')";
      $result = mysql_query($query);

      $vgroup=mysql_insert_id();

	  echo "<font size=1>New VGroup</font><br>";
   }

   ////////////////////////////////////
   ////////////FIND/CREATE SALESMAN
   ////////////////////////////////////
   $query = "SELECT * FROM vend_salesman WHERE sales_name LIKE '$pieces[9]' AND businessid = '$businessid'";
   $result = mysql_query($query);
   $num=mysql_numrows($result);

   if($num>0){
      $salesman=@mysql_result($result,0,"salesid");

	  echo "<font size=1>Found Salesman</font><br>";
   }
   else{
      $query = "INSERT INTO vend_salesman (sales_name,businessid) VALUES ('$pieces[9]','$businessid')";
      $result = mysql_query($query);

      $salesman=mysql_insert_id();

	  echo "<font size=1>New Salesman</font><br>";
   }

   ////////////////////////////////////
   ////////////FIND/CREATE ACCOUNT
   ////////////////////////////////////
   $query = "SELECT * FROM accounts WHERE accountnum LIKE '$pieces[2]' AND businessid = '$businessid'";
   $result = mysql_query($query);
   $num=mysql_numrows($result);

   if($num>0){
      $accountid=@mysql_result($result,0,"accountid");

	  echo "<font size=1>Found Account</font><br>";
   }
   else{
      $pieces[12]=str_replace('"','',$pieces[12]);
      $address=explode(",",$pieces[12]);
	  $city=$address[0];

	  $address[1]=trim($address[1]);
	  $address2=explode(" ",$address[1]);
	  $state=$address2[0];
	  if($address2[1]!=""){$zip=$address2[1];}
	  else{$zip=$address2[2];}

      $query = "INSERT INTO accounts (businessid,companyid,name,accountnum,attn,street,city,state,zip,salesman) VALUES ('$businessid','$companyid','$pieces[10]','$pieces[2]','$pieces[13]','$pieces[11]','$city','$state','$zip','$salesman')";
      $result = mysql_query($query);

      $accountid=mysql_insert_id();

	  echo "<font size=1>New Account</font><br>";
   }

   ////////////////////////////////////
   ////////////FIND/CREATE CHANNEL/////
   ////////////////////////////////////
   $query = "SELECT * FROM vend_channel WHERE channel_name LIKE '$pieces[7]'";
   $result = mysql_query($query);
   $num=mysql_numrows($result);

   if($num>0){
      $channel=@mysql_result($result,0,"channelid");

	  echo "<font size=1>Found Channel</font><br>";
   }
   else{
      $query = "INSERT INTO vend_channel (channel_name,businessid) VALUES ('$pieces[7]','$businessid')";
      $result = mysql_query($query);

      $channel=mysql_insert_id();

	  echo "<font size=1>New Channel</font><br>";
   }

   ////////////////////////////////////
   ////////////FIND/CREATE FAMILY
   ////////////////////////////////////
   $query = "SELECT * FROM vend_family WHERE family_name LIKE '$pieces[8]' AND businessid = '$businessid'";
   $result = mysql_query($query);
   $num=mysql_numrows($result);

   if($num>0){
      $family=@mysql_result($result,0,"familyid");

	  echo "<font size=1>Found Family</font><br>";
   }
   else{
      $query = "INSERT INTO vend_family (family_name,businessid) VALUES ('$pieces[8]','$businessid')";
      $result = mysql_query($query);

      $family=mysql_insert_id();

	  echo "<font size=1>New Family</font><br>";
   }

   ////////////////////////////////////
   ///////////INSERT RECORD INTO vend_machine_sales
   ////////////////////////////////////

   ////////CHANGE ROUTE NUMBERS FROM MEI
   if($businessid==97||$businessid==106){
                //kc
		if (strpos($pieces[4], "Rt") !== false){
			$pieces[4]=str_replace("Rt ","",$pieces[4]);
			while(strlen($pieces[4])<3){$pieces[4]="0$pieces[4]";}
		}
                //omaha
                elseif (strpos($pieces[4], "RT") !== false){
			$pieces[4]=str_replace("RT ","",$pieces[4]);
			while(strlen($pieces[4])<3){$pieces[4]="0$pieces[4]";}
			$pieces[4]="20$pieces[4]";
		}
                //des moines
		elseif (strpos($pieces[4], "DM") !== false){
			$pieces[4]=str_replace("DM","",$pieces[4]);
			while(strlen($pieces[4])<3){$pieces[4]="0$pieces[4]";}
			$pieces[4]="40$pieces[4]";
		}
   }

   $query = "INSERT INTO vend_machine_sales (machineid,businessid,date,accountid,install_date,route,has_vend,has_coffee,channel,family,salesman,fills,collects,vgroup,waste,refunds,over_short) VALUES ('$pieces[1]','$businessid','$newdate','$accountid','$new_install_date','$pieces[4]','$pieces[5]','$pieces[6]','$channel','$family','$salesman','$pieces[15]','$pieces[16]','$vgroup','$pieces[19]','$pieces[20]','$pieces[21]')";
   $result = mysql_query($query);

   echo "<font size=2>$query</font><br>";
}
}
else{echo "Bad File<p>";}
mysql_close();
fclose($handle);
echo "Done";
?>