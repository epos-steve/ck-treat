<?php

define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');

function money($diff){
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+4);
        $diff=round($diff, 2);
        if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
        elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
        $diff = substr($diff, 0, $dot+3);
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        return $diff;
}

function nextday($nextd,$day_format=''){ //Function returns next day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monn = substr($nextd, 5, 2);
   $dayn = substr($nextd, 8, 2);
   $yearn = substr($nextd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monn, $dayn+1, $yearn));
   return $tempdate;
}

function prevday($prevd,$day_format=''){ //Function returns previous day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monp = substr($prevd, 5, 2);
   $dayp = substr($prevd, 8, 2);
   $yearp = substr($prevd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monp, $dayp-1, $yearp));
   return $tempdate;
}

function dayofweek($date1){
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}

if ( !defined('DOC_ROOT') )
{
	define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
}
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');

///////VARIABLES
$companyid = isset($_GET["cid"])?$_GET["cid"]:'';
$mysecurity=1;

///////mysql connect
mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");

$today=date("Y-m-d");
$tomorrow=nextday($today);
$tomorrow=nextday($tomorrow);

////////COMPANY LOOP
$query93 = "SELECT companyid,week_end,payroll_date,reference FROM company WHERE companyid != '2'";
$result93 = mysql_query($query93);

while($r3=mysql_fetch_array($result93)){
	$companyid=$r3["companyid"];
	$payroll_date=$r3["payroll_date"];
	$week_end=$r3["week_end"];
	$company_name=$r3["reference"];

	$data="";
	$send=0;

	//$today=prevday($today);

	////////IF THIS IS A WEEKEND {RUN}
    if(dayofweek($today)==$week_end){
		echo "This is end of week for $company_name...<p>";

		$query = "SELECT labor_move.id,labor_move.loginid,labor_move.to_business FROM labor_move,business WHERE business.companyid = '$companyid' AND business.businessid = labor_move.to_business AND labor_move.move_date = '0000-00-00 00:00:00'";
		$result = mysql_query($query);

		while($r=mysql_fetch_array($result)){
			$id=$r["id"];
			$loginid=$r["loginid"];
			$to_business=$r["to_business"];

			///////create data for email
			$send=1;
			$query2 = "SELECT firstname,lastname,empl_no,oo2 FROM login WHERE loginid = '$loginid'";
			$result2 = mysql_query($query2);

			$firstname=@mysql_result($result2,0,"firstname");
			$lastname=@mysql_result($result2,0,"lastname");
			$empl_no=@mysql_result($result2,0,"empl_no");
			$oo2=@mysql_result($result2,0,"oo2");

			$query2 = "SELECT businessname FROM business WHERE businessid = '$oo2'";
			$result2 = mysql_query($query2);

			$frombus=@mysql_result($result2,0,"businessname");

			$query2 = "SELECT businessname FROM business WHERE businessid = '$to_business'";
			$result2 = mysql_query($query2);

			$tobus=@mysql_result($result2,0,"businessname");

			$data.="[$empl_no] $firstname $lastname moved from $frombus to $tobus. <br>";
			///////end data

			$query2 = "UPDATE login SET oo2 = '$to_business', move_date = '$tomorrow' WHERE loginid = '$loginid'";
			$result2 = mysql_query($query2);

			$query2 = "UPDATE labor_move SET move_date = now() WHERE id = '$id'";
			$result2 = mysql_query($query2);
		}

		if($send==1){
			$email="smartin@essentialpos.com.test-google-a.com,jang@treatamerica.com";
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= "From: support@treatamerica.com\r\n";
			$subject = "$company_name Employee Transfers";
			mail($email, $subject, $data, $headers);
			echo "Email sent<p>";
		}
	}
}
mysql_close();


echo "Done.";
?>