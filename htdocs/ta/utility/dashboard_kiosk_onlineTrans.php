<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');

$yest = @$_GET["yest"];

$date = date("Y-m-d");
if($yest == 1){
	$date = date("Y-m-d",mktime(0, 0, 0, date("m") , date("d")-1, date("Y")));
}

$payments = array(1 => 'Online CC Reload', 3 => 'Refund', 4 => 'Promo');

$charges = array();

////online Trans
$query = "select ROUND(sum(ct.chargetotal) / IF(COUNT( DISTINCT cp.client_programid) > 1, COUNT( DISTINCT cp.client_programid), 1), 2) AS amount,
				c.businessid, 
				ct.trans_type_id, 
				b.companyid, 
				mcct.creditid,
				mcct.debitid,
				b.businessname
			FROM customer_transactions ct
			JOIN customer c ON c.customerid = ct.customer_id
			JOIN business b ON b.businessid = c.businessid
			LEFT JOIN mburris_manage.client_programs cp ON cp.businessid = b.businessid 
				AND cp.db_name = '".\EE\Config::singleton()->get('db', 'database')."'
			LEFT JOIN mburris_manage.mapping_cc_transactions mcct ON mcct.client_programid = cp.client_programid 
				AND mcct.type = IF(ct.trans_type_id = 1, 0, IF( ct.trans_type_id = 3, 1, IF( ct.trans_type_id = 4, 2, -1)))
			WHERE DATE(ct.date_created) = '$date'
			AND ct.response = 'APPROVED'
			GROUP BY c.businessid, ct.trans_type_id
			ORDER BY c.businessid, ct.trans_type_id";
$result = Treat_DB_ProxyOldProcessHost::query( $query );
    
while($r = mysql_fetch_array($result)){
	$businessid = $r["businessid"];
	$trans_type_id = $r["trans_type_id"];

	$charges["$businessid:$trans_type_id"]["amount"] = $r["amount"];
	$charges["$businessid:$trans_type_id"]["businessid"] = $r["businessid"];
	$charges["$businessid:$trans_type_id"]["companyid"] = $r["companyid"];
	$charges["$businessid:$trans_type_id"]["trans_type_id"] = $r["trans_type_id"];
	$charges["$businessid:$trans_type_id"]["creditid"] = $r["creditid"];
	$charges["$businessid:$trans_type_id"]["debitid"] = $r["debitid"];
	$charges["$businessid:$trans_type_id"]["businessname"] = $r["businessname"];
}


////charges not in home unit
$query = "SELECT ROUND( SUM( ct.chargetotal ) / IF( COUNT( DISTINCT cp.client_programid ) >1, COUNT( DISTINCT cp.client_programid ) , 1 ) , 2 ) AS amount, 
				b.businessid, 
				ct.trans_type_id, 
				b.companyid, 
				mcct.creditid, 
				mcct.debitid, 
				b.businessname, 
				c.businessid AS homeBusiness
			FROM customer_transactions ct
			JOIN customer_transactions_business ctb ON ctb.ct_id = ct.id
			JOIN business b ON b.businessid = ctb.businessid
			JOIN customer c ON c.customerid = ct.customer_id
			LEFT JOIN mburris_manage.client_programs cp ON cp.businessid = b.businessid
				AND cp.db_name =  '".\EE\Config::singleton()->get('db', 'database')."'
			LEFT JOIN mburris_manage.mapping_cc_transactions mcct ON mcct.client_programid = cp.client_programid
				AND mcct.type = IF( ct.trans_type_id =1, 0, IF( ct.trans_type_id =3, 1, IF( ct.trans_type_id =4, 2, -1 ) ) ) 
			WHERE DATE( ct.date_created ) =  '$date'
				AND ct.response =  'APPROVED'
			GROUP BY b.businessid, ct.trans_type_id
			ORDER BY b.businessid, ct.trans_type_id";
$result = Treat_DB_ProxyOldProcessHost::query( $query );
    
while($r = mysql_fetch_array($result)){
	$businessid = $r["businessid"];
	$trans_type_id = $r["trans_type_id"];
	$homeBusiness = $r["homeBusiness"];
	$amount = $r["amount"];
	
	///remove amounts
	if(array_key_exists("$homeBusiness:$trans_type_id",$charges)){
		$charges["$homeBusiness:$trans_type_id"]["amount"] -= $amount;
	}
	else{
		$charges["$homeBusiness:$trans_type_id"]["amount"] = $amount * -1;
	}

	///add amounts
	if(array_key_exists("$businessid:$trans_type_id",$charges)){
		$charges["$businessid:$trans_type_id"]["amount"] += $amount;
	}
	else{
		$charges["$businessid:$trans_type_id"]["amount"] = $amount;
	}
	$charges["$businessid:$trans_type_id"]["businessid"] = $r["businessid"];
	$charges["$businessid:$trans_type_id"]["companyid"] = $r["companyid"];
	$charges["$businessid:$trans_type_id"]["trans_type_id"] = $r["trans_type_id"];
	$charges["$businessid:$trans_type_id"]["creditid"] = $r["creditid"];
	$charges["$businessid:$trans_type_id"]["debitid"] = $r["debitid"];
	$charges["$businessid:$trans_type_id"]["businessname"] = $r["businessname"];
}

$errors = "";

foreach($charges AS $key => $line){
	$amount = $line["amount"];
	$businessid = $line["businessid"];
	$companyid = $line["companyid"];
	$trans_type_id = $line["trans_type_id"];
	$creditid = $line["creditid"];
	$debitid = $line["debitid"];
	$businessname = $line["businessname"];

	if($trans_type_id > 4){
		//skip record
	}
	elseif($creditid < 1 || $debitid < 1){
		$errors .= "$businessid: $businessname ({$payments[$trans_type_id]}) is not mapped and has value of $$amount for $date<br>";
	}
	else{
		$query2 = "INSERT INTO creditdetail (companyid,businessid,amount,date,creditid) 
					VALUES ($companyid,$businessid,'$amount','$date',$creditid)
					ON DUPLICATE KEY UPDATE amount = '$amount';";
		$result2 = Treat_DB_ProxyOld::query($query2);	
		echo $query2 . "<br>";
		
		$query2 = "INSERT INTO debitdetail (companyid,businessid,amount,date,debitid) 
					VALUES ($companyid,$businessid,'$amount','$date',$debitid)
					ON DUPLICATE KEY UPDATE amount = '$amount';";
		$result2 = Treat_DB_ProxyOld::query($query2);	
		echo $query2 . "<p>";
	}
}

if($errors != ""){
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\n";
	$headers .= "From: support@companykitchen.com\r\n";
	$subject = "Online Mapping Problems";
	$email = "peteb@treatamerica.com,smartin@essentialpos.com";
	
	mail($email, $subject, $errors, $headers);
}

echo $errors;
?>