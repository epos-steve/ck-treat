<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');

$start_date = '2013-04-22';
$cc_date[0] = '2012-12-14';

$tempdate = $cc_date[0];
for($counter = 1;$counter <= 18; $counter++){
	$tempdate = date("Y-m-d",mktime(0, 0, 0, substr($tempdate,5,2) , substr($tempdate,8,2)+7, substr($tempdate,0,4)));
	$cc_date[$counter] = $tempdate;
}

$allOld = array();
$allNew = array();

echo "<table border=1>";

$query = "SELECT companyid, companyname, week_end FROM company ORDER BY companyname";
$result = Treat_DB_ProxyOld::query( $query );

while($r = mysql_fetch_array($result)){
	$companyid = $r["companyid"];
	$companyname = $r["companyname"];
	$week_end = $r["week_end"];
	
	$coOld = array();
	$coNew = array();
	
	$tempdate = $start_date;
	
	//cc_fee date
	while(dayofweek($tempdate) != $week_end){
		$tempdate = prevday($tempdate);
	}
	
	$dates[0] = $tempdate;
	
	for($counter = 1;$counter <= 18; $counter++){
		$tempdate = date("Y-m-d",mktime(0, 0, 0, substr($tempdate,5,2) , substr($tempdate,8,2)+7, substr($tempdate,0,4)));
		$dates[$counter] = $tempdate;
	}
	
	////print
	echo "<tr bgcolor=#E8E7E7><td bgcolor=#E8E7E7><b>$companyname</b></td>";
	
	for($counter=1;$counter<=18;$counter++){
		echo "<td colspan=3 bgcolor=#E8E7E7>$dates[$counter]</td>";
	}
	echo "<td bgcolor=red><b>Total</b></td></tr>";
	
	$query2 = "SELECT b.businessid, b.businessname, sd.value FROM business b 
		LEFT JOIN setup_detail sd ON sd.businessid = b.businessid AND sd.setupid = 72
		WHERE b.companyid = $companyid ORDER BY b.businessname";
	$result2 = Treat_DB_ProxyOld::query( $query2 );
	
	while($r2 = mysql_fetch_array($result2)){
		$businessid = $r2["businessid"];
		$businessname = $r2["businessname"];
		$online_cc = round($r2["value"] / 100, 2);
		
		$ccfeeValuesNew = array();
		$ccfeeValuesOld = array();
		for($counter=1;$counter<=18;$counter++){
			///online cc
			$query3 = "SELECT SUM(debitdetail.amount) AS total_fee FROM debitdetail
				JOIN debits ON debits.debitid = debitdetail.debitid AND debits.businessid = $businessid AND debits.category = 12
				JOIN mirror_accounts ON mirror_accounts.accountid = debits.debitid AND mirror_accounts.type = 2 AND mirror_accounts.accountnum != -1
				WHERE debitdetail.date > '{$dates[$counter-1]}' AND debitdetail.date <= '{$dates[$counter]}'";
			$result3 = Treat_DB_ProxyOld::query( $query3 );
			
			$online_fee = round(@mysql_result($result3,0,"total_fee") * $online_cc, 2);
			
			$query3 = "SELECT SUM(amount) AS total FROM cc_fee WHERE businessid = $businessid AND date > '{$dates[$counter-1]}' AND date <= '{$dates[$counter]}'";
			$result3 = Treat_DB_ProxyOld::query( $query3 );
echo $query3 . "<br>";			
			$ccfeeValuesOld[$counter] = @mysql_result($result3,0,"total") + $online_fee;
			
			$coOld[$counter] += $ccfeeValuesOld[$counter];
			$allOld[$counter] += $ccfeeValuesOld[$counter];
			
			$query3 = "SELECT SUM(amount) AS total FROM cc_fee_copy WHERE businessid = $businessid AND date > '{$dates[$counter-1]}' AND date <= '{$dates[$counter]}'";
			$result3 = Treat_DB_ProxyOld::query( $query3 );
			
			$ccfeeValuesNew[$counter] = @mysql_result($result3,0,"total") + $online_fee;
			
			$coNew[$counter] += $ccfeeValuesNew[$counter];
			$allNew[$counter] += $ccfeeValuesNew[$counter];
		}
		
		if(array_sum($ccfeeValuesOld) != 0 || array_sum($ccfeeValuesNew) != 0){
			echo "<tr><td>$businessname</td>";

			for($counter=1;$counter<=18;$counter++){
				$diff = $ccfeeValuesNew[$counter] - $ccfeeValuesOld[$counter];
				echo "<td>$ccfeeValuesOld[$counter]</td><td>$ccfeeValuesNew[$counter]</td><td bgcolor=yellow>$diff</td>";
			}
			
			$diff = array_sum($ccfeeValuesNew) - array_sum($ccfeeValuesOld);
			
			echo "<td bgcolor=red>$diff</td></tr>";
		}
	}
	
	echo "<tr bgcolor=orange><td bgcolor=orange>Totals</td>";
	for($counter=1;$counter<=18;$counter++){
		$diff = $coNew[$counter] - $coOld[$counter];
		echo "<td bgcolor=orange>$coOld[$counter]</td><td bgcolor=orange>$coNew[$counter]</td><td bgcolor=yellow>$diff</td>";
	}
	$diff = array_sum($coNew) - array_sum($coOld);
	echo "<td bgcolor=red>$diff</td></tr>";
	
}

echo "<tr bgcolor=green><td bgcolor=green>Totals</td>";
for($counter=1;$counter<=18;$counter++){
	$diff = $allNew[$counter] - $allOld[$counter];
	echo "<td bgcolor=green>$allOld[$counter]</td><td bgcolor=green>$allNew[$counter]</td><td bgcolor=yellow>$diff</td>";
}
$diff = array_sum($allNew) - array_sum($allOld);
echo "<td bgcolor=red>$diff</td></tr>";

echo "</table>";
    
?>