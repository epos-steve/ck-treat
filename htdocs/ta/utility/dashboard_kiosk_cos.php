<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');

require_once( DOC_ROOT . '/ta/lib/class.menu_items.php' );
require_once( DOC_ROOT . '/ta/lib/class.Kiosk.php' );

ini_set( 'memory_limit', '1024M' );

if( $_GET['businessid'] ) {
	$whereClause = 'AND businessid = '.$_GET['businessid'];
}

$queryBus = "
	SELECT
		businessid,
		GROUP_CONCAT( machine_num ) AS machine_list
	FROM business
	JOIN machine_bus_link USING( businessid )
	WHERE business.categoryid=4
	$whereClause
	GROUP BY businessid
";
$resultBus = Treat_DB_ProxyOld::query( $queryBus );

$dateTodayStr = date( 'Y-m-d' );
$dateStartStr = $_GET['dateStart'] ?: $dateTodayStr;
$dateEndStr = $_GET['dateEnd'] ?: $dateTodayStr;

$dateStart = DateTime::createFromFormat( 'Y-m-d', $dateStartStr );
$dateEnd = DateTime::createFromFormat( 'Y-m-d', $dateEndStr );

$oneDay = new DateInterval( 'P1D' );

if( $_GET['numDays'] > 1 ) {
	$numDays = new DateInterval( 'P' . ( $_GET['numDays'] - 1 ) . 'D' );
	$dateStart = clone $dateEnd;
	$dateStart->sub( $numDays );
}

echo "<table border='1' cellpadding='4' style='border: solid 1px black; border-collapse: collapse;'>";
echo '<tr><td>bid</td><td>date</td><td>menu</td><td>sold</td><td>waste</td><td>cost</td><td>waste cost</td><td>shrink cost</td><td>inv date</td></tr>';
while( $r = mysql_fetch_assoc( $resultBus ) ) {
	$businessId = $r['businessid'];
	$machineList = $r['machine_list'];
	$machineArray = explode( ',', $machineList );

	$items = menu_items::list_whole_menu( $r['businessid'] );

	$query = "
		SELECT
			MAX( last_inv_date ) AS lid
		FROM dashboard
		WHERE businessid = $businessId
	";
	$result = Treat_DB_ProxyOld::query( $query );

	$lid = @mysql_result( $result, 0, 'lid' );
	mysql_free_result( $result );

	for( $dateCurrent = clone $dateStart; $dateCurrent <= $dateEnd; $dateCurrent->add( $oneDay ) ) {
		$totalCost = $totalSold = $totalWaste = $totalCostWaste = $totalCostShrink = 0;
		$lastInvDate = '';
		$date = $dateCurrent->format( 'Y-m-d' );
		$dateAM = $date . ' 00:00:00';
		$datePM = $date . ' 23:59:59';

		//$items = menu_items::list_active_menu( $r['businessid'], 0, $date );
		//echo '<tr><td>'.$businessId.'</td><td colspan="7">items: '.count( $items ).'</td></tr>';

		//$items = menu_items::list_active_menu( $r['businessid'], 0, NULL, $date );
		$countMenu = count( $items );

		foreach( $items as $posId => $itemName ) {
			$queryItem = "
				SELECT
					menu_items_new.id,
					menu_items_new.item_code,
					menu_items_price.cost
				FROM menu_items_new
				JOIN menu_items_price ON menu_items_new.id = menu_items_price.menu_item_id
				WHERE menu_items_new.businessid=$businessId
				AND menu_items_new.pos_id = $posId
				LIMIT 1
			";

			$resultItem = Treat_DB_ProxyOld::query( $queryItem );
			$item = mysql_fetch_assoc( $resultItem );
			mysql_free_result( $resultItem );

			$itemId = $item['id'];
			$itemCost = $item['cost'];
			$itemCode = $item['item_code'];

			$countSold = $countWaste = 0;

			$countSold = menu_items::items_sold( $posId, $businessId, $date, $date, 0,  menu_items::DATE_POSTED);
			$countWaste = Kiosk::waste( $machineArray, $itemCode, $dateAM, $datePM );

			$itemCount = $countSold;
			//$itemTotalCost = $countSold * $itemCost;
			$itemTotalCost = menu_items::item_cost( $posId, $businessId, $date, $date, 0, menu_items::DATE_POSTED);
			///was $itemCount
			$costWaste = $countWaste * $itemCost;

			$totalCost += $itemTotalCost;
			$totalSold += $countSold;
			$totalWaste += $countWaste;
			$totalCostWaste += $costWaste;

			// shrink
			//$invDates = Kiosk::last_inv_dates( $machineArray, $itemCode );
			$var1 = '\'' . implode( '\', \'', $machineArray ) . '\'';

			$last_inv_dates_query = "SELECT icv1.date_time FROM mburris_report_tables.posreports_inv_count_vend_dashboard icv1 WHERE icv1.machine_num IN (" . $var1 . ") AND icv1.item_code = '" . $itemCode . "' AND icv1.is_inv = '1' ORDER BY icv1.date_time DESC LIMIT 0, 2";
			$last_inv_dates_result = Treat_DB_ProxyOld::query( $last_inv_dates_query );
			
			$invDates[0] = Treat_DB_ProxyOld::mysql_result($last_inv_dates_result, 1, 'date_time');
			$invDates[1] = Treat_DB_ProxyOld::mysql_result($last_inv_dates_result, 0, 'date_time');
			
			echo "<h3>" . $invDates[0] . "</h3>";
			echo "<h3>" . $invDates[1] . "</h3>";

			$invDateOnly = date( 'Y-m-d', strtotime( $invDates[1] ) );
			if( $invDateOnly == $date ) {
				$lastInvDate = $invDates[1];

				$onHand1 = Kiosk::inv_onhand( $machineArray, $itemCode, $invDates[0] );
				$onHand2 = Kiosk::inv_onhand( $machineArray, $itemCode, $invDates[1], 1 );
				$invFills = Kiosk::fills( $machineArray, $itemCode, $invDates[0], $invDates[1] );
				$invWaste = Kiosk::waste( $machineArray, $itemCode, $invDates[0], $invDates[1] );
				$invSold = menu_items::items_sold( $posId, $businessId, $invDates[0], $invDates[1] );

				$onHandCalc = $onHand1 + $invFills - $invWaste - $invSold;
				$countShrink = $onHandCalc - $onHand2;

				$costShrink = $countShrink * $itemCost;

				$totalCostShrink += $costShrink;
				
				///save shrink
				if($countShrink != 0){
					$shrinkQuery = "INSERT INTO menu_item_shrink (menu_item_id,date,qty,amount) 
									VALUES ($itemId,'$date',$countShrink,$costShrink)
									ON DUPLICATE KEY UPDATE qty = $countShrink, amount = $costShrink";
					$shrinkResult = Treat_DB_ProxyOld::query( $shrinkQuery );
				}
			}
		}

		if( $lastInvDate != '' ) {
			$lastInvSql = "last_inv_date = '$lastInvDate',";
		} else {
			$lastInvSql = '';
		}

		/*
		$query = "
			UPDATE dashboard SET
				$lastInvSql
				cos = $totalCost,
				waste = $totalCostWaste,
				shrink = $totalCostShrink
			WHERE businessid = $businessId
			AND date = '$date'
		";
		*/
		
		////insert update
		if( $lastInvDate != '' ) {
			$query = "INSERT INTO dashboard (businessid,date,cos,waste,shrink,last_inv_date) 
				VALUES ($businessId,'$date',$totalCost,$totalCostWaste,$totalCostShrink,'$lastInvDate')
				ON DUPLICATE KEY UPDATE cos = $totalCost, waste = $totalCostWaste, shrink = $totalCostShrink, last_inv_date = '$lastInvDate'";
		}
		else{
			$query = "INSERT INTO dashboard (businessid,date,cos,waste,shrink) 
				VALUES ($businessId,'$date',$totalCost,$totalCostWaste,$totalCostShrink)
				ON DUPLICATE KEY UPDATE cos = $totalCost, waste = $totalCostWaste, shrink = $totalCostShrink";
		}
		
		$result = Treat_DB_ProxyOld::query( $query );
		mysql_free_result( $result );

		echo "<tr>";
		echo "<td>$businessId</td><td>$date</td><td>$countMenu</td>";
		echo "<td>$totalSold</td><td>$totalWaste</td><td>$totalCost</td>";
		echo "<td>$totalCostWaste</td><td>$totalCostShrink</td><td>$lastInvDate</td>";
		echo "</tr>";
	}
}
echo "</table>";

//php>
