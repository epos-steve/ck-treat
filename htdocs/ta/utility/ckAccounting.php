<html>
<head>
	<script type="text/javascript" src="../javascripts/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="../javascripts/jquery-ui-1.7.2.custom.min.js"></script>
    <script type="text/javascript">$(document).ready(function (){$('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' });});</script>
	<link rel="stylesheet" href="../css/jq_themes/redmond/jquery-ui-1.7.2.custom.css">
</head>
<body>
<?php

function checks($businessid, $date1, $date2){
	$query = "SELECT check_number, bill_posted, taxes, total FROM checks
				WHERE businessid = $businessid 
				AND bill_posted BETWEEN '$date1 00:00:00' AND '$date2 23:59:59'
				ORDER BY bill_posted";
	$result = Treat_DB_ProxyOldProcessHost::query($query);
	
	$html .= "<table style=\"border:1px solid black;font-size:12px;background-color:white;\" cellspacing=0 cellpadding=1 width=99%>";
	$html .= "<tr bgcolor=#FFFF99><td style=\"border:1px solid black;\" width=25%>&nbsp;Check#</td>
				<td style=\"border:1px solid black;\" width=25%>&nbsp;Date</td>
				<td style=\"border:1px solid black;\" width=25%>&nbsp;Taxes</td>
				<td style=\"border:1px solid black;\" width=25%>&nbsp;Total</td></tr>";
				
	while($r = mysql_fetch_array($result)){
		$check_number = $r["check_number"];
		$posted = $r["bill_posted"];
		$taxes = $r["taxes"];
		$total = $r["total"];
		
		$tax_total += $taxes;
		$check_total += $total;
		
		$taxes = number_format($taxes, 2);
		$total = number_format($total, 2);
		
		$html .= "<tr><td style=\"border:1px solid black;\" width=25%>&nbsp;$check_number</td>
				<td style=\"border:1px solid black;\" width=25%>&nbsp;$posted</td>
				<td style=\"border:1px solid black;\" width=25% align=right>$$taxes</td>
				<td style=\"border:1px solid black;\" width=25% align=right>$$total</td></tr>";
	}
	
	$tax_total = number_format($tax_total, 2);
	$check_total = number_format($check_total, 2);
	
	$html .= "<tr bgcolor=#CCCCCC><td style=\"border:1px solid black;\" width=25%>&nbsp;</td>
				<td style=\"border:1px solid black;\" width=25%>&nbsp;</td>
				<td style=\"border:1px solid black;\" width=25% align=right>$$tax_total</td>
				<td style=\"border:1px solid black;\" width=25% align=right>$$check_total</td></tr></table>";
	
	return $html;
}

function checkdetail($category,$businessid,$date1,$date2){
	$query = "SELECT c.check_number, c.bill_posted, cd.quantity, cd.price, min.name
		FROM checks c
		JOIN checkdetail cd ON cd.businessid = c.businessid
		AND cd.bill_number = c.check_number
		JOIN menu_items_new min ON min.businessid = cd.businessid
		AND min.pos_id = cd.item_number
		JOIN menu_groups_new mgn ON mgn.id = min.group_id
		JOIN mburris_manage.client_programs cp ON cp.businessid = c.businessid
		AND cp.db_name =  'mburris_businesstrack'
		JOIN mburris_manage.mapping mp ON mp.client_programid = cp.client_programid
		AND mp.type =1
		AND mp.posid = mgn.pos_id
		JOIN credits cr ON cr.creditid = mp.localid
		AND cr.category = $category
		WHERE c.businessid =$businessid
		AND c.bill_posted
		BETWEEN  '$date1 00:00:00'
		AND  '$date2 23:59:59'
		GROUP BY cd.id
		ORDER BY c.bill_posted";
	$result = Treat_DB_ProxyOldProcessHost::query($query);
	
	$html .= "<table style=\"border:1px solid black;font-size:12px;background-color:white;\" cellspacing=0 cellpadding=1 width=99%>";
	$html .= "<tr bgcolor=#FFFF99><td style=\"border:1px solid black;\" width=20%>&nbsp;Check#</td>
				<td style=\"border:1px solid black;\" width=20%>&nbsp;Date</td>
				<td style=\"border:1px solid black;\" width=20%>&nbsp;Item</td>
				<td style=\"border:1px solid black;\" width=20%>&nbsp;Qty</td>
				<td style=\"border:1px solid black;\" width=20%>&nbsp;Amt</td></tr>";
				
	while($r = mysql_fetch_array($result)){
		$check_number = $r["check_number"];
		$posted = $r["bill_posted"];
		$item = $r["name"];
		$qty =  $r["quantity"];
		$price = $r["price"];
		
		$total_price += round($qty * $price, 2);
		$amt = number_format($qty * $price, 2);
		
		$html .= "<tr><td style=\"border:1px solid black;\" width=20%>&nbsp;$check_number</td>
				<td style=\"border:1px solid black;\" width=20%>&nbsp;$posted</td>
				<td style=\"border:1px solid black;\" width=20%>$item</td>
				<td style=\"border:1px solid black;\" width=20% align=right>$qty</td>
				<td style=\"border:1px solid black;\" width=20% align=right>$$amt</td></tr>";
	}
	
	$total_price = number_format($total_price, 2);
	
	$html .= "<tr bgcolor=#CCCCCC><td style=\"border:1px solid black;\" width=20%>&nbsp;</td>
				<td style=\"border:1px solid black;\" width=20%>&nbsp;</td>
				<td style=\"border:1px solid black;\" width=20%>&nbsp;</td>
				<td style=\"border:1px solid black;\" width=20% align=right>&nbsp;</td>
				<td style=\"border:1px solid black;\" width=20% align=right>$$total_price</td></tr></table>";
	
	return $html;
}

function promotions($category,$businessid,$date1,$date2){
	$query = "SELECT c.check_number, c.bill_posted, pr.name, cd.amount
		FROM checks c
		JOIN checkdiscounts cd ON cd.businessid = c.businessid
		AND cd.check_number = c.check_number
		JOIN promotions pr ON (pr.businessid = cd.businessid AND pr.pos_id = cd.pos_id) OR pr.pos_id = cd.pos_id AND pr.businessid IS NULL
		JOIN mburris_manage.client_programs cp ON cp.businessid = c.businessid
		AND cp.db_name =  'mburris_businesstrack'
		JOIN mburris_manage.mapping mp ON mp.client_programid = cp.client_programid
		AND mp.type =7
		JOIN debits db ON db.debitid = mp.localid
		AND db.category = $category
		WHERE c.businessid =$businessid
		AND c.bill_posted
		BETWEEN  '$date1 00:00:00'
		AND  '$date2 23:59:59'
		GROUP BY cd.id
		ORDER BY c.bill_posted";
	$result = Treat_DB_ProxyOldProcessHost::query($query);
	
	$html .= "<table style=\"border:1px solid black;font-size:12px;background-color:white;\" cellspacing=0 cellpadding=1 width=99%>";
	$html .= "<tr bgcolor=#FFFF99><td style=\"border:1px solid black;\" width=25%>&nbsp;Check#</td>
				<td style=\"border:1px solid black;\" width=25%>&nbsp;Date</td>
				<td style=\"border:1px solid black;\" width=25%>&nbsp;Item</td>
				<td style=\"border:1px solid black;\" width=25%>&nbsp;Amt</td></tr>";
				
	while($r = mysql_fetch_array($result)){
		$check_number = $r["check_number"];
		$posted = $r["bill_posted"];
		$item = $r["name"];
		$amount = $r["amount"];
		
		$total_price += round( $amount, 2 );
		$amt = number_format( $amount, 2 );
		
		$html .= "<tr><td style=\"border:1px solid black;\" width=25%>&nbsp;$check_number</td>
				<td style=\"border:1px solid black;\" width=25%>&nbsp;$posted</td>
				<td style=\"border:1px solid black;\" width=25%>$item</td>
				<td style=\"border:1px solid black;\" width=25% align=right>$$amt</td></tr>";
	}
	
	$total_price = number_format($total_price, 2);
	
	$html .= "<tr bgcolor=#CCCCCC><td style=\"border:1px solid black;\" width=25%>&nbsp;</td>
				<td style=\"border:1px solid black;\" width=25%>&nbsp;</td>
				<td style=\"border:1px solid black;\" width=25%>&nbsp;</td>
				<td style=\"border:1px solid black;\" width=25% align=right>$$total_price</td></tr></table>";
	
	return $html;
}

function payment_detail($category,$businessid,$date1,$date2){
	if($category == 2){
		////ck card
		$payment_type = 14;
	}
	elseif($category == 1){
		///credit card 
		$payment_type = 4;
	}
	elseif($category == 11){
		///cc reload
		$payment_type = 6;
	}
	elseif($category == 10){
		///cash reload
		$payment_type = 1;
	}
	elseif($category == 6){
		///voucher/pd
		$payment_type = 5;
	}
	else{
		$payment_type = 0;
	}
	
	$query = "SELECT c.check_number, c.bill_posted, 
		IF(pd.payment_type = 14, 'Member Card', 
           IF(pd.payment_type = 1, 'Cash', 
              IF(pd.payment_type = 4, 'Credit Card', 
                 IF(pd.payment_type = 5, 'Payroll/Voucher',
					IF(pd.payment_type = 6, 'Credit Card Reload', 'Payment'))))) AS name,
		pd.received,
		CONCAT(cust.first_name, ' ', cust.last_name) AS customer
		FROM checks c
		JOIN payment_detail pd ON pd.businessid = c.businessid
		AND pd.check_number = c.check_number
		AND pd.payment_type = $payment_type
		LEFT JOIN customer cust ON cust.scancode = pd.scancode
		JOIN mburris_manage.client_programs cp ON cp.businessid = c.businessid
		AND cp.db_name =  'mburris_businesstrack'
		JOIN mburris_manage.mapping mp ON mp.client_programid = cp.client_programid
		AND mp.type =5
		JOIN debits db ON db.debitid = mp.localid
		AND db.category = $category
		WHERE c.businessid = $businessid
		AND c.bill_posted
		BETWEEN  '$date1 00:00:00'
		AND  '$date2 23:59:59'
		GROUP BY pd.id
		ORDER BY c.bill_posted";
	$result = Treat_DB_ProxyOldProcessHost::query($query);
	
	$html .= "<table style=\"border:1px solid black;font-size:12px;background-color:white;\" cellspacing=0 cellpadding=1 width=99%>";
	$html .= "<tr bgcolor=#FFFF99><td style=\"border:1px solid black;\" width=20%>&nbsp;Check#</td>
				<td style=\"border:1px solid black;\" width=20%>&nbsp;Date</td>
				<td style=\"border:1px solid black;\" width=20%>&nbsp;Payment Type</td>
				<td style=\"border:1px solid black;\" width=20%>&nbsp;Customer</td>
				<td style=\"border:1px solid black;\" width=20%>&nbsp;Amt</td></tr>";
				
	while($r = mysql_fetch_array($result)){
		$check_number = $r["check_number"];
		$posted = $r["bill_posted"];
		$name = $r["name"];
		$customer =  $r["customer"];
		$amt = $r["received"];
		
		$total_price += round($amt, 2);
		$amt = number_format($amt, 2);
		
		$html .= "<tr><td style=\"border:1px solid black;\" width=20%>&nbsp;$check_number</td>
				<td style=\"border:1px solid black;\" width=20%>&nbsp;$posted</td>
				<td style=\"border:1px solid black;\" width=20%>$name</td>
				<td style=\"border:1px solid black;\" width=20%>$customer</td>
				<td style=\"border:1px solid black;\" width=20% align=right>$$amt</td></tr>";
	}
	
	$total_price = number_format($total_price, 2);
	
	$html .= "<tr bgcolor=#CCCCCC><td style=\"border:1px solid black;\" width=20%>&nbsp;</td>
				<td style=\"border:1px solid black;\" width=20%>&nbsp;</td>
				<td style=\"border:1px solid black;\" width=20%>&nbsp;</td>
				<td style=\"border:1px solid black;\" width=20% align=right>&nbsp;</td>
				<td style=\"border:1px solid black;\" width=20% align=right>$$total_price</td></tr></table>";
	
	return $html;
}

function customer_transactions($category,$businessid,$date1,$date2){
	if($category == 21 || $category == 12){
		$trans_type = 1;
		$name = "Credit Card Reload";
	}
	elseif($category == 14 || $category == 23){
		$trans_type = 4;
		$name = "Promotion";
	}
	elseif($category == 4 || $category == 24){
		$trans_type = 3;
		$name = "Refund";
	}
	else{
		$trans_type = 0;
	}
	
	$query = "select ct.id, ct.date_created, CONCAT(c.first_name, ' ', c.last_name) AS customer, ct.chargetotal
		FROM customer_transactions ct
		JOIN customer c ON c.customerid = ct.customer_id AND c.businessid = $businessid
		WHERE ct.date_created BETWEEN '$date1 00:00:00' AND '$date2 23d:59:59'
		AND ct.trans_type_id = $trans_type
		AND ct.response = 'APPROVED'
		ORDER BY ct.date_created";
	$result = Treat_DB_ProxyOldProcessHost::query($query);
	
	$html .= "<table style=\"border:1px solid black;font-size:12px;background-color:white;\" cellspacing=0 cellpadding=1 width=99%>";
	$html .= "<tr bgcolor=#FFFF99><td style=\"border:1px solid black;\" width=20%>&nbsp;Check#</td>
				<td style=\"border:1px solid black;\" width=20%>&nbsp;Date</td>
				<td style=\"border:1px solid black;\" width=20%>&nbsp;Payment Type</td>
				<td style=\"border:1px solid black;\" width=20%>&nbsp;Customer</td>
				<td style=\"border:1px solid black;\" width=20%>&nbsp;Amt</td></tr>";
				
	while($r = mysql_fetch_array($result)){
		$check_number = $r["id"];
		$posted = $r["date_created"];
		$customer =  $r["customer"];
		$amt = $r["chargetotal"];
		
		$total_price += round($amt, 2);
		$amt = number_format($amt, 2);
		
		$html .= "<tr><td style=\"border:1px solid black;\" width=20%>&nbsp;$check_number</td>
				<td style=\"border:1px solid black;\" width=20%>&nbsp;$posted</td>
				<td style=\"border:1px solid black;\" width=20%>$name</td>
				<td style=\"border:1px solid black;\" width=20%>$customer</td>
				<td style=\"border:1px solid black;\" width=20% align=right>$$amt</td></tr>";
	}
	
	$total_price = number_format($total_price, 2);
	
	$html .= "<tr bgcolor=#CCCCCC><td style=\"border:1px solid black;\" width=20%>&nbsp;</td>
				<td style=\"border:1px solid black;\" width=20%>&nbsp;</td>
				<td style=\"border:1px solid black;\" width=20%>&nbsp;</td>
				<td style=\"border:1px solid black;\" width=20% align=right>&nbsp;</td>
				<td style=\"border:1px solid black;\" width=20% align=right>$$total_price</td></tr></table>";
	
	return $html;
}

define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');

ini_set('display_errors', false);

$user = isset($_COOKIE["usercook"])?$_COOKIE["usercook"]:'';
$pass = isset($_COOKIE["passcook"])?$_COOKIE["passcook"]:'';

$export_what = array();

$date1 = $_POST["date1"];
$date2 = $_POST["date2"];
$groupby = $_POST["groupby"];
$export_what = $_POST["export_what"];

$drilldown = $_GET["dd"];
if($drilldown == 1){
	$date1 = $_GET["date1"];
	$date2 = $_GET["date2"];
	$groupby = $GET["groupby"];
	$export_what = $_GET["export_what"];
	$old_export_what = array($export_what);
	$show_what = $_GET["show_what"];
	
	///explode into detail
	$export_what = explode(":", $export_what);
	if($show_what < 1){
		$export_what = $old_export_what;
		$drilldown = 0;
	}
	elseif($export_what[0] == 4){
		$level = 3;
		$query = "SELECT companyid AS id FROM company WHERE companyid != 2 ORDER BY companyname";
		$result = Treat_DB_ProxyOldProcessHost::query($query);
	}
	elseif($export_what[0] == 3){
		$level = 2;
		$query = "SELECT districtid AS id FROM district WHERE companyid = $export_what[1] ORDER BY districtname";
		$result = Treat_DB_ProxyOldProcessHost::query($query);
	}
	elseif($export_what[0] == 2){
		$level = 1;
		$query = "SELECT businessid AS id FROM business WHERE districtid = $export_what[1] ORDER BY businessname";
		$result = Treat_DB_ProxyOldProcessHost::query($query);
	}
	else{
		$level = 0;
		$export_what = $old_export_what;
	}
	
	if($level > 0){
		$export_what = array();
		while($r = mysql_fetch_array($result)){
			$export_what[] = $level . ":" . $r["id"];
		}
	}
}

if($date1 == ""){
	$date1 = date("Y-m-d",mktime(0, 0, 0, date("m") , date("d")-7, date("Y")));
	$date2 = date("Y-m-d",mktime(0, 0, 0, date("m") , date("d")-1, date("Y")));
}

$query = "SELECT username,password,security_level FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOldProcessHost::query($query);
$num=mysql_numrows($result);

$security_level = mysql_result($result,0,"security_level");

if($security_level < 7 || $num == 0) exit;

	$query = "SELECT company.companyname,
					company.companyid,
					company.week_end,
					district.districtname,
					district.districtid,
					business.businessname,
					business.businessid
					FROM business
					JOIN district ON district.districtid = business.districtid
					JOIN company ON company.companyid = business.companyid
					WHERE business.companyid != 2
					ORDER BY company.companyname,district.districtname,business.businessname";
	$result = Treat_DB_ProxyOldProcessHost::query( $query );
	
	$lastdistrictid = 0;
	$lastcompanyid = 0;
	
	echo "<img src=\"/assets/images/logo.jpg\" height=\"43\" width=\"205\" border=\"0\" alt=\"logo\" />";
	echo "<table width=100%>";

	echo "<tr bgcolor=#E8E7E7 valign=top><td width=25%><p><form action=ckAccounting.php method=post><select name=export_what[] multiple='multiple' size=20>";
	
	if(is_array($export_what) && in_array("4:1",$export_what)){
		$sel1 = "SELECTED";
	}
	else{
		$sel1 = "";
	}
	
	if($groupby == 1){
		$sel2 = "SELECTED";
	}
	else{
		$sel2 = "";
	}
	if($groupby == 2){
		$sel3 = "SELECTED";
	}
	else{
		$sel3 = "";
	}
	if($groupby == 3){
		$sel4 = "SELECTED";
	}
	else{
		$sel4 = "";
	}
	
	echo "<option value=4:1 $sel1>All</option>";
	
		while($r = mysql_fetch_array($result)){
			$businessid = $r["businessid"];
			$businessname = $r["businessname"];
			$districtid = $r["districtid"];
			$districtname = $r["districtname"];
			$companyid = $r["companyid"];
			$companyname = $r["companyname"];
			$week_end = $r["week_end"];
		
			if($companyid != $lastcompanyid){
				if(is_array($export_what) && in_array("3:$companyid",$export_what)){$sel="SELECTED";}
				else{$sel = "";}
				echo "<option value=3:$companyid $sel>$companyname ($week_end)</option>";
			}
			if($districtid != $lastdistrictid){
				if(is_array($export_what) && in_array("2:$districtid",$export_what)){$sel="SELECTED";}
				else{$sel = "";}
				echo "<option value=2:$districtid $sel>&nbsp;&nbsp;&nbsp;&nbsp;$districtname</option>";
			}
		
			if(is_array($export_what) && in_array("1:$businessid",$export_what)){$sel="SELECTED";}
			else{$sel = "";}
			echo "<option value=1:$businessid $sel>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$businessname</option>";
		
			$lastdistrictid = $districtid;
			$lastcompanyid = $companyid;
		}
		echo "</select><br>
			<input type=text name='date1' value='$date1' size=8 class=\"datepicker\"> 
				to <input type=text name='date2' value='$date2' size=8 class=\"datepicker\">
				Group by
			<select name=groupby>
				<option value=0>Date Range</option>
				<option value=1 $sel2>Day</option>
				<option value=2 $sel3>Week</option>
				<option value=3 $sel4>Month</option>
			</select>
			<input type=submit value='GO' style=\"border:1px solid #999999;background-color:#E8E7E7;font-size:12px;\">
			</form><p></td><td>";
			
			if(is_array($export_what)){
				/////////////////////
				///figure date ranges
				/////////////////////
				$date_array = array();
				$label_array = array();
				///daily
				if($groupby == 1){
					$temp_date = $date1;
					while($temp_date <= nextday($date2)){
						$date_array[] = $temp_date;
						$label_array[] = $temp_date;
						$temp_date = nextday($temp_date);	
					}
				}
				///weekly
				elseif($groupby == 2){
					$week_start = dayofweek($date1);
					$week_end = dayofweek(prevday($date1));

					$temp_date = $date1;
					while($temp_date <= nextday($date2)){
						if($temp_date == $date1 || dayofweek($temp_date) == $week_start || $temp_date == nextday($date2)){
							$date_array[] = $temp_date;
						}
						if(dayofweek($temp_date) == $week_end || $temp_date == $date2){$label_array[] = $temp_date;}
						$temp_date = nextday($temp_date);
					}
				}
				///monthly
				elseif($groupby == 3){
					$temp_date = $date1;
					while($temp_date <= nextday($date2)){
						if($temp_date == $date1 || substr($temp_date,8,2) == "01" || $temp_date == nextday($date2)){
							$date_array[] = $temp_date;
						}
						if(substr(nextday($temp_date),8,2) == "01" || $temp_date == $date2){$label_array[] = $temp_date;}
						$temp_date = nextday($temp_date);
					}
				}
				///by date range
				else{
					$date_array[] = $date1;
					$date_array[] = nextday($date2);
				}

				$date_limit = count($date_array) - 2;
				
				//////////////////////
				////level of reporting
				//////////////////////
				$level = 0;
				$businessids = "";
				$districtids = "";
				$companyids = "";
				
				foreach($export_what AS $key => $value){
					$value = explode(":", $value);
					
					if($value[0] == 4){
						$level = $value[0];
						break;
					}
					elseif($value[0] == 3 && ($level == 3 || $level == 0)){
						if($companyids == ''){
							$companyids = $value[1];
							$level = $value[0];
						}
						else{
							$companyids .= "," . $value[1];
						}
					}
					elseif($value[0] == 2 && ($level == 2 || $level == 0)){
						if($districtids == ''){
							$districtids = $value[1];
							$level = $value[0];
						}
						else{
							$districtids .= "," . $value[1];
						}
					}
					elseif($value[0] == 1 && ($level == 1 || $level == 0)){
						if($businessids == ''){
							$businessids = $value[1];
							$level = $value[0];
						}
						else{
							$businessids .= "," . $value[1];
						}
					}
				}
				
				////////////////////////////////
				////build table and start report
				////////////////////////////////
				$totalGroups = array(1 => 'CK CC Commerce 10800', 2 => 'CK CC Heart 10800', 3 => 'CK Card Balance 20820', 4 => 'CK AP 20830');
				$totalGroupsAmt = array();
				$categories = array();
				$colWid = round(100 / ($date_limit + 3));
				
				if($show_what > 0){
					$whereQry = "WHERE id = $show_what";
				}
				else{
					$whereQry = "";
				}
				
				$query = "SELECT rc.* FROM reporting_category rc
					$whereQry ORDER BY rc.type, rc.groupid, rc.orderid";
				$result = Treat_DB_ProxyOldProcessHost::query( $query );
				
				while($r = mysql_fetch_array($result)){					
					$categories[$r["id"]] = $r["category"];
					$categoriesType[$r["id"]] = $r["type"];
					$categoriesTypeKey[$r["id"]] = $r["type_key"];
					$categoriesTypeDetail[$r["id"]] = $r["type_detail"];
					$categoriesGroup[$r["id"]] = $r["groupid"];
					$categoriesTotalsGroup[$r["id"]] = $r["totals_group"];
					$categoriesTotalsGroupMath[$r["id"]] = $r["totals_math"];
				}
				
				////start table
				$rowTotal = 0;
				$groupTotal = array();
				$sectionTotal = array();
				$lastGroup = 0;
				$lastType = "";
				
				echo "<p><center><table style=\"border:1px solid black;font-size:12px;background-color:white;\" cellspacing=0 cellpadding=1 width=99%>";
				
				echo "<tr bgcolor=#FFFF99><td style=\"border:1px solid black;\" width=$colWid%>&nbsp;Account Name</td>";
				$download_data = "\"Account Name\"";
				for($temp = 0; $temp <= $date_limit; $temp++ ){
					echo "<td style=\"border:1px solid black;text-align:right;\" width=$colWid%>$label_array[$temp]&nbsp;</td>";
					$download_data .= ",\"$label_array[$temp]\"";
				}
				echo "<td style=\"border:1px solid black;text-align:right;\" width=$colWid%>Total&nbsp;</td></tr>";
				$download_data .= ",\"Total\"\r\n";
				
				///table body
				foreach($categories AS $categoryid => $categoryName){
					//////groups
					if($lastGroup != $categoriesGroup[$categoryid] && $lastGroup != 0 && $drilldown != 1){
						if($lastType == "credits"){
							$query = "SELECT credittypename AS groupName FROM credittype WHERE credittypeid = $lastGroup";
						}
						else{
							$query = "SELECT debittypename AS groupName FROM debittype WHERE debittypeid = $lastGroup";
						}
						$result = Treat_DB_ProxyOldProcessHost::query( $query );
						
						$groupName = @mysql_result($result,0,"groupName");
						
						echo "<tr bgcolor=#E8E7E7><td style=\"border:1px solid black;\">$groupName</td>";
						$download_data .= "\"$groupName\"";
						
						for($temp = 0; $temp <= $date_limit; $temp++ ){
							$showGroupTotal = number_format($groupTotal[$temp], 2);
							echo "<td style=\"border:1px solid black;text-align:right;\">$$showGroupTotal&nbsp;</td>";
							$download_data .= ",\"$showGroupTotal\"";
						}
						$rowTotal = number_format(array_sum($groupTotal), 2);
						echo "<td style=\"border:1px solid black;text-align:right;\">$$rowTotal&nbsp;</td>";
						$download_data .= ",\"$rowTotal\"\r\n";
						
						$rowTotal = 0;
						$groupTotal = array();
					}
					///section
					if($lastType != $categoriesType[$categoryid] && $lastType != "" && $drilldown != 1){
						echo "<tr bgcolor=#CCCCCC><td style=\"border:1px solid black;\">Totals</td>";
						$download_data .= "\"Totals\"";
						
						for($temp = 0; $temp <= $date_limit; $temp++ ){
							@$showSectionTotal = number_format($sectionTotal[$temp], 2);
							echo "<td style=\"border:1px solid black;text-align:right;\">$$showSectionTotal&nbsp;</td>";
							$download_data .= ",\"$showSectionTotal\"";
						}
						$rowTotal = number_format(array_sum($sectionTotal), 2);
						echo "<td style=\"border:1px solid black;text-align:right;\">$$rowTotal&nbsp;</td>";
						$download_data .= ",\"$rowTotal\"\r\n\r\n";
						
						$rowTotal = 0;
						$creditArray = $sectionTotal;
						$sectionTotal = array();
					}
					
					//echo "<tr><td style=\"border:1px solid black;\">$categoryName</td>";
					//$download_data .= "\"$categoryName\"";
/////////////////////
					if($drilldown == 1){
						foreach($export_what AS $key => $value){
							$value = explode(":", $value);
							
							if($level == 1){
								$query = "SELECT businessname FROM business WHERE businessid = $value[1]";
								$result = Treat_DB_ProxyOldProcessHost::query( $query );
								
								$showName = @mysql_result($result,0,"businessname");
							
								$joinQuery = "JOIN business ON business.businessid = {$categoriesType[$categoryid]}.businessid
									AND business.businessid IN ($value[1])";
							}
							elseif($level == 2){
								$query = "SELECT districtname FROM district WHERE districtid = $value[1]";
								$result = Treat_DB_ProxyOldProcessHost::query( $query );
								
								$showName = @mysql_result($result,0,"districtname");
							
								$joinQuery = "JOIN business ON business.businessid = {$categoriesType[$categoryid]}.businessid
									JOIN district ON district.districtid = business.districtid AND district.districtid IN ($value[1])";
							}
							elseif($level == 3){
								$query = "SELECT companyname FROM company WHERE companyid = $value[1]";
								$result = Treat_DB_ProxyOldProcessHost::query( $query );
								
								$showName = @mysql_result($result,0,"companyname");
							
								$joinQuery = "JOIN business ON business.businessid = {$categoriesType[$categoryid]}.businessid
									JOIN company ON company.companyid = business.companyid AND company.companyid IN ($value[1])";
							}
							else{
								$joinQuery = "";
							}
							
							//echo "<tr><td style=\"border:1px solid black;\">$showName $categoryName</td>";
							echo "<tr><td style=\"border:1px solid black;\">
								<a style=\"text-decoration:none;\" href=\"?dd=1&date1=$date1&date2=$date2&export_what=$export_what[$key]&show_what=&groupby=0\"><font color=black>
								$showName $categoryName</font></a></td>";
							$download_data .= "\"$showName $categoryName\"";
							
							for($temp = 0; $temp <= $date_limit; $temp++ ){
								$query = "SELECT SUM({$categoriesTypeDetail[$categoryid]}.amount) AS amount FROM {$categoriesTypeDetail[$categoryid]}
									JOIN {$categoriesType[$categoryid]} ON {$categoriesType[$categoryid]}.{$categoriesTypeKey[$categoryid]} = {$categoriesTypeDetail[$categoryid]}.{$categoriesTypeKey[$categoryid]}
										AND {$categoriesType[$categoryid]}.category = $categoryid
										$joinQuery
										WHERE {$categoriesTypeDetail[$categoryid]}.date >= '{$date_array[$temp]}' 
											AND {$categoriesTypeDetail[$categoryid]}.date < '{$date_array[$temp+1]}'";
								$result = Treat_DB_ProxyOldProcessHost::query( $query );
							
								$amount = @mysql_result($result,0,"amount");
								$rowTotal += $amount;
								@$groupTotal[$temp] += $amount;
								@$sectionTotal[$temp] += $amount;
								
								///add up totals
								if($categoriesTotalsGroup[$categoryid] > 0){
									if($categoriesTotalsGroupMath[$categoryid] == 1){
										$totalGroupsAmt[$categoriesTotalsGroup[$categoryid]][$temp] -= $amount;
									}
									else{
										$totalGroupsAmt[$categoriesTotalsGroup[$categoryid]][$temp] += $amount;
									}
								}
								
								$amount = number_format($amount, 2);
								
								//echo "<td style=\"border:1px solid black;text-align:right;\">$$amount</td>";
								$newdate1 = $date_array[$temp];
								$newdate2 = prevday($date_array[$temp + 1]);
								echo "<td style=\"border:1px solid black;text-align:right;\">
									<a style=\"text-decoration:none;\" href=\"?dd=1&date1=$newdate1&date2=$newdate2&export_what=$export_what[$key]&show_what=$categoryid&groupby=0\"><font color=black>
									$$amount</font></a></td>";
								$download_data .= ",\"$amount\"";
							}
							$rowTotal = number_format($rowTotal, 2);
							echo "<td style=\"border:1px solid black;text-align:right;\">$$rowTotal</td></tr>";
							$download_data .= ",\"$rowTotal\"\r\n";
							$rowTotal = 0;
						}
					}
					else{
						if($level == 1){
							$joinQuery = "JOIN business ON business.businessid = {$categoriesType[$categoryid]}.businessid
								AND business.businessid IN ($businessids)";
						}
						elseif($level == 2){
							$joinQuery = "JOIN business ON business.businessid = {$categoriesType[$categoryid]}.businessid
								JOIN district ON district.districtid = business.districtid AND district.districtid IN ($districtids)";
						}
						elseif($level == 3){
							$joinQuery = "JOIN business ON business.businessid = {$categoriesType[$categoryid]}.businessid
								JOIN company ON company.companyid = business.companyid AND company.companyid IN ($companyids)";
						}
						else{
							$joinQuery = "";
						}
						
						echo "<tr><td style=\"border:1px solid black;\">$categoryName</td>";
						$download_data .= "\"$categoryName\"";
						
						for($temp = 0; $temp <= $date_limit; $temp++ ){
							$query = "SELECT SUM({$categoriesTypeDetail[$categoryid]}.amount) AS amount FROM {$categoriesTypeDetail[$categoryid]}
								JOIN {$categoriesType[$categoryid]} ON {$categoriesType[$categoryid]}.{$categoriesTypeKey[$categoryid]} = {$categoriesTypeDetail[$categoryid]}.{$categoriesTypeKey[$categoryid]}
									AND {$categoriesType[$categoryid]}.category = $categoryid
									$joinQuery
									WHERE {$categoriesTypeDetail[$categoryid]}.date >= '{$date_array[$temp]}' 
										AND {$categoriesTypeDetail[$categoryid]}.date < '{$date_array[$temp+1]}'";
							$result = Treat_DB_ProxyOldProcessHost::query( $query );
						
							$amount = @mysql_result($result,0,"amount");
							$rowTotal += $amount;
							@$groupTotal[$temp] += $amount;
							@$sectionTotal[$temp] += $amount;
							
							///add up totals
							if($categoriesTotalsGroup[$categoryid] > 0){
								if($categoriesTotalsGroupMath[$categoryid] == 1){
									$totalGroupsAmt[$categoriesTotalsGroup[$categoryid]][$temp] -= $amount;
								}
								else{
									$totalGroupsAmt[$categoriesTotalsGroup[$categoryid]][$temp] += $amount;
								}
							}
							
							$amount = number_format($amount, 2);
							
							//echo "<td style=\"border:1px solid black;text-align:right;\">$$amount</td>";
							$newdate1 = $date_array[$temp];
							$newdate2 = prevday($date_array[$temp + 1]);
							echo "<td style=\"border:1px solid black;text-align:right;\">
								<a style=\"text-decoration:none;\" href=\"?dd=1&date1=$newdate1&date2=$newdate2&export_what=$export_what[0]&show_what=$categoryid&groupby=0\"><font color=black>
								$$amount</font></a></td>";
							$download_data .= ",\"$amount\"";
						}
						$rowTotal = number_format($rowTotal, 2);
						echo "<td style=\"border:1px solid black;text-align:right;\">$$rowTotal</td></tr>";
						$download_data .= ",\"$rowTotal\"\r\n";
						$rowTotal = 0;
					}
///////////////////					
					$lastGroup = $categoriesGroup[$categoryid];
					$lastType = $categoriesType[$categoryid];
				}
				
				//////last group
				if($lastType == "credits"){
					$query = "SELECT credittypename AS groupName FROM credittype WHERE credittypeid = $lastGroup";
				}
				else{
					$query = "SELECT debittypename AS groupName FROM debittype WHERE debittypeid = $lastGroup";
				}
				$result = Treat_DB_ProxyOldProcessHost::query( $query );

				$groupName = @mysql_result($result,0,"groupName");

				echo "<tr bgcolor=#E8E7E7><td style=\"border:1px solid black;\">$groupName</td>";
				$download_data .= "\"$groupName\"";

				for($temp = 0; $temp <= $date_limit; $temp++ ){
					$showGroupTotal = number_format($groupTotal[$temp], 2);
					echo "<td style=\"border:1px solid black;text-align:right;\">$$showGroupTotal&nbsp;</td>";
					$download_data .= ",\"$showGroupTotal\"";
				}
				$rowTotal = number_format(array_sum($groupTotal), 2);
				echo "<td style=\"border:1px solid black;text-align:right;\">$$rowTotal&nbsp;</td>";
				$download_data .= ",\"$rowTotal\"\r\n";

				$rowTotal = 0;
				$groupTotal = array();

				///last section
				echo "<tr bgcolor=#CCCCCC><td style=\"border:1px solid black;\">Totals</td>";
				$download_data .= "\"Totals\"";

				for($temp = 0; $temp <= $date_limit; $temp++ ){
					@$showSectionTotal = number_format($sectionTotal[$temp], 2);
					echo "<td style=\"border:1px solid black;text-align:right;\">$$showSectionTotal&nbsp;</td>";
					$download_data .= ",\"$showSectionTotal\"";
				}
				$rowTotal = number_format(array_sum($sectionTotal), 2);
				echo "<td style=\"border:1px solid black;text-align:right;\">$$rowTotal&nbsp;</td>";
				$download_data .= ",\"$rowTotal\"\r\n";
				
				///diff
				if($drilldown != 1){
					$rowTotal = 0;
					echo "<tr bgcolor=#CCCCCC><td style=\"border:1px solid black;\">Diff</td>";
					$download_data .= "\"Diff\"";

					for($temp = 0; $temp <= $date_limit; $temp++ ){
						$diff = abs(round($sectionTotal[$temp] - $creditArray[$temp], 2));
						$rowTotal += round($sectionTotal[$temp] - $creditArray[$temp], 2);
						if($diff != 0){
							$diffColor = "red";
						}
						else{
							$diffColor = "#00FF00";
						}
						$diff = number_format($diff, 2);
						echo "<td style=\"border:1px solid black;text-align:right;background-color:$diffColor;\">$$diff&nbsp;</td>";
						$download_data .= ",\"$diff\"";
					}
					$rowTotal = number_format($rowTotal, 2);
					echo "<td style=\"border:1px solid black;text-align:right;\">$$rowTotal&nbsp;</td>";
					$download_data .= ",\"$rowTotal\"\r\n";
				}
				
				echo "</table><p>";
				
				/////details
				if($drilldown == 1 && $level == 1 && $date1 == $date2 && $show_what > 0 && count($export_what) == 1){
					$back = explode(":", $old_export_what[0]);
					
					if($show_what == 7){
						$html = checks($back[1],$date1,$date2);
					}
					elseif($show_what == 15 || $show_what == 16 || $show_what == 17 || $show_what == 20 || $show_what == 26 || $show_what == 8 || $show_what == 19){
						$html = checkdetail($show_what,$back[1],$date1,$date2);
					}
					elseif($show_what == 3 || $show_what == 9 || $show_what == 13){
						$html = promotions($show_what,$back[1],$date1,$date2);
					}
					elseif($show_what == 1 || $show_what == 2 || $show_what == 6 || $show_what == 10 || $show_what == 11){
						$html = payment_detail($show_what,$back[1],$date1,$date2);
					}
					elseif($show_what == 21 || $show_what == 12 || $show_what == 14 || $show_what == 23 || $show_what == 4 || $show_what == 24){
						$html = customer_transactions($show_what,$back[1],$date1,$date2);
					}
					else{
						$html = "No Details";
					}
					echo "<center>$html</center><p>";
				}
				
				if($drilldown == 1){
					$back = explode(":", $old_export_what[0]);
					
					if($back[0] == 4){
						$new_what = "4:1";
					}
					elseif($back[0] == 3){
						$new_what = "4:1";
					}
					elseif($back[0] == 2){
						$query = "SELECT companyid FROM district WHERE districtid = $back[1]";
						$result = Treat_DB_ProxyOldProcessHost::query( $query );
						
						$newCompanyid = @mysql_result($result,0,"companyid");
					
						$new_what = "3:$newCompanyid";
					}
					elseif($back[0] == 1){
						$query = "SELECT districtid FROM business WHERE businessid = $back[1]";
						$result = Treat_DB_ProxyOldProcessHost::query( $query );
						
						$newDistrictId = @mysql_result($result,0,"districtid");
					
						$new_what = "2:$newDistrictId";
					}
				
					echo "<a style=\"text-decoration:none;\" href=\"?dd=1&date1=$newdate1&date2=$newdate2&export_what=$new_what&show_what=$categoryid&groupby=0\"><font color=blue size=2>
								BACK</font></a>";
				}

				$rowTotal = 0;
				$sectionTotal = array();
				
				///////////////////////////////////
				//////////////FEES/////////////////
				///////////////////////////////////
				if($drilldown != 1){
					
					///credit card fees
					$fees = array();
					$feeNames = array(1 => 'Heartland Fee', 2 => 'Commerce Fee', 3 => 'CK Fee', 4 => 'EFN Fee', 5 => 'Monthly Fee');
					
					if($level == 1){
						$joinQuery = "JOIN business ON business.businessid = cc_fee.businessid
							AND business.businessid IN ($businessids)";
					}
					elseif($level == 2){
						$joinQuery = "JOIN business ON business.businessid = cc_fee.businessid
							JOIN district ON district.districtid = business.districtid AND district.districtid IN ($districtids)";
					}
					elseif($level == 3){
						$joinQuery = "JOIN business ON business.businessid = cc_fee.businessid
							JOIN company ON company.companyid = business.companyid AND company.companyid IN ($companyids)";
					}
					else{
						$joinQuery = "";
					}
					
					for($temp = 0; $temp <= $date_limit; $temp++ ){
						$query = "SELECT SUM(amount) AS cc_fee, 
									SUM(online_fee) AS online_fee FROM cc_fee
								$joinQuery
								WHERE cc_fee.date >= '{$date_array[$temp]}' AND cc_fee.date < '{$date_array[$temp+1]}'";
						$result = Treat_DB_ProxyOldProcessHost::query( $query );
						
						$fees[1][$temp] = @mysql_result($result,0,"cc_fee");
						$fees[2][$temp] = @mysql_result($result,0,"online_fee");
					}
					
					///ck fees
					if($level == 1){
						$joinQuery = "JOIN business ON business.businessid = ck_fee.businessid
							AND business.businessid IN ($businessids)";
					}
					elseif($level == 2){
						$joinQuery = "JOIN business ON business.businessid = ck_fee.businessid
							JOIN district ON district.districtid = business.districtid AND district.districtid IN ($districtids)";
					}
					elseif($level == 3){
						$joinQuery = "JOIN business ON business.businessid = ck_fee.businessid
							JOIN company ON company.companyid = business.companyid AND company.companyid IN ($companyids)";
					}
					else{
						$joinQuery = "";
					}
					
					for($temp = 0; $temp <= $date_limit; $temp++ ){
						$query = "SELECT SUM(fee) AS ck_fee, 
									SUM(efn_fee) AS efn_fee, 
									SUM(monthly_fee) AS monthly_fee FROM ck_fee
								$joinQuery
								WHERE ck_fee.date >= '{$date_array[$temp]}' AND ck_fee.date < '{$date_array[$temp+1]}'";
						$result = Treat_DB_ProxyOldProcessHost::query( $query );
						
						$fees[3][$temp] = @mysql_result($result,0,"ck_fee");
						$fees[4][$temp] = @mysql_result($result,0,"efn_fee");
						$fees[5][$temp] = @mysql_result($result,0,"monthly_fee");
					}
					
					///display fees
					echo "<table style=\"border:1px solid black;font-size:12px;background-color:white;\" cellspacing=0 cellpadding=1 width=99%>";
					
					echo "<tr bgcolor=#FFFF99><td style=\"border:1px solid black;\" width=$colWid%>&nbsp;Fees</td>";
					$download_data .= "\r\n\"Fees\"";
					for($temp = 0; $temp <= $date_limit; $temp++ ){
						echo "<td style=\"border:1px solid black;text-align:right;\" width=$colWid%>$label_array[$temp]&nbsp;</td>";
						$download_data .= ",\"$label_array[$temp]\"";
					}
					echo "<td style=\"border:1px solid black;text-align:right;\" width=$colWid%>Total&nbsp;</td></tr>";
					$download_data .= ",\"Total\"\r\n";
					
					for($counter = 1; $counter <= 5; $counter++){
						echo "<tr><td style=\"border:1px solid black;\">{$feeNames[$counter]}</td>";
						$download_data .= "\"{$feeNames[$counter]}\"";
						foreach($fees[$counter] AS $key => $value){
							$amount = @number_format($value, 2);
							echo "<td style=\"border:1px solid black;text-align:right;\">$$amount</td>";
							$download_data .= ",\"$amount\"";
						}
						$rowTotal = number_format(array_sum($fees[$counter]), 2);
						echo "<td style=\"border:1px solid black;text-align:right;\">$$rowTotal&nbsp;</td>";
						$download_data .= ",\"$rowTotal\"\r\n";
					}
					
					echo "</table><p>";
					
					/////totals table
					echo "<table style=\"border:1px solid black;font-size:12px;background-color:white;\" cellspacing=0 cellpadding=1 width=99%>";
					
					echo "<tr bgcolor=#FFFF99><td style=\"border:1px solid black;\" width=$colWid%>&nbsp;Account Name</td>";
					$download_data .= "\r\n\"Account Name\"";
					for($temp = 0; $temp <= $date_limit; $temp++ ){
						echo "<td style=\"border:1px solid black;text-align:right;\" width=$colWid%>$label_array[$temp]&nbsp;</td>";
						$download_data .= ",\"$label_array[$temp]\"";
					}
					echo "<td style=\"border:1px solid black;text-align:right;\" width=$colWid%>Total&nbsp;</td></tr>";
					$download_data .= ",\"Total\"\r\n";
					
					foreach($totalGroups AS $key => $totalGroup){
						echo "<tr><td style=\"border:1px solid black;\">$totalGroup</td>";
						$download_data .= "\"$totalGroup\"";
						
						for($temp = 0; $temp <= $date_limit; $temp++ ){
							$amount = number_format($totalGroupsAmt[$key][$temp] , 2);
							echo "<td style=\"border:1px solid black;text-align:right;\">$$amount</td>";
							$download_data .= ",\"$amount\"";
						}
						$amount = number_format(array_sum($totalGroupsAmt[$key]), 2);
						echo "<td style=\"border:1px solid black;text-align:right;\">$$amount</td></tr>";
						$download_data .= ",\"$amount\"\r\n";
					}
					
					echo "</table>";
				}
				
				////download data
				echo "<p><center><form action='../posreport.php' method='post' style=\"margin:0;padding:0;display:inline;\">
				<input type=hidden name=download_data value='$download_data'>
				<input type=submit value='Download' style=\"border: 1px solid #999999; font-size: 10px; background-color:#CCCCCC;\" /></form></center>";
			}
			else{
				echo "<p><br><p><br><p><center>Please choose a filter</center>";
			}
		
			echo "</td></tr>";
		echo "</table></center><p>";
?>
</body>
</html>
