<?php

function nextday($nextd,$day_format=''){ //Function returns next day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monn = substr($nextd, 5, 2);
   $dayn = substr($nextd, 8, 2);
   $yearn = substr($nextd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monn, $dayn+1, $yearn));
   return $tempdate;
}

function prevday($prevd,$day_format=''){ //Function returns previous day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monp = substr($prevd, 5, 2);
   $dayp = substr($prevd, 8, 2);
   $yearp = substr($prevd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monp, $dayp-1, $yearp));
   return $tempdate;
}

define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');

$style = "text-decoration:none";

$today=date("Y-m-d");
$d_thirty=$today;
for($counter=1;$counter<=30;$counter++){$d_thirty=prevday($d_thirty);}
$d_sixty=$d_thirty;
for($counter=1;$counter<=30;$counter++){$d_sixty=prevday($d_sixty);}
$d_ninety=$d_sixty;
for($counter=1;$counter<=30;$counter++){$d_ninety=prevday($d_ninety);}

echo "$today, $d_thirty, $d_sixty, $d_ninety<br>";

mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");

$query3 = "SELECT businessid,businessname FROM business WHERE companyid != '2' ORDER BY companyid DESC, districtid DESC, businessname DESC";
$result3 = mysql_query($query3);
$num3=mysql_numrows($result3);

echo "<table border=1>";

$num3--;
while ($num3>=0)
{
   $businessid=@mysql_result($result3,$num3,"businessid");
   $businessname=@mysql_result($result3,$num3,"businessname");

   $query = "SELECT SUM(outstanding) AS totalamt FROM invoice WHERE businessid = '$businessid' AND cleared = '0' AND posted = '1' AND date > '$d_thirty'";
   $result = mysql_query($query);

   $zero=@mysql_result($result,0,"totalamt");

   $query = "SELECT SUM(outstanding) AS totalamt FROM invoice2 WHERE businessid = '$businessid' AND cleared = '0' AND date > '$d_thirty'";
   $result = mysql_query($query);

   $totalamt=@mysql_result($result,0,"totalamt");

   $zero+=$totalamt;

   $query = "SELECT SUM(outstanding) AS totalamt FROM invoice WHERE businessid = '$businessid' AND cleared = '0' AND posted = '1' AND date > '$d_sixty' AND date <= '$d_thirty'";
   $result = mysql_query($query);

   $thirty=@mysql_result($result,0,"totalamt");

   $query = "SELECT SUM(outstanding) AS totalamt FROM invoice2 WHERE businessid = '$businessid' AND cleared = '0' AND date > '$d_sixty' AND date <= '$d_thirty'";
   $result = mysql_query($query);

   $totalamt=@mysql_result($result,0,"totalamt");

   $thirty+=$totalamt;

   $query = "SELECT SUM(outstanding) AS totalamt FROM invoice WHERE businessid = '$businessid' AND cleared = '0' AND posted = '1' AND date > '$d_ninety' AND date <= '$d_sixty'";
   $result = mysql_query($query);

   $sixty=@mysql_result($result,0,"totalamt");

   $query = "SELECT SUM(outstanding) AS totalamt FROM invoice2 WHERE businessid = '$businessid' AND cleared = '0' AND date > '$d_ninety' AND date <= '$d_sixty'";
   $result = mysql_query($query);

   $totalamt=@mysql_result($result,0,"totalamt");

   $sixty+=$totalamt;

   $query = "SELECT SUM(outstanding) AS totalamt FROM invoice WHERE businessid = '$businessid' AND cleared = '0' AND posted = '1' AND date <= '$d_ninety'";
   $result = mysql_query($query);

   $ninety=@mysql_result($result,0,"totalamt");

   $query = "SELECT SUM(outstanding) AS totalamt FROM invoice2 WHERE businessid = '$businessid' AND cleared = '0' AND date <= '$d_ninety'";
   $result = mysql_query($query);

   $totalamt=@mysql_result($result,0,"totalamt");

   $ninety+=$totalamt;

   $query2 = "SELECT * FROM dashboard_aging WHERE businessid = '$businessid'";
   $result2 = mysql_query($query2);
   $num2=mysql_numrows($result2);

   if($num2>0){
      $query2 = "UPDATE dashboard_aging SET zero = '$zero', thirty = '$thirty', sixty = '$sixty', ninety = '$ninety' WHERE businessid = '$businessid'";
      $result2 = mysql_query($query2);
   }
   else{
      $query2 = "INSERT INTO dashboard_aging (businessid,zero,thirty,sixty,ninety) VALUES ('$businessid','$zero','$thirty','$sixty','$ninety')";
      $result2 = mysql_query($query2);
   }

   ////////////////BY SALESMAN
   $query22 = "SELECT * FROM vend_salesman WHERE businessid = '$businessid'";
   $result22 = mysql_query($query22);
   $num22=mysql_numrows($result22);

   $num22--;
   while($num22>=0){
      $salesid=@mysql_result($result22,$num22,"salesid");

      $query = "SELECT SUM(invoice.outstanding) AS totalamt FROM invoice,accounts WHERE invoice.businessid = '$businessid' AND invoice.cleared = '0' AND invoice.posted = '1' AND invoice.date > '$d_thirty' AND invoice.accountid = accounts.accountid AND accounts.salesman = '$salesid' AND accounts.businessid = '$businessid'";
      $result = mysql_query($query);

      $zero=@mysql_result($result,0,"totalamt");

      $query = "SELECT SUM(invoice2.outstanding) AS totalamt FROM invoice2,accounts WHERE invoice2.businessid = '$businessid' AND invoice2.cleared = '0' AND invoice2.date > '$d_thirty' AND TRIM(LEADING '0' FROM invoice2.cust_num) LIKE accounts.accountnum AND accounts.salesman = '$salesid' AND accounts.businessid = '$businessid'";
      $result = mysql_query($query);

      $totalamt=@mysql_result($result,0,"totalamt");

      $zero+=$totalamt;

      $query = "SELECT SUM(invoice.outstanding) AS totalamt FROM invoice,accounts WHERE invoice.businessid = '$businessid' AND invoice.cleared = '0' AND invoice.posted = '1' AND invoice.date <= '$d_thirty' AND invoice.date > '$d_sixty' AND invoice.accountid = accounts.accountid AND accounts.salesman = '$salesid' AND accounts.businessid = '$businessid'";
      $result = mysql_query($query);

      $thirty=@mysql_result($result,0,"totalamt");

      $query = "SELECT SUM(invoice2.outstanding) AS totalamt FROM invoice2,accounts WHERE invoice2.businessid = '$businessid' AND invoice2.cleared = '0' AND invoice2.date <= '$d_thirty' AND invoice2.date > '$d_sixty' AND TRIM(LEADING '0' FROM invoice2.cust_num) LIKE accounts.accountnum AND accounts.salesman = '$salesid' AND accounts.businessid = '$businessid'";
      $result = mysql_query($query);

      $totalamt=@mysql_result($result,0,"totalamt");

      $thirty+=$totalamt;

      $query = "SELECT SUM(invoice.outstanding) AS totalamt FROM invoice,accounts WHERE invoice.businessid = '$businessid' AND invoice.cleared = '0' AND invoice.posted = '1' AND invoice.date <= '$d_sixty' AND invoice.date > '$d_ninety' AND invoice.accountid = accounts.accountid AND accounts.salesman = '$salesid' AND accounts.businessid = '$businessid'";
      $result = mysql_query($query);

      $sixty=@mysql_result($result,0,"totalamt");

      $query = "SELECT SUM(invoice2.outstanding) AS totalamt FROM invoice2,accounts WHERE invoice2.businessid = '$businessid' AND invoice2.cleared = '0' AND invoice2.date <= '$d_sixty' AND invoice2.date > '$d_ninety' AND TRIM(LEADING '0' FROM invoice2.cust_num) LIKE accounts.accountnum AND accounts.salesman = '$salesid' AND accounts.businessid = '$businessid'";
      $result = mysql_query($query);

      $totalamt=@mysql_result($result,0,"totalamt");

      $sixty+=$totalamt;

      $query = "SELECT SUM(invoice.outstanding) AS totalamt FROM invoice,accounts WHERE invoice.businessid = '$businessid' AND invoice.cleared = '0' AND invoice.posted = '1' AND invoice.date <= '$d_ninety' AND invoice.accountid = accounts.accountid AND accounts.salesman = '$salesid' AND accounts.businessid = '$businessid'";
      $result = mysql_query($query);

      $ninety=@mysql_result($result,0,"totalamt");

      $query = "SELECT SUM(invoice2.outstanding) AS totalamt FROM invoice2,accounts WHERE invoice2.businessid = '$businessid' AND invoice2.cleared = '0' AND invoice2.date <= '$d_ninety' AND TRIM(LEADING '0' FROM invoice2.cust_num) LIKE accounts.accountnum AND accounts.salesman = '$salesid' AND accounts.businessid = '$businessid'";
      $result = mysql_query($query);

      $totalamt=@mysql_result($result,0,"totalamt");

      $ninety+=$totalamt;

      $query2 = "SELECT * FROM dashboard_aging_salesmen WHERE salesmanid = '$salesid'";
      $result2 = mysql_query($query2);
      $num2=mysql_numrows($result2);

      if($num2>0){
         $query2 = "UPDATE dashboard_aging_salesmen SET zero = '$zero', thirty = '$thirty', sixty = '$sixty', ninety = '$ninety' WHERE salesmanid = '$salesid'";
         $result2 = mysql_query($query2);
      }
      else{
         $query2 = "INSERT INTO dashboard_aging_salesmen (salesmanid,zero,thirty,sixty,ninety) VALUES ('$salesid','$zero','$thirty','$sixty','$ninety')";
         $result2 = mysql_query($query2);
      }

      $num22--;
   }

   ////////////////////////////

   $total_ar=$zero+$thirty+$sixty+$ninety;
   echo "<tr><td>$businessname</td><td align=right>$zero</td><td align=right>$thirty</td><td align=right>$sixty</td><td align=right>$ninety</td><td align=right>$total_ar</td></tr>";

   $zero_tot+=$zero;
   $thirty_tot+=$thirty;
   $sixty_tot+=$sixty;
   $ninety_tot+=$ninety;

   $tot_total+=$total_ar;

   $zero=0;
   $thirty=0;
   $sixty=0;
   $ninety=0;

   $num3--;
}
mysql_close();

echo "<tr><td></td><td align=right>$zero_tot</td><td align=right>$thirty_tot</td><td align=right>$sixty_tot</td><td align=right>$ninety_tot</td><td align=right>$tot_total</td></tr></table><p>";
echo "Dashboard Updated, Close this Window";
?>