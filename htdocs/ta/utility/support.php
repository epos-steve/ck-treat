<html>
	<head>
		<title>Incident Follow Up Request</title>
		<script type="text/javascript" src="../javascripts/jquery-1.4.2.min.js"></script>
		<script type="text/javascript" src="../javascripts/jquery-ui-1.7.2.custom.min.js"></script>
		<script type="text/javascript">$(document).ready(function (){$('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' });});</script>
		<link rel="stylesheet" href="../css/jq_themes/redmond/jquery-ui-1.7.2.custom.css">
	</head>
	

<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once( DOC_ROOT . '/../lib/PHPExcel/PHPExcel.php' );
require_once(DOC_ROOT.'/bootstrap.php');

$user = isset($_COOKIE["usercook"])?$_COOKIE["usercook"]:'';
$pass = isset($_COOKIE["passcook"])?$_COOKIE["passcook"]:'';

$query = "SELECT username,password,security_level,email FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=mysql_numrows($result);

$security_level = mysql_result($result,0,"security_level");
$email = mysql_result($result,0,"email");

if($security_level < 3) exit;

$success = @$_GET["success"];

$date = date("Y-m-d");
?>
<body>
<img src="/assets/images/logo.jpg" height="43" width="205" alt="logo">
<p>
<?php
if($success == "ok"){
	echo "Thanks for submitting an incident.  We will get back to you as soon as possible.";
}
?>

<form name="Test" action="https://vtiger.essential-elements.net/modules/Webforms/capture.php" method="post" accept-charset="utf-8">
	<input type="hidden" name="publicid" value="aa94c366015e39b9b29ed50438fa3514"></input>
	<input type="hidden" name="name" value="Test"></input>
	<table bgcolor=#E8E7E7 style="border:2px solid #999999;" cellspacing=0 cellpadding=2>
		<thead>
			<tr bgcolor=#999999 style="border:2px solid #999999;">
				<td colspan=2>
					<b><center>Incident Follow Up Request</center></b>
				</td>
			</tr>
		</thead>
		<tr>
			<td>
				<label>User:</label>
			</td>
			<td>
				<input type="text" name="lastname"  required="true" value="<?php echo $user; ?>"></input>
			</td>
		</tr>
		<tr>
			<td>
				<label>Email</label>
			</td>
			<td>
				<input type="text" value="<?php echo $email; ?>" name="email"  required="true"></input>
			</td>
		</tr>
		<tr>
			<td>
				<label><label>Location Name</label>
			</td>
			<td>
				<input type="text" value="" name="label:Location_Name"  required="true"> <font size=1>(Please include TID if possible)</font>
			</td>
		</tr>
		<tr>
			<td>
				<label><label>Operator</label>
			</td>
			<td>
				<input type="text" value="" name="label:Operator"  required="true"></input>
			</td>
		</tr>
		<tr>
			<td>
				<label>Description (Limit 200 char.)</label>
			</td>
			<td>
				<textarea name="label:Description2" required="true" maxlength=200 rows=5 cols=40></textarea>
			</td>
		</tr>
		<tr>
			<td>
				<label>Date of Issue</label>
			</td>
			<td>
				<input type="text" value="<?php echo $date; ?>" class="datepicker" name="label:Date_of_Issue"  required="true"></input> <font size=1>(YYYY-MM-DD)</font>
			</td>
		</tr>
		<tr>
			<td>
				<label>Time of Issue</label>
			</td>
			<td>
				<input type="text" value="" class="timePicker" name="label:Time_of_Issue"  required="true"></input> <font size=1>(HH:MM:SS)</font>
			</td>
		</tr>
		<tr>
			<td>
				<label>Person making Initial Inquiry</label>
			</td>
			<td>
				<input type="text" value="" name="label:Person_making_Initial_Inquiry"  required="true"></input>
			</td>
		</tr>
		<tr>
			<td>
				<label>Type of Issue</label>
			</td>
			<td>
				<select name="label:Follow_Up[]" required="true">
		<option value="Support">Support</option>
		
		<option value="Incident">Incident</option>
		</select>
			</td>
		</tr>
		<tr>
			<td colspan=2 align=right>
				<input type="submit" value="Submit" ></input>
			</td>
		</tr>
	</table>
</form>
</body>
</html>