<?php

define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');

function money($diff){
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+4);
        $diff=round($diff, 2);
        if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
        elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
        $diff = substr($diff, 0, $dot+3);
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        return $diff;
}

function nextday($nextd,$day_format=''){ //Function returns next day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monn = substr($nextd, 5, 2);
   $dayn = substr($nextd, 8, 2);
   $yearn = substr($nextd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monn, $dayn+1, $yearn));
   return $tempdate;
}

function prevday($prevd,$day_format=''){ //Function returns previous day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monp = substr($prevd, 5, 2);
   $dayp = substr($prevd, 8, 2);
   $yearp = substr($prevd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monp, $dayp-1, $yearp));
   return $tempdate;
}

function dayofweek($date1){
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}

if ( !defined('DOC_ROOT') )
{
	define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
}
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');

///////VARIABLES
$arr = array("Y" => 7, "D" => 8, "N" => 9, "I" => 10);

///////mysql connect
mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");

$startdate = "2009-08-21";
$enddate = "2009-12-03";

echo "<table>";

$tempdate=$startdate;
echo "<tr><td></td>";
while($tempdate<=$enddate){

		echo "<td>$tempdate</td>";

		for($counter=1;$counter<=7;$counter++){$tempdate=nextday($tempdate);}
}
echo "</tr>";

foreach($arr AS $key => $value){
	$tempdate=$startdate;
	echo "<tr><td>$key</td>";
	while($tempdate<=$enddate){

		$query = "SELECT COUNT(commissionid) AS totalcount FROM labor_route_link WHERE date = '$tempdate' AND commissionid = '$value'";
		$result = mysql_query($query);

		$totalcount=@mysql_result($result,0,"totalcount");

		echo "<td align=right>$totalcount</td>";

		for($counter=1;$counter<=7;$counter++){$tempdate=nextday($tempdate);}
	}
	echo "</tr>";
}
echo "</table>";
///////END COMPANY LOOP

mysql_close();


echo "Done.";
?>