<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');

$today = date("Y-m-d");
$delete_date = date( "Y-m-d", mktime( 0, 0, 0, date( "m" ), date( "d" ) - 60, date( "Y" ) ) );

$query = "CALL get_hot_items()";
$result = Treat_DB_ProxyOldProcessHost::query( $query );

$query = "UPDATE menu_items_master mim2 SET mim2.ordertype = (SELECT order_type from hot_items where hot_items.UPC = mim2.upc group by upc)";
$result = Treat_DB_ProxyOldProcessHost::query( $query );

$query = "UPDATE menu_items_master mim2 SET mim2.sold = (SELECT sum(totals) from hot_items where hot_items.UPC = mim2.upc group by upc)";
$result = Treat_DB_ProxyOldProcessHost::query( $query );

///update default writer
$query = "DELETE FROM menu_item_master_hist WHERE date < '$delete_date'";
$result = Treat_DB_ProxyOld::query( $query );

$query = "SELECT Id, sold, ordertype FROM menu_items_master WHERE sold > 0";
$result = Treat_DB_ProxyOldProcessHost::query( $query );

while($r = mysql_fetch_array($result)){
	$Id = $r["Id"];
	$sold = $r["sold"];
	$ordertype = $r["ordertype"];

	$query2 = "UPDATE menu_items_master SET sold = $sold, ordertype = '$ordertype' WHERE Id = $Id";
	$result2 = Treat_DB_ProxyOld::query( $query2 );
	
	$query2 = "INSERT INTO menu_item_master_hist (Id,date,sold) VALUES ($Id,'$today',$sold) ON DUPLICATE KEY UPDATE sold = $sold";
	$result2 = Treat_DB_ProxyOld::query( $query2 );
}

//update rank overall
$counter = 1;

$query = "UPDATE menu_items_master SET rank = 0, category_rank = 0";
$result = Treat_DB_ProxyOld::query( $query );

$query = "SELECT Id FROM menu_items_master ORDER BY sold DESC LIMIT 100";
$result = Treat_DB_ProxyOldProcessHost::query( $query );    

while($r = mysql_fetch_array($result)){
	$Id = $r["Id"];

	$query2 = "UPDATE menu_items_master SET rank = $counter WHERE Id = $Id";
	$result2 = Treat_DB_ProxyOld::query( $query2 );

	$counter++;
}

///update category ranks
$categories = array(1,2,3,4,5);

foreach($categories AS $key => $category){
	$counter = 1;
	
	$query = "SELECT Id FROM menu_items_master WHERE ordertype = $category ORDER BY sold DESC LIMIT 100";
	$result = Treat_DB_ProxyOldProcessHost::query( $query );

	while($r = mysql_fetch_array($result)){
        	$Id = $r["Id"];

        	$query2 = "UPDATE menu_items_master SET category_rank = $counter WHERE Id = $Id";
        	$result2 = Treat_DB_ProxyOld::query( $query2 );

        	$counter++;
	}
}
?>
