<?php
if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../../' ) );
}
require_once( DOC_ROOT . '/bootstrap.php' );

if ( !defined( 'DEBUG_EMAIL' ) ) {
#	define( 'DEBUG_EMAIL', 'patrick@essential-elements.net' );
#	define( 'DEBUG_EMAIL', 'smartin@essentialpos.com' );
}
define( 'MAX_RECIPIENTS_PER_EMAIL', 100 );

$from_address = 'promos@companykitchen.com';

class Treat_Tmp_Mail extends Zend_Mail {
	public function getRecipients() {
		return $this->_recipients;
	}
	
	public function getBcc() {
		if ( array_key_exists( 'Bcc', $this->_headers ) ) {
			return $this->_headers['Bcc'];
		}
	}
}

class Treat_Tmp_Validate_EmailAddress extends Zend_Validate_EmailAddress {
	public function getErrors() {
		$rv = parent::getErrors();
		$this->resetErrors();
		return $rv;
	}
	
	
	public function getMessages() {
		$rv = parent::getMessages();
		$this->resetMessages();
		return $rv;
	}
	
	
	public function resetErrors() {
		$this->_errors = array();
	}
	
	
	public function resetMessages() {
		$this->_messages = array();
	}
}

///create message
$email_html = file_get_contents( DOC_ROOT . '/promo/company-kitchen/2011-08-29/email.html' );

$customerid = -1;
$list_of_customers = array();
/*
*/
$list_of_customers = Treat_Model_User_Customer::getCustomersByBusinessForEmailPromo(
	array(
#		307,
#		312,
#		332,
#		411,
		435,
	)
	,'Treat_ArrayObject'
);
/*
* /
$list_of_customers = Treat_Model_User_Customer::getCustomersByDistrictForEmailPromo(
	array(
		143
	)
	,'Treat_ArrayObject'
);

/*
* /
# uncomment this section to get some fake test data
$list_of_customers = array(
#	new Treat_ArrayObject( array (
#		'username' => 'bryanw@treatamerica.com',
#		'customerid' => $customerid--,
#	) ),
	new Treat_ArrayObject( array (
		'username' => 'peteb@treatamerica.com',
		'customerid' => $customerid--,
	) ),
	new Treat_ArrayObject( array (
		'username' => 'smartin@essentialpos.com',
		'customerid' => $customerid--,
	) ),
	new Treat_ArrayObject( array (
		'username' => 'patrick@essential-elements.net',
		'customerid' => $customerid--,
	) ),
);
/*
*/
$list_of_customers[] = new Treat_ArrayObject( array (
	'username' => 'smartin@essentialpos.com',
	'customerid' => $customerid--,
) );
$list_of_customers[] = new Treat_ArrayObject( array (
	'username' => 'patrick@essential-elements.net',
	'customerid' => $customerid--,
) );
$list_of_customers[] = new Treat_ArrayObject( array (
	'username' => 'patrick@whitelionsoft.com',
	'customerid' => $customerid--,
) );



$email_html = preg_replace(
	array(
		'@(src=["\'])[^"\']+?/([^/"\']+)(["\'])@i',
		'@90px\.gif@i',
	)
	,array(
		'$1http://treatamerica.essentialpos.com/promo/company-kitchen/2011-08-29/images/$2$3',
		'spacer.gif',
	)
	,$email_html
);

?>
<html>
	<head>
		<title>Sending CK Promo Email...</title>
		<style type="text/css">
			pre { border: 1px solid orange; }
			pre pre { border: 1px solid blue; }
		</style>
	</head>
	<body>
<?php
$success = array();
$problems = array();
////send emails
try {
	$email = new Treat_Tmp_Mail();
	$email->setFrom( $from_address );
	$email->addTo( $from_address );
	$email->setSubject( 'Company Kitchen Promotion Update' );
	$email->setBodyHtml( $email_html );
	
	$address_validator = new Treat_Tmp_Validate_EmailAddress();
	$pattern = array(
		'@ @',
		'@,@',
		'@\(@',
		'@\.commari$@',
		'@\.c0m$@',
		'@\.\.@',
		# theron doesn't exist & kills mail() function not allowing any other
		# BCC recipient to receive mail :(
#		'/theron@essential-elements.net/',
		'/@essential-elements.net/',
	);
	$replace = array(
		'',
		'.',
		'9',
		'.com',
		'.com',
		'.',
#		'theron@essentialpos.com',
		'@essentialpos.com',
	);
	$counter = 1;
	$total_emails = 0;
	$emails_sent_rv = array();
	$emails_sent_content = array();
	
	foreach ( $list_of_customers as $customer ) {
		$email_sent = false;
#		$customer->username = str_replace( ' ', '', $customer->username );
#		$customer->username = str_replace( ',', '.', $customer->username );
		$customer->username = preg_replace( $pattern, $replace, $customer->username );
		if ( $address_validator->isValid( $customer->username ) ) {
			//uncomment next 2 lines for test email
			$username = $customer->username;
			if ( defined( 'DEBUG_EMAIL' ) ) {
				$customer->username = DEBUG_EMAIL;
			}
			$email->addBcc( $customer->username );
			$success[] = array(
				'customer_id' => $customer->customerid,
#				'user' => $customer->username,
				'user' => $username,
				'messages' => $address_validator->getMessages(),
#				'errors' => $address_validator->getErrors(),
				'counter' => $counter,
			);
			$address_validator->resetErrors();
			if ( MAX_RECIPIENTS_PER_EMAIL == $counter ) {
				echo sprintf( '<pre>if "email" set, attempting to send email: %s</pre>', $counter );
				if ( array_key_exists( 'email', $_GET ) ) {
					try {
						$rv = $email->send();
#						$emails_sent_rv[] = var_export( $rv, true );
						# $email->send() returns $email or else throws an exception x.x
						$emails_sent_rv[] = var_export( true, true );
					}
					catch ( Zend_Mail_Exception $e ) {
						echo '<pre>';
						echo 'Error sending email: '
							,$e->getFile()
							,': '
							,'line number ', $e->getLine()
							,': '
							,'code ',$e->getCode()
							,': '
							,$e->getMessage()
							,PHP_EOL
						;
						var_dump( $e );
						echo '</pre>';
						$problems[] = array(
							'error' => 'problem occurred while attempting to send email',
							'customer_id' => $customer->customerid,
							'user' => $customer->username,
							'counter' => $counter,
						);
					}
				}
				$emails_sent_content[] = var_export( $email->getBcc(), true );
				$email_sent = true;
				$counter = 1;
				$total_emails++;
				$email->clearRecipients();
				$email->addTo( $from_address );
#				break;
			}
			else {
				$counter++;
			}
		}
		else {
			$problems[] = array(
				'customer_id' => $customer->customerid,
				'user' => $customer->username,
				'messages' => $address_validator->getMessages(),
#				'errors' => $address_validator->getErrors(),
			);
			$address_validator->resetErrors();
		}
	}
	
	if ( !$email_sent ) {
		echo sprintf( '<pre>if "email" set, attempting to send email: %s</pre>', $counter );
		if ( array_key_exists( 'email', $_GET ) ) {
			$rv = $email->send();
			$emails_sent_rv[] = var_export( $rv, true );
		}
		$emails_sent_content[] = var_export( $email->getBcc(), true );
		$total_emails++;
	}
}
catch ( Zend_Mail_Exception $e ) {
	echo 'Error sending email: '
		,$e->getFile()
		,':'
		,'line number ', $e->getLine()
		,':'
		,'code ',$e->getCode()
		,':'
		,$e->getMessage()
	;
}

?>

		<p>Total Emails: <?php echo var_export( $total_emails, true ); ?></p>
		<pre>$email->sent() = <?php echo implode( "</pre>\n\t\t<pre>\$email->sent() = ", $emails_sent_rv ); ?></pre>
		<pre>$problems = <?php var_dump( $problems ); ?></pre>
		<pre>$success = <?php var_dump( $success ); ?></pre>
		<pre>$email->getBcc() = <?php echo implode( "</pre>\n\t\t<pre>\$email->getBcc() = ", $emails_sent_content ); ?></pre>
<?php
/*
* /
?>
		<pre>$email = <?php var_dump( $email ); ?></pre>
<?php
/*
*/
?>

<?php
echo $email_html;
?>
	</body>
</html>