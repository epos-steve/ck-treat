<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');

		$ckfee_sql = "
			SELECT 
			fr2.businessid
			, fr2.businessname
			, fr2.companyid
			, c.companyname
			, fr2.ck_fee_percent
			, fr2.ck_monthly_maintenance_fee 
			FROM 
				mburris_report.financial_report_2_mockup fr2 
				INNER JOIN mburris_businesstrack.company c USING (companyid)
			WHERE 
				(ck_fee_percent = 0) OR (ck_monthly_maintenance_fee = 0) 
			ORDER BY 
				businessname ASC;
			";

		$ckfee_stmt = \EE\Model\Report\Process\FinancialReport::db()->prepare( $ckfee_sql );
		
		$ckfee_stmt->execute();
		$businesses_with_no_ckfee = $ckfee_stmt->fetchAll(\PDO::FETCH_OBJ);

		if ( is_array($businesses_with_no_ckfee) && count($businesses_with_no_ckfee) > 0 )
		{
			$message = "";
			foreach ( $businesses_with_no_ckfee as $no_ckfee )
			{
				$message .= $no_ckfee->companyname . " -- ";
				$message .= $no_ckfee->businessname . "<br />";
				$message .= "Current CK Fee Percent: " . $no_ckfee->ck_fee_percent . "%<br />";
				$message .= "Current CK Monthly Fee: $" . $no_ckfee->ck_monthly_maintenance_fee . "<br /><br />";
			}

			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= "From: support@companykitchen.com\r\n";
			$subject = $no_ckfee->companyname . " - CK-Fee Percent and Monthly CK-Fee Report";
			$email = "Sandrab@treatamerica.com,traceym@treatamerica.com";

			$email_msg = "<html><head></head><body>$message</body></html>";

			mail($email, $subject, $email_msg, $headers);
		}
		
		//echo $email_msg;

?>

