<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once DOC_ROOT . DIRECTORY_SEPARATOR . 'bootstrap.php';
$out = '<html><body>';

if ( ! empty($_FILES) )
{
	$update = array();
	// process the update file
	$file = tempnam(sys_get_temp_dir(), $_FILES['uploadedfile']['name']);
	if (move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $file))
	{
		$contents = file_get_contents($file);
		if ( '' === trim($contents) )
		{
			$out .= '<p>The upload file is empty. Please upload another file with the payroll.</p>';
			$out .= '</body></html>';
			echo $out;
			return;
		}
		$contents = str_replace(array("\r\n", "\r"), "\n", $contents);
		// Support enclosed newlines in csv file. A mistake perhaps since the
		// export most likely is from a machine and the csv will be standard.
		// An attempt to minimize mistakes.
		$payroll = str_getcsv($contents, "\n");
		$useTab = false;
		$payrollColumnsLine = array_shift($payroll);
		$columns = $columnsComma = str_getcsv( $payrollColumnsLine );
		$columnsTabs = str_getcsv( $payrollColumnsLine, "\t" );
		if ( count($columnsTabs) > $columnsComma )
		{
			$useTab = true;
			$columns = $columnsTabs;
		}

		$columns = array_map('strtolower', $columns);

		$mapped = array(
			'ee #' => 'employee number',
			'payrate' => 'pay rate',
			'rate eff date' => 'pay rate effective date',
			'rate desc' => 'pay rate description',
		);
		foreach ($mapped as $oldKey => $newKey)
		{
			$search = array_search($oldKey, $columns);
			if ( !!$search )
			{
				$columns[$search] = $newKey;
			}
		}

		foreach ($payroll as $data)
		{
			if ($useTab)
			{
				$info = str_getcsv($data, "\t");
			}
			else
			{
				$info = str_getcsv($data);
			}
			$array = array_combine($columns, $info);
			if ( strcasecmp($data['pay rate description'], 'rate 2') === 0 )
			{
				continue;
			}
			$update[] = $array;
		}
	}

	$sql_emps = '
		select
			jtd.*,
			jt.name,
			jt.hourly,
			l.firstname,
			l.lastname,
			l.empl_no,
			l.loginid
		from jobtypedetail jtd
		join login l on l.loginid = jtd.loginid
		join jobtype jt on jt.jobtypeid = jtd.jobtype
		where
			empl_no = :emp_no
			and jtd.is_deleted = 0
		order by jtd.jobtype
	';

	$summary = '';
	$updates = array();
	$unknowns = array();
	$redos = array();
	foreach ($update as $u)
	{
		$query = Treat_DB::singleton()->query($sql_emps, array(':emp_no' => $u['employee number']));
		$employee = $query->fetchAll(\PDO::FETCH_ASSOC);

		if (count($employee) == 1)
		{
			$e = $employee[0];
			if ($u['employee number'] == $e['empl_no'])
			{
				$diff = doubleval($u['pay rate']) - doubleval($e['rate']);
				if ($diff > 0)
				{
					$out .= sprintf(
						'<div><span>%s %s</span> as <i>%s</i> <b>WILL</b> be updated (Effective date: %s)</div>',
						$e['firstname'],
						$e['lastname'],
						$e['name'],
						date("Y-m-d", strtotime($u['pay rate effective date']))
					);
					$updates[] = array(
						'loginid' => $e['loginid'],
						'rate'    => $u['pay rate']
					);
				}
				else
				{
					$out .= sprintf(
						'<div><span>%s %s</span> as <i>%s</i> will not be updated. Rate mismatch:<span style="color:red">%s</span> (Effective date: %s)</div>',
						$e['firstname'],
						$e['lastname'],
						$e['name'],
						number_format($diff, 2),
						date("Y-m-d", strtotime($u['pay rate effective date']))
					);
				}
				$out .= "<hr>";
			}
		}
		else if (count($employee) > 1)
		{
			$redos[$employee[1]['empl_no']] = array(
				'name' => $employee[1]['firstname'] . ' ' . $employee[1]['lastname'],
				'job'  => $employee[1]['name']
			);
		}
		else if (count($employee) == 0)
		{
			$unknowns[$u['employee number']] = $u['employee number'];
		}
	}

	if (count($redos) > 0)
	{
		$summary .= sprintf('<div>The following employees will not be updated because of not enough unique info.<br />Please update "Rate Description" for job in Paycor and re-export.</div>', $e['firstname'], $e['lastname'], $e['name']);
		foreach ($redos as $n => $r)
		{
			$summary .= sprintf('<div><span>%s,</span><span> %s</span></div>', $r['name'], $r['job']);
		}
		$summary .= '<hr />';
		$out .= $summary;
	}

	if (count($unknowns) > 0)
	{
		$unk = '';
		$unk .= sprintf('<div>The following EE#\'s were not found in businesstracks.<br />');
		$unk .= implode('<br />', $unknowns);
		$unk .= '<hr />';
		$out .= $unk;
	}

	$_SESSION['updates'] = serialize($updates);

	$form = '';
	$form .= sprintf('<form method="post">');
	$form .= sprintf('Do you want to commit these changes?');
	$form .= sprintf('<input type="submit" name="updates" value=" Save ">');
	$form .= sprintf('</form>');
	$out .= $form;
}
else
{
	// perform the updates
	$updates = unserialize($_SESSION['updates']);
	foreach ($updates as $up)
	{
		$update = Treat_DB::singleton()->exec(
			'UPDATE `jobtypedetail` SET `rate` = :rate WHERE `loginid` = :loginid',
			array(
				'rate' => doubleval(str_replace('$', '', $up['rate'])),
				'loginid' => $up['loginid']
			)
		);
	}
	$out .= 'Completed';
}

$out .= '</body></html>';
echo $out;
