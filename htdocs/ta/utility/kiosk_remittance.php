<?php

function generateAP($code, $company_code, $master_vendor_id, $date){
	////CKL business
	$businessid = 328;
	$companyid = 7;
	
	$query = "SELECT companyname FROM company WHERE company_code = '$company_code'";
	$result = Treat_DB_ProxyOld::query($query);
	
	$companyname = mysql_result($result,0,"companyname");
	
	///master vendor info
	$query = "SELECT * FROM vendor_master WHERE vendor_master_id = $master_vendor_id";
	$result = Treat_DB_ProxyOld::query($query);
	
	$vendor_name = mysql_result($result,0,"vendor_name");
	$vendor_code = mysql_result($result,0,"vendor_code");
	
	///local vendor exists
	$query = "SELECT * FROM vendors WHERE businessid = $businessid AND master = $master_vendor_id";
	$result = Treat_DB_ProxyOld::query($query);
	
	$vendorid = mysql_result($result,0,"vendorid");
	
	if($vendorid < 1){
		///create local vendor
		$query = "INSERT INTO vendors (businessid,name,vendor_num,master) 
			VALUES ($businessid,'$vendor_name','$vendor_code',$master_vendor_id);";
		$result = Treat_DB_ProxyOld::query($query);
		$vendorid = mysql_insert_id();
	}
	
	///create apinvoice
	$invoicenum = str_replace("-","",$date);
	
	$query = "SELECT apinvoiceid,posted FROM apinvoice 
			WHERE invoicenum = '$invoicenum'
			AND master_vendor_id = $master_vendor_id";
	$result = Treat_DB_ProxyOld::query($query);
	
	$apinvoiceid = mysql_result($result,0,"apinvoiceid");
	$posted = mysql_result($result,0,"posted");
	
	if($posted == 1){return;}
	
	if($apinvoiceid < 1){
		$query = "INSERT INTO apinvoice (invoicenum,businessid,companyid,date,vendor,master_vendor_id)
					VALUES
					('$invoicenum',$businessid,$companyid,'$date',$vendorid,$master_vendor_id)";
		$result = Treat_DB_ProxyOld::query($query);
		
		$apinvoiceid = mysql_insert_id();
	}
	
	//apinvoice detail
	$invoice_total = 0;
	
	foreach($code AS $codes => $amount){
		if($codes == '52000'){
			$accountnum = '52000-0000-95000';
			$acct_name = "NAC Fee";
			$item = "NAC Fee";
		}
		else{
			$accountnum = "$codes-0000-$company_code";
			if($codes == "20830"){
				$acct_name = "$companyname AP";
				$item = "Sales/Tax/Promos/Refunds/Cash";
			}
			elseif($codes == "40522"){
				$acct_name = "$companyname Service Fee";
				$item = "Service Fee";
			}
			elseif($codes == "40523"){
				$acct_name = "$companyname Monthly Fee";
				$item = "Monthly Fee";
			}
			elseif($codes == "10800"){
				$acct_name = "$companyname CC Fee";
				$item = "Credit Card Fees";
			}
			elseif($codes == "12835"){
				$acct_name = "$companyname Vouchers";
				$item = "Vouchers";
			}
		}
		
		$amount = round($amount, 2);
		
		///accounts exists
		$query = "SELECT acct_payableid FROM acct_payable 
					WHERE companyid = $companyid
					AND businessid = $businessid
					AND accountnum = '$accountnum'";
		$result = Treat_DB_ProxyOld::query($query);
		
		$acct_payableid = mysql_result($result,0,"acct_payableid");
		
		if($acct_payableid < 1){
			$query = "INSERT INTO acct_payable (businessid,companyid,acct_name,accountnum)
						VALUES ($businessid,$companyid,'$acct_name','$accountnum')";
			$result = Treat_DB_ProxyOld::query($query);
			$acct_payableid = mysql_insert_id();
		}
		
		///apinvoice record
		$query = "SELECT itemid FROM apinvoicedetail WHERE apinvoiceid = $apinvoiceid AND apaccountid = $acct_payableid";
		$result = Treat_DB_ProxyOld::query($query);
		
		$itemid = mysql_result($result,0,"itemid");
		
		if($itemid < 1 && $amount != 0){
			$query = "INSERT INTO apinvoicedetail (apinvoiceid,amount,apaccountid,item)
						VALUES ($apinvoiceid,'$amount',$acct_payableid,'$item')";
			$result = Treat_DB_ProxyOld::query($query);
		}
		else{
			$query = "UPDATE apinvoicedetail SET amount = '$amount', item = '$item' WHERE itemid = $itemid";
			$result = Treat_DB_ProxyOld::query($query);
		}
		
		///invoice total
		$invoice_total += $amount;
	}
	
	///final invoice update
	$invoice_total = round($invoice_total, 2);
	
	$query =  "UPDATE apinvoice SET total = '$invoice_total', invtotal = '$invoice_total' WHERE apinvoiceid = $apinvoiceid";
	$result = Treat_DB_ProxyOld::query($query);
}

define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once( DOC_ROOT . '/../lib/PHPExcel/PHPExcel.php' );
require_once(DOC_ROOT.'/bootstrap.php');

$user = isset($_COOKIE["usercook"])?$_COOKIE["usercook"]:'';
$pass = isset($_COOKIE["passcook"])?$_COOKIE["passcook"]:'';

$query = "SELECT username,password,security_level FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=mysql_numrows($result);

$security_level = mysql_result($result,0,"security_level");

if($security_level < 7) exit;

$date1 = $_POST["date1"];

if($date1 == ""){
	$date1 = date("Y-m-d");
}

if(dayofweek($date1) == "Tuesday"){$createAP = 1;}
else{$createAP = 0;}

if(empty($_POST)){ ?>
	<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
	<html>
		<head>
			<title>Kiosk Remittance</title>
			<script type="text/javascript" src="../javascripts/jquery-1.4.2.min.js"></script>
			<script type="text/javascript" src="../javascripts/jquery-ui-1.7.2.custom.min.js"></script>
			<script type="text/javascript">$(document).ready(function (){$('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' });});</script>
			<link rel="stylesheet" href="../css/jq_themes/redmond/jquery-ui-1.7.2.custom.css">
		</head>
		<body>
			<p><img src="/assets/images/logo/ck.png"><br />				
				<b>Company Kitchen<br />8500 Shawnee Mission Parkway Ste 100<br />Merriam, Kansas 66202</b></p>
				
			<h2>Kiosk Remittance</h2>
			
			<form action="kiosk_remittance.php" method="post">
				<input type="text" name="date1" value="<?php print $date1; ?>" size="40" class="datepicker">
				<input type="submit" value="Download">
			</form>
			
			<p>
				AP invoices will be created if the date selected is a Tuesday.  You can create/update invoices for the week multiple times until the invoices are submitted (by downloading the remittance report again).  The invoices will be locked once they are submitted and can NOT be modified again.
			<p>	
			
			<h2>Credit Card Fees</h2>
			
			<form action="kiosk_cc_fee.php" method="post">
				<input type="text" name="date1" value="<?php print $date1; ?>" size="40" class="datepicker">
				<input type="submit" value="Download">
			</form>
			
		</body>
	</html>
	<?php
} else{
	setlocale(LC_MONETARY, 'en_US.UTF-8');

	$query = "SELECT business.businessid,business.businessname,district.districtname,company.companyname,company.week_end,company.master_vendor_id,company.company_code,business_setting.useLose FROM business
				JOIN company ON company.companyid = business.companyid
				JOIN district ON district.districtid = business.districtid
				LEFT JOIN business_setting ON business_setting.business_id = business.businessid
				WHERE business.categoryid = 4 ORDER BY company.companyname,business.districtid,business.businessname";
	$result = Treat_DB_ProxyOld::query($query);	

	//force download of xls file
	/*header( "Content-type: application/vnd.ms-excel" );
	header( "Content-Disposition: attachment; filename=kiosk_remittance_".$date1.".xls" );
	header( 'Cache-Control: max-age=0' );
	header( "Pragma: no-cache" );
	header( "Expires: 100" );*/
	
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="kiosk_remittance_'.$date1.'.xlsx"' );
	header('Cache-Control: max-age=0');
	header( "Pragma: no-cache" );
	header( "Expires: 100" );
	
	///credit card date
	$cc_date = $date1;
	while(dayofweek($cc_date) != "Friday"){
		$cc_date = prevday($cc_date);
	}

	$excel = array();
	$sheet_index = 0;
	$sheet_summary = 'summary';
	
	set_time_limit(0);

	$excel[$sheet_summary]['report_header'][] = 'Summary';
	$excel[$sheet_summary]['report_tab'][] = 'Summary';
	$excel[$sheet_summary]['location'][] = 'Location';
	$excel[$sheet_summary]['kiosks'][] = '#Kiosks';
	$excel[$sheet_summary]['gross_sales'][] = 'Net Sales';
	$excel[$sheet_summary]['service_fee'][] = 'Service Fee';
	$excel[$sheet_summary]['nac_fee'][] = 'NAC Fee';
	$excel[$sheet_summary]['tax'][] = 'Tax';
	$excel[$sheet_summary]['btl_deposit'][] = 'Btl Deposit';
	$excel[$sheet_summary]['monthly_chrg'][] = 'Monthly Chrg';
	$excel[$sheet_summary]['cc_tax_fee'][] = 'CC Trx Fee';
	$excel[$sheet_summary]['cash'][] = 'Cash';
	//$excel[$sheet_summary]['cust_promos'][] = 'Cust Promos';
	$excel[$sheet_summary]['refunds'][] = 'Refunds';
	$excel[$sheet_summary]['vouchers'][] = 'Vouchers';
	$excel[$sheet_summary]['net'][] = 'Net';
	$excel[$sheet_summary]['kiosk_sales'][] = 'Kiosk_Sales';
	$excel[$sheet_summary]['cafe_sales'][] = 'Cafe_Sales';
	$excel[$sheet_summary]['ck_card_promo'][] = 'CK_Card_Promo';
	$excel[$sheet_summary]['operator_promo'][] = 'Operator_Promo';
	$excel[$sheet_summary]['meal_tickets'][] = 'Meal_Tickets';

	while($r = mysql_fetch_array($result)){
		$businessid = $r["businessid"];
		$businessname = $r["businessname"];
		$districtname = $r["districtname"];
		$companyname = $r["companyname"];
		$week_end = $r["week_end"];
		$master_vendor_id = $r["master_vendor_id"];
		$company_code = $r["company_code"];
		$useLose = $r["useLose"];
		$kiosk_sales = 0;
		$cafe_sales = 0;
		$ck_card_promo = 0;
		$operator_promo = 0;
		$meal_tickets = 0;
		
		///figure week end
		$temp_date = prevday($date1);
		
		while(dayofweek($temp_date) != $week_end){
			$temp_date = prevday($temp_date);
		}
		
		$week_start = date("Y-m-d",mktime(0, 0, 0, substr($temp_date,5,2) , substr($temp_date,8,2)-6, substr($temp_date,0,4)));
		
		///figure 1st of month
		$month_start = substr($temp_date,0,7);
		$month_start = "$month_start-01";
		
		if($month_start >= $week_start && $month_start <= $temp_date){$charge_fee = 1;}
		else{$charge_fee = 0;}
	
		///district total
		if($lastdistrictname != $districtname && $lastdistrictname != ""){
			$d_sales = money_format('%(n', $d_sales);
			$d_fee = money_format('%(n', $d_fee);
			$d_efn_amt = money_format('%(n', $d_efn_amt);
			$d_tax = money_format('%(n', $d_tax);
			$d_btl_dpst = money_format('%(n', $d_btl_dpst);
			$d_mo_fee = money_format('%(n', $d_mo_fee);
			$d_cc_fee = money_format('%(n', $d_cc_fee);
			$d_cash = money_format('%(n', $d_cash);
			//$d_promos = money_format('%(n', $d_promos);
			$d_refunds = money_format('%(n', $d_refunds);
			$d_vouchers = money_format('%(n', $d_vouchers);
			$d_net = money_format('%(n', $d_net);
			$d_kiosk_sales = money_format('%(n', $d_kiosk_sales);
			$d_cafe_sales = money_format('%(n', $d_cafe_sales);
			$d_ck_card_promo = money_format('%(n', $d_ck_card_promo);
			$d_operator_promo = money_format('%(n', $d_operator_promo);
			$d_meal_tickets = money_format('%(n', $d_meal_tickets);

			$excel[$sheet_index]['location'][] = $lastdistrictname.'_@';  //mark row for styling in excel rendering
			$excel[$sheet_index]['kiosks'][] = $d_total;
			$excel[$sheet_index]['kiosk_sales'][] = $d_kiosk_sales;
			$excel[$sheet_index]['cafe_sales'][] = $d_cafe_sales;
			$excel[$sheet_index]['gross_sales'][] = $d_sales;
			$excel[$sheet_index]['service_fee'][] = $d_fee;
			$excel[$sheet_index]['nac_fee'][] = $d_efn_amt;
			$excel[$sheet_index]['btl_deposit'][] = $d_btl_dpst;
			$excel[$sheet_index]['tax'][] = $d_tax;
			$excel[$sheet_index]['monthly_chrg'][] = $d_mo_fee;
			$excel[$sheet_index]['cc_tax_fee'][] = $d_cc_fee;
			$excel[$sheet_index]['cash'][] = $d_cash;
			$excel[$sheet_index]['ck_card_promo'][] = $d_ck_card_promo;
			$excel[$sheet_index]['operator_promo'][] = $d_operator_promo;
			//$excel[$sheet_index]['cust_promos'][] = $d_promos;
			$excel[$sheet_index]['refunds'][] = $d_refunds;
			$excel[$sheet_index]['vouchers'][] = $d_vouchers;
			$excel[$sheet_index]['meal_tickets'][] = $d_meal_tickets;
			$excel[$sheet_index]['net'][] = $d_net;

			////clear totals
			$d_sales = 0;
			$d_fee = 0;
			$d_efn_amt = 0;
			$d_tax = 0;
			$d_btl_dpst = 0;
			$d_mo_fee = 0;
			$d_cc_fee = 0;
			$d_cash = 0;
			//$d_promos = 0;
			$d_refunds = 0;
			$d_vouchers = 0;
			$d_total = 0;
			$d_kiosk_sales = 0;
			$d_cafe_sales = 0;
			$d_ck_card_promo = 0;
			$d_operator_promo = 0;
			$d_meal_tickets = 0;

		}
	
		///company total
		if($lastcompanyname != $companyname){
			if($lastcompanyname != ""){
				///summary
				$ck_total += $c_total;
				$ck_sales += $c_sales;
				$ck_fee += $c_fee;
				$ck_efn_amt += $c_efn_amt;
				$ck_tax += $c_tax;
				$ck_btl_dpst += $c_btl_dpst;
				$ck_mo_fee += $c_mo_fee;
				$ck_cc_fee += $c_cc_fee;
				$ck_cash += $c_cash;
				$ck_promos += $c_promos;
				$ck_refunds += $c_refunds;
				$ck_vouchers += $c_vouchers;
				$ck_net += $c_net;
				$ck_kiosk_sales += $c_kiosk_sales;
				$ck_cafe_sales += $c_cafe_sales;
				$ck_ck_card_promo += $c_ck_card_promo;
				$ck_operator_promo += $c_operator_promo;
				$ck_meal_tickets += $c_meal_tickets;
				
				///create company AP
				if($createAP == 1){
					$code = array();
					$code['20830'] = round($c_sales + $c_tax + $c_btl_dpst + $c_cash + $c_promos + $c_refunds, 2);
					$code['40522'] = round($c_fee, 2);
					$code['52000'] = round($c_efn_amt, 2);
					$code['40523'] = round($c_mo_fee, 2);
					$code['10800'] = round($c_cc_fee, 2);
					$code['12835'] = round($c_vouchers, 2);
					
					if($master_vendor_id != 0 && $company_code != 0 && array_sum($code) != 0){
						generateAP($code,$company_code,$master_vendor_id,$date1);
					}
				}
			
				$c_sales = money_format('%(n', $c_sales);
				$c_fee = money_format('%(n', $c_fee);
				$c_efn_amt = money_format('%(n', $c_efn_amt);
				$c_tax = money_format('%(n', $c_tax);
				$c_btl_dpst = money_format('%(n', $c_btl_dpst);
				$c_mo_fee = money_format('%(n', $c_mo_fee);
				$c_cc_fee = money_format('%(n', $c_cc_fee);
				$c_cash = money_format('%(n', $c_cash);
				$c_promos = money_format('%(n', $c_promos);
				$c_refunds = money_format('%(n', $c_refunds);
				$c_vouchers = money_format('%(n', $c_vouchers);
				$c_net = money_format('%(n', $c_net);
				$c_kiosk_sales = money_format('%(n', $c_kiosk_sales);
				$c_cafe_sales = money_format('%(n', $c_cafe_sales);
				$c_ck_card_promo = money_format('%(n', $c_ck_card_promo);
				$c_operator_promo = money_format('%(n', $c_operator_promo);
				$c_meal_tickets = money_format('%(n', $c_meal_tickets);

				$excel[$sheet_index]['location'][] = $lastcompanyname.'_@';  //mark row for styling in excel rendering
				$excel[$sheet_index]['kiosks'][] = $c_total;
				$excel[$sheet_index]['kiosk_sales'][] = $c_kiosk_sales;
				$excel[$sheet_index]['cafe_sales'][] = $c_cafe_sales;
				$excel[$sheet_index]['gross_sales'][] = $c_sales;
				$excel[$sheet_index]['service_fee'][] = $c_fee;
				$excel[$sheet_index]['nac_fee'][] = $c_efn_amt;
				$excel[$sheet_index]['btl_deposit'][] = $c_btl_dpst;
				$excel[$sheet_index]['tax'][] = $c_tax;
				$excel[$sheet_index]['monthly_chrg'][] = $c_mo_fee;
				$excel[$sheet_index]['cc_tax_fee'][] = $c_cc_fee;
				$excel[$sheet_index]['cash'][] = $c_cash;
				$excel[$sheet_index]['ck_card_promo'][] = $c_ck_card_promo;
				$excel[$sheet_index]['operator_promo'][] = $c_operator_promo;
				//$excel[$sheet_index]['cust_promos'][] = $c_promos;
				$excel[$sheet_index]['refunds'][] = $c_refunds;
				$excel[$sheet_index]['vouchers'][] = $c_vouchers;
				$excel[$sheet_index]['meal_tickets'][] = $c_meal_tickets;
				$excel[$sheet_index]['net'][] = $c_net;
			
				$excel[$sheet_summary]['location'][] = $lastcompanyname;
				$excel[$sheet_summary]['kiosks'][] = $c_total;
				$excel[$sheet_summary]['kiosk_sales'][] = $c_kiosk_sales;
				$excel[$sheet_summary]['cafe_sales'][] = $c_cafe_sales;
				$excel[$sheet_summary]['gross_sales'][] = $c_sales;
				$excel[$sheet_summary]['service_fee'][] = $c_fee;
				$excel[$sheet_summary]['nac_fee'][] = $c_efn_amt;
				$excel[$sheet_summary]['btl_deposit'][] = $c_btl_dpst;
				$excel[$sheet_summary]['tax'][] = $c_tax;
				$excel[$sheet_summary]['monthly_chrg'][] = $c_mo_fee;
				$excel[$sheet_summary]['cc_tax_fee'][] = $c_cc_fee;
				$excel[$sheet_summary]['cash'][] = $c_cash;
				$excel[$sheet_summary]['ck_card_promo'][] = $c_ck_card_promo;
				$excel[$sheet_summary]['operator_promo'][] = $c_operator_promo;
				//$excel[$sheet_summary]['cust_promos'][] = $c_promos;
				$excel[$sheet_summary]['refunds'][] = $c_refunds;
				$excel[$sheet_summary]['vouchers'][] = $c_vouchers;
				$excel[$sheet_summary]['meal_tickets'][] = $c_meal_tickets;
				$excel[$sheet_summary]['net'][] = $c_net;

			}	
			
			$sheet_index++;
		
			$excel[$sheet_index]['report_header'][] = $companyname.' - Week Ending '.$temp_date;
			$excel[$sheet_index]['report_tab'][] = substr($companyname,0,28);
			$excel[$sheet_index]['location'][] = 'Location';
			$excel[$sheet_index]['kiosks'][] = '#Kiosks';
			$excel[$sheet_index]['kiosk_sales'][] = 'Kiosk_Sales';
			$excel[$sheet_index]['cafe_sales'][] = 'Cafe_Sales';
			$excel[$sheet_index]['gross_sales'][] = 'Net Sales';
			$excel[$sheet_index]['service_fee'][] = 'Service Fee';
			$excel[$sheet_index]['nac_fee'][] = 'NAC Fee';
			$excel[$sheet_index]['btl_deposit'][] = 'Btl Deposit';
			$excel[$sheet_index]['tax'][] = 'Tax';
			$excel[$sheet_index]['monthly_chrg'][] = 'Monthly Chrg';
			$excel[$sheet_index]['cc_tax_fee'][] = 'CC Trx Fee';
			$excel[$sheet_index]['cash'][] = 'Cash';
			$excel[$sheet_index]['ck_card_promo'][] = 'CK_Card_Promo';
			$excel[$sheet_index]['operator_promo'][] = 'Operator_Promo';
			//$excel[$sheet_index]['cust_promos'][] = 'Cust Promos';
			$excel[$sheet_index]['refunds'][] = 'Refunds';
			$excel[$sheet_index]['vouchers'][] = 'Vouchers';
			$excel[$sheet_index]['meal_tickets'][] = 'Meal_Tickets';
			$excel[$sheet_index]['net'][] = 'Net';

			///clear totals
			$c_sales = 0;
			$c_fee = 0;
			$c_efn_amt = 0;
			$c_tax = 0;
			$c_btl_dpst = 0;
			$c_mo_fee = 0;
			$c_cc_fee = 0;
			$c_cash = 0;
			$c_promos = 0;
			$c_refunds = 0;
			$c_vouchers = 0;
			$c_total = 0;
			$c_net = 0;
			$c_kiosk_sales = 0;
			$c_cafe_sales = 0;
			$c_ck_card_promo = 0;
			$c_operator_promo = 0;
			$c_meal_tickets = 0;
		}
	
		////Kiosk Sales
		$query2 = "SELECT SUM(creditdetail.amount) AS total_kiosk_sales FROM creditdetail
				JOIN credits ON credits.creditid = creditdetail.creditid AND credits.businessid = $businessid AND credits.category IN (8,15,19,20,26)
				JOIN mirror_accounts ON mirror_accounts.accountid = credits.creditid AND mirror_accounts.type = 1 AND mirror_accounts.category = 4 AND mirror_accounts.accountnum != -1
				WHERE creditdetail.date BETWEEN '$week_start' AND '$temp_date'";
		$result2 = Treat_DB_ProxyOld::query($query2);
		$kiosk_sales = round(mysql_result($result2,0,"total_kiosk_sales"), 2);

		////Cafe Sales
		$cafe_sales_query = "SELECT SUM(creditdetail.amount) AS total_cafe_sales FROM creditdetail
				JOIN credits ON credits.creditid = creditdetail.creditid AND credits.businessid = $businessid AND credits.category = 17
				JOIN mirror_accounts ON mirror_accounts.accountid = credits.creditid AND mirror_accounts.type = 1 AND mirror_accounts.category = 4 AND mirror_accounts.accountnum != -1
				WHERE creditdetail.date BETWEEN '$week_start' AND '$temp_date'";
		$cafe_sales_result = Treat_DB_ProxyOld::query($cafe_sales_query);
		$cafe_sales = round(mysql_result($cafe_sales_result,0,"total_cafe_sales"), 2);
		
		///remove promos from sales
		// $query2 = "SELECT SUM(debitdetail.amount) AS total_debits FROM debitdetail
		// 		JOIN debits ON debits.debitid = debitdetail.debitid AND debits.businessid = $businessid AND debits.category = 3
		// 		JOIN mirror_accounts ON mirror_accounts.accountid = debits.debitid AND mirror_accounts.type = 2 AND mirror_accounts.category = 4 AND mirror_accounts.accountnum != -1
		// 		WHERE debitdetail.date BETWEEN '$week_start' AND '$temp_date'";
		// $result2 = Treat_DB_ProxyOld::query($query2);

		//$sales -= round(mysql_result($result2,0,"total_debits"), 2);
	
		///Tax
		$query2 = "SELECT SUM(creditdetail.amount) AS total_tax FROM creditdetail
				JOIN credits ON credits.creditid = creditdetail.creditid AND credits.businessid = $businessid AND credits.category = 7
				JOIN mirror_accounts ON mirror_accounts.accountid = credits.creditid AND mirror_accounts.type = 1 AND mirror_accounts.category = 4 AND mirror_accounts.accountnum != -1
				WHERE creditdetail.date BETWEEN '$week_start' AND '$temp_date'";
		$result2 = Treat_DB_ProxyOld::query($query2);

		$tax = round(mysql_result($result2,0,"total_tax"), 2);
	
		///Bottle Deposits
		$query2 = "SELECT SUM(creditdetail.amount) AS total_dep FROM creditdetail
				JOIN credits ON credits.creditid = creditdetail.creditid AND credits.businessid = $businessid AND credits.category = 5
				JOIN mirror_accounts ON mirror_accounts.accountid = credits.creditid AND mirror_accounts.type = 1 AND mirror_accounts.category = 4 AND mirror_accounts.accountnum != -1
				WHERE creditdetail.date BETWEEN '$week_start' AND '$temp_date'";
		$result2 = Treat_DB_ProxyOld::query($query2);

		$btl_dpst = round(mysql_result($result2,0,"total_dep"), 2);
	
		///active kiosk/ck fee percent/efn/efn percent
		$query2 = "SELECT sd1.value AS active,
						sd2.value AS ck_fee_prcnt,
						sd3.value AS efn,
						sd4.value AS efn_prcnt,
						sd5.value AS monthly_fee,
						sd6.value AS online_cc_fee
					FROM setup_detail
					LEFT JOIN setup_detail sd1 ON sd1.setupid = 66 AND sd1.businessid = $businessid
					LEFT JOIN setup_detail sd2 ON sd2.setupid = 67 AND sd2.businessid = $businessid
					LEFT JOIN setup_detail sd3 ON sd3.setupid = 68 AND sd3.businessid = $businessid
					LEFT JOIN setup_detail sd4 ON sd4.setupid = 69 AND sd4.businessid = $businessid
					LEFT JOIN setup_detail sd5 ON sd5.setupid = 71 AND sd5.businessid = $businessid
					LEFT JOIN setup_detail sd6 ON sd6.setupid = 72 AND sd6.businessid = $businessid
					LIMIT 1";
		$result2 = Treat_DB_ProxyOld::query($query2);
	
		$active = mysql_result($result2,0,"active");
		$ck_fee_prcnt = mysql_result($result2,0,"ck_fee_prcnt");
		$efn = mysql_result($result2,0,"efn");
		$efn_prcnt = mysql_result($result2,0,"efn_prcnt");
		$monthly_fee = mysql_result($result2,0,"monthly_fee");
		
		// Online CC Fee
		//
		// Make sure that the fee is an integer (double actually) and if not
		// less than 1, then assume that it is a whole percent and divide by
		// 100;
		$online_cc_fee = doubleval(str_replace('%', '', mysql_result($result2, 0, 'online_cc_fee')));
		if( $online_cc_fee > 1 )
		{
			$online_cc_fee /= 100;
                        $online_cc_fee = round($online_cc_fee, 2);
		}
		
		///monthly charge
		if($charge_fee == 1 && $active == 1){
			$query2 = "SELECT value FROM setup_detail WHERE businessid = $businessid AND setupid = 41";
			$result2 = Treat_DB_ProxyOld::query($query2);

			$num_kiosks = mysql_result($result2,0,"value");

			$mo_fee = ($monthly_fee * -1) * $num_kiosks;
		}
		else{
			$mo_fee = 0;
		}
	
		///cc fee
		$query2 = "SELECT SUM(cc_fee.amount) AS total_fee FROM cc_fee
				WHERE cc_fee.businessid = $businessid AND cc_fee.date BETWEEN '$week_start' AND '$temp_date'";
		$result2 = Treat_DB_ProxyOld::query($query2);
		
		$cc_fee = round(mysql_result($result2,0,"total_fee") * -1, 2);
                
        ///online cc fee
		$query2 = "SELECT SUM(debitdetail.amount) AS total_fee FROM debitdetail
				JOIN debits ON debits.debitid = debitdetail.debitid AND debits.businessid = $businessid AND debits.category = 12
				JOIN mirror_accounts ON mirror_accounts.accountid = debits.debitid AND mirror_accounts.type = 2 AND mirror_accounts.accountnum != -1
				WHERE debitdetail.date BETWEEN '$week_start' AND '$temp_date'";
		$result2 = Treat_DB_ProxyOld::query($query2);
		$cc_fee = round((mysql_result($result2, 0, "total_fee") * $online_cc_fee * -1) + $cc_fee, 2);

		///Operator promos
		$query2 = "SELECT SUM(debitdetail.amount) AS total_operator_promotions FROM debitdetail
				JOIN debits ON debits.debitid = debitdetail.debitid AND debits.businessid = $businessid AND debits.category IN (3)
				JOIN mirror_accounts ON mirror_accounts.accountid = debits.debitid AND mirror_accounts.type = 2 AND mirror_accounts.accountnum != -1
				WHERE debitdetail.date BETWEEN '$week_start' AND '$temp_date'";
		$result2 = Treat_DB_ProxyOld::query($query2);
		$operator_promo = round(mysql_result($result2,0,"total_operator_promotions") * -1, 2);

		///5% fee
		if($active == 1){
			//$fee = round(($sales + $cc_fee) * ($ck_fee_prcnt / 100) * -1, 2);
			$fee = round(($kiosk_sales + $cc_fee + $operator_promo) * ($ck_fee_prcnt / 100) * -1, 2);
		}
		else{$fee = 0;}
		
		if ($fee > 0){
			$fee = 0;
		}
		
		////national account company
		if($efn == 1){
			//$efn_amt = round(($sales) * ($efn_prcnt / 100) * -1, 2);
			$efn_amt = round(($kiosk_sales) * ($efn_prcnt / 100) * -1, 2);
		}
		else{
			$efn_amt = 0;
		}
	
		///cash received
		$query2 = "SELECT SUM(debitdetail.amount) AS total_debits FROM debitdetail
				JOIN debits ON debits.debitid = debitdetail.debitid AND debits.businessid = $businessid AND debits.debittype = 3
				JOIN mirror_accounts ON mirror_accounts.accountid = debits.debitid AND mirror_accounts.type = 2 AND mirror_accounts.category = 4 AND mirror_accounts.accountnum != -1
				WHERE debitdetail.date BETWEEN '$week_start' AND '$temp_date'";
		$result2 = Treat_DB_ProxyOld::query($query2);

		$cash = round(mysql_result($result2,0,"total_debits") * -1, 2);

		///Online CK Card promos
		$query2_a = "SELECT SUM(debitdetail.amount) AS total_ck_card_promotions FROM debitdetail
				JOIN debits ON debits.debitid = debitdetail.debitid AND debits.businessid = $businessid AND debits.category IN (14)
				JOIN mirror_accounts ON mirror_accounts.accountid = debits.debitid AND mirror_accounts.type = 2 AND mirror_accounts.accountnum != -1
				WHERE debitdetail.date BETWEEN '$week_start' AND '$temp_date'";
		$result2_a = Treat_DB_ProxyOld::query($query2_a);
		$ck_card_promo = round(mysql_result($result2_a,0,"total_ck_card_promotions") * -1, 2);

		///cust refunds
		$query2 = "SELECT SUM(debitdetail.amount) AS total_debits FROM debitdetail
				JOIN debits ON debits.debitid = debitdetail.debitid AND debits.businessid = $businessid AND debits.category = 4
				JOIN mirror_accounts ON mirror_accounts.accountid = debits.debitid AND mirror_accounts.type = 2 AND mirror_accounts.accountnum != -1
				WHERE debitdetail.date BETWEEN '$week_start' AND '$temp_date'";
		$result2 = Treat_DB_ProxyOld::query($query2);

		$refunds = round(mysql_result($result2,0,"total_debits") * -1, 2);
		
		////vouchers
		$query2 = "SELECT SUM(debitdetail.amount) AS total_debits FROM debitdetail
				JOIN debits ON debits.debitid = debitdetail.debitid AND debits.businessid = $businessid AND debits.category = 6
				JOIN mirror_accounts ON mirror_accounts.accountid = debits.debitid AND mirror_accounts.type = 2 AND mirror_accounts.accountnum != -1
				WHERE debitdetail.date BETWEEN '$week_start' AND '$temp_date'";
		$result2 = Treat_DB_ProxyOld::query($query2);

		$vouchers = round(mysql_result($result2,0,"total_debits") * -1, 2);

		////Meal Tickets
		$query2 = "SELECT SUM(debitdetail.amount) AS total_meal_tickets FROM debitdetail
				JOIN debits ON debits.debitid = debitdetail.debitid AND debits.businessid = $businessid AND debits.category = 18
				JOIN mirror_accounts ON mirror_accounts.accountid = debits.debitid AND mirror_accounts.type = 2 AND mirror_accounts.accountnum != -1
				WHERE debitdetail.date BETWEEN '$week_start' AND '$temp_date'";
		$result2 = Treat_DB_ProxyOld::query($query2);
		$meal_tickets = round(mysql_result($result2,0,"total_meal_tickets") * -1, 2);

		////Use it or Lose it vouchers
		if($useLose == 1){
			$query2 = "SELECT SUM(amountSpent) AS vouchers FROM useLoseHistory 
					WHERE businessid = $businessid AND DATE(date2) >= '$week_start' AND DATE(date2) <= '$temp_date'";
			$result2 = Treat_DB_ProxyOld::query($query2);
			
			$vouchers += round(mysql_result($result2,0,"vouchers") * -1, 2);
		}
		
		$sql_num_kiosk = "select `value` AS 'num_of_kiosk' from setup_detail where setupid = 41 and businessid = $businessid";
		$result_num_kiosk = Treat_DB_ProxyOld::query($sql_num_kiosk);
		$num_of_kiosk = mysql_result($result_num_kiosk, 0, "num_of_kiosk");
		
		if ($num_of_kiosk > 0){
			$num_of_kiosk = $num_of_kiosk;
		} else {
			$num_of_kiosk = 0;
		}
		
		$sales = $kiosk_sales + $cafe_sales;
	
		///net due
		$net = round($sales + $fee + $efn_amt + $tax + $btl_dpst + $mo_fee + $cc_fee + $cash + $operator_promo + $ck_card_promo + $refunds + $vouchers + $meal_tickets, 2);
		
		////district totals
		$d_sales += $sales;
		$d_fee += $fee;
		$d_efn_amt += $efn_amt;
		$d_tax += $tax;
		$d_btl_dpst += $btl_dpst;
		$d_mo_fee += $mo_fee;
		$d_cc_fee += $cc_fee;
		$d_cash += $cash;
		$d_promos += $promos;
		$d_refunds += $refunds;
		$d_vouchers += $vouchers;
		$d_kiosk_sales += $kiosk_sales;
		$d_cafe_sales += $cafe_sales;
		$d_ck_card_promo += $ck_card_promo;
		$d_operator_promo += $operator_promo;
		$d_meal_tickets += $meal_tickets;

		if($sales != 0){
			//$d_total++;
			$d_total += $num_of_kiosk;
		}
		$d_net += $net;
	
		////company totals
		$c_sales += $sales;
		$c_fee += $fee;
		$c_efn_amt += $efn_amt;
		$c_tax += $tax;
		$c_btl_dpst += $btl_dpst;
		$c_mo_fee += $mo_fee;
		$c_cc_fee += $cc_fee;
		$c_cash += $cash;
		$c_promos += $promos;
		$c_refunds += $refunds;
		$c_vouchers += $vouchers;
		$c_kiosk_sales += $kiosk_sales;
		$c_cafe_sales += $cafe_sales;
		$c_ck_card_promo += $ck_card_promo;
		$c_operator_promo += $operator_promo;
		$c_meal_tickets += $meal_tickets;

		if($active == 1){$c_total++;}
		$c_net += $net;
		
		////create company AP
		if($createAP == 1){
			$code = array();
			$code['20830'] = round($c_sales + $c_tax + $c_btl_dpst + $c_cash + $c_promos + $c_refunds, 2);
			$code['40522'] = round($c_fee, 2);
			$code['52000'] = round($c_efn_amt, 2);
			$code['40523'] = round($c_mo_fee, 2);
			$code['10800'] = round($c_cc_fee, 2);
			$code['12835'] = round($c_vouchers, 2);
					
			if($master_vendor_id != 0 && $company_code != 0 && array_sum($code) != 0){
				generateAP($code,$company_code,$master_vendor_id,$date1);
			}
		}
		
		///output
		$sales = money_format('%(n', $sales);
		$fee = money_format('%(n', $fee);
		$efn_amt = money_format('%(n', $efn_amt);
		$tax = money_format('%(n', $tax);
		$btl_dpst = money_format('%(n', $btl_dpst);
		$mo_fee = money_format('%(n', $mo_fee);
		$cc_fee = money_format('%(n', $cc_fee);
		$cash = money_format('%(n', $cash);
		$promos = money_format('%(n', $promos);
		$refunds = money_format('%(n', $refunds);
		$vouchers = money_format('%(n', $vouchers);
		$net = money_format('%(n', $net);
		$kiosk_sales = money_format('%(n', $kiosk_sales);
		$cafe_sales = money_format('%(n', $cafe_sales);
		$ck_card_promo = money_format('%(n', $ck_card_promo);
		$operator_promo = money_format('%(n', $operator_promo);
		$meal_tickets = money_format('%(n', $meal_tickets);

		$excel[$sheet_index]['location'][] = $businessname;
		//$excel[$sheet_index]['kiosks'][] = '';
		$excel[$sheet_index]['kiosks'][] = $num_of_kiosk;
		$excel[$sheet_index]['kiosk_sales'][] = $kiosk_sales;
		$excel[$sheet_index]['cafe_sales'][] = $cafe_sales;
		$excel[$sheet_index]['gross_sales'][] = $sales;
		$excel[$sheet_index]['service_fee'][] = $fee;
		$excel[$sheet_index]['nac_fee'][] = $efn_amt;
		$excel[$sheet_index]['btl_deposit'][] = $btl_dpst;
		$excel[$sheet_index]['tax'][] = $tax;
		$excel[$sheet_index]['monthly_chrg'][] = $mo_fee;
		$excel[$sheet_index]['cc_tax_fee'][] = $cc_fee;
		$excel[$sheet_index]['cash'][] = $cash;
		$excel[$sheet_index]['ck_card_promo'][] = $ck_card_promo;
		$excel[$sheet_index]['operator_promo'][] = $operator_promo;
		//$excel[$sheet_index]['cust_promos'][] = $promos;
		$excel[$sheet_index]['refunds'][] = $refunds;
		$excel[$sheet_index]['vouchers'][] = $vouchers;
		$excel[$sheet_index]['meal_tickets'][] = $meal_tickets;
		$excel[$sheet_index]['net'][] = $net;

		$lastdistrictname = $districtname;
		$lastcompanyname = $companyname;
	}  //end while

	///last district
	$d_sales = money_format('%(n', $d_sales);
	$d_fee = money_format('%(n', $d_fee);
	$d_efn_amt = money_format('%(n', $d_efn_amt);
	$d_tax = money_format('%(n', $d_tax);
	$d_btl_dpst = money_format('%(n', $d_btl_dpst);
	$d_mo_fee = money_format('%(n', $d_mo_fee);
	$d_cc_fee = money_format('%(n', $d_cc_fee);
	$d_cash = money_format('%(n', $d_cash);
	$d_promos = money_format('%(n', $d_promos);
	$d_refunds = money_format('%(n', $d_refunds);
	$d_vouchers = money_format('%(n', $d_vouchers);
	$d_net = money_format('%(n', $d_net);
	$d_kiosk_sales = money_format('%(n', $d_kiosk_sales);
	$d_cafe_sales = money_format('%(n', $d_cafe_sales);
	$d_ck_card_promo = money_format('%(n', $d_ck_card_promo);
	$d_operator_promo = money_format('%(n', $d_operator_promo);
	$d_meal_tickets = money_format('%(n', $d_meal_tickets);

	$excel[$sheet_index]['location'][] = $districtname.'_@';  //mark row for styling in excel rendering
	$excel[$sheet_index]['kiosks'][] = $d_total;
	$excel[$sheet_index]['kiosk_sales'][] = $d_kiosk_sales;
	$excel[$sheet_index]['cafe_sales'][] = $d_cafe_sales;
	$excel[$sheet_index]['gross_sales'][] = $d_sales;
	$excel[$sheet_index]['service_fee'][] = $d_fee;
	$excel[$sheet_index]['nac_fee'][] = $d_efn_amt;
	$excel[$sheet_index]['btl_deposit'][] = $d_btl_dpst;
	$excel[$sheet_index]['tax'][] = $d_tax;
	$excel[$sheet_index]['monthly_chrg'][] = $d_mo_fee;
	$excel[$sheet_index]['cc_tax_fee'][] = $d_cc_fee;
	$excel[$sheet_index]['cash'][] = $d_cash;
	$excel[$sheet_index]['ck_card_promo'][] = $d_ck_card_promo;
	$excel[$sheet_index]['operator_promo'][] = $d_operator_promo;
	//$excel[$sheet_index]['cust_promos'][] = $d_promo;
	$excel[$sheet_index]['refunds'][] = $d_refund;
	$excel[$sheet_index]['vouchers'][] = $d_vouchers;
	$excel[$sheet_index]['meal_tickets'][] = $d_meal_tickets;
	$excel[$sheet_index]['net'][] = $d_net;
	
	///clear last district totals
	$d_sales = 0;
	$d_fee = 0;
	$d_efn_amt = 0;
	$d_tax = 0;
	$d_btl_dpst = 0;
	$d_mo_fee = 0;
	$d_cc_fee = 0;
	$d_cash = 0;
	$d_promos = 0;
	$d_refunds = 0;
	$d_vouchers = 0;
	$d_total = 0;
	$d_kiosk_sales = 0;
	$d_cafe_sales = 0;
	$d_ck_card_promo = 0;
	$d_operator_promo = 0;
	$d_meal_tickets = 0;

	///summary
	$ck_total += $c_total;
	$ck_sales += $c_sales;
	$ck_fee += $c_fee;
	$ck_efn_amt += $c_efn_amt;
	$ck_tax += $c_tax;
	$ck_btl_dpst += $c_btl_dpst;
	$ck_mo_fee += $c_mo_fee;
	$ck_cc_fee += $c_cc_fee;
	$ck_cash += $c_cash;
	$ck_promos += $c_promos;
	$ck_refunds += $c_refunds;
	$ck_vouchers += $c_vouchers;
	$ck_net += $c_net;
	$ck_kiosk_sales += $c_kiosk_sales;
	$ck_cafe_sales += $c_cafe_sales;
	$ck_ck_card_promo += $c_ck_card_promo;
	$ck_operator_promo += $c_operator_promo;
	$ck_meal_tickets += $c_meal_tickets;


	///last company
	$c_sales = money_format('%(n', $c_sales);
	$c_fee = money_format('%(n', $c_fee);
	$c_efn_amt = money_format('%(n', $c_efn_amt);
	$c_tax = money_format('%(n', $c_tax);
	$c_btl_dpst = money_format('%(n', $c_btl_dpst);
	$c_mo_fee = money_format('%(n', $c_mo_fee);
	$c_cc_fee = money_format('%(n', $c_cc_fee);
	$c_cash = money_format('%(n', $c_cash);
	$c_promos = money_format('%(n', $c_promos);
	$c_refunds = money_format('%(n', $c_refunds);
	$c_vouchers = money_format('%(n', $c_vouchers);
	$c_net = money_format('%(n', $c_net);
	$c_kiosk_sales = money_format('%(n', $c_kiosk_sales);
	$c_cafe_sales = money_format('%(n', $c_cafe_sales);
	$c_ck_card_promo = money_format('%(n', $c_ck_card_promo);
	$c_operator_promo = money_format('%(n', $c_operator_promo);
	$c_meal_tickets = money_format('%(n', $c_meal_tickets);
			
	$excel[$sheet_index]['location'][] = $companyname.'_@';  //mark row for styling in excel rendering
	$excel[$sheet_index]['kiosks'][] = $c_total;
	$excel[$sheet_index]['kiosk_sales'][] = $c_kiosk_sales;
	$excel[$sheet_index]['cafe_sales'][] = $c_cafe_sales;
	$excel[$sheet_index]['gross_sales'][] = $c_sales;
	$excel[$sheet_index]['service_fee'][] = $c_fee;
	$excel[$sheet_index]['nac_fee'][] = $c_efn_amt;
	$excel[$sheet_index]['btl_deposit'][] = $c_btl_dpst;
	$excel[$sheet_index]['tax'][] = $c_tax;
	$excel[$sheet_index]['monthly_chrg'][] = $c_mo_fee;
	$excel[$sheet_index]['cc_tax_fee'][] = $c_cc_fee;
	$excel[$sheet_index]['cash'][] = $c_cash;
	$excel[$sheet_index]['ck_card_promo'][] = $c_ck_card_promo;
	$excel[$sheet_index]['operator_promo'][] = $c_operator_promo;
	//$excel[$sheet_index]['cust_promos'][] = $c_promo;
	$excel[$sheet_index]['refunds'][] = $c_refund;
	$excel[$sheet_index]['vouchers'][] = $c_vouchers;
	$excel[$sheet_index]['meal_tickets'][] = $c_meal_tickets;
	$excel[$sheet_index]['net'][] = $c_net;

	$excel[$sheet_summary]['location'][] = $companyname;
	$excel[$sheet_summary]['kiosks'][] = $c_total;
	$excel[$sheet_summary]['kiosk_sales'][] = $c_kiosk_sales;
	$excel[$sheet_summary]['cafe_sales'][] = $c_cafe_sales;
	$excel[$sheet_summary]['gross_sales'][] = $c_sales;
	$excel[$sheet_summary]['service_fee'][] = $c_fee;
	$excel[$sheet_summary]['nac_fee'][] = $c_efn_amt;
	$excel[$sheet_summary]['btl_deposit'][] = $c_btl_dpst;
	$excel[$sheet_summary]['tax'][] = $c_tax;
	$excel[$sheet_summary]['monthly_chrg'][] = $c_mo_fee;
	$excel[$sheet_summary]['cc_tax_fee'][] = $c_cc_fee;
	$excel[$sheet_summary]['cash'][] = $c_cash;
	$excel[$sheet_summary]['ck_card_promo'][] = $c_ck_card_promo;
	$excel[$sheet_summary]['operator_promo'][] = $c_operator_promo;
	//$excel[$sheet_summary]['cust_promos'][] = $c_promo;
	$excel[$sheet_summary]['refunds'][] = $c_refund;
	$excel[$sheet_summary]['vouchers'][] = $c_vouchers;
	$excel[$sheet_summary]['meal_tickets'][] = $c_meal_tickets;
	$excel[$sheet_summary]['net'][] = $c_net;
	
	$ck_sales = money_format('%(n', $ck_sales);
	$ck_fee = money_format('%(n', $ck_fee);
	$ck_efn_amt = money_format('%(n', $ck_efn_amt);
	$ck_tax = money_format('%(n', $ck_tax);
	$ck_btl_dpst = money_format('%(n', $ck_btl_dpst);
	$ck_mo_fee = money_format('%(n', $ck_mo_fee);
	$ck_cc_fee = money_format('%(n', $ck_cc_fee);
	$ck_cash = money_format('%(n', $ck_cash);
	$ck_promos = money_format('%(n', $ck_promos);
	$ck_refunds = money_format('%(n', $ck_refunds);
	$ck_vouchers = money_format('%(n', $ck_vouchers);
	$ck_net = money_format('%(n', $ck_net);	
	$ck_kiosk_sales = money_format('%(n', $ck_kiosk_sales);	
	$ck_cafe_sales = money_format('%(n', $ck_cafe_sales);	
	$ck_ck_card_promo = money_format('%(n', $ck_ck_card_promo);	
	$ck_operator_promo = money_format('%(n', $ck_operator_promo);	
	$ck_meal_tickets = money_format('%(n', $ck_meal_tickets);	
			
	$excel[$sheet_summary]['location'][] = 'Company Kitchen'.'_@';  //mark row for styling in excel rendering
	$excel[$sheet_summary]['kiosks'][] = $ck_total;
	$excel[$sheet_summary]['kiosk_sales'][] = $ck_kiosk_sales;
	$excel[$sheet_summary]['cafe_sales'][] = $ck_cafe_sales;
	$excel[$sheet_summary]['gross_sales'][] = $ck_sales;
	$excel[$sheet_summary]['service_fee'][] = $ck_fee;
	$excel[$sheet_summary]['nac_fee'][] = $ck_efn_amt;
	$excel[$sheet_summary]['btl_deposit'][] = $ck_btl_dpst;
	$excel[$sheet_summary]['tax'][] = $ck_tax;
	$excel[$sheet_summary]['monthly_chrg'][] = $ck_mo_fee;
	$excel[$sheet_summary]['cc_tax_fee'][] = $ck_cc_fee;
	$excel[$sheet_summary]['cash'][] = $ck_cash;
	$excel[$sheet_summary]['ck_card_promo'][] = $ck_ck_card_promo;
	$excel[$sheet_summary]['operator_promo'][] = $ck_operator_promo;
	//$excel[$sheet_summary]['cust_promos'][] = $ck_promos;
	$excel[$sheet_summary]['refunds'][] = $ck_refunds;
	$excel[$sheet_summary]['vouchers'][] = $ck_vouchers;
	$excel[$sheet_summary]['meal_tickets'][] = $ck_meal_tickets;
	$excel[$sheet_summary]['net'][] = $ck_net;

	$xls = new PHPExcel();

	$xls->getProperties()->setCreator('Essential Elements')
		->setLastModifiedBy('Essential Elements')->setTitle('Kiosk Remittance')
		->setSubject('Kiosk Remittance')->setDescription('Kiosk Remittance')
		->setKeywords('Kiosk Remittance');

	$tab_index = 0;
	$worksheet = array();

	$header_style = array(
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'startcolor' => array(
				'argb' => 'FF00CCFF',
			),
		),
	);

	$title_style = array(
		'font' => array(
			'bold' => true
		),
	);

	$highlighted_style = array(
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'startcolor' => array(
				'argb' => 'FFC0C0C0',
			),
		),
	);
	
	$excel[ count($excel) ] = $excel['summary'];
	unset($excel['summary']);
	
	$offset = 4;  //offset between excel array index and row number
	
	/* iterate over array to create tabs */	
	foreach($excel as $tab){
		$worksheet[$tab_index] = $xls->createSheet();  //create worksheet
		
		$xls->setActiveSheetIndex($tab_index);  //make new worksheet active
		
		/* iterate over rows */
		foreach($tab['location'] as $xls_key=>$xls_value){
			$highlight = false;
		
			if($xls_key == 0){
				$worksheet[$tab_index]->setTitle( $tab['report_tab'][$xls_key] );  //set tab title				
				$worksheet[$tab_index]->setCellValue( 'A3', $tab['report_header'][$xls_key] );  //title of report
				$worksheet[$tab_index]->mergeCells('A3:G3');				
				$worksheet[$tab_index]->getStyle('A3:G3')->applyFromArray($title_style);
				
				//autosize all columns
				$worksheet[$tab_index]->getColumnDimension('A')->setAutoSize(true);
				$worksheet[$tab_index]->getColumnDimension('B')->setAutoSize(true);
				$worksheet[$tab_index]->getColumnDimension('C')->setAutoSize(true);
				$worksheet[$tab_index]->getColumnDimension('D')->setAutoSize(true);
				$worksheet[$tab_index]->getColumnDimension('E')->setAutoSize(true);
				$worksheet[$tab_index]->getColumnDimension('F')->setAutoSize(true);
				$worksheet[$tab_index]->getColumnDimension('G')->setAutoSize(true);
				$worksheet[$tab_index]->getColumnDimension('H')->setAutoSize(true);
				$worksheet[$tab_index]->getColumnDimension('I')->setAutoSize(true);
				$worksheet[$tab_index]->getColumnDimension('J')->setAutoSize(true);
				$worksheet[$tab_index]->getColumnDimension('K')->setAutoSize(true);
				$worksheet[$tab_index]->getColumnDimension('L')->setAutoSize(true);
				$worksheet[$tab_index]->getColumnDimension('M')->setAutoSize(true);
				$worksheet[$tab_index]->getColumnDimension('N')->setAutoSize(true);
				$worksheet[$tab_index]->getColumnDimension('O')->setAutoSize(true);
				$worksheet[$tab_index]->getColumnDimension('P')->setAutoSize(true);
				$worksheet[$tab_index]->getColumnDimension('Q')->setAutoSize(true);
				$worksheet[$tab_index]->getColumnDimension('R')->setAutoSize(true);
				//$worksheet[$tab_index]->getColumnDimension('S')->setAutoSize(true);
				
				$worksheet[$tab_index]->getStyle('A4:S4')->applyFromArray($header_style);				
				
				$logo = new PHPExcel_Worksheet_Drawing();
				$logo->setPath(DOC_ROOT.'/assets/images/logo/ck.png');
				$logo->setWidth('100%');
				$logo->setHeight('100%');
				$worksheet[$tab_index]->getRowDimension(1)->setRowHeight(115);
				$logo->setCoordinates('A1');
				$logo->setWorksheet($worksheet[$tab_index]);
				
				$worksheet[$tab_index]->mergeCells('A1:C1');
				$worksheet[$tab_index]->setCellValue('A1', "Company Kitchen\n8500 Shawnee Mission Parkway Ste 100\nMerriam, Kansas 66202");
				
				$worksheet[$tab_index]->getStyle('A1')->applyFromArray($title_style);
			}		
		
			//insert cell data		
			if( strrpos($tab['location'][$xls_key], '_@') ){
				$worksheet[$tab_index]->setCellValue( 'A'.($xls_key+$offset), substr($tab['location'][$xls_key],0,strlen($tab['location'][$xls_key])-2 ) );  //drop '_@' from cell
				$highlight = true;
			} else {
				$worksheet[$tab_index]->setCellValue( 'A'.($xls_key+$offset), $tab['location'][$xls_key] );
			}
			
			$worksheet[$tab_index]->setCellValue( 'B'.($xls_key+$offset), $tab['kiosks'][$xls_key] );
			$worksheet[$tab_index]->setCellValue( 'C'.($xls_key+$offset), $tab['kiosk_sales'][$xls_key] );
			$worksheet[$tab_index]->setCellValue( 'D'.($xls_key+$offset), $tab['cafe_sales'][$xls_key] );
			$worksheet[$tab_index]->setCellValue( 'E'.($xls_key+$offset), $tab['gross_sales'][$xls_key] );
			$worksheet[$tab_index]->setCellValue( 'F'.($xls_key+$offset), $tab['service_fee'][$xls_key] );
			$worksheet[$tab_index]->setCellValue( 'G'.($xls_key+$offset), $tab['nac_fee'][$xls_key] );
			$worksheet[$tab_index]->setCellValue( 'H'.($xls_key+$offset), $tab['btl_deposit'][$xls_key] );
			$worksheet[$tab_index]->setCellValue( 'I'.($xls_key+$offset), $tab['tax'][$xls_key] );
			$worksheet[$tab_index]->setCellValue( 'J'.($xls_key+$offset), $tab['monthly_chrg'][$xls_key] );
			$worksheet[$tab_index]->setCellValue( 'K'.($xls_key+$offset), $tab['cc_tax_fee'][$xls_key] );
			$worksheet[$tab_index]->setCellValue( 'L'.($xls_key+$offset), $tab['cash'][$xls_key] );
			$worksheet[$tab_index]->setCellValue( 'M'.($xls_key+$offset), $tab['ck_card_promo'][$xls_key] );
			$worksheet[$tab_index]->setCellValue( 'N'.($xls_key+$offset), $tab['operator_promo'][$xls_key] );
			//$worksheet[$tab_index]->setCellValue( 'O'.($xls_key+$offset), $tab['cust_promos'][$xls_key] );
			$worksheet[$tab_index]->setCellValue( 'O'.($xls_key+$offset), $tab['refunds'][$xls_key] );
			$worksheet[$tab_index]->setCellValue( 'P'.($xls_key+$offset), $tab['vouchers'][$xls_key] );
			$worksheet[$tab_index]->setCellValue( 'Q'.($xls_key+$offset), $tab['meal_tickets'][$xls_key] );
			$worksheet[$tab_index]->setCellValue( 'R'.($xls_key+$offset), $tab['net'][$xls_key] );
			
			if($highlight){
				$worksheet[$tab_index]->getStyle( 'A'.($xls_key+$offset).':R'.($xls_key+$offset) )->applyFromArray($highlighted_style);
			}
		} //end foreach
	
		$tab_index++;  //increment for next tab
	}  //end foreach
	
	$xls->removeSheetByIndex(0);  //remove blank worksheet

	$xls->setActiveSheetIndex(0);
	
	//$xlsWriter = PHPExcel_IOFactory::createWriter( $xls, 'Excel5' );  //output excel file to screen for download
	$xlsWriter = PHPExcel_IOFactory::createWriter( $xls, 'Excel2007' );  //output excel file to screen for download
	$xlsWriter->save( 'php://output' );
	
}  //end else, we have POST vars
?>
