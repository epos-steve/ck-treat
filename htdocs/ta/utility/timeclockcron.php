<?php


if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../../' ) );
}
require_once( DOC_ROOT . '/bootstrap.php' );

function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}

function nextday($nextd,$day_format=''){ //Function returns next day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monn = substr($nextd, 5, 2);
   $dayn = substr($nextd, 8, 2);
   $yearn = substr($nextd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monn, $dayn+1, $yearn));
   return $tempdate;
}

function prevday($prevd,$day_format=''){ //Function returns previous day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monp = substr($prevd, 5, 2);
   $dayp = substr($prevd, 8, 2);
   $yearp = substr($prevd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monp, $dayp-1, $yearp));
   return $tempdate;
}

$today=date("Y-m-d");
$starttime="$today 00:00:00";
$endtime="$today 23:59:59";
$second="00";

///////////////////////AUTO CLOCK OUT

$query = "SELECT * FROM labor_clockin_options WHERE clockout = '1'";
$result = Treat_DB_ProxyOld::query($query);
$num=mysql_numrows($result);

$num--;
while($num>=0){
   $continue=0;

   $loginid=@mysql_result($result,$num,"loginid");
   $clockout_time=@mysql_result($result,$num,"clockout_time");

   if(stripos($clockout_time,"a")>0){$am=1;}
   elseif(stripos($clockout_time,"p")>0){$am=2;}
   else{$continue=1;}

   if($continue!=1){

      $clockout_time=str_replace("a","",$clockout_time);
      $clockout_time=str_replace("p","",$clockout_time);
      $clockout_time=explode(":",$clockout_time);

      $minute=$clockout_time[1];

      if($am==1&&$clockout_time[0]==12){$hour="23";$minute="59";$second="59";}
      elseif($am==1){$hour=$clockout_time[0];}
      elseif($am==2&&$clockout_time[0]==12){$hour=12;}
      else{$hour=$clockout_time[0]+12;}

      if(strlen($hour)<2){$hour="0$hour";}

      $clockout="$today $hour:$minute:$second";

      $query2 = "SELECT * FROM labor_clockin WHERE loginid = '$loginid' AND clockin >= '$starttime' AND clockin <= '$endtime' AND clockout = '0000-00-00 00:00:00'";
      $result2 = Treat_DB_ProxyOld::query($query2);
      $num2=mysql_numrows($result2);

      if($num2>0){
         $num2--;
         while($num2>=0){
            $clockinid=@mysql_result($result2,$num2,"clockinid");

            $query3 = "UPDATE labor_clockin SET clockout = '$clockout' WHERE clockinid = '$clockinid'";
            $result3 = Treat_DB_ProxyOld::query($query3);

            $num2--;
         }
      }
   }

   $num--;
}

///////////////////////AUTO LUNCH BREAK

$todayname=dayofweek($today);

if($todayname=="Sunday"){$query = "SELECT * FROM labor_clockin_options WHERE lunch_sun = '1'";}
elseif($todayname=="Monday"){$query = "SELECT * FROM labor_clockin_options WHERE lunch_mon = '1'";}
elseif($todayname=="Tuesday"){$query = "SELECT * FROM labor_clockin_options WHERE lunch_tue = '1'";}
elseif($todayname=="Wednesday"){$query = "SELECT * FROM labor_clockin_options WHERE lunch_wed = '1'";}
elseif($todayname=="Thursday"){$query = "SELECT * FROM labor_clockin_options WHERE lunch_thu = '1'";}
elseif($todayname=="Friday"){$query = "SELECT * FROM labor_clockin_options WHERE lunch_fri = '1'";}
elseif($todayname=="Saturday"){$query = "SELECT * FROM labor_clockin_options WHERE lunch_sat = '1'";}

$result = Treat_DB_ProxyOld::query($query);
$num=mysql_numrows($result);

$num--;
while($num>=0){
   $continue=0;

   $loginid=@mysql_result($result,$num,"loginid");

   $myquery = "SELECT firstname,lastname FROM login WHERE loginid = $loginid";
   $myresult = Treat_DB_ProxyOld::query($myquery);

   $firstname = mysql_result($myresult,0,"firstname");
   $lastname = mysql_result($myresult,0,"lastname");

   if($todayname=="Sunday"){$lunch_break=@mysql_result($result,$num,"lunch_sun_time");}
   elseif($todayname=="Monday"){$lunch_break=@mysql_result($result,$num,"lunch_mon_time");}
   elseif($todayname=="Tuesday"){$lunch_break=@mysql_result($result,$num,"lunch_tue_time");}
   elseif($todayname=="Wednesday"){$lunch_break=@mysql_result($result,$num,"lunch_wed_time");}
   elseif($todayname=="Thursday"){$lunch_break=@mysql_result($result,$num,"lunch_thu_time");}
   elseif($todayname=="Friday"){$lunch_break=@mysql_result($result,$num,"lunch_fri_time");}
   elseif($todayname=="Saturday"){$lunch_break=@mysql_result($result,$num,"lunch_sat_time");}

   $query2 = "SELECT * FROM labor_clockin WHERE loginid = '$loginid' AND clockin >= '$starttime' AND clockin <= '$endtime' AND clockout != '0000-00-00 00:00:00' AND is_deleted = '0'";
   $result2 = Treat_DB_ProxyOld::query($query2);
   $num2=mysql_numrows($result2);

   if($num2==1){
      $clockinid=@mysql_result($result2,0,"clockinid");
      $clockin=@mysql_result($result2,0,"clockin");
      $clockout=@mysql_result($result2,0,"clockout");
      $businessid=@mysql_result($result2,0,"businessid");
      $jobtype=@mysql_result($result2,0,"jobtype");

      $query3="SELECT TimeDiff(clockout,clockin) AS totalamt FROM labor_clockin WHERE clockinid = '$clockinid'";
      $result3 = Treat_DB_ProxyOld::query($query3);

      $totalamt=@mysql_result($result3,0,"totalamt");

      $totalamt=round($totalamt/2,0);

      $temp_date=$today;
      $starthour=substr($clockin,11,2);
      $startmin=substr($clockin,14,2);
      while($totalamt!=0){$starthour++; $totalamt--; if($starthour==24){$starthour="00";$temp_date=nextday($temp_date);}}

      if(strlen($starthour)<2){$starthour="0$starthour";}
      $newclockout="$temp_date $starthour:$startmin:00";

      $query3="UPDATE labor_clockin SET clockout = '$newclockout' WHERE clockinid = '$clockinid'";
      $result3 = Treat_DB_ProxyOld::query($query3);

      $endmin=$startmin;
      $breakmins=$lunch_break*60;
      for($counter=1;$counter<=$breakmins;$counter++){$endmin++;if($endmin==60){$endmin=0;$starthour++;if($starthour==24){$starthour=0;$temp_date=nextday($temp_date);}}}

      $newclockin="$temp_date $starthour:$endmin:00";

      $query3="INSERT INTO labor_clockin (loginid,businessid,clockin,clockout,jobtype) VALUES ('$loginid','$businessid','$newclockin','$clockout','$jobtype')";
      $result3 = Treat_DB_ProxyOld::query($query3);

	  $data .= "Created lunch for $firstname $lastname<br>";
   }
   else{
	  $data .= "$firstname $lastname skipped. Number of clockins: $num2<br>";
   }

   $num--;
}

///////////auto punches
$today=date("Y-m-d");
$today=nextday($today);
if(dayofweek($today)!="Saturday"&&dayofweek($today)!="Sunday"){
	$query = "SELECT * FROM labor_clockin_options WHERE auto_punch = '1'";
	$result = Treat_DB_ProxyOld::query($query);

	while($r=mysql_fetch_array($result)){
		$loginid = $r["loginid"];
		$auto_punch_hours = $r2["auto_punch_hours"];

		$query2 = "SELECT oo2 FROM login WHERE loginid = '$loginid'";
		$result2 = Treat_DB_ProxyOld::query($query2);

		$businessid = @mysql_result($result2,0,"oo2");

		$query2 = "SELECT jobtype FROM jobtypedetail WHERE loginid = '$loginid' AND is_deleted = '0' LIMIT 0,1";
		$result2 = Treat_DB_ProxyOld::query($query2);

		$jobtype = @mysql_result($result2,0,"jobtype");

		$punch = "$today 08:00:00";
		$punch2 = "$today 08:00:00";
		if($auto_punch_hours > 0){$punch2 = "$today 16:00:00";}

		$query2 = "INSERT INTO labor_clockin (loginid,businessid,clockin,clockout,jobtype) VALUES ('$loginid','$businessid','$punch','$punch2','$jobtype')";
		$result2 = Treat_DB_ProxyOld::query($query2);

	}
}
/////send email
	$email = "smartin@essentialpos.com.test-google-a.com";
	$subject = "Auto Lunches";
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	$headers .= "From: alerts@treatamerica.com\r\n";
	mail($email, $subject, $data, $headers);
echo "Done";
?>
