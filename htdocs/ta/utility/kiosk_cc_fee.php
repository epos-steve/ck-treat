<?php

////cc_fees

define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');

$date1 = $_POST["date1"];

$districts = array();
$companies = array();

if( $date1 != '' ){
	$download_data .= "\"Operator\",\"District\",BusinessID,Category,\"Merchant#\",\"Business\",Kiosk CC,Heartland Fee,Online CC,Online Fee,Total Fee,Period Start,Period End\r\n";

	$query = "SELECT business.businessid,business.businessname,business_category.category_name,district.districtname,company.companyname,company.week_end,sd6.value AS online_cc_fee,sd7.value AS merchant FROM business
					JOIN company ON company.companyid = business.companyid
					JOIN district ON district.districtid = business.districtid
					LEFT JOIN business_category ON business_category.categoryid = business.categoryid
					LEFT JOIN setup_detail sd6 ON sd6.setupid = 72 AND sd6.businessid = business.businessid
					LEFT JOIN setup_detail sd7 ON sd7.setupid = 65 AND sd7.businessid = business.businessid
					ORDER BY company.companyname,business.districtid,business.businessname";
	$result = Treat_DB_ProxyOld::query($query);	

	while($r = mysql_fetch_array($result)){
		$businessid = $r["businessid"];
		$businessname = $r["businessname"];
		$category_name = $r["category_name"];
		$districtname = $r["districtname"];
		$companyname = $r["companyname"];
		$week_end = $r["week_end"];
		$online_cc_fee = $r["online_cc_fee"];
		$merchant = $r["merchant"];
		
		////totals
		if($lastdistrictname != $districtname && in_array($lastdistrictname,$districts)){
			$download_data .= "\"$lastdistrictname TOTALS\",,,,,,,,,,$d_total,,\r\n";
			$d_total = 0;
		}
		if($lastcompanyname != $companyname && in_array($lastcompanyname,$companies)){
			$download_data .= "\"$lastcompanyname TOTALS\",,,,,,,,,,$c_total,,\r\n\r\n\r\n";
			$c_total = 0;
		}
		
		///figure week
		$temp_date = $date1;
		while(dayofweek($temp_date) != $week_end){
			$temp_date = prevday($temp_date);
		}
		$week_start = date("Y-m-d",mktime(0, 0, 0, substr($temp_date,5,2) , substr($temp_date,8,2)-6, substr($temp_date,0,4)));
		
		///kiosk cc purchase
		$query2 = "SELECT SUM(debitdetail.amount) AS cc_purch FROM debitdetail
				JOIN debits ON debits.debitid = debitdetail.debitid AND debits.businessid = $businessid AND debits.category IN (1,11)
				WHERE debitdetail.date BETWEEN '$week_start' AND '$temp_date'";
		$result2 = Treat_DB_ProxyOld::query($query2);
		
		$kiosk_cc_amt = round(@mysql_result($result2,0,"cc_purch"), 2);
		
		///heartland fee
		$query2 = "SELECT SUM(cc_fee.amount) AS total_fee FROM cc_fee
				WHERE cc_fee.businessid = $businessid AND cc_fee.date BETWEEN '$week_start' AND '$temp_date'";
		$result2 = Treat_DB_ProxyOld::query($query2);
		
		$cc_fee = round(@mysql_result($result2,0,"total_fee"), 2);
		
		///online cc fee
		$query2 = "SELECT SUM(debitdetail.amount) AS total_fee FROM debitdetail
				JOIN debits ON debits.debitid = debitdetail.debitid AND debits.businessid = $businessid AND debits.category = 12
				WHERE debitdetail.date BETWEEN '$week_start' AND '$temp_date'";
		$result2 = Treat_DB_ProxyOld::query($query2);

		$online_cc_amt = round(@mysql_result($result2,0,"total_fee"), 2);
		$online_cc_fee = round((@mysql_result($result2, 0, "total_fee") * ( $online_cc_fee / 100 )), 2);
		
		$total_fee = round(($cc_fee + $online_cc_fee) , 2);
		
		if($cc_fee != 0 || $online_cc_fee != 0){
			
			///error checking
			if($kiosk_cc_amt != 0 && $cc_fee == 0 && $category_name == "Kiosk"){
				$errors .= "\"*$businessname has kiosk activity and no fees.\"\r\n";
			}
			if($online_cc_amt != 0 && $online_cc_fee == 0 && $category_name == "Kiosk"){
				$errors .= "\"*$businessname has online activity and no fees.\"\r\n";
			}
			if($kiosk_cc_amt == 0 && $cc_fee != 0 && $category_name == "Kiosk"){
				$errors .= "\"*$businessname has fees and NO kiosk activity.\"\r\n";
			}
			if($online_cc_amt == 0 && $online_cc_fee != 0 && $category_name == "Kiosk"){
				$errors .= "\"*$businessname has fees and NO online activity.\"\r\n";
			}
			
			$districts[] = $districtname;
			$companies[] = $companyname;
			
			$c_total += $total_fee;
			$d_total += $total_fee;
			$a_total += $total_fee;
			
			$a_kiosk_cc_amt += $kiosk_cc_amt;
			$a_cc_fee += $cc_fee;
			$a_online_cc_amt += $online_cc_amt;
			$a_online_cc_fee += $online_cc_fee;
			
			$download_data .= "\"$companyname\",\"$districtname\",$businessid,$category_name,\"$merchant\",\"$businessname\",$kiosk_cc_amt,$cc_fee,$online_cc_amt,$online_cc_fee,$total_fee,$week_start,$temp_date\r\n";
		}
	
		$lastcompanyname = $companyname;
		$lastdistrictname = $districtname;
		
	}
	if( in_array($districtname,$districts) ){
		$download_data .= "\"$districtname TOTALS\",,,,,,,,,,$d_total,,\r\n";
	}
	if( in_array($companyname,$companies) ){
		$download_data .= "\"$companyname TOTALS\",,,,,,,,,,$c_total,,\r\n\r\n\r\n";
	}
	
	$download_data .= "\"CK Totals\",,,,,,$a_kiosk_cc_amt,$a_cc_fee,$a_online_cc_amt,$a_online_cc_fee,$a_total,,\r\n";
	
	$download_data = "$errors\r\n$download_data";
	
	$file="cc_report.csv";
	header("Content-type: application/x-msdownload");
	header("Content-Disposition: attachment; filename=$file");
	header("Pragma: no-cache");
	header("Expires: 100");
	echo $download_data;
}
?>