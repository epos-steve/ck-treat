<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once DOC_ROOT . DIRECTORY_SEPARATOR . 'bootstrap.php';

$businessid = $_POST["businessid"];
$companyid = $_POST["companyid"];
$orderid = $_POST["orderid"];
$ordertype = $_POST["ordertype"];

$success = 0;

if ( ! empty($_FILES) )
{
	// process the update file
	$file = tempnam(sys_get_temp_dir(), $_FILES['fileinput']['name']);
	if (move_uploaded_file($_FILES['fileinput']['tmp_name'], $file))
	{
		$contents = file_get_contents($file);
		
		$contents = explode("\r\n", $contents);
		
		foreach($contents AS $line){
			$line = explode(",", $line);
			
			$qty = $line[0];
			$item_code = $line[1];
			$new_cost = $line[2];
			
			$query = "SELECT min.id, mip.cost FROM menu_items_new min 
				JOIN menu_items_price mip ON mip.menu_item_id = min.id
				WHERE min.businessid = $businessid AND min.item_code = '$item_code' AND min.ordertype = $ordertype";
			$result = Treat_DB_ProxyOld::query( $query );
			
			$id = @mysql_result($result,0,"id");
			$cost = @mysql_result($result,0,"cost");
			
			if($id > 0){
				$success++;
				
				if($new_cost != $cost){
					$new_cost = round($new_cost, 2);
				}
				else{
					$new_cost = "NULL";
				}
				
				$query = "INSERT INTO machine_orderdetail (orderid,menu_items_new_id,item_code,qty,new_cost)
					VALUES ($orderid,$id,'$item_code','$qty',$new_cost)
					ON DUPLICATE KEY UPDATE qty = '$qty', new_cost = $new_cost";
				$result = Treat_DB_ProxyOld::query( $query );
			}
		}
	}
}
header('Location: ' . $_SERVER['HTTP_REFERER'] . "&success=" . $success);
