<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');

$today = date("Y-m-d");
$lastdate = date("Y-m-d",mktime(0, 0, 0, date("m") , date("d")-6, date("Y")));

$query = "select company.companyid,
			company.companyname,
			count(DISTINCT business.businessid) AS running
			FROM business
			JOIN credits ON credits.businessid = business.businessid
			JOIN creditdetail ON creditdetail.creditid = credits.creditid AND creditdetail.date BETWEEN '$lastdate' AND '$today'
			JOIN company ON company.companyid = business.companyid
			WHERE business.categoryid = 4
			GROUP by company.companyid";
$result = DB_Proxy::query($query);

while($r = mysql_fetch_array($result)){
	$company[$r['companyid']] = $r['running'];
	$company_name[$r['companyid']] = $r['companyname'];
}

$query = "select business.companyid,
			COUNT(business.businessid) AS total
			FROM business WHERE business.categoryid = 4
			GROUP BY business.companyid";
$result = DB_Proxy::query($query);

while($r = mysql_fetch_array($result)){
	$company_total[$r['companyid']] = $r['total'];
}
	
echo "<table>";
echo "<tr><td>Company</td><td>Running</td><td>Ordered</td><td>Total</td></tr>";
foreach($company_name AS $key => $value){
	$not_running = $company_total[$key] - $company[$key];
	echo "<tr><td>$value</td><td>$company[$key]</td><td>$not_running</td><td>$company_total[$key]</td></tr>";
}
echo "</table>";
?>
