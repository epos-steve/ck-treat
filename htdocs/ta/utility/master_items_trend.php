<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');

ini_set("display_errors",false);

///clear messages
$query = "DELETE FROM menu_item_master_trend";
$result = Treat_DB_ProxyOld::query( $query );

///day change increases
$date1 = date( "Y-m-d", mktime( 0, 0, 0, date( "m" ), date( "d" ) - 1, date( "Y" ) ) );
$date2 = date("Y-m-d");

$query = "SELECT Id, Name, ordertype, UPC, rank, category_rank from menu_items_master WHERE sold > 250 ORDER BY sold DESC";
$result = Treat_DB_ProxyOld::query( $query );

while($r = mysql_fetch_array($result)){
	$id = $r["Id"];
	$name = $r["Name"];
	$ordertype = $r["ordertype"];
	$upc = $r["UPC"];
	$rank = $r["rank"];
	$category_rank = $r["category_rank"];
	
	$query2 = "SELECT * FROM menu_item_master_hist WHERE Id = $id ORDER BY date DESC LIMIT 60";
	$result2 = Treat_DB_ProxyOld::query( $query2 );
	
	$counter = 1;
	$itemData = array();
	$daysIncrease = 0;
	$daysDecrease = 0;
	while($r2 = mysql_fetch_array($result2)){
		$curDate = $r2["date"];
		$sold = $r2["sold"];
		
		$itemData[$counter] = $sold;
		
		///days increasing/decreasing
		if($counter == 1){
		
		}
		elseif($sold < $itemData[$counter-1] && $daysDecrease == 0){
			$daysIncrease++;
			if($counter == 2){$daysIncrease++;}
		}
		elseif($sold > $itemData[$counter-1] && $daysIncrease == 0){
			$daysDecrease++;
			if($counter == 2){$daysDecrease++;}
		}
		else{
			break;
		}
		
		$counter++;
	}
	
	if($daysIncrease >= 5 || $daysDecrease >= 5){
		if($daysIncrease > 0){
			if($daysIncrease % 2){
				$days = $daysIncrease - 1;
			}
			else{
				$days = $daysIncrease;
			}
		}
		else{
			if($daysDecrease % 2){
				$days = $daysDecrease - 1;
			}
			else{
				$days = $daysDecrease;
			}
		}
		
		///figure % change
		$period1 = array_sum(array_slice($itemData,$days/2,$days/2));
		$period2 = array_sum(array_slice($itemData,0,$days/2));
		
		$percent = round(($period2 - $period1) / $period1 * 100, 2); 
		$name = str_replace("'","`",$name);
		
		if($percent >= 10){
			//echo "$id, $name, $daysIncrease, <font color=red>$daysDecrease</font>, $days,$period1,$period2, $percent%<br>";
			$query2 = "INSERT INTO menu_item_master_trend (category,details) VALUES ('Trend','<font color=green onclick=\"graphItem($id,\'$name\');\">$name +$percent% over the last $days days.</font>')";
			$result2 = Treat_DB_ProxyOld::query( $query2 );
		}
		elseif($percent <= -10){
			$query2 = "INSERT INTO menu_item_master_trend (category,details) VALUES ('Trend','<font color=red onclick=\"graphItem($id,\'$name\');\">$name $percent% over the last $days days.</font>')";
			$result2 = Treat_DB_ProxyOld::query( $query2 );
		}
	}
}

//new to market
$query = "select mim.Id, 
		mim.Name, 
		count(mih.Id) as total, 
		mih.date, 
		SUM(mih.sold) as sold 
	FROM menu_item_master_hist mih
	JOIN menu_items_master mim ON mim.Id = mih.Id
	GROUP BY mim.Id
	HAVING total < 10";
$result = Treat_DB_ProxyOld::query( $query );

while($r = mysql_fetch_array($result)){
	$id = $r["Id"];
	$name = $r["Name"];
	$sold = $r["sold"];
	$total = $r["total"];
	$avg = round($sold / $total);
	
	if($avg >= 50){
		$query2 = "INSERT INTO menu_item_master_trend (category,details) VALUES ('New Item','<font color=black onclick=\"graphItem($id,\'$name\');\">New to market: $name</font>')";
		$result2 = Treat_DB_ProxyOld::query( $query2 );
	}
}
?>