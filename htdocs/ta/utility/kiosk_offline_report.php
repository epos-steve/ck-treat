<?php

////look for offline/incorrect schedule kiosks

define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');

function whoIs($queryInput){

    // create a new cURL resource
    $ch = curl_init();

    // set URL and other appropriate options
    curl_setopt($ch, CURLOPT_URL, 'http://whois.arin.net/rest/ip/' . $queryInput);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));

    // execute
    $returnValue = curl_exec($ch);

    // close cURL resource, and free up system resources
    curl_close($ch);

    $result = json_decode($returnValue);
    
    return $result;
}

function fetchPrev($date, $client_programid){
    $query2 = "SELECT * FROM offline_logging 
        WHERE client_programid = $client_programid 
            AND datetime_value < '$date 00:00:00'
        ORDER BY valueid DESC
        LIMIT 1";
    $result2 = Treat_DB_ProxyOld::query($query2);
    
    $new_status = mysql_result($result2,0,"new_status");
    $datetime_value = mysql_result($result2,0,"datetime_value");
    
    $value = "$new_status|$datetime_value";
    return $value;
}

$query = "USE mburris_manage";
$result = Treat_DB_ProxyOld::query_both($query);

////dates
$dates = array();
for($counter=4;$counter>=0;$counter--){
    $dates[$counter] = date("Y-m-d",mktime(0, 0, 0, date("m"), date("d")-($counter), date("Y")));
}

$query = "SELECT cp.client_programid,
		cp.name,
		cp.client_ip,
		cp.sync_version,
		cpb.name AS parent,
        COUNT(ol.valueid) AS total
	FROM client_programs cp
		JOIN client_programs cpb ON cpb.client_programid = cp.parent
                JOIN offline_logging ol ON ol.client_programid = cp.client_programid 
                    AND ol.datetime_value BETWEEN '$dates[4] 00:00:00' AND '$dates[0] 00:00:00'
	WHERE cp.pos_type = 7 
            AND cp.client_ip != '72.22.219.4'
            AND cp.client_ip != '72.22.219.5'
            AND cp.client_ip != '74.62.165.207'
        GROUP BY cp.client_programid
	ORDER BY total DESC, cp.parent";
$result = Treat_DB_ProxyOld::query($query);

echo "<table cellspacing=0 cellpadding=1 style=\"border:1px solid black;\"><tr>
    <td colspan=6 style=\"border:1px solid black;\">&nbsp;</td><td colspan=4 style=\"border:1px solid black;\">$dates[3]</td><td colspan=4 style=\"border:1px solid black;\">$dates[2]</td><td colspan=4 style=\"border:1px solid black;\">$dates[1]</td><td colspan=4 style=\"border:1px solid black;\">$dates[0]</td><td colspan=4 style=\"border:1px solid black;\">&nbsp;</td></tr>";
echo "<tr>
    <td style=\"border:1px solid black;\">ID</td>
    <td style=\"border:1px solid black;\">Location</td>
    <td style=\"border:1px solid black;\">Operator</td>
    <td style=\"border:1px solid black;\">ARIN</td>
    <td style=\"border:1px solid black;\">REF</td>
	<td style=\"border:1px solid black;\">Version</td>
    <td style=\"border:1px solid black;\">#Offline</td>
    <td style=\"border:1px solid black;\">Emails</td>
    <td style=\"border:1px solid black;\">Hours</td>
    <td style=\"border:1px solid black;\">%</td>
    <td style=\"border:1px solid black;\">#Offline</td>
    <td style=\"border:1px solid black;\">Emails</td>
    <td style=\"border:1px solid black;\">Hours</td>
    <td style=\"border:1px solid black;\">%</td>
    <td style=\"border:1px solid black;\">#Offline</td>
    <td style=\"border:1px solid black;\">Emails</td>
    <td style=\"border:1px solid black;\">Hours</td>
    <td style=\"border:1px solid black;\">%</td>
    <td style=\"border:1px solid black;\">#Offline</td>
    <td style=\"border:1px solid black;\">Emails</td>
    <td style=\"border:1px solid black;\">Hours</td>
    <td style=\"border:1px solid black;\">%</td>
    <td colspan=4 style=\"border:1px solid black;\">TOTALS</td></tr>";

while($r = mysql_fetch_array($result)){
	$name = $r["name"];
	$client_programid = $r["client_programid"];
        $client_ip = $r["client_ip"];
		$sync_version = $r["sync_version"];
        $parent = $r["parent"];
        $tot_offline = $r["total"];
        $total_online = 0;
        $total_offline = 0;
        $total_offline_count = 0;
        $total_email_sent = 0;
        
        echo "<tr>
            <td style=\"border:1px solid black;\">$client_programid</td>
            <td style=\"border:1px solid black;\">$name</td>
            <td style=\"border:1px solid black;\">$parent</td>"; 
        
        $data = whoIs($client_ip);
        
        echo "<td style=\"border:1px solid black;\"><font size=2>{$data->net->name->{'$'}}</font></td>
        <td style=\"border:1px solid black;\"><font size=2>{$data->net->orgRef->{'@name'}}</font></td>
		<td style=\"border:1px solid black;\"><font size=2>$sync_version</td>
        ";
	
        $graph = array();
        
        for($counter=4;$counter>0;$counter--){
            
            $query2 = "SELECT * FROM offline_logging 
                        WHERE client_programid = $client_programid 
                            AND datetime_value BETWEEN '{$dates[$counter]} 00:00:00' AND '{$dates[$counter-1]} 00:00:00'
                        ORDER BY valueid";
            $result2 = Treat_DB_ProxyOld::query($query2);
            
            $start_stat = fetchPrev($dates[$counter], $client_programid);
            $start_stat = explode("|", $start_stat);
            $lasttime = "$dates[$counter] 00:00:00";
            $new_status = $start_stat[0];
            
            if($new_status == ""){
                $new_status = "OFFLINE";
            }
            
            $endtime = "{$dates[$counter-1]} 00:00:00";
            $online = 0;
            $offline = 0;
            $offline_count = 0;
            $email_sent = 0;
            
            $graph[$counter] .= "$new_status    $lasttime       <br>";
            
            while($r = mysql_fetch_array($result2)){
                $new_status = $r["new_status"];
                $datetime_value = $r["datetime_value"];
                
                $query3 = "SELECT TIMEDIFF('$datetime_value', '$lasttime') AS amount";
                $result3 = Treat_DB_ProxyOld::query($query3);
                
                $amount = mysql_result($result3, 0, "amount");
              
                //turn into minutes
                $pieces = explode(':', $amount);
                $pieces[0] *= 60;
                $pieces[1] += $pieces[0];
                
                if($new_status == "ONLINE" && $laststatus != "ONLINE"){
                    $offline += $pieces[1];
                    $total_offline += $pieces[1];
                    
                    if($pieces[1] >= 60){
                        $email_sent++;
                        $total_email_sent++;
                    }
                }
                else{
                    $online += $pieces[1];
                    $total_online += $pieces[1];
                    $offline_count++;
                    $total_offline_count++;
                }
                
                $graph[$counter] .= "$new_status      $datetime_value     $amount       $online     $offline<br>";
                
                $lasttime = $datetime_value;
                $laststatus = $new_status;
            }
            
            ///end of period
            $query3 = "SELECT TIMEDIFF('$endtime', '$lasttime') AS amount";
            $result3 = Treat_DB_ProxyOld::query($query3);
                
            $amount = mysql_result($result3, 0, "amount");
     
            //turn into minutes
            $pieces = explode(':', $amount);
            $pieces[0] *= 60;
            $pieces[1] += $pieces[0];
            
            
            
            if($new_status == "ONLINE" || $pieces[1] == 10080){
                $online += $pieces[1];
                $total_online += $pieces[1];
            }
            else{
                $offline += $pieces[1];
                $total_offline += $pieces[1];
            }
            
            $graph[$counter] .= "$new_status      $endtime     $amount       $online     $offline<br>";
            
            $percent = round($offline / ($online + $offline) * 100, 2);
            
            $online = round($online / 60, 2);
            $offline = round($offline / 60, 2);
            
            echo "<td style=\"border:1px solid black;\"><font color=blue>$offline_count</font></td><td style=\"border:1px solid black;\"><font color=green>$email_sent</font></td><td style=\"border:1px solid black;\"><font color=red>$offline hrs</font></td><td style=\"border:1px solid black;\">$percent%</td>";
        }
        
        ///totals
        $percent = round($total_offline / ($total_online + $total_offline) * 100, 2);
            
        $total_online = round($total_online / 60, 2);
        $total_offline = round($total_offline / 60, 2);
        
        echo "<td style=\"border:1px solid black;\"><b><font color=blue>$total_offline_count</font></b></td><td style=\"border:1px solid black;\"><b><font color=green>$total_email_sent</font></b></td><td style=\"border:1px solid black;\"><b><font color=red>$total_offline hrs</font></b></td><td style=\"border:1px solid black;\"><b>$percent%</b></td></tr>";
        
        ////ISPs
        if($data->net->orgRef->{'@name'} != ""){
            $isp[$data->net->orgRef->{'@name'}]['online'] += $total_online;
            $isp[$data->net->orgRef->{'@name'}]['offline'] += $total_offline;
            $isp[$data->net->orgRef->{'@name'}]['offline_counter'] += $total_offline_count;
            $isp[$data->net->orgRef->{'@name'}]['count']++;
        }
        else{
            $isp[$data->net->name->{'$'}]['online'] += $total_online;
            $isp[$data->net->name->{'$'}]['offline'] += $total_offline;
            $isp[$data->net->name->{'$'}]['offline_counter'] += $total_offline_count;
            $isp[$data->net->name->{'$'}]['count']++;
        }
        
        //echo "<tr valign=top><td></td><td colspan=3><font size=2>$graph[4]</td><td colspan=3><font size=2>$graph[3]</td><td colspan=3><font size=2>$graph[2]</td><td colspan=3><font size=2>$graph[1]</td></tr>";
}
echo "</table>";

///ISPs
echo "<table style=\"border:1px solid black;\">";
echo "<tr><td style=\"border:1px solid black;\">ISP</td><td style=\"border:1px solid black;\">%Offline</td><td style=\"border:1px solid black;\">#Offline</td><td style=\"border:1px solid black;\">Num of Kiosks</td></tr>";
foreach($isp AS $name => $values){
    $percent = round($values['offline'] / ($values['online'] + $values['offline']) * 100, 2);
    
    echo "<tr><td style=\"border:1px solid black;\">$name</td><td style=\"border:1px solid black;\">$percent%</td><td style=\"border:1px solid black;\">{$values['offline_counter']}</td><td style=\"border:1px solid black;\">{$values['count']}</td></tr>";
}
echo "</table>";
?>