<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');

ini_set("display_errors","false");

$myFile = "heartland.csv";

$handle = fopen(DIR_VENDOR_IMPORTS . "/" . $myFile, 'r') or die("can't open file");

echo "Open $myFile<br>";

$counter = 1;
function insert_aeris_settings($businessID, $name, $value, $onDuplicateValue = '') {
	static $statement = null;
	
	if (!$statement) {
		$query = "
			INSERT INTO
				aeris_settings (businessid, registryKey, registryValue, uuid_aeris_settings)
			VALUES (?, ?, ?, ?)
			ON DUPLICATE KEY UPDATE registryValue = ?";
		$statement = \EE\Model\Aeris\Setting::db()->prepare($query);
	}
	$uuid = \EE\Model\Aeris\Setting::genUuid();
	$statement->execute(array($businessID, $name, $value, $uuid, $onDuplicateValue));
	$statement->debugDumpParams();
}

while ($pieces = fgetcsv($handle,NULL,",")) {

	insert_aeris_settings($pieces[8], 'vivipos.fec.settings.layout.aerispos.heartlandDeviceId', $pieces[3]);
	insert_aeris_settings($pieces[8], 'vivipos.fec.settings.layout.aerispos.heartlandLicenseId', $pieces[1], $pieces[1]);
	insert_aeris_settings($pieces[8], 'vivipos.fec.settings.layout.aerispos.heartlandPassword', $pieces[5], $pieces[5]);
	insert_aeris_settings($pieces[8], 'vivipos.fec.settings.layout.aerispos.heartlandSiteId', $pieces[2], $pieces[2]);
	insert_aeris_settings($pieces[8], 'vivipos.fec.settings.layout.aerispos.heartlandUsername', $pieces[4], $pieces[4]);

	$query = "SELECT mburris_manage.client_programid FROM client_programs WHERE businessid = $pieces[8] AND db_name = 'mburris_businesstrack'";
	$result = Treat_DB_ProxyOld::query($query);

	while($r = mysql_fetch_array($result)){
		$client_programid = $r["client_programid"];

		$query2 = "INSERT INTO kiosk_sync (client_programid,item_id,item_type) VALUES ($client_programid, 1, 9)";
		$result2 = Treat_DB_ProxyOld::query($query2);

		echo "<font color=red>" . $query2 . "</font><br>";
	}

}
