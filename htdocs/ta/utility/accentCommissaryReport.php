<?php

define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');

$date1 = $_POST["date1"];
$date2 = $_POST["date2"];
$go = $_POST["go"];

if($go != 1){
	?>
		<head>
			<script type="text/javascript" src="../javascripts/jquery-1.4.2.min.js"></script>
			<script type="text/javascript" src="../javascripts/jquery-ui-1.7.2.custom.min.js"></script>
			<script type="text/javascript">$(document).ready(function (){$('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' });});</script>
			<link rel="stylesheet" href="../css/jq_themes/redmond/jquery-ui-1.7.2.custom.css">
		</head>
	<?php
	
	if($date1 == ""){
		$date1 = date("Y-m-d");
	}
	if($date2 == ""){
		$date2 = date("Y-m-d");
	}

	echo "<form action=accentCommissaryReport.php method=post><input type=hidden name=go value=1><input type=text name='date1' value='$date1' size=20 class=\"datepicker\"> to <input type=text name='date2' value='$date2' size=20 class=\"datepicker\"> <input type=submit value='GO'></form>";
}
else{
	$query = "CALL inventory_commissary(12,'$date1','$date2')";
	$result = Treat_DB_ProxyOldProcessHost::query($query);
	
	$query = "SELECT * FROM mburris_report.inventory_commissary WHERE companyid = 12 ORDER BY districtname,businessname";
	$result = Treat_DB_ProxyOldProcessHost::query($query);
	
	$distSold = 0;
	$distFill = 0;
	$distWaste = 0;
	$comSold = 0;
	$comFill = 0;
	$comWaste = 0;
	$lastDistrict = 0;
	
	$data = "District,Business,Group,Item Code,Item Name,Sold,Filled,Wasted,Average,Active Status\r\n";
	
	while($r = mysql_fetch_array($result)){
		$currentDistrict = $r['districtid'];
		$currentDistrictName = $r['districtname'];
		
		if($currentDistrict != $lastDistrict && $lastDistrict != 0){
			$data .= "\"" . $lastDistrictName . " Totals\",,,,," . $distSold . "," . $distFill . "," . $distWaste . "\r\n";
			$distSold = 0;
			$distFill = 0;
			$distWaste = 0;
		}
		
		$data .= "\"" . $r['districtname'] . "\",\"" . $r['businessname'] . "\",\"" . $r['group_name'] . "\",\"" . $r['item_code'] . "\",\"" . $r['item_name'] . "\",\"" . $r['items_sold'] . "\",\"" . $r['items_filled'] . "\",\"" . $r['items_wasted'] . "\",\"" . $r['avg_daily_unit_sold'] . "\",\"" . $r['active_status'] . "\"\r\n";
		
		$distSold += $r['items_sold'];
		$distFill += $r['items_filled'];
		$distWaste += $r['items_wasted'];
		$comSold += $r['items_sold'];
		$comFill += $r['items_filled'];
		$comWaste += $r['items_wasted'];
		
		$lastDistrict = $currentDistrict;
		$lastDistrictName = $currentDistrictName;
	}
	
	////report totals
	$data .= "\"" . $currentDistrictName . " Totals\",,,,," . $distSold . "," . $distFill . "," . $distWaste . "\r\n";
	$data .= "\"" . "Company Totals\",,,,," . $comSold . "," . $comFill . "," . $comWaste . "\r\n";
	
	$file="report.csv";
	header("Content-type: application/x-msdownload");
	header("Content-Disposition: attachment; filename=$file");
	header("Pragma: no-cache");
	header("Expires: 100");
	echo $data;
}
?>