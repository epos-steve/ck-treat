<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');

$message = null;

sleep(15);

///current status list
$content = file_get_contents("/srv/home/steve/serverStatus.log");
$content = explode("\n", $content);
foreach($content AS $line){
	$data = explode(":",$line);
	$server = $data[0];
	$status = $data[1];
	$serverStatus[$server] = $status;
}

///Sync Processor File Check
$fileCount = iterator_count(new DirectoryIterator('/srv/home/uploads/manage/files/kiosksync/'));
if($fileCount > 2000){
	$message .= "Sync Processor ($fileCount files).<br>";
}
else{
	echo "$fileCount Files awaiting sync processor.<br>";
}

///Memory Check
$dbs = array('report1' => '192.168.1.90','report2' => '192.168.1.91','db1' => '192.168.1.31','report2' => '192.168.1.91','router2' => '192.168.1.2','db4' => '192.168.1.34','db5' => '192.168.1.35','db6' => '192.168.1.36');
foreach($dbs AS $db => $ip){
	$filename = $db . "Mem.log";
	$filename2 = $db . "Mem.last";
	
	$content = file_get_contents("/srv/home/steve/$filename");
	$lastMem = file_get_contents("/srv/home/steve/$filename2");
	
	$lastMem = explode(" ",$lastMem);
	
	$content = explode("\n", $content);
	$counter = 1;
	$totalMem = 0;
	$usedMem = 0;
	$totalSwap = 0;
	$usedSwap = 0;
	foreach($content AS $line){
		if($counter == 2){
			$lineData = preg_split('/\s+/', $line);
			$totalMem = $lineData[1];
			$usedMem = $lineData[2] - $lineData[6];
		}
		elseif($counter == 4){
			$lineData = preg_split('/\s+/', $line);
			$totalSwap = $lineData[1];
			$usedSwap = $lineData[2];
		}
		$counter++;
	}
	
	$memprcnt = round($usedMem / $totalMem * 100, 1);
	$swapprcnt = round($usedSwap / $totalSwap * 100, 1);
	
	file_put_contents("/srv/home/steve/$filename2", "$memprcnt $swapprcnt");
	
	////memory check
	if($memprcnt >= 98){
		$message .= "$db mem usage: $memprcnt%.<br>";
	}
	elseif($memprcnt >= 95 && $lastMem[0] < 95){
		$message .= "$db mem usage: $memprcnt%.<br>";
	}
	elseif($memprcnt >= 90 && $lastMem[0] < 90){
		$message .= "$db mem usage: $memprcnt%.<br>";
	}
	elseif($memprcnt < 90 && $lastMem[0] >= 90){
		$message .= "$db mem usage: $memprcnt%.<br>";
	}
		
	echo "$db mem usage: $memprcnt%<br>";
	
	///swap check
	if($swapprcnt >= 95 && $lastMem[1] < 95){
		$message .= "$db swap usage: $swapprcnt%.<br>";
	}
	elseif($swapprcnt >= 90 && $lastMem[1] < 90){
		$message .= "$db swap usage: $swapprcnt%.<br>";
	}
	elseif($swapprcnt < 90 && $lastMem[1] >= 90){
		$message .= "$db swap usage: $swapprcnt%.<br>";
	}
		
	echo "$db swap usage: $swapprcnt%<br>";
	
}

///Disk Check
$dbs = array('report1' => '192.168.1.90','report2' => '192.168.1.91','db1' => '192.168.1.31','report2' => '192.168.1.91','router2' => '192.168.1.2','db4' => '192.168.1.34','db5' => '192.168.1.35','db6' => '192.168.1.36', 
				'file1' => '192.168.1.10', 'web1' => '192.168.1.61', 'web2' => '192.168.1.62', 'web3' => '192.168.1.63', 'web4' => '192.168.1.64', 'web5' => '192.168.1.65', 'web6' => '192.168.1.66');
foreach($dbs AS $db => $ip){
	$filename = $db . "Disk.log";
	$filename2 = $db . "Disk.last";
	
	$content = file_get_contents("/srv/home/steve/$filename");
	$lastDisk = file_get_contents("/srv/home/steve/$filename2");
	
	$content = explode("\n", $content);
	$counter = 1;
	$highestDisk = 0;
	$currentPartion = "";
	foreach($content AS $line){
		if($counter > 1){
			$lineData = preg_split('/\s+/', $line);
			$partion = $lineData[5];
			$disk = str_replace("%", "", $lineData[4]);
			
			if($disk > $highestDisk && $partion != "/srv/home"){
				$highestDisk = $disk;
				$currentPartion = $partion;
			}
		}
		$counter++;
	}
	
	file_put_contents("/srv/home/steve/$filename2", "$highestDisk");
	
	////memory check
	if($highestDisk >= 98){
		$message .= "$db $currentPartion size: $highestDisk%.<br>";
	}
	elseif($highestDisk >= 95 && $lastDisk < 95){
		$message .= "$db $currentPartion size: $highestDisk%.<br>";
	}
	elseif($highestDisk >= 90 && $lastDisk < 90){
		$message .= "$db $currentPartion size: $highestDisk%.<br>";
	}
	elseif($highestDisk < 90 && $lastDisk >= 90){
		$message .= "$db $currentPartion size: $highestDisk%.<br>";
	}
		
	echo "$db $currentPartion size: $highestDisk%<br>";
}

///Sentinel Check
$sentinelLogs = array('web1' => 'web1Sentinel.log','web2' => 'web2Sentinel.log','web3' => 'web3Sentinel.log','web4' => 'web4Sentinel.log','web5' => 'web5Sentinel.log');
$sentinelProblem = "Could not accept new connection";

foreach($sentinelLogs AS $server => $filename){
	$content = file_get_contents("/srv/home/steve/$filename");
	if(strpos($content, $sentinelProblem)){
		$message .= "$server Sentinel has stopped responding.<br>";
	}
	else{
		echo "$server Sentinel status: ok, $content<br>";
	}
}

///Web Server Ping Out Check
$pingLogs = array('web1' => 'web1Ping.log','web2' => 'web2Ping.log','web3' => 'web3Ping.log','web4' => 'web4Ping.log','web5' => 'web5Ping.log','web6' => 'web6Ping.log');

foreach($pingLogs AS $server => $filename){
	$content = file_get_contents("/srv/home/steve/$filename");
	
	if($content === false){
		$message .= "$server ping file failure.";
	}
	else{
		$content = explode("\n", $content);
		//print_r($content);
		$line = explode(",",$content[4]);
		//print_r($line);
		if($line[0] == "1 packets transmitted" && $line[1] == " 1 received" && $line[2] == " 0% packet loss"){
			echo "$server ping out status: ok<br>";
		}
		else{
			$message .= "$server ping out has failed.<br>";
		}
	}
}

////database check
$dbs = array('db1' => '192.168.1.31','router2' => '192.168.1.2','db4' => '192.168.1.34','db5' => '192.168.1.35','db6' => '192.168.1.36','report1' => '192.168.1.90');

foreach($dbs AS $db => $ip){
	$serverStatusNew[$db] = "on";
	mysql_connect($ip,'mburris_user','bigdog');
	@mysql_select_db('vivipos_order') or $serverStatusNew[$db] = "off";
	$query = "SHOW PROCESSLIST";
	$result = mysql_query($query);
	$num=mysql_numrows($result);
	mysql_close();
	
	$counter = 0;
	$lock_counter = 0;
	while($r = mysql_fetch_array($result)){
		$counter++;
		$command = $r["State"];
		if($command == "Locked") $lock_counter++;
	}
	
	$ok = 0;
	if($counter > 750){
		$message .= "$db has $counter processes running<br>";
		$ok = 1;
	}
	if($lock_counter > 50){
		$message .= "$db has $lock_counter locked processes!!<br>";
		$ok = 1;
	}
	if($ok == 0){
		echo "$db status: OK, $num processes running, $lock_counter locked processes<br>";
	}
}

////mysql query check
$dbs = array('db1' => '192.168.1.31','router2' => '192.168.1.2','db4' => '192.168.1.34','db5' => '192.168.1.35','db6' => '192.168.1.36','report1' => '192.168.1.90');

foreach($dbs AS $db => $ip){
	mysql_connect($ip,'mburris_user','bigdog');
	@mysql_select_db('mburris_manage') or $message .= "";
	
	$starttime = microtime(true);
	
	$query = "SELECT * FROM client_programs";
	$result = mysql_query($query);
	
	$endtime = microtime(true);
	$duration = $endtime - $starttime;
	
	echo "$db: $duration<br>";
	
	mysql_close();
}

///blacklist check
$blackListFile = "mailBlacklist.log";
$blackListFileLast = "mailBlacklist.last";

$blackListCurrent = file_get_contents("/srv/home/steve/" . $blackListFile);
$blackListLast = file_get_contents("/srv/home/steve/" . $blackListFileLast);

$blackListCurrent = explode(" ",$blackListCurrent);
$blackListLast = explode(" ",$blackListLast);

$blackListCurrentNum = trim($blackListCurrent[0]);
$blackListLastNum = trim($blackListLast[0]);

if($blackListCurrentNum != $blackListLastNum){
	$message .= "Blacklist Change [$blackListCurrentNum/$blackListLastNum]\r\n";
	echo "Blacklist Change [$blackListCurrentNum/$blackListLastNum]<br>";
	
	file_put_contents("/srv/home/steve/" . $blackListFileLast, $blackListCurrentNum);
}
else{
	echo "No New Blacklisting change [$blackListCurrentNum/$blackListLastNum]<br>";
}

///ping check
$lbs = array('barracuda' => '192.168.3.3','file1' => '192.168.1.10','ns1' => '192.168.1.41','web1' => '192.168.1.61', 'web2' => '192.168.1.62', 'web3' => '192.168.1.63', 'web4' => '192.168.1.64','ck' => '192.168.1.221', 'monitor' => '192.168.1.30');

foreach($lbs AS $lb => $ip){
	exec(sprintf('ping -c 2 -W 5 %s', escapeshellarg($ip)), $output, $retval);
	if($retval != 0){
		//$message .= "$lb is unreachable<br>";
		$serverStatusNew[$lb] = "off";
	}
	else{
		echo "$lb status: OK<br>";
		$serverStatusNew[$lb] = "on";
	}
}

////monitor status
$myFileOld = "mmm_mond.old.status";
$myFile = "mmm_mond.status";

$mmm_status_old = file_get_contents(DIR_UTILITY . "/" . $myFileOld);
$mmm_status_old = explode("\n", $mmm_status_old);

$mmm_status_new = file_get_contents(DIR_UTILITY . "/" . $myFile);
file_put_contents(DIR_UTILITY . "/" . $myFileOld, $mmm_status_new);
$mmm_status_new = explode("\n", $mmm_status_new);

///see if status has changed
$chg_flag = 0;
$new_status = null;
for( $count = 0; $count <= 5; $count++ ){
	$serverOld = explode("|", $mmm_status_old[$count]);
	$server = explode("|",$mmm_status_new[$count]);
	
	echo "$server[0]: $server[1]<br>";
	
	if($serverOld[1] != $server[1]){
		$chg_flag = 1;
		$new_status .= "$server[0]: $server[1]<br>";
	}
}

if($chg_flag == 1){
	$message .= "$new_status<br>";
}
else{
	echo "db roles have not changed<br>";
}

///update statuses
foreach($serverStatusNew AS $server => $newStatus){
	if($newStatus != $serverStatus[$server]){
		if($newStatus == "off"){
			$message .= "Can not connect: $server<br>";
		}
		else{
			$message .= "Back online: $server<br>";
		}
	}
	$newData .= "$server:$newStatus\n";
}
file_put_contents("/srv/home/steve/serverStatus.log", $newData);

///show status
echo "<p>Message to be sent<br>-----------------------<br>$message";

if($message != null){
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\n";
	$headers .= "From: support@essentialpos.com\r\n";
	$subject = "Server Status";
	$email = "8168069122@vtext.com,jake@essential-elements.net,smartin@essentialpos.com,ryan@essential-elements.net";
	
	$message = str_replace("<br>","\r\n",$message);
	
	mail($email, $subject, $message, $headers);
}

?>
