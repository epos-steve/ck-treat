<?php

function nextday($nextd,$day_format=''){ //Function returns next day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monn = substr($nextd, 5, 2);
   $dayn = substr($nextd, 8, 2);
   $yearn = substr($nextd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monn, $dayn+1, $yearn));
   return $tempdate;
}

function prevday($prevd,$day_format=''){ //Function returns previous day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monp = substr($prevd, 5, 2);
   $dayp = substr($prevd, 8, 2);
   $yearp = substr($prevd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monp, $dayp-1, $yearp));
   return $tempdate;
}

function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}


define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');
$style = "text-decoration:none";

$today=date("Y-m-d");

///////////CREATE DATE ARRAY
//$startdate = isset($_GET["startdate"])?$_GET["startdate"]:'';
//$enddate = isset($_GET["enddate"])?$_GET["enddate"]:'';

//////////////////////WHICH DATES???????????//////////////
$startdate="2010-07-10";
$enddate="2010-09-03";
//////////////////////////////////////////////////////////

$tempdate=$startdate;
$counter=1;
$date_array[0]=$startdate;
while($tempdate<=$enddate){
	if(dayofweek($tempdate)=="Friday"){
		$date_array[$counter]=$tempdate;
		$counter++;
	}
	$tempdate=nextday($tempdate);
}

//////////END DATES

mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");

$query3 = "SELECT locationid,unitid FROM vend_locations ORDER BY unitid";
$result3 = mysql_query($query3);

while ($r3=mysql_fetch_array($result3)) {
	$businessid=$r3["unitid"];
	$locationid=$r3["locationid"];

	$query4 = "SELECT login_routeid,route,lastroute FROM login_route WHERE locationid = '$locationid'";
	$result4 = mysql_query($query4);

	while($r4=mysql_fetch_array($result4)){
		$login_routeid=$r4["login_routeid"];
		$route=$r4["route"];
		$lastroute=$r4["lastroute"];
		if($lastroute==$route){$lastroute=-1;}

		for($counter2=1;$counter2<$counter;$counter2++){
			$counter3=$counter2-1;

			//////get collects per week
			$query5 = "SELECT SUM(collects) AS totalamt FROM vend_machine_sales WHERE (route = '$route' OR route = '$lastroute') AND businessid = '$businessid' AND date > '$date_array[$counter3]' AND date <= '$date_array[$counter2]'";
			$result5 = mysql_query($query5);

			$totalamt=@mysql_result($result5,0,"totalamt");


			//////get stops per week
			$query5 = "SELECT date,fills,collects,accountid FROM vend_machine_sales WHERE (route = '$route' OR route = '$lastroute') AND businessid = '$businessid' AND date > '$date_array[$counter3]' AND date <= '$date_array[$counter2]' ORDER BY accountid,date";
			$result5 = mysql_query($query5);

			$totalstops=0;
			$lastaccountid=-1;
			$lastdate="0000-00-00";
			while($r5=mysql_fetch_array($result5)){
				$date=$r5["date"];
				$collects=$r5["collects"];
				$fills=$r5["fills"];
				$accountid=$r5["accountid"];

				if(($date!=$lastdate||$accountid!=$lastaccountid)&&($collects!=0||$fills!=0)){
					$totalstops++;
				}

				$lastaccountid=$accountid;
				$lastdate=$date;
			}

			///////get services per week
			$query5 = "SELECT DISTINCT vend_machine_service.id FROM vend_machine_service,vend_machine_sales WHERE vend_machine_sales.businessid = '$businessid' AND (vend_machine_sales.route LIKE '$route' OR vend_machine_sales.route LIKE '$lastroute') AND vend_machine_sales.date > '$date_array[$counter3]' AND vend_machine_sales.date <= '$date_array[$counter2]' AND vend_machine_sales.machineid = vend_machine_service.machineid AND vend_machine_service.businessid = '$businessid' AND vend_machine_service.date > '$date_array[$counter3]' AND vend_machine_service.date <= '$date_array[$counter2]'";
			$result5 = mysql_query($query5);

			$service=mysql_numrows($result5);

			if($totalamt!=0){
				$query5 = "DELETE FROM dashboard_route WHERE login_routeid = '$login_routeid' AND date = '$date_array[$counter2]'";
				$result5 = mysql_query($query5);

				$query5 = "INSERT INTO dashboard_route (login_routeid,date,collection,stops,service) VALUES ('$login_routeid','$date_array[$counter2]','$totalamt','$totalstops','$service')";
				$result5 = mysql_query($query5);

				echo "$query5<br>";
			}
		}
	}
}

////update dashboard time
$showtime=date("D n/j/y g:ia");
$query = "UPDATE dashboard_update SET routes = '$showtime'";
$result = mysql_query($query);

mysql_close();

?>