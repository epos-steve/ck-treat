<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');

ini_set("display_errors","true");

$myFile = "newkioskmenu.csv";

$handle = fopen($myFile, 'r') or die("can't open file");

echo "Open $myFile<br>";

/*
 * Import order.....
 * group_id,name,ordertype,item_code,upc,price,cost
 */
$businessid = 331;

$counter = 1;
while ($pieces = fgetcsv($handle,NULL,",")){
	$pieces[3] = trim($pieces[3]);
	$pieces[4] = trim($pieces[4]);
	$pieces[1] = str_replace("'","`",$pieces[1]);
	$pieces[5] = str_replace("'","`",$pieces[5]);

	////menu groups
	$query = "SELECT id FROM menu_groups_new WHERE businessid = $businessid AND name LIKE '$pieces[5]'";
	$result = DB_Proxy::query($query);
	$num = mysql_num_rows($result);

	if($num == 0){
		$new_group = \EE\Model\Menu\GroupsNew::create($businessid);
		$new_group->name = $pieces[5];
		$new_group->save();
		$group_id = $new_group->id;
	}
	else{
		$group_id = mysql_result($result,0,"id");
	}

	$uuid = \EE\Model\Menu\ItemNew::genUuid();
	/////menu items
	$query = "
		INSERT INTO menu_items_new (
			businessid,
			pos_id,
			group_id,
			name,
			ordertype,
			item_code,
			upc,
			uuid_menu_items_new
		) VALUES (
			$businessid,
			$counter,
			$group_id,
			'$pieces[1]',
			$pieces[6],
			'$pieces[0]',
			'$pieces[2]',
			'$uuid'
		)
	";
	$result = DB_Proxy::query($query);
	$id = mysql_insert_id();
	echo "$query<br>";
	$query = "
		INSERT INTO menu_items_price (
			businessid,
			menu_item_id,
			pos_price_id,
			price,
			cost
		) VALUES (
			$businessid,
			$id,
			1,
			'$pieces[4]',
			'$pieces[3]'
		)
	";
	$result = DB_Proxy::query($query);
	echo "$query<br>";
	$counter++;
}
?>