<?php

define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');
//require_once('../lib/class.bus_setup.php');

$errors = array();
$completed = array();

$query = "SELECT COUNT(id) AS total_setup FROM setup WHERE setup_status = 1";
$result = DB_Proxy::query($query);

$total_setup = @mysql_result($result,0,"total_setup");

$query = "SELECT business.businessid,
				business.businessname,
				company.companyname,
				setup.name,
				setup_detail.value,
				setup_detail.complete,
				setup_detail.editby,
				setup_detail.edittime
			FROM business
			JOIN company ON company.companyid = business.companyid
            JOIN setup ON setup.setup_status = 1
			LEFT JOIN setup_detail ON setup_detail.setupid = setup.id AND setup_detail.businessid = business.businessid
			JOIN setup_detail sd ON sd.businessid = business.businessid AND sd.setupid =31
			WHERE business.categoryid = 4 AND business.setup = 1 
            ORDER BY sd.value, business.companyid, business.businessname, setup.groupid, setup.orderid";
$result = DB_Proxy::query($query);

$counter = 0;
while($r = mysql_fetch_array($result)){
	$businessid = $r["businessid"];
	$businessname = $r["businessname"];
	$companyname = $r["companyname"];
	$setup_name = $r["name"];
	$value = $r["value"];
	$complete = $r["complete"];
	$editby = $r["editby"];
	$edittime = $r["edittime"];
	
	if($complete == 1){$completed[$businessid]++;}
	
	if($value == ""){
		$errors[$counter]['businessid'] = $businessid;
		$errors[$counter]['companyname'] = $companyname;
		$errors[$counter]['businessname'] = $businessname;
		$errors[$counter]['setup_name'] = $setup_name;
		$counter ++;
	}
	
	if($setup_name == "Ship Date/Pickup Date"){
		$ship_dates[$businessid] = $value;
		$ship_by[$businessid] = "$editby $edittime";
	}
	if($setup_name == "Date of Install"){
		$install_dates[$businessid] = $value;
	}
}

$lastcompanyname = "";
$critical_date = date("Y-m-d", mktime(0, 0, 0, date("m") , date("d")+7, date("Y")));
$today = date("Y-m-d");

$data = "<html><head></head><body><b><font color=blue>The following new installs are missing critical information. Kiosks in <font color=red>red</font> ship in under a week. Kiosks in <font color=#777777>grey</font> are past due.</font></b><p><table cellspacing=0 cellpadding=1 style=\"border:1px solid black;\" width=100%>";
$data .= "<tr bgcolor=black style=\"color:white;font-weight:bold;\"><td style=\"border:1px solid black;\">Business</td><td style=\"border:1px solid black;\">Company</td><td style=\"border:1px solid black;\">Ship Date</td><td style=\"border:1px solid black;\">Install Date</td><td style=\"border:1px solid black;\">Ordered By</td><td style=\"border:1px solid black;\" align=right>Status</td></tr>";
	foreach($errors AS $key => $value){
		$run = 1;
		$businessid = $errors[$key]['businessid'];
		$companyname = $errors[$key]['companyname'];
		$businessname = $errors[$key]['businessname'];
		$setup_name = $errors[$key]['setup_name'];
		
		$install_date = $install_dates[$businessid];
		$ship_date = $ship_dates[$businessid];
		$shipped_by = $ship_by[$businessid];
		
		if($lastbusinessname != $businessname){
			if($ship_date < $today){$color = "#777777";}
			elseif($ship_date < $critical_date){$color="red";}
			else{$color="black";}
			
			$done = $completed[$businessid];
			
			$percent = round($done / $total_setup * 100, 0);
			
			$data .= "<tr style=\"color:$color;\"><td style=\"border:1px solid black;\">$businessname</td><td style=\"border:1px solid black;\">$companyname</td><td style=\"border:1px solid black;\">$ship_date</td><td style=\"border:1px solid black;\">$install_date</td><td style=\"border:1px solid black;\">$shipped_by</td><td style=\"border:1px solid black;\" align=right>$percent% </td></tr>";
		}
		
		//$data .= "<li>$setup_name";
		
		$lastcompanyname = $companyname;
		$lastbusinessname = $businessname;
	}
$data .= "</table></body></html>";

echo $data;

///email
if($run == 1){
	$email = "smartin@essentialpos.com,peteb@treatamerica.com,traceym@treatamerica.com";
	//peteb@treatamerica.com,peteb@treatamerica.com,traceym@treatamerica.com";
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	$headers .= "From: support@companykitchen.com\r\n";
	$subject = "Upcoming CK Installs Missing Info";

	mail($email, $subject, $data, $headers);
}
?>