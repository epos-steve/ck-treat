<?php
putenv("TREAT_CONFIG=treat.ini");
define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');

ini_set("display_errors",true);

echo "<hr>";
///clear messages
$query = "DELETE FROM mburris_businesstrack.dashboard_shrink_recalculate ORDER BY businessid ASC LIMIT 30";
Treat_DB_ProxyOld::query( $query );
echo $query . "<br>";
echo "Sleeping at: " . date("Y-m-d G:i:s") . " for 15 seconds<br>";;

sleep(15);

echo "Continue Processing at: " . date("Y-m-d G:i:s") . "<br>";;
$query2 = "CALL mburris_businesstrack.shrink_recalculate()";
Treat_DB_ProxyOldProcessHost::query( $query2 );
echo $query2 . "<br>";

sleep(5);

$remove_old_menu_items = "SELECT businessid, date(date_posted) AS date_value FROM  mburris_report_tables.dashboard_shrink_recalculate_processing";
$remove_old_menu_items_result = Treat_DB_ProxyOldProcessHost::query( $remove_old_menu_items , true );
while($rs1 = Treat_DB_ProxyOldProcessHost::mysql_fetch_array($remove_old_menu_items_result)){

	$bid_value = $rs1['businessid'];
	$date_value = $rs1['date_value'];
	
	$rs1_query = "DELETE FROM mburris_businesstrack.menu_item_shrink WHERE businessid = '$bid_value' AND `date` >= '$date_value'";
	Treat_DB_ProxyOld::query( $rs1_query );
	
	$rs2_query = "UPDATE mburris_businesstrack.dashboard d1 SET d1.shrink = '0' WHERE d1.businessid = '$bid_value' AND d1.`date` >= '$date_value'";
	Treat_DB_ProxyOld::query( $rs2_query );
	
}

sleep(5);

$query3 = "SELECT menu_item_id, businessid, `date`, new_qty, new_amount FROM mburris_businesstrack.zzz_shrink_changes WHERE new_qty != 0";
$result = Treat_DB_ProxyOldProcessHost::query( $query3 );
echo $query3 . "<hr>";

$businessid_new = 0;
while($r = Treat_DB_ProxyOldProcessHost::mysql_fetch_array($result)){
	$menu_item_id = $r["menu_item_id"];
	$businessid = $r["businessid"];
	$date = $r["date"];
	$new_qty = $r["new_qty"];
	$new_amount = $r["new_amount"];
		
	if ($businessid != $businessid_new){
		echo "<hr>";
		$businessid_new = $businessid;
	}

	$sql1 = "INSERT IGNORE INTO mburris_businesstrack.menu_item_shrink (menu_item_id, `date`, qty, amount, businessid) VALUES ('$menu_item_id', '$date', '$new_qty', '$new_amount', '$businessid')";
	Treat_DB_ProxyOld::query( $sql1 );
	echo $sql1 . "<br>";
	
}
echo "<hr>";
$query3 = "SELECT businessid, date(date_posted) date_posted, date(now()) as today_date FROM mburris_report_tables.dashboard_shrink_recalculate_processing ORDER BY businessid ASC";
$result3 = Treat_DB_ProxyOldProcessHost::query( $query3 );
echo $query3 . "<hr>";

while($r = Treat_DB_ProxyOldProcessHost::mysql_fetch_array($result3)){

	$businessid = $r["businessid"];
	$date_posted = $r["date_posted"];
	$today_date = $r["today_date"];
	
	$sql2a = "SELECT businessid, date2, SUM(total_shrink_cost) total_shrink_cost, SUM(total_waste_cost) total_waste_cost FROM mburris_businesstrack.zzz_recalculate WHERE businessid = '$businessid' GROUP BY businessid, date2";
	$result2a = Treat_DB_ProxyOldProcessHost::query( $sql2a );

	$total_shrink_cost = 0;
	$total_waste_cost = 0;

	while($r2a = Treat_DB_ProxyOldProcessHost::mysql_fetch_array($result2a)){
	
		$bid = $r2a['businessid'];
		$date2 = $r2a['date2'];
		$total_shrink_cost = $r2a['total_shrink_cost'];
		$total_waste_cost = $r2a['total_waste_cost'];
		
		$sql2b = "INSERT IGNORE INTO dashboard (businessid, date, shrink) 
				VALUES ('$bid','$date2','$total_shrink_cost')
				ON DUPLICATE KEY UPDATE shrink = '$total_shrink_cost'";
				
		//$sql2b = "UPDATE mburris_businesstrack.dashboard d1 SET d1.shrink = '$total_shrink_cost' WHERE d1.businessid = '$bid' AND d1.date = '$date2' LIMIT 1";
		Treat_DB_ProxyOld::query( $sql2b );
		echo $sql2b . "<br>";
		
		$total_shrink_cost = 0;
		$total_waste_cost = 0;
	}

	$sql2 = "SELECT SUM( icv.waste * icv.unit_cost ) AS amount, DATE(icv.date_time) as waste_date FROM mburris_businesstrack.posreports_inv_count_vend icv LEFT JOIN mburris_businesstrack.posreports_menu_items_new min ON min.item_code = icv.item_code AND min.businessid = $businessid WHERE icv.machine_num IN ( select machine_num from mburris_businesstrack.posreports_machine_bus_link mbl where mbl.businessid = $businessid ) AND icv.date_time BETWEEN '$date_posted 00:00:00' AND '$today_date 23:59:59' AND icv.is_inv = 0 GROUP BY waste_date";
	$result4 = Treat_DB_ProxyOldProcessHost::query( $sql2 );
	echo $sql2 . "<hr>";
	
	while($r2 = Treat_DB_ProxyOldProcessHost::mysql_fetch_array($result4)){
		
		$amount = $r2["amount"];
		$waste_date = $r2["waste_date"];
		
		$sql3 = "INSERT IGNORE INTO dashboard (businessid, date, waste) 
				VALUES ('$businessid','$waste_date','$amount')
				ON DUPLICATE KEY UPDATE waste = '$amount'";
				
		//$sql3 = "UPDATE mburris_businesstrack.dashboard d1 SET d1.waste = '$amount' WHERE d1.businessid = '$businessid' AND d1.date = '$waste_date' LIMIT 1";
		Treat_DB_ProxyOld::query( $sql3 );
		echo $sql3 . "<br>";
		
	}

}

$query = "DELETE FROM mburris_report_tables.dashboard_shrink_recalculate_processing ORDER BY businessid ASC";
Treat_DB_ProxyOld::query( $query );
echo "<hr>" . $query . "<hr>";
?>