<?php

define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');

///get dates and default to yesterday
$date1 = $_GET["date1"];
$date2 = $_GET["date2"];

if($date1 == "" || $date2 == ""){
	$date1 = prevday(date("Y-m-d"));
	$date2 = $date1;
}

while($date1 <= $date2){
	
	$start = microtime(true);
	
	/////check audit table to see if has been run already
	$query = "SELECT * FROM ck_fee_audit WHERE category = 4 AND date = '$date1'";
	$result = Treat_DB_ProxyOld::query($query);
	$num=mysql_numrows($result);
	if($num > 0){break;}
		
	/////get businesses
	$query = "SELECT businessid, businessname FROM business WHERE categoryid = 4";
	$result = Treat_DB_ProxyOld::query($query);
	
	while($r = mysql_fetch_array($result)){
		$businessid = $r["businessid"];
		$businessname = $r["businessname"];
		
		///active kiosk/ck fee percent/efn/efn percent
		$query2 = "SELECT sd1.value AS active,
						sd2.value AS ck_fee_prcnt,
						sd3.value AS efn,
						sd4.value AS efn_prcnt,
						sd5.value AS monthly_fee,
						sd6.value AS online_cc_fee,
						sd7.value AS num_kiosks
					FROM setup_detail
					LEFT JOIN setup_detail sd1 ON sd1.setupid = 66 AND sd1.businessid = $businessid
					LEFT JOIN setup_detail sd2 ON sd2.setupid = 67 AND sd2.businessid = $businessid
					LEFT JOIN setup_detail sd3 ON sd3.setupid = 68 AND sd3.businessid = $businessid
					LEFT JOIN setup_detail sd4 ON sd4.setupid = 69 AND sd4.businessid = $businessid
					LEFT JOIN setup_detail sd5 ON sd5.setupid = 71 AND sd5.businessid = $businessid
					LEFT JOIN setup_detail sd6 ON sd6.setupid = 72 AND sd6.businessid = $businessid
					LEFT JOIN setup_detail sd7 ON sd7.setupid = 41 AND sd7.businessid = $businessid
					LIMIT 1";
		$result2 = Treat_DB_ProxyOld::query($query2);
	
		$active = mysql_result($result2,0,"active");
		$ck_fee_prcnt = mysql_result($result2,0,"ck_fee_prcnt");
		$efn = mysql_result($result2,0,"efn");
		$efn_prcnt = mysql_result($result2,0,"efn_prcnt");
		$monthly_fee = mysql_result($result2,0,"monthly_fee");
		$online_cc_fee = mysql_result($result2,0,"online_cc_fee");
		$num_kiosks = mysql_result($result2,0,"num_kiosks");
		
		if($active == 1){
		
			///ck sales
			$query2 = "SELECT SUM(creditdetail.amount) AS total_kiosk_sales FROM creditdetail
					JOIN credits ON credits.creditid = creditdetail.creditid AND credits.businessid = $businessid AND credits.category = 8
					JOIN mirror_accounts ON mirror_accounts.accountid = credits.creditid AND mirror_accounts.type = 1 AND mirror_accounts.category = 4 AND mirror_accounts.accountnum != -1
					WHERE creditdetail.date = '$date1'";
			$result2 = Treat_DB_ProxyOld::query($query2);
			$ck_sales = round(mysql_result($result2,0,"total_kiosk_sales"), 2);

			///cc fees
			$query2 = "SELECT SUM(cc_fee.amount) AS total_fee FROM cc_fee
					WHERE cc_fee.businessid = $businessid AND cc_fee.date = '$date1'";
			$result2 = Treat_DB_ProxyOld::query($query2);
		
			$cc_fee = round(mysql_result($result2,0,"total_fee"), 2);

			///operator promos
			$query2 = "SELECT SUM(debitdetail.amount) AS total_operator_promotions FROM debitdetail
					JOIN debits ON debits.debitid = debitdetail.debitid AND debits.businessid = $businessid AND debits.category IN (3)
					JOIN mirror_accounts ON mirror_accounts.accountid = debits.debitid AND mirror_accounts.type = 2 AND mirror_accounts.accountnum != -1
					WHERE debitdetail.date = '$date1'";
			$result2 = Treat_DB_ProxyOld::query($query2);
			$operator_promo = round(mysql_result($result2,0,"total_operator_promotions"), 2);
			
			///////////////////////////////////////////////////////
			////////////////////calculate fees/////////////////////
			///////////////////////////////////////////////////////
			$chargeFee = round(($ck_sales - $cc_fee - $operator_promo) * ($ck_fee_prcnt / 100), 2);
			
			if($efn == 1){
				$efnFee = round(($ck_sales) * ($efn_prcnt / 100), 2);
			}
			else{
				$efnFee = 0;
			}
			
			if(substr($date1, 8, 2) == "01"){
				$monthlyFee = $monthly_fee * $num_kiosks;
			}
			else{
				$monthlyFee = 0;
			}
			
			////insert/update ck_fee
			$query2 = "INSERT INTO ck_fee (businessid,date,fee,efn_fee,monthly_fee) 
						VALUES ($businessid,'$date1','$chargeFee','$efnFee','$monthlyFee')
						ON DUPLICATE KEY UPDATE fee = '$chargeFee', efn_fee = '$efnFee', monthly_fee = '$monthlyFee'";
			$result2 = Treat_DB_ProxyOld::query($query2);
			
			///online cc fee
			$query2 = "SELECT SUM(debitdetail.amount) AS total_fee FROM debitdetail
					JOIN debits ON debits.debitid = debitdetail.debitid AND debits.businessid = $businessid AND debits.category = 12
					JOIN mirror_accounts ON mirror_accounts.accountid = debits.debitid AND mirror_accounts.type = 2 AND mirror_accounts.accountnum != -1
					WHERE debitdetail.date = '$date1'";
			$result2 = Treat_DB_ProxyOld::query($query2);
			$online_cc_fee = round((mysql_result($result2, 0, "total_fee") * ($online_cc_fee / 100)), 2);
		
			////insert/update cc_fee
			$query2 = "INSERT INTO cc_fee (businessid,date,online_fee) 
						VALUES ($businessid,'$date1','$online_cc_fee')
						ON DUPLICATE KEY UPDATE online_fee = '$online_cc_fee'";
			$result2 = Treat_DB_ProxyOld::query($query2);
		}
	}
	
	echo "$date1 Finished....<br>";
	
	$end = microtime(true);
	
	$time = round($end - $start, 2);
	
	///audit record
	$query2 = "INSERT INTO ck_fee_audit (category,date,spentTime) VALUES (4,'$date1','$time')";
	$result2 = Treat_DB_ProxyOld::query($query2);
	
	$date1 = nextday($date1);
}
?>