<?php

define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='08' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      if ($month == "10"){$month="11";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='32')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function money($diff){
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+4);
        $diff=round($diff, 2);
        if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
        elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
        $diff = substr($diff, 0, $dot+3);
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        return $diff;
}

if ( ! defined('DOC_ROOT') )
{
	define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
}
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');
$style = "text-decoration:none";

$user = isset($_COOKIE["usercook"])?$_COOKIE["usercook"]:'';
$pass = isset($_COOKIE["passcook"])?$_COOKIE["passcook"]:'';
$companyid = isset($_COOKIE["compcook"])?$_COOKIE["compcook"]:'';
$apinvoiceid = isset($_POST["apinvoiceid"])?$_POST["apinvoiceid"]:'';
$email = isset($_POST["email"])?$_POST["email"]:'';
$message = isset($_POST["message"])?$_POST["message"]:'';
$day = date("d");
$year = date("Y");
$month = date("m");
$weekday = date("l");
$today="$year-$month-$day";

mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = mysql_query($query);
$num=mysql_numrows($result);

$security_level=@mysql_result($result,0,"security_level");
$myemail=@mysql_result($result,0,"myemail");
$firstname=@mysql_result($result,0,"firstname");
$lastname=@mysql_result($result,0,"lastname");

if ($num != 1)
{
    echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
	$data="";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM company WHERE companyid = '$companyid'";
    $result = mysql_query($query);
    //mysql_close();

    $companyname=@mysql_result($result,0,"companyname");
    $comstreet=@mysql_result($result,0,"street");
    $comcity=@mysql_result($result,0,"city");
    $comstate=@mysql_result($result,0,"state");
    $comzip=@mysql_result($result,0,"zip");
    $comphone=@mysql_result($result,0,"phone");
    $cominvoice_name=@mysql_result($result,0,"invoice_name");
    $cominvoice_remit=@mysql_result($result,0,"invoice_remit");

	$data.="<html><head></head>";

    $query = "SELECT * FROM apinvoice WHERE apinvoiceid = '$apinvoiceid'";
    $result = mysql_query($query);

    $vendor=@mysql_result($result,0,"vendor");
    $pc_type=@mysql_result($result,0,"pc_type");
    $notes=@mysql_result($result,0,"notes");
	$invoicenum=@mysql_result($result,0,"invoicenum");
	$businessid=@mysql_result($result,0,"businessid");

	/////save email
	$query34 = "UPDATE vendors SET email = '$email' WHERE vendorid = '$vendor'";
    $result34 = mysql_query($query34);

	$data.= "<body style=\"font-family: tahoma\" onload=\"window.print()\"><center><table width=100% cellspacing=0 cellpadding=0>";
    $data.= "<tr><td><font size=4 color=#993366>$companyname <br>Purchase Order #$invoicenum</td><td align=right><img src=\"/assets/images/vendlogo.jpg\"></tr>";
    $data.= "</table></center>";

    $notes=str_replace("\n","<br>",$notes);

    $query = "SELECT vendor_master.* FROM vendors,vendor_master WHERE vendors.vendorid = '$vendor' AND vendors.master = vendor_master.vendor_master_id";
    $result = mysql_query($query);

    $vendorname=@mysql_result($result,0,"vendor_name");
    $street=@mysql_result($result,0,"street");
	$street2=@mysql_result($result,0,"street2");
	$street3=@mysql_result($result,0,"street3");
	$city=@mysql_result($result,0,"city");
	$state=@mysql_result($result,0,"state");
	$zip=@mysql_result($result,0,"zip");
	$phone=@mysql_result($result,0,"phone");

	$query = "SELECT * FROM business WHERE businessid = '$businessid'";
    $result = mysql_query($query);

    $businessname=@mysql_result($result,0,"businessname");
	$bus_street=@mysql_result($result,0,"street");
	$bus_city=@mysql_result($result,0,"city");
	$bus_state=@mysql_result($result,0,"state");
	$bus_zip=@mysql_result($result,0,"zip");
	$bus_phone=@mysql_result($result,0,"phone");

    $data.= "<p><center><table width=100% cellspacing=0 cellpadding=0 style=\"font-size:12px;\">";
    $data.= "<tr><td width=47% style=\"border: 2px solid #999999;\">";
		$data.= "<font color=#999999>";
		$data.= "&nbsp;<b>$vendorname</b><br>";
		if($street!=""){$data.= "&nbsp;$street<br>";}
		if($street2!=""){$data.= "&nbsp;$street2<br>";}
		if($street3!=""){$data.= "&nbsp;$street3<br>";}
		$data.= "&nbsp;$city, $state $zip<br>";
		if($phone!=""){$data.= "&nbsp;$phone";}
		$data.= "</font>";
	$data.= "</td><td width=6%></td><td width=47% style=\"border: 2px solid #999999;\">";
		$data.= "<font color=#999999>";
		$data.= "&nbsp;<b>$businessname</b><br>";
		$data.= "&nbsp;$street<br>";
		$data.= "&nbsp;$city, $state $zip<br>";
		$data.= "&nbsp;$phone";
		$data.= "</font>";
	$data.= "</td></tr>";
    $data.= "</table></center><p>";

    ////////////DETAILS
    $data.= "<center><table width=100% cellspacing=0 cellpadding=0 style=\"border:1px solid #999999;font-size:12px;\">";
    $data.= "<tr bgcolor=#E8E7E7><td style=\"border:1px solid #999999;\" width=10%>&nbsp;<font color=black><b>Date</td><td style=\"border:1px solid #999999;\" width=15%><font color=black>&nbsp;<b>Category</td><td style=\"border:1px solid #999999;\" width=75%><font color=black>&nbsp;<b>Description</td><td style=\"border:1px solid #999999;\" width=15% align=right><font color=black>&nbsp;<b>Amount</td></tr>";

    $query = "SELECT * FROM apinvoicedetail WHERE apinvoiceid = '$apinvoiceid' ORDER BY date,itemid";
    $result = mysql_query($query);

    $mycolor="#FFFFDD";
	$total=0;
    while($r=mysql_fetch_array($result)){
       $pcard=$r["pcard"];
       $amount=$r["amount"];
       $apaccountid=$r["apaccountid"];
       $item=$r["item"];
       $qty=$r["qty"];
       $item_date=$r["date"];
       $catid=$r["categoryid"];

       $showdate=substr($item_date,5,2) . "/" . substr($item_date,8,2) . "/" . substr($item_date,2,2);

	   $query2 = "SELECT * FROM po_category WHERE categoryid = '$catid'";
	   $result2 = mysql_query($query2);

	   $cat_name=@mysql_result($result2,0,"category_name");

	   $total+=$amount;
	   $amount=number_format($amount,2);

       $data.= "<tr bgcolor=$mycolor><td style=\"border:1px solid #999999;\">&nbsp;$showdate</td><td style=\"border:1px solid #999999;\">&nbsp;$cat_name</td><td style=\"border:1px solid #999999;\">&nbsp;$item</td><td style=\"border:1px solid #999999;\" align=right>&nbsp;$amount</td></tr>";

       if($mycolor=="#FFFFDD"){$mycolor="white";}
       else{$mycolor="#FFFFDD";}
    }
	$total=number_format($total,2);
	$data.= "<tr bgcolor=#E8E7E7><td style=\"border:1px solid #999999;\" colspan=3 align=right><b>Total:</b>&nbsp;</td><td style=\"border:1px solid #999999;\" align=right><b>$$total</b></td></tr>";

    $data.= "</table></center><p><br><p><br>";

	$data.= "<font color=#999999>Signature __________________________________________</font>";

    $data.= "</body></html>";

	/////send email
	$subject="Treat America Purchase Order";
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	$headers .= "From: $myemail\r\n";
	mail($email, $subject, $data, $headers);
//////////////////////////////////////////////END////////////////////////////////////////////////////////////
       mysql_close();
}
?>