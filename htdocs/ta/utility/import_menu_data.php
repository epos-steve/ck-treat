<?php

define( 'DOC_ROOT', realpath( dirname( __FILE__ ).'/../../' ) );
require_once(DOC_ROOT.'/bootstrap.php');

if( isset( $_GET['progressbar'] ) )
{
	$import_id = $_GET['progressbar'];
	$progress = $_SESSION['import_'.$import_id.'_progress'];
	$error = $_SESSION['import_'.$import_id.'_error'] ?: '';
	if( !is_numeric( $progress ) )
		$progress = -2;
	//echo '{ "progress": '.$progress.', "error": "'.$error.'" }';
	echo json_encode( array( 'progress' => $progress, 'error' => $error ) );
	exit( 0 );
}

$import_id = $_POST['import_id'];

if( !isset( $_FILES['menu_data'] ) || $_FILES['menu_data']['error'] != UPLOAD_ERR_OK )
{
	session_start( );
	$progress = 101;
	$error = "<p>No menu data file selected or upload of data file failed.</p>";
	$_SESSION['import_'.$import_id.'_progress'] = $progress;
	$_SESSION['import_'.$import_id.'_error'] = $error;
	session_write_close( );
	//echo '{ "progress": '.$progress.', "error": "'.$error.'" }';
	echo json_encode( array( 'progress' => $progress, 'error' => $error ) );
	exit( 0 );
}

$import_filename = $_FILES['menu_data']['tmp_name'];
$import_filesize = filesize( $import_filename );

$handle = fopen($import_filename, 'r') or die("can't open file");

$return_url = $_POST['return_url'];
$businessid = $_POST['bid'];

$_SESSION['import_'.$import_id.'_progress'] = 0;
$_SESSION['import_'.$import_id.'_error'] = '';
session_write_close( );

$invalid_rows = array( );

$counter = 1;
$query = "SELECT COALESCE(MAX(pos_id),0)+1 AS pos_id FROM menu_items_new WHERE businessid = $businessid";
$result = Treat_DB_ProxyOld::query( $query, true );

$counter = mysql_result($result,0,"pos_id");

$row_count = $counter-1;
while ($pieces = fgetcsv($handle,NULL,",")){
	$import_position = ftell( $handle );
	$import_progress = ( $import_position / $import_filesize ) * 100;
	session_start( );
	$_SESSION['import_'.$import_id.'_progress'] = (int)floor( $import_progress );
	session_write_close( );

	$row_count++;

	// validate data
	$data_invalid = -1;
	if( count( $pieces ) < 7 )				$data_invalid = 0;
	if( empty( $pieces[0] ) )				$data_invalid = 1;
	if( empty( $pieces[1] ) )				$data_invalid = 2;
	if( trim( $pieces[2] ) == '' )			$data_invalid = 3;
	if( empty( $pieces[3] ) )				$pieces[3] = 0;	//$data_invalid = 4;
	if( empty( $pieces[4] ) )				$data_invalid = 5;
	if( empty( $pieces[5] ) )				$data_invalid = 6;
	if( !is_numeric( $pieces[6] ) )			$data_invalid = 7;

	if( $data_invalid > -1 )
	{
		$invalid_rows[$row_count] = array( 'error' => $data_invalid, 'data' => array_slice( $pieces, 0, 7 ) );
		continue;
	}

	$pieces[3] = trim( $pieces[3] );
	$pieces[4] = trim( $pieces[4] );
	$pieces[1] = str_replace( "'", "`", $pieces[1] );
	$pieces[5] = str_replace( "'", "`", $pieces[5] );

	////menu groups
	$query = "SELECT id FROM menu_groups_new WHERE businessid = $businessid AND name LIKE '$pieces[5]'";
	$result = Treat_DB_ProxyOld::query( $query, true );
	$num = mysql_num_rows( $result );

	if( $num == 0 ) {
		$new_group = \EE\Model\Menu\GroupsNew::create($businessid);
		$new_group->name = $pieces[5];
		$new_group->save();
		$group_id = $new_group->id;
	} else {
		$group_id = mysql_result( $result, 0, "id" );
	}

	/////menu items
	$uuid = \EE\Model\Menu\ItemNew::genUuid();
	$query = "INSERT INTO menu_items_new (businessid,pos_id,group_id,name,ordertype,item_code,upc,active,uuid_menu_items_new)
										VALUES
										($businessid,$counter,$group_id,'$pieces[1]',$pieces[6],'$pieces[0]','$pieces[2]',1, '$uuid')";
	$result = Treat_DB_ProxyOld::query( $query );
	$id = mysql_insert_id();
	$query = "INSERT INTO menu_items_price (businessid,menu_item_id,pos_price_id,price,cost)
										VALUES
										($businessid,$id,1,'$pieces[4]','$pieces[3]')";
	$result = Treat_DB_ProxyOld::query( $query );
	$counter++;
}

session_start( );
$counter--;
$import_status = '<p>Imported '.$counter.' rows';
if( $row_count != $counter )
	$import_status .= ' with '.( $row_count - $counter ).' errors';
$import_status .= '.</p>';
$invalid_row_report = '';
if( !empty( $invalid_rows ) )
{
	foreach( $invalid_rows as $row_number => $row_data )
		$invalid_row_report .= '<p>' .
								'<label>row '.$row_number.', error '.$row_data['error'].':</label> ' .
								implode( ',', $row_data['data'] ).'</p>';
	$invalid_row_report = '<p>There were problems with some of the data:</p>' .
							'<div style=\'height: 200px; overflow: scroll; white-space: nowrap;\'>'.$invalid_row_report.'</div>';
}
$progress = 101;
$error = '<p id=\'import_response\'>Menu import complete.</p>'.$import_status.$invalid_row_report;
$error = str_replace( '"', "\'", $error );
$_SESSION['import_'.$import_id.'_error'] = $error;
$_SESSION['import_'.$import_id.'_progress'] = $progress;
session_write_close( );

echo '{ "progress": '.$progress.', "error": "'.$error.'" }';

//header( 'Location: '.$return_url );
?>
