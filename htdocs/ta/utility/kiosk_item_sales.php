<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');

require_once( DOC_ROOT . '/ta/lib/class.menu_items.php' );
require_once( DOC_ROOT . '/ta/lib/class.Kiosk.php' );

ini_set( 'memory_limit', '256M' );

///create item array
$query = "select distinct item_code,name from menu_items_new
	JOIN business ON business.businessid = menu_items_new.businessid AND (business.companyid = 3 OR business.companyid = 6)
	WHERE menu_items_new.active = 0";
$result = DB_Proxy::query($query);

$items = array();
while($r = mysql_fetch_array($result)){
	$item_name = $r["name"];
	$item_code = $r["item_code"];
	
	$items[$item_code] = $item_name; 
}

///figure dates
$date = date( "Y-m-d", mktime( 0, 0, 0, date( "m" ), date( "d" ) - 14, date( "Y" ) ) );
$date2 = date( "Y-m-d", mktime( 0, 0, 0, date( "m" ), date( "d" ) - 1, date( "Y" ) ) );

foreach($items AS $item_code => $name){$data .= ",$name [$item_code]";}
$data .= "\r\n";

///loop through kiosks
$query = "SELECT businessid,businessname FROM business WHERE (companyid = 3 OR companyid = 6) AND categoryid = 4";
$result = DB_Proxy::query($query);

while($r = mysql_fetch_array($result)){
	$businessid = $r["businessid"];
	$businessname = $r["businessname"];
	
	$data .= "$businessname";

	foreach($items AS $item_code => $item_name){
		$temp_date = $date;

		$query2 = "SELECT pos_id FROM menu_items_new WHERE businessid = $businessid AND item_code = '$item_code'";
		$result2 = DB_Proxy::query($query2);

		$pos_id = mysql_result($result2,0,"pos_id");

		$items_sold = menu_items::items_sold($pos_id,$businessid,$date,$date2);

		$data .= ",$items_sold";

	}
	$data .= "\r\n";
}

header("Content-type: application/x-msdownload");
header("Content-Disposition: attachment; filename=kioskSales.csv");
header("Pragma: no-cache");
header("Expires: 100");
echo $data;
?>
