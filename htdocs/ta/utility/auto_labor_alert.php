<?php

function nextday($nextd,$day_format=''){ //Function returns next day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monn = substr($nextd, 5, 2);
   $dayn = substr($nextd, 8, 2);
   $yearn = substr($nextd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monn, $dayn+1, $yearn));
   return $tempdate;
}

function prevday($prevd,$day_format=''){ //Function returns previous day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monp = substr($prevd, 5, 2);
   $dayp = substr($prevd, 8, 2);
   $yearp = substr($prevd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monp, $dayp-1, $yearp));
   return $tempdate;
}

function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}


define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');
$style = "text-decoration:none";
$date1=date("Y-m-d");
$date2=date("Y-m-d");

mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");

$query = "SELECT * FROM labor_alerts ORDER BY businessid";
$result = mysql_query($query);
$num = mysql_numrows($result);

$lastbusinessid=-1;
$emailmessage="";
$maxhours=0;
$num--;
while($num>=0){
   $businessid=@mysql_result($result,$num,"businessid");
   $loginid=@mysql_result($result,$num,"loginid");
   $hours=@mysql_result($result,$num,"hours");

   $query2 = "SELECT businessname FROM business wHERE businessid = '$businessid'";
   $result2 = mysql_query($query2);

   $busname=@mysql_result($result2,0,"businessname");

   $query2 = "SELECT email FROM login wHERE loginid = '$loginid'";
   $result2 = mysql_query($query2);

   $email=@mysql_result($result2,0,"email");

   if($lastbusinessid!=$businessid){
      $labor_hours=array();
      $labor_fname=array();
      $labor_lname=array();

      $query2 = "SELECT companyid FROM business WHERE businessid = '$businessid'";
      $result2 = mysql_query($query2);

      $companyid=@mysql_result($result2,0,"companyid");

      $query2 = "SELECT week_start FROM company WHERE companyid = '$companyid'";
      $result2 = mysql_query($query2);

      $week_start=@mysql_result($result2,0,"week_start");

      $date1=$date2;
      while(dayofweek($date1)!=$week_start){$date1=prevday($date1);}

      $query2 = "SELECT loginid,firstname,lastname FROM login WHERE oo2 = '$businessid' ORDER BY lastname DESC, firstname DESC";
      $result2 = mysql_query($query2);
      $num2 = mysql_numrows($result2);

      $num2--;
      while($num2>=0){
         $emp_loginid=@mysql_result($result2,$num2,"loginid");
         $lastname=@mysql_result($result2,$num2,"lastname");
         $firstname=@mysql_result($result2,$num2,"firstname");

         $query3 = "SELECT SUM(hours) AS totalamt FROM labor,payroll_code WHERE labor.loginid = '$emp_loginid' AND labor.businessid = '$businessid' AND labor.date >= '$date1' AND labor.date <= '$date2' AND labor.coded = payroll_code.codeid AND payroll_code.code LIKE 'REG'";
         $result3 = mysql_query($query3);
         $labor_hours[$emp_loginid]=round(@mysql_result($result3,0,"totalamt"),2);
         $labor_fname[$emp_loginid]=$firstname;
         $labor_lname[$emp_loginid]=$lastname;

         $num2--;
      }
   }

   foreach($labor_hours AS $key => $value){
      if($value>=$hours){$emailmessage.="$labor_fname[$key] $labor_lname[$key], $value hours\n";}
   }

   if($emailmessage!=""){
       $todayis = date("l, F j, Y, g:i a") ;
       $from = "From: support@treatamerica.com\r\n";
       $subject = "Labor Hours Alert for $busname";

       mail($email, $subject, $emailmessage, $from);
       echo "Emailed: $email<br>";
   }

   $emailmessage="";
   $lastbusinessid=$businessid;
   $num--;
}

mysql_close();
echo "Done";
?>