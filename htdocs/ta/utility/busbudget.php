<?php

define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');

function money($diff){
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+3);
        if ($diff > 0 && $diff < '0.01'){$diff="0.01";}
        elseif($diff < 0 && $diff > '-0.01'){$diff="-0.01";}
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      if ($month == "10"){$month="11";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='32')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='08' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      $day=$day-31;
      if ($day<10){$day="0$day";}
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";}
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";}
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";}
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";}
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

if ( !defined('DOC_ROOT'))
{
	define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
}
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');
$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");
$creditamount="creditamount";
$creditidnty="creditidnty";
$creditdate="creditdate";
$debitamount="debitamount";
$debitidnty="debitidnty";
$debitdate="debitdate";
$quatertotal=0;
$monthtotal=0;
$total=0;
$counter=0;
$findme='.';
$double='.00';
$single='0';
$double2='00';

$user = isset($_COOKIE["usercook"])?$_COOKIE["usercook"]:'';
$pass = isset($_COOKIE["passcook"])?$_COOKIE["passcook"]:'';
$view = isset($_COOKIE["viewcook"])?$_COOKIE["viewcook"]:'';
$date1 = isset($_COOKIE["date1cook"])?$_COOKIE["date1cook"]:'';
$date2 = isset($_COOKIE["date2cook"])?$_COOKIE["date2cook"]:'';
$businessid = isset($_GET["bid"])?$_GET["bid"]:'';
setcookie("caterbus",$businessid);
$companyid = isset($_GET["cid"])?$_GET["cid"]:'';
$view2 = isset($_GET["view2"])?$_GET["view2"]:'';
$roomeditid = isset($_GET["rid"])?$_GET["rid"]:'';

if ($businessid==""&&$companyid==""){
   $businessid = isset($_POST["businessid"])?$_POST["businessid"]:'';
   $companyid = isset($_POST["companyid"])?$_POST["companyid"]:'';
   $view2 = isset($_POST["view2"])?$_POST["view2"]:'';
   $date1 = isset($_POST["date1"])?$_POST["date1"]:'';
   $date2 = isset($_POST["date2"])?$_POST["date2"]:'';
   $findinv = isset($_POST["findinv"])?$_POST["findinv"]:'';
}

if ($date1==""&&$date2==""){
   $previous=$today;
   for ($count=1;$count<=31;$count++){$previous=prevday($previous);}
   $date1=$previous;$date2=$today;$view='All';
}

mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = mysql_query($query);
$num=mysql_numrows($result);

$security_level=@mysql_result($result,0,"security_level");
$mysecurity=@mysql_result($result,0,"security");
$bid=@mysql_result($result,0,"businessid");
$cid=@mysql_result($result,0,"companyid");
$bid2=@mysql_result($result,0,"busid2");
$bid3=@mysql_result($result,0,"busid3");
$bid4=@mysql_result($result,0,"busid4");
$bid5=@mysql_result($result,0,"busid5");
$bid6=@mysql_result($result,0,"busid6");
$bid7=@mysql_result($result,0,"busid7");
$bid8=@mysql_result($result,0,"busid8");
$bid9=@mysql_result($result,0,"busid9");
$bid10=@mysql_result($result,0,"busid10");
$loginid=@mysql_result($result,0,"loginid");

$pr1=@mysql_result($result,0,"payroll");
$pr2=@mysql_result($result,0,"pr2");
$pr3=@mysql_result($result,0,"pr3");
$pr4=@mysql_result($result,0,"pr4");
$pr5=@mysql_result($result,0,"pr5");
$pr6=@mysql_result($result,0,"pr6");
$pr7=@mysql_result($result,0,"pr7");
$pr8=@mysql_result($result,0,"pr8");
$pr9=@mysql_result($result,0,"pr9");
$pr10=@mysql_result($result,0,"pr10");

	$query4 = "SELECT * FROM security WHERE securityid = '$mysecurity'";
    $result4 = mysql_query($query4);

    $sec_bus_sales=@mysql_result($result4,0,"bus_sales");
    $sec_bus_invoice=@mysql_result($result4,0,"bus_invoice");
    $sec_bus_order=@mysql_result($result4,0,"bus_order");
    $sec_bus_payable=@mysql_result($result4,0,"bus_payable");
    $sec_bus_inventor1=@mysql_result($result4,0,"bus_inventor1");
    $sec_bus_control=@mysql_result($result4,0,"bus_control");
    $sec_bus_menu=@mysql_result($result4,0,"bus_menu");
    $sec_bus_order_setup=@mysql_result($result4,0,"bus_order_setup");
    $sec_bus_request=@mysql_result($result4,0,"bus_request");
    $sec_route_collection=@mysql_result($result4,0,"route_collection");
    $sec_route_labor=@mysql_result($result4,0,"route_labor");
    $sec_route_detail=@mysql_result($result4,0,"route_detail");
    $sec_bus_labor=@mysql_result($result4,0,"bus_labor");
	$sec_bus_budget=@mysql_result($result4,0,"bus_budget");

if ($num != 1 || $user == "" || $pass == "" || $sec_bus_budget==0 || ($businessid == $bid && $pr1 != 1)||($businessid == $bid2 && $pr2 != 1)||($businessid == $bid3 && $pr3 != 1)||($businessid == $bid4 && $pr4 != 1)||($businessid == $bid5 && $pr5 != 1)||($businessid == $bid6 && $pr6 != 1)||($businessid == $bid7 && $pr7 != 1)||($businessid == $bid8 && $pr8 != 1)||($businessid == $bid9 && $pr9 != 1)||($businessid == $bid10 && $pr10 != 1))
{
    echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
?>
<head>
<script language="JavaScript"
   type="text/JavaScript">
function changePage(newLoc)
 {
   nextPage = newLoc.options[newLoc.selectedIndex].value

   if (nextPage != "")
   {
      document.location.href = nextPage
   }
 }
</script>
</head>
<?
    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%><tr><td colspan=2>";
    echo "<a href=businesstrack.php><img src=logo.jpg border=0 height=43 width=205></a><p></td></tr>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM company WHERE companyid = '$companyid'";
    $result = mysql_query($query);
    //mysql_close();

    $companyname=@mysql_result($result,0,"companyname");

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM business WHERE companyid = '$companyid' AND businessid = '$businessid'";
    $result = mysql_query($query);
    //mysql_close();

    $businessname=@mysql_result($result,0,"businessname");
    $businessid=@mysql_result($result,0,"businessid");
    $street=@mysql_result($result,0,"street");
    $city=@mysql_result($result,0,"city");
    $state=@mysql_result($result,0,"state");
    $zip=@mysql_result($result,0,"zip");
    $phone=@mysql_result($result,0,"phone");
    $caternum=@mysql_result($result,0,"caternum");
    $caterdays=@mysql_result($result,0,"caterdays");
    $srvchrg=@mysql_result($result,0,"srvchrg");
    $srvchrgpcnt=@mysql_result($result,0,"srvchrgpcnt");
    $weekend=@mysql_result($result,0,"weekend");
    $unit_add=@mysql_result($result,0,"unit_add");
    $inv_note=@mysql_result($result,0,"inv_note");
    $cater_min=@mysql_result($result,0,"cater_min");
    $cater_spread=@mysql_result($result,0,"cater_spread");
	$cater_costcenter=@mysql_result($result,0,"cater_costcenter");
    $bus_unit=@mysql_result($result,0,"unit");

    if($cater_spread==0){$catspr1="SELECTED";}
    elseif($cater_spread==15){$catspr15="SELECTED";}
    elseif($cater_spread==30){$catspr30="SELECTED";}
    elseif($cater_spread==45){$catspr45="SELECTED";}
    elseif($cater_spread==60){$catspr60="SELECTED";}

	if($cater_costcenter==1){$check_costcenter="CHECKED";}

    //echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";
    echo "<tr bgcolor=#CCCCFF><td width=1%><img src=weblogo.jpg width=80 height=75></td><td><font size=4><b>$businessname #$bus_unit</b></font><br>$street<br>$city, $state $zip<br>$phone</td>";
    echo "<td align=right valign=top>";
    echo "<br><FORM ACTION=editbus.php method=post>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<input type=hidden name=businessid value=$businessid>";
    echo "<input type=hidden name=companyid value=$companyid>";
    echo "<INPUT TYPE=submit VALUE=' Store Setup '></FORM></td></tr>";
    //echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";

    echo "<tr><td colspan=3><font color=#999999>";
    if ($sec_bus_sales>0){echo "<a href=busdetail.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Sales</font></a>";}
    if ($sec_bus_invoice>0){echo " | <a href=businvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Invoicing</font></a>";}
    if ($sec_bus_order>0){echo " | <a href=buscatertrack.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Orders</font></a>";}
    if ($sec_bus_payable>0){echo " | <a href=busapinvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Payables</font></a>";}
    if ($sec_bus_inventor1>0){echo " | <a href=businventor.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Inventory</font></a>";}
    if ($sec_bus_control>0){echo " | <a href=buscontrol.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Control Sheet</font></a>";}
    if ($sec_bus_menu>0){echo " | <a href=buscatermenu.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Menus</font></a>";}
    if ($sec_bus_order_setup>0){echo " | <a href=buscaterroom.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Order Setup</font></a>";}
    if ($sec_bus_request>0){echo " | <a href=busrequest.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Requests</font></a>";}
    if ($sec_route_collection>0||$sec_route_labor>0||$sec_route_detail>0){echo " | <a href=busroute_collect.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Route Collections</font></a>";}
    if ($sec_bus_labor>0){echo " | <a href=buslabor.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Labor</font></a>";}
    if ($sec_bus_budget>0||($businessid == $bid && $pr1 == 1)||($businessid == $bid2 && $pr2 == 1)||($businessid == $bid3 && $pr3 == 1)||($businessid == $bid4 && $pr4 == 1)||($businessid == $bid5 && $pr5 == 1)||($businessid == $bid6 && $pr6 == 1)||($businessid == $bid7 && $pr7 == 1)||($businessid == $bid8 && $pr8 == 1)||($businessid == $bid9 && $pr9 == 1)||($businessid == $bid10 && $pr10 == 1)){echo " | <a href=busbudget.php?bid=$businessid&cid=$companyid><font color=red onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='red' style='text-decoration:none'>Budget</font></a>";}
	echo "</td></tr>";

    echo "</table></center><p>";

if ($security_level>0){
    echo "<p><center><table width=90%><tr><td width=50%><form action=businvoice.php method=post>";
    echo "<select name=gobus onChange='changePage(this.form.gobus)'>";

    $districtquery="";
    if ($security_level>2&&$security_level<7){
       ////mysql_connect($dbhost,$username,$password);
       ////@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM login_district WHERE loginid = '$loginid'";
       $result = mysql_query($query);
       $num=mysql_numrows($result);
       ////mysql_close();

       $num--;
       while($num>=0){
          $districtid=@mysql_result($result,$num,"districtid");
          if($districtquery==""){$districtquery="districtid = '$districtid'";}
          else{$districtquery="$districtquery OR districtid = '$districtid'";}
          $num--;
       }
    }

    ////mysql_connect($dbhost,$username,$password);
    ////@mysql_select_db($database) or die( "Unable to select database");
    if($security_level>6){$query = "SELECT * FROM business WHERE companyid = '$companyid' ORDER BY businessname DESC";}
    elseif($security_level==2||$security_level==1){$query = "SELECT * FROM business WHERE companyid = '$companyid' AND (businessid = '$bid' OR businessid = '$bid2' OR businessid = '$bid3' OR businessid = '$bid4' OR businessid = '$bid5' OR businessid = '$bid6' OR businessid = '$bid7' OR businessid = '$bid8' OR businessid = '$bid9' OR businessid = '$bid10') ORDER BY businessname DESC";}
    elseif($security_level>2&&$security_level<7){$query = "SELECT * FROM business WHERE companyid = '$companyid' AND ($districtquery) ORDER BY businessname DESC";}
    $result = mysql_query($query);
    $num=mysql_numrows($result);
    ////mysql_close();

    $num--;
    while($num>=0){
       $businessname=@mysql_result($result,$num,"businessname");
       $busid=@mysql_result($result,$num,"businessid");

       if ($busid==$businessid){$show="SELECTED";}
       else{$show="";}

       echo "<option value=busbudget.php?bid=$busid&cid=$companyid $show>$businessname</option>";
       $num--;
    }

    echo "</select></td><td></form></td><td width=50% align=right></td></tr></table></center><p>";
}

	echo "<center><table width=90% style=\"border:1px solid #CCCCCC;\" bgcolor=#E8E7E7 cellspacing=0 cellpadding=0>";

	echo "<tr><td style=\"border:1px solid #CCCCCC;\" colspan=2><b>2010 Budget</b></td></tr>";
	echo "<tr><td style=\"border:1px solid #CCCCCC;\" width=50%>&nbsp;<img src=excel.gif height=46 width=47 border=0> <form action=download.php methos=post style=\"margin:0;padding:0;display:inline;\"><input type=hidden name=dl value=1><input type=hidden name=companyid value=$companyid><input type=hidden name=businessid value=$businessid><input type=hidden name=unit value=$unit><input type=submit value='Download'></form></td><td rowspan=2 style=\"border:1px solid #CCCCCC;\">&nbsp;</td></tr>";
	echo "<tr><td style=\"border:1px solid #CCCCCC;\">&nbsp;Upload Revised Budget: <form action=download.php methos=post style=\"margin:0;padding:0;display:inline;\"><input type=hidden name=companyid value=$companyid><input type=hidden name=businessid value=$businessid><input type=hidden name=unit value=$unit><input type=file name=uploadedfile size=40><input type=submit value='GO'></form></td></tr>";

	echo "</table></center>";
//////END TABLE////////////////////////////////////////////////////////////////////////////////////

    echo "<br><FORM ACTION=businesstrack.php method=post>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<center><INPUT TYPE=submit VALUE=' Return to Main Page'></FORM></center></body>";

	google_page_track();
}
mysql_close();
?>