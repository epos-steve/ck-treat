<?php
if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
require_once( DOC_ROOT . '/bootstrap.php' );

$user = Treat_Controller_Abstract::getSessionCookieVariable( 'usercook' );
$pass = Treat_Controller_Abstract::getSessionCookieVariable( 'passcook' );
$businessid = Treat_Controller_Abstract::getPostOrGetVariable( 'businessid' );
$companyid = Treat_Controller_Abstract::getPostOrGetVariable( 'companyid' );
$date1 = Treat_Controller_Abstract::getPostOrGetVariable( 'date1' );
$prevperiod = Treat_Controller_Abstract::getPostOrGetVariable( 'prevperiod' );
$sec_level = Treat_Controller_Abstract::getPostOrGetVariable( 'sec_level' );
$week = Treat_Controller_Abstract::getPostOrGetVariable( 'week' );

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query( $query );
$num = @mysql_numrows( $result );

$security = @mysql_result( $result, 0, 'security' );

$location="busroute_detail.php?bid=$businessid&cid=$companyid&date1=$date1";
header('Location: ./' . $location);

if ( $num != 1 )
{
	echo "<center><h3>Failed</h3></center>";
}

else
{
	if($sec_level<2){$export=0;}
	else{$export=0;}
	$mydate=date("Y-m-d");

	/////////////////////////
	///SUBMIT DEPARTMENTS////
	/////////////////////////
	if($week==1){$temp_date=$prevperiod;}
	else{$temp_date=$date1;}

	//for($counter=1;$counter<=6;$counter++){$temp_date=nextday($temp_date);}

	$query = "SELECT * FROM security_departments WHERE securityid = '$security'";
	$result = Treat_DB_ProxyOld::query( $query );
	$num = @mysql_numrows( $result );

	$submitted_depts="";

	while($r=mysql_fetch_array($result)){
		$dept_id=$r["dept_id"];
	
		$query2 = "SELECT * FROM submit_labor_dept WHERE businessid = '$businessid' AND date = '$temp_date' AND dept_id = '$dept_id'";
		$result2 = Treat_DB_ProxyOld::query( $query2, true );
		$num2 = @mysql_numrows( $result2 );

		if($num2==0){
			$query3 = "INSERT INTO submit_labor_dept (businessid,date,dept_id) VALUES ('$businessid','$temp_date','$dept_id')";
			$result3 = Treat_DB_ProxyOld::query( $query3, true );

			$submitted_depts.=", $dept_id";
		}

	}

	/////////////////////////
	///CHECK FOR ALL DEPTS///
	/////////////////////////
	$query = "SELECT COUNT(dept_id) AS totaldepts FROM payroll_department";
	$result = Treat_DB_ProxyOld::query( $query, true );

	$totaldepts = @mysql_result( $result, 0, 'totaldepts' );

	$query = "SELECT COUNT(businessid) AS totalsubmits FROM submit_labor_dept WHERE businessid = '$businessid' AND date = '$temp_date'";
	$result = Treat_DB_ProxyOld::query( $query, true );

	$totalsubmits = @mysql_result( $result, 0, 'totalsubmits' );

	/////////////////////////
	///SUBMIT BUSINESS///////
	/////////////////////////

	if($totaldepts==$totalsubmits){

		if($mydate>=$prevperiod&&$week==1){
			$temp_date=$prevperiod;

			$query = "INSERT INTO audit_labor (date,user,comment,businessid) VALUES ('$temp_date','$user','<font color=red><b>SUBMITTED</b></font>','$businessid')";
			$result = Treat_DB_ProxyOld::query( $query, true );

			for ($counter=1;$counter<=7;$counter++){

				$query = "SELECT * FROM submit_labor WHERE businessid = '$businessid' AND date = '$temp_date'";
				$result = Treat_DB_ProxyOld::query( $query, true );
				$num = @mysql_numrows( $result );

				if($num==0)
				{
					$query = "INSERT INTO submit_labor (businessid,date,export) VALUES ('$businessid','$temp_date','$export')";
					$result = Treat_DB_ProxyOld::query( $query, true );
					$go=1;
				}
				elseif($num>0&&$sec_level>1){
					$query = "UPDATE submit_labor SET export = '0' WHERE businessid = '$businessid' AND date = '$temp_date'";
					$result = Treat_DB_ProxyOld::query( $query, true );
				}

				$temp_date=prevday($temp_date);
			}
		}

		if($mydate>=$date1&&$week==2){
			$temp_date=$date1;

			$query = "INSERT INTO audit_labor (date,user,comment,businessid) VALUES ('$temp_date','$user','<font color=red><b>SUBMITTED</b></font>','$businessid')";
			$result = Treat_DB_ProxyOld::query( $query, true );

			for ($counter=1;$counter<=7;$counter++){

				$query = "SELECT * FROM submit_labor WHERE businessid = '$businessid' AND date = '$temp_date'";
				$result = Treat_DB_ProxyOld::query( $query, true );
				$num = @mysql_numrows( $result );

				if($num==0)
				{
					$query = "INSERT INTO submit_labor (businessid,date,export) VALUES ('$businessid','$temp_date','$export')";
					$result = Treat_DB_ProxyOld::query( $query, true );
					$go=1;
				}
				elseif($num>0&&$sec_level>1){
					$query = "UPDATE submit_labor SET export = '0' WHERE businessid = '$businessid' AND date = '$temp_date'";
					$result = Treat_DB_ProxyOld::query( $query, true );
				}

				$temp_date=prevday($temp_date);
			}
		}

		if($go==1&&$sec_level==1){
			$query = "SELECT email_labor FROM vend_settings WHERE businessid = '$businessid'";
			$result = Treat_DB_ProxyOld::query( $query, true );

			$email_labor = @mysql_result( $result, 0, 'email_labor' );

			$query = "SELECT businessname FROM business WHERE businessid = '$businessid'";
			$result = Treat_DB_ProxyOld::query( $query, true );

			$businessname = @mysql_result( $result, 0, 'businessname' );

			if($email_labor!=""){
				$todayis = date("l, F j, Y, g:i a");
				$visitormail="support@treatamerica.com";
				$from = "From: $visitormail\r\n";
				$subject = "Commissions Submitted";

				$message = "Vending Commissions for $businessname have been submitted by $user ($todayis).\n\nSupport";

				mail($email_labor, $subject, $message, $from);
			}
		}

	}

	$query = "INSERT INTO audit_labor (date,user,comment,businessid) VALUES ('$date','$user','<font color=red><b>SUBMITTED $submitted_depts</b></font>','$businessid')";
	$result = Treat_DB_ProxyOld::query( $query, true );

	
}
//mysql_close();
?>