<?php

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+4);
        $diff=round($diff, 2);
        if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
        elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
        $diff = substr($diff, 0, $dot+3);
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        return $diff;
}

function nextday($nextd,$day_format){ //Function returns next day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monn = substr($nextd, 5, 2);
   $dayn = substr($nextd, 8, 2);
   $yearn = substr($nextd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monn, $dayn+1, $yearn));
   return $tempdate;
}

function prevday($prevd,$day_format){ //Function returns previous day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monp = substr($prevd, 5, 2);
   $dayp = substr($prevd, 8, 2);
   $yearp = substr($prevd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monp, $dayp-1, $yearp));
   return $tempdate;
}

function dateDiff($start, $end) {
  $start_ts = strtotime($start);
  $end_ts = strtotime($end);
  $diff = $end_ts - $start_ts;
  return floor($diff / 86400);
}

function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}

function daysBetween($start_date, $end_date, $weekDay) {
   $totaldays=0;
   while($start_date<=$end_date){
      if($weekDay==dayofweek($start_date)){$totaldays++;}
      $start_date=nextday($start_date);
   }
   return $totaldays;
}

function rndm_color_code($b_safe = TRUE) {
    
    //make sure the parameter is boolean
    if(!is_bool($b_safe)) {return FALSE;}
    
    //if a browser safe color is requested then set the array up
    //so that only a browser safe color can be returned
    if($b_saafe) {
        $ary_codes = array('00','33','66','99','CC','FF');
        $max = 5; //the highest array offest
    //if a browser safe color is not requested then set the array
    //up so that any color can be returned.
    } else {
        $ary_codes = array();
        for($i=0;$i<16;$i++) {
            $t_1 = dechex($i);
            for($j=0;$j<16;$j++) {
                $t_2 = dechex($j);
                $ary_codes[] = "$t_1$t_2";
            } //end for j
        } //end for i
        $max = 256; //the highest array offset
    } //end if
    
    $retVal = '';
    
    //generate a random color code
    for($i=0;$i<3;$i++) {
        $offset = rand(0,$max);
        $retVal .= $ary_codes[$offset];
    } //end for i
    
    return $retVal;
} //end rndm_color_code

//include("db.php");
define('DOC_ROOT', dirname(dirname(__FILE__)));
require_once(DOC_ROOT.'/bootstrap.php');
$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");

/*$user=$_COOKIE["usercook"];
$pass=$_COOKIE["passcook"];
$companyid=$_COOKIE["compcook"];*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook');
$companyid = \EE\Controller\Base::getSessionCookieVariable('compcook');

/*$budgetyear=$_POST["budgetyear"];
$businessid=$_POST["businessid"];
$finalize=$_POST["finalize"];*/

$budgetyear = \EE\Controller\Base::getPostVariable('budgetyear');
$businessid = \EE\Controller\Base::getPostVariable('businessid');
$finalize = \EE\Controller\Base::getPostVariable('finalize');

/*mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( mysql_error());*/

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=Treat_DB_ProxyOld::mysql_numrows($result);

$location="budget_calc2.php";
header('Location: ./' . $location);

if ($num != 1)
{
    echo "<center><h3>Failed</h3></center>";
}

else
{
  if($finalize==1){
		//$updatebudget=unserialize(urldecode($_POST["updatebudget"]));
	  $updatebudget=unserialize(urldecode(\EE\Controller\Base::getPostVariable('updatebudget')));
		
		foreach($updatebudget AS $key => $value){
			$query = "SELECT budgetid FROM vend_budget WHERE businessid = '$businessid' AND date = '$key' AND type = '10'";
			$result = Treat_DB_ProxyOld::query($query);
			$num=Treat_DB_ProxyOld::mysql_numrows($result);
		
			if($num==0 && $value != 0){
				$query = "INSERT INTO vend_budget (businessid,date,type,amount) VALUES ('$businessid','$key','10','$value')";
				$result = Treat_DB_ProxyOld::query($query);
			}
			else{
				$budgetid = Treat_DB_ProxyOld::mysql_result($result,0,"budgetid");
			
				$query = "UPDATE vend_budget SET amount = '$value' WHERE budgetid = '$budgetid'";
				$result = Treat_DB_ProxyOld::query($query);
			}
		}
  }
  else{
   $startdate="$budgetyear-01-01";
   $enddate="$budgetyear-12-31";
   
   /////adjustments
   while($startdate<=$enddate){
		$adjust = $_POST["adj$startdate"];
		
		$query = "SELECT id FROM budget_adjust WHERE businessid = '$businessid' AND date = '$startdate'";
		$result = Treat_DB_ProxyOld::query($query);
		$num=Treat_DB_ProxyOld::mysql_numrows($result);
		
		if($num==0 && $adjust != 0){
			$query = "INSERT INTO budget_adjust (businessid,date,amount) VALUES ('$businessid','$startdate','$adjust')";
			$result = Treat_DB_ProxyOld::query($query);
		}
		else{
			$id = Treat_DB_ProxyOld::mysql_result($result,0,"id");
			
			$query = "UPDATE budget_adjust SET amount = '$adjust' WHERE id = '$id'";
			$result = Treat_DB_ProxyOld::query($query);
		}
		
		$startdate=nextday($startdate);
   }
   
   /////monthly budgets
   $query = "SELECT companyid,unit FROM business WHERE businessid = '$businessid'";
   $result = Treat_DB_ProxyOld::query($query);
   
   $companyid = Treat_DB_ProxyOld::mysql_result($result,0,"companyid");
   $unit = Treat_DB_ProxyOld::mysql_result($result,0,"unit");
   
	$jan = $_POST["budget1"];
	$feb = $_POST["budget2"];
	$mar = $_POST["budget3"];
	$apr = $_POST["budget4"];
    $may = $_POST["budget5"];
	$jun = $_POST["budget6"];
	$jul = $_POST["budget7"];
	$aug = $_POST["budget8"];
	$sep = $_POST["budget9"];
	$oct = $_POST["budget10"];
	$nov = $_POST["budget11"];
	$dec = $_POST["budget11"];
	
	$query = "SELECT budgetid FROM budget WHERE businessid = '$unit' AND companyid = '$companyid' AND year = '$budgetyear' AND type = '10'";
	$result = Treat_DB_ProxyOld::query($query);
	$num = Treat_DB_ProxyOld::mysql_numrows($result);
	
	if($num==0){
		$query = "INSERT INTO budget (businessid,companyid,year,type,`01`,`02`,`03`,`04`,`05`,`06`,`07`,`08`,`09`,`10`,`11`,`12`) VALUES ('$unit','$companyid','$budgetyear','10','$jan','$feb','$mar','$apr','$may','$jun','$jul','$aug','$sep','$oct','$nov','$dec')";
		$result = Treat_DB_ProxyOld::query($query);
	}
	else{
		$budgetid = Treat_DB_ProxyOld::mysql_result($result,0,"budgetid");
	
		$query = "UPDATE budget SET `01` = '$jan', `02` = '$feb', `03` = '$mar', `04` = '$apr', `05`= '$may', `06` = '$jun', `07` = '$jul', `08` = '$aug', `09` = '$sep', `10` = '$oct', `11` = '$nov', `12` = '$dec' WHERE budgetid = '$budgetid'";
		$result = Treat_DB_ProxyOld::query($query);
	}
  }
}
//mysql_close();
?>