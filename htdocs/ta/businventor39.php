<?php

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+4);
        $diff=round($diff, 2);
        if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
        elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
        $diff = substr($diff, 0, $dot+3);
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      elseif ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      elseif ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='32')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      $day=$day-31;  
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";} 
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}

//include("db.php");
define('DOC_ROOT', dirname(dirname(__FILE__)));
require_once(DOC_ROOT.'/bootstrap.php');

$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");
$creditamount="creditamount";
$creditidnty="creditidnty";
$creditdate="creditdate";
$debitamount="debitamount";
$debitidnty="debitidnty";
$debitdate="debitdate";
$quatertotal=0;
$monthtotal=0;
$total=0;
$counter=0;
$findme='.';
$double='.00';
$single='0';
$double2='00';

/*$user=$_COOKIE["usercook"];
$pass=$_COOKIE["passcook"];
$businessid=$_GET["bid"];
$companyid=$_GET["cid"];
$editid=$_GET["editid"];
$view2=$_GET["view2"];
$apinv=$_GET["apinv"];
$editinvarea=$_GET["editinvarea"];
$newvendor=$_GET["newvendor"];
$showhidden=$_GET["showhidden"];*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook');
$businessid = \EE\Controller\Base::getGetVariable('bid');
$companyid = \EE\Controller\Base::getGetVariable('cid');
$editid = \EE\Controller\Base::getGetVariable('editid');
$view2 = \EE\Controller\Base::getGetVariable('view2');
$apinv = \EE\Controller\Base::getGetVariable('apinv');
$editinvarea = \EE\Controller\Base::getGetVariable('editinvarea');
$newvendor = \EE\Controller\Base::getGetVariable('newvendor');
$showhidden = \EE\Controller\Base::getGetVariable('showhidden');

//if ($showhidden==""){$showhidden=$_COOKIE["showhidden"];}
if ($showhidden==""){$showhidden = \EE\Controller\Base::getSessionCookieVariable('showhidden');}
if ($showhidden==""){$showhidden=1;}

if ($businessid==""&&$companyid==""){
   /*$businessid=$_POST["businessid"];
   $companyid=$_POST["companyid"];
   $editid=$_POST["editid"];
   $view2=$_POST["view2"];
   $view=$_POST["view"];
   $date1=$_POST["date1"];
   $date2=$_POST["date2"];
   $findinv=$_POST["findinv"];*/
	
	$businessid = \EE\Controller\Base::getPostVariable('businessid');
	$companyid = \EE\Controller\Base::getPostVariable('companyid');
	$editid = \EE\Controller\Base::getPostVariable('editid');
	$view2 = \EE\Controller\Base::getPostVariable('view2');
	$view = \EE\Controller\Base::getPostVariable('view');
	$date1 = \EE\Controller\Base::getPostVariable('date1');
	$date2 = \EE\Controller\Base::getPostVariable('date2');
	$findinv = \EE\Controller\Base::getPostVariable('findinv');
}

/*mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");*/

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=Treat_DB_ProxyOld::mysql_numrows($result);
//mysql_close();

$loginid=Treat_DB_ProxyOld::mysql_result($result,0,"loginid");
$security_level=Treat_DB_ProxyOld::mysql_result($result,0,"security_level");
$mysecurity=Treat_DB_ProxyOld::mysql_result($result,0,"security");
$bid=Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
$bid2=Treat_DB_ProxyOld::mysql_result($result,0,"busid2");
$bid3=Treat_DB_ProxyOld::mysql_result($result,0,"busid3");
$bid4=Treat_DB_ProxyOld::mysql_result($result,0,"busid4");
$bid5=Treat_DB_ProxyOld::mysql_result($result,0,"busid5");
$bid6=Treat_DB_ProxyOld::mysql_result($result,0,"busid6");
$bid7=Treat_DB_ProxyOld::mysql_result($result,0,"busid7");
$bid8=Treat_DB_ProxyOld::mysql_result($result,0,"busid8");
$bid9=Treat_DB_ProxyOld::mysql_result($result,0,"busid9");
$bid10=Treat_DB_ProxyOld::mysql_result($result,0,"busid10");
$cid=Treat_DB_ProxyOld::mysql_result($result,0,"companyid");

if ($num != 1 || ($security_level==1 && ($bid != $businessid || $cid != $companyid)) || $user == "" || $pass == "")
{
    echo "<center><h3>Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
    setcookie("showhidden",$showhidden);
?>
<head>

<script type="text/javascript" src="javascripts/prototype.js"></script>
<script type="text/javascript" src="javascripts/scriptaculous.js"></script>

<?
echo "
<script type=\"text/javascript\">
window.onload = function(){
   $('autoBox').blur();
   var auto = new Form.AutoComplete('autoBox', {
            dbTable: 'inv_items',
            dbDisp: 'item_name',
            dbVal: 'inv_itemid',
            dbWhere: \"businessid = '$businessid' ORDER BY item_name\",
            watermark: '(Select an Item)',
            showOnEmpty: true});
	    auto.onchange = function(){ auto.input.form.submit(); }

}
</script>
";
?>

<style type="text/css">
	.matched {background-color: #ccf; font-weight: bold;}
	.keyHover {background-color: #39f; color: white; border: 1px dotted red;}
	.mouseHover {background-color: #39f; border: 1px dotted red; color: white;}

	#autoBox{background: url('images/down_arrow.gif') no-repeat center right; background-color: white; border: 1px solid #1b1b1b;}
	#autoBox.hover{background: url('images/down_arrow_hover.gif') no-repeat center right; background-color: white; border: 1px solid #3c7fb1;}
	
	.dispVal{font-weight: bold; color: green; text-decoration: underline;}
</style>

<STYLE type="text/css">
OPTION.mar{background-color:yellow}
</STYLE>

<script language="JavaScript"
   type="text/JavaScript">
function popup(url)
  {
     popwin=window.open(url,"Converter","location=no,menubar=no,titlebar=no,resizeable=no,height=175,width=350");   
  }
</script>

<SCRIPT LANGUAGE=javascript><!--
function delconfirm(){return confirm('Are you sure you want to delete this area?\nThis is not recommended if there are items assigned to this area.');}
// --></SCRIPT>

<SCRIPT LANGUAGE=javascript><!--
function delconfirm2(){return confirm('Are you sure you want to delete this item?');}
// --></SCRIPT>

<SCRIPT LANGUAGE=javascript><!--
function delconfirm3(){return confirm('WARNING! If there are counts for this area and you delete it, your totals will be off.');}
// --></SCRIPT>

<SCRIPT LANGUAGE=javascript><!--
function delconfirm7(){return confirm('Are you sure you want to update these items?');}
// --></SCRIPT>

<script language="JavaScript" 
   type="text/JavaScript">
function changePage(newLoc)
 {
   nextPage = newLoc.options[newLoc.selectedIndex].value
		
   if (nextPage != "")
   {
      document.location.href = nextPage
   }
 }
</script>

<SCRIPT LANGUAGE="JavaScript">
<!-- Web Site:  http://dynamicdrive.com -->

<!-- This script and many more are available free online at -->
<!-- The JavaScript Source!! http://javascript.internet.com -->

<!-- Begin
function disableForm(theform) {
if (document.all || document.getElementById) {
for (i = 0; i < theform.length; i++) {
var tempobj = theform.elements[i];
if (tempobj.type.toLowerCase() == "submit" || tempobj.type.toLowerCase() == "reset")
tempobj.disabled = true;
}

}
else {
alert("The form has been submitted.  But, since you're not using IE 4+ or NS 6, the submit button was not disabled on form submission.");
return false;
   }
}
//  End -->
</script>

</head>
<?php

    if ($apinv!=""){echo "<body onLoad='focus();invitem5.itemname.focus()'>";}
    else {echo "<body>";} 

    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%><tr><td colspan=2>";
    echo "<a name=top><a href=businesstrack.php><img src=logo.jpg border=0 height=43 width=205></a></a><p></td></tr>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM company WHERE companyid = '$companyid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $companyname=Treat_DB_ProxyOld::mysql_result($result,0,"companyname");

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM security WHERE securityid = '$mysecurity'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $sec_item_list=Treat_DB_ProxyOld::mysql_result($result,0,"item_list");
    $sec_bus_sales=Treat_DB_ProxyOld::mysql_result($result,0,"bus_sales");
    $sec_bus_invoice=Treat_DB_ProxyOld::mysql_result($result,0,"bus_invoice");
    $sec_bus_order=Treat_DB_ProxyOld::mysql_result($result,0,"bus_order");
    $sec_bus_payable=Treat_DB_ProxyOld::mysql_result($result,0,"bus_payable");
    $sec_bus_inventor1=Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor1");
    $sec_bus_inventor2=Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor2");
    $sec_bus_inventor3=Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor3");
    $sec_bus_inventor4=Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor4");
    $sec_bus_inventor5=Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor5");
    $sec_bus_control=Treat_DB_ProxyOld::mysql_result($result,0,"bus_control");
    $sec_bus_menu=Treat_DB_ProxyOld::mysql_result($result,0,"bus_menu");
    $sec_bus_order_setup=Treat_DB_ProxyOld::mysql_result($result,0,"bus_order_setup");
    $sec_bus_request=Treat_DB_ProxyOld::mysql_result($result,0,"bus_request");
    $sec_route_collection=Treat_DB_ProxyOld::mysql_result($result,0,"route_collection");
    $sec_route_labor=Treat_DB_ProxyOld::mysql_result($result,0,"route_labor");
    $sec_route_detail=Treat_DB_ProxyOld::mysql_result($result,0,"route_detail");
    $sec_bus_labor=Treat_DB_ProxyOld::mysql_result($result,0,"bus_labor");

    $security_level=$sec_bus_inventor1;

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM business WHERE companyid = '$companyid' AND businessid = '$businessid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $businessname=Treat_DB_ProxyOld::mysql_result($result,0,"businessname");
    $businessid=Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
    $street=Treat_DB_ProxyOld::mysql_result($result,0,"street");
    $city=Treat_DB_ProxyOld::mysql_result($result,0,"city");
    $state=Treat_DB_ProxyOld::mysql_result($result,0,"state");
    $zip=Treat_DB_ProxyOld::mysql_result($result,0,"zip");
    $phone=Treat_DB_ProxyOld::mysql_result($result,0,"phone");
    $districtid=Treat_DB_ProxyOld::mysql_result($result,0,"districtid");
    $show_inv_price=Treat_DB_ProxyOld::mysql_result($result,0,"show_inv_price");
    $show_inv_count=Treat_DB_ProxyOld::mysql_result($result,0,"show_inv_count");
    $show_last_count=Treat_DB_ProxyOld::mysql_result($result,0,"show_last_count");
    $count_type=Treat_DB_ProxyOld::mysql_result($result,0,"count_type");
    $contract=Treat_DB_ProxyOld::mysql_result($result,0,"contract");
    $bus_unit=Treat_DB_ProxyOld::mysql_result($result,0,"unit");

    //echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";
    echo "<tr bgcolor=#CCCCFF><td width=1%><img src=weblogo.jpg width=80 height=75></td><td><font size=4><b>$businessname #$bus_unit</b></font><br>$street<br>$city, $state $zip<br>$phone</td>";
    echo "<td align=right valign=top><br>";
    echo "<FORM ACTION='editbus.php' method=post name='store'>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<input type=hidden name=businessid value=$businessid>";
    echo "<input type=hidden name=companyid value=$companyid>";
    echo "<INPUT TYPE=submit VALUE=' Store Setup '> ";
    echo "</form></td></tr>";
    //echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";
    
    echo "<tr><td colspan=3><font color=#999999>";
    if ($sec_bus_sales>0){echo "<a href=busdetail.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Sales</font></a>";}
    if ($sec_bus_invoice>0){echo " | <a href=businvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Invoicing</font></a>";}
    if ($sec_bus_order>0){echo " | <a href=buscatertrack.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Orders</font></a>";}
    if ($sec_bus_payable>0){echo " | <a href=busapinvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Payables</font></a>";}
    if ($sec_bus_inventor1>0){echo " | <a href=businventor.php?bid=$businessid&cid=$companyid><font color=red onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='red' style='text-decoration:none'>Inventory</font></a>";}
    if ($sec_bus_control>0){echo " | <a href=buscontrol.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Control Sheet</font></a>";}
    if ($sec_bus_menu>0){echo " | <a href=buscatermenu.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Menus</font></a>";}
    if ($sec_bus_order_setup>0){echo " | <a href=buscaterroom.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Order Setup</font></a>";}
    if ($sec_bus_request>0){echo " | <a href=busrequest.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Requests</font></a>";}
    if ($sec_route_collection>0||$sec_route_labor>0||$sec_route_detail>0){echo " | <a href=busroute_collect.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Route Collections</font></a>";}
    if ($sec_bus_labor>0){echo " | <a href=buslabor.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Labor</font></a>";}
    echo "</td></tr>";

    echo "</table></center><p>";

if ($security_level>0){
    echo "<center><table width=90%><tr><td><form action=businventor3.php method=post>";
    echo "<select name=gobus onChange='changePage(this.form.gobus)'>";

    $districtquery="";
    if ($security_level>2&&$security_level<7){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM login_district WHERE loginid = '$loginid'";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();

       $num--;
       while($num>=0){
          $districtid=Treat_DB_ProxyOld::mysql_result($result,$num,"districtid");
          if($districtquery==""){$districtquery="districtid = '$districtid'";}
          else{$districtquery="$districtquery OR districtid = '$districtid'";}
          $num--;
       }
    }

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    if($security_level>6){$query = "SELECT * FROM business WHERE companyid = '$companyid' ORDER BY businessname DESC";}
    elseif($security_level==2||$security_level==1){$query = "SELECT * FROM business WHERE companyid = '$companyid' AND (businessid = '$bid' OR businessid = '$bid2' OR businessid = '$bid3' OR businessid = '$bid4' OR businessid = '$bid5' OR businessid = '$bid6' OR businessid = '$bid7' OR businessid = '$bid8' OR businessid = '$bid9' OR businessid = '$bid10') ORDER BY businessname DESC";}
    elseif($security_level>2&&$security_level<7){$query = "SELECT * FROM business WHERE companyid = '$companyid' AND ($districtquery) ORDER BY businessname DESC";}
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    $num--;
    while($num>=0){
       $businessname=Treat_DB_ProxyOld::mysql_result($result,$num,"businessname");
       $busid=Treat_DB_ProxyOld::mysql_result($result,$num,"businessid");

       if ($busid==$businessid){$show="SELECTED";}
       else{$show="";}

       echo "<option value=businventor3.php?bid=$busid&cid=$companyid $show>$businessname</option>";
       $num--;
    }
 
    echo "</select></td><td></form></td></tr></table></center><p>";
}

    $year=date("Y");
    $month=date("n");
    $day=date("j");
 
    $today="$year-$month-$day";

    if ($day<=4){$month--;}
    if ($month==0){$month=12;$year--;}
    if ($month<10){$endmonth="0$month";}
    else {$endmonth=$month;}
    $enddate="$year-$endmonth-01";

    $startmonth=$month-9;
    if ($startmonth<1){$startmonth=$startmonth+12;$year--;}
    if ($startmonth<10){$startmonth="0$startmonth";}
    $startdate="$year-$startmonth-01";
    $startyear=$year;

/////////////////////////EDIT/ADD ITEMS////////////////
    /*
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM inv_items WHERE businessid = '$businessid' ORDER BY item_name DESC";
       $result2 = mysql_query($query2);
       $num2=mysql_numrows($result2);
       //mysql_close();
    */

    if ($sec_bus_inventor2>0){$businventor2="businventor2.php";}
    else{$businventor2="businesstrack.php";}
    if ($sec_bus_inventor3>0){$businventor3="businventor3.php";}
    else{$businventor3="businesstrack.php";}
    if ($sec_bus_inventor4>0){$businventor4="businventor4.php";}
    else{$businventor4="businesstrack.php";}
    if ($sec_bus_inventor5>0){$businventor5="businventor5.php";}
    else{$businventor5="businesstrack.php";}

    echo "<center><table width=90% cellspacing=0 cellpadding=0>";

    echo "<tr bgcolor=#CCCCCC valign=top><td width=1%><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=15%><center><a href=businventor.php?cid=$companyid&bid=$businessid style=$style><font color=blue>Inventory $'s</a></center></td><td width=1% align=right><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1%><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=15%><center><a href=businventor2.php?cid=$companyid&bid=$businessid style=$style style=$style><font color=blue>Inventory Count</a></center></td><td width=1% align=right><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1% bgcolor=#E8E7E7><img src=leftcrn.jpg height=6 width=6 align=top></td><td bgcolor=#E8E7E7 width=15%><center><a href=businventor3.php?cid=$companyid&bid=$businessid style=$style><font color=black>Inventory Setup</a></center></td><td bgcolor=#E8E7E7 width=1% align=right><img src=rightcrn.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1%><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=15%><center><a href=businventor4.php?cid=$companyid&bid=$businessid style=$style style=$style><font color=blue>Recipes</a></center></td><td width=1% align=right><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1%><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=15%><center><a href=businventor5.php?cid=$companyid&bid=$businessid style=$style style=$style><font color=blue>Vendors/PO's</a></center></td><td width=1% align=right><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td bgcolor=white width=25%></td></tr>";

    echo "<tr height=2><td colspan=4></td><td colspan=4></td><td colspan=3 bgcolor=#E8E7E7></td><td></td><td colspan=4></td><td colspan=4></td><td colspan=1></td></tr>";

    echo "</table></center><center><table width=90% cellspacing=0 cellpadding=0>";

    echo "<tr bgcolor=#E8E7E7><td colspan=3><b>Inventory Items </b>| <a href=#item style=$style><font color=blue>Count Areas & Count Sheet Options</font></a> | <a href=businventor6.php?bid=$businessid&cid=$companyid style=$style><font color=blue>Update Pricing</font></a></td>";

    echo "<td align=right colspan=3 bgcolor=#E8E7E7><form action='businventor3.php' method='post' name=sendmessage><input type=hidden name=businessid value=$businessid><input type=hidden name=companyid value=$companyid><input type=hidden name=view2 value='editinv'><input type=text name=editid id=autoBox size=40 value='Loading...Please Wait'/>";
    /*while ($num2>=0){
       $editname=mysql_result($result2,$num2,"item_name");
       $itemid=mysql_result($result2,$num2,"inv_itemid");
       if ($itemid==$editid){$show="SELECTED";}
       else{$show="";}
       echo "<option value='businventor3.php?bid=$businessid&cid=$companyid&view2=editinv&editid=$itemid#invitems' $show>$editname</option>";
       $num2--;
    }
    echo "</select>";*/ 

    echo "</td><td bgcolor=#E8E7E7></form></td></tr>";
    echo "<tr><td colspan=9 height=1 bgcolor=black></td></tr></table></center>";
    if ($view2=="editinv"){
       echo "<a name='invitems'>";

       echo "<center><table width=90% bgcolor=#E8E7E7><tr valign=top><td>";

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM inv_items WHERE inv_itemid = '$editid'";
       $result2 = Treat_DB_ProxyOld::query($query2);
       //mysql_close();

       $editname=Treat_DB_ProxyOld::mysql_result($result2,0,"item_name");
       $item_code=Treat_DB_ProxyOld::mysql_result($result2,0,"item_code");
       $inv_areaid=Treat_DB_ProxyOld::mysql_result($result2,0,"inv_areaid");
       $apaccountid=Treat_DB_ProxyOld::mysql_result($result2,0,"apaccountid");
       $units=Treat_DB_ProxyOld::mysql_result($result2,0,"units");
       $price=Treat_DB_ProxyOld::mysql_result($result2,0,"price");
       $vendor=Treat_DB_ProxyOld::mysql_result($result2,0,"vendor");
       $price_date=Treat_DB_ProxyOld::mysql_result($result2,0,"price_date");
       $rec_num=Treat_DB_ProxyOld::mysql_result($result2,0,"rec_num");
       $rec_size=Treat_DB_ProxyOld::mysql_result($result2,0,"rec_size");
       $rec_num2=Treat_DB_ProxyOld::mysql_result($result2,0,"rec_num2");
       $rec_size2=Treat_DB_ProxyOld::mysql_result($result2,0,"rec_size2");
       $order_size=Treat_DB_ProxyOld::mysql_result($result2,0,"order_size");
       $pack_size=Treat_DB_ProxyOld::mysql_result($result2,0,"pack_size");
       $pack_qty=Treat_DB_ProxyOld::mysql_result($result2,0,"pack_qty");
       $active=Treat_DB_ProxyOld::mysql_result($result2,0,"active");
       $par_level=Treat_DB_ProxyOld::mysql_result($result2,0,"par_level");
       $batch=Treat_DB_ProxyOld::mysql_result($result2,0,"batch");

       if ($active==0){$showactive="CHECKED";}
       else{$showactive="";}
       if ($batch==1){$showactive2="CHECKED";}
       else{$showactive2="";}

       $showdelete="<a href=editinv_item.php?cid=$companyid&bid=$businessid&edit=2&editid=$editid onclick='return delconfirm2()'><img border=0 src=delete.gif height=16 width=16 alt='DELETE ITEM'></a>";

       echo "<center><table width=100% bgcolor='#E8E7E7' style=\"border: 2px solid black;\"><tr><td colspan=2><a name='invitems'></a><form action=editinv_item.php method=post onSubmit='return disableForm(this);' style=\"margin:0;padding:0;display:inline;\"></td></tr>";
       echo "<tr><td colspan=2><INPUT TYPE=hidden name=companyid value=$companyid><INPUT TYPE=hidden name=businessid value=$businessid><INPUT TYPE=hidden name=editid value=$editid><INPUT TYPE=hidden name=edit value=1><input type=hidden name=old_price value='$price'><input type=hidden name=editinvarea value='$editinvarea'></td></tr>";
       echo "<tr><td align=right><font color=red>Item Name:</font></td><td> <INPUT TYPE=text name=itemname value='$editname' size=35> $showdelete</td></tr>";
       echo "<tr><td align=right>SUPC:</td><td> <INPUT TYPE=text name=item_code value='$item_code' size=20> <input type=checkbox name=is_active $showactive>Active <input type=checkbox name=batch value=1 $showactive2>Batch Recipe</td></tr>";
       echo "<tr valign=top><td align=right>Count Units:</td><td> <INPUT TYPE=text name=units value='$units' size=5> Order Units: <input type=text size=5 name=pack_size value='$pack_size' onblur=this.style.backgroundColor='white' onfocus=this.style.backgroundColor='yellow' title='Updated if using the Auto Update Feature'><br>Count Units in Order Units: <input type=text size=3 name=pack_qty value='$pack_qty'><br> Par Level (# of Order Units): <input type=text size=3 name=par_level value=$par_level></td></tr>";
       echo "<tr><td align=right>Recipe Units:</td><td><select name=rec_size>";

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query3 = "SELECT * FROM size ORDER BY sizename DESC";
       $result3 = Treat_DB_ProxyOld::query($query3);
       $num3=Treat_DB_ProxyOld::mysql_numrows($result3);
       //mysql_close();

       $num3--;$num4=$num3;
       while ($num3>=0){
          $sizename=Treat_DB_ProxyOld::mysql_result($result3,$num3,"sizename");
          $sizeid=Treat_DB_ProxyOld::mysql_result($result3,$num3,"sizeid");

          if ($rec_size==$sizeid){$showsel="SELECTED";}
          else{$showsel="";}

          echo "<option value=$sizeid $showsel>$sizename</option>";

          $num3--;
       }
       echo "</select>";

      if ($contract==1){
       echo " - Order Units <select name=order_size>";
       while ($num4>=0){
          $sizename=Treat_DB_ProxyOld::mysql_result($result3,$num4,"sizename");
          $sizeid=Treat_DB_ProxyOld::mysql_result($result3,$num4,"sizeid");

          if ($order_size==$sizeid){$showsel="SELECTED";}
          else{$showsel="";}

          echo "<option value=$sizeid $showsel>$sizename</option>";

          $num4--;
       }
       echo "</select>";
      }

       echo " <a onclick=popup('/ta/unitconvert.html')><img src=calc.gif height=16 width=16 alt='Unit Converter' border=0></a></td></tr>";
       echo "<tr><td></td><td>Recipe Units in Order Units: <input type=text size=3 name=rec_num value='$rec_num'></td></tr>";
       echo "<tr><td align=right>Alt. Recipe Units:</td><td><select name=rec_size2>";

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query3 = "SELECT * FROM size ORDER BY sizename DESC";
       $result3 = Treat_DB_ProxyOld::query($query3);
       $num3=Treat_DB_ProxyOld::mysql_numrows($result3);
       //mysql_close();

       $num4=$num3;
       while ($num3>=0){
          $sizename=Treat_DB_ProxyOld::mysql_result($result3,$num3,"sizename");
          $sizeid=Treat_DB_ProxyOld::mysql_result($result3,$num3,"sizeid");

          if ($rec_size2==$sizeid){$showsel="SELECTED";}
          else{$showsel="";}

          echo "<option value=$sizeid $showsel>$sizename</option>";

          $num3--;
       }
       echo "</select>";

      if ($contract==1){
       echo " - Order Units <select name=order_size>";
       while ($num4>=0){
          $sizename=Treat_DB_ProxyOld::mysql_result($result3,$num4,"sizename");
          $sizeid=Treat_DB_ProxyOld::mysql_result($result3,$num4,"sizeid");

          if ($order_size==$sizeid){$showsel="SELECTED";}
          else{$showsel="";}

          echo "<option value=$sizeid $showsel>$sizename</option>";

          $num4--;
       }
       echo "</select>";
      }

       echo "</td></tr>";
       echo "<tr><td></td><td>Recipe Units in Order Units: <input type=text size=3 name=rec_num2 value='$rec_num2'></td></tr>";
       echo "<tr><td align=right>Cost:</td><td> <INPUT TYPE=text name=price value='$price' size=5 onblur=this.style.backgroundColor='white' onfocus=this.style.backgroundColor='yellow' title='Updated if using the Auto Update Feature'> Last Update: $price_date</td></tr>";

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query3 = "SELECT * FROM inv_area WHERE businessid = '$businessid' AND companyid = '$companyid' ORDER BY inv_areaname DESC";
       $result3 = Treat_DB_ProxyOld::query($query3);
       $num3=Treat_DB_ProxyOld::mysql_numrows($result3);
       //mysql_close();
/*
       echo "<tr><td align=right>Default Count Area:</td><td> <select name=area>";

       while ($num3>=0){
          $areaname=mysql_result($result3,$num3,"inv_areaname");
          $areaid=mysql_result($result3,$num3,"inv_areaid");

          if ($areaid==$inv_areaid){$showsel="SELECTED";}
          else{$showsel="";}

          echo "<option value=$areaid $showsel>$areaname</option>";

          $num3--;
       }
       echo "</select></td></tr>";
*/
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query3 = "SELECT * FROM acct_payable WHERE businessid = '$businessid' AND companyid = '$companyid' AND cos = '1' AND cs_type = '0' ORDER BY acct_name DESC";
       $result3 = Treat_DB_ProxyOld::query($query3);
       $num3=Treat_DB_ProxyOld::mysql_numrows($result3);
       //mysql_close();

       echo "<tr><td align=right>G/L Acct:</td><td> <select name=apacct>";

       $num3--;
       while ($num3>=0){
          $acct_name=Treat_DB_ProxyOld::mysql_result($result3,$num3,"acct_name");
          $acct_payableid=Treat_DB_ProxyOld::mysql_result($result3,$num3,"acct_payableid");

          if ($apaccountid==$acct_payableid){$showsel="SELECTED";}
          else{$showsel="";}

          echo "<option value=$acct_payableid $showsel>$acct_name</option>";

          $num3--;
       }
       echo "</select></td></tr>";

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query3 = "SELECT * FROM vendors WHERE businessid = '$businessid' AND is_deleted = '0' ORDER BY name DESC";
       $result3 = Treat_DB_ProxyOld::query($query3);
       $num3=Treat_DB_ProxyOld::mysql_numrows($result3);
       //mysql_close();

       echo "<tr><td align=right>Vendor:</td><td> <select name=vendor>";

       $num3--;
       while ($num3>=0){
          $name=Treat_DB_ProxyOld::mysql_result($result3,$num3,"name");
          $master=Treat_DB_ProxyOld::mysql_result($result3,$num3,"master");

          if ($vendor==$master){$showsel="SELECTED";}
          else{$showsel="";}

          echo "<option value=$master $showsel>$name</option>";

          $num3--;
       }
       echo "</select></td></tr>";
       
       echo "<tr><td align=right><INPUT TYPE=submit value='Save'></form></td><td><form action=businventor3.php?bid=$businessid&cid=$companyid method=post style=\"margin:0;padding:0;display:inline;\"><input type=submit value='Return'></form></td></tr>";
       echo "</table></center></td><td valign=top width=55%>";

       ////////////////////////NUTRITION/RECIPE INFORMATION//////

       echo "<center><table width=98% style=\"border: 2px solid black;\" >";

       $query3 = "SELECT * FROM nutrition WHERE item_code = '$item_code'";
       $result3 = Treat_DB_ProxyOld::query($query3);
       $num3=Treat_DB_ProxyOld::mysql_numrows($result3);

       if($num3!=0){
          if($sec_item_list>0){$shownutrlink="<font color=black size=1>[<a href=caternutrition.php?bid=$businessid&cid=$companyid&item_code=$item_code style=$style><font color=blue>VIEW</font></a>]</font>";}
          echo "<tr><td><font color=green>Nutritional information exists for this item. $shownutrlink</td></tr>";
  
          $rec_size10=Treat_DB_ProxyOld::mysql_result($result3,0,"rec_size");

          $query3 = "SELECT * FROM size WHERE sizeid = '$rec_size10'";
          $result3 = Treat_DB_ProxyOld::query($query3);

          $size_name11=Treat_DB_ProxyOld::mysql_result($result3,0,"sizename");
          echo "<tr><td>Recipe units should be <b><font style=background-color:yellow;>$size_name11</b></font> or convertable units. ";

          $query3 = "SELECT * FROM size WHERE sizeid = '$rec_size'";
          $result3 = Treat_DB_ProxyOld::query($query3);
          $size_name11=Treat_DB_ProxyOld::mysql_result($result3,0,"sizename");

          $query3 = "SELECT * FROM conversion WHERE (sizeid = '$rec_size' AND sizeidto = '$rec_size10') OR (sizeidto = '$rec_size' AND sizeid = '$rec_size10')";
          $result3 = Treat_DB_ProxyOld::query($query3);
          $num3=Treat_DB_ProxyOld::mysql_numrows($result3);

          if($rec_size==$rec_size10){echo "You have <font style=background-color:yellow;><b>$size_name11</b></font>. ";}
          elseif($num3>0){echo "<font style=background-color:yellow;><b>$size_name11</b></font> will convert. ";}
          else{echo "<font style=background-color:red;><b>$size_name11</b></font> does not convert. ";}

          /////ALT RECIPE UNITS
          $query3 = "SELECT * FROM size WHERE sizeid = '$rec_size2'";
          $result3 = Treat_DB_ProxyOld::query($query3);
          $size_name11=Treat_DB_ProxyOld::mysql_result($result3,0,"sizename");

          $query3 = "SELECT * FROM conversion WHERE (sizeid = '$rec_size2' AND sizeidto = '$rec_size10') OR (sizeidto = '$rec_size2' AND sizeid = '$rec_size10')";
          $result3 = Treat_DB_ProxyOld::query($query3);
          $num3=Treat_DB_ProxyOld::mysql_numrows($result3);

          if($rec_size2==$rec_size10&&$rec_size2!=0){echo "You have <font style=background-color:yellow;><b>$size_name11</b></font>. ";}
          elseif($num3>0&&$rec_size2!=0){echo "<font style=background-color:yellow;><b>$size_name11</b></font> will convert. ";}
          elseif($rec_size2!=0){echo "<font style=background-color:red;><b>$size_name11</b></font> does not convert. ";}

          echo "</td></tr>";
       }
       else{
          if($sec_item_list>0){$shownutrlink="<font color=black size=1>[<a href=caternutrition.php?bid=$businessid&cid=$companyid style=$style><font color=blue>ADD</font></a>]</font>";}
          echo "<tr><td><i><font color=red>No nutritional information exists.</i> $shownutrlink</td></tr>";
       }

       $query12 = "SELECT menu_items.item_name,menu_items.menu_item_id FROM recipe,menu_items WHERE recipe.inv_itemid = '$editid' AND recipe.menu_itemid = menu_items.menu_item_id";
       $result12 = Treat_DB_ProxyOld::query($query12);
       $num12=Treat_DB_ProxyOld::mysql_numrows($result12);

       echo "<tr><td>This item is used in <b>$num12</b> recipe(s). ";

       $num12--;
       while($num12>=0){
          $r_item_name=Treat_DB_ProxyOld::mysql_result($result12,$num12,"menu_items.item_name");
          $r_item_id=Treat_DB_ProxyOld::mysql_result($result12,$num12,"menu_items.menu_item_id");
          if($num12==0){echo "<a href=businventor4.php?bid=$businessid&cid=$companyid&editid=$r_item_id&hl=$editid style=$style>$r_item_name</a>.";}
          else{echo "<a href=businventor4.php?bid=$businessid&cid=$companyid&editid=$r_item_id&hl=$editid style=$style>$r_item_name</a>, ";}
          $num12--;
       }
       echo "</td></tr>";

       echo "</table></center><p>";

       ////////////////////////COUNT AREAS///////////////////////

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query3 = "SELECT * FROM inv_areadetail WHERE businessid = '$businessid' AND inv_itemid = '$editid'";
       $result3 = Treat_DB_ProxyOld::query($query3);
       $num3=Treat_DB_ProxyOld::mysql_numrows($result3);
       //mysql_close();

       echo "<center><table width=98% style=\"border: 2px solid black;\"><tr><td>"; 
       echo "<b><u>Count Areas</u></b><br>";

       $num3--;
       while ($num3>=0){
          $inv_areadetailid=Treat_DB_ProxyOld::mysql_result($result3,$num3,"inv_areadetailid");
          $areaid=Treat_DB_ProxyOld::mysql_result($result3,$num3,"areaid");
          $location=Treat_DB_ProxyOld::mysql_result($result3,$num3,"location");

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query2 = "SELECT inv_areaname FROM inv_area WHERE inv_areaid = '$areaid'";
          $result2 = Treat_DB_ProxyOld::query($query2);
          //mysql_close();

          $area_name=Treat_DB_ProxyOld::mysql_result($result2,0,"inv_areaname");

          echo "$area_name (#$location) <a href=editarea.php?bid=$businessid&cid=$companyid&did=$inv_areadetailid&edit=2&editid=$editid onclick='return delconfirm3()'><img src=delete.gif height=16 width=16 border=0 alt='DELETE'></a><br>";

          $num3--;
       }

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query3 = "SELECT * FROM inv_area WHERE businessid = '$businessid' AND companyid = '$companyid' ORDER BY inv_areaname DESC";
       $result3 = Treat_DB_ProxyOld::query($query3);
       $num3=Treat_DB_ProxyOld::mysql_numrows($result3);
       //mysql_close();

       echo "<form action=editarea.php method=post onSubmit='return disableForm(this);'><input type=hidden name=editid value=$editid><input type=hidden name=companyid value=$companyid><input type=hidden name=businessid value=$businessid>New: <select name=area>";

       $num3--;
       while ($num3>=0){
          $areaname=Treat_DB_ProxyOld::mysql_result($result3,$num3,"inv_areaname");
          $areaid=Treat_DB_ProxyOld::mysql_result($result3,$num3,"inv_areaid");

          echo "<option value=$areaid>$areaname</option>";

          $num3--;
       }
       echo "</select> Loc#:<input type=text size=5 name=location> <input type=submit value='Add'></td></tr></table></form></td></tr></table></center>";

       ///////////////////////////////////////////////////////////
    }
    else {

       echo "<center><table width=90% bgcolor='#E8E7E7'><tr><td colspan=2><a name='invitemS'></a><form action=editinv_item.php method=post name=invitem5 onSubmit='return disableForm(this);'></td></tr>";
       echo "<tr><td colspan=2><INPUT TYPE=hidden name=apinv value=$apinv><INPUT TYPE=hidden name=companyid value=$companyid><INPUT TYPE=hidden name=businessid value=$businessid></td></tr>";
       echo "<tr><td align=right width=20%>Item Name:</td><td> <INPUT TYPE=text name=itemname size=35></td></tr>";
       echo "<tr><td align=right width=20%>SUPC:</td><td> <INPUT TYPE=text name=item_code size=20> <input type=checkbox name=batch value=1> Batch Recipe</td></tr>";
       echo "<tr valign=top><td align=right>Count Units:</td><td> <INPUT TYPE=text name=units size=5> Order Units: <input type=text size=5 name=pack_size value='$pack_size' onblur=this.style.backgroundColor='white' onfocus=this.style.backgroundColor='yellow' title='Updated if using the Auto Update Feature'><br>Count Units in Order Units: <input type=text size=3 name=pack_qty value='$pack_qty'><br> Par Level (# of Order Units): <input type=text size=3 name=par_level value=$par_level></td></tr>";
       echo "<tr><td align=right>Recipe Units:</td><td><select name=rec_size>";

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query3 = "SELECT * FROM size ORDER BY sizename DESC";
       $result3 = Treat_DB_ProxyOld::query($query3);
       $num3=Treat_DB_ProxyOld::mysql_numrows($result3);
       //mysql_close();

       $num3--;$num4=$num3;
       while ($num3>=0){
          $sizename=Treat_DB_ProxyOld::mysql_result($result3,$num3,"sizename");
          $sizeid=Treat_DB_ProxyOld::mysql_result($result3,$num3,"sizeid");

          if ($rec_size==$sizeid){$showsel="SELECTED";}
          else{$showsel="";}

          echo "<option value=$sizeid $showsel>$sizename</option>";

          $num3--;
       }
       echo "</select>";

      if ($contract==1){
       echo " - Order Units <select name=order_size>";
       while ($num4>=0){
          $sizename=Treat_DB_ProxyOld::mysql_result($result3,$num4,"sizename");
          $sizeid=Treat_DB_ProxyOld::mysql_result($result3,$num4,"sizeid");

          if ($order_size==$sizeid){$showsel="SELECTED";}
          else{$showsel="";}

          echo "<option value=$sizeid $showsel>$sizename</option>";

          $num4--;
       }
       echo "</select>";
      }

       echo " <a onclick=popup('/ta/unitconvert.html')><img src=calc.gif height=16 width=16 alt='Unit Converter' border=0></a></td></tr>";
       echo "<tr><td></td><td>Recipe Units in Order Units: <input type=text size=3 name=rec_num value='$rec_num'></td></tr>";
       echo "<tr><td align=right>Cost:</td><td> <INPUT TYPE=text name=price size=5 onblur=this.style.backgroundColor='white' onfocus=this.style.backgroundColor='yellow' title='Updated if using the Auto Update Feature'></td></tr>";

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query3 = "SELECT * FROM inv_area WHERE businessid = '$businessid' AND companyid = '$companyid' ORDER BY inv_areaname DESC";
       $result3 = Treat_DB_ProxyOld::query($query3);
       $num3=Treat_DB_ProxyOld::mysql_numrows($result3);
       //mysql_close();

       echo "<tr><td align=right>Default Count Area:</td><td> <select name=area>";

       while ($num3>=0){
          $areaname=Treat_DB_ProxyOld::mysql_result($result3,$num3,"inv_areaname");
          $areaid=Treat_DB_ProxyOld::mysql_result($result3,$num3,"inv_areaid");

          echo "<option value=$areaid>$areaname</option>";

          $num3--;
       }
       echo "</select> Loc#:<input type=text name=location size=5></td></tr>";

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query3 = "SELECT * FROM acct_payable WHERE businessid = '$businessid' AND companyid = '$companyid' AND cos = '1' AND cs_type = '0' ORDER BY acct_name DESC";
       $result3 = Treat_DB_ProxyOld::query($query3);
       $num3=Treat_DB_ProxyOld::mysql_numrows($result3);
       //mysql_close();

       echo "<tr><td align=right>G/L Acct:</td><td> <select name=apacct>";

       $num3--;
       while ($num3>=0){
          $acct_name=Treat_DB_ProxyOld::mysql_result($result3,$num3,"acct_name");
          $acct_payableid=Treat_DB_ProxyOld::mysql_result($result3,$num3,"acct_payableid");

          echo "<option value=$acct_payableid>$acct_name</option>";

          $num3--;
       }
       echo "</select></td></tr>";

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query3 = "SELECT * FROM vendors WHERE businessid = '$businessid' AND is_deleted = '0' ORDER BY name DESC";
       $result3 = Treat_DB_ProxyOld::query($query3);
       $num3=Treat_DB_ProxyOld::mysql_numrows($result3);
       //mysql_close();

       echo "<tr><td align=right>Vendor:</td><td> <select name=vendor>";

       $num3--;
       while ($num3>=0){
          $name=Treat_DB_ProxyOld::mysql_result($result3,$num3,"name");
          $master=Treat_DB_ProxyOld::mysql_result($result3,$num3,"master");

          if ($vendor==$master||$master==$newvendor){$showsel="SELECTED";}
          else{$showsel="";}

          echo "<option value=$master $showsel>$name</option>";

          $num3--;
       }
       echo "</select></td></tr>";
       
       echo "<tr><td align=right><INPUT TYPE=submit value='Add'></td><td></form></td></tr>";
       echo "</table></center>";
    }

///////////////////////INVENTORY ITEMS
    $lastdate=$today;
    $counter=45;
    while($counter>=0){$lastdate=prevday($lastdate);$counter--;}

    echo "<center><table width=90% cellspacing=0 cellpadding=0>";
    echo "<tr bgcolor=black><td height=1 colspan=10></td></tr>";
    if ($showhidden==0){echo "<tr bgcolor=#E8E7E7><td colspan=10>&nbsp<a href=businventor3.php?cid=$companyid&bid=$businessid&showhidden=1 style=$style><img src=hide.gif height=16 width=16 border=0 valign=middle><font color=blue> Hide Inactive Items</font></a></td></tr>";}
    elseif ($showhidden==1){echo "<tr bgcolor=#E8E7E7><td colspan=10>&nbsp<a href=businventor3.php?cid=$companyid&bid=$businessid&showhidden=0 style=$style><img src=show.gif height=16 width=16 border=0 valign=middle><font color=blue> Show Inactive Items</font></a></td></tr>";}
    echo "<tr bgcolor=black><td height=1 colspan=10><form action=updateinv_items.php method=post><input type=hidden name=businessid value=$businessid><input type=hidden name=companyid value=$companyid></td></tr>";
    
    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    if($showhidden==0){$query = "SELECT * FROM inv_items WHERE businessid = '$businessid' ORDER BY apaccountid DESC,item_name";}
    elseif($showhidden==1){$query = "SELECT * FROM inv_items WHERE businessid = '$businessid' AND active = '0' ORDER BY apaccountid DESC,item_name";}
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    $inv_counter=0;
    $showcolor="white";
    $lastapaccountid=-1;
    $lastvendor=-1;
    $num--;
    //while ($num>=0){
    while($r=Treat_DB_ProxyOld::mysql_fetch_array($result)){
       $inv_itemid=$r["inv_itemid"];
       $item_name=$r["item_name"];
       $item_code=$r["item_code"];
       $inv_areaid=$r["inv_areaid"];
       $apaccountid=$r["apaccountid"];
       $units=$r["units"];
       $price=$r["price"];
       $vendor=$r["vendor"];
       $price_date=$r["price_date"];
       $active=$r["active"];
       $pack_size=$r["pack_size"];
       $v_update=$r["v_update"];

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query23 = "SELECT item_code FROM nutrition WHERE item_code = '$item_code'";
       $result23 = Treat_DB_ProxyOld::query($query23);
       $num23=Treat_DB_ProxyOld::mysql_numrows($result23);
       //mysql_close();

       if ($num23>0){$shownutr="<font color=red>*</font>";}
       else{$shownutr="";}

       if($v_update==1){$fontcolor="blue";}
       else{$fontcolor="black";}

       $day=substr($price_date,8,2);
       $month=substr($price_date,5,2);
       $year=substr($price_date,2,2);
       if ($lastdate>$price_date){$showprice="<sup><font color=gray size=2>$month/$day/$year</font></sup>";}
       else{$showprice="<sup><font color=gray size=2>$month/$day/$year</font></sup>";}

       if($item_code=="0"){$item_code="";}
       if ($active==1){$item_name="<strike>$item_name</strike>";}

       if($pack_size!=""){$units=$pack_size;}

       if ($lastapaccount!=$apaccountid){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT acct_name FROM acct_payable WHERE acct_payableid = '$apaccountid'";
       $result2 = Treat_DB_ProxyOld::query($query2);
       //mysql_close();

       $acct_name=Treat_DB_ProxyOld::mysql_result($result2,0,"acct_name");
       }
       $acct_name=substr($acct_name,0,15);
       if ($acct_name==""){$acct_name="<img src=error.gif height=16 width=20 alt='No Account Defined'>";}

       $num44=0;

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM inv_areadetail WHERE inv_itemid = '$inv_itemid'";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num44=Treat_DB_ProxyOld::mysql_numrows($result2);
       //mysql_close();

       if ($num44==0){$shownoareas="<img src=error.gif height=16 width=20 alt='No Count Areas'>";}
       else{$shownoareas="";}

       if ($lastvendor!=$vendor){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT vendor_name FROM vendor_master WHERE vendor_master_id = '$vendor'";
       $result2 = Treat_DB_ProxyOld::query($query2);
       //mysql_close();

       $vendor_name=Treat_DB_ProxyOld::mysql_result($result2,0,"vendor_name");
       $myvendor=$vendor_name;
       }
       $vendor_name=substr($vendor_name,0,15);
       if ($vendor_name==""){$vendor_name="<img src=error.gif height=16 width=20 alt='No Vendor Defined'>";}

       $showedit="<a href=businventor3.php?cid=$companyid&bid=$businessid&view2=editinv&editid=$inv_itemid#invitems><img border=0 src=edit.gif height=16 width=16 alt='EDIT ITEM'></a>";
       $showdelete="<a href=editinv_item.php?cid=$companyid&bid=$businessid&edit=2&editid=$inv_itemid onclick='return delconfirm2()'><img border=0 src=delete.gif height=16 width=16 alt='DELETE ITEM'></a>";
       if($active==0){$showactive="<a href=editinv_item.php?cid=$companyid&bid=$businessid&edit=3&editid=$inv_itemid&active=1><img border=0 src=on.gif height=16 width=16 alt='ACTIVE'></a>";}
       else{$showactive="<a href=editinv_item.php?cid=$companyid&bid=$businessid&edit=3&editid=$inv_itemid&active=0><img border=0 src=off.gif height=16 width=16 alt='INACTIVE'></a>";}

       if ($lastapaccountid!=$apaccountid){
             if ($showcolor=="#F3F3F3"){$showcolor="white";}
             elseif ($showcolor=="white"){$showcolor="#F3F3F3";}
             $showcolor2=$showcolor;
       }

       $price=money($price);
       echo "<tr bgcolor='$showcolor' onMouseOver=this.bgColor='#999999' onMouseOut=this.bgColor='$showcolor'><td><input type=checkbox name=$inv_counter value=$inv_itemid><font size=2 color=$fontcolor>$item_code</font></td><td>$shownoareas <font size=2 color=$fontcolor>$shownutr$item_name</td><td align=right><font size=2 color=$fontcolor>$$price</td><td><font size=2 color=$fontcolor>&nbsp $units$showprice</td><td></td><td><font size=2 color=$fontcolor>$acct_name</td><td><font size=2 color=$fontcolor>$vendor_name</td><td width=4%>$showactive</td><td width=4%>$showedit</td><td width=4%>$showdelete</td></tr>";
       echo "<tr bgcolor=#CCCCCC><td height=1 colspan=10></td></tr>";

       $showcolor=$showcolor2;
       $lastapaccountid=$apaccountid;
       $vendor_name=$myvendor;
       $lastvendor=$vendor;
       $num--;
       $inv_counter++;
    }
  
    echo "<tr bgcolor=black><td height=1 colspan=10></td></tr>";
    echo "<tr><td colspan=10>&nbsp;&nbsp;<img src=turnarrow.gif height=16 width=16> <select name=update><option value=1.1>Make Inactive</option><option value=1.2>Make Active</option><option value=1.3>Delete</option><optgroup label='OR Update Vendor to...'>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM vendors WHERE businessid = '$businessid' AND is_deleted = '0' ORDER BY name DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    $num--;
    while($num>=0){
       $vendor_name=Treat_DB_ProxyOld::mysql_result($result,$num,"name");
       $master=Treat_DB_ProxyOld::mysql_result($result,$num,"master");

       echo "<option value='2.$master'>$vendor_name</option>";

       $num--;
    }

    echo "</optgroup><optgroup label='OR Update G/L Account to...'>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM acct_payable WHERE businessid = '$businessid' AND is_deleted = '0' AND cs_type = '0' AND cos = '1' ORDER BY acct_name DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    $num--;
    while($num>=0){
       $apaccountid=Treat_DB_ProxyOld::mysql_result($result,$num,"acct_payableid");
       $acct_name=Treat_DB_ProxyOld::mysql_result($result,$num,"acct_name");

       echo "<option value='3.$apaccountid'>$acct_name</option>";

       $num--;
    }

    echo "</optgroup><optgroup label='OR Add Count Area'>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM inv_area WHERE businessid = '$businessid' AND companyid = '$companyid' ORDER BY inv_areaname DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    $num--;
    while($num>=0){
       $inv_areaid=Treat_DB_ProxyOld::mysql_result($result,$num,"inv_areaid");
       $inv_areaname=Treat_DB_ProxyOld::mysql_result($result,$num,"inv_areaname");

       echo "<option value='4.$inv_areaid'>$inv_areaname</option>";

       $num--;
    }

    echo "</select><input type=submit value='GO' onclick='return delconfirm7()'><input type=hidden name=inv_counter value=$inv_counter></td></tr>";
    echo "<tr><td colspan=10>*<font size=2 color=blue>Items showing in blue, have last been updated by your vendor.</font></td></tr>";
    echo "<tr><td colspan=10><font size=2><font color=red>*</font>Nutritional information exists for that item.</td></tr></table></center><br>";

////////////////////////INVENTORY COUNT AREAS

    $total=0;
    echo "</form><p><center><table width=90% cellspacing=0 cellpadding=0>";
    //echo "<tr><td colspan=3 height=1 bgcolor=black></td></tr>";
    echo "<tr><td colspan=2 bgcolor=#E8E7E7><b>Inventory Count Areas</b></td><td align=right bgcolor=#E8E7E7><a href=#top style=$style><font color=blue size=2>Return to Top</a></font></td></tr>";
    echo "<tr><td colspan=3 height=1 bgcolor=black></td></tr>";
    
    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM inv_area WHERE companyid = '$companyid' AND businessid = '$businessid' ORDER BY inv_areaname DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    $num--;
    while ($num>=0){
       $inv_areaid=Treat_DB_ProxyOld::mysql_result($result,$num,"inv_areaid");
       $inv_areaname=Treat_DB_ProxyOld::mysql_result($result,$num,"inv_areaname");

       $showedit="<a href=businventor3.php?cid=$companyid&bid=$businessid&view2=edit&editid=$inv_areaid#item><img border=0 src=edit.gif height=16 width=16 alt='EDIT AREA'></a>";
       $showdelete="<a href=editinv_area.php?cid=$companyid&bid=$businessid&edit=2&editid=$inv_areaid onclick='return delconfirm()'><img border=0 src=delete.gif height=16 width=16 alt='DELETE AREA'></a>";

       echo "<tr bgcolor='white' onMouseOver=this.bgColor='#999999' onMouseOut=this.bgColor='white'><td width=20%>$inv_areaname</td><td width=4%>$showedit</td><td width=4%>$showdelete</td></tr>";
       echo "<tr bgcolor=#CCCCCC><td height=1 colspan=3></td></tr>";

       $num--;
    }
  
    echo "<tr bgcolor=black><td height=1 colspan=3></td></tr></table></center>";
    
    if ($view2=="edit"){
       echo "<a name='item'>";
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM inv_area WHERE inv_areaid = '$editid'";
       $result2 = Treat_DB_ProxyOld::query($query2);
       //mysql_close();

       $editname=Treat_DB_ProxyOld::mysql_result($result2,0,"inv_areaname");

       echo "<center><table width=90% bgcolor='#E8E7E7'><tr><td colspan=1><a name='item'></a><form action=editinv_area.php method=post onSubmit='return disableForm(this);'></td><td align=right></td></tr>";
       echo "<tr><td colspan=2><INPUT TYPE=hidden name=companyid value=$companyid><INPUT TYPE=hidden name=businessid value=$businessid><INPUT TYPE=hidden name=editid value=$editid><INPUT TYPE=hidden name=edit value=1></td></tr>";
       echo "<tr><td><font color=red>Edit Area Name: </font><INPUT TYPE=text name=areaname value='$editname' size=20><INPUT TYPE=submit value='Save'></td><td></form></td></tr>";
       echo "</table></center>";
    }
    else {

       echo "<center><table width=90% bgcolor='#E8E7E7'><tr><td colspan=1><a name='item'><form action=editinv_area.php method=post onSubmit='return disableForm(this);'></td><td align=right></td></tr>";
       echo "<tr><td colspan=2><INPUT TYPE=hidden name=companyid value=$companyid><INPUT TYPE=hidden name=businessid value=$businessid></td></tr>";
       echo "<tr height=1><td>New Area Name: <INPUT TYPE=text name=areaname size=20><INPUT TYPE=submit value='Add'></td><td></form></td></tr>";
       echo "</table></center>";
    }

       if($show_inv_price==1){$showinv="CHECKED";}
       else{$showinv="";}
       if ($count_type==1){$showcounttype="CHECKED";}
       else{$showcounttype="";}
       echo "<p><center><table width=90% bgcolor='#E8E7E7' cellspacing=0 cellpadding=0><tr><td colspan=1><a name='item'><form action=inv_countoptions.php method=post onSubmit='return disableForm(this);'><b>Count Sheet Options</td><td align=right><a href=#top style=$style><font color=blue size=2>Return to Top</a></font></td></tr>";
       echo "<tr height=1 bgcolor=black><td colspan=2></td></tr>";
       echo "<tr><td colspan=2><INPUT TYPE=hidden name=companyid value=$companyid><INPUT TYPE=hidden name=businessid value=$businessid></td></tr>";
       echo "<tr height=1><td><INPUT TYPE=checkbox name=count_type $showcounttype value=1>Show SUPC, <INPUT TYPE=checkbox name=show_inv_price value=1 $showinv>Show Costs, Show <INPUT TYPE=text name=show_last_count value=$show_last_count size=2> Previous Count(s), Show <input type=text name=show_inv_count size=3 value='$show_inv_count'> Blank Column(s) for Counts <INPUT TYPE=submit value='Save'></td><td></form></td></tr>";
       echo "</table></center>";

    //mysql_close();

    echo "<br><FORM ACTION=businesstrack.php method=post>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<center><INPUT TYPE=submit VALUE=' Return to Main Page'></FORM></center></body>";
	
	google_page_track();
}
?>
