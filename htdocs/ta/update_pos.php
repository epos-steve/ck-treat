<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once(DOC_ROOT.'/bootstrap.php');
require_once(realpath(DOC_ROOT.'/../lib/').'/manage_db.php');

define('XHR', (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')? true : false);
$db = new DB_Proxy();
if (empty($_POST['businessid']) && XHR) {
	echo '0';
}

$sql = 'SELECT client_programid FROM client_programs WHERE businessid = '.$_POST['businessid'];
$result = $db->query('SELECT client_programid FROM client_programs WHERE businessid = '.$_POST['businessid']);
if ($result !== false && ($id = mysql_result($result, 0, 0)) !== false) {
	$db->query(
'UPDATE
	client_programs AS cp
SET
	cp.send_status = 2,
	cp.send_schedule = (
		SELECT
			cs.id
		FROM
			client_schedule AS cs
		WHERE
			cs.client_programid = '.$id.' AND cs.default = 1
	)
WHERE
	cp.client_programid = '.$id);

	if (XHR) echo '1';
} else {
	if (XHR) echo '0';
}

if (!XHR) {
	header('Location: /ta/buscatermenu4.php?cid=3&bid='.$_POST['businessid']);
}
