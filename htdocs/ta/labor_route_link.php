<?php

function dayofweek($date1)
{
	$day=substr($date1,8,2);
	$month=substr($date1,5,2);
	$year=substr($date1,0,4);
	$dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
	return $dayofweek;
}

function money($diff){
	$findme='.';
	$double='.00';
	$single='0';
	$double2='00';
	$pos1 = strpos($diff, $findme);
	$pos2 = strlen($diff);
	if ($pos1==""){$diff="$diff$double";}
	elseif ($pos2-$pos1==2){$diff="$diff$single";}
	elseif ($pos2-$pos1==1){$diff="$diff$double2";}
	else{}
	$dot = strpos($diff, $findme);
	$diff = substr($diff, 0, $dot+4);
	$diff=round($diff, 2);
	if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
	elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
	$diff = substr($diff, 0, $dot+3);
	$pos1 = strpos($diff, $findme);
	$pos2 = strlen($diff);
	if ($pos1==""){$diff="$diff$double";}
	elseif ($pos2-$pos1==2){$diff="$diff$single";}
	elseif ($pos2-$pos1==1){$diff="$diff$double2";}
	else{}
	return $diff;
}

function nextday($date2)
{
	$day=substr($date2,8,2);
	$month=substr($date2,5,2);
	$year=substr($date2,0,4);
	$leap = date("L");

	if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
	{
		if ($month == "01"){$month="02";}
		elseif ($month == "03"){$month="04";}
		elseif ($month == "05"){$month="06";}
		elseif ($month == "07"){$month="08";}
		elseif ($month == "08"){$month="09";}
		elseif ($month == "10"){$month="11";}
		$day='01';
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($month=='02' && $day == '29' && $leap == '1')
	{
		$month='03';
		$day='01';
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($month=='02' && $day == '28' && $leap == '0')
	{
		$month='03';
		$day='01';
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
	{
		if ($month == "04"){$month="05";}
		if ($month == "06"){$month="07";}
		if ($month == "09"){$month="10";}
		if ($month == "11"){$month="12";}
		$day='01';
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($month=='12' && $day=='31')
	{
		$day='01';
		$month='01';
		$year++;
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else
	{
		$day=$day+1;
		if ($day<10){$day="0$day";}
		$tomorrow="$year-$month-$day";
		return $tomorrow;
	}
}

function prevday($date2) {
	$day=substr($date2,8,2);
	$month=substr($date2,5,2);
	$year=substr($date2,0,4);
	$day=$day-1;
	$leap = date("L");

	if ($day <= 0)
	{
		if ($month == 01)
		{
			$month = '12';
			$year--;
			$day=$day+31;
			$yesterday = "$year-$month-$day";
			return $yesterday;
		}
		else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
		{
			$month--;
			if ($month < 10)
			{
				$month="0$month";
			}
			$day=$day+31;
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
		else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
		{
			$month--;
			if ($month < 10)
			{
				$month="0$month";
			}
			$day=$day+30;
			$yesterday="$year-$month-$day";
			return $yesterday;
		}

		elseif ($leap==1&&$month=='03')
		{
			$day=$day+29;
			$month='02';
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
		else
		{
			$day=$day+28;
			$month='02';
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
	}
	else
	{
		if ($day < 10)
		{
			$day="0$day";
		}
		$yesterday="$year-$month-$day";
		return $yesterday;
	}
}

function futureday($num) {
	$day = date("d");
	$year = date("Y");
	$month = date("m");
	$leap = date("L");
	$day=$day+$num;

	if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
	{
		if ($month == "03"){$month="04";}
		if ($month == "05"){$month="06";}
		if ($month == "07"){$month="08";}
		if ($month == "08"){$month="09";}
		$day=$day-31;  
		if ($day<10){$day="0$day";} 
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($month=='02' && $leap == '1' && $day >= '29')
	{
		$month='03';
		$day=$day-29;
		if ($day<10){$day="0$day";} 
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($month=='02' && $leap == '0' && $day >= '28')
	{
		$month='03';
		$day=$day-28;
		if ($day<10){$day="0$day";} 
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
	{
		if ($month == "04"){$month="05";}
		if ($month == "06"){$month="07";}
		if ($month == "09"){$month="10";}
		if ($month == "11"){$month="12";}
		$day=$day-30;
		if ($day<10){$day="0$day";} 
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($month==12 && $day>=32)
	{
		$day=$day-31;
		if ($day<10){$day="0$day";} 
		$month='01';
		$year++;
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else
	{
		if ($day<10){$day="0$day";}
		$tomorrow="$year-$month-$day";
		return $tomorrow;
	}
}

function pastday($num) {
	$day = date("d");
	$day=$day-$num;
	if ($day <= 0)
	{
		$year = date("Y");
		$month = date("m");
		if ($month == 01)
		{
			$month = '12';
			$year--;
			$day=$day+31;
			$yesterday = "$year-$month-$day";
			return $yesterday;
		}
		else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
		{
			$month--;
			if ($month < 10)
			{
				$month="0$month";
			}
			$day=$day+31;
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
		else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
		{
			$month--;
			if ($month < 10)
			{
				$month="0$month";
			}
			$day=$day+30;
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
		else
		{
			$day=$day+28;
			$month='02';
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
	}
	else
	{
		if ($day < 10)
		{
			$day="0$day";
		}
		$year=date("Y");
		$month=date("m");
		$yesterday="$year-$month-$day";
		return $yesterday;
	}
}


if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
require_once( DOC_ROOT . '/bootstrap.php' );
$style = "text-decoration:none";
$user = Treat_Controller_Abstract::getSessionCookieVariable( 'usercook' );
$pass = Treat_Controller_Abstract::getSessionCookieVariable( 'passcook' );
	$businessid = Treat_Controller_Abstract::getPostOrGetVariable( 'businessid' );
	$companyid = Treat_Controller_Abstract::getPostOrGetVariable( 'companyid' );
	$date = Treat_Controller_Abstract::getPostOrGetVariable( 'date' );


//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query = sprintf( '
		SELECT *
		FROM login
		WHERE username = "%s"
			AND password = "%s"
			AND username != ""
			AND password != ""
	'
	,$user
	,$pass
);
$result = Treat_DB_ProxyOld::query( $query );
$num = @mysql_numrows( $result );
//mysql_close();

$security_level = @mysql_result( $result, 0, 'security_level' );
$mysecurity = @mysql_result( $result, 0, 'security' );
$bid = @mysql_result( $result, 0, 'businessid' );
$cid = @mysql_result( $result, 0, 'companyid' );
$bid2 = @mysql_result( $result, 0, 'busid2' );
$bid3 = @mysql_result( $result, 0, 'busid3' );
$bid4 = @mysql_result( $result, 0, 'busid4' );
$bid5 = @mysql_result( $result, 0, 'busid5' );
$bid6 = @mysql_result( $result, 0, 'busid6' );
$bid7 = @mysql_result( $result, 0, 'busid7' );
$bid8 = @mysql_result( $result, 0, 'busid8' );
$bid9 = @mysql_result( $result, 0, 'busid9' );
$bid10 = @mysql_result( $result, 0, 'busid10' );
$loginid = @mysql_result( $result, 0, 'loginid' );
if ($num != 1 || ($security_level==1 && ($bid != $businessid || $cid != $companyid)) || $user == "" || $pass == "")
{
	echo "<center><h3>Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
	/////////////////////CREATE COMMISSION RECORDS

	$query = sprintf( '
			SELECT login.loginid
				,jobtypedetail.jobtype
				,jobtypedetail.rate
			FROM login
				,jobtype
				,jobtypedetail
			WHERE login.oo2 = "%d"
				AND login.is_deleted = "0"
				AND jobtypedetail.loginid = login.loginid
				AND jobtype.jobtypeid = jobtypedetail.jobtype
				AND jobtype.commission = "1"
			ORDER BY login.lastname
		'
		,$businessid
	);
	$result = Treat_DB_ProxyOld::query( $query );
	$num = @mysql_numrows( $result );
/*
	$num--;
	while ( $num >= 0 ) {
		$loginid = @mysql_result( $result, $num, 'loginid' );
		$jobtype = @mysql_result( $result, $num, 'jobtype' );
		$rate = @mysql_result( $result, $num, 'rate' );

		$query2 = "SELECT * FROM labor_temp_comm WHERE loginid = '$loginid' AND date = '$date'";
		$result2 = Treat_DB_ProxyOld::query( $query2 );
		$num2 = @mysql_numrows( $result2 );

		if ( $num2 == 0 ) {
			$query3 = "INSERT INTO labor_temp_comm (loginid,businessid,jobtypeid,date,rate) VALUES ('$loginid','$businessid','$jobtype','$date','$rate')";
			$result3 = Treat_DB_ProxyOld::query( $query3 );
		}
		else {
			$temp_commid = @mysql_result( $result2, 0, 'tempid' );
			$query3 = "DELETE FROM labor_comm WHERE temp_commid = '$temp_commid'";
			$result3 = Treat_DB_ProxyOld::query( $query3 );
		}

		$num--;
	}
*/
	/////////////////////START SAVING
	$startdate=$date;
	for ( $mycount1 = 1; $mycount1 <= 6; $mycount1++ ) {
		$startdate = prevday( $startdate );
	}

	$counter=1;

	$query33 = sprintf( '
			SELECT login.firstname
				,login.lastname
				,login.empl_no
				,login.loginid
				,jobtypedetail.jobtype
				,jobtypedetail.rate
			FROM login
				,jobtype
				,jobtypedetail
			WHERE login.oo2 = "%d"
				AND login.is_deleted = "0"
				AND jobtypedetail.loginid = login.loginid
				AND jobtype.jobtypeid = jobtypedetail.jobtype
				AND jobtype.commission = "1"
			ORDER BY login.lastname DESC
		'
		,$businessid
	);
	$result33 = Treat_DB_ProxyOld::query( $query33 );
	$num33 = @mysql_numrows( $result33 );

	$query = sprintf( '
			SELECT *
			FROM vend_locations
			WHERE unitid = "%d"
		'
		,$businessid
	);
	$result = Treat_DB_ProxyOld::query( $query );
	$num = @mysql_numrows( $result );

	$num--;
	while ( $num >= 0 ) {
		$location_name = @mysql_result( $result, $num, 'location_name' );
		$locationid = @mysql_result( $result, $num, 'locationid' );

		$query2 = sprintf( '
				SELECT *
				FROM login_route
				WHERE locationid = "%d"
					AND active = "0"
				ORDER BY route DESC
			'
			,$locationid
		);
		$result2 = Treat_DB_ProxyOld::query( $query2 );
		$num2 = @mysql_numrows( $result2 );

		$num2--;
		while ( $num2 >= 0 ) {
			$login_routeid = @mysql_result( $result2, $num2, 'login_routeid' );
			$route = @mysql_result( $result2, $num2, 'route' );
			$commissionid = @mysql_result( $result2, $num2, 'commissionid' );

			$freevend = isset($_POST["freevend_$counter"])?$_POST["freevend_$counter"]:'';
			$noncash = isset($_POST["noncash_$counter"])?$_POST["noncash_$counter"]:'';
			$kiosk = isset($_POST["kiosk_$counter"])?$_POST["kiosk_$counter"]:'';
			$csv = isset($_POST["csv_$counter"])?$_POST["csv_$counter"]:'';
			$employee = isset($_POST["employee_$counter"])?$_POST["employee_$counter"]:'';
			$weekend = isset($_POST["weekend_$counter"])?$_POST["weekend_$counter"]:'';

			$query3 = sprintf( '
					SELECT *
					FROM labor_route_link
					WHERE login_routeid = "%d"
						AND date = "%s"
				'
				,$login_routeid
				,$date
			);
			$result3 = Treat_DB_ProxyOld::query( $query3 );
			$num3 = @mysql_numrows( $result3 );

			if ( $num3 == 0 ) {
				$query4 = sprintf( '
						INSERT INTO labor_route_link (
							date
							,login_routeid
							,loginid
							,freevend
							,noncash
							,kiosk
							,csv
							,weekend_pay
						)
						VALUES (
							"%s"
							,"%d"
							,"%d"
							,"%s"
							,"%s"
							,"%s"
							,"%s"
							,"%s"
						)
					'
					,$date
					,$login_routeid
					,$employee
					,$freevend
					,$noncash
					,$kiosk
					,$csv
					,$weekend
				);
				$result4 = Treat_DB_ProxyOld::query( $query4 );
			}
			else {
				$query4 = sprintf( '
						UPDATE labor_route_link
						SET loginid = "%d"
							, csv = "%s"
							, freevend = "%s"
							, noncash = "%s"
							, kiosk = "%s"
							, weekend_pay = "%s"
						WHERE date = "%s"
							AND login_routeid = "%d"
					'
					,$employee
					,$csv
					,$freevend
					,$noncash
					,$kiosk
					,$weekend
					,$date
					,$login_routeid
				);
				$result4 = Treat_DB_ProxyOld::query( $query4 );
			}
/*
			/////////////////////////////UPDATE LABOR/////////////////////////////////////
			/////////////////////////////SALES
			$query12 = "SELECT SUM(route_total) AS totalamt FROM vend_route_collection WHERE login_routeid = '$login_routeid' AND date >= '$startdate' AND date <= '$date'";
			$result12 = Treat_DB_ProxyOld::query( $query12 );

			$totalamt = @mysql_result( $result12, 0, 'totalamt' );
			$totalamt=money($totalamt);
			/////////////////////////////END SALES
			/////////////////////////////GET COMMISSION DETAILS
			$query12 = "SELECT * FROM vend_commission WHERE commissionid = '$commissionid'";
			$result12 = Treat_DB_ProxyOld::query( $query12 );

			$base_link = @mysql_result( $result12, 0, 'base_link' );
			$commission_link = @mysql_result( $result12, 0, 'commission_link' );
			$freevend_link = @mysql_result( $result12, 0, 'freevend_link' );
			$csv_link = @mysql_result( $result12, 0, 'csv_link' );
			$weekend_link = @mysql_result( $result12, 0, 'weekend_link' );
			$base = @mysql_result( $result12, 0, 'base' );

			/////////////////////////////END COMMISSION

			/////////////////////////////UPDATE LABOR_COMM TABLE
			$query22 = "SELECT * FROM labor_temp_comm WHERE loginid = '$employee' AND date = '$date'";
			$result22 = Treat_DB_ProxyOld::query( $query22 );
			$num22 = @mysql_numrows( $result22 );

			if($num22!=1){$errormessage=1;}
			elseif ( $num22 == 1 ) {
				$temp_commid = @mysql_result( $result22, 0, 'tempid' );

				if ( $temp_commid > 0 ) {

					$totalpayroll=0;
					$commission_id=0;
					$freevend_id=0;
					$csv_id=0;
					/////////////CALCULATE PAYROLL
					////////SALES
					$payroll=0;
					$query12 = "SELECT * FROM vend_commission_detail WHERE commissionid = '$commissionid' AND type = '1'";
					$result12 = Treat_DB_ProxyOld::query( $query12 );
					$num12 = @mysql_numrows( $result12 );
					$num12--;
					while ( $num12 >= 0 ) {
						$lower = @mysql_result( $result12, $num12, 'lower' );
						$upper = @mysql_result( $result12, $num12, 'upper' );
						$calc_type = @mysql_result( $result12, $num12, 'calc_type' );
						$percent = @mysql_result( $result12, $num12, 'percent' );

						if($calc_type==1&&$totalamt>=$lower&&$totalamt<=$upper){$payroll=$totalamt*($percent/100);}
						elseif($calc_type==2&&$freevend>=$lower&&$freevend<=$upper){$payroll=$totalamt*($percent/100);}
						elseif($calc_type==3&&$csv>=$lower&&$csv<=$upper){$payroll=$totalamt*($percent/100);}
						elseif($calc_type==4&&($totalamt+$freevend)>=$lower&&($totalamt+$freevend)<=$upper){$payroll=$totalamt*($percent/100);}
						elseif($calc_type==5&&($freevend+$csv)>=$lower&&($freevend+$csv)<=$upper){$payroll=$totalamt*($percent/100);}
						elseif($calc_type==6&&($totalamt+$csv)>=$lower&&($totalamt+$csv)<=$upper){$payroll=$totalamt*($percent/100);}
						elseif($calc_type==7&&($totalamt+$freevend+$csv)>=$lower&&($totalamt+$freevend+$csv)<=$upper){$payroll=$totalamt*($percent/100);}

						$num12--;
					}

					$totalpayroll+=$payroll;
					///UPDATE SALES
					if ( $payroll != 0 ) {
						$payroll=money($payroll);

						$query3 = "SELECT * FROM labor_comm WHERE temp_commid = '$temp_commid' AND coded = '$commission_link'";
						$result3 = Treat_DB_ProxyOld::query( $query3 );
						$num3 = @mysql_numrows( $result3 );

						if ( $num3 > 0 ) {
							$id = @mysql_result( $result3, 0, 'id' );
							$commission_id=$id;
							$query3 = "UPDATE labor_comm SET amount = amount + '$payroll' WHERE id = '$id'";
							$result3 = Treat_DB_ProxyOld::query( $query3 );
						}
						else {
							$query3 = "INSERT INTO labor_comm (temp_commid,coded,amount) VALUES ('$temp_commid','$commission_link','$payroll')";
							$result3 = Treat_DB_ProxyOld::query( $query3 );
							$myid=mysql_insert_id();
						}
						//echo "$query3,$myid<br>";
					}

					///////////////END SALES
					////////FREEVEND
					$payroll=0;
					$query12 = "SELECT * FROM vend_commission_detail WHERE commissionid = '$commissionid' AND type = '2'";
					$result12 = Treat_DB_ProxyOld::query( $query12 );
					$num12 = @mysql_numrows( $result12 );
					$num12--;
					while ( $num12 >= 0 ) {
						$lower = @mysql_result( $result12, $num12, 'lower' );
						$upper = @mysql_result( $result12, $num12, 'upper' );
						$calc_type = @mysql_result( $result12, $num12, 'calc_type' );
						$percent = @mysql_result( $result12, $num12, 'percent' );

						if($calc_type==1&&$totalamt>=$lower&&$totalamt<=$upper){$payroll=$freevend*($percent/100);}
						elseif($calc_type==2&&$freevend>=$lower&&$freevend<=$upper){$payroll=$freevend*($percent/100);}
						elseif($calc_type==3&&$csv>=$lower&&$csv<=$upper){$payroll=$freevend*($percent/100);}
						elseif($calc_type==4&&($totalamt+$freevend)>=$lower&&($totalamt+$freevend)<=$upper){$payroll=$freevend*($percent/100);}
						elseif($calc_type==5&&($freevend+$csv)>=$lower&&($freevend+$csv)<=$upper){$payroll=$freevend*($percent/100);}
						elseif($calc_type==6&&($totalamt+$csv)>=$lower&&($totalamt+$csv)<=$upper){$payroll=$freevend*($percent/100);}
						elseif($calc_type==7&&($totalamt+$freevend+$csv)>=$lower&&($totalamt+$freevend+$csv)<=$upper){$payroll=$freevend*($percent/100);}

						$num12--;
					}

					$totalpayroll+=$payroll;
					///UPDATE PAYROLL
					if ( $payroll != 0 ) {
						$payroll=money($payroll);

						$query3 = "SELECT * FROM labor_comm WHERE temp_commid = '$temp_commid' AND coded = '$freevend_link'";
						$result3 = Treat_DB_ProxyOld::query( $query3 );
						$num3 = @mysql_numrows( $result3 );

						if ( $num3 > 0 ) {
							$id = @mysql_result( $result3, 0, 'id' );
							$freevend_id=$id;
							$query3 = "UPDATE labor_comm SET amount = amount + '$payroll' WHERE id = '$id'";
							$result3 = Treat_DB_ProxyOld::query( $query3 );
						}
						else {
							$query3 = "INSERT INTO labor_comm (temp_commid,coded,amount) VALUES ('$temp_commid','$freevend_link','$payroll')";
							$result3 = Treat_DB_ProxyOld::query( $query3 );
							$myid=mysql_insert_id();
						}
						//echo "$query3,$myid<br>";
					}
					///////////////END FREEVEND
					////////CSV
					$payroll=0;
					$query12 = "SELECT * FROM vend_commission_detail WHERE commissionid = '$commissionid' AND type = '3'";
					$result12 = Treat_DB_ProxyOld::query( $query12 );
					$num12 = @mysql_numrows( $result12 );
					$num12--;
					while ( $num12 >= 0 ) {
						$lower = @mysql_result( $result12, $num12, 'lower' );
						$upper = @mysql_result( $result12, $num12, 'upper' );
						$calc_type = @mysql_result( $result12, $num12, 'calc_type' );
						$percent = @mysql_result( $result12, $num12, 'percent' );

						if($calc_type==1&&$totalamt>=$lower&&$totalamt<=$upper){$payroll=$csv*($percent/100);}
						elseif($calc_type==2&&$freevend>=$lower&&$freevend<=$upper){$payroll=$csv*($percent/100);}
						elseif($calc_type==3&&$csv>=$lower&&$csv<=$upper){$payroll=$csv*($percent/100);}
						elseif($calc_type==4&&($totalamt+$freevend)>=$lower&&($totalamt+$freevend)<=$upper){$payroll=$csv*($percent/100);}
						elseif($calc_type==5&&($freevend+$csv)>=$lower&&($freevend+$csv)<=$upper){$payroll=$csv*($percent/100);}
						elseif($calc_type==6&&($totalamt+$csv)>=$lower&&($totalamt+$csv)<=$upper){$payroll=$csv*($percent/100);}
						elseif($calc_type==7&&($totalamt+$freevend+$csv)>=$lower&&($totalamt+$freevend+$csv)<=$upper){$payroll=$csv*($percent/100);}

						$num12--;
					}

					$totalpayroll+=$payroll;
					///UPDATE PAYROLL
					if ( $payroll != 0 ) {
						$payroll=money($payroll);

						$query3 = "SELECT * FROM labor_comm WHERE temp_commid = '$temp_commid' AND coded = '$csv_link'";
						$result3 = Treat_DB_ProxyOld::query( $query3 );
						$num3 = @mysql_numrows( $result3 );

						if ( $num3 > 0 ) {
							$id = @mysql_result( $result3, 0, 'id' );
							$csv_id=$id;
							$query3 = "UPDATE labor_comm SET amount = amount + '$payroll' WHERE id = '$id'";
							$result3 = Treat_DB_ProxyOld::query( $query3 );
						}
						else {
							$query3 = "INSERT INTO labor_comm (temp_commid,coded,amount) VALUES ('$temp_commid','$csv_link','$payroll')";
							$result3 = Treat_DB_ProxyOld::query( $query3 );
							$myid=mysql_insert_id();
						}
						//echo "$query3,$myid<br>";
					}
					
					///UPDATE WEEKEND
					if ( $weekend != 0 ) {
						$weekend=money($weekend);

						$query3 = "SELECT * FROM labor_comm WHERE temp_commid = '$temp_commid' AND coded = '$weekend_link'";
						$result3 = Treat_DB_ProxyOld::query( $query3 );
						$num3 = @mysql_numrows( $result3 );

						if ( $num3 > 0 ) {
							$id = @mysql_result( $result3, 0, 'id' );
							$query3 = "UPDATE labor_comm SET amount = amount + '$weekend' WHERE id = '$id'";
							$result3 = Treat_DB_ProxyOld::query( $query3 );
						}
						else {
							$query3 = "INSERT INTO labor_comm (temp_commid,coded,amount) VALUES ('$temp_commid','$weekend_link','$weekend')";
							$result3 = Treat_DB_ProxyOld::query( $query3 );
							$myid=mysql_insert_id();
						}
						//echo "$query3,$myid<br>";
					}
					/////////////BASE//////////////////////////

					if ( $totalpayroll < $base ) {
						$query3 = "UPDATE labor_comm SET amount = '0' WHERE id = '$commission_id' OR id = '$freevend_id' OR id = '$csv_id'";
						$result3 = Treat_DB_ProxyOld::query( $query3 );

						$query3 = "SELECT * FROM labor_comm WHERE temp_commid = '$temp_commid' AND coded = '$base_link'";
						$result3 = Treat_DB_ProxyOld::query( $query3 );
						$num3 = @mysql_numrows( $result3 );

						if ( $num3 > 0 ) {
							$id = @mysql_result( $result3, 0, 'id' );
							$query3 = "UPDATE labor_comm SET amount = '$base' WHERE id = '$id'";
							$result3 = Treat_DB_ProxyOld::query( $query3 );
						}
						else {
							$query3 = "INSERT INTO labor_comm (temp_commid,coded,amount) VALUES ('$temp_commid','$base_link','$base')";
							$result3 = Treat_DB_ProxyOld::query( $query3 );
							$myid=mysql_insert_id();
						}
						//echo "$query3,$myid<br>";
					}
					/////////////END BASE//////////////////////
					////////////END PAYROLL
				}
				
			}
			/////////////////////////////END UPDATE
			/////////////////////////////END UPDATE LABOR/////////////////////////////////
*/
			$num2--;
			$counter++;
		}

		$num--;
	}

	$querystring = array(
		'bid' => $businessid,
		'cid' => $companyid,
		'date1' => $date,
		'error' => $errormessage,
	);
	$location = '/ta/busroute_labor.php?' . http_build_query( $querystring );
	header( 'Location: ' . $location );

}
//mysql_close();
