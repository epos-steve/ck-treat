<?php

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+3);
        if ($diff > 0 && $diff < '0.01'){$diff="0.01";}
        elseif($diff < 0 && $diff > '-0.01'){$diff="-0.01";}
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      if ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='32')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='08' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      $day=$day-31;  
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";} 
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

define('DOC_ROOT', dirname(dirname(__FILE__)));
require_once(DOC_ROOT.'/bootstrap.php');

$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");

/*$user=$_COOKIE["usercook"];
$pass=$_COOKIE["passcook"];
$date1=$_COOKIE["date1cook"];
$date2=$_COOKIE["date2cook"];
$companyid=$_GET["cid"];*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook');
$date1 = \EE\Controller\Base::getSessionCookieVariable('date1cook');
$date2 = \EE\Controller\Base::getSessionCookieVariable('date2cook');
$companyid = \EE\Controller\Base::getGetVariable('cid');

if ($companyid==""){
   /*$companyid=$_POST["companyid"];
   $date1=$_POST["date1"];
   $date2=$_POST["date2"];*/
	
	$companyid = \EE\Controller\Base::getPostVariable('companyid');
	$date1 = \EE\Controller\Base::getPostVariable('date1');
	$date2 = \EE\Controller\Base::getPostVariable('date2');
}
if ($curreportid==""){
   //$curreportid=$_POST["curreportid"];
	$curreportid = \EE\Controller\Base::getPostVariable('curreportid');
}
if ($curreportid==""){
   //$curreportid=$_GET["curreportid"];
	$curreportid = \EE\Controller\Base::getGetVariable('curreportid');
}

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=Treat_DB_ProxyOld::mysql_numrows($result);

$security_level=Treat_DB_ProxyOld::mysql_result($result,0,"security_level");
$bid=Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
$cid=Treat_DB_ProxyOld::mysql_result($result,0,"companyid");

if ($num != 1 || $user == "" || $pass == "")
{
    echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}
elseif($security_level<9){
   $location="comreport.php?cid=$companyid";
    header('Location: ./' . $location);
}
else
{
    setcookie("date1cook",$date1);
    setcookie("date2cook",$date2);

?>

<html>
<head>

<SCRIPT LANGUAGE=javascript><!--
function delconfirm(){return confirm('Are you sure you want to delete this report?');}
// --></SCRIPT>

<script type="text/javascript">
function checkq(field) {
var valid=true;
var rexp, testf, ecount, fld;
ecount=field.testcount.value;
ecount *= 1;
var i=1;
while (i <= ecount){
fld = field.elements['query'+i].value;
//rexp=/[a-zA-z]$/;
//testf = rexp.test(fld,rexp);

if (testf==true){
 valid=false;
field.elements['query'+i].focus();
 alert('Cannot contain and letters from A-Z!');
}

//if (fld==""){
// valid=false;
//field.elements['query'+i].focus(); 
// alert('The account field cannot be blank!');
//}

i++;
slp=setTimeout("sleep5()",10000);
}
return valid;

}
function sleep5(){
return 0;
}
</script>

<!-- Link script file to the HTML document in the header -->
<script language=JavaScript src="picker.js"></script>

<SCRIPT LANGUAGE=javascript><!--
function delconfirm2(){return confirm('Are you sure you want to delete this row?');}
// --></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" SRC="CalendarPopup.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">document.write(getCalendarStyles());</SCRIPT>

<STYLE>
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation
			{
			background-color:#6677DD;
			text-align:center;
			vertical-align:center;
			text-decoration:none;
			color:#FFFFFF;
			font-weight:bold;
			}
	.TESTcpDayColumnHeader,
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation,
	.TESTcpCurrentMonthDate,
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDate,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDate,
	.TESTcpCurrentDateDisabled,
	.TESTcpTodayText,
	.TESTcpTodayTextDisabled,
	.TESTcpText
			{
			font-family:arial;
			font-size:8pt;
			}
	TD.TESTcpDayColumnHeader
			{
			text-align:right;
			border:solid thin #6677DD;
			border-width:0 0 1 0;
			}
	.TESTcpCurrentMonthDate,
	.TESTcpOtherMonthDate,
	.TESTcpCurrentDate
			{
			text-align:right;
			text-decoration:none;
			}
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDateDisabled
			{
			color:#D0D0D0;
			text-align:right;
			text-decoration:line-through;
			}
	.TESTcpCurrentMonthDate
			{
			color:#6677DD;
			font-weight:bold;
			}
	.TESTcpCurrentDate
			{
			color: #FFFFFF;
			font-weight:bold;
			}
	.TESTcpOtherMonthDate
			{
			color:#808080;
			}
	TD.TESTcpCurrentDate
			{
			color:#FFFFFF;
			background-color: #6677DD;
			border-width:1;
			border:solid thin #000000;
			}
	TD.TESTcpCurrentDateDisabled
			{
			border-width:1;
			border:solid thin #FFAAAA;
			}
	TD.TESTcpTodayText,
	TD.TESTcpTodayTextDisabled
			{
			border:solid thin #6677DD;
			border-width:1 0 0 0;
			}
	A.TESTcpTodayText,
	SPAN.TESTcpTodayTextDisabled
			{
			height:20px;
			}
	A.TESTcpTodayText
			{
			color:#6677DD;
			font-weight:bold;
			}
	SPAN.TESTcpTodayTextDisabled
			{
			color:#D0D0D0;
			}
	.TESTcpBorder
			{
			border:solid thin #6677DD;
			}
</STYLE>

<script language="JavaScript" 
   type="text/JavaScript">
function changePage(newLoc)
 {
   nextPage = newLoc.options[newLoc.selectedIndex].value
		
   if (nextPage != "")
   {
      document.location.href = nextPage
   }
 }
</script>

<script type="text/javascript" language="javascript">
function checkKey(){

var key = event.keyCode;
var DaIndexId = event.srcElement.IndexId-0
if(key==38) {
document.getElementByIndex(DaIndexId-1).focus();
return false;
}
else if(key==40) {
document.getElementByIndex(DaIndexId+1).focus();
return false;
}

return true
} // end checkKey function
document.getElementByIndex=function(){
for(var i=0;i<document.all.length;i++)
if(document.all[i].IndexId==arguments[0])
return document.all[i]
return null
}
</script>

</head>

<?
    echo "<body>";
    echo "<div style=border:50px solid red;padding:10px;>";
    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=100%><tr><td colspan=2>";
    echo "<img src=logo.jpg><p></td></tr>";

    $query = "SELECT * FROM company WHERE companyid = '$companyid'";
    $result = Treat_DB_ProxyOld::query($query);

    $companyname=Treat_DB_ProxyOld::mysql_result($result,0,"companyname");

    //echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";
    echo "<tr bgcolor=#CCCCFF><td colspan=1><font size=4><b>Reporting Setup - $companyname</b></font></td><td align=right><form action='comreport.php?cid=$companyid' method=post><input type=submit value='Return'></td><td width=0%></form></td></tr>";
    echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";
    echo "</table></center><p>";

    $query = "SELECT * FROM reports ORDER BY report_name DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);

    echo "<center><table width=100%><tr><td><form action=comreportsetup.php method=post> Report: <select name=curreportid onChange='changePage(this.form.curreportid)'>";

    while ($num>=0){
       $reportid=Treat_DB_ProxyOld::mysql_result($result,$num,"reportid");
       $reportname=Treat_DB_ProxyOld::mysql_result($result,$num,"report_name");
       if ($curreportid==$reportid){$show="SELECTED";}
       else{$show="";}
       echo "<option value=comreportsetup.php?cid=$companyid&curreportid=$reportid $show>$reportname</option>";
       $num--;
    }
    echo "</select></form></td>";

    echo "<td align=right><form action=addreport.php method=post><input type=hidden name=edit value=1><input type=hidden name=companyid value=$companyid><input type=text size=20 name=reportname> <input type=submit value='New Report'></form></td></tr></table></center>";

    $curarrow=1;
    if ($curreportid!=""){

       $query = "SELECT * FROM reports WHERE reportid = '$curreportid'";
       $result = Treat_DB_ProxyOld::query($query);

       $reportname=Treat_DB_ProxyOld::mysql_result($result,0,"report_name");
       $seperate=Treat_DB_ProxyOld::mysql_result($result,0,"seperate");

       if ($seperate==0){$s0="SELECTED";}
       elseif($seperate==1){$s1="SELECTED";}

       echo "<form action=savereport.php method=post name=editrep id=editrep><input type=hidden name=curreportid value=$curreportid><input type=hidden name=companyid value=$companyid><center><table border=1><tr><td colspan=14 bgcolor=#E8E7E7><b>Report Name:</b><input type=text size=40 name=reportname value='$reportname'> <a href=addreport.php?cid=1&curreportid=$curreportid onclick='return delconfirm()'><img src=delete.gif height=16 width=16 border=0 alt='DELETE REPORT'></a></td></tr>";
       echo "<tr><td align=right>ID</td><td><i>Name</i></td><td><i>Type</i></td><td><i>Acct/Formula</i> <font size=1>[<a href=findacct.php?cid=$companyid target='_blank'><font color=blue>SEARCH</font></a>]</td><td><i>Graph Color</i></td><td><i>Grph</i></td><td><i>Pie</i></td><td><i>Avg</i></td><td><i>Bold</i></td><td><i>Hide</i></td><td><i>Dec.</i></td><td><i>Suffix</i></td><td><i>Order</i></td><td></td></tr>";

       $query = "SELECT * FROM reportdetail WHERE reportid = '$curreportid' ORDER BY orderid DESC";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);

       $counter=1;
       $id=1;
       $rtot=$num;
       $num--;
       while ($num>=0){
          $detailid=Treat_DB_ProxyOld::mysql_result($result,$num,"reportdetailid");
          $detailname=Treat_DB_ProxyOld::mysql_result($result,$num,"name");
          $type=Treat_DB_ProxyOld::mysql_result($result,$num,"type");
          $reportquery=Treat_DB_ProxyOld::mysql_result($result,$num,"query");
          $orderid=Treat_DB_ProxyOld::mysql_result($result,$num,"orderid");
          $avg=Treat_DB_ProxyOld::mysql_result($result,$num,"avg");
          $bold=Treat_DB_ProxyOld::mysql_result($result,$num,"bold");
          $curcolor=Treat_DB_ProxyOld::mysql_result($result,$num,"g_color");
          $graph_it=Treat_DB_ProxyOld::mysql_result($result,$num,"graph_it");
          $on_pie=Treat_DB_ProxyOld::mysql_result($result,$num,"on_pie");
          $hide=Treat_DB_ProxyOld::mysql_result($result,$num,"hide");
          $decimal=Treat_DB_ProxyOld::mysql_result($result,$num,"num_decimal");
          $suffix=Treat_DB_ProxyOld::mysql_result($result,$num,"suffix");
	  if ($graph_it=="1"){$g_sel="CHECKED";}
	  else{$g_sel="";}
          if ($on_pie=="1"){$gp_sel="CHECKED";}
          else {$gp_sel="";}
          if ($type==1){$c1="SELECTED";}
          elseif($type==2){$c2="SELECTED";}
          elseif($type==3){$c3="SELECTED";}
          elseif($type==4){$c4="SELECTED";}
          elseif($type==5){$c5="SELECTED";}
          elseif($type==6){$c6="SELECTED";}
          elseif($type==7){$c7="SELECTED";}
          elseif($type==8){$c8="SELECTED";}
          elseif($type==9){$c9="SELECTED";}
          elseif($type==10){$c10="SELECTED";}
          elseif($type==11){$c11="SELECTED";}
          elseif($type==12){$c12="SELECTED";}
          else{$c1="";$c2="";$c3="";$c4="";$c5="";$c6="";$c7="";$c8="";$c9="";$c10="";$c11="";$c12="";}
          if ($hide==1){$showhide="CHECKED";}
          else{$showhide="";}

          if ($avg==1){$showcheck="CHECKED";}
          else{$showcheck="";}

          if ($bold==1){$showcheck2="CHECKED";}
          else{$showcheck2="";}

          $curarrow2=$curarrow+1000;
          $curarrow3=$curarrow2+1000;
	  $colorurl="'javascript:TCP.popup(this.form.color$counter)'";
	  echo "<input type=hidden name=queryb$counter value='$reportquer' />";
          echo "<tr><td align=right>$id<td><input type=text name=name$counter size=25 value='$detailname' onkeydown='return checkKey()' IndexId=$curarrow></td><td><select name=type$counter><option value=1 $c1>Credit</option><option value=2 $c2>Debit</option><option value=3 $c3>AP G/L</option><option value=4 $c4>Inventory</option><option value=7 $c7>Stats</option><option value=9 $c9>Labor ADJ</option><option value=10 $c10>Budget</option><option value=11 $c11>Recur</option><option value=12 $c12>Benefit%</option><option value=8 $c8>Op. Days</option><option value=6 $c6>FORMULA</option></select></td><td><input type=text name=query$counter size=20 value='$reportquery'  onkeydown='return checkKey()' IndexId=$curarrow2></td><td align=right bgcolor=$curcolor><input type=text name=color$counter value='$curcolor' size=6><a href=\"javascript:TCP.popup(document.forms['editrep'].elements['color$counter'])\"><img width=15 height=13 border=0 alt='Click Here to Choose a color' src=sel.gif></a></td><td align=right><input type=checkbox name=graph_it$counter value='$graph_it' $g_sel /></td><td align=right><input type=checkbox name=on_pie$counter value='$on_pie' $gp_sel /></td><td align=right><input type=checkbox name=avg$counter value=1 $showcheck></td><td align=right><input type=checkbox name=bold$counter value=1 $showcheck2></td><td align=right><input type=checkbox name=hide$counter value=1 $showhide></td><td align=right><input type=text name=decimal$counter value='$decimal' size=3></td><td align=right><input type=text name=suffix$counter value='$suffix' size=3></td>";
          echo "<td><input type=text name=order$counter size=3 value='$orderid' onkeydown='return checkKey()' IndexId=$curarrow3></td><td><a href=reportdetail.php?cid=$companyid&curreportid=$curreportid&detailid=$detailid onclick='return delconfirm2()'><img src=delete.gif height=16 width=16 border=0 alt='DELETE'></a></td></tr>";
	
          $num--;
          $counter++;
          $id++;
          $curarrow++;
          $c1="";$c2="";$c3="";$c4="";$c5="";$c6="";$c7="";$c8="";$c9="";$c10="";$c11="";$c12="";
       }
       echo "</table></center>";
	  echo "<input type=hidden name=testcount id=testcount value='$rtot' />";
       echo "<center><table border=0><tr><td><input type=submit value='Save' onclick=\"return checkq(document.forms['editrep']);\"></td><td></form><td><form action=reportdetail.php method=post><input type=hidden name=curreportid value=$curreportid><input type=hidden name=companyid value=$companyid><input type=hidden name=edit value=1><input type=hidden name=orderid value=$orderid><input type=submit value=' Add '></td><td></form></td></tr></table></center>";
    }
    echo "<DIV ID=testdiv1 STYLE=position:absolute;visibility:hidden;background-color:white;layer-background-color:white;></DIV>";
  
    echo "<a name=new><br><FORM ACTION=businesstrack.php method=post>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<center><INPUT TYPE=submit VALUE=' Return to Main Page'></FORM></center></a></body>";
}
google_page_track();
?>