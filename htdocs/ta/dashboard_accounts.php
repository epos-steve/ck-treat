<?php

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+4);
        if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
        elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
        $diff = substr($diff, 0, $dot+3);
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      elseif ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      elseif ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='32')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      $day=$day-31;  
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";} 
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

/*include("db.php");
require('lib/class.db-proxy.php');*/
define('DOC_ROOT', dirname(dirname(__FILE__)));
require_once(DOC_ROOT.'/bootstrap.php');

$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";

/*$user=$_COOKIE["usercook"];
$pass=$_COOKIE["passcook"];
$viewtype=$_GET["viewtype"];
$companyid=$_GET["cid"];
$viewbus=$_GET["viewbus"];
$ytdstart=$_GET["ytdstart"];
$mysort=$_GET["sort"];
$view=$_GET["v"];*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook');
$viewtype = \EE\Controller\Base::getGetVariable('viewtype');
$companyid = \EE\Controller\Base::getGetVariable('cid');
$viewbus = \EE\Controller\Base::getGetVariable('viewbus');
$ytdstart = \EE\Controller\Base::getGetVariable('ytdstart');
$mysort = \EE\Controller\Base::getGetVariable('sort');
$view = \EE\Controller\Base::getGetVariable('v');

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=Treat_DB_ProxyOld::mysql_numrows($result);

if ($num!=0){
    $security_level=Treat_DB_ProxyOld::mysql_result($result,0,"security_level");
    $bid=Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
    $cid=Treat_DB_ProxyOld::mysql_result($result,0,"companyid");
    $loginid=Treat_DB_ProxyOld::mysql_result($result,0,"loginid");
}

if ($num != 1 || $user == "" || $pass == "") 
{
    echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
    if($viewtype==1&&$security_level>8){
       $compquery="business.companyid != '2'";
    }
    elseif($viewtype==2){
       $compquery="business.companyid = company.companyid AND company.company_type = '$viewbus'";
       $fromtables=",company";
    }
    elseif($viewtype==3){
       $compquery="business.companyid = company.companyid  AND company.companyid = '$viewbus'";
       $fromtables=",company";
    }
    elseif($viewtype==4){
       $compquery="business.districtid = district.districtid AND district.divisionid = '$viewbus'";
       $fromtables=",district";
    }
    elseif($viewtype==5){
       $compquery="business.districtid = '$viewbus'";
    }
    elseif($viewtype==6&&$viewbus>0){
       $compquery="business.businessid = '$viewbus'";
    }

    ?>
    <html>
    <head>

    <STYLE>
    #loading2 {
 	width: 0px;
 	height: 0px;
 	background-color: ;
 	position: absolute;
 	left: 50%;
 	top: 50%;
 	margin-top: -50px;
 	margin-left: -100px;
 	text-align: center;
    }
    </STYLE>

    <script type=text/javascript>
       window.onload=function(){
          document.getElementById('loading2').style.display='none';
       }
    </script>
  
    </head>
	<body style="margin:2px;">
    <?

    $today=date("Y-m-d");
    if(substr($today,0,4)!=substr($ytdstart,0,4)){$today=substr($ytdstart,0,4);$today="$today-12-31";}
    ////////ACCOUNTS
       if(1==1){
          $query1="SELECT invoice.total,invoice.accountid,accounts.name FROM accounts,invoice,business$fromtables WHERE invoice.status != '3' AND invoice.date >= '$ytdstart' AND invoice.date <= '$today' AND invoice.accountid = accounts.accountid AND accounts.businessid = business.businessid AND ($compquery) ORDER BY invoice.accountid";
          //$result1 = DB_Proxy::query($query1) or die(mysql_error());
		  $result1 = Treat_DB_ProxyOld::query($query1);
          $num1=Treat_DB_ProxyOld::mysql_numrows($result1);

          $lastacctid=-1;
          $invtotal=0;
          $num1--;

          while($num1>=-1){
             $accountid=Treat_DB_ProxyOld::mysql_result($result1,$num1,"invoice.accountid");
             $accountname=Treat_DB_ProxyOld::mysql_result($result1,$num1,"accounts.name");
             $total=Treat_DB_ProxyOld::mysql_result($result1,$num1,"invoice.total");

             $invtotal+=$total;

             if($lastacctid!=$accountid&&$lastacctid!=-1){

                $db_accounts[$lastacctid]=$invtotal;
                $db_acctname[$lastacctid]=$lastacctname;

                $invtotal=0;
             }

             $lastacctid=$accountid;
             $lastacctname=$accountname;
             $num1--;
          }

		  /*
          if($viewtype==6){
          $query1="SELECT vend_machine_sales.collects,vend_machine_sales.accountnum,vend_machine_sales.locationid FROM vend_machine_sales,vend_locations,business$fromtables WHERE vend_machine_sales.date >= '$ytdstart' AND vend_machine_sales.date <= '$today' AND vend_machine_sales.locationid = vend_locations.locationid AND vend_locations.unitid = business.businessid AND ($compquery) ORDER BY vend_machine_sales.accountnum";
          $result1 = DB_Proxy::query($query1) or die(mysql_error());
          $num1=mysql_numrows($result1);

          $lastacctid=-1;
          $invtotal=0;
          $num1--;

          //while($num1>=-1){
          while($r = mysql_fetch_array($result1)){

             $accountnum=$r["accountnum"];
             $total=$r["amount"];
             $locationid=$r["locationid"];

             if($lastacctid!=$accountnum&&$lastacctid!=-1){

                $query2="SELECT accounts.accountid,accounts.name FROM accounts,business,vend_locations WHERE accounts.accountnum LIKE '$lastacctid' AND accounts.businessid = business.businessid AND business.businessid = vend_locations.unitid AND vend_locations.locationid = '$lastlocation'";
                $result2 = DB_Proxy::query($query2) or die(mysql_error());

                $accountid=mysql_result($result2,0,"accounts.accountid");
                $accountname=mysql_result($result2,0,"accounts.name");

                $db_accounts[$accountid]=$invtotal;
                $db_acctname[$accountid]=$accountname;

                $invtotal=0;
             }

             $invtotal+=$total;

             $lastacctid=$accountnum;
             $lastlocation=$locationid;
             //$num1--;
          }
          }*/
       }

       if($mysort==1){arsort($db_accounts);}
       else{asort($db_accounts);}

       echo "<table cellspacing=0 cellpadding=0 width=100%>";
       $counter=1;
       foreach($db_accounts AS $key => $total){
           $total=number_format($total,2);
           echo "<tr onMouseOver=this.bgColor='yellow' onMouseOut=this.bgColor='white' valign=top><td align=right width=5%><font size=2>$counter.&nbsp;</td><td width=60%><font size=2>$db_acctname[$key]</td><td align=right width=35%><font size=2>$total</td></tr>";
           if($counter==10&&$view!="all"){break;}
           $counter++;
       }
       echo "</table></body></html>";

}
//mysql_close();
?>
