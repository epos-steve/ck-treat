<?php

define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once(DOC_ROOT.'/bootstrap.php');
require_once( DOC_ROOT . '/ta/lib/class.menu_items.php' );
require_once( DOC_ROOT . '/ta/lib/class.Kiosk.php' );
//Treat_DB_ProxyOld::$debug = true;
//ini_set('display_errors','true');

$style = "text-decoration:none";

$showall = $_GET["showall"];
if($showall == ""){$showall = isset($_COOKIE["showall"])?$_COOKIE["showall"]:'';}
setcookie(showall,"$showall");

if ( require_once( realpath( DOC_ROOT . '/../lib/inc/cookie-login.php' ) ) ) {

    $query = "SELECT * FROM security WHERE securityid = '$mysecurity'";
    $result = Treat_DB_ProxyOld::query($query);

    $sec_bus_sales=Treat_DB_ProxyOld::mysql_result($result,0,"bus_sales");
    $sec_bus_invoice=Treat_DB_ProxyOld::mysql_result($result,0,"bus_invoice");
    $sec_bus_order=Treat_DB_ProxyOld::mysql_result($result,0,"bus_order");
    $sec_bus_payable=Treat_DB_ProxyOld::mysql_result($result,0,"bus_payable");
    $sec_bus_inventor1=Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor1");
    $sec_bus_control=Treat_DB_ProxyOld::mysql_result($result,0,"bus_control");
    $sec_bus_menu=Treat_DB_ProxyOld::mysql_result($result,0,"bus_menu");
    $sec_bus_order_setup=Treat_DB_ProxyOld::mysql_result($result,0,"bus_order_setup");
    $sec_bus_request=Treat_DB_ProxyOld::mysql_result($result,0,"bus_request");
    $sec_route_collection=Treat_DB_ProxyOld::mysql_result($result,0,"route_collection");
    $sec_route_labor=Treat_DB_ProxyOld::mysql_result($result,0,"route_labor");
    $sec_route_detail=Treat_DB_ProxyOld::mysql_result($result,0,"route_detail");
	$sec_route_machine=@Treat_DB_ProxyOld::mysql_result($result,0,"route_machine");
    $sec_bus_labor=Treat_DB_ProxyOld::mysql_result($result,0,"bus_labor");
    $sec_bus_budget=Treat_DB_ProxyOld::mysql_result($result,0,"bus_budget");
    $sec_bus_menu_pos_report=Treat_DB_ProxyOld::mysql_result($result,0,"bus_menu_pos_report");

    if($sec_bus_menu_pos_report<1){
        $location="buscatermenu.php?bid=$businessid&cid=$companyid";
        header('Location: ./' . $location);
    }
?>
<html>
	<head>
		<title>buscatermenu5 :: Treat America</title>
		<script type="text/javascript" src="/assets/js/calendarPopup.js"></script>
		<script type="text/javascript" language="JavaScript">document.write(getCalendarStyles());</script>
		<link rel="stylesheet" type="text/css" href="/assets/css/calendarPopup.css" />

		<script type="text/javascript" src="/assets/js/prototype.js"></script>
		<script type="text/javascript" src="/assets/js/prototype-eef.js"></script>
		<script type="text/javascript" src="/assets/js/sorttable.js"></script>

		<script type="text/javascript" src="/assets/js/common.js"></script>
		<script type="text/javascript" src="/assets/js/ta/buscatermenu4-functions.js"></script>

		<STYLE>
			#loading {
				width: 0px;
				height: 0px;
				background-color: ;
				position: absolute;
				left: 50%;
				top: 50%;
				margin-top: -50px;
				margin-left: -100px;
				text-align: center;
			}
		</STYLE>
		<script type="text/javascript" src="preLoadingMessage2.js"></script>
		
		<link type="text/css" rel="stylesheet" href="/assets/css/jq_themes/to-theme/jquery-ui-1.7.2.custom.css" />
		<script type="text/javascript" src="/assets/js/jquery-1.4.3.min.js"></script>
		<script type="text/javascript" src="/assets/js/jquery-ui-1.7.2.custom.min.js"></script>
		
		<link type="text/css" rel="stylesheet" href="/assets/css/jquery.fileupload-ui.css" />
		<script type="text/javascript" src="/assets/js/jquery.fileupload.js"></script>
		<script type="text/javascript" src="/assets/js/jquery.fileupload-ui.js"></script>
		
	</head>
	<body>
<?php if($sec_bus_menu_pos_report>1): ?>
	<div id="dialog-upload" title="Upload File" style="display: none;">
		<center>
			<form id="dialog-form-upload" name="dialog-form-upload" action="./buscatermenu5-1.php" method="post" enctype="multipart/form-data">
				<input type="hidden" id="step" name="step" value="U" />
				<input type="hidden" id="bid" name="bid" value="<?php echo @$businessid ?>" />
				<input type="file" name="file" multiple />
				<div>Choose File to Upload</div>
			</form>
		</center>
		<table id="file_list" style="width: 100%;">
			<tr>
				<th colspan="3">
					<hr>
						<div>
							Upload Results <img class="trigger" src="/assets/images/help.png" width="20px"/>
						</div>
						<div class="bubbleInfo">
							<div class="popup">
							Files must be in CSV format and contain 6 columns in the following order: 
							Item Code, Machine Number, Amount, Date, Time, Count/Fill/Waste.   Once the file is uploaded,
							each row will be inspected for errors and if any are present, processing will stop.  No data 
							will be inserted into the database until after the rows are checked for accuracy.  If any errors 
							are encountered, the error will be displayed and it must be fixed before the upload can be attempted again.
							Dates should be in the format MM/DD/YYYY and times should be in the format HH::MM AM/PM.
							</div>
						</div>
					<hr>
				</th>
			</tr>
		</table>
	</div>
	<div id="dialog-form" title="Add New Record" style="display: none;">
		<form id="dialog-form-data">
			<table>
				<tr>
					<td>Item</td>
					<td>
						<strong id="item-dialog-product"></strong>
						<input type="hidden" id="itemcode" name="itemcode" value="" />
						<input type="hidden" id="step" name="step" value="A" />
					</td>
				</tr>
				<tr>
					<td>Date</td>
					<td>
						<select name="month" id="month" class="text ui-widget-content ui-corner-all" />
						<?php for($x = 1; $x <= 12; $x++)echo "<option " . (date("n") == $x ? "default=true selected" : "") . ">$x</option>"; ?>
						</select> / 
						<select name="day" id="day" class="text ui-widget-content ui-corner-all" />
						<?php for($x = 1; $x <= 31; $x++)echo "<option " . (date("j") == $x ? "default=true selected" : "") . ">$x</option>"; ?>
						</select> / 
						<select name="year" id="year" class="text ui-widget-content ui-corner-all" />
						<?php for($x = date("Y"); $x > date("Y") - 2; $x--)echo "<option " . (date("y") == $x ? "default=true selected" : "") . ">$x</option>"; ?>
						</select>
					</td>
				</tr>
				<tr>
					<td>Time</td>
					<td>
						<select name="hour" id="hour" class="text ui-widget-content ui-corner-all" />
						<?php for($x = 1; $x <= 12; $x++)echo "<option " . (date("g") == $x ? "default=true selected" : "") . ">$x</option>"; ?>
						</select> : 
						<select name="minute" id="minute" class="text ui-widget-content ui-corner-all" />
						<?php for($x = 0; $x <= 59; $x++)echo "<option>" . str_pad($x, 2, "0", STR_PAD_LEFT) . "</option>"; ?>
						</select>&nbsp;
						<select name="ampm" id="ampm" class="text ui-widget-content ui-corner-all" />
							<option <?php  echo (date("A") == "AM" ? "default=true selected" : ""); ?>>AM</option>
							<option <?php  echo (date("A") == "PM" ? "default=true selected" : ""); ?>>PM</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Type</td>
					<td>
						<select name="type" id="type" class="text ui-widget-content ui-corner-all" />
							<option default=true>COUNT</option>
							<option>FILL</option>
							<option>WASTE</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Amount</td>
					<td><input type="text" name="amount" id="amount" value="" class="text ui-widget-content ui-corner-all" /></td>
				</tr>
				<tr>
					<td colspan="2" id="dialog-status"></td>
				</tr>
				<tr>
					<td colspan="2"><hr>Item History</td>
				</tr>
				<tr>
					<td colspan="2">
						<div id="item-dialog-history"></div>
					</td>
				</tr>
			</table>
		</form>
	</div>
	<script type="text/javascript">
		jQuery.noConflict();
		
		var dataResponse ={status:"ERROR",message:"There was an error recieving the response and no data was returned"};
		
		function refresh_item_history(itemId, keepStatus) {
			jQuery("#dialog-status").html("Retrieving item history").removeClass().addClass("ui-state-default");
			jQuery.ajax({
				type: 'POST',
				cache: false,
				url: "buscatermenu5-1.php",
				data: {step:"V",item:itemId,bid:<?php echo @$businessid; ?>,cur_term:"<?php echo @$_GET["terminal_no"]; ?>"},
				dataType: "html",
				success: function(data){
					jQuery("#item-dialog-history").html(data);
					if(!keepStatus)jQuery("#dialog-status").html("").removeClass();
				},
				error: function() {
					jQuery("#dialog-status").html("There was an error retrieving the history").removeClass().addClass("ui-state-error");
				}
			});
		}
		
		function open_dialog(itemId, itemName, machineNum) {
			jQuery("#item-dialog-product").html(itemId + " (" + itemName + ")");
			jQuery("#item-dialog-history").html("");
			jQuery("#itemcode").val(itemId);
			jQuery("#machine_num").val(machineNum);
			
			jQuery("#dialog-form").dialog("open");
			
			refresh_item_history(itemId);
	
			return;
		}
		
		jQuery(document).ready( function() {
		
			jQuery("#amount").keypress( function(e) {
				if(e.keyCode == 13) {
					jQuery('.ui-dialog-buttonpane button:contains(Save)').trigger('click');
					return false;
				}
			});
			
			jQuery('select > option:[selected]', '#dialog-form-data').attr("default", true);
			
			jQuery("#upload_file_link")
			.click( function() {
				jQuery("#dialog-upload").dialog("open");
				return false;
			})
			.addClass("ui-state-default")
			;
			
			jQuery( "#dialog-upload" ).dialog({
				autoOpen: false,
				height: 500,
				width: 450,
				modal: true,
				position: 'top',
				buttons: {
					Cancel: function() {
						jQuery(this).dialog("close");
					},
					"Clear Results": function() {
						jQuery("tr:first", "#file_list").nextAll().remove();
					}
				},
				close: function() {
					jQuery("#dialog-form-upload")[0].reset();
					jQuery("#dialog-upload-status").removeClass().html("");
				}
			});

			jQuery('#dialog-form-upload').fileUploadUI({
				uploadTable: jQuery('#file_list'),
				dragDropSupport: false,
				buildUploadRow: function (files, index) {
					var fname = files[index].name.split('\\').pop();
					return jQuery('<tr><td><small>' + fname + '</small><\/td>' +
							'<td class="file_upload_progress"><div /><\/td>' +
							'<td class="file_upload_cancel">' +
							'<button class="ui-state-default ui-corner-all" title="Cancel">' +
							'<span class="ui-icon ui-icon-cancel">Cancel<\/span>' +
							'<\/button><\/td><\/tr>');
				},
				onLoad: function(event, files, index, xhr, handler) {
					var data = jQuery.parseJSON(typeof xhr.responseText !== 'undefined' ? xhr.responseText : xhr.contents().text());
					response = jQuery.extend({}. dataResponse, data);
					if(response.status == "SUCCESS"){
						jQuery(handler.uploadRow).find(".file_upload_progress").html(response.message).addClass("ui-state-default");
					}
					else if(response.status === "ERROR") {
						jQuery(handler.uploadRow).find(".file_upload_progress").html(response.message).addClass("ui-state-error");
					}
					else {
						jQuery(handler.uploadRow).find(".file_upload_progress").html("There was an error uploading the file").addClass("ui-state-error");
					}
					jQuery("button", handler.uploadRow).attr("title", "Delete from List");
				},
				onError: function(event, files, index, xhr, handler) {
					jQuery(handler.uploadRow).html("An unknown error has occurred").addClass("ui-state-error");
				}
			});
			
			
			jQuery( "#dialog-form" ).dialog({
				autoOpen: false,
				height: 500,
				width: 450,
				modal: true,
				position: 'top',
				buttons: {
					"Save": function() {
						var dialog_buttons = jQuery('.ui-dialog-buttonpane button');
						dialog_buttons.addClass('ui-state-disabled');
						jQuery("#dialog-status").html("Sending data").removeClass().addClass("ui-state-default");
						jQuery.ajax({
							type: 'POST',
							cache: false,
							url: "buscatermenu5-1.php",
							data: jQuery("#dialog-form-data").serialize(),
							dataType: "html",
							success: function(data){
								data = jQuery.parseJSON(data);
								response = jQuery.extend({}. dataResponse, data);
								if(response.status == "SUCCESS"){
									jQuery(':text, :password, :file', '#dialog-form-data').val('');
									jQuery('select > option[selected]', '#dialog-form-data').removeAttr("selected");
									jQuery('select > option[default]', '#dialog-form-data').attr("selected", "selected");
									refresh_item_history(jQuery("#itemcode").val(), true);
									jQuery("#dialog-status").html(response.message).removeClass().addClass("ui-state-default");
								}
								else if(response.status == "ERROR") {
									jQuery("#dialog-status").html(response.message).removeClass().addClass("ui-state-error");
								}
								else {
									jQuery("#dialog-status").html("There was an error adding the item").removeClass().addClass("ui-state-error");
								}
							},
							error: function() {
								jQuery("#dialog-status").html("There was an error calling the script").removeClass().addClass("ui-state-error");
							},
							complete: function() {
								dialog_buttons.removeClass('ui-state-disabled');
							}
						});
					},
					Cancel: function() {
						jQuery(this).dialog("close");
					}
				},
				close: function() {
					jQuery("#dialog-form-data")[0].reset();
					jQuery("#dialog-status").removeClass().html("");
				}
			});
			
			jQuery('.bubbleInfo').each(function () {
				// options
				var distance = 10;
				var time = 250;
				var hideDelay = 500;

				var hideDelayTimer = null;

				// tracker
				var beingShown = false;
				var shown = false;

				var trigger = jQuery(this).parent().find('.trigger');
				var popup = jQuery('.popup', this).css('opacity', 0);

				// set the mouseover and mouseout on both element
				jQuery([trigger.get(0), popup.get(0)]).mouseover(function () {
				  // stops the hide event if we move from the trigger to the popup element
				  if (hideDelayTimer) clearTimeout(hideDelayTimer);

				  // don't trigger the animation again if we're being shown, or already visible
				  if (beingShown || shown) {
					return;
				  } else {
					beingShown = true;

					// reset position of popup box
					popup.css({
					  top: -100,
					  left: -33,
					  display: 'block' // brings the popup back in to view
					})

					// (we're using chaining on the popup) now animate it's opacity and position
					.animate({
					  top: '-=' + distance + 'px',
					  opacity: 1
					}, time, 'swing', function() {
					  // once the animation is complete, set the tracker variables
					  beingShown = false;
					  shown = true;
					});
				  }
				}).mouseout(function () {
				  // reset the timer if we get fired again - avoids double animations
				  if (hideDelayTimer) clearTimeout(hideDelayTimer);
				  
				  // store the timer so that it can be cleared in the mouseover if required
				  hideDelayTimer = setTimeout(function () {
					hideDelayTimer = null;
					popup.animate({
					  top: '-=' + distance + 'px',
					  opacity: 0
					}, time, 'swing', function () {
					  // once the animate is complete, set the tracker variables
					  shown = false;
					  // hide the popup entirely after the effect (opacity alone doesn't do the job)
					  popup.css('display', 'none');
					});
				  }, hideDelay);
				});
			});
		});
	</script>
<?php endif; ?>
<?php
    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%><tr><td colspan=2>";
    echo "<a href=businesstrack.php><img src=logo.jpg border=0 height=43 width=205></a><p></td></tr>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM company WHERE companyid = '$companyid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $companyname=Treat_DB_ProxyOld::mysql_result($result,0,"companyname");
    $com_logo=@Treat_DB_ProxyOld::mysql_result($result,0,"logo");

    if($com_logo == ""){$com_logo = "talogo.gif";}

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM security WHERE securityid = '$mysecurity'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $sec_bus_sales=Treat_DB_ProxyOld::mysql_result($result,0,"bus_sales");
    $sec_bus_invoice=Treat_DB_ProxyOld::mysql_result($result,0,"bus_invoice");
    $sec_bus_order=Treat_DB_ProxyOld::mysql_result($result,0,"bus_order");
    $sec_bus_payable=Treat_DB_ProxyOld::mysql_result($result,0,"bus_payable");
    $sec_bus_inventor1=Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor1");
    $sec_bus_control=Treat_DB_ProxyOld::mysql_result($result,0,"bus_control");
    $sec_bus_menu=Treat_DB_ProxyOld::mysql_result($result,0,"bus_menu");
    $sec_bus_order_setup=Treat_DB_ProxyOld::mysql_result($result,0,"bus_order_setup");
    $sec_bus_request=Treat_DB_ProxyOld::mysql_result($result,0,"bus_request");
    $sec_route_collection=Treat_DB_ProxyOld::mysql_result($result,0,"route_collection");
    $sec_route_labor=Treat_DB_ProxyOld::mysql_result($result,0,"route_labor");
    $sec_route_detail=Treat_DB_ProxyOld::mysql_result($result,0,"route_detail");
    $sec_bus_labor=Treat_DB_ProxyOld::mysql_result($result,0,"bus_labor");
    $sec_bus_budget=Treat_DB_ProxyOld::mysql_result($result,0,"bus_budget");

    echo "<tr bgcolor=#CCCCFF><td width=1%><img src=logo/$com_logo></td><td><font size=4><b>$businessname #$bus_unit</b></font><br>$street<br>$city, $state $zip<br>$phone</td>";
    echo "<td align=right valign=top>";
    echo "<br><FORM ACTION=editbus.php method=post>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<input type=hidden name=businessid value=$businessid>";
    echo "<input type=hidden name=companyid value=$companyid>";
    echo "<INPUT TYPE=submit VALUE=' Store Setup '></FORM></td></tr>";

    echo "<tr><td colspan=3><font color=#999999>";
    if ($sec_bus_sales>0){echo "<a href=busdetail.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Sales</font></a>";}
    if ($sec_bus_invoice>0){echo " | <a href=businvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Invoicing</font></a>";}
    if ($sec_bus_order>0){echo " | <a href=buscatertrack.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Orders</font></a>";}
    if ($sec_bus_payable>0){echo " | <a href=busapinvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Payables</font></a>";}
    if ($sec_bus_inventor1>0){echo " | <a href=businventor.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Inventory</font></a>";}
    if ($sec_bus_control>0){echo " | <a href=buscontrol.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Control Sheet</font></a>";}
    if ($sec_bus_menu>0){echo " | <a href=buscatermenu.php?bid=$businessid&cid=$companyid><font color=red onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='red' style='text-decoration:none'>Menus</font></a>";}
    if ($sec_bus_order_setup>0){echo " | <a href=buscaterroom.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Order Setup</font></a>";}
    if ($sec_bus_request>0){echo " | <a href=busrequest.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Requests</font></a>";}
    if ($sec_route_collection>0||$sec_route_labor>0||$sec_route_detail>0||$sec_route_machine>0){echo " | <a href=busroute_collect.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Route Collections</font></a>";}
    if ($sec_bus_labor>0){echo " | <a href=buslabor.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Labor</font></a>";}
	if ($sec_bus_budget>0||($businessid == $bid && $pr1 == 1)||($businessid == $bid2 && $pr2 == 1)||($businessid == $bid3 && $pr3 == 1)||($businessid == $bid4 && $pr4 == 1)||($businessid == $bid5 && $pr5 == 1)||($businessid == $bid6 && $pr6 == 1)||($businessid == $bid7 && $pr7 == 1)||($businessid == $bid8 && $pr8 == 1)||($businessid == $bid9 && $pr9 == 1)||($businessid == $bid10 && $pr10 == 1)){echo " | <a href=busbudget.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Budget</font></a>";}
    echo "</td></tr>";

    echo "</table></center><p>";

if ($security_level>0){
    echo "<center><table width=90%><tr><td width=50%><form action=businvoice.php method=post>";
    echo "<select name=gobus onChange='changePage(this.form.gobus)'>";

    $districtquery="";
    if ($security_level>2&&$security_level<7){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM login_district WHERE loginid = '$loginid'";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();

       $num--;
       while($num>=0){
          $districtid=Treat_DB_ProxyOld::mysql_result($result,$num,"districtid");
          if($districtquery==""){$districtquery="districtid = '$districtid'";}
          else{$districtquery="$districtquery OR districtid = '$districtid'";}
          $num--;
       }
    }

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    if($security_level>6){$query = "SELECT * FROM business WHERE companyid = '$companyid' ORDER BY businessname DESC";}
    elseif($security_level==2||$security_level==1){$query = "SELECT * FROM business WHERE companyid = '$companyid' AND (businessid = '$bid' OR businessid = '$bid2' OR businessid = '$bid3' OR businessid = '$bid4' OR businessid = '$bid5' OR businessid = '$bid6' OR businessid = '$bid7' OR businessid = '$bid8' OR businessid = '$bid9' OR businessid = '$bid10') ORDER BY businessname DESC";}
    elseif($security_level>2&&$security_level<7){$query = "SELECT * FROM business WHERE companyid = '$companyid' AND ($districtquery) ORDER BY businessname DESC";}
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    $num--;
    while($num>=0){
       $businessname=Treat_DB_ProxyOld::mysql_result($result,$num,"businessname");
       $busid=Treat_DB_ProxyOld::mysql_result($result,$num,"businessid");

       if ($busid==$businessid){$show="SELECTED";}
       else{$show="";}

       echo "<option value=buscatermenu5.php?bid=$busid&cid=$companyid $show>$businessname</option>";
       $num--;
    }
 
    echo "</select></td><td></form></td><td width=50% align=right>&nbsp;&nbsp;</td></tr></table></center><p>";
}


    /////tabs
    echo "<center><table width=90% cellspacing=0 cellpadding=0>";
    echo "<tr bgcolor=#CCCCCC valign=top><td width=1%><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=15%><center><a href=buscatermenu.php?cid=$companyid&bid=$businessid style=$style><font color=blue>Menus</a></center></td><td width=1% align=right><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1% bgcolor=#CCCCCC><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=15% bgcolor=#CCCCCC><center><a href=buscatermenu3.php?cid=$companyid&bid=$businessid style=$style style=$style><font color=blue>POS Menu Linking</a></center></td><td width=1% align=right bgcolor=#CCCCCC><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1% bgcolor=#CCCCCC><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=15% bgcolor=#CCCCCC><center><a href=buscatermenu4.php?cid=$companyid&bid=$businessid style=$style style=$style><font color=blue>POS Menu</a></center></td><td width=1% align=right bgcolor=#CCCCCC><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1% bgcolor=#E8E7E7><img src=leftcrn.jpg height=6 width=6 align=top></td><td width=15% bgcolor=#E8E7E7><center><a href=buscatermenu5.php?cid=$companyid&bid=$businessid style=$style style=$style><font color=black>POS Reporting</a></center></td><td width=1% align=right bgcolor=#E8E7E7><img src=rightcrn.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1% bgcolor=#CCCCCC><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=15% bgcolor=#CCCCCC><center><a href=buscatermenu6.php?cid=$companyid&bid=$businessid style=$style><font color=blue>POS Mgmt</a></center></td><td width=1% align=right bgcolor=#CCCCCC><img src=rightcrn2.jpg height=6 width=6 align=top></td><td bgcolor=white width=60%></td></tr>";
    echo "<tr height=2><td colspan=4></td><td colspan=4></td><td colspan=4></td><td colspan=3 bgcolor=#E8E7E7></td><td></td><td colspan=4></td><td colspan=1></td></tr>";
    echo "</table>";

    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%>";
    echo "<tr bgcolor=#E8E7E7><td colspan=2>&nbsp;</td></tr>";
    echo "<tr height=1 bgcolor=black valign=top><td colspan=2><img src=spacer.gif></td></tr>";

	/////multi kiosk????
	$query = "SELECT kiosk_multi_inv FROM business WHERE businessid = $businessid";
	$result = Treat_DB_ProxyOld::query($query);

	$kiosk_multi_inv = Treat_DB_ProxyOld::mysql_result($result,0,"kiosk_multi_inv");

	$terminal_no = array();
	$terminal_name = array();

	$query = "USE mburris_manage";
	$result = Treat_DB_ProxyOld::query_both($query);

	$query = "SELECT client_programid,name FROM client_programs WHERE businessid = $businessid";
	$result = Treat_DB_ProxyOld::query($query);

	$counter = 0;
	while($r = Treat_DB_ProxyOld::mysql_fetch_array($result)){
		$terminal_no[$counter] = $r["client_programid"];
		$terminal_name[$counter] = $r["name"];
		$counter++;
	}

	$query = "USE mburris_businesstrack";
	$result = DB_Proxy::query_both($query);
	/////end multi kiosk

    /////choose report
    /*$report = $_GET["report"];
    $ordertype = $_GET["ordertype"];
	$current_terminal = $_GET["terminal_no"];
	$which = $_GET["which"];*/
	
	$report = \EE\Controller\Base::getGetVariable('report');
	$ordertype = \EE\Controller\Base::getGetVariable('ordertype');
	$current_terminal = \EE\Controller\Base::getGetVariable('terminal_no');
	$which = \EE\Controller\Base::getGetVariable('which');

	if($which == ""){$which = 0;}

	if($ordertype == ""){$ordertype = 1;}

    if($report==1){$sel1="SELECTED";}
    elseif($report==2){$sel2="SELECTED";}
    elseif($report==3){$sel3="SELECTED";}
    elseif($report==4){$sel4="SELECTED";}
    elseif($report==5){$sel5="SELECTED";}

	////choose report
    echo "<tr bgcolor=#E8E7E7><td colspan=2><form action=buscatermenu5.php method=post style=\"margin:0;padding:0;display:inline;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Report: <select name=report onChange='changePage(this.form.report)'><option value=buscatermenu5.php?bid=$businessid&cid=$companyid&report=>Please Choose...</option><option value=buscatermenu5.php?bid=$businessid&cid=$companyid&report=3&terminal_no=$current_terminal $sel3>Variance</option><option value=buscatermenu5.php?bid=$businessid&cid=$companyid&report=4&terminal_no=$current_terminal $sel4>Running Variance</option><option value=buscatermenu5.php?bid=$businessid&cid=$companyid&report=1&terminal_no=$current_terminal $sel1>OnHand/ReOrder</option><option value=buscatermenu5.php?bid=$businessid&cid=$companyid&report=2&terminal_no=$current_terminal $sel2>Daily Sales</option><option value=buscatermenu5.php?bid=$businessid&cid=$companyid&report=5&terminal_no=$current_terminal $sel5>Transactions</option></select></form>";

	////if multi kiosks
	if(count($terminal_no) > 1 && $kiosk_multi_inv == 1){
		if($current_terminal == 0){$current_terminal = $terminal_no[0];}

		echo "<form action=buscatermenu5.php method=post style=\"margin:0;padding:0;display:inline;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Terminal: <select name=terminal onChange='changePage(this.form.terminal)'>";

		foreach($terminal_no AS $key => $value){
			if($value == $current_terminal){$sel="SELECTED";}
			else{$sel = "";}

			echo "<option value=buscatermenu5.php?cid=$companyid&bid=$businessid&report=$report&ordertype=$ordertype&terminal_no=$value $sel>$terminal_name[$key]</option>";
		}

		echo "</select></form>";

		$forms = 5;
	}
	else{
		$current_terminal = 0;
		$forms = 4;
	}

    ////if onhand report - drill down by order type
    if($report >= 1 && $report <= 4){
        echo "<form action=buscatermenu5.php method=post style=\"margin:0;padding:0;display:inline;\">&nbsp;&nbsp;&nbsp;Order Type: <select name=ordertype onChange='changePage(this.form.ordertype)'><option value=buscatermenu5.php?bid=$businessid&cid=$companyid&report=$report&ordertype=0>All Types</option>";

        $query = "SELECT * FROM menu_items_ordertype ORDER BY id";
		$result = Treat_DB_ProxyOld::query($query);

        while($r = Treat_DB_ProxyOld::mysql_fetch_array($result)){
            $ordertype_id = $r["id"];
            $ordertype_name = $r["name"];

            if($ordertype == $ordertype_id){$sel="SELECTED";}
            else{$sel="";}

            echo "<option value=buscatermenu5.php?bid=$businessid&cid=$companyid&report=$report&ordertype=$ordertype_id&terminal_no=$current_terminal&which=$which $sel>$ordertype_name</option>";
        }

        echo "</select></form>";
    }

	//////////////////////
	////filter daily sales
    if($report==2){
        /*$t_date1 = $_POST["t_date1"];
        $t_date2 = $_POST["t_date2"];*/
		
		$t_date1 = \EE\Controller\Base::getPostVariable('t_date1');
		$t_date2 = \EE\Controller\Base::getPostVariable('t_date2');

        if($t_date1 == ""){
			$t_date1 = date("Y-m-d");
			for($counter=1;$counter<=6;$counter++){$t_date1 = prevday($t_date1);}
		}
        if($t_date2 == ""){$t_date2 = date("Y-m-d");}

        echo "<form action=buscatermenu5.php?bid=$businessid&cid=$companyid&report=2&ordertype=$ordertype&terminal_no=$current_terminal method=post style=\"margin:0;padding:0;display:inline;\">&nbsp;&nbsp;&nbsp;From ";
		echo "<SCRIPT LANGUAGE='JavaScript' ID='js18'> var cal18 = new CalendarPopup('testdiv1');cal18.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=t_date1 VALUE='$t_date1' SIZE=8> <A HREF=\"javascript:void();\" onClick=cal18.select(document.forms[$forms].t_date1,'anchor18','yyyy-MM-dd'); return false; TITLE=cal18.select(document.forms[$forms].t_date1,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor18' ID='anchor18'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A> to ";
		echo "<SCRIPT LANGUAGE='JavaScript' ID='js19'> var cal19 = new CalendarPopup('testdiv1');cal19.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=t_date2 VALUE='$t_date2' SIZE=8> <A HREF=\"javascript:void();\" onClick=cal19.select(document.forms[$forms].t_date2,'anchor19','yyyy-MM-dd'); return false; TITLE=cal19.select(document.forms[$forms].t_date2,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor19' ID='anchor19'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A>";
		echo " <input type=submit value='GO'></form>";
    }
    ////filter for transactions
    elseif($report==5){
        /*$t_date1 = $_POST["t_date1"];
        $t_date2 = $_POST["t_date2"];*/
		
		$t_date1 = \EE\Controller\Base::getPostVariable('t_date1');
		$t_date2 = \EE\Controller\Base::getPostVariable('t_date2');

        if($t_date1 == ""){$t_date1 = date("Y-m-d H:i:s", mktime(date("H") - 3, date("i"), date("s"), date("m"), date("d"), date("Y")));}
        if($t_date2 == ""){$t_date2 = date("Y-m-d H:i:s");}

        echo "<form action=buscatermenu5.php?bid=$businessid&cid=$companyid&report=5&ordertype=$ordertype&terminal_no=$current_terminal method=post style=\"margin:0;padding:0;display:inline;\">&nbsp;&nbsp;&nbsp;From <input type=text name=t_date1 value='$t_date1' size=16> to <input type=text name=t_date2 value='$t_date2' size=16> <input type=submit value='GO'></form>";
    }
	elseif($report == 3){
		$prevwhich = $which + 1;
		$nextwhich = $which - 1;

		echo "&nbsp;&nbsp;&nbsp;<a href=buscatermenu5.php?bid=$businessid&cid=$companyid&report=$report&terminal_no=$current_terminal&which=$prevwhich&ordertype=$ordertype style=$style><font size=2 color=blue>[PREV]</font></a>";
		
		if($which == 0){
			echo " |<font size=2> [LATEST]</font>";
		}
		else{
			echo " | <a href=buscatermenu5.php?bid=$businessid&cid=$companyid&report=$report&terminal_no=$current_terminal&which=0&ordertype=$ordertype style=$style><font size=2 color=blue>[LATEST]</font></a>";
		}

		if($nextwhich >= 0){
			echo " | <a href=buscatermenu5.php?bid=$businessid&cid=$companyid&report=$report&terminal_no=$current_terminal&which=$nextwhich&ordertype=$ordertype style=$style><font size=2 color=blue>[NEXT]</font></a>";
		}
		else{
			echo " |<font size=2> [NEXT]</font>";
		}
	}
    echo "</td></tr>";

    ///get machine number tied to this business
	if($current_terminal > 0){$add_query = "AND client_programid = $current_terminal";}
	else{$add_query = "";}

    $query = "SELECT machine_num FROM machine_bus_link WHERE businessid = $businessid $add_query";
    $result = Treat_DB_ProxyOld::query($query);

    $machine_num = array();
    $counter=0;
    while($r=Treat_DB_ProxyOld::mysql_fetch_array($result)){
        $machine_num[$counter] = $r["machine_num"];
        $counter++;
    }

    ///get list of active menu items
    if($ordertype > 0 && ($report == 1 || $report == 2 || $report == 3 || $report == 4)){
        $items = menu_items::list_menu($businessid,$ordertype);
    }
    elseif($report == 1 || $report == 3 || $report == 4){
        $items = menu_items::list_menu($businessid);
    }
    elseif($ordertype > 0){
        $items = menu_items::list_active_menu($businessid,$ordertype);
    }
    else{
        $items = menu_items::list_active_menu($businessid,0);
    }

    ////////////////////////
    ////DISPLAY REPORT//////
    ////////////////////////
    
        echo "<tr bgcolor=#E8E7E7><td colspan=2><center>";
    
        //onhand
        if($report==1){
            $date2 = date("Y-m-d H:i:s");
			$forty_date = date("Y-m-d",mktime(0, 0, 0, date("m") , date("d")-40, date("Y")));

            echo "<table class=\"sortable\" style=\"border:1px solid black;background-color:white;font-size:12px;\" cellspacing=0 cellpadding=1 width=98%>";
            echo "<thead><tr bgcolor=#FFFF99><td style=\"border:1px solid black;\" width=8%>Item Code</td><td style=\"border:1px solid black;\" width=20%>Item</td><td style=\"border:1px solid black;\" width=10%>Count Date</td><td style=\"border:1px solid black;\" width=5%>Last Count</td><td style=\"border:1px solid black;\" width=5%>Fills</td><td style=\"border:1px solid black;\" width=5%>Waste</td><td style=\"border:1px solid black;\" width=5%>Items Sold</td><td style=\"border:1px solid black;\" width=5%>OnHand</td><td style=\"border:1px solid black;\" width=5%>Cost</td><td style=\"border:1px solid black;\" width=5%>Total</td><td style=\"border:1px solid black;\" width=5%>Max</td><td style=\"border:1px solid black;\" width=5%>RO Pnt</td><td style=\"border:1px solid black;\" width=5%>RO Amt</td><td style=\"border:1px solid black;\" width=5%>ReOrder</td></tr></thead><tbody>";

			////download
			$download_data .= "Item Code,Item,Count Date,Last Count,Fills,Waste,Items Sold,OnHand,Cost,Total,Max,RO Pnt,RO Amt,ReOrder\r\n";

			foreach($items AS $key => $value){
                ////get item_code
                $complete_query = "SELECT id,item_code,max,reorder_point,reorder_amount FROM menu_items_new WHERE businessid = '$businessid' AND pos_id = '$key'";
				$result = Treat_DB_ProxyOld::query($complete_query);

                $item_id = Treat_DB_ProxyOld::mysql_result($result,0,"id");
                $item_code = Treat_DB_ProxyOld::mysql_result($result,0,"item_code");
                $max = Treat_DB_ProxyOld::mysql_result($result,0,"max");
                $reorder_point = Treat_DB_ProxyOld::mysql_result($result,0,"reorder_point");
                $reorder_amount = Treat_DB_ProxyOld::mysql_result($result,0,"reorder_amount");

                ////get dates
                $dates = Kiosk::last_inv_dates($machine_num,$item_code);
                $date = $dates[1];
                if($date==""){$date=$dates[0];}

				/*
				///get mei name
				$query5 = "SELECT item_name FROM mei_product WHERE item_code = '$item_code'";
				$result5 = Treat_DB_ProxyOld::query($query5);

				$mei_name = mysql_result($result5,0,"item_name");
				 * <td style=\"border:1px solid black;\">&nbsp;$mei_name</td>
				*/

				////cost
				$query5 = "SELECT cost FROM menu_items_price WHERE menu_item_id = $item_id";
				$result5 = Treat_DB_ProxyOld::query($query5);

				$cost = Treat_DB_ProxyOld::mysql_result($result5,0,"cost");

                ////last count
                $onhand2 = Kiosk::inv_onhand($machine_num, $item_code, $date);

                ////fills
                $fills = Kiosk::fills($machine_num, $item_code, $date, $date2);

                ////waste
                $waste = Kiosk::waste($machine_num, $item_code, $date, $date2);

                ////get check detail
                $items_sold = menu_items::items_sold($key,$businessid,$date,$date2,$current_terminal);

				////sold in last 40 days
				$forty_sold = menu_items::items_sold($key,$businessid,$forty_date,$date2,$current_terminal);

                $onhand = $onhand2 + $fills - $waste - $items_sold;

                /////REORDER
                if($reorder_point == 0 && $max != 0){
                    $reorder = $max - $onhand;
                }
                elseif($max != 0){
                    if($onhand <= $reorder_point){
                        if($reorder_amount != 0 && $reorder_amount > ($max - $onhand)){
                            $reorder = $reorder_amount;
                        }
                        else{
                            $reorder = $max - $onhand;
                        }
                    }
                    else{$reorder = "&nbsp;";}
                }
                else{$reorder = 0;}
                if($reorder > $max){$reorder = $max;}
                if($reorder <= 0){$reorder="&nbsp;";}

				if($forty_sold == 0){$none_sold="<font color=red>*</font>";}
				else{$none_sold = "";}

				$report_total += $onhand * $cost;
				$total_cost = number_format($onhand * $cost, 2);
				$cost = number_format($cost, 2);

                echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor=''><td style=\"border:1px solid black;\">&nbsp;$item_code</td><td style=\"border:1px solid black;\"><a href=buscatermenu4.php?bid=$businessid&cid=$companyid&id=$item_id&dir=1&report=$report&ordertype=$ordertype style=$style>$none_sold<font color = blue>$value</font></a></td><td style=\"border:1px solid black;\">&nbsp;$date</td><td style=\"border:1px solid black;\">&nbsp;$onhand2</td><td style=\"border:1px solid black;\">&nbsp;$fills</td><td style=\"border:1px solid black;\">&nbsp;$waste</td><td style=\"border:1px solid black;\">&nbsp;$items_sold</td><td style=\"border:1px solid black;\" bgcolor=#E8E7E7>&nbsp;$onhand</td><td style=\"border:1px solid black;\">&nbsp;$$cost</td><td style=\"border:1px solid black;\">&nbsp;$$total_cost</td><td style=\"border:1px solid black;\">&nbsp;$max</td><td style=\"border:1px solid black;\">&nbsp;$reorder_point</td><td style=\"border:1px solid black;\">&nbsp;$reorder_amount</td><td style=\"border:1px solid black;\">&nbsp;$reorder</td></tr>";

				///download
				$download_data .= "$item_code,\"$value\",$date,$onhand2,$fills,$waste,$items_sold,$onhand,$cost,$total_cost,$max,$reorder_point,$reorder_amount,$reorder\r\n";
            }
			echo "</tbody>";
			$report_total = number_format($report_total, 2);
			echo "<tfoot><tr bgcolor=#FFFF99><td colspan=9 style=\"border:1px solid black;\">&nbsp;</td><td colspan=1 style=\"border:1px solid black;\">&nbsp;$$report_total</td><td colspan=4 style=\"border:1px solid black;\">&nbsp;</td></tr></tfoot>";
            echo "</table>&nbsp;&nbsp;&nbsp;<font color=red>*</font>None Sold in last 40 Days.<br>";
        }
        //sales
        elseif($report==2){
            $date = $t_date1;
			$today = $t_date2;
			$date2 = date("Y-m-d");

			///sum of sales for period
			$query = "SELECT SUM(creditdetail.amount) AS totalsales FROM creditdetail,credits WHERE credits.businessid = $businessid AND credits.credittype = 1 AND credits.creditid = creditdetail.creditid AND creditdetail.date >= '$date' AND creditdetail.date <= '$today'";
			$result = Treat_DB_ProxyOld::query($query);
			
			$totalsales = Treat_DB_ProxyOld::mysql_result($result,0,"totalsales");

            echo "<table class=\"sortable\" style=\"border:1px solid black;font-size:12px;background-color:white;\" cellspacing=0 cellpadding=1 width=98%>";
            echo "<thead><tr bgcolor=#FFFF99><td style=\"border:1px solid black;\" width=8%>Item Code</td><td style=\"border:1px solid black;\" width=20%>Item</td><td style=\"border:1px solid black;\">Cost</td><td style=\"border:1px solid black;\">Price</td>";

			$download_data .= "Item Code,Item,Cost,Price,";

                $date1 = $date;
                while($date1<=$today){
                    $day=substr($date1,8,2);
                    $month=substr($date1,5,2);
                    echo "<td style=\"border:1px solid black;\">$month/$day</td>";
					$download_data .= "$month/$day,";
                    $date1=nextday($date1);
                }
            echo "<td style=\"border:1px solid black;\"><b>Total</b></td><td style=\"border:1px solid black;\"><b>Waste</b></td><td style=\"border:1px solid black;\"><b>Par</b></td><td style=\"border:1px solid black;\"><b>OnHand</b></td><td style=\"border:1px solid black;\" title='Percent of Sales'><center><b>%</b></center></td></tr></thead><tbody>";

			$download_data .= "Total,Waste,Par,OnHand,%\r\n";

            foreach($items AS $key => $value){
                ////get item_code
                $complete_query = "SELECT item_code,id,max FROM menu_items_new WHERE businessid = '$businessid' AND pos_id = '$key'";
				$result = Treat_DB_ProxyOld::query($complete_query);

                $item_code = Treat_DB_ProxyOld::mysql_result($result,0,"item_code");
                $id = Treat_DB_ProxyOld::mysql_result($result,0,"id");
				$max = Treat_DB_ProxyOld::mysql_result($result,0,"max");

                $complete_query = "SELECT price FROM menu_items_price WHERE menu_item_id = '$id'";
				$result = Treat_DB_ProxyOld::query($complete_query);

                $price = Treat_DB_ProxyOld::mysql_result($result,0,"price");
                $price = number_format($price,2);

				////cost
				$query5 = "SELECT cost FROM menu_items_price WHERE menu_item_id = $id";
				$result5 = Treat_DB_ProxyOld::query($query5);

				$cost = mysql_result($result5,0,"cost");
				$cost = number_format($cost, 2);

                echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor=''><td style=\"border:1px solid black;\">&nbsp;$item_code</td><td style=\"border:1px solid black;\">&nbsp;$value</td><td style=\"border:1px solid black;\">&nbsp;$$cost</td><td style=\"border:1px solid black;\">&nbsp;$$price</td>";
                
				$download_data .= "$item_code,\"$value\",$cost,$price,";

				////get check detail
                $date1 = $date;
                $total=0;
                while($date1<=$today){
                    $items_sold = menu_items::items_sold($key,$businessid,$date1,$date1,$current_terminal);
                    $total+=$items_sold;
					if($items_sold == ""){$items_sold="&nbsp;";}
                    echo "<td style=\"border:1px solid black;\">&nbsp;$items_sold</td>";
					$download_data .= "$items_sold,";
                    $date1 = nextday($date1);
                }

				////get dates
                $inv_dates = Kiosk::last_inv_dates($machine_num,$item_code);
                $inv_date = $inv_dates[1];
                if($inv_date==""){$inv_date=$inv_dates[0];}

                ////last count
                $onhand2 = Kiosk::inv_onhand($machine_num, $item_code, $inv_date);

                ////fills
                $fills = Kiosk::fills($machine_num, $item_code, $inv_date, $date2);

                ////waste
                $waste = Kiosk::waste($machine_num, $item_code, $inv_date, $date2);

				//// date range waste
                $dated_waste = Kiosk::waste($machine_num, $item_code, "$date 00:00:00", "$today 23:59:59", 1);

                ////get check detail
                $items_sold = menu_items::items_sold($key,$businessid,$inv_date,$date2,$current_terminal);

                $onhand = $onhand2 + $fills - $waste - $items_sold;

				$percent = round((($total*$price)/$totalsales)*100,2);
                echo "<td style=\"border:1px solid black;\" bgcolor=#E8E7E7>&nbsp;$total</td><td style=\"border:1px solid black;\">&nbsp;$dated_waste</td><td style=\"border:1px solid black;\">&nbsp;$max</td><td style=\"border:1px solid black;\">&nbsp;$onhand</td><td style=\"border:1px solid black;\" bgcolor=#E8E7E7><center><font color=blue size=1>&nbsp;$percent%</font></center></td></tr>";
				$download_data .= "$total,$dated_waste,$max,$onhand,$percent%\r\n";
            }
            echo "</tbody></table><br>";
        }
        //variance
        elseif($report==3){
			$totalcost = 0;
			$totalprice = 0;
            echo "<table class=\"sortable\" style=\"border:1px solid black;font-size:12px;background-color:white;\" cellspacing=0 cellpadding=1 width=98%>";
            echo "<thead><tr bgcolor=#FFFF99><td style=\"border:1px solid black;\" width=8%>Item Code</td><td style=\"border:1px solid black;\" width=20%>Item</td><td style=\"border:1px solid black;\" width=10%>Count1 Date</td><td style=\"border:1px solid black;\" width=6%>Count1</td><td style=\"border:1px solid black;\" width=6%>Fills</td><td style=\"border:1px solid black;\" width=6%>Waste</td><td style=\"border:1px solid black;\" width=10%>Count2 Date</td><td style=\"border:1px solid black;\" width=6%>Count2</td><td style=\"border:1px solid black;\" width=6%>Count +/-</td><td style=\"border:1px solid black;\" width=6%>Items Sold</td><td style=\"border:1px solid black;\" width=4%>+/-</td><td style=\"border:1px solid black;\" width=%>Cost</td><td style=\"border:1px solid black;\" width=%>Price</td></tr></thead><tbody>";
			$download_data .= "Item Code,Item,Count1 Date,Count1,Fills,Waste,Count2 Date,Count2,Count +/-,Items Sold,+/-,Cost,Price\r\n";
            foreach($items AS $key => $value){
                ////get item_code
                $complete_query = "SELECT item_code,id FROM menu_items_new WHERE businessid = '$businessid' AND pos_id = '$key'";
				$result = Treat_DB_ProxyOld::query($complete_query);

                $item_code = Treat_DB_ProxyOld::mysql_result($result,0,"item_code");
				$id = Treat_DB_ProxyOld::mysql_result($result,0,"id");

				///get item price
				$complete_query = "SELECT price FROM menu_items_price WHERE menu_item_id = '$id'";
				$result = Treat_DB_ProxyOld::query($complete_query);

                $price = Treat_DB_ProxyOld::mysql_result($result,0,"price");

                ////get dates
                $dates = Kiosk::last_inv_dates($machine_num,$item_code,$which);
                $date1 = $dates[0];
                $date2 = $dates[1];

                ////get onhand qty's
                $onhand1 = Kiosk::inv_onhand($machine_num, $item_code, $date1);
                $onhand2 = Kiosk::inv_onhand($machine_num, $item_code, $date2,1);
                $fills = Kiosk::fills($machine_num, $item_code, $date1, $date2);

                ////waste
                $waste = Kiosk::waste($machine_num, $item_code, $date1, $date2);

                ////get check detail
                $items_sold = menu_items::items_sold($key,$businessid,$date1,$date2,$current_terminal);

				///cost
				$cost = Kiosk::cost($machine_num, $item_code);

                $diff1 = $onhand1 + $fills - $waste - $onhand2;
                $diff2 = $items_sold - $diff1;

				$totalcost += $cost*$diff2;
				$totalprice += $price*$diff2;
				$cost = number_format($cost*$diff2,2);
				$price = number_format($price*$diff2,2);

                if($diff2==0){$color="#00FF00";}
                elseif(abs($diff2) > 0 && abs($diff2) <= 3){$color="yellow";}
                else{$color="red";}
			
				$onclick = $sec_bus_menu_pos_report>1 ? "onClick='open_dialog(\"$item_code\", \"" . htmlentities($value, ENT_QUOTES) . "\", \"$machine_num\")'" : "";
                echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor='' $onclick><td style=\"border:1px solid black;\">&nbsp;$item_code</td><td style=\"border:1px solid black;\">&nbsp;$value</td><td style=\"border:1px solid black;\">&nbsp;$date1</td><td style=\"border:1px solid black;\">&nbsp;$onhand1</td><td style=\"border:1px solid black;\">&nbsp;$fills</td><td style=\"border:1px solid black;\">&nbsp;$waste</td><td style=\"border:1px solid black;\">&nbsp;$date2</td><td style=\"border:1px solid black;\">&nbsp;$onhand2</td><td style=\"border:1px solid black;\">&nbsp;$diff1</td><td style=\"border:1px solid black;\">&nbsp;$items_sold</td><td style=\"border:1px solid black;\" bgcolor=$color>&nbsp;$diff2</td><td style=\"border:1px solid black;\" bgcolor=$color>&nbsp;$cost</td><td style=\"border:1px solid black;\" bgcolor=$color>&nbsp;$price</td></tr>";
				$download_data .= "$item_code,\"$value\",$date1,$onhand1,$fills,$waste,$date2,$onhand2,$diff1,$items_sold,$diff2,$cost,$price\r\n";
            }
            $totalcost = number_format($totalcost,2);
            $totalprice = number_format($totalprice,2);
            echo "</tbody><tfoot><tr><td style=\"border:1px solid black;\" colspan=11>&nbsp;</td><td style=\"border:1px solid black;\" title='Cost Variance'><center><b>$$totalcost</b></center></td><td style=\"border:1px solid black;\" title='Price Variance'><center><b>$$totalprice</b></center></td></tr></tfoot></table><br>";
        }
        //running variance
        elseif($report==4){
            echo "<table class=\"sortable\" style=\"border:1px solid black;font-size:12px;background-color:white;\" cellspacing=0 cellpadding=1 width=98%>";
            echo "<thead><tr bgcolor=#FFFF99><td style=\"border:1px solid black;\" width=8%>Item Code</td><td style=\"border:1px solid black;\" width=20%>Item</td>";

			$download_data .= "Item Code,Item,Variance1,Variance2,Variance3,Variance4,Variance5,Total\r\n";

            for($counter=0;$counter<=4;$counter++){
                $mycount = $counter + 1;
                echo "<td style=\"border:1px solid black;\" width=8%>Variance$mycount</td>";
            }
            echo "<td style=\"border:1px solid black;\" width=8%>Total</td></tr></thead><tbody>";

            foreach($items AS $key => $value){
                ////get item_code
                $complete_query = "SELECT item_code FROM menu_items_new WHERE businessid = '$businessid' AND pos_id = '$key'";
				$result = Treat_DB_ProxyOld::query($complete_query);

                $item_code = Treat_DB_ProxyOld::mysql_result($result,0,"item_code");

                echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor=''><td style=\"border:1px solid black;\">&nbsp;$item_code</td><td style=\"border:1px solid black;\">&nbsp;$value</td>";
				$download_data .= "$item_code,\"$value\",";

                $total=0;
                for($counter=4;$counter>=0;$counter--){
                ////get dates
                $dates = Kiosk::last_inv_dates($machine_num,$item_code,$counter);
                $date1 = $dates[0];
                $date2 = $dates[1];

                ////get onhand qty's
                $onhand1 = Kiosk::inv_onhand($machine_num, $item_code, $date1);
                $onhand2 = Kiosk::inv_onhand($machine_num, $item_code, $date2,1);
                $fills = Kiosk::fills($machine_num, $item_code, $date1, $date2);

                ////waste
                $waste = Kiosk::waste($machine_num, $item_code, $date1, $date2);

                ////get check detail
                $items_sold = menu_items::items_sold($key,$businessid,$date1,$date2,$current_terminal);

                $diff1 = $onhand1 + $fills - $waste - $onhand2;
                $diff2 = $items_sold - $diff1;

                $total += $diff2;

                if($diff2==0){$diff2="";}
                echo "<td style=\"border:1px solid black;\" width=8%>&nbsp;$diff2</td>";
				$download_data .= "$diff2,";
                }

                if($total==0){$color="#00FF00";}
                elseif(abs($total) > 0 && abs($total) <= 3){$color="yellow";}
                else{$color="red";}
                echo "<td style=\"border:1px solid black;\" width=8% bgcolor=$color>&nbsp;$total</td></tr>";
				$download_data .= "$total\r\n";
            }
            echo "</tbody></table><br>";
        }
		////transactions
        elseif($report==5){
			if($current_terminal > 0){$add_query = "AND terminal_group = $current_terminal";}
			else{$add_query = "";}

            $query = "SELECT * FROM checks WHERE businessid = '$businessid' AND bill_datetime >= '$t_date1' AND checks.bill_datetime <= '$t_date2' $add_query ORDER BY bill_datetime";
            $result = Treat_DB_ProxyOld::query($query);

            echo "<table class=\"sortable\" style=\"border:1px solid black;font-size:12px;background-color:white;\" cellspacing=0 cellpadding=1 width=98%>";
            echo "<thead><tr bgcolor=#FFFF99><td style=\"border:1px solid black;\">Check#</td><td style=\"border:1px solid black;\">Time</td><td style=\"border:1px solid black;\">Item</td><td style=\"border:1px solid black;\">Qty</td><td style=\"border:1px solid black;\">Price</td><td style=\"border:1px solid black;\">Total</td></tr></thead><tbody>";

			$download_data .= "Check#,Time,Item,Qty,Price,Total\r\n";

            $lascheck = 0;
            while($r = Treat_DB_ProxyOld::mysql_fetch_array($result)){
                $check_number = $r["check_number"];
                $bill_datetime = $r["bill_datetime"];
                $total = $r["total"];
                $taxes = $r["taxes"];
                $check_is_void = $r["is_void"];

                ///check detail
                $query2 = "SELECT checkdetail.quantity,checkdetail.price,checkdetail.is_void,menu_items_new.name FROM checkdetail,menu_items_new WHERE checkdetail.bill_number = '$check_number' AND checkdetail.businessid = '$businessid' AND checkdetail.item_number = menu_items_new.pos_id AND menu_items_new.businessid = '$businessid'";
                $result2 = Treat_DB_ProxyOld::query($query2);
                $num_detail = Treat_DB_ProxyOld::mysql_numrows($result2);

                while($r2 = Treat_DB_ProxyOld::mysql_fetch_array($result2)){
                    $qty = $r2["quantity"];
                    $price = $r2["price"];
                    $name = $r2["name"];
                    $is_void = $r2["is_void"];

                    $total = number_format($qty*$price,2);

                    if($check_is_void == 1){$show_color="red";}
                    else{$show_color="white";}

                    if($is_void==1){$show_style="text-decoration: line-through;";}
                    else{$show_style="text-decoration: none;";}

                    echo "<tr bgcolor=$show_color><td style=\"border:1px solid black;$show_style\">&nbsp;$check_number</td><td style=\"border:1px solid black;$show_style\">&nbsp;$bill_datetime</td><td style=\"border:1px solid black;$show_style\">&nbsp;$name</td><td style=\"border:1px solid black;$show_style\" width=10%>&nbsp;$qty</td><td style=\"border:1px solid black;$show_style\" width=10%>&nbsp;$price</td><td style=\"border:1px solid black;$show_style\" width=10%>&nbsp;$total</td></tr>";
					$download_data .= "$check_number,$bill_datetime,\"$name\",$qty,$price,$total\r\n";
                }

                ///tax
                if($taxes!=0){
					echo "<tr bgcolor=#E8E7E7><td style=\"border:1px solid black;\">&nbsp;$check_number</td><td style=\"border:1px solid black;\">&nbsp;$bill_datetime</td><td style=\"border:1px solid black;\">&nbsp;TAX</td><td style=\"border:1px solid black;\">&nbsp;</td><td style=\"border:1px solid black;\">&nbsp;</td><td style=\"border:1px solid black;\">&nbsp;$taxes</td></tr>";
					$download_data .= "$check_number,$bill_datetime,TAX,,,$taxes\r\n";
				}

                ///payments
                $query2 = "SELECT * FROM payment_detail WHERE check_number = '$check_number' AND businessid = '$businessid'";
                $result2 = Treat_DB_ProxyOld::query($query2);

                while($r2 = Treat_DB_ProxyOld::mysql_fetch_array($result2)){
                    $payment_type = $r2["payment_type"];
                    $received = $r2["received"];
                    $scancode = $r2["scancode"];

                    if($payment_type == 14){$payment_type = "Gift Card";}
                    elseif($payment_type == 4){$payment_type = "Credit Card";}
					elseif($payment_type == 5){$payment_type = "Payroll Deduct";}
                    else{$payment_type = "Cash";}

                    $show_name = "";
                    if($scancode != ""){
                        $query3 = "SELECT first_name,last_name FROM customer WHERE scancode = '$scancode'";
                        $result3 = Treat_DB_ProxyOld::query($query3);

                        $first_name = Treat_DB_ProxyOld::mysql_result($result3,0,"first_name");
                        $last_name = Treat_DB_ProxyOld::mysql_result($result3,0,"last_name");

                        if($fist_name == "" && $last_name == ""){$show_name="(<i>unknown</i> - $scancode)";}
                        else{$show_name = "($first_name $last_name - $scancode)";}
                    }

                    if($num_detail == 0){$show_color="#CCCC99";}
                    else{$show_color="#BBBBBB";}

                    echo "<tr bgcolor=$show_color><td style=\"border:1px solid black;\">&nbsp;$check_number</td><td style=\"border:1px solid black;\">&nbsp;$bill_datetime</td><td style=\"border:1px solid black;\">&nbsp;$payment_type $show_name</td><td style=\"border:1px solid black;\">&nbsp;</td><td style=\"border:1px solid black;\">&nbsp;</td><td style=\"border:1px solid black;\">&nbsp;$received</td></tr>";
					$download_data .= "$check_number,$bill_datetime,\"$payment_type $show_name\",,,$received\r\n";
                }
            }
            echo "</tbody></table></center><br>";
        }
        else{
            echo "Please Choose a Report...<p>";
			$download_disable = "DISABLED";
        }
        
        echo "</center></td></tr>";

	$download_data = str_replace("'","`",$download_data);

	echo "<tr bgcolor=#E8E7E7><td align=left>&nbsp;<form action=posreport.php method=post style=\"margin:0;padding:0;display:inline;\"><input type=hidden name=download_data value='$download_data'><input type=submit value='Download' $download_disable style=\"border: 1px solid #999999; font-size: 10px; background-color:#CCCCCC;\"></form></td></tr>";
    echo "</table></center>";

    echo "<DIV ID=testdiv1 STYLE=position:absolute;visibility:hidden;background-color:white;layer-background-color:white;></DIV>";

    echo "<br><FORM ACTION=businesstrack.php method=post>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
	if($report == 3 && $sec_bus_menu_pos_report>1)echo "<center><a href='#' id='upload_file_link'>Upload Data File</a></center>";
    echo "<center><INPUT TYPE=submit VALUE=' Return to Main Page'></FORM></center>";

    google_page_track();
}
?>
	</body>
</html>