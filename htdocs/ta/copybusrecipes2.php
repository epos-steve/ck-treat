<?php
if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
require_once( DOC_ROOT . '/bootstrap.php' );

$style = 'text-decoration:none';
$today = date( 'Y-m-d' );

$user = \EE\Controller\Base::getSessionCookieVariable( 'usercook' );
$pass = \EE\Controller\Base::getSessionCookieVariable( 'passcook' );
$businessid = \EE\Controller\Base::getPostVariable( 'businessid' );
$companyid = \EE\Controller\Base::getPostVariable( 'companyid' );
$which_menu = \EE\Controller\Base::getPostVariable( 'which_menu' );

$items_missing = \EE\Controller\Base::getPostVariable( 'items_missing' );
$items_no_supc = \EE\Controller\Base::getPostVariable( 'items_no_supc' );

$menu_item = unserialize( urldecode( \EE\Controller\Base::getPostVariable( 'menu_item' ) ) );
$inv_items_needed = unserialize( urldecode( \EE\Controller\Base::getPostVariable( 'inv_items_needed' ) ) );
$missing_inv_items = unserialize( urldecode( \EE\Controller\Base::getPostVariable( 'missing_inv_items' ) ) );
$no_supc_items = unserialize( urldecode( \EE\Controller\Base::getPostVariable( 'no_supc_items' ) ) );

$mycounter = \EE\Controller\Base::getPostVariable( 'mycounter' );
$mycounter2 = \EE\Controller\Base::getPostVariable( 'mycounter2' );

$passed_cookie_login = require_once( realpath( DOC_ROOT . '/../lib/inc/cookie-login.php' ) );
if (
	$passed_cookie_login
	&& (
		(
			$GLOBALS['security_level'] == 1
			&& $GLOBALS['bid'] == $page_business->getBusinessId()
			&& $GLOBALS['cid'] == $page_business->getCompanyId()
		)
		|| (
			$GLOBALS['security_level'] > 1
			&& $GLOBALS['cid'] == $page_business->getCompanyId()
		)
	)
) {
?>
<html>
<head>
	<title>Essential Elements :: Treat America :: copybusrecipes2</title>
		<link rel="stylesheet" type="text/css" href="/assets/css/style.css" />
		<link rel="stylesheet" type="text/css" href="/assets/css/ta/generic.css" />
		<link rel="stylesheet" type="text/css" href="/assets/css/ta/recipes.css" />
<script type="text/javascript" src="/assets/js/preload/preLoadingMessage.js"></script>

<script language=javascript type="text/javascript">
function contconfirm(){return confirm('Are you sure you are finished?');}
</script>
</head>

	<body>
<?php
	if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
/*
*/
?>
	<pre style="text-align: left;"><?php
		var_dump( $_POST );
		echo '$menu_item';
		var_dump( $menu_item );
		echo '$inv_items_needed';
		var_dump( $inv_items_needed );
		echo '$missing_inv_items';
		var_dump( $missing_inv_items );
		echo '$no_supc_items';
		var_dump( $no_supc_items );
	?></pre>
<?php
/*
*/
	}
?>
		<center class="page_wrapper">
			<table cellspacing="0" cellpadding="0" border="0" width="90%">
				<tr>
					<td colspan="2">
						<img src="logo.jpg" alt="logo" />
						<p>&nbsp;</p>
					</td>
				</tr>
			</table>
			
			<div class="flash_error">
				<strong>
					If you are in Kansas City, Saint Louis, Indiana, Omaha or Des Moines you may disregard this page except to click finish below.
				</strong>
			</div>
<?php

/*
* /
	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM company WHERE companyid = '$companyid'";
	$result = Treat_DB_ProxyOld::query( $query, true );
	//mysql_close();

	$companyname = @mysql_result( $result, 0, 'companyname' );

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM business WHERE businessid = '$businessid'";
	$result = Treat_DB_ProxyOld::query( $query, true );
	//mysql_close();

	$businessname = @mysql_result( $result, 0, 'businessname' );
/*
*/


	$recipe_link = array();
	$update_rec_units = array();
 
	///////////////////////////////////////////////
	///////////DISPLAY/////////////////////////////
	///////////////////////////////////////////////
?>
			<table width="90%" border="1">
				<tr bgcolor="#E8E7E7">
					<td colspan="3">
						<font color="black">
							<strong>Menu Items Added</strong>
						</font>
					</td>
				</tr>
<?php
	$pattern = array(
#		'@ - (B&?V|Corp\.? Chef|FRB|Quest|W&?D)$@i',
	);
	$replace = array(
#		'',
	);
	# $key = $menu_itemid
	# $value = $busid
	foreach ( $menu_item as $menu_itemid => $busid ) {
		if ( $menu_itemid ) {
			$menu_item_orig = Treat_Model_Menu_Item::getById( $menu_itemid );
?>
<?php /*
* / ?>
				<tr>
					<td colspan="3"><pre>$menu_item_orig = <?php var_dump( $menu_item_orig ); ?></pre></td>
				</tr>
<?php /*
*/ ?>
<?php
			if ( null !== $menu_item_orig ) {
				$menu_item_orig->keeper = 0;
				$menu_item_orig->save();

				$menu_item_orig->menu_item_id = 0; // this should force a new item
				$menu_item_orig->item_name = preg_replace(
					$pattern
					,$replace
					,$menu_item_orig->item_name
				);
				$menu_item_orig->businessid = $businessid;
				$menu_item_orig->companyid = $companyid;
				$menu_item_orig->price = 0;
				$menu_item_orig->menu_typeid = $which_menu;
				$menu_item_orig->recipe_active = 1;
				$menu_item_orig->save();
				# $key = $menu_itemid
				# $value = menu_item_id
				$menu_item[ $menu_itemid ] = Treat_Model_Menu_Item::db()->lastInsertId();
?>
				<tr>
					<td>
						<?php echo $menu_itemid; ?>
					</td>
					<td colspan="2">
						<?php echo $menu_item[ $menu_itemid ]; ?>
						<font color="blue"><?php echo $menu_item_orig->item_name; ?></font>
					</td>
				</tr>
<?php /*
* / ?>
				<tr>
					<td colspan="3"><pre><?php var_dump( $menu_item_orig ); ?></pre></td>
				</tr>
				<tr>
					<td colspan="3"><hr/></td>
				</tr>
<?php /*
*/ ?>
<?php
			}
			else {
				$menu_item[ $menu_itemid ] = 0;
			}
		}
		else {
			$menu_item[ $menu_itemid ] = 0;
		}
	}

?>
				<tr bgcolor="#E8E7E7">
					<td colspan="3">
						<font color="blue">
							<strong>Existing Inventory Items Updated</strong>
						</font>
					</td>
				</tr>
<?php
	# $key = $item_code
	# $value = $inv_itemid
	foreach ( $inv_items_needed as $inv_itemid => $item_code ) {
		if ( $inv_itemid ) {
			$inv_item_orig = Treat_Model_Inventory_Item::getById( $inv_itemid );
			if ( $inv_item_orig ) {
				$new = false;
				$inv_item = Treat_Model_Inventory_Item::getByBusinessIdAndItemCode( $businessid, $item_code );
				if ( $inv_item ) {
					$inv_item = current( $inv_item );
				}
				else {
					$inv_item = new Treat_Model_Inventory_Item();
					$inv_item->rec_num   = null;
					$inv_item->rec_size  = null;
					$inv_item->rec_num2  = null;
					$inv_item->rec_size2 = null;
					$new = true;
				}
				
				$inv_item->rec_num   = $inv_item_orig->rec_num;
				$inv_item->rec_size  = $inv_item_orig->rec_size;
				$inv_item->rec_num2  = $inv_item_orig->rec_num2;
				$inv_item->rec_size2 = $inv_item_orig->rec_size2;
				$inv_item->save();
				
				if ( $new ) {
					$inv_item_id = Treat_Model_Inventory_Item::db()->lastInsertId();
				}
				else {
					$inv_item_id = $inv_item->inv_itemid;
				}
				
				$recipe_link[ $inv_itemid ] = $inv_item_id;
			}
		}
	}
	
?>
				<tr bgcolor="#E8E7E7">
					<td colspan="3">
						<font color="red">
							<strong>Missing Inventory Items Updated</strong>
						</font>
					</td>
				</tr>
<?php

	$mycounter2 = -1;
	# $key = $item_code
	# $value = $inv_itemid
	foreach ( $missing_inv_items as $inv_itemid => $item_code ) {
		$newvalue = $items_missing[ $item_code ];
		if ( $newvalue != $inv_itemid ) {
			$inv_item_orig = Treat_Model_Inventory_Item::getById( $inv_itemid );
			if ( $inv_item_orig ) {
				$inv_item = Treat_Model_Inventory_Item::getById( $newvalue );
				if ( !$inv_item ) {
					$inv_item = new Treat_Model_Inventory_Item();
					$inv_item->rec_num   = null;
					$inv_item->rec_size  = null;
					$inv_item->rec_num2  = null;
					$inv_item->rec_size2 = null;
				}
				
				$inv_item->rec_num   = $inv_item_orig->rec_num;
				$inv_item->rec_size  = $inv_item_orig->rec_size;
				$inv_item->rec_num2  = $inv_item_orig->rec_num2;
				$inv_item->rec_size2 = $inv_item_orig->rec_size2;
				$inv_item->save();

				$recipe_link[ $inv_itemid ] = $newvalue;
				# $key = $inv_itemid
				# $value = 1
				$update_rec_units[ $newvalue ] = 1;

				$fontcolor = 'blue';
			}
		}
		else {
			$inv_item = Treat_Model_Inventory_Item::getById( $inv_itemid );
			if ( $inv_item ) {
				$inv_item->inv_itemid = 0; // this should force a new item
				$inv_item->businessid = $businessid;
				$inv_item->companyid = $companyid;
				$inv_item->price_date = date( 'Y-m-d' );
				$inv_item->save();

				$recipe_link[ $inv_itemid ] = Treat_Model_Inventory_Item::db()->lastInsertId();

				$fontcolor = 'red';
			}
		}

		//echo "<tr><td><font color=red>$item_code</td><td colspan=2><font color=$fontcolor>$inv_itemid => $newvalue</font></td></tr>";
		$mycounter2--;
	}

?>
				<tr bgcolor="#E8E7E7">
					<td colspan="3">
						<strong>Items Missing SUPC`s Updated</strong>
					</td>
				</tr>
<?php

	$mycounter = 0;
	# $key = $inv_itemid
	# $value = $inv_itemid
	foreach ( $no_supc_items as $key => $inv_itemid ) {
		$newvalue = $items_no_supc[ $inv_itemid ];
		if ( $newvalue != $inv_itemid ) {

			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query = "SELECT * FROM inv_items WHERE inv_itemid = '$inv_itemid'";
			$result = Treat_DB_ProxyOld::query( $query, true );
			//mysql_close();

			$rec_num = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'rec_num' );
			$rec_size = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'rec_size' );
			$rec_num2 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'rec_num2' );
			$rec_size2 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'rec_size2' );

			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query = "UPDATE inv_items SET rec_num = '$rec_num', rec_size = '$rec_size', rec_num2 = '$rec_num2', rec_size2 = '$rec_size2' WHERE inv_itemid = '$newvalue'";
			$result = Treat_DB_ProxyOld::query( $query, true );
			//mysql_close();

			$recipe_link[ $inv_itemid ] = $newvalue;
			# $key = $inv_itemid
			# $value = 1
			$update_rec_units[ $newvalue ] = 1;

			$fontcolor="blue";
		}
		else {

			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query = "SELECT * FROM inv_items WHERE inv_itemid = '$inv_itemid'";
			$result = Treat_DB_ProxyOld::query( $query, true );
			//mysql_close();

			$item_name = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'item_name' );
			$item_code = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'item_code' );
			$units = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'units' );
			$price = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'price' );
			$apaccountid = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'apaccountid' );
			$vendor = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'vendor' );
			$rec_num = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'rec_num' );
			$rec_size = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'rec_size' );
			$rec_num2 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'rec_num2' );
			$rec_size2 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'rec_size2' );
			$order_size = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'order_size' );
			$each_size = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'each_size' );
			$pack_size = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'pack_size' );
			$pack_qty = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'pack_qty' );

			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query = "INSERT INTO inv_items (businessid,companyid,item_name,item_code,units,price,vendor,price_date,rec_num,rec_size,rec_num2,rec_size2,order_size,each_size,pack_size,pack_qty,active) VALUES ('$businessid','$companyid','$item_name','$item_code','$units','$price','$vendor','$today','$rec_num','$rec_size','$rec_num2','$rec_size2','$order_size','$each_size','$pack_size','$pack_qty','1')";
			$result = Treat_DB_ProxyOld::query( $query, true );
			$recipe_link[ $inv_itemid ] = Treat_DB_ProxyOld::mysql_insert_id();
			//mysql_close();

			$fontcolor="black";
		}
		//echo "<tr><td>$key</td><td colspan=2><font color=$fontcolor>$inv_itemid => $newvalue</td></tr>";

		$mycounter++;
	}

	# $key = $menu_itemid
	# $value = menu_item_id
	foreach ( $menu_item as $menu_itemid => $menu_item_id ) {
		$recipe_items = Treat_Model_Nutrition_RecipeItem::getByMenuItemId( $menu_itemid );
		if ( $recipe_items ) {
			foreach ( $recipe_items as $recipe ) {
				if (
					isset( $recipe_link[ $recipe->inv_itemid ] )
				) {
					$recipe->recipeid = 0;
					$recipe->menu_itemid = $menu_item_id;
					$recipe->inv_itemid = $recipe_link[ $recipe->inv_itemid ];
					$recipe->save();
				}
			}
		}
	}

?>
			</table>
			<p>&nbsp;</p>

		<form action="copybusrecipes3.php" method="post">
			<table width="90%" border="1">
				<tr>
					<td colspan="3" bgcolor="yellow">
						<input type="hidden" name="businessid" value="<?php echo $businessid; ?>" />
						<input type="hidden" name="companyid" value="<?php echo $companyid; ?>" />
						<strong>Update Recipe Units in Order Units</strong>
					</td>
				</tr>
<?php
	$counter = 0;
	# $key = $inv_itemid
	# $value = 1
	foreach ( $update_rec_units as $inv_itemid => $value ) {
	
		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query = sprintf( '
				SELECT
					ii.*
					,s.sizename   AS sizename1
					,s2.sizename  AS sizename2
				FROM inv_items ii
					LEFT JOIN size s
						ON ii.rec_size = s.sizeid
					LEFT JOIN size s2
						ON ii.rec_size2 = s2.sizeid
				WHERE ii.inv_itemid = %d
			'
			,$inv_itemid
		);
		$result = Treat_DB_ProxyOld::query( $query, true );
		//mysql_close();

		$inv_item = new Treat_ArrayObject( @Treat_DB_ProxyOld::mysql_fetch_array( $result ) );

		$varname = $counter . '-1';

		if ( $inv_item->pack_qty <= 1 ) {
			$inv_item->pack_size = $inv_item->units;
		}

?>
				<tr>
					<td>
						<?php echo $inv_item->item_code; ?>
					</td>
					<td>
						<?php echo $inv_item->item_name; ?>
					</td>
					<td bgcolor="yellow">
						<input type="text" size="3" name="inv_items[<?php
							echo $inv_item->inv_itemid;
						?>][rec_num]" value="<?php echo $inv_item->rec_num; ?>" />
						<?php echo $inv_item->sizename1; ?> in <?php echo $inv_item->pack_size; ?>
					</td>
				</tr>
<?php
		$counter++;
		if ( $inv_item->rec_size2 > 0 ) {
			$varname = $counter . '-2';
?>
				<tr>
					<td>
						<?php echo $inv_item->item_code; ?>
					</td>
					<td>
						<?php echo $inv_item->item_name; ?>
					</td>
					<td bgcolor="yellow">
						<input type="text" size="3" name="inv_items[<?php
							echo $inv_item->inv_itemid;
						?>][rec_num2]" value="<?php echo $inv_item->rec_num2; ?>" />
						<?php echo $inv_item->sizename2; ?> in <?php echo $inv_item->pack_size; ?>
					</td>
				</tr>
<?php
			$counter++;
		}
	}

	$update_rec_units = urlencode( serialize( $update_rec_units ) );

?>
				<tr>
					<td colspan="3">
						<input type="hidden" name="counter" value="<?php echo $counter; ?>" />
						<input type="hidden" name="update_rec_units" value="<?php echo $update_rec_units; ?>" />
						<input type="submit" value="Finish" onclick="return contconfirm()" />
					</td>
				</tr>
			</table>
		</form>
		<p>&nbsp;</p>
		</center>
<?php
}
?>
	</body>
</html>