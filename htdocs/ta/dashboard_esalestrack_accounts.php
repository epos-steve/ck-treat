<html>
<head>
<script type="text/javascript" src="prototype.js"></script>
<script src="sorttable.js"></script>

<SCRIPT LANGUAGE="JavaScript">
function hide_show_routes(supervisorid){
    var img = $('img'+supervisorid);
	var ex = img.src.split('/');
	var src = ex[ex.length-1];
	if(src == "exp.gif"){
		img.src="/ta/exp2.gif";
		img.alt="Show";
		$('routes'+supervisorid).hide();
	}
	else{
		img.src="/ta/exp.gif";
		img.alt="Hide";
		$('routes'+supervisorid).show();
	}
 }

 function hide_show_leads(which){
     if(which == 1){
         var rows = $$('.over_twenty');
		rows.each(function(row){
			row.hide();
		});

                $('under_total').show();
                $('over_total').hide();
     }
     else{
		var rows = $$('.over_twenty');
		rows.each(function(row){
			row.show();
		});

                $('under_total').hide();
                $('over_total').show();
     }
 }

 function hide_show_lost(which){
     if(which == 1){
         var rows = $$('.over_twenty_lost');
		rows.each(function(row){
			row.hide();
		});

                $('under_total_lost').show();
                $('over_total_lost').hide();
     }
     else{
		var rows = $$('.over_twenty_lost');
		rows.each(function(row){
			row.show();
		});

                $('under_total_lost').hide();
                $('over_total_lost').show();
     }
 }
 </script>
</head>
<?php

function money($diff){
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+4);
        if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
        elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
        $diff = substr($diff, 0, $dot+3);
        return $diff;
}

function nextday($nextd,$day_format=''){ //Function returns next day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monn = substr($nextd, 5, 2);
   $dayn = substr($nextd, 8, 2);
   $yearn = substr($nextd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monn, $dayn+1, $yearn));
   return $tempdate;
}

function prevday($prevd,$day_format=''){ //Function returns previous day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monp = substr($prevd, 5, 2);
   $dayp = substr($prevd, 8, 2);
   $yearp = substr($prevd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monp, $dayp-1, $yearp));
   return $tempdate;
}

function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}

define('DOC_ROOT', dirname(dirname(__FILE__)));
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');

$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";

$user = isset($_COOKIE["usercook"])?$_COOKIE["usercook"]:'';
$pass = isset($_COOKIE["passcook"])?$_COOKIE["passcook"]:'';
$owner = isset($_GET["owner"])?$_GET["owner"]:'';
$year = isset($_GET["year"])?$_GET["year"]:'';
$type = isset($_GET["type"])?$_GET["type"]:'';
$show = isset($_GET['show'])?$_GET['show']:'';

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query( $query );
$num=mysql_numrows($result);

if ($num!=0){
    $security_level=@mysql_result($result,0,"security_level");
    $bid=@mysql_result($result,0,"businessid");
    $cid=@mysql_result($result,0,"companyid");
    $loginid=@mysql_result($result,0,"loginid");
}

if ($num != 1 || $user == "" || $pass == "")
{
    echo "<center><p><h3>Failed</h3></center>";
}
else
{
	echo "<body>";
	if($show==1){
		$startdate="$year-01-01";
		$enddate="$year-12-31";

		$query = "SELECT * FROM est_accountowner WHERE accountowner = '$owner'";
		$result = Treat_DB_ProxyOld::query( $query );

		$name=@mysql_result($result,0,"name");
		$display_name=@mysql_result($result,0,"display_name");
		$link=@mysql_result($result,0,"link");

		if($display_name!=""){$name=$display_name;}

		if($link>0){$orquery=" OR accountowner = '$link'";}
		else{$orquery="";}

		echo "<b>$name - Business Gained $year</b>";
		echo "<table width=100% cellspacing=0 cellpadding=0 style=\"border:1px solid #cccccc;\" class=\"sortable\">";
		echo "<tr bgcolor=#E8E7E7><th style=\"border:1px solid #cccccc;\" width=16.66%><b>Opportunity</b></th><th style=\"border:1px solid #cccccc;\" width=16.66%><b>Account</b></th><th style=\"border:1px solid #cccccc;\" width=16.66%><b>Account Owner</b></th><th style=\"border:1px solid #cccccc;\" width=16.66%><b>Status</b></th><th style=\"border:1px solid #cccccc;\" width=16.66%><b>Agreement Date</b></th><th style=\"border:1px solid #cccccc;\" width=16.66%><b>Income</b></th></tr>";

		$query = "SELECT * FROM est_opportunity WHERE (accountowner = '$owner' $orquery) AND expecteddate >= '$startdate' AND expecteddate <= '$enddate' AND (status = '14' OR status = '11' OR status = '5' OR status = '7') ORDER BY expecteddate";
		$result = Treat_DB_ProxyOld::query( $query );

		$total=0;
                $count=1;
		while($r=mysql_fetch_array($result)){
			$systemid=$r["systemid"];
			$opname=$r["name"];
			$accountowner=$r["accountowner"];
			$customer=$r["customer"];
			$income1=$r["income1"];
			$install_date=$r["expecteddate"];
			$status=$r["status"];
			$_customer=$r["_customer"];

			$total+=$income1;
			$income1=number_format($income1,2);

			$query2 = "SELECT * FROM est_op_status WHERE status = '$status'";
			$result2 = Treat_DB_ProxyOld::query( $query2 );

			$status=@mysql_result($result2,0,"name");

			$query2 = "SELECT name FROM est_accountowner WHERE accountowner = '$accountowner'";
			$result2 = Treat_DB_ProxyOld::query( $query2 );

			$accountowner=@mysql_result($result2,0,"name");

			$query2 = "SELECT name FROM est_accounts WHERE systemid = '$_customer'";
			$result2 = Treat_DB_ProxyOld::query( $query2 );

			$customer=@mysql_result($result2,0,"name");

                        if($count<=20){$showclass="under_twenty";$showstyle="block";}
                        else{$showclass="over_twenty";$showstyle="none";}

			echo "<tr bgcolor=white onMouseOver=this.bgColor='yellow' onMouseOut=this.bgColor='white' class=$showclass style=\"display: $showstyle;\"><td style=\"border:1px solid #cccccc;\">$opname</td><td style=\"border:1px solid #cccccc;\">$customer</td><td style=\"border:1px solid #cccccc;\">$accountowner</td><td style=\"border:1px solid #cccccc;\">$status</td><td style=\"border:1px solid #cccccc;\">$install_date</td><td style=\"border:1px solid #cccccc;\" align=right>$income1</td></tr>";
			$count++;
		}
		$total=number_format($total,2);
		echo "</table>";
		echo "<table width=100%><tr><td align=right><b>Total: $total</b></td></tr></table>";
	}
	elseif($show==2){
		$startdate="$year-01-01";
		$enddate="$year-12-31";

		$query = "SELECT * FROM est_accountowner WHERE accountowner = '$owner'";
		$result = Treat_DB_ProxyOld::query( $query );

		$name=@mysql_result($result,0,"name");
		$display_name=@mysql_result($result,0,"display_name");
		$link=@mysql_result($result,0,"link");

		if($display_name!=""){$name=$display_name;}

		if($link>0){$orquery=" OR accountowner = '$link'";}
		else{$orquery="";}

		echo "<b>$name - Business Lost $year</b>";
		echo "<table width=100% cellspacing=0 cellpadding=0 style=\"border:1px solid #cccccc;\" class=\"sortable\">";
		echo "<tr bgcolor=#E8E7E7><th style=\"border:1px solid #cccccc;\" width=20%><b>Business</b></th><th style=\"border:1px solid #cccccc;\" width=20%><b>&nbsp;</b></th><th style=\"border:1px solid #cccccc;\" width=20%><b>Status</b></th><th style=\"border:1px solid #cccccc;\" width=20%><b>Date Lost</b></th><th style=\"border:1px solid #cccccc;\" width=20%><b>Income</b></th></tr>";

		$query = "SELECT * FROM est_accounts WHERE (accountowner = '$owner' $orquery) AND datelost >= '$startdate' AND datelost <= '$enddate' AND customertype = '4' AND status = '2' ORDER BY datelost";
		$result = Treat_DB_ProxyOld::query( $query );

		$total=0;
		while($r=mysql_fetch_array($result)){
			$systemid=$r["systemid"];
			$name=$r["name"];
			$yrlyrevenue=$r["yrlyrevenue"];
                        $cafeannualrevenue=$r["cafeannualrevenue"];
                        $ocsrevenue=$r["ocsrevenue"];
			$datelost=$r["datelost"];
			$customertype=$r["customertype"];
			$contract=$r["contract"];
			$contractinfofoodserv=$r["contractinfofoodserv"];
			$contractvend=$r["contractvend"];
			$contractinfovending=$r["contractinfovending"];

			$total+=$yrlyrevenue+$cafeannualrevenue+$ocsrevenue;
			$yrlyrevenue=number_format($yrlyrevenue+$cafeannualrevenue+$ocsrevenue,2);

			$query2 = "SELECT * FROM est_customertype WHERE customertype = '$customertype'";
			$result2 = Treat_DB_ProxyOld::query( $query2 );

			$status=@mysql_result($result2,0,"name");

			echo "<tr bgcolor=white onMouseOver=this.bgColor='yellow' onMouseOut=this.bgColor='white'><td style=\"border:1px solid #cccccc;\">$name</td><td style=\"border:1px solid #cccccc;\">&nbsp;</td><td style=\"border:1px solid #cccccc;\">$status</td><td style=\"border:1px solid #cccccc;\">$datelost</td><td style=\"border:1px solid #cccccc;\" align=right>$yrlyrevenue</td></tr>";

		}
		$total=number_format($total,2);
		echo "</table>";
		echo "<table width=100%><tr><td align=right><b>Total: $total</b></td></tr></table>";
	}
	elseif($show==3){

	}
	/////red list
	elseif($show==4){
		$all = isset($_GET["a"])?$_GET["a"]:'';
		$startdate="$year-01-01";
		$enddate="$year-12-31";

		if($all==1){
			$name="Company";
		}
		elseif($all==2){
			$name="Vending";
		}
		elseif($all==3){
			$name="Food Service";
		}
		else{
			$query = "SELECT name FROM est_accountowner WHERE accountowner = '$owner'";
			$result = Treat_DB_ProxyOld::query( $query );

			$name=@mysql_result($result,0,"name");
		}

		echo "<b>$name - Red List $year</b>";
		echo "<table width=100% cellspacing=0 cellpadding=0 style=\"border:1px solid #cccccc;\" class=\"sortable\">";
		echo "<tr bgcolor=#E8E7E7><th style=\"border:1px solid #cccccc;\" width=20%><b>Business</b></th><th style=\"border:1px solid #cccccc;\" width=20%><b>Account Owner</b></th><th style=\"border:1px solid #cccccc;\" width=20%><b>Status</b></th><th style=\"border:1px solid #cccccc;\" width=20%><b>Red List Date</b></th><th style=\"border:1px solid #cccccc;\" width=20%><b>Income</b></th></tr>";

		if($all==1){
			$query = "SELECT * FROM est_accounts WHERE customertype = '6' AND status = '2'";
			$result = Treat_DB_ProxyOld::query( $query );
		}
		elseif($all==2){
			$query = "SELECT * FROM est_accounts WHERE customertype = '6' AND status = '2' AND (yrlyrevenue > '0' OR ocsrevenue > '0')";
			$result = Treat_DB_ProxyOld::query( $query );
		}
		elseif($all==3){
			$query = "SELECT * FROM est_accounts WHERE customertype = '6' AND status = '2' AND cafeannualrevenue > '0'";
			$result = Treat_DB_ProxyOld::query( $query );
		}
		elseif($all==4){
			$query = "SELECT * FROM est_accounts WHERE accountowner = '$owner' AND customertype = '6' AND status = '2' AND (yrlyrevenue > '0' OR ocsrevenue > '0')";
			$result = Treat_DB_ProxyOld::query( $query );
		}
		elseif($all==5){
			$query = "SELECT * FROM est_accounts WHERE accountowner = '$owner' AND customertype = '6' AND status = '2' AND cafeannualrevenue > '0'";
			$result = Treat_DB_ProxyOld::query( $query );
		}

		$total=0;
		while($r=mysql_fetch_array($result)){
			$systemid=$r["systemid"];
			$name=$r["name"];
			$accountowner=$r["accountowner"];
			$yrlyrevenue=$r["yrlyrevenue"];
			$cafeannualrevenue=$r["cafeannualrevenue"];
			$ocsrevenue=$r["ocsrevenue"];
			$datelost=$r["redlistdate"];
			$customertype=$r["customertype"];
			$note=$r["notes"];
			$note=$r["notes"];
			$contract=$r["contract"];
			$contractinfofoodserv=$r["contractinfofoodserv"];
			$contractvend=$r["contractvend"];
			$contractinfovending=$r["contractinfovending"];

			if($all==1){
				$total+=$yrlyrevenue+$cafeannualrevenue+$ocsrevenue;
				$yrlyrevenue=number_format($yrlyrevenue+$cafeannualrevenue+$ocsrevenue,2);
			}
			elseif($all==2||$all==4){
				$total+=$yrlyrevenue+$ocsrevenue;
				$yrlyrevenue=number_format($yrlyrevenue+$ocsrevenue,2);
			}
			elseif($all==3||$all==5){
				$total+=$cafeannualrevenue;
				$yrlyrevenue=number_format($cafeannualrevenue,2);
			}

			$query2 = "SELECT name FROM est_customertype WHERE customertype = '$customertype'";
			$result2 = Treat_DB_ProxyOld::query( $query2 );

			$status=@mysql_result($result2,0,"name");

			$query2 = "SELECT name FROM est_accountowner WHERE accountowner = '$accountowner'";
			$result2 = Treat_DB_ProxyOld::query( $query2 );

			$accountowner=@mysql_result($result2,0,"name");

			if($note!=""||$contract!=""||$contractvend!=""){$shownote="<a href='javascript:void();' onclick=\"hide_show_routes($systemid);\"><img id='img$systemid' src=exp2.gif height=16 width=16 border=0 alt='View Notes'></a>";}
			else{$shownote="";}

			echo "<tr bgcolor=white onMouseOver=this.bgColor='yellow' onMouseOut=this.bgColor='white' valign=top><td style=\"border:1px solid #cccccc;\">$name</td><td style=\"border:1px solid #cccccc;\">$accountowner</td><td style=\"border:1px solid #cccccc;\">$status $shownote<br>";
			echo "<div id=routes$systemid style=\"display:none;\">";
			if($contract!=""){
				echo "<font size=2><b><u>Foodservice Contract</u>: $contract";
				if($contractinfofoodserv!=""){
					echo "<br>$contractinfofoodserv";
				}
				echo "</b></font>";
			}
			if($contractvend!=""){
				if($contract!=""){echo "<br>";}
				echo "<font size=2><b><u>Vending Contract</u>: $contractvend";
				if($contractinfovending!=""){
					echo "<br>$contractinfovending";
				}
				echo "</b></font><br>";
			}
			elseif($contract!=""){echo "<br>";}

			echo "<font size=2>$note</font>";
			echo "</div>";
			echo "</td><td style=\"border:1px solid #cccccc;\">$datelost</td><td style=\"border:1px solid #cccccc;\" align=right>$yrlyrevenue</td></tr>";

		}
		$total=number_format($total,2);
		echo "</table>";
		echo "<table width=100%><tr><td align=right><b>Total: $total</b></td></tr></table>";
	}
	elseif($show==5){
		$startdate="$year-01-01";
		$enddate="$year-12-31";

		echo "<b>Leads Won - $year</b> <a href=\"javascript:void(0);\" onclick=\"hide_show_leads(0);\" style=\"text-decoration:none;\"><font size=1 color=blue>[SHOW ALL]</font></a> <a href=\"javascript:void(0);\" onclick=\"hide_show_leads(1);\" style=\"text-decoration:none;\"><font size=1 color=blue>[LAST 20]</font></a>";
		echo "<table width=100% cellspacing=0 cellpadding=0 style=\"border:1px solid #cccccc;\" class=\"sortable\">";
		echo "<tr bgcolor=#E8E7E7><th style=\"border:1px solid #cccccc;\" width=16.66%><b>Opportunity</b></th><th style=\"border:1px solid #cccccc;\" width=16.66%><b>Account</b></th><th style=\"border:1px solid #cccccc;\" width=16.66%><b>Account Owner</b></th><th style=\"border:1px solid #cccccc;\" width=16.66%><b>Status</b></th><th style=\"border:1px solid #cccccc;\" width=16.66%><b>Agreement Date</b></th><th style=\"border:1px solid #cccccc;\" width=16.66%><b>Income</b></th></tr>";

		if($type!=""){
			$query = "SELECT * FROM est_accountowner,est_accountowner_budget,est_opportunity WHERE est_accountowner.type = '$type' AND est_accountowner.accountowner = est_accountowner_budget.accountowner AND est_accountowner_budget.date = '$startdate' AND est_accountowner_budget.gained > '0' AND est_accountowner.accountowner = est_opportunity.accountowner AND est_opportunity.expecteddate >= '$startdate' AND est_opportunity.expecteddate <= '$enddate' AND est_opportunity.income1 > '0' AND (est_opportunity.status = '14' OR est_opportunity.status = '11' OR est_opportunity.status = '5' OR est_opportunity.status = '7') ORDER BY est_opportunity.expecteddate DESC";
			$result = Treat_DB_ProxyOld::query( $query );
		}
		else{
			$query = "SELECT * FROM est_accountowner,est_accountowner_budget,est_opportunity WHERE est_accountowner.accountowner = est_accountowner_budget.accountowner AND est_accountowner_budget.date = '$startdate' AND est_accountowner_budget.gained > '0' AND est_accountowner.accountowner = est_opportunity.accountowner AND est_opportunity.expecteddate >= '$startdate' AND est_opportunity.expecteddate <= '$enddate' AND est_opportunity.income1 > '0' AND (est_opportunity.status = '14' OR est_opportunity.status = '11' OR est_opportunity.status = '5' OR est_opportunity.status = '7') ORDER BY est_opportunity.expecteddate DESC";
			$result = Treat_DB_ProxyOld::query( $query );
		}

		$total=0;
                $count=1;
                $under_total=0;
		while($r=mysql_fetch_array($result)){
			$systemid=$r["systemid"];
			$opname=$r["name"];
			$customer=$r["customer"];
			$income1=$r["income1"];
			$accountowner=$r["accountowner"];
			$install_date=$r["expecteddate"];
			$status=$r["status"];
			$note=$r["note"];
			$_customer = $r["_customer"];

			if($note!=""){$shownote="<a href='javascript:void();' onclick=\"hide_show_routes($systemid);\"><img id='img$systemid' src=exp2.gif height=16 width=16 border=0 alt='View Notes'></a>";}
			else{$shownote="";}

                        if($count<=20){$showclass="under_twenty";$showstyle="";$under_total+=$income1;}
                        else{$showclass="over_twenty";$showstyle="none";}

			$total+=$income1;
			$income1=number_format($income1,2);

			$query2 = "SELECT name FROM est_op_status WHERE status = '$status'";
			$result2 = Treat_DB_ProxyOld::query( $query2 );

			$status=@mysql_result($result2,0,"name");

			$query2 = "SELECT name FROM est_accountowner WHERE accountowner = '$accountowner'";
			$result2 = Treat_DB_ProxyOld::query( $query2 );

			$name=@mysql_result($result2,0,"name");

			$query2 = "SELECT name FROM est_accounts WHERE systemid = '$_customer'";
			$result2 = Treat_DB_ProxyOld::query( $query2 );

			$customer=@mysql_result($result2,0,"name");

			echo "<tr style=\"display: $showstyle;\" bgcolor=white onMouseOver=this.bgColor='yellow' onMouseOut=this.bgColor='white' class=\"$showclass\" valign=top><td style=\"border:1px solid #cccccc;\">$opname</td><td style=\"border:1px solid #cccccc;\">$customer</td><td style=\"border:1px solid #cccccc;\">$name</td><td style=\"border:1px solid #cccccc;\">$status $shownote<br>";
			echo "<div id=routes$systemid style=\"display:none;\">";
			echo "<font size=2>$note</font>";
			echo "</div>";
			echo "</td><td style=\"border:1px solid #cccccc;\">$install_date</td><td style=\"border:1px solid #cccccc;\" align=right>$income1</td></tr>";

                        $count++;
		}
		$total=number_format($total,2);
                $under_total=number_format($under_total,2);
		echo "</table>";
                echo "<table width=100% id='under_total'><tr><td align=right><b>Total: $under_total</b></td></tr></table><p>";
		echo "<table width=100% id='over_total' style=\"display: none;\"><tr><td align=right><b>Total: $total</b></td></tr></table><p>";

		//////lost leads
		echo "<b>Leads Lost - $year</b> <a href=\"javascript:void(0);\" onclick=\"hide_show_lost(0);\" style=\"text-decoration:none;\"><font size=1 color=blue>[SHOW ALL]</font></a> <a href=\"javascript:void(0);\" onclick=\"hide_show_lost(1);\" style=\"text-decoration:none;\"><font size=1 color=blue>[LAST 20]</font></a>";
		echo "<table width=100% cellspacing=0 cellpadding=0 style=\"border:1px solid #cccccc;\" class=\"sortable\">";
		echo "<tr bgcolor=#E8E7E7><th style=\"border:1px solid #cccccc;\" width=16.66%><b>Opportunity</b></th><th style=\"border:1px solid #cccccc;\" width=16.66%><b>Account</b></th><th style=\"border:1px solid #cccccc;\" width=16.66%><b>Account Owner</b></th><th style=\"border:1px solid #cccccc;\" width=16.66%><b>Status</b></th><th style=\"border:1px solid #cccccc;\" width=16.66%><b>Agreement Date</b></th><th style=\"border:1px solid #cccccc;\" width=16.66%><b>Income</b></th></tr>";

		if($type!=""){
			$query = "SELECT * FROM est_accountowner,est_opportunity WHERE est_accountowner.type = '$type' AND est_accountowner.accountowner = est_opportunity.accountowner AND est_opportunity.expecteddate >= '$startdate' AND est_opportunity.expecteddate <= '$enddate' AND est_opportunity.income1 > '0' AND (est_opportunity.status = '15' OR est_opportunity.status = '6') ORDER BY est_opportunity.expecteddate DESC";
			$result = Treat_DB_ProxyOld::query( $query );
		}
		else{
			$query = "SELECT * FROM est_opportunity WHERE expecteddate >= '$startdate' AND expecteddate <= '$enddate' AND (status = '15' OR status = '6') ORDER BY expecteddate DESC";
			$result = Treat_DB_ProxyOld::query( $query );
		}

		$total=0;
                $under_total=0;
                $count=1;
		while($r=mysql_fetch_array($result)){
			$systemid=$r["systemid"];
			$opname=$r["name"];
			$customer=$r["customer"];
			$accountowner=$r["accountowner"];
			$income1=$r["income1"];
			$install_date=$r["expecteddate"];
			$status=$r["status"];
			$note=$r["note"];
			$lost_reason=$r["lost_reason"];
			$other_lost_reason=$r["other_lost_reason"];
			$_customer = $r["_customer"];

			if($note!=""||$lost_reason!=""){$shownote="<a href='javascript:void();' onclick=\"hide_show_routes($systemid);\"><img id='img$systemid' src=exp2.gif height=16 width=16 border=0 alt='View Notes'></a>";}
			else{$shownote="";}

                        if($count<=20){$showclass="under_twenty_lost";$showstyle="";$under_total+=$income1;}
                        else{$showclass="over_twenty_lost";$showstyle="none";}

			$total+=$income1;
			$income1=number_format($income1,2);

			$query2 = "SELECT name FROM est_op_status WHERE status = '$status'";
			$result2 = Treat_DB_ProxyOld::query( $query2 );

			$status=@mysql_result($result2,0,"name");

			$query2 = "SELECT name FROM est_accountowner WHERE accountowner = '$accountowner'";
			$result2 = Treat_DB_ProxyOld::query( $query2 );

			$name=@mysql_result($result2,0,"name");

			$query2 = "SELECT name FROM est_accounts WHERE systemid = '$_customer'";
			$result2 = Treat_DB_ProxyOld::query( $query2 );

			$customer=@mysql_result($result2,0,"name");

			echo "<tr class=\"$showclass\" style=\"display: $showstyle;\" bgcolor=white onMouseOver=this.bgColor='yellow' onMouseOut=this.bgColor='white' valign=top><td style=\"border:1px solid #cccccc;\">$opname</td><td style=\"border:1px solid #cccccc;\">$customer</td><td style=\"border:1px solid #cccccc;\">$name</td><td style=\"border:1px solid #cccccc;\">$status $shownote<br>";
			echo "<div id=routes$systemid style=\"display:none;\">";
			if($lost_reason!=""){
				echo "<font size=2><b>$lost_reason";
				if($other_lost_reason!=""){
					echo ": $other_lost_reason";
				}
				echo "</b></font><br>";
			}
			echo "<font size=2>$note</font>";
			echo "</div>";
			echo "</td><td style=\"border:1px solid #cccccc;\">$install_date</td><td style=\"border:1px solid #cccccc;\" align=right>$income1</td></tr>";

                        $count++;
		}
		$total=number_format($total,2);
                $under_total=number_format($under_total,2);
		echo "</table>";
		echo "<table width=100% id='under_total_lost'><tr><td align=right><b>Total: $under_total</b></td></tr></table>";
                echo "<table width=100% id='over_total_lost' style=\"display: none;\"><tr><td align=right><b>Total: $total</b></td></tr></table>";
	}
	//////lost business
	elseif($show==6){
		$startdate="$year-01-01";
		$enddate="$year-12-31";

		echo "<b>Business Lost - $year</b> <a href=\"javascript:void(0);\" onclick=\"hide_show_lost(0);\" style=\"text-decoration:none;\"><font size=1 color=blue>[SHOW ALL]</font></a> <a href=\"javascript:void(0);\" onclick=\"hide_show_lost(1);\" style=\"text-decoration:none;\"><font size=1 color=blue>[LAST 20]</font></a>";
		echo "<table width=100% cellspacing=0 cellpadding=0 style=\"border:1px solid #cccccc;\" class=\"sortable\">";
		echo "<tr bgcolor=#E8E7E7><th style=\"border:1px solid #cccccc;\" width=20%><b>Business</b></th><th style=\"border:1px solid #cccccc;\" width=20%><b>Account Owner</b></th><th style=\"border:1px solid #cccccc;\" width=20%><b>Status</b></th><th style=\"border:1px solid #cccccc;\" width=20%><b>Date Lost</b></th><th style=\"border:1px solid #cccccc;\" width=20%><b>Income</b></th></tr>";

		if($type!=""){
			$query = "SELECT * FROM est_accountowner,est_accounts WHERE est_accountowner.type = '$type' AND est_accountowner.accountowner = est_accounts.accountowner AND est_accounts.datelost >= '$startdate' AND est_accounts.datelost <= '$enddate' AND est_accounts.customertype = '4' AND est_accounts.status = '2' ORDER BY est_accounts.datelost DESC";
			$result = Treat_DB_ProxyOld::query( $query );
		}
		else{
			$query = "SELECT * FROM est_accounts WHERE datelost >= '$startdate' AND datelost <= '$enddate' AND customertype = '4' AND status = '2' ORDER BY datelost DESC";
			$result = Treat_DB_ProxyOld::query( $query );
		}

		$total=0;
                $count=1;
                $under_total=0;
		while($r=mysql_fetch_array($result)){
			$systemid=$r["systemid"];
			$name=$r["name"];
			$income1=$r["yrlyrevenue"];
                        $cafeannualrevenue=$r["cafeannualrevenue"];
                        $ocsrevenue=$r["ocsrevenue"];
			$datelost=$r["datelost"];
			$customertype=$r["customertype"];
			$accountowner=$r["accountowner"];
			$note=$r["notes"];
			$contract=$r["contract"];
			$contractinfofoodserv=$r["contractinfofoodserv"];
			$contractvend=$r["contractvend"];
			$contractinfovending=$r["contractinfovending"];

			if($note!=""||$contract!=""||$contractvend!=""){$shownote="<a href='javascript:void();' onclick=\"hide_show_routes($systemid);\"><img id='img$systemid' src=exp2.gif height=16 width=16 border=0 alt='View Notes'></a>";}
			else{$shownote="";}

                        if($count<=20){$showclass="under_twenty_lost";$showstyle="";
                            if($type==""){$under_total+=$income1+$cafeannualrevenue+$ocsrevenue;}
                            elseif($type==0){$under_total+=$income1+$ocsrevenue;}
                            else{$under_total+=$cafeannualrevenue;}

                         }
                        else{$showclass="over_twenty_lost";$showstyle="none";}

                        if($type==""){
                            $total+=$income1+$cafeannualrevenue+$ocsrevenue;
                            $income1=number_format($income1+$cafeannualrevenue+$ocsrevenue,2);
                        }
                        elseif($type==1){
                            $total+=$cafeannualrevenue;
                            $income1=number_format($cafeannualrevenue,2);
                        }
                        else{
                            $total+=$income1+$ocsrevenue;
                            $income1=number_format($income1+$ocsrevenue,2);
                        }

			$query2 = "SELECT * FROM est_customertype WHERE customertype = '$customertype'";
			$result2 = Treat_DB_ProxyOld::query( $query2 );

			$status=@mysql_result($result2,0,"name");

			$query2 = "SELECT name FROM est_accountowner WHERE accountowner = '$accountowner'";
			$result2 = Treat_DB_ProxyOld::query( $query2 );

			$accountowner=@mysql_result($result2,0,"name");

			echo "<tr class=\"$showclass\" style=\"display: $showstyle;\" bgcolor=white onMouseOver=this.bgColor='yellow' onMouseOut=this.bgColor='white' valign=top><td style=\"border:1px solid #cccccc;\">$name</td><td style=\"border:1px solid #cccccc;\">$accountowner</td><td style=\"border:1px solid #cccccc;\">$status $shownote<br>";
			echo "<div id=routes$systemid style=\"display:none;\">";
			if($contract!=""){
				echo "<font size=2><b><u>Foodservice Contract</u>: $contract";
				if($contractinfofoodserv!=""){
					echo "<br>$contractinfofoodserv";
				}
				echo "</b></font>";
			}
			if($contractvend!=""){
				if($contract!=""){echo "<br>";}
				echo "<font size=2><b><u>Vending Contract</u>: $contractvend";
				if($contractinfovending!=""){
					echo "<br>$contractinfovending";
				}
				echo "</b></font><br>";
			}
			elseif($contract!=""){echo "<br>";}

			echo "<font size=2>$note</font>";
			echo "</div>";
			echo "</td><td style=\"border:1px solid #cccccc;\">$datelost</td><td style=\"border:1px solid #cccccc;\" align=right>$income1</td></tr>";

                        $count++;
		}
		$total=number_format($total,2);
                $under_total=number_format($under_total,2);
		echo "</table>";
		echo "<table width=100% id='under_total_lost'><tr><td align=right><b>Total: $under_total</b></td></tr></table>";
                echo "<table width=100% id='over_total_lost' style=\"display: none;\"><tr><td align=right><b>Total: $total</b></td></tr></table>";

	}
	/////pipe line
	elseif($show==7){
		$status_array = array(1 => 13, 2 => 12, 3 => 10, 4 => 2);

		$all = isset($_GET["a"])?$_GET["a"]:'';
		$month = isset($_GET["m"])?$_GET["m"]:'';
		if(strlen($month)<2){$month="0$month";}
		$startdate="$year-$month-01";
		$enddate="$year-$month-31";

		if($all==-1){
			$name="Company";
		}
		elseif($all!=""){
			$query = "SELECT name FROM est_accountowner_type WHERE id = '$all'";
			$result = Treat_DB_ProxyOld::query( $query );

			$name=@mysql_result($result,0,"name");
		}
		else{
			$query = "SELECT name,display_name,link FROM est_accountowner WHERE accountowner = '$owner'";
			$result = Treat_DB_ProxyOld::query( $query );

			$name=@mysql_result($result,0,"name");
			$display_name=@mysql_result($result,0,"display_name");
			$link=@mysql_result($result,0,"link");

			if($display_name!=""){$name=$display_name;}
		}

		if($month==1){$monthname="January";}
		elseif($month==2){$monthname="February";}
		elseif($month==3){$monthname="March";}
		elseif($month==4){$monthname="April";}
		elseif($month==5){$monthname="May";}
		elseif($month==6){$monthname="June";}
		elseif($month==7){$monthname="July";}
		elseif($month==8){$monthname="August";}
		elseif($month==9){$monthname="September";}
		elseif($month==10){$monthname="October";}
		elseif($month==11){$monthname="November";}
		elseif($month==12){$monthname="December";}

		echo "<b>$name - Pipeline $monthname $year</b>";

		//////break out each status
		foreach($status_array AS $key => $value){
		if($all==-1){
			$query="SELECT est_opportunity.* FROM est_opportunity,est_accountowner,est_accountowner_budget WHERE (est_opportunity.status = '$value') AND est_opportunity.accountowner = est_accountowner.accountowner AND est_accountowner.accountowner = est_accountowner_budget.accountowner AND est_accountowner_budget.accountowner AND est_accountowner_budget.date = '$startdate' AND est_accountowner_budget.gained > '0' ORDER BY est_opportunity.status,est_opportunity.expecteddate";
			$result = Treat_DB_ProxyOld::query( $query );
		}
		elseif($all!=""){
			$query="SELECT est_opportunity.* FROM est_opportunity,est_accountowner,est_accountowner_budget WHERE (est_opportunity.status = '$value') AND est_opportunity.accountowner = est_accountowner.accountowner AND est_accountowner.type = '$all' AND est_accountowner.accountowner = est_accountowner_budget.accountowner AND est_accountowner_budget.accountowner AND est_accountowner_budget.date = '$startdate' AND est_accountowner_budget.gained > '0' ORDER BY est_opportunity.status,est_opportunity.expecteddate";
			$result = Treat_DB_ProxyOld::query( $query );
		}
		else{
			if($link>0){$orquery=" OR accountowner = '$link'";}
			else{$orquery="";}

			$query="SELECT * FROM est_opportunity WHERE (accountowner = '$owner' $orquery) AND (status = '$value') ORDER BY status,expecteddate";
			$result = Treat_DB_ProxyOld::query( $query );
		}

		echo "<table width=100% cellspacing=0 cellpadding=0 style=\"border:1px solid #cccccc;\" class=\"sortable\">";
		echo "<tr bgcolor=#E8E7E7><th style=\"border:1px solid #cccccc;\" width=14.28%><b>Opportunity</b></th><th style=\"border:1px solid #cccccc;\" width=14.28%><b>Account</b></th><th style=\"border:1px solid #cccccc;\" width=14.28%><b>Contact</b></th><th style=\"border:1px solid #cccccc;\" width=14.28%><b>Account Owner</b></th><th style=\"border:1px solid #cccccc;\" width=14.28%><b>Status</b></th><th style=\"border:1px solid #cccccc;\" width=14.28%><b>Agreement Date</b></th><th style=\"border:1px solid #cccccc;\" width=14.28%><b>Income</b></th></tr>";

		$total=0;
		while($r=mysql_fetch_array($result)){
			$systemid=$r["systemid"];
			$opname=$r["name"];
			$customer=$r["customer"];
			$contact=$r["contact"];
			$income1=$r["income1"];
			$install_date=$r["expecteddate"];
			$status=$r["status"];
			$opowner=$r["accountowner"];
			$note=$r["note"];
			$_customer=$r["_customer"];

			$total+=$income1;
			$income1=number_format($income1,2);

			$query2 = "SELECT name FROM est_accountowner WHERE accountowner = '$opowner'";
			$result2 = Treat_DB_ProxyOld::query( $query2 );

			$ownername=@mysql_result($result2,0,"name");

			$query2 = "SELECT * FROM est_op_status WHERE status = '$status'";
			$result2 = Treat_DB_ProxyOld::query( $query2 );

			$status=@mysql_result($result2,0,"name");

			$query2 = "SELECT name FROM est_accounts WHERE systemid = '$_customer'";
			$result2 = Treat_DB_ProxyOld::query( $query2 );

			$customer=@mysql_result($result2,0,"name");

			if($note!=""){$shownote="<a href='javascript:void();' onclick=\"hide_show_routes($systemid);\"><img id='img$systemid' src=exp2.gif height=16 width=16 border=0 alt='View Notes'></a>";}
			else{$shownote="";}

			echo "<tr valign=top bgcolor=white onMouseOver=this.bgColor='yellow' onMouseOut=this.bgColor='white'><td style=\"border:1px solid #cccccc;\">$opname</td><td style=\"border:1px solid #cccccc;\">$customer</td><td style=\"border:1px solid #cccccc;\">$contact</td><td style=\"border:1px solid #cccccc;\">$ownername</td><td style=\"border:1px solid #cccccc;\">$status $shownote<br>";
			echo "<div id=routes$systemid style=\"display:none;\">";
			echo "<font size=2>$note</font>";
			echo "</div>";
			echo "</td><td style=\"border:1px solid #cccccc;\">$install_date</td><td style=\"border:1px solid #cccccc;\" align=right>$income1</td></tr>";
		}
		$total=number_format($total,2);
		echo "</table>";
		echo "<table width=100%><tr><td align=right><b>Total: $total</b></td></tr></table><p>";
		////end array loop
		}
	}
	elseif($show==8){
		$startdate="$year-01-01";
		$enddate="$year-12-31";

		echo "<b>Business Lost - Unknown Date</b>";
		echo "<table width=100% cellspacing=0 cellpadding=0 style=\"border:1px solid #cccccc;\" class=\"sortable\">";
		echo "<tr bgcolor=#E8E7E7><th style=\"border:1px solid #cccccc;\" width=20%><b>Business</b></th><th style=\"border:1px solid #cccccc;\" width=20%><b>Account Owner</b></th><th style=\"border:1px solid #cccccc;\" width=20%><b>Status</b></th><th style=\"border:1px solid #cccccc;\" width=20%><b>Date Lost</b></th><th style=\"border:1px solid #cccccc;\" width=20%><b>Income</b></th></tr>";

		$query = "SELECT * FROM est_accounts WHERE datelost = '0000-00-00' AND customertype = '4' AND status = '2' ORDER BY datelost DESC LIMIT 0,20";
		$result = Treat_DB_ProxyOld::query( $query );

		$total=0;
		while($r=mysql_fetch_array($result)){
			$systemid=$r["systemid"];
			$name=$r["name"];
			$income1=$r["yrlyrevenue"];
			$datelost=$r["datelost"];
			$customertype=$r["customertype"];
			$accountowner=$r["accountowner"];
			$note=$r["notes"];

			if($note!=""){$shownote="<a href='javascript:void();' onclick=\"hide_show_routes($systemid);\"><img id='img$systemid' src=exp2.gif height=16 width=16 border=0 alt='View Notes'></a>";}
			else{$shownote="";}

			$total+=$income1;
			$income1=number_format($income1,2);

			$query2 = "SELECT * FROM est_customertype WHERE customertype = '$customertype'";
			$result2 = Treat_DB_ProxyOld::query( $query2 );

			$status=@mysql_result($result2,0,"name");

			$query2 = "SELECT name FROM est_accountowner WHERE accountowner = '$accountowner'";
			$result2 = Treat_DB_ProxyOld::query( $query2 );

			$accountowner=@mysql_result($result2,0,"name");

			echo "<tr bgcolor=white onMouseOver=this.bgColor='yellow' onMouseOut=this.bgColor='white' valign=top><td style=\"border:1px solid #cccccc;\">$name</td><td style=\"border:1px solid #cccccc;\">$accountowner</td><td style=\"border:1px solid #cccccc;\">$status $shownote<br>";
			echo "<div id=routes$systemid style=\"display:none;\">";
			echo "<font size=2>$note</font>";
			echo "</div>";
			echo "</td><td style=\"border:1px solid #cccccc;\">$datelost</td><td style=\"border:1px solid #cccccc;\" align=right>$income1</td></tr>";

		}
		$total=number_format($total,2);
		echo "</table>";
		echo "<table width=100%><tr><td align=right><b>Total: $total</b></td></tr></table><p>";

	}
	echo "</body>";
}
//mysql_close();
?>
</html>