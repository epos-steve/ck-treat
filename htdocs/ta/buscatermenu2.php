<?php

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+3);
        if ($diff > 0 && $diff < '0.01'){$diff="0.01";}
        elseif($diff < 0 && $diff > '-0.01'){$diff="-0.01";}
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      if ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='32')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='08' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      $day=$day-31;  
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";} 
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

//include("db.php");
define('DOC_ROOT', dirname(dirname(__FILE__)));
require_once(DOC_ROOT.'/bootstrap.php');
$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");
$creditamount="creditamount";
$creditidnty="creditidnty";
$creditdate="creditdate";
$debitamount="debitamount";
$debitidnty="debitidnty";
$debitdate="debitdate";
$quatertotal=0;
$monthtotal=0;
$total=0;
$counter=0;
$findme='.';
$double='.00';
$single='0';
$double2='00';

/*$user=$_COOKIE["usercook"];
$pass=$_COOKIE["passcook"];
$view=$_COOKIE["viewcook"];
$date1=$_COOKIE["date1cook"];
$date2=$_COOKIE["date2cook"];
$businessid=$_GET["bid"];*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook');
$view = \EE\Controller\Base::getSessionCookieVariable('viewcook');
$date1 = \EE\Controller\Base::getSessionCookieVariable('date1cook');
$date2 = \EE\Controller\Base::getSessionCookieVariable('date2cook');
$businessid = \EE\Controller\Base::getGetVariable('bid');

setcookie("caterbus",$businessid);

/*$companyid=$_GET["cid"];
$view1=$_GET["view1"];
$itemeditid=$_GET["mid"];
$copyitemid2=$_GET["copyitemid"];
$curmenu=$_COOKIE["curmenu"];
$finditem=$_POST["finditem"];
$findnext=$_POST["findnext"];
$lastfind=$_POST["lastfind"];*/

$companyid = \EE\Controller\Base::getGetVariable('cid');
$view1 = \EE\Controller\Base::getGetVariable('view1');
$itemeditid = \EE\Controller\Base::getGetVariable('mid');
$copyitemid2 = \EE\Controller\Base::getGetVariable('copyitemid');
$curmenu = \EE\Controller\Base::getSessionCookieVariable('curmenu');
$finditem = \EE\Controller\Base::getPostVariable('finditem');
$findnext = \EE\Controller\Base::getPostVariable('findnext');
$lastfind = \EE\Controller\Base::getPostVariable('lastfind');

if($findnext==""){$findnext=0;}

/*mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");*/

if ($finditem!=""){

   if($lastfind!=$finditem){$findnext=0;}
   if($lastfind==$finditem){$findnext++;}
  
   //mysql_connect($dbhost,$username,$password);
   //@mysql_select_db($database) or die( "Unable to select database");
   $query = "SELECT menu_item_id FROM menu_items WHERE menu_item_id = '$finditem' AND businessid = '$businessid'";
   $result = Treat_DB_ProxyOld::query($query);
   $num=Treat_DB_ProxyOld::mysql_numrows($result);
   //mysql_close();

   $itemeditid=Treat_DB_ProxyOld::mysql_result($result,$findnext,"menu_item_id");

   if($itemeditid>0){$view1="edit";}
   else{$itemeditid="";}

   //echo "<font size=1>$finditem,$lastfind,$findnext</font>";
}

if ($businessid==""&&$companyid==""){
   /*$businessid=$_POST["businessid"];
   $companyid=$_POST["companyid"];
   $view1=$_POST["view1"];
   $date1=$_POST["date1"];
   $date2=$_POST["date2"];
   $findinv=$_POST["findinv"];*/
	
	$businessid = \EE\Controller\Base::getPostVariable('businessid');
	$companyid = \EE\Controller\Base::getPostVariable('companyid');
	$view1 = \EE\Controller\Base::getPostVariable('view1');
	$date1 = \EE\Controller\Base::getPostVariable('date1');
	$date2 = \EE\Controller\Base::getPostVariable('date2');
	$findinv = \EE\Controller\Base::getPostVariable('findinv');
}

if ($date1==""&&$date2==""){
   $previous=$today;
   for ($count=1;$count<=31;$count++){$previous=prevday($previous);}
   $date1=$previous;$date2=$today;$view='All';
}

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=Treat_DB_ProxyOld::mysql_numrows($result);
//mysql_close();

$security_level=Treat_DB_ProxyOld::mysql_result($result,0,"security_level");
$mysecurity=Treat_DB_ProxyOld::mysql_result($result,0,"security");
$bid=Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
$cid=Treat_DB_ProxyOld::mysql_result($result,0,"companyid");
$bid2=Treat_DB_ProxyOld::mysql_result($result,0,"busid2");
$bid3=Treat_DB_ProxyOld::mysql_result($result,0,"busid3");
$bid4=Treat_DB_ProxyOld::mysql_result($result,0,"busid4");
$bid5=Treat_DB_ProxyOld::mysql_result($result,0,"busid5");
$bid6=Treat_DB_ProxyOld::mysql_result($result,0,"busid6");
$bid7=Treat_DB_ProxyOld::mysql_result($result,0,"busid7");
$bid8=Treat_DB_ProxyOld::mysql_result($result,0,"busid8");
$bid9=Treat_DB_ProxyOld::mysql_result($result,0,"busid9");
$bid10=Treat_DB_ProxyOld::mysql_result($result,0,"busid10");
$loginid=Treat_DB_ProxyOld::mysql_result($result,0,"loginid");

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM business WHERE companyid = '$companyid' AND businessid = '$businessid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $businessname=Treat_DB_ProxyOld::mysql_result($result,0,"businessname");
    $businessid=Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
    $street=Treat_DB_ProxyOld::mysql_result($result,0,"street");
    $city=Treat_DB_ProxyOld::mysql_result($result,0,"city");
    $state=Treat_DB_ProxyOld::mysql_result($result,0,"state");
    $zip=Treat_DB_ProxyOld::mysql_result($result,0,"zip");
    $phone=Treat_DB_ProxyOld::mysql_result($result,0,"phone");
    $contract=Treat_DB_ProxyOld::mysql_result($result,0,"contract");
    $bus_unit=Treat_DB_ProxyOld::mysql_result($result,0,"unit");
    if ($curmenu==""){
       $curmenu=Treat_DB_ProxyOld::mysql_result($result,0,"cafe_menu");
       setcookie("curmenu",$curmenu);
    }

if ($num != 1 || ($security_level==1 && ($bid != $businessid || $cid != $companyid)) || ($security_level > 1 AND $cid != $companyid) || ($user == "" && $pass == ""))
{
    echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}

else
{

?>
<head>

<script type="text/javascript" src="javascripts/prototype.js"></script>
<script type="text/javascript" src="javascripts/scriptaculous.js"></script>

<style type="text/css">
	.matched {background-color: #ccf; font-weight: bold;}
	.keyHover {background-color: #39f; color: white; border: 1px dotted red;}
	.mouseHover {background-color: #39f; border: 1px dotted red; color: white;}

	#autoBox{background: url('images/down_arrow.gif') no-repeat center right; background-color: white; border: 1px solid #1b1b1b;}
	#autoBox.hover{background: url('images/down_arrow_hover.gif') no-repeat center right; background-color: white; border: 1px solid #3c7fb1;}
	
	.dispVal{font-weight: bold; color: green; text-decoration: underline;}
</style>

<?
echo "
<script type=\"text/javascript\">
window.onload = function(){
   var auto = new Form.AutoComplete('autoBox', {
            dbTable: 'menu_items',
            dbDisp: 'item_name',
            dbVal: 'menu_item_id',
            dbWhere: \"businessid = '$businessid' AND menu_typeid = '$curmenu' ORDER BY item_name\",
            watermark: '(Select an Item)',
            showOnEmpty: true});
            auto.onchange=function(){auto.input.form.submit();}
}
</script>
";
?>

<script language="JavaScript"
   type="text/JavaScript">
function popup(url)
  {
     popwin=window.open(url,"Nutrition","location=no,menubar=no,titlebar=no,resizeable=no,height=250,width=400");   
  }
  
  function toggle_active(menu_item_id){
     var img = $('img'+menu_item_id);
	 var ex = img.src.split('/');
	 var src = ex[ex.length-1];
	 if(src == "on.gif"){
		img.src="/ta/off.gif";
		var val = 1;
		$('imgname'+menu_item_id).style.textDecoration = 'line-through';
	}
	 else{
		img.src="/ta/on.gif";
		var val = 0;
		$('imgname'+menu_item_id).style.textDecoration = 'none';
	}
	new Ajax.Request('ajaxmenu.php',{parameters: {'method': 'toggle_active','menu_item_id': menu_item_id,'val': val}});
  }
</script>

<script type="text/javascript" language="javascript">
function checkKey(){

var key = event.keyCode;
var DaIndexId = event.srcElement.IndexId-0
if(key==38) {
document.getElementByIndex(DaIndexId-1).focus();
return false;
}
else if(key==40) {
document.getElementByIndex(DaIndexId+1).focus();
return false;
}
else if(key==37) {
document.getElementByIndex(DaIndexId-1000).focus();
return false;
}
else if(key==39) {
document.getElementByIndex(DaIndexId+1000).focus();
return false;
}
return true
} // end checkKey function
document.getElementByIndex=function(){
for(var i=0;i<document.all.length;i++)
if(document.all[i].IndexId==arguments[0])
return document.all[i]
return null
}
</script>

<SCRIPT LANGUAGE=javascript><!--
function delconfirm(){return confirm('Are you sure you want to delete this group?');}
// --></SCRIPT>

<SCRIPT LANGUAGE=javascript><!--
function delconfirm2(){return confirm('Are you sure you want to delete this item?');}
// --></SCRIPT>

<SCRIPT LANGUAGE=javascript><!--
function confirmsave(){return confirm('Are you sure you want to update these items?');}
// --></SCRIPT>

<script language="JavaScript" 
   type="text/JavaScript">
function changePage(newLoc)
 {
   nextPage = newLoc.options[newLoc.selectedIndex].value
		
   if (nextPage != "")
   {
      document.location.href = nextPage
   }
 }
</script>

<script type="text/javascript" language="javascript">
function checkKey(){

var key = event.keyCode;
var DaIndexId = event.srcElement.IndexId-0
if(key==38) {
document.getElementByIndex(DaIndexId-1).focus();
return false;
}
else if(key==40) {
document.getElementByIndex(DaIndexId+1).focus();
return false;
}
else if(key==37) {
document.getElementByIndex(DaIndexId-1000).focus();
return false;
}
else if(key==39) {
document.getElementByIndex(DaIndexId+1000).focus();
return false;
}
return true
} // end checkKey function
document.getElementByIndex=function(){
for(var i=0;i<document.all.length;i++)
if(document.all[i].IndexId==arguments[0])
return document.all[i]
return null
}
</script>

<SCRIPT LANGUAGE="JavaScript">
<!-- Web Site:  http://dynamicdrive.com -->

<!-- This script and many more are available free online at -->
<!-- The JavaScript Source!! http://javascript.internet.com -->

<!-- Begin
function disableForm(theform) {
if (document.all || document.getElementById) {
for (i = 0; i < theform.length; i++) {
var tempobj = theform.elements[i];
if (tempobj.type.toLowerCase() == "submit" || tempobj.type.toLowerCase() == "reset")
tempobj.disabled = true;
}

}
else {
alert("The form has been submitted.  But, since you're not using IE 4+ or NS 6, the submit button was not disabled on form submission.");
return false;
   }
}
//  End -->
</script>

<script type="text/javascript" src="public_smo_scripts.js"></script>

</head>
<?php
    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%><tr><td colspan=2>";
    echo "<a href=businesstrack.php><img src=logo.jpg border=0 height=43 width=205></a><p></td></tr>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM company WHERE companyid = '$companyid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $companyname=Treat_DB_ProxyOld::mysql_result($result,0,"companyname");

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM security WHERE securityid = '$mysecurity'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $sec_bus_sales=Treat_DB_ProxyOld::mysql_result($result,0,"bus_sales");
    $sec_bus_invoice=Treat_DB_ProxyOld::mysql_result($result,0,"bus_invoice");
    $sec_bus_order=Treat_DB_ProxyOld::mysql_result($result,0,"bus_order");
    $sec_bus_payable=Treat_DB_ProxyOld::mysql_result($result,0,"bus_payable");
    $sec_bus_inventor1=Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor1");
    $sec_bus_control=Treat_DB_ProxyOld::mysql_result($result,0,"bus_control");
    $sec_bus_menu=Treat_DB_ProxyOld::mysql_result($result,0,"bus_menu");
    $sec_bus_order_setup=Treat_DB_ProxyOld::mysql_result($result,0,"bus_order_setup");
    $sec_bus_request=Treat_DB_ProxyOld::mysql_result($result,0,"bus_request");
    $sec_route_collection=Treat_DB_ProxyOld::mysql_result($result,0,"route_collection");
    $sec_route_labor=Treat_DB_ProxyOld::mysql_result($result,0,"route_labor");
    $sec_route_detail=Treat_DB_ProxyOld::mysql_result($result,0,"route_detail");
	$sec_route_machine=@Treat_DB_ProxyOld::mysql_result($result,0,"route_machine");
    $sec_bus_labor=Treat_DB_ProxyOld::mysql_result($result,0,"bus_labor");

    //echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";
    echo "<tr bgcolor=#CCCCFF><td width=1%><img src=weblogo.jpg width=80 height=75></td><td><font size=4><b>$businessname #$bus_unit</b></font><br>$street<br>$city, $state $zip<br>$phone</td>";
    echo "<td align=right valign=top>";
    echo "<br><FORM ACTION=editbus.php method=post>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<input type=hidden name=businessid value=$businessid>";
    echo "<input type=hidden name=companyid value=$companyid>";
    echo "<INPUT TYPE=submit VALUE=' Store Setup '></FORM></td></tr>";
    //echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";
    
    echo "<tr><td colspan=3><font color=#999999>";
    if ($sec_bus_sales>0){echo "<a href=busdetail.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Sales</font></a>";}
    if ($sec_bus_invoice>0){echo " | <a href=businvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Invoicing</font></a>";}
    if ($sec_bus_order>0){echo " | <a href=buscatertrack.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Orders</font></a>";}
    if ($sec_bus_payable>0){echo " | <a href=busapinvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Payables</font></a>";}
    if ($sec_bus_inventor1>0){echo " | <a href=businventor.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Inventory</font></a>";}
    if ($sec_bus_control>0){echo " | <a href=buscontrol.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Control Sheet</font></a>";}
    if ($sec_bus_menu>0){echo " | <a href=buscatermenu.php?bid=$businessid&cid=$companyid><font color=red onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='red' style='text-decoration:none'>Menus</font></a>";}
    if ($sec_bus_order_setup>0){echo " | <a href=buscaterroom.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Order Setup</font></a>";}
    if ($sec_bus_request>0){echo " | <a href=busrequest.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Requests</font></a>";}
    if ($sec_route_collection>0||$sec_route_labor>0||$sec_route_detail>0||$sec_route_machine>0){echo " | <a href=busroute_collect.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Route Collections</font></a>";}
    if ($sec_bus_labor>0){echo " | <a href=buslabor.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Labor</font></a>";}
    echo "</td></tr>";

    echo "</table></center><p>";

if ($security_level>0){
    echo "<center><table width=90%><tr><td width=50%><form action=businvoice.php method=post>";
    echo "<select name=gobus onChange='changePage(this.form.gobus)'>";

    $districtquery="";
    if ($security_level>2&&$security_level<7){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM login_district WHERE loginid = '$loginid'";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();

       $num--;
       while($num>=0){
          $districtid=Treat_DB_ProxyOld::mysql_result($result,$num,"districtid");
          if($districtquery==""){$districtquery="districtid = '$districtid'";}
          else{$districtquery="$districtquery OR districtid = '$districtid'";}
          $num--;
       }
    }

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    if($security_level>6){$query = "SELECT * FROM business WHERE companyid = '$companyid' ORDER BY businessname DESC";}
    elseif($security_level==2||$security_level==1){$query = "SELECT * FROM business WHERE companyid = '$companyid' AND (businessid = '$bid' OR businessid = '$bid2' OR businessid = '$bid3' OR businessid = '$bid4' OR businessid = '$bid5' OR businessid = '$bid6' OR businessid = '$bid7' OR businessid = '$bid8' OR businessid = '$bid9' OR businessid = '$bid10') ORDER BY businessname DESC";}
    elseif($security_level>2&&$security_level<7){$query = "SELECT * FROM business WHERE companyid = '$companyid' AND ($districtquery) ORDER BY businessname DESC";}
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    $num--;
    while($num>=0){
       $businessname=Treat_DB_ProxyOld::mysql_result($result,$num,"businessname");
       $busid=Treat_DB_ProxyOld::mysql_result($result,$num,"businessid");

       if ($busid==$businessid){$show="SELECTED";}
       else{$show="";}

       echo "<option value=buscatermenu.php?bid=$busid&cid=$companyid $show>$businessname</option>";
       $num--;
    }
 
    echo "</select></td><td></form></td><td width=50% align=right>&nbsp;&nbsp;</td></tr></table></center><p>";
}

	//mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query2 = "SELECT type FROM menu_type WHERE menu_typeid = '$curmenu'";
    $result2 = Treat_DB_ProxyOld::query($query2);
    //mysql_close();

    $curmenutype=Treat_DB_ProxyOld::mysql_result($result2,0,"type");

////////////////////////PLACE MENU/FIND ITEMS

    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%>";
    
    echo "<tr bgcolor=#E8E7E7><td height=1 colspan=4><a name='item'></a><form action=placemenu.php?cid=$companyid&bid=$businessid method=post><input type=hidden name=direct value=3><select name=curmenu>";

    $showordermenu="";
    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM menu_type WHERE companyid = '$companyid' AND (businessid = '$businessid' OR businessid = '0') ORDER BY menu_typeid DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    //$num--;
    while ($num>=0){
       $menu_typename=Treat_DB_ProxyOld::mysql_result($result,$num,"menu_typename");
       $menu_typeid=Treat_DB_ProxyOld::mysql_result($result,$num,"menu_typeid");
       $menutype=Treat_DB_ProxyOld::mysql_result($result,$num,"type");
       if ($curmenu==$menu_typeid){$showmenu="SELECTED"; $curmenutype=$menutype; $menu_parent=Treat_DB_ProxyOld::mysql_result($result,$num,"parent");}
       else{$showmenu="";}
       if ($curmenu==$menu_typeid&&$menutype==1){$showordermenu="<a href=busordermenu.php?bid=$businessid&cid=$companyid style=$style><font color=blue>Menu Schedule Setup</a>";}
       elseif ($curmenu==$menu_typeid&&$menutype==2){$showordermenu="<a href=busordermenu2.php?bid=$businessid&cid=$companyid style=$style><font color=blue>Menu Schedule Setup</a>";}
       elseif ($curmenu==$menu_typeid&&$menutype==0){$showordermenu="<a href=busordermenu10.php?bid=$businessid&cid=$companyid style=$style><font color=blue>Menu Schedule Setup</a>";}
       echo "<option value=$menu_typeid $showmenu>$menu_typename</option>";
       $num--;
    }

    echo "</select><input type=submit value='GO'> $showordermenu <a href=printmenu.php?cid=$companyid&bid=$businessid&curmenu=$curmenu style=$style target='_blank'><img src=print.gif height=16 width=16 border=0> <font color=blue>Printable Menu</font></a></td><td></form></td><td align=right><form action=buscatermenu.php?cid=$companyid&bid=$businessid method=post><input type=text name=finditem id=autoBox size=40/><input type=hidden name=findnext value='$findnext'><input type=hidden name=lastfind value='$finditem'></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td colspan=6></form></td></tr>";
    echo "<tr bgcolor=black><td height=1 colspan=6></td></tr></table>";

///////////////////////EDIT ITEM
    if ($view1=="edit"){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM menu_items WHERE menu_item_id = '$itemeditid'";
       $result2 = Treat_DB_ProxyOld::query($query2);
       //mysql_close();

       $editname=Treat_DB_ProxyOld::mysql_result($result2,0,"item_name");
       $mfrupc=Treat_DB_ProxyOld::mysql_result($result2,0,"upc");
       $unit=Treat_DB_ProxyOld::mysql_result($result2,0,"unit");
       $editprice=Treat_DB_ProxyOld::mysql_result($result2,0,"price");
       $parent=Treat_DB_ProxyOld::mysql_result($result2,0,"parent");
       $description=Treat_DB_ProxyOld::mysql_result($result2,0,"description");
       $grouptypeid=Treat_DB_ProxyOld::mysql_result($result2,0,"groupid");
       $true_unit=Treat_DB_ProxyOld::mysql_result($result2,0,"true_unit");
       $order_unit=Treat_DB_ProxyOld::mysql_result($result2,0,"order_unit");
       $su_in_ou=Treat_DB_ProxyOld::mysql_result($result2,0,"su_in_ou");
       $serving_size=Treat_DB_ProxyOld::mysql_result($result2,0,"serving_size");
       $label=Treat_DB_ProxyOld::mysql_result($result2,0,"label");
       $order_min=Treat_DB_ProxyOld::mysql_result($result2,0,"order_min");
       $item_code=Treat_DB_ProxyOld::mysql_result($result2,0,"item_code");
       $sales_acct=Treat_DB_ProxyOld::mysql_result($result2,0,"sales_acct");
       $tax_acct=Treat_DB_ProxyOld::mysql_result($result2,0,"tax_acct");
       $contract_option=Treat_DB_ProxyOld::mysql_result($result2,0,"contract_option");
       $myimage=Treat_DB_ProxyOld::mysql_result($result2,0,"image");
       $active=Treat_DB_ProxyOld::mysql_result($result2,0,"deleted");

       $deleteitem="<a href=editcateritem.php?cid=$companyid&bid=$businessid&edit=3&editid=$itemeditid onclick='return delconfirm2()'><img border=0 src=delete.gif height=16 width=16 alt='DELETE'></a>";
       $showrecipe="<a href=businventor4.php?cid=$companyid&bid=$businessid&editid=$itemeditid><img border=0 src=\"/assets/images/recipe/recipe.gif\" height=16 width=16 alt='EDIT RECIPE'></a>";
       
       if ($active==1){$showactive="";}
       else{$showactive="CHECKED";}

       echo "<center><table width=90% bgcolor='#E8E7E7'><tr><td colspan=2><form action=editcateritem.php method=post onSubmit='return disableForm(this);' style=\"margin:0;padding:0;display:inline;\"></td></tr>";
       echo "<tr bgcolor=#CCCCCC><td colspan=2><INPUT TYPE=hidden name=companyid value=$companyid><INPUT TYPE=hidden name=businessid value=$businessid><INPUT TYPE=hidden name=editid value=$itemeditid><INPUT TYPE=hidden name=edit value=1><b><font color=red>*</font><u>Edit Menu Item</u></b></td></tr>";
       echo "<tr><td align=right width=20%>Item Name/Code:</td><td><INPUT TYPE=text name=item_name value='$editname' size=30> <input type=text name=item_code size=20 value='$item_code'> <input type=checkbox name=active value=1 $showactive>Active $showrecipe $deleteitem</td></tr>";
       if ($curmenutype==2){echo "<tr><td align=right width=20%>Manufacturer UPC:</td><td><INPUT TYPE=text name=mfrupc value='$mfrupc' size=30></td></tr>";}
       echo "<tr><td align=right>Units:</td><td><INPUT TYPE=text name=unit value='$unit' size=10> Recipe Serves: <input type=text size=3 name=serving_size value='$serving_size'> Min. for Online Order: <input type=text name=order_min value='$order_min' size=3></td></tr>";
      /////////CONTRACT UNITS
      if ($contract==1){
       if ($label==1){$showcheck="CHECKED";}
       else{$showcheck="";}
       if ($contract_option==1){$showcheck2="CHECKED";}
       else{$showcheck2="";}

       echo "<tr><TD align=right>Production Units:</td><td><input type=text name=su_in_ou value=$su_in_ou size=5> <SELECT NAME=true_unit>";       

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM size ORDER BY sizename";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
       //mysql_close();

       while ($num2>=0){
          $sizeid=Treat_DB_ProxyOld::mysql_result($result2,$num2,"sizeid");
          $sizename=Treat_DB_ProxyOld::mysql_result($result2,$num2,"sizename");
          if ($sizeid==$true_unit){$check="SELECTED";$units=$sizename;}
          else {$check="";}
          echo "<option value=$sizeid $check>$sizename</option>";
          $num2--;
       }
       echo "</select> in ";
       echo "Order Units: <SELECT NAME=order_unit>";       

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM size ORDER BY sizename";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
       //mysql_close();

       while ($num2>=0){
          $sizeid=Treat_DB_ProxyOld::mysql_result($result2,$num2,"sizeid");
          $sizename=Treat_DB_ProxyOld::mysql_result($result2,$num2,"sizename");
          if ($sizeid==$order_unit){$check="SELECTED";}
          else {$check="";}
          echo "<option value=$sizeid $check>$sizename</option>";
          $num2--;
       }
       echo "</select> <input type=checkbox name=label value=1 $showcheck> Print Labels <input type=checkbox name=contract_option value=1 $showcheck2> Allow Substitution</td></tr>";
      }
      ///////////END CONTRACT UNITS
       echo "<tr><td align=right>Item Price:</td><td><INPUT TYPE=text name=item_price value=$editprice size=6><input type=hidden name=oldprice value='$editprice'></td></tr>";
       echo "<tr><TD align=right>Parent:</td><td><SELECT NAME=parent>";       

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM menu_items WHERE menu_typeid = '$menu_parent' ORDER BY item_name DESC";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
       //mysql_close();

       while ($num2>=0){
          $parent_id=Treat_DB_ProxyOld::mysql_result($result2,$num2,"menu_item_id");
          $parentname=Treat_DB_ProxyOld::mysql_result($result2,$num2,"item_name");
          if ($parent==$parent_id){$check="SELECTED";}
          else {$check="";}
          echo "<option value=$parent_id $check>$parentname</option>";
          $num2--;
       }
       echo "</select></td></tr>";

////////////////CATERING AND CONTRACT GROUP TYPE

     if ($curmenutype!=2){
       echo "<tr><td align=right>Group Type:</td><td><select name=grouptype>";

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM menu_groups WHERE companyid = '$companyid' AND menu_typeid = '$curmenu' ORDER BY groupname DESC";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();

       $num--;
       while ($num>=0){
          $groupname=Treat_DB_ProxyOld::mysql_result($result,$num,"groupname");
          $groupid=Treat_DB_ProxyOld::mysql_result($result,$num,"menu_group_id");
          if ($groupid==$grouptypeid){$sel="SELECTED";}
          else {$sel="";}
          echo "<option value=$groupid $sel>$groupname</option>";
          $num--;
       }
       echo "</select></td></tr>";
      }
      ////////////VENDING GROUP TYPE & PACKAGING
      else{
       echo "<tr><td align=right>Group Type:</td><td><select name=grouptype>";

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM menu_pricegroup WHERE menu_typeid = '$curmenu' ORDER BY menu_groupid,orderid DESC";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();

       $num--;
       while ($num>=0){
          $menu_groupname=Treat_DB_ProxyOld::mysql_result($result,$num,"menu_pricegroupname");
          $groupid=Treat_DB_ProxyOld::mysql_result($result,$num,"menu_pricegroupid");
          $menu_groupid=Treat_DB_ProxyOld::mysql_result($result,$num,"menu_groupid");
          if ($groupid==$grouptypeid){$sel="SELECTED";}
          else {$sel="";}

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query5 = "SELECT groupname FROM menu_groups WHERE menu_group_id = '$menu_groupid'";
          $result5 = Treat_DB_ProxyOld::query($query5);
          //mysql_close();

          $groupname=Treat_DB_ProxyOld::mysql_result($result5,0,"groupname");

          echo "<option value=$groupid $sel>$groupname - $menu_groupname</option>";
          $num--;
       }
       echo "</select></td></tr>";
     ////PACKAGING
       echo "<tr><td align=right>Packaging:</td><td><select name=true_unit>";

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM vend_pckg ORDER BY pckg_name DESC";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();

       $num--;
       while ($num>=0){
          $pckg_name=Treat_DB_ProxyOld::mysql_result($result,$num,"pckg_name");
          $vend_pckgid=Treat_DB_ProxyOld::mysql_result($result,$num,"vend_pckgid");
          if ($true_unit==$vend_pckgid){$sel="SELECTED";}
          else {$sel="";}

          echo "<option value=$vend_pckgid $sel>$pckg_name</option>";
          $num--;
       }
       echo "</select></td></tr>";
       echo "<tr><td align=right>Case Size:</td><td><input type=text size=3 name=su_in_ou value='$su_in_ou'> <font color=red><b>THIS IS HOW DRIVERS ORDER (ie. COOKIES BY THE DOZEN)</td><tr>";
      }
/////////////////GL
       echo "<tr><td align=right><i>Testing</i> G/L Sales:</td><td><select name=sales_acct>";

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM credits WHERE credittype = '1' AND businessid = '$businessid' AND invoicesales = '1' ORDER BY creditname DESC";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();

       $num--;
       while ($num>=0){
          $creditname=Treat_DB_ProxyOld::mysql_result($result,$num,"creditname");
          $creditid=Treat_DB_ProxyOld::mysql_result($result,$num,"creditid");
          if ($sales_acct==$creditid){$sel="SELECTED";}
          else {$sel="";}

          echo "<option value=$creditid $sel>$creditname</option>";
          $num--;
       }
       echo "</select>";
       echo " Tax: <select name=tax_acct>";

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM credits WHERE credittype = '2' AND businessid = '$businessid' AND invoicetax ='1' ORDER BY creditname DESC";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();

       $num--;
       while ($num>=0){
          $creditname=Treat_DB_ProxyOld::mysql_result($result,$num,"creditname");
          $creditid=Treat_DB_ProxyOld::mysql_result($result,$num,"creditid");
          if ($tax_acct==$creditid){$sel="SELECTED";}
          else {$sel="";}

          echo "<option value=$creditid $sel>$creditname</option>";
          $num--;
       }
       if ($tax_acct==0){$sel="SELECTED";}
       else{$sel="";}
       echo "<option value=0 $sel>Not Taxed</option></select>";
       echo " Non-Taxed: <select name=nontax_acct>";

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM credits WHERE credittype = '1' AND businessid = '$businessid' AND invoicesales_notax ='1' ORDER BY creditname DESC";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();

       $num--;
       while ($num>=0){
          $creditname=Treat_DB_ProxyOld::mysql_result($result,$num,"creditname");
          $creditid=Treat_DB_ProxyOld::mysql_result($result,$num,"creditid");
          if ($tax_acct==$creditid){$sel="SELECTED";}
          else {$sel="";}

          echo "<option value=$creditid $sel>$creditname</option>";
          $num--;
       }
       echo "</select></td></tr>";
       
/////////////////END GL
       echo "<tr><td align=right>Description:</td><td><textarea name=description rows=3 cols=75>$description</textarea></td></tr>";

       echo "<tr><td></td><td><INPUT TYPE=submit value='Save'></form> <form action=buscatermenu.php?bid=$businessid&cid=$companyid method=post onSubmit='return disableForm(this);' style=\"margin:0;padding:0;display:inline;\"><input type=submit value='Return'></form></td></tr>";

///////ALTERNATE PRICING////////////////////////////////////////
       echo "<tr bgcolor=#CCCCCC><td colspan=2><b><font color=red>*</font><u>Alternate Pricing</u></b></td></tr>";

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM menu_alt_price WHERE menu_itemid = '$itemeditid' ORDER BY priceid DESC";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
       //mysql_close();

       $num2--;
       while($num2>=0){
          $priceid=Treat_DB_ProxyOld::mysql_result($result2,$num2,"priceid");
          $price=Treat_DB_ProxyOld::mysql_result($result2,$num2,"price");
          $comment=Treat_DB_ProxyOld::mysql_result($result2,$num2,"comment");

          $showdelete="<a href=alt_price.php?cid=$companyid&bid=$businessid&edit=2&editid=$priceid&itemeditid=$itemeditid onclick='return delconfirm()'><img border=0 src=delete.gif height=16 width=16 alt='Delete'></a>";

          $price=money($price);
          echo "<tr><td colspan=2><ul><li>$price/$comment $showdelete</td></tr>";

          $num2--;
       }
       echo "<tr><td colspan=2><form action=alt_price.php method=post style=\"margin:0;padding:0;display:inline;\"><li><input type=hidden name=itemeditid value=$itemeditid><input type=hidden name=businessid value=$businessid><input type=hidden name=companyid value=$companyid><b>$</b><input type=text size=3 name=price> <input type=text size=25 name=comment maxlength=40> <input type=submit value='Add'></form></td></tr>";
///////IMAGE////////////////////////////////////////
       echo "<tr bgcolor=#CCCCCC><td colspan=2><b><font color=red>*</font><u>Image</u></b></td></tr>";
       echo "<tr><td colspan=2><li><form enctype='multipart/form-data' action=menuimage.php method=post target='_blank' style=\"margin:0;padding:0;display:inline;\"><input type=hidden name=menu_itemid value='$itemeditid'><input type=hidden name=MAX_FILE_SIZE value=10000000000><input type=file name=uploadedfile size=50> <input type=submit value='Save Picture'> </form>";
       if($myimage!=""){echo "<img src='menuimage/$myimage'> <a href=menuimage.php?bid=$businessid&cid=$companyid&menu_itemid=$itemeditid&del=1 target='_blank'><img src=delete.gif height=16 width=16 alt='Delete' border=0></a>";}
       echo " <a href=usepic.php?bid=$businessid&cid=$companyid&mid=$itemeditid target='_blank' style=$style><font color=blue>Use Existing Picture</font></a></td></tr>";
///////OPTION GROUPS////////////////////////////////////////
       echo "<tr bgcolor=#CCCCCC><td colspan=2><b><font color=red>*</font><u>Options for $editname</u></b></td></tr>";

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM optiongroup WHERE menu_item_id = '$itemeditid' ORDER BY orderid DESC";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
       //mysql_close();

       $num2--;
       while ($num2>=0){
          $optiongroupid=Treat_DB_ProxyOld::mysql_result($result2,$num2,"optiongroupid");
          $description=Treat_DB_ProxyOld::mysql_result($result2,$num2,"description");
          $type=Treat_DB_ProxyOld::mysql_result($result2,$num2,"type");
          $grouporder=Treat_DB_ProxyOld::mysql_result($result2,$num2,"orderid");

          if ($type==1){$showtype="CHECKED";}
          else {$showtype="";}

          $showdelete="<a href=editoptiongroup.php?cid=$companyid&bid=$businessid&edit=2&editid=$optiongroupid&itemeditid=$itemeditid onclick='return delconfirm()'><img border=0 src=delete.gif height=16 width=16 alt='Delete'></a>";

          echo "<tr><td colspan=2><form action=updateoptions.php method=post style=\"margin:0;padding:0;display:inline;\"><input type=hidden name=optiongroupid value=$optiongroupid><input type=hidden name=companyid value='$companyid'><input type=hidden name=businessid value='$businessid'><input type=hidden name=itemeditid value='$itemeditid'><input type=text size=3 name=grouporder value='$grouporder'> <i><u><input type=text size=30 name=description value='$description'></u> Mulitples: <input type=checkbox name=multiple value=1 $showtype> $showdelete</i></td></tr>";

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query = "SELECT * FROM options WHERE optiongroupid = '$optiongroupid' ORDER BY orderid DESC";
          $result = Treat_DB_ProxyOld::query($query);
          $num=Treat_DB_ProxyOld::mysql_numrows($result);
          //mysql_close();

          $num--;
          while ($num>=0){
             $optionid=Treat_DB_ProxyOld::mysql_result($result,$num,"optionid");
             $description2=Treat_DB_ProxyOld::mysql_result($result,$num,"description");
             $price=Treat_DB_ProxyOld::mysql_result($result,$num,"price");
             $order=Treat_DB_ProxyOld::mysql_result($result,$num,"orderid");
             $noprint=Treat_DB_ProxyOld::mysql_result($result,$num,"noprint");
             $price=money($price);

             if ($noprint==1){$showprint="CHECKED";}
             else{$showprint="";}

             $deleteoption="<a href=editoption.php?cid=$companyid&bid=$businessid&edit=3&editid=$optionid&itemeditid=$itemeditid onclick='return delconfirm2()'><img border=0 src=delete.gif height=16 width=16 alt='Delete'></a>";

             echo "<tr><td colspan=2><ul><li> <input type=text name='order$num' value='$order' size=3> <input type=text size=30 name='desc$num' value='$description2'> $<input type=text size=3 name='price$num' value='$price'> <input type=checkbox name='noprint$num' value=1 $showprint><font size=1>DON'T PRINT</font> $deleteoption</td></tr>";

             $num--;
          }
          $num2--;
          $order++;
          echo "<tr><td><input type=submit value='Save'></td><td></form></td></tr>";
          echo "<tr><td colspan=2><li><form action=editoption.php method=post><input type=hidden name=order value=$order><input type=hidden name=optiongroupid value='$optiongroupid'><input type=hidden name=companyid value='$companyid'><input type=hidden name=businessid value='$businessid'><input type=hidden name=itemeditid value='$itemeditid'>New Option: <INPUT TYPE=text name=optionname size=20> Price: <INPUT TYPE=text name=optionprice size=3> <INPUT TYPE=submit value='Add'></form></td></tr>";
          echo "<tr height=2 bgcolor=#CCCCCC><td colspan=2></td></tr>";
       }
       $grouporder++;
       echo "<tr><td colspan=2><li><form action=editoptiongroup.php method=post style=\"margin:0;padding:0;display:inline;\"><input type=hidden name=grouporder value=$grouporder><input type=hidden name=companyid value='$companyid'><input type=hidden name=businessid value='$businessid'><input type=hidden name=itemeditid value='$itemeditid'>New Option Group: <INPUT TYPE=text name=optiongroupname size=20> <INPUT TYPE=checkbox name=multiple> Multiple Options <INPUT TYPE=submit value='Add'></form></td></tr>";
    }
///////Serving Sizes////////////////////////////////////////
    if ($view1=="edit" && $curmenutype == 1){
       $showrecipe="<a href=businventor4.php?cid=$companyid&bid=$businessid&editid=$itemeditid><img border=0 src=\"/assets/images/recipe/recipe.gif\" height=16 width=16 alt='EDIT RECIPE'></a>";
       echo "<tr bgcolor=#CCCCCC><td colspan=2><form action=copysizes.php method=post><b><font color=red>*</font><u>Serving/Production Sizes for $editname</u></b> $showrecipe <br>Copy Serving Sizes: <select name=servsize onChange='changePage(this.form.servsize)'>"; 

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM menu_items WHERE businessid = '$businessid' ORDER BY item_name DESC";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
       //mysql_close();

       while($num2>=0){
          $copyitemid=Treat_DB_ProxyOld::mysql_result($result2,$num2,"menu_item_id");
          $copyitemname=Treat_DB_ProxyOld::mysql_result($result2,$num2,"item_name");
          echo "<option value=buscatermenu.php?cid=$companyid&bid=$businessid&view1=edit&mid=$itemeditid&copyitemid=$copyitemid#item>$copyitemname</option>";
          $num2--;
       }

       echo "</select></form><form action=editportion.php method=post><input type=hidden name=optiongroupid value='$optiongroupid'><input type=hidden name=companyid value='$companyid'><input type=hidden name=businessid value='$businessid'><input type=hidden name=editid value='$itemeditid'></td></tr>";

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM menu_type WHERE companyid = '$companyid' AND type = '1' AND businessid = '$businessid' ORDER BY menu_typename DESC";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
       //mysql_close();

       $counter=1;
       $curarrow=1;

       $num2--;
       while ($num2>=0){
          $menu_typeid=Treat_DB_ProxyOld::mysql_result($result2,$num2,"menu_typeid");
          $menu_typename=Treat_DB_ProxyOld::mysql_result($result2,$num2,"menu_typename");

          echo "<tr><td colspan=2><i><u>$menu_typename</u></i></td></tr>";

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query12 = "SELECT * FROM menu_daypart WHERE menu_typeid = '$menu_typeid' ORDER BY orderid DESC";
          $result12 = Treat_DB_ProxyOld::query($query12);
          $num12=Treat_DB_ProxyOld::mysql_numrows($result12);
          //mysql_close();
          $num14=$num12;

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query = "SELECT * FROM menu_portion WHERE menu_typeid = '$menu_typeid' ORDER BY orderid DESC";
          $result = Treat_DB_ProxyOld::query($query);
          $num=Treat_DB_ProxyOld::mysql_numrows($result);
          //mysql_close();

          echo "<tr><td colspan=2>";

          echo "<table border=1>";

             echo "<tr><td></td>";
             $num14--;
             while($num14>=0){
                $menu_daypartid=Treat_DB_ProxyOld::mysql_result($result12,$num14,"menu_daypartid");
                $menu_daypartname=Treat_DB_ProxyOld::mysql_result($result12,$num14,"menu_daypartname");
                echo "<td align=right><font size=2>$menu_daypartname</td>";
                $num14--;
             }
             echo "</tr>";

          $num--;
          while ($num>=0){
             $menu_portionid=Treat_DB_ProxyOld::mysql_result($result,$num,"menu_portionid");
             $menu_portionname=Treat_DB_ProxyOld::mysql_result($result,$num,"menu_portionname");

             $num14=$num12-1;
             while($num14>=0){
                $menu_daypartid=Treat_DB_ProxyOld::mysql_result($result12,$num14,"menu_daypartid");
                $menu_daypartname=Treat_DB_ProxyOld::mysql_result($result12,$num14,"menu_daypartname");

                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                if ($copyitemid2!=""){$query5 = "SELECT * FROM menu_portiondetail WHERE menu_itemid = '$copyitemid2' AND menu_typeid = '$menu_typeid' AND menu_portionid = '$menu_portionid' AND menu_daypartid = '$menu_daypartid'";}
                else{$query5 = "SELECT * FROM menu_portiondetail WHERE menu_itemid = '$itemeditid' AND menu_typeid = '$menu_typeid' AND menu_portionid = '$menu_portionid' AND menu_daypartid = '$menu_daypartid'";}
                $result5 = Treat_DB_ProxyOld::query($query5);
                //mysql_close();

                $portionsize=Treat_DB_ProxyOld::mysql_result($result5,0,"portion");
                $srv_size=Treat_DB_ProxyOld::mysql_result($result5,0,"srv_size");
      
                $var_name="portion$counter";
                $var_name2="srv_size$counter";

                if ($num14==$num12-1){echo "<tr><td align=right><font size=2 align=right>$menu_portionname</td>";}

                $colwidth=100/($num12+1);

                echo "<td width=$colwidth% align=right><input type=text size=3 name=$var_name2 value='$srv_size'>/<input type=text size=3 name=$var_name value='$portionsize' onkeydown='return checkKey()' IndexId=$curarrow> <font size=2>$units</td>";
                $curarrow=$curarrow+1000;
                if ($num14==0){echo "</tr>";}

                $num14--;
                $counter++;
             }
             $curarrow=$curarrow-($num12*1000);
             $curarrow++;
             $num14=$num12;
             $num--;
          }
          $num2--;
          echo "</table>";
          echo "</td></tr>";
       }
       echo "<tr bgcolor=#CCCCCC><td align=right><input type=submit value='Save'></td><td></form></td></tr>";
       echo "</table></center>";
    }

    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%>";
    echo "<tr bgcolor=#E8E7E7><td height=1 colspan=6></td></tr><tr><td colspan=6>";

/////////////////////////////////////Add Item
    if ($view1!="edit"){
       echo "<center><table width=100% bgcolor='#E8E7E7'><tr><td colspan=2><form action=editcateritem.php method=post onSubmit='return disableForm(this);' style=\"margin:0;padding:0;display:inline;\"></td></tr>";
       echo "<tr bgcolor=#CCCCCC><td colspan=2><INPUT TYPE=hidden name=companyid value=$companyid><INPUT TYPE=hidden name=businessid value=$businessid><b><u>Add Menu Item</u></b></td></tr>";
       echo "<tr><td align=right width=20%>Item Name/Code:</td><td><INPUT TYPE=text name=item_name size=30> <input type=text name=item_code size=20></td></tr>";
       if ($curmenutype==2){echo "<tr><td align=right width=20%>Manufacturer UPC:</td><td><INPUT TYPE=text name=mfrupc value='$mfrupc' size=30></td></tr>";}
       echo "<tr><td align=right>Units:</td><td><INPUT TYPE=text name=unit value='each' size=10> Recipe Serves: <input type=text size=3 name=serving_size value='1'> Min. for Online Order: <input type=text name=order_min value='$order_min' size=3></td></tr>";
     ///////////CONTRACT UNITS
     if ($contract==1){
       echo "<tr><TD align=right>Production Units:</td><td><input type=text name=su_in_ou size=5> <SELECT NAME=true_unit>";       

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM size ORDER BY sizename";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
       //mysql_close();

       while ($num2>=0){
          $sizeid=Treat_DB_ProxyOld::mysql_result($result2,$num2,"sizeid");
          $sizename=Treat_DB_ProxyOld::mysql_result($result2,$num2,"sizename");
          echo "<option value=$sizeid>$sizename</option>";
          $num2--;
       }
       echo "</select> in ";
       echo "Order Units: <SELECT NAME=order_unit>";       

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM size ORDER BY sizename";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
       //mysql_close();

       while ($num2>=0){
          $sizeid=Treat_DB_ProxyOld::mysql_result($result2,$num2,"sizeid");
          $sizename=Treat_DB_ProxyOld::mysql_result($result2,$num2,"sizename");
          if ($sizeid==$order_unit){$check="SELECTED";}
          else {$check="";}
          echo "<option value=$sizeid $check>$sizename</option>";
          $num2--;
       }
       echo "</select> <input type=checkbox name=label value=1 CHECKED> Print Labels <input type=checkbox name=contract_option value=1 $showcheck2> Allow Substitution</td></tr>";
      }
      /////END CONTRACT UNITS
       echo "<tr><td align=right>Item Price:</td><td><INPUT TYPE=text name=item_price size=6></td></tr>";
       echo "<tr><TD align=right>Parent:</td><td><SELECT NAME=parent>";

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM menu_items WHERE menu_typeid = '$menu_parent' ORDER BY item_name DESC";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
       //mysql_close();

       while ($num2>=0){
          $parent_id=Treat_DB_ProxyOld::mysql_result($result2,$num2,"menu_item_id");
          $parentname=Treat_DB_ProxyOld::mysql_result($result2,$num2,"item_name");
          echo "<option value=$parent_id>$parentname</option>";
          $num2--;
       }
       echo "</select></td></tr>";

/////CATERING & CONTRACT GROUP TYPE

      if ($curmenutype!=2){
       echo "<tr><td align=right>Group Type:</td><td><select name=grouptype>";

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM menu_groups WHERE companyid = '$companyid' AND menu_typeid = '$curmenu' ORDER BY groupname DESC";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();

       $num--;
       while ($num>=0){
          $groupname=Treat_DB_ProxyOld::mysql_result($result,$num,"groupname");
          $groupid=Treat_DB_ProxyOld::mysql_result($result,$num,"menu_group_id");
          if ($groupid==$grouptypeid){$sel="SELECTED";}
          else {$sel="";}
          echo "<option value=$groupid $sel>$groupname</option>";
          $num--;
       }
       echo "</select></a></td></tr>";
      }
      ////////////VENDING GROUP TYPE
      else{
       echo "<tr><td align=right>Group Type:</td><td><select name=grouptype>";

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM menu_pricegroup WHERE menu_typeid = '$curmenu' ORDER BY menu_groupid,orderid DESC";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();

       $num--;
       while ($num>=0){
          $menu_groupname=Treat_DB_ProxyOld::mysql_result($result,$num,"menu_pricegroupname");
          $groupid=Treat_DB_ProxyOld::mysql_result($result,$num,"menu_pricegroupid");
          $menu_groupid=Treat_DB_ProxyOld::mysql_result($result,$num,"menu_groupid");
          if ($groupid==$grouptypeid){$sel="SELECTED";}
          else {$sel="";}

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query5 = "SELECT groupname FROM menu_groups WHERE menu_group_id = '$menu_groupid'";
          $result5 = Treat_DB_ProxyOld::query($query5);
          //mysql_close();

          $groupname=Treat_DB_ProxyOld::mysql_result($result5,0,"groupname");

          echo "<option value=$groupid $sel>$groupname - $menu_groupname</option>";
          $num--;
       }
       echo "</select></a></td></tr>";
      
     ////PACKAGING
       echo "<tr><td align=right>Packaging:</td><td><select name=true_unit>";

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM vend_pckg ORDER BY pckg_name DESC";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();

       $num--;
       while ($num>=0){
          $pckg_name=Treat_DB_ProxyOld::mysql_result($result,$num,"pckg_name");
          $vend_pckgid=Treat_DB_ProxyOld::mysql_result($result,$num,"vend_pckgid");
          if ($true_unit==$vend_pckgid){$sel="SELECTED";}
          else {$sel="";}

          echo "<option value=$vend_pckgid $sel>$pckg_name</option>";
          $num--;
       }
       echo "</select></td></tr>";
       echo "<tr><td align=right>Case Size:</td><td><input type=text size=3 name=su_in_ou value='1'></td><tr>";
      }
///////////END GROUP TYPE
/////////////////GL
       echo "<tr><td align=right><i>Testing</i> G/L Sales:</td><td><select name=sales_acct>";

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM credits WHERE credittype = '1' AND businessid = '$businessid' AND invoicesales = '1' ORDER BY creditname DESC";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();

       $num--;
       while ($num>=0){
          $creditname=Treat_DB_ProxyOld::mysql_result($result,$num,"creditname");
          $creditid=Treat_DB_ProxyOld::mysql_result($result,$num,"creditid");

          echo "<option value=$creditid $sel>$creditname</option>";
          $num--;
       }
       echo "</select>";
       echo " Tax: <select name=tax_acct>";

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM credits WHERE credittype = '2' AND businessid = '$businessid' AND invoicetax = '1' ORDER BY creditname DESC";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();

       $num--;
       while ($num>=0){
          $creditname=Treat_DB_ProxyOld::mysql_result($result,$num,"creditname");
          $creditid=Treat_DB_ProxyOld::mysql_result($result,$num,"creditid");

          echo "<option value=$creditid $sel>$creditname</option>";
          $num--;
       }
       echo "<option value=0>Not Taxed</option></select>";
       echo " Non-Taxed: <select name=nontax_acct>";

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM credits WHERE credittype = '1' AND businessid = '$businessid' AND invoicesales_notax ='1' ORDER BY creditname DESC";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();

       $num--;
       while ($num>=0){
          $creditname=Treat_DB_ProxyOld::mysql_result($result,$num,"creditname");
          $creditid=Treat_DB_ProxyOld::mysql_result($result,$num,"creditid");
          if ($tax_acct==$creditid){$sel="SELECTED";}
          else {$sel="";}

          echo "<option value=$creditid $sel>$creditname</option>";
          $num--;
       }
       echo "</select></td></tr>";
       
/////////////////END GL

       echo "<tr><td align=right>Description:</td><td><textarea name=description rows=3 cols=50>$description</textarea></td></tr>";

       echo "<tr><td></td><td><INPUT TYPE=submit value='Add Item'></form> </td></tr></table></center>";
    }
/////////////////////////LIST ITEMS//////////////////////////////////////////////
    echo "</td></tr><tr bgcolor=#CCCCCC><td height=1 colspan=6><form action=updatemenuprice.php method=post onSubmit='return disableForm(this);'><input type=hidden name=businessid value=$businessid><input type=hidden name=companyid value=$companyid><input type=hidden name=curmenu value=$curmenu></td></tr>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM menu_items WHERE businessid = '$businessid' AND menu_typeid = '$curmenu' ORDER BY groupid,item_name";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    $menu_counter=1;
    $lastgroup="";
    $num--;
    while ($r=Treat_DB_ProxyOld::mysql_fetch_array($result)){
       $menu_item_id=$r["menu_item_id"];
       $item_name=$r["item_name"];
       $price=$r["price"];
       $parent=$r["parent"];
       $groupid=$r["groupid"];
       $deleted=$r["deleted"];
       $recipe_station=$r["recipe_station"];
       $heart_healthy=$r["heart_healthy"];

       ////////////HAS RECIPE/NUTRITION
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query61 = "SELECT * FROM recipe WHERE menu_itemid = '$menu_item_id'";
       $result61 = Treat_DB_ProxyOld::query($query61);
       $num61=Treat_DB_ProxyOld::mysql_numrows($result61);
       //mysql_close();

       if ($num61>0){$shownutr="<a onclick=popup('nutrition.php?mid=$menu_item_id')><img src=heart.jpg height=16 width=14 alt='Nutritional Information' border=0></a>";}
       else{$shownutr="";}
       /////////////////////////////////

       if ($parent!=0){$parent_name="<font size=2 color=gray>(LINKED)</font>";}
       else {$parent_name="";}

       if ($recipe_station>0){
          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query7 = "SELECT station_name FROM servery_station WHERE stationid = '$recipe_station'";
          $result7 = Treat_DB_ProxyOld::query($query7);
          //mysql_close();

          $station_name=Treat_DB_ProxyOld::mysql_result($result7,0,"station_name");
          $station_name="<font size=2 color=blue>($station_name)</font>";
       }
       else{$station_name="";}

       $price=money($price);

       if ($lastgroup!=$groupid){
          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          if($curmenutype==2){$query7 = "SELECT menu_pricegroupname,menu_groupid FROM menu_pricegroup WHERE menu_pricegroupid = '$groupid'";}
          else{$query7 = "SELECT * FROM menu_groups WHERE menu_group_id = '$groupid'";}
          $result7 = Treat_DB_ProxyOld::query($query7);
          //mysql_close();
 
          if($curmenutype==2){
              $groupname=Treat_DB_ProxyOld::mysql_result($result7,0,"menu_pricegroupname");
              $menu_groupid=Treat_DB_ProxyOld::mysql_result($result7,0,"menu_groupid");

              //mysql_connect($dbhost,$username,$password);
              //@mysql_select_db($database) or die( "Unable to select database");
              $query7 = "SELECT groupname FROM menu_groups WHERE menu_group_id = '$menu_groupid'";
              $result7 = Treat_DB_ProxyOld::query($query7);
              //mysql_close();

              $groupname2=Treat_DB_ProxyOld::mysql_result($result7,0,"groupname");

              $groupname="$groupname2 - $groupname";
          }
          else{$groupname=Treat_DB_ProxyOld::mysql_result($result7,0,"groupname");}

          echo "<tr bgcolor=#FFFF99><td colspan=6><b><i>$groupname</i></b></td></tr>";
          echo "<tr bgcolor=#CCCCCC><td height=1 colspan=6></td></tr>";
     
          $lastgroup=$groupid;
       }

       if($heart_healthy==1){$showhh="<img src=hh1.gif height=16 width=17>";}
       else{$showhh="";}

       $showedit="<a href=buscatermenu.php?cid=$companyid&bid=$businessid&view1=edit&mid=$menu_item_id#item><img border=0 src=edit.gif height=16 width=16 alt='EDIT MENU ITEM'></a>";
       $showrecipe="<a href=businventor4.php?cid=$companyid&bid=$businessid&editid=$menu_item_id><img border=0 src=\"/assets/images/recipe/recipe.gif\" height=16 width=16 alt='EDIT RECIPE'></a>";
       if ($parent<0){$showedit="";}
	   
	   ///////////NEW ACTIVE/INACTIVE
       if ($deleted==0){$showdelete2="<a href=\"javascript:void(0);\" onclick=\"toggle_active('$menu_item_id');\"><img border=0 id='img$menu_item_id' src=on.gif height=15 width=15 alt='ACTIVE'></a>";$item_name="<span id='imgname$menu_item_id'>$item_name</span>";}
       else {$showdelete2="<a href=\"javascript:void(0);\" onclick=\"toggle_active('$menu_item_id');\"><img border=0 src=off.gif id='img$menu_item_id' height=15 width=15 alt='INACTIVE'></a>";$item_name="<span id='imgname$menu_item_id' style=\"text-decoration:line-through;\">$item_name</span>";}	  
	  
	   if ($parent<0){$deleteitem="";}
       else {$deleteitem="<a href=editcateritem.php?cid=$companyid&bid=$businessid&edit=3&editid=$menu_item_id onclick='return delconfirm2()'><img border=0 src=delete.gif height=16 width=16 alt='DELETE'></a>";}

       $editme="edit$menu_item_id";

       echo "<tr bgcolor='white' onMouseOver=this.bgColor='#999999' onMouseOut=this.bgColor='white' id='row$menu_counter'><td width=60%><input type=checkbox name=$editme value=1 id='$menu_item_id'> $showhh$shownutr$item_name $parent_name $station_name</td><td width=4%>$showdelete2</td><td width=4%>$showedit</td><td width=4%>$deleteitem</td><td>$showrecipe</td><td align=right>$<input type=text size=4 value='$price' name='$menu_item_id' tabindex='1' onfocus='this.select();' onchange=\"row$menu_counter.style.backgroundColor='yellow'\" STYLE='font-size: 10px;'></td></tr>";
       echo "<tr bgcolor=#CCCCCC><td height=1 colspan=6></td></tr>";

       $menu_counter++;
       $num--;
    }
  
    echo "<tr bgcolor=#E8E7E7><td colspan=4>&nbsp;&nbsp;<img src=turnarrow.gif height=16 width=16> <select name=myupdate>";

    echo "<option value=0>Please Choose...</option>";
    echo "<option value=2>Make Inactive</option>";
    echo "<option value=3>Make Active</option>";
    echo "<optgroup label='OR Update Recipe Group to...'>";

    $query11 = "SELECT * FROM servery_station ORDER BY station_name DESC";
    $result11 = Treat_DB_ProxyOld::query($query11);
    $num11=Treat_DB_ProxyOld::mysql_numrows($result11);

    $num11--;
    while($num11>=0){
       $stationid=Treat_DB_ProxyOld::mysql_result($result11,$num11,"stationid");
       $station_name=Treat_DB_ProxyOld::mysql_result($result11,$num11,"station_name");

       echo "<option value='1:$stationid'>$station_name</option>";

       $num11--;
    }
    echo "</optgroup>";

    echo "<optgroup label='OR Update Menu Group to...'>";

    if($curmenutype==2){
       $query11 = "SELECT * FROM menu_groups WHERE menu_typeid = '$curmenu' ORDER BY groupname DESC";
       $result11 = Treat_DB_ProxyOld::query($query11);
       $num11=Treat_DB_ProxyOld::mysql_numrows($result11);

       $num11--;
       while($num11>=0){
          $menu_group_id=Treat_DB_ProxyOld::mysql_result($result11,$num11,"menu_group_id");
          $groupname=Treat_DB_ProxyOld::mysql_result($result11,$num11,"groupname");

          $query12 = "SELECT * FROM menu_pricegroup WHERE menu_groupid = '$menu_group_id' ORDER BY menu_pricegroupname DESC";
          $result12 = Treat_DB_ProxyOld::query($query12);
          $num12=Treat_DB_ProxyOld::mysql_numrows($result12);

          $num12--;
          while($num12>=0){
             $menu_pricegroupid=Treat_DB_ProxyOld::mysql_result($result12,$num12,"menu_pricegroupid");
             $menu_pricegroupname=Treat_DB_ProxyOld::mysql_result($result12,$num12,"menu_pricegroupname");

             echo "<option value='4:$menu_pricegroupid'>$groupname - $menu_pricegroupname</option>";

             $num12--;
          }

          $num11--;
       }
       echo "</optgroup>";
    }
    else{
       $query11 = "SELECT * FROM menu_groups WHERE menu_typeid = '$curmenu' ORDER BY groupname DESC";
       $result11 = Treat_DB_ProxyOld::query($query11);
       $num11=Treat_DB_ProxyOld::mysql_numrows($result11);

       $num11--;
       while($num11>=0){
          $menu_group_id=Treat_DB_ProxyOld::mysql_result($result11,$num11,"menu_group_id");
          $groupname=Treat_DB_ProxyOld::mysql_result($result11,$num11,"groupname");

          echo "<option value='4:$menu_group_id'>$groupname</option>";

          $num11--;
       }
       echo "</optgroup>";
    }

    echo "</select></td><td height=1 colspan=2 align=right><input type=submit value='Save' onclick='return confirmsave()'></td></tr>";
    echo "<tr bgcolor=#E8E7E7 height=1><td colspan=6></td></tr></table></form></a></center>";

///////////PRICE MATRIX///////////////////////////////////////////////////////////////////////////

 if ($curmenutype==1){

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query11 = "SELECT * FROM menu_portion WHERE menu_typeid = '$curmenu' ORDER BY orderid DESC";
    $result11 = Treat_DB_ProxyOld::query($query11);
    $num11=Treat_DB_ProxyOld::mysql_numrows($result11);
    //mysql_close();

    $colnum=$num11+1;
    $colnum2=$colnum+2;
    $curarrow=1;

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM menu_daypart WHERE menu_typeid = '$curmenu' ORDER BY orderid DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    echo "<p><center><a name=matrix></a><table width=90% cellspacing=0 cellpadding=0>";
    echo "<tr><td colspan=$colnum2 height=1 bgcolor=black><form action=saveordermatrix.php method=post><input type=hidden name=businessid value=$businessid><input type=hidden name=companyid value=$companyid></td></tr>";
    echo "<tr><td width=1 bgcolor=black></td><td colspan=$colnum style='filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr=white, startColorstr=#DCDCDC, gradientType=0);'><b>Price Matrix</b></td><td width=1 bgcolor=black></td></tr>";

    $num--;
    while ($num>=0){
       $menu_daypartid=Treat_DB_ProxyOld::mysql_result($result,$num,"menu_daypartid"); 
       $menu_daypartname=Treat_DB_ProxyOld::mysql_result($result,$num,"menu_daypartname"); 

       echo "<tr bgcolor=black height=1><td colspan=$colnum2>";
       echo "<input type=hidden name=username value=$user>";
       echo "<input type=hidden name=password value=$pass>";
       echo "</td></tr>";
       echo "<tr><td width=1 bgcolor=#E8E7E7></td><td bgcolor=white><b>$menu_daypartname</b></td>";

       $counter=$num11;
       $counter--;
       while ($counter>=0){
             $portionname=Treat_DB_ProxyOld::mysql_result($result11,$counter,"menu_portionname"); 
             $portionid=Treat_DB_ProxyOld::mysql_result($result11,$counter,"menu_portionid"); 
             echo "<td align=right bgcolor=white><b>$portionname</b></td>";
             $counter--;
       }

       echo "<td width=1 bgcolor=#E8E7E7></td></tr>";
       echo "<tr bgcolor=#E8E7E7 height=1><td colspan=$colnum2></td></tr>";

       echo "<tr><td width=1 bgcolor=#E8E7E7></td><td bgcolor=white><i>Price:</i></td>";
       $counter=$num11;
       $counter--;
       $countme=1;
       while ($counter>=0){
             $portionname=Treat_DB_ProxyOld::mysql_result($result11,$counter,"menu_portionname"); 
             $portionid=Treat_DB_ProxyOld::mysql_result($result11,$counter,"menu_portionid"); 

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query12 = "SELECT * FROM order_matrix WHERE daypartid = '$menu_daypartid' AND portionid = '$portionid' AND menu_typeid = '$curmenu' AND businessid = '$businessid'";
             $result12 = Treat_DB_ProxyOld::query($query12);
             //mysql_close();
             $portionvalue=Treat_DB_ProxyOld::mysql_result($result12,0,"price"); 

             $var_name="portion$num$counter";
             echo "<td align=right bgcolor=white><b>$</b><input type=text size=4 name='$var_name' value='$portionvalue' onkeydown='return checkKey()' IndexId=$curarrow></td>";
             $counter--;
             $curarrow=$curarrow+1000;$countme++;
       }
       echo "<td width=1 bgcolor=#E8E7E7></td></tr>";
       if ($countme!=1){$curarrow=$curarrow-(($countme-1)*1000)+1;}

       $num--;
    }

    echo "<tr><td colspan=$colnum2 height=1 bgcolor=black></td></tr>";
    echo "<tr><td width=1 bgcolor=black></td><td colspan=$colnum align=right style='filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr=#DCDCDC, startColorstr=white, gradientType=0);'><input type=submit value='Save' onclick='return confirmsave()'></td><td width=1 bgcolor=black></form></td></tr>";
    echo "<tr><td colspan=$colnum2 height=1 bgcolor=black></td></tr>";
    echo "</table></center>";

 }
    
//////END TABLE////////////////////////////////////////////////////////////////////////////////////

    //mysql_close();

    echo "<br><FORM ACTION=businesstrack.php method=post>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<center><INPUT TYPE=submit VALUE=' Return to Main Page'></FORM></center></body>";
	
	google_page_track();
}
?>