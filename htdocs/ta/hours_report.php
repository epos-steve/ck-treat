<head>
<SCRIPT LANGUAGE="JavaScript" SRC="CalendarPopup.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">document.write(getCalendarStyles());</SCRIPT>

<STYLE>
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation
			{
			background-color:#6677DD;
			text-align:center;
			vertical-align:center;
			text-decoration:none;
			color:#FFFFFF;
			font-weight:bold;
			}
	.TESTcpDayColumnHeader,
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation,
	.TESTcpCurrentMonthDate,
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDate,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDate,
	.TESTcpCurrentDateDisabled,
	.TESTcpTodayText,
	.TESTcpTodayTextDisabled,
	.TESTcpText
			{
			font-family:arial;
			font-size:8pt;
			}
	TD.TESTcpDayColumnHeader
			{
			text-align:right;
			border:solid thin #6677DD;
			border-width:0 0 1 0;
			}
	.TESTcpCurrentMonthDate,
	.TESTcpOtherMonthDate,
	.TESTcpCurrentDate
			{
			text-align:right;
			text-decoration:none;
			}
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDateDisabled
			{
			color:#D0D0D0;
			text-align:right;
			text-decoration:line-through;
			}
	.TESTcpCurrentMonthDate
			{
			color:#6677DD;
			font-weight:bold;
			}
	.TESTcpCurrentDate
			{
			color: #FFFFFF;
			font-weight:bold;
			}
	.TESTcpOtherMonthDate
			{
			color:#808080;
			}
	TD.TESTcpCurrentDate
			{
			color:#FFFFFF;
			background-color: #6677DD;
			border-width:1;
			border:solid thin #000000;
			}
	TD.TESTcpCurrentDateDisabled
			{
			border-width:1;
			border:solid thin #FFAAAA;
			}
	TD.TESTcpTodayText,
	TD.TESTcpTodayTextDisabled
			{
			border:solid thin #6677DD;
			border-width:1 0 0 0;
			}
	A.TESTcpTodayText,
	SPAN.TESTcpTodayTextDisabled
			{
			height:20px;
			}
	A.TESTcpTodayText
			{
			color:#6677DD;
			font-weight:bold;
			}
	SPAN.TESTcpTodayTextDisabled
			{
			color:#D0D0D0;
			}
	.TESTcpBorder
			{
			border:solid thin #6677DD;
			}
</STYLE>
</head>
<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once(DOC_ROOT.'/bootstrap.php');

if ( require_once( realpath( DOC_ROOT . '/../lib/inc/cookie-login.php' ) ) ) {
	$businessid = $_GET["bid"];

	$todo = $_POST["todo"];

	if($todo == 1){
		$date1 = $_POST["date1"];
		$date2 = $_POST["date2"];

		////display report
		$data = "<center><table width=90% cellspacing=0 cellpadding=0 style=\"border:1px solid black;font-size:12px;\">";

		///dates
		$data .= "<tr bgcolor=#E8E7E7><td style=\"border:1px solid black;\">&nbsp;<i>Hours From $date1 to $date2</i></td>";
		$tempdate = $date1;
		while($tempdate<=$date2){
			$showdate = substr($tempdate,5,2) . "/" . substr($tempdate,8,2);
			$data .= "<td align=right style=\"border:1px solid black;\">$showdate</td>";
			$tempdate = nextday($tempdate);
		}
		$data .= "<td align=right style=\"border:1px solid black;\">Total</td></tr>";

		///list employees
		$query = "SELECT * FROM login WHERE oo2 = $businessid AND is_deleted = 0 ORDER BY lastname,firstname";
		$result = Treat_DB_ProxyOld::query($query);

		while($r = mysql_fetch_array($result)){
			$loginid = $r["loginid"];
			$firstname = $r["firstname"];
			$lastname = $r["lastname"];
			$go = 0;
			$total = 0;
			$go = $_POST["lid$loginid"];

			if($go == 1){
				$data .= "<tr><td style=\"border:1px solid black;\">&nbsp;$lastname, $firstname</td>";

				$tempdate = $date1;
				while($tempdate <= $date2){
					$query2 = "SELECT SUM(TIME_TO_SEC(TimeDiff(clockout,clockin))) AS totalamt FROM labor_clockin WHERE loginid = '$loginid' AND clockin >= '$tempdate 00:00:00' AND clockin <= '$tempdate 23:59:59' AND is_deleted = '0'";
					$result2 = Treat_DB_ProxyOld::query($query2);

					$week_hours = @mysql_result($result2,0,"totalamt");
					$week_hours = round($week_hours/60/60,1);

					$total += $week_hours;

					$data .= "<td align=right style=\"border:1px solid black;\">$week_hours</td>";
					$tempdate = nextday($tempdate);
				}
				$data .= "<td align=right style=\"border:1px solid black;\">$total</td></tr>";
			}
		}

		$data .= "</table></center>";

		///download file
		$file="hours-$date1-$date2.xls";
		header("Content-type: application/x-msdownload");
		header("Content-Disposition: attachment; filename=$file");
		header("Pragma: no-cache");
		header("Expires: 100");
		echo $data;
	}
	else{
		$today = date("Y-m-d");

		echo "<form action=hours_report.php?bid=$businessid method=post>";
		echo "<center><table width=50% cellspacing=0 cellpadding=0 style=\"border:1px solid black;\">";
		echo "<tr bgcolor=#E8E7E7><td colspan=2 style=\"border:1px solid black;\">&nbsp;<i>Please choose who to display...</i></td></tr>";
		///list employess
		$query = "SELECT * FROM login WHERE oo2 = $businessid AND is_deleted = 0 ORDER BY lastname,firstname";
		$result = Treat_DB_ProxyOld::query($query);

		while($r = mysql_fetch_array($result)){
			$loginid = $r["loginid"];
			$firstname = $r["firstname"];
			$lastname = $r["lastname"];

			echo "<tr><td style=\"border:1px solid black;\"><center><input type=checkbox name='lid$loginid' value=1></td><td style=\"border:1px solid black;\">&nbsp;$lastname, $firstname</td></tr>";
		}
		echo "<tr><td colspan=2 align=right style=\"border:1px solid black;\">";
		echo "<i>From</i> <input type=text name=date1 value='$today' size=8> <SCRIPT LANGUAGE='JavaScript' ID='js14'> var cal14 = new CalendarPopup('testdiv1'); cal14.setCssPrefix('TEST');</SCRIPT> <A HREF='javascript:void();' onClick=cal14.select(document.forms[0].date1,'anchor14','yyyy-MM-dd'); return false; TITLE=cal14.select(document.forms[0].date1,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor14' ID='anchor14'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A> <i>to</i> ";
		echo "<input type=text name=date2 value='$today' size=8> <SCRIPT LANGUAGE='JavaScript' ID='js15'> var cal15 = new CalendarPopup('testdiv1'); cal15.setCssPrefix('TEST');</SCRIPT> <A HREF='javascript:void();' onClick=cal15.select(document.forms[0].date2,'anchor15','yyyy-MM-dd'); return false; TITLE=cal15.select(document.forms[0].date2,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor15' ID='anchor15'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A> ";
		echo "<input type=submit value='Submit'><input type=hidden name=todo value=1>&nbsp;";
		echo "</table></center></form>";

		echo "<DIV ID=testdiv1 STYLE=position:absolute;visibility:hidden;background-color:white;layer-background-color:white;></DIV>";
	}
}
?>
