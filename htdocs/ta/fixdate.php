<?php

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      elseif ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      elseif ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '1')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='31')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;
$leap = date("L");

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   
   elseif ($leap==1&&$month=='03')
   {
      $day=$day+29;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

//include("db.php");

define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');

$style = "text-decoration:none";

$user=$_COOKIE["usercook"];
$pass=$_COOKIE["passcook"];
$businessid=$_GET['bid'];
$companyid=$_GET['cid'];
$date1=$_GET['date1'];
$date2=$_GET['date2'];
$clockinid=$_GET['clockinid'];
$clear=$_GET['clear'];
$del=$_GET['del'];
$delete=$_GET['delete'];
$loginid=$_GET['loginid'];
$chdate=$_GET['chdate'];

mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOldProcessHost::query($query);
$num=mysql_numrows($result);

$location="buslabor5.php?bid=$businessid&cid=$companyid&date1=$date1&date2=$date2&addclock=$clockinid#$loginid";

if ($num != 1)
{
    echo "<center><h3>Failed</h3></center>";
}
elseif($chdate==1){
    $query = "SELECT * FROM labor_clockin WHERE clockinid = '$clockinid'";
    $result = Treat_DB_ProxyOldProcessHost::query($query);

    $clockin=mysql_result($result,0,"clockin");
    $clockout=mysql_result($result,0,"clockout");

    $newdate=substr($clockout,0,10);
    $newtime=substr($clockout,11,8);

    $newdate=nextday($newdate);
    $newdate2=$newdate;

    $newdate="$newdate $newtime";

    $query = "UPDATE labor_clockin SET clockout = '$newdate' WHERE clockinid = '$clockinid'";
    $result = Treat_DB_ProxyOld::query($query);

    if($newdate>$date2){$location="buslabor5.php?bid=$businessid&cid=$companyid&date1=$date1&date2=$newdate2&addclock=$clockinid#$loginid";}
}
elseif($chdate==2){
    $query = "SELECT * FROM labor_clockin WHERE clockinid = '$clockinid'";
    $result = Treat_DB_ProxyOldProcessHost::query($query);

    $clockin=mysql_result($result,0,"clockin");
    $clockout=mysql_result($result,0,"clockout");

    $newdate=substr($clockout,0,10);
    $newtime=substr($clockout,11,8);

    $newdate=prevday($newdate);
    $newdate2=$newdate;

    $newdate="$newdate $newtime";

    $query = "UPDATE labor_clockin SET clockout = '$newdate' WHERE clockinid = '$clockinid'";
    $result = Treat_DB_ProxyOld::query($query);

    if($newdate<$date1){$location="buslabor5.php?bid=$businessid&cid=$companyid&date1=$newdate2&date2=$date2&addclock=$clockinid#$loginid";}
}
elseif($chdate==3){
    $query = "SELECT * FROM labor_clockin WHERE clockinid = '$clockinid'";
    $result = Treat_DB_ProxyOldProcessHost::query($query);

    $clockin=mysql_result($result,0,"clockin");
    $clockout=mysql_result($result,0,"clockout");

    $newdate=substr($clockin,0,10);
    $newtime=substr($clockin,11,8);

    $newdate=nextday($newdate);
    $newdate2=$newdate;

    $newdate="$newdate $newtime";

    $query = "UPDATE labor_clockin SET clockin = '$newdate' WHERE clockinid = '$clockinid'";
    $result = Treat_DB_ProxyOld::query($query);

    if($newdate>$date2){$location="buslabor5.php?bid=$businessid&cid=$companyid&date1=$date1&date2=$newdate2&addclock=$clockinid#$loginid";}
}
elseif($chdate==4){
    $query = "SELECT * FROM labor_clockin WHERE clockinid = '$clockinid'";
    $result = Treat_DB_ProxyOldProcessHost::query($query);

    $clockin=mysql_result($result,0,"clockin");
    $clockout=mysql_result($result,0,"clockout");

    $newdate=substr($clockin,0,10);
    $newtime=substr($clockin,11,8);

    $newdate=prevday($newdate);
    $newdate2=$newdate;

    $newdate="$newdate $newtime";

    $query = "UPDATE labor_clockin SET clockin = '$newdate' WHERE clockinid = '$clockinid'";
    $result = Treat_DB_ProxyOld::query($query);

    if($newdate<$date1){$location="buslabor5.php?bid=$businessid&cid=$companyid&date1=$newdate2&date2=$date2&addclock=$clockinid#$loginid";}
}
elseif($del==1){
    $query = "UPDATE labor_clockin SET is_deleted = '$delete' WHERE clockinid = '$clockinid'";
    $result = Treat_DB_ProxyOld::query($query);
}
elseif($clear==1){
    $query = "UPDATE labor_clockin SET clockout = '0000-00-00 00:00:00' WHERE clockinid = '$clockinid'";
    $result = Treat_DB_ProxyOld::query($query);
}
else
{
    $query = "SELECT * FROM labor_clockin WHERE clockinid = '$clockinid'";
    $result = Treat_DB_ProxyOldProcessHost::query($query);

    $clockin=mysql_result($result,0,"clockin");
    $clockout=mysql_result($result,0,"clockout");

    $newdate=substr($clockin,0,10);
    $newtime=substr($clockout,11,8);

    $newdate="$newdate $newtime";

    $query = "UPDATE labor_clockin SET clockout = '$newdate' WHERE clockinid = '$clockinid'";
    $result = Treat_DB_ProxyOld::query($query);
}
mysql_close();

header('Location: ./' . $location);
?>