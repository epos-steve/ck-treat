<?php

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+3);
        if ($diff > 0 && $diff < '0.01'){$diff="0.01";}
        elseif($diff < 0 && $diff > '-0.01'){$diff="-0.01";}
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      if ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='32')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='08' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      $day=$day-31;  
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";} 
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}


if ( !defined('DOC_ROOT'))
{
	define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
}
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');
$style = "text-decoration:none";

$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";

$user = isset($_POST['username'])?$_POST['username']:'';
$pass = isset($_POST['password'])?$_POST['password']:'';
if ($user==""&&$pass==""){
   $user = isset($_COOKIE["usercook"])?$_COOKIE["usercook"]:'';
   $pass = isset($_COOKIE["passcook"])?$_COOKIE["passcook"]:'';
}

mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = mysql_query($query);
$num=mysql_numrows($result);
//mysql_close();

if ($num != 1 || $user == "" || $pass == "")
{
    echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
    $user=@mysql_result($result,0,"username");

    setcookie("usercook",$user);
    setcookie("passcook",$pass);

    $date = isset($_POST['date'])?$_POST['date']:'';
    $peoplenum = isset($_POST['peoplenum'])?$_POST['peoplenum']:'';
    $roomid = isset($_POST['room'])?$_POST['room']:'';
    $start_hour = isset($_POST['start_hour'])?$_POST['start_hour']:'';
    $start_min = isset($_POST['start_min'])?$_POST['start_min']:'';
    $end_hour = isset($_POST['end_hour'])?$_POST['end_hour']:'';
    $end_min = isset($_POST['end_min'])?$_POST['end_min']:'';
    $start_am = isset($_POST['start_am'])?$_POST['start_am']:'';
    $end_am = isset($_POST['end_am'])?$_POST['end_am']:'';
    $roomcomment = isset($_POST['roomcomment'])?$_POST['roomcomment']:'';
    $comment = isset($_POST['comment'])?$_POST['comment']:'';
    $reserveid = isset($_POST['reserveid'])?$_POST['reserveid']:'';
    $bid = isset($_POST['bid'])?$_POST['bid']:'';
    $cid = isset($_POST['cid'])?$_POST['cid']:'';

    if ($start_am=="AM" && $start_hour==12){$start_hour="00";}
    elseif ($start_am=="PM" && $start_hour!=12){$start_hour=$start_hour+12;}
    if ($end_am=="AM" && $end_hour==12){$end_hour="00";}
    elseif ($end_am=="PM" && $end_hour!=12){$end_hour=$end_hour+12;}

    $start_hour="$start_hour$start_min";
    $end_hour="$end_hour$end_min";

    $start_hour=$start_hour+0;
    $end_hour=$end_hour+0;

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM reserve WHERE reserveid != '$reserveid' AND roomid = '$roomid' AND roomid != '0' AND date = '$date' AND ((start_hour >= '$start_hour' AND start_hour <= '$end_hour') OR (end_hour >= '$start_hour' AND end_hour <= '$end_hour') OR (start_hour <= '$start_hour' AND end_hour >= '$end_hour'))";
    $result = mysql_query($query);
    $num=mysql_numrows($result);
    //mysql_close();

    if ($num!=0){echo "<p><br><p><br><p><br><center><font color=red><b>Room already Booked.</b></font><p><i>Please use your browser's back button to return</center>";}
    elseif ($peoplenum==0 || $peoplenum==""){echo "<p><br><p><br><p><br><center><font color=red><b>Please enter number of guests.</b></font><p><i>Please use your browser's back button to return</center>";}
    elseif ($end_hour < $start_hour){echo "<p><br><p><br><p><br><center><font color=red><b>Time is Off.</b></font><p><i>Please use your browser's back button to return</center>";}
    elseif (time() > strtotime($date)){echo "<p><br><p><br><p><br><center><font color=red><b>Not a valid Date.</b></font><p><i>Please use your browser's back button to return</center>";}
    elseif ($roomid==0&&$roomcomment==""){echo "<p><br><p><br><p><br><center><font color=red><b>Please specify why location is N/A.</b></font><p><i>Please use your browser's back button to return</center>";}
    elseif ($comment==""){echo "<p><br><p><br><p><br><center><font color=red><b>Please enter a reference.</b></font><p><i>Please use your browser's back button to return</center>";}
    else{
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query="UPDATE reserve SET date = '$date',start_hour = '$start_hour',end_hour = '$end_hour',peoplenum = '$peoplenum',roomid = '$roomid',roomcomment = '$roomcomment',comment = '$comment' WHERE reserveid = '$reserveid'";
       $result = mysql_query($query);
       //mysql_close();

       ////////////////////////////////////////////////////////
      ////////////////insert/update custom fields/////////////
      ////////////////////////////////////////////////////////
      $updateid = $reserveid;

      $query_custom = "SELECT * FROM invoice_custom_fields WHERE businessid = '$bid' AND active = '0'";
      $result_custom = mysql_query($query_custom);
      $num_custom = mysql_num_rows($result_custom);

      if($num_custom>0){

           while($r_cust=mysql_fetch_array($result_custom)){
               $cust_id=$r_cust["id"];
			   $type=$r_cust["type"];
               $newvalue = $_POST["cust$cust_id"];

               $query_custom2 = "SELECT id FROM invoice_custom_values WHERE reserveid = '$updateid' AND customid = '$cust_id'";
               $result_custom2 = mysql_query($query_custom2);
               $num_custom2 = mysql_num_rows($result_custom2);

			   if($type == 4){}
               elseif($num_custom2!=0){
                   $valueid = @mysql_result($result_custom2,0,"id");
                   $query_custom2 = "UPDATE invoice_custom_values SET value = '$newvalue' WHERE id = '$valueid'";
                   $result_custom2 = mysql_query($query_custom2);
               }
               elseif($newvalue!=""){
                   $query_custom2 = "INSERT INTO invoice_custom_values (reserveid,customid,value) VALUES ('$updateid','$cust_id','$newvalue')";
                   $result_custom2 = mysql_query($query_custom2);
               }
           }
      }

      ////////////////////////////////////////////////////////
      ////////////////end custom fields///////////////////////
      ////////////////////////////////////////////////////////

       $location="buscatertrack.php?bid=$bid&cid=$cid";
       header('Location: ./' . $location); 
    }
}
mysql_close();
?>