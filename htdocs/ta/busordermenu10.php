<?php
define( 'DOC_ROOT', dirname(dirname(__FILE__)) );
require_once( DOC_ROOT . '/bootstrap.php' );

$time = null; # Notice: Undefined variable
$date = null; # Notice: Undefined variable
$weekend = null; # Notice: Undefined variable
$menu_daypartid = null;
$moveitem = null;

$user = \EE\Controller\Base::getSessionCookieVariable( 'usercook' );
$pass = \EE\Controller\Base::getSessionCookieVariable( 'passcook' );
$cid = \EE\Controller\Base::getSessionCookieVariable( 'compcook' );
$caterbus = \EE\Controller\Base::getSessionCookieVariable( 'caterbus' );
$curmenu = \EE\Controller\Base::getSessionCookieVariable( 'curmenu' );
$curdaypart = \EE\Controller\Base::getSessionCookieVariable( 'curdaypart' );
$number = \EE\Controller\Base::getGetVariable( 'number' );
$newmonth = \EE\Controller\Base::getGetVariable( 'newmonth' );
$newyear = \EE\Controller\Base::getGetVariable( 'newyear' );
$newstart = \EE\Controller\Base::getGetVariable( 'newstart' );
$busid = \EE\Controller\Base::getGetVariable( 'bid' );
$adddate = \EE\Controller\Base::getGetVariable( 'adddate' );
$menu = \EE\Controller\Base::getGetVariable( 'menu' );
$newcurdaypart = \EE\Controller\Base::getGetVariable( 'curdaypart' );
if ( $newcurdaypart != '' ) {
	setcookie( 'curdaypart', $newcurdaypart );
	$curdaypart = $newcurdaypart;
}

if( $busid && !$caterbus ) {
	$caterbus = $busid;
	\EE\Controller\Base::setCookie( 'caterbus', $caterbus, 0, '/ta/' );
}

$today = date("l");
$todaynum = date("j");
$todaynum2 = date("j");
$year = date("Y");
$monthnum = date("n");
$day = 1;
$high = 6;
$long = 1;
$leap = date("L");
$back = "white";

$sync_schedule = \EE\Controller\Base::getGetVariable( 'sync_schedule' );

if( $sync_schedule ) {
	header( "Location: ?bid=$busid&cid=$cid" );
	Treat_Model_KioskSync::syncSpecialItems( $busid );
	exit( );
}

?>
<html>
<head>
	<title>busordermenu10.php</title>
<script language="JavaScript" type="text/javascript" src="/assets/js/calendarPopup.js"></script>

<script language="JavaScript" type="text/javascript">document.write(getCalendarStyles());</script>

<style type="text/css">
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation
			{
			background-color:#6677DD;
			text-align:center;
			vertical-align:center;
			text-decoration:none;
			color:#FFFFFF;
			font-weight:bold;
			}
	.TESTcpDayColumnHeader,
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation,
	.TESTcpCurrentMonthDate,
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDate,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDate,
	.TESTcpCurrentDateDisabled,
	.TESTcpTodayText,
	.TESTcpTodayTextDisabled,
	.TESTcpText
			{
			font-family:arial;
			font-size:8pt;
			}
	TD.TESTcpDayColumnHeader
			{
			text-align:right;
			border:solid thin #6677DD;
			border-width:0 0 1 0;
			}
	.TESTcpCurrentMonthDate,
	.TESTcpOtherMonthDate,
	.TESTcpCurrentDate
			{
			text-align:right;
			text-decoration:none;
			}
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDateDisabled
			{
			color:#D0D0D0;
			text-align:right;
			text-decoration:line-through;
			}
	.TESTcpCurrentMonthDate
			{
			color:#6677DD;
			font-weight:bold;
			}
	.TESTcpCurrentDate
			{
			color: #FFFFFF;
			font-weight:bold;
			}
	.TESTcpOtherMonthDate
			{
			color:#808080;
			}
	TD.TESTcpCurrentDate
			{
			color:#FFFFFF;
			background-color: #6677DD;
			border-width:1;
			border:solid thin #000000;
			}
	TD.TESTcpCurrentDateDisabled
			{
			border-width:1;
			border:solid thin #FFAAAA;
			}
	TD.TESTcpTodayText,
	TD.TESTcpTodayTextDisabled
			{
			border:solid thin #6677DD;
			border-width:1 0 0 0;
			}
	A.TESTcpTodayText,
	SPAN.TESTcpTodayTextDisabled
			{
			height:20px;
			}
	A.TESTcpTodayText
			{
			color:#6677DD;
			font-weight:bold;
			}
	SPAN.TESTcpTodayTextDisabled
			{
			color:#D0D0D0;
			}
	.TESTcpBorder
			{
			border:solid thin #6677DD;
			}
</style>

<script language="JavaScript" type="text/javascript">
function changePage(newLoc)
 {
   nextPage = newLoc.options[newLoc.selectedIndex].value
		
   if (nextPage != "")
   {
	  document.location.href = nextPage
   }
 }
</script>

<script language="JavaScript" type="text/javascript">
<!-- Web Site:  http://dynamicdrive.com -->

<!-- This script and many more are available free online at -->
<!-- The JavaScript Source!! http://javascript.internet.com -->

<!-- Begin
function disableForm(theform) {
if (document.all || document.getElementById) {
for (i = 0; i < theform.length; i++) {
var tempobj = theform.elements[i];
if (tempobj.type.toLowerCase() == "submit" || tempobj.type.toLowerCase() == "reset")
tempobj.disabled = true;
}

}
else {
alert("The form has been submitted.  But, since you're not using IE 4+ or NS 6, the submit button was not disabled on form submission.");
return false;
   }
}
//  End -->
</script>

</head>

<?php

function BetweenOneAndSeven( $num ) {
	$rv = null;
	if ( $num < 1 ) {
		$rv = BetweenOneAndSeven( $num + 7 );
	}
	elseif ( $num > 7 ) {
		$rv = BetweenOneAndSeven( $num - 7 );
	}
	else {
		$rv = $num;
	}
	
	return $rv;
}

$count = 0;
if ($newstart == "")
{
/*
* /
	while ($todaynum > 7){$todaynum = $todaynum - 7; $count++;}
	if ($today == 'Sunday' && $todaynum == 1){$start=1;}
	if ($today == 'Monday' && $todaynum == 1){$start=2;}
	if ($today == 'Tuesday' && $todaynum == 1){$start=3;}
	if ($today == 'Wednesday' && $todaynum == 1){$start=4;}
	if ($today == 'Thursday' && $todaynum == 1){$start=5;}
	if ($today == 'Friday' && $todaynum == 1){$start=6;}
	if ($today == 'Saturday' && $todaynum == 1){$start=7;}
	if ($today == 'Sunday' && $todaynum == 2){$start=7;}
	if ($today == 'Monday' && $todaynum == 2){$start=1;}
	if ($today == 'Tuesday' && $todaynum == 2){$start=2;}
	if ($today == 'Wednesday' && $todaynum == 2){$start=3;}
	if ($today == 'Thursday' && $todaynum == 2){$start=4;}
	if ($today == 'Friday' && $todaynum == 2){$start=5;}
	if ($today == 'Saturday' && $todaynum == 2){$start=6;}
	if ($today == 'Sunday' && $todaynum == 3){$start=6;}
	if ($today == 'Monday' && $todaynum == 3){$start=7;}
	if ($today == 'Tuesday' && $todaynum == 3){$start=1;}
	if ($today == 'Wednesday' && $todaynum == 3){$start=2;}
	if ($today == 'Thursday' && $todaynum == 3){$start=3;}
	if ($today == 'Friday' && $todaynum == 3){$start=4;}
	if ($today == 'Saturday' && $todaynum == 3){$start=5;}
	if ($today == 'Sunday' && $todaynum == 4){$start=5;}
	if ($today == 'Monday' && $todaynum == 4){$start=6;}
	if ($today == 'Tuesday' && $todaynum == 4){$start=7;}
	if ($today == 'Wednesday' && $todaynum == 4){$start=1;}
	if ($today == 'Thursday' && $todaynum == 4){$start=2;}
	if ($today == 'Friday' && $todaynum == 4){$start=3;}
	if ($today == 'Saturday' && $todaynum == 4){$start=4;}
	if ($today == 'Sunday' && $todaynum == 5){$start=4;}
	if ($today == 'Monday' && $todaynum == 5){$start=5;}
	if ($today == 'Tuesday' && $todaynum == 5){$start=6;}
	if ($today == 'Wednesday' && $todaynum == 5){$start=7;}
	if ($today == 'Thursday' && $todaynum == 5){$start=1;}
	if ($today == 'Friday' && $todaynum == 5){$start=2;}
	if ($today == 'Saturday' && $todaynum == 5){$start=3;}
	if ($today == 'Sunday' && $todaynum == 6){$start=3;}
	if ($today == 'Monday' && $todaynum == 6){$start=4;}
	if ($today == 'Tuesday' && $todaynum == 6){$start=5;}
	if ($today == 'Wednesday' && $todaynum == 6){$start=6;}
	if ($today == 'Thursday' && $todaynum == 6){$start=7;}
	if ($today == 'Friday' && $todaynum == 6){$start=1;}
	if ($today == 'Saturday' && $todaynum == 6){$start=2;}
	if ($today == 'Sunday' && $todaynum == 7){$start=2;}
	if ($today == 'Monday' && $todaynum == 7){$start=3;}
	if ($today == 'Tuesday' && $todaynum == 7){$start=4;}
	if ($today == 'Wednesday' && $todaynum == 7){$start=5;}
	if ($today == 'Thursday' && $todaynum == 7){$start=6;}
	if ($today == 'Friday' && $todaynum == 7){$start=7;}
	if ($today == 'Saturday' && $todaynum == 7){$start=1;}
/*
*/
	$start_values = array(
		1 => 7,
		2 => 6,
		3 => 5,
		4 => 4,
		5 => 3,
		6 => 2,
		7 => 1,
	);
	
	$count = floor($todaynum / 7);
	$todaynum %= 7;
	
	switch ( $today ) {
		case 'Sunday':
			$start = $start_values[ BetweenOneAndSeven( $todaynum + 6 ) ];
			break;
		case 'Monday':
			$start = $start_values[ BetweenOneAndSeven( $todaynum + 5 ) ];
			break;
		case 'Tuesday':
			$start = $start_values[ BetweenOneAndSeven( $todaynum + 4 ) ];
			break;
		case 'Wednesday':
			$start = $start_values[ BetweenOneAndSeven( $todaynum + 3 ) ];
			break;
		case 'Thursday':
			$start = $start_values[ BetweenOneAndSeven( $todaynum + 2 ) ];
			break;
		case 'Friday':
			$start = $start_values[ BetweenOneAndSeven( $todaynum + 1 ) ];
			break;
		case 'Saturday':
			$start = $start_values[ BetweenOneAndSeven( $todaynum ) ];
			break;
	}
}
elseif ($newstart > 7)
{
	$start = $newstart - 7;
}
else
{
	$start = $newstart;
}

if ( $newmonth != "" ) {}
else {
	$newmonth = $monthnum;
}
if ( $newyear != "" ) {
	$year = $newyear;
}
else {
	$newyear = $year;
}

if ( $adddate == "" ) {
	$adddate = date( 'Y-m-d' );
}


$style = "text-decoration:none";



$query = "SELECT * FROM business WHERE businessid = '$caterbus'";
$result = Treat_DB_ProxyOld::query( $query );

if ( $curmenu == "" ) {
	$curmenu = Treat_DB_ProxyOld::mysql_result( $result, 0, 'cafe_menu' );
}
$default_menu = Treat_DB_ProxyOld::mysql_result( $result, 0, 'default_menu' );
$cafe_menu = Treat_DB_ProxyOld::mysql_result( $result, 0, 'cafe_menu' );
$vend_menu = Treat_DB_ProxyOld::mysql_result( $result, 0, 'vend_menu' );
$default_menu_alias = Treat_DB_ProxyOld::mysql_result( $result, 0, 'default_menu_alias' );
$cafe_menu_alias = Treat_DB_ProxyOld::mysql_result( $result, 0, 'cafe_menu_alias' );
$vend_menu_alias = Treat_DB_ProxyOld::mysql_result( $result, 0, 'vend_menu_alias' );

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM menu_type WHERE menu_typeid = '$curmenu'";
$result = Treat_DB_ProxyOld::query( $query );
//mysql_close();

$menuname = Treat_DB_ProxyOld::mysql_result( $result, 0, 'menu_typename' );
$parent = Treat_DB_ProxyOld::mysql_result( $result, 0, 'parent' );

if ( $curmenu == $default_menu && $default_menu_alias > 0 ) {
	$aliasid = $default_menu_alias;
}
elseif ( $curmenu == $cafe_menu && $cafe_menu_alias > 0 ) {
	$aliasid = $cafe_menu_alias;
}
elseif ( $curmenu == $vend_menu && $vend_menu_alias > 0 ) {
	$aliasid = $vend_menu_alias;
}
else {
	$aliasid = 0;
}

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query( $query );
$num = Treat_DB_ProxyOld::mysql_num_rows( $result );
//mysql_close();

if ( $num != 1 || ( $user == "" && $pass == "" ) )
{
	echo "<center><h4>Failed</h4></center>";
}

else
{
	$mymonth = $newmonth;
	$myyear = $newyear;
	$mystart = $newstart;

	//onload=focus();additem6.menu_itemid.focus()
	echo "<body>";

	$fontsize = 2;

	$bid = $caterbus;
	$cid = Treat_DB_ProxyOld::mysql_result( $result, 0, 'companyid' );
	$security_level = Treat_DB_ProxyOld::mysql_result( $result, 0, 'security_level' );

	if ($newmonth == 1){$month = "January";}
	elseif ($newmonth == 2){$month = "February";}
	elseif ($newmonth == 3){$month = "March";}
	elseif ($newmonth == 4){$month = "April";}
	elseif ($newmonth == 5){$month = "May";}
	elseif ($newmonth == 6){$month = "June";}
	elseif ($newmonth == 7){$month = "July";}
	elseif ($newmonth == 8){$month = "August";}
	elseif ($newmonth == 9){$month = "September";}
	elseif ($newmonth == 10){$month = "October";}
	elseif ($newmonth == 11){$month = "November";}
	elseif ($newmonth == 12){$month = "December";}

	echo "<center><table cellspacing=0 cellpadding=0 border=0 width=99%><tr><td colspan=2><a href=businesstrack.php><img src=logo.jpg border=0 height=43 width=205></a><p></td></tr>";
	echo "</table><p>";
	echo "<table width=99% style=\"border:2px solid #CCCCCC;\" cellspacing=0 cellpadding=0>";
	echo "<tr bgcolor=#CCCCFF><td colspan=2><center><br><h3>Daily Specials for $menuname ";

	echo " - $month $year</h3></center></td></tr>";

	$leap=isLeapYear($year);
	if($leap==29){$leap=1;}
	else{$leap=0;}


////////////////////////ADD ITEMS


	echo "<tr bgcolor=#CCCCFF><td><FORM ACTION=orderadd10.php METHOD=post onSubmit='return disableForm(this);'><input type=hidden name=businessid value=$caterbus><input type=hidden name=daypartid value=$curdaypart>";
	echo "<input type=hidden name=username value=$user>";
	echo "<input type=hidden name=password value=$pass>";
	echo "<input type=hidden name=date value=$date>";
	echo "<input type=hidden name=newstart value=$newstart>";
	echo "<input type=hidden name=newmonth value=$newmonth>";
	echo "<input type=hidden name=newyear value=$newyear>";
	echo "<input type=hidden name=direct value=1>";
	echo "<input type=hidden name=weekend value=$weekend>";
	if($parent>0){echo "<input type=hidden name=parent value=$curmenu>";}
	echo "<i>Add Items For</i> ";
	echo "<SCRIPT LANGUAGE='JavaScript' ID='js12'> var cal12 = new CalendarPopup('testdiv1');cal12.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=\"date5\" VALUE='$adddate' SIZE=8> <A HREF='javascript:void();' onClick=cal12.select(document.forms[0].date5,'anchor12','yyyy-MM-dd'); return false; TITLE=cal12.select(document.forms[0].date5,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor12' ID='anchor12'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A>";
	echo "<br><select name=menu_itemid[] MULTIPLE SIZE=16>";
	$lastgroupid=0;

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	if($aliasid==0&&$parent==0){$query4 = "SELECT * FROM menu_items,servery_station WHERE menu_items.businessid = '$caterbus' AND menu_items.deleted = '0' AND menu_items.menu_typeid = '$curmenu' AND menu_items.recipe_station > '0' AND menu_items.recipe_station = servery_station.stationid ORDER BY servery_station.orderid DESC, menu_items.item_name DESC";}
	elseif($parent>0){$query4 = "SELECT * FROM menu_items,servery_station WHERE menu_items.businessid = '$caterbus' AND menu_items.deleted = '0' AND menu_items.menu_typeid = '$parent' AND menu_items.recipe_station > '0' AND menu_items.recipe_station = servery_station.stationid ORDER BY servery_station.orderid DESC, menu_items.item_name DESC";}
	else{$query4 = "SELECT * FROM menu_items,servery_station WHERE menu_items.businessid = '$aliasid' AND menu_items.deleted = '0' AND menu_items.menu_typeid = '$curmenu' AND menu_items.recipe_station > '0' AND menu_items.recipe_station = servery_station.stationid ORDER BY servery_station.orderid DESC, menu_items.item_name DESC";}
	$result4 = Treat_DB_ProxyOld::query($query4);
	$num4 = Treat_DB_ProxyOld::mysql_num_rows( $result4 );
	//mysql_close();

	$num4--;
	while ( $num4 >= 0 ) {

		$item_name2 = Treat_DB_ProxyOld::mysql_result( $result4, $num4, 'item_name' );
		$menu_item_id2 = Treat_DB_ProxyOld::mysql_result( $result4, $num4, 'menu_item_id' );
		$price2 = Treat_DB_ProxyOld::mysql_result( $result4, $num4, 'price' );
		$menu_unit2 = Treat_DB_ProxyOld::mysql_result( $result4, $num4, 'unit' );
		$groupid2 = Treat_DB_ProxyOld::mysql_result( $result4, $num4, 'recipe_station' );

		if ( $lastgroupid != $groupid2 ) {

			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query8 = "SELECT station_name FROM servery_station WHERE stationid = '$groupid2'";
			$result8 = Treat_DB_ProxyOld::query($query8);
			//mysql_close();

			$groupname = Treat_DB_ProxyOld::mysql_result( $result8, 0, 'station_name' );
			echo "<option value=0 style=\"background-color:yellow;\">===============================$groupname</option>";
			$lastgroupid=$groupid2;
		}
		else {
			echo "<option value=$menu_item_id2>$item_name2</option>";
			$num4--;
		}

	}
	echo "</select><br><input type=submit value='Add'></form></td>";

	/////ADD NOTE
	echo "<td align=right><a name=add><FORM ACTION=orderadd10.php METHOD=post onSubmit='return disableForm(this);' name=additem8><input type=hidden name=businessid value=$caterbus><input type=hidden name=daypartid value=$curdaypart>";
	echo "<input type=hidden name=username value=$user>";
	echo "<input type=hidden name=password value=$pass>";
	echo "<input type=hidden name=date value=$date>";
	echo "<input type=hidden name=newstart value=$newstart>";
	echo "<input type=hidden name=newmonth value=$newmonth>";
	echo "<input type=hidden name=newyear value=$newyear>";
	echo "<input type=hidden name=edit value=5>";
	echo "<input type=hidden name=weekend value=$weekend>";
	echo "<i>Add Note for</i> <SCRIPT LANGUAGE='JavaScript' ID='js19'> var cal19 = new CalendarPopup('testdiv1');cal19.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date5 VALUE='$adddate' SIZE=8> <A HREF='javascript:void();' onClick=cal19.select(document.forms[1].date5,'anchor19','yyyy-MM-dd'); return false; TITLE=cal19.select(document.forms[1].date5,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor19' ID='anchor19'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A> <i>to</i> <SCRIPT LANGUAGE='JavaScript' ID='js20'> var cal20 = new CalendarPopup('testdiv1');cal20.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date6 VALUE='$adddate' SIZE=8> <A HREF='javascript:void();' onClick=cal20.select(document.forms[1].date6,'anchor20','yyyy-MM-dd'); return false; TITLE=cal20.select(document.forms[1].date6,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor20' ID='anchor20'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A> <i>align</i> <select name=recipe_station><option value=0>Top</option>";

	$query2 = "SELECT * FROM servery_station ORDER BY orderid DESC";
	$result2 = Treat_DB_ProxyOld::query($query2);
	$num2 = Treat_DB_ProxyOld::mysql_num_rows( $result2 );

	$num2--;
	while ( $num2 >= 0 ) {
		$station_name = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'station_name' );
		$stationid = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'stationid' );

		echo "<option value=$stationid\">$station_name</option>";
		$num2--;
	}

	echo "</select><br><textarea name=notes rows=16 cols=60></textarea>";
	echo "<br><input type=submit value='Add'></form></center></a></td></tr>";

	$newstart11 = $newstart;
////////////////////////////////
	echo "<tr bgcolor=#CCCCFF><td colspan=2>";

	echo "<center><table border=1 cellpadding=0 cellspacing=0>";
	echo "<tr bgcolor=#FFFF99><td><center>Sunday</center></td><td><center>Monday</center></td><td><center>Tuesday</center></td><td><center>Wednesday</center></td><td><center>Thursday</center></td><td><center>Friday</center></td><td><center>Saturday</center></td></tr>";
	
	echo "<tr height=110>";
	$weekend = "F";
	while ( $long < 8 )
	{
		if ( $long >= $start )
		{
			if ($long == 1 || $long == 7){$weekend="T";}

			if ($day < 10 && $newmonth < 10){$date = "$newyear-0$newmonth-0$day";}
			elseif ($day < 10 && $newmonth > 10){$date = "$newyear-$newmonth-0$day";}
			elseif ($day > 10 && $newmonth < 10){$date = "$newyear-0$newmonth-$day";}
			else {$date = "$newyear-$newmonth-$day";}

			if ($newstart == ""){$newstart=$start;}
			$correcturl = "orderdate.php?date=$date&newmonth=$newmonth&newyear=$newyear&newstart=$newstart&day=$day&weekend=$weekend";

			if ($day == $todaynum2 && $monthnum == $newmonth && $year == $newyear){$color = "red";}
			else {$color = "black";}

			echo "<td bgcolor=$back width=150 valign=top onMouseOver=this.bgColor='#999999' onMouseOut=this.bgColor='$back'>";
			
			echo "<b><font color=$color>$day</font></b><br>";
			/////////////////////////////////////////////////////////////

			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query2 = "SELECT * FROM caternote WHERE businessid = '$caterbus' AND date <= '$date' AND enddate >= '$date' AND recipe_station = '0'";
			$result2 = Treat_DB_ProxyOld::query($query2);
			$num2 = Treat_DB_ProxyOld::mysql_num_rows( $result2 );

			$num2--;
			while($num2>=0){
				$caternoteid = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'caternoteid' );
				$note = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'note' );

				$note = str_replace("\n","<br>",$note);
				$deletenote = "<a href=orderadd10.php?bid=$caterbus&edit=6&date=$date&newmonth=$newmonth&newyear=$newyear&newstart=$newstart&id=$caternoteid style=$style><sup><b><font color=red>x</font></b></sup></a>";

				if($note!=""){echo "<font size=1 color=blue>$note</font>$deletenote<br>";}
				$num2--;
			}

			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			if($parent>0){$query2 = "SELECT * FROM order_item,menu_items,servery_station WHERE order_item.daypartid = '$curmenu' AND order_item.date = '$date' AND order_item.businessid = '$caterbus' AND order_item.menu_itemid = menu_items.menu_item_id AND menu_items.menu_typeid = '$parent' AND menu_items.recipe_station = servery_station.stationid ORDER BY servery_station.orderid DESC, order_item.orderid DESC";}
			else{$query2 = "SELECT * FROM order_item,menu_items,servery_station WHERE order_item.daypartid = '0' AND order_item.date = '$date' AND order_item.businessid = '$caterbus' AND order_item.menu_itemid = menu_items.menu_item_id AND menu_items.menu_typeid = '$curmenu' AND menu_items.recipe_station = servery_station.stationid ORDER BY servery_station.orderid DESC, order_item.orderid DESC";}
			$result2 = Treat_DB_ProxyOld::query($query2);
			$num2 = Treat_DB_ProxyOld::mysql_num_rows( $result2 );
			//mysql_close();

			$lastrc=-1;
			$num2--;
			while ( $num2 >= 0 ) {
				$order_itemid = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'order_itemid' );
				$prev_itemid = Treat_DB_ProxyOld::mysql_result( $result2, $num2+1, 'order_itemid' );
				$menu_itemid = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'menu_itemid' );
				$orderid = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'orderid' );
				$recipe_station = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'recipe_station' );

				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( "Unable to select database");
				$query3 = "SELECT * FROM menu_items WHERE menu_item_id = '$menu_itemid'";
				$result3 = Treat_DB_ProxyOld::query($query3);
				//mysql_close();

				$item_name = Treat_DB_ProxyOld::mysql_result( $result3, 0, 'item_name' );

				$deleteitem="<a href=orderadd10.php?bid=$caterbus&edit=2&deleteid=$order_itemid&date=$date&number=$number&newmonth=$newmonth&newyear=$newyear&newstart=$newstart&weekend=$weekend&daypartid=$menu_daypartid&direct=1 style=$style><sup><b><font color=red>x</font></b></sup></a>";
				if($prev_itemid>0){$moveitem="<a href=orderadd10.php?bid=$caterbus&edit=3&deleteid=$order_itemid&date=$date&number=$number&newmonth=$newmonth&newyear=$newyear&newstart=$newstart&weekend=$weekend&daypartid=$menu_daypartid&direct=1&prev_itemid=$prev_itemid style=$style><sup><b><font color=green>+</font></b></sup></a>";}
				else{$movitem="";}

				if ( $lastrc != $recipe_station ) {
					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( "Unable to select database");
					$query3 = "SELECT station_name FROM servery_station WHERE stationid = '$recipe_station'";
					$result3 = Treat_DB_ProxyOld::query($query3);
					//mysql_close();

					$station_name = Treat_DB_ProxyOld::mysql_result( $result3, 0, 'station_name' );
					echo "<font size=1 color=green><u>$station_name</u></font><br>";

					$query22 = "SELECT * FROM caternote WHERE businessid = '$caterbus' AND date <= '$date' AND enddate >= '$date' AND recipe_station = '$recipe_station'";
					$result22 = Treat_DB_ProxyOld::query($query22);
					$num22 = Treat_DB_ProxyOld::mysql_num_rows( $result22 );

					$num22--;
					while ( $num22 >= 0 ) {
						$caternoteid = Treat_DB_ProxyOld::mysql_result( $result22, $num22, 'caternoteid' );
						$note = Treat_DB_ProxyOld::mysql_result( $result22, $num22, 'note' );

						$note = str_replace("\n","<br>",$note);
						$deletenote = "<a href=orderadd10.php?bid=$caterbus&edit=6&date=$date&number=$number&newmonth=$newmonth&newyear=$newyear&newstart=$newstart&weekend=$weekend&daypartid=$menu_daypartid&direct=1&id=$caternoteid style=$style><sup><b><font color=red>x</font></b></sup></a>";

						if($note!=""){echo "<font size=1 color=blue>$note</font>$deletenote<br>";}

						$num22--;
					}
				}
				if ($recipe_station==0){echo "<font size=1 color=red>$item_name $deleteitem $moveitem</font><br>";}
				else{echo "<font size=1>$item_name $deleteitem $moveitem</font><br>";}

				$lastrc = $recipe_station;
				$num2--;
			}

			/////////////////////////////////////////////////////////////
			$counter = 0;

			echo "</td>";
			$back = "white";

			if ($day == 1 && $long == 1){$backstart=7;}
			elseif ($day == 1 && $long != 1){$backstart=$long-1;}

			$day++;
		}
		else
		{
			echo "<td width=110 bgcolor=#DCDCDC></td>";
		}
		$weekend = "F";
		$long++;
	}
	echo "</tr>";
	
	while ( $high > 1 )
	{
		$long = 1;
		echo "<tr height=110>";
		$weekend = "F";
		while ( $long < 8 )
		{
			if ($long == 1 || $long == 7){$weekend="T";}

			if ($day < 10 && $newmonth < 10){$date = "$newyear-0$newmonth-0$day";}
			elseif ($day < 10 && $newmonth > 10){$date = "$newyear-$newmonth-0$day";}
			elseif ($day >= 10 && $newmonth < 10){$date = "$newyear-0$newmonth-$day";}
			else {$date = "$newyear-$newmonth-$day";}

			if ($newstart == ""){$newstart=$start;}
			$correcturl = "orderdate.php?date=$date&newmonth=$newmonth&newyear=$newyear&newstart=$newstart&day=$day&weekend=$weekend";

			$show = $day;
			if ($day > 28 && $newmonth == 2 && $leap == 0){$show = "";}
			elseif ($day > 29 && $newmonth == 2 and $leap == 1){$show = "";}
			elseif ($day > 30 && ($newmonth == 4 || $newmonth == 6 || $newmonth == 9 || $newmonth == 11)){$show = "";}
			elseif ($day > 31){$show = "";}
			else {;}
			
			if ($day > 28 && $newmonth == 2 && $leap == 0){$back = '#DCDCDC';}
			elseif ($day > 29 && $newmonth == 2 and $leap == 1){$back = '#DCDCDC';}
			elseif ($day > 30 && ($newmonth == 4 || $newmonth == 6 || $newmonth == 9 || $newmonth == 11)){$back = '#DCDCDC';}
			elseif ($day > 31){$back = '#DCDCDC';}
			else {$back = 'white';}

			if ($day == 29 && $newmonth == 2 && $leap ==0){$newstart=$long;}
			if ($day == 30 && $newmonth == 2 && $leap ==1){$newstart=$long;}
			if ($day == 31 && ($newmonth == 4 || $newmonth == 6 || $newmonth == 9 || $newmonth == 11)){$newstart=$long;}
			if ($day == 32 && ($newmonth == 1 || $newmonth == 3 || $newmonth == 5 || $newmonth == 7 || $newmonth == 8 || $newmonth == 10 || $newmonth == 12)){$newstart=$long;}
		
			if ($day == $todaynum2 && $monthnum == $newmonth && $year == $newyear){$color = "red";}
			else {$color = "black";}

			echo "<td bgcolor=$back width=150 valign=top onMouseOver=this.bgColor='#999999' onMouseOut=this.bgColor='$back'>";
			
			echo "<b><font color=$color>$show</font></b><br>";
			/////////////////////////////////////////////////////////////
			if ( $back != "#DCDCDC" ) {

				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( "Unable to select database");
				$query2 = "SELECT * FROM caternote WHERE businessid = '$caterbus' AND date <= '$date' AND enddate >= '$date' AND recipe_station = '0'";
				$result2 = Treat_DB_ProxyOld::query($query2);
				$num2 = Treat_DB_ProxyOld::mysql_num_rows( $result2 );

				$num2--;
				while ( $num2 >= 0 ) {
					$caternoteid = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'caternoteid' );
					$note = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'note' );

					$note = str_replace("\n","<br>",$note);
					$deletenote = "<a href=orderadd10.php?bid=$caterbus&edit=6&date=$date&newmonth=$newmonth&newyear=$newyear&newstart=$newstart&id=$caternoteid style=$style><sup><b><font color=red>x</font></b></sup></a>";

					if($note!=""){echo "<font size=1 color=blue>$note</font>$deletenote<br>";}
					$num2--;
				}

				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( "Unable to select database");
				if($parent>0){$query2 = "SELECT * FROM order_item,menu_items,servery_station WHERE order_item.daypartid = '$curmenu' AND order_item.date = '$date' AND order_item.businessid = '$caterbus' AND order_item.menu_itemid = menu_items.menu_item_id AND menu_items.menu_typeid = '$parent' AND menu_items.recipe_station = servery_station.stationid ORDER BY servery_station.orderid DESC, order_item.orderid DESC";}
				else{$query2 = "SELECT * FROM order_item,menu_items,servery_station WHERE order_item.daypartid = '0' AND order_item.date = '$date' AND order_item.businessid = '$caterbus' AND order_item.menu_itemid = menu_items.menu_item_id AND menu_items.menu_typeid = '$curmenu' AND menu_items.recipe_station = servery_station.stationid ORDER BY servery_station.orderid DESC, order_item.orderid DESC";}
				$result2 = Treat_DB_ProxyOld::query($query2);
				$num2 = Treat_DB_ProxyOld::mysql_num_rows( $result2 );
				//mysql_close();

				$num2--;
				$lastrc=-1;
				while ( $num2 >= 0 ) {
					$order_itemid = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'order_itemid' );
					$prev_itemid = Treat_DB_ProxyOld::mysql_result( $result2, $num2+1, 'order_itemid' );
					$menu_itemid = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'menu_itemid' );
					$orderid = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'orderid' );
					$recipe_station = Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'recipe_station' );

					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( "Unable to select database");
					$query3 = "SELECT * FROM menu_items WHERE menu_item_id = '$menu_itemid'";
					$result3 = Treat_DB_ProxyOld::query($query3);
					//mysql_close();

					$item_name = Treat_DB_ProxyOld::mysql_result( $result3, 0, 'item_name' );

					$deleteitem="<a href=orderadd10.php?bid=$caterbus&edit=2&deleteid=$order_itemid&date=$date&number=$number&newmonth=$newmonth&newyear=$newyear&newstart=$newstart&weekend=$weekend&daypartid=$menu_daypartid&direct=1 style=$style><sup><b><font color=red>x</font></b></sup></a>";
					if($prev_itemid>0){$moveitem="<a href=orderadd10.php?bid=$caterbus&edit=3&deleteid=$order_itemid&date=$date&number=$number&newmonth=$newmonth&newyear=$newyear&newstart=$newstart&weekend=$weekend&daypartid=$menu_daypartid&direct=1&prev_itemid=$prev_itemid style=$style><sup><b><font color=green>+</font></b></sup></a>";}
					else{$movitem="";}

					if ( $lastrc != $recipe_station ) {
						//mysql_connect($dbhost,$username,$password);
						//@mysql_select_db($database) or die( "Unable to select database");
						$query3 = "SELECT station_name FROM servery_station WHERE stationid = '$recipe_station'";
						$result3 = Treat_DB_ProxyOld::query($query3);
						//mysql_close();

						$station_name = Treat_DB_ProxyOld::mysql_result( $result3, 0, 'station_name' );
						echo "<font size=1 color=green><u>$station_name</u></font><br>";

						$query22 = "SELECT * FROM caternote WHERE businessid = '$caterbus' AND date <= '$date' AND enddate >= '$date' AND recipe_station = '$recipe_station'";
						$result22 = Treat_DB_ProxyOld::query($query22);
						$num22 = @mysql_numrows($result22);

						$num22--;
						while ( $num22 >= 0 ) {
							$caternoteid = Treat_DB_ProxyOld::mysql_result( $result22, $num22, 'caternoteid' );
							$note = Treat_DB_ProxyOld::mysql_result( $result22, $num22, 'note' );

							$note = str_replace("\n","<br>",$note);
							$deletenote = "<a href=orderadd10.php?bid=$caterbus&edit=6&date=$date&number=$number&newmonth=$newmonth&newyear=$newyear&newstart=$newstart&weekend=$weekend&daypartid=$menu_daypartid&direct=1&id=$caternoteid style=$style><sup><b><font color=red>x</font></b></sup></a>";

							if($note!=""){echo "<font size=1 color=blue>$note</font>$deletenote<br>";}

							$num22--;
						}
					}
					if ($recipe_station==0){echo "<font size=1 color=red>$item_name $deleteitem $moveitem</font><br>";}
					else{echo "<font size=1>$item_name $deleteitem $moveitem</font><br>";}

					$lastrc = $recipe_station;
					$num2--;
				}
			}
			/////////////////////////////////////////////////////////////
			$counter = 0;
		
			echo "</td>";
			$back = "white";

			$day++;
			$weekend = "F";
			$long++;
		}
		echo "</tr>";
		$high--;
	}
	echo "</table></center>";

	if ($newmonth == 5 || $newmonth == 7 || $newmonth == 10 || $newmonth == 12){$count=30;}
	elseif ($newmonth == 1 || $newmonth == 2 || $newmonth == 4 || $newmonth == 6 || $newmonth == 8 || $newmonth == 9 || $newmonth == 11){$count=31;}
	elseif ($newmonth == 3 && $leap == 0){$count=28;}
	elseif ($newmonth == 3 && $leap == 1){$count=29;}

	$count2 = $backstart;
	while ($count > 1)
	{
		if ( $count2 == 1 ) {
			$count2 = 8;
		}
		$count--;
		$count2--;
	}
	$backstart = $count2;

	$prevmonth = $newmonth - 1;
	$prevyear = $newyear;
	if ( $prevmonth == 0 ) {
		$prevmonth = 12;
		$prevyear = $year - 1;
	}
	$nextmonth = $newmonth + 1;
	$nextyear = $newyear;
	if ( $nextmonth == 13 ) {
		$nextmonth = 1;
		$nextyear = $year + 1;
	}
	echo "<center><a href='busordermenu10.php?newmonth=$prevmonth&newyear=$prevyear&newstart=$backstart'>Previous Month</a> :: <a href='busordermenu10.php?newmonth=$nextmonth&newyear=$nextyear&newstart=$newstart'>Next Month</a></center><p>";

	echo "</td></tr>";

	$today = date("Y-m-d");

	echo "<tr bgcolor=#CCCCFF><td colspan=2><center><form action=ordermove10.php method=post>";
	echo "<input type=hidden name=businessid value=$caterbus>";
	echo "<input type=hidden name=date value=$date>";
	echo "<input type=hidden name=newstart value=$newstart11>";
	echo "<input type=hidden name=newmonth value=$newmonth>";
	echo "<input type=hidden name=newyear value=$newyear>";
	echo "<input type=hidden name=weekend value=$weekend>";
	echo "<select name=move><option value=1>Move</option><option value=2>Copy</option></select> <input type=text size=2 name=day_num value=5> Days starting <SCRIPT LANGUAGE='JavaScript' ID='js29'> var cal29 = new CalendarPopup('testdiv1');cal29.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date1 VALUE='$today' SIZE=8> <A HREF='javascript:void();' onClick=cal29.select(document.forms[2].date1,'anchor29','yyyy-MM-dd'); return false; TITLE=cal29.select(document.forms[2].date1,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor29' ID='anchor29'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A> to <SCRIPT LANGUAGE='JavaScript' ID='js30'> var cal30 = new CalendarPopup('testdiv1');cal30.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date2 VALUE='$today' SIZE=8> <A HREF='javascript:void();' onClick=cal30.select(document.forms[2].date2,'anchor30','yyyy-MM-dd'); return false; TITLE=cal30.select(document.forms[2].date2,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor30' ID='anchor30'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A> <input type=submit value='GO'></form></center></td></tr>";

	echo "</table><p>";
	
	echo "<form method='get'>";
	echo "<input type='hidden' name='bid' value='$caterbus' /><input type='hidden' name='cid' value='$cid' />";
	echo "<input type='hidden' name='sync_schedule' value='1' /><input type='submit' value='Sync Schedule to Terminal' />";
	echo "</form>";


	/////////////////////////////////////////////////////

	$action2 = "buscatermenu.php?bid=$caterbus&cid=$cid";
	$method = "post";
	echo "<center><FORM ACTION='$action2' method=$method>";
	echo "<input type=hidden name=username value=$user>";
	echo "<input type=hidden name=password value=$pass>";
	echo "<INPUT TYPE=submit VALUE=' Return '></FORM></center><DIV ID=testdiv1 STYLE=position:absolute;visibility:hidden;background-color:white;layer-background-color:white;></DIV></body>";
	
	google_page_track();
}
?>