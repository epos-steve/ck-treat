<?
function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      elseif ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      elseif ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '1')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='31')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;
$leap = date("L");

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   
   elseif ($leap==1&&$month=='03')
   {
      $day=$day+29;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+3);
        if ($diff > 0 && $diff < '0.01'){$diff="0.01";}
        elseif($diff < 0 && $diff > '-0.01'){$diff="-0.01";}
        return $diff;
}


if ( !defined('DOC_ROOT'))
{
	define('DOC_ROOT', dirname(dirname(__FILE__)));
}
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');


$user = isset($_COOKIE["usercook"])?$_COOKIE["usercook"]:'';
$pass = isset($_COOKIE["passcook"])?$_COOKIE["passcook"]:'';
$curmenu = isset($_COOKIE["curmenu"])?$_COOKIE["curmenu"]:'';
$curcust = isset($_COOKIE["curcust"])?$_COOKIE["curcust"]:'';
$businessid = isset($_POST['bid'])?$_POST['bid']:'';
$companyid = isset($_POST['cid'])?$_POST['cid']:'';
$date = isset($_POST['date'])?$_POST['date']:'';
$cur_machine = isset($_POST['cur_machine'])?$_POST['cur_machine']:'';
$order_by_machine = isset($_POST['order_by_machine'])?$_POST['order_by_machine']:'';
$security_level = isset($_POST['security_level'])?$_POST['security_level']:'';
$static_menu = isset($_POST['static_menu'])?$_POST['static_menu']:'';
$startroute = isset($_POST['startroute'])?$_POST['startroute']:'';

if($user==""||$pass==""){
   $user = isset($_POST['username'])?$_POST['username']:'';
   $pass = isset($_POST['password'])?$_POST['password']:'';
}

$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$showtoday = date("F j, Y, g:i a");

/////////////////////////DELIVER,PRODUCTION,ORDER DATES
$day8 = date("d");
$year8 = date("Y");
$month8 = date("m");
$deliver="$year8-$month8-$day8";
$hour = date("H");

$todayname=dayofweek($deliver);

$ordering=$deliver;

if ($todayname=="Monday"&&$hour>=10){$ordering=nextday($ordering);$ordering=nextday($ordering);$ordering=nextday($ordering);}
elseif($todayname=="Monday"){$ordering=nextday($ordering);$ordering=nextday($ordering);}
elseif ($todayname=="Tuesday"&&$hour>=10){$ordering=nextday($ordering);$ordering=nextday($ordering);$ordering=nextday($ordering);}
elseif($todayname=="Tuesday"){$ordering=nextday($ordering);$ordering=nextday($ordering);}
elseif ($todayname=="Wednesday"&&$hour>=10){$ordering=nextday($ordering);$ordering=nextday($ordering);$ordering=nextday($ordering);$ordering=nextday($ordering);$ordering=nextday($ordering);}
elseif($todayname=="Wednesday"){$ordering=nextday($ordering);$ordering=nextday($ordering);}
elseif ($todayname=="Thursday"&&$hour>=10){$ordering=nextday($ordering);$ordering=nextday($ordering);$ordering=nextday($ordering);$ordering=nextday($ordering);$ordering=nextday($ordering);}
elseif($todayname=="Thursday"){$ordering=nextday($ordering);$ordering=nextday($ordering);$ordering=nextday($ordering);$ordering=nextday($ordering);}
elseif ($todayname=="Friday"&&$hour>=10){$ordering=nextday($ordering);$ordering=nextday($ordering);$ordering=nextday($ordering);$ordering=nextday($ordering);$ordering=nextday($ordering);}
elseif($todayname=="Friday"){$ordering=nextday($ordering);$ordering=nextday($ordering);$ordering=nextday($ordering);$ordering=nextday($ordering);}

$todayname=dayofweek($deliver);

$production=$deliver;

if ($todayname=="Monday"&&$hour>=10){$production=nextday($production);$production=nextday($production);}
elseif($todayname=="Monday"){$production=nextday($production);$production=nextday($production);}
elseif ($todayname=="Tuesday"&&$hour>=10){$production=nextday($production);$production=nextday($production);}
elseif($todayname=="Tuesday"){$production=nextday($production);$production=nextday($production);}
elseif ($todayname=="Wednesday"&&$hour>=10){$production=nextday($production);$production=nextday($production);}
elseif($todayname=="Wednesday"){$production=nextday($production);$production=nextday($production);}
elseif ($todayname=="Thursday"&&$hour>=10){$production=nextday($production);$production=nextday($production);$production=nextday($production);$production=nextday($production);}
elseif($todayname=="Thursday"){$production=nextday($production);$production=nextday($production);$production=nextday($production);$production=nextday($production);}
elseif ($todayname=="Friday"&&$hour>=10){$production=nextday($production);$production=nextday($production);$production=nextday($production);$production=nextday($production);}
elseif($todayname=="Friday"){$production=nextday($production);$production=nextday($production);$production=nextday($production);$production=nextday($production);}

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM caterclose WHERE businessid = '$businessid' AND date >= '$deliver' AND date <= '$ordering'";
$result = DB_Proxy::query($query);
$num=mysql_numrows($result);
//mysql_close();

for ($counter=1;$counter<=$num;$counter++){
   $ordering=nextday($ordering);
   $production=nextday($production);
}

if((dayofweek($ordering)=="Saturday"||dayofweek($ordering)=="Sunday")&&$num>0){
   $ordering=nextday($ordering);$ordering=nextday($ordering);
}
if((dayofweek($production)=="Saturday"||dayofweek($production)=="Sunday")&&$num>0){
   $ordering=nextday($production);$ordering=nextday($production);
}
////////////////////////END FIND DATES

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = DB_Proxy::query($query);
$num=mysql_numrows($result);
//mysql_close();

if ($num!=1) 
{
    echo "<center><h4>Failed</h4></center>";
}
elseif ($curcust!=$startroute){echo "<p><br><p><br><center><font size=5>Error: <b>Do Not</b> use your browser's back button.</font></center>";}
elseif ($day > 28 && $newmonth == 2 && $leap == 0){echo "<center>Date does not exist. Use your back button.</center>";}
elseif ($day > 29 && $newmonth == 2 and $leap == 1){echo "<center>Date does not exist. Use your back button.</center>";}
elseif ($day > 30 && ($newmonth == 4 || $newmonth == 6 || $newmonth == 9 || $newmonth == 11)){echo "<center>Date does not exist. Use your back button.</center>";}
elseif ($day > 31){echo "<center>Date does not exist. Use your back button.</center>";}  
else
{
   ///////echo "$today,$deliver,$production,$ordering<br>";
   $location="orderdate3.php?date=$date&newmonth=$newmonth&newyear=$newyear&newstart=$newstart&weekend=$weekend&bid=$businessid&cid=$companyid&cur_machine=$cur_machine";
   header('Location: ./' . $location);

   //mysql_connect($dbhost,$username,$password);
   //@mysql_select_db($database) or die( "Unable to select database");
   $query = "SELECT * FROM login_route WHERE login_routeid = '$curcust'";
   $result = DB_Proxy::query($query);
   $num=mysql_numrows($result);
   //mysql_close();

   $firstname=@mysql_result($result,0,"firstname");
   $lastname=@mysql_result($result,0,"lastname");
   $route=@mysql_result($result,0,"route");
   $locationid=@mysql_result($result,0,"locationid");
   $login_routeid=@mysql_result($result,0,"login_routeid");
   $cstore=@mysql_result($result,0,"cstore");

   //mysql_connect($dbhost,$username,$password);
   //@mysql_select_db($database) or die( "Unable to select database");
   $query = "INSERT INTO audit_vend (login_routeid,user,date) VALUES ('$login_routeid','$user','$date')";
   $result = DB_Proxy::query($query);
   //mysql_close();

   /*
   //mysql_connect($dbhost,$username,$password);
   //@mysql_select_db($database) or die( "Unable to select database");
   $query = "SELECT default_menu FROM business WHERE businessid = '$businessid'";
   $result = DB_Proxy::query($query);
   //mysql_close();
    * */


   //mysql_connect($dbhost,$username,$password);
   //@mysql_select_db($database) or die( "Unable to select database");
   $query = "SELECT * FROM vend_locations WHERE locationid = '$locationid'";
   $result = DB_Proxy::query($query);
   //mysql_close();

   $location_name=@mysql_result($result,0,"location_name");
   $curmenu=@mysql_result($result,0,"menu_typeid");

   /////////////////////////////
   ///menu over ride for kiosk//
   /////////////////////////////
   $query = "SELECT menu_typeid FROM vend_machine WHERE machineid = $cur_machine";
   $result = DB_Proxy::query($query);

   $menu_override = mysql_result($result,0,"menu_typeid");
   if($menu_override > 0){$curmenu = $menu_override;}
   /////////////////////////////
   ////end menu over ride///////
   /////////////////////////////

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM menu_type WHERE menu_typeid = '$curmenu'";
    $result = DB_Proxy::query($query);
    //mysql_close();

    $menuname=@mysql_result($result,0,"menu_typename");
    $static_menu=@mysql_result($result,0,"static_menu");
    $dayname=dayofweek($date);

    $thismonth=substr($date,5,2);
    $nextday=nextday($date);
    $nextmonth=substr($nextday,5,2);
    $prevday=prevday($date);
    $prevmonth=substr($prevday,5,2);

/////////////////////////////////////////////ADD ITEMS

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM menu_groups WHERE menu_typeid = '$curmenu'";
    $result = DB_Proxy::query($query);
    $num=mysql_numrows($result);
    //mysql_close();

    $countme=1;

    $num--;
    while($num>=0){

       $groupname=@mysql_result($result,$num,"groupname");
       $groupid=@mysql_result($result,$num,"menu_group_id");

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM menu_pricegroup WHERE menu_groupid = '$groupid' ORDER BY orderid DESC";
       $result2 = DB_Proxy::query($query2);
       $num2=mysql_numrows($result2);
       //mysql_close();

       $num2--;
       while($num2>=0){

          $pricegroupname=@mysql_result($result2,$num2,"menu_pricegroupname");
          $pricegroupid=@mysql_result($result2,$num2,"menu_pricegroupid");

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
		  if($static_menu==1){$query3 = "SELECT * FROM vend_item WHERE groupid = '$pricegroupid' AND date = '0000-00-00' AND businessid = '$businessid' ORDER BY vend_itemid DESC";}
          else{$query3 = "SELECT * FROM vend_item WHERE groupid = '$pricegroupid' AND date = '$date' AND businessid = '$businessid' ORDER BY vend_itemid DESC";}
          $result3 = DB_Proxy::query($query3);
          $num3=mysql_numrows($result3);
          //mysql_close();

          $num3--;
          $firstnum=$num3;

          while($num3>=0){

             $menu_itemid=@mysql_result($result3,$num3,"menu_itemid");
             $vend_itemid=@mysql_result($result3,$num3,"vend_itemid");

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query4 = "SELECT item_name,price,unit FROM menu_items WHERE menu_item_id = '$menu_itemid'";
             $result4 = DB_Proxy::query($query4);
             //mysql_close();
 
             $menu_itemname=@mysql_result($result4,0,"item_name");
             $menu_itemprice=@mysql_result($result4,0,"price");
             $menu_itemunit=@mysql_result($result4,0,"unit");
             $menu_itemprice=money($menu_itemprice);

             $varname="amt$countme";
             $varname2="ship$countme";
             $varname3="rec$countme";
             $varname4="par$menu_itemid";
             $varname5="count$menu_itemid";

             $amount = isset($_POST[$varname])?$_POST[$varname]:'';
             $shipped = isset($_POST[$varname2])?$_POST[$varname2]:'';
             $received = isset($_POST[$varname3])?$_POST[$varname3]:'';

             ////cstore par level
             if($cstore==1 && $cur_machine == 0){
                 $parlevel = isset($_POST["parlevel"])?$_POST["parlevel"]:'';

                 if($parlevel==1){
                    $par = isset($_POST[$varname4])?$_POST[$varname4]:'';

                    $query5 = "INSERT INTO route_parlevel (login_routeid,menu_itemid,par_level) VALUES ('$curcust','$menu_itemid','$par') ON DUPLICATE KEY UPDATE par_level = '$par'";
                    $result5 = DB_Proxy::query($query5);
                 }

                 $invcount = isset($_POST["invcount"])?$_POST["invcount"]:'';

                 if($invcount==1){
                    $count = isset($_POST[$varname5])?$_POST[$varname5]:'';

                    if($count=="d"){
                        $query5 = "DELETE FROM route_inv WHERE login_routeid = '$login_routeid' AND menu_itemid = '$menu_itemid' ANd date = '$date'";
                        $result5 = DB_Proxy::query($query5);
                    }
                    elseif($count!==""){
                        $query5 = "INSERT INTO route_inv (login_routeid,menu_itemid,date,count) VALUES ('$curcust','$menu_itemid','$date','$count') ON DUPLICATE KEY UPDATE count = '$count'";
                        $result5 = DB_Proxy::query($query5);
                    }
                 }
             }

             ////////////ORDERS ONLY
             //&&$date>=$ordering
             if ($security_level==3){
                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                if($order_by_machine==1){$query4 = "SELECT * FROM vend_order WHERE date = '$date' AND vend_itemid = '$vend_itemid' AND login_routeid = '$curcust' AND machineid = '$cur_machine'";}
				else{$query4 = "SELECT * FROM vend_order WHERE date = '$date' AND vend_itemid = '$vend_itemid' AND login_routeid = '$curcust'";}
                $result4 = DB_Proxy::query($query4);
                $num4=mysql_numrows($result4);
                //mysql_close();

                if ($num4!=0){
                   //mysql_connect($dbhost,$username,$password);
                   //@mysql_select_db($database) or die( "Unable to select database");
				   if($order_by_machine==1){$query5 = "UPDATE vend_order SET amount = '$amount', shipped = '$amount', received = '$amount', locationid = '$locationid' WHERE date = '$date' AND vend_itemid = '$vend_itemid' AND login_routeid = '$login_routeid' AND machineid = '$cur_machine'";}
				   else{$query5 = "UPDATE vend_order SET amount = '$amount', shipped = '$amount', received = '$amount', locationid = '$locationid' WHERE date = '$date' AND vend_itemid = '$vend_itemid' AND login_routeid = '$login_routeid'";}
                   $result5 = DB_Proxy::query($query5);
                   //mysql_close();
                }
                elseif ($amount!=0&&$amount!=""){
                   //mysql_connect($dbhost,$username,$password);
                   //@mysql_select_db($database) or die( "Unable to select database");\
				   if($order_by_machine==1){$query5 = "INSERT INTO vend_order (businessid,amount,shipped,received,date,login_routeid,vend_itemid,locationid,machineid) VALUES ('$businessid','$amount','$amount','$amount','$date','$login_routeid','$vend_itemid','$locationid','$cur_machine')";}
				   else{$query5 = "INSERT INTO vend_order (businessid,amount,shipped,received,date,login_routeid,vend_itemid,locationid) VALUES ('$businessid','$amount','$amount','$amount','$date','$login_routeid','$vend_itemid','$locationid')";}
                   $result5 = DB_Proxy::query($query5);
                   //mysql_close();
                }
             }
             ////SHIP ONLY
             //&&$date>=$production
             elseif ($security_level==1){
                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                if($order_by_machine==1){$query4 = "SELECT * FROM vend_order WHERE date = '$date' AND vend_itemid = '$vend_itemid' AND login_routeid = '$curcust' AND machineid = '$cur_machine'";}
				else{$query4 = "SELECT * FROM vend_order WHERE date = '$date' AND vend_itemid = '$vend_itemid' AND login_routeid = '$curcust'";}
                $result4 = DB_Proxy::query($query4);
                $num4=mysql_numrows($result4);
                //mysql_close();

                if ($num4!=0){
                   //mysql_connect($dbhost,$username,$password);
                   //@mysql_select_db($database) or die( "Unable to select database");
                   if($order_by_machine==1){$query5 = "UPDATE vend_order SET shipped = '$shipped', received = '$shipped', locationid = '$locationid' WHERE date = '$date' AND vend_itemid = '$vend_itemid' AND login_routeid = '$login_routeid' AND machineid = '$cur_machine'";}
				   else{$query5 = "UPDATE vend_order SET shipped = '$shipped', received = '$shipped', locationid = '$locationid' WHERE date = '$date' AND vend_itemid = '$vend_itemid' AND login_routeid = '$login_routeid'";}
                   $result5 = DB_Proxy::query($query5);
                   //mysql_close();
                }
                elseif ($shipped!=0&&$shipped!=""){
                   //mysql_connect($dbhost,$username,$password);
                   //@mysql_select_db($database) or die( "Unable to select database");
                   if($order_by_machine==1){$query5 = "INSERT INTO vend_order (businessid,shipped,received,date,login_routeid,vend_itemid,locationid,machineid) VALUES ('$businessid','$shipped','$shipped','$date','$login_routeid','$vend_itemid','$locationid','$machineid')";}
				   else{$query5 = "INSERT INTO vend_order (businessid,shipped,received,date,login_routeid,vend_itemid,locationid) VALUES ('$businessid','$shipped','$shipped','$date','$login_routeid','$vend_itemid','$locationid')";}
                   $result5 = DB_Proxy::query($query5);
                   //mysql_close();
                }
             }
             //////////RECEIVE ONLY
             elseif ($security_level==2){
                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                if($order_by_machine==1){$query4 = "SELECT * FROM vend_order WHERE date = '$date' AND vend_itemid = '$vend_itemid' AND login_routeid = '$curcust' AND machineid = '$cur_machine'";}
				else{$query4 = "SELECT * FROM vend_order WHERE date = '$date' AND vend_itemid = '$vend_itemid' AND login_routeid = '$curcust'";}
                $result4 = DB_Proxy::query($query4);
                $num4=mysql_numrows($result4);
                //mysql_close();

                if ($num4!=0){
                   //mysql_connect($dbhost,$username,$password);
                   //@mysql_select_db($database) or die( "Unable to select database");
                   if($order_by_machine==1){$query5 = "UPDATE vend_order SET received = '$received', locationid = '$locationid' WHERE date = '$date' AND vend_itemid = '$vend_itemid' AND login_routeid = '$login_routeid' AND machineid = '$cur_machine'";}
				   else{$query5 = "UPDATE vend_order SET received = '$received', locationid = '$locationid' WHERE date = '$date' AND vend_itemid = '$vend_itemid' AND login_routeid = '$login_routeid'";}
                   $result5 = DB_Proxy::query($query5);
                   //mysql_close();
                }
                elseif ($received!=0&&$received!=""){
                   //mysql_connect($dbhost,$username,$password);
                   //@mysql_select_db($database) or die( "Unable to select database");
                   if($order_by_machine==1){$query5 = "INSERT INTO vend_order (businessid,received,date,login_routeid,vend_itemid,locationid,machineid) VALUES ('$businessid','$received','$date','$login_routeid','$vend_itemid','$locationid','$cur_machine')";}
				   else{$query5 = "INSERT INTO vend_order (businessid,received,date,login_routeid,vend_itemid,locationid) VALUES ('$businessid','$received','$date','$login_routeid','$vend_itemid','$locationid')";}
                   $result5 = DB_Proxy::query($query5);
                   //mysql_close();
                }
             }
             /////////EDIT ALL
             elseif ($security_level>=4){
                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                if($order_by_machine==1){$query4 = "SELECT * FROM vend_order WHERE date = '$date' AND vend_itemid = '$vend_itemid' AND login_routeid = '$curcust' AND machineid = '$cur_machine'";}
				else{$query4 = "SELECT * FROM vend_order WHERE date = '$date' AND vend_itemid = '$vend_itemid' AND login_routeid = '$curcust'";}
                $result4 = DB_Proxy::query($query4);
                $num4=mysql_numrows($result4);
                //mysql_close();

                if ($num4!=0){
                   //mysql_connect($dbhost,$username,$password);
                   //@mysql_select_db($database) or die( "Unable to select database");
                   if($order_by_machine==1){$query5 = "UPDATE vend_order SET amount = '$amount', shipped = '$shipped', received = '$received', locationid = '$locationid' WHERE date = '$date' AND vend_itemid = '$vend_itemid' AND login_routeid = '$login_routeid' AND machineid = '$cur_machine'";}
				   else{$query5 = "UPDATE vend_order SET amount = '$amount', shipped = '$shipped', received = '$received', locationid = '$locationid' WHERE date = '$date' AND vend_itemid = '$vend_itemid' AND login_routeid = '$login_routeid'";}
                   $result5 = DB_Proxy::query($query5);
                   //mysql_close();
                }
                elseif ($amount>0||$received>0||$shipped>0){
                   //mysql_connect($dbhost,$username,$password);
                   //@mysql_select_db($database) or die( "Unable to select database");
                   if($order_by_machine==1){$query5 = "INSERT INTO vend_order (businessid,amount,shipped,received,date,login_routeid,vend_itemid,locationid,machineid) VALUES ('$businessid','$amount','$shipped','$received','$date','$login_routeid','$vend_itemid','$locationid','$cur_machine')";}
				   else{$query5 = "INSERT INTO vend_order (businessid,amount,shipped,received,date,login_routeid,vend_itemid,locationid) VALUES ('$businessid','$amount','$shipped','$received','$date','$login_routeid','$vend_itemid','$locationid')";}
                   $result5 = DB_Proxy::query($query5);
                   //mysql_close();
                }
             }

             $num3--;
             $countme++;
          }

          $num2--;
       }
       $num--;
    }
}
//mysql_close();
?>