<?php

if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
require_once( DOC_ROOT . '/bootstrap.php' );

$showrecipes = null; # Notice: Undefined variable
$newcompany = null; # Notice: Undefined variable

$page_values = new Treat_ArrayObject();

$page_values->style = 'text-decoration:none';

$page_values->user = getSessionCookieVariable( 'usercook' );
$page_values->pass = getSessionCookieVariable( 'passcook' );
$page_values->companyid = getPostGetSessionCookieVariable( array( 'companyid', 'cid' ) );
if( $_POST ) {
	$page_values->menuitem = getPostOrGetVariable( 'menuitem' );
	$page_values->recipe_station = getPostOrGetVariable( 'recipe_station' );
	$page_values->requested = getPostOrGetVariable( 'requested' );
	$page_values->unit = getPostOrGetVariable( 'unit' );
	$page_values->which_menu = getPostOrGetVariable( 'which_menu' );
}
else {
	$page_values->menuitem = getSessionCookieVariable( 'menuitem' );
	$page_values->recipe_station = getSessionCookieVariable( 'recipe_station' );
	$page_values->requested = getSessionCookieVariable( 'requested' );
	$page_values->unit = getSessionCookieVariable( 'unit' );
	$page_values->which_menu = getSessionCookieVariable( 'which_menu' );
}

$passed_cookie_login = require_once( realpath( DOC_ROOT . '/../lib/inc/cookie-login.php' ) );
if (
	$passed_cookie_login
/*
	&& (
		(
			$GLOBALS['security_level'] == 1
			&& $GLOBALS['bid'] == $page_business->getBusinessId()
			&& $GLOBALS['cid'] == $page_business->getCompanyId()
		)
		|| (
			$GLOBALS['security_level'] > 1
			&& $GLOBALS['cid'] == $page_business->getCompanyId()
		)
	)
*/
) {
	$loginid = $GLOBALS['loginid'];
	$security_level = $GLOBALS['security_level'];
	$mysecurity = $GLOBALS['mysecurity'];
	$bid = $GLOBALS['bid'];
	$bid2 = $GLOBALS['bid2'];
	$bid3 = $GLOBALS['bid3'];
	$bid4 = $GLOBALS['bid4'];
	$bid5 = $GLOBALS['bid5'];
	$bid6 = $GLOBALS['bid6'];
	$bid7 = $GLOBALS['bid7'];
	$bid8 = $GLOBALS['bid8'];
	$bid9 = $GLOBALS['bid9'];
	$bid10 = $GLOBALS['bid10'];
	$cid = $GLOBALS['cid'];


	setcookie( 'unit', $page_values->unit );
	setcookie( 'menuitem', $page_values->menuitem );
	setcookie( 'recipe_station', $page_values->recipe_station );
	setcookie( 'which_menu', $page_values->which_menu );
	setcookie( 'requested', $page_values->requested );
?>
<html lang="en">
	<head>
		<title>Essential Elements :: Treat America :: comrecipe.php </title>
		<link rel="stylesheet" type="text/css" href="/assets/css/style.css" />
		<link rel="stylesheet" type="text/css" href="/assets/css/ta/generic.css" />
		<link rel="stylesheet" type="text/css" href="/assets/css/ta/recipes.css" />
		<script type="text/javascript" src="/ta/public_smo_scripts.js"></script>
		<script type="text/javascript" src="/assets/js/dynamicdrive-disableform.js"></script>
		<script type="text/javascript" src="/assets/js/jquery-1.4.3.min.js"></script>
		<script type="text/javascript" src="/assets/js/jquery-ui-1.7.2.custom.min.js"></script>
		<script type="text/javascript" src="/assets/js/ta/comrecipe.js"></script>
	</head>
	<body>
<?php
	if ( $GLOBALS['security_level'] <= 2 ) {
		$page_business = Treat_Model_Business_Singleton::getSingleton( $_SESSION->getUser()->getBusinessId( ) );
	}
	
	if ( !$page_business ) {
		$page_business = Treat_Model_Company::getById( $page_values->companyid, 'Treat_ArrayObject' );
	}
	
?>

		<center>
			<table cellspacing="0" cellpadding="0" border="0" width="90%">
				<tr>
					<td colspan="2">
						<a href="/ta/businesstrack.php">
							<img src="/ta/logo.jpg" border="0" height="43" width="205" alt="Site Logo" />
						</a>
						<br />
					</td>
				</tr>
<?php

	$query = "SELECT * FROM security WHERE securityid = '$mysecurity'";
	$result = Treat_DB_ProxyOld::query( $query );
	
	$sec_bus_sales = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_sales' );
	$sec_bus_invoice = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_invoice' );
	$sec_bus_order = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_order' );
	$sec_bus_payable = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_payable' );
	$sec_bus_inventor1 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_inventor1' );
	$sec_bus_control = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_control' );
	$sec_bus_menu = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_menu' );
	$sec_bus_order_setup = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_order_setup' );
	$sec_bus_request = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_request' );
	$sec_route_collection = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'route_collection' );
	$sec_route_labor = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'route_labor' );
	$sec_route_detail = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'route_detail' );
	$sec_bus_labor = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_labor' );
	$sec_nutrition = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'nutrition' );

	//echo "<tr bgcolor=black><td colspan=1 height=1></td></tr>";
?>

				<tr bgcolor="#ccccff">
					<td colspan="1">
						<font size="4">
							<b>
								Recipes - <?php echo $page_business->companyname; ?>
							</b>
						</font>
					</td>
				</tr>
				<tr bgcolor="black">
					<td colspan="1" height="1"></td>
				</tr>
			</table>
		</center>
		<br />

		<form action="/ta/comrecipe.php?<?php
			$query_vals = array(
				'cid=' . $page_business->companyid,
			);
			if ( $page_business->businessid ) {
				$query_vals[] = 'bid=' . $page_business->businessid;
			}
			echo implode( '&amp;', $query_vals );
		?>" method="post" onSubmit="return disableForm(this);">
			<input type="hidden" name="username" value="<?php echo $page_values->user; ?>" />
			<input type="hidden" name="password" value="<?php echo $page_values->pass; ?>" />
			<input type="hidden" name="businessid" value="<?php echo $page_business->businessid; ?>" />
			<input type="hidden" name="companyid" value="<?php echo $page_business->companyid; ?>" />
			<center>
				<table width="90%" border="0" cellspacing="0" cellpadding="0">
					<tr valign="bottom">
						<td>
							<input type="text" size="30" name="menuitem" value="<?php echo $page_values->menuitem; ?>" />
<?php
	$query2 = "
			SELECT *
			FROM servery_station
			ORDER BY station_name
		"
	;
	$result2 = Treat_DB_ProxyOld::query( $query2 );
?>

							<select name="recipe_station">
								<option value="">All Categories</option>
<?php
	while ( $row = Treat_DB_ProxyOld::mysql_fetch_object( $result2 ) ) {
?>

								<option value="<?php
									echo $row->stationid;
								?>"<?php
		if ( $row->stationid == $page_values->recipe_station ) {
			echo ' selected="selected"';
		}
								?>><?php
									echo $row->station_name;
								?></option>
<?php
	}
?>

							</select>
<?php
	////////////////////////////////
	/////////WHICH MENUS////////////
	////////////////////////////////
	$query2 = sprintf( '
			SELECT *
			FROM menu_type
			WHERE companyid = %d
			ORDER BY menu_typename
		'
		,$page_business->companyid
	);
	$result2 = Treat_DB_ProxyOld::query( $query2 );
?>

							<select name="which_menu">
								<option value="">All Menus</option>
<?php
	while ( $row = Treat_DB_ProxyOld::mysql_fetch_object( $result2 ) ) {
?>

								<option value="<?php
									echo $row->menu_typeid;
								?>"<?php
		if ( $row->menu_typeid == $page_values->which_menu ) {
			echo ' selected="selected"';
		}
								?>><?php
									echo $row->menu_typename;
								?></option>
<?php
	}

?>

							</select>
							<select name="unit">
<?php
	if ( $GLOBALS['security_level'] <= 2 ) {
		if ( 1 == $page_business->districtid ) {
			$query2 = "
				SELECT *
				FROM business
				WHERE `businessname` = '[KC] Corporate Recipes'
				ORDER BY businessname
			";
		} elseif (193 == $loginid) {
			$query2 = sprintf( "
					SELECT *
					FROM business
					WHERE `businessname` IN ( '[KC] Corporate Recipes', '[BK] Corporate Recipes')
					ORDER BY businessname"
			);
		}
		else {
			$query2 = sprintf( "
					SELECT *
					FROM business
					WHERE `districtid` = '%d'
					ORDER BY businessname
				"
				,$page_business->districtid
			);
		}
	}
	else {
		$query2 = sprintf ( '
				SELECT *
				FROM business
				WHERE companyid = %d
				ORDER BY businessname
			'
			,$page_business->companyid
		);
?>

									<option value="">All Units</option>
<?php
	}
	$result2 = Treat_DB_ProxyOld::query( $query2 );
	while ( $row = Treat_DB_ProxyOld::mysql_fetch_object( $result2 ) ) {
		if ( $row->businessid == $page_values->unit ) {
			$CHECK = ' selected="selected"';
			$newcompany = $row->companyid;
		}
		else {
			$CHECK = '';
		}

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query3 = sprintf( '
				SELECT *
				FROM recipe r
					JOIN menu_items mi
						ON r.menu_itemid = mi.menu_item_id
					JOIN business b
						ON mi.businessid = b.businessid
				WHERE b.businessid = %d
					AND mi.recipe_active = 0
			'
			,$row->businessid
		);
		$result3 = Treat_DB_ProxyOld::query( $query3 );
		$num3 = Treat_DB_ProxyOld::mysql_numrows( $result3 );
		//mysql_close();

		if ( $num3 > 0 ) {
?>
								<option value="<?php
									echo $row->businessid;
								?>"<?php
									echo $CHECK;
								?>><?php
									echo $row->businessname,' ', $showrecipes;
								?></option>
<?php
		}
	}

?>

							</select>
<?php

	////corporate recipes
	if ( $sec_nutrition > 0 ) {
		if ( $page_values->requested  ==  1 ) {
			$showreq = ' checked="checked"';
			$showcolor = '#ffff99';
		}
		else {
			$showreq = "";
			$showcolor = '#ffffff';
		}

?>

							<label style="background: <?php
								echo $showcolor;
							?>;border: 1px solid black;">
								<input type="checkbox" name="requested" value="1"<?php
									echo $showreq;
								?> />Requested Corporate Recipes
							</label>
<?php
	}
	////end corporate recipes

?>

							<input type="submit" value="Search" />
						</td>
					</tr>
					<tr bgcolor="white">
						<td height="10"></td>
					</tr>
				</table>
			</center>
		</form>
		<form action="/ta/copybusrecipes.php" method="post" class="list_of_recipes">
			<center>
				<input type="hidden" name="companyid" value="<?php echo $page_business->companyid; ?>" />
				<table width="90%" border="0" cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<th width="31%" class="item_name">
								<input type="checkbox" name="checkall" onclick="checkUncheckAll(this);" />
								Item
							</th>
							<th width="16%" class="menu_name">
								Menu
							</th>
							<th width="16%" class="station_name">
								Category
							</th>
							<th width="7%" class="showrating">
								Rating
							</th>
							<th width="7%" class="showrating2">
								Prep
							</th>
							<th width="9%" class="item_district">
								District
							</th>
							<th width="9%" class="item_unit">
								Unit
							</th>
							<?php if ( $page_values->requested == 1 && $sec_nutrition > 0 ) {?>
								<th width="5%" class="item_buttons">
									&nbsp;
								</th>
							<?php } ?>
						</tr>
					</thead>
					<tbody>
<?php
	
	$totalcount=0;
	if ( $page_values->unit == '' && $page_values->menuitem == '' && $page_values->recipe_station == '' && $page_values->requested != 1 ) {
	}
	else {

		$findme = '';
		$pieces = explode( ',', $page_values->menuitem, 10 );
		$item_names = array();
		foreach ( $pieces as $piece ) {
			$item_names[] = sprintf(
				'item_name LIKE "%%%1$s%%"'
				,$piece
			);
		}
		if ( $item_names ) {
			$findme = implode( ' OR ', $item_names );
		}

		if ( $page_values->recipe_station > 0 ) {
			$findcat = sprintf(
				"AND recipe_station = '%s'"
				,$page_values->recipe_station
			);
		}
		else {
			$findcat = '';
		}

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");

		if ( $page_values->which_menu > 0 ) {
			$myquery = sprintf(
				"AND menu_typeid = '%s'"
				,$page_values->which_menu
			);
		}
		else {
			$myquery = '';
		}

		if ( $page_values->requested  ==  1 ) {
			$query2 = sprintf( "
					SELECT *
					FROM vMenuItems_District
					WHERE companyid  = '%d'
						AND keeper > 0
					ORDER BY item_name
				"
				,$page_business->companyid
			);
		}
		elseif ( $page_values->unit == '' && $page_values->recipe_station == '' ) {
			$query2 = sprintf( '
					SELECT *
					FROM vMenuItems_District
					WHERE (%s)
						AND recipe_active = "0"
						%s
						AND companyid  = "%d"
						%s
					ORDER BY item_name
				'
				,$findme
				,$findcat
				,$page_business->companyid
				,$myquery
			);
		}
		elseif ( $page_values->unit == '' && $page_values->recipe_station > 0 && $page_values->menuitem == '' ) {
			$query2 = sprintf( '
					SELECT *
					FROM vMenuItems_District
					WHERE recipe_active = "0"
						%s
						AND companyid  = "%d"
						%s
					ORDER BY item_name
				'
				,$findcat
				,$page_business->companyid
				,$myquery
			);
		}
		elseif ( $page_values->unit == '' && $page_values->recipe_station > 0 && $page_values->menuitem != '' ) {
			$query2 = sprintf( '
					SELECT *
					FROM vMenuItems_District
					WHERE (%s)
						AND recipe_active = "0"
						%s
						AND companyid  = "%d"
						%s
					ORDER BY item_name
				'
				,$findme
				,$findcat
				,$page_business->companyid
				,$myquery
			);
		}
		elseif ( $page_values->unit != '' && $page_values->menuitem == '' ) {
			$query2 = sprintf( '
					SELECT *
					FROM vMenuItems_District
					WHERE businessid = "%1$s"
						AND recipe_active = "0"
						%2$s
						AND companyid  = "%3$s"
						%4$s
					ORDER BY item_name
				'
				,$page_values->unit
				,$findcat
				,$page_business->companyid
				,$myquery
			);
		}
		else {
			$query2 = sprintf( '
					SELECT *
					FROM vMenuItems_District
					WHERE (%1$s)
						AND businessid = "%2$s"
						AND recipe_active = "0"
						%3$s
						AND companyid = "%4$s"
						%5$s
					ORDER BY item_name
				'
				,$findme
				,$page_values->unit
				,$findcat
				,$page_business->companyid
				,$myquery
			);
		}

		$result2 = Treat_DB_ProxyOld::query( $query2 );
#		echo '<pre>';
#		echo '$query2 = ';
#		var_dump( $query2 );
#		echo '</pre>';
		$num2 = @Treat_DB_ProxyOld::mysql_numrows( $result2 );
		//mysql_close();

		$rec_counter=0;
		$num2--;
		while ( $vMenuItems_District = @Treat_DB_ProxyOld::mysql_fetch_object( $result2 ) ) {
			$query3 = sprintf( '
					SELECT *
					FROM recipe
					WHERE menu_itemid = "%d"
				'
				,$vMenuItems_District->menu_item_id
			);
			$result3 = Treat_DB_ProxyOld::query( $query3 );
			$num3 = Treat_DB_ProxyOld::mysql_numrows( $result3 );

			if ( $num3 != 0 ) {
				/////////////RECIPE RATING
				$query81 = sprintf( '
						SELECT SUM(rating) AS totalamt
						FROM recipe_rating
						WHERE menu_itemid = "%d"
					'
					,$vMenuItems_District->menu_item_id
				);
				$result81 = Treat_DB_ProxyOld::query( $query81 );

				$item_rating = @Treat_DB_ProxyOld::mysql_result( $result81, 0, 'totalamt' );

				$query81 = sprintf( '
						SELECT recipe_ratingid
						FROM recipe_rating
						WHERE menu_itemid = "%d"
					'
					,$vMenuItems_District->menu_item_id
				);
				$result81 = Treat_DB_ProxyOld::query( $query81 );
				$num81 = Treat_DB_ProxyOld::mysql_numrows( $result81 );
				
				//$num81 = @mysql_result( $result3, 0, 'count_rating' );
				
				$rating = @round( $item_rating / $num81, 1 );
				$finalrating = $rating;

				$showrating = '';
				while ( $rating >= 1 ) {
					$showrating = "$showrating<img src=star.gif height=16 width=16 valign=bottom alt='$finalrating'>";
					$rating = $rating - 1;
				}
				if ( $rating >= .5 ) {
					$showrating = "$showrating<img src=halfstar.gif valign=bottom height=16 width=8>";
				}
				if ( $num81 > 0 ) {
					$showrating = "$showrating <font size=2>($num81)</font>";
				}
		
				///////////RECIPE PREP
				$query81 = sprintf( '
						SELECT SUM(rating) AS totalamt
						FROM recipe_prep
						WHERE menu_itemid = "%d"
					'
					,$vMenuItems_District->menu_item_id
				);
				$result81 = Treat_DB_ProxyOld::query( $query81 );

				$item_prep = @Treat_DB_ProxyOld::mysql_result( $result81, 0, 'totalamt' );

				$query81 = sprintf( '
						SELECT recipe_prepid
						FROM recipe_prep
						WHERE menu_itemid = "%d"
					'
					,$vMenuItems_District->menu_item_id
				);
				$result81 = Treat_DB_ProxyOld::query( $query81 );
				$num81 = Treat_DB_ProxyOld::mysql_numrows( $result81 );
				
				//$num81 = @mysql_result( $result3, 0, 'count_prep' );
				
				$rating2 = @round( $item_prep / $num81, 1 );

				if ( $rating2 >= 3.5 ) {
					$showrating2 = '<img src="/ta/recipe_great.gif" height="15" width="15" border="0" alt="Minimal Preparation" />';
				}
				elseif ( $rating2 < 3.5 && $rating2 >= 1.9 ) {
					$showrating2 = '<img src="/ta/recipe_good.gif" height="15" width="15" border="0" alt="Some Preparation" />';
				}
				elseif ( $rating2 < 1.9 && $rating2 > 0 ) {
					$showrating2 = '<img src="/ta/recipe_hard.gif" height="15" width="15" border="0" alt="Much Preparation" />';
				}
				else {
					$showrating2 = '';
				}

				if ( $num81 > 0 ) {
					$showrating2 = "$showrating2 <font size = 2>($num81)</font>";
				}

?>

						<tr class="menu_recipe">
							<td class="item_name">
								<input type="checkbox" name="menu_recipes[<?php
									echo $vMenuItems_District->menu_item_id;
								?>]" id="menu_recipes_<?php
									echo $vMenuItems_District->menu_item_id;
								?>" value="<?php echo $vMenuItems_District->menu_item_id; ?>" />
								<a href="/ta/comviewrecipe.php?editid=<?php
									echo $vMenuItems_District->menu_item_id;
								?>&amp;cid=<?php
									echo $page_business->companyid;
								?>" style="<?php
									echo $page_values->style;
								?>"><?php echo $vMenuItems_District->item_name; ?></a>
							</td>
							<td class="menu_name">
								<label for="menu_recipes_<?php
									echo $vMenuItems_District->menu_item_id;
								?>">
									<?php echo $vMenuItems_District->menu_typename; ?>
								</label>
							</td>
							<td class="station_name">
								<label for="menu_recipes_<?php
									echo $vMenuItems_District->menu_item_id;
								?>">
									<?php echo $vMenuItems_District->station_name ? $vMenuItems_District->station_name : '&nbsp;'; ?>
								</label>
							</td>
							<td class="showrating">
								<label for="menu_recipes_<?php
									echo $vMenuItems_District->menu_item_id;
								?>">
									<?php echo $showrating ? $showrating : '&nbsp;'; ?>
								</label>
							</td>
							<td class="showrating2">
								<label for="menu_recipes_<?php
									echo $vMenuItems_District->menu_item_id;
								?>">
									<?php echo $showrating2 ? $showrating2 : '&nbsp;'; ?>
								</label>
							</td>
							<td class="item_district">
								<label for="menu_recipes_<?php
									echo $vMenuItems_District->menu_item_id;
								?>">
									<?php echo $vMenuItems_District->districtname ?: '&nbsp;'; ?>
								</label>
							</td>
							<td class="item_unit">
								<label for="menu_recipes_<?php
									echo $vMenuItems_District->menu_item_id;
								?>">
									<?php echo $vMenuItems_District->businessname ? $vMenuItems_District->businessname : '&nbsp;'; ?>
								</label>
							</td>
								<?php
				/////reject recipes
				if ( $page_values->requested == 1 && $sec_nutrition > 0 ) {
?>

							<td class="item_buttons">
								<a href="/ta/businventor4.php?cid=<?php
									echo $vMenuItems_District->companyid;
								?>&amp;bid=<?php
									echo $vMenuItems_District->businessid;
								?>&amp;editid=<?php
									echo $vMenuItems_District->menu_item_id;
								?>" target='_blank'><img
									src="/assets/images/recipe/recipe.gif"
									border="0"
									alt="Edit Recipe"
									title="Edit Recipe"
								/></a>

								&nbsp;
								<span
									id="id<?php echo $vMenuItems_District->menu_item_id; ?>"
									onclick="reject_item( <?php echo $vMenuItems_District->menu_item_id; ?> );"
									style="cursor: pointer;"
									class="keeper_<?php echo $vMenuItems_District->keeper; ?>">
										<?php if( $vMenuItems_District->keeper == 1 ) { ?>
											<img src="/ta/po1.gif" title="Pending" alt="Pending" />
											<span />
										<?php } else { 
											$keepArray = array( 2 => 'NF', 3 => 'JE', 4 => 'JK' );
											?>
											<img src="" />
											<span><?php echo $keepArray[ $vMenuItems_District->keeper ]; ?></span>
										<?php } ?>
								</span>
								&nbsp;
							</td>
<?php
				}
				/////end reject
								?>
						</tr>
<?php
				$totalcount++;
				$rec_counter++;
			}

			$num2--;
		}
		if ( $totalcount == 0 ) {
?>

						<tr>
							<td colspan="8">
								<i>No Items</i>
							</td>
						</tr>
<?php
		}
?>

						<tr bgcolor="black">
							<td height="1" colspan="8"></td>
						</tr>


						<tr>
<?php
		
	////////////////COPY RECIPES!!!!!!!!!!!!!!!!!!!!!
		if ( $totalcount != 0 && $sec_bus_inventor1 > 0 ) {
?>

							<td colspan="5">
								&nbsp;&nbsp;
								<img src="/ta/turnarrow.gif" height="16" width="16" alt="turnarrow.gif" />
<?php
			if ( $security_level < 9 ) {
				$copy_disable = ' disabled="disabled"';
			}

			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			if ( $security_level > 6 ) {
				$query = sprintf( '
						SELECT *
						FROM business
						WHERE companyid  = "%d"
						ORDER BY businessname
					'
					,$page_business->companyid
				);
			}
			elseif ( $security_level == 2 || $security_level == 1 ) {
				$query = sprintf( '
						SELECT *
						FROM business
						WHERE companyid = "%d"
							AND businessid IN ( %d, %d, %d, %d, %d, %d, %d, %d, %d, %d )
						ORDER BY businessname
					'
					,$page_business->companyid
					,$_SESSION->getUser()->businessid
					,$_SESSION->getUser()->busid2
					,$_SESSION->getUser()->busid3
					,$_SESSION->getUser()->busid4
					,$_SESSION->getUser()->busid5
					,$_SESSION->getUser()->busid6
					,$_SESSION->getUser()->busid7
					,$_SESSION->getUser()->busid8
					,$_SESSION->getUser()->busid9
					,$_SESSION->getUser()->busid10
				);
			}
			elseif ( $security_level > 2 && $security_level < 7 ) {
				$query = sprintf( "
						SELECT
							b.*
						FROM business b
							JOIN login_district ld
								USING( districtid )
						WHERE b.companyid  = '%d'
							AND ld.loginid = '%d'
						ORDER BY b.businessname
					"
					,$page_business->companyid
					,$loginid
				);
			}
			$result = Treat_DB_ProxyOld::query( $query );
			$num = Treat_DB_ProxyOld::mysql_numrows( $result );
			//mysql_close();

?>

								Copy Selected Recipes to
								<select name="businessid">
<?php
			$num--;
#			while ( $num >= 0 ) {
			while ( $business = @Treat_DB_ProxyOld::mysql_fetch_object( $result ) ) {
#				$businessname = @mysql_result( $result, $num, 'businessname' );
#				$busid = @mysql_result( $result, $num, 'businessid' );

?>

									<option value="<?php
										echo $business->businessid;
									?>"<?php
										if ( $_SESSION->getUser()->businessid == $business->businessid ) {
											echo ' selected="selected"';
										}
									?>><?php
										echo $business->businessname;
									?></option>
<?php
				$num--;
			}
?>

								</select>
								<input type="hidden" name="rec_counter" value="<?php echo $rec_counter; ?>" />
								<input type="hidden" name="newcompany" value="<?php echo $newcompany; ?>" />
								<input type="submit" value="GO" />
<?php
		}
		else {
?>

							<td colspan="5"></td>
<?php
		}

		if ( $totalcount != 0 ) {
?>
							<td colspan="3" align="right">
								<i>Results: <?php echo $totalcount; ?></i>
							</td>
<?php
		}
		else {
?>
							<td colspan="3"></td>
<?php
		}
	}
	
?>
						</tr>
					</tbody>
				</table>
			</center>
		</form>
<?php
////////////////////////
	//mysql_close();

?>

		<br/>
		<form action="/ta/businesstrack.php" method="post">
			<center>
				<input type="hidden" name="username" value="<?php echo $page_values->user; ?>" />
				<input type="hidden" name="password" value="<?php echo $page_values->pass; ?>" />
				<input type="submit" VALUE=" Return to Main Page" />
			</center>
		</form>
<?php
	
	google_page_track();
}
?>
	</body>
</html>