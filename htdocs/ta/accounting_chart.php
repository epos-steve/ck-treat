<?php

////////Display Account Chart Month to Month

define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');
$style = "text-decoration:none";

/*$companyid=$_GET["cid"];
$month=$_GET["month"];
$year=$_GET["year"];*/

$companyid = \EE\Controller\Base::getGetVariable('cid');
$month = \EE\Controller\Base::getGetVariable('month');
$year = \EE\Controller\Base::getGetVariable('year');

if($month==""||$year==""){
    $year=date("Y");
    $month=date("m");
}

////dates
$today=date("Y-m-d");
$date="$year-$month-01";
$enddate=date("Y-m-t", strtotime($date));
$monthname = date("F", strtotime($date));

///next and prev month
$prevyear=$year;
$nextyear=$year;

$prevmonth=$month-1;
if($prevmonth==0){$prevmonth=12;$prevyear--;}
while(strlen($prevmonth)<2){$prevmonth="0$prevmonth";}

$nextmonth=$month+1;
if($nextmonth==13){$nextmonth="01";$nextyear++;}
while(strlen($nextmonth)<2){$nextmonth="0$nextmonth";}

///display
?>
<html>
    <head></head>
    <body>
        <? echo "<center><a href=accounting_chart.php?cid=$companyid&year=$prevyear&month=$prevmonth style=$style><font color=blue size=2>[PREV]</font></a> <b>$monthname $year</b> <a href=accounting_chart.php?cid=$companyid&year=$nextyear&month=$nextmonth style=$style><font color=blue size=2>[NEXT]</font></a></center>"; ?>
        <table width="100%" style="border:2px solid #E8E7E7;">
               <tr bgcolor="#E8E7E7"><td colspan="2"><i><b>Business</b></i></td></tr>

<?php
$lastdistrict=0;

$query = "SELECT * FROM business WHERE companyid = '$companyid' ORDER BY districtid,businessname";
//$result = DB_Proxy::query($query);
$result = Treat_DB_ProxyOld::query($query);

//while ($r=mysql_fetch_array($result)){
while ($r=Treat_DB_ProxyOld::mysql_fetch_array($result)){
    $businessid = $r["businessid"];
    $businessname = $r["businessname"];
    $unit = $r["unit"];
    $districtid = $r["districtid"];

    /////display districts
    if($districtid!=$lastdistrict){
        $query2 = "SELECT districtname FROM district WHERE districtid = '$districtid'";
        $result2 = Treat_DB_ProxyOld::query($query2);

        $districtname = Treat_DB_ProxyOld::mysql_result($result2,0,"districtname");
        echo "<tr bgcolor=#CCCCFF><td colspan=2><i><b>$districtname</i></b></td></tr>";
    }

    ////display business
    echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor='white'><td width=35%>$businessname</td><td align=right>";

    $tempdate=$date;
    $graph="<table><tr>";
    while($tempdate <= $enddate){
        $query2 = "SELECT closed,closed_reason FROM stats WHERE businessid = '$businessid' AND date = '$tempdate'";
        $result2 = Treat_DB_ProxyOld::query($query2);

        $closed = Treat_DB_ProxyOld::mysql_result($result2,0,"closed");
        $closed_reason = Treat_DB_ProxyOld::mysql_result($result2,0,"closed_reason");

        $query2 = "SELECT export FROM submit WHERE businessid = '$businessid' AND date = '$tempdate'";
        $result2 = Treat_DB_ProxyOld::query($query2);
        $num2 = Treat_DB_ProxyOld::mysql_numrows($result2);

        $export = Treat_DB_ProxyOld::mysql_result($result2,0,"export");

        ////create chart
        $fontcolor="black";
        if($closed == 1){
            $color="red";
        }
        elseif($export == 1){
            $color="#444455";
            $fontcolor="white";
        }
        elseif($num2 == 1){
            $color="#00FF00";
        }
        else{
            $color="#00CCFF";
        }

        $day=substr($tempdate,8,2)*1;
        $dayname=date("l", strtotime($tempdate));

        if($closed_reason != ""){$dayname.=": $closed_reason";}

        if($tempdate==$today){$showbold="font-weight:bold";$fontsize=1;$showborder="border:1px solid #0000A0;";}
        else{$showbold="font-weight:normal";$fontsize=1;$showborder="border:0px solid white;";}

        $graph.="<td width=15 height=15 style='$showborder' bgcolor=$color title='$dayname'><center><font size=$fontsize style=$showbold; color=$fontcolor>$day</font></center></td>";

        $tempdate=nextday($tempdate);
    }
    $graph.="</tr></table>";
    echo "$graph</td></tr>";

    $lastdistrict=$districtid;
}
?>
        </table>
    </body>
</html>