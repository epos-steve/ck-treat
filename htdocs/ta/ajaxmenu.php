<?php
mb_internal_encoding("UTF-8");
mb_http_output("UTF-8");
ob_start("mb_output_handler");

define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');

header ("Content-type: text/javascript");


function money($diff){
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+3);
        if ($diff > 0 && $diff < '0.01'){$diff="0.01";}
        elseif($diff < 0 && $diff > '-0.01'){$diff="-0.01";}
        return $diff;
}

/*mysql_connect($dbhost, $username, $password);
mysql_select_db($database);*/

/*$m = $_POST['method'];
$row = $_POST['row'];
$table = $_POST['table'];
$id = $_POST['id'];
$bid = $_POST['bid'];
$user = isset($_COOKIE["usercook"])?$_COOKIE["usercook"]:'';*/

$m = \EE\Controller\Base::getPostVariable('method');
$row = \EE\Controller\Base::getPostVariable('row');
$table = \EE\Controller\Base::getPostVariable('table');
$id = \EE\Controller\Base::getPostVariable('id');
$bid = \EE\Controller\Base::getPostVariable('bid');
$user = \EE\Controller\Base::getSessionCookieVariable('usercook','');

switch($m){

case 'toggle_active':
	$menu_item_id = $_POST['menu_item_id'];
	//$val = $_POST['val'];
	$val = \EE\Controller\Base::getPostVariable('val');

	$query = "UPDATE menu_items SET deleted = '$val' WHERE menu_item_id = '$menu_item_id'";
        $result = Treat_DB_ProxyOld::mysql_query($query);

	break;

case 'toggle_active_inv':
	/*$inv_itemid = $_POST['inv_itemid'];
	$val = $_POST['val'];*/
	
	$inv_itemid = \EE\Controller\Base::getPostVariable('inv_itemid');
	$val = \EE\Controller\Base::getPostVariable('val');

	$query = "UPDATE inv_items SET active = '$val' WHERE inv_itemid = '$inv_itemid'";
        $result = Treat_DB_ProxyOld::query($query);

	$query = "UPDATE inv_areadetail SET active = '$val' WHERE inv_itemid = '$inv_itemid'";
        $result = Treat_DB_ProxyOld::query($query);

	break;

case 'del_menu_item':
	//$menu_item_id = $_POST['menu_item_id'];
	$menu_item_id = \EE\Controller\Base::getPostVariable('menu_item_id');

	$query = "DELETE FROM menu_items WHERE menu_item_id = '$menu_item_id'";
        $result = Treat_DB_ProxyOld::query($query);

        $query = str_replace("'","",$query);

        $query2 = "INSERT INTO audit_menu_items (user, myquery) VALUES ('$user','$query')";
        $result2 = Treat_DB_ProxyOld::query($query2);

	break;

case 'del_inv_item':
	//$inv_itemid = $_POST['inv_itemid'];
	$inv_itemid = \EE\Controller\Base::getPostVariable('inv_itemid');

	$query = "DELETE FROM inv_items WHERE inv_itemid = '$inv_itemid'";
        $result = Treat_DB_ProxyOld::query($query);

	$query = "DELETE FROM inv_areadetail WHERE inv_itemid = '$inv_itemid'";
        $result = Treat_DB_ProxyOld::query($query);

        $query = "DELETE FROM inv_count WHERE inv_itemid = '$inv_itemid'";
        $result = Treat_DB_ProxyOld::query($query);

	$query = "DELETE FROM recipe WHERE inv_itemid = '$inv_itemid'";
        $result = Treat_DB_ProxyOld::query($query);

	break;

case 'edit_menu_item':

	/*$menu_item_id = $_POST['menu_item_id'];
	$menu_counter = $_POST['menu_counter'];*/
	
	$menu_item_id = \EE\Controller\Base::getPostVariable('menu_item_id');
	$menu_counter = \EE\Controller\Base::getPostVariable('menu_counter');

	$str="<center><iframe src=/ta/editcateritem2.php?mid=$menu_item_id width=95% height=100% id=\'form$menu_item_id\' frameborder=0 style=\"border:2px solid orange;\"></iframe><br><div style=\"background: orange; width: 95%; text-align: right;\"><input type=button value=Close onclick=\"close_menu_item($menu_item_id,$menu_counter);\"></center>";

	echo "$('reportFloater').update('$str'); ";

	break;

case 'update_menu_item':

	//$menu_item_id = $_POST['menu_item_id'];
	$menu_item_id = \EE\Controller\Base::getPostVariable('menu_item_id');

	$query = "SELECT item_name,price,deleted FROM menu_items WHERE menu_item_id = '$menu_item_id'";
    $result = Treat_DB_ProxyOld::query($query);

	$item_name=@Treat_DB_ProxyOld::mysql_result($result,0,"item_name");
	$price=@Treat_DB_ProxyOld::mysql_result($result,0,"price");
	$active=@Treat_DB_ProxyOld::mysql_result($result,0,"deleted");

	echo "$('imgname$menu_item_id').update('$item_name'); ";
	if($active==1){echo "$('imgname$menu_item_id').style.textDecoration = 'line-through'; ";}
	else{echo "$('imgname$menu_item_id').style.textDecoration = 'none'; ";}

	$price=money($price);
	echo "$('price$menu_item_id').value = $price; ";

	$str = "<div></div>";
	echo "$('reportFloater').update('$str'); ";

	break;
}

//mysql_close();
?>
