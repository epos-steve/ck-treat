<?php

function money($diff){
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+3);
        if ($diff > 0 && $diff < '0.01'){$diff="0.01";}
        elseif($diff < 0 && $diff > '-0.01'){$diff="-0.01";}
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      if ($month == "10"){$month="11";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='32')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='08' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      $day=$day-31;
      if ($day<10){$day="0$day";}
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";}
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";}
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";}
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";}
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

define('DOC_ROOT', dirname(dirname(__FILE__)));
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');

$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");

$user=$_COOKIE["usercook"];
$pass=$_COOKIE["passcook"];
$businessid=$_GET["bid"];
$companyid=$_GET["cid"];
$showtrans=$_GET["showtrans"];
$date1=$_POST["date1"];
$date2=$_POST["date2"];
if ($date1==""){
   $date1=$_COOKIE["date1cook"];
   $date2=$_COOKIE["date2cook"];
}
if ($date2==""){$date2=$today;}
if ($showtrans==""){$showtrans=$_COOKIE["showtranscook"];}

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=mysql_numrows($result);
mysql_close();

$security_level=mysql_result($result,0,"security_level");
$bid=mysql_result($result,0,"businessid");
$cid=mysql_result($result,0,"companyid");

if ($num != 1 || ($security_level==1 && ($bid != $businessid || $cid != $companyid)) || ($security_level > 1 AND $cid != $companyid) || $user == "" || $pass == "")
{
    echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
    setcookie("showtranscook",$showtrans);
    setcookie("date1cook",$date1);
    setcookie("date2cook",$date2);

?>
<html>
<head>
<SCRIPT LANGUAGE="JavaScript" SRC="CalendarPopup.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">document.write(getCalendarStyles());</SCRIPT>

<STYLE>
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation
			{
			background-color:#6677DD;
			text-align:center;
			vertical-align:center;
			text-decoration:none;
			color:#FFFFFF;
			font-weight:bold;
			}
	.TESTcpDayColumnHeader,
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation,
	.TESTcpCurrentMonthDate,
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDate,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDate,
	.TESTcpCurrentDateDisabled,
	.TESTcpTodayText,
	.TESTcpTodayTextDisabled,
	.TESTcpText
			{
			font-family:arial;
			font-size:8pt;
			}
	TD.TESTcpDayColumnHeader
			{
			text-align:right;
			border:solid thin #6677DD;
			border-width:0 0 1 0;
			}
	.TESTcpCurrentMonthDate,
	.TESTcpOtherMonthDate,
	.TESTcpCurrentDate
			{
			text-align:right;
			text-decoration:none;
			}
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDateDisabled
			{
			color:#D0D0D0;
			text-align:right;
			text-decoration:line-through;
			}
	.TESTcpCurrentMonthDate
			{
			color:#6677DD;
			font-weight:bold;
			}
	.TESTcpCurrentDate
			{
			color: #FFFFFF;
			font-weight:bold;
			}
	.TESTcpOtherMonthDate
			{
			color:#808080;
			}
	TD.TESTcpCurrentDate
			{
			color:#FFFFFF;
			background-color: #6677DD;
			border-width:1;
			border:solid thin #000000;
			}
	TD.TESTcpCurrentDateDisabled
			{
			border-width:1;
			border:solid thin #FFAAAA;
			}
	TD.TESTcpTodayText,
	TD.TESTcpTodayTextDisabled
			{
			border:solid thin #6677DD;
			border-width:1 0 0 0;
			}
	A.TESTcpTodayText,
	SPAN.TESTcpTodayTextDisabled
			{
			height:20px;
			}
	A.TESTcpTodayText
			{
			color:#6677DD;
			font-weight:bold;
			}
	SPAN.TESTcpTodayTextDisabled
			{
			color:#D0D0D0;
			}
	.TESTcpBorder
			{
			border:solid thin #6677DD;
			}
</STYLE>
</head>

<?php

    echo "<body>";
    echo "<div style=border:50px solid red;padding:10px;>";
    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%><tr><td colspan=2>";
    echo "<a href=businesstrack.php><img src=logo.jpg border=0 height=43 width=205></a></td></tr></table><p>";

    $query = "SELECT * FROM company WHERE companyid = '$companyid'";
    $result = Treat_DB_ProxyOld::query($query);

    $companyname=mysql_result($result,0,"companyname");

    echo "<center><table width=90% cellspacing=0 cellpadding=0>";
    echo "<tr bgcolor=#CCCCFF><td colspan=2><font size=4><b>Requests - $companyname</b></font></td>";
    echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";
    echo "</table></center><p>";

    echo "<center><table width=90% cellspacing=0 cellpadding=0>";

    echo "<center><table width=90% border=0 cellspacing=0 cellpadding=0><tr><td colspan=4>";
    //echo "<tr><td bgcolor=black height=1 colspan=6></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td><b>Account Name</b></td><td colspan=3><b>Business</b></td><td><b>District</b></td><td align=right><b>Type</b></td></tr>";
    echo "<tr><td bgcolor=black height=1 colspan=6></td></tr>";
    echo "<tr><td bgcolor=#CCCCCC height=1 colspan=6></td></tr>";

    $query5 = "SELECT * FROM accountpend WHERE companyid = '$companyid' ORDER BY accountnum DESC";
    $result5 = Treat_DB_ProxyOld::query($query5);
    $num5=mysql_numrows($result5);

    if ($num5==0){
       echo "<tr><td colspan=6><i>No Pending Account Requests</i></td></tr>";
       echo "<tr><td bgcolor=#CCCCCC height=1 colspan=6></td></tr>";
    }

    $num5--;
    while ($num5>=0){
       $accountid=mysql_result($result5,$num5,"accountid");
       $accountref=mysql_result($result5,$num5,"accountref");
       $accountname=mysql_result($result5,$num5,"name");
       $accountbid=mysql_result($result5,$num5,"businessid");
       $accountnum=mysql_result($result5,$num5,"accountnum");
       $num5--;

       $query = "SELECT * FROM business WHERE businessid = '$accountbid'";
       $result = Treat_DB_ProxyOld::query($query);

       $busname=mysql_result($result,0,"businessname");
       $districtid=mysql_result($result,0,"districtid");

       $query = "SELECT * FROM district WHERE districtid = '$districtid'";
       $result = Treat_DB_ProxyOld::query($query);

       $districtname=mysql_result($result,0,"districtname");

       if ($accountref!=0){$showstatus="<font color=green>Update</font>";}
       else {$showstatus="<font color=red>New</font>";}

       echo "<tr bgcolor='white' onMouseOver=this.bgColor='#999999' onMouseOut=this.bgColor='white'><td><a style=$style href=acceptaccount.php?accountid=$accountid&cid=$companyid><font color=blue>$accountname</font></a></td><td colspan=3>$busname</td><td>$districtname</td><td align=right>$showstatus</td></tr>";
       echo "<tr><td bgcolor=#CCCCCC height=1 colspan=6></td></tr>";
    }

    $query5 = "SELECT * FROM vendorpend WHERE companyid = '$companyid' ORDER BY vendorname DESC";
    $result5 = Treat_DB_ProxyOld::query($query5);
    $num5=mysql_numrows($result5);

    //echo "<tr><td bgcolor=black height=1 colspan=6></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td><b>Vendor Name</b></td><td colspan=3><b>Business</b></td><td><b>District</b></td><td align=right><b>Type</b></td></tr>";
    echo "<tr><td bgcolor=black height=1 colspan=6></td></tr>";

    if ($num5==0){
       echo "<tr><td colspan=6><i>No Pending Vendor Requests</i></td></tr>";
       echo "<tr><td bgcolor=#CCCCCC height=1 colspan=6></td></tr>";
    }

    $num5--;
    while ($num5>=0){
       $vendorpendid=mysql_result($result5,$num5,"vendorpendid");
       $vendorname=mysql_result($result5,$num5,"vendorname");
       $vendorbid=mysql_result($result5,$num5,"businessid");
       $num5--;

       $query = "SELECT * FROM business WHERE businessid = '$vendorbid'";
       $result = Treat_DB_ProxyOld::query($query);

       $busname=mysql_result($result,0,"businessname");
       $districtid=mysql_result($result,0,"districtid");

       $query = "SELECT * FROM district WHERE districtid = '$districtid'";
       $result = Treat_DB_ProxyOld::query($query);

       $districtname=mysql_result($result,0,"districtname");
       $showstatus="<font color=red>New</font>";

       echo "<tr bgcolor='white' onMouseOver=this.bgColor='#999999' onMouseOut=this.bgColor='white'><td><a style=$style href=acceptvendor.php?vendorpendid=$vendorpendid&cid=$companyid><font color=blue>$vendorname</font></a></td><td colspan=3>$busname</td><td>$districtname</td><td align=right>$showstatus</td></tr>";
       echo "<tr><td bgcolor=#CCCCCC height=1 colspan=6></td></tr>";
    }

    echo "<tr><td bgcolor=black height=1 colspan=6></td></tr>";

    /*//////////////////////////////////////TRANSFERS///////////////////////////////////////////////

    if ($showtrans==1){$query5 = "SELECT * FROM apinvoice WHERE companyid = '$companyid' AND transfer = '1' AND date >= '$date1' AND date <= '$date2' ORDER BY date";}
    else{$query5 = "SELECT * FROM apinvoice WHERE companyid = '$companyid' AND posted = '2'";}
    $result5 = Treat_DB_ProxyOld::query($query5);
    $num5=mysql_numrows($result5);

    if ($showtrans==1){$translink="<font size=1>[<a style=$style href=requests.php?cid=$companyid&showtrans=0#transfer><font color=blue>SHOW PENDING</font></a>]</font>";}
    else{$translink="<font size=1>[<a style=$style href=requests.php?cid=$companyid&showtrans=1#transfer><font color=blue>SHOW ALL</font></a>]</font>";}

    if ($showtrans==1){
       echo "<tr><td colpsan=6><a name=transfer></a><FORM ACTION='requests.php?cid=$companyid' method='post'></td></tr>";
       echo "<tr bgcolor=#E8E7E7><td><b>Transfer#</b> $translink</td><td colspan=3>";
       echo "View Transfers from <SCRIPT LANGUAGE='JavaScript' ID='js18'> var cal18 = new CalendarPopup('testdiv1');cal18.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date1 VALUE='$date1' SIZE=8> <A HREF=#transfer onClick=cal18.select(document.forms[0].date1,'anchor18','yyyy-MM-dd'); return false; TITLE=cal18.select(document.forms[0].date1,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor18' ID='anchor18'><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A> to ";
       echo "<SCRIPT LANGUAGE='JavaScript' ID='js19'> var cal19 = new CalendarPopup('testdiv1');cal19.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date2 VALUE='$date2' SIZE=8> <A HREF=#transfer onClick=cal19.select(document.forms[0].date2,'anchor19','yyyy-MM-dd'); return false; TITLE=cal19.select(document.forms[0].date2,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor19' ID='anchor19'><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A>";
       echo " <input type=submit value='GO'>";
       echo "</td><td><b>District</b></td><td align=right><b>Status</b></td></tr>";
       echo "<tr><td bgcolor=black height=1 colspan=6></form></td></tr>";
    }
    else{
       echo "<tr bgcolor=#E8E7E7><td><a name=transfer></a><b>Transfer#</b> $translink</td><td colspan=3><b>Business</b></td><td><b>District</b></td><td align=right><b>Status</b></td></tr>";
       echo "<tr><td bgcolor=black height=1 colspan=6></td></tr>";
    }

    if ($num5==0){
       echo "<tr><td colspan=6><i>No Pending Transfers</i></td></tr>";
       echo "<tr><td bgcolor=#CCCCCC height=1 colspan=6></td></tr>";
    }

    $num5--;
    while ($num5>=0){
       $apinvoiceid=mysql_result($result5,$num5,"apinvoiceid");
       $invoice_num=mysql_result($result5,$num5,"invoicenum");
       $busid=mysql_result($result5,$num5,"businessid");
       $vendorid=mysql_result($result5,$num5,"vendor");
       $posted=mysql_result($result5,$num5,"posted");
       $export=mysql_result($result5,$num5,"export");
       $date=mysql_result($result5,$num5,"date");
       $total=mysql_result($result5,$num5,"total");
       $total=money($total*-1);
       $num5--;

       $query = "SELECT * FROM business WHERE businessid = '$busid'";
       $result = Treat_DB_ProxyOld::query($query);

       $busname=mysql_result($result,0,"businessname");
       $districtid=mysql_result($result,0,"districtid");

       $query = "SELECT businessname FROM business WHERE businessid = '$vendorid'";
       $result = Treat_DB_ProxyOld::query($query);

       $tobusname=mysql_result($result,0,"businessname");

       $query = "SELECT * FROM district WHERE districtid = '$districtid'";
       $result = Treat_DB_ProxyOld::query($query);

       $districtname=mysql_result($result,0,"districtname");
       if($export==1){$showstatus="<font size=2>Exported</font>";}
       elseif($posted==2){$showstatus="<font color=red size=2>Pending</font>";}
       elseif($posted==1){$showstatus="<font color=red size=2>Submitted</font>";}
       else{$showstatus="<font size=2 color=green>In Progess</font>";}

       if ($posted==2&&$showtrans==1){$transcolor="yellow";}
       else{$transcolor="white";}

       echo "<tr bgcolor='$transcolor' onMouseOver=this.bgColor='#999999' onMouseOut=this.bgColor='$transcolor'><td><a style=$style href=apinvoice.php?bid=$busid&cid=$companyid&apinvoiceid=$apinvoiceid&goto=2><font color=blue>$invoice_num</font></a></td><td><font size=2> $busname <font color=red>=></font> $tobusname</td><td><font size=2>$date</font></td><td align=right><font size=2> $$total</font>&nbsp&nbsp&nbsp&nbsp</td><td><font size=2>$districtname</font></td><td align=right>$showstatus</td></tr>";
       echo "<tr><td bgcolor=#CCCCCC height=1 colspan=6></td></tr>";
    }
    */
    //echo "<tr><td bgcolor=black height=1 colspan=6></td></tr>";

    echo "</table></center><DIV ID=testdiv1 STYLE=position:absolute;visibility:hidden;background-color:white;layer-background-color:white;></DIV>";
//////END TABLE////////////////////////////////////////////////////////////////////////////////////

    echo "<br><FORM ACTION=businesstrack.php method=post>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<center><INPUT TYPE=submit VALUE=' Return to Main Page'></FORM></center></body>";
}
?>
