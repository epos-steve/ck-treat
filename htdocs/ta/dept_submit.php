<?php

function nextday($nextd,$day_format){ //Function returns next day of passed date and formats accordingly.
	if($day_format==""){$day_format="Y-m-d";}
	$monn = substr($nextd, 5, 2);
	$dayn = substr($nextd, 8, 2);
	$yearn = substr($nextd, 0,4);
	$tempdate = date($day_format , mktime(0,0,0, $monn, $dayn+1, $yearn));
	return $tempdate;
}

function prevday($prevd,$day_format = 'Y-m-d'){ //Function returns previous day of passed date and formats accordingly.
	if($day_format==""){$day_format="Y-m-d";}
	$monp = substr($prevd, 5, 2);
	$dayp = substr($prevd, 8, 2);
	$yearp = substr($prevd, 0,4);
	$tempdate = date($day_format , mktime(0,0,0, $monp, $dayp-1, $yearp));
	return $tempdate;
}

function dayofweek($date1)
{
	$day=substr($date1,8,2);
	$month=substr($date1,5,2);
	$year=substr($date1,0,4);
	$dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
	return $dayofweek;
}

function showtime($time){
	$time=explode(":",$time);
	$days=0;
	$hours=0;
	$mins=0;

	while($time[0]>=24){$days++;$time[0]=$time[0]-24;}
	$hours=$time[0];
	$mins=$time[1];

	$newtime="";
	if($days!=0){$newtime="$days Days ";}
	if($hours!=0){$hours=$hours*1; $newtime.="$hours Hours ";}
	if($mins!=0||$newtime==""){
		$mins=$mins*1; 
		if($newtime==""){$newtime.="<b><font color=red>$mins Mins</font></b>";}
		else{$newtime.="$mins Mins";}
	}

	return $newtime;
}

$companyid=$_GET["cid"];
$businessid=$_GET["bid"];
$date=$_GET["date"];
$st=$_GET["st"];
$showp=@$_GET["showp"];

if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
include_once( 'db.php' );
require_once( DOC_ROOT . '/bootstrap.php' );

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");

	if($st!=1){for($counter=1;$counter<=6;$counter++){$date=nextday($date);}}
	$previousdate=$date;
	for($counter=1;$counter<=7;$counter++){$previousdate=prevday($previousdate);}

?>
<html>
	<head>
		<title>dept_submit.php</title>
		<meta http-equiv="refresh" content="60" />
	</head>
	<body style="margin: 0px 0px 0px 0px;">
<?php

	echo "<center><table width=100%>";
	
	$query2="SELECT * FROM payroll_department ORDER BY dept_name";
	$result2 = Treat_DB_ProxyOld::query($query2);

	$rownum=1;
	while ( $r = mysql_fetch_array( $result2 ) ) {

		$dept_id=$r["dept_id"];
		$dept_name=$r["dept_name"];

		$query3="SELECT * FROM submit_labor_dept WHERE date = '$date' AND businessid = '$businessid' AND dept_id = '$dept_id'";
		$result3 = Treat_DB_ProxyOld::query($query3);
		$num3 = mysql_numrows($result3);

		if($showp==1){
			$query4="SELECT * FROM submit_labor_dept WHERE date = '$previousdate' AND businessid = '$businessid' AND dept_id = '$dept_id'";
			$result4 = Treat_DB_ProxyOld::query($query4);
			$num4 = mysql_numrows($result4);
		}

		if($num3>0){
			$showsubmit="<table cellspacing=0 cellpadding=0 bgcolor=#00FF00 style=\"border:1px solid black;\"><tr height=10><td width=10><font size=1>&nbsp;</font></td></tr></table>";
		}
		elseif($showp==1&&$num4>0){
			$showsubmit="<table cellspacing=0 cellpadding=0 bgcolor=yellow style=\"border:1px solid black;\"><tr height=10><td width=10><font size=1>&nbsp;</font></td></tr></table>";
		}
		else{
			$showsubmit="<table cellspacing=0 cellpadding=0 bgcolor=red style=\"border:1px solid black;\"><tr height=10><td width=10><font size=1>&nbsp;</font></td></tr></table>";
		}
		
		if($rownum==1){echo "<tr>";}
		echo "<td width=3.33%>$showsubmit</td><td width=30%><font size=2>$dept_name</font></td>";
		if($rownum==3){echo "</tr>";}

		if($rownum==1){$rownum=2;}
		elseif($rownum==2){$rownum=3;}
		else{$rownum=1;}
	}

	if($rownum==1){echo "<td></td></tr>";}
	elseif($rownum==2){echo "<td></td><td></td></tr>";}
	echo "</table>";
?>
	</body>
</html>
<?php
	
//mysql_close();

?>
