<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');

function isLeapYear($year)
{
    /*
    # Check for valid parameters #
    if (!is_int($year) || $year < 0)
    {
        printf('Wrong parameter for $year in function isLeapYear. It must be a positive integer.');
        exit();
    }
    */

    # In the Gregorian calendar there is a leap year every year divisible by four
    # except for years which are both divisible by 100 and not divisible by 400.

    if ($year % 4 != 0)
    {
        return 28;
    }
    else
    {
        if ($year % 100 != 0)
        {
            return 29;    # Leap year
        }
        else
        {
            if ($year % 400 != 0)
            {
                return 28;
            }
            else
            {
                return 29;    # Leap year
            }
        }
    }
}

$user = Treat_Controller_Abstract::getSessionCookieVariable('usercook');
$pass= Treat_Controller_Abstract::getSessionCookieVariable('passcook');

$cid = Treat_Controller_Abstract::getPostGetSessionOrCookieVariable(array('cid','compcook'));
$caterbus = Treat_Controller_Abstract::getPostGetSessionOrCookieVariable(array('bid','caterbus'));

setcookie("caterbus",$caterbus);

$number = Treat_Controller_Abstract::getPostOrGetVariable('number');
$newmonth = Treat_Controller_Abstract::getPostOrGetVariable('newmonth');
$newyear = Treat_Controller_Abstract::getPostOrGetVariable('newyear');
$newstart = Treat_Controller_Abstract::getPostOrGetVariable('newstart');

$page_business =  Treat_Model_Business_Singleton::getSingleton( $caterbus );
if (!$page_business){
	$page_business = Treat_Model_Company_Singleton::getSingleton( $cid );
}

$today = date("l");
$todaynum = date("j");
$todaynum2 = date("j");
$year = date("Y");
$monthnum = date("n");
$day=1;
$high = 6;
$long = 1;
$leap = date("L");
$back = "white";
$count = 0;
$style = "text-decoration:none";
$brief ="";
$you = "";

if ($newstart == "")
{
   while ($todaynum > 7){$todaynum = $todaynum - 7; $count++;}
   if ($today == 'Sunday' && $todaynum == 1){$start=1;}
   if ($today == 'Monday' && $todaynum == 1){$start=2;}
   if ($today == 'Tuesday' && $todaynum == 1){$start=3;}
   if ($today == 'Wednesday' && $todaynum == 1){$start=4;}
   if ($today == 'Thursday' && $todaynum == 1){$start=5;}
   if ($today == 'Friday' && $todaynum == 1){$start=6;}
   if ($today == 'Saturday' && $todaynum == 1){$start=7;}
   if ($today == 'Sunday' && $todaynum == 2){$start=7;}
   if ($today == 'Monday' && $todaynum == 2){$start=1;}
   if ($today == 'Tuesday' && $todaynum == 2){$start=2;}
   if ($today == 'Wednesday' && $todaynum == 2){$start=3;}
   if ($today == 'Thursday' && $todaynum == 2){$start=4;}
   if ($today == 'Friday' && $todaynum == 2){$start=5;}
   if ($today == 'Saturday' && $todaynum == 2){$start=6;}
   if ($today == 'Sunday' && $todaynum == 3){$start=6;}
   if ($today == 'Monday' && $todaynum == 3){$start=7;}
   if ($today == 'Tuesday' && $todaynum == 3){$start=1;}
   if ($today == 'Wednesday' && $todaynum == 3){$start=2;}
   if ($today == 'Thursday' && $todaynum == 3){$start=3;}
   if ($today == 'Friday' && $todaynum == 3){$start=4;}
   if ($today == 'Saturday' && $todaynum == 3){$start=5;}
   if ($today == 'Sunday' && $todaynum == 4){$start=5;}
   if ($today == 'Monday' && $todaynum == 4){$start=6;}
   if ($today == 'Tuesday' && $todaynum == 4){$start=7;}
   if ($today == 'Wednesday' && $todaynum == 4){$start=1;}
   if ($today == 'Thursday' && $todaynum == 4){$start=2;}
   if ($today == 'Friday' && $todaynum == 4){$start=3;}
   if ($today == 'Saturday' && $todaynum == 4){$start=4;}
   if ($today == 'Sunday' && $todaynum == 5){$start=4;}
   if ($today == 'Monday' && $todaynum == 5){$start=5;}
   if ($today == 'Tuesday' && $todaynum == 5){$start=6;}
   if ($today == 'Wednesday' && $todaynum == 5){$start=7;}
   if ($today == 'Thursday' && $todaynum == 5){$start=1;}
   if ($today == 'Friday' && $todaynum == 5){$start=2;}
   if ($today == 'Saturday' && $todaynum == 5){$start=3;}
   if ($today == 'Sunday' && $todaynum == 6){$start=3;}
   if ($today == 'Monday' && $todaynum == 6){$start=4;}
   if ($today == 'Tuesday' && $todaynum == 6){$start=5;}
   if ($today == 'Wednesday' && $todaynum == 6){$start=6;}
   if ($today == 'Thursday' && $todaynum == 6){$start=7;}
   if ($today == 'Friday' && $todaynum == 6){$start=1;}
   if ($today == 'Saturday' && $todaynum == 6){$start=2;}
   if ($today == 'Sunday' && $todaynum == 7){$start=2;}
   if ($today == 'Monday' && $todaynum == 7){$start=3;}
   if ($today == 'Tuesday' && $todaynum == 7){$start=4;}
   if ($today == 'Wednesday' && $todaynum == 7){$start=5;}
   if ($today == 'Thursday' && $todaynum == 7){$start=6;}
   if ($today == 'Friday' && $todaynum == 7){$start=7;}
   if ($today == 'Saturday' && $todaynum == 7){$start=1;}
}
elseif ($newstart > 7)
{
   $start = $newstart - 7;
}
else
{
   $start = $newstart;
}

if ($newmonth != ""){;}
else {$newmonth = $monthnum;}
if ($newyear != ""){$year=$newyear;}
else {$newyear = $year;}

$hour = date("H");
$min = date("i");
$year1 = date("y");
$mon = date("m");
$day2 = date("d");
$time2 = "1$year1$mon$day2$hour$min";

$style = "text-decoration:none";

$hour = date("H");
$min = date("i");
$year2 = date("y");
$mon = date("m");
$day2 = date("d");
$now = "1$year2$mon$day2$hour$min";
$dif = $now-$time;

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=mysql_numrows($result);
//mysql_close();

if ($num!=1 || ($user == "" && $pass == ""))
{
    echo "<center><h4>Failed</h4></center>";
}

else
{
    setcookie("usertime",$now);

    $bid=mysql_result($result,0,"businessid");
    if ($bid==0){$bid=$caterbus;}
    $cid=mysql_result($result,0,"companyid");
    $security_level=mysql_result($result,0,"security_level");

    if ($newmonth == 1){$month = "January";}
    elseif ($newmonth == 2){$month = "February";}
    elseif ($newmonth == 3){$month = "March";}
    elseif ($newmonth == 4){$month = "April";}
    elseif ($newmonth == 5){$month = "May";}
    elseif ($newmonth == 6){$month = "June";}
    elseif ($newmonth == 7){$month = "July";}
    elseif ($newmonth == 8){$month = "August";}
    elseif ($newmonth == 9){$month = "September";}
    elseif ($newmonth == 10){$month = "October";}
    elseif ($newmonth == 11){$month = "November";}
    elseif ($newmonth == 12){$month = "December";}

    $query33 = "SELECT payroll_date FROM company WHERE companyid = '$cid'";
    $result33 = Treat_DB_ProxyOld::query($query33);

    $payroll_date=mysql_result($result33,0,"payroll_date");

    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%><tr><td colspan=2><a href=businesstrack.php><img src=logo.jpg border=0 height=43 width=205></a><p></td></tr>";
    //echo "<tr bgcolor=black><td height=1 colspan=2></td></tr>";
	echo "</table><p><table width=90% cellspacing=0 cellpadding=0 style=\"border:2px solid #CCCCCC;\">";
    echo "<tr bgcolor=#E8E7E7><td colspan=2><center><br><h2>$month $year</h2></center></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td colspan=2>";

    $leap=isLeapYear($year);
    if($leap==29){$leap=1;}
    else{$leap=0;}

    echo "<center><table border=1 cellpadding=0 cellspacing=0>";
    echo "<tr bgcolor=#FFFF99><td><center>Sunday</center></td><td><center>Monday</center></td><td><center>Tuesday</center></td><td><center>Wednesday</center></td><td><center>Thursday</center></td><td><center>Friday</center></td><td><center>Saturday</center></td></tr>";

    echo "<tr height=110>";
    $weekend = "F";
    while ($long < 8)
    {
       if ($long >= $start)
       {
          if ($long == 1 || $long == 7){$weekend="T";}

          if ($day < 10 && $newmonth < 10){$date = "$newyear-0$newmonth-0$day";}
          elseif ($day < 10 && $newmonth > 10){$date = "$newyear-$newmonth-0$day";}
          elseif ($day > 10 && $newmonth < 10){$date = "$newyear-0$newmonth-$day";}
          else {$date = "$newyear-$newmonth-$day";}

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query = "SELECT * FROM calender WHERE date = '$date' AND companyid = '$cid' ORDER BY number DESC";
          $result = Treat_DB_ProxyOld::query($query);
          $num=mysql_numrows($result);
          //mysql_close();

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query98 = "SELECT * FROM invoice WHERE date = '$date' AND companyid = '$cid' AND businessid = '$bid' AND status != '3'";
          $result98 = Treat_DB_ProxyOld::query($query98);
          $num98=mysql_numrows($result98);
          //mysql_close();

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query97 = "SELECT * FROM caterclose WHERE date = '$date' AND businessid = '$caterbus'";
          $result97 = Treat_DB_ProxyOld::query($query97);
          $num97=mysql_numrows($result97);
          //mysql_close();

          if ($newstart == ""){$newstart=$start;}
          $correcturl = "date.php?bid={$page_business->getBusinessId()}&date=$date&newmonth=$newmonth&newyear=$newyear&newstart=$newstart&day=$day&weekend=$weekend";
		  //$correcturl = "date.php?bid=$caterbus&date=$date&newmonth=$newmonth&newyear=$newyear&newstart=$newstart&day=$day&weekend=$weekend";
		  

          if ($day == $todaynum2 && $monthnum == $newmonth && $year == $newyear){$color = "red";}
          else {$color = "black";}

          echo "<a style=$style href=$correcturl><td bgcolor=$back width=120 valign=top onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor='$back'>";

          echo "<a style=$style href=$correcturl><b><font color=$color>$day</font></b></a><br>";

          ////////PAYROLL DATE
          $findme=".";
          $test=round((strtotime($payroll_date) - strtotime($date)) / (60 * 60 * 24) / 14,1);
          if(strpos($test, $findme)==0){echo "<font size=2 color=orange>PAYROLL END<br></font>";}

          $counter=0;
          if ($num != 0 || $num98 != 0 || $num97 != 0) {

          echo "<table width=100% border=0 cellpadding=0 cellspacing=0>";
          if ($num97!=0){echo "<tr><td><font size=2 color=red>CLOSED</td></tr>";}
          while ($num > 0)
          {
             $color2="white";
             $personal=mysql_result($result,$num-1,"personal");
             $you=mysql_result($result,$num-1,"user");
             $brief=mysql_result($result,$num-1,"brief");
             $cont=mysql_result($result,$num-1,"cont");
             if ($cont==1){$color2="#00FFFF";$counter++;}
             if ($counter==1 && $brief2==$brief){echo "<tr><td height=15></td></tr>";$color2="yellow";}
             if ($counter==2 && $cont==1){$color2="yellow";$brief2="$brief";}
             if (strlen($brief)>15){$brief=substr($brief,0,10);$brief="$brief...";}
             if ($personal == 'T' && $user == $you){
                echo "<tr><td bgcolor=$color2><font color=red size=2>>></font><font size=2 color=blue>$brief<br></font></td></tr>";}
             elseif ($personal == 'T' && $user != $you){}
             else{
                echo "<tr><td bgcolor=$color2><font size=2><font color=red>>></font>$brief<br></font></td></tr>";}
             $num--;
          }
          $num98--;
          while ($num98>=0)
          {
             $color2="white";
             $event_time=mysql_result($result98,$num98,"event_time");
             if ($event_time!=""){echo "<tr><td bgcolor=$color2><font size=2><font color=red>>></font><font color=#999999>$event_time Catering<br></font></td></tr>";}
             $num98--;
          }
          echo "</table>";
          }

          echo "</td></a>";
          $back = "white";

          if ($day == 1 && $long == 1){$backstart=7;}
          elseif ($day == 1 && $long != 1){$backstart=$long-1;}

          $day++;
       }
       else
       {
          echo "<td width=110 bgcolor=#DCDCDC></td>";
       }
       $weekend="F";
       $long++;
    }
    echo "</tr>";

    while ($high > 1)
    {
       $long=1;
       echo "<tr height=110>";
       $weekend = "F";
       while ($long < 8)
       {
          if ($long == 1 || $long == 7){$weekend="T";}

          if ($day < 10 && $newmonth < 10){$date = "$newyear-0$newmonth-0$day";}
          elseif ($day < 10 && $newmonth > 10){$date = "$newyear-$newmonth-0$day";}
          elseif ($day >= 10 && $newmonth < 10){$date = "$newyear-0$newmonth-$day";}
          else {$date = "$newyear-$newmonth-$day";}

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query = "SELECT * FROM calender WHERE date = '$date' AND companyid = '$cid' ORDER BY number DESC";
          $result = Treat_DB_ProxyOld::query($query);
          $num=mysql_numrows($result);
          //mysql_close();

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query98 = "SELECT * FROM invoice WHERE date = '$date' AND companyid = '$cid' AND businessid = '$bid' AND status != '3'";
          $result98 = Treat_DB_ProxyOld::query($query98);
          $num98=mysql_numrows($result98);
          //mysql_close();

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query97 = "SELECT * FROM caterclose WHERE date = '$date' AND businessid = '$caterbus'";
          $result97 = Treat_DB_ProxyOld::query($query97);
          $num97=mysql_numrows($result97);
          //mysql_close();

          if ($newstart == ""){$newstart=$start;}
          $correcturl = "date.php?bid={$page_business->getBusinessId()}&date=$date&newmonth=$newmonth&newyear=$newyear&newstart=$newstart&day=$day&weekend=$weekend";
		  //$correcturl = "date.php?bid=$caterbus&date=$date&newmonth=$newmonth&newyear=$newyear&newstart=$newstart&day=$day&weekend=$weekend";
		  

          $show = $day;
          if ($day > 28 && $newmonth == 2 && $leap == 0){$show = "";}
          elseif ($day > 29 && $newmonth == 2 and $leap == 1){$show = "";}
          elseif ($day > 30 && ($newmonth == 4 || $newmonth == 6 || $newmonth == 9 || $newmonth == 11)){$show = "";}
          elseif ($day > 31){$show = "";}
          else {;}

          if ($day > 28 && $newmonth == 2 && $leap == 0){$back = '#DCDCDC';}
          elseif ($day > 29 && $newmonth == 2 and $leap == 1){$back = '#DCDCDC';}
          elseif ($day > 30 && ($newmonth == 4 || $newmonth == 6 || $newmonth == 9 || $newmonth == 11)){$back = '#DCDCDC';}
          elseif ($day > 31){$back = '#DCDCDC';}
          else {$back = 'white';}

          if ($day == 29 && $newmonth == 2 && $leap ==0){$newstart=$long;}
          if ($day == 30 && $newmonth == 2 && $leap ==1){$newstart=$long;}
          if ($day == 31 && ($newmonth == 4 || $newmonth == 6 || $newmonth == 9 || $newmonth == 11)){$newstart=$long;}
          if ($day == 32 && ($newmonth == 1 || $newmonth == 3 || $newmonth == 5 || $newmonth == 7 || $newmonth == 8 || $newmonth == 10 || $newmonth == 12)){$newstart=$long;}

          if ($day == $todaynum2 && $monthnum == $newmonth && $year == $newyear){$color = "red";}
          else {$color = "black";}

          echo "<a style=$style href=$correcturl><td bgcolor=$back width=120 valign=top onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor='$back'>";

          echo "<a style=$style href=$correcturl><b><font color=$color>$show</font></b></a><br>";

          ////////PAYROLL DATE
          $findme=".";
          $test=round((strtotime($payroll_date) - strtotime($date)) / (60 * 60 * 24) / 14,1);
          if(strpos($test, $findme)==0&&$day<32){echo "<font size=2 color=orange>PAYROLL END<br></font>";}

          $counter=0;
          if ($num != 0 || $num98 != 0 || $num97 != 0) {

          echo "<table width=100% border=0 cellpadding=0 cellspacing=0>";
          if ($num97!=0){echo "<tr><td><font size=2 color=red>CLOSED</td></tr>";}
          while ($num > 0)
          {
             $color2="white";
             $personal=mysql_result($result,$num-1,"personal");
             $you=mysql_result($result,$num-1,"user");
             $brief=mysql_result($result,$num-1,"brief");
             $cont=mysql_result($result,$num-1,"cont");
             if ($cont==1){$color2="#00FFFF";$counter++;}
             if ($counter==1 && $brief2==$brief){echo "<tr><td height=15></td></tr>";$color2="yellow";}
             if ($counter==2 && $cont==1){$color2="yellow";$brief2="$brief";}
             if (strlen($brief)>15){$brief=substr($brief,0,10);$brief="$brief...";}
             if ($personal == 'T' && $user == $you){
                echo "<tr><td bgcolor=$color2><font color=red size=2>>></font><font size=2 color=blue>$brief<br></font></td><tr>";}
             elseif ($personal == 'T' && $user != $you){}
             else{
                echo "<tr><td bgcolor=$color2><font size=2><font color=red>>></font>$brief<br></font></td></tr>";}
             $num--;
          }
          $num98--;
          while ($num98>=0)
          {
             $color2="white";
             $event_time=mysql_result($result98,$num98,"event_time");
             if ($event_time!=""){echo "<tr><td bgcolor=$color2><font size=2><font color=red>>></font><font color=#999999>$event_time Catering<br></font></td></tr>";}
             $num98--;
          }
          echo "</table>";
          }

          echo "</td></a>";
          $back = "white";

          $day++;
          $weekend = "F";
          $long++;
       }
       echo "</tr>";
       $high--;
    }
    echo "</table></center>";

    if ($newmonth == 5 || $newmonth == 7 || $newmonth == 10 || $newmonth == 12){$count=30;}
    elseif ($newmonth == 1 || $newmonth == 2 || $newmonth == 4 || $newmonth == 6 || $newmonth == 8 || $newmonth == 9 || $newmonth == 11){$count=31;}
    elseif ($newmonth == 3 && $leap == 0){$count=28;}
    elseif ($newmonth == 3 && $leap == 1){$count=29;}

    $count2 = $backstart;
    while ($count > 1)
    {
       if ($count2 == 1){$count2 = 8;}
       $count--;
       $count2--;
    }
    $backstart = $count2;

    $prevmonth = $newmonth - 1;
    $prevyear = $newyear;
    if ($prevmonth == 0){$prevmonth = 12; $prevyear = $year - 1;}
    $nextmonth = $newmonth + 1;
    $nextyear = $newyear;
    if ($nextmonth == 13){$nextmonth = 1; $nextyear=$year + 1;}
    echo "<center><a href='calender.php?newmonth=$prevmonth&newyear=$prevyear&newstart=$backstart'>Previous Month</a> | <a href='calender.php?newmonth=$nextmonth&newyear=$nextyear&newstart=$newstart'>Next Month</a></center><p>";

    echo "</td></tr>";
    //echo "<tr bgcolor=black><td height=1 colspan=2></td></tr>
    echo "</table>";

    //mysql_close();

    $action2 = "businesstrack.php";
    $method = "post";
    echo "<center><FORM ACTION=$action2 method=$method>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<INPUT TYPE=submit VALUE=' Return to Main Page'></FORM></center>";
}
?>