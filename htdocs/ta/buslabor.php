<?php

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+4);
        $diff=round($diff, 2);
        if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
        elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
        $diff = substr($diff, 0, $dot+3);
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        return $diff;
}

function nextday($nextd,$day_format=''){ //Function returns next day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monn = substr($nextd, 5, 2);
   $dayn = substr($nextd, 8, 2);
   $yearn = substr($nextd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monn, $dayn+1, $yearn));
   return $tempdate;
}

function prevday($prevd,$day_format=''){ //Function returns previous day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monp = substr($prevd, 5, 2);
   $dayp = substr($prevd, 8, 2);
   $yearp = substr($prevd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monp, $dayp-1, $yearp));
   return $tempdate;
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      $day=$day-31;  
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";} 
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}


if ( !defined('DOC_ROOT'))
{
	define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
}
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');

$date2 = null; # Notice: Undefined variable
$save_disable = null; # Notice: Undefined variable
$showpend = null; # Notice: Undefined variable
$sendmessage = null; # Notice: Undefined variable

$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$today2=$today;

/*$user = isset($_COOKIE["usercook"])?$_COOKIE["usercook"]:'';
$pass = isset($_COOKIE["passcook"])?$_COOKIE["passcook"]:'';
$businessid = isset($_GET["bid"])?$_GET["bid"]:'';
$companyid = isset($_GET["cid"])?$_GET["cid"]:'';
$newdate = isset($_GET["newdate"])?$_GET["newdate"]:'';
$audit = isset($_GET["audit"])?$_GET["audit"]:'';
$viewrate = isset($_GET["viewrate"])?$_GET["viewrate"]:'';
$sbt = isset($_GET["sbt"])?$_GET["sbt"]:'';*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook','');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook','');
$businessid = \EE\Controller\Base::getGetVariable('bid','');
$companyid = \EE\Controller\Base::getGetVariable('cid','');
$newdate = \EE\Controller\Base::getGetVariable('newdate','');
$audit = \EE\Controller\Base::getGetVariable('audit','');
$viewrate = \EE\Controller\Base::getGetVariable('viewrate','');
$sbt = \EE\Controller\Base::getGetVariable('sbt','');

if ($businessid==""&&$companyid==""){
   /*$businessid = isset($_POST["businessid"])?$_POST["businessid"]:'';
   $companyid = isset($_POST["companyid"])?$_POST["companyid"]:'';
   $newdate = isset($_POST["newdate"])?$_POST["newdate"]:'';*/
	
	$businessid = \EE\Controller\Base::getPostVariable('businessid','');
	$companyid = \EE\Controller\Base::getPostVariable('companyid','');
	$newdate = \EE\Controller\Base::getPostVariable('newdate','');
}

if($newdate!=""){$today=$newdate;}
$todayname = dayofweek($today);

/*mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");*/

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=Treat_DB_ProxyOld::mysql_numrows($result);
//mysql_close();

$loginid=@Treat_DB_ProxyOld::mysql_result($result,0,"loginid");
$security_level=@Treat_DB_ProxyOld::mysql_result($result,0,"security_level");
$mysecurity=@Treat_DB_ProxyOld::mysql_result($result,0,"security");
$bid=@Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
$bid2=@Treat_DB_ProxyOld::mysql_result($result,0,"busid2");
$bid3=@Treat_DB_ProxyOld::mysql_result($result,0,"busid3");
$bid4=@Treat_DB_ProxyOld::mysql_result($result,0,"busid4");
$bid5=@Treat_DB_ProxyOld::mysql_result($result,0,"busid5");
$bid6=@Treat_DB_ProxyOld::mysql_result($result,0,"busid6");
$bid7=@Treat_DB_ProxyOld::mysql_result($result,0,"busid7");
$bid8=@Treat_DB_ProxyOld::mysql_result($result,0,"busid8");
$bid9=@Treat_DB_ProxyOld::mysql_result($result,0,"busid9");
$bid10=@Treat_DB_ProxyOld::mysql_result($result,0,"busid10");
$cid=@Treat_DB_ProxyOld::mysql_result($result,0,"companyid");

$pr1=@Treat_DB_ProxyOld::mysql_result($result,0,"payroll");
$pr2=@Treat_DB_ProxyOld::mysql_result($result,0,"pr2");
$pr3=@Treat_DB_ProxyOld::mysql_result($result,0,"pr3");
$pr4=@Treat_DB_ProxyOld::mysql_result($result,0,"pr4");
$pr5=@Treat_DB_ProxyOld::mysql_result($result,0,"pr5");
$pr6=@Treat_DB_ProxyOld::mysql_result($result,0,"pr6");
$pr7=@Treat_DB_ProxyOld::mysql_result($result,0,"pr7");
$pr8=@Treat_DB_ProxyOld::mysql_result($result,0,"pr8");
$pr9=@Treat_DB_ProxyOld::mysql_result($result,0,"pr9");
$pr10=@Treat_DB_ProxyOld::mysql_result($result,0,"pr10");

if ($num != 1 || ($security_level==1 && ($bid != $businessid || $cid != $companyid)) || $user == "" || $pass == "")
{
    echo "<center><h3>Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
	////time range security 11/12/10
	$query = "SELECT startdate FROM security_labor WHERE loginid = $loginid AND businessid = $businessid";
    $result = Treat_DB_ProxyOld::query($query);
	$num = Treat_DB_ProxyOld::mysql_numrows($result);
	
	if($num == 1){
		$sec_date = Treat_DB_ProxyOld::mysql_result($result,0,"startdate");
		if($today < $sec_date){$today = $sec_date;}
	}

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM security WHERE securityid = '$mysecurity'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $sec_bus_sales=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_sales");
    $sec_bus_invoice=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_invoice");
    $sec_bus_order=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_order");
    $sec_bus_payable=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_payable");
    $sec_bus_inventor1=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor1");
    $sec_bus_inventor2=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor2");
    $sec_bus_inventor3=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor3");
    $sec_bus_inventor4=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor4");
    $sec_bus_inventor5=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor5");
    $sec_bus_control=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_control");
    $sec_bus_menu=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_menu");
    $sec_bus_order_setup=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_order_setup");
    $sec_bus_request=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_request");
    $sec_route_collection=@Treat_DB_ProxyOld::mysql_result($result,0,"route_collection");
    $sec_route_labor=@Treat_DB_ProxyOld::mysql_result($result,0,"route_labor");
    $sec_route_detail=@Treat_DB_ProxyOld::mysql_result($result,0,"route_detail");
    $sec_bus_labor=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_labor");
    $sec_bus_timeclock=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_timeclock");
	$sec_bus_budget=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_budget");

    ////SECURITY LEVELS
    if($sec_bus_labor<.5&&$sec_bus_timeclock>0){
       $location="buslabor5.php?bid=$businessid&cid=$companyid";
       header('Location: ./' . $location);
    }
    elseif($sec_bus_labor<.5){
       $location="businesstrack.php";
       header('Location: ./' . $location);
    }
    elseif($sec_bus_labor<2&&$bid!=$businessid){
       $location="businesstrack.php";
       header('Location: ./' . $location);
    }
    else{}

?>

<html>
<head>

<script language="JavaScript"
   type="text/JavaScript">
function calc_popup(url)
  {
     popwin=window.open(url,"Calculator","location=no,menubar=no,titlebar=no,resizeable=no,height=150,width=350");   
  }
</script>

<script language="JavaScript" 
   type="text/JavaScript">
function changePage(newLoc)
 {
   nextPage = newLoc.options[newLoc.selectedIndex].value
		
   if (nextPage != "")
   {
      document.location.href = nextPage
   }
 }
</script>

<SCRIPT LANGUAGE="JavaScript" SRC="CalendarPopup.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">document.write(getCalendarStyles());</SCRIPT>

<STYLE>
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation
			{
			background-color:#6677DD;
			text-align:center;
			vertical-align:center;
			text-decoration:none;
			color:#FFFFFF;
			font-weight:bold;
			}
	.TESTcpDayColumnHeader,
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation,
	.TESTcpCurrentMonthDate,
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDate,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDate,
	.TESTcpCurrentDateDisabled,
	.TESTcpTodayText,
	.TESTcpTodayTextDisabled,
	.TESTcpText
			{
			font-family:arial;
			font-size:8pt;
			}
	TD.TESTcpDayColumnHeader
			{
			text-align:right;
			border:solid thin #6677DD;
			border-width:0 0 1 0;
			}
	.TESTcpCurrentMonthDate,
	.TESTcpOtherMonthDate,
	.TESTcpCurrentDate
			{
			text-align:right;
			text-decoration:none;
			}
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDateDisabled
			{
			color:#D0D0D0;
			text-align:right;
			text-decoration:line-through;
			}
	.TESTcpCurrentMonthDate
			{
			color:#6677DD;
			font-weight:bold;
			}
	.TESTcpCurrentDate
			{
			color: #FFFFFF;
			font-weight:bold;
			}
	.TESTcpOtherMonthDate
			{
			color:#808080;
			}
	TD.TESTcpCurrentDate
			{
			color:#FFFFFF;
			background-color: #6677DD;
			border-width:1;
			border:solid thin #000000;
			}
	TD.TESTcpCurrentDateDisabled
			{
			border-width:1;
			border:solid thin #FFAAAA;
			}
	TD.TESTcpTodayText,
	TD.TESTcpTodayTextDisabled
			{
			border:solid thin #6677DD;
			border-width:1 0 0 0;
			}
	A.TESTcpTodayText,
	SPAN.TESTcpTodayTextDisabled
			{
			height:20px;
			}
	A.TESTcpTodayText
			{
			color:#6677DD;
			font-weight:bold;
			}
	SPAN.TESTcpTodayTextDisabled
			{
			color:#D0D0D0;
			}
	.TESTcpBorder
			{
			border:solid thin #6677DD;
			}
</STYLE>

<SCRIPT LANGUAGE=javascript><!--
function submit2(){return confirm('MAKE SURE YOU HAVE SAVED! YOU WILL NOT BE ABLE TO EDIT IT AGAIN.');}
// --></SCRIPT>

<SCRIPT LANGUAGE=javascript><!--
function submit3(){return confirm('ARE YOU SURE YOU WANT TO ADD THIS EMPLOYEE?');}
// --></SCRIPT>

<SCRIPT LANGUAGE=javascript><!--
function submit4(){return confirm('ARE YOU SURE THIS IS A NEW EMPLOYEE?');}
// --></SCRIPT>

<SCRIPT LANGUAGE=javascript><!--
function submit5(){return confirm('ARE YOU SURE YOU WANT TO CHANGE THIS?');}
// --></SCRIPT>

<SCRIPT LANGUAGE=javascript><!--
function deletetemp(){return confirm('ARE YOU SURE YOU WANT TO DELETE THIS TEMP RECORD?');}
// --></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
<!-- Web Site:  http://dynamicdrive.com -->

<!-- This script and many more are available free online at -->
<!-- The JavaScript Source!! http://javascript.internet.com -->
<!-- Begin
function disableForm(theform) {
if (document.all || document.getElementById) {
for (i = 0; i < theform.length; i++) {
var tempobj = theform.elements[i];
if (tempobj.type.toLowerCase() == "submit" || tempobj.type.toLowerCase() == "reset")
tempobj.disabled = true;
}

}
else {
alert("The form has been submitted.  But, since you're not using IE 4+ or NS 6, the submit button was not disabled on form submission.");
return false;
   }
}
//  End -->
</script>

<script type="text/javascript" src="javascripts/prototype.js"></script>

<script language="JavaScript" 
   type="text/JavaScript">
window.onload = function(){
   new ArrowGrid('test2');
   new headerScroll('test2',{head: 'header'});
} 
</script>

</head>

<?

    echo "<body>";  
    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%><tr><td colspan=2>";
    echo "<a href=businesstrack.php><img src=logo.jpg border=0 height=43 width=205></a><p></td></tr>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM company WHERE companyid = '$companyid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $companyname=@Treat_DB_ProxyOld::mysql_result($result,0,"companyname");
    $week_end=@Treat_DB_ProxyOld::mysql_result($result,0,"week_end");
    $week_start=@Treat_DB_ProxyOld::mysql_result($result,0,"week_start");
	$com_logo=@Treat_DB_ProxyOld::mysql_result($result,0,"logo");

	if($com_logo == ""){$com_logo = "talogo.gif";}

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM business WHERE companyid = '$companyid' AND businessid = '$businessid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $businessname=@Treat_DB_ProxyOld::mysql_result($result,0,"businessname");
    $businessid=@Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
    $street=@Treat_DB_ProxyOld::mysql_result($result,0,"street");
    $city=@Treat_DB_ProxyOld::mysql_result($result,0,"city");
    $state=@Treat_DB_ProxyOld::mysql_result($result,0,"state");
    $zip=@Treat_DB_ProxyOld::mysql_result($result,0,"zip");
    $phone=@Treat_DB_ProxyOld::mysql_result($result,0,"phone");
    $districtid=@Treat_DB_ProxyOld::mysql_result($result,0,"districtid");
    $operate=@Treat_DB_ProxyOld::mysql_result($result,0,"operate");
    $bus_unit=@Treat_DB_ProxyOld::mysql_result($result,0,"unit");
    $auto_submit_labor=@Treat_DB_ProxyOld::mysql_result($result,0,"auto_submit_labor");

    //echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";
    echo "<tr bgcolor=#CCCCFF><td width=1%><img src=logo/$com_logo></td><td><font size=4><b>$businessname #$bus_unit</b></font><br>$street<br>$city, $state $zip<br>$phone</td>";
    echo "<td align=right valign=top><br>";
    echo "<FORM ACTION='editbus.php' method=post name='store'>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<input type=hidden name=businessid value=$businessid>";
    echo "<input type=hidden name=companyid value=$companyid>";
    echo "<INPUT TYPE=submit VALUE=' Store Setup '> ";
    echo "</form></td></tr>";
    //echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";
    
    echo "<tr><td colspan=3><font color=#999999>";
    if ($sec_bus_sales>0){echo "<a href=busdetail.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Sales</font></a>";}
    if ($sec_bus_invoice>0){echo " | <a href=businvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Invoicing</font></a>";}
    if ($sec_bus_order>0){echo " | <a href=buscatertrack.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Orders</font></a>";}
    if ($sec_bus_payable>0){echo " | <a href=busapinvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Payables</font></a>";}
    if ($sec_bus_inventor1>0){echo " | <a href=businventor.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Inventory</font></a>";}
    if ($sec_bus_control>0){echo " | <a href=buscontrol.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Control Sheet</font></a>";}
    if ($sec_bus_menu>0){echo " | <a href=buscatermenu.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Menus</font></a>";}
    if ($sec_bus_order_setup>0){echo " | <a href=buscaterroom.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Order Setup</font></a>";}
    if ($sec_bus_request>0){echo " | <a href=busrequest.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Requests</font></a>";}
    if ($sec_route_collection>0||$sec_route_labor>0||$sec_route_detail>0){echo " | <a href=busroute_collect.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Route Collections</font></a>";}
    if ($sec_bus_labor>0){echo " | <a href=buslabor.php?bid=$businessid&cid=$companyid><font color=red onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='red' style='text-decoration:none'>Labor</font></a>";}
    if ($sec_bus_budget>0||($businessid == $bid && $pr1 == 1)||($businessid == $bid2 && $pr2 == 1)||($businessid == $bid3 && $pr3 == 1)||($businessid == $bid4 && $pr4 == 1)||($businessid == $bid5 && $pr5 == 1)||($businessid == $bid6 && $pr6 == 1)||($businessid == $bid7 && $pr7 == 1)||($businessid == $bid8 && $pr8 == 1)||($businessid == $bid9 && $pr9 == 1)||($businessid == $bid10 && $pr10 == 1)){echo " | <a href=busbudget.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Budget</font></a>";}
	echo "</td></tr>";

    echo "</table></center><p>";

    $date1=$today;
    while($todayname!="$week_start"){
       $date1=prevday($date1);
       $todayname=dayofweek($date1);
    }

    $showsubmitdate=$date1;
    for($counter=1;$counter<=6;$counter++){$showsubmitdate=nextday($showsubmitdate);}

if ($security_level>0){
    echo "<center><table width=90%><tr><td><form action=buslabor.php method=post>";
    echo "<select name=gobus onChange='changePage(this.form.gobus)'>";

    $districtquery="";
    if ($security_level>2&&$security_level<7){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM login_district WHERE loginid = '$loginid'";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();

       $num--;
       while($num>=0){
          $districtid=@Treat_DB_ProxyOld::mysql_result($result,$num,"districtid");
          if($districtquery==""){$districtquery="districtid = '$districtid'";}
          else{$districtquery="$districtquery OR districtid = '$districtid'";}
          $num--;
       }
    }

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    if($security_level>6){$query = "SELECT * FROM business WHERE (companyid = '$companyid' AND exp_payroll = '0') OR (businessid = '$businessid')  ORDER BY businessname DESC";}
    elseif($security_level==2||$security_level==1){$query = "SELECT * FROM business WHERE (companyid = '$companyid' AND (businessid = '$bid' OR businessid = '$bid2' OR businessid = '$bid3' OR businessid = '$bid4' OR businessid = '$bid5' OR businessid = '$bid6' OR businessid = '$bid7' OR businessid = '$bid8' OR businessid = '$bid9' OR businessid = '$bid10') AND exp_payroll = '0') OR (businessid = '$businessid') ORDER BY businessname DESC";}
    elseif($security_level>2&&$security_level<7){$query = "SELECT * FROM business WHERE (companyid = '$companyid' AND ($districtquery) AND exp_payroll = '0') OR (businessid = '$businessid') ORDER BY businessname DESC";}
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    $num--;
    while($num>=0){
       $businessname=@Treat_DB_ProxyOld::mysql_result($result,$num,"businessname");
       $busid=@Treat_DB_ProxyOld::mysql_result($result,$num,"businessid");

       if ($busid==$businessid){$show="SELECTED";}
       else{$show="";}

       echo "<option value=buslabor.php?bid=$busid&cid=$companyid&newdate=$newdate $show>$businessname</option>";
       $num--;
    }
 
    echo "</select></td><td></form></td><td width=50% align=right><form action=buslabor.php method=post onSubmit='return disableForm(this);'><input type=hidden name=businessid value=$businessid><input type=hidden name=companyid value=$companyid><SCRIPT LANGUAGE='JavaScript' ID='js19'> var cal19 = new CalendarPopup('testdiv1');cal19.setCssPrefix('TEST');</SCRIPT> <A HREF=# onClick=cal19.select(document.forms[2].newdate,'anchor19','yyyy-MM-dd'); return false; TITLE=cal19.select(document.forms[2].newdate,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor19' ID='anchor19'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A> <input type=text name=newdate value='$today' size=8> <input type=submit value='GO'></td><td></form></td></tr>";

    if($auto_submit_labor==1){echo "<tr valign=top><td colspan=2><iframe src='dept_submit.php?cid=$companyid&bid=$businessid&date=$showsubmitdate&st=1' WIDTH=99% HEIGHT=100 frameborder=0></iframe></td><td align=right colspan=1><iframe src='showtime.php' WIDTH=99% HEIGHT=24 frameborder=0></iframe></td></tr>";}

    echo "</table></center><p>";
}
    ////SECURITY LEVELS
    if($sec_bus_labor==2&&$bid!=$businessid){$security_level=1;}
    else{$security_level=$sec_bus_labor;}

///////////PREV AND NEXT PERIODS
    $prevdate=prevday($date1);

    $nextdate=$date1;
    $todayname2=$todayname;
    while($todayname2!=$week_end){
       $nextdate=nextday($nextdate);
       $todayname2=dayofweek($nextdate);
    }
    $enddate=$nextdate;
    $nextdate=nextday($nextdate);
    
    $labordollar=array();
//////////////////////////////
	$max_exceeded=0;

    echo "<center><table width=90% cellspacing=0 cellpadding=0>";

    echo "<tr bgcolor=#CCCCCC valign=top><td width=1% bgcolor=#E8E7E7><img src=leftcrn.jpg height=6 width=6 align=top></td><td bgcolor=#E8E7E7 width=15%><center><a href=buslabor.php?cid=$companyid&bid=$businessid style=$style><font color=black>Hours</a></center></td><td bgcolor=#E8E7E7 width=1% align=right><img src=rightcrn.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1% ><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=15%><center><a href=buslabor4.php?cid=$companyid&bid=$businessid&newdate=$date1 style=$style style=$style><font color=blue>Tips</a></center></td><td width=1% align=right><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1%><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=15%><center><a href=buslabor2.php?cid=$companyid&bid=$businessid&newdate=$date1 style=$style style=$style><font color=blue>Commissions</a></center></td><td width=1% align=right><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1%><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=15%><center><a href=buslabor3.php?cid=$companyid&bid=$businessid&newdate=$date1 style=$style style=$style><font color=blue>Summary</a></center></td><td width=1% align=right><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1%><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=15%><center><a href=buslabor5.php?cid=$companyid&bid=$businessid&newdate=$date1 style=$style style=$style><font color=blue>Time Clock</a></center></td><td width=1% align=right><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td bgcolor=white width=25%></td></tr>";

    echo "<tr height=2><td colspan=3 bgcolor=#E8E7E7></td><td></td><td colspan=4></td><td colspan=4></td><td colspan=4></td><td colspan=4></td><td colspan=1></td></tr>";

    echo "</table></center><center><table width=90% cellspacing=0 cellpadding=0>";

    echo "<tr><td colspan=10><center><table width=100% border=0 bgcolor=#E8E7E7 id='test2' cellpadding=0 cellspacing=1>";
    echo "<tr height=0><td colspan=18><form action=savelabor.php method=post onSubmit='return disableForm(this);'><input type=hidden name=companyid value=$companyid><input type=hidden name=businessid value=$businessid><input type=hidden name=date value=$date1><input type=hidden name=date2 value='$date2'> </td></tr>";

	if($loginid==58){$viewmyrate="";}
    elseif($viewrate==1){$viewmyrate="<font size=1>[<a href=buslabor.php?bid=$businessid&cid=$companyid&newdate=$date1&viewrate=0#labor><font color=blue>HIDE RATES</font></a>]</font>";}
    else{$viewmyrate="<font size=1>[<a href=buslabor.php?bid=$businessid&cid=$companyid&newdate=$date1&viewrate=1#labor><font color=blue>SHOW RATES</font></a>]</font>";}

    echo "<tr bgcolor=#E8E7E7 id='header'><td colspan=3 align=right>";
    echo "<a onclick=calc_popup('labor_calc.php')><img src=calc.gif height=16 width=16 alt='Labor Calculator' border=0></a> $viewmyrate  <font size=1>[<a href=buslabor.php?bid=$businessid&cid=$companyid&newdate=$prevdate#labor><font color=blue>PREV</font></a>]</font></td>";
    $temp_date=$date1;
    for ($counter=1;$counter<=7;$counter++){

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query36 = "SELECT * FROM stats WHERE businessid = '$businessid' AND date = '$temp_date'";
       $result36 = Treat_DB_ProxyOld::query($query36);
       $num36=Treat_DB_ProxyOld::mysql_numrows($result36);
       //mysql_close();

       $labordollar[$counter]=@Treat_DB_ProxyOld::mysql_result($result36,0,"labor");

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query36 = "SELECT * FROM submit_labor WHERE businessid = '$businessid' AND date = '$temp_date'";
       $result36 = Treat_DB_ProxyOld::query($query36);
       $num36=Treat_DB_ProxyOld::mysql_numrows($result36);
       //mysql_close();

       $export=@Treat_DB_ProxyOld::mysql_result($result36,0,"export");

       $showfont="black";
       if($export==1){$showcolor="#444455";$showfont="white";}
       elseif($export==-1){$showcolor="orange";$showfont="black";}
       elseif($num36>0){$showcolor="#00FF00";}
       else{$showcolor="#00CCFF";}

       if (($counter==7&&$temp_date>$today2)||$security_level<1){$submit_disable="DISABLED";}
       elseif(($counter==7&&$num36>0)||$security_level<1){$submit_disable="DISABLED";}
       //elseif($security_level<2){$submit_disable="DISABLED";}
	   elseif($sbt != 1){$submit_disable="DISABLED"; $showwhy = "<font color=red>You must save before submitting.</font>";}
       else{$submit_disable="";}

       if ($counter==7){$end_date=$temp_date;}

       if (($counter==7&&$num36>0&&$security_level<9)||$security_level<1){$save_disable="DISABLED";}

       echo "<td align=right bgcolor=$showcolor width=11% colspan=2><font color=$showfont>$temp_date</td>";
       $temp_date=nextday($temp_date);
    }
    echo "<td><font size=1>[<a href=buslabor.php?bid=$businessid&cid=$companyid&newdate=$nextdate#labor><font color=blue>NEXT</font></a>]</font></td></tr>";
    //echo "<tr height=24><td colspan=18></td></tr>";

    echo "<tr bgcolor=#CCCC99><td width=4%>Empl#</td><td width=11%>Name</td><td align=right>Rate</td>";
    $temp_date=$date1;
    for ($counter=1;$counter<=7;$counter++){
       $todayname=substr($todayname,0,3);
       if ($todayname=="Sat" || $todayname=="Sun"){$showcolor="yellow";}
       else{$showcolor="";}
       echo "<td align=right bgcolor=$showcolor>Coded</td><td align=right bgcolor=$showcolor>$todayname</td>";
       if ($counter==7){$date2=$temp_date;}
       $temp_date=nextday($temp_date);
       $todayname=dayofweek($temp_date);
    }
    echo "<td align=right><font size=2 width=4%><b></b></td></tr>";

/////////////////LIST EMPLOYEES
    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query34 = "SELECT * FROM payroll_code WHERE companyid = '$companyid' ORDER BY orderid DESC";
    $result34 = Treat_DB_ProxyOld::query($query34);
    $num34=Treat_DB_ProxyOld::mysql_numrows($result34);
    //mysql_close();

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM jobtype WHERE companyid = '$companyid' AND commission = '0' ORDER BY hourly DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    $num--;
    $mycounter=0;
    $coltotal=array();
    $coldollar=array();
    $sheettotal=0;
    while($num>=0){
       $jobtypeid=@Treat_DB_ProxyOld::mysql_result($result,$num,"jobtypeid");
       $jobtypename=@Treat_DB_ProxyOld::mysql_result($result,$num,"name");
       $hourly=@Treat_DB_ProxyOld::mysql_result($result,$num,"hourly");

/////////////NORMAL LABOR
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT distinct login.loginid,login.firstname,login.lastname,login.empl_no,jobtypedetail.rate FROM login,jobtypedetail,payroll_departmentdetail,security_departments WHERE login.oo2 = '$businessid' AND login.move_date <= '$end_date' AND (login.is_deleted = '0' OR (login.is_deleted = '1' AND login.del_date >= '$date1')) AND jobtypedetail.loginid = login.loginid AND jobtypedetail.jobtype = '$jobtypeid' AND (jobtypedetail.is_deleted = '0' OR (jobtypedetail.is_deleted = '1' AND jobtypedetail.del_date >= '$date1')) AND (login.loginid = payroll_departmentdetail.loginid AND payroll_departmentdetail.dept_id = security_departments.dept_id AND security_departments.securityid = '$mysecurity') ORDER BY login.lastname DESC, login.firstname DESC";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
       //mysql_close();

       $num2--;
       $people=0;
       $job_coltotal=array();
       $job_coldollar=array();
       $job_rowtotal=0;

       while($num2>=0){
          $firstname=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"firstname"); 
          $lastname=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"lastname"); 
          $empl_no=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"empl_no");
          $loginid=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"loginid");
          $rate=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"rate");
          $people++;
          $rate=money($rate);

          if($empl_no<1&&$security_level<9){$submit_disable="DISABLED";$showwhy="<font color=red>Pending Employee Number</font>";}
          elseif($empl_no<1){$showwhy="<font color=red>Pending Employee Number</font>";}          

          if ($hourly==0||$security_level<1){$showrate="xxx";}
          elseif($viewrate!=1){$showrate="xxx";}
          else{$showrate=$rate;}

          $rowtotal=0;

          if ($security_level==9){echo "<tr valign=top><td><font size=2>$empl_no</td><td><font size=2><a href=edit_emp_labor.php?cid=$companyid&bid=$businessid&eid=$loginid style=$style><font color=blue>$lastname,$firstname</font></a></td><td align=right><font size=2>$$showrate</td>";}
          else{echo "<tr valign=top><td><font size=2>$empl_no</td><td><font size=2>$lastname,$firstname</td><td align=right><font size=2>$$showrate</td>";}

          $temp_date=$date1;
          $rowhours=0;
          for($counter=1;$counter<=7;$counter++){

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query3 = "SELECT * FROM labor WHERE date = '$temp_date' AND loginid = '$loginid' AND jobtypeid = '$jobtypeid' AND businessid = '$businessid' AND tempid = '0'";
             $result3 = Treat_DB_ProxyOld::query($query3);
             $num3=Treat_DB_ProxyOld::mysql_numrows($result3);
             //mysql_close();

             $rate=@Treat_DB_ProxyOld::mysql_result($result3,0,"rate");
             $hours=@Treat_DB_ProxyOld::mysql_result($result3,0,"hours");
             $coded=@Treat_DB_ProxyOld::mysql_result($result3,0,"coded");

             $rowhours=$rowhours+$hours;

             if($hourly==1){$rowtotal=money($rowtotal+($rate*$hours));}
             elseif($hourly==0&&$hours!=0){$rowtotal=money($rowtotal+($rate/($operate*2)));}

             $job_coltotal[$counter]=@$job_coltotal[$counter]+$hours;

             if($hourly==1){$job_coldollar[$counter]=money(@$job_coldollar[$counter]+($rate*$hours));}
             elseif($hourly==0&&$hours!=0){$job_coldollar[$counter]=money($job_coldollar[$counter]+($rate/($operate*2)));}

             $coltotal[$counter]=@$coltotal[$counter]+$hours;

             if($hourly==1){$coldollar[$counter]=money(@$coldollar[$counter]+($rate*$hours));}
             elseif($hourly==0&&$hours!=0){$coldollar[$counter]=money($coldollar[$counter]+($rate/($operate*2)));}

             echo "<td align=right><select name=code$mycounter tabindex=$counter STYLE='font-size: 10px;' onChange='return submit5()'>";
             $temp_num=$num34-1;
             while($temp_num>=0){
                $codeid=@Treat_DB_ProxyOld::mysql_result($result34,$temp_num,"codeid");
                $code=@Treat_DB_ProxyOld::mysql_result($result34,$temp_num,"code");

                if ($codeid==$coded){$showsel="SELECTED";}
                else{$showsel="";}

                echo "<option value=$codeid $showsel>$code</option>";
                $temp_num--;
             }
             echo "</select></td>";

             if($hourly==1){echo "<td align=right><input type=text size=3 name=hour$mycounter value='$hours' tabindex=$counter STYLE='font-size: 10px;'></td>";}
             else{

                echo "<td align=right><input type=text size=3 name=hour$mycounter value='$hours' tabindex=$counter STYLE='font-size: 10px;'></td>";
             }

             $temp_date=nextday($temp_date);
             $mycounter++;
          }

          echo "<td align=right><font size=2><b>$rowhours</font></td></tr>"; 
          $sheettotal=money($sheettotal+$rowtotal); 
          $job_rowtotal=money($job_rowtotal+$rowtotal);

          if ($rowhours>40&&$hourly==0){$submit_disable="DISABLED";$showwhy="<font color=red>Salaried employee can't have more than 40 hours.</font>";}
		  elseif ($rowhours>50&&$hourly==1){$showwhy="<font color=red>Hourly Employee has more than 50 hours!</font>";}
		  
          $num2--;
       }
/////////////PAST LABOR
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT distinct labor.loginid,labor.rate FROM login,labor WHERE labor.businessid = '$businessid' AND labor.date >= '$date1' AND labor.date<= '$end_date' AND labor.jobtypeid = '$jobtypeid' AND login.oo2 != '$businessid' AND labor.loginid = login.loginid AND labor.tempid = '0'";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
       //mysql_close();

       $num2--;

       while($num2>=0){
          $loginid=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"loginid");
          $rate=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"rate");

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query99 = "SELECT lastname,firstname,empl_no FROM login WHERE loginid = '$loginid'";
          $result99 = Treat_DB_ProxyOld::query($query99);
          //mysql_close();

          $firstname=@Treat_DB_ProxyOld::mysql_result($result99,$num99,"firstname"); 
          $lastname=@Treat_DB_ProxyOld::mysql_result($result99,$num99,"lastname"); 
          $empl_no=@Treat_DB_ProxyOld::mysql_result($result99,$num99,"empl_no");

          $people++;
          $rate=money($rate);
          
          if ($hourly==0||$security_level<1){$showrate="xxx";}
          elseif($viewrate!=1){$showrate="xxx";}
          else{$showrate=$rate;}

          $rowtotal=0;

          if ($security_level==9){echo "<tr valign=top bgcolor=#FF6666><td><font size=2>$empl_no</td><td><font size=2><a href=edit_emp_labor.php?cid=$companyid&bid=$businessid&eid=$loginid style=$style><font color=blue>$lastname,$firstname</font></a></td><td align=right><font size=2>$$showrate</td>";}
          else{echo "<tr valign=top bgcolor=#FF6666><td><font size=2>$empl_no</td><td><font size=2>$lastname,$firstname</td><td align=right><font size=2>$$showrate</td>";}
          
          $temp_date=$date1;
          $rowhours=0;
          for($counter=1;$counter<=7;$counter++){

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query3 = "SELECT * FROM labor WHERE date = '$temp_date' AND loginid = '$loginid' AND jobtypeid = '$jobtypeid' AND businessid = '$businessid' AND tempid = '0'";
             $result3 = Treat_DB_ProxyOld::query($query3);
             $num3=Treat_DB_ProxyOld::mysql_numrows($result3);
             //mysql_close();

             $rate=@Treat_DB_ProxyOld::mysql_result($result3,0,"rate");
             $hours=@Treat_DB_ProxyOld::mysql_result($result3,0,"hours");
             $coded=@Treat_DB_ProxyOld::mysql_result($result3,0,"coded");

             $rowhours=$rowhours+$hours;

             if($hourly==1){$rowtotal=money($rowtotal+($rate*$hours));}
             elseif($hourly==0&&$hours!=0){$rowtotal=money($rowtotal+($rate/($operate*2)));}

             $job_coltotal[$counter]=$job_coltotal[$counter]+$hours;

             if($hourly==1){$job_coldollar[$counter]=money($job_coldollar[$counter]+($rate*$hours));}
             elseif($hourly==0&&$hours!=0){$job_coldollar[$counter]=money($job_coldollar[$counter]+($rate/($operate*2)));}

             $coltotal[$counter]=$coltotal[$counter]+$hours;

             if($hourly==1){$coldollar[$counter]=money($coldollar[$counter]+($rate*$hours));}
             elseif($hourly==0&&$hours!=0){$coldollar[$counter]=money($coldollar[$counter]+($rate/($operate*2)));}

             echo "<td align=right><select name=code$mycounter tabindex=$counter STYLE='font-size: 10px;'>";
             $temp_num=$num34-1;
             while($temp_num>=0){
                $codeid=@Treat_DB_ProxyOld::mysql_result($result34,$temp_num,"codeid");
                $code=@Treat_DB_ProxyOld::mysql_result($result34,$temp_num,"code");

                if ($codeid==$coded){$showsel="SELECTED";}
                else{$showsel="";}

                echo "<option value=$codeid $showsel>$code</option>";
                $temp_num--;
             }
             echo "</select></td><td align=right><input type=text size=3 name=hour$mycounter value='$hours' tabindex=$counter STYLE='font-size: 10px;'></td>";

             $temp_date=nextday($temp_date);
             $mycounter++;
          }

          echo "<td align=right><font size=2><b>$rowhours</font></td></tr>"; 
          $sheettotal=money($sheettotal+$rowtotal); 
          $job_rowtotal=money($job_rowtotal+$rowtotal);
		  
		  if ($rowhours>50&&$hourly==1){$showwhy="<font color=red>Hourly Employee has more than 50 hours!</font>";}

          $num2--;
       }
/////////////ADDED LABOR
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM labor_temp WHERE businessid = '$businessid' AND date >= '$date1' AND date <= '$end_date' AND jobtypeid = '$jobtypeid'";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
       //mysql_close();

       $num2--;

       while($num2>=0){
          $mytempid=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"tempid");
          $loginid=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"loginid");
          $rate=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"rate");

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query99 = "SELECT lastname,firstname,empl_no FROM login WHERE loginid = '$loginid'";
          $result99 = Treat_DB_ProxyOld::query($query99);
          //mysql_close();

          $firstname=@Treat_DB_ProxyOld::mysql_result($result99,$num99,"firstname"); 
          $lastname=@Treat_DB_ProxyOld::mysql_result($result99,$num99,"lastname"); 
          $empl_no=@Treat_DB_ProxyOld::mysql_result($result99,$num99,"empl_no");

          $people++;
          $rate=money($rate);
          
          if ($hourly==0||$security_level<1){$showrate="xxx";}
          elseif($viewrate!=1){$showrate="xxx";}
          else{$showrate=$rate;}

          $rowtotal=0;
 
          $showdelete="<a href=deltempempl.php?bid=$businessid&cid=$companyid&newdate=$date1&tempid=$mytempid onclick='return deletetemp()'><img src=delete.gif height=16 width=16 border=0 alt='Delete Temp'></a>";

          if ($security_level==9){echo "<tr valign=top bgcolor=#FFFF99><td><font size=2>$empl_no</td><td>$showdelete<font size=2><a href=edit_emp_labor.php?cid=$companyid&bid=$businessid&eid=$loginid style=$style><font color=blue>$lastname,$firstname</font></a></td><td align=right><font size=2>$$showrate</td>";}
          else{echo "<tr valign=top bgcolor=#FFFF99><td><font size=2>$empl_no</td><td><font size=2>$lastname,$firstname</td><td align=right><font size=2>$$showrate</td>";}
          //echo "<tr bgcolor=#FFFF99 valign=top><td><font size=2>$empl_no</td><td><font size=2>$lastname,$firstname</td><td align=right><font size=2>$$showrate</td>";

          $temp_date=$date1;
          $rowhours=0;
          for($counter=1;$counter<=7;$counter++){

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query3 = "SELECT * FROM labor WHERE date = '$temp_date' AND loginid = '$loginid' AND jobtypeid = '$jobtypeid' AND businessid = '$businessid'";
             $result3 = Treat_DB_ProxyOld::query($query3);
             $num3=Treat_DB_ProxyOld::mysql_numrows($result3);
             //mysql_close();

             $rate=@Treat_DB_ProxyOld::mysql_result($result3,0,"rate");
             $hours=@Treat_DB_ProxyOld::mysql_result($result3,0,"hours");
             $coded=@Treat_DB_ProxyOld::mysql_result($result3,0,"coded");

             $rowhours=$rowhours+$hours;

             if($hourly==1){$rowtotal=money($rowtotal+($rate*$hours));}
             elseif($hourly==0&&$hours!=0){$rowtotal=money($rowtotal+($rate/($operate*2)));}

             $job_coltotal[$counter]=$job_coltotal[$counter]+$hours;

             if($hourly==1){$job_coldollar[$counter]=money($job_coldollar[$counter]+($rate*$hours));}
             elseif($hourly==0&&$hours!=0){$job_coldollar[$counter]=money($job_coldollar[$counter]+($rate/($operate*2)));}

             $coltotal[$counter]=$coltotal[$counter]+$hours;

             if($hourly==1){$coldollar[$counter]=money($coldollar[$counter]+($rate*$hours));}
             elseif($hourly==0&&$hours!=0){$coldollar[$counter]=money($coldollar[$counter]+($rate/($operate*2)));}

             echo "<td align=right><select name=code$mycounter tabindex=$counter STYLE='font-size: 10px;'>";
             $temp_num=$num34-1;
             while($temp_num>=0){
                $codeid=@Treat_DB_ProxyOld::mysql_result($result34,$temp_num,"codeid");
                $code=@Treat_DB_ProxyOld::mysql_result($result34,$temp_num,"code");

                if ($codeid==$coded){$showsel="SELECTED";}
                else{$showsel="";}

                echo "<option value=$codeid $showsel>$code</option>";
                $temp_num--;
             }
             echo "</select></td><td align=right><input type=text size=3 name=hour$mycounter value='$hours' tabindex=$counter STYLE='font-size: 10px;'></td>";

             $temp_date=nextday($temp_date);
             $mycounter++;
          }

          echo "<td align=right><font size=2><b>$rowhours</font></td></tr>"; 
          $sheettotal=money($sheettotal+$rowtotal); 
          $job_rowtotal=money($job_rowtotal+$rowtotal);
		  
		  if ($rowhours>50&&$hourly==1){$showwhy="<font color=red>Hourly Employee has more than 50 hours!</font>";}

          $num2--;
       }
/////////END ADDED LABOR
       $num--;
       if($people>0){echo "<tr bgcolor=#CCCCCC><td colspan=3><font size=2><b>$jobtypename Totals</font></td><td align=right colspan=2><font size=2>$job_coltotal[1] hrs</td><td align=right colspan=2><font size=2>$job_coltotal[2] hrs</td><td align=right colspan=2><font size=2>$job_coltotal[3] hrs</td><td align=right colspan=2><font size=2>$job_coltotal[4] hrs</td><td align=right colspan=2><font size=2>$job_coltotal[5] hrs</td><td align=right colspan=2><font size=2>$job_coltotal[6] hrs</td><td align=right colspan=2><font size=2>$job_coltotal[7] hrs</td><td align=right><font size=2><b></td></tr>";}
    }
    echo "<tr bgcolor=#CCCC99><td colspan=3><font size=2><b>Totals</font></td><td align=right colspan=2><font size=2><b>$coltotal[1] hrs</td><td align=right colspan=2><font size=2><b>$coltotal[2] hrs</td><td align=right colspan=2><font size=2><b>$coltotal[3] hrs</td><td align=right colspan=2><font size=2><b>$coltotal[4] hrs</td><td align=right colspan=2><font size=2><b>$coltotal[5] hrs</td><td align=right colspan=2><font size=2><b>$coltotal[6] hrs</td><td align=right colspan=2><font size=2><b>$coltotal[7] hrs</td><td align=right><font size=2><b></td></tr>";

  if ($security_level>1){
    echo "<tr bgcolor=#CCCC99><td colspan=3><font size=2><b>Dollars</font></td>";
    for ($counter=1;$counter<=7;$counter++){$labordollar[$counter]=money($labordollar[$counter]);echo "<td align=right colspan=2><font size=2><b>$$labordollar[$counter]</td>";}
    echo "<td></td></tr>";
  }
//////////////Removed by bd $submit_disable
    if($auto_submit_labor==1){$myform="submit_labor_dept.php";}
    else{$myform="submit_labor.php";}

    echo "<tr><td colspan=3><input type=submit value='Save' tabindex=8 $save_disable></td><td></form></td><td align=right colspan=14><form action=$myform method=post onSubmit='return disableForm(this);'><input type=hidden name=companyid value=$companyid><input type=hidden name=businessid value=$businessid><input type=hidden name=date value=$date1> $showwhy <input type=submit value='Submit Labor' tabindex=9  onclick='return submit2()' $submit_disable></td></tr>";
    echo "<tr><td colspan=18></form></td></tr>";
    echo "</table></center>";
    echo "<tr height=1 bgcolor=black><td colspan=10></td></tr>";
    echo "</td></tr></table></center>";

/////////////ADD TEMP EMPLOYEE
    echo "<p><center><table width=90% border=0><tr><td>";
    echo "<table bgcolor=#E8E7E7 width=75%>";

    echo "<tr><td><form action=add_emp_labor.php method=post onSubmit='return disableForm(this);'><input type=hidden name=companyid value=$companyid><input type=hidden name=businessid value=$businessid><input type=hidden name=date value=$date1><select name=loginid>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM login,jobtype,jobtypedetail WHERE login.companyid = '$companyid' AND login.security_level <= '3' AND login.oo2 != '$businessid' AND login.is_deleted = '0' AND (jobtypedetail.is_deleted = '0' AND jobtypedetail.loginid = login.loginid AND jobtypedetail.jobtype = jobtype.jobtypeid AND jobtype.hourly = '1') ORDER BY lastname DESC,firstname DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    $num--;
    while($num>=0){
       $loginid=@Treat_DB_ProxyOld::mysql_result($result,$num,"loginid");
       $lastname=@Treat_DB_ProxyOld::mysql_result($result,$num,"lastname");
       $firstname=@Treat_DB_ProxyOld::mysql_result($result,$num,"firstname");
       $empl_no=@Treat_DB_ProxyOld::mysql_result($result,$num,"empl_no");
       $oo2=@Treat_DB_ProxyOld::mysql_result($result,$num,"oo2");
       $jobtype=@Treat_DB_ProxyOld::mysql_result($result,$num,"jobtype");

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT name FROM jobtype WHERE jobtypeid = '$jobtype'";
       $result2 = Treat_DB_ProxyOld::query($query2);
       //mysql_close();

       $job_name=@Treat_DB_ProxyOld::mysql_result($result2,0,"name");

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM labor_temp WHERE businessid = '$businessid' AND date = '$date1' AND loginid = '$loginid'";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
       //mysql_close();

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM jobtypedetail WHERE loginid = '$loginid'";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num3=Treat_DB_ProxyOld::mysql_numrows($result2);
       //mysql_close();

       if($num2==0&&$num3>0){echo "<option value=$loginid>$lastname, $firstname ($empl_no) - $job_name</option>";}

       $num--;
    }
    echo "</select> <input type=submit value='Add Employee' $save_disable onclick='return submit3()'></td><td></form></td></tr>";
    echo "</table>";
    echo "</td></tr></table></center>";

/////////////////////////NEW EMPLOYEES
    echo "<center><table width=90%><tr><td>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query3 = "SELECT * FROM login_pend WHERE businessid = '$businessid'";
    $result3 = Treat_DB_ProxyOld::query($query3);
    $num3=Treat_DB_ProxyOld::mysql_numrows($result3);
    //mysql_close();

    if ($num3>0){$showpend="<b> - <font color=blue>$num3 Request(s) Pending</font></b>";}

    echo "<table width=75% bgcolor=#E8E7E7>";

    echo "<tr><td colspan=2><b>Request New Employee</b> $showpend</td></tr>";
    echo "<tr><td><form action=labor_request.php method=post><input type=hidden name=newdate value=$date1><input type=hidden name=companyid value=$companyid><input type=hidden name=businessid value=$businessid><input type=text name=firstname value='firstname' size=20> <input type=text name=lastname value='lastname' size=20> <select name=jobtypeid>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query2 = "SELECT * FROM jobtype WHERE companyid = '$companyid' AND hourly = '1' ORDER BY name DESC";
    $result2 = Treat_DB_ProxyOld::query($query2);
    $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
    //mysql_close();

    $num2--;
    while($num2>=0){
       $job_typename=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"name");
       $jobtypeid=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"jobtypeid");

       echo "<option value=$jobtypeid>$job_typename</option>";   

       $num2--;
    }
    echo "</select> <b>$</b><input type=text size=5 name=rate> <input type=submit value='Request' onclick='return submit4()'></td><td></form></td></tr></table>";

    echo "</td></tr></table></center>";
/////////////////////////REQUEST TERM/TRANSFER
    $is_sent = isset($_GET["send"])?$_GET["send"]:'';

    if($is_sent==1){$sendmessage="<font color=green> - Message Sent</font>";}
    elseif($is_sent==2){$sendmessage="<font color=red> - Please enter a Message</font>";}

    echo "<center><table width=90%><tr><td>";

    echo "<table width=75% bgcolor=#E8E7E7>";

    echo "<tr><td colspan=2><a name=message></a><b>Request Termination/Transfer/Other $sendmessage</b></td></tr>";
    echo "<tr><td><form action=email_labor_request.php method=post><input type=hidden name=districtid value=$districtid><input type=hidden name=companyid value=$companyid><input type=hidden name=businessid value=$businessid><select name=eid>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query2 = "SELECT * FROM login WHERE oo2 = '$businessid' AND is_deleted = '0'";
    $result2 = Treat_DB_ProxyOld::query($query2);
    $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
    //mysql_close();

    $num2--;
    while($num2>=0){
       $loginid=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"loginid");
       $lastname=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"lastname");
       $firstname=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"firstname");
       $empl_no=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"empl_no");

       echo "<option value=$loginid>$lastname, $firstname ($empl_no)</option>";   

       $num2--;
    }
    echo "</select> Message: <input type=text size=40 name=request> <input type=submit value='Send'></td><td></form></td></tr></table>";

    echo "</td></tr></table></center>";
/////////////////////////AUDIT TRAIL
    echo "<center><table width=90%><tr><td>";

    $temp_date=prevday($temp_date);
    echo "<table width=50% bgcolor=#E8E7E7>";

    if($audit==1){$showchange="<a href=buslabor.php?cid=$companyid&bid=$businessid&newdate=$date1 style=$style><font color=blue>HIDE</font></a>";}
    else{$showchange="<a href=buslabor.php?cid=$companyid&bid=$businessid&newdate=$date1&audit=1#audit style=$style><font color=blue>SHOW</font></a>";}

    echo "<tr><td colspan=3><font size=1><b><a name=audit>AUDIT TRAIL</a> </b> $showchange</td></tr>";
    if($audit==1){
    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query2 = "SELECT * FROM audit_labor WHERE businessid = '$businessid' AND date >= '$date1' AND date <= '$temp_date' ORDER BY time";
    $result2 = Treat_DB_ProxyOld::query($query2);
    $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
    //mysql_close();

    $num2--;
    while($num2>=0){
       $time=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"time");
       $comment=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"comment");
       $who=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"user");

       echo "<tr bgcolor=white><td><font size=1>$comment</td><td><font size=1>$who</td><td><font size=1>$time</td></tr>";   

       $num2--;
    }
    }
    echo "</table>";

    echo "</td></tr></table></center>";

    //mysql_close();

    echo "<br><FORM ACTION=businesstrack.php method=post>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<center><INPUT TYPE=submit VALUE=' Return to Main Page'></FORM></center><DIV ID=testdiv1 STYLE=position:absolute;visibility:hidden;background-color:white;layer-background-color:white;></DIV></body>";
	
	google_page_track();
}
?>