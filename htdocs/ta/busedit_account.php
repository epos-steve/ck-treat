<?php
setlocale( LC_ALL, 'en_US.utf8' );

function money($diff){
	$findme='.';
	$double='.00';
	$single='0';
	$double2='00';
	$pos1 = strpos($diff, $findme);
	$pos2 = strlen($diff);
	if ($pos1==""){$diff="$diff$double";}
	elseif ($pos2-$pos1==2){$diff="$diff$single";}
	elseif ($pos2-$pos1==1){$diff="$diff$double2";}
	else{}
	$dot = strpos($diff, $findme);
	$diff = substr($diff, 0, $dot+4);
	$diff=round($diff, 2);
	if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
	elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
	$diff = substr($diff, 0, $dot+3);
	$pos1 = strpos($diff, $findme);
	$pos2 = strlen($diff);
	if ($pos1==""){$diff="$diff$double";}
	elseif ($pos2-$pos1==2){$diff="$diff$single";}
	elseif ($pos2-$pos1==1){$diff="$diff$double2";}
	else{}
	return $diff;
}

//include_once( 'db.php' );
if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
require_once( DOC_ROOT . '/bootstrap.php' );

$style = 'text-decoration:none';

/*$user = $_COOKIE['usercook'];
$pass = $_COOKIE['passcook'];
$businessid = $_GET['bid'];
$companyid = $_GET['cid'];
$accountid = $_GET['aid'];*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook');
$businessid = \EE\Controller\Base::getGetVariable('bid');
$companyid = \EE\Controller\Base::getGetVariable('cid');
$accountid = \EE\Controller\Base::getGetVariable('aid');

if ( isset( $_GET['fwo'] ) && $_GET['fwo'] && 'false' != $_GET['fwo'] ) {
	$force_write_only = true;
}
else {
	$force_write_only = false;
}

$passed_cookie_login = require_once( realpath( DOC_ROOT . '/../lib/inc/cookie-login.php' ) );
$page_business = Treat_Model_Business_Singleton::getSingleton();

if (
	(
		$passed_cookie_login
		&& (
			(
				$GLOBALS['security_level'] == 1
				&& $GLOBALS['bid'] == $page_business->getBusinessId()
				&& $GLOBALS['cid'] == $page_business->getCompanyId()
			)
			|| (
				$GLOBALS['security_level'] > 1
				&& $GLOBALS['cid'] == $page_business->getCompanyId()
			)
		)
	)
) {
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
	<head>
		<title>Essential Elements :: Treat America :: busedit_account.php </title>
		<script type="text/javascript" src="/assets/js/dynamicdrive-disableform.js"></script>
		<script type="text/javascript" src="/assets/js/changePage.js"></script>
		<script type="text/javascript" src="/assets/js/ta/busedit_account.js"></script>
		<link rel="stylesheet" type="text/css" href="/assets/css/style.css" />
	</head>
	<body>
		<center id="page_wrapper">
			<div style="text-align: left; width: 90%;">
				<img src="/ta/logo.jpg" alt="" />
			</div>

<?php
if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
	echo $_SESSION->getFlashDebug( "\t\t\t<pre class=\"flash_debug\">%s</pre>\n" );
}
echo $_SESSION->getFlashError(   "\t\t\t<div class=\"flash_error\">%s</div>\n"   );
echo $_SESSION->getFlashMessage( "\t\t\t<div class=\"flash_message\">%s</div>\n" );
?>
<?php
//////////////////////////////////////////////BEGIN EDIT ACCOUNT////////////////////////////////////////////////////////////
	$page_account = Treat_Model_Account::getByAccountId( $accountid );
	if ( !$page_account ) {
		$page_account = new Treat_Model_Account_Object();
	}
?>

			<form action="/ta/bussaveaccount.php" method="post" onSubmit="return disableForm(this);">
				<input type="hidden" name="username" value="<?php echo $user; ?>" />
				<input type="hidden" name="password" value="<?php echo $pass; ?>" />
				<input type="hidden" name="businessid" value="<?php echo $page_business->businessid; ?>" />
				<input type="hidden" name="companyid" value="<?php echo $companyid; ?>" />
				<input type="hidden" name="accountid" value="<?php echo $accountid; ?>" />
				<center>
					<table cellspacing="0" cellpadding="0" border="0" width="60%">
						<tr bgcolor="black">
							<td colspan="2" height="1"></td>
						</tr>
						<tr valign="top" bgcolor="#e8e7e7">
							<td colspan="2">
								<table>
									<tr>
										<td align="right">
											<font color="red">Account:</font>
										</td>
										<td>
											<?php echo $page_account->name; ?>
											(<?php echo $page_account->accountnum; ?>)
										</td>
									</tr>
									<tr bgcolor="#e8e7e7">
										<td align="right">
											<font color="red">Email:</font>
										</td>
										<td>
											<input type="text" name="accountemail" size="30" value="<?php echo $page_account->email; ?>" />
										</td>
									</tr>
									<tr bgcolor="#e8e7e7">
										<td align="right">
											<font color="red">Address:</font>
										</td>
										<td>
											<input type="text" name="accountattn" size="30" value="<?php echo $page_account->attn; ?>" />
										</td>
									</tr>
									<tr bgcolor="#e8e7e7">
										<td align="right">
											<font color="red">Address:</font>
										</td>
										<td>
											<input type="text" name="accountstreet" size="30" value="<?php echo $page_account->street; ?>" />
										</td>
									</tr>
									<tr bgcolor="#e8e7e7">
										<td align="right">
											<font color="red">City:</font>
										</td>
										<td>
											<input type="text" name="accountcity" size="20" value="<?php echo $page_account->city; ?>" />
										</td>
									</tr>
									<tr bgcolor="#e8e7e7">
										<td align="right">
											<font color="red">State:</font>
										</td>
										<td>
											<input type="text" name="accountstate" size="5" value="<?php echo $page_account->state; ?>" />
										</td>
									</tr>
									<tr bgcolor="#e8e7e7">
										<td align="right">
											<font color="red">Zip Code:</font>
										</td>
										<td>
											<input type="text" name="accountzip" size="8" value="<?php echo $page_account->zip; ?>" />
										</td>
									</tr>
									<tr bgcolor="#e8e7e7">
										<td align="right">
											<input type="checkbox" name="print_labels" <?php
												if ( 1 == $page_account->print_labels ) {
													echo ' checked="checked"';
												}
											?> />
										</td>
										<td>
											Print Labels
										</td>
									</tr>

									<tr bgcolor="black">
										<td colspan="2" height="1"></td>
									</tr>
									<tr bgcolor="#e8e7e7">
										<td align="right">
											<input type="checkbox" name="b_mailing" <?php
												if ( 1 == $page_account->b_mailing ) {
													echo ' checked="checked"';
												}
											?> />
										</td>
										<td>
											<font color="gray">Alternate Billing Address</font>
										</td>
									</tr>
									<tr bgcolor="#e8e7e7">
										<td align="right">
											<font color="gray">Address:</font>
										</td>
										<td>
											<input type="text" name="b_accountstreet" size="30" value="<?php echo $page_account->b_street; ?>" />
										</td>
									</tr>
									<tr bgcolor="#e8e7e7">
										<td align="right">
											<font color="gray">City:</font>
										</td>
										<td>
											<input type="text" name="b_accountcity" size="20" value="<?php echo $page_account->b_city; ?>" />
										</td>
									</tr>
									<tr bgcolor="#e8e7e7">
										<td align="right">
											<font color="gray">State:</font>
										</td>
										<td>
											<input type="text" name="b_accountstate" size="5" value="<?php echo $page_account->b_state; ?>" />
										</td>
									</tr>
									<tr bgcolor="#e8e7e7">
										<td align="right">
											<font color="gray">Zip Code:</font>
										</td>
										<td>
											<input type="text" name="b_accountzip" size="8" value="<?php echo $page_account->b_zip; ?>" />
										</td>
									</tr>
									<tr bgcolor="black">
										<td colspan="2" height="1"></td>
									</tr>

									<tr bgcolor="#e8e7e7">
										<td align="right">
											<font color="red">Phone #:</font>
										</td>
										<td>
											<input type="text" name="accountphone" size="12" value="<?php echo $page_account->phone; ?>" />
										</td>
									</tr>
									<tr bgcolor="#e8e7e7">
										<td align="right">
											<font color="red">Fax #:</font>
										</td>
										<td>
											<input type="text" name="accountfax" size="12" value="<?php echo $page_account->fax; ?>" />
										</td>
									</tr>
									<tr bgcolor="#e8e7e7">
										<td align="right">
											<font color="red">Tax Exempt #:</font>
										</td>
										<td>
											<input type="text" name="taxid" size="12" value="<?php echo $page_account->taxid; ?>" />
										</td>
									</tr>
									<tr bgcolor="#e8e7e7">
										<td align="right">
											<font color="red">Tax Rate (%):</font>
										</td>
										<td>
											<input type="text" name="tax_rate" size="12" value="<?php echo $page_account->tax_rate; ?>" />
										</td>
									</tr>
									<tr bgcolor="#e8e7e7">
										<td align="right">
											<font color="red">Cost Center:</font>
										</td>
										<td>
											<input type="text" name="costcenter" size="20" value="<?php echo $page_account->costcenter; ?>" />
										</td>
									</tr>
									<tr bgcolor="#e8e7e7">
										<td align="right">
											<font color="red">Local Acct#:</font>
										</td>
										<td>
											<input type="text" name="localacct" size="20" value="<?php echo $page_account->contract_num; ?>" />
										</td>
									</tr>
									<tr bgcolor="#e8e7e7">
										<td align="right">
											<font color="red">Delivery Charge (Under 50):</font>
										</td>
										<td>
											<input type="text" name="acct_charge" size="20" value="<?php echo $page_account->deliver_charge; ?>" />
										</td>
									</tr>
									<tr bgcolor="#e8e7e7">
										<td align="right">
											<font color="red">Delivery Charge (Over 50):</font>
										</td>
										<td>
											<input type="text" name="acct_charge2" size="20" value="<?php echo $page_account->deliver_charge2; ?>" />
										</td>
									</tr>
									<tr bgcolor="#e8e7e7">
										<td align="right">
											<font color="red">Driver:</font>
										</td>
										<td>
											<input type="text" name="assign" size="20" value="<?php echo $page_account->assign; ?>" />
										</td>
									</tr>
									<tr bgcolor="#e8e7e7">
										<td align="right">
											<input type="checkbox" name="allow_sub" <?php
												if ( 1 == $page_account->allow_sub ) {
													echo ' checked="checked"';
												}
											?> />
										</td>
										<td>
											Allow Menu Substitutions
										</td>
									</tr>
									<tr bgcolor="#e8e7e7">
										<td align="right">
											<font color="red">Menu:</font>
										</td>
										<td>
											<select name="menu">
												<option value="-1">Default</option>

<?php
	
	#mysql_connect($dbhost,$username,$password);
	#@mysql_select_db($database) or die( 'Unable to select database');
	$query = "SELECT * FROM menu_type WHERE companyid = '$companyid' AND (businessid = '{$page_business->businessid}' OR businessid = '0') AND type = '1' ORDER BY menu_typename DESC";
	$result = Treat_DB_ProxyOld::query( $query );
	$num = Treat_DB_ProxyOld::mysql_numrows( $result );
	#mysql_close();

	$num--;
	//while ( $row = mysql_fetch_object( $result ) ) {
	while ( $row = Treat_DB_ProxyOld::mysql_fetch_object( $result ) ) {
?>

												<option value="<?php
													echo $row->menu_typeid;
												?>"<?php
													if ( $page_account->menu == $row->menu_typeid ) {
														echo ' selected="selected"';
													}
												?>><?php
													echo $row->menu_typename;
												?></option>
<?php
		$num--;
	}
	
?>

											</select>
										</td>
									</tr>

									<tr bgcolor="#e8e7e7">
										<td align="right">
											<font color="red">Account Status:</font>
										</td>
										<td>
											<select name="acct_status">
												<option value="0">Active</option>
												<option value="1"<?php
													if ( 1 == $page_account->status ) {
														echo ' selected="selected"';
													}
												?>>Inactive</option>
												<option value="2"<?php
													if ( 2 == $page_account->status ) {
														echo ' selected="selected"';
													}
												?>>Suspended</option>
												<option value="3"<?php
													if ( 3 == $page_account->status ) {
														echo ' selected="selected"';
													}
												?>>COD</option>
											</select>
										</td>
									</tr>
									<tr bgcolor="#e8e7e7">
										<td colspan="2">
											<center>
												<input type="submit" value="  Save  " />
											</center>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr bgcolor="black">
							<td colspan="2" height="1"></td>
						</tr>
					</table>
				</center>
			</form>
<?php

	//////////////////////////////PRICE MATRIX
	#mysql_connect($dbhost,$username,$password);
	#@mysql_select_db($database) or die( 'Unable to select database');
	$query11 = "SELECT * FROM menu_portion WHERE menu_typeid = '{$page_account->menu}' ORDER BY orderid DESC";
	$result11 = Treat_DB_ProxyOld::query( $query11 );
	$num11 = Treat_DB_ProxyOld::mysql_numrows( $result11 );
	$menu_portions = array();
	while ( $row = Treat_DB_ProxyOld::mysql_fetch_object( $result11 ) ) {
		$menu_portions[ $row->menu_portionid ] = $row;
	}
	#mysql_close();

	$colnum = $num11 + 1;
	$colnum2 = $colnum + 2;
	$curarrow = 1;

	$daypartcount = 1;

	#mysql_connect($dbhost,$username,$password);
	#@mysql_select_db($database) or die( 'Unable to select database');
	$query = "SELECT * FROM menu_daypart WHERE menu_typeid = '{$page_account->menu}' ORDER BY orderid DESC";
	$result = Treat_DB_ProxyOld::query( $query );
	$num = Treat_DB_ProxyOld::mysql_numrows( $result );
	#mysql_close();

?>

			<p>
			<a name="matrix"></a>
			<form action="/ta/saveordermatrixcust.php" method="post" onSubmit="return disableForm(this);">
				<input type="hidden" name="menu_type" value="<?php echo $page_account->menu; ?>" />
				<input type="hidden" name="businessid" value="<?php echo $page_business->businessid; ?>" />
				<input type="hidden" name="companyid" value="<?php echo $companyid; ?>" />
				<input type="hidden" name="accountid" value="<?php echo $accountid; ?>" />
				<table width="60%" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="<?php echo $colnum2; ?>" height="16" bgcolor="black"></td>
					</tr>
					<tr>
						<td width="1" bgcolor="black"></td>
						<td colspan="<?php echo $colnum; ?>"
							style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr=white, startColorstr=#DCDCDC, gradientType=0);"
						>
							<b>Price Matrix</b>
						</td>
						<td width="1" bgcolor="black"></td>
					</tr>
<?php

	$num--;
	//while ( $row = mysql_fetch_object( $result ) ) {
	while ( $row = Treat_DB_ProxyOld::mysql_fetch_object( $result ) ) {
		#mysql_connect($dbhost,$username,$password);
		#@mysql_select_db($database) or die( 'Unable to select database');
		$query10 = "SELECT * FROM menu_daypartdetail WHERE daypartid = '{$row->menu_daypartid}' AND accountid = '$accountid'";
		$result10 = Treat_DB_ProxyOld::query( $query10, $force_write_only );
		$num10 = Treat_DB_ProxyOld::mysql_numrows( $result10 );
		#mysql_close();

		if ( $num10 != 0 ) {
			$isactive = ' checked="checked"';
			$showcolor = 'yellow';
		}
		else {
			$isactive = '';
			$showcolor = 'white';
		}
?>

					<tr bgcolor="black" height="1">
						<td colspan="<?php echo $colnum2; ?>">
							<input type="hidden" name="username" value="<?php echo $user; ?>" />
							<input type="hidden" name="password" value="<?php echo $pass; ?>" />
						</td>
					</tr>
					<tr bgcolor="<?php echo $showcolor; ?>">
						<td width="1" bgcolor="#e8e7e7"></td>
						<td>
							<input type="checkbox" name="active[<?php
								echo $row->menu_daypartid;
							?>]"<?php
								echo $isactive;
							?> />
							<b><?php echo $row->menu_daypartname; ?></b>
						</td>
<?php

		foreach ( $menu_portions as $menu_portion ) {
?>

						<td align="right">
							<b><?php echo $menu_portion->menu_portionname; ?></b>
						</td>
<?php
		}

?>

						<td width="1" bgcolor="#e8e7e7"></td>
					</tr>
					<tr bgcolor="#e8e7e7" height="1">
						<td colspan="<?php echo $colnum2; ?>"></td>
					</tr>
					<tr bgcolor="<?php echo $showcolor; ?>">
						<td width="1" bgcolor="#e8e7e7"></td>
						<td>
							<i>Price:</i>
						</td>
<?php
		$countme = 1;
		foreach ( $menu_portions as $menu_portion ) {
			$query12 = "SELECT * FROM order_matrixcust WHERE daypartid = '{$row->menu_daypartid}' AND portionid = '{$menu_portion->menu_portionid}' AND menu_typeid = '{$page_account->menu}' AND accountid = '$accountid'";
			$result12 = Treat_DB_ProxyOld::query( $query12, $force_write_only );
			$portionvalue = @Treat_DB_ProxyOld::mysql_result( $result12, 0, 'price' );
?>

						<td align="right">
							<b>$</b
							><input type="text" size="4" name="portion[<?php
								echo $row->menu_daypartid;
							?>][<?php
								echo $menu_portion->menu_portionid;
							?>]" value="<?php
								echo $portionvalue;
							?>" onkeydown="return checkKey()" IndexId="<?php
								echo $curarrow;
							?>" />
						</td>
<?php
			$curarrow = $curarrow + 1000;
			$countme++;
		}
?>

						<td width="1" bgcolor="#e8e7e7"></td>
					</tr>
<?php
		if ( $countme != 1 ) {
			$curarrow = $curarrow - ( ( $countme - 1 ) * 1000 ) + 1;
		}

		$daypartcount++;
		$num--;
	}

?>

					<tr>
						<td colspan="<?php echo $colnum2; ?>" height="1" bgcolor="black"></td>
					</tr>
					<tr>
						<td width="1" bgcolor="black"></td>
						<td colspan="<?php echo $colnum; ?>" align="right"
							style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr=#DCDCDC, startColorstr=white, gradientType=0);"
						>
							<input type="submit" value="Save" onclick="return confirmsave()" />
						</td>
						<td width="1" bgcolor="black"></td>
					</tr>
					<tr>
						<td colspan="<?php echo $colnum2; ?>" height="1" bgcolor="black"></td>
					</tr>
				</table>
			</form>
<?php

//////////////////////////////////////////////END////////////////////////////////////////////////////////////

?>

			<form action="/ta/busordertrack.php?cid=<?php
				echo $companyid;
			?>&amp;bid=<?php
				echo $page_business->businessid;
			?>" method="post" style="margin-bottom: 1em;">
				<input type="hidden" name="username" value="<?php echo $user; ?>" />
				<input type="hidden" name="password" value="<?php echo $pass; ?>" />
				<input type="submit" value=" Return " />
			</form>
		</center>
<?php
	
	google_page_track();
?>
	</body>
</html>
<?php
}
?>