<head>
<SCRIPT LANGUAGE=javascript><!--
function delconfirm(){return confirm('ARE YOU SURE THIS IS A CUSTOMER AND NOT A VENDOR???  If it is a vendor, press Cancel and scroll down to Vendor Requests.');}
// --></SCRIPT>

<script language="JavaScript" 
   type="text/JavaScript">
function changePage(newLoc)
 {
   nextPage = newLoc.options[newLoc.selectedIndex].value
		
   if (nextPage != "")
   {
      document.location.href = nextPage
   }
 }
</script>

</head>

<?php

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+3);
        if ($diff > 0 && $diff < '0.01'){$diff="0.01";}
        elseif($diff < 0 && $diff > '-0.01'){$diff="-0.01";}
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      if ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='32')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='08' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      $day=$day-31;  
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";} 
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

define('DOC_ROOT', dirname(dirname(__FILE__)));
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');

$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");

/*$user=$_COOKIE["usercook"];
$pass=$_COOKIE["passcook"];
$view=$_COOKIE["viewcook"];
$date1=$_COOKIE["date1cook"];
$date2=$_COOKIE["date2cook"];
$businessid=$_GET["bid"];
$companyid=$_GET["cid"];
$showall=$_GET["showall"];
$success=$_GET["success"];*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook');
$view = \EE\Controller\Base::getSessionCookieVariable('viewcook');
$date1 = \EE\Controller\Base::getSessionCookieVariable('date1cook');
$date2 = \EE\Controller\Base::getSessionCookieVariable('date2cook');
$businessid = \EE\Controller\Base::getGetVariable('bid');
$companyid = \EE\Controller\Base::getGetVariable('cid');
$showall = \EE\Controller\Base::getGetVariable('showall');
$success = \EE\Controller\Base::getGetVariable('success');

/*mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");*/
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=Treat_DB_ProxyOld::mysql_numrows($result);
//

$security_level=Treat_DB_ProxyOld::mysql_result($result,0,"security_level");
$mysecurity=Treat_DB_ProxyOld::mysql_result($result,0,"security");
$bid=Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
$cid=Treat_DB_ProxyOld::mysql_result($result,0,"companyid");
$bid2=Treat_DB_ProxyOld::mysql_result($result,0,"busid2");
$bid3=Treat_DB_ProxyOld::mysql_result($result,0,"busid3");
$bid4=Treat_DB_ProxyOld::mysql_result($result,0,"busid4");
$bid5=Treat_DB_ProxyOld::mysql_result($result,0,"busid5");
$bid6=Treat_DB_ProxyOld::mysql_result($result,0,"busid6");
$bid7=Treat_DB_ProxyOld::mysql_result($result,0,"busid7");
$bid8=Treat_DB_ProxyOld::mysql_result($result,0,"busid8");
$bid9=Treat_DB_ProxyOld::mysql_result($result,0,"busid9");
$bid10=Treat_DB_ProxyOld::mysql_result($result,0,"busid10");
$loginid=Treat_DB_ProxyOld::mysql_result($result,0,"loginid");

$pr1=Treat_DB_ProxyOld::mysql_result($result,0,"payroll");
$pr2=Treat_DB_ProxyOld::mysql_result($result,0,"pr2");
$pr3=Treat_DB_ProxyOld::mysql_result($result,0,"pr3");
$pr4=Treat_DB_ProxyOld::mysql_result($result,0,"pr4");
$pr5=Treat_DB_ProxyOld::mysql_result($result,0,"pr5");
$pr6=Treat_DB_ProxyOld::mysql_result($result,0,"pr6");
$pr7=Treat_DB_ProxyOld::mysql_result($result,0,"pr7");
$pr8=Treat_DB_ProxyOld::mysql_result($result,0,"pr8");
$pr9=Treat_DB_ProxyOld::mysql_result($result,0,"pr9");
$pr10=Treat_DB_ProxyOld::mysql_result($result,0,"pr10");

if ($num != 1 || ($security_level==1 && ($bid != $businessid || $cid != $companyid)) || ($security_level > 1 AND $cid != $companyid) || ($user == "" && $pass == ""))
{
    echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%><tr><td colspan=2>";
    echo "<a href=businesstrack.php><img src=logo.jpg border=0 height=43 width=205></a><p></td></tr>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM company WHERE companyid = '$companyid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $companyname=Treat_DB_ProxyOld::mysql_result($result,0,"companyname");
	$com_logo=@Treat_DB_ProxyOld::mysql_result($result,0,"logo");

	if($com_logo == ""){$com_logo = "talogo.gif";}

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM security WHERE securityid = '$mysecurity'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $sec_bus_sales=Treat_DB_ProxyOld::mysql_result($result,0,"bus_sales");
    $sec_bus_invoice=Treat_DB_ProxyOld::mysql_result($result,0,"bus_invoice");
    $sec_bus_order=Treat_DB_ProxyOld::mysql_result($result,0,"bus_order");
    $sec_bus_payable=Treat_DB_ProxyOld::mysql_result($result,0,"bus_payable");
    $sec_bus_inventor1=Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor1");
    $sec_bus_control=Treat_DB_ProxyOld::mysql_result($result,0,"bus_control");
    $sec_bus_menu=Treat_DB_ProxyOld::mysql_result($result,0,"bus_menu");
    $sec_bus_order_setup=Treat_DB_ProxyOld::mysql_result($result,0,"bus_order_setup");
    $sec_bus_request=Treat_DB_ProxyOld::mysql_result($result,0,"bus_request");
    $sec_route_collection=Treat_DB_ProxyOld::mysql_result($result,0,"route_collection");
    $sec_route_labor=Treat_DB_ProxyOld::mysql_result($result,0,"route_labor");
    $sec_route_detail=Treat_DB_ProxyOld::mysql_result($result,0,"route_detail");
    $sec_bus_labor=Treat_DB_ProxyOld::mysql_result($result,0,"bus_labor");
	$sec_bus_budget=Treat_DB_ProxyOld::mysql_result($result,0,"bus_budget");

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM business WHERE companyid = '$companyid' AND businessid = '$businessid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $businessname=Treat_DB_ProxyOld::mysql_result($result,0,"businessname");
    $businessid=Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
    $street=Treat_DB_ProxyOld::mysql_result($result,0,"street");
    $city=Treat_DB_ProxyOld::mysql_result($result,0,"city");
    $state=Treat_DB_ProxyOld::mysql_result($result,0,"state");
    $zip=Treat_DB_ProxyOld::mysql_result($result,0,"zip");
    $phone=Treat_DB_ProxyOld::mysql_result($result,0,"phone");
    $bus_unit=Treat_DB_ProxyOld::mysql_result($result,0,"unit");

    //echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";
    echo "<tr bgcolor=#CCCCFF><td width=1%><img src=logo/$com_logo></td><td><font size=4><b>$businessname #$bus_unit</b></font><br>$street<br>$city, $state $zip<br>$phone</td>";
    echo "<td align=right valign=top>";
    echo "<br><FORM ACTION=editbus.php method=post>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<input type=hidden name=businessid value=$businessid>";
    echo "<input type=hidden name=companyid value=$companyid>";
    echo "<INPUT TYPE=submit VALUE=' Store Setup '></FORM></td></tr>";
    //echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";
    
    echo "<tr><td colspan=3><font color=#999999>";
    if ($sec_bus_sales>0){echo "<a href=busdetail.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Sales</font></a>";}
    if ($sec_bus_invoice>0){echo " | <a href=businvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Invoicing</font></a>";}
    if ($sec_bus_order>0){echo " | <a href=buscatertrack.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Orders</font></a>";}
    if ($sec_bus_payable>0){echo " | <a href=busapinvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Payables</font></a>";}
    if ($sec_bus_inventor1>0){echo " | <a href=businventor.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Inventory</font></a>";}
    if ($sec_bus_control>0){echo " | <a href=buscontrol.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Control Sheet</font></a>";}
    if ($sec_bus_menu>0){echo " | <a href=buscatermenu.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Menus</font></a>";}
    if ($sec_bus_order_setup>0){echo " | <a href=buscaterroom.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Order Setup</font></a>";}
    if ($sec_bus_request>0){echo " | <a href=busrequest.php?bid=$businessid&cid=$companyid><font color=red onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='red' style='text-decoration:none'>Requests</font></a>";}
    if ($sec_route_collection>0||$sec_route_labor>0||$sec_route_detail>0){echo " | <a href=busroute_collect.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Route Collections</font></a>";}
    if ($sec_bus_labor>0){echo " | <a href=buslabor.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Labor</font></a>";}
    if ($sec_bus_budget>0||($businessid == $bid && $pr1 == 1)||($businessid == $bid2 && $pr2 == 1)||($businessid == $bid3 && $pr3 == 1)||($businessid == $bid4 && $pr4 == 1)||($businessid == $bid5 && $pr5 == 1)||($businessid == $bid6 && $pr6 == 1)||($businessid == $bid7 && $pr7 == 1)||($businessid == $bid8 && $pr8 == 1)||($businessid == $bid9 && $pr9 == 1)||($businessid == $bid10 && $pr10 == 1)){echo " | <a href=busbudget.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Budget</font></a>";}
	echo "</td></tr>";

    echo "</table></center><p>";

if ($security_level>0){
    echo "<p><center><table width=90%><tr><td width=50%><form action=busrequest.php method=post>";
    echo "<select name=gobus onChange='changePage(this.form.gobus)'>";

    $districtquery="";
    if ($security_level>2&&$security_level<7){
       ////mysql_connect($dbhost,$username,$password);
       ////@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM login_district WHERE loginid = '$loginid'";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       ////mysql_close();

       $num--;
       while($num>=0){
          $districtid=Treat_DB_ProxyOld::mysql_result($result,$num,"districtid");
          if($districtquery==""){$districtquery="districtid = '$districtid'";}
          else{$districtquery="$districtquery OR districtid = '$districtid'";}
          $num--;
       }
    }

    ////mysql_connect($dbhost,$username,$password);
    ////@mysql_select_db($database) or die( "Unable to select database");
    if($security_level>6){$query = "SELECT * FROM business WHERE companyid = '$companyid' ORDER BY businessname DESC";}
    elseif($security_level==2||$security_level==1){$query = "SELECT * FROM business WHERE companyid = '$companyid' AND (businessid = '$bid' OR businessid = '$bid2' OR businessid = '$bid3' OR businessid = '$bid4' OR businessid = '$bid5' OR businessid = '$bid6' OR businessid = '$bid7' OR businessid = '$bid8' OR businessid = '$bid9' OR businessid = '$bid10') ORDER BY businessname DESC";}
    elseif($security_level>2&&$security_level<7){$query = "SELECT * FROM business WHERE companyid = '$companyid' AND ($districtquery) ORDER BY businessname DESC";}
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    ////mysql_close();

    $num--;
    while($num>=0){
       $businessname=Treat_DB_ProxyOld::mysql_result($result,$num,"businessname");
       $busid=Treat_DB_ProxyOld::mysql_result($result,$num,"businessid");

       if ($busid==$businessid){$show="SELECTED";}
       else{$show="";}

       echo "<option value=busrequest.php?bid=$busid&cid=$companyid $show>$businessname</option>";
       $num--;
    }
 
    echo "</select></td><td></form></td><td width=50% align=right></td></tr></table></center><p>";
}


    echo "<center><table width=90% bgcolor=#E8E7E7 cellspacing=0 cellpadding=0>";

    if($success==1){$showsuccess="<font color=red> - Account added successfully</font>";}
    elseif($success==2){$showsuccess="<font color=red> - Account updated successfully</font>";}
    else{$showsuccess="";}

    echo "<tr bgcolor=#E8E7E7><td colspan=2><a name='accounts'></a><b>CUSTOMER REQUESTS $showsuccess</b><br>";
    echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    if($showall==1){$query = "SELECT * FROM accounts WHERE companyid = '$companyid' AND businessid = '$businessid' AND is_deleted = '0' ORDER BY name DESC";}
    else{$query = "SELECT * FROM accounts WHERE companyid = '$companyid' AND businessid = '$businessid' AND accountnum != '' AND is_deleted = '0' ORDER BY name DESC";}
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    if($showall==1){$showall=0;$showme="Hide";}
    else{$showall=1;$showme="Show All";}

    echo "<tr><td colspan=2><p><br><FORM ACTION=edit_accountpend.php method=post>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<input type=hidden name=businessid value=$businessid>";
    echo "<input type=hidden name=companyid value=$companyid><ul><li><select name=accountid>";
    $num--;
    while ($num>=0){
       $accountname=Treat_DB_ProxyOld::mysql_result($result,$num,"name");
       $accountnum=Treat_DB_ProxyOld::mysql_result($result,$num,"accountnum");
       $accountid=Treat_DB_ProxyOld::mysql_result($result,$num,"accountid");
       $parent=Treat_DB_ProxyOld::mysql_result($result,$num,"parent");
       if ($parent==-1){$show="*Master Account*";}
       else {$show="";}
       echo "<option value=$accountid>$accountname ($accountnum) $show</option> ";
       $num--;
    }
    echo "</select> <INPUT TYPE=submit VALUE='Request Account Change'> <a href=busrequest.php?bid=$businessid&cid=$companyid&showall=$showall style=$style><font size=2 color=blue>$showme</font></a></FORM></ul></td></tr>";

    echo "<FORM ACTION=addaccountpend.php method=post>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<input type=hidden name=businessid value=$businessid>";
    echo "<input type=hidden name=companyid value=$companyid>";
    echo "<tr valign=top bgcolor=#E8E7E7><td colspan=2><table><tr><td align=right><font color=red>Account Name:</font></td><td> <INPUT TYPE=text NAME=accountname SIZE=30></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td align=right><font color=red>Email:</font></td><td> <INPUT TYPE=text NAME=accountemail SIZE=30></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td align=right><font color=red>Address:</font></td><td> <INPUT TYPE=text NAME=accountattn SIZE=30></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td align=right><font color=red>Address:</font></td><td> <INPUT TYPE=text NAME=accountstreet SIZE=30 value='$street'></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td align=right><font color=red>City:</font></td><td> <INPUT TYPE=text NAME=accountcity SIZE=20 value='$city'></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td align=right><font color=red>State:</font></td><td><INPUT TYPE=text NAME=accountstate SIZE=5 value='$state'></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td align=right><font color=red>Zip Code:</font></td><td><INPUT TYPE=text NAME=accountzip SIZE=8 value='$zip'></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td align=right><font color=red>Phone #:</font></td><td><INPUT TYPE=text NAME=accountphone SIZE=12></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td align=right><font color=red>Fax #:</font></td><td><INPUT TYPE=text NAME=accountfax SIZE=12></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td align=right><font color=red>Tax Exempt #:</font></td><td><INPUT TYPE=text NAME=taxid SIZE=12></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td align=right><font color=red>Cost Center:</font></td><td><INPUT TYPE=text NAME=costcenter SIZE=20></td></tr>";

    echo "<tr bgcolor=#E8E7E7><td colspan=2><center><INPUT TYPE=submit onclick='return delconfirm()' VALUE='Request New Account'></FORM></center></td></tr></table></td></tr>";

    echo "<tr bgcolor=white><td colspan=2 height=20></td></tr>";
/////////////Vendor Requests/////////////////////////////////////////////////////
    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM vendorpend WHERE companyid = '$companyid' AND businessid = '$businessid'";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    if ($num!=0){$reqcolor="blue";}
    else {$reqcolor="black";}
    echo "<tr bgcolor=#E8E7E7><td colspan=2><a name='request'></a><b>VENDOR REQUESTS - <font color=$reqcolor>$num Requests Pending</font></b><br>";
    echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";

    echo "<FORM ACTION=addvendorpend.php method=post>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<input type=hidden name=businessid value=$businessid>";
    echo "<input type=hidden name=companyid value=$companyid>";
    echo "<tr valign=top bgcolor=#E8E7E7><td colspan=2><table><tr><td align=right><font color=red>Vendor Name:</font></td><td> <INPUT TYPE=text NAME=vendorname SIZE=30></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td align=right><font color=red>Address:</font></td><td> <INPUT TYPE=text NAME=vendorattn SIZE=30></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td align=right><font color=red>Address:</font></td><td> <INPUT TYPE=text NAME=vendorstreet SIZE=30></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td align=right><font color=red>City:</font></td><td> <INPUT TYPE=text NAME=vendorcity SIZE=20></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td align=right><font color=red>State:</font></td><td><INPUT TYPE=text NAME=vendorstate SIZE=5></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td align=right><font color=red>Zip Code:</font></td><td><INPUT TYPE=text NAME=vendorzip SIZE=8></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td align=right><font color=red>Phone #:</font></td><td><INPUT TYPE=text NAME=vendorphone SIZE=12></td></tr>";

    echo "<tr bgcolor=#E8E7E7><td colspan=2><center><INPUT TYPE=submit VALUE='Request New Vendor'></FORM></center></td></tr></table></td></tr>";

    //echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";

    echo "</table></center>";

    echo "<br><FORM ACTION=businesstrack.php method=post>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<center><INPUT TYPE=submit VALUE=' Return to Main Page'></FORM></center></body>";
	
	google_page_track();
}
//mysql_close();
?>