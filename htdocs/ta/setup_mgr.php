<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<?php
$current = "";
if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
require_once( dirname(__FILE__) . '/../../application/bootstrap.php' );

require_once('lib/class.bus_setup.php');

$style = "text-decoration:none;";

if ( require_once( realpath( DOC_ROOT . '/../lib/inc/cookie-login.php' ) ) ) {

	////users
	$loginid = $GLOBALS['loginid'];
	$security_level = $GLOBALS['security_level'];

	///company
	$showcid = \EE\Controller\Base::getGetVariable('cid','');
	$showdate = \EE\Controller\Base::getGetVariable('showdate','');

	if($showcid == "") {
		$showcid = isset($_COOKIE["showcid"]) ? $_COOKIE["showcid"] : '';
	}
	if($showdate == "") {
		$showdate = isset($_COOKIE["showdate"]) ? $_COOKIE["showdate"] : '';
	}
	if($showdate == "") {
		$showdate = 11;
	}

	setcookie("showcid",$showcid);
	setcookie("showdate", $showdate);
?>
<html>
	<head>
		<script type="text/javascript" src="javascripts/jquery-1.4.2.min.js"></script>

		<script type="text/javascript">
			function show_setup(bid, setup_status){
				$('#setup_list').show();
				document.getElementById('setup_frame').src='setup.php?bid=' + bid + '&goto=1&setup_status=' + setup_status;
			}
			function transfer_kiosk(bid) {
				$('#setup_list').show();
				document.getElementById('setup_frame').src='setup.php?bid=0&goto=1&setup_status=1&warehoused=' + bid;
			}

			function changePage(newLoc)
			{
				nextPage = newLoc.options[newLoc.selectedIndex].value

				if (nextPage != "")
				{
					document.location.href = nextPage
				}
			}

			function disableForm(theform) {
				if (document.all || document.getElementById) {
					for (i = 0; i < theform.length; i++) {
						var tempobj = theform.elements[i];
						if (tempobj.type.toLowerCase() == "submit" || tempobj.type.toLowerCase() == "reset")
						tempobj.disabled = true;
					}
				}
				else {
					alert("The form has been submitted.  But, since you're not using IE 4+ or NS 6, the submit button was not disabled on form submission.");
					return false;
				}
			}

			$(function() {
				$('#convert-to-kiosk').submit(function(event) {
					var confirmed = confirm("Are you sure? This cannot be undone.");
					if (!confirmed) {
						event.preventDefault();
						return false;
					}
				});
<?php if (isset($_GET['popup_bid']) && $_GET['popup_bid'] > 0) { ?>

				show_setup(<?php echo intval($_GET['popup_bid']); ?>, 1);
<?php } ?>

			});
		</script>
	</head>
	<body>
		<center>
			<table width="90%">
				<tr>
					<td align="left">
						<a href="businesstrack.php">
							<img src="logo.jpg" height="43" width="205" border="0">
						</a>
					</td>
				</tr>
			</table>
		</center>
<?php ///setup popup ?>

		<div id="setup_list" style="display:none; position:fixed; top:3%; left:3%; right:3%; border:3px solid #999999; background:#FFFFFF; z-index:100;">
			<div style="width:100%; background:#999999; color:#FFFFFF;">
				<center><b>Setup Check List</b></center>
			</div>
			<iframe name="setup_frame" id="setup_frame" src="setup.php?bid=0&goto=1" width="100%" height="525"></iframe>
		</div>
<?php
	/////dates
	$today = \EE\Controller\Base::getGetVariable('today','');
	if($today=="") {
		$today = date("Y-m-d");
	}
	$today2 = date("Y-m-d");

	$date1 = $today;
	while(dayofweek($date1) != "Monday") {
		$date1 = prevday($date1);
	}
	$nextweek = date("Y-m-d",mktime(0, 0, 0, substr($date1,5,2) , substr($date1,8,2)+7, substr($date1,0,4)));
	$prevweek = date("Y-m-d",mktime(0, 0, 0, substr($date1,5,2) , substr($date1,8,2)-7, substr($date1,0,4)));

	if($today2 >= $today && $today2 < $nextweek) {
		$current = 1;
	}

	/////companies allowed
	$security_query = "";
	$add_query = "AND ";
	$filter_company_ids = array();

	$query = "SELECT * FROM login_company WHERE loginid = $loginid";
	$result = Treat_DB_ProxyOld::query($query);

	while($r = Treat_DB_ProxyOld::mysql_fetch_array($result)){
		$cid = $r["companyid"];
		$filter_company_ids[] = $cid;

		if($security_query == ""){
			$security_query = "( companyid = $cid ";
			$add_query .= "( business.companyid = $cid ";
		} else {
			$security_query .= "OR companyid = $cid ";
			$add_query .= "OR business.companyid = $cid ";
		}
	}
	$security_query .= ")";
	$add_query .= ")";

	if($showcid > 0) {
		$filter_company_ids = array();
		$filter_company_ids[] = $showcid;
		$add_query = " AND business.companyid = $showcid";
	}

	////company filter
?>

		<br>
		<center>
			<table width="90%" style="border: 2px solid black; background: #E8E7E7;">
				<tr>
					<td align="left" width="25%">
						<a href="setup_mgr.php?today=<?php echo $prevweek; ?>" style="<?php echo $style; ?>">
							<font color="blue" size="2">[PREV WEEK]</font>
						</a>
					</td>
					<td>
						<center>
							<form action="setup_mgr.php" method="POST" style="margin:0; padding:0; display:inline;">
								Filter: 
								<select name="show_comp" onchange="changePage(this.form.show_comp);">
									<option value="setup_mgr.php?cid=0">All Companies</option>
<?php
	$query = "SELECT * FROM company WHERE $security_query ORDER BY reference";
	$result = Treat_DB_ProxyOld::query($query);

	while($r = Treat_DB_ProxyOld::mysql_fetch_array($result)){
		$companyid = $r["companyid"];
		$reference = $r["reference"];

		$sel = "";
		if($showcid == $companyid) {
			$sel = "SELECTED";
		}
?>

									<option value="setup_mgr.php?cid=<?php echo $companyid; ?>" <?php echo $sel; ?>><?php echo $reference; ?></option>
<?php
	}
?>

								</select>
							</form>
<?php  ////date filter ?>

							<form action="setup_mgr.php" method="POST" style="margin:0;padding:0;display:inline;">
								<select name="show_date" onchange="changePage(this.form.show_date);">
<?php
	$query = "SELECT * FROM setup WHERE type = 3";
	$result = Treat_DB_ProxyOld::query($query);

	while($r = Treat_DB_ProxyOld::mysql_fetch_array($result)) {
		$id = $r["id"];
		$name = $r["name"];

		$sel = "";
		if($showdate == $id){
			$sel = "SELECTED";
		}
?>

									<option value="setup_mgr.php?showdate=<?php echo $id; ?>" <?php echo $sel; ?>><?php echo $name; ?></option>
<?php
	}
?>

								</select>
							</form>
						</center>
					</td>
					<td align="right" width="25%">
						<a href="setup_mgr.php?today=<?php echo $nextweek; ?>" style="<?php echo $style; ?>">
							<font color="blue" size="2">[NEXT WEEK]</font>
						</a>
					</td>
				</tr>
			</table>
		</center>
<?php
	///2nd stage completion
	if($security_level >= 7) {
		$query = "
			SELECT
				business.businessid,
				business.businessname,
				company.reference
			FROM business
			JOIN company ON company.companyid = business.companyid
			WHERE business.setup = 2 $add_query
		";
		$result = Treat_DB_ProxyOld::query($query);
		$num = Treat_DB_ProxyOld::mysql_fetch_array($result);

		if($num > 0) {
?>

		<br>
		<center>
			<table width="90%" cellspacing="0" cellpadding="1" style="border: 1px solid black;">
				<tr bgcolor="black">
					<td colspan="3">
						<center>
							<b><font color="yellow">Completed Kiosks</font></b>
						</center>
					</td>
				</tr>
				<tr valign="top">
					<td width="30%" style="border: 1px solid black; background: #FF8040;">
						<b>FINAL STAGE</b>
					</td>
					<td style="border: 1px solid black; background: #E8E7E7;">
						<table width="100%">
<?php
			if($num == 0) {
?>

							<tr>
								<td><i>Nothing to Display</i></td>
							</tr>
<?php
			} else {
				while($r = Treat_DB_ProxyOld::mysql_fetch_array($result)) {
					$businessid = $r["businessid"];
					$businessname = $r["businessname"];
					$companyname = $r["reference"];

					$show_setup = bus_setup::setup($businessid,1);
					$show_setup = "<span onclick=\"show_setup($businessid, 2);\" onmouseover=\"style.cursor = 'pointer';\">$show_setup</span>";
?>

							<tr>
								<td width="33%" align="left"><?php echo $show_setup; ?></td>
								<td width="33%" align="left"><?php echo $businessname; ?></td>
								<td width="33%" align="left"><?php echo $companyname; ?></td>
							</tr>
<?php
				}
			}
?>

						</table>
					</td>
				</tr>
			</table>
			<br>
<?php
		}
	}

	///past due!!!
	$query = "
		SELECT
			business.businessid,
			business.businessname,
			company.reference,
			setup_detail.value
		FROM business
		LEFT JOIN setup_detail ON setup_detail.businessid = business.businessid
		JOIN company ON company.companyid = business.companyid
		WHERE
			business.setup = 1
			$add_query
			AND setup_detail.setupid = $showdate
			AND setup_detail.value > '2000-01-01'
			AND setup_detail.value < '$today2'
	";
	$result = Treat_DB_ProxyOld::query($query);
	$num = Treat_DB_ProxyOld::mysql_fetch_array($result);
	if($num > 0) {
?>

			<br>
			<center>
				<table width="90%" cellspacing="0" cellpadding="1" style="border: 1px solid black;">
					<tr bgcolor="black">
						<td colspan="3">
							<center><b><font color="yellow">ERRORS</font></b></center>
						</td>
					</tr>
					<tr valign="top">
						<td width="30%" style="border: 1px solid black; background: #F20056;">
							<img src="error.gif">
							<b>PAST DUE</b>
						</td>
						<td style="border: 1px solid black; background: #E8E7E7;">
							<table width="100%">
<?php
		if($num == 0) {
?>

								<tr>
									<td><i>Nothing to Display</i></td>
								</tr>
<?php
		} else {
			while($r = Treat_DB_ProxyOld::mysql_fetch_array($result)) {
				$businessid = $r["businessid"];
				$businessname = $r["businessname"];
				$install_date = $r["value"];
				$companyname = $r["reference"];

				$query2 = "SELECT value FROM setup_detail WHERE businessid = $businessid AND setupid = 3";
				$result2 = Treat_DB_ProxyOld::query($query2);

				$businessname = mysql_result($result2, 0, "value");

				$show_setup = bus_setup::setup($businessid,1);
				$show_setup = "<span onclick=\"show_setup($businessid);\" onmouseover=\"style.cursor = 'pointer';\">$show_setup</span>";
?>

								<tr>
									<td width="33%" align="left"><?php echo $show_setup; ?></td>
									<td width="33%" align="left"><?php echo $businessname; ?></td>
									<td width="33%" align="left"><?php echo $companyname; ?></td>
								</tr>
<?php
			}
		}
?>

							</table>
						</td>
					</tr>
				</table>
				<br>
<?php
	}

	////current week
?>

				<br>
				<center>
					<table width="90%" cellspacing="0" cellpadding="1" style="border: 1px solid black;">
<?php
	///current week
	if($current == 1) {
?>

						<tr>
							<td colspan="3" bgcolor="black">
								<center><b><font color="white">Current Week</font></b></center>
							</td>
						</tr>
<?php
	} else {
?>

						<tr>
							<td colspan="3" bgcolor="#999999">
								<center><b><font color="white">Week of <?php echo $today; ?></font></b></center>
							</td>
						</tr>
<?php
	}

	///list units
	////days of the week
	for($counter=1;$counter<=7;$counter++) {
		$showdayname = dayofweek($date1);
		$showday = substr($date1,8,2);
		$showmonth = substr($date1,5,2);
		$showyear = substr($date1,2,2);
		$show_date = "$showmonth/$showday/$showyear";
?>

						<tr valign="top">
							<td width="15%" style="border: 1px solid black; background: #FFFF99;"><?php echo $showdayname; ?></td>
							<td width="15%" style="border: 1px solid black; background:#FFFF99;"><?php echo $show_date; ?></td>
<?php ////details ?>

							<td width="70%" style="border: 1px solid black; background:#E8E7E7;">
								<table width="100%" border="0">
<?php
		$query = "
			SELECT
				business.businessid,
				business.businessname,
				setup_detail.value,
				company.reference
			FROM business
			JOIN setup_detail ON setup_detail.businessid = business.businessid
			JOIN company ON company.companyid = business.companyid
			WHERE
				business.setup = 1
				$add_query
				AND setup_detail.setupid = $showdate
				AND setup_detail.value = '$date1'
		";
		$result = Treat_DB_ProxyOld::query($query);
		$num = Treat_DB_ProxyOld::mysql_num_rows($result);

		if($num == 0) {
?>

									<tr>
										<td align="left"><i>Nothing to Display</i></td>
									</tr>
<?php
		} else {
			while($r = Treat_DB_ProxyOld::mysql_fetch_array($result)) {
				$businessid = $r["businessid"];
				$businessname = $r["businessname"];
				$install_date = $r["value"];
				$companyname = $r["reference"];

				$query2 = "SELECT value FROM setup_detail WHERE businessid = $businessid AND setupid = 3";
				$result2 = Treat_DB_ProxyOld::query($query2);

				$businessname = mysql_result($result2, 0, "value");

				$show_setup = bus_setup::setup($businessid,1);
?>

									<tr>
										<td width="33%" align="left">
											<div onclick="show_setup(<?php echo $businessid; ?>);" onmouseover="style.cursor = 'pointer';">
												<?php echo $show_setup; ?>
											</div>
										</td>
										<td width="33%" align="left"><?php echo $businessname; ?></td>
										<td width="33%" align="left"><?php echo $companyname; ?></td>
									</tr>
<?php
			}
		}
?>

								</table>
							</td>
						</tr>
<?php
		$date1 = nextday($date1);
	}
?>

					</table>
				</center>
<?php
	////future weeks
	$nextdate = $nextweek;
	for($counter = 1; $counter <= 3 ;$counter++) {
		$showday = substr($nextdate,8,2);
		$showmonth = substr($nextdate,5,2);
		$showyear = substr($nextdate,2,2);
		$show_date = "$showmonth/$showday/$showyear";
		$date2 = date("Y-m-d",mktime(0, 0, 0, substr($nextdate,5,2) , substr($nextdate,8,2)+6, substr($nextdate,0,4)));
?>


				<br>
				<center>
					<table width="90%" cellspacing="0" cellpadding="1" style="border:1px solid black;">
						<tr valign="top">
							<td width="30%" style="border: 1px solid black; background: #CCCC99;">Week of <?php echo $show_date; ?></td>
							<td style="border: 1px solid black; background: #E8E7E7;">
								<table width="100%">
<?php
		$query = "
			SELECT
				business.businessid,
				business.businessname,
				setup_detail.value,
				company.reference
			FROM business
			JOIN setup_detail ON setup_detail.businessid = business.businessid
			JOIN company ON company.companyid = business.companyid
			WHERE
				business.setup = 1
				$add_query
				AND setup_detail.setupid = $showdate
				AND setup_detail.value >= '$nextdate'
				AND setup_detail.value <= '$date2'
			ORDER BY setup_detail.value
		";
		$result = Treat_DB_ProxyOld::query($query);
		$num = Treat_DB_ProxyOld::mysql_num_rows($result);

		if($num == 0) {
?>

									<tr>
										<td><i>Nothing to Display</i></td>
									</tr>
<?php
		} else {
			while($r = Treat_DB_ProxyOld::mysql_fetch_array($result)) {
				$businessid = $r["businessid"];
				$businessname = $r["businessname"];
				$install_date = $r["value"];
				$companyname = $r["reference"];

				$query2 = "SELECT value FROM setup_detail WHERE businessid = $businessid AND setupid = 3";
				$result2 = Treat_DB_ProxyOld::query($query2);

				$businessname = mysql_result($result2,0,"value");

				$show_setup = bus_setup::setup($businessid,1);
				$show_setup = "<span onclick=\"show_setup($businessid);\" onmouseover=\"style.cursor = 'pointer';\">$show_setup</span>";
				$install_date = dayofweek($install_date);
?>

									<tr>
										<td width="33%" align="left"><?php echo $show_setup; ?></td>
										<td width="33%" align="left"><?php echo $businessname; ?> (<?php echo $install_date; ?>)</td>
										<td width="33%" align="left"><?php echo $companyname; ?></td>
									</tr>
<?php
			}
		}
?>

								</table>
							</td>
						</tr>
					</table>
				</center>
<?php
		$nextdate = date("Y-m-d",mktime(0, 0, 0, substr($nextdate,5,2) , substr($nextdate,8,2)+7, substr($nextdate,0,4)));
	}

	////future installs
?>

			<br>

			<center>
				<table width="90%" cellspacing="0" cellpadding="1" style="border: 1px solid black;">
					<tr>
						<td colspan="2" bgcolor="black">
							<center>
								<span onclick="$('#future').show();" onmouseover="this.style.cursor='pointer';">
									<b><font color=white>Future Installs</font></b>
								</span>
							</center>
						</td>
					</tr>
					<tr valign="top" style="display:none;" id="future">
						<td width="30%" style="border: 1px solid black; background: #CCCC99;">Future Installs</td>
						<td style="border: 1px solid black; background: #E8E7E7;">
							<table width="100%">
<?php
	$showday = substr($nextdate,8,2);
	$showmonth = substr($nextdate,5,2);
	$showyear = substr($nextdate,2,2);
	$show_date = "$showmonth/$showday/$showyear";
	$date2 = date("Y-m-d",mktime(0, 0, 0, substr($nextdate,5,2) , substr($nextdate,8,2)+6, substr($nextdate,0,4)));
	$query = "
		SELECT
			business.businessid,
			business.businessname,
			setup_detail.value,
			company.reference
		FROM business
		JOIN setup_detail ON setup_detail.businessid = business.businessid
		JOIN company ON company.companyid = business.companyid
		WHERE
			business.setup = 1
			$add_query
			AND setup_detail.setupid = $showdate
			AND setup_detail.value >= '$nextdate'
		ORDER BY setup_detail.value
	";
	$result = Treat_DB_ProxyOld::query($query);
	$num = Treat_DB_ProxyOld::mysql_num_rows($result);

	if($num == 0) {
		echo "<tr><td><i>Nothing to Display</td></tr>";
	} else {
		while($r = Treat_DB_ProxyOld::mysql_fetch_array($result)) {
			$businessid = $r["businessid"];
			$businessname = $r["businessname"];
			$install_date = $r["value"];
			$companyname = $r["reference"];

			$query2 = "SELECT value FROM setup_detail WHERE businessid = $businessid AND setupid = 3";
			$result2 = Treat_DB_ProxyOld::query($query2);

			$businessname = mysql_result($result2,0,"value");

			$show_setup = bus_setup::setup($businessid,1);
			$show_setup = "<span onclick=\"show_setup($businessid);\" onmouseover=\"style.cursor = 'pointer';\">$show_setup</span>";
			//$install_date = dayofweek($install_date);
?>

								<tr>
									<td width="33%" align="left"><?php echo $show_setup; ?></td>
									<td width="33%" align="left"><?php echo $businessname; ?> (<?php echo $install_date; ?>)</td>
									<td width="33%" align="left"><?php echo $companyname; ?></td>
								</tr>
<?php
		}
	}
?>

							</table>
						</td>
					</tr>
				</table>
			</form>
		</center>
		<br>

<?php
			////transferred
			$kiosks = \EE\Model\Kiosk\Deactivation::getTransferredList($filter_company_ids);
			if (is_array($kiosks) && count($kiosks) > 0) :
?>

			<center>
				<table width="90%" cellspacing="0" cellpadding="1" style="border: 1px solid black;">
					<tr>
						<td colspan="2" bgcolor="black">
							<div style="text-align: center; color: white; font-weight: bold;">
								Transferred Kiosks
							</div>
						</td>
					</tr>
					<tr valign="top" id="transferred">
						<td width="30%" style="border: 1px solid black; background: #CCCC99;">Transferred Kiosks</td>
						<td style="border: 1px solid black; background: #E8E7E7;">
						<table width="100%">
<?php
				foreach($kiosks as $kiosk) {
?>

							<tr>
								<td width="33%" align="left">
									<span onclick="show_setup(<?php echo $kiosk->transferred_business_id; ?>, 1);" onmouseover="style.cursor = 'pointer';">
									<?php echo bus_setup::setup($kiosk->transferred_business_id, true); ?>
									</span>
								</td>
								<td width="23%" align="left"><?php echo $kiosk->transferred_business_name; ?></td>
								<td width="10%" align="left"><?php echo $kiosk->transferred_company_reference; ?></td>
								<td width="33%" align="left"><?php echo $kiosk->closed_business_name; ?></td>
							</tr>
<?php
				}
?>

						</table>
						</td>
					</tr>
				</table>
			</form>
		</center>
		<br>
<?php
			endif;


			////warehoused
			$kiosks = \EE\Model\Kiosk\Deactivation::getWarehousedList($filter_company_ids);
			if (is_array($kiosks) && count($kiosks) > 0) :
?>

			<center>
				<table width="90%" cellspacing="0" cellpadding="1" style="border: 1px solid black;">
					<tr>
						<td colspan="2" bgcolor="black">
							<div style="text-align: center; color: white; font-weight: bold;">
								Warehoused Kiosks
							</div>
						</td>
					</tr>
					<tr valign="top" id="warehoused">
						<td width="30%" style="border: 1px solid black; background: #CCCC99;">Warehoused Kiosks</td>
						<td style="border: 1px solid black; background: #E8E7E7;">
						<table width="100%">
<?php
				foreach($kiosks as $kiosk) {
?>

							<tr>
								<td width="33%" align="center">
									<a style="color: blue; font-weight: bold;" onclick="if(confirm('Are you sure you want to transfer kiosk? There is no undo.')) { transfer_kiosk(<?php echo $kiosk->business_id; ?>);}" onmouseover="style.cursor = 'pointer';">
										Transfer Kiosk
									</a>
								</td>
								<td width="33%" align="left"><?php echo $kiosk->business_name; ?></td>
								<td width="33%" align="left"><?php echo $kiosk->company_reference; ?></td>
							</tr>
<?php
				}
?>

						</table>
						</td>
					</tr>
				</table>
			</form>
		</center>
		<br>
<?php
			endif;
?>


<?php /////blank units ?>

		<center>
			<table width="90%" cellspacing="0" cellpadding="1" style="border: 1px solid black;">
				<tr bgcolor="black">
					<td colspan="3">
						<center>
							<b><font color=white>Blank Units</font></b>
						</center>
					</td>
				</tr>
				<tr valign="top" id="blanks">
					<td width="30%" style="border: 1px solid black; background: #00CCFF;">
						<b>Blank Units</b>
					</td>
					<td style="border: 1px solid black; background: #999999;">
						<table width="100%">
<?php
	$query = "
		SELECT
			business.businessid,
			business.businessname,
			company.reference
		FROM business
		LEFT JOIN setup_detail ON setup_detail.businessid = business.businessid
		JOIN company ON company.companyid = business.companyid
		WHERE
			business.setup = 1
			$add_query
			AND (
				(
					setup_detail.setupid = $showdate
					AND setup_detail.value = ''
				)
				OR setup_detail.businessid IS NULL
			)
		UNION
		SELECT
			business.businessid,
			business.businessname,
			company.reference
		FROM business
		JOIN company ON company.companyid = business.companyid
		WHERE NOT EXISTS (
			SELECT * FROM setup_detail
			WHERE
				setup_detail.businessid = business.businessid
				AND setup_detail.setupid = $showdate
			)
			AND business.setup = 1
			$add_query
	";
	$result = Treat_DB_ProxyOld::query($query);
	$num = Treat_DB_ProxyOld::mysql_num_rows($result);

	if($num == 0) {
?>

							<tr>
								<td>
									<i>Nothing to Display</i>
								</td>
							</tr>
<?php
	} else {
		while($r = Treat_DB_ProxyOld::mysql_fetch_array($result)) {
			$businessid = $r["businessid"];
			$businessname = $r["businessname"];
			$install_date = @$r["value"];
			$companyname = $r["reference"];

			$show_setup = bus_setup::setup($businessid,1);
			$show_setup = "<span onclick=\"show_setup($businessid);\" onmouseover=\"style.cursor = 'pointer';\">$show_setup</span>";
?>

							<tr>
								<td width="33%" align="left"><?php echo $show_setup; ?></td>
								<td width="33%" align="left"><?php echo $businessname; ?></td>
								<td width="33%" align="left"><?php echo $companyname; ?></td>
							</tr>
<?php
		}
	}
?>

						</table>
					</td>
				</tr>
				<tr bgcolor="#999999">
					<td style="border: 1px solid black;" align="left">
						<form action="setup_add.php" method="POST" style="margin:0; padding:0; display:inline;">
							<select name="companyid">
<?php
	if($showcid > 0) {
		$query = "SELECT * FROM company WHERE companyid = $showcid ORDER BY reference";
	} else {
		$query = "SELECT * FROM company WHERE $security_query ORDER BY reference";
	}
	$result = Treat_DB_ProxyOld::query($query);

	while($r = Treat_DB_ProxyOld::mysql_fetch_array($result)) {
		$companyid = $r["companyid"];
		$reference = $r["reference"];
?>

								<option value="<?php echo $companyid; ?>">
									<?php echo $reference; ?>
								</option>
<?php
	}

?>

							</select>
							<input type="submit" value="Add Unit">
						</form>
					</td>
					<td style="border: 1px solid black;" align="right">
						<form action="setup_add.php" id="convert-to-kiosk" method="POST" style="margin:0; padding:0; display:inline;">
							<input type="hidden" name="convert" value="1">
<?php
	$business_conversion_group_company = false;
	$business_conversion_list = array();
	if($showcid > 0) {
		$query = "
			SELECT
				business.businessid,
				business.businessname
			FROM business
			JOIN company ON business.companyid = company.companyid
			WHERE
				company.companyid = $showcid
				AND business.categoryid != 4
			ORDER BY business.businessname
		";
		$result = Treat_DB_ProxyOld::query($query);
		while($r = Treat_DB_ProxyOld::mysql_fetch_array($result)) {
			$business_conversion_list[$r['businessid']] = $r['businessname'];
		}
	} else {
		$business_company_filter = '';
		if (count($filter_company_ids) > 0) {
			$business_company_filter = sprintf('AND company.companyid IN (%s)', implode(',', $filter_company_ids));
		}
		$query = "
			SELECT
				business.businessid,
				business.businessname,
				company.reference
			FROM business
			JOIN company ON business.companyid = company.companyid
			WHERE
				business.categoryid != 4
				$business_company_filter
			ORDER BY company.reference, business.businessname
		";
		$business_conversion_group_company = true;
		$result = Treat_DB_ProxyOld::query($query);
		while($r = Treat_DB_ProxyOld::mysql_fetch_array($result)) {
			$business_conversion_list[$r['reference']][$r['businessid']] = $r['businessname'];
		}
	}
?>

							<select name="businessid">
<?php
	if ($business_conversion_group_company) {
		foreach($business_conversion_list as $company => $businesses) {
?>

								<optgroup label="<?php echo $company; ?>">
<?php
			foreach($businesses as $id => $name) :
?>

									<option value="<?php echo $id; ?>">
										<?php echo $name; ?>
									</option>
<?php
			endforeach;
?>

								</optgroup>
<?php
		}
	} else {
		foreach($business_conversion_list as $id => $name) {
?>

								<option value="<?php echo $id; ?>">
									<?php echo $name; ?>
								</option>
<?php
		}
	}
?>

							</select>
							<input type="submit" value="Convert Unit">
						</form>
					</td>
				</tr>
			</table>
		</center>
<?php
	//////add operator
	if($security_level >= 8) :
?>

		<br>
		<center>
			<form action="setup_operator.php" method="POST" style="margin:0; padding:0; display:inline;">
				<table width="90%" cellspacing="0" cellpadding="1" style="border:1px solid black;">
					<tr bgcolor="black">
						<td colspan="3">
							<center><b><font color="white">New Operator</font></b></center>
						</td>
					</tr>
					<tr>
						<td style="border:1px solid black;" colspan="3">
							<table width="100%">
								<tr>
									<td align="right">Name:</td>
									<td align="left"><input type="text" size="25" name="companyname"></td>
								</tr>
								<tr>
									<td align="right">Reference (shows in dropdown):</td>
									<td align="left"><input type="text" size="25" name="reference"></td>
								</tr>
								<tr>
									<td align="right">Street:</td>
									<td align="left"><input type="text" size="25" name="street"></td>
								</tr>
								<tr>
									<td align="right">City:</td>
									<td align="left"><input type="text" size="25" name="city"></td>
								</tr>
								<tr>
									<td align="right">State:</td>
									<td align="left"><input type="text" size="25" name="state"></td>
								</tr>
								<tr>
									<td align="right">Zip:</td>
									<td align="left"><input type="text" size="25" name="zip"></td>
								</tr>
								<tr>
									<td align="right">Phone:</td>
									<td align="left"><input type="text" size="25" name="phone"></td>
								</tr>
								<tr>
									<td align="right">Operator ID (Company Code):</td>
									<td align="left"><input type="text" size="25" name="operatorid"></td>
								</tr>
								<tr>
									<td align="right">Vendor#:</td>
									<td align="left"><input type="text" size="25" name="vendornum"></td>
								</tr>
								<tr>
									<td align="right">Week Ends:</td>
									<td align="left">
										<select name="week_end">
											<option value="Monday">Monday</option>
											<option value="Tuesday">Tuesday</option>
											<option value="Wednesday">Wednesday</option>
											<option value="Thursday">Thursday</option>
											<option value="Friday">Friday</option>
											<option value="Saturday">Saturday</option>
											<option value="Sunday">Sunday</option>
										</select>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2" align="right">
							<input type="submit" value="Add">
						</td>
					</tr>
				</table>
			</form>
		</center>
<?php
	endif;
?>

		<br>
		<center>
			<form action="businesstrack.php" method="POST">
				<input type="submit" value=" Return to Main Page ">
			</form>
		</center>
<?php
	google_page_track();
}
?>

	</body>
</html>