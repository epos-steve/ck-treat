<?php

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+4);
        $diff=round($diff, 2);
        if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
        elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
        $diff = substr($diff, 0, $dot+3);
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        return $diff;
}

function nextday($nextd,$day_format=''){ //Function returns next day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monn = substr($nextd, 5, 2);
   $dayn = substr($nextd, 8, 2);
   $yearn = substr($nextd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monn, $dayn+1, $yearn));
   return $tempdate;
}

function prevday($prevd,$day_format=''){ //Function returns previous day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monp = substr($prevd, 5, 2);
   $dayp = substr($prevd, 8, 2);
   $yearp = substr($prevd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monp, $dayp-1, $yearp));
   return $tempdate;
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      $day=$day-31;  
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";} 
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}


if ( !defined('DOC_ROOT'))
{
	define('DOC_ROOT', dirname(dirname(__FILE__)));
}
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');
 
$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";

/*$user = isset($_COOKIE["usercook"])?$_COOKIE["usercook"]:'';
$pass = isset($_COOKIE["passcook"])?$_COOKIE["passcook"]:'';
$businessid = isset($_GET["bid"])?$_GET["bid"]:'';
$companyid = isset($_GET["cid"])?$_GET["cid"]:'';
$newdate = isset($_GET["newdate"])?$_GET["newdate"]:'';
$date1 = isset($_GET["date1"])?$_GET["date1"]:'';
$date2 = isset($_GET["date2"])?$_GET["date2"]:'';*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook','');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook','');
$businessid = \EE\Controller\Base::getGetVariable('bid','');
$companyid = \EE\Controller\Base::getGetVariable('cid','');
$newdate = \EE\Controller\Base::getGetVariable('newdate','');
$date1 = \EE\Controller\Base::getGetVariable('date1','');
$date2 = \EE\Controller\Base::getGetVariable('date2','');

if ($businessid==""&&$companyid==""){
   /*$businessid = isset($_POST["businessid"])?$_POST["businessid"]:'';
   $companyid = isset($_POST["companyid"])?$_POST["companyid"]:'';
   $date1 = isset($_POST["date1"])?$_POST["date1"]:'';
   $date2 = isset($_POST["date2"])?$_POST["date2"]:'';*/
	
	$businessid = \EE\Controller\Base::getPostVariable('businessid','');
	$companyid = \EE\Controller\Base::getPostVariable('companyid','');
	$date1 = \EE\Controller\Base::getPostVariable('date1','');
	$date2 = \EE\Controller\Base::getPostVariable('date2','');
}

if($newdate!=""){$today=$newdate;}
$todayname = dayofweek($today);

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=Treat_DB_ProxyOld::mysql_numrows($result);

$loginid=@Treat_DB_ProxyOld::mysql_result($result,0,"loginid");
$security_level=@Treat_DB_ProxyOld::mysql_result($result,0,"security_level");
$mysecurity=@Treat_DB_ProxyOld::mysql_result($result,0,"security");
$bid=@Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
$bid2=@Treat_DB_ProxyOld::mysql_result($result,0,"busid2");
$bid3=@Treat_DB_ProxyOld::mysql_result($result,0,"busid3");
$bid4=@Treat_DB_ProxyOld::mysql_result($result,0,"busid4");
$bid5=@Treat_DB_ProxyOld::mysql_result($result,0,"busid5");
$bid6=@Treat_DB_ProxyOld::mysql_result($result,0,"busid6");
$bid7=@Treat_DB_ProxyOld::mysql_result($result,0,"busid7");
$bid8=@Treat_DB_ProxyOld::mysql_result($result,0,"busid8");
$bid9=@Treat_DB_ProxyOld::mysql_result($result,0,"busid9");
$bid10=@Treat_DB_ProxyOld::mysql_result($result,0,"busid10");
$cid=@Treat_DB_ProxyOld::mysql_result($result,0,"companyid");

$pr1=@Treat_DB_ProxyOld::mysql_result($result,0,"payroll");
$pr2=@Treat_DB_ProxyOld::mysql_result($result,0,"pr2");
$pr3=@Treat_DB_ProxyOld::mysql_result($result,0,"pr3");
$pr4=@Treat_DB_ProxyOld::mysql_result($result,0,"pr4");
$pr5=@Treat_DB_ProxyOld::mysql_result($result,0,"pr5");
$pr6=@Treat_DB_ProxyOld::mysql_result($result,0,"pr6");
$pr7=@Treat_DB_ProxyOld::mysql_result($result,0,"pr7");
$pr8=@Treat_DB_ProxyOld::mysql_result($result,0,"pr8");
$pr9=@Treat_DB_ProxyOld::mysql_result($result,0,"pr9");
$pr10=@Treat_DB_ProxyOld::mysql_result($result,0,"pr10");

if ($num != 1 || ($security_level==1 && ($bid != $businessid || $cid != $companyid)) || $user == "" || $pass == "")
{
    echo "<center><h3>Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM company WHERE companyid = '$companyid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $companyname=@Treat_DB_ProxyOld::mysql_result($result,0,"companyname");
    $week_start=@Treat_DB_ProxyOld::mysql_result($result,0,"week_start");
    $week_end=@Treat_DB_ProxyOld::mysql_result($result,0,"week_end");
	$com_logo=@Treat_DB_ProxyOld::mysql_result($result,0,"logo");

	if($com_logo == ""){$com_logo = "talogo.gif";}

    if ($date1==""){$date1=$today;}
    if ($date2==""){$date2=$today;}

	////time range security 11/12/10
	$query = "SELECT startdate FROM security_labor WHERE loginid = $loginid AND businessid = $businessid";
    $result = Treat_DB_ProxyOld::query($query);
	$num = Treat_DB_ProxyOld::mysql_numrows($result);

	if($num == 1){
		$sec_date = Treat_DB_ProxyOld::mysql_result($result,0,"startdate");
		if($date1 < $sec_date){$date1 = $sec_date;}
		if($date2 < $sec_date){$date2 = $sec_date;}
	}
	/////end security

    $day = date("d");
    $year = date("Y");
    $month = date("m");
    $today2="$year-$month-$day";

    $temp_date=$today2;
    while(dayofweek($temp_date)!=$week_end){$temp_date=prevday($temp_date);}
    $week_end_date=$temp_date;
    $is_start=0;
    while($is_start!=2){
       $temp_date=prevday($temp_date);
       if (dayofweek($temp_date)==$week_start){$is_start++;}
    }
    $week_start_date=$temp_date;

    $showlastperiod="<font size=2>[<a href=buslabor3.php?bid=$businessid&cid=$companyid&date1=$week_start_date&date2=$week_end_date><font color=blue>Last 2 Weeks</font></a>]</font>";

    $query = "SELECT * FROM security WHERE securityid = '$mysecurity'";
    $result = Treat_DB_ProxyOld::query($query);

    $sec_emp_labor=@Treat_DB_ProxyOld::mysql_result($result,0,"emp_labor");
    $sec_bus_sales=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_sales");
    $sec_bus_invoice=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_invoice");
    $sec_bus_order=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_order");
    $sec_bus_payable=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_payable");
    $sec_bus_inventor1=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor1");
    $sec_bus_inventor2=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor2");
    $sec_bus_inventor3=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor3");
    $sec_bus_inventor4=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor4");
    $sec_bus_inventor5=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor5");
    $sec_bus_control=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_control");
    $sec_bus_menu=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_menu");
    $sec_bus_order_setup=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_order_setup");
    $sec_bus_request=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_request");
    $sec_route_collection=@Treat_DB_ProxyOld::mysql_result($result,0,"route_collection");
    $sec_route_labor=@Treat_DB_ProxyOld::mysql_result($result,0,"route_labor");
    $sec_route_detail=@Treat_DB_ProxyOld::mysql_result($result,0,"route_detail");
    $sec_bus_labor=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_labor");
	$sec_bus_budget=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_budget");

    ////SECURITY LEVELS
    if($sec_bus_labor<.6){
       $location="businesstrack.php";
       header('Location: ./' . $location);
    }
    elseif($sec_bus_labor<=2&&$bid!=$businessid){
       $location="buslabor.php?bid=$businessid&cid=$companyid";
       header('Location: ./' . $location);
    }
    else{}

    if ($sec_emp_labor>0){$showallunits="<font size=2>[<a href=emp_labor.php?bid=$businessid&cid=$companyid><font color=blue>Employee Labor</font></a>]";}

?>

<html>
<head>
<script language="JavaScript" 
   type="text/JavaScript">
function changePage(newLoc)
 {
   nextPage = newLoc.options[newLoc.selectedIndex].value
		
   if (nextPage != "")
   {
      document.location.href = nextPage
   }
 }
</script>

<SCRIPT LANGUAGE="JavaScript" SRC="CalendarPopup.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">document.write(getCalendarStyles());</SCRIPT>

<STYLE>
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation
			{
			background-color:#6677DD;
			text-align:center;
			vertical-align:center;
			text-decoration:none;
			color:#FFFFFF;
			font-weight:bold;
			}
	.TESTcpDayColumnHeader,
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation,
	.TESTcpCurrentMonthDate,
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDate,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDate,
	.TESTcpCurrentDateDisabled,
	.TESTcpTodayText,
	.TESTcpTodayTextDisabled,
	.TESTcpText
			{
			font-family:arial;
			font-size:8pt;
			}
	TD.TESTcpDayColumnHeader
			{
			text-align:right;
			border:solid thin #6677DD;
			border-width:0 0 1 0;
			}
	.TESTcpCurrentMonthDate,
	.TESTcpOtherMonthDate,
	.TESTcpCurrentDate
			{
			text-align:right;
			text-decoration:none;
			}
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDateDisabled
			{
			color:#D0D0D0;
			text-align:right;
			text-decoration:line-through;
			}
	.TESTcpCurrentMonthDate
			{
			color:#6677DD;
			font-weight:bold;
			}
	.TESTcpCurrentDate
			{
			color: #FFFFFF;
			font-weight:bold;
			}
	.TESTcpOtherMonthDate
			{
			color:#808080;
			}
	TD.TESTcpCurrentDate
			{
			color:#FFFFFF;
			background-color: #6677DD;
			border-width:1;
			border:solid thin #000000;
			}
	TD.TESTcpCurrentDateDisabled
			{
			border-width:1;
			border:solid thin #FFAAAA;
			}
	TD.TESTcpTodayText,
	TD.TESTcpTodayTextDisabled
			{
			border:solid thin #6677DD;
			border-width:1 0 0 0;
			}
	A.TESTcpTodayText,
	SPAN.TESTcpTodayTextDisabled
			{
			height:20px;
			}
	A.TESTcpTodayText
			{
			color:#6677DD;
			font-weight:bold;
			}
	SPAN.TESTcpTodayTextDisabled
			{
			color:#D0D0D0;
			}
	.TESTcpBorder
			{
			border:solid thin #6677DD;
			}
</STYLE>

<SCRIPT LANGUAGE=javascript><!--
function caution(){return confirm('THERE ARE UNSUBMITTED DAYS WITHIN THIS DATE RANGE!');}
// --></SCRIPT>

</head>

<?
    //if ($date2==$date1){for($counter=1;$counter<7;$counter++){$date2=nextday($date2);}}

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM business WHERE companyid = '$companyid' AND businessid = '$businessid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $businessname=@Treat_DB_ProxyOld::mysql_result($result,0,"businessname");
    $businessid=@Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
    $street=@Treat_DB_ProxyOld::mysql_result($result,0,"street");
    $city=@Treat_DB_ProxyOld::mysql_result($result,0,"city");
    $state=@Treat_DB_ProxyOld::mysql_result($result,0,"state");
    $zip=@Treat_DB_ProxyOld::mysql_result($result,0,"zip");
    $phone=@Treat_DB_ProxyOld::mysql_result($result,0,"phone");
    $districtid=@Treat_DB_ProxyOld::mysql_result($result,0,"districtid");
    $operate=@Treat_DB_ProxyOld::mysql_result($result,0,"operate");
    $bus_unit=@Treat_DB_ProxyOld::mysql_result($result,0,"unit");
	$over_time_type=@Treat_DB_ProxyOld::mysql_result($result,0,"over_time_type");
	
    echo "<body>";  
    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%><tr><td colspan=2>";
    echo "<a href=businesstrack.php><img src=logo.jpg border=0 height=43 width=205></a><p></td></tr>";

    //echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";
    echo "<tr bgcolor=#CCCCFF><td width=1%><img src=logo/$com_logo></td><td><font size=4><b>$businessname #$bus_unit</b></font><br>$street<br>$city, $state $zip<br>$phone</td>";
    echo "<td align=right valign=top><br>";
    echo "<FORM ACTION='editbus.php' method=post name='store'>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<input type=hidden name=businessid value=$businessid>";
    echo "<input type=hidden name=companyid value=$companyid>";
    echo "<INPUT TYPE=submit VALUE=' Store Setup '> ";
    echo "</form></td></tr>";
    //echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";
    
    echo "<tr><td colspan=3><font color=#999999>";
    if ($sec_bus_sales>0){echo "<a href=busdetail.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Sales</font></a>";}
    if ($sec_bus_invoice>0){echo " | <a href=businvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Invoicing</font></a>";}
    if ($sec_bus_order>0){echo " | <a href=buscatertrack.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Orders</font></a>";}
    if ($sec_bus_payable>0){echo " | <a href=busapinvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Payables</font></a>";}
    if ($sec_bus_inventor1>0){echo " | <a href=businventor.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Inventory</font></a>";}
    if ($sec_bus_control>0){echo " | <a href=buscontrol.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Control Sheet</font></a>";}
    if ($sec_bus_menu>0){echo " | <a href=buscatermenu.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Menus</font></a>";}
    if ($sec_bus_order_setup>0){echo " | <a href=buscaterroom.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Order Setup</font></a>";}
    if ($sec_bus_request>0){echo " | <a href=busrequest.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Requests</font></a>";}
    if ($sec_route_collection>0||$sec_route_labor>0||$sec_route_detail>0){echo " | <a href=busroute_collect.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Route Collections</font></a>";}
    if ($sec_bus_labor>0){echo " | <a href=buslabor.php?bid=$businessid&cid=$companyid><font color=red onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='red' style='text-decoration:none'>Labor</font></a>";}
    if ($sec_bus_budget>0||($businessid == $bid && $pr1 == 1)||($businessid == $bid2 && $pr2 == 1)||($businessid == $bid3 && $pr3 == 1)||($businessid == $bid4 && $pr4 == 1)||($businessid == $bid5 && $pr5 == 1)||($businessid == $bid6 && $pr6 == 1)||($businessid == $bid7 && $pr7 == 1)||($businessid == $bid8 && $pr8 == 1)||($businessid == $bid9 && $pr9 == 1)||($businessid == $bid10 && $pr10 == 1)){echo " | <a href=busbudget.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Budget</font></a>";}
	echo "</td></tr>";

    echo "</table></center><p>";

if ($security_level>0){
    echo "<center><table width=90%><tr><td><form action=buslabor3.php method=post>";
    echo "<select name=gobus onChange='changePage(this.form.gobus)'>";

    $districtquery="";
    if ($security_level>2&&$security_level<7){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM login_district WHERE loginid = '$loginid'";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();

       $num--;
       while($num>=0){
          $districtid=@Treat_DB_ProxyOld::mysql_result($result,$num,"districtid");
          if($districtquery==""){$districtquery="districtid = '$districtid'";}
          else{$districtquery="$districtquery OR districtid = '$districtid'";}
          $num--;
       }
    }

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    if($security_level>6){$query = "SELECT * FROM business WHERE (companyid = '$companyid' AND exp_payroll = '0') OR (businessid = '$businessid') ORDER BY businessname DESC";}
    elseif($security_level==2||$security_level==1){$query = "SELECT * FROM business WHERE (companyid = '$companyid' AND (businessid = '$bid' OR businessid = '$bid2' OR businessid = '$bid3' OR businessid = '$bid4' OR businessid = '$bid5' OR businessid = '$bid6' OR businessid = '$bid7' OR businessid = '$bid8' OR businessid = '$bid9' OR businessid = '$bid10') AND exp_payroll = '0') OR (businessid = '$businessid') ORDER BY businessname DESC";}
    elseif($security_level>2&&$security_level<7){$query = "SELECT * FROM business WHERE (companyid = '$companyid' AND ($districtquery) AND exp_payroll = '0') OR (businessid = '$businessid') ORDER BY businessname DESC";}
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    $num--;
    while($num>=0){
       $businessname=@Treat_DB_ProxyOld::mysql_result($result,$num,"businessname");
       $busid=@Treat_DB_ProxyOld::mysql_result($result,$num,"businessid");

       if ($busid==$businessid){$show="SELECTED";}
       else{$show="";}

       echo "<option value=buslabor3.php?bid=$busid&cid=$companyid&date1=$date1&date2=$date2 $show>$businessname</option>";
       $num--;
    }
 
    echo "</select></td><td></form></td><td width=50% align=right><form action=buslabor3.php method=post><input type=hidden name=businessid value=$businessid><input type=hidden name=companyid value=$companyid>$showallunits $showlastperiod <SCRIPT LANGUAGE='JavaScript' ID='js18'> var cal18 = new CalendarPopup('testdiv1'); cal18.setCssPrefix('TEST');</SCRIPT> <A HREF=# onClick=cal18.select(document.forms[2].date1,'anchor18','yyyy-MM-dd'); return false; TITLE=cal18.select(document.forms[2].date1,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor18' ID='anchor18'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A> <input type=text name=date1 value='$date1' size=8> <b><i>to</i></b> <SCRIPT LANGUAGE='JavaScript' ID='js19'> var cal19 = new CalendarPopup('testdiv1'); cal19.setCssPrefix('TEST');</SCRIPT> <A HREF=# onClick=cal19.select(document.forms[2].date2,'anchor19','yyyy-MM-dd'); return false; TITLE=cal19.select(document.forms[2].date2,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor19' ID='anchor19'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A> <input type=text name=date2 value='$date2' size=8> <input type=submit value='GO'></td><td></form></td></tr></table></center><p>";
}
    $security_level=$sec_bus_labor;

    echo "<center><table width=90% cellspacing=0 cellpadding=0>";

    echo "<tr bgcolor=#CCCCCC valign=top><td width=1%><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=15%><center><a href=buslabor.php?cid=$companyid&bid=$businessid style=$style><font color=blue>Hours</a></center></td><td width=1% align=right><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1% ><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=15%><center><a href=buslabor4.php?cid=$companyid&bid=$businessid&newdate=$date1 style=$style style=$style><font color=blue>Tips</a></center></td><td width=1% align=right><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1%><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=15%><center><a href=buslabor2.php?cid=$companyid&bid=$businessid&newdate=$date1 style=$style style=$style><font color=blue>Commissions</a></center></td><td width=1% align=right><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1% bgcolor=#E8E7E7><img src=leftcrn.jpg height=6 width=6 align=top></td><td bgcolor=#E8E7E7 width=15%><center><a href=buslabor3.php?cid=$companyid&bid=$businessid&newdate=$date1 style=$style><font color=black>Summary</a></center></td><td bgcolor=#E8E7E7 width=1% align=right><img src=rightcrn.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1%><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=15%><center><a href=buslabor5.php?cid=$companyid&bid=$businessid&newdate=$date1 style=$style style=$style><font color=blue>Time Clock</a></center></td><td width=1% align=right><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td bgcolor=white width=25%></td></tr>";

    echo "<tr height=2><td colspan=4></td><td colspan=4></td><td colspan=4></td><td colspan=3 bgcolor=#E8E7E7></td><td></td><td colspan=4></td><td colspan=1></td></tr>";

    echo "</table></center><center><table width=90% cellspacing=0 cellpadding=0>";

    echo "<tr><td colspan=10>";
    echo "<center><table width=100% border=0 bgcolor=#E8E7E7>";
    echo "<tr bgcolor=#CCCC99><td width=4%><font size=2><b>Empl#</b></td><td width=18%><font size=2><b>Name</b></td><td align=right width=8%><font size=2><b>Rate</b></td>";

    $sh_totpay=0;
    $sh_tothour=0;
    $sh_codehour=array();
    $sh_totreg=0;
    $sh_totot=0;

    $sh_net_tips=0;
    $sh_pay_tips=0;
/////////////////LIST EMPLOYEES
    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query34 = "SELECT * FROM payroll_code WHERE companyid = '$companyid' ORDER BY orderid DESC";
    $result34 = Treat_DB_ProxyOld::query($query34);
    $num34=Treat_DB_ProxyOld::mysql_numrows($result34);
    //mysql_close();

    $temp_num=$num34-1;
    $myspan=38/($num34+1);
    while($temp_num>=0){
       $codeid=@Treat_DB_ProxyOld::mysql_result($result34,$temp_num,"codeid");
       $code=@Treat_DB_ProxyOld::mysql_result($result34,$temp_num,"code");
       $code_charge[$codeid]=@Treat_DB_ProxyOld::mysql_result($result34,$temp_num,"charge");

       echo "<td align=right width=$myspan%><font size=2><b>$code</b></td>";
       if ($code=="REG"){echo "<td align=right width=$myspan%><font size=2><b>OT</b></td>";}

       $temp_num--;
    }
    echo "<td align=right width=8%><font size=2><b>TTL HRS</td><td align=right width=8%><font size=2><b>NET TIPS</td><td align=right width=8%><font size=2><b>PR TIPS</td><td align=right width=8%><font size=2><b>TTL PAY</td></tr>";

/////////////REGULAR LABOR
    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM login,payroll_departmentdetail,security_departments WHERE login.oo2 = '$businessid' AND login.move_date <= '$date2' AND (login.is_deleted = '0' OR (login.is_deleted = '1' AND login.del_date >= '$date1')) AND login.empl_no != '' AND (login.loginid = payroll_departmentdetail.loginid AND payroll_departmentdetail.dept_id = security_departments.dept_id AND security_departments.securityid = '$mysecurity') ORDER BY payroll_departmentdetail.dept_id,.login.security_level,login.lastname DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    $num--;
    $mycounter=0;
    $coltotal=array();
    $coldollar=array();
    $sheettotal=0;
	$lastdept_id = 0;
    while($num>=0){
       $firstname=@Treat_DB_ProxyOld::mysql_result($result,$num,"login.firstname"); 
       $lastname=@Treat_DB_ProxyOld::mysql_result($result,$num,"login.lastname"); 
       $empl_no=@Treat_DB_ProxyOld::mysql_result($result,$num,"login.empl_no");
       $loginid=@Treat_DB_ProxyOld::mysql_result($result,$num,"login.loginid");
	   $dept_id=@Treat_DB_ProxyOld::mysql_result($result,$num,"payroll_departmentdetail.dept_id");

	   /////group by departments
	   if($lastdept_id != $dept_id){

		    ////department totals
			if($lastdept_id != 0){
				$dept_span2 = $dept_span - 2;
				$dept_total = number_format($dept_total,2);
				echo "<tr bgcolor=#CCCCCC><td colspan=2><font size=2>Department Total</font></td><td colspan=$dept_span2 align=right><font size=2>&nbsp;$$dept_total</font></td></tr>";
				$dept_total = 0;
			}

			$dept_span = $num34 + 8;
			$query14 = "SELECT dept_name FROM payroll_department WHERE dept_id = $dept_id";
			$result14 = Treat_DB_ProxyOld::query($query14);

			$dept_name = @Treat_DB_ProxyOld::mysql_result($result14,0,"dept_name");
			if($dept_name == ""){$dept_name = "<i>Unassigned</i>";}
			echo "<tr bgcolor=#FFFF99><td colspan=$dept_span><font size=2>&nbsp;<b>$dept_name</b></font></td></tr>";
	   }

       $hourtotal=0;
       $othourtotal=0;
       $payroll_code_hours=array();
       $total_hours=0;
       $rowtotal=0;

       $pay_array=array();
       $pay_counter=0;
       $total_days=0;

       $temp_date=$date1;
       while($temp_date<=$date2){
       $charge_hours=0;
       $non_charge_hours=0;
	   $totalnewhours=0;
       for($counter=1;$counter<=7;$counter++){

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query2 = "SELECT * FROM labor WHERE loginid = '$loginid' AND businessid = '$businessid' AND date = '$temp_date' AND tempid = '0'";
             $result2 = Treat_DB_ProxyOld::query($query2);
             $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
             //mysql_close();

             $total_days=$total_days+$num2;

             $num2--;
             while($num2>=0){
                $coded=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"coded");
                $hours=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"hours");
                $rate=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"rate");
                $jobtype=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"jobtypeid");

                ///////////CHECK FOR MUTLIPLE RATES
                $pay_check=0;
                for($pay_counter2=0;$pay_counter2<=$pay_counter;$pay_counter2++){
                   if($pay_array[$pay_counter2]==$rate){$pay_check=1;}
                }
                if($pay_check==0){$pay_array[$pay_counter]=$rate;$pay_counter++;}

                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query3 = "SELECT hourly,pto_override FROM jobtype WHERE jobtypeid = '$jobtype'";
                $result3 = Treat_DB_ProxyOld::query($query3);
                //mysql_close();

                $hourly=@Treat_DB_ProxyOld::mysql_result($result3,0,"hourly");
                $pto_override=@Treat_DB_ProxyOld::mysql_result($result3,0,"pto_override");

                if($pto_override>0){$rate=$pto_override;}

                if($hourly==1||$security_level==9){$showrate=money($rate);}
                else{$showrate="xxx";}
                
                $payroll_code_hours[$coded]=$payroll_code_hours[$coded]+$hours;
                $total_hours=$total_hours+$hours;

                if($code_charge[$coded]==1){
					
					if($hourly==1){
						//////////CALIFORNIA OVERTIME
						if($over_time_type==1){
							$newhours=0;
							if($hours>8){
								$newhours=$hours-8;
								$hours=8;
								
								$rowtotal+=money(($rate*1.5)*$newhours);
								//$charge_hours+=$newhours;
								$totalnewhours+=$newhours;
							}
						}
					
						if($charge_hours>40){$rowtotal=money($rowtotal+(($rate*1.5)*$hours));}
						elseif(($charge_hours+$hours)>40){
							$rowtotal=money($rowtotal+(($charge_hours+$hours-40)*($rate*1.5)));
							$rowtotal=money($rowtotal+(($hours-($charge_hours+$hours-40))*$rate));
						}
						else{$rowtotal=$rowtotal+($hours*$rate);}
					}
					////////////////////use to be ($rate/($operate*2)
					elseif($hourly==0&&$hours!=0){$rowtotal=$rowtotal+($rate/80*$hours);} 

					$charge_hours=$charge_hours+$hours;
				
                }
                else{$non_charge_hours=$non_charge_hours+$hours;}

                $num2--;
          }
          if ($temp_date==$date2){$counter=8;}
          $temp_date=nextday($temp_date);
       }
	    ///////////CALFORNIA OVERTIME
	    if($over_time_type==1){$othourtotal+=$totalnewhours;}
		
		if($charge_hours>40){$hourtotal=$hourtotal+40;$othourtotal=$othourtotal+($charge_hours-40);}
		else{$hourtotal=$hourtotal+$charge_hours;}
		
       }

       if($total_days==0){
          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query32 = "SELECT jobtypedetail.rate FROM jobtypedetail,jobtype WHERE jobtypedetail.loginid = '$loginid' AND jobtypedetail.jobtype = jobtype.jobtypeid AND jobtype.hourly = '1'";
          $result32 = Treat_DB_ProxyOld::query($query32);
          //mysql_close();

          $showrate=@Treat_DB_ProxyOld::mysql_result($result32,0,"rate");
          $showrate=money($showrate);
       }

       if ($pay_array[0]>0&&$pay_array[1]>0){$showrate="$showrate<font color=blue>*</font>";$showmultipay="<font color=blue size=2>*Multiple Rates of Pay</font>";}

       echo "<tr bgcolor=#E8E7E7 onMouseOver=this.bgColor='yellow' onMouseOut=this.bgColor=''><td><font size=2>$empl_no</td><td><font size=2>$lastname,$firstname</td><td align=right><font size=2>$$showrate</td>";

       $temp_num=$num34-1;
       while($temp_num>=0){
          $codeid=@Treat_DB_ProxyOld::mysql_result($result34,$temp_num,"codeid");
          $code=@Treat_DB_ProxyOld::mysql_result($result34,$temp_num,"code");
          if ($code=="REG"){$payroll_code_hours[$codeid]=$payroll_code_hours[$codeid]-$othourtotal; echo "<td align=right><font size=2>$payroll_code_hours[$codeid]</td>"; $othourtotal=round($othourtotal,2); echo "<td align=right><font size=2>$othourtotal</td>";$sh_totreg=$sh_totreg+$hourtotal;$sh_totot=$sh_totot+$othourtotal;}
          else{echo "<td align=right><font size=2>$payroll_code_hours[$codeid]</td>";$sh_codehour[$codeid]=$sh_codehour[$codeid]+$payroll_code_hours[$codeid];}
          $temp_num--;
       }
       $sh_totpay=$sh_totpay+$rowtotal;
	   $dept_total+=$rowtotal;
       $sh_tothour=$sh_tothour+$total_hours;

       if ($hourly==1||$security_level==9){$showrowtotal=money($rowtotal);}
       else{$showrowtotal="xxx";}
       $rowtotal=money($rowtotal);

       //////////////TIPS
             ////////////////REPORTED TIPS
             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query4 = "SELECT SUM(cash_tips+charge_tips+cash_out+charge_out) AS tip_amt FROM labor_tips WHERE loginid = '$loginid' AND businessid = '$businessid' AND date >= '$date1' AND date <= '$date2'";
             $result4 = Treat_DB_ProxyOld::query($query4);
             //mysql_close();

             $net_tip=@Treat_DB_ProxyOld::mysql_result($result4,0,"tip_amt");
             if($net_tip!=0){$net_tip=money($net_tip);$sh_net_tips=$sh_net_tips+$net_tip;}

             ////////////////REIMBURSED TIPS
             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query4 = "SELECT SUM(charge_tips+charge_out) AS tip_amt FROM labor_tips WHERE loginid = '$loginid' AND businessid = '$businessid' AND date >= '$date1' AND date <= '$date2'";
             $result4 = Treat_DB_ProxyOld::query($query4);
             //mysql_close();

             $pay_tip=@Treat_DB_ProxyOld::mysql_result($result4,0,"tip_amt");
             if($pay_tip!=0){$pay_tip=money($pay_tip);$sh_pay_tips=$sh_pay_tips+$pay_tip;}

       ///////////////END TIPS

       echo "<td align=right><font size=2>$total_hours</td><td align=right><font size=2>$$net_tip</td><td align=right><font size=2>$$pay_tip</td><td align=right><font size=2>$$showrowtotal</td></tr>";
       $num--;
	   $lastdept_id = $dept_id;
    }
/////////////PAST LABOR
    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT distinct labor.loginid FROM login,labor WHERE labor.businessid = '$businessid' AND labor.date >= '$date1' AND labor.date <= '$date2' AND login.oo2 != '$businessid' AND labor.loginid = login.loginid AND labor.tempid = '0'";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    $num--;

    while($num>=0){

       $loginid=@Treat_DB_ProxyOld::mysql_result($result,$num,"loginid");

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query73 = "SELECT * FROM login WHERE loginid = '$loginid'";
       $result73 = Treat_DB_ProxyOld::query($query73);
       //mysql_close();

       $firstname=@Treat_DB_ProxyOld::mysql_result($result73,0,"firstname"); 
       $lastname=@Treat_DB_ProxyOld::mysql_result($result73,0,"lastname"); 
       $empl_no=@Treat_DB_ProxyOld::mysql_result($result73,0,"empl_no");

       $hourtotal=0;
       $othourtotal=0;
       $payroll_code_hours=array();
       $total_hours=0;
       $rowtotal=0;

       $temp_date=$date1;
       while($temp_date<=$date2){
       $charge_hours=0;
       $non_charge_hours=0;
	   $totalnewhours=0;
       for($counter=1;$counter<=7;$counter++){

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query2 = "SELECT * FROM labor WHERE loginid = '$loginid' AND businessid = '$businessid' AND date = '$temp_date' AND tempid = '0'";
             $result2 = Treat_DB_ProxyOld::query($query2);
             $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
             //mysql_close();

             $num2--;
             while($num2>=0){
                $coded=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"coded");
                $hours=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"hours");
                $rate=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"rate");
                $jobtype=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"jobtypeid");

                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query3 = "SELECT hourly,pto_override FROM jobtype WHERE jobtypeid = '$jobtype'";
                $result3 = Treat_DB_ProxyOld::query($query3);
                //mysql_close();

                $hourly=@Treat_DB_ProxyOld::mysql_result($result3,0,"hourly");
                $pto_override=@Treat_DB_ProxyOld::mysql_result($result3,0,"pto_override");

                if($pto_override>0){$rate=$pto_override;}

                if($hourly==1||$security_level==9){$showrate=money($rate);}
                else{$showrate="xxx";}
                
                $payroll_code_hours[$coded]=$payroll_code_hours[$coded]+$hours;
                $total_hours=$total_hours+$hours;

                if($code_charge[$coded]==1){
				
                   if($hourly==1){
					  //////////CALIFORNIA OVERTIME
						if($over_time_type==1){
							$newhours=0;
							if($hours>8){
								$newhours=$hours-8;
								$hours=8;
								
								$rowtotal+=money(($rate*1.5)*$newhours);
								//$charge_hours+=$newhours;
								$totalnewhours+=$newhours;
							}
						}
				   
                      if($charge_hours>40){$rowtotal=money($rowtotal+(($rate*1.5)*$hours));}
                      elseif(($charge_hours+$hours)>40){
                         $rowtotal=money($rowtotal+(($charge_hours+$hours-40)*($rate*1.5)));
                         $rowtotal=money($rowtotal+(($hours-($charge_hours+$hours-40))*$rate));
                      }
                      else{$rowtotal=$rowtotal+($hours*$rate);}
                   }
                   elseif($hourly==0&&$hours!=0){$rowtotal=$rowtotal+($rate/80*$hours);} 

                   $charge_hours=$charge_hours+$hours;
                }
                else{$non_charge_hours=$non_charge_hours+$hours;}

                $num2--;
          }
          if ($temp_date==$date2){$counter=8;}
          $temp_date=nextday($temp_date);
       }
	   ///////////CALFORNIA OVERTIME
	   if($over_time_type==1){$othourtotal+=$totalnewhours;}
	   
       if($charge_hours>40){$hourtotal=$hourtotal+40;$othourtotal=$othourtotal+($charge_hours-40);}
       else{$hourtotal=$hourtotal+$charge_hours;}
       }
       echo "<tr bgcolor=#FF6666 onMouseOver=this.bgColor='yellow' onMouseOut=this.bgColor='#FF6666'><td><font size=2>$empl_no</td><td><font size=2>$lastname,$firstname</td><td align=right><font size=2>$$showrate</td>";

       $temp_num=$num34-1;
       while($temp_num>=0){
          $codeid=@Treat_DB_ProxyOld::mysql_result($result34,$temp_num,"codeid");
          $code=@Treat_DB_ProxyOld::mysql_result($result34,$temp_num,"code");
          if ($code=="REG"){$payroll_code_hours[$codeid]=$payroll_code_hours[$codeid]-$othourtotal; echo "<td align=right><font size=2>$payroll_code_hours[$codeid]</td>"; echo "<td align=right><font size=2>$othourtotal</td>";$sh_totreg=$sh_totreg+$hourtotal;$sh_totot=$sh_totot+$othourtotal;}
          else{echo "<td align=right><font size=2>$payroll_code_hours[$codeid]</td>";$sh_codehour[$codeid]=$sh_codehour[$codeid]+$payroll_code_hours[$codeid];}
          $temp_num--;
       }
       $sh_totpay=$sh_totpay+$rowtotal;
	   $dept_total+=$rowtotal;
       $sh_tothour=$sh_tothour+$total_hours;

       if ($hourly==1||$security_level==9){$showrowtotal=money($rowtotal);}
       else{$showrowtotal="xxx";}
       $rowtotal=money($rowtotal);

       //////////////TIPS
             ////////////////REPORTED TIPS
             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query4 = "SELECT SUM(cash_tips+charge_tips+cash_out+charge_out) AS tip_amt FROM labor_tips WHERE loginid = '$loginid' AND businessid = '$businessid' AND date >= '$date1' AND date <= '$date2'";
             $result4 = Treat_DB_ProxyOld::query($query4);
             //mysql_close();

             $net_tip=@Treat_DB_ProxyOld::mysql_result($result4,0,"tip_amt");
             if($net_tip!=0){$net_tip=money($net_tip);$sh_net_tips=$sh_net_tips+$net_tip;}

             ////////////////REIMBURSED TIPS
             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query4 = "SELECT SUM(charge_tips+charge_out) AS tip_amt FROM labor_tips WHERE loginid = '$loginid' AND businessid = '$businessid' AND date >= '$date1' AND date <= '$date2'";
             $result4 = Treat_DB_ProxyOld::query($query4);
             //mysql_close();

             $pay_tip=@Treat_DB_ProxyOld::mysql_result($result4,0,"tip_amt");
             if($pay_tip!=0){$pay_tip=money($pay_tip);$sh_pay_tips=$sh_pay_tips+$pay_tip;}

       ///////////////END TIPS

       echo "<td align=right><font size=2>$total_hours</td><td align=right><font size=2>$$net_tip</td><td align=right><font size=2>$$pay_tip</td><td align=right><font size=2>$$showrowtotal</td></tr>";
       $num--;
    }

/////////////ADD-ON LABOR
    $lastloginid=0;

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM labor_temp WHERE businessid = '$businessid' AND date >= '$date1' AND date <= '$date2' ORDER BY loginid";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    $num--;
    while($num>=0){
       $loginid=@Treat_DB_ProxyOld::mysql_result($result,$num,"loginid");
       $tempid=@Treat_DB_ProxyOld::mysql_result($result,$num,"tempid");

       ///////CHECK FOR DOUBLES
       if ($lastloginid!=$loginid){

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query73 = "SELECT * FROM login WHERE loginid = '$loginid'";
       $result73 = Treat_DB_ProxyOld::query($query73);
       //mysql_close();

       $firstname=@Treat_DB_ProxyOld::mysql_result($result73,0,"firstname"); 
       $lastname=@Treat_DB_ProxyOld::mysql_result($result73,0,"lastname"); 
       $empl_no=@Treat_DB_ProxyOld::mysql_result($result73,0,"empl_no");
       $move_date=@Treat_DB_ProxyOld::mysql_result($result73,0,"move_date");

       $hourtotal=0;
       $othourtotal=0;
       $payroll_code_hours=array();
       $total_hours=0;
       $rowtotal=0;

//////FIND HOURS IN HOME UNIT
       $temp_date=$date1;
       $temp_hours=array();
       $weekcounter=1;
       while($temp_date<=$date2){
       for($counter=1;$counter<=7;$counter++){

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query2 = "SELECT oo2 FROM login WHERE loginid = '$loginid'";
             $result2 = Treat_DB_ProxyOld::query($query2);
             //mysql_close();

             $temp_bid=@Treat_DB_ProxyOld::mysql_result($result2,0,"oo2");

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query2 = "SELECT * FROM labor WHERE loginid = '$loginid' AND businessid = '$temp_bid' AND date = '$temp_date' AND tempid = '0' AND date >= '$move_date'";
             $result2 = Treat_DB_ProxyOld::query($query2);
             $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
             //mysql_close();

             $num2--;
             while($num2>=0){
                $coded=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"coded");
                $hours=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"hours");
                $rate=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"rate");
                $jobtype=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"jobtypeid");

                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query3 = "SELECT hourly FROM jobtype WHERE jobtypeid = '$jobtype'";
                $result3 = Treat_DB_ProxyOld::query($query3);
                //mysql_close();

                $hourly=@Treat_DB_ProxyOld::mysql_result($result3,0,"hourly");

                if($code_charge[$coded]==1&&$hourly==1){
                   $temp_hours[$weekcounter]=$temp_hours[$weekcounter]+$hours;
                }

                $num2--;
          }
          if ($temp_date==$date2){$counter=8;}
          $temp_date=nextday($temp_date);
       }
       $weekcounter++;
       }

//////FIND HOURS IN OTHER UNITS
//////WHERE tempid is less than this tempid

       $temp_date=$date1;
       $weekcounter=1;
       while($temp_date<=$date2){
       for($counter=1;$counter<=7;$counter++){

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query2 = "SELECT oo2 FROM login WHERE loginid = '$loginid'";
             $result2 = Treat_DB_ProxyOld::query($query2);
             //mysql_close();

             $temp_bid=@Treat_DB_ProxyOld::mysql_result($result2,0,"oo2");

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query2 = "SELECT * FROM labor WHERE labor.loginid = '$loginid' AND labor.businessid != '$temp_bid' AND labor.businessid != '$businessid' AND labor.date = '$temp_date' AND labor.tempid < '$tempid' AND labor.tempid != '$tempid' AND labor.tempid != '0'";
             $result2 = Treat_DB_ProxyOld::query($query2);
             $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
             //mysql_close();

             $num2--;
             while($num2>=0){
                $coded=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"coded");
                $hours=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"hours");
                $rate=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"rate");
                $jobtype=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"jobtypeid");

                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query3 = "SELECT hourly FROM jobtype WHERE jobtypeid = '$jobtype'";
                $result3 = Treat_DB_ProxyOld::query($query3);
                //mysql_close();

                $hourly=@Treat_DB_ProxyOld::mysql_result($result3,0,"hourly");

                if($code_charge[$coded]==1&&$hourly==1){
                   $temp_hours[$weekcounter]=$temp_hours[$weekcounter]+$hours;
                }

                $num2--;
          }
          if ($temp_date==$date2){$counter=8;}
          $temp_date=nextday($temp_date);
       }
       $weekcounter++;
       }

//////CALCULATE TRUE TIME
       $temp_date=$date1;
       $weekcounter=1;
       while($temp_date<=$date2){
       $charge_hours=0;
       $non_charge_hours=0;
	   $totalnewhours;
       for($counter=1;$counter<=7;$counter++){

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query2 = "SELECT * FROM labor WHERE loginid = '$loginid' AND businessid = '$businessid' AND date = '$temp_date' AND tempid > '0'";
             $result2 = Treat_DB_ProxyOld::query($query2);
             $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
             //mysql_close();

             $num2--;
             while($num2>=0){
                $coded=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"coded");
                $hours=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"hours");
                $rate=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"rate");
                $jobtype=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"jobtypeid");

                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query3 = "SELECT hourly,pto_override FROM jobtype WHERE jobtypeid = '$jobtype'";
                $result3 = Treat_DB_ProxyOld::query($query3);
                //mysql_close();

                $hourly=@Treat_DB_ProxyOld::mysql_result($result3,0,"hourly");
                $pto_override=@Treat_DB_ProxyOld::mysql_result($result3,0,"pto_override");

                if($pto_override>0){$rate=$pto_override;}

                if($hourly==1||$security_level==9){$showrate=money($rate);}
                else{$showrate="xxx";}
                
                $payroll_code_hours[$coded]=$payroll_code_hours[$coded]+$hours;
                $total_hours=$total_hours+$hours;

                if($code_charge[$coded]==1){
                   if($hourly==1){
					  //////////CALIFORNIA OVERTIME
						if($over_time_type==1){
							$newhours=0;
							if($hours>8){
								$newhours=$hours-8;
								$hours=8;
								
								$rowtotal+=money(($rate*1.5)*$newhours);
								//$charge_hours+=$newhours;
								$totalnewhours+=$newhours;
							}
						}
				   
                      if(($temp_hours[$weekcounter]+$charge_hours)>40){$rowtotal=money($rowtotal+(($rate*1.5)*$hours));}
                      elseif(($temp_hours[$weekcounter]+$charge_hours+$hours)>40){
                         $rowtotal=money($rowtotal+(($temp_hours[$weekcounter]+$charge_hours+$hours-40)*($rate*1.5)));
                         $rowtotal=money($rowtotal+(($hours-($temp_hours[$weekcounter]+$charge_hours+$hours-40))*$rate));
                      }
                      else{$rowtotal=$rowtotal+($hours*$rate);}
                   }
                   elseif($hourly==0&&$hours!=0){$rowtotal=$rowtotal+($rate/80*$hours);} 

                   $charge_hours=$charge_hours+$hours;
                }
                else{$non_charge_hours=$non_charge_hours+$hours;}

                $num2--;
          }
          if ($temp_date==$date2){$counter=8;}
          $temp_date=nextday($temp_date);
       }
       
       $reghoursleft=40-$temp_hours[$weekcounter];
	   
	   ///////////CALFORNIA OVERTIME
	   if($over_time_type==1){$othourtotal+=$totalnewhours;}

       if($temp_hours[$weekcounter]>0 && $reghoursleft<=0){$othourtotal=$othourtotal+$charge_hours;}
       elseif($temp_hours[$weekcounter]>0&&$charge_hours<=$reghoursleft){$hourtotal=$hourtotal+$charge_hours;}
       elseif($temp_hours[$weekcounter]>0&&$charge_hours>$reghoursleft){$hourtotal=$hourtotal+$reghoursleft;$othourtotal=$othourtotal+$charge_hours-$reghoursleft;}
       elseif($charge_hours>40){$hourtotal=$hourtotal+40;$othourtotal=$othourtotal+($charge_hours-40);}
       else{$hourtotal=$hourtotal+$charge_hours;}

       $weekcounter++;
       }
       echo "<tr bgcolor=#FFFF99 onMouseOver=this.bgColor='yellow' onMouseOut=this.bgColor='#FFFF99'><td><font size=2>$empl_no</td><td><font size=2>$lastname,$firstname</td><td align=right><font size=2>$$showrate</td>";

       $temp_num=$num34-1;
       while($temp_num>=0){
          $codeid=@Treat_DB_ProxyOld::mysql_result($result34,$temp_num,"codeid");
          $code=@Treat_DB_ProxyOld::mysql_result($result34,$temp_num,"code");
          if ($code=="REG"){$payroll_code_hours[$codeid]=$payroll_code_hours[$codeid]-$othourtotal; echo "<td align=right><font size=2>$hourtotal</td>"; $othourtotal=round($othourtotal,2); echo "<td align=right><font size=2>$othourtotal</td>";$sh_totreg=$sh_totreg+$hourtotal;$sh_totot=$sh_totot+$othourtotal;}
          else{echo "<td align=right><font size=2>$payroll_code_hours[$codeid]</td>";$sh_codehour[$codeid]=$sh_codehour[$codeid]+$payroll_code_hours[$codeid];}
          $temp_num--;
       }
       $sh_totpay=$sh_totpay+$rowtotal;
	   $dept_total+=$rowtotal;
       $sh_tothour=$sh_tothour+$total_hours;

       if ($hourly==1||$security_level==9){$showrowtotal=money($rowtotal);}
       else{$showrowtotal="xxx";}
       $rowtotal=money($rowtotal);

       //////////////TIPS
             ////////////////REPORTED TIPS
             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query4 = "SELECT SUM(cash_tips+charge_tips+cash_out+charge_out) AS tip_amt FROM labor_tips WHERE loginid = '$loginid' AND businessid = '$businessid' AND date >= '$date1' AND date <= '$date2'";
             $result4 = Treat_DB_ProxyOld::query($query4);
             //mysql_close();

             $net_tip=@Treat_DB_ProxyOld::mysql_result($result4,0,"tip_amt");
             if($net_tip!=0){$net_tip=money($net_tip);$sh_net_tips=$sh_net_tips+$net_tip;}

             ////////////////REIMBURSED TIPS
             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query4 = "SELECT SUM(charge_tips+charge_out) AS tip_amt FROM labor_tips WHERE loginid = '$loginid' AND businessid = '$businessid' AND date >= '$date1' AND date <= '$date2'";
             $result4 = Treat_DB_ProxyOld::query($query4);
             //mysql_close();

             $pay_tip=@Treat_DB_ProxyOld::mysql_result($result4,0,"tip_amt");
             if($pay_tip!=0){$pay_tip=money($pay_tip);$sh_pay_tips=$sh_pay_tips+$pay_tip;}

       ///////////////END TIPS

       echo "<td align=right><font size=2>$total_hours</td><td align=right><font size=2>$$net_tip</td><td align=right><font size=2>$$pay_tip</td><td align=right><font size=2>$$showrowtotal</td></tr>";
       }
       $lastloginid=$loginid;
       $num--;
    }

	///last deptartment total
	   $dept_total = number_format($dept_total,2);
	   echo "<tr bgcolor=#CCCCCC><td colspan=2><font size=2>Department Total</font></td><td colspan=$dept_span2 align=right><font size=2>&nbsp;$$dept_total</font></td></tr>";

////////////////////SHEET TOTALS
       echo "<tr bgcolor=#999999><td colspan=3>Sheet Total</td>";

       $temp_num=$num34-1;
       while($temp_num>=0){
          $codeid=@Treat_DB_ProxyOld::mysql_result($result34,$temp_num,"codeid");
          $code=@Treat_DB_ProxyOld::mysql_result($result34,$temp_num,"code");
          if ($code=="REG"){echo "<td align=right><font size=2>$sh_totreg</td>"; echo "<td align=right><font size=2>$sh_totot</td>";}
          else{echo "<td align=right><font size=2>$sh_codehour[$codeid]</td>";}
          $temp_num--;
       }

	   
	   ///sheet totals
       $sh_totpay=number_format($sh_totpay,2);
       $sh_net_tips=number_format($sh_net_tips,2);
       $sh_pay_tips=number_format($sh_pay_tips,2);
       echo "<td align=right><font size=2>$sh_tothour</td><td align=right><font size=2>$$sh_net_tips</td><td align=right><font size=2>$$sh_pay_tips</td><td align=right><font size=2>$$sh_totpay</td></tr>";

///////////////////END
    echo "</table></center></td></tr>";

///////////////////COMMISSIONS
    echo "<tr bgcolor=white height=16><td colspan=10></td></tr>";
    echo "<tr><td colspan=10>";

    echo "<center><table width=100% border=0 bgcolor=#E8E7E7>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query34 = "SELECT * FROM payroll_code_comm WHERE companyid = '$companyid' ORDER BY orderid DESC";
    $result34 = Treat_DB_ProxyOld::query($query34);
    $num34=Treat_DB_ProxyOld::mysql_numrows($result34);
    //mysql_close();

    ///////////////HEADER BEGIN DISPLAY
    $showspan=$num34+5;
    echo "<tr bgcolor=#CCCC99><td width=10%><b><font size=2>Empl#</td><td width=15%><b><font size=2>Employee</td><td width=5% align=right><b><font size=2>Rate</td>";

    $temp_num=$num34-1;
    $temp_width=60/($temp_num+1);

    while($temp_num>=0){
       $codeid=@Treat_DB_ProxyOld::mysql_result($result34,$temp_num,"codeid");
       $payroll_code=@Treat_DB_ProxyOld::mysql_result($result34,$temp_num,"code_name");

       echo "<td align=right width=$temp_width%><b><font size=2>$payroll_code</font></td>";
       if ($payroll_code=="REG"){echo "<td align=right width=$temp_width%><b><font size=2>OT</font></td>";}

       $temp_num--;
    }
    echo "<td width=10% align=right><font size=2><b>Total</td></tr>";
    /////////////END HEADER

    $com_total=0;

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM jobtype WHERE companyid = '$companyid' AND commission = '1'";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    $com_total=array();

    $num--;
    while($num>=0){
       $jobtypeid=@Treat_DB_ProxyOld::mysql_result($result,$num,"jobtypeid");
       $jobtypename=@Treat_DB_ProxyOld::mysql_result($result,$num,"name");
       $is_hourly=@Treat_DB_ProxyOld::mysql_result($result,$num,"hourly");

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM labor_temp_comm,payroll_departmentdetail,security_departments WHERE labor_temp_comm.jobtypeid = '$jobtypeid' AND labor_temp_comm.businessid = '$businessid' AND labor_temp_comm.date >= '$date1' AND labor_temp_comm.date <= '$date2' AND (labor_temp_comm.loginid = payroll_departmentdetail.loginid AND payroll_departmentdetail.dept_id = security_departments.dept_id AND security_departments.securityid = '$mysecurity') ORDER BY payroll_departmentdetail.dept_id,labor_temp_comm.loginid DESC";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
       //mysql_close();

       $codevalue=array();
       $ot=0;
       $totalpay=0;
       $lastloginid=0;
	   $lastdept_id = 0;
	   $dept_id = 0;
       $num2--;
       while($num2>=-1){
          $tempid=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"tempid");
          $loginid=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"loginid");

          if ($loginid!=$lastloginid&&$lastloginid!=0){

			  /////group by departments
			 if($lastdept_id != $dept_id){
				$query14 = "SELECT dept_name FROM payroll_department WHERE dept_id = $dept_id";
				$result14 = Treat_DB_ProxyOld::query($query14);

				$dept_name = @Treat_DB_ProxyOld::mysql_result($result14,0,"dept_name");
				if($dept_name == ""){$dept_name = "<i>Unassigned</i>";}
				echo "<tr bgcolor=#FFFF99><td colspan=$showspan><font size=2>&nbsp;<b>$dept_name</b></font></td></tr>";
			 }
			 $lastdept_id = $dept_id;

             if($is_hourly==1){$showrate="hr";}
             else{$showrate="Base";}
             echo "<tr bgcolor=#E8E7E7 onMouseOver=this.bgColor='yellow' onMouseOut=this.bgColor='#E8E7E7'><td><font size=2>$empl_no</td><td><font size=2>$lastname,$firstname</td><td align=right><font size=2>$$rate/$showrate</td>";

             $temp_num=$num34-1;
             while($temp_num>=0){
                $codeid=@Treat_DB_ProxyOld::mysql_result($result34,$temp_num,"codeid");
                $code_name=@Treat_DB_ProxyOld::mysql_result($result34,$temp_num,"code_name");
                $hours=@Treat_DB_ProxyOld::mysql_result($result34,$temp_num,"hours");

                echo "<td align=right><font size=2>$codevalue[$codeid]</td>";
                $com_total[$codeid]=$com_total[$codeid]+$codevalue[$codeid];
                if ($code_name=="REG"){echo "<td align=right><font size=2>$ot</td>";$com_total[ot]=$com_total[ot]+$ot;}

                $temp_num--;
             }
             $totalpay=money($totalpay);
             $com_total[total]=$com_total[total]+$totalpay;
             echo "<td align=right><font size=2>$totalpay</td></tr>";

             $com_total2+=$totalpay;

             $codevalue=array();
             $ot=0;
             $totalpay=0;
          }

          $rate=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"rate");

          $rate=money($rate);

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query3 = "SELECT * FROM login WHERE loginid = '$loginid'";
          $result3 = Treat_DB_ProxyOld::query($query3);
          //mysql_close();

          $firstname=@Treat_DB_ProxyOld::mysql_result($result3,0,"firstname");
          $lastname=@Treat_DB_ProxyOld::mysql_result($result3,0,"lastname");
          $empl_no=@Treat_DB_ProxyOld::mysql_result($result3,0,"empl_no");

		  /////departments
		  $query14 = "SELECT payroll_departmentdetail.dept_id FROM login,payroll_departmentdetail WHERE login.loginid = $loginid AND login.loginid = payroll_departmentdetail.loginid";
		  $result14 = Treat_DB_ProxyOld::query($query14);
		  $dept_id = Treat_DB_ProxyOld::mysql_result($result14,0,"dept_id");

          $temp_num=$num34-1;
          while($temp_num>=0){
             $codeid=@Treat_DB_ProxyOld::mysql_result($result34,$temp_num,"codeid");
             $code_name=@Treat_DB_ProxyOld::mysql_result($result34,$temp_num,"code_name");
             $hours=@Treat_DB_ProxyOld::mysql_result($result34,$temp_num,"hours");
             $nocharge=@Treat_DB_ProxyOld::mysql_result($result34,$temp_num,"nocharge");

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query3 = "SELECT * FROM labor_comm WHERE temp_commid = '$tempid' AND coded = '$codeid'";
             $result3 = Treat_DB_ProxyOld::query($query3);
             //mysql_close();

             $value=@Treat_DB_ProxyOld::mysql_result($result3,0,"amount");

             if($code_name=="REG"){
                if($value>40){
                   $totalpay=$totalpay+(40*$rate);
                   $totalpay=$totalpay+(($value-40)*($rate*1.5));

                   $codevalue[$codeid]=$codevalue[$codeid]+40;
                   $ot=$ot+($value-40);
                }
                else{
                   $totalpay=$totalpay+($value*$rate);

                   $codevalue[$codeid]=$codevalue[$codeid]+$value;
                }
             }
             elseif($hours==1){
                if($nocharge!=1){$totalpay=$totalpay+($value*$rate);}

                $codevalue[$codeid]=$codevalue[$codeid]+$value;
             }
             else{
                if($nocharge!=1){$totalpay=$totalpay+$value;}

                $codevalue[$codeid]=$codevalue[$codeid]+$value;
             }

             $temp_num--;
          }

          $lastloginid=$loginid;
		  
          $num2--;
       }

       $myspan=$showspan-1;
       $com_total2=number_format($com_total2,2);
       echo "<tr bgcolor=#CCCCCC><td colspan=$myspan><b><font size=2>$jobtypename</font></td><td align=right><b><font size=2>$$com_total2</tr>";
       $com_total2=0;
       $num--;
    }

    echo "<tr bgcolor=#999999><td colspan=3><a href=printlabor.php?bid=$businessid&cid=$companyid&date1=$date1&date2=$date2 style=$style target='_blank'><font color=blue size=2><b>Print</b></font></a></td>";
    $temp_num=$num34-1;
    while($temp_num>=0){
          $codeid=@Treat_DB_ProxyOld::mysql_result($result34,$temp_num,"codeid");
          $code_name=@Treat_DB_ProxyOld::mysql_result($result34,$temp_num,"code_name");
          $hours=@Treat_DB_ProxyOld::mysql_result($result34,$temp_num,"hours");

          if($hours!=1){$com_total[$codeid]=number_format($com_total[$codeid],2);}
          echo "<td align=right><font size=2><b>$com_total[$codeid]</td>";
          if ($code_name=="REG"){echo "<td align=right><font size=2><b>$com_total[ot]</td>";}

          $temp_num--;
    }
    $com_total[total]=number_format($com_total[total],2);
    echo "<td align=right><font size=2><b>$com_total[total]</td></tr>";
    echo "</table></td></tr>";
///////////////////END COMMISSIONS
    echo "<tr height=1 bgcolor=black><td colspan=10></td></tr>";
    echo "<tr><td colspan=10>$showmultipay</td></tr>";

    //////////////EXPORT
    if ($security_level>8){
       $temp_date=$date1;
       $counter=1;
       while($temp_date!=$date2){$temp_date=nextday($temp_date);$counter++;}

       $query3 = "SELECT * FROM submit_labor WHERE businessid = '$businessid' AND date >= '$date1' AND date <= '$date2'";
       $result3 = Treat_DB_ProxyOld::query($query3);
       $num3=Treat_DB_ProxyOld::mysql_numrows($result3);

       if($num3!=$counter){$showcaution="onclick='return caution()'";}

       echo "<tr><td colspan=10 align=right><a name=bottom><form action=labor_export.php method=post><input type=hidden name=businessid value=$businessid><input type=hidden name=companyid value=$companyid><SCRIPT LANGUAGE='JavaScript' ID='js20'> var cal20 = new CalendarPopup('testdiv1'); cal20.setCssPrefix('TEST');</SCRIPT> <A HREF=#bottom onClick=cal20.select(document.forms[3].date1,'anchor20','yyyy-MM-dd'); return false; TITLE=cal20.select(document.forms[3].date1,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor20' ID='anchor20'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A> <input type=text name=date1 value='$date1' size=8> <b><i>to</i></b> <SCRIPT LANGUAGE='JavaScript' ID='js21'> var cal21 = new CalendarPopup('testdiv1'); cal21.setCssPrefix('TEST');</SCRIPT> <A HREF=#bottom onClick=cal21.select(document.forms[3].date2,'anchor21','yyyy-MM-dd'); return false; TITLE=cal21.select(document.forms[3].date2,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor21' ID='anchor21'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A> <input type=text name=date2 value='$date2' size=8> <input type=submit value='Export' $showcaution></a></td></tr>";
       echo "<tr><td colspan=10></form></td></tr>";
    }
    echo "</table></center>"; 

    echo "<br><FORM ACTION=businesstrack.php method=post>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<center><INPUT TYPE=submit VALUE=' Return to Main Page'></FORM></center><DIV ID=testdiv1 STYLE=position:absolute;visibility:hidden;background-color:white;layer-background-color:white;></DIV></body>";
	
    google_page_track();
}
?>