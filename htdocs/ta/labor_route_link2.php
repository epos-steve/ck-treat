<?php

function dayofweek($date1)
{
	$day=substr($date1,8,2);
	$month=substr($date1,5,2);
	$year=substr($date1,0,4);
	$dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
	return $dayofweek;
}

function money($diff){
	$findme='.';
	$double='.00';
	$single='0';
	$double2='00';
	$pos1 = strpos($diff, $findme);
	$pos2 = strlen($diff);
	if ($pos1==""){$diff="$diff$double";}
	elseif ($pos2-$pos1==2){$diff="$diff$single";}
	elseif ($pos2-$pos1==1){$diff="$diff$double2";}
	else{}
	$dot = strpos($diff, $findme);
	$diff = substr($diff, 0, $dot+4);
	$diff=round($diff, 2);
	if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
	elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
	$diff = substr($diff, 0, $dot+3);
	$pos1 = strpos($diff, $findme);
	$pos2 = strlen($diff);
	if ($pos1==""){$diff="$diff$double";}
	elseif ($pos2-$pos1==2){$diff="$diff$single";}
	elseif ($pos2-$pos1==1){$diff="$diff$double2";}
	else{}
	return $diff;
}

function nextday($nextd,$day_format=''){ //Function returns next day of passed date and formats accordingly.
	if($day_format==""){$day_format="Y-m-d";}
	$monn = substr($nextd, 5, 2);
	$dayn = substr($nextd, 8, 2);
	$yearn = substr($nextd, 0,4);
	$tempdate = date($day_format , mktime(0,0,0, $monn, $dayn+1, $yearn));
	return $tempdate;
}

function prevday($prevd,$day_format=''){ //Function returns previous day of passed date and formats accordingly.
	if($day_format==""){$day_format="Y-m-d";}
	$monp = substr($prevd, 5, 2);
	$dayp = substr($prevd, 8, 2);
	$yearp = substr($prevd, 0,4);
	$tempdate = date($day_format , mktime(0,0,0, $monp, $dayp-1, $yearp));
	return $tempdate;
}

function futureday($num) {
	$day = date("d");
	$year = date("Y");
	$month = date("m");
	$leap = date("L");
	$day=$day+$num;

	if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
	{
		if ($month == "03"){$month="04";}
		if ($month == "05"){$month="06";}
		if ($month == "07"){$month="08";}
		if ($month == "08"){$month="09";}
		$day=$day-31;  
		if ($day<10){$day="0$day";} 
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($month=='02' && $leap == '1' && $day >= '29')
	{
		$month='03';
		$day=$day-29;
		if ($day<10){$day="0$day";} 
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($month=='02' && $leap == '0' && $day >= '28')
	{
		$month='03';
		$day=$day-28;
		if ($day<10){$day="0$day";} 
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
	{
		if ($month == "04"){$month="05";}
		if ($month == "06"){$month="07";}
		if ($month == "09"){$month="10";}
		if ($month == "11"){$month="12";}
		$day=$day-30;
		if ($day<10){$day="0$day";} 
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($month==12 && $day>=32)
	{
		$day=$day-31;
		if ($day<10){$day="0$day";} 
		$month='01';
		$year++;
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else
	{
		if ($day<10){$day="0$day";}
		$tomorrow="$year-$month-$day";
		return $tomorrow;
	}
}

function pastday($num) {
	$day = date("d");
	$day=$day-$num;
	if ($day <= 0)
	{
		$year = date("Y");
		$month = date("m");
		if ($month == 01)
		{
			$month = '12';
			$year--;
			$day=$day+31;
			$yesterday = "$year-$month-$day";
			return $yesterday;
		}
		else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
		{
			$month--;
			if ($month < 10)
			{
				$month="0$month";
			}
			$day=$day+31;
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
		else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
		{
			$month--;
			if ($month < 10)
			{
				$month="0$month";
			}
			$day=$day+30;
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
		else
		{
			$day=$day+28;
			$month='02';
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
	}
	else
	{
		if ($day < 10)
		{
			$day="0$day";
		}
		$year=date("Y");
		$month=date("m");
		$yesterday="$year-$month-$day";
		return $yesterday;
	}
}


if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
require_once( DOC_ROOT . '/bootstrap.php' );

$style = "text-decoration:none";
$user = isset($_COOKIE["usercook"])?$_COOKIE["usercook"]:'';
$pass = isset($_COOKIE["passcook"])?$_COOKIE["passcook"]:'';
$mei = 0;
if ( $mei == "" ) {
#	$mei = isset( $_SESSION['mei'] ) ? $_SESSION['mei'] : '';
	$mei = isset( $_COOKIE['mei']  ) ? $_COOKIE['mei']  : '';
}
$businessid = isset($_POST["businessid"])?$_POST["businessid"]:'';
$companyid = isset($_POST["companyid"])?$_POST["companyid"]:'';
$date1 = isset($_POST["date1"])?$_POST["date1"]:'';
$date1_5wk = isset($_POST["date1_5wk"])?$_POST["date1_5wk"]:'';
$prevperiod = isset($_POST["prevperiod"])?$_POST["prevperiod"]:'';
$prevperiod_5wk = isset($_POST["prevperiod_5wk"])?$_POST["prevperiod_5wk"]:'';
$is_disabled1 = isset($_POST["is_disabled1"])?$_POST["is_disabled1"]:'';
$is_disabled2 = isset($_POST["is_disabled2"])?$_POST["is_disabled2"]:'';
$update_payroll = isset($_POST["update_payroll"])?$_POST["update_payroll"]:'';
$lbrgrp = isset($_POST["lbrgrp"])?$_POST["lbrgrp"]:'';

$com_calc_type = 0;
if ( $com_calc_type == "" ) {
#	$com_calc_type = isset( $_SESSION['com_calc_type'] ) ? $_SESSION['com_calc_type'] : '';
	$com_calc_type = isset( $_COOKIE['com_calc_type']  ) ? $_COOKIE['com_calc_type']  :'';
}

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query( $query );
$num = @mysql_numrows( $result );
//mysql_close();

$security_level = @mysql_result( $result, 0, 'security_level' );
$mysecurity = @mysql_result( $result, 0, 'security' );
$bid = @mysql_result( $result, 0, 'businessid' );
$cid = @mysql_result( $result, 0, 'companyid' );
$bid2 = @mysql_result( $result, 0, 'busid2' );
$bid3 = @mysql_result( $result, 0, 'busid3' );
$bid4 = @mysql_result( $result, 0, 'busid4' );
$bid5 = @mysql_result( $result, 0, 'busid5' );
$bid6 = @mysql_result( $result, 0, 'busid6' );
$bid7 = @mysql_result( $result, 0, 'busid7' );
$bid8 = @mysql_result( $result, 0, 'busid8' );
$bid9 = @mysql_result( $result, 0, 'busid9' );
$bid10 = @mysql_result( $result, 0, 'busid10' );
$loginid = @mysql_result( $result, 0, 'loginid' );
if ($num != 1 || ($security_level==1 && ($bid != $businessid || $cid != $companyid)) || $user == "" || $pass == "")
{
	echo "<center><h3>Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
	if($lbrgrp>0){$query133 = "SELECT login.firstname,login.lastname,login.empl_no,login.loginid,jobtypedetail.jobtype,jobtypedetail.rate FROM login,jobtype,jobtypedetail,labor_group_detail WHERE login.oo2 = '$businessid' AND ((login.is_deleted = '0') OR (login.is_deleted = '1' AND login.del_date >= '$date1')) AND login.loginid = labor_group_detail.loginid AND labor_group_detail.labor_group_id = '$lbrgrp' AND jobtypedetail.loginid = login.loginid AND jobtypedetail.is_deleted = '0' AND jobtype.jobtypeid = jobtypedetail.jobtype AND jobtype.commission = '1' ORDER BY login.lastname DESC";}
	else{$query133 = "SELECT login.firstname,login.lastname,login.empl_no,login.loginid,jobtypedetail.jobtype,jobtypedetail.rate FROM login,jobtype,jobtypedetail WHERE login.oo2 = '$businessid' AND ((login.is_deleted = '0') OR (login.is_deleted = '1' AND login.del_date >= '$date1')) AND jobtypedetail.loginid = login.loginid AND jobtypedetail.is_deleted = '0' AND jobtype.jobtypeid = jobtypedetail.jobtype AND jobtype.commission = '1' ORDER BY login.lastname DESC";}
	$result133 = Treat_DB_ProxyOld::query( $query133 );
	$num133 = @mysql_numrows( $result133 );

	$num133--;
	while ( $num133 >= 0 ) {

		$editlogin = @mysql_result( $result133, $num133, 'loginid' );
		$firstname = @mysql_result( $result133, $num133, 'firstname' );
		$lastname = @mysql_result( $result133, $num133, 'lastname' );
		$empl_no = @mysql_result( $result133, $num133, 'empl_no' );
		$empl_rate = @mysql_result( $result133, $num133, 'jobtypedetail.rate' );

		$emp_com1 = isset($_POST["$editlogin:emp_com1"])?$_POST["$editlogin:emp_com1"]:'';
		$emp_com2 = isset($_POST["$editlogin:emp_com2"])?$_POST["$editlogin:emp_com2"]:'';

		$route1 = isset($_POST["$editlogin:route1"])?$_POST["$editlogin:route1"]:'';
		$route2 = isset($_POST["$editlogin:route2"])?$_POST["$editlogin:route2"]:'';

		$weekend1 = isset($_POST["$editlogin:weekend1"])?$_POST["$editlogin:weekend1"]:'';
		$weekend2 = isset($_POST["$editlogin:weekend2"])?$_POST["$editlogin:weekend2"]:'';

		$pay_days1 = isset($_POST["$editlogin:pay_days1"])?$_POST["$editlogin:pay_days1"]:'';
		$pay_days1 = abs( $pay_days1 );
		$pay_days2 = isset($_POST["$editlogin:pay_days2"])?$_POST["$editlogin:pay_days2"]:'';
		$pay_days2 = abs( $pay_days2 );

		$nopay_days1 = isset($_POST["$editlogin:nopay_days1"])?$_POST["$editlogin:nopay_days1"]:'';
		$nopay_days1 = abs( $nopay_days1 );
		$nopay_days2 = isset($_POST["$editlogin:nopay_days2"])?$_POST["$editlogin:nopay_days2"]:'';
		$nopay_days2 = abs( $nopay_days2 );

		$pto1 = isset($_POST["$editlogin:pto1"])?$_POST["$editlogin:pto1"]:'';
		$pto2 = isset($_POST["$editlogin:pto2"])?$_POST["$editlogin:pto2"]:'';
		/////////////////////CREATE COMMISSION RECORDS
		if ( $update_payroll == 1 ) {
			$query2 = "SELECT * FROM jobtypedetail WHERE loginid = '$editlogin' AND is_deleted = '0'";
			$result2 = Treat_DB_ProxyOld::query( $query2 );

			$rate = @mysql_result( $result2, 0, 'rate' );
			$jobtype = @mysql_result( $result2, 0, 'jobtype' );

			////////////PREV PERIOD
			if ( $is_disabled2 != "DISABLED" ) {
				$query2 = "SELECT * FROM labor_temp_comm WHERE loginid = '$editlogin' AND date = '$prevperiod'";
				$result2 = Treat_DB_ProxyOld::query( $query2 );
				$num2 = @mysql_numrows( $result2 );

				if ( $num2 == 0 ) {
					$query3 = "INSERT INTO labor_temp_comm (loginid,businessid,jobtypeid,date,rate) VALUES ('$editlogin','$businessid','$jobtype','$prevperiod','$rate')";
					$result3 = Treat_DB_ProxyOld::query( $query3 );
				}
				else {
					$temp_commid = @mysql_result( $result2, 0, 'tempid' );
					$query3 = "DELETE FROM labor_comm WHERE temp_commid = '$temp_commid'";
					$result3 = Treat_DB_ProxyOld::query( $query3 );
				}
			}
			////////////DATE1
			if ( $is_disabled1 != "DISABLED" ) {
				$query2 = "SELECT * FROM labor_temp_comm WHERE loginid = '$editlogin' AND date = '$date1'";
				$result2 = Treat_DB_ProxyOld::query( $query2 );
				$num2 = @mysql_numrows( $result2 );

				if ( $num2 == 0 ) {
					$query3 = "INSERT INTO labor_temp_comm (loginid,businessid,jobtypeid,date,rate) VALUES ('$editlogin','$businessid','$jobtype','$date1','$rate')";
					$result3 = Treat_DB_ProxyOld::query( $query3 );
				}
				else {
					$temp_commid = @mysql_result( $result2, 0, 'tempid' );
					$query3 = "DELETE FROM labor_comm WHERE temp_commid = '$temp_commid'";
					$result3 = Treat_DB_ProxyOld::query( $query3 );
				}
			}
		}
		/////////////////////START SAVING
		if ( $is_disabled2 != "DISABLED" ) {
			$query3 = "SELECT * FROM labor_route_link WHERE (loginid = '$editlogin' OR (login_routeid = '$route1' AND loginid = '0')) AND date = '$prevperiod'";
			$result3 = Treat_DB_ProxyOld::query( $query3 );
			$num3 = @mysql_numrows( $result3 );

			if ( $num3 == 0 ) {
				$query4 = "INSERT INTO labor_route_link (date,login_routeid,loginid,weekend_pay,pay_days,nopay_days,pto,commissionid) VALUES ('$prevperiod','$route1','$editlogin','$weekend1','$pay_days1','$nopay_days1','$pto1','$emp_com1')";
				$result4 = Treat_DB_ProxyOld::query( $query4 );
			}
			else {
				$laborid = @mysql_result( $result3, 0, 'laborid' );
				$query4 = "UPDATE labor_route_link SET loginid = '$editlogin', login_routeid = '$route1', weekend_pay = '$weekend1', pay_days = '$pay_days1', nopay_days = '$nopay_days1', pto = '$pto1', commissionid = '$emp_com1' WHERE laborid = '$laborid'";
				$result4 = Treat_DB_ProxyOld::query( $query4 );
			}
		}
		if ( $is_disabled1 != "DISABLED" ) {
			$query3 = "SELECT * FROM labor_route_link WHERE (loginid = '$editlogin' OR (login_routeid = '$route2' AND loginid = '0')) AND date = '$date1'";
			$result3 = Treat_DB_ProxyOld::query( $query3 );
			$num3 = @mysql_numrows( $result3 );

			if ( $num3 == 0 ) {
				$query4 = "INSERT INTO labor_route_link (date,login_routeid,loginid,weekend_pay,pay_days,nopay_days,pto,commissionid) VALUES ('$date1','$route2','$editlogin','$weekend2','$pay_days2','$nopay_days2','$pto2','$emp_com2')";
				$result4 = Treat_DB_ProxyOld::query( $query4 );
			}
			else {
				$laborid = @mysql_result( $result3, 0, 'laborid' );
				$query4 = "UPDATE labor_route_link SET loginid = '$editlogin', login_routeid = '$route2', weekend_pay = '$weekend2', pay_days = '$pay_days2', nopay_days = '$nopay_days2', pto = '$pto2', commissionid = '$emp_com2' WHERE laborid = '$laborid'";
				$result4 = Treat_DB_ProxyOld::query( $query4 );
			}
		}
		////////////////////END SAVING
		/////////////CALCULATE PAYROLL 1

		////////////////GET SALES/FREEVEND/CSV
		$query = "SELECT * FROM labor_route_link WHERE loginid = '$editlogin' AND date = '$prevperiod'";
		$result = Treat_DB_ProxyOld::query( $query );

		$login_routeid1 = @mysql_result( $result, 0, 'login_routeid' );
		$freevend1 = @mysql_result( $result, 0, 'freevend' );
		$noncash1 = @mysql_result( $result, 0, 'noncash' );
		$kiosk1 = @mysql_result( $result, 0, 'kiosk' );
		$csv1 = @mysql_result( $result, 0, 'csv' );
		$weekend1 = @mysql_result( $result, 0, 'weekend_pay' );
		$pay_days1 = @mysql_result( $result, 0, 'pay_days' );
		$nopay_days1 = @mysql_result( $result, 0, 'nopay_days' );
		$emp_com1 = @mysql_result( $result, 0, 'commissionid' );

		$query = "SELECT route FROM login_route WHERE login_routeid = '$login_routeid1'";
		$result = Treat_DB_ProxyOld::query( $query );

		$mei_route1 = @mysql_result( $result, 0, 'route' );

		///////////////////CSV & FREEVEND FOR 5 WK AVG (WEEK 1)
		if ( $com_calc_type == 1 || $com_calc_type == 2 ) {
			$query = "SELECT SUM(freevend/5) AS freevend,SUM(csv/5) AS csv FROM labor_route_link WHERE loginid = '$editlogin' AND date <= '$prevperiod' AND date >= '$prevperiod_5wk' ORDER BY date";
			$result = Treat_DB_ProxyOld::query( $query );

			$freevend1 = @mysql_result( $result, 0, 'freevend' );
			$csv1 = @mysql_result( $result, 0, 'csv' );
		}

		$query2 = "SELECT * FROM labor_route_link WHERE loginid = '$editlogin' AND date = '$date1'";
		$result2 = Treat_DB_ProxyOld::query( $query2 );

		$login_routeid2 = @mysql_result( $result2, 0, 'login_routeid' );
		$freevend2 = @mysql_result( $result2, 0, 'freevend' );
		$noncash2 = @mysql_result( $result2, 0, 'noncash' );
		$kiosk2 = @mysql_result( $result2, 0, 'kiosk' );
		$csv2 = @mysql_result( $result2, 0, 'csv' );
		$weekend2 = @mysql_result( $result2, 0, 'weekend_pay' );
		$pay_days2 = @mysql_result( $result2, 0, 'pay_days' );
		$nopay_days2 = @mysql_result( $result2, 0, 'nopay_days' );
		$emp_com2 = @mysql_result( $result2, 0, 'commissionid' );

		$query2 = "SELECT route FROM login_route WHERE login_routeid = '$login_routeid2'";
		$result2 = Treat_DB_ProxyOld::query( $query2 );

		$mei_route2 = @mysql_result( $result2, 0, 'route' );

		///////////////////CSV & FREEVEND FOR 5 WK AVG (WEEK 2)
		if ( $com_calc_type == 1 || $com_calc_type == 2 ) {
			$query = "SELECT SUM(freevend/5) AS freevend,SUM(csv/5) AS csv FROM labor_route_link WHERE loginid = '$editlogin' AND date <= '$date1' AND date >= '$date1_5wk' ORDER BY date";
			$result = Treat_DB_ProxyOld::query( $query );

			$freevend2 = @mysql_result( $result, 0, 'freevend' );
			$csv2 = @mysql_result( $result, 0, 'csv' );
		}

		$prevperiodstart = $prevperiod;
		for ( $counter = 1; $counter <= 6; $counter++ ) {
			$prevperiodstart = prevday( $prevperiodstart );
		}

		//////////////////MEI SALES ADDED 5/19/09
		if ( $mei == 1 ) {
			if ( $com_calc_type == 1 || $com_calc_type == 2 ) {
				$query = "SELECT SUM(amount/5) AS totalamt FROM vend_mei_sales WHERE route LIKE '$mei_route1' AND businessid = '$businessid' AND date >= '$prevperiod_5wk' AND date <= '$prevperiod'";
				$result = Treat_DB_ProxyOld::query( $query );
			}
			else {
				$query = "SELECT SUM(amount) AS totalamt FROM vend_mei_sales WHERE route LIKE '$mei_route1' AND businessid = '$businessid' AND date >= '$prevperiodstart' AND date <= '$prevperiod'";
				$result = Treat_DB_ProxyOld::query( $query );
			}
		}
		else {
			if ( $com_calc_type == 1 || $com_calc_type == 2 ) {
				$query = "SELECT SUM(route_total/5) AS totalamt FROM vend_route_collection WHERE login_routeid = '$login_routeid1' AND date >= '$prevperiod_5wk' AND date <= '$prevperiod'";
				$result = Treat_DB_ProxyOld::query( $query );
			}
			else {
				$query = "SELECT SUM(route_total) AS totalamt FROM vend_route_collection WHERE login_routeid = '$login_routeid1' AND date >= '$prevperiodstart' AND date <= '$prevperiod'";
				$result = Treat_DB_ProxyOld::query( $query );
			}
		}

		$sales1 = @mysql_result( $result, 0, 'totalamt' );

		$date1start = $date1;
		for ( $counter = 1; $counter <= 6; $counter++ ) {
			$date1start = prevday( $date1start );
		}

		//////////////////MEI SALES ADDED 5/19/09
		if ( $mei == 1 ) {
			if ( $com_calc_type == 1 || $com_calc_type == 2 ) {
				$query2 = "SELECT SUM(amount/5) AS totalamt FROM vend_mei_sales WHERE route LIKE '$mei_route2' AND businessid = '$businessid' AND date >= '$date1_5wk' AND date <= '$date1'";
				$result2 = Treat_DB_ProxyOld::query( $query2 );
			}
			else {
				$query2 = "SELECT SUM(amount) AS totalamt FROM vend_mei_sales WHERE route LIKE '$mei_route2' AND businessid = '$businessid' AND date >= '$date1start' AND date <= '$date1'";
				$result2 = Treat_DB_ProxyOld::query( $query2 );
			}
		}
		else {
			if ( $com_calc_type == 1 || $com_calc_type == 2 ) {
				$query2 = "SELECT SUM(route_total/5) AS totalamt FROM vend_route_collection WHERE login_routeid = '$login_routeid2' AND date >= '$date1_5wk' AND date <= '$date1'";
				$result2 = Treat_DB_ProxyOld::query( $query2 );
			}
			else {
				$query2 = "SELECT SUM(route_total) AS totalamt FROM vend_route_collection WHERE login_routeid = '$login_routeid2' AND date >= '$date1start' AND date <= '$date1'";
				$result2 = Treat_DB_ProxyOld::query( $query2 );
			}
		}

		$sales2 = @mysql_result( $result2, 0, 'totalamt' );

		$weekdays1 = 5 - $nopay_days1 - $pay_days1;
		$weekdays2 = 5 - $nopay_days2 - $pay_days2;
		$weekdays_total = $weekdays1 + $weekdays2;
		//////////////////////////////////////
		$payroll1 = 0;

		$query12 = "SELECT commissionid FROM login_route WHERE login_routeid = '$route1'";
		$result12 = Treat_DB_ProxyOld::query( $query12 );

		$commissionid1 = @mysql_result( $result12, 0, 'commissionid' );
		if ( $emp_com1 > 0 ) {
			$commissionid1 = $emp_com1;
		}

		$query12 = "SELECT * FROM vend_commission WHERE commissionid = '$commissionid1'";
		$result12 = Treat_DB_ProxyOld::query( $query12 );

		if ( $route1 == -1 ) {
			$query12 = "SELECT * FROM vend_commission WHERE companyid = '$companyid' LIMIT 0,1";
			$result12 = Treat_DB_ProxyOld::query( $query12 );
		}

		$base1 = @mysql_result( $result12, 0, 'base' );
		if($route1==-1){$base1=500;}
		elseif($route1==-2){$base1=0;}
		$commission_name = @mysql_result( $result12, 0, 'name' );
		$fixed_amount1 = @mysql_result( $result12, 0, 'fixed_amount' );
		$base_link1 = @mysql_result( $result12, 0, 'base_link' );
		$com_link1 = @mysql_result( $result12, 0, 'commission_link' );
		$weekend_link1 = @mysql_result( $result12, 0, 'weekend_link' );
		$pto_link1 = @mysql_result( $result12, 0, 'pto_link' );
		////////SALES
		$query12 = "SELECT * FROM vend_commission_detail WHERE commissionid = '$commissionid1' AND type = '1'";
		$result12 = Treat_DB_ProxyOld::query( $query12 );
		$num12 = @mysql_numrows( $result12 );
		$num12--;
		while ( $num12 >= 0 ) {
			$lower = @mysql_result( $result12, $num12, 'lower' );
			$upper = @mysql_result( $result12, $num12, 'upper' );
			$calc_type = @mysql_result( $result12, $num12, 'calc_type' );
			$percent = @mysql_result( $result12, $num12, 'percent' );

			if($calc_type==1&&$sales1>=$lower&&$sales1<=$upper){$payroll1=$sales1*($percent/100);}
			elseif($calc_type==2&&($freevend1+$noncash1+$kiosk1)>=$lower&&($freevend1+$noncash1+$kiosk1)<=$upper){$payroll1=$sales1*($percent/100);}
			elseif($calc_type==3&&$csv1>=$lower&&$csv1<=$upper){$payroll1=$sales1*($percent/100);}
			elseif($calc_type==4&&($sales1+$freevend1+$noncash1+$kiosk1)>=$lower&&($sales1+$freevend1+$noncash1+$kiosk1)<=$upper){$payroll1=$sales1*($percent/100);}
			elseif($calc_type==5&&($freevend1+$noncash1+$kiosk1+$csv1)>=$lower&&($freevend1+$noncash1+$kiosk1+$csv1)<=$upper){$payroll1=$sales1*($percent/100);}
			elseif($calc_type==6&&($sales1+$csv1)>=$lower&&($sales1+$csv1)<=$upper){$payroll1=$sales1*($percent/100);}
			elseif($calc_type==7&&($sales1+$freevend1+$noncash1+$kiosk1+$csv1)>=$lower&&($sales1+$freevend1+$noncash1+$kiosk1+$csv1)<=$upper){$payroll1=$sales1*($percent/100);}

			$num12--;
		}
		///////////////END SALES
		////////FREEVEND
		$query12 = "SELECT * FROM vend_commission_detail WHERE commissionid = '$commissionid1' AND type = '2'";
		$result12 = Treat_DB_ProxyOld::query( $query12 );
		$num12 = @mysql_numrows( $result12 );
		$num12--;
		while ( $num12 >= 0 ) {
			$lower = @mysql_result( $result12, $num12, 'lower' );
			$upper = @mysql_result( $result12, $num12, 'upper' );
			$calc_type = @mysql_result( $result12, $num12, 'calc_type' );
			$percent = @mysql_result( $result12, $num12, 'percent' );

			if($calc_type==1&&$sales1>=$lower&&$sales1<=$upper){$payroll1+=($freevend1+$noncash1+$kiosk1)*($percent/100);}
			elseif($calc_type==2&&($freevend1+$noncash1+$kiosk1)>=$lower&&($freevend1+$noncash1+$kiosk1)<=$upper){$payroll1+=($freevend1+$noncash1+$kiosk1)*($percent/100);}
			elseif($calc_type==3&&$csv1>=$lower&&$csv1<=$upper){$payroll1+=($freevend1+$noncash1+$kiosk1)*($percent/100);}
			elseif($calc_type==4&&($sales1+$freevend1+$noncash1+$kiosk1)>=$lower&&($sales1+$freevend1+$noncash1+$kiosk1)<=$upper){$payroll1+=($freevend1+$noncash1+$kiosk1)*($percent/100);}
			elseif($calc_type==5&&($freevend1+$noncash1+$kiosk1+$csv1)>=$lower&&($freevend1+$noncash1+$kiosk1+$csv1)<=$upper){$payroll1+=($freevend1+$noncash1+$kiosk1)*($percent/100);}
			elseif($calc_type==6&&($sales1+$csv1)>=$lower&&($sales1+$csv1)<=$upper){$payroll1+=($freevend1+$noncash1+$kiosk1)*($percent/100);}
			elseif($calc_type==7&&($sales1+$freevend1+$noncash1+$kiosk1+$csv1)>=$lower&&($sales1+$freevend1+$noncash1+$kiosk1+$csv1)<=$upper){$payroll1+=($freevend1+$noncash1+$kiosk1)*($percent/100);}

			$num12--;
		}
		///////////////END FREEVEND
		////////CSV
		$query12 = "SELECT * FROM vend_commission_detail WHERE commissionid = '$commissionid1' AND type = '3'";
		$result12 = Treat_DB_ProxyOld::query( $query12 );
		$num12 = @mysql_numrows( $result12 );
		$num12--;
		while ( $num12 >= 0 ) {
			$lower = @mysql_result( $result12, $num12, 'lower' );
			$upper = @mysql_result( $result12, $num12, 'upper' );
			$calc_type = @mysql_result( $result12, $num12, 'calc_type' );
			$percent = @mysql_result( $result12, $num12, 'percent' );

			if($calc_type==1&&$sales1>=$lower&&$sales1<=$upper){$payroll1+=$csv1*($percent/100);}
			elseif($calc_type==2&&($freevend1+$noncash1+$kiosk1)>=$lower&&($freevend1+$noncash1+$kiosk1)<=$upper){$payroll1+=$csv1*($percent/100);}
			elseif($calc_type==3&&$csv1>=$lower&&$csv1<=$upper){$payroll1+=$csv1*($percent/100);}
			elseif($calc_type==4&&($sales1+$freevend1+$noncash1+$kiosk1)>=$lower&&($sales1+$freevend1+$noncash1+$kiosk1)<=$upper){$payroll1+=$csv1*($percent/100);}
			elseif($calc_type==5&&($freevend1+$noncash1+$kiosk1+$csv1)>=$lower&&($freevend1+$noncash1+$kiosk1+$csv1)<=$upper){$payroll1+=$csv1*($percent/100);}
			elseif($calc_type==6&&($sales1+$csv1)>=$lower&&($sales1+$csv1)<=$upper){$payroll1+=$csv1*($percent/100);}
			elseif($calc_type==7&&($sales1+$freevend1+$noncash1+$kiosk1+$csv1)>=$lower&&($sales1+$freevend1+$noncash1+$kiosk1+$csv1)<=$upper){$payroll1+=$csv1*($percent/100);}

			$num12--;
		}

		if ( $com_calc_type == 1 || $com_calc_type == 2 ) {
			if($payroll1>$base1){$com1=$payroll1*((5-$pay_days1)/5);$base1=0;}
			else{$base1=$base1*((5-$pay_days1)/5);$com1=0;}
		}
		else {
			if($payroll1>$base1){$com1=$payroll1;$base1=0;}
			else{$base1=$base1;$com1=0;}
		}

		$avg_pay1=($base1+$com1)/(5-$nopay_days1-$pay_days1);

						
		////////////END PAYROLL 1
		/////////////CALCULATE PAYROLL 2
		$payroll2=0;

		$query12 = "SELECT commissionid FROM login_route WHERE login_routeid = '$route2'";
		$result12 = Treat_DB_ProxyOld::query( $query12 );

		$commissionid2 = @mysql_result( $result12, 0, 'commissionid' );
		if($emp_com2>0){$commissionid2=$emp_com2;}

		$query12 = "SELECT * FROM vend_commission WHERE commissionid = '$commissionid2'";
		$result12 = Treat_DB_ProxyOld::query( $query12 );

		if ( $route2 == -1 ) {
			$query12 = "SELECT * FROM vend_commission WHERE companyid = '$companyid' LIMIT 0,1";
			$result12 = Treat_DB_ProxyOld::query( $query12 );
		}

		$base2 = @mysql_result( $result12, 0, 'base' );
		if($route2==-1){$base2=500;}
		elseif($route2==-2){$base2=0;}
		$commission_name = @mysql_result( $result12, 0, 'name' );
		$fixed_amount2 = @mysql_result( $result12, 0, 'fixed_amount' );
		$base_link2 = @mysql_result( $result12, 0, 'base_link' );
		$com_link2 = @mysql_result( $result12, 0, 'commission_link' );
		$weekend_link2 = @mysql_result( $result12, 0, 'weekend_link' );
		$pto_link2 = @mysql_result( $result12, 0, 'pto_link' );
		////////SALES
		$query12 = "SELECT * FROM vend_commission_detail WHERE commissionid = '$commissionid2' AND type = '1'";
		$result12 = Treat_DB_ProxyOld::query( $query12 );
		$num12 = @mysql_numrows( $result12 );
		$num12--;
		while ( $num12 >= 0 ) {
			$lower = @mysql_result( $result12, $num12, 'lower' );
			$upper = @mysql_result( $result12, $num12, 'upper' );
			$calc_type = @mysql_result( $result12, $num12, 'calc_type' );
			$percent = @mysql_result( $result12, $num12, 'percent' );

			if($calc_type==1&&$sales2>=$lower&&$sales2<=$upper){$payroll2=$sales2*($percent/100);}
			elseif($calc_type==2&&($freevend2+$noncash2+$kiosk2)>=$lower&&($freevend2+$noncash2+$kiosk2)<=$upper){$payroll2=$ales1*($percent/100);}
			elseif($calc_type==3&&$csv2>=$lower&&$csv2<=$upper){$payroll2=$sales2*($percent/100);}
			elseif($calc_type==4&&($sales2+$freevend2+$noncash2+$kiosk2)>=$lower&&($sales2+$freevend2+$noncash2+$kiosk2)<=$upper){$payroll2=$sales2*($percent/100);}
			elseif($calc_type==5&&($freevend2+$noncash2+$kiosk2+$csv2)>=$lower&&($freevend2+$noncash2+$kiosk2+$csv2)<=$upper){$payroll2=$sales2*($percent/100);}
			elseif($calc_type==6&&($sales2+$csv2)>=$lower&&($sales2+$csv2)<=$upper){$payroll2=$sales2*($percent/100);}
			elseif($calc_type==7&&($sales2+$freevend2+$noncash2+$kiosk2+$csv2)>=$lower&&($sales2+$freevend2+$noncash2+$kiosk2+$csv2)<=$upper){$payroll2=$sales2*($percent/100);}

			$num12--;
		}
		///////////////END SALES
		////////FREEVEND
		$query12 = "SELECT * FROM vend_commission_detail WHERE commissionid = '$commissionid2' AND type = '2'";
		$result12 = Treat_DB_ProxyOld::query( $query12 );
		$num12 = @mysql_numrows( $result12 );
		$num12--;
		while ( $num12 >= 0 ) {
			$lower = @mysql_result( $result12, $num12, 'lower' );
			$upper = @mysql_result( $result12, $num12, 'upper' );
			$calc_type = @mysql_result( $result12, $num12, 'calc_type' );
			$percent = @mysql_result( $result12, $num12, 'percent' );

			if($calc_type==1&&$sales2>=$lower&&$sales2<=$upper){$payroll2+=($freevend2+$noncash2+$kiosk2)*($percent/100);}
			elseif($calc_type==2&&($freevend2+$noncash2+$kiosk2)>=$lower&&($freevend2+$noncash2+$kiosk2)<=$upper){$payroll2+=($freevend2+$noncash2+$kiosk2)*($percent/100);}
			elseif($calc_type==3&&$csv2>=$lower&&$csv2<=$upper){$payroll2+=($freevend2+$noncash2+$kiosk2)*($percent/100);}
			elseif($calc_type==4&&($sales2+$freevend2+$noncash2+$kiosk2)>=$lower&&($sales2+$freevend2+$noncash2+$kiosk2)<=$upper){$payroll2+=($freevend2+$noncash2+$kiosk2)*($percent/100);}
			elseif($calc_type==5&&($freevend2+$noncash2+$kiosk2+$csv2)>=$lower&&($freevend2+$noncash2+$kiosk2+$csv2)<=$upper){$payroll2+=($freevend2+$noncash2+$kiosk2)*($percent/100);}
			elseif($calc_type==6&&($sales2+$csv2)>=$lower&&($sales2+$csv2)<=$upper){$payroll2+=($freevend2+$noncash2+$kiosk2)*($percent/100);}
			elseif($calc_type==7&&($sales2+$freevend2+$noncash2+$kiosk2+$csv2)>=$lower&&($sales2+$freevend2+$noncash2+$kiosk2+$csv2)<=$upper){$payroll2+=($freevend2+$noncash2+$kiosk2)*($percent/100);}

			$num12--;
		}
		///////////////END FREEVEND
		////////CSV
		$query12 = "SELECT * FROM vend_commission_detail WHERE commissionid = '$commissionid2' AND type = '3'";
		$result12 = Treat_DB_ProxyOld::query( $query12 );
		$num12 = @mysql_numrows( $result12 );
		$num12--;
		while ( $num12 >= 0 ) {
			$lower = @mysql_result( $result12, $num12, 'lower' );
			$upper = @mysql_result( $result12, $num12, 'upper' );
			$calc_type = @mysql_result( $result12, $num12, 'calc_type' );
			$percent = @mysql_result( $result12, $num12, 'percent' );

			if($calc_type==1&&$sales2>=$lower&&$sales2<=$upper){$payroll2+=$csv2*($percent/100);}
			elseif($calc_type==2&&($freevend2+$noncash2+$kiosk2)>=$lower&&($freevend2+$noncash2+$kiosk2)<=$upper){$payroll2+=$csv2*($percent/100);}
			elseif($calc_type==3&&$csv2>=$lower&&$csv2<=$upper){$payroll2+=$csv2*($percent/100);}
			elseif($calc_type==4&&($sales2+$freevend2+$noncash2+$kiosk2)>=$lower&&($sales2+$freevend2+$noncash2+$kiosk2)<=$upper){$payroll2+=$csv2*($percent/100);}
			elseif($calc_type==5&&($freevend2+$noncash2+$kiosk2+$csv2)>=$lower&&($freevend2+$noncash2+$kiosk2+$csv2)<=$upper){$payroll2+=$csv2*($percent/100);}
			elseif($calc_type==6&&($sales2+$csv2)>=$lower&&($sales2+$csv2)<=$upper){$payroll2+=$csv2*($percent/100);}
			elseif($calc_type==7&&($sales2+$freevend2+$noncash2+$kiosk2+$csv2)>=$lower&&($sales2+$freevend2+$noncash2+$kiosk2+$csv2)<=$upper){$payroll2+=$csv2*($percent/100);}

			$num12--;
		}

		if ( $com_calc_type == 1 || $com_calc_type == 2 ) {
			if($payroll2>$base2){$com2=$payroll2*((5-$pay_days2)/5);$base2=0;}
			else{$base2=$base2*((5-$pay_days2)/5);$com2=0;}
		}
		else {
			if($payroll2>$base2){$com2=$payroll2;$base2=0;}
			else{$base2=$base2;$com2=0;}
		}

		////////////TERMED EMPLOYEES
		if($login_routeid1==-2){$base1=0;$com1=0;$showcom1=0;}
		if($login_routeid2==-2){$base2=0;$com2=0;$showcom2=0;}

		////////////AVERAGES
		if ( $com_calc_type == 1 ) {
			$average1=($base1+$com1)/((5-$pay_days1)/5)/(5-$pay_days1);
			$average2=($base2+$com2)/((5-$pay_days2)/5)/(5-$pay_days2);
			$average=($base1+$com1+$base2+$com2)/((10-$pay_days1-$pay_days2)/10)/(10-$pay_days1-$pay_days2);
		}
		elseif ( $com_calc_type == 2 ) {
			$average1=($base1+$com1)/(5-$pay_days1);
			$average2=($base2+$com2)/(5-$pay_days2);
			$average=($base1+$com1+$base2+$com2)/(10-$pay_days1-$pay_days2);
		}
		else {
			$average1=($base1+$com1)/(5-$pay_days1);
			$average2=($base2+$com2)/(5-$pay_days2);
			$average=($base1+$com1+$base2+$com2)/(10-$pay_days1-$pay_days2);
		}

		if ( $com_calc_type == 5 ) {
			if ( $base1 == 500 && $pay_days1 > 0 ) {
				$total1=$base1+$com1+$weekend1-($average1*$nopay_days1);
			}
			else {
				$total1=$base1+$com1+$weekend1+($average*$pay_days1)-($average1*$nopay_days1); 
			}
			if ( $base2 == 500 && $pay_days2 > 0 ) {
				$total2=$base2+$com2+$weekend2-($average2*$nopay_days2);
			}
			else {
				$total2=$base2+$com2+$weekend2+($average*$pay_days2)-($average2*$nopay_days2); 
			}
		}
		else {
			$total1=$base1+$com1+$weekend1+($average*$pay_days1)-($average1*$nopay_days1); 

			$total2=$base2+$com2+$weekend2+($average*$pay_days2)-($average2*$nopay_days2);
		}
		
		////////////END PAYROLL 2

		/////////////////////////////////////////////////////////////
		////fixed amount 8/10/10 - for canteloupe transition routes//
		/////////////////////////////////////////////////////////////
		if ( $fixed_amount1 == 1 ) {
			$base1 = 0;
			$com1 = $empl_rate;
			$showcom1 = number_format( $empl_rate, 2  );
		}
		if ( $fixed_amount2 == 1 ) {
			$base2 = 0;
			$com2 = $empl_rate;
			$showcom2 = number_format( $empl_rate, 2 );
		}
		//////////end fixed amount

		//////////// UPDATE LABOR COMM
		if ( $update_payroll == 1 ) {
			//////////PAYROLL 1
			
			$query2 = "SELECT * FROM labor_temp_comm WHERE loginid = '$editlogin' AND date = '$prevperiod'";
			$result2 = Treat_DB_ProxyOld::query( $query2 );

			$tempid = @mysql_result( $result2, 0, 'tempid' );

			if ( $pto1 > 0 ) {
				$query2 = "INSERT INTO labor_comm (temp_commid,coded,amount) VALUES ('$tempid','$pto_link1','$pto1')";
				$result2 = Treat_DB_ProxyOld::query( $query2 );
			}
			if ( $weekend1 > 0 ) {
				$query2 = "INSERT INTO labor_comm (temp_commid,coded,amount) VALUES ('$tempid','$weekend_link1','$weekend1')";
				$result2 = Treat_DB_ProxyOld::query( $query2 );

				$totalpayroll1+=$weekend1;
			}
			if ( $com1 > 0 ) {
				if ( $fixed_amount1 == 1 ) {
					$total1=$com1;
				}
				else {
					$total1=money($total1-$weekend1);
				}
				$query2 = "INSERT INTO labor_comm (temp_commid,coded,amount) VALUES ('$tempid','$com_link1','$total1')";
				$result2 = Treat_DB_ProxyOld::query( $query2 );

				$totalpayroll1+=$total1;
			}
			elseif ( $base1 > 0 ) {
				$total1=money($total1-$weekend1);
				$query2 = "INSERT INTO labor_comm (temp_commid,coded,amount) VALUES ('$tempid','$base_link1','$total1')";
				$result2 = Treat_DB_ProxyOld::query( $query2 );

				$totalpayroll1+=$total1;
			}

			//////////PAYROLL 2
			$query2 = "SELECT * FROM labor_temp_comm WHERE loginid = '$editlogin' AND date = '$date1'";
			$result2 = Treat_DB_ProxyOld::query( $query2 );

			$tempid = @mysql_result( $result2, 0, 'tempid' );

			if ( $pto2 > 0 ) {
				$query2 = "INSERT INTO labor_comm (temp_commid,coded,amount) VALUES ('$tempid','$pto_link2','$pto2')";
				$result2 = Treat_DB_ProxyOld::query( $query2 );
			}
			if ( $weekend2 > 0 ) {
				$query2 = "INSERT INTO labor_comm (temp_commid,coded,amount) VALUES ('$tempid','$weekend_link2','$weekend2')";
				$result2 = Treat_DB_ProxyOld::query( $query2 );

				$totalpayroll2+=$weekend2;
			}
			if ( $com2 > 0 ) {
				if ( $fixed_amount2 == 1 ) {
					$total2=$com2;
				}
				else {
					$total2=money($total2-$weekend2);
				}
				$query2 = "INSERT INTO labor_comm (temp_commid,coded,amount) VALUES ('$tempid','$com_link2','$total2')";
				$result2 = Treat_DB_ProxyOld::query( $query2 );

				$totalpayroll2+=$total2;
			}
			elseif ( $base2 > 0 ) {
				$total2=money($total2-$weekend2);
				$query2 = "INSERT INTO labor_comm (temp_commid,coded,amount) VALUES ('$tempid','$base_link2','$total2')";
				$result2 = Treat_DB_ProxyOld::query( $query2 );

				$totalpayroll2+=$total2;
			}
		}
		/////////// END UPDATE

		$num133--;
	}
	/////////// AUDIT TRAIL
	if ( $update_payroll == 1 ) {
		$query2 = "INSERT INTO audit_route (date1,date2,loginid,route,route2,user) VALUES ('$prevperiod','$date1','$editlogin','$route1','$route2','$user')";
		$result2 = Treat_DB_ProxyOld::query( $query2 );

		/////////// END AUDIT

		/////////// WK1 total payroll
		$query2 = "SELECT * FROM stats WHERE businessid = '$businessid' AND date = '$prevperiod'";
		$result2 = Treat_DB_ProxyOld::query( $query2 );
		$num2 = @mysql_numrows( $result2 );

		if ( $num2 > 0 ) {
			$query2 = "UPDATE stats SET labor_vend = '$totalpayroll1' WHERE businessid = '$businessid' AND date = '$prevperiod'";
			$result2 = Treat_DB_ProxyOld::query( $query2 );
		}
		else {
			$query2 = "INSERT INTO stats (businessid,companyid,date,labor_vend) VALUES ('$businessid','$companyid','$prevperiod','$totalpayroll1')";
			$result2 = Treat_DB_ProxyOld::query( $query2 );
		}
		/////////// WK2 total payroll
		$query2 = "SELECT * FROM stats WHERE businessid = '$businessid' AND date = '$date1'";
		$result2 = Treat_DB_ProxyOld::query( $query2 );
		$num2 = @mysql_numrows( $result2 );

		if ( $num2 > 0 ) {
			$query2 = "UPDATE stats SET labor_vend = '$totalpayroll2' WHERE businessid = '$businessid' AND date = '$date1'";
			$result2 = Treat_DB_ProxyOld::query( $query2 );
		}
		else {
			$query2 = "INSERT INTO stats (businessid,companyid,date,labor_vend) VALUES ('$businessid','$companyid','$date1','$totalpayroll2')";
			$result2 = Treat_DB_ProxyOld::query( $query2 );
		}
	}
	////////////////////////////////////


	$location="busroute_detail.php?bid=$businessid&cid=$companyid&date1=$date1&lbrgrp=$lbrgrp&calc_type=$calc_type";
	header('Location: ./' . $location);

}
//mysql_close();
?>