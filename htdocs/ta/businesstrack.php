<?php

function graphData($businessid, $startday, $today, $week_end, $closed1, $show_inv){
	$status_array = array();
        $temp_date = $startday;
	while($temp_date <= $today){
		$status_array[$temp_date] = 0;
		$temp_date = nextday($temp_date);
	}

	$query = "SELECT  '1' AS sType , submit.date AS sDate, submit.export AS sStatus
				FROM business
				LEFT JOIN submit ON submit.businessid = business.businessid
				AND submit.date
				BETWEEN  '$startday'
				AND  '$today'
				WHERE business.businessid =$businessid
				UNION 
				SELECT '2' AS 
				sType , stats.date AS sDate, stats.closed AS sStatus
				FROM business
				LEFT JOIN stats ON stats.businessid = business.businessid
				AND stats.date
				BETWEEN  '$startday'
				AND  '$today'
				WHERE business.businessid =$businessid
				ORDER BY sDate ASC";
	$result = Treat_DB_ProxyOld::query( $query );

	while($r = mysql_fetch_array($result)){
		$type = $r["sType"];
		$sDate = $r["sDate"];
		$sStatus = $r["sStatus"];

		if($type == 1){
			if($sStatus == 1){
				$status_array[$sDate] = 2;
			}
			elseif($sStatus != ''){
				$status_array[$sDate] = 1;
			}
		}
		elseif($type == 2){
			if($sStatus == 1){
				$status_array[$sDate] = 3;
			}
		}
	}

	$graph = "<table border=0 cellspacing=0 cellpadding=0><tr>";

	foreach($status_array AS $date => $value){
                ///center of box
		if( substr( $date, 5, 2 ) != substr( nextday( $date ), 5, 2 ) && $show_inv == 1 ) {
			$monthbegin = substr( $date, 0, 7 ) . "-01";

			$query = "SELECT SUM(amount) AS inventory1, export, posted FROM inventor WHERE businessid = $businessid AND date = '$monthbegin' GROUP BY businessid";
			$result = Treat_DB_ProxyOld::query( $query );

			$inventory1 = @Treat_DB_ProxyOld::mysql_result( $result, 0, "inventory1" );
			$inv_export = @Treat_DB_ProxyOld::mysql_result( $result, 0, "export" );
			$inv_posted = @Treat_DB_ProxyOld::mysql_result( $result, 0, "posted" );

			$query = "SELECT SUM(inventor2.amount) AS inventory2 FROM acct_payable,inventor2 WHERE acct_payable.businessid = $businessid AND acct_payable.acct_payableid = inventor2.acct_payableid AND inventor2.date = '$date' GROUP BY inventor2.date";
			$result = Treat_DB_ProxyOld::query( $query );

			$inventory2 = @Treat_DB_ProxyOld::mysql_result( $result, 0, "inventory2" );

			$invcheck = abs( $inventory1 - $inventory2 ) > 0.05;

			if( $inventory1 == 0 || $inventory2 == 0 ) {
				$showthurs = "";
			}
			elseif( !$invcheck && $inv_export == 1 ) {
				$showthurs = "<center><img src=po3.gif height=10 width=10 title='Inventory Exported'></center>";
			}
			elseif( !$invcheck && $inv_posted == 1 ) {
				$showthurs = "<center><img src=po1.gif height=10 width=10 title='Inventory Ready for Export'></center>";
			}
			elseif( $invcheck && $export == 1 ) {
				$showthurs = "<center><img src=ex.gif height=10 width=10 title='Inventory Counts Do Not Match. Already Exported!'></center>";
			}
			elseif( $invcheck && $inv_posted == 1 ) {
				$inv_diff = number_format( $inventory1 - $inventory2, 2 );
				$showthurs = "<center><img src=ex.gif height=10 width=10 title='Inventory Counts Do Not Match ($inv_diff)'></center>";
			}			
		}
		elseif($value == -1){
			$showthurs = "<center><font color=white size=2>C</font></center>";
		}
		elseif(dayofweek($date) == $week_end){
			$showthurs = "<center><font size=2>" . substr($week_end, 0, 1) . "</font></center>";
		}
                else{
                    $showthurs = "";
                }
	
                ///box color
		if($value == 3){
			$graph .= "<td height=15 width=15 bgcolor=red>$showthurs</td><td width=1></td>";
		}
		elseif($value == 2){   
                        $showthurs = str_replace("<font size=2>", "<font size=2 color=white>", $showthurs);
			$graph .= "<td height=15 width=15 bgcolor=#444455>$showthurs</td><td width=1></td>";
		}
		elseif($value == 1){
			$graph .= "<td height=15 width=15 bgcolor=#00FF00>$showthurs</td><td width=1></td>";
		}
		elseif($closed1 == 1){
			$graph .= "<td height=15 width=15 bgcolor=#AAAAAA>$showthurs</td><td width=1></td>";
		}
		elseif($value === 0){
			$graph .= "<td height=15 width=15 bgcolor=#00CCFF>$showthurs</td><td width=1></td>";
		}
	}
	
	$graph .= "</tr></table>";

	return $graph;
}

if ( !defined( 'DOC_ROOT' ) ) {
        define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
require_once( dirname(__FILE__) . '/../../application/bootstrap.php' );

require_once( 'lib/class.bus_setup.php' );

$style_hide = null; # Notice: Undefined variable
$showrequest2 = null; # Notice: Undefined variable
$customer_utility = null; # Notice: Undefined variable
$style_hide = null; # Notice: Undefined variable
$style_hide = null; # Notice: Undefined variable
$style_hide = null; # Notice: Undefined variable
$style_hide = null; # Notice: Undefined variable
$style_hide = null; # Notice: Undefined variable

//include_once('db.php');
//require_once('lib/class.db-proxy.php');

//Treat_DB_ProxyOld::$debug = true;
$style = "text-decoration:none";
$style2 = "background-color:#FFFF99";
$day = date( "d" );
$year = date( "Y" );
$month = date( "m" );
$today = "$year-$month-$day";
$todayname = date( "l" );
$bbdayname = date( "l" );
$bbmonth = date( "F" );
$bbday = date( "j" );

$sessionUser = $_SESSION->getUser();
if ( !$sessionUser ) {
	$sessionUser = new \EE\Null();
}
$user = \EE\Controller\Base::getPostVariable( 'username', $sessionUser->username );
$pass = \EE\Controller\Base::getPostVariable( 'password', $sessionUser->password );

//////SQL INJECTION
$user = str_replace( "'", "", $user );
$pass = str_replace( "'", "", $pass );
$user = str_replace( ";", "", $user );
$pass = str_replace( ";", "", $pass );

$view = \EE\Controller\Base::getPostVariable('view','');
$spec = \EE\Controller\Base::getPostVariable('spec','');
$login_goto = \EE\Controller\Base::getPostVariable('login_goto','');
$date1 = \EE\Controller\Base::getSessionCookieVariable('date1cook','');

if( $user == '' && $pass == '' ) {
	$user = \EE\Controller\Base::getSessionCookieVariable('usercook','');
	$pass = \EE\Controller\Base::getSessionCookieVariable('passcook','');
}


$query = "SELECT * FROM login WHERE username = '$user' AND BINARY password = '$pass'";
$result = Treat_DB_ProxyOld::query( $query );
$num = @Treat_DB_ProxyOld::mysql_numrows( $result  );

$companyid = Treat_DB_ProxyOld::mysql_result($result, 0, 'companyid');
if (!empty($_SESSION['oldcompany']) && $_SESSION['oldcompany'] != $companyid) {
	$old = $_SESSION['oldcompany'];
	unset($_SESSION['oldcompany']);
	echo '<html><body onload="form.submit()"><form name="form" method="post" action="changecomp.php"><input type="hidden" name="newcompany" value="'.$old.'" /></form></body></html>';
	exit;
}

$mysecurity = @Treat_DB_ProxyOld::mysql_result( $result, 0, "security" );
$jobtype = @Treat_DB_ProxyOld::mysql_result( $result, 0, "jobtype" );
$loginid = @Treat_DB_ProxyOld::mysql_result( $result, 0, "loginid" );
$temp_expire = @Treat_DB_ProxyOld::mysql_result( $result, 0, "temp_expire" );
$passexpire = @Treat_DB_ProxyOld::mysql_result( $result, 0, "passexpire" );

$busid1 = $bid1 = @Treat_DB_ProxyOld::mysql_result( $result, 0, "businessid" );
$busid2 = $bid2 = @Treat_DB_ProxyOld::mysql_result( $result, 0, "busid2" );
$busid3 = $bid3 = @Treat_DB_ProxyOld::mysql_result( $result, 0, "busid3" );
$busid4 = $bid4 = @Treat_DB_ProxyOld::mysql_result( $result, 0, "busid4" );
$busid5 = $bid5 = @Treat_DB_ProxyOld::mysql_result( $result, 0, "busid5" );
$busid6 = $bid6 = @Treat_DB_ProxyOld::mysql_result( $result, 0, "busid6" );
$busid7 = $bid7 = @Treat_DB_ProxyOld::mysql_result( $result, 0, "busid7" );
$busid8 = $bid8 = @Treat_DB_ProxyOld::mysql_result( $result, 0, "busid8" );
$busid9 = $bid9 = @Treat_DB_ProxyOld::mysql_result( $result, 0, "busid9" );
$busid10 = $bid10 = @Treat_DB_ProxyOld::mysql_result( $result, 0, "busid10" );

$oo8 = @Treat_DB_ProxyOld::mysql_result( $result, 0, "oo8" );

if( isset( $_GET['oo4'] ) ) $oo4 = $_GET['oo4'];

$sqlHierarchy = 'SELECT business_hierarchy FROM company WHERE companyid = '.$companyid;
$resultHierarchy = Treat_DB_ProxyOld::query($sqlHierarchy);
$hierarchy = Treat_DB_ProxyOld::mysql_fetch_row($resultHierarchy);
$hierarchy = $hierarchy[0];
if (!empty($hierarchy) && !isset($oo4)) $oo4 = 2;

setcookie( "userjob", $jobtype );
setcookie( "logincook", $loginid );

# this blacklist_ip code needs to be removed as it is now handled before it reaches the web server
$ip = $_SERVER['REMOTE_ADDR'];
$query33 = "SELECT * FROM blacklist_ip WHERE ip_address LIKE '$ip'";
$result33 = Treat_DB_ProxyOld::query( $query33 );
$num33 = @Treat_DB_ProxyOld::mysql_numrows( $result33  );

if( $num33 > 0 ) {
	$query = "INSERT INTO audit_login (user,pass,ip,redirect) VALUES ('$user','$pass','$ip','1')";
	$result = Treat_DB_ProxyOld::query( $query );

	$location = "http://google.com";
	header( 'Location: $location' );
	exit( );
}
elseif( $num != 1 || ( $user == "" && $pass == "" ) ) {
	echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";

	$ip = $_SERVER['REMOTE_ADDR'];

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "INSERT INTO audit_login (user,pass,ip) VALUES ('$user','$pass','$ip')";
	$result = Treat_DB_ProxyOld::query( $query );
	//mysql_close();
}
elseif( $temp_expire <= $today && $temp_expire != "0000-00-00" ) {
	echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}
elseif( $passexpire <= $today ) {
	setcookie( "usercook", $user );
	setcookie( "passcook", $pass );
	$location = "editpass.php?ex=1&edit=1";
	header( 'Location: ./' . $location );
}
else {

	if( $todayname == "Friday" ) {
		$day1 = $today;
		$lastweek = pastday( 7 );
	}
	elseif( $todayname == "Saturday" ) {
		$day1 = pastday( 1 );
		$lastweek = pastday( 8 );
	}
	elseif( $todayname == "Sunday" ) {
		$day1 = pastday( 2 );
		$lastweek = pastday( 9 );
	}
	elseif( $todayname == "Monday" ) {
		$day1 = pastday( 3 );
		$lastweek = pastday( 10 );
	}
	elseif( $todayname == "Tuesday" ) {
		$day1 = pastday( 4 );
		$lastweek = pastday( 11 );
	}
	elseif( $todayname == "Wednesday" ) {
		$day1 = pastday( 5 );
		$lastweek = pastday( 12 );
	}
	elseif( $todayname == "Thursday" ) {
		$day1 = pastday( 6 );
		$lastweek = pastday( 13 );
	}

	$compcook = @Treat_DB_ProxyOld::mysql_result( $result, 0, "companyid" );
	$buscook = @Treat_DB_ProxyOld::mysql_result( $result, 0, "businessid" );
	$busid1 = $buscook;
	$user = @Treat_DB_ProxyOld::mysql_result( $result, 0, "username" );
	$loginid = @Treat_DB_ProxyOld::mysql_result( $result, 0, "loginid" );
	$mylinks = @Treat_DB_ProxyOld::mysql_result( $result, 0, "mylinks" );
	$myar = @Treat_DB_ProxyOld::mysql_result( $result, 0, "myar" );
	$myap = @Treat_DB_ProxyOld::mysql_result( $result, 0, "myap" );
	$order_only = @Treat_DB_ProxyOld::mysql_result( $result, 0, "order_only" );
	$oo2 = @Treat_DB_ProxyOld::mysql_result( $result, 0, "oo2" );
	$oo3 = @Treat_DB_ProxyOld::mysql_result( $result, 0, "oo3" );
	if( !isset( $_GET["oo4"] ) && !isset($oo4) ) {
		$oo4 = @Treat_DB_ProxyOld::mysql_result( $result, 0, "oo4" );
	}
	$oo5 = @Treat_DB_ProxyOld::mysql_result( $result, 0, "oo5" );
	$oo6 = @Treat_DB_ProxyOld::mysql_result( $result, 0, "oo6" );
	$defaultsub = @Treat_DB_ProxyOld::mysql_result( $result, 0, "viewsub" );
	$oo7 = @Treat_DB_ProxyOld::mysql_result( $result, 0, "oo7" );
	$oo8 = @Treat_DB_ProxyOld::mysql_result( $result, 0, "oo8" );
	$oo9 = @Treat_DB_ProxyOld::mysql_result( $result, 0, "oo9" );
	$oo10 = @Treat_DB_ProxyOld::mysql_result( $result, 0, "oo10" );
	$hide_expense_reports = @Treat_DB_ProxyOld::mysql_result( $result, 0, "hide_expense_reports" );

	///details from login table
	$companyid = @Treat_DB_ProxyOld::mysql_result( $result, 0, "companyid" );
	$businessid = @Treat_DB_ProxyOld::mysql_result( $result, 0, "businessid" );
	$security_level = @Treat_DB_ProxyOld::mysql_result( $result, 0, "security_level" );
	$security = @Treat_DB_ProxyOld::mysql_result( $result, 0, "security" );
	$jobtype = @Treat_DB_ProxyOld::mysql_result( $result, 0, "jobtype" );
	$loginid = @Treat_DB_ProxyOld::mysql_result( $result, 0, "loginid" );


	setcookie( "usercook", $user );
	setcookie( "passcook", $pass );
	setcookie( "logincook", $loginid );
	setcookie( "compcook", $compcook );
	if( $date1 == "" ) {
		setcookie( "date1cook", $day1 );
	}
	setcookie( "caterbus", 0 );

	$query = "SELECT * FROM security WHERE securityid = '$security'";
	$result = Treat_DB_ProxyOld::query( $query );

	$sec_name = @Treat_DB_ProxyOld::mysql_result( $result, 0, "security_name" );

	$sec_multi_comp = @Treat_DB_ProxyOld::mysql_result( $result, 0, "multi_company" );
	$sec_com_setup = @Treat_DB_ProxyOld::mysql_result( $result, 0, "com_setup" );
	$sec_user = @Treat_DB_ProxyOld::mysql_result( $result, 0, "user" );
	$sec_routes = @Treat_DB_ProxyOld::mysql_result( $result, 0, "routes" );
	$sec_invoice = @Treat_DB_ProxyOld::mysql_result( $result, 0, "invoice" );
	$sec_payable = @Treat_DB_ProxyOld::mysql_result( $result, 0, "payable" );
	$sec_item_list = @Treat_DB_ProxyOld::mysql_result( $result, 0, "item_list" );
	$sec_nutrition = @Treat_DB_ProxyOld::mysql_result( $result, 0, "nutrition" );
	$sec_request = @Treat_DB_ProxyOld::mysql_result( $result, 0, "request" );
	$sec_export = @Treat_DB_ProxyOld::mysql_result( $result, 0, "export" );
	$sec_utility = @Treat_DB_ProxyOld::mysql_result( $result, 0, "utility" );
	$sec_kpi = @Treat_DB_ProxyOld::mysql_result( $result, 0, "kpi" );
	$sec_kpi_graph = @Treat_DB_ProxyOld::mysql_result( $result, 0, "kpi_graph" );
	$sec_labor = @Treat_DB_ProxyOld::mysql_result( $result, 0, "labor" );
	$sec_reconcile = @Treat_DB_ProxyOld::mysql_result( $result, 0, "reconcile" );
	$sec_emp_labor = @Treat_DB_ProxyOld::mysql_result( $result, 0, "emp_labor" );
	$sec_project_manager = @Treat_DB_ProxyOld::mysql_result( $result, 0, "project_manager" );
	$sec_budget_calc = @Treat_DB_ProxyOld::mysql_result( $result, 0, "budget_calc" );
	$sec_rebate = @Treat_DB_ProxyOld::mysql_result( $result, 0, "rebate" );
	$sec_transfer = @Treat_DB_ProxyOld::mysql_result( $result, 0, "transfer" );
	$sec_reporting = @Treat_DB_ProxyOld::mysql_result( $result, 0, "reporting" );
	$sec_calendar = @Treat_DB_ProxyOld::mysql_result( $result, 0, "calendar" );
	$sec_faq = @Treat_DB_ProxyOld::mysql_result( $result, 0, "faq" );
	$sec_comrecipe = @Treat_DB_ProxyOld::mysql_result( $result, 0, "comrecipe" );
	$sec_comdirectory = @Treat_DB_ProxyOld::mysql_result( $result, 0, "comdirectory" );
	$sec_messages = @Treat_DB_ProxyOld::mysql_result( $result, 0, "messages" );
	$sec_dashboard = @Treat_DB_ProxyOld::mysql_result( $result, 0, "dashboard" );
	$sec_setup_mgr = @Treat_DB_ProxyOld::mysql_result( $result, 0, "setup_mgr" );
	$sec_customer_utility = @Treat_DB_ProxyOld::mysql_result( $result, 0, "customer_utility" );

	$sec_bus_sales = @Treat_DB_ProxyOld::mysql_result( $result, 0, "bus_sales" );
	$sec_bus_invoice = @Treat_DB_ProxyOld::mysql_result( $result, 0, "bus_invoice" );
	$sec_bus_order = @Treat_DB_ProxyOld::mysql_result( $result, 0, "bus_order" );
	$sec_bus_payable = @Treat_DB_ProxyOld::mysql_result( $result, 0, "bus_payable" );
	$sec_bus_payable2 = @Treat_DB_ProxyOld::mysql_result( $result, 0, "bus_payable2" );
	$sec_bus_inventor1 = @Treat_DB_ProxyOld::mysql_result( $result, 0, "bus_inventor1" );
	$sec_bus_control = @Treat_DB_ProxyOld::mysql_result( $result, 0, "bus_control" );
	$sec_bus_menu = @Treat_DB_ProxyOld::mysql_result( $result, 0, "bus_menu" );
	$sec_bus_order_setup = @Treat_DB_ProxyOld::mysql_result( $result, 0, "bus_order_setup" );
	$sec_bus_request = @Treat_DB_ProxyOld::mysql_result( $result, 0, "bus_request" );
	$sec_route_collection = @Treat_DB_ProxyOld::mysql_result( $result, 0, "route_collection" );
	$sec_route_labor = @Treat_DB_ProxyOld::mysql_result( $result, 0, "route_labor" );
	$sec_route_detail = @Treat_DB_ProxyOld::mysql_result( $result, 0, "route_detail" );
	$sec_bus_labor = @Treat_DB_ProxyOld::mysql_result( $result, 0, "bus_labor" );
	$sec_bus_timeclock = @Treat_DB_ProxyOld::mysql_result( $result, 0, "bus_timeclock" );	
	$sec_bus_promo = @Treat_DB_ProxyOld::mysql_result( $result, 0, "bus_promo_manager" );
	
	$sec_kiosk_menu_mgmt = @Treat_DB_ProxyOld::mysql_result( $result, 0, "bus_kiosk_menu_mgmt" );
	$sec_kiosk_route_mgmt = @Treat_DB_ProxyOld::mysql_result( $result, 0, "bus_kiosk_route_mgmt" );

	$viewsub = empty( $_GET['viewsub'] ) ? $defaultsub : $_GET['viewsub'];
	$viewsub = $sec_dashboard == 2 ? 10 : $viewsub;

	/*
	if(checkmobile()==true){
		$location="mobile/index.php";
		header('Location: ./' . $location);
	}*/
	if( $login_goto == 1 ) {

		$query22 = "SELECT * FROM security WHERE securityid = '$mysecurity'";
		$result22 = Treat_DB_ProxyOld::query( $query22 );

		$sec_bus_payable2 = @Treat_DB_ProxyOld::mysql_result( $result22, 0, "bus_payable2" );

		if( $sec_bus_payable2 > 0 ) {

			$query22 = "SELECT * FROM purchase_card WHERE loginid = '$loginid' AND type = '0' AND is_deleted = '0'";
			$result22 = Treat_DB_ProxyOld::query( $query22 );

			$er_busid = @Treat_DB_ProxyOld::mysql_result( $result22, 0, "businessid" );
			$er_comid = @Treat_DB_ProxyOld::mysql_result( $result22, 0, "companyid" );

			if( $er_busid > 0 ) {

				$query22 = "UPDATE login SET companyid = '$er_comid' WHERE loginid = '$loginid'";
				$result22 = Treat_DB_ProxyOld::query( $query22 );

				$location = "busapinvoice2.php?cid=$er_comid&bid=$er_busid";
				header( 'Location: ./' . $location );
				exit( );
			}
			else {
				$location = "/";
				header( 'Location: ' . $location );
				exit( );
			}
		}
		else {
			$location = "/";
			header( 'Location: ' . $location );
			exit( );
		}
	}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Essential Elements :: Treat America</title>

<style type="text/css">
<?php if( $loginid == '53' && date( 'm-d' ) == '04-01' && isset($_GET['hahalolaprilfools']) ) : ?>
body {
	-webkit-transform: rotate(-180deg);
	-moz-transform: rotate(-180deg);
	filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=2);
}
<?php endif; ?>
</STYLE>
<link rel="stylesheet" href="../assets/css/preload/preLoadingMessage.css" />
<script language="JavaScript"
	type="text/JavaScript">

function changePage(newLoc)
{
	nextPage = newLoc.options[newLoc.selectedIndex].value

	if (nextPage != "")
	{
			document.location.href = nextPage
	}
}

var update_expense = function(x,oo4){

		new Ajax.Request('ajax_expense.php', {parameters: {method: 'ajax_expense1', lid: x}});

								setTimeout( "document.getElementById('bboard').contentWindow.location.reload();", 10000);

								if(oo4 == 1){
			$('sales_ag').style.visibility = 'visible';
			$('sales_day').style.visibility = 'visible';
												$('custom').style.visibility = 'custom';
		}
};

function get_cookie ( cookie_name )
{
	var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );

	if ( results )
		return ( unescape ( results[2] ) );
	else
		return null;
}

function hide_show_units(districtid){
		var img = $('img'+districtid);
	var ex = img.src.split('/');
	var src = ex[ex.length-1];
	if(src == "exp.gif"){
		img.src="/ta/exp2.gif";
		img.alt="Show";
		var rows = $$('.d' + districtid);
		rows.each(function(row){
			row.hide();
		});

		document.cookie =
			'hide' + districtid + '=1; expires=Fri, 11 Dec 2037 20:47:11 UTC; path=/';


	}
	else{
		img.src="/ta/exp.gif";
		img.alt="Hide";
		var rows = $$('.d' + districtid);
		rows.each(function(row){
			row.show();
		});

		document.cookie =
			'hide' + districtid + '=0; expires=Fri, 11 Dec 2037 20:47:11 UTC; path=/'
	}
}

function hide_show_district(districtid){
		var img = $('imgbiz'+districtid);
	var ex = img.src.split('/');
	var src = ex[ex.length-1];
	if(src == "arrowup.gif"){
		img.src="/ta/arrowdown.gif";
		img.alt="Show";
		$('biz'+districtid).hide();
	}
	else{
		img.src="/ta/arrowup.gif";
		img.alt="Hide";
		$('biz'+districtid).show();
	}
}
</script>

<script language="JavaScript" type="text/javascript">

function disableForm(theform) {
if (document.all || document.getElementById) {
for (i = 0; i < theform.length; i++) {
var tempobj = theform.elements[i];
if (tempobj.type.toLowerCase() == "submit" || tempobj.type.toLowerCase() == "reset")
tempobj.disabled = true;
}

}
else {
alert("The form has been submitted.  But, since you're not using IE 4+ or NS 6, the submit button was not disabled on form submission.");
return false;
	}
}
//  End -->

</script>
<link rel="stylesheet" type="text/css" href="../assets/css/jq_themes/redmond/jquery-ui-1.8.11.custom.css" />
<link rel="stylesheet" rev="stylesheet" type="text/css" href="css/uploadify.css" />
<?php if( !( $oo4 == 1 && ( $viewsub == 10 ) ) ) { ?>
<script type="text/javascript" src="javascripts/prototype.js"></script>
<script type="text/javascript" src="javascripts/scriptaculous.js"></script>
<?php } ?>
<script type="text/javascript" src="../assets/js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="../assets/js/jquery-ui-1.8.11.custom.min.js"></script>
<script type="text/javascript" src="javascripts/jquery.uploadify.min.js"></script>
<script type="text/javascript" src="javascripts/swfobject.js"></script>
<?php if( !( $oo4 == 1 && ( $viewsub == 10 ) ) ) { ?>
<script type="text/javascript">
	$J = jQuery.noConflict();

	$J( function(){
		$J('#uploadify').uploadify( {
			uploader: 'javascripts/uploadify.swf',
			folder: '<?=HTTP_DASHBOARD_FILES ?>/new',
			script: 'dashboard_files.php',
			cancelImg: 'images/cancel.png',
			multi: true
		} );
	} );
</script>
<script language="javascript" type="text/javascript">

var divexpand=function(){



	//init();

	var newheight = document.getElementById("thermmonth").style.height;
	document.getElementById("thermmonth").style.height='0px';
	document.getElementById("thermmonth").style.background = '#ffffff';

	new Effect.Morph('thermmonth', {
			style: {
				background: '#414141',
				color: '#414141',
				height: newheight
			},
			duration: 3.0
	});

	var newheight = document.getElementById("thermyear").style.height;
	document.getElementById("thermyear").style.height='0px';
	document.getElementById("thermyear").style.background = '#ffffff';

	new Effect.Morph('thermyear', {
			style: {
				background: '#414141',
				color: '#414141',
				height: newheight
			},
			duration: 4.0
	});

	var newvalue = document.getElementById("thermyear_pcnt").innerHTML;
	newvalue = newvalue.replace("&nbsp;","");
	newvalue = newvalue.replace("&nbsp;","");
	newvalue = newvalue.replace("&nbsp;","");
	newvalue = newvalue.replace("&nbsp;","");
	newvalue = newvalue*1;

	if(newvalue >= 100){var mycolor = "#00FF00";}
	else if(newvalue >= 75){var mycolor = "#FF0000";}
	else if(newvalue >= 50){var mycolor = "#FF9900";}
	else if(newvalue >= 25){var mycolor = "#FFFF00";}
	else{var mycolor = "#FFFFFF";}

	new Effect.Morph('thermyear_pcnt', {
			style: {
				color: mycolor
			},
			duration: 6.0
	});

	var newvalue = document.getElementById("thermmonth_pcnt").innerHTML;
	newvalue = newvalue.replace("&nbsp;","");
	newvalue = newvalue.replace("&nbsp;","");
	newvalue = newvalue.replace("&nbsp;","");
	newvalue = newvalue.replace("&nbsp;","");
	newvalue = newvalue*1;

	if(newvalue >= 100){var mycolor = "#00FF00";}
	else if(newvalue >= 75){var mycolor = "#FF0000";}
	else if(newvalue >= 50){var mycolor = "#FF9900";}
	else if(newvalue >= 25){var mycolor = "#FFFF00";}
	else{var mycolor = "#FFFFFF";}

	new Effect.Morph('thermmonth_pcnt', {
			style: {
				color: mycolor
			},
			duration: 5.0
	});

};


var grabScreenPOS = function(){
	if($('reportFloater')){ //Make sure DIV exists on page on page
		var b = $('reportFloater');                    //The Div Tag
		var bh = b.getHeight();                        //Div Height
		var wh  = $('divbox').getHeight();             //Window Height: Hard Coded to avoid IE Bug
		var co = document.viewport.getScrollOffsets(); //Array of scroll offsets
		var sh = co[1];                                //ScrollTop

		var n = (wh / 2); //Grab 1/2 Window height
		n -= (bh / 2);    //Substract 1/2 Box height  [This gets it centered]
		n += sh;          //Add the scroll offset [Make it follow scrollbar]

		return n;
	}
};

var followScroll = function(){
	if($('reportFloater')){ //Make sure DIV exists on page on page
		var b = $('reportFloater');                    //The Div Tag

		var n = grabScreenPOS();
		b.style.top = n +'px'; //Set the new top value to you're div
	}
};
var initScrollBox = function(){
	if($('reportFloater')){ //Make sure DIV exists on page on page
		var b = $('reportFloater');                    //The Div Tag

		var n = grabScreenPOS();
		b.style.top = n +'px'; //Set the new top value to you're div

		if(!b.visible()) b.show(); //If display:none => set display: block  [Gets rid of jump onLoad]
	}
};


var showUpdate = function(link){
	var l = $(link);
	var p = l.positionedOffset();

	$('mailStatus').update('Retrieving Emails...');
	$('mailStatus').style.background = '#fff';
	$('mailStatus').style.left = p[0] + $('mailStatus').getWidth() + 'px';
	$('mailStatus').style.top = p[1] + 'px';
	$('mailStatus').show();
};

var clickHandler = function(e){
	var ev = e || window.event;

	if(ev.shiftKey){
		if(this.href.search(/busdetail/) > -1){
			showUpdate(this);
			new Ajax.Request('ajaxreq.php', {parameters: {method: 'btEmailLink', h: this.href}});
			return false;
		}
	}

	return true;
};

var bindShiftClickHandler = function(){
	var links = $('mainTable').getElementsByTagName('a');

	for(i = 0; i < links.length; i++){
		var l = links[i];
		l.onclick = clickHandler;
	}
};


Event.observe(window,'load', bindShiftClickHandler);


Event.observe(window,'load', divexpand);
Event.observe(window,'load', initScrollBox); //Make the box follow on page load
Event.observe(window,'scroll', followScroll); //Make the box follow on page scroll

Event.observe($('dashboardaccounts'),'load', function(){ alert('test');});



</script>
<?php } ?>

<script type="text/javascript" src="/assets/js/preload/preLoadingMessage2.js"></script>
<script type="text/javascript">
	function show_setup(bid){
		$('setup_list').show();
		document.getElementById('setup_frame').src='setup.php?bid=' + bid;
	}
</script>

	</head>
<?php

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query5 = "SELECT * FROM login_pend WHERE companyid = '$compcook'";
	$result5 = Treat_DB_ProxyOld::query( $query5 );
	$num5 = @Treat_DB_ProxyOld::mysql_numrows( $result5  );
	//mysql_close();

	$laborrequest = $num5;

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query5 = "SELECT * FROM accountpend WHERE companyid = '$compcook'";
	$result5 = Treat_DB_ProxyOld::query( $query5 );
	$num5 = @Treat_DB_ProxyOld::mysql_numrows( $result5  );
	//mysql_close();

	$requestcount = $num5;

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query5 = "SELECT * FROM vendorpend WHERE companyid = '$compcook'";
	$result5 = Treat_DB_ProxyOld::query( $query5 );
	$num5 = @Treat_DB_ProxyOld::mysql_numrows( $result5  );
	//mysql_close();

	$requestcount = $requestcount + $num5;

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query5 = "SELECT * FROM apinvoice WHERE companyid = '$compcook' AND posted = '2' AND (transfer = '1' OR transfer = '2')";
	$result5 = Treat_DB_ProxyOld::query( $query5 );
	$num5 = @Treat_DB_ProxyOld::mysql_numrows( $result5  );
	//mysql_close();

	$requestcount2 = $num5;

	if( $laborrequest != 0 ) {
		$showlaborrequest = "($laborrequest)";
		$showstyle1 = "background-color:yellow";
	}
	else {
		$showstyle1 = "background-color:white";
	}
	if( $requestcount != 0 ) {
		$showrequest = "($requestcount)";
		$showstyle2 = "background-color:yellow";
	}
	else {
		$showstyle2 = "background-color:white";
	}
	if( $requestcount2 != 0 ) {
		$showrequest2 = "($requestcount2)";
		$showstyle3 = "background-color:yellow";
	}
	else {
		$showstyle3 = "background-color:white";
	}

	$mystyle1 = "background-color:#E7E7E7";

	$recent = $today;
	for( $counter = 1; $counter <= 3; $counter++ ) {
		$recent = prevday( $recent );
	}

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query5 = "SELECT * FROM bboard WHERE date >= '$recent'";
	$result5 = Treat_DB_ProxyOld::query( $query5 );
	$num5 = @Treat_DB_ProxyOld::mysql_numrows( $result5  );
	//mysql_close();

	if( $num5 == 1 ) {
		$showfaq = "($num5 Update)";
	}
	elseif( $num5 > 1 ) {
		$showfaq = "($num5 Updates)";
	}
	else {
		$showfaq = "";
	}

	if( $hide_expense_reports == 1 ) {
		$query35 = "SELECT apinvoice.* FROM apinvoice,purchase_card WHERE (apinvoice.dm = '' OR apinvoice.dm = '-1') AND apinvoice.transfer = '3' AND apinvoice.businessid = purchase_card.businessid AND apinvoice.vendor = purchase_card.loginid AND purchase_card.is_deleted = '0' AND apinvoice.pc_type = purchase_card.type AND purchase_card.approve_loginid = '$loginid' AND posted != '0' ORDER BY apinvoice.date";
	}
	else {
		$query35 = "SELECT apinvoice.* FROM apinvoice,purchase_card WHERE apinvoice.dm = '' AND apinvoice.transfer = '3' AND apinvoice.businessid = purchase_card.businessid AND apinvoice.vendor = purchase_card.loginid AND apinvoice.pc_type = purchase_card.type AND purchase_card.approve_loginid = '$loginid' AND purchase_card.is_deleted = '0' AND posted != '0' ORDER BY apinvoice.date";
	}
	$result35 = Treat_DB_ProxyOld::query( $query35 );
	$num35 = @Treat_DB_ProxyOld::mysql_numrows( $result35  );

	$query36 = "SELECT apinvoice.* FROM apinvoice,business WHERE apinvoice.dm = '' AND apinvoice.transfer = '4' AND apinvoice.posted = '2' AND apinvoice.businessid = business.businessid AND business.po_approval = '$loginid'";
	$result36 = Treat_DB_ProxyOld::query( $query36 );
	$num36 = @Treat_DB_ProxyOld::mysql_numrows( $result36  );

	$query37 = "SELECT apinvoice.* FROM apinvoice,business WHERE apinvoice.dm != '' AND apinvoice.dm != '-1' AND apinvoice.transfer = '4' AND apinvoice.posted = '1' AND apinvoice.pc_type = '0' AND apinvoice.businessid = business.businessid AND business.po_notify_email = '$loginid'";
	$result37 = Treat_DB_ProxyOld::query( $query37 );
	$num37 = @Treat_DB_ProxyOld::mysql_numrows( $result37  );

	$query43 = "SELECT * FROM company WHERE companyid = '$companyid'";
	$result43 = Treat_DB_ProxyOld::query( $query43 );

	$companyname = @Treat_DB_ProxyOld::mysql_result( $result43, 0, "companyname" );
	$weekend = @Treat_DB_ProxyOld::mysql_result( $result43, 0, "week_end" );
	$com_logo = @Treat_DB_ProxyOld::mysql_result( $result43, 0, "logo" );
	$com_backdrop = @Treat_DB_ProxyOld::mysql_result( $result43, 0, "backdrop" );

	//////default logos
	if( $com_logo == "" ) {
		$com_logo = "talogo.gif";
	}
	if( $com_backdrop == "" ) {
		$com_backdrop = "backdrop.jpg";
	}

?>
	<body style="margin: 0 auto; text-align: left;">
		<center>
			<table cellspacing="0" cellpadding="0" border="0" width="90%" style="margin: 0 auto; text-align: left;">
				<tr>
					<td colspan="4">
						<a href="/ta/logout.php" onclick="return confirm('Logout?');">
							<img src="/assets/images/logo.jpg" height="43" width="205" border="0" alt="logo" />
						</a>
						<p></p>
					</td>
					<td align="right">
<?php
	if( checkmobile( ) == true ) {
		echo "<a href=mobile/index.php>Mobile Version</a>";
	}
	echo "</td></tr></table>";
	echo '<div id="mailStatus" style="display: none; position: absolute; border: 1px solid black; background: #fff; padding: 2px;">&nbsp;</div>';


	echo "<div id='divbox' style=\"position: absolute; height: 100%; visibility: hidden;\">&nbsp;</div>";

	////////////////////////////////////
	/////////////EXPENSE REPORTS////////
	////////////////////////////////////
	$showgraph = "block";
	if( ( $num35 > 0 && $hide_expense_reports == 0 ) || $num36 > 0 || $num37 > 0 ) {
		echo "<div id=\"reportFloater\" style=\"display: none; position: absolute; width: 100%; left: 0px; top: 100px; text-align: center; z-index: 1000;\">";

		if( $num35 > 0 && $num36 == 0 ) {
			$reportname = "Expense Reports";
		}
		elseif( $num35 == 0 && $num36 > 0 ) {
			$reportname = "Purchase Orders";
		}
		elseif( $num35 > 0 && $num36 > 0 ) {
			$reportname = "Expense Reports/Purchase Orders";
		}
		else {
			$reportname = "Purchase Orders";
		}

		echo "<center><table width=80% cellspacing=0 cellpadding=0 style=\"border: 2px solid black;\" bgcolor=white>";
		echo "<tr bgcolor=black height=16><td colspan=4><center><font color=white><b>$reportname</b></font></center></td></tr>";
		echo "<tr><td colspan=4>";

		echo "<iframe src='expense_report.php?lid=$loginid' height=500 width=100% frameborder=0></iframe>";

		echo "</td></tr>";
		echo "<tr><td colspan=\"4\" align=\"right\" style=\"font-size: 14px;\"><a href=\"javascript: void(0);\" onclick=\";$('reportFloater').hide();update_expense($loginid,$oo4)\">Hide</a></td></tr>";
		//echo '<tr><td colspan="4" align="right" style="font-size: 14px;"><a href="javascript: void(0);" onclick="update_expense();$(\'reportFloater\').hide();">Hide</a></td></tr>';
		echo "</table></center>";

		echo "</div>";
		$showgraph = "hidden";
	}
	/////////////////////////////////////

	////////////////////////////
	////setup list
	////////////////////////////
	echo "<div id='setup_list' style=\"display:none;position:fixed;top:6%;left:6%;right:6%;border:3px solid #999999;background:#FFFFFF;z-index:100;\"><div style=\"width:100%;background:#999999;color:#FFFFFF;\"><center><b>Setup Check List</b></center></div><iframe name='setup_frame' id='setup_frame' src=setup.php?bid=0 width=100% height=525></iframe></div>";

	if( $sec_dashboard < 1 ) {
		$showdash = "";
	}
	elseif( isset( $oo4 ) && ($oo4 == 1 || $oo4 ==2 )) {
		if ($oo4 == 1 && !empty($hierarchy)) {
			$showdash = "<a href=businesstrack.php?oo4=2 style=$style><b><font color=white onMouseOver=this.color='#FF9900' onMouseOut=this.color='white' title='Click to Change'>Sales Hierarchy</font></a>";
		} else {
			$showdash = "<a href=businesstrack.php?oo4=0 style=$style><b><font color=white onMouseOver=this.color='#FF9900' onMouseOut=this.color='white' title='Click to Change'>Units</font></a>";
		}
	}
	elseif( $security_level > 0 ) {
		$showdash = "<a href=businesstrack.php?oo4=1 style=$style><b><font color=white onMouseOver=this.color='#FF9900' onMouseOut=this.color='white' title='Click to Change'>Dashboard</font></a>";
	}


	echo "<center><table width=90% style=\"border:2px solid #CCCCFF;background-image: url(logo/$com_backdrop);background-repeat: no-repeat; margin: 0 auto; text-align: left;\" cellspacing=0 cellpadding=0>";
	echo "<tr><td colspan=3 bgcolor=#CCCCFF><a name=db><b><font color=white>Main Page</font></a></td><td colspan=2 bgcolor=#CCCCFF align=right>$showdash</td></tr>";
	if( $sec_multi_comp == 1 ) {
		echo "<tr valign=top><td width=10% id=\"t_talogo\"><img src=\"logo/$com_logo\"></td><td colspan=2 width=30%><font size=4 title='$sec_name'><b>Welcome $user</font> <a href=editpass.php><img src=edit.gif border=0 alt='Edit Your Password'></a><br>";
		echo "<form action=/ta/changecomp.php method=post onSubmit='return disableForm(this);'><input type=hidden name=loginid value=$loginid><select name=newcompany>";

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query = "SELECT * FROM company ORDER BY reference DESC";
		$result = Treat_DB_ProxyOld::query( $query );
		$num = @Treat_DB_ProxyOld::mysql_numrows( $result  );
		//mysql_close();

		$num--;
		while( $num >= 0 ) {
			$newcompanyname = @Treat_DB_ProxyOld::mysql_result( $result, $num, "companyname" );
			$reference = @Treat_DB_ProxyOld::mysql_result( $result, $num, "reference" );
			$newcompanyid = @Treat_DB_ProxyOld::mysql_result( $result, $num, "companyid" );
			if( $companyid == $newcompanyid ) {
				$showcomp = "SELECTED";
			}
			else {
				$showcomp = "";
			}

			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query27 = "SELECT * FROM login_company WHERE companyid = '$newcompanyid' AND loginid = '$loginid'";
			$result27 = Treat_DB_ProxyOld::query( $query27 );
			$num27 = @Treat_DB_ProxyOld::mysql_numrows( $result27  );
			//mysql_close();

			if( $num27 != 0 ) {
				echo "<option value=$newcompanyid $showcomp>$reference</option>";
			}
			$num--;
		}

		echo "<input type=submit value='GO'></b></font></form><p>";
	}
	else {
		echo "<tr valign=top><td width=10%><img src=logo/$com_logo></td><td colspan=2 width=30%><font size=4 title='$sec_name'><b>Welcome $user </font><a href=editpass.php><img src=edit.gif border=0 alt='Edit Your Password'></a><p><font size=4> $companyname</b> </font><p>";
	}


?>
						<form action="/ta/direct.php" method="post">
							<input type="hidden" name="username" value="<?php echo $user; ?>" />
							<input type="hidden" name="password" value="<?php echo $pass; ?>" />
							<input type="hidden" name="businessid" value="<?php echo $businessid; ?>" />
							<input type="hidden" name="companyid" value="<?php echo $companyid; ?>" />
<?php
	$businessid = ( isset( $businessid ) ) ? $businessid : '';
	$cid = ( isset( $cid ) ) ? $cid : '';
?>

							<select name="view" onChange="changePage(this.form.view)">
								<option value="/ta/businesstrack.php?bid=<?php
									echo $businessid;
								?>&amp;companyid=<?php
									echo $cid;
								?>" style="background: url('/assets/images/ta/home.gif') no-repeat; padding-left: 20px;">Main Page</option>
<?php

	if( $sec_com_setup == 1 || $sec_user == 1 || $sec_item_list == 1 ) {
		echo "<optgroup label=Settings style=$mystyle1>";
	}
	if( $sec_com_setup == 1 ) {
		echo "<option value=comsetup.php?bid=$businessid&cid=$companyid style=$mystyle1>Company Setup</option>";
	}
	if( $sec_user == 1 ) {
		echo "<option value=user_setup.php?bid=$businessid&cid=$companyid style=$mystyle1>Users/Security</option>";
	}
	if( $sec_routes == 1 ) {
		echo "<option value=user_routes.php?bid=$businessid&cid=$companyid style=$mystyle1>Vending Setup</option>";
	}
	if( $sec_item_list == 1 || $sec_nutrition == 1 ) {
		echo "<option value=catermenu.php?cid=$companyid style=$mystyle1>Menu Management</option>";
	}
	if( $sec_project_manager == 1 || $sec_com_setup == 1 || $sec_user == 1 || $sec_emp_labor == 1
			|| $sec_item_list == 1 ) {
		echo "</optgroup>";
	}

	if( $sec_reconcile == 1 || $sec_request == 1 || $sec_transfer == 1 || $sec_emp_labor == 1
			|| $sec_invoice == 1 || $sec_payable == 1 || $sec_setup_mgr > 0 || $customer_utility > 0 ) {
		echo "<optgroup label=Maintenance>";
	}
	if( $sec_reconcile == 1 ) {
		echo "<option value=reconcile.php?cid=$companyid>Reconcile</option>";
	}
	$showlaborrequest = ( isset( $showlaborrequest ) ) ? $showlaborrequest : '';
	if( $sec_emp_labor == 1 ) {
		echo "<option value=emp_labor.php?bid=$businessid&cid=$companyid style=$showstyle1>Employee Labor $showlaborrequest</option>";
	}
	$showrequest = ( isset( $showrequest ) ) ? $showrequest : '';
	if( $sec_request == 1 ) {
		echo "<option value=requests.php?cid=$companyid style=$showstyle2>Requests $showrequest</option>";
	}
	if( $sec_transfer == 1 ) {
		echo "<option value=comtransfer.php?cid=$companyid style=$showstyle3>Transfers/Purchase Cards $showrequest2</option>";
	}
	if( $sec_invoice == 1 ) {
		echo "<option value=invoice.php?cid=$companyid>Invoicing</option>";
	}
	if( $sec_payable == 1 ) {
		echo "<option value=comapinvoice.php?cid=$companyid>Payables</option>";
	}
	if( $sec_customer_utility > 0 ) {
		echo "<option value=customerutilities?cid=$companyid>Customer Utilities</option>";
	}
	
	//echo '<option value="promotions">Global Promotion Manager</option>';
	if( $sec_bus_promo > 0 ) {
		echo "<option value=promotions>Global Promotion Manager</option>";
	}
	
	
	if (
#		$sec_kiosk_menu_mgmt > 0 || $sec_kiosk_route_mgmt > 0
		$sessionUser->kiosk_route_mgmt
		|| $sessionUser->kiosk_menu_mgmt
		|| $sessionUser->kiosk_kiosk_mgmt
	) {
?>
		<option value="/ta/kioskutilities?cid=<?php echo $companyid; ?>">Kiosk Utilities</option>
<?php
	}
	
	
	
	
	if( $sec_setup_mgr > 0 ) {
		echo "<option value=setup_mgr.php>Setup Manager</option>";
	}
	if( $sec_reconcile == 1 || $sec_request == 1 || $sec_transfer == 1 || $sec_invoice == 1
			|| $sec_payable == 1 || $sec_setup_mgr > 0 || $customer_utility > 0 ) {
		echo "</optgroup>";
	}

?>
<?php
	echo "<optgroup label=Reports style=$mystyle1>";
?>
<?php
	if( $sec_labor == 1 ) {
		echo "<option value=comlabor.php?cid=$companyid style=$mystyle1>Labor Variance</option>";
	}
	if( $sec_kpi == 1 ) {
		echo "<option value='" . HTTP_COMKPI . "/comkpi.htm' style=$mystyle1>Company KPI</option>";
	}
	if( $sec_kpi_graph == 1 ) {
		echo "<option value=combudgetreport.php?cid=$companyid style=$mystyle1>Budget Report</option>";
	}
	if( $sec_reporting > 0 ) {
		echo "<option value=comreport.php?cid=$companyid style=$mystyle1>Reporting</option>";
	}
	if( $sec_rebate > 0 ) {
		echo "<option value=rebate.php style=$mystyle1>Rebates</option>";
	}
	if( $sec_reporting > 0 ) {
?>
							<option value="/ta/report?cid=<?php
								echo $companyid;
							?>" style="<?php
								echo $mystyle1;
							?>" >POS Reports</option>
<?php
	}
?>
<?php
	echo "</optgroup>";
?>
<?php

	echo "<optgroup label=Misc>";
	if( $sec_calendar > 0 ) {
		echo "<option value=calender.php?bid=$businessid&cid=$companyid>Calendar</option>";
	}
	if( $sec_faq > 0 ) {
		echo "\n<option value=bboard.php?cid=$companyid>FAQ's $showfaq</option>\n";
	}
	if( $sec_comrecipe > 0 ) {
		echo "<option value=comrecipe.php?cid=$companyid>Recipes</option>";
	}
	//if( $sec_xxxx > 0 ) {
		echo "<option value='/ta/functionPanels'>Function Panels</option>\n";
	//}
	echo "<option value=/menu?bid=143>Nutrition Demo Site</option>";
	if( $sec_comdirectory > 0 ) {
		echo "<option value=comdirectory.php?cid=$companyid>Company Directory</option>";
	}
	if( $sec_messages > 0 ) {
		echo "<option value=message.php?bid=$businessid&cid=$companyid>Message Center</option>";
	}
	echo "</optgroup>";

	if( $sec_project_manager > 0 || $sec_utility == 1 || $sec_export == 1 || $oo8 > 0 ) {
		echo "<optgroup label=Admin style=$mystyle1>";
	}
	if( $sec_project_manager > 0 ) {
		echo "<option value=http://{$_SERVER['SERVER_NAME']}/projects/gopm.php?goto=$sec_project_manager style=$mystyle1>BT Project Manager</option>";
	}
	if( $sec_budget_calc == 1 ) {
		echo "<option value=budget_calc.php style=$mystyle1>Budget Calculator</option>";
	}
	if( $sec_utility == 1 ) {
		echo "<option value=utility.php?cid=$companyid style=$mystyle1>Utilities</option>";
	}
	if( $sec_export == 1 ) {
		echo "<option value=exportdetail.php?cid=$companyid style=$mystyle1>Exports</option>";
	}
	if( $oo8 > 0 ) {
		echo "<option value=../projects/pm_list.php?loginid=$loginid style=$mystyle1>EPM</option>";
	}
	if( $sec_utility == 1 || $sec_export == 1 ) {
		echo "</optgroup>";
	}

?>

							</select>
						</form>
<?php
	echo "</td><td colspan=2 bgcolor=#CCCCFF width=60%><table border=0 width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr><td bgcolor=#E8E7E7><center><a href='bulletinboard.php?cid=$companyid&bid=$businessid' target='_blank' style=$style><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'><b>$bbdayname $bbmonth $bbday, $year</b></font></a></center></td></tr><tr bgcolor=white><td>";
	$iframeUrl = "bulletinboard.php?cid=$companyid&bid=$businessid";
	echo "\n<iframe src='$iframeUrl' WIDTH=100% HEIGHT=100 frameborder=0 id='bboard'></iframe>\n</td></tr></table>";
	echo "</td></tr></table><p>";

	if ($oo4 == 2 && !empty($hierarchy)) {
			require('hierarchy-sales.php');
	} elseif( $oo4 != 1 ) {
		echo "<center><table id=\"mainTable\" width=90% cellspacing=0 cellpadding=0 style=\"margin: 0 auto; text-align: left;\">";

		//unit mgr
		if( $security_level == 1 && $businessid != 0 ) {

			$startday = $today;
			$weekday = date( 'l' );
			$thurs1 = 0;
			$thurs2 = 0;
			for( $counter = 1; $counter <= 14; $counter++ ) {
				$startday = prevday( $startday );
				if( $weekday == "Sunday" ) {
					$weekday = "Saturday";
				}
				elseif( $weekday == "Monday" ) {
					$weekday = "Sunday";
				}
				elseif( $weekday == "Tuesday" ) {
					$weekday = "Monday";
				}
				elseif( $weekday == "Wednesday" ) {
					$weekday = "Tuesday";
				}
				elseif( $weekday == "Thursday" ) {
					$weekday = "Wednesday";
				}
				elseif( $weekday == "Friday" ) {
					$weekday = "Thursday";
				}
				elseif( $weekday == "Saturday" ) {
					$weekday = "Friday";
				}
			}
			$weekday2 = $weekday;
			$startday2 = $startday;

			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query = "SELECT * FROM business WHERE companyid = '$companyid' AND businessid = '$businessid'";
			$result = Treat_DB_ProxyOld::query( $query );
			$num = @Treat_DB_ProxyOld::mysql_numrows( $result  );
			//mysql_close();

			echo "<tr bgcolor=#E8E7E7><td><b><i>Name</td><td><b><i>Unit#</td><td><b><i>Menu</b></i></td><td align=right colspan=2><table><tr><td>Exported</td><td width=15 height=15 bgcolor=#444455></td><td>Submitted</td><td height=15 width=15 bgcolor=#00FF00></td><td>Pending</td><td height=15 width=15 bgcolor=#00CCFF></td><td>Closed</td><td height=15 width=15 bgcolor=red><font color=white><center>C</center></font></td></tr></table></td></tr>";
			echo "<tr bgcolor=black><td height=1 colspan=5></td></tr>";
			echo "<tr bgcolor=#CCCCCC><td height=1 colspan=5></td></tr>";

			$buscount = 0;
			$subcount1 = 0;
			$subcount2 = 0;
			$num--;
			$nextdistrictid = 0;
			while( $num >= 0 ) {

				$businessname = @Treat_DB_ProxyOld::mysql_result( $result, $num, "businessname" );
				$businessid = @Treat_DB_ProxyOld::mysql_result( $result, $num, "businessid" );
				$street = @Treat_DB_ProxyOld::mysql_result( $result, $num, "street" );
				$city = @Treat_DB_ProxyOld::mysql_result( $result, $num, "city" );
				$state = @Treat_DB_ProxyOld::mysql_result( $result, $num, "state" );
				$zip = @Treat_DB_ProxyOld::mysql_result( $result, $num, "zip" );
				$phone = @Treat_DB_ProxyOld::mysql_result( $result, $num, "phone" );
				$unit = @Treat_DB_ProxyOld::mysql_result( $result, $num, "unit" );
				$districtid = @Treat_DB_ProxyOld::mysql_result( $result, $num, "districtid" );
				$closed1 = @Treat_DB_ProxyOld::mysql_result( $result, $num, "closed" );
				$closed1_reason = @Treat_DB_ProxyOld::mysql_result( $result, $num, "closed_reason" );
				$is_setup = @Treat_DB_ProxyOld::mysql_result( $result, $num, "setup" );

				if( $closed1 == 1 ) {
					$showclosed1 = "<img src=offline.gif height=16 width=16 alt='$closed1_reason' border=0>";
				}
				else {
					$showclosed1 = "";
				}

                                $graph = graphData($businessid, $startday, $today, $weekend, $closed1, 0);
                                /*
				$graph = "<table border=0 cellspacing=0 cellpadding=0><tr>";

				$startday = $startday2;
				$weekday = $weekday2;
				while( $startday <= $today ) {
					$exported = 0;

					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( "Unable to select database");
					$query2 = "SELECT export FROM submit WHERE businessid = '$businessid' AND date = '$startday'";
					$result2 = Treat_DB_ProxyOld::query( $query2 );
					$num2 = @Treat_DB_ProxyOld::mysql_numrows( $result2  );
					//mysql_close();

					if( $num2 != 0 ) {
						$exported = @mysql_result( $result2, 0, "export" );
					}

					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( "Unable to select database");
					$query3 = "SELECT closed FROM stats WHERE businessid = '$businessid' AND date = '$startday'";
					$result3 = Treat_DB_ProxyOld::query( $query3 );
					$num3 = @Treat_DB_ProxyOld::mysql_numrows( $result3  );
					//mysql_close();

					if( $num3 != 0 ) {
						$closed = @Treat_DB_ProxyOld::mysql_result( $result3, 0, "closed" );
					}

					if( $closed == 1 ) {
						$showthurs = "<a title = 'Closed' href=reason.php?bid=$businessid&cid=$companyid&date=$startday target = '_blank' style=$style><center><font color=white size=2>C</font></center></a>";
					}
					else {
						if( $weekday == "Thursday" && $num2 != 0 && $exported == 0 ) {
							$showthurs = "<font size=2><center>T</center></font>";
						}
						elseif( $weekday == "Thursday" && $num2 != 0 && $exported == 1 ) {
							$showthurs = "<font size=2 color=white><center>T</center></font>";
						}
						elseif( $weekday == "Thursday" && $num2 == 0 ) {
							$showthurs = "<font size=2 color=black><center>T</center></font>";
						}
						else {
							$showthurs = "";
						}
					}

					if( $closed == 1 ) {
						$graph = "$graph <td height=15 width=15 bgcolor=red>$showthurs</td><td width=1></td>";
					}
					else {
						if( $num2 != 0 && $exported == 1 ) {
							$graph = "$graph <td height=15 width=15 bgcolor=#444455>$showthurs</td><td width=1></td>";
						}
						elseif( $num2 != 0 ) {
							$graph = "$graph <td height=15 width=15 bgcolor=#00FF00>$showthurs</td><td width=1></td>";
						}
						elseif( $closed1 == 1 ) {
							$graph = "$graph <td height=15 width=15 bgcolor=#AAAAAA>$showthurs</td><td width=1></td>";
						}
						else {
							$graph = "$graph <td height=15 width=15 bgcolor=#00CCFF>$showthurs</td><td width=1></td>";
						}
					}

					$startday = nextday( $startday );
					if( $weekday == "Sunday" ) {
						$weekday = "Monday";
					}
					elseif( $weekday == "Saturday" ) {
						$weekday = "Sunday";
					}
					elseif( $weekday == "Friday" ) {
						$weekday = "Saturday";
					}
					elseif( $weekday == "Thursday" ) {
						$weekday = "Friday";
					}
					elseif( $weekday == "Wednesday" ) {
						$weekday = "Thursday";
					}
					elseif( $weekday == "Tuesday" ) {
						$weekday = "Wednesday";
					}
					elseif( $weekday == "Monday" ) {
						$weekday = "Tuesday";
					}

					$closed = 0;
				}

				$graph = "$graph </tr></table>";
                                */

				//if($order_only==1){$goto="buscatertrack.php";}
				//else{$goto="busdetail.php";}

				if( $sec_bus_sales > 0 ) {
					$goto = "busdetail.php";
				}
				elseif( $sec_bus_invoice > 0 ) {
					$goto = "businvoice.php";
				}
				elseif( $sec_bus_order > 0 ) {
					$goto = "buscatertrack.php";
				}
				elseif( $sec_bus_payable > 0 ) {
					$goto = "busapinvoice.php";
				}
				elseif( $sec_bus_payable2 > 0 ) {
					$goto = "busapinvoice2.php";
				}
				elseif( $sec_bus_inventor1 > 0 ) {
					$goto = "businventor.php";
				}
				elseif( $sec_bus_control > 0 ) {
					$goto = "buscontrol.php";
				}
                                elseif( $sec_bus_menu > 0 ) {
					$goto = "buscatermenu.php";
				}
				elseif( $sec_bus_order_setup > 0 ) {
					$goto = "buscaterroom.php";
				}
				elseif( $sec_bus_request > 0 ) {
					$goto = "busrequest.php";
				}
				elseif( $sec_route_collections > 0 ) {
					$goto = "busroute_collect.php";
				}
				elseif( $sec_route_labor > 0 ) {
					$goto = "busroute_labor.php";
				}
				elseif( $sec_route_detail > 0 ) {
					$goto = "busroute_detail.php";
				}
				elseif( $sec_bus_labor > 0 ) {
					$goto = "buslabor.php";
				}
				elseif( $sec_bus_timeclock > 0 ) {
					$goto = "buslabor5.php";
				}
				else {
					$goto = "businesstrack.php";
				}

				echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor='$back'><td><a style=$style href=$goto?bid=$businessid&cid=$companyid>$showclosed1 <font color=blue>$businessname</font></a></td><td>$unit</td><td><a href=/menu?bid=$businessid target='_blank'><img src=apple.gif height=16 width=16 border=0></a></td><td align=right>$graph</td><td align=right></td></tr>";
				echo "<tr bgcolor=#CCCCCC><td height=1 colspan=5></td></tr>";
				$buscount++;
				$num--;

			}

			echo "<tr bgcolor=black><td height=1 colspan=5></td></tr>";
			echo "<tr bgcolor=#E8E7E7><td>Results: $buscount</td><td></td><td></td><td align=right></td><td align=right></td></tr>";
			//echo "<tr bgcolor=black><td height=1 colspan=4></td></tr>";

		}
		//Unit Manger,Multipe Units
		else if( $security_level == 2 ) {

			$startday = $today;
			$weekday = date( 'l' );
			$thurs1 = 0;
			$thurs2 = 0;
			for( $counter = 1; $counter <= 14; $counter++ ) {
				$startday = prevday( $startday );
				if( $weekday == "Sunday" ) {
					$weekday = "Saturday";
				}
				elseif( $weekday == "Monday" ) {
					$weekday = "Sunday";
				}
				elseif( $weekday == "Tuesday" ) {
					$weekday = "Monday";
				}
				elseif( $weekday == "Wednesday" ) {
					$weekday = "Tuesday";
				}
				elseif( $weekday == "Thursday" ) {
					$weekday = "Wednesday";
				}
				elseif( $weekday == "Friday" ) {
					$weekday = "Thursday";
				}
				elseif( $weekday == "Saturday" ) {
					$weekday = "Friday";
				}
			}
			$weekday2 = $weekday;
			$startday2 = $startday;

			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
			$result = Treat_DB_ProxyOld::query( $query );
			$num = @Treat_DB_ProxyOld::mysql_numrows( $result  );
			//mysql_close();

			$busid1 = @Treat_DB_ProxyOld::mysql_result( $result, 0, "businessid" );
			$busid2 = @Treat_DB_ProxyOld::mysql_result( $result, 0, "busid2" );
			$busid3 = @Treat_DB_ProxyOld::mysql_result( $result, 0, "busid3" );
			$busid4 = @Treat_DB_ProxyOld::mysql_result( $result, 0, "busid4" );
			$busid5 = @Treat_DB_ProxyOld::mysql_result( $result, 0, "busid5" );
			$busid6 = @Treat_DB_ProxyOld::mysql_result( $result, 0, "busid6" );
			$busid7 = @Treat_DB_ProxyOld::mysql_result( $result, 0, "busid7" );
			$busid8 = @Treat_DB_ProxyOld::mysql_result( $result, 0, "busid8" );
			$busid9 = @Treat_DB_ProxyOld::mysql_result( $result, 0, "busid9" );
			$busid10 = @Treat_DB_ProxyOld::mysql_result( $result, 0, "busid10" );

			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query = "SELECT * FROM business WHERE companyid = '$companyid' AND (businessid = '$busid1' OR businessid = '$busid2' OR businessid = '$busid3' OR businessid = '$busid4' OR businessid = '$busid5' OR businessid = '$busid6' OR businessid = '$busid7' OR businessid = '$busid8' OR businessid = '$busid9' OR businessid = '$busid10')ORDER BY districtid DESC, businessname DESC";
			$result = Treat_DB_ProxyOld::query( $query );
			$num = @Treat_DB_ProxyOld::mysql_numrows( $result  );
			//mysql_close();

			echo "<tr bgcolor=#E8E7E7><td><b><i>Name</td><td><b><i>Unit#</td><td><b><i>Menu</b></i><td align=right colspan=2><table><tr><td>Exported</td><td width=15 height=15 bgcolor=#444455></td><td>Submitted</td><td height=15 width=15 bgcolor=#00FF00></td><td>Pending</td><td height=15 width=15 bgcolor=#00CCFF></td><td>Closed</td><td height=15 width=15 bgcolor=red><font color=white><center>C</center></font></td></tr></table></td></tr>";
			echo "<tr bgcolor=black><td height=1 colspan=5></td></tr>";
			echo "<tr bgcolor=#CCCCCC><td height=1 colspan=5></td></tr>";

			$buscount = 0;
			$subcount1 = 0;
			$subcount2 = 0;
			$num--;
			$nextdistrictid = 0;
			while( $num >= 0 ) {

				$businessname = @Treat_DB_ProxyOld::mysql_result( $result, $num, "businessname" );
				$businessid = @Treat_DB_ProxyOld::mysql_result( $result, $num, "businessid" );
				$street = @Treat_DB_ProxyOld::mysql_result( $result, $num, "street" );
				$city = @Treat_DB_ProxyOld::mysql_result( $result, $num, "city" );
				$state = @Treat_DB_ProxyOld::mysql_result( $result, $num, "state" );
				$zip = @Treat_DB_ProxyOld::mysql_result( $result, $num, "zip" );
				$phone = @Treat_DB_ProxyOld::mysql_result( $result, $num, "phone" );
				$unit = @Treat_DB_ProxyOld::mysql_result( $result, $num, "unit" );
				$districtid = @Treat_DB_ProxyOld::mysql_result( $result, $num, "districtid" );
				$closed1 = @Treat_DB_ProxyOld::mysql_result( $result, $num, "closed" );
				$closed1_reason = @Treat_DB_ProxyOld::mysql_result( $result, $num, "closed_reason" );
				$is_setup = @Treat_DB_ProxyOld::mysql_result( $result, $num, "setup" );

				if( $closed1 == 1 ) {
					$showclosed1 = "<img src=offline.gif height=16 width=16 alt='$closed1_reason' border=0>";
				}
				else {
					$showclosed1 = "";
				}

                                $graph = graphData($businessid, $startday, $today, $weekend, $closed1, 0);
                                /*
				$graph = "<table border=0 cellspacing=0 cellpadding=0><tr>";

				$startday = $startday2;
				$weekday = $weekday2;
				while( $startday <= $today ) {
					$exported = 0;

					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( "Unable to select database");
					$query2 = "SELECT export FROM submit WHERE businessid = '$businessid' AND date = '$startday'";
					$result2 = Treat_DB_ProxyOld::query( $query2 );
					$num2 = @Treat_DB_ProxyOld::mysql_numrows( $result2  );
					//mysql_close();

					if( $num2 != 0 ) {
						$exported = @Treat_DB_ProxyOld::mysql_result( $result2, 0, "export" );
					}

					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( "Unable to select database");
					$query3 = "SELECT closed FROM stats WHERE businessid = '$businessid' AND date = '$startday'";
					$result3 = Treat_DB_ProxyOld::query( $query3 );
					$num3 = @Treat_DB_ProxyOld::mysql_numrows( $result3  );
					//mysql_close();

					if( $num3 != 0 ) {
						$closed = @Treat_DB_ProxyOld::mysql_result( $result3, 0, "closed" );
					}

					if( $closed == 1 ) {
						$showthurs = "<a title = 'Closed' href=reason.php?bid=$businessid&cid=$companyid&date=$startday target = '_blank' style=$style><center><font color=white size=2>C</font></center></a>";
					}
					else {
						if( $weekday == "Thursday" && $num2 != 0 && $exported == 0 ) {
							$showthurs = "<font size=2><center>T</center></font>";
						}
						elseif( $weekday == "Thursday" && $num2 != 0 && $exported == 1 ) {
							$showthurs = "<font size=2 color=white><center>T</center></font>";
						}
						elseif( $weekday == "Thursday" && $num2 == 0 ) {
							$showthurs = "<font size=2 color=black><center>T</center></font>";
						}
						else {
							$showthurs = "";
						}
					}

					if( $closed == 1 ) {
						$graph = "$graph <td height=15 width=15 bgcolor=red>$showthurs</td><td width=1></td>";
					}
					else {
						if( $num2 != 0 && $exported == 1 ) {
							$graph = "$graph <td height=15 width=15 bgcolor=#444455>$showthurs</td><td width=1></td>";
						}
						elseif( $num2 != 0 ) {
							$graph = "$graph <td height=15 width=15 bgcolor=#00FF00>$showthurs</td><td width=1></td>";
						}
						elseif( $closed1 == 1 ) {
							$graph = "$graph <td height=15 width=15 bgcolor=#AAAAAA>$showthurs</td><td width=1></td>";
						}
						else {
							$graph = "$graph <td height=15 width=15 bgcolor=#00CCFF>$showthurs</td><td width=1></td>";
						}
					}

					$startday = nextday( $startday );
					if( $weekday == "Sunday" ) {
						$weekday = "Monday";
					}
					elseif( $weekday == "Saturday" ) {
						$weekday = "Sunday";
					}
					elseif( $weekday == "Friday" ) {
						$weekday = "Saturday";
					}
					elseif( $weekday == "Thursday" ) {
						$weekday = "Friday";
					}
					elseif( $weekday == "Wednesday" ) {
						$weekday = "Thursday";
					}
					elseif( $weekday == "Tuesday" ) {
						$weekday = "Wednesday";
					}
					elseif( $weekday == "Monday" ) {
						$weekday = "Tuesday";
					}

					$closed = 0;
				}

				$graph = "$graph </tr></table>";
                                */

				if( $nextdistrictid != $districtid ) {
					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( "Unable to select database");
					$query88 = "SELECT districtname FROM district WHERE districtid = '$districtid'";
					$result88 = Treat_DB_ProxyOld::query( $query88 );
					//mysql_close();
					$nextdistrictid = $districtid;
					$districtname = @Treat_DB_ProxyOld::mysql_result( $result88, 0, "districtname" );
					echo "<tr><td colspan=5 style=\"background-image:url('gradient20.jpg');background-repeat:repeat-x;\"><b><i>$districtname</i></b></td></tr>";
					echo "<tr bgcolor=#CCCCCC><td height=1 colspan=5></td></tr>";
				}
				else {
					//if($order_only==1&&$busid1==$businessid){$goto="buscatertrack.php";}
					//elseif($oo2==1&&$busid2==$businessid){$goto="buscatertrack.php";}
					//elseif($oo3==1&&$busid3==$businessid){$goto="buscatertrack.php";}
					//elseif($oo4==1&&$busid4==$businessid){$goto="buscatertrack.php";}
					//elseif($oo5==1&&$busid5==$businessid){$goto="buscatertrack.php";}
					//elseif($oo6==1&&$busid6==$businessid){$goto="buscatertrack.php";}
					//elseif($oo7==1&&$busid7==$businessid){$goto="buscatertrack.php";}
					//elseif($oo8==1&&$busid8==$businessid){$goto="buscatertrack.php";}
					//elseif($oo9==1&&$busid9==$businessid){$goto="buscatertrack.php";}
					//elseif($oo10==1&&$busid10==$businessid){$goto="buscatertrack.php";}
					//else{$goto="busdetail.php";}

					if( $sec_bus_sales > 0 ) {
						$goto = "busdetail.php";
					}
					elseif( $sec_bus_invoice > 0 ) {
						$goto = "businvoice.php";
					}
					elseif( $sec_bus_order > 0 ) {
						$goto = "buscatertrack.php";
					}
					elseif( $sec_bus_payable > 0 ) {
						$goto = "busapinvoice.php";
					}
					elseif( $sec_bus_payable2 > 0 ) {
						$goto = "busapinvoice2.php";
					}
					elseif( $sec_bus_inventor1 > 0 ) {
						$goto = "businventor.php";
					}
					elseif( $sec_bus_control > 0 ) {
						$goto = "buscontrol.php";
					}
                                        elseif( $sec_bus_menu > 0 ) {
                                                $goto = "buscatermenu.php";
                                        }
					elseif( $sec_bus_order_setup > 0 ) {
						$goto = "buscaterroom.php";
					}
					elseif( $sec_bus_request > 0 ) {
						$goto = "busrequest.php";
					}
					elseif( $sec_route_collections > 0 ) {
						$goto = "busroute_collect.php";
					}
					elseif( $sec_route_labor > 0 ) {
						$goto = "busroute_labor.php";
					}
					elseif( $sec_route_detail > 0 ) {
						$goto = "busroute_detail.php";
					}
					elseif( $sec_bus_labor > 0 ) {
						$goto = "buslabor.php";
					}
					elseif( $sec_bus_timeclock > 0 ) {
						$goto = "buslabor5.php";
					}
					else {
						$goto = "businesstrack.php";
					}

					echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor='$back'><td><a style=$style href=$goto?bid=$businessid&cid=$companyid>$showclosed1 <font color=blue>$businessname</font></a></td><td>$unit</td><td><a href=/menu?bid=$businessid target='_blank'><img src=apple.gif height=16 width=16 border=0></a></td><td align=right>$graph</td><td align=right></td></tr>";
					echo "<tr bgcolor=#CCCCCC><td height=1 colspan=5></td></tr>";
					$buscount++;
					$num--;
				}
			}

			echo "<tr bgcolor=black><td height=1 colspan=5></td></tr>";
			echo "<tr bgcolor=#E8E7E7><td>Results: $buscount</td><td></td><td></td><td align=right></td><td align=right></td></tr>";
			//echo "<tr bgcolor=black><td height=1 colspan=4></td></tr>";

		}
		///////////////////////////////District Manager's
		else if( $security_level < 7 && $security_level > 2 ) {

			$startday = $today;
			$weekday = date( 'l' );
			$thurs1 = 0;
			$thurs2 = 0;
			for( $counter = 1; $counter <= 14; $counter++ ) {
				$startday = prevday( $startday );
				if( $weekday == "Sunday" ) {
					$weekday = "Saturday";
				}
				elseif( $weekday == "Monday" ) {
					$weekday = "Sunday";
				}
				elseif( $weekday == "Tuesday" ) {
					$weekday = "Monday";
				}
				elseif( $weekday == "Wednesday" ) {
					$weekday = "Tuesday";
				}
				elseif( $weekday == "Thursday" ) {
					$weekday = "Wednesday";
				}
				elseif( $weekday == "Friday" ) {
					$weekday = "Thursday";
				}
				elseif( $weekday == "Saturday" ) {
					$weekday = "Friday";
				}
			}
			$weekday2 = $weekday;
			$startday2 = $startday;

			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query34 = "SELECT * FROM login_district WHERE loginid = '$loginid'";
			$result34 = Treat_DB_ProxyOld::query( $query34 );
			$num34 = @Treat_DB_ProxyOld::mysql_numrows( $result34  );
			//mysql_close();

			echo "<tr bgcolor=#E8E7E7><td width=35%><b><i>Name</td><td width=15%><b><i>Unit#</td><td width=15%><b><i>Latest Budget</td><td><b><i>Menu</i></b></td><td align=right colspan=2><table><tr><td>Exported</td><td width=15 height=15 bgcolor=#444455></td><td>Submitted</td><td height=15 width=15 bgcolor=#00FF00></td><td>Pending</td><td height=15 width=15 bgcolor=#00CCFF></td><td>Closed</td><td height=15 width=15 bgcolor=red><font color=white><center>C</center></font></td></tr></table></td></tr>";
			echo "<tr bgcolor=black><td height=1 colspan=6></td></tr>";
			echo "<tr bgcolor=#CCCCCC><td height=1 colspan=6></td></tr>";

			$buscount = 0;
			$num34--;
			while( $num34 >= 0 ) {
				//////////////////
				$dist_id = @Treat_DB_ProxyOld::mysql_result( $result34, $num34, "districtid" );

				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( "Unable to select database");
				$query = "SELECT * FROM business WHERE districtid = '$dist_id' AND companyid = '$companyid' ORDER BY businessname DESC";
				$result = Treat_DB_ProxyOld::query( $query );
				$num = @Treat_DB_ProxyOld::mysql_numrows( $result  );
				//mysql_close();

				$subcount1 = 0;
				$subcount2 = 0;
				$num--;
				$nextdistrictid = 0;
				while( $num >= 0 ) {

					$businessname = @Treat_DB_ProxyOld::mysql_result( $result, $num, "businessname" );
					$businessid = @Treat_DB_ProxyOld::mysql_result( $result, $num, "businessid" );
					$street = @Treat_DB_ProxyOld::mysql_result( $result, $num, "street" );
					$city = @Treat_DB_ProxyOld::mysql_result( $result, $num, "city" );
					$state = @Treat_DB_ProxyOld::mysql_result( $result, $num, "state" );
					$zip = @Treat_DB_ProxyOld::mysql_result( $result, $num, "zip" );
					$phone = @Treat_DB_ProxyOld::mysql_result( $result, $num, "phone" );
					$unit = @Treat_DB_ProxyOld::mysql_result( $result, $num, "unit" );
					$districtid = @Treat_DB_ProxyOld::mysql_result( $result, $num, "districtid" );
					$closed1 = @Treat_DB_ProxyOld::mysql_result( $result, $num, "closed" );
					$closed1_reason = @Treat_DB_ProxyOld::mysql_result( $result, $num, "closed_reason" );
					$is_setup = @Treat_DB_ProxyOld::mysql_result( $result, $num, "setup" );

					if( $closed1 == 1 ) {
						$showclosed1 = "<img src=offline.gif height=16 width=16 alt='$closed1_reason' border=0 align=bottom>";
					}
					else {
						$showclosed1 = "";
					}
                                        
                                        $graph = graphData($businessid, $startday, $today, $weekend, $closed1, 0);
                                        /*
					$graph = "<table border=0 cellspacing=0 cellpadding=0><tr>";

					$startday = $startday2;
					$weekday = $weekday2;
					while( $startday <= $today ) {
						$exported = 0;

						//mysql_connect($dbhost,$username,$password);
						//@mysql_select_db($database) or die( "Unable to select database");
						$query2 = "SELECT export FROM submit WHERE businessid = '$businessid' AND date = '$startday'";
						$result2 = Treat_DB_ProxyOld::query( $query2 );
						$num2 = @Treat_DB_ProxyOld::mysql_numrows( $result2  );
						//mysql_close();

						if( $num2 != 0 ) {
							$exported = @Treat_DB_ProxyOld::mysql_result( $result2, 0, "export" );
						}

						//mysql_connect($dbhost,$username,$password);
						//@mysql_select_db($database) or die( "Unable to select database");
						$query3 = "SELECT closed FROM stats WHERE businessid = '$businessid' AND date = '$startday'";
						$result3 = Treat_DB_ProxyOld::query( $query3 );
						$num3 = @Treat_DB_ProxyOld::mysql_numrows( $result3  );
						//mysql_close();

						if( $num3 != 0 ) {
							$closed = @Treat_DB_ProxyOld::mysql_result( $result3, 0, "closed" );
						}

						if( $closed == 1 ) {
							$showthurs = "<a title = 'Closed' href=reason.php?bid=$businessid&cid=$companyid&date=$startday target = '_blank' style=$style><center><font color=white size=2>C</font></center></a>";
						}
						else {
							if( $weekday == "Thursday" && $num2 != 0 && $exported == 0 ) {
								$showthurs = "<font size=2><center>T</center></font>";
							}
							elseif( $weekday == "Thursday" && $num2 != 0 && $exported == 1 ) {
								$showthurs = "<font size=2 color=white><center>T</center></font>";
							}
							elseif( $weekday == "Thursday" && $num2 == 0 ) {
								$showthurs = "<font size=2 color=black><center>T</center></font>";
							}
							else {
								$showthurs = "";
							}
						}

						if( $closed == 1 ) {
							$graph = "$graph <td height=15 width=15 bgcolor=red>$showthurs</td><td width=1></td>";
						}
						else {
							if( $num2 != 0 && $exported == 1 ) {
								$graph = "$graph <td height=15 width=15 bgcolor=#444455>$showthurs</td><td width=1></td>";
							}
							elseif( $num2 != 0 ) {
								$graph = "$graph <td height=15 width=15 bgcolor=#00FF00>$showthurs</td><td width=1></td>";
							}
							elseif( $closed1 == 1 ) {
								$graph = "$graph <td height=15 width=15 bgcolor=#AAAAAA>$showthurs</td><td width=1></td>";
							}
							else {
								$graph = "$graph <td height=15 width=15 bgcolor=#00CCFF>$showthurs</td><td width=1></td>";
							}
						}

						$startday = nextday( $startday );
						if( $weekday == "Sunday" ) {
							$weekday = "Monday";
						}
						elseif( $weekday == "Saturday" ) {
							$weekday = "Sunday";
						}
						elseif( $weekday == "Friday" ) {
							$weekday = "Saturday";
						}
						elseif( $weekday == "Thursday" ) {
							$weekday = "Friday";
						}
						elseif( $weekday == "Wednesday" ) {
							$weekday = "Thursday";
						}
						elseif( $weekday == "Tuesday" ) {
							$weekday = "Wednesday";
						}
						elseif( $weekday == "Monday" ) {
							$weekday = "Tuesday";
						}

						$closed = 0;
					}

					$graph = "$graph </tr></table>";
                                        */

					if( $nextdistrictid != $districtid ) {
						//mysql_connect($dbhost,$username,$password);
						//@mysql_select_db($database) or die( "Unable to select database");
						$query88 = "SELECT districtname FROM district WHERE districtid = '$districtid'";
						$result88 = Treat_DB_ProxyOld::query( $query88 );
						//mysql_close();
						$nextdistrictid = $districtid;
						$districtname = @Treat_DB_ProxyOld::mysql_result( $result88, 0, "districtname" );
						//echo "<tr><td colspan=4 bgcolor=#777777><b><font color=white>$districtname</font></b></td></tr>";

						$hideme = isset( $_COOKIE["hide$districtid"] ) ? $_COOKIE["hide$districtid"]
																		: '';
						if( $hideme == 1 ) {
							$style_hide = "display:none;";
							$hide_img = "exp2.gif";
						}
						else {
							$style_hide = "";
							$hide_img = "exp.gif";
						}

						echo "<tr valign=bottom><td colspan=6 style=\"background-image:url('gradient20.jpg');background-repeat:repeat-x;\"><a onmousedown=\"hide_show_units('$districtid');\" href=\"javascript: void(0);\" style='$style'><img src=$hide_img height=16 width=16 border=0 id='img$districtid'><font color=black><b><i>$districtname</i></b></font></a></td></tr>";
						echo "<tr bgcolor=#CCCCCC><td height=1 colspan=6></td></tr>";
					}
					else {
						//////////////Latest Budget
						$query88 = "SELECT date,user FROM audit_budget WHERE businessid = '$businessid' AND type = '1' ORDER BY date DESC LIMIT 0,1";
						$result88 = Treat_DB_ProxyOld::query( $query88 );

						$time = @Treat_DB_ProxyOld::mysql_result( $result88, 0, "date" );
						$showuser = @Treat_DB_ProxyOld::mysql_result( $result88, 0, "user" );

						if( $time > 0 ) {
							$b_day = substr( $time, 8, 2 );
							$b_month = substr( $time, 5, 2 );
							$b_year = substr( $time, 2, 2 );
							$b_time = substr( $time, 11, 5 );
							$showtime = "$b_month/$b_day/$b_year $b_time";
						}
						else {
							$showtime = "";
						}

						if( $sec_bus_sales > 0 ) {
							$goto = "busdetail.php";
						}
						elseif( $sec_bus_invoice > 0 ) {
							$goto = "businvoice.php";
						}
						elseif( $sec_bus_order > 0 ) {
							$goto = "buscatertrack.php";
						}
						elseif( $sec_bus_payable > 0 ) {
							$goto = "busapinvoice.php";
						}
						elseif( $sec_bus_payable2 > 0 ) {
							$goto = "busapinvoice2.php";
						}
						elseif( $sec_bus_inventor1 > 0 ) {
							$goto = "businventor.php";
						}
						elseif( $sec_bus_control > 0 ) {
							$goto = "buscontrol.php";
						}
                                                elseif( $sec_bus_menu > 0 ) {
                                                        $goto = "buscatermenu.php";
                                                }
						elseif( $sec_bus_order_setup > 0 ) {
							$goto = "buscaterroom.php";
						}
						elseif( $sec_bus_request > 0 ) {
							$goto = "busrequest.php";
						}
						elseif( $sec_route_collections > 0 ) {
							$goto = "busroute_collect.php";
						}
						elseif( $sec_route_labor > 0 ) {
							$goto = "busroute_labor.php";
						}
						elseif( $sec_route_detail > 0 ) {
							$goto = "busroute_detail.php";
						}
						elseif( $sec_bus_labor > 0 ) {
							$goto = "buslabor.php";
						}
						elseif( $sec_bus_timeclock > 0 ) {
							$goto = "buslabor5.php";
						}
						else {
							$goto = "businesstrack.php";
						}

						echo "<tr bgcolor='$back' onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor='$back' class=\"d$districtid\" style=$style_hide><td><a style=$style href=$goto?bid=$businessid&cid=$companyid>$showerror $showclosed1 <font color=blue>$businessname</font></a></td><td>$unit</td><td title='$showuser'>$showtime</td><td><a href=/menu?bid=$businessid target='_blank'><img src=apple.gif height=16 width=16 border=0></a></td><td align=right>$graph</td><td align=right></td></tr>";
						echo "<tr bgcolor=#CCCCCC class=\"d$districtid\" style=$style_hide><td height=1 colspan=6></td></tr>";
						$buscount++;
						$num--;
					}
				}
				//////////////////
				$num34--;
			}

			echo "<tr bgcolor=black><td height=1 colspan=6></td></tr>";
			echo "<tr bgcolor=#E8E7E7><td>Results: $buscount</td><td></td><td></td><td></td><td align=right></td><td align=right></td></tr>";
			//echo "<tr bgcolor=black><td height=1 colspan=4></td></tr>";

		}
		//////Admin
		else if( $security_level > 6 ) {

			$startday = $today;
			$weekday = date( 'l' );
			$thurs1 = 0;
			$thurs2 = 0;
			//$startday=prevday($startday);
			for( $counter = 1; $counter <= 14; $counter++ ) {
				$startday = prevday( $startday );

				if( $weekday == "Sunday" ) {
					$weekday = "Saturday";
				}
				elseif( $weekday == "Monday" ) {
					$weekday = "Sunday";
				}
				elseif( $weekday == "Tuesday" ) {
					$weekday = "Monday";
				}
				elseif( $weekday == "Wednesday" ) {
					$weekday = "Tuesday";
				}
				elseif( $weekday == "Thursday" ) {
					$weekday = "Wednesday";
				}
				elseif( $weekday == "Friday" ) {
					$weekday = "Thursday";
				}
				elseif( $weekday == "Saturday" ) {
					$weekday = "Friday";
				}
			}
			$weekday2 = $weekday;
			$startday2 = $startday;

			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query = "SELECT * FROM business WHERE companyid = '$companyid' ORDER BY districtid DESC, businessname DESC";
			$result = Treat_DB_ProxyOld::query( $query );
			$num = @Treat_DB_ProxyOld::mysql_numrows( $result  );
			//mysql_close();

			echo "<tr bgcolor=#E8E7E7>
                                <td width=35%>
                                    <b><i>Name</i></b> <a href=accounting_chart.php?cid=$companyid style=$style target='_blank'><font color=blue size=1>[SHOW STATUS]</font></a> 
                                </td>
                                <td width=15%>
                                    <b><i>Unit#</i></b>
                                </td>
                                <td width=15%>
                                    <b><i>Latest Budget</i></b>
                                </td>
                                <td>
                                    <b><i>Menu</b></i>
                                </td>
                                <td align=right colspan=2>
                                    <table><tr>
                                        <td>Exported</td>
                                        <td width=15 height=15 bgcolor=#444455></td>
                                        <td>Submitted</td>
                                        <td height=15 width=15 bgcolor=#00FF00></td>
                                        <td>Pending</td>
                                        <td height=15 width=15 bgcolor=#00CCFF></td>
                                        <td>Closed</td>
                                        <td height=15 width=15 bgcolor=red><font color=white><center>C</center></font></td>
                                    </tr></table>
                                </td></tr>";
			echo "<tr bgcolor=black><td height=1 colspan=6></td></tr>";
			echo "<tr bgcolor=#CCCCCC><td height=1 colspan=6></td></tr>";

			$buscount = 0;
			$subcount1 = 0;
			$subcount2 = 0;
			$num--;
			$nextdistrictid = 0;
			while( $num >= 0 ) {

				$businessname = @Treat_DB_ProxyOld::mysql_result( $result, $num, "businessname" );
				$businessid = @Treat_DB_ProxyOld::mysql_result( $result, $num, "businessid" );
				$street = @Treat_DB_ProxyOld::mysql_result( $result, $num, "street" );
				$city = @Treat_DB_ProxyOld::mysql_result( $result, $num, "city" );
				$state = @Treat_DB_ProxyOld::mysql_result( $result, $num, "state" );
				$zip = @Treat_DB_ProxyOld::mysql_result( $result, $num, "zip" );
				$phone = @Treat_DB_ProxyOld::mysql_result( $result, $num, "phone" );
				$unit = @Treat_DB_ProxyOld::mysql_result( $result, $num, "unit" );
				$districtid = @Treat_DB_ProxyOld::mysql_result( $result, $num, "districtid" );
				$closed1 = @Treat_DB_ProxyOld::mysql_result( $result, $num, "closed" );
				$closed1_reason = @Treat_DB_ProxyOld::mysql_result( $result, $num, "closed_reason" );
				$is_setup = @Treat_DB_ProxyOld::mysql_result( $result, $num, "setup" );

				if( $closed1 == 1 ) {
					$showclosed1 = "<img src=offline.gif height=16 width=16 alt='$closed1_reason' border=0>";
				}
				else {
					$showclosed1 = "";
				}

				//$graph = "<table border=0 cellspacing=0 cellpadding=0><tr>";

				$back = "white";
				$showerror = "";
				$laststatus = -1;

				$startday = $startday2;
				$weekday = $weekday2;
                                
                                $graph = graphData($businessid, $startday, $today, $weekend, $closed1, 1);
                                
                                /*
				while( $startday <= $today ) {
					$exported = 0;

					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( "Unable to select database");
					$query2 = "SELECT export FROM submit WHERE businessid = '$businessid' AND date = '$startday'";
					$result2 = Treat_DB_ProxyOld::query( $query2 );
					$num2 = @Treat_DB_ProxyOld::mysql_numrows( $result2  );
					//mysql_close();

					if( $num2 != 0 ) {
						$exported = @Treat_DB_ProxyOld::mysql_result( $result2, 0, "export" );
					}

					if( $num2 > $laststatus && $laststatus != -1 ) {
						$showerror = "<img src=error.gif height=16 width=19 border=0 alt='Attention: Unsubmitted Days'>";
						$back = "#FFFF99";
					}

					$laststatus = $num2;

					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( "Unable to select database");
					$query3 = "SELECT closed FROM stats WHERE businessid = '$businessid' AND date = '$startday'";
					$result3 = Treat_DB_ProxyOld::query( $query3 );
					$num3 = @Treat_DB_ProxyOld::mysql_numrows( $result3  );
					//mysql_close();

					if( $num3 != 0 ) {
						$closed = @Treat_DB_ProxyOld::mysql_result( $result3, 0, "closed" );
					}

					//////check for inventory
					if( substr( $startday, 5, 2 ) != substr( nextday( $startday ), 5, 2 ) ) {
						$monthbegin = substr( $startday, 0, 7 ) . "-01";

						$query33 = "SELECT SUM(amount) AS inventory1, export, posted FROM inventor WHERE businessid = '$businessid' AND date = '$monthbegin' GROUP BY businessid";
						$result33 = Treat_DB_ProxyOld::query( $query33 );

						$inventory1 = @Treat_DB_ProxyOld::mysql_result( $result33, 0, "inventory1" );
						$inv_export = @Treat_DB_ProxyOld::mysql_result( $result33, 0, "export" );
						$inv_posted = @Treat_DB_ProxyOld::mysql_result( $result33, 0, "posted" );

						$query33 = "SELECT SUM(inventor2.amount) AS inventory2 FROM acct_payable,inventor2 WHERE acct_payable.businessid = '$businessid' AND acct_payable.acct_payableid = inventor2.acct_payableid AND inventor2.date = '$startday' GROUP BY inventor2.date";
						$result33 = Treat_DB_ProxyOld::query( $query33 );

						$inventory2 = @Treat_DB_ProxyOld::mysql_result( $result33, 0, "inventory2" );

						$invcheck = abs( $inventory1 - $inventory2 ) > 0.05;

						if( $inventory1 == 0 || $inventory2 == 0 ) {
							$showthurs = "";
						}
						elseif( !$invcheck && $inv_export == 1 ) {
							$showthurs = "<center><img src=po3.gif height=10 width=10 title='Inventory Exported'></center>";
						}
						elseif( !$invcheck && $inv_posted == 1 ) {
							$showthurs = "<center><img src=po1.gif height=10 width=10 title='Inventory Ready for Export'></center>";
						}
						elseif( $invcheck && $export == 1 ) {
							$showthurs = "<center><img src=ex.gif height=10 width=10 title='Inventory Counts Do Not Match. Already Exported!'></center>";
						}
						elseif( $invcheck && $inv_posted == 1 ) {
							$inv_diff = number_format( $inventory1 - $inventory2, 2 );
							$showthurs = "<center><img src=ex.gif height=10 width=10 title='Inventory Counts Do Not Match ($inv_diff)'></center>";
						}
					}
					elseif( isset( $closed ) && $closed == 1 ) {
						$showthurs = "<a title = 'Closed' href=reason.php?bid=$businessid&cid=$companyid&date=$startday target = '_blank' style=$style><center><font color=white size=2>C</font></center></a>";
					}
					else {
						if( $weekday == "Thursday" && $num2 != 0 && $exported == 0 ) {
							$showthurs = "<font size=2><center>T</center></font>";
						}
						elseif( $weekday == "Thursday" && $num2 != 0 && $exported == 1 ) {
							$showthurs = "<font size=2 color=white><center>T</center></font>";
						}
						elseif( $weekday == "Thursday" && $num2 == 0 ) {
							$showthurs = "<font size=2 color=black><center>T</center></font>";
						}
						else {
							$showthurs = "";
						}
					}

					if( isset( $closed ) && $closed == 1 ) {
						$graph = "$graph <td height=15 width=15 bgcolor=red>$showthurs</td><td width=1></td>";
					}
					else {
						if( $num2 != 0 && $exported == 1 ) {
							$graph = "$graph <td height=15 width=15 bgcolor=#444455>$showthurs</td><td width=1></td>";
						}
						elseif( $num2 != 0 ) {
							$graph = "$graph <td height=15 width=15 bgcolor=#00FF00>$showthurs</td><td width=1></td>";
						}
						elseif( $closed1 == 1 ) {
							$graph = "$graph <td height=15 width=15 bgcolor=#AAAAAA>$showthurs</td><td width=1></td>";
						}
						else {
							$graph = "$graph <td height=15 width=15 bgcolor=#00CCFF>$showthurs</td><td width=1></td>";
						}
					}

					$startday = nextday( $startday );
					if( $weekday == "Sunday" ) {
						$weekday = "Monday";
					}
					elseif( $weekday == "Saturday" ) {
						$weekday = "Sunday";
					}
					elseif( $weekday == "Friday" ) {
						$weekday = "Saturday";
					}
					elseif( $weekday == "Thursday" ) {
						$weekday = "Friday";
					}
					elseif( $weekday == "Wednesday" ) {
						$weekday = "Thursday";
					}
					elseif( $weekday == "Tuesday" ) {
						$weekday = "Wednesday";
					}
					elseif( $weekday == "Monday" ) {
						$weekday = "Tuesday";
					}

					$closed = 0;
				}

				$graph = "$graph </tr></table>";
                                */

				if( $nextdistrictid != $districtid ) {
					//mysql_connect($dbhost,$username,$password);
					//@mysql_select_db($database) or die( "Unable to select database");
					$query88 = "SELECT districtname FROM district WHERE districtid = '$districtid'";
					$result88 = Treat_DB_ProxyOld::query( $query88 );
					//mysql_close();
					$nextdistrictid = $districtid;
					$districtname = @Treat_DB_ProxyOld::mysql_result( $result88, 0, "districtname" );
					//echo "<tr><td colspan=4 bgcolor=#777777><b><font color=white>$districtname</font></b></td></tr>";hereiam

					$hideme = isset( $_COOKIE["hide$districtid"] ) ? $_COOKIE["hide$districtid"] : 0;
					if( $hideme == 1 ) {
						$style_hide = "display:none;";
						$hide_img = "exp2.gif";
					}
					else {
						$style_hide = "";
						$hide_img = "exp.gif";
					}

					echo "<tr valign=bottom><td colspan=6 style=\"background-image:url('gradient20.jpg');background-repeat:repeat-x;\"><a onmousedown=\"hide_show_units('$districtid');\" href=\"javascript: void(0);\" style='$style'><img src=$hide_img height=16 width=16 border=0 id='img$districtid'><font color=black><b><i>$districtname</i></b></font></a></td></tr>";
					echo "<tr bgcolor=#CCCCCC><td height=1 colspan=6></td></tr>";
				}
				else {
					if( $sec_bus_sales > 0 ) {
						$goto = "busdetail.php";
					}
					elseif( $sec_bus_invoice > 0 ) {
						$goto = "businvoice.php";
					}
					elseif( $sec_bus_order > 0 ) {
						$goto = "buscatertrack.php";
					}
					elseif( $sec_bus_payable > 0 ) {
						$goto = "busapinvoice.php";
					}
					elseif( $sec_bus_payable2 > 0 ) {
						$goto = "busapinvoice2.php";
					}
					elseif( $sec_bus_inventor1 > 0 ) {
						$goto = "businventor.php";
					}
					elseif( $sec_bus_control > 0 ) {
						$goto = "buscontrol.php";
					}
                                        elseif( $sec_bus_menu > 0 ) {
                                                $goto = "buscatermenu.php";
                                        }
					elseif( $sec_bus_order_setup > 0 ) {
						$goto = "buscaterroom.php";
					}
					elseif( $sec_bus_request > 0 ) {
						$goto = "busrequest.php";
					}
					elseif( $sec_route_collections > 0 ) {
						$goto = "busroute_collect.php";
					}
					elseif( $sec_route_labor > 0 ) {
						$goto = "busroute_labor.php";
					}
					elseif( $sec_route_detail > 0 ) {
						$goto = "busroute_detail.php";
					}
					elseif( $sec_bus_labor > 0 ) {
						$goto = "buslabor.php";
					}
					elseif( $sec_bus_timeclock > 0 ) {
						$goto = "buslabor5.php";
					}
					else {
						$goto = "businesstrack.php";
					}

					//////////////Latest Budget
					$query88 = "SELECT date,user FROM audit_budget WHERE businessid = '$businessid' AND type = '1' ORDER BY date DESC LIMIT 0,1";
					$result88 = Treat_DB_ProxyOld::query( $query88 );

					$time = @@Treat_DB_ProxyOld::mysql_result( $result88, 0, "date" );
					$showuser = @Treat_DB_ProxyOld::mysql_result( $result88, 0, "user" );

					if( $time > 0 ) {
						$b_day = substr( $time, 8, 2 );
						$b_month = substr( $time, 5, 2 );
						$b_year = substr( $time, 2, 2 );
						$b_time = substr( $time, 11, 5 );
						$showtime = "$b_month/$b_day/$b_year $b_time";
					}
					else {
						$showtime = "";
					}

					//////setup kiosks
					if( $is_setup == 1 ) {
						$show_setup = bus_setup::setup( $businessid );
						$show_setup = "<span onclick=\"show_setup($businessid);\" onmouseover=\"style.cursor = 'hand';\">$show_setup</span>";
					}
					else {
						$show_setup = "";
					}

					echo "<tr bgcolor='$back' onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor='$back' class=\"d$districtid\" style=$style_hide><td>$show_setup <a style=$style href=$goto?bid=$businessid&cid=$companyid>$showerror $showclosed1 <font color=blue>$businessname</font></a></td><td>$unit</td><td title='$showuser'>$showtime</td><td><a href=/menu?bid=$businessid target='_blank'><img src=apple.gif height=16 width=16 border=0></a></td><td align=right>" . $graph . "</td><td align=right></td></tr>";
					echo "<tr bgcolor=#CCCCCC class=\"d$districtid\" style=$style_hide><td height=1 colspan=6></td></tr>";
					$buscount++;
					$num--;
				}
			}

			echo "<tr bgcolor=black><td height=1 colspan=6></td></tr>";
			echo "<tr bgcolor=#E8E7E7><td>Results: $buscount</td><td></td><td></td><td></td><td align=right></td><td align=right></td></tr>";
			//echo "<tr bgcolor=black><td height=1 colspan=4></td></tr>";

		}

		echo "</table></center><p><br>";

		echo "<center><table width=90% style=\"margin: 0 auto; text-align: left;\"><tr>";
		$date1 = $day1;
		$date2 = $today;

		$tablecount = 0;

		if( $mylinks == 1 ) {
			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query = "SELECT * FROM mylinks WHERE loginid = '$loginid' ORDER BY name DESC";
			$result = Treat_DB_ProxyOld::query( $query );
			$num = @Treat_DB_ProxyOld::mysql_numrows( $result  );
			//mysql_close();

			echo "<td width=33% valign=top><center><table width=95% cellspacing=0 cellpadding=0 style=\"margin: 0 auto; text-align: left;\">";
			echo "<tr height=2><td colspan=3 bgcolor=#CCCCFF></td></tr>";
			echo "<tr><td width=2 bgcolor=#CCCCFF></td><td bgcolor=#EFEFFF><a href=editpass.php?edit=2#mystuff style=$style title='Edit My Links'><font size=4 color=blue><u><b>My Links</a></td><td width=2 bgcolor=#CCCCFF></td></tr>";
			echo "<tr height=2><td colspan=3 bgcolor=#CCCCFF></td></tr>";
			$num--;
			while( $num >= 0 ) {
				$name = @Treat_DB_ProxyOld::mysql_result( $result, $num, "name" );
				$myurl = @Treat_DB_ProxyOld::mysql_result( $result, $num, "url" );
				echo "<tr><td width=2 bgcolor=#CCCCFF></td><td><a href='$myurl' style='$style' target='_blank'><font color=blue>$name</font></a></td><td width=2 bgcolor=#CCCCFF></td></tr>";
				$num--;
			}
			echo "<tr height=2><td colspan=3 bgcolor=#CCCCFF></td></tr></table></center></td>";

			$tablecount++;
		}

		if( $myap == 1 ) {
			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query = "SELECT * FROM vendors WHERE businessid = '$buscook' ORDER BY name DESC";
			$result = Treat_DB_ProxyOld::query( $query );
			$num = @Treat_DB_ProxyOld::mysql_numrows( $result  );
			//mysql_close();

			echo "<td width=33% valign=top><center><table width=95% cellspacing=0 cellpadding=0 style=\"margin: 0 auto; text-align: left;\">";
			echo "<tr height=2><td colspan=4 bgcolor=#CCCCFF></td></tr>";
			echo "<tr><td width=2 bgcolor=#CCCCFF></td><td bgcolor=#EFEFFF colspan=2><a href=busapinvoice.php?bid=$buscook&cid=$companyid style=$style title='Payables'><font size=4 color=blue><u><b>Payables</td><td width=2 bgcolor=#CCCCFF></td></tr>";
			echo "<tr height=2><td colspan=4 bgcolor=#CCCCFF></td></tr>";
			$num--;
			$totalinv = 0;
			while( $num >= 0 ) {
				$name = @Treat_DB_ProxyOld::mysql_result( $result, $num, "name" );
				$vendorid = @Treat_DB_ProxyOld::mysql_result( $result, $num, "vendorid" );
				$aptotal = 0;

				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( "Unable to select database");
				$query2 = "SELECT * FROM apinvoice WHERE businessid = '$buscook' && date >= '$date1' && date <= '$date2' && vendor = '$vendorid'";
				$result2 = Treat_DB_ProxyOld::query( $query2 );
				$num2 = @Treat_DB_ProxyOld::mysql_numrows( $result2  );
				//mysql_close();

				$num2--;
				while( $num2 >= 0 ) {
					$total = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, "total" );
					$aptotal = $aptotal + $total;
					$num2--;
				}

				$aptotal = money( $aptotal );
				$totalinv = $totalinv + $aptotal;
				if( $aptotal != 0 ) {
					echo "<tr><td width=2 bgcolor=#CCCCFF></td><td><font color=blue size=2>$name</font></td><td align=right><font color=blue size=2>$$aptotal</td><td width=2 bgcolor=#CCCCFF></td></tr>";
				}
				$num--;
			}
			$totalinv = money( $totalinv );
			echo "<tr><td width=2 bgcolor=#CCCCFF></td><td><font color=blue size=2><b>Total</font></td><td align=right><font color=blue size=2><b>$$totalinv</td><td width=2 bgcolor=#CCCCFF></td></tr>";
			echo "<tr height=2><td colspan=4 bgcolor=#CCCCFF></td></tr></table></center></td>";

			$tablecount++;
		}

		if( $myar == 1 ) {
			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query = "SELECT * FROM invoice WHERE businessid = '$buscook' AND date >= '$date1' AND date <= '$date2' ORDER BY date, event_time DESC";
			$result = Treat_DB_ProxyOld::query( $query );
			$num = @Treat_DB_ProxyOld::mysql_numrows( $result  );
			//mysql_close();

			echo "<td width=33% valign=top><center><table width=95% cellspacing=0 cellpadding=0 border=0 style=\"margin: 0 auto; text-align: left;\">";
			echo "<tr height=2><td colspan=5 bgcolor=#CCCCFF></td></tr>";
			echo "<tr><td width=2 bgcolor=#CCCCFF></td><td bgcolor=#EFEFFF colspan=3><a href=businvoice.php?bid=$buscook&cid=$companyid style=$style title='Invoicing'><font size=4 color=blue><u><b>Invoicing</a></td><td width=2 bgcolor=#CCCCFF></td></tr>";
			echo "<tr height=2><td colspan=5 bgcolor=#CCCCFF></td></tr>";
			$num--;
			while( $num >= 0 ) {
				$invoiceid = @Treat_DB_ProxyOld::mysql_result( $result, $num, "invoiceid" );
				$accountid = @Treat_DB_ProxyOld::mysql_result( $result, $num, "accountid" );
				$event_time = @Treat_DB_ProxyOld::mysql_result( $result, $num, "event_time" );
				$invdate = @Treat_DB_ProxyOld::mysql_result( $result, $num, "date" );
				$invstatus = @Treat_DB_ProxyOld::mysql_result( $result, $num, "status" );

				$inc_day = substr( $invdate, 8, 2 );
				$inc_month = substr( $invdate, 5, 2 );
				$invdate = "$inc_month/$inc_day";

				if( $invstatus == 1 ) {
					$invcolor = "green";
				}
				else {
					$invcolor = "blue";
				}

				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( "Unable to select database");
				$query2 = "SELECT name FROM accounts WHERE accountid = '$accountid'";
				$result2 = Treat_DB_ProxyOld::query( $query2 );
				$num2 = @Treat_DB_ProxyOld::mysql_numrows( $result2  );
				//mysql_close();

				$acct_name = @Treat_DB_ProxyOld::mysql_result( $result2, 0, "name" );

				echo "<tr><td width=2 bgcolor=#CCCCFF></td><td><font color=$invcolor size=2>$invdate</font></td><td><font color=$invcolor size=2>$acct_name</td><td><a href=saveinvoice.php?bid=$buscook&cid=$companyid&invoiceid=$invoiceid style=$style><font color=$invcolor size=2>$invoiceid</a></td><td width=2 bgcolor=#CCCCFF></td></tr>";
				$num--;
			}

			echo "<tr height=2><td colspan=5 bgcolor=#CCCCFF></td></tr></table></center></td>";

			$tablecount++;
		}

		if( $tablecount == 0 ) {
			echo "</tr></table></center>";
		}
		elseif( $tablecount == 1 ) {
			echo "<td width=33%></td><td width=33%></td></tr></table></center>";
		}
		elseif( $tablecount == 2 ) {
			echo "<td width=33%></td></tr></table></center>";
		}
		elseif( $tablecount == 3 ) {
			echo "</tr></table></center>";
		}

		//////////MYGADGETS
		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query = "SELECT * FROM mygadgets WHERE loginid = '$loginid'";
		$result = Treat_DB_ProxyOld::query( $query );
		$num = @Treat_DB_ProxyOld::mysql_numrows( $result  );
		//mysql_close();

		echo "<p><center><table width=90% style=\"margin: 0 auto; text-align: left;\">";

		$num--;
		$counter = 0;
		while( $num >= 0 ) {
			if( $counter == 0 ) {
				$counter = 1;
			}
			$mygadgetid = @Treat_DB_ProxyOld::mysql_result( $result, $num, "mygadgetid" );
			$code = @Treat_DB_ProxyOld::mysql_result( $result, $num, "code" );

			if( $counter == 1 ) {
				echo "<tr><td colspan=2><hr></td></tr><tr><td><center>$code</center></td>";
				$counter = 2;
			}
			elseif( $counter == 2 ) {
				echo "<td><center>$code</center></td></tr>";
				$counter = 1;
			}

			$num--;
		}
		if( $counter == 1 ) {
			echo "<td></td></tr>";
		}

		echo "<tr><td colspan=2><hr></td></tr></table></center>";

		$query19 = "UPDATE login SET oo4 = '0' WHERE loginid = '$loginid'";
		$result19 = Treat_DB_ProxyOld::query( $query19 );
	}
	elseif( $oo4 == 1 ) {
		// redundant query, this is done above
		//
		//$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
		//$result = Treat_DB_ProxyOld::query($query);
		//$num = @mysql_numrows( $result );
		//mysql_close();

		//$busid1=@mysql_result($result,0,"businessid");
		//$busid2=@mysql_result($result,0,"busid2");
		//$busid3=@mysql_result($result,0,"busid3");
		//$busid4=@mysql_result($result,0,"busid4");
		//$busid5=@mysql_result($result,0,"busid5");
		//$busid6=@mysql_result($result,0,"busid6");
		//$busid7=@mysql_result($result,0,"busid7");
		//$busid8=@mysql_result($result,0,"busid8");
		//$busid9=@mysql_result($result,0,"busid9");
		//$busid10=@mysql_result($result,0,"busid10");

		$thisyear = \EE\Controller\Base::getGetVariable( 'thisyear' );
		$thismonth = \EE\Controller\Base::getGetVariable( 'thismonth' );
		$showwkds = \EE\Controller\Base::getGetVariable( 'showwkds' );

		echo "<p><center><table width=90% cellpadding=0 cellspacing=0 border=0 style=\"margin: 0 auto; text-align: left;\">";

		///////////////////////
		/////////////////FILTER
		///////////////////////
		$viewbus = \EE\Controller\Base::getGetVariable( 'viewbus' );
		$viewtype = \EE\Controller\Base::getGetVariable( 'viewtype' );
		//$viewsub = \EE\Controller\Base::getGetVariable( 'viewsub' );
		$viewsales = \EE\Controller\Base::getGetVariable( 'viewsales' );

		if( $viewtype == "" ) {
			$viewbus = $oo6;
			$viewtype = $oo5;
		}

		if( $showwkds == "" ) {
			$showwkds = $oo7;
		}

		/*if( $viewsub == "" ) {
			$viewsub = $defaultsub;
		}*/
		/////////////////////////
		$query1 = "UPDATE login SET oo4 = '1', oo5 = '$viewtype', oo6 = '$viewbus', viewsub = '$viewsub', oo7 = '$showwkds' WHERE loginid = '$loginid'";
		$result1 = Treat_DB_ProxyOld::query( $query1 );

		$query12 = "SELECT * FROM login_all WHERE loginid = '$loginid'";
		$result12 = Treat_DB_ProxyOld::query( $query12 );
		$num12 = @Treat_DB_ProxyOld::mysql_numrows( $result12  );

		if( $sec_dashboard > 0 && $sec_dashboard != 2 ) {
			if( $viewtype == 1 && $security_level >= 7 ) {
				$compquery = "business.companyid != '2'";

				echo "<tr bgcolor=#E8E7E7 style=\"border:2px solid #E8E7E7;\"><td colspan=1 style=\"border:2px solid #E8E7E7;\"><div style=\" width:150px; background:#FFFF99; border:1px solid #454545; float:left;\"><center>Treat America</center></div>";

				$query58 = "SELECT * FROM company_type ORDER BY type_name DESC";
				$result58 = Treat_DB_ProxyOld::query( $query58 );
				$num58 = @Treat_DB_ProxyOld::mysql_numrows( $result58  );

				$num58--;
				while( $num58 >= 0 ) {
					$type_name = @Treat_DB_ProxyOld::mysql_result( $result58, $num58, "type_name" );
					$typeid = @Treat_DB_ProxyOld::mysql_result( $result58, $num58, "typeid" );

					echo "<div id='2.$typeid' style=\" width:150px; background:#CCCCCC; border:1px solid #454545; float:left; margin-left:3px;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=2&viewbus=$typeid&viewsub=0 style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>$type_name</font></center></div>";

					$num58--;
				}
				echo "<div style=\" width:50px; background:#CCCCCC; border:1px solid #454545; float:left; margin-left:3px;\"><a href='javascript:void();' onclick=\"Effect.SlideDown('navigation', { duration: 1.0 }); return false;\" style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Nav</font></center></a></div>";
				echo "</td>";
			}
			elseif( $viewtype == 2 ) {
				$compquery = "business.companyid = company.companyid AND company.company_type = '$viewbus'";
				$fromtables = ",company";

				$query58 = "SELECT * FROM login_all WHERE loginid = '$loginid'";
				$result58 = Treat_DB_ProxyOld::query( $query58 );
				$can_goback = @Treat_DB_ProxyOld::mysql_result( $result58, 0, "login_all" );

				$query58 = "SELECT * FROM company_type WHERE typeid = '$viewbus'";
				$result58 = Treat_DB_ProxyOld::query( $query58 );
				$com_typename = @Treat_DB_ProxyOld::mysql_result( $result58, 0, "type_name" );

				echo "<tr bgcolor=#E8E7E7 style=\"border:2px solid #E8E7E7;\"><td colspan=1 style=\"border:2px solid #E8E7E7;\"><div style=\" width:150px; background:#FFFF99; border:1px solid #454545; float:left;\"><center>$com_typename</center></div> <form action=businesstrack.php method=post style=\"margin:0; padding:0; float:left; margin-left:3px;\"><select name=viewbus onChange='changePage(this.form.viewbus)'>";

				$query58 = "SELECT * FROM company WHERE company_type = '$viewbus' ORDER  BY companyname DESC";
				$result58 = Treat_DB_ProxyOld::query( $query58 );
				$num58 = @Treat_DB_ProxyOld::mysql_numrows( $result58  );

				$num58--;
				echo "<option value=>Please Choose...</option>";
				while( $num58 >= 0 ) {
					$compid2 = @Treat_DB_ProxyOld::mysql_result( $result58, $num58, "companyid" );
					$compname2 = @Treat_DB_ProxyOld::mysql_result( $result58, $num58, "reference" );

					//echo "<div id='3.$compid2' style=\" width:150px; background:#CCCCCC; border:1px solid #454545; float:left; margin-left:3px;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=3&viewbus=$compid2 style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>$compname2</font></center></div>";
					echo "<option value=businesstrack.php?viewbus=$compid2&viewtype=3&thismonth=$thismonth&thisyear=$thisyear&whichdist=$viewbus&viewsub=$viewsub>$compname2</option>";

					$num58--;
				}
				echo "</select></form>";
				echo "<div style=\"width:50px; background:#CCCCCC; border:1px solid #454545; float:left; margin-left:3px;\"><a href='javascript:void();' onclick=\"Effect.SlideDown('navigation', { duration: 1.0 }); return false;\" style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Nav</font></center></a></div>";
				if( $num12 == 1 ) {
					echo "<div style=\"width:50px; background:#CCCCCC; border:1px solid #454545; float:left; margin-left:3px;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=1&viewbus=0&viewsub=$viewsub style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Back</font></center></a></div>";
				}
				if( $num12 == 1 ) {
					echo "<div style=\"width:50px; background:#CCCCCC; border:1px solid #454545; float:left; margin-left:3px;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=1&viewbus=0&viewsub=0 style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Home</font></center></a></div>";
				}
				echo "</td>";
			}
			elseif( $viewtype == 3 ) {
				$compquery = "business.companyid = company.companyid  AND company.companyid = '$viewbus'";
				$fromtables = ",company";

				$query58 = "SELECT * FROM login_company_type WHERE loginid = '$loginid'";
				$result58 = Treat_DB_ProxyOld::query( $query58 );
				$can_goback = @Treat_DB_ProxyOld::mysql_result( $result58, 0, "login_all" );

				$query58 = "SELECT * FROM company WHERE companyid = '$viewbus'";
				$result58 = Treat_DB_ProxyOld::query( $query58 );
				$companyname = @Treat_DB_ProxyOld::mysql_result( $result58, 0, "reference" );
				$companytype = @Treat_DB_ProxyOld::mysql_result( $result58, 0, "company_type" );

				echo "<tr bgcolor=#E8E7E7 style=\"border:2px solid #E8E7E7;\"><td colspan=1 style=\"border:2px solid #E8E7E7;\"><div style=\" width:150px; background:#FFFF99; border:1px solid #454545; float:left;\"><center>$companyname</center></div> <form action=businesstrack.php method=post style=\"margin:0; padding:0; float:left; margin-left:3px;\"><select name=viewbus onChange='changePage(this.form.viewbus)'>";

				$query58 = "SELECT * FROM division WHERE companyid = '$viewbus' ORDER  BY division_name DESC";
				$result58 = Treat_DB_ProxyOld::query( $query58 );
				$num58 = @Treat_DB_ProxyOld::mysql_numrows( $result58  );

				$num58--;
				echo "<option value=>Please Choose...</option>";
				while( $num58 >= 0 ) {
					$divisionid = @Treat_DB_ProxyOld::mysql_result( $result58, $num58, "divisionid" );
					$division_name = @Treat_DB_ProxyOld::mysql_result( $result58, $num58, "division_name" );

					//echo "<div id='4.$divisionid' style=\" width:150px; background:#CCCCCC; border:1px solid #454545; float:left; margin-left:3px;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=4&viewbus=$divisionid style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>$division_name</font></center></div>";
					echo "<option value=businesstrack.php?viewbus=$divisionid&viewtype=4&thismonth=$thismonth&thisyear=$thisyear&whichdist=$viewbus&viewsub=$viewsub>$division_name</option>";

					$num58--;
				}
				echo "</select></form>";
				echo "<div style=\" width:50px; background:#CCCCCC; border:1px solid #454545; float:left; margin-left:3px;\"><a href='javascript:void();' onclick=\"Effect.SlideDown('navigation', { duration: 1.0 }); return false;\" style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Nav</font></center></a></div>";
				if( $num12 == 1 ) {
					echo "<div style=\" width:50px; background:#CCCCCC; border:1px solid #454545; float:left; margin-left:3px;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=2&viewbus=$companytype&viewsub=$viewsub style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Back</font></center></div>";
				}
				if( $num12 == 1 ) {
					echo "<div style=\" width:50px; background:#CCCCCC; border:1px solid #454545; float:left; margin-left:3px;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=1&viewbus=0&viewsub=0 style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Home</font></center></a></a></div>";
				}
				echo "</td>";
			}
			elseif( $viewtype == 4 ) {
				$compquery = "business.districtid = district.districtid AND district.divisionid = '$viewbus'";
				$fromtables = ",district";

				$query58 = "SELECT * FROM division WHERE divisionid = '$viewbus'";
				$result58 = Treat_DB_ProxyOld::query( $query58 );
				$divisionname = @Treat_DB_ProxyOld::mysql_result( $result58, 0, "division_name" );
				$comid = @Treat_DB_ProxyOld::mysql_result( $result58, 0, "companyid" );

				echo "<tr bgcolor=#E8E7E7 style=\"border:2px solid #E8E7E7;\"><td colspan=1 style=\"border:2px solid #E8E7E7;\"><div style=\" width:150px; background:#FFFF99; border:1px solid #454545; float:left;\"><center>$divisionname</center></div> <form action=businesstrack.php method=post style=\"margin:0; padding:0; float:left; margin-left:3px;\"><select name=viewbus onChange='changePage(this.form.viewbus)'>";

				$query58 = "SELECT * FROM district WHERE divisionid = '$viewbus' ORDER  BY districtname DESC";
				$result58 = Treat_DB_ProxyOld::query( $query58 );
				$num58 = @Treat_DB_ProxyOld::mysql_numrows( $result58  );

				$num58--;
				echo "<option value=>Please Choose...</option>";
				while( $num58 >= 0 ) {
					$districtid = @Treat_DB_ProxyOld::mysql_result( $result58, $num58, "districtid" );
					$districtname = @Treat_DB_ProxyOld::mysql_result( $result58, $num58, "districtname" );

					//echo "<div id='5.$districtid' style=\" width:150px; background:#CCCCCC; border:1px solid #454545; float:left; margin-left:3px;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=5&viewbus=$districtid style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>$districtname</font></center></div>";
					echo "<option value=businesstrack.php?viewbus=$districtid&viewtype=5&thismonth=$thismonth&thisyear=$thisyear&whichdist=$viewbus&viewsub=$viewsub>$districtname</option>";

					$num58--;
				}
				echo "</select></form>";
				echo "<div style=\" width:50px; background:#CCCCCC; border:1px solid #454545; float:left; margin-left:3px;\"><a href='javascript:void();' onclick=\"Effect.SlideDown('navigation', { duration: 1.0 }); return false;\" style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Nav</font></center></a></div>";
				if( $num12 == 1 ) {
					echo "<div style=\" width:50px; background:#CCCCCC; border:1px solid #454545; float:left; margin-left:3px;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=3&viewbus=$comid&viewsub=$viewsub style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Back</font></center></div>";
				}
				if( $num12 == 1 ) {
					echo "<div style=\" width:50px; background:#CCCCCC; border:1px solid #454545; float:left; margin-left:3px;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=1&viewbus=0&viewsub=0 style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Home</font></center></a></a></div>";
				}
				echo "</td>";
			}
			elseif( $viewtype == 5 ) {
				$compquery = "business.districtid = '$viewbus'";

				$query58 = "SELECT * FROM district WHERE districtid = '$viewbus'";
				$result58 = Treat_DB_ProxyOld::query( $query58 );
				$districtname = @Treat_DB_ProxyOld::mysql_result( $result58, 0, "districtname" );
				$divisionid = @Treat_DB_ProxyOld::mysql_result( $result58, 0, "divisionid" );

				echo "<tr bgcolor=#E8E7E7 style=\"border:2px solid #E8E7E7;\"><td colspan=1 style=\"border:2px solid #E8E7E7;\"><div style=\" width:150px; background:#FFFF99; border:1px solid #454545; float:left;\"><center>$districtname</center></div><form action=businesstrack.php method=post style=\"margin:0; padding:0; float:left; margin-left:3px;\"><select name=viewbus onChange='changePage(this.form.viewbus)'>";

				$query58 = "SELECT * FROM business WHERE districtid = '$viewbus' ORDER BY businessname DESC";
				$result58 = Treat_DB_ProxyOld::query( $query58 );
				$num58 = @Treat_DB_ProxyOld::mysql_numrows( $result58  );

				$num58--;
				echo "<option value=>Please Choose...</option>";
				while( $num58 >= 0 ) {
					$businessid = @Treat_DB_ProxyOld::mysql_result( $result58, $num58, "businessid" );
					$businessname = @Treat_DB_ProxyOld::mysql_result( $result58, $num58, "businessname" );
					$comid = @Treat_DB_ProxyOld::mysql_result( $result58, $num58, "companyid" );

					echo "<option value=businesstrack.php?viewbus=$businessid&viewtype=6&thismonth=$thismonth&thisyear=$thisyear&whichdist=$viewbus&viewsub=$viewsub $show>$businessname</option>";

					$num58--;
				}
				echo "</select></form>";
				echo "<div style=\" width:50px; background:#CCCCCC; border:1px solid #454545; float:left; margin-left:3px;\"><a href='javascript:void();' onclick=\"Effect.SlideDown('navigation', { duration: 1.0 }); return false;\" style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Nav</font></center></a></div>";
				if( $num12 == 1 ) {
					echo "<div style=\" width:50px; background:#CCCCCC; border:1px solid #454545; float:left; margin-left:3px;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=4&viewbus=$divisionid&viewsub=$viewsub style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Back</font></center></div>";
				}
				if( $num12 == 1 ) {
					echo "<div style=\" width:50px; background:#CCCCCC; border:1px solid #454545; float:left; margin-left:3px;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=1&viewbus=0&viewsub=0 style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Home</font></center></a></div>";
				}
				echo "</td>";

				$query1 = "UPDATE login SET companyid = '$comid' WHERE loginid = '$loginid'";
				$result1 = Treat_DB_ProxyOld::query( $query1 );
			}
			elseif( $viewtype == 6 && $viewbus > 0 ) {
				$compquery = "business.businessid = '$viewbus'";

				$query58 = "SELECT districtid FROM business WHERE businessid = '$viewbus'";
				$result58 = Treat_DB_ProxyOld::query( $query58 );
				$whichdist = @Treat_DB_ProxyOld::mysql_result( $result58, 0, "districtid" );

				$query58 = "SELECT * FROM district WHERE districtid = '$whichdist'";
				$result58 = Treat_DB_ProxyOld::query( $query58 );
				$districtname = @Treat_DB_ProxyOld::mysql_result( $result58, 0, "districtname" );
				$divisionid = @Treat_DB_ProxyOld::mysql_result( $result58, 0, "divisionid" );

				echo "<tr bgcolor=#E8E7E7 style=\"border:2px solid #E8E7E7;\" align=bottom><td colspan=1 style=\"border:2px solid #E8E7E7;\"><form action=businesstrack.php method=post style=\"margin:0;padding:0;float:left;\"><select name=viewbus onChange='changePage(this.form.viewbus)'>";

				if( $security_level < 3 ) {
					$query58 = "SELECT * FROM business WHERE (businessid = '$busid1' OR businessid = '$busid2' OR businessid = '$busid3' OR businessid = '$busid4' OR businessid = '$busid5' OR businessid = '$busid6' OR businessid = '$busid7' OR businessid = '$busid8' OR businessid = '$busid9' OR businessid = '$busid10') ORDER BY businessname DESC";
				}
				else {
					$query58 = "SELECT * FROM business WHERE districtid = '$whichdist' ORDER BY businessname DESC";
				}
				$result58 = Treat_DB_ProxyOld::query( $query58 );
				$num58 = @Treat_DB_ProxyOld::mysql_numrows( $result58  );

				$num58--;
				echo "<option value=>Please Choose...</option>";
				while( $num58 >= 0 ) {
					$businessid = @Treat_DB_ProxyOld::mysql_result( $result58, $num58, "businessid" );
					$businessname = @Treat_DB_ProxyOld::mysql_result( $result58, $num58, "businessname" );
					$districtid = @Treat_DB_ProxyOld::mysql_result( $result58, $num58, "districtid" );
					$comid = @Treat_DB_ProxyOld::mysql_result( $result58, $num58, "companyid" );

					if( $viewbus == $businessid ) {
						$show = "SELECTED";
						$prevbus = @Treat_DB_ProxyOld::mysql_result( $result58, $num58 + 1, "businessid" );
						$nextbus = @Treat_DB_ProxyOld::mysql_result( $result58, $num58 - 1, "businessid" );
						$mystyle1 = "background-color:#FFFF99";
					}
					else {
						$show = "";
						$mystyle1 = "background-color:#FFFFFF";
					}

					echo "<option value=businesstrack.php?viewbus=$businessid&viewtype=6&thismonth=$thismonth&thisyear=$thisyear&whichdist=$districtid&viewsub=$viewsub $show style=$mystyle1>$businessname</option>";

					$num58--;
				}
				echo "</select></form>";

				if( $prevbus > 0 && $security_level > 1 ) {
					echo "<div style=\" width:50px; background:#CCCCCC; border:1px solid #454545; float:left; margin-left:3px;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=6&viewbus=$prevbus&viewsub=$viewsub style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Prev</font></center></a></div>";
				}
				else {
					echo "<div style=\" width:50px; background:#CCCCCC; border:1px solid #454545; float:left; margin-left:3px;\"><center><font color=black>Prev</font></center></div>";
				}
				if( $nextbus > 0 && $security_level > 1 ) {
					echo "<div style=\" width:50px; background:#CCCCCC; border:1px solid #454545; float:left; margin-left:3px;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=6&viewbus=$nextbus&viewsub=$viewsub style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Next</font></center></a></div>";
				}
				else {
					echo "<div style=\" width:50px; background:#CCCCCC; border:1px solid #454545; float:left; margin-left:3px;\"><center><font color=black>Next</font></center></div>";
				}

				echo "<div style=\" width:50px; background:#CCCCCC; border:1px solid #454545; float:left; margin-left:3px;\"><a href='javascript:void();' onclick=\"Effect.SlideDown('navigation', { duration: 1.0 }); return false;\" style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Nav</font></center></a></div>";
				if( $num12 == 1 ) {
					echo "<div style=\" width:50px; background:#CCCCCC; border:1px solid #454545; float:left; margin-left:3px;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=5&viewbus=$districtid&viewsub=$viewsub style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Back</font></center></a></div>";
				}
				if( $num12 == 1 ) {
					echo "<div style=\" width:50px; background:#CCCCCC; border:1px solid #454545; float:left; margin-left:3px;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=1&viewbus=0 style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Home</font></center></a></a></div>";
				}

				echo "</td>";

				if( $sec_bus_sales > 0 ) {
					$goto = "busdetail.php";
				}
				elseif( $sec_bus_invoice > 0 ) {
					$goto = "businvoice.php";
				}
				elseif( $sec_bus_order > 0 ) {
					$goto = "buscatertrack.php";
				}
				elseif( $sec_bus_payable > 0 ) {
					$goto = "busapinvoice.php";
				}
				elseif( $sec_bus_payable2 > 0 ) {
					$goto = "busapinvoice2.php";
				}
				elseif( $sec_bus_inventor1 > 0 ) {
					$goto = "businventor.php";
				}
				elseif( $sec_bus_control > 0 ) {
					$goto = "buscontrol.php";
				}
				elseif( $sec_bus_order_setup > 0 ) {
					$goto = "buscaterroom.php";
				}
				elseif( $sec_bus_request > 0 ) {
					$goto = "busrequest.php";
				}
				elseif( $sec_route_collections > 0 ) {
					$goto = "busroute_collect.php";
				}
				elseif( $sec_route_labor > 0 ) {
					$goto = "busroute_labor.php";
				}
				elseif( $sec_route_detail > 0 ) {
					$goto = "busroute_detail.php";
				}
				elseif( $sec_bus_labor > 0 ) {
					$goto = "buslabor.php";
				}
				else {
					$goto = "businesstrack.php";
				}

				$query1 = "UPDATE login SET companyid = '$comid' WHERE loginid = '$loginid'";
				$result1 = Treat_DB_ProxyOld::query( $query1 );

				$showdetail = " | <a href=$goto?bid=$viewbus&cid=$comid style=$style><font color=blue  onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>View Details</font></a>";
			}
			else {
				echo "<tr bgcolor=#E8E7E7 style=\"border:2px solid #E8E7E7;\" align=bottom><td colspan=1 style=\"border:2px solid #E8E7E7;\">";

				echo "<div style=\" width:150px; background:#FFFF99; border:1px solid #454545; display:inline;\"><center>Treat America</center></div>&nbsp;<div style=\" width:50px; background:#CCCCCC; border:1px solid #454545; display:inline;\"><a href='javascript:void();' onclick=\"Effect.SlideDown('navigation', { duration: 1.0 }); return false;\" style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Nav</font></center></a></div></td>";
			}

			if( $showwkds == 1 && $viewtype > 0 ) {
				$showweekends = "<a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&showwkds=0&viewtype=$viewtype&viewbus=$viewbus&whichdist=$whichdist style=$style><font color=blue  onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Hide Weekends</font></a>";
			}
			elseif( $viewtype > 0 ) {
				$showweekends = "<a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&showwkds=1&viewtype=$viewtype&viewbus=$viewbus&whichdist=$whichdist style=$style><font color=blue  onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Show Weekends</font></a>";
			}

			echo "<td align=right style=\"border:2px solid #E8E7E7;\"> $showweekends$showdetail$showprev$shownext&nbsp;</td></tr>";

			/////////////////////////
			///NAVIGATION////////////
			/////////////////////////
			echo "<tr><td colspan=2>";
			echo "<div id=\"navigation\" style=\"display:none; width:100%; background:#FFFFFF; border:2px solid #E8E7E7;\"><br>";

			$query12 = "SELECT * FROM login_all WHERE loginid = '$loginid'";
			$result12 = Treat_DB_ProxyOld::query( $query12 );
			$num12 = @Treat_DB_ProxyOld::mysql_numrows( $result12  );

			if( $num12 == 1 ) {
				$allow = 1;
				$allow_all = 1;
				echo "<a href=\"businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=1&viewbus=0&viewsub=0\" style=$style><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Treat America</b></font></a><br>";
			}

			///////////COMPANY TYPE
			$query = "SELECT * FROM company_type ORDER BY type_name DESC";
			$result = Treat_DB_ProxyOld::query( $query );
			$num = @Treat_DB_ProxyOld::mysql_numrows( $result  );
			$typewidth = round( 100 / $num, 2 );

			echo "<table width=100% style=\"font-size: 13px; margin: 0 auto; text-align: left;\"><tr valign=top>";

			$num--;
			while( $num >= 0 ) {
				echo "<td width=$typewidth%>";

				$typeid = @Treat_DB_ProxyOld::mysql_result( $result, $num, "typeid" );
				$type_name = @Treat_DB_ProxyOld::mysql_result( $result, $num, "type_name" );

				$query45 = "SELECT * FROM login_company_type WHERE loginid = '$loginid' AND company_type = '$typeid'";
				$result45 = Treat_DB_ProxyOld::query( $query45 );
				$num45 = @Treat_DB_ProxyOld::mysql_numrows( $result45  );

				if( $num45 != 0 || $allow_all == 1 ) {
					$allow = 1;
					echo "<a href=\"businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=2&viewbus=$typeid&viewsub=0\" style=$style><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><u>$type_name</u></b></font></a><br>";
				}

				/////////COMPANIES
				$query2 = "SELECT * FROM company WHERE company_type = '$typeid' ORDER BY companyname DESC";
				$result2 = Treat_DB_ProxyOld::query( $query2 );
				$num2 = @Treat_DB_ProxyOld::mysql_numrows( $result2  );

				$num2--;
				while( $num2 >= 0 ) {
					$co_id = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, "companyid" );
					$companyname = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, "reference" );

					$query45 = "SELECT * FROM login_comp WHERE loginid = '$loginid' AND companyid = '$co_id'";
					$result45 = Treat_DB_ProxyOld::query( $query45 );
					$num45 = @Treat_DB_ProxyOld::mysql_numrows( $result45  );

					if( $num45 != 0 || $allow == 1 || $allow_all == 1 ) {
						$allow = 1;
						echo "<a href=\"businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=3&viewbus=$co_id&viewsub=$viewsub\" style=$style><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$companyname</font></a><br>";
					}

					/////////////DIVISIONS
					$query3 = "SELECT * FROM division WHERE companyid = '$co_id' ORDER BY division_name DESC";
					$result3 = Treat_DB_ProxyOld::query( $query3 );
					$num3 = @Treat_DB_ProxyOld::mysql_numrows( $result3  );

					$num3--;
					while( $num3 >= 0 ) {
						$divisionid = @Treat_DB_ProxyOld::mysql_result( $result3, $num3, "divisionid" );
						$division_name = @Treat_DB_ProxyOld::mysql_result( $result3, $num3, "division_name" );

						$query45 = "SELECT * FROM login_division WHERE loginid = '$loginid' AND divisionid = '$divisionid'";
						$result45 = Treat_DB_ProxyOld::query( $query45 );
						$num45 = @Treat_DB_ProxyOld::mysql_numrows( $result45  );

						if( $num45 != 0 || $allow == 1 || $allow_all == 1 ) {
							$allow = 1;
							echo "<a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=4&viewbus=$divisionid&viewsub=$viewsub style=$style><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$division_name</font></a><br>";
						}

						/////////////DISTRICTS
						$query4 = "SELECT * FROM district WHERE divisionid = '$divisionid' ORDER BY districtname DESC";
						$result4 = Treat_DB_ProxyOld::query( $query4 );
						$num4 = @Treat_DB_ProxyOld::mysql_numrows( $result4  );

						$num4--;
						while( $num4 >= 0 ) {
							$districtid = @Treat_DB_ProxyOld::mysql_result( $result4, $num4, "districtid" );
							$districtname = @Treat_DB_ProxyOld::mysql_result( $result4, $num4, "districtname" );

							$query45 = "SELECT * FROM login_district WHERE loginid = '$loginid' AND districtid = '$districtid'";
							$result45 = Treat_DB_ProxyOld::query( $query45 );
							$num45 = @Treat_DB_ProxyOld::mysql_numrows( $result45  );

							if( $num45 != 0 || $allow == 1 || $allow_all == 1 ) {
								$allow = 1;
								echo "<a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=5&viewbus=$districtid&viewsub=$viewsub style=$style><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$districtname</font></a>&nbsp;<a href='javascript:void();' onclick=\"hide_show_district($districtid);\"><img id='imgbiz$districtid' src=arrowdown.gif height=10 width=10 border=0 alt='Expand'></a><br>";
							}

							/////////////BUSINESS
							if( $security_level > 2 ) {
								$query5 = "SELECT businessname,businessid FROM business WHERE districtid = '$districtid' ORDER BY businessname DESC";
							}
							else {
								$query5 = "SELECT businessname,businessid FROM business WHERE (businessid = '$busid1' OR businessid = '$busid2' OR businessid = '$busid3' OR businessid = '$busid4' OR businessid = '$busid5' OR businessid = '$busid6' OR businessid = '$busid7' OR businessid = '$busid8' OR businessid = '$busid9' OR businessid = '$busid10') AND districtid = '$districtid' ORDER BY businessname DESC";
							}
							$result5 = Treat_DB_ProxyOld::query( $query5 );
							$num5 = @Treat_DB_ProxyOld::mysql_numrows( $result5  );

							if( $security_level < 3 ) {
								echo "<div id='biz$districtid' style=\"width:100%; background:#FFFFFF;\"><img src=clear.gif height=1 width=1>";
							}
							else {
								echo "<div id='biz$districtid' style=\"display:none; width:100%; background:#FFFFFF;\"><img src=clear.gif height=1 width=1>";
							}

							$num5--;
							while( $num5 >= 0 ) {
								$bizid = @Treat_DB_ProxyOld::mysql_result( $result5, $num5, "businessid" );
								$businessname = @Treat_DB_ProxyOld::mysql_result( $result5, $num5, "businessname" );

								echo "<a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=6&viewbus=$bizid&viewsub=$viewsub style=$style><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$businessname</font></a><br>";

								$num5--;
							}

							if( $security_level > 2 ) {
								echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:void();' onclick=\"Effect.SlideUp(biz$districtid, { duration: 1.0 }); return false;\"><img src=arrowup.gif height=10 width=10 border=0 alt='Collapse'></a></div>";
							}

							$num4--;
						}
						$allow = 0;
						$num3--;
					}
					$allow = 0;
					$num2--;
				}
				$allow = 0;
				echo "</td>";
				$num--;
			}
			echo "</tr></table>";

			echo "<a href='javascript:void();' onclick=\"$('navigation').blindUp(); return false;\" style=$style><font size=2 color=#454545>Close</font></a></div>";
			echo "</td></tr>";
			////////////////END NAV

			////////////////////////////////////////
			////////////////SECONDARY NAV///////////
			////////////////////////////////////////
			$query11 = "SELECT * FROM dashboard_update";
			$result11 = Treat_DB_ProxyOld::query( $query11 );

			$update_route = @Treat_DB_ProxyOld::mysql_result( $result11, 0, "routes" );
			$update_sales = @Treat_DB_ProxyOld::mysql_result( $result11, 0, "sales" );
			$update_service = @Treat_DB_ProxyOld::mysql_result( $result11, 0, "service" );

			/*
			$query11="SELECT * FROM vend_locations,business$fromtables WHERE vend_locations.unitid = business.businessid AND ($compquery)";
			$result11 = Treat_DB_ProxyOld::query($query11);
			$num11 = @mysql_numrows( $result11 );
			 */

			/////vending/foodservice navigation...
			if( $viewtype == 1 ) {
				$num11 = 0;
			}
			elseif( $viewtype == 2 ) {
				if( $viewbus == 2 ) {
					$num11 = 1;
				}
				else {
					$num11 = 0;
				}
			}
			elseif( $viewtype == 3 ) {
				$query11 = "SELECT company_type.typeid FROM company,company_type WHERE company.companyid = '$viewbus' AND company.company_type = company_type.typeid";
				$result11 = Treat_DB_ProxyOld::query( $query11 );

				$db_comtype = @Treat_DB_ProxyOld::mysql_result( $result11, 0, "typeid" );

				if( $db_comtype == 2 ) {
					$num11 = 1;
				}
				else {
					$num11 = 0;
				}
			}
			elseif( $viewtype == 4 ) {
				$query11 = "SELECT company_type.typeid FROM company,company_type,division WHERE division.divisionid = '$viewbus' AND division.companyid = company.companyid AND company.company_type = company_type.typeid";
				$result11 = Treat_DB_ProxyOld::query( $query11 );

				$db_comtype = @Treat_DB_ProxyOld::mysql_result( $result11, 0, "typeid" );

				if( $db_comtype == 2 ) {
					$num11 = 1;
				}
				else {
					$num11 = 0;
				}
			}
			elseif( $viewtype == 5 ) {
				$query11 = "SELECT company_type.typeid FROM company,company_type,district WHERE district.districtid = '$viewbus' AND district.companyid = company.companyid AND company.company_type = company_type.typeid";
				$result11 = Treat_DB_ProxyOld::query( $query11 );

				$db_comtype = @Treat_DB_ProxyOld::mysql_result( $result11, 0, "typeid" );

				if( $db_comtype == 2 ) {
					$num11 = 1;
				}
				else {
					$num11 = 0;
				}
			}
			elseif( $viewtype == 6 && $viewbus > 0 ) {
				$query11 = "SELECT company_type.typeid FROM company,company_type,business WHERE business.businessid = '$viewbus' AND business.companyid = company.companyid AND company.company_type = company_type.typeid";
				$result11 = Treat_DB_ProxyOld::query( $query11 );

				$db_comtype = @Treat_DB_ProxyOld::mysql_result( $result11, 0, "typeid" );

				if( $db_comtype == 2 ) {
					$num11 = 1;
				}
				else {
					$num11 = 0;
				}
			}
			///////////////////////
			//////Vending Nav//////
			///////////////////////
			if( $num11 > 0 && $viewtype >= 2 && $viewbus != 1 ) {
				echo "<tr bgcolor=#E8E7E7><td style=\"border:2px solid #E8E7E7;\">";

				//if($user=="SteveM"||$user=="EdS"||$user=="MMeurer"||$user=="JimM"||$user=="billp"||$user=="johnmjr"||$user=='ryanp'){
				if( $sec_dashboard > 1 ) {
					if( $viewsub == 10 ) {
						echo "<div style=\" width:80px; background:#FFFF99; border:1px solid #454545; float:left;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=10 style=$style><center><font color=black onMouseOver=this.color='#FF9900' onMouseOut=this.color='black'>Custom</font></center></a></div>";
					}
					else {
						echo "<div style=\" width:80px; background:#CCCCCC; border:1px solid #454545; float:left;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=10 style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Custom</font></center></a></div>";
					}
				}

				if( $viewsub == "" || $viewsub == 0 ) {
					echo "<div style=\" width:80px; background:#FFFF99; border:1px solid #454545; float:left; margin-left:3px;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=0 style=$style><center><font color=black onMouseOver=this.color='#FF9900' onMouseOut=this.color='black'>Overview</font></center></a></div>";
				}
				else {
					echo "<div style=\" width:80px; background:#CCCCCC; border:1px solid #454545; margin-left:3px; float:left;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=0 style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Overview</font></center></a></div>";
				}

				if( $viewsub == 1 ) {
					echo "<div style=\" width:80px; background:#FFFF99; border:1px solid #454545; float:left; margin-left:3px;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=1 style=$style><center><font color=black onMouseOver=this.color='#FF9900' onMouseOut=this.color='black'>Customers</font></center></a></div>";
				}
				else {
					echo "<div style=\" width:80px; background:#CCCCCC; border:1px solid #454545; float:left; margin-left:3px;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=1 style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Customers</font></center></a></div>";
				}

				if( $viewsub == 2 ) {
					echo "<div style=\" width:80px; background:#FFFF99; border:1px solid #454545; float:left; margin-left:3px;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=2 style=$style><center><font color=black onMouseOver=this.color='#FF9900' onMouseOut=this.color='black'>Routes</font></center></a></div>";
				}
				else {
					echo "<div style=\" width:80px; background:#CCCCCC; border:1px solid #454545; float:left; margin-left:3px;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=2 style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Routes</font></center></a></div>";
				}

				if( $viewsub == 3 ) {
					echo "<div style=\" width:80px; background:#FFFF99; border:1px solid #454545; float:left; margin-left:3px;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=3 style=$style><center><font color=black onMouseOver=this.color='#FF9900' onMouseOut=this.color='black'>Rt Drivers</font></center></a></div>";
				}
				else {
					echo "<div style=\" width:80px; background:#CCCCCC; border:1px solid #454545; float:left; margin-left:3px;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=3 style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Rt Drivers</font></center></a></div>";
				}

				if( $viewsub == 7 ) {
					echo "<div style=\" width:80px; background:#FFFF99; border:1px solid #454545; float:left; margin-left:3px;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=7 style=$style><center><font color=black onMouseOver=this.color='#FF9900' onMouseOut=this.color='black'>Production</font></center></a></div>";
				}
				else {
					echo "<div style=\" width:80px; background:#CCCCCC; border:1px solid #454545; float:left; margin-left:3px;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=7 style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Production</font></center></a></div>";
				}

				if( $viewsub == 4 ) {
					echo "<div style=\" width:80px; background:#FFFF99; border:1px solid #454545; float:left; margin-left:3px;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=4 style=$style><center><font color=black onMouseOver=this.color='#FF9900' onMouseOut=this.color='black'>Vehicles</font></center></a></div>";
				}
				else {
					echo "<div style=\" width:80px; background:#CCCCCC; border:1px solid #454545; float:left; margin-left:3px;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=4 style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Vehicles</font></center></a></div>";
				}

				if( $viewsub == 5 ) {
					echo "<div style=\" width:80px; background:#FFFF99; border:1px solid #454545; float:left; margin-left:3px;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=5 style=$style><center><font color=black onMouseOver=this.color='#FF9900' onMouseOut=this.color='black'>Service</font></center></a></div>";
				}
				else {
					echo "<div style=\" width:80px; background:#CCCCCC; border:1px solid #454545; float:left; margin-left:3px;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=5 style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Service</font></center></a></div>";
				}

				if( $viewsub == 9 ) {
					echo "<div style=\" width:80px; background:#FFFF99; border:1px solid #454545; float:left; margin-left:3px;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=9 style=$style><center><font color=black onMouseOver=this.color='#FF9900' onMouseOut=this.color='black'>CapEx</font></center></a></div>";
				}
				else {
					echo "<div style=\" width:80px; background:#CCCCCC; border:1px solid #454545; float:left; margin-left:3px;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=9 style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>CapEx</font></center></a></div>";
				}

				if( $viewsub == 6 ) {
					echo "<div style=\" width:80px; background:#FFFF99; border:1px solid #454545; float:left; margin-left:3px;\" onmouseover=\"$('salesbox').show();\" onmouseout=\"$('salesbox').hide();\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=6 style=$style><center><font color=black onMouseOver=this.color='#FF9900' onMouseOut=this.color='black'>Sales</font></center></a> <div id='salesbox' onmouseout=\"$('salesbox').hide();\" style=\" width:99px; position: absolute; z-index: 200; border:1px solid #454545; background: #FFFFFF; display: none;\">&nbsp;<a href=dashboard_esalestrack.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&show=1 target='salesdash' style=$style onclick=\"$('salesbox').hide();\"><font color=blue>Sold/Pipeline</font></a> <br>&nbsp;<a href=dashboard_esalestrack.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&show=2 target='salesdash' style=$style onclick=\"$('salesbox').hide();\"><font color=blue>Lost</font></a> <br> &nbsp;<a href=dashboard_esalestrack.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&show=4 target='salesdash' style=$style onclick=\"$('salesbox').hide();\"><font color=blue>Red List</font></a> <br>&nbsp;<a href=dashboard_esalestrack.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&show=6 target='salesdash' style=$style onclick=\"$('salesbox').hide();\"><font color=blue>Gain/Loss</font></a> <br> &nbsp;<a href=dashboard_esalestrack.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&show=7 target='salesdash' style=$style onclick=\"$('salesbox').hide();\"><font color=blue>Sales Gap</font></a>";

					/////lost w/o dates
					$query49 = "SELECT COUNT(est_accounts.systemid) AS totalcount FROM est_accounts,est_accountowner WHERE est_accounts.datelost = '0000-00-00' AND est_accounts.customertype = '4' AND est_accounts.status = '2' AND est_accounts.accountowner = est_accountowner.accountowner AND est_accountowner.type > -1 ORDER BY est_accounts.datelost DESC";
					$result49 = Treat_DB_ProxyOld::query( $query49 );

					$lostcount = @Treat_DB_ProxyOld::mysql_result( $result49, 0, "totalcount" );

					if( $lostcount > 0 ) {
						echo "<br><a href=dashboard_esalestrack_accounts.php?show=8 target='salesdash' style=$style onclick=\"$('salesbox').hide();\"><font color=red size=2>&nbsp;Lost w/o Date ($lostcount)</font></a>";
					}

					/////gauges and settings
					if( $security_level >= 7 || $user == "johnmjr" || $user == "jimm" ) {
						echo "<br><a href=dashboard_esalestrack.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&show=5 target='salesdash' style=$style onclick=\"$('salesbox').hide();\"><font color=black><i>&nbsp;Settings</i></font></a> <br><a href=http://essentialpos.com/dashAPI/demo.php?nouser=taauto target='salesdash' style=$style onclick=\"$('salesbox').hide();\"><font color=black><i>&nbsp;Coming Soon</i></font></a>";
					}
					echo "</div></div> ";
				}
				else {
					echo "<div style=\" width:80px; background:#CCCCCC; border:1px solid #454545; float:left; margin-left:3px;\" onmouseover=\"$('salesbox').show();\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=6 style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Sales</font></center></a> </div>";
				}

				//////show last update
				if( $viewsub == 2 ) {
					$showupdate = "<font size=2 color=#888888><b>Updated $update_route&nbsp;</b></font>";
				}
				elseif( $viewsub == 5 ) {
					$showupdate = "<font size=2 color=#888888><b>Updated $update_service&nbsp;</b></font>";
				}
				elseif( $viewsub == 6 ) {
					$showupdate = "<font size=2 color=#888888><b>Updated $update_sales&nbsp;</b></font>";
				}
				else {
					$showupdate = "&nbsp;";
				}

				echo "</td><td style=\"border:2px solid #E8E7E7;\" align=right>$showupdate</td></tr>";

				///////show vending customer graph
				$showcust = 1;
			}
			//////////////////////////
			//////Foodservice Nav/////
			//////////////////////////
			else {
				echo "<tr bgcolor=#E8E7E7><td style=\"border:2px solid #E8E7E7;\">";

				//if($user=="SteveM"||$user=="EdS"||$user=="MMeurer"||$user=="JimM"||$user=="billp"||$user=="johnmjr"||$user=='ryanp'){
				if( $sec_dashboard > 1 ) {
					if( $viewsub == 10 ) {
						echo "<div style=\" width:80px; background:#FFFF99; border:1px solid #454545; float:left;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=10 style=$style><center><font color=black onMouseOver=this.color='#FF9900' onMouseOut=this.color='black'>Custom</font></center></a></div>";
					}
					else {
						echo "<div style=\" width:80px; background:#CCCCCC; border:1px solid #454545; float:left;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=10 style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Custom</font></center></a></div>";
					}
				}

				if( $viewsub == "" || $viewsub == 0 ) {
					echo "<div style=\" width:80px; background:#FFFF99; border:1px solid #454545; float:left; margin-left:3px;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=0 style=$style><center><font color=black onMouseOver=this.color='#FF9900' onMouseOut=this.color='black'>Overview</font></center></a></div>";
				}
				else {
					echo "<div style=\" width:80px; background:#CCCCCC; border:1px solid #454545; float:left; margin-left:3px;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=0 style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Overview</font></center></a></div>";
				}

				if( $viewsub == 1 ) {
					echo "<div style=\" width:80px; background:#FFFF99; border:1px solid #454545; float:left; margin-left:3px;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=1 style=$style><center><font color=black onMouseOver=this.color='#FF9900' onMouseOut=this.color='black'>Customers</font></center></a></div>";
				}
				else {
					echo "<div style=\" width:80px; background:#CCCCCC; border:1px solid #454545; float:left; margin-left:3px;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=1 style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Customers</font></center></a></div>";
				}

				if( $viewsub == 8 ) {
					echo "<div style=\" width:80px; background:#FFFF99; border:1px solid #454545; float:left; margin-left:3px;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=8 style=$style><center><font color=black onMouseOver=this.color='#FF9900' onMouseOut=this.color='black'>Expenses</font></center></a></div>";
				}
				else {
					echo "<div style=\" width:80px; background:#CCCCCC; border:1px solid #454545; float:left; margin-left:3px;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=8 style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Expenses</font></center></a></div>";
				}

				//if($viewsub==9){echo "<div style=\" width:80px; background:#FFFF99; border:1px solid #454545; float:left; margin-left:3px;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=9 style=$style><center><font color=black onMouseOver=this.color='#FF9900' onMouseOut=this.color='black'>CapEx</font></center></a></div>";}
				//else{echo "<div style=\" width:80px; background:#CCCCCC; border:1px solid #454545; float:left; margin-left:3px;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=9 style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>CapEx</font></center></a></div>";}

				if( $viewsub == 6 ) {
					echo "<div style=\" width:80px; background:#FFFF99; border:1px solid #454545; float:left; margin-left:3px;\" onmouseover=\"$('salesbox').show();\" onmouseout=\"$('salesbox').hide();\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=6 style=$style><center><font color=black onMouseOver=this.color='#FF9900' onMouseOut=this.color='black'>Sales</font></center></a> <div id='salesbox' onmouseout=\"$('salesbox').hide();\" style=\" width:99px; position: absolute; z-index: 2; border:1px solid #454545; background:#FFFFFF; display: none;\">&nbsp;<a href=dashboard_esalestrack.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&show=1 target='salesdash' style=$style onclick=\"$('salesbox').hide();\"><font color=blue>Sold/Pipeline</font></a> <br>&nbsp;<a href=dashboard_esalestrack.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&show=2 target='salesdash' style=$style onclick=\"$('salesbox').hide();\"><font color=blue>Lost</font></a> <br>  &nbsp;<a href=dashboard_esalestrack.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&show=4 target='salesdash' style=$style onclick=\"$('salesbox').hide();\"><font color=blue>Red List</font></a> &nbsp;<a href=dashboard_esalestrack.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&show=6 target='salesdash' style=$style onclick=\"$('salesbox').hide();\"><font color=blue>Gain/Loss</font></a> <br> &nbsp;<a href=dashboard_esalestrack.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&show=7 target='salesdash' style=$style onclick=\"$('salesbox').hide();\"><font color=blue>Sales Gap</font></a>";

					/////lost w/o dates
					$query49 = "SELECT COUNT(est_accounts.systemid) AS totalcount FROM est_accounts,est_accountowner WHERE est_accounts.datelost = '0000-00-00' AND est_accounts.customertype = '4' AND est_accounts.status = '2' AND est_accounts.accountowner = est_accountowner.accountowner AND est_accountowner.type > -1 ORDER BY est_accounts.datelost DESC";
					$result49 = Treat_DB_ProxyOld::query( $query49 );

					$lostcount = @Treat_DB_ProxyOld::mysql_result( $result49, 0, "totalcount" );

					if( $lostcount > 0 ) {
						echo "<br><a href=dashboard_esalestrack_accounts.php?show=8 target='salesdash' style=$style onclick=\"$('salesbox').hide();\"><font color=red size=2>&nbsp;Lost w/o Date ($lostcount)</font></a>";
					}

					/////gauges and settings
					if( $security_level > 8 || $user == "johnmjr" || $user == "jimm"
							|| $user == "billp" ) {
						echo "<br><a href=dashboard_esalestrack.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&show=5 target='salesdash' style=$style onclick=\"$('salesbox').hide();\"><font color=black><i>&nbsp;Settings</i></font></a> <br><a href=http://essentialpos.com/dashAPI/demo.php?nouser=taauto target='salesdash' style=$style onclick=\"$('salesbox').hide();\"><font color=black><i>&nbsp;Coming Soon</i></font></a>";
					}
					echo "</div></div>";
				}
				else {
					echo "<div style=\" width:80px; background:#CCCCCC; border:1px solid #454545; float:left; margin-left:3px;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=6 style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Sales</font></center></a></div>";
				}

				//////show last update
				if( $viewsub == 2 ) {
					$showupdate = "<font size=2 color=#888888><b>Updated $update_route&nbsp;</b></font>";
				}
				elseif( $viewsub == 6 ) {
					$showupdate = "<font size=2 color=#888888><b>Updated $update_sales&nbsp;</b></font>";
				}
				else {
					$showupdate = "&nbsp;";
				}

				echo "</td><td style=\"border:2px solid #E8E7E7;\" align=right>$showupdate</td></tr>";
			}
		}

		echo "</table><table width=90% cellpadding=1>";

		///////////////////////////
		///////OVERVIEW////////////
		///////////////////////////
		if( $viewtype > 0 && $viewtype < 7 && $viewbus != "" && ( $viewsub == "" || $viewsub == 0 ) ) {

			echo "<tr valign=top><td width=66.66%>";
			///////////////////////////
			//////END FILTER///////////
			//////START DASHBOARD//////
			///////////////////////////

			///////GET DATE RANGES

			$today = date( "Y-m-d" );
			$whathour = date( "G" );
			if( $whathour < 16 ) {
				$today = prevday( $today );
			}
			$thisday = date( "d" );
			if( $thismonth == "" ) {
				$thismonth = date( "m" );
			}
			if( $thisyear == "" ) {
				$thisyear = date( "Y" );
			}

			$ytdstart = "$thisyear-01-01";
			$mtdstart = "$thisyear-$thismonth-01";
			$monthend = $thismonth + 1;
			if( $monthend < 10 ) {
				$monthend = "0$monthend";
			}
			if( $monthend == 13 ) {
				$sp_year = $thisyear + 1;
				$monthend = "01";
				$monthend = "$sp_year-$monthend-01";
				$monthend = prevday( $monthend );
			}
			else {
				$monthend = "$thisyear-$monthend-01";
				$monthend = prevday( $monthend );
			}
			$enddate = "$thisyear-$thismonth-31";
			$yearend = "$thisyear-12-31";

			if( $thisyear != date( "Y" ) ) {
				$enddate = "$thisyear-12-31";
			}

			////////////////////////////////////////////////
			//////////////////GET INFO INTO ARRAY///////////
			////////////////////////////////////////////////

			////////restrict commissary sales from overall view
			if( $viewtype < 6 ) {
				$db_restrict = "AND business.db_restrict = '0'";
			}
			else {
				$db_restrict = "";
			}

			$query1 = "SELECT SUM(dashboard.sales) AS sales, SUM(dashboard.sales_bgt) AS sales_bgt, dashboard.date FROM dashboard,business$fromtables WHERE dashboard.businessid = business.businessid $db_restrict AND ($compquery) AND dashboard.date >= '$ytdstart' AND dashboard.date <='$yearend' GROUP BY date";
			$result1 = Treat_DB_ProxyOld::query( $query1 );
			$num1 = @Treat_DB_ProxyOld::mysql_numrows( $result1  );

			$num1--;
			//while($num1>=0){
			while( $r = Treat_DB_ProxyOld::mysql_fetch_array( $result1 ) ) {
				$date = $r["date"];
				$sales = $r["sales"];
				$sales_bgt = $r["sales_bgt"];

				$db_sales[$date] += $sales;
				$db_sales_bgt[$date] += $sales_bgt;

				if( dayofweek( $date ) == "Monday" ) {
					$db_daily_sales[Monday] += $sales;
					$db_daily_sales_bgt[Monday] += $sales_bgt;
				}
				elseif( dayofweek( $date ) == "Tuesday" ) {
					$db_daily_sales[Tuesday] += $sales;
					$db_daily_sales_bgt[Tuesday] += $sales_bgt;
				}
				elseif( dayofweek( $date ) == "Wednesday" ) {
					$db_daily_sales[Wednesday] += $sales;
					$db_daily_sales_bgt[Wednesday] += $sales_bgt;
				}
				elseif( dayofweek( $date ) == "Thursday" ) {
					$db_daily_sales[Thursday] += $sales;
					$db_daily_sales_bgt[Thursday] += $sales_bgt;
				}
				elseif( dayofweek( $date ) == "Friday" ) {
					$db_daily_sales[Friday] += $sales;
					$db_daily_sales_bgt[Friday] += $sales_bgt;
				}
				elseif( dayofweek( $date ) == "Saturday" ) {
					$db_daily_sales[Saturday] += $sales;
					$db_daily_sales_bgt[Saturday] += $sales_bgt;
				}
				elseif( dayofweek( $date ) == "Sunday" ) {
					$db_daily_sales[Sunday] += $sales;
					$db_daily_sales_bgt[Sunday] += $sales_bgt;
				}

				$what_month = substr( $date, 5, 2 );
				$month_sales[$what_month] += $sales;
				$month_sales_budget[$what_month] += $sales_bgt;

				if( $date <= $today ) {
					$db_sales_ytd[$date] += $sales;
					$db_sales_bgt_ytd[$date] += $sales_bgt;

					$month_sales_ytd[$what_month] += $sales;
					$month_sales_budget_ytd[$what_month] += $sales_bgt;
				}

				$year_sales += $sales;

				if( $sales_total2 < $month_sales[$what_month] ) {
					$sales_total2 = $month_sales[$what_month];
				}

				$num1--;
			}

			$tempdate = $ytdstart;
			while( $tempdate <= $yearend ) {
				if( $db_sales[$tempdate] > 0 ) {
					$numdays[dayofweek( $tempdate )]++;
				}
				$tempdate = nextday( $tempdate );
			}

			$prevyear = $thisyear;
			$prevmonth = $thismonth - 1;
			if( $prevmonth == 0 ) {
				$prevmonth = 12;
				$prevyear = $thisyear - 1;
			}
			elseif( $prevmonth < 10 ) {
				$prevmonth = "0$prevmonth";
			}

			$nextyear = $thisyear;
			$nextmonth = $thismonth + 1;
			if( $nextmonth == 13 ) {
				$nextmonth = "01";
				$nextyear = $thisyear + 1;
			}
			elseif( $nextmonth < 10 ) {
				$nextmonth = "0$nextmonth";
			}

			/////////////////////////////////////////////
			///////////////////TOTAL MONTH BUDGET////////
			/////////////////////////////////////////////

			$query1 = "SELECT SUM(vend_budget.amount) AS totalamt FROM vend_budget,business$fromtables WHERE vend_budget.businessid = business.businessid $db_restrict AND ($compquery) AND vend_budget.date >= '$mtdstart' ANd vend_budget.date <= '$monthend' AND vend_budget.type = '10'";
			$result1 = Treat_DB_ProxyOld::query( $query1 );

			$month_budget = @Treat_DB_ProxyOld::mysql_result( $result1, 0, "totalamt" );

			$query1 = "SELECT SUM(budget.`$thismonth`) AS totalamt FROM budget,business$fromtables WHERE budget.businessid = business.unit AND business.budget_type = '0' $db_restrict AND ($compquery) AND budget.year = '$thisyear' AND budget.type = '10'";
			$result1 = Treat_DB_ProxyOld::query( $query1 );

			$month_budget += @Treat_DB_ProxyOld::mysql_result( $result1, 0, "totalamt" );

			/////////////////////////////////////////////
			///////////////////TOTAL YEAR BUDGET/////////
			/////////////////////////////////////////////

			$query1 = "SELECT SUM(vend_budget.amount) AS totalamt FROM vend_budget,business$fromtables WHERE vend_budget.businessid = business.businessid $db_restrict AND ($compquery) AND vend_budget.date >= '$ytdstart' ANd vend_budget.date <= '$yearend' AND vend_budget.type = '10'";
			$result1 = Treat_DB_ProxyOld::query( $query1 );

			$year_budget = @Treat_DB_ProxyOld::mysql_result( $result1, 0, "totalamt" );

			$query1 = "SELECT SUM(budget.01+budget.02+budget.03+budget.04+budget.05+budget.06+budget.07+budget.08+budget.09+budget.10+budget.11+budget.12) AS totalamt FROM budget,business$fromtables WHERE budget.businessid = business.unit AND business.budget_type = '0' $db_restrict AND ($compquery) AND budget.year = '$thisyear' AND budget.type = '10'";
			$result1 = Treat_DB_ProxyOld::query( $query1 );

			$year_budget += @Treat_DB_ProxyOld::mysql_result( $result1, 0, "totalamt" );

			////////////////////////////////////////////////////////////////////
			///////////////BEGIN SALES AGGREGATE GRAPH//////////////////////////
			////////////////////////////////////////////////////////////////////
			if( $thismonth == 01 ) {
				$showtitle = "January $thisyear";
			}
			elseif( $thismonth == 02 ) {
				$showtitle = "February $thisyear";
			}
			elseif( $thismonth == 03 ) {
				$showtitle = "March $thisyear";
			}
			elseif( $thismonth == 04 ) {
				$showtitle = "April $thisyear";
			}
			elseif( $thismonth == 5 ) {
				$showtitle = "May $thisyear";
			}
			elseif( $thismonth == 6 ) {
				$showtitle = "June $thisyear";
			}
			elseif( $thismonth == 7 ) {
				$showtitle = "July $thisyear";
			}
			elseif( $thismonth == 8 ) {
				$showtitle = "August $thisyear";
			}
			elseif( $thismonth == 9 ) {
				$showtitle = "September $thisyear";
			}
			elseif( $thismonth == 10 ) {
				$showtitle = "October $thisyear";
			}
			elseif( $thismonth == 11 ) {
				$showtitle = "November $thisyear";
			}
			elseif( $thismonth == 12 ) {
				$showtitle = "December $thisyear";
			}

?>
						<br>
						<table width="100%" cellspacing="0" cellpadding="0" style="border:2px solid #e8e7e7;">
							<tr bgcolor="#e8e7e7">
								<td>
									<a href="/ta/businesstrack.php?thismonth=<?php
										echo $prevmonth;
									?>&amp;thisyear=<?php
										echo $prevyear;
									?>" style="<?php
										echo $style;
									?>">
										<font size="1" color="blue">
											[PREV]
										</font>
									</a>
									Sales Aggregate <?php echo $showtitle; ?>
									<a href="/ta/businesstrack.php?thismonth=<?php
										echo $nextmonth;
									?>&amp;thisyear=<?php
										echo $nextyear;
									?>" style="<?php
										echo $style;
									?>">
										<font size="1" color="blue">
											[NEXT]
										</font>
									</a>
								</td>
							</tr>
							<tr>
								<td>
									<div id="sales_ag" width="100%" style="visibility: <?php echo $showgraph; ?>;">
			<applet code="LineGraphApplet.class" archive="Linegraph.jar" width="100%" height=300">

			<!-- Start Up Parameters -->
			<param name="LOADINGMESSAGE" value="Creating Chart - Please Wait."/>
			<param name="STEXTCOLOR"     value="#000060"/>
			<param name="STARTUPCOLOR"   value="#FFFFFF"/>

			<!-- Line Data -->
			<!--   value,URL,Target Frame -->
<?php
			$series1 = "series1";
			$series2 = "series2";

			$counter = 1;
			$tempdate = $mtdstart;
			while( $tempdate <= $monthend ) {
				$dayname = dayofweek( $tempdate );
				if( $showwkds != 1 && ( $dayname == "Saturday" || $dayname == "Sunday" ) ) {}
				else {
					//$showsales+=$db_sales[$tempdate];
					$showbudget += $db_sales_bgt[$tempdate];
					echo "<PARAM name=data$counter$series1 value=$showbudget>";
					//if($tempdate<=$today){echo "<PARAM name=data$counter$series2 value=$showsales>";}

					$counter++;
				}

				$tempdate = nextday( $tempdate );
			}
			$counter = 1;
			$tempdate = $mtdstart;
			while( $tempdate <= $monthend ) {
				$dayname = dayofweek( $tempdate );
				if( $showwkds != 1 && ( $dayname == "Saturday" || $dayname == "Sunday" ) ) {}
				else {
					$showsales += $db_sales[$tempdate];
					//$showbudget+=$db_sales_bgt[$tempdate];
					//echo "<PARAM name=data$counter$series1 value=$showbudget>";
					if( $tempdate <= $today ) {
						echo "<PARAM name=data$counter$series2 value=$showsales>";
					}

					$counter++;
				}

				$tempdate = nextday( $tempdate );
			}

?>

			<!-- Properties -->
			<param name="ylabels"		  value="S|M|T|W|T|F|S"/>
			<param name="xlabels"		  value="S|M|T|W|T|F|S"/>
			<param name="titlefontsize"	  value="0"/>
			<param name="xtitlefontsize"	  value="0"/>
			<param name="ytitlefontsize"	  value="0"/>
			<param name="autoscale"            value="true"/>
			<param name="gridbgcolor"          value="#FFFFFF"/>
			<param name="series1"              value="red|6|5|false|dotted"/>
			<param name="series2"              value="blue|6|5|true|dotted"/>
			<param name="ndecplaces"           value="0"/>
			<param name="labelOrientation"     value="Up Angle"/>
			<param name="xlabel_pre"           value=""/>
			<param name="xlabel_font"          value="Arial,N,10"/>
<?php
			$counter = 1;
			$tempdate = $mtdstart;
			while( $tempdate <= $monthend ) {
				$dayname = dayofweek( $tempdate );
				if( $showwkds != 1 && ( $dayname == "Saturday" || $dayname == "Sunday" ) ) {}
				else {
					$g_day = substr( $tempdate, 8, 2 );
					$g_month = substr( $tempdate, 5, 2 );
					echo "<PARAM name='label$counter' value='$g_month/$g_day'>";

					$counter++;
				}

				$tempdate = nextday( $tempdate );
			}
?>
			</applet>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<table style="margin: 0; padding: 0; display: inline;">
										<tr height="4">
											<td width="10" bgcolor="#0000FF"></td>
											<td bgcolor="white">
												<font size="1">
													Sales
												</font>
											</td>
										</tr>
									</table>
									<table style="margin: 0; padding: 0; display: inline;">
										<tr height="4">
											<td width="10" bgcolor="#FF0000"></td>
											<td bgcolor="white">
												<font size="1">
													Budget
												</font>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
<?php

			/////////////////////////////////////////////
			//////////////////////SALES FOR MONTH////////
			/////////////////////////////////////////////

			//////FIGURE GRAPH TOTALS
			$sales_total = 0;
			$tempdate = $mtdstart;
			for( $counter = 1; $counter < 8; $counter++ ) {
				$dayname = dayofweek( $tempdate );
				if( $sales_total < ( $db_daily_sales[$dayname] / $numdays[$dayname] ) ) {
					$sales_total = $db_daily_sales[$dayname] / $numdays[$dayname];
				}
				$tempdate = nextday( $tempdate );
			}
			$sales_total = $sales_total * 1.3;
			$sales_total2 = $sales_total2 * 1.3;

			echo "<br><table width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td colspan=4><a href=businesstrack.php?thismonth=$prevmonth&thisyear=$prevyear style=$style><font size=1 color=blue>[PREV]</font></a> Daily Sales/Budget/Average YTD $showtitle <a href=businesstrack.php?thismonth=$nextmonth&thisyear=$nextyear style=$style><font size=1 color=blue>[NEXT]</font></a></td></tr><tr><td colspan=4>";

			echo "<table cellspacing=0 cellpadding=0 width=100% background=grid.jpg><tr height=200>";
			$tempdate = $mtdstart;
			while( $tempdate <= $monthend ) {
				$dayname = dayofweek( $tempdate );

				$avg = $db_daily_sales[$dayname] / $numdays[$dayname];

				$graph1 = hgraph( $db_sales_ytd[$tempdate], $sales_total, '#0000FF', 8 );
				$graph2 = hgraph( $db_sales_bgt_ytd[$tempdate], $sales_total, '#FF0000', 8 );
				$graph3 = hgraph( $avg, $sales_total, '#00FF00', 8 );
				if( $showwkds != 1 && ( $dayname == "Saturday" || $dayname == "Sunday" ) ) {}
				else {
					echo "$graph1$graph2$graph3<td width=8>&nbsp;</td>";
				}
				$tempdate = nextday( $tempdate );
			}
			echo "</tr>";
			echo "<tr bgcolor=white>";
			$tempdate = $mtdstart;
			while( $tempdate <= $monthend ) {
				$dayname = dayofweek( $tempdate );
				$is_day = substr( $tempdate, 8, 2 );
				if( $showwkds != 1 && ( $dayname == "Saturday" || $dayname == "Sunday" ) ) {}
				else {
					echo "<td colspan=3 style=\"border-style: solid; border-color: black; border-width: 0px 1px 1px 1px;\"><font size=1><center>$is_day</center></font></td><td></td>";
				}
				$tempdate = nextday( $tempdate );
			}
			echo "<td></td></tr>";

			echo "</table>";
			echo "</td></tr>";
			echo "<tr><td> <table style=\"margin:0;padding:0;display:inline;\"><tr height=4><td width=10 bgcolor=#0000FF></td><td bgcolor=white><font size=1>Sales</td></tr></table> <table style=\"margin:0;padding:0;display:inline;\"><tr height=4><td width=10 bgcolor=#FF0000></td><td bgcolor=white><font size=1>Budget</td></tr></table> <table style=\"margin:0;padding:0;display:inline;\"><tr height=4><td width=10 bgcolor=#00FF00></td><td bgcolor=white><font size=1>Daily Average (YTD)</td></tr></table> </td></tr>";
			echo "</table>";

			/////////////////////////////////////////////
			//////////////////////SALES FOR YEAR/////////
			/////////////////////////////////////////////
			$prevyear = $thisyear - 1;
			$nextyear = $thisyear + 1;

			echo "<br><table width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td colspan=4><a href=businesstrack.php?thismonth=$thismonth&thisyear=$prevyear style=$style><font size=1 color=blue>[PREV]</font></a> Monthly Sales/Budget $thisyear <a href=businesstrack.php?thismonth=$thismonth&thisyear=$nextyear style=$style><font size=1 color=blue>[NEXT]</font></a></td></tr><tr><td colspan=4>";

			echo "<table cellspacing=0 cellpadding=0 width=100% background=grid.jpg><tr height=200><td width=15>&nbsp;</td>";
			$tempdate = $ytdstart;
			for( $counter = 1; $counter <= 12; $counter++ ) {
				if( $counter < 10 ) {
					$what_month = "0$counter";
				}
				else {
					$what_month = $counter;
				}

				$diff = round(
						$month_sales_ytd[$what_month] - $month_sales_budget_ytd[$what_month], 2 );
				$graph1 = hgraph( $month_sales_ytd[$what_month], $sales_total2, '#0000FF', 20,
						$diff );
				$graph2 = hgraph( $month_sales_budget_ytd[$what_month], $sales_total2, '#FF0000',
						20 );
				echo "$graph1$graph2<td width=30>&nbsp;</td>";

				$tempdate = nextday( $tempdate );
			}
			echo "<td></td></tr>";
			echo "<tr bgcolor=white>";
			echo "<td>&nbsp;</td><td colspan=2 style=\"border-style: solid; border-color: black; border-width: 0px 1px 1px 1px;\"><font size=2><center>JAN</center></font></td><td></td><td colspan=2 style=\"border-style: solid; border-color: black; border-width: 0px 1px 1px 1px;\"><font size=2><center>FEB</center></font></td><td></td><td colspan=2 style=\"border-style: solid; border-color: black; border-width: 0px 1px 1px 1px;\"><font size=2><center>MAR</center></font></td><td></td><td colspan=2 style=\"border-style: solid; border-color: black; border-width: 0px 1px 1px 1px;\"><font size=2><center>APR</center></font></td><td></td><td colspan=2 style=\"border-style: solid; border-color: black; border-width: 0px 1px 1px 1px;\"><font size=2><center>MAY</center></font></td><td></td><td colspan=2 style=\"border-style: solid; border-color: black; border-width: 0px 1px 1px 1px;\"><font size=2><center>JUN</center></font></td><td></td><td colspan=2 style=\"border-style: solid; border-color: black; border-width: 0px 1px 1px 1px;\"><font size=2><center>JUL</center></font></td><td></td><td colspan=2 style=\"border-style: solid; border-color: black; border-width: 0px 1px 1px 1px;\"><font size=2><center>AUG</center></font></td><td></td><td colspan=2 style=\"border-style: solid; border-color: black; border-width: 0px 1px 1px 1px;\"><font size=2><center>SEP</center></font></td><td></td><td colspan=2 style=\"border-style: solid; border-color: black; border-width: 0px 1px 1px 1px;\"><font size=2><center>OCT</center></font></td><td></td><td colspan=2 style=\"border-style: solid; border-color: black; border-width: 0px 1px 1px 1px;\"><font size=2><center>NOV</center></font></td><td></td><td colspan=2 style=\"border-style: solid; border-color: black; border-width: 0px 1px 1px 1px;\"><font size=2><center>DEC</center></font></td><td></td>";
			echo "<td></td></tr>";

			echo "</table>";
			echo "</td></tr>";
			echo "<tr><td> <table style=\"margin:0;padding:0;display:inline;\"><tr height=4><td width=10 bgcolor=#0000FF></td><td bgcolor=white><font size=1>Sales</td></tr></table> <table style=\"margin:0;padding:0;display:inline;\"><tr height=4><td width=10 bgcolor=#FF0000></td><td bgcolor=white><font size=1>Budget</td></tr></table> </td></tr>";
			echo "</table>";

			////////////////////////////////////////////////////////////////////
			///////////////BEGIN SALES GRAPH////////////////////////////////////
			////////////////////////////////////////////////////////////////////
?>
						<br>
						<table width="100%" cellspacing="0" cellpadding="0" style="border: 2px solid #e8e7e7;">
							<tr bgcolor="#e8e7e7">
								<td>
									Sales/Budget Last 25 Days
								</td>
							</tr>
							<tr>
								<td>
									<div id="sales_day" width="100%" style="visibility: <?php echo $showgraph; ?>;">
			<applet code="LineGraphApplet.class" archive="Linegraph.jar" width="100%" height=200">

			<!-- Start Up Parameters -->
			<PARAM name="LOADINGMESSAGE" value="Creating Chart - Please Wait.">
			<PARAM name="STEXTCOLOR"     value="#000060">
			<PARAM name="STARTUPCOLOR"   value="#FFFFFF">

			<!-- Line Data -->
			<!--   value,URL,Target Frame -->
<?php
			$series1 = "series1";
			$series2 = "series2";
			$tempdate = $today;
			for( $counter = 1; $counter <= 24; $counter++ ) {
				$tempdate = prevday( $tempdate );
				if( dayofweek( $tempdate ) == "Sunday" && $showwkds != 1 ) {
					$tempdate = prevday( $tempdate );
				}
				if( dayofweek( $tempdate ) == "Saturday" && $showwkds != 1 ) {
					$tempdate = prevday( $tempdate );
				}
			}
			$tempdate2 = $tempdate;

			$counter = 1;
			while( $tempdate <= $today ) {
				$dayname = dayofweek( $tempdate );
				if( $showwkds != 1 && ( $dayname == "Saturday" || $dayname == "Sunday" ) ) {}
				else {
					echo "<PARAM name=data$counter$series1 value=$db_sales_bgt[$tempdate]>";
					echo "<PARAM name=data$counter$series2 value=$db_sales[$tempdate]>";

					$counter++;
				}

				$tempdate = nextday( $tempdate );
			}
?>

			<!-- Properties -->
			<PARAM name="ylabels"		  value="S|M|T|W|T|F|S">
			<PARAM name="xlabels"		  value="S|M|T|W|T|F|S">
			<PARAM name="titlefontsize"	  value="0">
			<PARAM name="xtitlefontsize"	  value="0">
			<PARAM name="ytitlefontsize"	  value="0">
			<PARAM name="autoscale"            value="true">
			<PARAM name="gridbgcolor"          value="#FFFFFF">
			<PARAM name="series1"              value="red|6|5|false|dotted">
			<PARAM name="series2"              value="blue|6|5|true|dotted">
			<PARAM name="ndecplaces"           value="0">
			<PARAM name="labelOrientation"     value="Up Angle">
			<PARAM name="xlabel_pre"           value="">
			<PARAM name="xlabel_font"          value="Arial,N,10">
<?
			$counter = 1;
			$tempdate = $tempdate2;
			while( $tempdate <= $today ) {
				$dayname = dayofweek( $tempdate );
				if( $showwkds != 1 && ( $dayname == "Saturday" || $dayname == "Sunday" ) ) {}
				else {
					$g_day = substr( $tempdate, 8, 2 );
					$g_month = substr( $tempdate, 5, 2 );
					echo "<PARAM name='label$counter' value='$g_month/$g_day'>";

					$counter++;
				}

				$tempdate = nextday( $tempdate );
			}
?>
			</applet>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<table style="margin: 0; padding: 0; display: inline;">
										<tr height="4">
											<td width="10" bgcolor="#0000ff"></td>
											<td bgcolor="white">
												<font size="1">
													Sales
												</font>
											</td>
										</tr>
									</table>
									<table style="margin: 0; padding: 0; display: inline;">
										<tr height="4">
											<td width="10" bgcolor="#ff0000"></td>
											<td bgcolor="white">
												<font size="1">
													Budget
												</font>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<p></p>
<?php

			//////////////page break

			echo "</td><td width=33.33%>";

			//////////////////////////////////////////////////////////
			/////////////////MONTH & YEAR SALES/BUDGET THERMOMETER////
			//////////////////////////////////////////////////////////
			$mytitle = explode( ' ', $showtitle );

			$mytitle[0] = strtoupper( substr( $mytitle[0], 0, 3 ) );

			echo "<br><table width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td colspan=4>Sales % of Budget $showtitle</td></tr><tr><td colspan=2 width=50%><center>";

			$showpercent = round( $month_sales[$thismonth] / $month_budget * 100, 1 );
			$len = strlen( $showpercent );
			while( $len < 3 ) {
				$showpercent = "&nbsp;$showpercent";
				$len++;
			}
			echo "<table cellspacing=0 cellpadding=0 border=0 width=55>";
			$graph1 = hgraph( $month_sales[$thismonth], $month_budget, '#414141', 12, "", "", "",
					"thermmonth" );
			$graph2 = hgraph( $month_budget, $month_budget, 'white', 20, 0, 'white', 10, "", "" );

			echo "<tr height=220><td width=11><img src=clear.gif></td>$graph1$graph2</tr>";
			echo "<tr height=40><td width=25 colspan=3 align=left style=\"background-image:url('beaker3.jpg'); background-repeat: no-repeat;\"><div id='thermmonth_pcnt' style=\"align:center;color:#414141;font-size:10pt;font-weight:bold; padding-left:7px;\">$showpercent</div></td></tr>";
			echo "<tr height=4><td colspan=3><center><font size=2>$mytitle[0]</font></center></td></tr>";
			echo "</table></center>";

			echo "</td><td colspan=2 width=50%><center>";

			$showpercent = round( $year_sales / $year_budget * 100, 1 );
			$len = strlen( $showpercent );
			while( $len < 3 ) {
				$showpercent = "&nbsp;$showpercent";
				$len++;
			}
			echo "<table cellspacing=0 cellpadding=0 border=0 width=55>";
			$graph1 = hgraph( $year_sales, $year_budget, '#414141', 12, "", "", "", "thermyear" );
			$graph2 = hgraph( $year_budget, $year_budget, 'white', 20, 0, 'white', 10 );

			echo "<tr height=220><td width=11><img src=clear.gif></td>$graph1$graph2</tr>";
			echo "<tr height=40><td width=25 colspan=3 align=left style=\"background-image:url('beaker3.jpg'); background-repeat: no-repeat;\"><div id='thermyear_pcnt' style=\"align:center;color:#414141;font-size:10pt;font-weight:bold; padding-left:7px;\">$showpercent</div></td></tr>";
			echo "<tr height=4><td colspan=3><center><font size=2>$mytitle[1]</font></td></tr>";
			echo "</table></center>";

			echo "</center></td></tr></table>";

			/////////////////
			/////AGING///////
			/////////////////
			if( $viewtype == 6 && $viewsales > 0 ) {
				$query1 = "SELECT SUM(dashboard_aging_salesmen.zero) AS zero,SUM(dashboard_aging_salesmen.thirty) AS thirty,SUM(dashboard_aging_salesmen.sixty) AS sixty,SUM(dashboard_aging_salesmen.ninety) AS ninety FROM dashboard_aging_salesmen WHERE dashboard_aging_salesmen.salesmanid = '$viewsales'";
				$result1 = Treat_DB_ProxyOld::query( $query1 );

				$zero = @Treat_DB_ProxyOld::mysql_result( $result1, 0, "zero" );
				$thirty = @Treat_DB_ProxyOld::mysql_result( $result1, 0, "thirty" );
				$sixty = @Treat_DB_ProxyOld::mysql_result( $result1, 0, "sixty" );
				$ninety = @Treat_DB_ProxyOld::mysql_result( $result1, 0, "ninety" );
			}
			else {
				$query1 = "SELECT SUM(dashboard_aging.zero) AS zero,SUM(dashboard_aging.thirty) AS thirty,SUM(dashboard_aging.sixty) AS sixty,SUM(dashboard_aging.ninety) AS ninety FROM dashboard_aging,business$fromtables WHERE dashboard_aging.businessid = business.businessid AND ($compquery)";
				$result1 = Treat_DB_ProxyOld::query( $query1 );

				$zero = @Treat_DB_ProxyOld::mysql_result( $result1, 0, "zero" );
				$thirty = @Treat_DB_ProxyOld::mysql_result( $result1, 0, "thirty" );
				$sixty = @Treat_DB_ProxyOld::mysql_result( $result1, 0, "sixty" );
				$ninety = @Treat_DB_ProxyOld::mysql_result( $result1, 0, "ninety" );
			}


			if( $zero < 0 ) {
				$s_zero = 0;
			}
			else {
				$s_zero = $zero;
			}
			if( $thirty < 0 ) {
				$s_thirty = 0;
			}
			else {
				$s_thirty = $thirty;
			}
			if( $sixty < 0 ) {
				$s_sixty = 0;
			}
			else {
				$s_sixty = $sixty;
			}
			if( $ninety < 0 ) {
				$s_ninety = 0;
			}
			else {
				$s_ninety = $ninety;
			}

			if( $viewtype == 6 ) {
				$mytarget = "businvoice.php?cid=$companyid&bid=$viewbus&ageme=1";
			}
			else {
				$mytarget = "businesstrack.php";
			}

			echo "<br><table width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td colspan=4><a href=$mytarget style=$style><font color=black onMouseOver=this.color='#FF9900' onMouseOut=this.color='black' title='View'>Aging</font></a>";

			//////////BY SALESMAN
			$query11 = "SELECT * FROM vend_salesman WHERE businessid = '$viewbus' ORDER BY sales_name DESC";
			$result11 = Treat_DB_ProxyOld::query( $query11 );
			$num11 = @Treat_DB_ProxyOld::mysql_numrows( $result11  );

			if( $num11 > 0 && $viewtype == 6 ) {
				echo "&nbsp;&nbsp;<form action=businesstrack.php method=post style=\"margin:0;padding:0;display:inline;\"><select name=viewsalesman onChange='changePage(this.form.viewsalesman)' style=\"font:10px;\"><option value=businesstrack.php?viewbus=$viewbus&viewtype=6&viewsales=0>All Salesmen</option>";

				$num11--;
				while( $num11 >= 0 ) {
					$salesmanid = @Treat_DB_ProxyOld::mysql_result( $result11, $num11, "salesid" );
					$salesmanname = @Treat_DB_ProxyOld::mysql_result( $result11, $num11, "sales_name" );

					if( $salesmanid == $viewsales ) {
						$showsel9 = "SELECTED";
					}
					else {
						$showsel9 = "";
					}

					echo "<option value=businesstrack.php?viewbus=$viewbus&viewtype=6&viewsales=$salesmanid&thismonth=$thismonth&thisyear=$thisyear $showsel9>$salesmanname</option>";
					$num11--;
				}
				echo "</select></form>";
			}

			////build data for pie chart (remove zero's)
			$piedata = "";
			if( $s_zero > 0 ) {
				$piedata = "$s_zero";
			}
			if( $s_thirty > 0 && $piedata != "" ) {
				$piedata .= "*$s_thirty";
			}
			elseif( $s_thirty > 0 && $piedata == "" ) {
				$piedata = "$s_thirty";
			}
			if( $s_sixty > 0 && $piedata != "" ) {
				$piedata .= "*$s_sixty";
			}
			elseif( $s_sixty > 0 && $piedata == "" ) {
				$piedata = "$s_sixty";
			}
			if( $s_ninety > 0 && $piedata != ""
					&& ( $s_ninety / ( $s_zero + $s_thirty + $s_sixty + $s_ninety ) >= .01 ) ) {
				$piedata .= "*$s_ninety";
			}
			elseif( $s_ninety > 0 && $piedata == ""
					&& ( $s_ninety / ( $s_zero + $s_thirty + $s_sixty + $s_ninety ) >= .01 ) ) {
				$piedata = "$s_ninety";
			}

			echo "</td></tr><tr height=9><td></td></tr><tr><td colspan=4>";
			echo "<img src='piechart.php?pie_color=E8E8E8~FFFF00~FF9900~FF0000&data=$piedata&label=<30*30-60*60-90*>90'/ height=115 width=309> ";
			echo "</td></tr><tr height=24><td width=25%>";

			$zero = number_format( $zero, 2 );
			$thirty = number_format( $thirty, 2 );
			$sixty = number_format( $sixty, 2 );
			$ninety = number_format( $ninety, 2 );

			echo "<table><tr height=4><td width=10 bgcolor=#E8E8E8></td><td bgcolor=white><font size=1>$zero</td></tr></table> </td><td width=25%><table><tr height=4><td width=10 bgcolor=#FFFF00></td><td><font size=1>$thirty</td></tr></table> </td><td width=25%><table><tr height=4><td width=10 bgcolor=#FF9900></td><td><font size=1>$sixty</td></tr></table> </td><td width=25%><table><tr height=4><td width=10 bgcolor=#FF0000></td><td><font size=1>$ninety</td></tr></table>";

			echo "</td></tr></table>";

			/////////////////////////
			//////ACCOUNTS///////////
			/////////////////////////
			if( $viewtype >= 2 ) {
				echo "<br><table width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td colspan=4><a href=dashboard_accounts.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&ytdstart=$ytdstart&v=all&sort=1 target='_blank' style=$style><font color=black onMouseOver=this.color='#FF9900' onMouseOut=this.color='black' title='Show All'>Top 10 Accounts (YTD)</font></a></td></tr><tr><td><iframe id='dashboardaccounts' src='dashboard_accounts.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&ytdstart=$ytdstart&sort=1' WIDTH=100% HEIGHT=175 frameborder=0></iframe></td></tr></table>";
			}

			///////////////////////
			////////ROUTES/////////
			///////////////////////

			$query1 = "SELECT * FROM vend_locations,business$fromtables WHERE vend_locations.unitid = business.businessid AND ($compquery)";
			$result1 = Treat_DB_ProxyOld::query( $query1 );
			$num1 = @Treat_DB_ProxyOld::mysql_numrows( $result1  );

			if( $num1 > 0 && $viewtype >= 2 ) {
				echo "<br><table width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td colspan=4><a href=dashboard_route.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&ytdstart=$ytdstart&sort=1&v=all target='_blank' style=$style><font color=black onMouseOver=this.color='#FF9900' onMouseOut=this.color='black' title='Show All'>Top 10 Routes (YTD)</font></a></td></tr><tr><td><iframe src='dashboard_route.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&ytdstart=$ytdstart&sort=1' WIDTH=100% HEIGHT=175 frameborder=0></iframe></td></tr></table>";

				echo "<br><table width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td colspan=4><a href=dashboard_route.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&ytdstart=$ytdstart&v=all target='_blank' style=$style><font color=black onMouseOver=this.color='#FF9900' onMouseOut=this.color='black' title='Show All'>Bottom 10 Routes (YTD)</font></a></td></tr><tr><td><iframe src='dashboard_route.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&ytdstart=$ytdstart' WIDTH=100% HEIGHT=175 frameborder=0></iframe></td></tr></table>";

				//echo "<br><table width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td colspan=4><a href=dashboard_mach_sales.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&ytdstart=$ytdstart&sort=1&view=all target='_blank' style=$style><font color=black onMouseOver=this.color='#FF9900' onMouseOut=this.color='black' title='Show All'>Top 10 VGroups (YTD)</font></a></td></tr><tr><td><iframe src='dashboard_mach_sales.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&ytdstart=$ytdstart&sort=1' WIDTH=100% HEIGHT=175 frameborder=0></iframe></td></tr></table>";

				//echo "<br><table width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td colspan=4><a href=dashboard_mach_service.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&ytdstart=$ytdstart&v=all&sort=1 target='_blank' style=$style><font color=black onMouseOver=this.color='#FF9900' onMouseOut=this.color='black' title='Show All'>Top 10 Complaints/Repairs (YTD)</font></a></td></tr><tr><td><iframe src='dashboard_mach_service.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&ytdstart=$ytdstart&sort=1' WIDTH=100% HEIGHT=175 frameborder=0></iframe></td></tr></table>";
			}


			/////////////////////////
			////////////MEMOS////////
			/////////////////////////
			if( $viewtype == 6 ) {
				echo "<br><table width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td colspan=4>Memos</td></tr><tr><td><iframe src='dashboard_memos.php?cid=$companyid&viewbus=$viewbus' WIDTH=100% HEIGHT=135 frameborder=0></iframe></td></tr></table>";
			}

			echo "</td></tr>";
		}
		//////////////////////////////
		//////////CUSTOMERS///////////
		//////////////////////////////
		elseif( $viewtype > 0 && $viewtype < 7 && $viewbus != "" && $viewsub == 1 ) {
			echo "<tr><td colspan=2>";

			///////same account sales
			if( $showcust == 1 ) {
				echo "<p><br><table width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td>Same Account Sales</td></tr>";
				echo "<tr><td>";
				echo "<div width=100% id='sales_ag' style=\"visibility:$showgraph;\"><iframe src='"
						. HTTP_EXPORTS
						. "/dashdivsales.html' WIDTH=100% HEIGHT=500 frameborder=0></iframe></div>";
				echo "</td></tr>";
				echo "</table>";
			}
			else {
				echo "<br>";
			}

			//////contracts
			echo "<p><table width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td>Contract Information: <a href=dashboard_esalestrack.php?show=8&viewtype=$viewtype&viewbus=$viewbus target='contract' style=$style><font color=blue>Expirations</font></a> | <a href=dashboard_esalestrack.php?show=8&viewtype=$viewtype&viewbus=$viewbus&contr=3 target='contract' style=$style><font color=blue>Expired</font></a> | <a href=dashboard_esalestrack.php?show=8&viewtype=$viewtype&viewbus=$viewbus&contr=2 target='contract' style=$style><font color=blue>Month to Month</font></a> | <a href=dashboard_esalestrack.php?show=8&viewtype=$viewtype&viewbus=$viewbus&contr=1 target='contract' style=$style><font color=blue>Unknown</font></a></td></tr>";
			echo "<tr><td>";
			echo "<iframe src='dashboard_esalestrack.php?show=8&viewtype=$viewtype&viewbus=$viewbus' WIDTH=100% HEIGHT=400 frameborder=0 name='contract'></iframe>";
			echo "</td></tr>";
			echo "</table>";

			//////list files
			echo "<p><table width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td>Files</td></tr>";
			echo "<tr><td>";

			$query1 = "SELECT dashboard_files.* FROM dashboard_files,business$fromtables WHERE dashboard_files.businessid = business.businessid AND ($compquery) ORDER BY dashboard_files.businessid,dashboard_files.display_name";
			$result1 = Treat_DB_ProxyOld::query( $query1 );
			$num1 = @Treat_DB_ProxyOld::mysql_numrows( $result1  );

			if( $num1 == 0 ) {
				echo "<i>&nbsp;Nothing to Display</i>";
			}

			echo "<table width=100%>";
			$counter = 1;
			while( $r = Treat_DB_ProxyOld::mysql_fetch_array( $result1 ) ) {
				$fileid = $r["id"];
				$newbusid = $r["businessid"];
				$filename = $r["file_name"];
				$display_name = $r["display_name"];

				$query2 = "SELECT businessname FROM business WHERE businessid = '$newbusid'";
				$result2 = Treat_DB_ProxyOld::query( $query2 );

				if( $counter == 1 ) {
					echo "<tr>";
				}

				if( $security_level > 8 ) {
					$showdelete = "<a href=dashboard_files.php?edit=1&id=$fileid&viewsub=$viewsub onclick=\"javascript: return confirm('Are you sure you want to delete this file?');\"><img src=delete.gif height=16 width=16 border=0></a>";
				}
				else {
					$showdelete = "";
				}

				$businessname = @Treat_DB_ProxyOld::mysql_result( $result2, 0, "businessname" );
				echo "<td width=50%><a href='" . HTTP_DASHBOARD_FILES
						. "/$filename' style=$style><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue' size=2><b>$display_name ($businessname)</b></font></a> $showdelete</td>";

				if( $counter == 3 ) {
					echo "</tr>";
				}

				$counter++;
				if( $counter == 3 ) {
					$counter = 1;
				}
			}
			if( $counter == 2 ) {
				echo "<td width=50%>&nbsp;</td></tr>";
			}

			echo "</table>";

			echo "</td></tr></table>";

			if( $security_level > 8 || $user == "DaveAhles" ) {
				echo "<p><table width=100%><tr valign=top><td width=50%>";

				echo "<table width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td>Upload Files</td></tr>";
				echo "<tr><td><i>*Very Important: Files MUST start with a five digit unit number and a dash (ie. '75000-').</i></td></tr>";
				echo "<tr><td><i><b>*<u>Directions</u>:</b> Click Browse and select the files you want to upload. After files load, click Upload and wait for them to upload. After all files have uploaded, click Publish to assign files to units.</i></td></tr>";
				echo "<tr><td><form action=dashboard_files.php  enctype='multipart/form-data' method=post style=\"margin:0;padding:0;display:inline;\"><input type=hidden name=viewsub value=$viewsub>";
				echo "<input type=file name=uploadedfile id=\"uploadify\"> <input type=submit value='UPLOAD' onclick=\"\$J('#uploadify').uploadifyUpload(); return false\" style=\"border: 1px solid #FFFFFF; width: 110px; height: 30px; font-size: 14px; color: #FFFFFF; background-color:#444444;\"></form>";
				echo "<form action=dashboard_files_publish.php method=post style=\"margin:0;padding:0;display:inline;\"><input type=submit value='PUBLISH' style=\"border: 1px solid #FFFFFF; width: 110px; height: 30px; font-size: 14px; color: #FFFFFF; background-color:#444444;\"></form>";
				echo "</td></tr></table>";

				/////mass delete files
				echo "</td><td width=50%>";
				echo "<form action=dashboard_files.php method=post><input type=hidden name=edit value=2>";
				echo "<table width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td colspan=2>Delete Files</td></tr>";

				$query1 = "SELECT * FROM dashboard_files ORDER BY display_name";
				$result1 = Treat_DB_ProxyOld::query( $query1 );

				while( $r = Treat_DB_ProxyOld::mysql_fetch_array( $result1 ) ) {
					$id = $r["id"];
					$filebusid = $r["businessid"];
					$file_name = $r["file_name"];
					$display_name = $r["display_name"];

					echo "<tr><td align=right><input type=checkbox name=$id value=1 CHECKED></td><td>$display_name</td></tr>";
				}
				echo "<tr><td colspan=2 align=right><input type=submit value='DELETE' onclick=\"return confirm('Are you sure you want to delete these files?');\" style=\"border: 1px solid #FFFFFF; width: 110px; height: 30px; font-size: 14px; color: #FFFFFF; background-color:#444444;\"></td></tr>";
				echo "</table></form>";

				echo "</td></tr></table>";
			}
			/*
					////////////////////FIGURE DATES OF LAST 8 WEEKS

					$tempdate=date("Y-m-d");

					$week[1]=0;
					$counter=9;

					while($week[1]==0){
							while(dayofweek($tempdate)!=$weekend){$tempdate=prevday($tempdate);}
							$week[$counter]=$tempdate;

							$tempdate=prevday($tempdate);
							$counter--;
					}

					echo "<table width=100%>";
					echo "<tr valign=top><td width=33%>";
					////////FIRST COLUMN
							////////////////VENDING NAVIGATION
							$viewroute = isset($_GET["viewr"])?$_GET["viewr"]:'';
							if($viewroute==""&&$viewtype>=6){$viewroute="b-$viewbus";}
							$viewroute=explode("-",$viewroute);

							echo "<br><table width=100% cellspacing=0 cellpadding=0 style=\"border:1px solid black;\"><tr bgcolor=#FFFF99><td>View</td></tr><tr><td><form method=post action=businesstrack.php style=\"margin:0;padding:0;display:inline;\"><select name=viewroute onChange='changePage(this.form.viewroute)' size=6 style=\"width:100%;\" >";

							if($viewroute[0]==""){echo "<optgroup label='All'>";}
							elseif($viewroute[0]=="b"&&$viewtype<6){echo "<option value=businesstrack.php?viewbus=$viewbus&viewtype=$viewtype&viewsub=1&thismonth=$thismonth&thisyear=$thisyear>Up</option>";}
							elseif($viewroute[0]=="s"){
									$query1="SELECT businessid FROM vend_supervisor WHERE supervisorid = '$viewroute[1]'";
									$result1 = Treat_DB_ProxyOld::query($query1);

									$businessid=@mysql_result($result1,0,"businessid");

									echo "<option value=businesstrack.php?viewbus=$viewbus&viewtype=$viewtype&viewsub=1&thismonth=$thismonth&thisyear=$thisyear&viewr=b-$businessid>Up</option>";
							}
							elseif($viewroute[0]=="r"){
									$query1="SELECT vend_supervisor.* FROM vend_supervisor,login_route WHERE login_route.login_routeid = '$viewroute[1]' AND login_route.supervisor = vend_supervisor.supervisorid";
									$result1 = Treat_DB_ProxyOld::query($query1);

									$supervisorid=@mysql_result($result1,0,"supervisorid");

									echo "<option value=businesstrack.php?viewbus=$viewbus&viewtype=$viewtype&viewsub=1&thismonth=$thismonth&thisyear=$thisyear&viewr=s-$supervisorid>Up</option>";
							}

							///////////CHOOSE
							if($viewroute[0]==""){
									$query1="SELECT DISTINCT business.businessid,business.businessname FROM business$fromtables WHERE $compquery";
									$result1 = Treat_DB_ProxyOld::query($query1);

									while($r=mysql_fetch_array($result1)){
											$businessid=$r["businessid"];
											$business_name=$r["businessname"];

											$query2="SELECT * FROM vend_locations WHERE vend_locations.unitid = '$businessid'";
											$result2 = Treat_DB_ProxyOld::query($query2);
											$num2 = @mysql_numrows( $result2 );

											if($num2>0){
													echo "<option value=businesstrack.php?viewbus=$viewbus&viewtype=$viewtype&viewsub=1&thismonth=$thismonth&thisyear=$thisyear&viewr=b-$businessid>$business_name</option>";
											}
									}
							}
							elseif($viewroute[0]=="b"){
									$query1="SELECT businessname FROM business WHERE businessid = '$viewroute[1]'";
									$result1 = Treat_DB_ProxyOld::query($query1);

									$showname=@mysql_result($result1,0,"businessname");

									echo "<optgroup label='$showname'>";

									$query1="SELECT * FROM vend_supervisor WHERE businessid = '$viewroute[1]' ORDER BY supervisor_name";
									$result1 = Treat_DB_ProxyOld::query($query1);

									while($r=mysql_fetch_array($result1)){
											$supervisorid=$r["supervisorid"];
											$supervisor_name=$r["supervisor_name"];

											echo "<option value=businesstrack.php?viewbus=$viewbus&viewtype=$viewtype&viewsub=1&thismonth=$thismonth&thisyear=$thisyear&viewr=s-$supervisorid>$supervisor_name</option>";
									}
									echo "</optgroup>";
							}
							elseif($viewroute[0]=="s"){
									$query1="SELECT supervisor_name FROM vend_supervisor WHERE supervisorid = '$viewroute[1]'";
									$result1 = Treat_DB_ProxyOld::query($query1);

									$showname=@mysql_result($result1,0,"supervisor_name");

									echo "<optgroup label='$showname'>";

									$query1="SELECT * FROM login_route WHERE supervisor = '$viewroute[1]' ORDER BY route";
									$result1 = Treat_DB_ProxyOld::query($query1);

									while($r=mysql_fetch_array($result1)){
											$login_routeid=$r["login_routeid"];
											$route=$r["route"];

											echo "<option value=businesstrack.php?viewbus=$viewbus&viewtype=$viewtype&viewsub=1&thismonth=$thismonth&thisyear=$thisyear&viewr=r-$login_routeid>$route</option>";
									}
									echo "</optgroup>";
							}
							elseif($viewroute[0]=="r"){
									$query1="SELECT supervisor FROM login_route WHERE login_routeid = '$viewroute[1]'";
									$result1 = Treat_DB_ProxyOld::query($query1);

									$mysuper=@mysql_result($result1,0,"supervisor");

									$query1="SELECT * FROM login_route WHERE supervisor = '$mysuper' ORDER BY route";
									$result1 = Treat_DB_ProxyOld::query($query1);

									while($r=mysql_fetch_array($result1)){
											$login_routeid=$r["login_routeid"];
											$route=$r["route"];

											if($login_routeid == $viewroute[1]){$sel="SELECTED";}
											else{$sel="";}

											echo "<option value=businesstrack.php?viewbus=$viewbus&viewtype=$viewtype&viewsub=1&thismonth=$thismonth&thisyear=$thisyear&viewr=r-$login_routeid $sel>$route</option>";
									}
							}
							if($viewroute[0]==""){echo "</optgroup>";}
							echo "</select></form>";
							echo "</td></tr></table>";

							///////////////////////////////////////////////////
							////////////////////GROUP TYPE COLLECTS GRAPH (LAST 8 WEEKS)
							///////////////////////////////////////////////////

							?>
							<br><table width=100% cellspacing=0 cellpadding=0 style="border:2px solid #E8E7E7;"><tr bgcolor=#E8E7E7><td>Group Type Collection Last 8 Weeks</td></tr><tr><td>
							<applet code="LineGraphApplet.class" archive="Linegraph.jar" width="100%" height=175">

							<!-- Start Up Parameters -->
							<PARAM name="LOADINGMESSAGE" value="Creating Chart - Please Wait.">
							<PARAM name="STEXTCOLOR"     value="#000060">
							<PARAM name="STARTUPCOLOR"   value="#FFFFFF">

							<!-- Line Data -->
							<!--   value,URL,Target Frame -->

							<?php

							if($viewroute[0]=="b"){$myquery="vend_machine_sales.businessid = '$viewroute[1]' AND "; $fromtables.="";}
							elseif($viewroute[0]=="r"){$myquery="login_route.login_routeid = '$viewroute[1]' AND login_route.route = vend_machine_sales.route AND"; $fromtables.=",login_route";}
							elseif($viewroute[0]=="s"){$myquery="login_route.supervisor = '$viewroute[1]' AND login_route.route = vend_machine_sales.route AND"; $fromtables.=",login_route";}
							else{$myquery="";}

							$query1="SELECT * FROM vend_machine_vgroup_type ORDER BY vgroup_typename";
							$result1 = Treat_DB_ProxyOld::query($query1);

							$series="series";
							$seriescount=1;
							while($r = mysql_fetch_array($result1)){
									$vgroup_typeid=$r["vgroup_typeid"];
									$groupname[$vgroup_typeid]=$r["vgroup_typename"];
									$groupcolor[$vgroup_typeid]=$r["color"];

									for($counter=1;$counter<=8;$counter++){
											$counter2=$counter+1;
											$query2="SELECT SUM(vend_machine_sales.collects) AS totalcollects FROM vend_machine_vgroup,vend_machine_sales,business$fromtables WHERE $myquery vend_machine_sales.vgroup = vend_machine_vgroup.vgroupid AND vend_machine_vgroup.vgroup_type = '$vgroup_typeid' AND vend_machine_vgroup.hidden = '0' AND vend_machine_sales.date > '$week[$counter]' AND vend_machine_sales.date <= '$week[$counter2]' AND vend_machine_sales.businessid = business.businessid AND ($compquery)";
											$result2 = Treat_DB_ProxyOld::query($query2);

											$totalcollects=@mysql_result($result2,0,"totalcollects");
											if($totalcollects==""){$totalcollects=0;}
											//echo "$query2: <font color=red>$totalcollects</font><br>";
											echo "<PARAM name=data$counter$series$seriescount value=$totalcollects>";
									}
									$seriescount++;
							}

							?>

							<!-- Properties -->
							<PARAM name="ylabels"		  value="S|M|T|W|T|F|S">
							<PARAM name="xlabels"		  value="S|M|T|W|T|F|S">
							<PARAM name="titlefontsize"	  value="0">
							<PARAM name="xtitlefontsize"	  value="0">
							<PARAM name="ytitlefontsize"	  value="0">
							<PARAM name="autoscale"            value="true">
							<PARAM name="gridbgcolor"          value="#FFFFFF">
							<PARAM name="series1"              value="red|6|5|false|dotted">
							<PARAM name="series2"              value="blue|6|5|true|dotted">
							<PARAM name="series3"              value="green|6|5|true|dotted">
							<?
							$counter=1;
							foreach($groupname AS $key => $value){
									echo "<PARAM name=\"series$counter\"              value=\"$groupcolor[$key]|6|5|false|dotted\">";
									$counter++;
							}
							?>
							<PARAM name="ndecplaces"           value="0">
							<PARAM name="labelOrientation"     value="Up Angle">
							<PARAM name="xlabel_pre"           value="">
							<PARAM name="xlabel_font"          value="Arial,N,10">
							<?

							for($counter=2;$counter<=9;$counter++){
									$value=$week[$counter];

									$g_day=substr($value,8,2);
									$g_month=substr($value,5,2);
									$counter2=$counter-1;
									echo "<PARAM name='label$counter2' value='$g_month/$g_day'>";
							}
							?>
							</applet>
							</td></tr>
							<tr><td>
							<?
							foreach($groupname AS $key => $value){
									echo "<table style=\"margin:0;padding:0;display:inline;\"><tr height=4><td width=10 bgcolor=$groupcolor[$key]></td><td bgcolor=white><font size=1>$value</td></tr></table>";
							}
							?>
							</td></tr>
							</table>

							<?

							///////////////////////////////////////////////////
							////////////////////GROUP TYPE FILLS GRAPH (LAST 8 WEEKS)
							///////////////////////////////////////////////////

							?>
							<br><table width=100% cellspacing=0 cellpadding=0 style="border:2px solid #E8E7E7;"><tr bgcolor=#E8E7E7><td>Group Type Fills Last 8 Weeks</td></tr><tr><td>
							<applet code="LineGraphApplet.class" archive="Linegraph.jar" width="100%" height=175">

							<!-- Start Up Parameters -->
							<PARAM name="LOADINGMESSAGE" value="Creating Chart - Please Wait.">
							<PARAM name="STEXTCOLOR"     value="#000060">
							<PARAM name="STARTUPCOLOR"   value="#FFFFFF">

							<!-- Line Data -->
							<!--   value,URL,Target Frame -->

							<?php

							if($viewroute[0]=="b"){$myquery="vend_machine_sales.businessid = '$viewroute[1]' AND "; $fromtables.="";}
							elseif($viewroute[0]=="r"){$myquery="login_route.login_routeid = '$viewroute[1]' AND login_route.route = vend_machine_sales.route AND";}
							elseif($viewroute[0]=="s"){$myquery="login_route.supervisor = '$viewroute[1]' AND login_route.route = vend_machine_sales.route AND";}
							else{$myquery="";}

							$query1="SELECT * FROM vend_machine_vgroup_type ORDER BY vgroup_typename";
							$result1 = Treat_DB_ProxyOld::query($query1);

							$seriescount=1;
							while($r = mysql_fetch_array($result1)){
									$vgroup_typeid=$r["vgroup_typeid"];
									$groupname[$vgroup_typeid]=$r["vgroup_typename"];
									$groupcolor[$vgroup_typeid]=$r["color"];

									for($counter=1;$counter<=8;$counter++){
											$counter2=$counter+1;
											$query2="SELECT SUM(vend_machine_sales.fills) AS totalfills FROM vend_machine_vgroup,vend_machine_sales,business$fromtables WHERE $myquery vend_machine_sales.vgroup = vend_machine_vgroup.vgroupid AND vend_machine_vgroup.hidden = '0' AND vend_machine_vgroup.vgroup_type = '$vgroup_typeid' AND vend_machine_sales.date > '$week[$counter]' AND vend_machine_sales.date <= '$week[$counter2]' AND vend_machine_sales.businessid = business.businessid AND ($compquery)";
											$result2 = Treat_DB_ProxyOld::query($query2);

											$totalfills=@mysql_result($result2,0,"totalfills");
											if($totalfills==""){$totalfills=0;}
											echo "<PARAM name=data$counter$series$seriescount value='$totalfills'>";
									}
									$seriescount++;
							}

							?>

							<!-- Properties -->
							<PARAM name="grid"		      value="true">
							<PARAM name="ylabels"		  value="S|M|T|W|T|F|S">
							<PARAM name="xlabels"		  value="S|M|T|W|T|F|S">
							<PARAM name="titlefontsize"	  value="0">
							<PARAM name="xtitlefontsize"	  value="0">
							<PARAM name="ytitlefontsize"	  value="0">
							<PARAM name="autoscale"            value="true">
							<PARAM name="gridbgcolor"          value="#FFFFFF">
							<PARAM name="series1"              value="red|6|5|false|dotted">
							<PARAM name="series2"              value="blue|6|5|true|dotted">
							<PARAM name="series3"              value="green|6|5|true|dotted">
							<?
							$counter=1;
							foreach($groupname AS $key => $value){
									echo "<PARAM name=\"series$counter\"              value=\"$groupcolor[$key]|6|5|false|dotted\">";
									$counter++;
							}
							?>
							<PARAM name="ndecplaces"           value="0">
							<PARAM name="labelOrientation"     value="Up Angle">
							<PARAM name="xlabel_pre"           value="">
							<PARAM name="xlabel_font"          value="Arial,N,10">
							<?

							for($counter=2;$counter<=9;$counter++){
									$value=$week[$counter];

									$g_day=substr($value,8,2);
									$g_month=substr($value,5,2);

									$counter2=$counter-1;
									echo "<PARAM name='label$counter2' value='$g_month/$g_day'>";
							}
							?>
							</applet>
							</td></tr>
							<tr><td>
							<?
							foreach($groupname AS $key => $value){
									echo "<table style=\"margin:0;padding:0;display:inline;\"><tr height=4><td width=10 bgcolor=$groupcolor[$key]></td><td bgcolor=white><font size=1>$value</td></tr></table>";
							}
							?>
							</td></tr>
							</table>

							<?

							///////////////////FILL/COLLECTS AVERAGES GRAPH
							if($viewroute[0]==""){$showroute="";}
							else{$showroute="$viewroute[0]-$viewroute[1]";}

							echo "<br><table width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td title='*Averages are figured by Total/#Stops'>Collect/Fill Averages</td></tr>";
							if(file_exists("_UTILITY_FILES/dashboard/6-$viewtype-$viewbus-$showroute.html")==false){echo "<tr><td><iframe src='dashboard_mach_sales.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&viewr=$showroute&vgroup=$vgroup_typeid&show=6' WIDTH=100% HEIGHT=188 frameborder=0></iframe>";}
							else{echo "<tr><td><iframe src='_UTILITY_FILES/dashboard/6-$viewtype-$viewbus-$showroute.html' WIDTH=100% HEIGHT=188 frameborder=0></iframe>";}
							echo "<tr><td> <table style=\"margin:0;padding:0;display:inline;\"><tr height=4><td width=10 bgcolor=#0000FF></td><td bgcolor=white><font size=1>Avg Collects</td></tr></table> <table style=\"margin:0;padding:0;display:inline;\"><tr height=4><td width=10 bgcolor=#FF0000></td><td bgcolor=white><font size=1>Avg Fills</td></tr></table> </td></tr>";
							echo "</td></tr></table>";

					echo "</td><td width=33%>";
					////////SECOND COLUMN
							////////////////////GROUP TYPES

							$query1="SELECT * FROM vend_machine_vgroup_type ORDER by vgroup_typeid";
							$result1 = Treat_DB_ProxyOld::query($query1);

							while($r=mysql_fetch_array($result1)){
									$vgroup_typeid=$r["vgroup_typeid"];
									$vgroupt_typename=$r["vgroup_typename"];

									echo "<br><table width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td><a href=dashboard_mach_sales.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&viewr=$showroute&vgroup=$vgroup_typeid&show=7&exp=1 target='_blank' style=$style><font color=black onMouseOver=this.color='#FF9900' onMouseOut=this.color='black' title='Click to enlarge'>$vgroupt_typename Collects Last 8 Weeks</font></a></td></tr>";
									echo "<tr><td>";

									if($viewroute[0]=="s"){
											if(file_exists("_UTILITY_FILES/dashboard/0-$viewtype-$viewbus-$showroute-$vgroup_typeid.html")==false){echo "<iframe src='dashboard_mach_sales.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&viewr=$showroute&vgroup=$vgroup_typeid' WIDTH=100% HEIGHT=225 frameborder=0></iframe>";}
											else{echo "<iframe src='_UTILITY_FILES/dashboard/0-$viewtype-$viewbus-$showroute-$vgroup_typeid.html' WIDTH=100% HEIGHT=225 frameborder=0></iframe>";}
									}
									else{
											if(file_exists("_UTILITY_FILES/dashboard/7-$viewtype-$viewbus-$showroute-$vgroup_typeid.html")==false){echo "<iframe src='dashboard_mach_sales.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&viewr=$showroute&vgroup=$vgroup_typeid&show=7' WIDTH=100% HEIGHT=225 frameborder=0></iframe>";}
											else{echo "<iframe src='_UTILITY_FILES/dashboard/7-$viewtype-$viewbus-$showroute-$vgroup_typeid.html' WIDTH=100% HEIGHT=225 frameborder=0></iframe>";}
									}

									//if($viewroute[0]=="s"){echo "<iframe src='_UTILITY_FILES/dashboard/0-$viewtype-$viewbus-$showroute.html' WIDTH=100% HEIGHT=225 frameborder=0></iframe>";}
									//else{echo "<iframe src='_UTILITY_FILES/dashboard/7-$viewtype-$viewbus-$showroute.html' WIDTH=100% HEIGHT=225 frameborder=0></iframe>";}

									echo "</td></tr></table>";
							}

					echo "</td><td width=33%>";
					////////THIRD COLUMN

							//////DISPLAY CHANNELS
							if($viewroute[0]=="s"||$viewroute[0]=="r"){
									echo "<br><table width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td>Channel Collection Last 8 Weeks</td></tr>";
									if(file_exists("_UTILITY_FILES/dashboard/1-$viewtype-$viewbus-$showroute.html")==false){echo "<tr><td><iframe src='dashboard_mach_sales.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&viewr=$showroute&vgroup=$vgroup_typeid&show=1' WIDTH=100% HEIGHT=225 frameborder=0></iframe></td></tr></table>";}
									else{echo "<tr><td><iframe src='_UTILITY_FILES/dashboard/1-$viewtype-$viewbus-$showroute.html' WIDTH=100% HEIGHT=225 frameborder=0></iframe></td></tr></table>";}
							}
							else{
									echo "<br><table width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td title='Sorted by Total Collections for last 8 weeks'>Channel Collection Weekly Percent Change</td></tr>";
									if(1==1){echo "<tr><td><iframe src='dashboard_mach_sales.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&viewr=$showroute&vgroup=$vgroup_typeid&show=10' WIDTH=100% HEIGHT=225 frameborder=0></iframe></td></tr></table>";}
									else{echo "<tr><td><iframe src='_UTILITY_FILES/dashboard/10-$viewtype-$viewbus-$showroute.html' WIDTH=100% HEIGHT=225 frameborder=0></iframe></td></tr></table>";}
							}
							//////DISPLAY FAMILIES
							echo "<br><table width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td>Family Collection Last 8 Weeks</td></tr>";
							if(file_exists("_UTILITY_FILES/dashboard/2-$viewtype-$viewbus-$showroute.html")==false){echo "<tr><td><iframe src='dashboard_mach_sales.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&viewr=$showroute&vgroup=$vgroup_typeid&show=2' WIDTH=100% HEIGHT=225 frameborder=0></iframe></td></tr></table>";}
							else{echo "<tr><td><iframe src='_UTILITY_FILES/dashboard/2-$viewtype-$viewbus-$showroute.html' WIDTH=100% HEIGHT=225 frameborder=0></iframe></td></tr></table>";}
							//////DISPLAY CUSTOMERS
							echo "<br><table width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td>Account Collection Last 8 Weeks</td></tr>";
							if(file_exists("_UTILITY_FILES/dashboard/3-$viewtype-$viewbus-$showroute.html")==false){echo "<tr><td><iframe src='dashboard_mach_sales.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&viewr=$showroute&vgroup=$vgroup_typeid&show=3' WIDTH=100% HEIGHT=225 frameborder=0></iframe></td></tr></table>";}
							else{echo "<tr><td><iframe src='_UTILITY_FILES/dashboard/3-$viewtype-$viewbus-$showroute.html' WIDTH=100% HEIGHT=225 frameborder=0></iframe></td></tr></table>";}
							//////DISPLAY MACHINES
							if($viewroute[0]=="s"||$viewroute[0]=="r"){
									echo "<br><table width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td>Machine Collection Last 8 Weeks</td></tr>";
									if(file_exists("_UTILITY_FILES/dashboard/4-$viewtype-$viewbus-$showroute.html")==false){echo "<tr><td><iframe src='dashboard_mach_sales.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&viewr=$showroute&vgroup=$vgroup_typeid&show=4' WIDTH=100% HEIGHT=225 frameborder=0></iframe></td></tr></table>";}
									else{echo "<tr><td><iframe src='_UTILITY_FILES/dashboard/4-$viewtype-$viewbus-$showroute.html' WIDTH=100% HEIGHT=225 frameborder=0></iframe></td></tr></table>";}
							}

					echo "</td></tr></table>";
					echo "</table>";
			 */
			echo "</td></tr>";
		}
		/////////////////////////////
		///////ROUTES////////////////
		/////////////////////////////
		elseif( $viewtype > 0 && $viewtype < 7 && $viewbus != "" && $viewsub == 2 ) {
			echo "<tr><td colspan=3><p><br>";

			//echo "<table width=100% cellspacing=0 cellpadding=0>";
			//echo "<tr valign=top><td>";
			////////FIRST COLUMN

			echo "<table width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td>Average Weekly Collections</td></tr>";
			echo "<tr><td><div width=100% id='sales_ag' style=\"visibility:$showgraph;\"><iframe src='dashboard_mach_sales_route.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&viewr=$showroute&show=1' WIDTH=100% HEIGHT=425 frameborder=0></iframe></div></td></tr></table>";

			echo "<p><br><table width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td>Collects Per Service</td></tr>";
			echo "<tr><td><div width=100% id='sales_day' style=\"visibility:$showgraph;\"><iframe src='dashboard_mach_sales_service.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&viewr=$showroute&show=1' WIDTH=100% HEIGHT=425 frameborder=0></iframe></div></td></tr></table>";

			//echo "<p><table width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td>Collections Per Stop</td></tr>";
			//echo "<tr><td><iframe src='dashboard_mach_sales_route_stops.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&viewr=$showroute&show=1' WIDTH=100% HEIGHT=425 frameborder=0></iframe></td></tr></table>";

			//echo "</td></tr></table>";
			echo "</td></tr>";
		}
		elseif( $viewtype > 0 && $viewtype < 7 && $viewbus != "" && $viewsub == 3 ) {
			echo "<tr><td colspan=2><p><br><center><b><font size=4>Coming Soon...</font></b></center></td></tr>";
		}
		elseif( $viewtype > 0 && $viewtype < 7 && $viewbus != "" && $viewsub == 4 ) {
			echo "<tr><td colspan=2><p><br><center><b><font size=4>Coming Soon...</font></b></center></td></tr>";
		}
		///////////////////////
		//////PRODUCTION///////
		///////////////////////
		elseif( $viewtype > 0 && $viewtype < 7 && $viewbus != "" && $viewsub == 7 ) {
			echo "<tr><td colspan=2><p><br><iframe src='menuprofitability.php' WIDTH=100% HEIGHT=2000 frameborder=0></iframe></td></tr>";
		}
		///////////////////////
		///////SERVICE/////////
		///////////////////////
		elseif( $viewtype > 0 && $viewtype < 7 && $viewbus != "" && $viewsub == 5 ) {
			echo "<tr><td colspan=2><p><br>";

			echo "<table width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td>Service Calls Per Technician</td></tr>";
			echo "<tr><td><div width=100% id='sales_ag' style=\"visibility:$showgraph;\"><iframe src='dashboard_mach_services.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&viewr=$showroute&show=1' WIDTH=100% HEIGHT=425 frameborder=0></iframe></div></td></tr></table>";

			///////collect per thousand dollars
			echo "<p><br><table width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td>Service Calls Per $1000 Collects</td></tr>";
			echo "<tr><td><div width=100% id='sales_day' style=\"visibility:$showgraph;\"><iframe src='dashboard_services.php' WIDTH=100% HEIGHT=425 frameborder=0></iframe></div></td></tr></table>";

			///////overtime per vending unit
			echo "<p><br><table width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td>Service Techs Overtime</td></tr>";
			echo "<tr><td><div width=100% id='custom' style=\"visibility:$showgraph;\"><iframe src='dashboard_overtime.php' WIDTH=100% HEIGHT=350 frameborder=0></iframe></div></td></tr></table>";

			echo "</td></tr>";
		}
		///////////////////////
		////////SALES//////////
		///////////////////////
		elseif( $viewtype > 0 && $viewtype < 7 && $viewbus != "" && $viewsub == 6 ) {
			echo "<tr><td colspan=2><p><br><center>";

			echo "<iframe src='dashboard_esalestrack.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&viewr=$showroute&show=9' WIDTH=100% HEIGHT=2500 frameborder=0 name=salesdash></iframe>";

			echo "</center></td></tr>";
		}
		///////////////////////
		////////EXPENSES//////////
		///////////////////////
		elseif( $viewtype > 0 && $viewtype < 7 && $viewbus != "" && $viewsub == 8 ) {
			echo "<tr><td colspan=2><p><br><center>";

			echo "<iframe src='dashboard_expenses.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus' WIDTH=100% HEIGHT=1100 frameborder=0 name=salesdash></iframe>";

			echo "</center></td></tr>";
		}
		///////////////////////
		////////CUSTOM/////////
		///////////////////////
		elseif( $viewtype > 0 && $viewtype < 7 && $viewbus != "" && $viewsub == 10 ) {
			echo "<tr><td colspan=2><div width=100% id='sales_ag' style=\"visibility:$showgraph;\">";

			//echo "<iframe src='./custom_db?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus' WIDTH=100% HEIGHT=2000 frameborder=0 name=salesdash></iframe>";

			require( 'charts/dashboard.php' );

			echo "</div></td></tr>";
		}
		///////////////////////
		///////CAPEX///////////
		///////////////////////
		elseif( $viewtype > 0 && $viewtype < 7 && $viewbus != "" && $viewsub == 9 ) {
			echo "<tr><td colspan=2><p><br><center><div width=100% id='sales_ag' style=\"visibility:$showgraph;\">";

			echo "<iframe src='custom_db/dashboard_view.php?loginid=4662' WIDTH=100% HEIGHT=2000 frameborder=0 name=salesdash></iframe>";

			echo "</div></center></td></tr>";
		}

		echo "</table></center>";

	}

?>
					</td>
				</tr>
			</table>
		</center>
<?php
	//google_page_track( );

	//mysql_close();

}
?>
	</body>
</html>
