<?php
require_once( __DIR__ . '/../../application/bootstrap.php' );

$businessid = null; # Notice: Undefined variable
$user = \EE\Controller\Base::getSessionCookieVariable('usercook');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook');
$companyid = (int) \EE\Controller\Base::getSessionCookieVariable('compcook');
$districtid = (int) \EE\Controller\Base::getPostOrGetVariable('districtid');
$from_bid = (int) \EE\Controller\Base::getPostOrGetVariable('from_bid');
$from_cid = (int) \EE\Controller\Base::getPostOrGetVariable('from_cid');


$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num = Treat_DB_ProxyOld::mysql_num_rows($result);

if ($num != 1 || $pass =="" || $user == "") :
?>
<center>
	<h3>Failed</h3>
	Use your browser's back button to try again.
</center>
<?php
	exit();
endif;

$security_level = Treat_DB_ProxyOld::mysql_result($result, 0, "security_level");

$query = "SELECT * FROM company WHERE companyid = '$companyid'";
$result = Treat_DB_ProxyOld::query($query);
$companyname = Treat_DB_ProxyOld::mysql_result($result,0,"companyname");

//////////////////////////////////////////////BEGIN EDIT ACCOUNT////////////////////////////////////////////////////////////
$query = "SELECT * FROM district WHERE districtid = '$districtid'";
$result = Treat_DB_ProxyOld::query($query);
$districtObject = Treat_DB_ProxyOld::mysql_fetch_object($result);

$divisionid = $districtObject->divisionid;
$districtname = $districtObject->districtname;
$arrequest = $districtObject->arrequest;
$aprequest = $districtObject->aprequest;
$labor_request = $districtObject->labor_request;
$dm = $districtObject->dm;
$submit = $districtObject->submit;
$email_submit = $districtObject->email_submit;
$labor_submit = $districtObject->labor_submit;
$labor_email = $districtObject->labor_email;
$bad_ar = $districtObject->bad_ar;
$bad_ar_email = $districtObject->bad_ar_email;
$email_orders = $districtObject->email_orders;
$production = $districtObject->production;
$deactivate_items = $districtObject->deactivate_items;
$kiosk_activation_emails = $districtObject->kiosk_activation_emails;
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Edit District - <?php echo $companyname; ?></title>
		<style type="text/css">
			.center {
				margin: 0px auto;
			}
			.container {
				padding: 0px;
				width: 90%;
			}
			.header {
				color: red;
				text-align: right;
			}
			.textinput {
				width: 50%;
				min-width: 150px;
			}
			tr td {
				padding: 10px 2px 2px;
				border-spacing: 4px;
			}
		</style>
	</head>
	<body>

		<div class="center container">
			<a href="/ta/businesstracks.php">
				<img src="logo.jpg">
			</a>
		</div>

		<div class="center container">
			<form action="savedistrict.php" method="POST">
				<input type="hidden" name="username" value="<?php echo $user; ?>">
				<input type="hidden" name="password" value="<?php echo $pass; ?>">
				<input type="hidden" name="businessid" value="<?php echo $businessid; ?>">
				<input type="hidden" name="companyid" value="<?php echo $companyid; ?>">
				<input type="hidden" name="districtid" value="<?php echo $districtid; ?>">
				<center>
					<table cellspacing="0" cellpadding="0" border="0" style="border: 2px solid #333; background-color: #E8E7E7;">
						<tr style="background-color: #333;">
							<th colspan="2" style="text-align: center; color: #fff; font-weight: bold;">
								District for <?php echo $companyname; ?>
							</td>
						</tr>
						<tr>
							<td class="header">District Name:</td>
							<td>
								<input type="text" name="districtname" size="30" class="textinput" value="<?php echo $districtname; ?>">
							</td>
						</tr>
<?php
///hide if coming from machine management
if ($from_bid == "") :
?>

						<tr>
							<td class="header">Division:</td>
							<td>
								<select name="divisionid" class="textinput">
<?php
	$query = "SELECT * FROM division WHERE companyid = '$companyid' ORDER BY division_name ASC";
	$result = Treat_DB_ProxyOld::query($query);
	while (false !== ($row = Treat_DB_ProxyOld::mysql_fetch_object($result))) :
?>

									<option value="<?php echo $row->divisionid; ?>"<?php if($row->divisionid == $divisionid) : ?> selected="selected"<?php endif; ?>>
										<?php echo $row->division_name; ?>
									</option>
<?php
	endwhile;
?>

								</select>
							</td>
						</tr>
						<tr>
							<td class="header">A/R Requests:</td>
							<td>
								<select name="arrequest" class="textinput">
<?php
	$query = "SELECT * FROM login WHERE security_level >= '3' AND is_deleted = 0 ORDER BY lastname ASC";
	$result = Treat_DB_ProxyOld::query($query);
	while (false !== ($row = Treat_DB_ProxyOld::mysql_fetch_object($result))) :
?>

									<option value="<?php echo $row->loginid; ?>"<?php if($row->loginid == $arrequest) : ?> selected="selected"<?php endif; ?>>
										<?php echo $row->lastname; ?>, <?php echo $row->firstname; ?>
									</option>
<?php
	endwhile;
?>

								</select>
							</td>
						</tr>
						<tr>
							<td class="header">A/P Requests:</td>
							<td>
								<select name="aprequest" class="textinput">
<?php
	$query = "SELECT * FROM login WHERE (companyid = '$companyid' AND security_level > 2) OR security_level = '9' ORDER BY lastname ASC";
	$result = Treat_DB_ProxyOld::query($query);
	while (false !== ($row = Treat_DB_ProxyOld::mysql_fetch_object($result))) :
?>

									<option value="<?php echo $row->loginid; ?>"<?php if($row->loginid == $aprequest) : ?> selected="selected"<?php endif; ?>>
										<?php echo $row->lastname; ?>, <?php echo $row->firstname; ?>
									</option>
<?php
	endwhile;
?>

								</select>
							</td>
						</tr>
						<tr>
							<td class="header">Labor Requests:</td>
							<td>
								<input type="text" name="labor_request" size="50" class="textinput" value="<?php echo $labor_request; ?>">
							</td>
						</tr>
						<tr>
							<td class="header">District Manager:</td>
							<td>
								<select name="dm" class="textinput">
<?php
	$query = "SELECT * FROM login WHERE (companyid = '$companyid' AND security_level > 2) OR security_level = '9' ORDER BY lastname ASC";
	$result = Treat_DB_ProxyOld::query($query);
	while (false !== ($row = Treat_DB_ProxyOld::mysql_fetch_object($result))) :
?>

									<option value="<?php echo $row->loginid; ?>"<?php if($row->loginid == $aprequest) : ?> selected="selected"<?php endif; ?>>
										<?php echo $row->lastname; ?>, <?php echo $row->firstname; ?>
									</option>
<?php
	endwhile;
?>

								</select>
							</td>
						</tr>
						<tr>
							<td class="header">Notify on Submit (Sales):</td>
							<td>
								<input type="checkbox" name="submit" value="1"<?php if($submit == 1) : ?> checked="checked"<?php endif; ?>>
								<input type="text" name="email_submit" size="50" class="textinput" value="<?php echo $email_submit; ?>">
							</td>
						</tr>
						<tr>
							<td class="header">Notify on Submit (Labor):</td>
							<td>
								<input type="checkbox" name="labor_submit" value="1"<?php if($labor_submit == 1) : ?> checked="checked"<?php endif; ?>>
								<input type="text" name="labor_email" size="50" class="textinput" value="<?php echo $labor_email; ?>">
							</td>
						</tr>
						<tr>
							<td class="header">Notify on Create (Invoices for Clients w/ 90+ AR):</td>
							<td>
								<input type="checkbox" name="bad_ar" value="1"<?php if($bad_ar == 1) : ?> checked="checked"<?php endif; ?>>
								<input type="text" name="bad_ar_email" size="50" class="textinput" value="<?php echo $bad_ar_email; ?>">
							</td>
						</tr>
<?php
else :
?>

						<input type="hidden" name="divisionid" value="<?php echo $divisionid; ?>">
						<input type="hidden" name="arrequest" value="<?php echo $arrequest; ?>">
						<input type="hidden" name="aprequest" value="<?php echo $aprequest; ?>">
						<input type="hidden" name="labor_request" value="<?php echo $labor_request; ?>">
						<input type="hidden" name="dm" value="<?php echo $dm; ?>">
						<input type="hidden" name="submit" value="<?php echo $submit; ?>">
						<input type="hidden" name="labor_submit" value="<?php echo $labor_submit; ?>">
						<input type="hidden" name="bad_ar" value="<?php echo $bad_ar; ?>">
						<input type="hidden" name="email_submit" value="<?php echo $email_submit; ?>">
						<input type="hidden" name="labor_email" value="<?php echo $labor_email; ?>">
						<input type="hidden" name="bad_ar_email" value="<?php echo $bad_ar_email; ?>">
						<input type="hidden" name="from_bid" value="<?php echo $from_bid; ?>">
						<input type="hidden" name="from_cid" value="<?php echo $from_cid; ?>">
<?php
endif;
?>

						<tr>
							<td class="header">Email Orders (Kiosk):</td>
							<td>
								<input type="text" name="email_orders" size="50" class="textinput" value="<?php echo $email_orders; ?>">
							</td>
						</tr>
						<tr>
							<td class="header">Kiosk Activation / Deactivation Notification Emails:</td>
							<td>
								<input type="text" name="kiosk_activation_emails" size="50" class="textinput" value="<?php echo $kiosk_activation_emails; ?>">
							</td>
						</tr>
						<tr>
							<td class="header">Production:</td>
							<td>
<?php
	////production days
	$days = array(1 => "Mon", 2 => "Tue", 3 => "Wed", 4 => "Thu", 5 => "Fri", 6 => "Sat", 7 => "Sun");
	foreach($days as $dayNum => $dayDisplay) :
		$value = pow(2, $dayNum);
?>

								<label>
									<input type="checkbox" name="production[]" value="<?php echo $value; ?>"<?php if($value & $production) : ?> checked="checked"<?php endif; ?>>
									<?php echo $dayDisplay; ?>
								</label>
<?php
	endforeach;
?>

							</td>
						</tr>
<?php
	////kiosk order setup
	$query2 = "
		SELECT
			kiosk_order_type.id,
			kiosk_order_type.ordertype,
			kiosk_order_sched.active,
			kiosk_order_sched.datetime,
			kiosk_order_sched.weekend,
			kiosk_order_sched.email,
			kiosk_order_sched.route_total
		FROM kiosk_order_type
		LEFT JOIN kiosk_order_sched ON
			kiosk_order_sched.ordertype = kiosk_order_type.id
			AND kiosk_order_sched.districtid = $districtid
		ORDER BY kiosk_order_type.id";
	$result2 = Treat_DB_ProxyOld::query($query2);

	$doubleDigit = function($item) {
		if ($item < 10) {
			return '0'.$item;
		}
		return $item;
	};
	$hourRange = array_map($doubleDigit, range(0, 23, 1));
	$minuteRange = array_map($doubleDigit, range(0, 59, 5));
	while($r2 = Treat_DB_ProxyOld::mysql_fetch_array($result2)) :
		$id = $r2["id"];
		$ordertype = $r2["ordertype"];
		$active = $r2["active"];
		$datetime = $r2["datetime"];
		$weekend = $r2["weekend"];
		$email = $r2["email"];
		$route_total = $r2["route_total"];

		$hour = substr($datetime,0,2);
		$minute = substr($datetime,2,2);
?>

						<tr>
							<td class="header"><?php echo $ordertype; ?>:</td>
							<td>
								<input type="checkbox" name="active<?php echo $id; ?>" value="1"<?php if($active == 1) : ?> checked="checked"<?php endif;?>>
								<select name="hour<?php echo $id; ?>">
<?php
		foreach($hourRange as $value) :
?>

									<option value="<?php echo $value; ?>"<?php if($hour == $value) : ?> selected="selected"<?php endif; ?>>
										<?php echo $value; ?>
									</option>
<?php
		endforeach;
?>

								</select>
								:
								<select name="minute<?php echo $id; ?>">
<?php
		foreach($minuteRange as $value) :
?>

									<option value="<?php echo $value; ?>"<?php if($minute == $value) : ?> selected="selected"<?php endif; ?>>
										<?php echo $value; ?>
									</option>
<?php
		endforeach;
?>

								</select>
								Email: <input type="text" name="email<?php echo $id; ?>" value="<?php echo $email; ?>" size="45">
								<label>
									Route Totals:
									<input type="checkbox" value="1" name="route_total<?php echo $id; ?>"<?php if($route_total == 1) : ?> checked="checked"<?php endif;?>>
								</label>
							</td>
						</tr>
<?php
	endwhile;
?>

						<tr>
							<td class="header">Automatically Deactivate Items:</td>
							<td>
								After
								<input type="text" size="3" name="deactivate_items" value="<?php echo $deactivate_items; ?>">
								Zero Counts (Set to zero to turn off)
							</td>
						</tr>
						<tr>
							<td colspan="2" class="header"><input type="submit" value="  Save  "></td>
						</tr>
					</table>
				</center>
			</form>
<?php
//////////////////////////////////////////////END LABOR////////////////////////////////////////////////////////////
?>

			<div style="text-align: center;">
<?php
if($from_bid == "") :
?>

				<form action="comsetup.php?cid=<?php echo $companyid; ?>" method="POST">
<?php
else :
?>

				<form action="busroute_machine.php?cid=<?php echo $from_cid; ?>&bid=<?php echo $from_bid; ?>" method="POST">
<?php
endif;
?>
					<input type="submit" value=" Return ">
				</form>
			</div>
		</div>
	</body>
</html>