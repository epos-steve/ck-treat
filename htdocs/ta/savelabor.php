<?php

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+4);
        $diff=round($diff, 2);
        if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
        elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
        $diff = substr($diff, 0, $dot+3);
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      elseif ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      elseif ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '1')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='31')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;
$leap = date("L");

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   
   elseif ($leap==1&&$month=='03')
   {
      $day=$day+29;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      $day=$day-31;  
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";} 
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}

define('DOC_ROOT', dirname(dirname(__FILE__)));
require_once(DOC_ROOT.'/bootstrap.php');

$user=$_COOKIE["usercook"];
$pass=$_COOKIE["passcook"];
$businessid=$_POST["businessid"];
$companyid=$_POST["companyid"];
$today=$_POST["date"];
$date1=$today;

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=mysql_numrows($result);
//mysql_close();

$loginid=mysql_result($result,0,"loginid");
$security_level=mysql_result($result,0,"security_level");
$mysecurity=mysql_result($result,0,"security");
$bid=mysql_result($result,0,"businessid");
$bid2=mysql_result($result,0,"busid2");
$bid3=mysql_result($result,0,"busid3");
$bid4=mysql_result($result,0,"busid4");
$bid5=mysql_result($result,0,"busid5");
$bid6=mysql_result($result,0,"busid6");
$bid7=mysql_result($result,0,"busid7");
$bid8=mysql_result($result,0,"busid8");
$bid9=mysql_result($result,0,"busid9");
$bid10=mysql_result($result,0,"busid10");
$cid=mysql_result($result,0,"companyid");

if ($num != 1 || ($security_level==1 && ($bid != $businessid || $cid != $companyid)) || $user == "" || $pass == "")
{
    echo "<center><h3>Failed</h3>Use your browser's back button to try again.</center>";
}
else
{
    $date1=$today;
    $enddate=$date1;
    for($counter=1;$counter<=7;$counter++){$enddate=nextday($enddate);}
	$enddate2=prevday($enddate);
/////////////////SAVE HOURS

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM jobtype WHERE companyid = '$companyid' AND commission = '0' ORDER BY hourly DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=mysql_numrows($result);
    //mysql_close();

    $num--;
    $mycounter=0;
    while($num>=0){
       $jobtypeid=mysql_result($result,$num,"jobtypeid");
       $jobtypename=mysql_result($result,$num,"name");
       $hourly=mysql_result($result,$num,"hourly");

////////NORMAL LABOR
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT distinct login.firstname,login.lastname,login.empl_no,login.loginid,jobtypedetail.rate FROM login,jobtypedetail,payroll_departmentdetail,security_departments WHERE login.oo2 = '$businessid' AND login.move_date <= '$enddate2' AND (login.is_deleted = '0' OR (login.is_deleted = '1' AND login.del_date >= '$date1')) AND jobtypedetail.loginid = login.loginid AND jobtypedetail.jobtype = '$jobtypeid' AND (jobtypedetail.is_deleted = '0' OR (jobtypedetail.is_deleted = '1' AND jobtypedetail.del_date >= '$date1')) AND (login.loginid = payroll_departmentdetail.loginid AND payroll_departmentdetail.dept_id = security_departments.dept_id AND security_departments.securityid = '$mysecurity') ORDER BY login.lastname DESC, login.firstname DESC";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=mysql_numrows($result2);
       //mysql_close();

       $num2--;
       while($num2>=0){
          $firstname=mysql_result($result2,$num2,"firstname"); 
          $lastname=mysql_result($result2,$num2,"lastname"); 
          $empl_no=mysql_result($result2,$num2,"empl_no");
          $loginid=mysql_result($result2,$num2,"loginid");
          $rate=mysql_result($result2,$num2,"rate");
          $rate=money($rate);

          $temp_date=$date1;
          for($counter=1;$counter<=7;$counter++){

             //GET CODED
             $code=$_POST["code$mycounter"];

             //GET HOURS
             $hours=$_POST["hour$mycounter"];

             $pos=strpos($hours,":");

             if ($pos !== "FALSE"){
                $pieces = explode(":", $hours);
                $pieces[1]=round($pieces[1]/60,2);
                $hours=$pieces[0]+$pieces[1];
             }

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query3 = "SELECT * FROM labor WHERE date = '$temp_date' AND loginid = '$loginid' AND jobtypeid = '$jobtypeid' AND businessid = '$businessid' AND tempid = '0'";
             $result3 = Treat_DB_ProxyOld::query($query3);
             $num3=mysql_numrows($result3);
             //mysql_close();

             if ($num3>0){
                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query3 = "UPDATE labor SET hours='$hours',coded = '$code' WHERE date = '$temp_date' AND loginid = '$loginid' AND jobtypeid = '$jobtypeid' AND businessid = '$businessid' AND tempid = '0'";
                $result3 = Treat_DB_ProxyOld::query($query3);
                //mysql_close();
             }
             elseif ($hours>0){
                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query3 = "INSERT INTO labor (loginid,businessid,companyid,hours,date,rate,coded,jobtypeid) VALUES ('$loginid','$businessid','$companyid','$hours','$temp_date','$rate','$code','$jobtypeid')";
                $result3 = Treat_DB_ProxyOld::query($query3);
                //mysql_close();
             }

             $temp_date=nextday($temp_date);
             $mycounter++;
          } 

          $num2--;
       }
/////////////PAST LABOR
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT distinct labor.loginid,labor.rate FROM login,labor WHERE labor.businessid = '$businessid' AND labor.date >= '$date1' AND labor.date<= '$enddate2' AND labor.jobtypeid = '$jobtypeid' AND login.oo2 != '$businessid' AND labor.loginid = login.loginid AND labor.tempid = '0'";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=mysql_numrows($result2);
       //mysql_close();

       $num2--;

       while($num2>=0){
          $loginid=mysql_result($result2,$num2,"loginid");
          $rate=mysql_result($result2,$num2,"rate");

          $rate=money($rate);

          $temp_date=$date1;
          for($counter=1;$counter<=7;$counter++){

             //GET CODED
             $code=$_POST["code$mycounter"];

             //GET HOURS
             $hours=$_POST["hour$mycounter"];

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query3 = "SELECT * FROM labor WHERE date = '$temp_date' AND loginid = '$loginid' AND jobtypeid = '$jobtypeid' AND businessid = '$businessid' AND tempid = '0'";
             $result3 = Treat_DB_ProxyOld::query($query3);
             $num3=mysql_numrows($result3);
             //mysql_close();

             if ($num3>0){
                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query3 = "UPDATE labor SET hours='$hours',coded = '$code' WHERE date = '$temp_date' AND loginid = '$loginid' AND jobtypeid = '$jobtypeid' AND tempid = '0' AND businessid = '$businessid'";
                $result3 = Treat_DB_ProxyOld::query($query3);
                //mysql_close();
             }
             elseif ($hours>0){
                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query3 = "INSERT INTO labor (loginid,businessid,companyid,hours,date,rate,coded,jobtypeid,tempid) VALUES ('$loginid','$businessid','$companyid','$hours','$temp_date','$rate','$code','$jobtypeid','0')";
                $result3 = Treat_DB_ProxyOld::query($query3);
                //mysql_close();
             }

             $temp_date=nextday($temp_date);
             $mycounter++;
          }
          $num2--;
       }
/////////////ADDED LABOR
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM labor_temp WHERE businessid = '$businessid' AND date >= '$date1' AND date <= '$enddate2' AND jobtypeid = '$jobtypeid'";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=mysql_numrows($result2);
       //mysql_close();

       $num2--;

       while($num2>=0){
          $loginid=mysql_result($result2,$num2,"loginid");
          $rate=mysql_result($result2,$num2,"rate");
          $tempid=mysql_result($result2,$num2,"tempid");

          $rate=money($rate);

          $temp_date=$date1;
          for($counter=1;$counter<=7;$counter++){

             //GET CODED
             $code=$_POST["code$mycounter"];

             //GET HOURS
             $hours=$_POST["hour$mycounter"];

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query3 = "SELECT * FROM labor WHERE date = '$temp_date' AND loginid = '$loginid' AND jobtypeid = '$jobtypeid' AND businessid = '$businessid' AND tempid = '$tempid'";
             $result3 = Treat_DB_ProxyOld::query($query3);
             $num3=mysql_numrows($result3);
             //mysql_close();

             if ($num3>0){
                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query3 = "UPDATE labor SET hours='$hours',coded = '$code' WHERE date = '$temp_date' AND loginid = '$loginid' AND jobtypeid = '$jobtypeid' AND tempid = '$tempid' AND businessid = '$businessid'";
                $result3 = Treat_DB_ProxyOld::query($query3);
                //mysql_close();
             }
             elseif ($hours>0){
                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query3 = "INSERT INTO labor (loginid,businessid,companyid,hours,date,rate,coded,jobtypeid,tempid) VALUES ('$loginid','$businessid','$companyid','$hours','$temp_date','$rate','$code','$jobtypeid','$tempid')";
                $result3 = Treat_DB_ProxyOld::query($query3);
                //mysql_close();
             }

             $temp_date=nextday($temp_date);
             $mycounter++;
          }
          $num2--;
       }
/////////END ADDED LABOR
       $num--;
    }

///////////////////////////////////////////
/////////////UPDATE SALES SHEET////////////
///////////////////////////////////////////

$day_hours=array();
$day_dollars=array();
$date2=$date1;

    $location="buslabor.php?bid=$businessid&cid=$companyid&newdate=$today&sbt=1#labor";
    header('Location: ./' . $location);

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM business WHERE businessid = '$businessid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $operate=mysql_result($result,0,"operate");

    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%><tr><td colspan=2>";
    echo "<img src=logo.jpg><p></td></tr></table><p>";    

    echo "<center><table width=90% cellspacing=0 cellpadding=0>";
    echo "<tr height=1 bgcolor=black><td colspan=6></td></tr>";

    echo "<tr><td colspan=6>";
    echo "<center><table width=100% border=0 bgcolor=#E8E7E7>";
    echo "<tr bgcolor=#CCCC99><td width=4%><font size=2><b>Empl#</b></td><td width=18%><font size=2><b>Name</b></td><td align=right width=8%><font size=2><b>Rate</b></td>";

    $sh_totpay=0;
    $sh_tothour=0;
    $sh_codehour=array();
    $sh_totreg=0;
    $sh_totot=0;

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query34 = "SELECT * FROM payroll_code WHERE companyid = '$companyid' ORDER BY orderid DESC";
    $result34 = Treat_DB_ProxyOld::query($query34);
    $num34=mysql_numrows($result34);
    //mysql_close();

    $temp_num=$num34-1;
    $myspan=50/($num34+1);
    while($temp_num>=0){
       $codeid=mysql_result($result34,$temp_num,"codeid");
       $code=mysql_result($result34,$temp_num,"code");
       $code_charge[$codeid]=mysql_result($result34,$temp_num,"charge");

       echo "<td align=right width=$myspan%><font size=2><b>$code</b></td>";
       if ($code=="REG"){echo "<td align=right width=$myspan%><font size=2><b>OT</b></td>";}

       $temp_num--;
    }
    echo "<td align=right width=10%><font size=2><b>TOTAL HRS</td><td align=right width=10%><font size=2><b>TOTAL PAY</td></tr>";

/////////////REGULAR LABOR
    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM login WHERE oo2 = '$businessid' AND move_date <= '$enddate2' AND (is_deleted = '0' OR (is_deleted = '1' AND del_date >= '$date1')) ORDER BY security_level";
    $result = Treat_DB_ProxyOld::query($query);
    $num=mysql_numrows($result);
    //mysql_close();
	
	//echo "$query";

    $num--;
    $mycounter=0;
    $coltotal=array();
    $coldollar=array();
    $sheettotal=0;
    while($num>=0){
       $firstname=mysql_result($result,$num,"firstname"); 
       $lastname=mysql_result($result,$num,"lastname"); 
       $empl_no=mysql_result($result,$num,"empl_no");
       $loginid=mysql_result($result,$num,"loginid");

       $hourtotal=0;
       $othourtotal=0;
       $payroll_code_hours=array();
       $total_hours=0;
       $rowtotal=0;

       $temp_date=$date1;
       while($temp_date<=$date2){
       $charge_hours=0;
       $non_charge_hours=0;
       for($counter=1;$counter<=7;$counter++){

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query2 = "SELECT * FROM labor WHERE loginid = '$loginid' AND businessid = '$businessid' AND date = '$temp_date' AND tempid = '0'";
             $result2 = Treat_DB_ProxyOld::query($query2);
             $num2=mysql_numrows($result2);
             //mysql_close();

             $num2--;
             while($num2>=0){
                $coded=mysql_result($result2,$num2,"coded");
                $hours=mysql_result($result2,$num2,"hours");
                $rate=mysql_result($result2,$num2,"rate");
                $jobtype=mysql_result($result2,$num2,"jobtypeid");

                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query3 = "SELECT hourly,pto_override FROM jobtype WHERE jobtypeid = '$jobtype'";
                $result3 = Treat_DB_ProxyOld::query($query3);
                //mysql_close();

                $hourly=mysql_result($result3,0,"hourly");
                $pto_override=mysql_result($result3,0,"pto_override");

                if($pto_override>0){$rate=$pto_override;}

                if($hourly==1){$showrate=money($rate);}
                else{$showrate="xxx";}
                
                $payroll_code_hours[$coded]=$payroll_code_hours[$coded]+$hours;
                $total_hours=$total_hours+$hours;

                if($code_charge[$coded]==1){
                   if($hourly==1){
                      if($charge_hours>40){$rowtotal=money($rowtotal+(($rate*1.5)*$hours));$day_dollars[$counter]=$day_dollars[$counter]+(($rate*1.5)*$hours);}
                      elseif(($charge_hours+$hours)>40){
                         $rowtotal=money($rowtotal+(($charge_hours+$hours-40)*($rate*1.5)));
                         $day_dollars[$counter]=$day_dollars[$counter]+(($charge_hours+$hours-40)*($rate*1.5));
                         $rowtotal=money($rowtotal+(($hours-($charge_hours+$hours-40))*$rate));
                         $day_dollars[$counter]=$day_dollars[$counter]+(($hours-($charge_hours+$hours-40))*$rate);
                      }
                      else{
                         $rowtotal=$rowtotal+($hours*$rate);
                         $day_dollars[$counter]=$day_dollars[$counter]+($hours*$rate);
                      }
                   }
                   elseif($hourly==0&&$hours!=0){$rowtotal=$rowtotal+($rate/80*$hours);$day_dollars[$counter]=$day_dollars[$counter]+($rate/80*$hours);} 

                   $charge_hours=$charge_hours+$hours;
                   $day_hours[$counter]=$day_hours[$counter]+$hours;
                }
                else{$non_charge_hours=$non_charge_hours+$hours;}

                $num2--;
          }
          $temp_date=nextday($temp_date);
       }
       if($charge_hours>40){$hourtotal=$hourtotal+40;$othourtotal=$othourtotal+($charge_hours-40);}
       else{$hourtotal=$hourtotal+$charge_hours;}
       }
       echo "<tr bgcolor=#E8E7E7 onMouseOver=this.bgColor='yellow' onMouseOut=this.bgColor=''><td><font size=2>$empl_no</td><td><font size=2>$lastname,$firstname</td><td align=right><font size=2>$$showrate</td>";

       $temp_num=$num34-1;
       while($temp_num>=0){
          $codeid=mysql_result($result34,$temp_num,"codeid");
          $code=mysql_result($result34,$temp_num,"code");
          if ($code=="REG"){$payroll_code_hours[$codeid]=$payroll_code_hours[$codeid]-$othourtotal; echo "<td align=right><font size=2>$payroll_code_hours[$codeid]</td>"; echo "<td align=right><font size=2>$othourtotal</td>";$sh_totreg=$sh_totreg+$hourtotal;$sh_totot=$sh_totot+$othourtotal;}
          else{echo "<td align=right><font size=2>$payroll_code_hours[$codeid]</td>";$sh_codehour[$codeid]=$sh_codehour[$codeid]+$payroll_code_hours[$codeid];}
          $temp_num--;
       }
       $sh_totpay=$sh_totpay+$rowtotal;
       $sh_tothour=$sh_tothour+$total_hours;

       if ($hourly==1||$security_level==9){$showrowtotal=money($rowtotal);}
       else{$showrowtotal="xxx";}
       $rowtotal=money($rowtotal);

       echo "<td align=right><font size=2>$total_hours</td><td align=right><font size=2>$$showrowtotal</td></tr>";
       $num--;
    }
/////////////PAST LABOR
    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT distinct labor.loginid FROM login,labor WHERE labor.businessid = '$businessid' AND labor.date >= '$date1' AND labor.date <= '$date2' AND login.oo2 != '$businessid' AND labor.loginid = login.loginid AND labor.tempid = '0'";
    $result = Treat_DB_ProxyOld::query($query);
    $num=mysql_numrows($result);
    //mysql_close();

    $num--;

    while($num>=0){

       $loginid=mysql_result($result,$num,"loginid");

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query73 = "SELECT * FROM login WHERE loginid = '$loginid'";
       $result73 = Treat_DB_ProxyOld::query($query73);
       //mysql_close();

       $firstname=mysql_result($result73,0,"firstname"); 
       $lastname=mysql_result($result73,0,"lastname"); 
       $empl_no=mysql_result($result73,0,"empl_no");

       $hourtotal=0;
       $othourtotal=0;
       $payroll_code_hours=array();
       $total_hours=0;
       $rowtotal=0;

       $temp_date=$date1;
       while($temp_date<=$date2){
       $charge_hours=0;
       $non_charge_hours=0;
       for($counter=1;$counter<=7;$counter++){

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query2 = "SELECT * FROM labor WHERE loginid = '$loginid' AND businessid = '$businessid' AND date = '$temp_date' AND tempid = '0'";
             $result2 = Treat_DB_ProxyOld::query($query2);
             $num2=mysql_numrows($result2);
             //mysql_close();

             $num2--;
             while($num2>=0){
                $coded=mysql_result($result2,$num2,"coded");
                $hours=mysql_result($result2,$num2,"hours");
                $rate=mysql_result($result2,$num2,"rate");
                $jobtype=mysql_result($result2,$num2,"jobtypeid");

                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query3 = "SELECT hourly,pto_override FROM jobtype WHERE jobtypeid = '$jobtype'";
                $result3 = Treat_DB_ProxyOld::query($query3);
                //mysql_close();

                $hourly=mysql_result($result3,0,"hourly");
                $pto_override=mysql_result($result3,0,"pto_override");

                if($pto_override>0){$rate=$pto_override;}

                if($hourly==1){$showrate=money($rate);}
                else{$showrate="xxx";}
                
                $payroll_code_hours[$coded]=$payroll_code_hours[$coded]+$hours;
                $total_hours=$total_hours+$hours;

                if($code_charge[$coded]==1){
                   if($hourly==1){
                      if($charge_hours>40){$rowtotal=money($rowtotal+(($rate*1.5)*$hours));$day_dollars[$counter]=$day_dollars[$counter]+(($rate*1.5)*$hours);}
                      elseif(($charge_hours+$hours)>40){
                         $rowtotal=money($rowtotal+(($charge_hours+$hours-40)*($rate*1.5)));
                         $day_dollars[$counter]=$day_dollars[$counter]+(($charge_hours+$hours-40)*($rate*1.5));
                         $rowtotal=money($rowtotal+(($hours-($charge_hours+$hours-40))*$rate));
                         $day_dollars[$counter]=$day_dollars[$counter]+(($hours-($charge_hours+$hours-40))*$rate);
                      }
                      else{
                         $rowtotal=$rowtotal+($hours*$rate);
                         $day_dollars[$counter]=$day_dollars[$counter]+($hours*$rate);
                      }
                   }
                   elseif($hourly==0&&$hours!=0){$rowtotal=$rowtotal+($rate/80*$hours);$day_dollars[$counter]=$day_dollars[$counter]+($rate/80*$hours);} 

                   $charge_hours=$charge_hours+$hours;
                   $day_hours[$counter]=$day_hours[$counter]+$hours;
                }
                else{$non_charge_hours=$non_charge_hours+$hours;}

                $num2--;
          }
          $temp_date=nextday($temp_date);
       }
       if($charge_hours>40){$hourtotal=$hourtotal+40;$othourtotal=$othourtotal+($charge_hours-40);}
       else{$hourtotal=$hourtotal+$charge_hours;}
       }
       echo "<tr bgcolor=red onMouseOver=this.bgColor='yellow' onMouseOut=this.bgColor='red'><td><font size=2>$empl_no</td><td><font size=2>$lastname,$firstname</td><td align=right><font size=2>$$showrate</td>";

       $temp_num=$num34-1;
       while($temp_num>=0){
          $codeid=mysql_result($result34,$temp_num,"codeid");
          $code=mysql_result($result34,$temp_num,"code");
          if ($code=="REG"){$payroll_code_hours[$codeid]=$payroll_code_hours[$codeid]-$othourtotal; echo "<td align=right><font size=2>$payroll_code_hours[$codeid]</td>"; echo "<td align=right><font size=2>$othourtotal</td>";$sh_totreg=$sh_totreg+$hourtotal;$sh_totot=$sh_totot+$othourtotal;}
          else{echo "<td align=right><font size=2>$payroll_code_hours[$codeid]</td>";$sh_codehour[$codeid]=$sh_codehour[$codeid]+$payroll_code_hours[$codeid];}
          $temp_num--;
       }
       $sh_totpay=$sh_totpay+$rowtotal;
       $sh_tothour=$sh_tothour+$total_hours;

       if ($hourly==1||$security_level==9){$showrowtotal=money($rowtotal);}
       else{$showrowtotal="xxx";}
       $rowtotal=money($rowtotal);

       echo "<td align=right><font size=2>$total_hours</td><td align=right><font size=2>$$showrowtotal</td></tr>";
       $num--;
    }

/////////////ADD-ON LABOR
    $lastloginid=0;

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM labor_temp WHERE businessid = '$businessid' AND date >= '$date1' AND date <= '$date2' ORDER BY loginid";
    $result = Treat_DB_ProxyOld::query($query);
    $num=mysql_numrows($result);
    //mysql_close();

    $num--;
    while($num>=0){
       $loginid=mysql_result($result,$num,"loginid");
       $tempid=mysql_result($result,$num,"tempid");

       ///////CHECK FOR DOUBLES
       if ($lastloginid!=$loginid){

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query73 = "SELECT * FROM login WHERE loginid = '$loginid'";
       $result73 = Treat_DB_ProxyOld::query($query73);
       //mysql_close();

       $firstname=mysql_result($result73,0,"firstname"); 
       $lastname=mysql_result($result73,0,"lastname"); 
       $empl_no=mysql_result($result73,0,"empl_no");

       $hourtotal=0;
       $othourtotal=0;
       $payroll_code_hours=array();
       $total_hours=0;
       $rowtotal=0;

//////FIND HOURS IN HOME UNIT
       $temp_date=$date1;
       $temp_hours=0;
       while($temp_date<=$date2){
       for($counter=1;$counter<=7;$counter++){

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query2 = "SELECT oo2 FROM login WHERE loginid = '$loginid'";
             $result2 = Treat_DB_ProxyOld::query($query2);
             //mysql_close();

             $temp_bid=mysql_result($result2,0,"oo2");

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query2 = "SELECT * FROM labor WHERE loginid = '$loginid' AND businessid = '$temp_bid' AND date = '$temp_date'";
             $result2 = Treat_DB_ProxyOld::query($query2);
             $num2=mysql_numrows($result2);
             //mysql_close();

             $num2--;
             while($num2>=0){
                $coded=mysql_result($result2,$num2,"coded");
                $hours=mysql_result($result2,$num2,"hours");
                $rate=mysql_result($result2,$num2,"rate");
                $jobtype=mysql_result($result2,$num2,"jobtypeid");

                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query3 = "SELECT hourly FROM jobtype WHERE jobtypeid = '$jobtype'";
                $result3 = Treat_DB_ProxyOld::query($query3);
                //mysql_close();

                $hourly=mysql_result($result3,0,"hourly");

                if($code_charge[$coded]==1&&$hourly==1){
                   $temp_hours=$temp_hours+$hours;
                }

                $num2--;
          }
          $temp_date=nextday($temp_date);
       }
       }

//////FIND HOURS IN OTHER UNITS
//////WHERE tempid is less than this tempid

       $temp_date=$date1;
       while($temp_date<=$date2){
       for($counter=1;$counter<=7;$counter++){

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query2 = "SELECT oo2 FROM login WHERE loginid = '$loginid'";
             $result2 = Treat_DB_ProxyOld::query($query2);
             //mysql_close();

             $temp_bid=mysql_result($result2,0,"oo2");

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query2 = "SELECT * FROM labor WHERE labor.loginid = '$loginid' AND labor.businessid != '$temp_bid' AND labor.businessid != '$businessid' AND labor.date = '$temp_date' AND labor.tempid < '$tempid' AND labor.tempid != '$tempid'";
             $result2 = Treat_DB_ProxyOld::query($query2);
             $num2=mysql_numrows($result2);
             //mysql_close();

             $num2--;
             while($num2>=0){
                $coded=mysql_result($result2,$num2,"coded");
                $hours=mysql_result($result2,$num2,"hours");
                $rate=mysql_result($result2,$num2,"rate");
                $jobtype=mysql_result($result2,$num2,"jobtypeid");

                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query3 = "SELECT hourly FROM jobtype WHERE jobtypeid = '$jobtype'";
                $result3 = Treat_DB_ProxyOld::query($query3);
                //mysql_close();

                $hourly=mysql_result($result3,0,"hourly");

                if($code_charge[$coded]==1&&$hourly==1){
                   $temp_hours=$temp_hours+$hours;
                }

                $num2--;
          }
          $temp_date=nextday($temp_date);
       }
       }

//////CALCULATE TRUE TIME
       $temp_date=$date1;
       while($temp_date<=$date2){
       $charge_hours=0;
       $non_charge_hours=0;
       for($counter=1;$counter<=7;$counter++){

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query2 = "SELECT * FROM labor WHERE loginid = '$loginid' AND businessid = '$businessid' AND date = '$temp_date'";
             $result2 = Treat_DB_ProxyOld::query($query2);
             $num2=mysql_numrows($result2);
             //mysql_close();

             $num2--;
             while($num2>=0){
                $coded=mysql_result($result2,$num2,"coded");
                $hours=mysql_result($result2,$num2,"hours");
                $rate=mysql_result($result2,$num2,"rate");
                $jobtype=mysql_result($result2,$num2,"jobtypeid");

                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query3 = "SELECT hourly,pto_override FROM jobtype WHERE jobtypeid = '$jobtype'";
                $result3 = Treat_DB_ProxyOld::query($query3);
                //mysql_close();

                $hourly=mysql_result($result3,0,"hourly");
                $pto_override=mysql_result($result3,0,"pto_override");

                if($pto_override>0){$rate=$pto_override;}

                if($hourly==1){$showrate=money($rate);}
                else{$showrate="xxx";}
                
                $payroll_code_hours[$coded]=$payroll_code_hours[$coded]+$hours;
                $total_hours=$total_hours+$hours;

                if($code_charge[$coded]==1){
                   if($hourly==1){
                      if(($temp_hours+$charge_hours)>40){$rowtotal=money($rowtotal+(($rate*1.5)*$hours));$day_dollars[$counter]=$day_dollars[$counter]+($rate*1.5)*$hours;}
                      elseif(($temp_hours+$charge_hours+$hours)>40){
                         $rowtotal=money($rowtotal+(($temp_hours+$charge_hours+$hours-40)*($rate*1.5)));
                         $day_dollars[$counter]=$day_dollars[$counter]+(($temp_hours+$charge_hours+$hours-40)*($rate*1.5));
                         $rowtotal=money($rowtotal+(($hours-($temp_hours+$charge_hours+$hours-40))*$rate));
                         $day_dollars[$counter]=$day_dollars[$counter]+(($hours-($temp_hours+$charge_hours+$hours-40))*$rate);
                      }
                      else{
                         $rowtotal=$rowtotal+($hours*$rate);
                         $day_dollars[$counter]=$day_dollars[$counter]+($hours*$rate);
                      }
                   }
                   elseif($hourly==0&&$hours!=0){
                      $rowtotal=$rowtotal+($rate/80*$hours);
                      $day_dollars[$counter]=$day_dollars[$counter]+($rate/80*$hours);
                   } 

                   $charge_hours=$charge_hours+$hours;
                   $day_hours[$counter]=$day_hours[$counter]+$hours;
                }
                else{$non_charge_hours=$non_charge_hours+$hours;}

                $num2--;
          }
          $temp_date=nextday($temp_date);
       }

       $reghoursleft=40-$temp_hours;

       if($temp_hours>0 && $reghoursleft<=0){$othourtotal=$othourtotal+$charge_hours;}
       elseif($temp_hours>0&&$charge_hours<=$reghoursleft){$hourtotal=$hourtotal+$charge_hours;}
       elseif($temp_hours>0&&$charge_hours>$reghoursleft){$hourtotal=$hourtotal+$reghoursleft;$othourtotal=$othourtotal+$charge_hours-$reghoursleft;}
       elseif($charge_hours>40){$hourtotal=$hourtotal+40;$othourtotal=$othourtotal+($charge_hours-40);}
       else{$hourtotal=$hourtotal+$charge_hours;}

       }
       echo "<tr bgcolor=#FFFF99 onMouseOver=this.bgColor='yellow' onMouseOut=this.bgColor='#FFFF99'><td><font size=2>$empl_no</td><td><font size=2>$lastname,$firstname</td><td align=right><font size=2>$$showrate</td>";

       $temp_num=$num34-1;
       while($temp_num>=0){
          $codeid=mysql_result($result34,$temp_num,"codeid");
          $code=mysql_result($result34,$temp_num,"code");
          if ($code=="REG"){$payroll_code_hours[$codeid]=$payroll_code_hours[$codeid]-$othourtotal; echo "<td align=right><font size=2>$hourtotal</td>"; echo "<td align=right><font size=2>$othourtotal</td>";$sh_totreg=$sh_totreg+$hourtotal;$sh_totot=$sh_totot+$othourtotal;}
          else{echo "<td align=right><font size=2>$payroll_code_hours[$codeid]</td>";$sh_codehour[$codeid]=$sh_codehour[$codeid]+$payroll_code_hours[$codeid];}
          $temp_num--;
       }
       $sh_totpay=$sh_totpay+$rowtotal;
       $sh_tothour=$sh_tothour+$total_hours;

       if ($hourly==1||$security_level==9){$showrowtotal=money($rowtotal);}
       else{$showrowtotal="xxx";}
       $rowtotal=money($rowtotal);

       echo "<td align=right><font size=2>$total_hours</td><td align=right><font size=2>$$showrowtotal</td></tr>";
       }
       $lastloginid=$loginid;
       $num--;
    }
////////////////////SHEET TOTALS
       echo "<tr bgcolor=#CCCCCC><td colspan=3></td>";

       $temp_num=$num34-1;
       while($temp_num>=0){
          $codeid=mysql_result($result34,$temp_num,"codeid");
          $code=mysql_result($result34,$temp_num,"code");
          if ($code=="REG"){echo "<td align=right><font size=2>$sh_totreg</td>"; echo "<td align=right><font size=2>$sh_totot</td>";}
          else{echo "<td align=right><font size=2>$sh_codehour[$codeid]</td>";}
          $temp_num--;
       }
       $sh_totpay=number_format($sh_totpay,2);
       echo "<td align=right><font size=2>$sh_tothour</td><td align=right><font size=2>$$sh_totpay</td></tr>";

///////////////////END DISPLAY LABOR
    echo "</table></center></td></tr>";
    echo "<tr height=1 bgcolor=black><td colspan=6></td></tr>";
    echo "</table></center>";  
////////////////////END UPDATE SALES SHEET

    $temp_date=$date1;
    for($counter=1;$counter<=7;$counter++){

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM stats WHERE businessid = '$businessid' AND date = '$temp_date'";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=mysql_numrows($result2);
       //mysql_close();

       if ($num2>0){
          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query2 = "UPDATE stats SET labor = '$day_dollars[$counter]', labor_hour = '$day_hours[$counter]' WHERE businessid = '$businessid' AND date = '$temp_date'";
          $result2 = Treat_DB_ProxyOld::query($query2);
          //mysql_close();
       }
       else{
          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query2 = "INSERT INTO stats (businessid,companyid,date,labor,labor_hour) VALUES ('$businessid','$companyid','$temp_date','$day_dollars[$counter]','$day_hours[$counter]')";
          $result2 = Treat_DB_ProxyOld::query($query2);
          //mysql_close();
       }

       $temp_date=nextday($temp_date);

    }

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "INSERT INTO audit_labor (date,user,comment,businessid) VALUES ('$today','$user','Week of $today Saved','$businessid')";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    echo "<br><FORM ACTION=$location method=post>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<center><INPUT TYPE=submit VALUE=' Return '></FORM></center>";

}
?>