<?php

function money($diff){
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+3);
        if ($diff > 0 && $diff < '0.01'){$diff="0.01";}
        elseif($diff < 0 && $diff > '-0.01'){$diff="-0.01";}
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      if ($month == "10"){$month="11";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='32')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='08' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      $day=$day-31;
      if ($day<10){$day="0$day";}
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";}
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";}
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";}
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";}
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');

$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");
$creditamount="creditamount";
$creditidnty="creditidnty";
$creditdate="creditdate";
$debitamount="debitamount";
$debitidnty="debitidnty";
$debitdate="debitdate";
$quatertotal=0;
$monthtotal=0;
$total=0;
$counter=0;
$findme='.';
$double='.00';
$single='0';
$double2='00';

/*$user=$_COOKIE["usercook"];
$pass=$_COOKIE["passcook"];
$view=$_COOKIE["viewcook"];
$sort=$_COOKIE["sortcook"];
$date1=$_COOKIE["date1cook"];
$date2=$_COOKIE["date2cook"];
$vendor=$_COOKIE["vendorcook"];
$viewtotal=$_COOKIE["viewtotalcook"];
$businessid=$_GET["bid"];
$companyid=$_GET["cid"];*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook');
$view = \EE\Controller\Base::getSessionCookieVariable('viewcook');
$sort = \EE\Controller\Base::getSessionCookieVariable('sortcook');
$date1 = \EE\Controller\Base::getSessionCookieVariable('date1cook');
$date2 = \EE\Controller\Base::getSessionCookieVariable('date2cook');
$vendor = \EE\Controller\Base::getSessionCookieVariable('vendorcook');
$viewtotal = \EE\Controller\Base::getSessionCookieVariable('viewtotalcook');
$businessid = \EE\Controller\Base::getGetVariable('bid');
$companyid = \EE\Controller\Base::getGetVariable('cid');

if ($businessid==""&&$companyid==""){
   /*$businessid=$_POST["businessid"];
   $companyid=$_POST["companyid"];
   $view=$_POST["view"];
   $date1=$_POST["date1"];
   $date2=$_POST["date2"];
   $findinv=$_POST["findinv"];
   $sort=$_POST["sort"];
   $vendor=$_POST["vendor"];
   $viewtotal=$_POST["viewtotal"];*/
	
	$businessid = \EE\Controller\Base::getPostVariable('businessid');
	$companyid = \EE\Controller\Base::getPostVariable('companyid');
	$view = \EE\Controller\Base::getPostVariable('view');
	$date1 = \EE\Controller\Base::getPostVariable('date1');
	$date2 = \EE\Controller\Base::getPostVariable('date2');
	$findinv = \EE\Controller\Base::getPostVariable('findinv');
	$sort = \EE\Controller\Base::getPostVariable('sort');
	$vendor = \EE\Controller\Base::getPostVariable('vendor');
	$viewtotal = \EE\Controller\Base::getPostVariable('viewtotal');
}

if ($date2==""){
   $date2=$today;$view='All';
}

if ($vendor==""){$vendor=0;}

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=Treat_DB_ProxyOld::mysql_numrows($result);
//mysql_close();

$security_level=Treat_DB_ProxyOld::mysql_result($result,0,"security_level");
$bid=Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
$cid=Treat_DB_ProxyOld::mysql_result($result,0,"companyid");

if ($num != 1 || ($security_level==1 && ($bid != $businessid || $cid != $companyid)) || ($security_level > 1 AND $cid != $companyid) || ($user == "" && $pass == ""))
{
    echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
    setcookie("viewcook",$view);
    setcookie("sortcook",$sort);
    setcookie("date1cook",$date1);
    setcookie("date2cook",$date2);
    setcookie("vendorcook",$vendor);
    setcookie("viewtotalcook",$viewtotal);

?>

<html>
<head>
<STYLE>
#loading {
 	width: 200px;
 	height: 48px;
 	background-color: ;
 	position: absolute;
 	left: 50%;
 	top: 50%;
 	margin-top: -50px;
 	margin-left: -100px;
 	text-align: center;
}
</STYLE>

<script type="text/javascript" src="preLoadingMessage.js"></script>

<SCRIPT LANGUAGE="JavaScript" SRC="CalendarPopup.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">document.write(getCalendarStyles());</SCRIPT>

<STYLE>
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation
			{
			background-color:#6677DD;
			text-align:center;
			vertical-align:center;
			text-decoration:none;
			color:#FFFFFF;
			font-weight:bold;
			}
	.TESTcpDayColumnHeader,
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation,
	.TESTcpCurrentMonthDate,
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDate,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDate,
	.TESTcpCurrentDateDisabled,
	.TESTcpTodayText,
	.TESTcpTodayTextDisabled,
	.TESTcpText
			{
			font-family:arial;
			font-size:8pt;
			}
	TD.TESTcpDayColumnHeader
			{
			text-align:right;
			border:solid thin #6677DD;
			border-width:0 0 1 0;
			}
	.TESTcpCurrentMonthDate,
	.TESTcpOtherMonthDate,
	.TESTcpCurrentDate
			{
			text-align:right;
			text-decoration:none;
			}
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDateDisabled
			{
			color:#D0D0D0;
			text-align:right;
			text-decoration:line-through;
			}
	.TESTcpCurrentMonthDate
			{
			color:#6677DD;
			font-weight:bold;
			}
	.TESTcpCurrentDate
			{
			color: #FFFFFF;
			font-weight:bold;
			}
	.TESTcpOtherMonthDate
			{
			color:#808080;
			}
	TD.TESTcpCurrentDate
			{
			color:#FFFFFF;
			background-color: #6677DD;
			border-width:1;
			border:solid thin #000000;
			}
	TD.TESTcpCurrentDateDisabled
			{
			border-width:1;
			border:solid thin #FFAAAA;
			}
	TD.TESTcpTodayText,
	TD.TESTcpTodayTextDisabled
			{
			border:solid thin #6677DD;
			border-width:1 0 0 0;
			}
	A.TESTcpTodayText,
	SPAN.TESTcpTodayTextDisabled
			{
			height:20px;
			}
	A.TESTcpTodayText
			{
			color:#6677DD;
			font-weight:bold;
			}
	SPAN.TESTcpTodayTextDisabled
			{
			color:#D0D0D0;
			}
	.TESTcpBorder
			{
			border:solid thin #6677DD;
			}
</STYLE>
</head>

<?
    echo "<body>";
    echo "<div style=border:50px solid red;padding:10px;>";
    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%><tr><td colspan=2>";
    echo "<a href=businesstrack.php><img src=logo.jpg border=0 height=43 width=205></a><p></td></tr>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM company WHERE companyid = '$companyid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $companyname=Treat_DB_ProxyOld::mysql_result($result,0,"companyname");

    //echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";
    echo "<tr bgcolor=#CCCCFF><td colspan=2><font size=4><b>Payables - $companyname</b></font></td>";
    echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";
    echo "</table></center><p>";

    if ($sort=="invoiceid"){$sort="businessid";}
    if ($date1==""){$date1=$today;}

    if ($findinv!=""){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM apinvoice WHERE companyid = '$companyid' AND invoicenum = '$findinv' AND transfer = '0'";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();
    }
    elseif ($sort=="submit"){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM apinvoice WHERE companyid = '$companyid' AND posted = '1' AND export = '0' AND transfer = '0' ORDER BY businessid, master_vendor_id, invoicenum, date DESC";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();
    }
    elseif ($sort=="unsubmit"){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM apinvoice WHERE companyid = '$companyid' AND posted = '0' AND export = '0' AND transfer = '0' ORDER BY businessid, master_vendor_id, invoicenum, date DESC";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();
    }
    elseif ($vendor!=0){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM apinvoice WHERE companyid = '$companyid' AND date >= '$date1' AND date <= '$date2' AND master_vendor_id = '$vendor' AND transfer = '0' ORDER BY $sort, date";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();
    }
    else{
       if ($sort==""){$sort="businessid";}
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM apinvoice WHERE companyid = '$companyid' AND date >= '$date1' AND date <= '$date2' AND transfer = '0' ORDER BY $sort, date";
       $result = Treat_DB_ProxyOld::query($query);
       if ($result!=""){$num=Treat_DB_ProxyOld::mysql_numrows($result);}
       else {$num=0;}
       //mysql_close();
    }

    echo "<center><table width=90% border=0 cellspacing=0 cellpadding=0><tr valign=bottom><td colspan=5><FORM ACTION='comapinvoice.php' method='post'>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<input type=hidden name=businessid value=$businessid>";
    echo "<input type=hidden name=companyid value=$companyid>";
    echo "<table width=100%><tr><td>Display ";
    echo "<SCRIPT LANGUAGE='JavaScript' ID='js18'> var cal18 = new CalendarPopup('testdiv1');cal18.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date1 VALUE='$date1' SIZE=8> <A HREF=# onClick=cal18.select(document.forms[0].date1,'anchor18','yyyy-MM-dd'); return false; TITLE=cal18.select(document.forms[0].date1,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor18' ID='anchor18'><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A> to ";
    echo "<SCRIPT LANGUAGE='JavaScript' ID='js19'> var cal19 = new CalendarPopup('testdiv1');cal19.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date2 VALUE='$date2' SIZE=8> <A HREF=# onClick=cal19.select(document.forms[0].date2,'anchor19','yyyy-MM-dd'); return false; TITLE=cal19.select(document.forms[0].date2,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor19' ID='anchor19'><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A>";

    if ($sort=="businessid"){$c1="SELECTED";}
    elseif ($sort=="master_vendor_id"){$c2="SELECTED";}
    elseif ($sort=="invoicenum"){$c3="SELECTED";}
    elseif ($sort=="total"){$c4="SELECTED";}
    elseif ($sort=="date"){$c5="SELECTED";}
    elseif ($sort=="submit"){$c6="SELECTED";}
    elseif ($sort=="unsubmit"){$c7="SELECTED";}
    echo " Sort by <select name=sort><option value='businessid' $c1>Business</option><option value='master_vendor_id' $c2>Vendor</option><option value='invoicenum' $c3>Invoice#</option><option value='total' $c4>Total</option><option value='date' $c5>Date</option><option value='submit' $c6>Submitted</option><option value='unsubmit' $c7>Unsubmitted</option></select> ";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query2 = "SELECT * FROM vendor_master WHERE companyid = '$companyid' AND ((is_deleted = '0') OR (is_deleted = '1' AND del_date >= '$date1')) ORDER BY vendor_code DESC";
    $result2 = Treat_DB_ProxyOld::query($query2);
    $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
    //mysql_close();

    echo "<select name=vendor><option value=0>All Vendors</option>";
    $num2--;
    while ($num2>=0){
      $vendor_master_id=Treat_DB_ProxyOld::mysql_result($result2,$num2,"vendor_master_id");
      $vendor_code=Treat_DB_ProxyOld::mysql_result($result2,$num2,"vendor_code");
      $vendor_name=Treat_DB_ProxyOld::mysql_result($result2,$num2,"vendor_name");
      if ($vendor_master_id==$vendor){$CHECK="SELECTED";}
      else {$CHECK="";}
      echo "<option value=$vendor_master_id $CHECK>$vendor_code - $vendor_name</option>";
      $num2--;
    }

    if ($viewtotal=="on"){$viewcheck="CHECKED";}
    else {$viewcheck="";}
    echo "</select> <input type=checkbox name='viewtotal' $viewcheck>Subtotals";

    echo " <br><i><b><u>OR</u></b></i> Find Invoice#: <INPUT TYPE=text name=findinv size=10> <INPUT TYPE=submit VALUE='GO'></FORM></td><td align=right></td></tr></table></td></tr>";
    //echo "<tr bgcolor=black><td height=1 colspan=5></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td><b>Invoice#</b></td><td><b>Business</b><td><b>Vendor</b></td><td><b>Date</b></td><td align=right><b>Total</b></td></tr>";
    echo "<tr bgcolor=black><td height=1 colspan=5></td></tr>";

    $grandtotal=0;
    $errorcount=-1;
    $results=$num;
    $subtotal=0;
    $subcount=0;
    $num--;
    $lastbusinessid=0;
    $lastvendorid=0;
    $lastdate=0;
    $lastinvoicenum=0;
    while ($num>=-1){
       $total=0;

       if ($num!="-1"){
          $apinvoiceid=Treat_DB_ProxyOld::mysql_result($result,$num,"apinvoiceid");
          $businessid=Treat_DB_ProxyOld::mysql_result($result,$num,"businessid");
          $vendorid=Treat_DB_ProxyOld::mysql_result($result,$num,"vendor");
          $apinvoicedate=Treat_DB_ProxyOld::mysql_result($result,$num,"date");
          $invoicenum=Treat_DB_ProxyOld::mysql_result($result,$num,"invoicenum");
          $total=Treat_DB_ProxyOld::mysql_result($result,$num,"total");
          $inputtotal=Treat_DB_ProxyOld::mysql_result($result,$num,"invtotal");
          $notes=Treat_DB_ProxyOld::mysql_result($result,$num,"notes");
          $total=money($total);

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query2 = "SELECT * FROM vendors WHERE vendorid = '$vendorid'";
          $result2 = Treat_DB_ProxyOld::query($query2);
          //mysql_close();
          $master=Treat_DB_ProxyOld::mysql_result($result2,0,"master");

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query2 = "SELECT * FROM vendor_master WHERE vendor_master_id = '$master'";
          $result2 = Treat_DB_ProxyOld::query($query2);
          //mysql_close();
          $vendor_name=Treat_DB_ProxyOld::mysql_result($result2,0,"vendor_name");
       }
       else{
          $businessid="-99";
          $master="-99";
          $apinvoicedate="0000-00-00";
          $invoicenum="-99";
       }

       if ($viewtotal=="on" && $sort == "businessid" && $lastbusinessid != $businessid && $lastbusinessid != 0)
       {
          $subtotal=money($subtotal);
          echo "<tr bgColor='#E8E7E7'><td>Results: $subcount</td><td colspan=3></td><td align=right><b>$$subtotal</b></td></tr>";
          echo "<tr bgcolor=#CCCCCC><td height=1 colspan=6></td></tr>";
          $subcount=0;
          $subtotal=0;
          $lastbusinessid=0;
       }
       else if ($viewtotal=="on" && $sort == "date" && $lastdate != $apinvoicedate && $lastdate != 0)
       {
          $subtotal=money($subtotal);
          echo "<tr bgColor='#E8E7E7'><td>Results: $subcount</td><td colspan=3></td><td align=right><b>$$subtotal</b></td></tr>";
          echo "<tr bgcolor=#CCCCCC><td height=1 colspan=6></td></tr>";
          $subcount=0;
          $subtotal=0;
          $lastdate=0;
       }
       else if ($viewtotal=="on" && $sort == "master_vendor_id" && $lastvendorid != $master && $lastvendorid != 0)
       {
          $subtotal=money($subtotal);
          echo "<tr bgColor='#E8E7E7'><td>Results: $subcount</td><td colspan=3></td><td align=right><b>$$subtotal</b></td></tr>";
          echo "<tr bgcolor=#CCCCCC><td height=1 colspan=6></td></tr>";
          $subcount=0;
          $subtotal=0;
          $lastvendorid=0;
       }
       else if ($viewtotal=="on" && $sort == "submit" && $lastbusinessid != $businessid && $lastbusinessid != 0)
       {
          $subtotal=money($subtotal);
          echo "<tr bgColor='#E8E7E7'><td>Results: $subcount</td><td colspan=3></td><td align=right><b>$$subtotal</b></td></tr>";
          echo "<tr bgcolor=#CCCCCC><td height=1 colspan=6></td></tr>";
          $subcount=0;
          $subtotal=0;
          $lastbusinessid=0;
       }
       else if ($viewtotal=="on" && $sort == "unsubmit" && $lastbusinessid != $businessid && $lastbusinessid != 0)
       {
          $subtotal=money($subtotal);
          echo "<tr bgColor='#E8E7E7'><td>Results: $subcount</td><td colspan=3></td><td align=right><b>$$subtotal</b></td></tr>";
          echo "<tr bgcolor=#CCCCCC><td height=1 colspan=6></td></tr>";
          $subcount=0;
          $subtotal=0;
          $lastbusinessid=0;
       }
       else if ($viewtotal=="on" && $sort == "invoicenum" && "$lastinvoicenum" !== "$invoicenum" && $lastinvoicenum != 0)
       {
          $subtotal=money($subtotal);
          echo "<tr bgColor='#E8E7E7'><td>Results: $subcount</td><td colspan=3></td><td align=right><b>$$subtotal</b></td></tr>";
          echo "<tr bgcolor=#CCCCCC><td height=1 colspan=6></td></tr>";
          $subcount=0;
          $subtotal=0;
          $lastinvoicenum=0;
       }
       else{

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query2 = "SELECT * FROM business WHERE businessid = '$businessid'";
          $result2 = Treat_DB_ProxyOld::query($query2);
          //mysql_close();
          if ($num!=-1){$businessname=Treat_DB_ProxyOld::mysql_result($result2,0,"businessname");}

          $grandtotal=$grandtotal+$total;
          $subtotal=$subtotal+$total;
          $subcount++;

          $amount2=0;
          if ($sort=="submit"){
             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query7 = "SELECT * FROM apinvoicedetail WHERE apinvoiceid = '$apinvoiceid'";
             $result7 = Treat_DB_ProxyOld::query($query7);
             //mysql_close();
             $num7=Treat_DB_ProxyOld::mysql_numrows($result7);

             $amount2=0;
             $num7--;
             while($num7>=0){
                $amount=Treat_DB_ProxyOld::mysql_result($result7,$num7,"amount");
                $amount2=$amount2+$amount;
                $num7--;
             }
          }

          $amount2=money($amount2);
          if ($total!=$amount2 && $sort=="submit"){$showerror7="<b><font color=red>ERROR</font></b>";$errorcount++;}
          else {$showerror7="";}

          if (money($total)!=money($inputtotal)){$showerror="<font color=red>*</font>";}
          else {$showerror="";}
          if ($total<0){$invcolor="red";}
          else {$invcolor="blue";}
          if ($total<0){$total="<font color=red>$total</font>";}
          if ($notes!=""){$shownotes="<a href=showapnotes.php?apinvoiceid=$apinvoiceid target='_blank'><font size=1 color=red>[NOTE]</a>";}
          else {$shownotes="";}

          if ($num!=-1){echo "<tr onMouseOver=this.bgColor='#999999' onMouseOut=this.bgColor='white'><td><a style=$style href=apinvoice.php?bid=$businessid&cid=$companyid&apinvoiceid=$apinvoiceid&goto=1><font color='$invcolor'>$invoicenum</font></a> $shownotes</td><td>$businessname</td><td>$vendor_name</td><td>$apinvoicedate</td><td align=right>$$total$showerror $showerror7</td></tr>";}
          if ($num!=-1){echo "<tr bgcolor=#CCCCCC><td height=1 colspan=6></td></tr>";}

          $lastdate=$apinvoicedate;
          $lastbusinessid=$businessid;
          $lastvendorid=$master;
          $lastinvoicenum=$invoicenum;

          $num--;
       }
    }

    echo "<tr bgcolor=black><td height=1 colspan=5></td></tr>";
    $grandtotal=money($grandtotal);
    echo "<tr><td>Results: $results</td><td colspan=3></td><td align=right><b>Total: $$grandtotal</b></td></tr>";
    if ($sort=="submit"){echo "<tr><td></td><td colspan=3></td><td align=right><b>Errors: $errorcount</b></td></tr>";}

    echo "</table></center><DIV ID=testdiv1 STYLE=position:absolute;visibility:hidden;background-color:white;layer-background-color:white;></DIV>";

    //mysql_close();

    echo "<br><FORM ACTION=businesstrack.php method=post>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<center><INPUT TYPE=submit VALUE=' Return to Main Page'></FORM></center></body>";

	google_page_track();
}
?>