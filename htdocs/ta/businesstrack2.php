<?php
define('DOC_ROOT', dirname(dirname(__FILE__)) );
require_once(DOC_ROOT.'/bootstrap.php');

function creategraph($sales,$budget,$color,$showtitle){

       if($budget!=0){
          $diff=round($sales/($budget*2)*200,0);
          if($diff>200){$diff=200;}
       }
       elseif($sales>0){$diff=200;}
       else{$diff=0;}

       if($sales>=$budget&&$sales!=0){$graphcolor=$color;}
       else{$graphcolor="#CCCCCC";}

       $showdiff=number_format($sales-$budget,2);
       if($showdiff>0){$showdiff="+$showdiff";}

       //if($showtitle==1){$showbudg=number_format($budget,2);$showavg="Daily Average: $showbudg";}
       //else{$showbudg=number_format($budget,2);$showavg="Budget: $showbudg";}

       if($showtitle==1){$showtitle=number_format($sales,2);}
       else{$showtitle=$showdiff;}

       $graph="<table cellspacing=0 cellpadding=0 width=200 title='$showtitle'>";
       $graph="$graph<tr height=2 bgcolor=#EEEEEE><td colspan=200><img src=clear.gif height=1 width=1></td></tr>";
       $graph="$graph<tr height=2 bgcolor=#EEEEEE><td colspan=99><img src=clear.gif height=1 width=1></td><td colspan=2 bgcolor=$graphcolor title='$showavg'><img src=clear.gif height=1 width=1></td><img src=clear.gif height=1 width=1><td colspan=99><img src=clear.gif height=1 width=1></td></tr>";
       if($diff<1){
          $graph="$graph<tr height=6><td colspan=99 bgcolor=#EEEEEE><img src=clear.gif height=1 width=1></td><td colspan=2 bgcolor=$graphcolor title='$showavg'><img src=clear.gif height=1 width=1></td><td colspan=99 bgcolor=#EEEEEE><img src=clear.gif height=1 width=1></td></tr>";
       }
       elseif($diff<=98){
          $diff2=99-$diff;
          $graph="$graph<tr height=6><td colspan=$diff bgcolor=#444455><img src=clear.gif height=1 width=1></td><td colspan=$diff2 bgcolor=#EEEEEE><img src=clear.gif height=1 width=1></td><td colspan=2 bgcolor=$graphcolor title='$showavg'><img src=clear.gif height=1 width=1></td><td colspan=99 bgcolor=#EEEEEE><img src=clear.gif height=1 width=1></td></tr>";
       }
       elseif($diff>98&&$diff<102){
          $graph="$graph<tr height=6><td colspan=99 bgcolor=#444455><img src=clear.gif height=1 width=1></td><td colspan=2 bgcolor=$graphcolor title='$showavg'><img src=clear.gif height=1 width=1></td><td colspan=99 bgcolor=#EEEEEE><img src=clear.gif height=1 width=1></td></tr>";
       }
       elseif($diff>=102){
          $diff2=$diff-101;
          if($diff2>99){$diff2=99;}
          $diff3=99-$diff2;
          $graph="$graph<tr height=6><td colspan=99 bgcolor=#444455><img src=clear.gif height=1 width=1></td><td colspan=2 bgcolor=$graphcolor title='$showavg'><img src=clear.gif height=1 width=1></td><td colspan=$diff2 bgcolor=#444455><img src=clear.gif height=1 width=1></td><td colspan=$diff3 bgcolor=#EEEEEE><img src=clear.gif height=1 width=1></td></tr>";
       }
       $graph="$graph<tr height=2 bgcolor=#EEEEEE><td colspan=99><img src=clear.gif height=1 width=1></td><td colspan=2 bgcolor=$graphcolor title='$showavg'><img src=clear.gif height=1 width=1></td><td colspan=99><img src=clear.gif height=1 width=1></td></tr>";
       $graph="$graph<tr height=2 bgcolor=#EEEEEE><td colspan=200><img src=clear.gif height=1 width=1></td></tr></table>";

       return $graph;
}

function hgraph($number,$total,$color,$width,$diff,$bgcolor,$showpercent,$divid){
       if($divid!=""){$color="white";}

       if($diff!=0){
          if($diff>=0){$diff=number_format($diff,2); $showdiff="<img src=budget_good.gif height=16 width=13 alt=$diff>";}
          else{$diff=number_format($diff,2); $showdiff="<img src=budget_bad.gif height=16 width=15 alt=$diff>";}
       }

       $showheight=round($number/($total/200),0);
       if($showheight>200){$showheight=200;}
       $showtitle=number_format($number,2);
       if($number<=0){$color="";}
       if($showpercent>0){
          $newheight=round($showheight/$showpercent,1);

          $graph="<td valign=bottom width=$width title='$showtitle' bgcolor=$bgcolor>$showdiff";

          for($counter=100;$counter>=(100/$showpercent);$counter=$counter-(100/$showpercent)){
             $graph.="<div style=\"height:$newheight px; width:100%; background-color:$color;\"><div style=\"height:1 px; width:100%; background-color:black;\"><img src=clear.gif></div><font size=1 face=arial><b>$counter</b></font></div>";
          }

          $graph.="</td>";
       }
       elseif($showheight>0){$graph="<td valign=bottom width=$width title='$showtitle' bgcolor=$bgcolor>$showdiff<div id='$divid' style=\"height:$showheight px; width:100%; background-color:$color;\">&nbsp;</div></td>";}
       else{$graph="<td valign=bottom width=$width title='$showtitle' bgcolor=$bgcolor>$showdiff</td>";}

       return $graph;
}

$style = "text-decoration:none";
$style2 = "background-color:#FFFF99";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");
$bbdayname=date("l");
$bbmonth=date("F");
$bbday=date("j");

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( mysql_error());

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "DELETE FROM reserve WHERE date < '$today' AND status = '2'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

/*$user=$_POST['username'];
$pass=$_POST['password'];*/
	
$user = \EE\Controller\Base::getPostVariable('username');
$pass = \EE\Controller\Base::getPostVariable('password');

//////SQL INJECTION 
$user = str_replace("'","",$user);
$pass = str_replace("'","",$pass);
$user = str_replace(";","",$user);
$pass = str_replace(";","",$pass);

/*$view=$_POST['view'];
$spec=$_POST['spec'];
$login_goto=$_POST['login_goto'];
$date1=$_COOKIE["date1cook"];*/

$view = \EE\Controller\Base::getPostVariable('view');
$spec = \EE\Controller\Base::getPostVariable('spec');
$login_goto = \EE\Controller\Base::getPostVariable('login_goto');
$date1 = \EE\Controller\Base::getSessionCookieVariable('date1cook');

if ($user==""&&$pass==""){
   /*$user=$_COOKIE["usercook"];
   $pass=$_COOKIE["passcook"];*/
	
	$user = \EE\Controller\Base::getSessionCookieVariable('usercook');
	$pass = \EE\Controller\Base::getSessionCookieVariable('passcook');
}

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=Treat_DB_ProxyOld::mysql_numrows($result);
//mysql_close();

$jobtype=Treat_DB_ProxyOld::mysql_result($result,0,"jobtype");
$loginid=Treat_DB_ProxyOld::mysql_result($result,0,"loginid");
$temp_expire=Treat_DB_ProxyOld::mysql_result($result,0,"temp_expire");
$passexpire=Treat_DB_ProxyOld::mysql_result($result,0,"passexpire");

$bid1=Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
$bid2=Treat_DB_ProxyOld::mysql_result($result,0,"busid2");
$bid3=Treat_DB_ProxyOld::mysql_result($result,0,"busid3");
$bid4=Treat_DB_ProxyOld::mysql_result($result,0,"busid4");
$bid5=Treat_DB_ProxyOld::mysql_result($result,0,"busid5");
$bid6=Treat_DB_ProxyOld::mysql_result($result,0,"busid6");
$bid7=Treat_DB_ProxyOld::mysql_result($result,0,"busid7");
$bid8=Treat_DB_ProxyOld::mysql_result($result,0,"busid8");
$bid9=Treat_DB_ProxyOld::mysql_result($result,0,"busid9");
$bid10=Treat_DB_ProxyOld::mysql_result($result,0,"busid10");

$oo8=Treat_DB_ProxyOld::mysql_result($result,0,"oo8");

setcookie("userjob",$jobtype);
setcookie("logincook",$loginid);

$ip=$_SERVER['REMOTE_ADDR']; 
$query33 = "SELECT * FROM blacklist_ip WHERE ip_address LIKE '$ip'";
$result33 = Treat_DB_ProxyOld::query($query33);
$num33=Treat_DB_ProxyOld::mysql_numrows($result33);

if($num33>0){
   $location="/error.htm";
   header('Location: ' . $location);

   $query = "INSERT INTO audit_login (user,pass,ip,redirect) VALUES ('$user','$pass','$ip','1')";
   $result = Treat_DB_ProxyOld::query($query);
}
elseif ($num != 1 || ($user == "" && $pass == ""))
{
    echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";

    $ip=$_SERVER['REMOTE_ADDR']; 

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "INSERT INTO audit_login (user,pass,ip) VALUES ('$user','$pass','$ip')";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();
}

elseif ($temp_expire<=$today&&$temp_expire!="0000-00-00")
{
    echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}
elseif ($passexpire<=$today)
{
    setcookie("usercook",$user);
    setcookie("passcook",$pass);
    $location="editpass.php?ex=1&edit=1";
    header('Location: ./' . $location);
}
else
{

    if ($todayname=="Friday"){
       $day1=$today;
       $lastweek=pastday(7);
    }
    elseif ($todayname=="Saturday"){
       $day1=pastday(1);
       $lastweek=pastday(8);
    }
    elseif ($todayname=="Sunday"){
       $day1=pastday(2);
       $lastweek=pastday(9);
    }
    elseif ($todayname=="Monday"){
       $day1=pastday(3);
       $lastweek=pastday(10);
    }
    elseif ($todayname=="Tuesday"){
       $day1=pastday(4);
       $lastweek=pastday(11);
    }
    elseif ($todayname=="Wednesday"){
       $day1=pastday(5);
       $lastweek=pastday(12);
    }
    elseif ($todayname=="Thursday"){
       $day1=pastday(6);
       $lastweek=pastday(13);
    }

    $compcook=Treat_DB_ProxyOld::mysql_result($result,0,"companyid");
    $buscook=Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
    $busid1=$buscook;
    $user=Treat_DB_ProxyOld::mysql_result($result,0,"username");
    $loginid=Treat_DB_ProxyOld::mysql_result($result,0,"loginid");
    $mylinks=Treat_DB_ProxyOld::mysql_result($result,0,"mylinks");
    $myar=Treat_DB_ProxyOld::mysql_result($result,0,"myar");
    $myap=Treat_DB_ProxyOld::mysql_result($result,0,"myap");
    $order_only=Treat_DB_ProxyOld::mysql_result($result,0,"order_only");
    $oo2=Treat_DB_ProxyOld::mysql_result($result,0,"oo2");
    $oo3=Treat_DB_ProxyOld::mysql_result($result,0,"oo3");
    if($oo4==""){$oo4=Treat_DB_ProxyOld::mysql_result($result,0,"oo4");}
    $oo5=Treat_DB_ProxyOld::mysql_result($result,0,"oo5");
    $oo6=Treat_DB_ProxyOld::mysql_result($result,0,"oo6");
    $oo7=Treat_DB_ProxyOld::mysql_result($result,0,"oo7");
    $oo8=Treat_DB_ProxyOld::mysql_result($result,0,"oo8");
    $oo9=Treat_DB_ProxyOld::mysql_result($result,0,"oo9");
    $oo10=Treat_DB_ProxyOld::mysql_result($result,0,"oo10");

    setcookie("usercook",$user);
    setcookie("passcook",$pass);
    setcookie("logincook",$loginid);
    setcookie("compcook",$compcook);
    if ($date1==""){setcookie("date1cook",$day1);}
    setcookie("caterbus",0);

    if(checkmobile()==true){
       $location="mobile/index.php";
       header('Location: ./' . $location);
    }
    elseif($login_goto==1){
       $mysecurity=Treat_DB_ProxyOld::mysql_result($result,0,"security");

       $query22 = "SELECT * FROM security WHERE securityid = '$mysecurity'";
       $result22 = Treat_DB_ProxyOld::query($query22);

       $sec_bus_payable2=Treat_DB_ProxyOld::mysql_result($result22,0,"bus_payable2");

       if($sec_bus_payable2>0){

          $query22 = "SELECT * FROM purchase_card WHERE loginid = '$loginid' AND type = '0' AND is_deleted = '0'";
          $result22 = Treat_DB_ProxyOld::query($query22);

          $er_busid=Treat_DB_ProxyOld::mysql_result($result22,0,"businessid");
          $er_comid=Treat_DB_ProxyOld::mysql_result($result22,0,"companyid");

          if($er_busid>0){

             $query22 = "UPDATE login SET companyid = '$er_comid' WHERE loginid = '$loginid'";
             $result22 = Treat_DB_ProxyOld::query($query22);

             $location="busapinvoice2.php?cid=$er_comid&bid=$er_busid";
             header('Location: ./' . $location);
          }
          else{
             $location="/";
             header('Location: ' . $location);
          }
       }
       else{
          $location="/";
          header('Location: ' . $location);
       }
    }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" >
<html>
<head>
<title>Essential Elements :: Treat America</title>

<STYLE>
body, div, table {text-align: left;}
.matched {background: yellow; border: 1px solid #888;}
.matchedA {background: #cfc; border: 1px solid #888;}
.quickNavActive {background: #cfc; border: 1px solid #888;}

#navBox {padding: 2px; width: 300px;}
#navBox ul{list-style: circle;}
#navBox li.mult {list-style-type: disc; padding: 0;}
#navBox li.multA {list-style-type: square; padding: 0;}
#navBox li ul {
		list-style: none;
		padding: 0 0 0 5px;
		display: inline;
	}
#navBox li ul li{ padding-left: 5px; }

#loading {
 	width: 0px;
 	height: 0px;
 	background-color: ;
 	position: absolute;
 	left: 50%;
 	top: 50%;
 	margin-top: -50px;
 	margin-left: -100px;
 	text-align: center;
}
</STYLE>

<script language="JavaScript" 
   type="text/JavaScript">
function changePage(newLoc)
 {
   nextPage = newLoc.options[newLoc.selectedIndex].value
		
   if (nextPage != "")
   {
      document.location.href = nextPage
   }
 }

 var update_expense = function(){
        var x = get_cookie ( "logincook" );
	new Ajax.Request('ajax_expense.php', {parameters: {method: 'ajax_expense1', lid: x}});
        
        setTimeout( "document.getElementById('bboard').contentWindow.location.reload();", 1500); 
 }

function get_cookie ( cookie_name )
{
  var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );

  if ( results )
    return ( unescape ( results[2] ) );
  else
    return null;
}

</script>

<SCRIPT LANGUAGE="JavaScript">
<!-- Web Site:  http://dynamicdrive.com -->

<!-- This script and many more are available free online at -->
<!-- The JavaScript Source!! http://javascript.internet.com -->

<!-- Begin
function disableForm(theform) {
if (document.all || document.getElementById) {
for (i = 0; i < theform.length; i++) {
var tempobj = theform.elements[i];
if (tempobj.type.toLowerCase() == "submit" || tempobj.type.toLowerCase() == "reset")
tempobj.disabled = true;
}

}
else {
alert("The form has been submitted.  But, since you're not using IE 4+ or NS 6, the submit button was not disabled on form submission.");
return false;
   }
}
//  End -->

</script>

<script type="text/javascript" src="javascripts/prototype.js"></script>
<script type="text/javascript" src="javascripts/scriptaculous.js"></script>

<script language="javascript" type="text/javascript">

var divexpand=function(){

   <?php if($user == "ZZZZSteveMZZZZ" ){ ?>

	var links = document.getElementsByTagName('a');
	for(i = 0; i < links.length; i++){
            if (links[i].innerHTML.match("American Family Insurance")){
		links[i].onmouseover = function(){ var ov = this.innerHTML; this.style.position = 'absolute'; var c = $(this).positionedOffset(); var ax = 150; var ay = 150; if(Math.random() > .6) ax = ax * -1; if(Math.random() > .6) ay = ay * -1; var nx = c[0] + ax; var ny = c[1] + ay; var obj = $(this); setTimeout(function(){new Effect.Shake(obj, {duration: .5});}, 500); setTimeout(function(){obj.update(ov);}, 1500); new Effect.Move(this, {mode: 'absolute', x: nx, y: ny, duration: .3}); } 
	    }
	}


   <?php } ?>

   //init();

   var newheight = document.getElementById("thermmonth").style.height;
   document.getElementById("thermmonth").style.height='0px';
   
   new Effect.Morph('thermmonth', {
      style: {
         background: '#414141',
         color: '#414141',
         height: newheight
      }, 
      duration: 3.0
   });  

   var newheight = document.getElementById("thermyear").style.height;
   document.getElementById("thermyear").style.height='0px';
   
   new Effect.Morph('thermyear', {
      style: {
         background: '#414141',
         color: '#414141',
         height: newheight
      }, 
      duration: 4.0
   });  

   var newvalue = document.getElementById("thermyear_pcnt").innerHTML;
   newvalue = newvalue.replace("&nbsp;","");
   newvalue = newvalue.replace("&nbsp;","");
   newvalue = newvalue.replace("&nbsp;","");
   newvalue = newvalue.replace("&nbsp;","");
   newvalue = newvalue*1;

   if(newvalue >= 100){var mycolor = "#00FF00";}
   else if(newvalue >= 75){var mycolor = "#FF0000";}
   else if(newvalue >= 50){var mycolor = "#FF9900";}
   else if(newvalue >= 25){var mycolor = "#FFFF00";}
   else{var mycolor = "#FFFFFF";}

   new Effect.Morph('thermyear_pcnt', {
      style: {
         color: mycolor
      }, 
      duration: 6.0
   });  

   var newvalue = document.getElementById("thermmonth_pcnt").innerHTML;
   newvalue = newvalue.replace("&nbsp;","");
   newvalue = newvalue.replace("&nbsp;","");
   newvalue = newvalue.replace("&nbsp;","");
   newvalue = newvalue.replace("&nbsp;","");
   newvalue = newvalue*1;

   if(newvalue >= 100){var mycolor = "#00FF00";}
   else if(newvalue >= 75){var mycolor = "#FF0000";}
   else if(newvalue >= 50){var mycolor = "#FF9900";}
   else if(newvalue >= 25){var mycolor = "#FFFF00";}
   else{var mycolor = "#FFFFFF";}

   new Effect.Morph('thermmonth_pcnt', {
      style: {
         color: mycolor
      }, 
      duration: 5.0
   });  

}


var grabScreenPOS = function(){
	if($('reportFloater')){ //Make sure DIV exists on page on page
		var b = $('reportFloater');                    //The Div Tag
		var bh = b.getHeight();                        //Div Height
		var wh  = $('divbox').getHeight();                                 //Window Height: Hard Coded to avoid IE Bug
		var co = document.viewport.getScrollOffsets(); //Array of scroll offsets
		var sh = co[1];                                //ScrollTop

		var n = (wh / 2); //Grab 1/2 Window height
		n -= (bh / 2);    //Substract 1/2 Box height  [This gets it centered]
		n += sh;          //Add the scroll offset [Make it follow scrollbar]

		return n;
	}
}

var followScroll = function(){
	if($('reportFloater')){ //Make sure DIV exists on page on page
		var b = $('reportFloater');                    //The Div Tag

		var n = grabScreenPOS();
		b.style.top = n +'px'; //Set the new top value to you're div
	}
}
var initScrollBox = function(){
	if($('reportFloater')){ //Make sure DIV exists on page on page
		var b = $('reportFloater');                    //The Div Tag

		var n = grabScreenPOS();
		b.style.top = n +'px'; //Set the new top value to you're div

		if(!b.visible()) b.show(); //If display:none => set display: block  [Gets rid of jump onLoad]
	}
}


var showUpdate = function(link){
	var l = $(link);
	var p = l.positionedOffset();

	$('mailStatus').update('Retrieving Emails...');
	$('mailStatus').style.left = p[0] + $('mailStatus').getWidth() + 'px';
	$('mailStatus').style.top = p[1] + 'px';
	$('mailStatus').show();
}

var clickHandler = function(e){
	var ev = e || window.event;
	
	if(ev.shiftKey){
		if(this.href.search(/busdetail/) > -1){
			showUpdate(this);
			new Ajax.Request('ajaxreq.php', {parameters: {method: 'btEmailLink', h: this.href}});
			return false;
		}
	}

	return true;
}

var bindShiftClickHandler = function(){
	var links = $('mainTable').getElementsByTagName('a');
	
	for(i = 0; i < links.length; i++){
		var l = links[i];
		l.onclick = clickHandler;
	}
}

/*                                  Quick Navigation: TF 7-22-09          */
var quickNav = {};
//First Tree
quickNav['Sales']             = 'busdetail';
quickNav['Invoices']          = 'businvoice';
quickNav['Orders']            = 'buscatertrack';
quickNav['Payables']          = {};
quickNav['Inventory']         = {};
quickNav['Control']           = 'buscontrol';
quickNav['Menus']             = 'buscatermenu';
quickNav['Order Setup']       = 'buscaterroom';
quickNav['Requests']          = 'busrequest';
quickNav['Route Collections'] = {};
quickNav['Labor']             = 'buslabor';
//Sub Tree Inventory
quickNav['Inventory']['main']    = 'businventor';
quickNav['Inventory']['Count']   = 'businventor2';
quickNav['Inventory']['Setup']   = 'businventor3';
quickNav['Inventory']['Recipes'] = 'businventor4';
quickNav['Inventory']['Vendors'] = 'businventor5';
//Sub Tree Payables
quickNav['Payables']['main']             = 'busapinvoice';
quickNav['Payables']['Expense Reports']  = 'busapinvoice2';
quickNav['Payables']['Capital Expenses'] = 'busapinvoice3';
//Sub Tree Route Coll.
quickNav['Route Collections']['main']            = 'busroute_collect';
quickNav['Route Collections']['Balance Sheet']   = 'busroute_balance';
quickNav['Route Collections']['Reporting']       = 'busroute_report';
quickNav['Route Collections']['Payroll/Linking'] = 'busroute_labor';
quickNav['Route Collections']['Route Details']   = 'busroute_detail';

var quickMenu = function(td, cid, bid){
	if($('navBox').tdEl) $('navBox').tdEl.className = '';
	td.className = 'quickNavActive';
	$('navBox').update('Loading Menu...');
	$('navBox').tdEl = td;
	$('navBox').style.top = $(td).positionedOffset().top + 'px';
	$('navBox').style.left = $(td).positionedOffset().left + $(td).getWidth() - 40 + 'px';
	$('navBox').show();
	loadNavMenu(cid, bid);
}
var clearMenu = function(td){
	setTimeout(function(){ if(!$('navBox').passedOv){ $('navBox').hide(); $('navBox').passedOv = false; $('navBox').tdEl.className = ''; }}, 3000);
}
var loadNavMenu = function(cid, bid){
	$('navBox').update('<div align="center"><i>Quick Nav</i></div>');
	var d  = $(document.createElement('div'));
	var mu = $(document.createElement('ul'));
	var t  = new Template('.php?cid=#{cid}&bid=#{bid}');
	var o  = {cid: cid, bid: bid};
	var q  = t.evaluate(o);

	for(i in quickNav){
		if(Object.isString(quickNav[i])){
			var li = $(document.createElement('li'));
			var link = $(document.createElement('a'));
			link.update(i);
			link.href = quickNav[i]+q;
			li.appendChild(link);
			mu.appendChild(li);
		}
		else{
			var li = $(document.createElement('li'));
			li.className = 'mult';
			var link  = $(document.createElement('a'));
			link.update(i);
			link.href = quickNav[i]['main']+q;
			li.appendChild(link);
			var su = $(document.createElement('ul'));
			su.style.display = 'none';
			for(x in quickNav[i]){
				if(x != 'main'){
					var si = $(document.createElement('li'));
					var link  = $(document.createElement('a'));
					link.update(x);
					link.href = quickNav[i][x]+q;
					si.appendChild(link);
					su.appendChild(si);
				}
			}
			li.onmouseover = function(){ $(this).down('ul').show(); this.className = 'multA'; };
			li.onmouseout = function(){ $(this).down('ul').hide();  this.className = 'mult';};
			li.appendChild(su);
			mu.appendChild(li);
		}
	}

	d.appendChild(mu);
	$('navBox').appendChild(d);
}

Event.observe(window,'load', bindShiftClickHandler);


var LiveSearch = function(opt){
	this.opt = opt;
	this.init();
	this.fetchLinks();
	this.render();
	this.bindListeners();
	this.focus();
}
LiveSearch.prototype = {
	init: function(){
		this.changedVals = [];
		this.windowH    = document.viewport.getHeight();
		this.scrolledOS = document.viewport.getScrollOffsets().top;
		Event.observe(window, 'resize', function(){ this.windowH = document.viewport.getHeight(); }.bind(this));
		Event.observe(window, 'scroll', function(){ this.scrolledOS = document.viewport.getScrollOffsets().top; }.bind(this));
	},
	fetchLinks: function(){
		var root = this.opt.root || document;
		var a = root.getElementsByTagName('a');
		this.links = a;
	},
	bindListeners: function(){
		this.input.observe('keydown', this.keyControlListener.bind(this));
		this.input.observe('keyup', this.listener.bind(this));
		this.input.observe('blur', this.blurred.bind(this));
		Event.observe(document, 'mousedown', this.findFocused.bind(this));
	},
	findFocused: function(e){
		var ev = e || window.event;

		this.focusedEl = Event.element(ev);
	},
	focus: function(){
		this.input.focus();
	},
	clear: function(){
		this.input.value = "";
	},
	resetLinks: function(){
		for(i = 0; i < this.changedVals.length; i++){
			var cur = this.changedVals[i];
			cur.el.className = '';
			$(cur.el).update(cur.val);
		}
		this.changedVals = [];
	},
	reset: function(){
		window.status = 'reset';
		this.clear();
		this.resetLinks();
	},
	goToMatch: function(match){
		var link = $(this.changedVals[match].el);
		var pos = link.positionedOffset();

		link.className = 'matchedA';
		var screen = this.scrolledOS + this.windowH;
		window.status = 'Match: ' + (this.curMatch+1) + ' of '+this.changedVals.length;
			window.status += "  " +pos[1] + ':' + screen;
		if(pos[1] >= screen || pos[1] <= this.scrolledOS){
			var half = Math.round(pos[1] - (this.windowH / 2));
			this.scrollTO = setTimeout(function(){ this.scrolledOS = half; window.status += ' New Screen: '+(this.scrolledOS+this.windowH); window.scrollTo(0,half); }.bind(this), 500);
		}
	},
	resetMatch: function(){
		if(this.curMatch > -1){
			this.changedVals[this.curMatch].el.className = '';
		}
	},
	cycleMatch: function(dir){
		this.resetMatch();
		this.curMatch += dir;

		if(this.curMatch < 0) this.curMatch = this.changedVals.length -1;
		if(this.curMatch > this.changedVals.length -1) this.curMatch = 0;

		this.goToMatch(this.curMatch);
	},
	activateMatch: function(){
		var link = this.changedVals[this.curMatch].el;
		link.focus();
	},
	keyControlListener: function(e){
		var ev = e || window.event;
		if(this.TO != null) clearTimeout(this.TO);
		
		var key = ev.keyCode;
		if(key != 16){ //Shift key
			if(key == 13){ //Enter Key
				this.activateMatch();
			}
			if(ev.shiftKey){
				if(key == 78){ 
					this.cycleMatch(1);
					return false;
				}
				if(key == 80){ 
					this.cycleMatch(-1);
					return false;
				}
			}
		}
		else return false
		return true;
	},
	listener: function(e){
		var ev = e || window.event;

		var key = ev.keyCode;
		if(key != 16 && (!ev.shiftKey || (key != 78 && key != 80))){
			this.curMatch = -1;
			this.resetLinks();
			if(this.TO != null) clearTimeout(this.TO);
			this.TO = setTimeout(this.reset.bind(this), this.opt.timeout || 1500);

			var val = this.input.value;
			if(val.length > 0){
				var reg = new RegExp();
				reg.compile(val, 'gi');

				//First find for now...
				var x = 0;
				for(i = 0; i < this.links.length; i++){
					var link = $(this.links[i]);
					if(this.opt.linkEmbed != null){
						if(link.down(this.opt.linkEmbed)){
							var disp = link.down(this.opt.linkEmbed).innerHTML;
						}
						else var disp = link.innerHTML;
					}
					else var disp = link.innerHTML;

					if(disp.search(reg) > -1){
						this.changedVals[this.changedVals.length] = {el: link, val: link.innerHTML};
						link.update(disp.replace(reg, '<span class="matched">'+val+'</span>'));
					}
					x++;
				}
				this.curMatch = 0;
				this.goToMatch(0);
			}
		}
		else{
			if(key == 78 || key == 80){
				this.input.value = this.input.value.substring(0, this.input.value.length-1);
			}
		}
	},
	blurred: function(e){
		var ev = e || window.event;

		if(this.focusedEl != null){
			var tn = this.focusedEl.tagName.toLowerCase();
			if(tn != 'select' && tn != 'input'){
				setTimeout(function(){ this.input.focus(); }.bind(this), 100);
			}
			else{
				this.focusedEl.onblur = this.blurred.bind(this);
			}
		}
	},
	render: function(){
		var i = $(document.createElement('input'));
		i.type = 'text';
		i.style.position = 'absolute';
		i.style.top = '-100px';
		i.style.left = '0px';
		this.input = i;
		document.body.appendChild(this.input);
	}};

var startLiveSearch = function(){
	new LiveSearch({
			timeout: 1500,
			linkEmbed: 'font', 
			root: $('mainTable')
			});
}

Event.observe(window, 'load', startLiveSearch);


Event.observe(window,'load', divexpand);
Event.observe(window,'load', initScrollBox); //Make the box follow on page load
Event.observe(window,'scroll', followScroll); //Make the box follow on page scroll

Event.observe($('dashboardaccounts'),'load', function(){ alert('test');});


</script>

<script type="text/javascript" src="preLoadingMessage2.js"></script>

</head>

<?php

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query5 = "SELECT * FROM login_pend WHERE companyid = '$compcook'";
    $result5 = Treat_DB_ProxyOld::query($query5);
    $num5=Treat_DB_ProxyOld::mysql_numrows($result5);
    //mysql_close();

    $laborrequest=$num5;

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query5 = "SELECT * FROM accountpend WHERE companyid = '$compcook'";
    $result5 = Treat_DB_ProxyOld::query($query5);
    $num5=Treat_DB_ProxyOld::mysql_numrows($result5);
    //mysql_close();

    $requestcount=$num5;

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query5 = "SELECT * FROM vendorpend WHERE companyid = '$compcook'";
    $result5 = Treat_DB_ProxyOld::query($query5);
    $num5=Treat_DB_ProxyOld::mysql_numrows($result5);
    //mysql_close();

    $requestcount=$requestcount+$num5;

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query5 = "SELECT * FROM apinvoice WHERE companyid = '$compcook' AND posted = '2'";
    $result5 = Treat_DB_ProxyOld::query($query5);
    $num5=Treat_DB_ProxyOld::mysql_numrows($result5);
    //mysql_close();

    $requestcount2=$num5;

    if ($laborrequest!=0){$showlaborrequest="($laborrequest)";$showstyle1="background-color:yellow";}
    else{$showstyle1="background-color:white";}
    if ($requestcount!=0){$showrequest="($requestcount)";$showstyle2="background-color:yellow";}
    else{$showstyle2="background-color:white";}
    if ($requestcount2!=0){$showrequest2="($requestcount2)";$showstyle3="background-color:yellow";}
    else{$showstyle3="background-color:white";}

    $mystyle1="background-color:#E7E7E7";

    $recent=$today;
    for($counter=1;$counter<=3;$counter++){$recent=prevday($recent);}

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query5 = "SELECT * FROM bboard WHERE date >= '$recent'";
    $result5 = Treat_DB_ProxyOld::query($query5);
    $num5=Treat_DB_ProxyOld::mysql_numrows($result5);
    //mysql_close();

    if ($num5==1){$showfaq="($num5 Update)";}
    elseif ($num5>1){$showfaq="($num5 Updates)";}
    else{$showfaq="";}

    $query35 = "SELECT apinvoice.* FROM apinvoice,purchase_card WHERE apinvoice.dm = '' AND apinvoice.transfer = '3' AND apinvoice.businessid = purchase_card.businessid AND apinvoice.vendor = purchase_card.loginid AND apinvoice.pc_type = purchase_card.type AND purchase_card.approve_loginid = '$loginid' AND apinvoice.posted != '0' ORDER BY apinvoice.date";
    $result35 = Treat_DB_ProxyOld::query($query35);
    $num35=Treat_DB_ProxyOld::mysql_numrows($result35);

    echo "<body><center><table cellspacing=0 cellpadding=0 border=0 width=90%><tr><td colspan=4><a href=logout.php><img src=logo.jpg height=43 width=205 border=0></a><p></td></tr></table>";
	echo '<div id="mailStatus" style="display: none; position: absolute; border: 1px solid black; background: #fff; padding: 2px;">&nbsp;</div>';

	echo '<div id="navBox" onmouseover="clearTimeout(this.TO); this.passedOv = true;" onmouseout="this.passedOv = false; this.TO = setTimeout(function(){ $(this).hide(); this.tdEl.className = \'\'; }.bind(this), 1000);" style="display: none; text-align: left; position: absolute; border: 1px solid black; background: #fff; padding: 2px;"></div>';
    
    echo "<div id='divbox' style=\"position: absolute; height: 100%; visibility: hidden;\">&nbsp;</div>";

    ////////////////////////////////////
    /////////////EXPENSE REPORTS////////
    ////////////////////////////////////
    if($num35>0){
       echo "<div id=\"reportFloater\" style=\"display: none; position: absolute; width: 100%; left: 0px; top: 100px; text-align: center;\">";

       echo "<center><table width=80% cellspacing=0 cellpadding=0 style=\"border: 2px solid black;\" bgcolor=white>";
       echo "<tr bgcolor=black height=16><td colspan=4><center><font color=white><b>Expense Reports</b></font></center></td></tr>";
       echo "<tr><td colspan=4>";
       
       echo "<iframe src='expense_report.php?lid=$loginid' height=500 width=100% frameborder=0></iframe>";

       echo "</td></tr>";
       echo '<tr><td colspan="4" align="right" style="font-size: 14px;"><a href="javascript: void(0);" onclick="update_expense();$(\'reportFloater\').hide();">Hide</a></td></tr>';
       echo "</table></center>";
       echo "</div>"; 
    }
    /////////////////////////////////////

    $companyid=Treat_DB_ProxyOld::mysql_result($result,0,"companyid");
    $businessid=Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
    $security_level=Treat_DB_ProxyOld::mysql_result($result,0,"security_level");
    $security=Treat_DB_ProxyOld::mysql_result($result,0,"security");
    $jobtype=Treat_DB_ProxyOld::mysql_result($result,0,"jobtype");
    $loginid=Treat_DB_ProxyOld::mysql_result($result,0,"loginid");

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM company WHERE companyid = '$companyid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $companyname=Treat_DB_ProxyOld::mysql_result($result,0,"companyname");

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM security WHERE securityid = '$security'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $sec_name=Treat_DB_ProxyOld::mysql_result($result,0,"security_name");

    $sec_multi_comp=Treat_DB_ProxyOld::mysql_result($result,0,"multi_company");
    $sec_com_setup=Treat_DB_ProxyOld::mysql_result($result,0,"com_setup");
    $sec_user=Treat_DB_ProxyOld::mysql_result($result,0,"user");
    $sec_routes=Treat_DB_ProxyOld::mysql_result($result,0,"routes");
    $sec_invoice=Treat_DB_ProxyOld::mysql_result($result,0,"invoice");
    $sec_payable=Treat_DB_ProxyOld::mysql_result($result,0,"payable");
    $sec_item_list=Treat_DB_ProxyOld::mysql_result($result,0,"item_list");
    $sec_request=Treat_DB_ProxyOld::mysql_result($result,0,"request");
    $sec_export=Treat_DB_ProxyOld::mysql_result($result,0,"export");
    $sec_utility=Treat_DB_ProxyOld::mysql_result($result,0,"utility");
    $sec_kpi=Treat_DB_ProxyOld::mysql_result($result,0,"kpi");
    $sec_kpi_graph=Treat_DB_ProxyOld::mysql_result($result,0,"kpi_graph");
    $sec_labor=Treat_DB_ProxyOld::mysql_result($result,0,"labor");
    $sec_reconcile=Treat_DB_ProxyOld::mysql_result($result,0,"reconcile");
    $sec_emp_labor=Treat_DB_ProxyOld::mysql_result($result,0,"emp_labor");
    $sec_project_manager=Treat_DB_ProxyOld::mysql_result($result,0,"project_manager");
    $sec_budget_calc=Treat_DB_ProxyOld::mysql_result($result,0,"budget_calc");
    $sec_transfer=Treat_DB_ProxyOld::mysql_result($result,0,"transfer");

    $sec_bus_sales=Treat_DB_ProxyOld::mysql_result($result,0,"bus_sales");
    $sec_bus_invoice=Treat_DB_ProxyOld::mysql_result($result,0,"bus_invoice");
    $sec_bus_order=Treat_DB_ProxyOld::mysql_result($result,0,"bus_order");
    $sec_bus_payable=Treat_DB_ProxyOld::mysql_result($result,0,"bus_payable");
    $sec_bus_inventor1=Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor1");
    $sec_bus_control=Treat_DB_ProxyOld::mysql_result($result,0,"bus_control");
    $sec_bus_menu=Treat_DB_ProxyOld::mysql_result($result,0,"bus_menu");
    $sec_bus_order_setup=Treat_DB_ProxyOld::mysql_result($result,0,"bus_order_setup");
    $sec_bus_request=Treat_DB_ProxyOld::mysql_result($result,0,"bus_request");
    $sec_route_collection=Treat_DB_ProxyOld::mysql_result($result,0,"route_collection");
    $sec_route_labor=Treat_DB_ProxyOld::mysql_result($result,0,"route_labor");
    $sec_route_detail=Treat_DB_ProxyOld::mysql_result($result,0,"route_detail");
    $sec_bus_labor=Treat_DB_ProxyOld::mysql_result($result,0,"bus_labor");
    $sec_bus_timeclock=Treat_DB_ProxyOld::mysql_result($result,0,"bus_timeclock");

    if($oo4==1){$showdash="<a href=businesstrack.php?oo4=0 style=$style><b><font color=white onMouseOver=this.color='#FF9900' onMouseOut=this.color='white' title='Click to Change'>Units</font></a>";}
    elseif($security_level>=3){$showdash="<a href=businesstrack.php?oo4=1 style=$style><b><font color=white onMouseOver=this.color='#FF9900' onMouseOut=this.color='white' title='Click to Change'>Dashboard</font></a>";}


    echo "<center><table width=90% style=\"border:2px solid #CCCCFF;background-image: url(backdrop.jpg);background-repeat: no-repeat;\" cellspacing=0 cellpadding=0>";
    echo "<tr><td colspan=3 bgcolor=#CCCCFF><a name=db><b><font color=white>Main Page</font></a></td><td colspan=2 bgcolor=#CCCCFF align=right>$showdash</td></tr>";
    if ($sec_multi_comp==1){echo "<tr valign=top><td width=10%><img src=talogo.gif height=72 width=79></td><td colspan=2 width=30%><font size=4 title='$sec_name'><b>Welcome $user</font> <a href=editpass.php><img src=edit.gif border=0 alt='Edit Your Password'></a><br>";
       echo "<form action=changecomp.php method=post onSubmit='return disableForm(this);'><input type=hidden name=loginid value=$loginid><select name=newcompany>";

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM company ORDER BY reference DESC";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();

       $num--;
       while ($num>=0){
          $newcompanyname=Treat_DB_ProxyOld::mysql_result($result,$num,"companyname");
          $reference=Treat_DB_ProxyOld::mysql_result($result,$num,"reference");
          $newcompanyid=Treat_DB_ProxyOld::mysql_result($result,$num,"companyid");
          if ($companyid==$newcompanyid){$showcomp="SELECTED";}
          else{$showcomp="";}

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query27 = "SELECT * FROM login_company WHERE companyid = '$newcompanyid' AND loginid = '$loginid'";
          $result27 = Treat_DB_ProxyOld::query($query27);
          $num27=Treat_DB_ProxyOld::mysql_numrows($result27);
          //mysql_close();

          if($num27!=0){echo "<option value=$newcompanyid $showcomp>$reference</option>";}
          $num--;
       }

       echo "<input type=submit value='GO'></b></font></form>";
     }
     else{echo "<tr valign=top><td width=10%><img src=talogo.png height=72 width=79></td><td colspan=2 width=30%><font size=4 title='$sec_name'><b>Welcome $user </font><a href=editpass.php><img src=edit.gif border=0 alt='Edit Your Password'></a><p><font size=4> $companyname</b> </font><br>";}


    echo "<FORM ACTION=direct.php method=post>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<input type=hidden name=businessid value=$businessid>";
    echo "<input type=hidden name=companyid value=$companyid>";
    echo "<select name=view onChange='changePage(this.form.view)'><option value=businesstrack.php?bid=$businessidid&companyid=$cid style=\"background:url('images/home.gif') no-repeat;\">Main Page</option>";
    
    if ($sec_com_setup==1||$sec_user==1||$sec_item_list==1){echo "<optgroup label=Settings style=$mystyle1>";}
    if ($sec_com_setup==1){echo "<option value=comsetup.php?bid=$businessid&cid=$companyid style=$mystyle1>Company Setup</option>";}
    if ($sec_user==1){echo "<option value=user_setup.php?bid=$businessid&cid=$companyid style=$mystyle1>Users/Security</option>";}
    if ($sec_routes==1){echo "<option value=user_routes.php?bid=$businessid&cid=$companyid style=$mystyle1>Vending Setup</option>";}
    if ($sec_item_list==1){echo "<option value=catermenu.php?cid=$companyid style=$mystyle1>Menu Management</option>";}   
    if ($sec_project_manager==1||$sec_com_setup==1||$sec_user==1||$sec_emp_labor==1||$sec_item_list==1){echo "</optgroup>";}

    if ($sec_reconcile==1||$sec_request==1||$sec_transfer==1||$sec_emp_labor==1||$sec_invoice==1||$sec_payable==1){echo "<optgroup label=Maintenance>";}
    if ($sec_reconcile==1){echo "<option value=reconcile.php?cid=$companyid>Reconcile</option>";}
    if ($sec_emp_labor==1){echo "<option value=emp_labor.php?bid=$businessid&cid=$companyid style=$showstyle1>Employee Labor $showlaborrequest</option>";}   
    if ($sec_request==1){echo "<option value=requests.php?cid=$companyid style=$showstyle2>Requests $showrequest</option>";}
    if ($sec_transfer==1){echo "<option value=comtransfer.php?cid=$companyid style=$showstyle3>Transfers/Purchase Cards $showrequest2</option>";}
    if ($sec_invoice==1){echo "<option value=invoice.php?cid=$companyid>Invoicing</option>";}
    if ($sec_payable==1){echo "<option value=comapinvoice.php?cid=$companyid>Payables</option>";}
    if ($sec_reconcile==1||$sec_request==1||$sec_transfer==1||$sec_invoice==1||$sec_payable==1){echo "</optgroup>";}

    echo "<optgroup label=Reports style=$mystyle1>";
    if ($sec_labor==1){echo "<option value=comlabor.php?cid=$companyid style=$mystyle1>Labor Variance</option>";}
    if ($sec_kpi==1){echo "<option value='".HTTP_COMKPI."/comkpi.htm' style=$mystyle1>Company KPI</option>";}
    if ($sec_kpi_graph==1){echo "<option value=combudgetreport.php?cid=$companyid style=$mystyle1>Budget Report</option>";}
    echo "<option value=comreport.php?cid=$companyid style=$mystyle1>Reporting</option>";
    echo "</optgroup>";

    echo "<optgroup label=Misc>";
    echo "<option value=calender.php?bid=$businessid&cid=$companyid>Calendar</option>";
    echo "<option value=bboard.php?cid=$companyid>FAQ's $showfaq</option>";
    echo "<option value=comrecipe.php?cid=$companyid>Recipes</option>";
    echo "<option value=/menu?bid=143>Nutrition Demo Site</option>";
    echo "<option value=comdirectory.php?cid=$companyid>Company Directory</option>";
    echo "<option value=message.php?bid=$businessid&cid=$companyid>Message Center</option>";
    echo "</optgroup>";

    if ($sec_project_manager==1||$sec_utility==1||$sec_export==1||$oo8>0){echo "<optgroup label=Admin style=$mystyle1>";}
    if ($sec_project_manager==1){echo "<option value=http://essential-elements.net/projects/gopm.php?goto=1 style=$mystyle1>Project Manager</option>";}
    if ($sec_budget_calc==1){echo "<option value=budget_calc.php style=$mystyle1>Budget Calculator</option>";}
    if ($sec_utility==1){echo "<option value=utility.php?cid=$companyid style=$mystyle1>Utilities</option>";}
    if ($sec_export==1){echo "<option value=exportdetail.php?cid=$companyid style=$mystyle1>Exports</option>";}
    if ($oo8>0){echo "<option value=../projects/pm_list.php?loginid=$loginid style=$mystyle1>EPM</option>";}
    if ($sec_utility==1||$sec_export==1){echo "</optgroup>";}

    echo "</select></FORM></td><td colspan=2 bgcolor=#CCCCFF width=60%><table border=0 width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr><td bgcolor=#E8E7E7><center><a href='bulletinboard.php?cid=$companyid&bid=$businessid' target='_blank' style=$style><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'><b>$bbdayname $bbmonth $bbday, $year</b></font></a></center></td></tr><tr bgcolor=white><td><iframe src='bulletinboard2.php?cid=$companyid&bid=$businessid' WIDTH=100% HEIGHT=100 frameborder=0 id='bboard'></iframe></td></tr></table>";
    echo "</td></tr></table><p>";

if($oo4!=1){
    echo "<center><table id=\"mainTable\" width=90% cellspacing=0 cellpadding=0>";

    //unit mgr
    if ($security_level==1&&$businessid!=0){

       $startday=$today;
       $weekday=date('l');
       $thurs1=0;
       $thurs2=0;
       for ($counter=1;$counter<=14;$counter++)
       {
          $startday=prevday($startday);
          if ($weekday=="Sunday"){$weekday="Saturday";}
          elseif ($weekday=="Monday"){$weekday="Sunday";}
          elseif ($weekday=="Tuesday"){$weekday="Monday";}
          elseif ($weekday=="Wednesday"){$weekday="Tuesday";}
          elseif ($weekday=="Thursday"){$weekday="Wednesday";}
          elseif ($weekday=="Friday"){$weekday="Thursday";}
          elseif ($weekday=="Saturday"){$weekday="Friday";}
       }
       $weekday2=$weekday;
       $startday2=$startday;
       
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM business WHERE companyid = '$companyid' AND businessid = '$businessid'";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();

       echo "<tr bgcolor=#E8E7E7><td><b><i>Name</td><td><b><i>Unit#</td><td align=right colspan=2><table><tr><td>Exported</td><td width=15 height=15 bgcolor=#444455></td><td>Submitted</td><td height=15 width=15 bgcolor=#00FF00></td><td>Pending</td><td height=15 width=15 bgcolor=#00CCFF></td><td>Closed</td><td height=15 width=15 bgcolor=red><font color=white><center>C</center></font></td></tr></table></td></tr>";
       echo "<tr bgcolor=black><td height=1 colspan=4></td></tr>"; 
       echo "<tr bgcolor=#CCCCCC><td height=1 colspan=4></td></tr>";

       $buscount=0;
       $subcount1=0;
       $subcount2=0;
       $num--;
       $nextdistrictid=0;
       while ($num>=0){

          $businessname=Treat_DB_ProxyOld::mysql_result($result,$num,"businessname");
          $businessid=Treat_DB_ProxyOld::mysql_result($result,$num,"businessid");
          $street=Treat_DB_ProxyOld::mysql_result($result,$num,"street");
          $city=Treat_DB_ProxyOld::mysql_result($result,$num,"city");
          $state=Treat_DB_ProxyOld::mysql_result($result,$num,"state");
          $zip=Treat_DB_ProxyOld::mysql_result($result,$num,"zip");
          $phone=Treat_DB_ProxyOld::mysql_result($result,$num,"phone");
          $unit=Treat_DB_ProxyOld::mysql_result($result,$num,"unit");
          $districtid=Treat_DB_ProxyOld::mysql_result($result,$num,"districtid");
          $closed1=Treat_DB_ProxyOld::mysql_result($result,$num,"closed");
          $closed1_reason=Treat_DB_ProxyOld::mysql_result($result,$num,"closed_reason");

          if ($closed1==1){$showclosed1="<img src=offline.gif height=16 width=16 alt='$closed1_reason' border=0>";}
          else {$showclosed1="";}

          $graph="<table border=0 cellspacing=0 cellpadding=0><tr>";

          $startday=$startday2;
          $weekday=$weekday2;
          while ($startday<=$today){
             $exported=0;
 
             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query2 = "SELECT * FROM submit WHERE businessid = '$businessid' AND date = '$startday'";
             $result2 = Treat_DB_ProxyOld::query($query2);
             $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
             //mysql_close();

             if ($num2!=0){$exported=mysql_result($result2,0,"export");}

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query3 = "SELECT closed FROM stats WHERE businessid = '$businessid' AND date = '$startday'";
             $result3 = Treat_DB_ProxyOld::query($query3);
             $num3=Treat_DB_ProxyOld::mysql_numrows($result3);
             //mysql_close();

             if ($num3!=0){$closed=Treat_DB_ProxyOld::mysql_result($result3,0,"closed");}

             if ($closed==1){
                $showthurs="<a title = 'Closed' href=reason.php?bid=$businessid&cid=$companyid&date=$startday target = '_blank' style=$style><center><font color=white size=2>C</font></center></a>";
             }
             else{
                if ($weekday=="Thursday"&& $num2!=0 && $exported==0){$showthurs="<font size=2><center>T</center></font>";}
                elseif ($weekday=="Thursday"&& $num2!=0 && $exported==1){$showthurs="<font size=2 color=white><center>T</center></font>";}
                elseif ($weekday=="Thursday"&&$num2==0){$showthurs="<font size=2 color=black><center>T</center></font>";}
                else {$showthurs="";}
             }

             if ($closed==1){
                $graph="$graph <td height=15 width=15 bgcolor=red>$showthurs</td><td width=1></td>";
             }
             else{
                if ($num2!=0 && $exported==1){$graph="$graph <td height=15 width=15 bgcolor=#444455>$showthurs</td><td width=1></td>";}
                elseif ($num2!=0){$graph="$graph <td height=15 width=15 bgcolor=#00FF00>$showthurs</td><td width=1></td>";}
                elseif ($closed1==1){$graph="$graph <td height=15 width=15 bgcolor=#AAAAAA>$showthurs</td><td width=1></td>";}
                else {$graph = "$graph <td height=15 width=15 bgcolor=#00CCFF>$showthurs</td><td width=1></td>";}
             } 

             $startday=nextday($startday);
             if ($weekday=="Sunday"){$weekday="Monday";}
             elseif ($weekday=="Saturday"){$weekday="Sunday";}
             elseif ($weekday=="Friday"){$weekday="Saturday";}
             elseif ($weekday=="Thursday"){$weekday="Friday";}
             elseif ($weekday=="Wednesday"){$weekday="Thursday";}
             elseif ($weekday=="Tuesday"){$weekday="Wednesday";}
             elseif ($weekday=="Monday"){$weekday="Tuesday";}

             $closed=0;
          }           

          $graph="$graph </tr></table>";   

          //if($order_only==1){$goto="buscatertrack.php";}
          //else{$goto="busdetail.php";}

          if ($sec_bus_sales>0){$goto="busdetail.php";}
          elseif ($sec_bus_invoice>0){$goto="businvoice.php";}
          elseif ($sec_bus_order>0){$goto="buscatertrack.php";}
          elseif ($sec_bus_payable>0){$goto="busapinvoice.php";}
          elseif ($sec_bus_inventor1>0){$goto="businventor.php";}
          elseif ($sec_bus_control>0){$goto="buscontrol.php";}
          elseif ($sec_bus_order_setup>0){$goto="buscaterroom.php";}
          elseif ($sec_bus_request>0){$goto="busrequest.php";}
          elseif ($sec_route_collections>0){$goto="busroute_collect.php";}
          elseif ($sec_route_labor>0){$goto="busroute_labor.php";}
          elseif ($sec_route_detail>0){$goto="busroute_detail.php";}
          elseif ($sec_bus_labor>0){$goto="buslabor.php";}
          elseif ($sec_bus_timeclock>0){$goto="buslabor5.php";}
          else {$goto="businesstrack.php";}

          echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor='$back'><td><a style=$style href=$goto?bid=$businessid&cid=$companyid>$showclosed1 <font color=blue>$businessname</font></a></td><td onmouseover=\"quickMenu(this, '$companyid', '$businessid')\" onmouseout=\"clearMenu(this)\">$unit</td><td align=right>$graph</td><td align=right></td></tr>";
          echo "<tr bgcolor=#CCCCCC><td height=1 colspan=4></td></tr>";  
          $buscount++;
          $num--;
          
       }
     
       echo "<tr bgcolor=black><td height=1 colspan=4></td></tr>";
       echo "<tr bgcolor=#E8E7E7><td>Results: $buscount</td><td></td><td align=right></td><td align=right></td></tr>";
       //echo "<tr bgcolor=black><td height=1 colspan=4></td></tr>";

    }

    //Unit Manger,Multipe Units
    else if ($security_level==2){

       $startday=$today;
       $weekday=date('l');
       $thurs1=0;
       $thurs2=0;
       for ($counter=1;$counter<=14;$counter++)
       {
          $startday=prevday($startday);
          if ($weekday=="Sunday"){$weekday="Saturday";}
          elseif ($weekday=="Monday"){$weekday="Sunday";}
          elseif ($weekday=="Tuesday"){$weekday="Monday";}
          elseif ($weekday=="Wednesday"){$weekday="Tuesday";}
          elseif ($weekday=="Thursday"){$weekday="Wednesday";}
          elseif ($weekday=="Friday"){$weekday="Thursday";}
          elseif ($weekday=="Saturday"){$weekday="Friday";}
       }
       $weekday2=$weekday;
       $startday2=$startday;

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();

       $busid1=Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
       $busid2=Treat_DB_ProxyOld::mysql_result($result,0,"busid2");
       $busid3=Treat_DB_ProxyOld::mysql_result($result,0,"busid3");
       $busid4=Treat_DB_ProxyOld::mysql_result($result,0,"busid4");
       $busid5=Treat_DB_ProxyOld::mysql_result($result,0,"busid5");
       $busid6=Treat_DB_ProxyOld::mysql_result($result,0,"busid6");
       $busid7=Treat_DB_ProxyOld::mysql_result($result,0,"busid7");
       $busid8=Treat_DB_ProxyOld::mysql_result($result,0,"busid8");
       $busid9=Treat_DB_ProxyOld::mysql_result($result,0,"busid9");
       $busid10=Treat_DB_ProxyOld::mysql_result($result,0,"busid10");
       
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM business WHERE companyid = '$companyid' AND (businessid = '$busid1' OR businessid = '$busid2' OR businessid = '$busid3' OR businessid = '$busid4' OR businessid = '$busid5' OR businessid = '$busid6' OR businessid = '$busid7' OR businessid = '$busid8' OR businessid = '$busid9' OR businessid = '$busid10')ORDER BY districtid DESC, businessname DESC";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();

       echo "<tr bgcolor=#E8E7E7><td><b><i>Name</td><td><b><i>Unit#</td><td align=right colspan=2><table><tr><td>Exported</td><td width=15 height=15 bgcolor=#444455></td><td>Submitted</td><td height=15 width=15 bgcolor=#00FF00></td><td>Pending</td><td height=15 width=15 bgcolor=#00CCFF></td><td>Closed</td><td height=15 width=15 bgcolor=red><font color=white><center>C</center></font></td></tr></table></td></tr>";
       echo "<tr bgcolor=black><td height=1 colspan=4></td></tr>"; 
       echo "<tr bgcolor=#CCCCCC><td height=1 colspan=4></td></tr>";

       $buscount=0;
       $subcount1=0;
       $subcount2=0;
       $num--;
       $nextdistrictid=0;
       while ($num>=0){

          $businessname=Treat_DB_ProxyOld::mysql_result($result,$num,"businessname");
          $businessid=Treat_DB_ProxyOld::mysql_result($result,$num,"businessid");
          $street=Treat_DB_ProxyOld::mysql_result($result,$num,"street");
          $city=Treat_DB_ProxyOld::mysql_result($result,$num,"city");
          $state=Treat_DB_ProxyOld::mysql_result($result,$num,"state");
          $zip=Treat_DB_ProxyOld::mysql_result($result,$num,"zip");
          $phone=Treat_DB_ProxyOld::mysql_result($result,$num,"phone");
          $unit=Treat_DB_ProxyOld::mysql_result($result,$num,"unit");
          $districtid=Treat_DB_ProxyOld::mysql_result($result,$num,"districtid");
          $closed1=Treat_DB_ProxyOld::mysql_result($result,$num,"closed");
          $closed1_reason=Treat_DB_ProxyOld::mysql_result($result,$num,"closed_reason");

          if ($closed1==1){$showclosed1="<img src=offline.gif height=16 width=16 alt='$closed1_reason' border=0>";}
          else {$showclosed1="";}

          $graph="<table border=0 cellspacing=0 cellpadding=0><tr>";

          $startday=$startday2;
          $weekday=$weekday2;
          while ($startday<=$today){
             $exported=0;
 
             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query2 = "SELECT * FROM submit WHERE businessid = '$businessid' AND date = '$startday'";
             $result2 = Treat_DB_ProxyOld::query($query2);
             $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
             //mysql_close();

             if ($num2!=0){$exported=mysql_result($result2,0,"export");}

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query3 = "SELECT closed FROM stats WHERE businessid = '$businessid' AND date = '$startday'";
             $result3 = Treat_DB_ProxyOld::query($query3);
             $num3=Treat_DB_ProxyOld::mysql_numrows($result3);
             //mysql_close();

             if ($num3!=0){$closed=Treat_DB_ProxyOld::mysql_result($result3,0,"closed");}

             if ($closed==1){
                $showthurs="<a title = 'Closed' href=reason.php?bid=$businessid&cid=$companyid&date=$startday target = '_blank' style=$style><center><font color=white size=2>C</font></center></a>";
             }
             else{
                if ($weekday=="Thursday"&& $num2!=0 && $exported==0){$showthurs="<font size=2><center>T</center></font>";}
                elseif ($weekday=="Thursday"&& $num2!=0 && $exported==1){$showthurs="<font size=2 color=white><center>T</center></font>";}
                elseif ($weekday=="Thursday"&&$num2==0){$showthurs="<font size=2 color=black><center>T</center></font>";}
                else {$showthurs="";}
             }

             if ($closed==1){
                $graph="$graph <td height=15 width=15 bgcolor=red>$showthurs</td><td width=1></td>";
             }
             else{
                if ($num2!=0 && $exported==1){$graph="$graph <td height=15 width=15 bgcolor=#444455>$showthurs</td><td width=1></td>";}
                elseif ($num2!=0){$graph="$graph <td height=15 width=15 bgcolor=#00FF00>$showthurs</td><td width=1></td>";}
                elseif ($closed1==1){$graph="$graph <td height=15 width=15 bgcolor=#AAAAAA>$showthurs</td><td width=1></td>";}
                else {$graph = "$graph <td height=15 width=15 bgcolor=#00CCFF>$showthurs</td><td width=1></td>";}
             } 

             $startday=nextday($startday);
             if ($weekday=="Sunday"){$weekday="Monday";}
             elseif ($weekday=="Saturday"){$weekday="Sunday";}
             elseif ($weekday=="Friday"){$weekday="Saturday";}
             elseif ($weekday=="Thursday"){$weekday="Friday";}
             elseif ($weekday=="Wednesday"){$weekday="Thursday";}
             elseif ($weekday=="Tuesday"){$weekday="Wednesday";}
             elseif ($weekday=="Monday"){$weekday="Tuesday";}

             $closed=0;
          }           

          $graph="$graph </tr></table>";        

          if ($nextdistrictid!=$districtid){
             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query88 = "SELECT districtname FROM district WHERE districtid = '$districtid'";
             $result88 = Treat_DB_ProxyOld::query($query88);
             //mysql_close();
             $nextdistrictid=$districtid;
             $districtname=Treat_DB_ProxyOld::mysql_result($result88,0,"districtname");
             echo "<tr><td colspan=4 style='filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr=white, startColorstr=#CCCCFF, gradientType=0);'><b><i>$districtname</i></b></td></tr>";
             echo "<tr bgcolor=#CCCCCC><td height=1 colspan=4></td></tr>"; 
          }
          else{
             //if($order_only==1&&$busid1==$businessid){$goto="buscatertrack.php";}
             //elseif($oo2==1&&$busid2==$businessid){$goto="buscatertrack.php";}
             //elseif($oo3==1&&$busid3==$businessid){$goto="buscatertrack.php";}
             //elseif($oo4==1&&$busid4==$businessid){$goto="buscatertrack.php";}
             //elseif($oo5==1&&$busid5==$businessid){$goto="buscatertrack.php";}
             //elseif($oo6==1&&$busid6==$businessid){$goto="buscatertrack.php";}
             //elseif($oo7==1&&$busid7==$businessid){$goto="buscatertrack.php";}
             //elseif($oo8==1&&$busid8==$businessid){$goto="buscatertrack.php";}
             //elseif($oo9==1&&$busid9==$businessid){$goto="buscatertrack.php";}
             //elseif($oo10==1&&$busid10==$businessid){$goto="buscatertrack.php";}
             //else{$goto="busdetail.php";}

             if ($sec_bus_sales>0){$goto="busdetail.php";}
             elseif ($sec_bus_invoice>0){$goto="businvoice.php";}
             elseif ($sec_bus_order>0){$goto="buscatertrack.php";}
             elseif ($sec_bus_payable>0){$goto="busapinvoice.php";}
             elseif ($sec_bus_inventor1>0){$goto="businventor.php";}
             elseif ($sec_bus_control>0){$goto="buscontrol.php";}
             elseif ($sec_bus_order_setup>0){$goto="buscaterroom.php";}
             elseif ($sec_bus_request>0){$goto="busrequest.php";}
             elseif ($sec_route_collections>0){$goto="busroute_collect.php";}
             elseif ($sec_route_labor>0){$goto="busroute_labor.php";}
             elseif ($sec_route_detail>0){$goto="busroute_detail.php";}
             elseif ($sec_bus_labor>0){$goto="buslabor.php";}
             elseif ($sec_bus_timeclock>0){$goto="buslabor5.php";}
             else {$goto="businesstrack.php";}

             echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor='$back'><td><a style=$style href=$goto?bid=$businessid&cid=$companyid>$showclosed1 <font color=blue>$businessname</font></a></td><td>$unit</td><td align=right>$graph</td><td align=right></td></tr>";
             echo "<tr bgcolor=#CCCCCC><td height=1 colspan=4></td></tr>";  
             $buscount++;
             $num--;
          }
       }
     
       echo "<tr bgcolor=black><td height=1 colspan=4></td></tr>";
       echo "<tr bgcolor=#E8E7E7><td>Results: $buscount</td><td></td><td align=right></td><td align=right></td></tr>";
       //echo "<tr bgcolor=black><td height=1 colspan=4></td></tr>";

    }

///////////////////////////////District Manager's
    else if ($security_level<7&&$security_level>2){

       $startday=$today;
       $weekday=date('l');
       $thurs1=0;
       $thurs2=0;
       for ($counter=1;$counter<=14;$counter++)
       {
          $startday=prevday($startday);
          if ($weekday=="Sunday"){$weekday="Saturday";}
          elseif ($weekday=="Monday"){$weekday="Sunday";}
          elseif ($weekday=="Tuesday"){$weekday="Monday";}
          elseif ($weekday=="Wednesday"){$weekday="Tuesday";}
          elseif ($weekday=="Thursday"){$weekday="Wednesday";}
          elseif ($weekday=="Friday"){$weekday="Thursday";}
          elseif ($weekday=="Saturday"){$weekday="Friday";}
       }
       $weekday2=$weekday;
       $startday2=$startday;
       
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query34 = "SELECT * FROM login_district WHERE loginid = '$loginid'";
       $result34 = Treat_DB_ProxyOld::query($query34);
       $num34=Treat_DB_ProxyOld::mysql_numrows($result34);
       //mysql_close();

       echo "<tr bgcolor=#E8E7E7><td><b><i>Name</td><td><b><i>Unit#</td><td align=right colspan=2><table><tr><td>Exported</td><td width=15 height=15 bgcolor=#444455></td><td>Submitted</td><td height=15 width=15 bgcolor=#00FF00></td><td>Pending</td><td height=15 width=15 bgcolor=#00CCFF></td><td>Closed</td><td height=15 width=15 bgcolor=red><font color=white><center>C</center></font></td></tr></table></td></tr>";
       echo "<tr bgcolor=black><td height=1 colspan=4></td></tr>"; 
       echo "<tr bgcolor=#CCCCCC><td height=1 colspan=4></td></tr>";

       $buscount=0;
       $num34--;
       while($num34>=0){
//////////////////
       $dist_id=Treat_DB_ProxyOld::mysql_result($result34,$num34,"districtid");

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM business WHERE companyid = '$companyid' AND districtid = '$dist_id' ORDER BY businessname DESC";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();

       $subcount1=0;
       $subcount2=0;
       $num--;
       $nextdistrictid=0;
       while ($num>=0){

          $businessname=Treat_DB_ProxyOld::mysql_result($result,$num,"businessname");
          $businessid=Treat_DB_ProxyOld::mysql_result($result,$num,"businessid");
          $street=Treat_DB_ProxyOld::mysql_result($result,$num,"street");
          $city=Treat_DB_ProxyOld::mysql_result($result,$num,"city");
          $state=Treat_DB_ProxyOld::mysql_result($result,$num,"state");
          $zip=Treat_DB_ProxyOld::mysql_result($result,$num,"zip");
          $phone=Treat_DB_ProxyOld::mysql_result($result,$num,"phone");
          $unit=Treat_DB_ProxyOld::mysql_result($result,$num,"unit");
          $districtid=Treat_DB_ProxyOld::mysql_result($result,$num,"districtid");
          $closed1=Treat_DB_ProxyOld::mysql_result($result,$num,"closed");
          $closed1_reason=Treat_DB_ProxyOld::mysql_result($result,$num,"closed_reason");

          if ($closed1==1){$showclosed1="<img src=offline.gif height=16 width=16 alt='$closed1_reason' border=0 align=bottom>";}
          else {$showclosed1="";}

          $graph="<table border=0 cellspacing=0 cellpadding=0><tr>";

          $startday=$startday2;
          $weekday=$weekday2;
          while ($startday<=$today){
             $exported=0;
 
             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query2 = "SELECT * FROM submit WHERE businessid = '$businessid' AND date = '$startday'";
             $result2 = Treat_DB_ProxyOld::query($query2);
             $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
             //mysql_close();

             if ($num2!=0){$exported=Treat_DB_ProxyOld::mysql_result($result2,0,"export");}

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query3 = "SELECT closed FROM stats WHERE businessid = '$businessid' AND date = '$startday'";
             $result3 = Treat_DB_ProxyOld::query($query3);
             $num3=Treat_DB_ProxyOld::mysql_numrows($result3);
             //mysql_close();

             if ($num3!=0){$closed=Treat_DB_ProxyOld::mysql_result($result3,0,"closed");}

             if ($closed==1){
                $showthurs="<a title = 'Closed' href=reason.php?bid=$businessid&cid=$companyid&date=$startday target = '_blank' style=$style><center><font color=white size=2>C</font></center></a>";
             }
             else{
                if ($weekday=="Thursday"&& $num2!=0 && $exported==0){$showthurs="<font size=2><center>T</center></font>";}
                elseif ($weekday=="Thursday"&& $num2!=0 && $exported==1){$showthurs="<font size=2 color=white><center>T</center></font>";}
                elseif ($weekday=="Thursday"&&$num2==0){$showthurs="<font size=2 color=black><center>T</center></font>";}
                else {$showthurs="";}
             }

             if ($closed==1){
                $graph="$graph <td height=15 width=15 bgcolor=red>$showthurs</td><td width=1></td>";
             }
             else{
                if ($num2!=0 && $exported==1){$graph="$graph <td height=15 width=15 bgcolor=#444455>$showthurs</td><td width=1></td>";}
                elseif ($num2!=0){$graph="$graph <td height=15 width=15 bgcolor=#00FF00>$showthurs</td><td width=1></td>";}
                elseif ($closed1==1){$graph="$graph <td height=15 width=15 bgcolor=#AAAAAA>$showthurs</td><td width=1></td>";}
                else {$graph = "$graph <td height=15 width=15 bgcolor=#00CCFF>$showthurs</td><td width=1></td>";}
             } 

             $startday=nextday($startday);
             if ($weekday=="Sunday"){$weekday="Monday";}
             elseif ($weekday=="Saturday"){$weekday="Sunday";}
             elseif ($weekday=="Friday"){$weekday="Saturday";}
             elseif ($weekday=="Thursday"){$weekday="Friday";}
             elseif ($weekday=="Wednesday"){$weekday="Thursday";}
             elseif ($weekday=="Tuesday"){$weekday="Wednesday";}
             elseif ($weekday=="Monday"){$weekday="Tuesday";}

             $closed=0;
          }           

          $graph="$graph </tr></table>";        

          if ($nextdistrictid!=$districtid){
             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query88 = "SELECT districtname FROM district WHERE districtid = '$districtid'";
             $result88 = Treat_DB_ProxyOld::query($query88);
             //mysql_close();
             $nextdistrictid=$districtid;
             $districtname=Treat_DB_ProxyOld::mysql_result($result88,0,"districtname");
             echo "<tr><td colspan=4 style='filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr=white, startColorstr=#CCCCFF, gradientType=0);'><b><i>$districtname</i></b></td></tr>";
             echo "<tr bgcolor=#CCCCCC><td height=1 colspan=4></td></tr>"; 
          }
          else{
             if ($sec_bus_sales>0){$goto="busdetail.php";}
             elseif ($sec_bus_invoice>0){$goto="businvoice.php";}
             elseif ($sec_bus_order>0){$goto="buscatertrack.php";}
             elseif ($sec_bus_payable>0){$goto="busapinvoice.php";}
             elseif ($sec_bus_inventor1>0){$goto="businventor.php";}
             elseif ($sec_bus_control>0){$goto="buscontrol.php";}
             elseif ($sec_bus_order_setup>0){$goto="buscaterroom.php";}
             elseif ($sec_bus_request>0){$goto="busrequest.php";}
             elseif ($sec_route_collections>0){$goto="busroute_collect.php";}
             elseif ($sec_route_labor>0){$goto="busroute_labor.php";}
             elseif ($sec_route_detail>0){$goto="busroute_detail.php";}
             elseif ($sec_bus_labor>0){$goto="buslabor.php";}
             elseif ($sec_bus_timeclock>0){$goto="buslabor5.php";}
             else {$goto="businesstrack.php";}

             echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor='$back'><td><a style=$style href=$goto?bid=$businessid&cid=$companyid>$showclosed1 <font color=blue>$businessname</font></a></td><td>$unit</td><td align=right>$graph</td><td align=right></td></tr>";
             echo "<tr bgcolor=#CCCCCC><td height=1 colspan=4></td></tr>";  
             $buscount++;
             $num--;
          }
       }
//////////////////
       $num34--;
       }

       echo "<tr bgcolor=black><td height=1 colspan=4></td></tr>";
       echo "<tr bgcolor=#E8E7E7><td>Results: $buscount</td><td></td><td align=right></td><td align=right></td></tr>";
       //echo "<tr bgcolor=black><td height=1 colspan=4></td></tr>";

    }

//////Admin
    else if ($security_level>6){

       $startday=$today;
       $weekday=date('l');
       $thurs1=0;
       $thurs2=0;
       for ($counter=1;$counter<=14;$counter++)
       {
          $startday=prevday($startday);
          if ($weekday=="Sunday"){$weekday="Saturday";}
          elseif ($weekday=="Monday"){$weekday="Sunday";}
          elseif ($weekday=="Tuesday"){$weekday="Monday";}
          elseif ($weekday=="Wednesday"){$weekday="Tuesday";}
          elseif ($weekday=="Thursday"){$weekday="Wednesday";}
          elseif ($weekday=="Friday"){$weekday="Thursday";}
          elseif ($weekday=="Saturday"){$weekday="Friday";}
       }
       $weekday2=$weekday;
       $startday2=$startday;
       
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM business WHERE companyid = '$companyid' ORDER BY districtid DESC, businessname DESC";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();

       echo "<tr bgcolor=#E8E7E7><td><b><i>Name</td><td><b><i>Unit#</td><td align=right colspan=2><table><tr><td>Exported</td><td width=15 height=15 bgcolor=#444455></td><td>Submitted</td><td height=15 width=15 bgcolor=#00FF00></td><td>Pending</td><td height=15 width=15 bgcolor=#00CCFF></td><td>Closed</td><td height=15 width=15 bgcolor=red><font color=white><center>C</center></font></td></tr></table></td></tr>";
       echo "<tr bgcolor=black><td height=1 colspan=4></td></tr>"; 
       echo "<tr bgcolor=#CCCCCC><td height=1 colspan=4></td></tr>";

       $buscount=0;
       $subcount1=0;
       $subcount2=0;
       $num--;
       $nextdistrictid=0;
       while ($num>=0){

          $businessname=Treat_DB_ProxyOld::mysql_result($result,$num,"businessname");
          $businessid=Treat_DB_ProxyOld::mysql_result($result,$num,"businessid");
          $street=Treat_DB_ProxyOld::mysql_result($result,$num,"street");
          $city=Treat_DB_ProxyOld::mysql_result($result,$num,"city");
          $state=Treat_DB_ProxyOld::mysql_result($result,$num,"state");
          $zip=Treat_DB_ProxyOld::mysql_result($result,$num,"zip");
          $phone=Treat_DB_ProxyOld::mysql_result($result,$num,"phone");
          $unit=Treat_DB_ProxyOld::mysql_result($result,$num,"unit");
          $districtid=Treat_DB_ProxyOld::mysql_result($result,$num,"districtid");
          $closed1=Treat_DB_ProxyOld::mysql_result($result,$num,"closed");
          $closed1_reason=Treat_DB_ProxyOld::mysql_result($result,$num,"closed_reason");

          if ($closed1==1){$showclosed1="<img src=offline.gif height=16 width=16 alt='$closed1_reason' border=0>";}
          else {$showclosed1="";}

          $graph="<table border=0 cellspacing=0 cellpadding=0><tr>";

          $back="white";
          $showerror="";
          $laststatus=-1;

          $startday=$startday2;
          $weekday=$weekday2;
          while ($startday<=$today){
             $exported=0;
 
             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query2 = "SELECT * FROM submit WHERE businessid = '$businessid' AND date = '$startday'";
             $result2 = Treat_DB_ProxyOld::query($query2);
             $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
             //mysql_close();

             if ($num2!=0){$exported=Treat_DB_ProxyOld::mysql_result($result2,0,"export");}

             if ($num2>$laststatus&&$laststatus!=-1){$showerror="<img src=error.gif height=16 width=19 border=0 alt='Attention: Unsubmitted Days'>";$back="#FFFF99";}

             $laststatus=$num2;
 
             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query3 = "SELECT closed FROM stats WHERE businessid = '$businessid' AND date = '$startday'";
             $result3 = Treat_DB_ProxyOld::query($query3);
             $num3=Treat_DB_ProxyOld::mysql_numrows($result3);
             //mysql_close();

             if ($num3!=0){$closed=Treat_DB_ProxyOld::mysql_result($result3,0,"closed");}

             if ($closed==1){
                $showthurs="<a title = 'Closed' href=reason.php?bid=$businessid&cid=$companyid&date=$startday target = '_blank' style=$style><center><font color=white size=2>C</font></center></a>";
             }
             else{
                if ($weekday=="Thursday"&& $num2!=0 && $exported==0){$showthurs="<font size=2><center>T</center></font>";}
                elseif ($weekday=="Thursday"&& $num2!=0 && $exported==1){$showthurs="<font size=2 color=white><center>T</center></font>";}
                elseif ($weekday=="Thursday"&&$num2==0){$showthurs="<font size=2 color=black><center>T</center></font>";}
                else {$showthurs="";}
             }

             if ($closed==1){
                $graph="$graph <td height=15 width=15 bgcolor=red>$showthurs</td><td width=1></td>";
             }
             else{
                if ($num2!=0 && $exported==1){$graph="$graph <td height=15 width=15 bgcolor=#444455>$showthurs</td><td width=1></td>";}
                elseif ($num2!=0){$graph="$graph <td height=15 width=15 bgcolor=#00FF00>$showthurs</td><td width=1></td>";}
                elseif ($closed1==1){$graph="$graph <td height=15 width=15 bgcolor=#AAAAAA>$showthurs</td><td width=1></td>";}
                else {$graph = "$graph <td height=15 width=15 bgcolor=#00CCFF>$showthurs</td><td width=1></td>";}
             } 

             $startday=nextday($startday);
             if ($weekday=="Sunday"){$weekday="Monday";}
             elseif ($weekday=="Saturday"){$weekday="Sunday";}
             elseif ($weekday=="Friday"){$weekday="Saturday";}
             elseif ($weekday=="Thursday"){$weekday="Friday";}
             elseif ($weekday=="Wednesday"){$weekday="Thursday";}
             elseif ($weekday=="Tuesday"){$weekday="Wednesday";}
             elseif ($weekday=="Monday"){$weekday="Tuesday";}

             $closed=0;
          }           

          $graph="$graph </tr></table>";        

          if ($nextdistrictid!=$districtid){
             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query88 = "SELECT districtname FROM district WHERE districtid = '$districtid'";
             $result88 = Treat_DB_ProxyOld::query($query88);
             //mysql_close();
             $nextdistrictid=$districtid;
             $districtname=Treat_DB_ProxyOld::mysql_result($result88,0,"districtname");
             echo "<tr><td colspan=4 style='filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr=white, startColorstr=#CCCCFF, gradientType=0);'><b><i>$districtname</i></b></td></tr>";
             echo "<tr bgcolor=#CCCCCC><td height=1 colspan=4></td></tr>"; 
          }
          else{
             if ($sec_bus_sales>0){$goto="busdetail.php";}
             elseif ($sec_bus_invoice>0){$goto="businvoice.php";}
             elseif ($sec_bus_order>0){$goto="buscatertrack.php";}
             elseif ($sec_bus_payable>0){$goto="busapinvoice.php";}
             elseif ($sec_bus_inventor1>0){$goto="businventor.php";}
             elseif ($sec_bus_control>0){$goto="buscontrol.php";}
             elseif ($sec_bus_order_setup>0){$goto="buscaterroom.php";}
             elseif ($sec_bus_request>0){$goto="busrequest.php";}
             elseif ($sec_route_collections>0){$goto="busroute_collect.php";}
             elseif ($sec_route_labor>0){$goto="busroute_labor.php";}
             elseif ($sec_route_detail>0){$goto="busroute_detail.php";}
             elseif ($sec_bus_labor>0){$goto="buslabor.php";}
             elseif ($sec_bus_timeclock>0){$goto="buslabor5.php";}
             else {$goto="businesstrack.php";}

             echo "<tr bgcolor='$back' onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor='$back'><td><a style=$style href=$goto?bid=$businessid&cid=$companyid>$showerror $showclosed1 <font color=blue>$businessname</font></a></td><td onmouseover=\"quickMenu(this, '$companyid', '$businessid');\" onmouseout=\"clearMenu(this)\">$unit</td><td align=right>$graph</td><td align=right></td></tr>";
             echo "<tr bgcolor=#CCCCCC><td height=1 colspan=4></td></tr>";  
             $buscount++;
             $num--;
          }
       }
     
       echo "<tr bgcolor=black><td height=1 colspan=4></td></tr>";
       echo "<tr bgcolor=#E8E7E7><td>Results: $buscount</td><td></td><td align=right></td><td align=right></td></tr>";
       //echo "<tr bgcolor=black><td height=1 colspan=4></td></tr>";

    }

    echo "</table></center><p><br>";

    echo "<center><table width=90%><tr>";
    $date1=$day1;
    $date2=$today;

    $tablecount=0; 

    if ($mylinks==1){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM mylinks WHERE loginid = '$loginid' ORDER BY name DESC";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();

       echo "<td width=33% valign=top><center><table width=95% cellspacing=0 cellpadding=0>";
       echo "<tr height=2><td colspan=3 bgcolor=#CCCCFF></td></tr>";
       echo "<tr><td width=2 bgcolor=#CCCCFF></td><td bgcolor=#EFEFFF><a href=editpass.php?edit=2#mystuff style=$style title='Edit My Links'><font size=4 color=blue><u><b>My Links</a></td><td width=2 bgcolor=#CCCCFF></td></tr>";
       echo "<tr height=2><td colspan=3 bgcolor=#CCCCFF></td></tr>";
       $num--;
       while ($num>=0){
          $name=Treat_DB_ProxyOld::mysql_result($result,$num,"name");
          $myurl=Treat_DB_ProxyOld::mysql_result($result,$num,"url");
          echo "<tr><td width=2 bgcolor=#CCCCFF></td><td><a href='$myurl' style='$style' target='_blank'><font color=blue>$name</font></a></td><td width=2 bgcolor=#CCCCFF></td></tr>";
          $num--;
       }
       echo "<tr height=2><td colspan=3 bgcolor=#CCCCFF></td></tr></table></center></td>";

       $tablecount++;
    }

    if ($myap==1){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM vendors WHERE businessid = '$buscook' ORDER BY name DESC";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();

       echo "<td width=33% valign=top><center><table width=95% cellspacing=0 cellpadding=0>";
       echo "<tr height=2><td colspan=4 bgcolor=#CCCCFF></td></tr>";
       echo "<tr><td width=2 bgcolor=#CCCCFF></td><td bgcolor=#EFEFFF colspan=2><a href=busapinvoice.php?bid=$buscook&cid=$companyid style=$style title='Payables'><font size=4 color=blue><u><b>Payables</td><td width=2 bgcolor=#CCCCFF></td></tr>";
       echo "<tr height=2><td colspan=4 bgcolor=#CCCCFF></td></tr>";
       $num--;
       $totalinv=0;
       while ($num>=0){
          $name=Treat_DB_ProxyOld::mysql_result($result,$num,"name");
          $vendorid=Treat_DB_ProxyOld::mysql_result($result,$num,"vendorid");
          $aptotal=0;
 
          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query2 = "SELECT * FROM apinvoice WHERE businessid = '$buscook' && date >= '$date1' && date <= '$date2' && vendor = '$vendorid'";
          $result2 = Treat_DB_ProxyOld::query($query2);
          $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
          //mysql_close();

          $num2--;
          while ($num2>=0){
             $total=Treat_DB_ProxyOld::mysql_result($result2,$num2,"total");  
             $aptotal=$aptotal+$total;     
             $num2--;
          }

          $aptotal=money($aptotal);
          $totalinv=$totalinv+$aptotal;
          if ($aptotal!=0){echo "<tr><td width=2 bgcolor=#CCCCFF></td><td><font color=blue size=2>$name</font></td><td align=right><font color=blue size=2>$$aptotal</td><td width=2 bgcolor=#CCCCFF></td></tr>";}
          $num--;
       }
       $totalinv=money($totalinv);
       echo "<tr><td width=2 bgcolor=#CCCCFF></td><td><font color=blue size=2><b>Total</font></td><td align=right><font color=blue size=2><b>$$totalinv</td><td width=2 bgcolor=#CCCCFF></td></tr>";
       echo "<tr height=2><td colspan=4 bgcolor=#CCCCFF></td></tr></table></center></td>";

       $tablecount++;
    }

    if ($myar==1){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM invoice WHERE businessid = '$buscook' AND date >= '$date1' AND date <= '$date2' ORDER BY date, event_time DESC";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();

       echo "<td width=33% valign=top><center><table width=95% cellspacing=0 cellpadding=0 border=0>";
       echo "<tr height=2><td colspan=5 bgcolor=#CCCCFF></td></tr>";
       echo "<tr><td width=2 bgcolor=#CCCCFF></td><td bgcolor=#EFEFFF colspan=3><a href=businvoice.php?bid=$buscook&cid=$companyid style=$style title='Invoicing'><font size=4 color=blue><u><b>Invoicing</a></td><td width=2 bgcolor=#CCCCFF></td></tr>";
       echo "<tr height=2><td colspan=5 bgcolor=#CCCCFF></td></tr>";
       $num--;
       while ($num>=0){
          $invoiceid=Treat_DB_ProxyOld::mysql_result($result,$num,"invoiceid");
          $accountid=Treat_DB_ProxyOld::mysql_result($result,$num,"accountid");
          $event_time=Treat_DB_ProxyOld::mysql_result($result,$num,"event_time");
          $invdate=Treat_DB_ProxyOld::mysql_result($result,$num,"date");
          $invstatus=Treat_DB_ProxyOld::mysql_result($result,$num,"status");

          $inc_day=substr($invdate,8,2);
          $inc_month=substr($invdate,5,2);
          $invdate="$inc_month/$inc_day";

          if ($invstatus==1){$invcolor="green";}
          else{$invcolor="blue";}

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query2 = "SELECT name FROM accounts WHERE accountid = '$accountid'";
          $result2 = Treat_DB_ProxyOld::query($query2);
          $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
          //mysql_close();

          $acct_name=Treat_DB_ProxyOld::mysql_result($result2,0,"name");  

          echo "<tr><td width=2 bgcolor=#CCCCFF></td><td><font color=$invcolor size=2>$invdate</font></td><td><font color=$invcolor size=2>$acct_name</td><td><a href=saveinvoice.php?bid=$buscook&cid=$companyid&invoiceid=$invoiceid style=$style><font color=$invcolor size=2>$invoiceid</a></td><td width=2 bgcolor=#CCCCFF></td></tr>";
          $num--;
       }

       echo "<tr height=2><td colspan=5 bgcolor=#CCCCFF></td></tr></table></center></td>";

       $tablecount++;
    }

    if ($tablecount==0){echo "</tr></table></center>";}
    elseif ($tablecount==1){echo "<td width=33%></td><td width=33%></td></tr></table></center>";}
    elseif ($tablecount==2){echo "<td width=33%></td></tr></table></center>";}
    elseif ($tablecount==3){echo "</tr></table></center>";}

//////////MYGADGETS
    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM mygadgets WHERE loginid = '$loginid'";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    echo "<p><center><table width=90%>";

    $num--;
    $counter=0;
    while ($num>=0){
       if ($counter==0){$counter=1;}
       $mygadgetid=Treat_DB_ProxyOld::mysql_result($result,$num,"mygadgetid");
       $code=Treat_DB_ProxyOld::mysql_result($result,$num,"code");

       if ($counter==1){echo "<tr><td colspan=2><hr></td></tr><tr><td><center>$code</center></td>";$counter=2;}
       elseif($counter==2){echo "<td><center>$code</center></td></tr>";$counter=1;}

       $num--;
    }
    if ($counter==1){echo "<td></td></tr>";}

    echo "<tr><td colspan=2><hr></td></tr></table></center>";

    $query19="UPDATE login SET oo4 = '0' WHERE loginid = '$loginid'";
    $result19 = Treat_DB_ProxyOld::query($query19);
}
elseif($oo4==1){

    $thisyear=$_GET["thisyear"];
    $thismonth=$_GET["thismonth"];
    $showwkds=$_GET["showwkds"];

    echo "<p><center><table width=90% cellpadding=0 cellspacing=0 border=0>";

    ///////////////////////
    /////////////////FILTER
    ///////////////////////
    $viewbus=$_GET['viewbus'];
    $viewtype=$_GET['viewtype'];
    $viewsub=$_GET['viewsub'];
    $viewsales=$_GET['viewsales'];

    if($viewtype==""){
       $viewbus=$oo6;
       $viewtype=$oo5;
    }

    if($showwkds==""){
       $showwkds=$oo7;
    }

    $query12="SELECT * FROM login_all WHERE loginid = '$loginid'";
    $result12 = Treat_DB_ProxyOld::query($query12);
    $num12=Treat_DB_ProxyOld::mysql_numrows($result12);
    
    if($viewtype==1&&$security_level>=7){
       $compquery="business.companyid != '2'";

       echo "<tr bgcolor=#E8E7E7 style=\"border:2px solid #E8E7E7;\"><td colspan=1 style=\"border:2px solid #E8E7E7;\"><div style=\" width:150px; background:#FFFF99; border:1px solid #454545; display:inline;\"><center>Treat America</center></div>&nbsp;";

       $query58 = "SELECT * FROM company_type ORDER BY type_name DESC";
       $result58 = Treat_DB_ProxyOld::query($query58);
       $num58=Treat_DB_ProxyOld::mysql_numrows($result58);

       $num58--;
       while($num58>=0){
          $type_name=Treat_DB_ProxyOld::mysql_result($result58,$num58,"type_name");
          $typeid=Treat_DB_ProxyOld::mysql_result($result58,$num58,"typeid");

          echo "<div id='2.$typeid' style=\" width:150px; background:#CCCCCC; border:1px solid #454545; display:inline;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=2&viewbus=$typeid style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>$type_name</font></center></div>&nbsp;";

          $num58--;
       }
       echo "<div style=\" width:50px; background:#CCCCCC; border:1px solid #454545; display:inline;\"><a href='javascript:void();' onclick=\"Effect.SlideDown('navigation', { duration: 1.0 }); return false;\" style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Nav</font></center></a></div>";
       echo "</td>";
    }
    elseif($viewtype==2){
       $compquery="business.companyid = company.companyid AND company.company_type = '$viewbus'";
       $fromtables=",company";

       $query58 = "SELECT * FROM login_all WHERE loginid = '$loginid'";
       $result58 = Treat_DB_ProxyOld::query($query58);
       $can_goback=Treat_DB_ProxyOld::mysql_result($result58,0,"login_all");

       $query58 = "SELECT * FROM company_type WHERE typeid = '$viewbus'";
       $result58 = Treat_DB_ProxyOld::query($query58);
       $com_typename=Treat_DB_ProxyOld::mysql_result($result58,0,"type_name");

       echo "<tr bgcolor=#E8E7E7 style=\"border:2px solid #E8E7E7;\"><td colspan=1 style=\"border:2px solid #E8E7E7;\"><div style=\" width:150px; background:#FFFF99; border:1px solid #454545; display:inline;\"><center>$com_typename</center></div>&nbsp;";

       $query58 = "SELECT * FROM company WHERE company_type = '$viewbus' ORDER  BY companyname DESC";
       $result58 = Treat_DB_ProxyOld::query($query58);
       $num58=Treat_DB_ProxyOld::mysql_numrows($result58);

       $num58--;
       while($num58>=0){
          $compid2=Treat_DB_ProxyOld::mysql_result($result58,$num58,"companyid");
          $compname2=Treat_DB_ProxyOld::mysql_result($result58,$num58,"reference");

          echo "<div id='3.$compid2' style=\" width:150px; background:#CCCCCC; border:1px solid #454545; display:inline;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=3&viewbus=$compid2 style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>$compname2</font></center></div>&nbsp;";

          $num58--;
       }
       echo "<div style=\" width:50px; background:#CCCCCC; border:1px solid #454545; display:inline;\"><a href='javascript:void();' onclick=\"Effect.SlideDown('navigation', { duration: 1.0 }); return false;\" style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Nav</font></center></a></div>";
       if($num12==1){echo "&nbsp;<div style=\" width:50px; background:#CCCCCC; border:1px solid #454545; display:inline;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=1&viewbus=0 style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Back</font></center></div>";}
       if($num12==1){echo "&nbsp;<div style=\" width:50px; background:#CCCCCC; border:1px solid #454545; display:inline;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=1&viewbus=0 style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Home</font></center></a></a></div>";}
       echo "</td>";
    }
    elseif($viewtype==3){
       $compquery="business.companyid = company.companyid  AND company.companyid = '$viewbus'";
       $fromtables=",company";

       $query58 = "SELECT * FROM login_company_type WHERE loginid = '$loginid'";
       $result58 = Treat_DB_ProxyOld::query($query58);
       $can_goback=Treat_DB_ProxyOld::mysql_result($result58,0,"login_all");

       $query58 = "SELECT * FROM company WHERE companyid = '$viewbus'";
       $result58 = Treat_DB_ProxyOld::query($query58);
       $companyname=Treat_DB_ProxyOld::mysql_result($result58,0,"reference");
       $companytype=Treat_DB_ProxyOld::mysql_result($result58,0,"company_type");

       echo "<tr bgcolor=#E8E7E7 style=\"border:2px solid #E8E7E7;\"><td colspan=1 style=\"border:2px solid #E8E7E7;\"><div style=\" width:150px; background:#FFFF99; border:1px solid #454545; display:inline;\"><center>$companyname</center></div>&nbsp;";

       $query58 = "SELECT * FROM division WHERE companyid = '$viewbus' ORDER  BY division_name DESC";
       $result58 = Treat_DB_ProxyOld::query($query58);
       $num58=Treat_DB_ProxyOld::mysql_numrows($result58);

       $num58--;
       while($num58>=0){
          $divisionid=Treat_DB_ProxyOld::mysql_result($result58,$num58,"divisionid");
          $division_name=Treat_DB_ProxyOld::mysql_result($result58,$num58,"division_name");

          echo "<div id='4.$divisionid' style=\" width:150px; background:#CCCCCC; border:1px solid #454545; display:inline;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=4&viewbus=$divisionid style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>$division_name</font></center></div>&nbsp;";

          $num58--;
       }
       echo "<div style=\" width:50px; background:#CCCCCC; border:1px solid #454545; display:inline;\"><a href='javascript:void();' onclick=\"Effect.SlideDown('navigation', { duration: 1.0 }); return false;\" style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Nav</font></center></a></div>";
       if($num12==1){echo "&nbsp;<div style=\" width:50px; background:#CCCCCC; border:1px solid #454545; display:inline;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=2&viewbus=$companytype style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Back</font></center></div>";}
       if($num12==1){echo "&nbsp;<div style=\" width:50px; background:#CCCCCC; border:1px solid #454545; display:inline;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=1&viewbus=0 style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Home</font></center></a></a></div>";}
       echo "</td>";
    }
    elseif($viewtype==4){
       $compquery="business.districtid = district.districtid AND district.divisionid = '$viewbus'";
       $fromtables=",district";

       $query58 = "SELECT * FROM division WHERE divisionid = '$viewbus'";
       $result58 = Treat_DB_ProxyOld::query($query58);
       $divisionname=Treat_DB_ProxyOld::mysql_result($result58,0,"division_name");
       $comid=Treat_DB_ProxyOld::mysql_result($result58,0,"companyid");

       echo "<tr bgcolor=#E8E7E7 style=\"border:2px solid #E8E7E7;\"><td colspan=1 style=\"border:2px solid #E8E7E7;\"><div style=\" width:150px; background:#FFFF99; border:1px solid #454545; display:inline;\"><center>$divisionname</center></div>&nbsp;";

       $query58 = "SELECT * FROM district WHERE divisionid = '$viewbus' ORDER  BY districtname DESC";
       $result58 = Treat_DB_ProxyOld::query($query58);
       $num58=Treat_DB_ProxyOld::mysql_numrows($result58);

       $num58--;
       while($num58>=0){
          $districtid=Treat_DB_ProxyOld::mysql_result($result58,$num58,"districtid");
          $districtname=Treat_DB_ProxyOld::mysql_result($result58,$num58,"districtname");

          echo "<div id='5.$districtid' style=\" width:150px; background:#CCCCCC; border:1px solid #454545; display:inline;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=5&viewbus=$districtid style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>$districtname</font></center></div>&nbsp;";

          $num58--;
       }
       echo "<div style=\" width:50px; background:#CCCCCC; border:1px solid #454545; display:inline;\"><a href='javascript:void();' onclick=\"Effect.SlideDown('navigation', { duration: 1.0 }); return false;\" style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Nav</font></center></a></div>";
       if($num12==1){echo "&nbsp;<div style=\" width:50px; background:#CCCCCC; border:1px solid #454545; display:inline;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=3&viewbus=$comid style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Back</font></center></div>";}
       if($num12==1){echo "&nbsp;<div style=\" width:50px; background:#CCCCCC; border:1px solid #454545; display:inline;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=1&viewbus=0 style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Home</font></center></a></a></div>";}
       echo "</td>";
    }
    elseif($viewtype==5){
       $compquery="business.districtid = '$viewbus'";

       $query58 = "SELECT * FROM district WHERE districtid = '$viewbus'";
       $result58 = Treat_DB_ProxyOld::query($query58);
       $districtname=Treat_DB_ProxyOld::mysql_result($result58,0,"districtname");
       $divisionid=Treat_DB_ProxyOld::mysql_result($result58,0,"divisionid");

       echo "<tr bgcolor=#E8E7E7 style=\"border:2px solid #E8E7E7;\"><td colspan=1 style=\"border:2px solid #E8E7E7;\"><div style=\" width:150px; background:#FFFF99; border:1px solid #454545; display:inline;\"><center>$districtname</center></div>&nbsp;<form action=businesstrack.php method=post style=\"margin:0;padding:0;display:inline;\"><select name=viewbus onChange='changePage(this.form.viewbus)'>";

       $query58 = "SELECT * FROM business WHERE districtid = '$viewbus' ORDER BY businessname DESC";
       $result58 = Treat_DB_ProxyOld::query($query58);
       $num58=Treat_DB_ProxyOld::mysql_numrows($result58);

       $num58--;
       echo "<option value=>Please Choose...</option>";
       while($num58>=0){
          $businessid=Treat_DB_ProxyOld::mysql_result($result58,$num58,"businessid");
          $businessname=Treat_DB_ProxyOld::mysql_result($result58,$num58,"businessname");
          $comid=Treat_DB_ProxyOld::mysql_result($result58,$num58,"companyid");

          echo "<option value=businesstrack.php?viewbus=$businessid&viewtype=6&thismonth=$thismonth&thisyear=$thisyear&whichdist=$viewbus $show>$businessname</option>";

          $num58--;
       }
       echo "</select>";
       echo "&nbsp;<div style=\" width:50px; background:#CCCCCC; border:1px solid #454545; display:inline;\"><a href='javascript:void();' onclick=\"Effect.SlideDown('navigation', { duration: 1.0 }); return false;\" style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Nav</font></center></a></div>";
       if($num12==1){echo "&nbsp;<div style=\" width:50px; background:#CCCCCC; border:1px solid #454545; display:inline;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=4&viewbus=$divisionid style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Back</font></center></div>";}
       if($num12==1){echo "&nbsp;<div style=\" width:50px; background:#CCCCCC; border:1px solid #454545; display:inline;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=1&viewbus=0 style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Home</font></center></a></div>";}
       echo "</td>";

       $query1="UPDATE login SET companyid = '$comid' WHERE loginid = '$loginid'";
       $result1 = Treat_DB_ProxyOld::query($query1);
    }
    elseif($viewtype==6&&$viewbus>0){
       $compquery="business.businessid = '$viewbus'";

       $query58 = "SELECT districtid FROM business WHERE businessid = '$viewbus'";
       $result58 = Treat_DB_ProxyOld::query($query58);
       $whichdist=Treat_DB_ProxyOld::mysql_result($result58,0,"districtid");

       $query58 = "SELECT * FROM district WHERE districtid = '$whichdist'";
       $result58 = Treat_DB_ProxyOld::query($query58);
       $districtname=Treat_DB_ProxyOld::mysql_result($result58,0,"districtname");
       $divisionid=Treat_DB_ProxyOld::mysql_result($result58,0,"divisionid");

       echo "<tr bgcolor=#E8E7E7 style=\"border:2px solid #E8E7E7;\" align=bottom><td colspan=1 style=\"border:2px solid #E8E7E7;\"><form action=businesstrack.php method=post style=\"margin:0;padding:0;display:inline;\"><select name=viewbus onChange='changePage(this.form.viewbus)'>";

       if($security_level<3){$query58 = "SELECT * FROM business WHERE (businessid = '$busid1' OR businessid = '$busid2' OR businessid = '$busid3' OR businessid = '$busid4' OR businessid = '$busid5' OR businessid = '$busid6' OR businessid = '$busid7' OR businessid = '$busid8' OR businessid = '$busid9' OR businessid = '$busid10') ORDER BY businessname DESC";}
       else{$query58 = "SELECT * FROM business WHERE districtid = '$whichdist' ORDER BY businessname DESC";}
       $result58 = Treat_DB_ProxyOld::query($query58);
       $num58=Treat_DB_ProxyOld::mysql_numrows($result58);

       $num58--;
       echo "<option value=>Please Choose...</option>";
       while($num58>=0){
          $businessid=Treat_DB_ProxyOld::mysql_result($result58,$num58,"businessid");
          $businessname=Treat_DB_ProxyOld::mysql_result($result58,$num58,"businessname");
          $districtid=Treat_DB_ProxyOld::mysql_result($result58,$num58,"districtid");
          $comid=Treat_DB_ProxyOld::mysql_result($result58,$num58,"companyid");

          if($viewbus==$businessid){
             $show="SELECTED";
             $prevbus=Treat_DB_ProxyOld::mysql_result($result58,$num58+1,"businessid");
             $nextbus=Treat_DB_ProxyOld::mysql_result($result58,$num58-1,"businessid");
             $mystyle1="background-color:#FFFF99";
          }
          else{$show="";$mystyle1="background-color:#FFFFFF";}

          echo "<option value=businesstrack.php?viewbus=$businessid&viewtype=6&thismonth=$thismonth&thisyear=$thisyear&whichdist=$districtid $show style=$mystyle1>$businessname</option>";

          $num58--;
       }
       echo "</select></form>&nbsp;";

       if($prevbus>0&&$security_level>2){echo "&nbsp;<div style=\" width:50px; background:#CCCCCC; border:1px solid #454545; display:inline;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=6&viewbus=$prevbus style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Prev</font></center></a></div>";}
       else{echo "&nbsp;<div style=\" width:50px; background:#CCCCCC; border:1px solid #454545; display:inline;\"><center><font color=black>Prev</font></center></div>";}
       if($nextbus>0&&$security_level>2){echo "&nbsp;<div style=\" width:50px; background:#CCCCCC; border:1px solid #454545; display:inline;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=6&viewbus=$nextbus style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Next</font></center></a></div>";}
       else{echo "&nbsp;<div style=\" width:50px; background:#CCCCCC; border:1px solid #454545; display:inline;\"><center><font color=black>Next</font></center></div>";}

       echo "&nbsp;<div style=\" width:50px; background:#CCCCCC; border:1px solid #454545; display:inline;\"><a href='javascript:void();' onclick=\"Effect.SlideDown('navigation', { duration: 1.0 }); return false;\" style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Nav</font></center></a></div>";
       if($num12==1){echo "&nbsp;<div style=\" width:50px; background:#CCCCCC; border:1px solid #454545; display:inline;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=5&viewbus=$districtid style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Back</font></center></a></div>";}
       if($num12==1){echo "&nbsp;<div style=\" width:50px; background:#CCCCCC; border:1px solid #454545; display:inline;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=1&viewbus=0 style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Home</font></center></a></a></div>";}

       echo "</td>";

          if ($sec_bus_sales>0){$goto="busdetail.php";}
          elseif ($sec_bus_invoice>0){$goto="businvoice.php";}
          elseif ($sec_bus_order>0){$goto="buscatertrack.php";}
          elseif ($sec_bus_payable>0){$goto="busapinvoice.php";}
          elseif ($sec_bus_inventor1>0){$goto="businventor.php";}
          elseif ($sec_bus_control>0){$goto="buscontrol.php";}
          elseif ($sec_bus_order_setup>0){$goto="buscaterroom.php";}
          elseif ($sec_bus_request>0){$goto="busrequest.php";}
          elseif ($sec_route_collections>0){$goto="busroute_collect.php";}
          elseif ($sec_route_labor>0){$goto="busroute_labor.php";}
          elseif ($sec_route_detail>0){$goto="busroute_detail.php";}
          elseif ($sec_bus_labor>0){$goto="buslabor.php";}
          else {$goto="businesstrack.php";}

          $query1="UPDATE login SET companyid = '$comid' WHERE loginid = '$loginid'";
          $result1 = Treat_DB_ProxyOld::query($query1);

          $showdetail=" | <a href=$goto?bid=$viewbus&cid=$comid style=$style><font color=blue  onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>View Details</font></a>";
    }
    else{
       echo "<tr bgcolor=#E8E7E7 style=\"border:2px solid #E8E7E7;\" align=bottom><td colspan=1 style=\"border:2px solid #E8E7E7;\">";

       echo "<div style=\" width:150px; background:#FFFF99; border:1px solid #454545; display:inline;\"><center>Treat America</center></div>&nbsp;<div style=\" width:50px; background:#CCCCCC; border:1px solid #454545; display:inline;\"><a href='javascript:void();' onclick=\"Effect.SlideDown('navigation', { duration: 1.0 }); return false;\" style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Nav</font></center></a></div></td>";
    }

    if($showwkds==1&&$viewtype>0){$showweekends="<a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&showwkds=0&viewtype=$viewtype&viewbus=$viewbus&whichdist=$whichdist style=$style><font color=blue  onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Hide Weekends</font></a>";}
    elseif($viewtype>0){$showweekends="<a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&showwkds=1&viewtype=$viewtype&viewbus=$viewbus&whichdist=$whichdist style=$style><font color=blue  onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Show Weekends</font></a>";}

    echo "<td align=right style=\"border:2px solid #E8E7E7;\"> $showweekends$showdetail$showprev$shownext&nbsp;</td></tr>";

    /////////////////////////
    $query1="UPDATE login SET oo4 = '1', oo5 = '$viewtype', oo6 = '$viewbus', oo7 = '$showwkds' WHERE loginid = '$loginid'";
    $result1 = Treat_DB_ProxyOld::query($query1);

    /////////////////////////
    ///NAVIGATION////////////
    /////////////////////////
    echo "<tr><td colspan=2>";
    echo "<div id=\"navigation\" style=\"display:none; width:100%; background:#FFFFFF; border:2px solid #E8E7E7;\"><br>";

    $query12="SELECT * FROM login_all WHERE loginid = '$loginid'";
    $result12 = Treat_DB_ProxyOld::query($query12);
    $num12=Treat_DB_ProxyOld::mysql_numrows($result12);

    if($num12==1){$allow=1; $allow_all=1; echo "<a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=1&viewbus=0 style=$style><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Treat America</b></font></a><br>";}

    ///////////COMPANY TYPE
    $query = "SELECT * FROM company_type ORDER BY type_name DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);

    $num--;
    while ($num>=0){
       $typeid=Treat_DB_ProxyOld::mysql_result($result,$num,"typeid");
       $type_name=Treat_DB_ProxyOld::mysql_result($result,$num,"type_name");

       $query45 = "SELECT * FROM login_company_type WHERE loginid = '$loginid' AND company_type = '$typeid'";
       $result45 = Treat_DB_ProxyOld::query($query45);
       $num45=Treat_DB_ProxyOld::mysql_numrows($result45);

       if ($num45!=0||$allow_all==1){$allow=1; echo "<a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=2&viewbus=$typeid style=$style><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>$type_name</b></font></a><br>";}

       /////////COMPANIES
       $query2 = "SELECT * FROM company WHERE company_type = '$typeid' ORDER BY companyname DESC";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=Treat_DB_ProxyOld::mysql_numrows($result2);

       $num2--;
       while ($num2>=0){
          $co_id=Treat_DB_ProxyOld::mysql_result($result2,$num2,"companyid");
          $companyname=Treat_DB_ProxyOld::mysql_result($result2,$num2,"reference");

          $query45 = "SELECT * FROM login_comp WHERE loginid = '$loginid' AND companyid = '$co_id'";
          $result45 = Treat_DB_ProxyOld::query($query45);
          $num45=Treat_DB_ProxyOld::mysql_numrows($result45);

          if ($num45!=0||$allow==1||$allow_all==1){$allow=1; echo "<a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=3&viewbus=$co_id style=$style><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$companyname</font></a><br>";}

          /////////////DIVISIONS
          $query3 = "SELECT * FROM division WHERE companyid = '$co_id' ORDER BY division_name DESC";
          $result3 = Treat_DB_ProxyOld::query($query3);
          $num3=Treat_DB_ProxyOld::mysql_numrows($result3);

          $num3--;
          while ($num3>=0){
             $divisionid=Treat_DB_ProxyOld::mysql_result($result3,$num3,"divisionid");
             $division_name=Treat_DB_ProxyOld::mysql_result($result3,$num3,"division_name");

             $query45 = "SELECT * FROM login_division WHERE loginid = '$loginid' AND divisionid = '$divisionid'";
             $result45 = Treat_DB_ProxyOld::query($query45);
             $num45=Treat_DB_ProxyOld::mysql_numrows($result45);

             if ($num45!=0||$allow==1||$allow_all==1){$allow=1; echo "<a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=4&viewbus=$divisionid style=$style><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$division_name</font></a><br>";}

             /////////////DISTRICTS
             $query4 = "SELECT * FROM district WHERE divisionid = '$divisionid' ORDER BY districtname DESC";
             $result4 = Treat_DB_ProxyOld::query($query4);
             $num4=Treat_DB_ProxyOld::mysql_numrows($result4);

             $num4--;
             while ($num4>=0){
                $districtid=Treat_DB_ProxyOld::mysql_result($result4,$num4,"districtid");
                $districtname=Treat_DB_ProxyOld::mysql_result($result4,$num4,"districtname");

                $query45 = "SELECT * FROM login_district WHERE loginid = '$loginid' AND districtid = '$districtid'";
                $result45 = Treat_DB_ProxyOld::query($query45);
                $num45=Treat_DB_ProxyOld::mysql_numrows($result45);

                if ($num45!=0||$allow==1||$allow_all==1){$allow=1; echo "<a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=5&viewbus=$districtid style=$style><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$districtname</font></a>&nbsp;<a href='javascript:void();' onclick=\"Effect.SlideDown(biz$districtid, { duration: 1.0 }); return false;\"><img src=arrowdown.gif height=10 width=10 border=0 alt='Expand'></a><br>";}

                /////////////BUSINESS
                if($security_level>2){$query5 = "SELECT businessname,businessid FROM business WHERE districtid = '$districtid' ORDER BY businessname DESC";}
                else{$query5 = "SELECT businessname,businessid FROM business WHERE (businessid = '$busid1' OR businessid = '$busid2' OR businessid = '$busid3' OR businessid = '$busid4' OR businessid = '$busid5' OR businessid = '$busid6' OR businessid = '$busid7' OR businessid = '$busid8' OR businessid = '$busid9' OR businessid = '$busid10') AND districtid = '$districtid' ORDER BY businessname DESC";}
                $result5 = Treat_DB_ProxyOld::query($query5);
                $num5=Treat_DB_ProxyOld::mysql_numrows($result5);

                if($security_level<3){echo "<div id='biz$districtid' style=\"width:100%; background:#FFFFFF;\"><img src=clear.gif height=1 width=1>";}
                else{echo "<div id='biz$districtid' style=\"display:none; width:100%; background:#FFFFFF;\"><img src=clear.gif height=1 width=1>";}

                $num5--;
                while ($num5>=0){
                   $bizid=Treat_DB_ProxyOld::mysql_result($result5,$num5,"businessid");
                   $businessname=Treat_DB_ProxyOld::mysql_result($result5,$num5,"businessname");

                   echo "<a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewtype=6&viewbus=$bizid style=$style><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$businessname</font></a><br>";

                   $num5--;
                } 

                if($security_level>2){echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:void();' onclick=\"Effect.SlideUp(biz$districtid, { duration: 1.0 }); return false;\"><img src=arrowup.gif height=10 width=10 border=0 alt='Collapse'></a></div>";}

                $num4--;
             } 
             $allow=0;
             $num3--;
          } 
          $allow=0;
          $num2--;
       }
       $allow=0;
       $num--;
    }

    echo "<a href='javascript:void();' onclick=\"$('navigation').hide(); return false;\" style=$style><font size=2 color=#454545>Close</font></a></div>";
    echo "</td></tr>";
////////////////END NAV

/////////////////////////////
////////////////VENDING NAV
/////////////////////////////
    $query11="SELECT * FROM vend_locations,business$fromtables WHERE vend_locations.unitid = business.businessid AND ($compquery)";
    $result11 = Treat_DB_ProxyOld::query($query11);
    $num11=Treat_DB_ProxyOld::mysql_numrows($result11);

    if($num11>0&&$viewtype>=2){
       echo "<tr bgcolor=#E8E7E7><td colspan=2 style=\"border:2px solid #E8E7E7;\">";

       if($viewsub==""){echo "<div style=\" width:100px; background:#FFFF99; border:1px solid #454545; display:inline;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub= style=$style><center><font color=black onMouseOver=this.color='#FF9900' onMouseOut=this.color='black'>Overview</font></center></a></div>";}
       else{echo "<div style=\" width:100px; background:#CCCCCC; border:1px solid #454545; display:inline;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub= style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Overview</font></center></a></div>";}

       if($viewsub==1){echo "&nbsp;<div style=\" width:100px; background:#FFFF99; border:1px solid #454545; display:inline;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=1 style=$style><center><font color=black onMouseOver=this.color='#FF9900' onMouseOut=this.color='black'>Customers</font></center></a></div>";}
       else{echo "&nbsp;<div style=\" width:100px; background:#CCCCCC; border:1px solid #454545; display:inline;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=1 style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Customers</font></center></a></div>";}

       if($viewsub==2){echo "&nbsp;<div style=\" width:100px; background:#FFFF99; border:1px solid #454545; display:inline;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=2 style=$style><center><font color=black onMouseOver=this.color='#FF9900' onMouseOut=this.color='black'>Routes</font></center></a></div>";}
       else{echo "&nbsp;<div style=\" width:100px; background:#CCCCCC; border:1px solid #454545; display:inline;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=2 style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Routes</font></center></a></div>";}

       if($viewsub==3){echo "&nbsp;<div style=\" width:100px; background:#FFFF99; border:1px solid #454545; display:inline;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=3 style=$style><center><font color=black onMouseOver=this.color='#FF9900' onMouseOut=this.color='black'>Route Drivers</font></center></a></div>";}
       else{echo "&nbsp;<div style=\" width:100px; background:#CCCCCC; border:1px solid #454545; display:inline;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=3 style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Route Drivers</font></center></a></div>";}

       if($viewsub==4){echo "&nbsp;<div style=\" width:100px; background:#FFFF99; border:1px solid #454545; display:inline;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=4 style=$style><center><font color=black onMouseOver=this.color='#FF9900' onMouseOut=this.color='black'>Vehicles</font></center></a></div>";}
       else{echo "&nbsp;<div style=\" width:100px; background:#CCCCCC; border:1px solid #454545; display:inline;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=4 style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Vehicles</font></center></a></div>";}

       if($viewsub==5){echo "&nbsp;<div style=\" width:100px; background:#FFFF99; border:1px solid #454545; display:inline;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=5 style=$style><center><font color=black onMouseOver=this.color='#FF9900' onMouseOut=this.color='black'>Service</font></center></a></div>";}
       else{echo "&nbsp;<div style=\" width:100px; background:#CCCCCC; border:1px solid #454545; display:inline;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=5 style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Service</font></center></a></div>";}

       if($viewsub==6){echo "&nbsp;<div style=\" width:100px; background:#FFFF99; border:1px solid #454545; display:inline;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=6 style=$style><center><font color=black onMouseOver=this.color='#FF9900' onMouseOut=this.color='black'>Sales</font></center></a></div>";}
       else{echo "&nbsp;<div style=\" width:100px; background:#CCCCCC; border:1px solid #454545; display:inline;\"><a href=businesstrack.php?thismonth=$thismonth&thisyear=$thisyear&viewsub=6 style=$style><center><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Sales</font></center></a></div>";}

       echo "</td></tr>";
    }

    echo "</table><table width=90% cellpadding=1>";
if($viewtype>0&&$viewtype<7&&$viewbus!=""&&$viewsub==""){

    echo "<tr valign=top><td width=66.66%>";
    ///////////////////////////
    //////END FILTER///////////
    //////START DASHBOARD//////
    ///////////////////////////

       ///////GET DATE RANGES

       $today=date("Y-m-d");
       $whathour=date("G");
       if($whathour<16){$today=prevday($today);}
       $thisday=date("d");
       if($thismonth==""){$thismonth=date("m");}
       if($thisyear==""){$thisyear=date("Y");}

       $ytdstart="$thisyear-01-01";
       $mtdstart="$thisyear-$thismonth-01";
       $monthend=$thismonth+1;
       if($monthend<10){$monthend="0$monthend";}
       if($monthend==13){$sp_year=$thisyear+1;$monthend="01";$monthend="$sp_year-$monthend-01";$monthend=prevday($monthend);}
       else{$monthend="$thisyear-$monthend-01"; $monthend=prevday($monthend);}
       $enddate="$thisyear-$thismonth-31";
       $yearend="$thisyear-12-31";

       if($thisyear!=date("Y")){$enddate="$thisyear-12-31";}
       
       ////////////////////////////////////////////////
       //////////////////GET INFO INTO ARRAY///////////
       ////////////////////////////////////////////////

       $query1="SELECT dashboard.sales,dashboard.sales_bgt,dashboard.date FROM dashboard,business$fromtables WHERE dashboard.businessid = business.businessid AND ($compquery) AND dashboard.date >= '$ytdstart' AND dashboard.date <='$yearend'";
       $result1 = Treat_DB_ProxyOld::query($query1);
       $num1=Treat_DB_ProxyOld::mysql_numrows($result1);

       $num1--;
       //while($num1>=0){
       while($r=Treat_DB_ProxyOld::mysql_fetch_array($result1)){
          $date=$r["date"];
          $sales=$r["sales"];
          $sales_bgt=$r["sales_bgt"];

          $db_sales[$date]+=$sales;
          $db_sales_bgt[$date]+=$sales_bgt;

          if(dayofweek($date)=="Monday"){$db_daily_sales[Monday]+=$sales;$db_daily_sales_bgt[Monday]+=$sales_bgt;}
          elseif(dayofweek($date)=="Tuesday"){$db_daily_sales[Tuesday]+=$sales;$db_daily_sales_bgt[Tuesday]+=$sales_bgt;}
          elseif(dayofweek($date)=="Wednesday"){$db_daily_sales[Wednesday]+=$sales;$db_daily_sales_bgt[Wednesday]+=$sales_bgt;}
          elseif(dayofweek($date)=="Thursday"){$db_daily_sales[Thursday]+=$sales;$db_daily_sales_bgt[Thursday]+=$sales_bgt;}
          elseif(dayofweek($date)=="Friday"){$db_daily_sales[Friday]+=$sales;$db_daily_sales_bgt[Friday]+=$sales_bgt;}
          elseif(dayofweek($date)=="Saturday"){$db_daily_sales[Saturday]+=$sales;$db_daily_sales_bgt[Saturday]+=$sales_bgt;}
          elseif(dayofweek($date)=="Sunday"){$db_daily_sales[Sunday]+=$sales;$db_daily_sales_bgt[Sunday]+=$sales_bgt;}

          $what_month=substr($date,5,2);
          $month_sales[$what_month]+=$sales;
          $month_sales_budget[$what_month]+=$sales_bgt;

          if($date<=$today){
             $db_sales_ytd[$date]+=$sales;
             $db_sales_bgt_ytd[$date]+=$sales_bgt;

             $month_sales_ytd[$what_month]+=$sales;
             $month_sales_budget_ytd[$what_month]+=$sales_bgt;
          }

          $year_sales+=$sales;

          if($sales_total2<$month_sales[$what_month]){$sales_total2=$month_sales[$what_month];}

          $num1--;
       }

       $tempdate=$ytdstart;
       while($tempdate<=$enddate){
          if($db_sales[$tempdate]>0){$numdays[dayofweek($tempdate)]++;}
          $tempdate=nextday($tempdate);
       }

       $prevyear=$thisyear;
       $prevmonth=$thismonth-1;
       if($prevmonth==0){$prevmonth=12;$prevyear=$thisyear-1;}
       elseif($prevmonth<10){$prevmonth="0$prevmonth";}

       $nextyear=$thisyear;
       $nextmonth=$thismonth+1;
       if($nextmonth==13){$nextmonth="01";$nextyear=$thisyear+1;}
       elseif($nextmonth<10){$nextmonth="0$nextmonth";}

       /////////////////////////////////////////////
       ///////////////////TOTAL MONTH BUDGET////////
       /////////////////////////////////////////////

       $query1="SELECT SUM(vend_budget.amount) AS totalamt FROM vend_budget,business$fromtables WHERE vend_budget.businessid = business.businessid AND ($compquery) AND vend_budget.date >= '$mtdstart' ANd vend_budget.date <= '$monthend' AND vend_budget.type = '10'";
       $result1 = Treat_DB_ProxyOld::query($query1);

       $month_budget=Treat_DB_ProxyOld::mysql_result($result1,0,"totalamt");

       $query1="SELECT SUM(budget.`$thismonth`) AS totalamt FROM budget,business$fromtables WHERE budget.businessid = business.unit AND ($compquery) AND budget.year = '$thisyear' AND budget.type = '10'";  
       $result1 = Treat_DB_ProxyOld::query($query1);

       $month_budget+=Treat_DB_ProxyOld::mysql_result($result1,0,"totalamt");

       /////////////////////////////////////////////
       ///////////////////TOTAL YEAR BUDGET/////////
       /////////////////////////////////////////////

       $query1="SELECT SUM(vend_budget.amount) AS totalamt FROM vend_budget,business$fromtables WHERE vend_budget.businessid = business.businessid AND ($compquery) AND vend_budget.date >= '$ytdstart' ANd vend_budget.date <= '$yearend' AND vend_budget.type = '10'";
       $result1 = Treat_DB_ProxyOld::query($query1);

       $year_budget=Treat_DB_ProxyOld::mysql_result($result1,0,"totalamt");

       $query1="SELECT SUM(budget.01+budget.02+budget.03+budget.04+budget.05+budget.06+budget.07+budget.08+budget.09+budget.10+budget.11+budget.12) AS totalamt FROM budget,business$fromtables WHERE budget.businessid = business.unit AND ($compquery) AND budget.year = '$thisyear' AND budget.type = '10'";
       $result1 = Treat_DB_ProxyOld::query($query1);

       $year_budget+=Treat_DB_ProxyOld::mysql_result($result1,0,"totalamt");

       ////////////////////////////////////////////////////////////////////
       ///////////////BEGIN SALES AGGREGATE GRAPH//////////////////////////
       ////////////////////////////////////////////////////////////////////
       if($thismonth==01){$showtitle="January $thisyear";}
       elseif($thismonth==02){$showtitle="February $thisyear";}
       elseif($thismonth==03){$showtitle="March $thisyear";}
       elseif($thismonth==04){$showtitle="April $thisyear";}
       elseif($thismonth==5){$showtitle="May $thisyear";}
       elseif($thismonth==6){$showtitle="June $thisyear";}
       elseif($thismonth==7){$showtitle="July $thisyear";}
       elseif($thismonth==8){$showtitle="August $thisyear";}
       elseif($thismonth==9){$showtitle="September $thisyear";}
       elseif($thismonth==10){$showtitle="October $thisyear";}
       elseif($thismonth==11){$showtitle="November $thisyear";}
       elseif($thismonth==12){$showtitle="December $thisyear";}

       echo "<br><table width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td><a href=businesstrack.php?thismonth=$prevmonth&thisyear=$prevyear style=$style><font size=1 color=blue>[PREV]</font></a> Sales Aggregate $showtitle <a href=businesstrack.php?thismonth=$nextmonth&thisyear=$nextyear style=$style><font size=1 color=blue>[NEXT]</font></a></td></tr><tr><td>";
?>
       <applet code="LineGraphApplet.class" archive="Linegraph.jar" width="100%" height=300">

       <!-- Start Up Parameters -->
       <PARAM name="LOADINGMESSAGE" value="Creating Chart - Please Wait.">
       <PARAM name="STEXTCOLOR"     value="#000060">
       <PARAM name="STARTUPCOLOR"   value="#FFFFFF">

       <!-- Line Data -->
       <!--   value,URL,Target Frame -->

<?php
    $series1="series1";
    $series2="series2";

    $counter=1;
    $tempdate=$mtdstart;
    while($tempdate<=$monthend){
       $dayname=dayofweek($tempdate);
       if($showwkds!=1&&($dayname=="Saturday"||$dayname=="Sunday")){}
       else{
         //$showsales+=$db_sales[$tempdate];
         $showbudget+=$db_sales_bgt[$tempdate];
         echo "<PARAM name=data$counter$series1 value=$showbudget>";
         //if($tempdate<=$today){echo "<PARAM name=data$counter$series2 value=$showsales>";}

         $counter++;
       }

       $tempdate=nextday($tempdate);
    }
    $counter=1;
    $tempdate=$mtdstart;
    while($tempdate<=$monthend){
       $dayname=dayofweek($tempdate);
       if($showwkds!=1&&($dayname=="Saturday"||$dayname=="Sunday")){}
       else{
         $showsales+=$db_sales[$tempdate];
         //$showbudget+=$db_sales_bgt[$tempdate];
         //echo "<PARAM name=data$counter$series1 value=$showbudget>";
         if($tempdate<=$today){echo "<PARAM name=data$counter$series2 value=$showsales>";}

         $counter++;
       }

       $tempdate=nextday($tempdate);
    }

?>

       <!-- Properties -->
       <PARAM name="ylabels"		  value="S|M|T|W|T|F|S">
       <PARAM name="xlabels"		  value="S|M|T|W|T|F|S">
       <PARAM name="titlefontsize"	  value="0">
       <PARAM name="xtitlefontsize"	  value="0">
       <PARAM name="ytitlefontsize"	  value="0">
       <PARAM name="autoscale"            value="true">
       <PARAM name="gridbgcolor"          value="#FFFFFF">
       <PARAM name="series1"              value="red|6|5|false|dotted">
       <PARAM name="series2"              value="blue|6|5|true|dotted">
       <PARAM name="ndecplaces"           value="0">
       <PARAM name="labelOrientation"     value="Up Angle">
       <PARAM name="xlabel_pre"           value="">
       <PARAM name="xlabel_font"          value="Arial,N,10">
<?
    $counter=1;
    $tempdate=$mtdstart;
    while($tempdate<=$monthend){
       $dayname=dayofweek($tempdate);
       if($showwkds!=1&&($dayname=="Saturday"||$dayname=="Sunday")){}
       else{
         $g_day=substr($tempdate,8,2);
         $g_month=substr($tempdate,5,2);
         echo "<PARAM name='label$counter' value='$g_month/$g_day'>";

         $counter++;
       }

       $tempdate=nextday($tempdate);
    }
?>
       </applet>
       </td></tr>
       <tr><td> <table style="margin:0;padding:0;display:inline;"><tr height=4><td width=10 bgcolor=#0000FF></td><td bgcolor=white><font size=1>Sales</td></tr></table> <table style="margin:0;padding:0;display:inline;"><tr height=4><td width=10 bgcolor=#FF0000></td><td bgcolor=white><font size=1>Budget</td></tr></table> </td></tr>
       </table>

<?
       
       /////////////////////////////////////////////
       //////////////////////SALES FOR MONTH////////
       /////////////////////////////////////////////

       //////FIGURE GRAPH TOTALS
       $sales_total=0;
       $tempdate=$mtdstart;
       for($counter=1;$counter<8;$counter++){
          $dayname=dayofweek($tempdate);
          if($sales_total<($db_daily_sales[$dayname]/$numdays[$dayname])){$sales_total=$db_daily_sales[$dayname]/$numdays[$dayname];}
          $tempdate=nextday($tempdate);
       }
       $sales_total=$sales_total*1.3;
       $sales_total2=$sales_total2*1.3;

       echo "<br><table width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td colspan=4><a href=businesstrack.php?thismonth=$prevmonth&thisyear=$prevyear style=$style><font size=1 color=blue>[PREV]</font></a> Daily Sales/Budget/Average YTD $showtitle <a href=businesstrack.php?thismonth=$nextmonth&thisyear=$nextyear style=$style><font size=1 color=blue>[NEXT]</font></a></td></tr><tr><td colspan=4>";

       echo "<table cellspacing=0 cellpadding=0 width=100% background=grid.jpg><tr height=200>";
       $tempdate=$mtdstart;
       while($tempdate<=$monthend){
          $dayname=dayofweek($tempdate);

          $avg=$db_daily_sales[$dayname]/$numdays[$dayname];

          $graph1=hgraph($db_sales_ytd[$tempdate],$sales_total,'#0000FF',8);
          $graph2=hgraph($db_sales_bgt_ytd[$tempdate],$sales_total,'#FF0000',8);
          $graph3=hgraph($avg,$sales_total,'#00FF00',8);
          if($showwkds!=1&&($dayname=="Saturday"||$dayname=="Sunday")){}
          else{echo "$graph1$graph2$graph3<td width=8>&nbsp;</td>";}
          $tempdate=nextday($tempdate);
       }
       echo "</tr>";
       echo "<tr bgcolor=white>";
       $tempdate=$mtdstart;
       while($tempdate<=$monthend){
          $dayname=dayofweek($tempdate);
          $is_day=substr($tempdate,8,2);
          if($showwkds!=1&&($dayname=="Saturday"||$dayname=="Sunday")){}
          else{echo "<td colspan=4><font size=1>$is_day</font></td>";}
          $tempdate=nextday($tempdate);
       }
       echo "<td></td></tr>";

       echo "</table>";
       echo "</td></tr>";
       echo "<tr><td> <table style=\"margin:0;padding:0;display:inline;\"><tr height=4><td width=10 bgcolor=#0000FF></td><td bgcolor=white><font size=1>Sales</td></tr></table> <table style=\"margin:0;padding:0;display:inline;\"><tr height=4><td width=10 bgcolor=#FF0000></td><td bgcolor=white><font size=1>Budget</td></tr></table> <table style=\"margin:0;padding:0;display:inline;\"><tr height=4><td width=10 bgcolor=#00FF00></td><td bgcolor=white><font size=1>Daily Average (YTD)</td></tr></table> </td></tr>";
       echo "</table>";

       /////////////////////////////////////////////
       //////////////////////SALES FOR YEAR/////////
       /////////////////////////////////////////////
       $prevyear=$thisyear-1;
       $nextyear=$thisyear+1;

       echo "<br><table width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td colspan=4><a href=businesstrack.php?thismonth=$thismonth&thisyear=$prevyear style=$style><font size=1 color=blue>[PREV]</font></a> Monthly Sales/Budget $thisyear <a href=businesstrack.php?thismonth=$thismonth&thisyear=$nextyear style=$style><font size=1 color=blue>[NEXT]</font></a></td></tr><tr><td colspan=4>";

       echo "<table cellspacing=0 cellpadding=0 width=100% background=grid.jpg><tr height=200><td width=15>&nbsp;</td>";
       $tempdate=$ytdstart;
       for($counter=1;$counter<=12;$counter++){
          if($counter<10){$what_month="0$counter";}
          else{$what_month=$counter;}

          $diff=round($month_sales_ytd[$what_month]-$month_sales_budget_ytd[$what_month],2);
          $graph1=hgraph($month_sales_ytd[$what_month],$sales_total2,'#0000FF',15,$diff);
          $graph2=hgraph($month_sales_budget_ytd[$what_month],$sales_total2,'#FF0000',15);
          echo "$graph1$graph2<td width=24>&nbsp;</td>";

          $tempdate=nextday($tempdate);
       }
       echo "<td></td></tr>";
       echo "<tr bgcolor=white>";
       echo "<td>&nbsp;</td><td colspan=3><font size=2>JAN</font></td><td colspan=3><font size=2>FEB</font></td><td colspan=3><font size=2>MAR</font></td><td colspan=3><font size=2>APR</font></td><td colspan=3><font size=2>MAY</font></td><td colspan=3><font size=2>JUN</font></td><td colspan=3><font size=2>JUL</font></td><td colspan=3><font size=2>AUG</font></td><td colspan=3><font size=2>SEP</font></td><td colspan=3><font size=2>OCT</font></td><td colspan=3><font size=2>NOV</font></td><td colspan=3><font size=2>DEC</font></td>";
       echo "<td></td></tr>";

       echo "</table>";
       echo "</td></tr>";
       echo "<tr><td> <table style=\"margin:0;padding:0;display:inline;\"><tr height=4><td width=10 bgcolor=#0000FF></td><td bgcolor=white><font size=1>Sales</td></tr></table> <table style=\"margin:0;padding:0;display:inline;\"><tr height=4><td width=10 bgcolor=#FF0000></td><td bgcolor=white><font size=1>Budget</td></tr></table> </td></tr>";
       echo "</table>";

       ////////////////////////////////////////////////////////////////////
       ///////////////BEGIN SALES GRAPH////////////////////////////////////
       ////////////////////////////////////////////////////////////////////
?>
       <br><table width=100% cellspacing=0 cellpadding=0 style="border:2px solid #E8E7E7;"><tr bgcolor=#E8E7E7><td>Sales/Budget Last 25 Days</td></tr><tr><td>
       <applet code="LineGraphApplet.class" archive="Linegraph.jar" width="100%" height=200">

       <!-- Start Up Parameters -->
       <PARAM name="LOADINGMESSAGE" value="Creating Chart - Please Wait.">
       <PARAM name="STEXTCOLOR"     value="#000060">
       <PARAM name="STARTUPCOLOR"   value="#FFFFFF">

       <!-- Line Data -->
       <!--   value,URL,Target Frame -->

<?php
    $series1="series1";
    $series2="series2";
    $tempdate=$today;
    for($counter=1;$counter<=24;$counter++){
       $tempdate=prevday($tempdate);
       if(dayofweek($tempdate)=="Sunday"&&$showwkds!=1){$tempdate=prevday($tempdate);}
       if(dayofweek($tempdate)=="Saturday"&&$showwkds!=1){$tempdate=prevday($tempdate);}
    }
    $tempdate2=$tempdate;

    $counter=1;
    while($tempdate<=$today){
       $dayname=dayofweek($tempdate);
       if($showwkds!=1&&($dayname=="Saturday"||$dayname=="Sunday")){}
       else{
         echo "<PARAM name=data$counter$series1 value=$db_sales_bgt[$tempdate]>";
         echo "<PARAM name=data$counter$series2 value=$db_sales[$tempdate]>";

         $counter++;
       }

       $tempdate=nextday($tempdate);
    }
?>

       <!-- Properties -->
       <PARAM name="ylabels"		  value="S|M|T|W|T|F|S">
       <PARAM name="xlabels"		  value="S|M|T|W|T|F|S">
       <PARAM name="titlefontsize"	  value="0">
       <PARAM name="xtitlefontsize"	  value="0">
       <PARAM name="ytitlefontsize"	  value="0">
       <PARAM name="autoscale"            value="true">
       <PARAM name="gridbgcolor"          value="#FFFFFF">
       <PARAM name="series1"              value="red|6|5|false|dotted">
       <PARAM name="series2"              value="blue|6|5|true|dotted">
       <PARAM name="ndecplaces"           value="0">
       <PARAM name="labelOrientation"     value="Up Angle">
       <PARAM name="xlabel_pre"           value="">
       <PARAM name="xlabel_font"          value="Arial,N,10">
<?
    $counter=1;
    $tempdate=$tempdate2;
    while($tempdate<=$today){
       $dayname=dayofweek($tempdate);
       if($showwkds!=1&&($dayname=="Saturday"||$dayname=="Sunday")){}
       else{
         $g_day=substr($tempdate,8,2);
         $g_month=substr($tempdate,5,2);
         echo "<PARAM name='label$counter' value='$g_month/$g_day'>";

         $counter++;
       }

       $tempdate=nextday($tempdate);
    }
?>
       </applet>
       </td></tr>
       <tr><td> <table style="margin:0;padding:0;display:inline;"><tr height=4><td width=10 bgcolor=#0000FF></td><td bgcolor=white><font size=1>Sales</td></tr></table> <table style="margin:0;padding:0;display:inline;"><tr height=4><td width=10 bgcolor=#FF0000></td><td bgcolor=white><font size=1>Budget</td></tr></table> </td></tr>
       </table><p>

<?

//////////////page break

       echo "</td><td width=33.33%>";

       //////////////////////////////////////////////////////////
       /////////////////MONTH & YEAR SALES/BUDGET THERMOMETER////
       //////////////////////////////////////////////////////////
       $mytitle=explode(' ',$showtitle);

       $mytitle[0]=strtoupper(substr($mytitle[0],0,3));

       echo "<br><table width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td colspan=4>Sales % of Budget $showtitle</td></tr><tr><td colspan=2 width=50%><center>";

       $showpercent=round($month_sales[$thismonth]/$month_budget*100,1);
       $len=strlen($showpercent);
       while($len<6){$showpercent="&nbsp;$showpercent";$len++;}
       echo "<table cellspacing=0 cellpadding=0 border=0 width=55>";      
       $graph1=hgraph($month_sales[$thismonth],$month_budget,'#414141',12,"","","","thermmonth");
       $graph2=hgraph($month_budget,$month_budget,'white',20,0,'white',10,"","");

       echo "<tr height=220><td width=11><img src=clear.gif></td>$graph1$graph2</tr>";
       echo "<tr height=40><td width=25 colspan=3 style=\"background-image:url('beaker3.jpg'); background-repeat: no-repeat;\"><div id='thermmonth_pcnt' style=\"align:center;color:#414141;font-size:10pt;font-weight:bold;\">$showpercent</div></td></tr>";
       echo "<tr height=4><td colspan=3><center><font size=2>$mytitle[0]</font></center></td></tr>";
       echo "</table></center>";

       echo "</td><td colspan=2 width=50%><center>";

       $showpercent=round($year_sales/$year_budget*100,1);
       $len=strlen($showpercent);
       while($len<6){$showpercent="&nbsp;$showpercent";$len++;}
       echo "<table cellspacing=0 cellpadding=0 border=0 width=55>";
       $graph1=hgraph($year_sales,$year_budget,'#414141',12,"","","","thermyear");
       $graph2=hgraph($year_budget,$year_budget,'white',20,0,'white',10);

       echo "<tr height=220><td width=11><img src=clear.gif></td>$graph1$graph2</tr>";
       echo "<tr height=40><td width=25 colspan=3 align=right style=\"background-image:url('beaker3.jpg'); background-repeat: no-repeat;\"><div id='thermyear_pcnt' style=\"align:center;color:#414141;font-size:10pt;font-weight:bold;\">$showpercent</div></td></tr>";
       echo "<tr height=4><td colspan=3><center><font size=2>$mytitle[1]</font></td></tr>";
       echo "</table></center>";

       echo "</center></td></tr></table>";
      
       /////////////////
       /////AGING///////
       /////////////////
       if($viewtype==6&&$viewsales>0){
          $query1="SELECT SUM(dashboard_aging_salesmen.zero) AS zero,SUM(dashboard_aging_salesmen.thirty) AS thirty,SUM(dashboard_aging_salesmen.sixty) AS sixty,SUM(dashboard_aging_salesmen.ninety) AS ninety FROM dashboard_aging_salesmen WHERE dashboard_aging_salesmen.salesmanid = '$viewsales'";
          $result1 = Treat_DB_ProxyOld::query($query1);

          $zero=Treat_DB_ProxyOld::mysql_result($result1,0,"zero");
          $thirty=Treat_DB_ProxyOld::mysql_result($result1,0,"thirty");
          $sixty=Treat_DB_ProxyOld::mysql_result($result1,0,"sixty");
          $ninety=Treat_DB_ProxyOld::mysql_result($result1,0,"ninety");
       }
       else{
          $query1="SELECT SUM(dashboard_aging.zero) AS zero,SUM(dashboard_aging.thirty) AS thirty,SUM(dashboard_aging.sixty) AS sixty,SUM(dashboard_aging.ninety) AS ninety FROM dashboard_aging,business$fromtables WHERE dashboard_aging.businessid = business.businessid AND ($compquery)";
          $result1 = Treat_DB_ProxyOld::query($query1);

          $zero=Treat_DB_ProxyOld::mysql_result($result1,0,"zero");
          $thirty=Treat_DB_ProxyOld::mysql_result($result1,0,"thirty");
          $sixty=Treat_DB_ProxyOld::mysql_result($result1,0,"sixty");
          $ninety=Treat_DB_ProxyOld::mysql_result($result1,0,"ninety");
       }
       

       if($zero<0){$s_zero=0;}
       else{$s_zero=$zero;}
       if($thirty<0){$s_thirty=0;}
       else{$s_thirty=$thirty;}
       if($sixty<0){$s_sixty=0;}
       else{$s_sixty=$sixty;}
       if($ninety<0){$s_ninety=0;}
       else{$s_ninety=$ninety;}

       if($viewtype==6){$mytarget="businvoice.php?cid=$companyid&bid=$viewbus&ageme=1";}
       else{$mytarget="businesstrack.php";}

       echo "<br><table width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td colspan=4><a href=$mytarget style=$style><font color=black onMouseOver=this.color='#FF9900' onMouseOut=this.color='black' title='View'>Aging</font></a>";

       //////////BY SALESMAN
       $query11="SELECT * FROM vend_salesman WHERE businessid = '$viewbus' ORDER BY sales_name DESC";
       $result11 = Treat_DB_ProxyOld::query($query11);
       $num11=Treat_DB_ProxyOld::mysql_numrows($result11);

       if($num11>0&&$viewtype==6){
          echo "&nbsp;&nbsp;<form action=businesstrack.php method=post style=\"margin:0;padding:0;display:inline;\"><select name=viewsalesman onChange='changePage(this.form.viewsalesman)' style=\"font:10px;\"><option value=businesstrack.php?viewbus=$viewbus&viewtype=6&viewsales=0>All Salesmen</option>";

          $num11--;
          while($num11>=0){
             $salesmanid=Treat_DB_ProxyOld::mysql_result($result11,$num11,"salesid");
             $salesmanname=Treat_DB_ProxyOld::mysql_result($result11,$num11,"sales_name");

             if($salesmanid==$viewsales){$showsel9="SELECTED";}
             else{$showsel9="";}

             echo "<option value=businesstrack.php?viewbus=$viewbus&viewtype=6&viewsales=$salesmanid&thismonth=$thismonth&thisyear=$thisyear $showsel9>$salesmanname</option>";
             $num11--;
          }
          echo "</select></form>";
       }

       echo "</td></tr><tr height=9><td></td></tr><tr><td colspan=4>";
       echo "<img src='piechart.php?pie_color=E8E8E8~FFFF00~FF9900~FF0000&data=$s_zero*$s_thirty*$s_sixty*$s_ninety&label=<30*30-60*60-90*>90'/ height=115 width=309> ";
       echo "</td></tr><tr height=24><td width=25%>";

       $zero=number_format($zero,2);
       $thirty=number_format($thirty,2);
       $sixty=number_format($sixty,2);
       $ninety=number_format($ninety,2);

       echo "<table><tr height=4><td width=10 bgcolor=#E8E8E8></td><td bgcolor=white><font size=1>$zero</td></tr></table> </td><td width=25%><table><tr height=4><td width=10 bgcolor=#FFFF00></td><td><font size=1>$thirty</td></tr></table> </td><td width=25%><table><tr height=4><td width=10 bgcolor=#FF9900></td><td><font size=1>$sixty</td></tr></table> </td><td width=25%><table><tr height=4><td width=10 bgcolor=#FF0000></td><td><font size=1>$ninety</td></tr></table>";

       echo "</td></tr></table>";

       /////////////////////////
       //////ACCOUNTS///////////
       /////////////////////////
       if($viewtype>=2){
          echo "<br><table width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td colspan=4><a href=dashboard_accounts.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&ytdstart=$ytdstart&v=all&sort=1 target='_blank' style=$style><font color=black onMouseOver=this.color='#FF9900' onMouseOut=this.color='black' title='Show All'>Top 10 Accounts (YTD)</font></a></td></tr><tr><td><iframe id='dashboardaccounts' src='dashboard_accounts.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&ytdstart=$ytdstart&sort=1' WIDTH=100% HEIGHT=175 frameborder=0></iframe></td></tr></table>";
       }

       ///////////////////////
       ////////ROUTES/////////
       ///////////////////////

       $query1="SELECT * FROM vend_locations,business$fromtables WHERE vend_locations.unitid = business.businessid AND ($compquery)";
       $result1 = Treat_DB_ProxyOld::query($query1);
       $num1=Treat_DB_ProxyOld::mysql_numrows($result1);

       if($num1>0&&$viewtype>=2){
          echo "<br><table width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td colspan=4><a href=dashboard_route.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&ytdstart=$ytdstart&sort=1&v=all target='_blank' style=$style><font color=black onMouseOver=this.color='#FF9900' onMouseOut=this.color='black' title='Show All'>Top 10 Routes (YTD)</font></a></td></tr><tr><td><iframe src='dashboard_route.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&ytdstart=$ytdstart&sort=1' WIDTH=100% HEIGHT=175 frameborder=0></iframe></td></tr></table>";

          echo "<br><table width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td colspan=4><a href=dashboard_route.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&ytdstart=$ytdstart&v=all target='_blank' style=$style><font color=black onMouseOver=this.color='#FF9900' onMouseOut=this.color='black' title='Show All'>Bottom 10 Routes (YTD)</font></a></td></tr><tr><td><iframe src='dashboard_route.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&ytdstart=$ytdstart' WIDTH=100% HEIGHT=175 frameborder=0></iframe></td></tr></table>";

          echo "<br><table width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td colspan=4><a href=dashboard_mach_sales.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&ytdstart=$ytdstart&sort=1&view=all target='_blank' style=$style><font color=black onMouseOver=this.color='#FF9900' onMouseOut=this.color='black' title='Show All'>Top 10 VGroups (YTD)</font></a></td></tr><tr><td><iframe src='dashboard_mach_sales.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&ytdstart=$ytdstart&sort=1' WIDTH=100% HEIGHT=175 frameborder=0></iframe></td></tr></table>";

          echo "<br><table width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td colspan=4><a href=dashboard_mach_service.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&ytdstart=$ytdstart&v=all&sort=1 target='_blank' style=$style><font color=black onMouseOver=this.color='#FF9900' onMouseOut=this.color='black' title='Show All'>Top 10 Complaints/Repairs (YTD)</font></a></td></tr><tr><td><iframe src='dashboard_mach_service.php?viewtype=$viewtype&cid=$companyid&viewbus=$viewbus&ytdstart=$ytdstart&sort=1' WIDTH=100% HEIGHT=175 frameborder=0></iframe></td></tr></table>";
       }


       /////////////////////////
       ////////////MEMOS////////
       /////////////////////////
       if($viewtype==6){
          echo "<br><table width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td colspan=4>Memos</td></tr><tr><td><iframe src='dashboard_memos.php?cid=$companyid&viewbus=$viewbus' WIDTH=100% HEIGHT=135 frameborder=0></iframe></td></tr></table>";
       }

       echo "</td></tr>";


    /////////////////////////
    /////END DASHBOARD///////
    /////////////////////////
}
elseif($viewtype>0&&$viewtype<7&&$viewbus!=""&&$viewsub==1){
   echo "<tr><td colspan=2><p><br><center><b><font size=4>Coming Soon...</font></b></center></td></tr>";
}
elseif($viewtype>0&&$viewtype<7&&$viewbus!=""&&$viewsub==2){
   echo "<tr><td colspan=2><p><br><center><b><font size=4>Coming Soon...</font></b></center></td></tr>";
}
elseif($viewtype>0&&$viewtype<7&&$viewbus!=""&&$viewsub==3){
   echo "<tr><td colspan=2><p><br><center><b><font size=4>Coming Soon...</font></b></center></td></tr>";
}
elseif($viewtype>0&&$viewtype<7&&$viewbus!=""&&$viewsub==4){
   echo "<tr><td colspan=2><p><br><center><b><font size=4>Coming Soon...</font></b></center></td></tr>";
}
elseif($viewtype>0&&$viewtype<7&&$viewbus!=""&&$viewsub==5){
   echo "<tr><td colspan=2><p><br><center><b><font size=4>Coming Soon...</font></b></center></td></tr>";
}
elseif($viewtype>0&&$viewtype<7&&$viewbus!=""&&$viewsub==6){
   echo "<tr><td colspan=2><p><br><center><b><font size=4>Coming Soon...</font></b></center></td></tr>";
}
    echo "</table></center>";

}
echo "</body></html>";
//mysql_close();

}
?>
