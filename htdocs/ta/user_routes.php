<?php

/////////////////////////////////////////REVISION HISTORY////////////////////////////////////////////
//2/21/07 - Added edit user by business -SM
/////////////////////////////////////////REVISION HISTORY////////////////////////////////////////////


if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
require_once( DOC_ROOT . '/bootstrap.php' );

$style = "text-decoration:none";

$user = Treat_Controller_Abstract::getPostVariable( 'username', '' );
$pass = Treat_Controller_Abstract::getPostVariable( 'password', '' );
$view = Treat_Controller_Abstract::getPostVariable( 'view', '' );
if ( $user == '' && $pass == '' ) {
	$user = Treat_Controller_Abstract::getSessionCookieVariable( 'usercook' );
	$pass = Treat_Controller_Abstract::getSessionCookieVariable( 'passcook' );
}

$compcook = Treat_Controller_Abstract::getSessionCookieVariable( 'compcook' );
$edituser = Treat_Controller_Abstract::getGetVariable( 'edituser', '' );
$editsecurity = Treat_Controller_Abstract::getGetVariable( 'editsecurity', '' );
$editlocation = Treat_Controller_Abstract::getGetVariable( 'editlocation', '' );
$editcommission = Treat_Controller_Abstract::getGetVariable( 'editcommission', '' );

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query( $query );
$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );
//mysql_close();

$compid = @mysql_result( $result, 0, 'companyid' );

if ( $num != 1 || $compcook != $compid || $user == '' || $pass == '' )
{
	echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
	$security_level = @mysql_result( $result, 0, 'security_level' );
	$mysecurity = @mysql_result( $result, 0, 'security' );
	$security_level2 = $security_level;
	if ( $security_level < 8 ) {
		$location = '/ta/businesstrack.php';
		header( 'Location: ' . $location );
		exit();
	}
?>
<html>
	<head>
		<title>Treat America :: user_routes.php </title>
<script language="JavaScript" type="text/javascript">
function changePage(newLoc)
{
	nextPage = newLoc.options[newLoc.selectedIndex].value

	if (nextPage != "")
	{
		document.location.href = nextPage
	}
}
</script>

<script language="javascript" type="text/javascript"><!--
function delconfirm(){return confirm('ARE YOU SURE YOU WANT TO DELETE THIS ROUTE?');}
// --></script>
		<link type="text/css" rel="stylesheet" href="/assets/css/ta/user_routes.css" />
	</head>
	<body>
		<center>
			<table cellspacing="0" cellpadding="0" border="0" width="90%">
				<tr>
					<td colspan="3">
						<a href="/ta/businesstrack.php">
							<img src="/assets/images/logo.jpg" border="0" height="43" width="205" alt="logo" />
						</a>
						<p></p>
					</td>
				</tr>
<?php
	$companyid = @mysql_result( $result, 0, 'companyid' );
	$businessid = @mysql_result( $result, 0, 'businessid' );
	$security_level = @mysql_result( $result, 0, 'security_level' );

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM company WHERE companyid = '$companyid'";
	$result = Treat_DB_ProxyOld::query( $query );
	//mysql_close();

	$companyname = @mysql_result( $result, 0, 'companyname' );

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM security WHERE securityid = '$mysecurity'";
	$result = Treat_DB_ProxyOld::query( $query );
	//mysql_close();

	$sec_routes = @mysql_result( $result, 0, 'routes' );
	$sec_securities = @mysql_result( $result, 0, 'securities' );

	//echo "<tr bgcolor=black><td height=1 colspan=2></td></tr>";
?>
				<tr bgcolor="#ccccff">
					<td colspan="3">
						<font size="4">
							<b>
								Vending Management - <?php
									echo $companyname;
								?> 
							</b>
						</font>
					</td>
				</tr>
				<tr bgcolor="black"><td height="1" colspan="3"></td></tr>
<?php
	
	/////////////////////VENDING/////////////////////////////////
	if ( $sec_routes == 1 ) {
		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query = "SELECT * FROM login_route WHERE companyid = '$companyid' AND is_deleted = '0' ORDER BY businessid,locationid,route DESC";
		$result = Treat_DB_ProxyOld::query( $query );
		$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );
		//mysql_close();
?>
				<tr bgcolor="#e8e7e7">
					<td colspan="3">
						<p>
							<br/>
							<b><u>
								ROUTE MANAGEMENT
							</u></b>
						</p>
					</td>
				</tr>
				<tr bgcolor="#e8e7e7">
					<td colspan="3">
						<ul>
							<li>
								<form action="/ta/user_routes.php" method="post">
									Route:
									<select name="user" onChange="changePage(this.form.user)">
										<option value="/ta/user_routes.php?cid=<?php
											echo $companyid;
										?>">============Please Choose============</option>
<?php
		$lastlocationid = -1;
		$lastuserbus = -1;
		$num--;
		while ( $num >= 0 ) {

			$login_routeid = @mysql_result( $result, $num, 'login_routeid' );
			$lastname = @mysql_result( $result, $num, 'lastname' );
			$firstname = @mysql_result( $result, $num, 'firstname' );
			$id1 = @mysql_result( $result, $num, 'username' );
			$id2 = @mysql_result( $result, $num, 'password' );
			$route = @mysql_result( $result, $num, 'route' );
			$userbus = @mysql_result( $result, $num, 'businessid' );
			$locationid = @mysql_result( $result, $num, 'locationid' );
			$isactive = @mysql_result( $result, $num, 'active' );
			$order_by_machine = @mysql_result( $result, $num, 'order_by_machine' );

			if ( $login_routeid == $edituser ) {
				$showsel = 'SELECTED';
			}
			else {
				$showsel = '';
			}


			if ( $isactive == 1 ) {
				$showinactive = "(Inactive)";
			}
			else {
				$showinactive = '';
			}

			if ( $lastuserbus != $userbus ) {
				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( "Unable to select database");
				$query5 = "SELECT businessname FROM business WHERE businessid = '$userbus'";
				$result5 = Treat_DB_ProxyOld::query( $query5 );
				//mysql_close();
				$busname = @mysql_result( $result5, 0, 'businessname' );
?>
										<option value="/ta/user_routes.php?cid=<?php
											echo $companyid;
										?>">~~~~~~~~~~~~~~~~~~~~<?php
											echo $busname;
										?></option>
<?php
			}
			if ( $lastlocationid != $locationid ) {
				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( "Unable to select database");
				$query5 = "SELECT location_name FROM vend_locations WHERE locationid = '$locationid'";
				$result5 = Treat_DB_ProxyOld::query( $query5 );
				//mysql_close();
				$locname = @mysql_result( $result5, 0, 'location_name' );
?>
										<option value="/ta/user_routes.php?cid=<?php
											echo $companyid;
										?>">====================<?php
											echo $locname;
										?></option>
<?php
			}
?>
										<option value="/ta/user_routes.php?edituser=<?php
											echo $login_routeid;
										?>&amp;cid=<?php
											echo $companyid;
										?>" <?php
											echo $showsel;
										?>>Route# <?php
											echo $route, ', ', $firstname, ' ', $lastname, ' ', $showinactive;
										?> </option>
<?php
			$lastuserbus = $userbus;
			$lastlocationid = $locationid;
			$num--;
		}
?>
									</select>
								</form>
							</li>
						</ul>
					</td>
				</tr>
<?php
		if ( $edituser != '' ) {
			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query = "SELECT * FROM login_route WHERE login_routeid = '$edituser'";
			$result = Treat_DB_ProxyOld::query( $query );
			//mysql_close();

			$login_routeid = @mysql_result( $result, 0, 'login_routeid' );
			$lastname = @mysql_result( $result, 0, 'lastname' );
			$firstname = @mysql_result( $result, 0, 'firstname' );
			$id1 = @mysql_result( $result, 0, 'username' );
			$id2 = @mysql_result( $result, 0, 'password' );
			$route = @mysql_result( $result, 0, 'route' );
			$truck = @mysql_result( $result, 0, 'truck' );
			$userbus = @mysql_result( $result, 0, 'businessid' );
			$locationid = @mysql_result( $result, 0, 'locationid' );
			$supervisor = @mysql_result( $result, 0, 'supervisor' );
			$sec_level = @mysql_result( $result, 0, 'sec_level' );
			$active = @mysql_result( $result, 0, 'active' );
			$commissionid = @mysql_result( $result, 0, 'commissionid' );
			$sourceid = @mysql_result( $result, 0, 'sourceid' );
			$order_by_machine = @mysql_result( $result, 0, 'order_by_machine' );

			if ( $active == 1 ) {
				$showactive = 'CHECKED';
			}
			else {
				$showactive = '';
			}
?>
				<tr bgcolor="#e8e7e7">
					<td colspan="3">
						<ul>
							<li style="list-style-type: none;">
								<form action="/ta/edit_venduser.php" method="post">
									<input type="hidden" name="login_routeid" value="<?php echo $login_routeid; ?>" />
									<input type="hidden" name="edit" value="1" />
									<input type="hidden" name="username" value="<?php echo $user; ?>" />
									<input type="hidden" name="password" value="<?php echo $pass; ?>" />
									<table bgcolor="#ffff99" style="border:1px solid black;">
										<tr>
											<td align="right">
												<b>Route#</b>:
											</td>
											<td>
												<input type="text" size="5" name="route" value="<?php echo $route; ?>" />
												<font size="2">
													<i>
														*Should be at least 3 digits long (ie 001) for route 1.
													</i>
												</font>
											</td>
										</tr>
										<tr>
											<td align="right">
												Truck#:
											</td>
											<td>
												<input type="text" size="5" name="truck" value="<?php echo $truck; ?>" />
											</td>
										</tr>
										<tr>
											<td align="right">
												Last Name:
											</td>
											<td>
												<input type="text" size="30" name="lastname" value="<?php echo $lastname; ?>" />
											</td>
										</tr>
										<tr>
											<td align="right">
												First Name:
											</td>
											<td>
												<input type="text" size="30" name="firstname" value="<?php echo $firstname; ?>" />
											</td>
										</tr>
										<tr>
											<td align="right">
												ID1:
											</td>
											<td>
												<input type="text" size="10" name="id1" value="<?php echo $id1; ?>" />
											</td>
										</tr>
										<tr>
											<td align="right">
												ID2:
											</td>
											<td>
												<input type="text" size="10" name="id2" value="<?php echo $id2; ?>" />
											</td>
										</tr>
										<tr>
											<td align="right">
												Location:
											</td>
											<td>
												<select name="newlocid">
<?php
			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query = "SELECT * FROM vend_locations ORDER BY businessid, location_name DESC";
			$result = Treat_DB_ProxyOld::query( $query );
			$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );
			//mysql_close();

			$num--;
			while ( $num >= 0 ) {
				$newlocid = @mysql_result( $result, $num, 'locationid' );
				$newlocname = @mysql_result( $result, $num, 'location_name' );
				$busid = @mysql_result( $result, $num, 'businessid' );
				if ( $newlocid == $locationid ) {
					$showsel2 = 'SELECTED';
				}
				else {
					$showsel2 = '';
				}


				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( "Unable to select database");
				$query4 = "SELECT businessname FROM business WHERE businessid = '$busid'";
				$result4 = Treat_DB_ProxyOld::query( $query4 );
				//mysql_close();

				$busname = @mysql_result( $result4, 0, 'businessname' );
?>
													<option value="<?php
														echo $newlocid;
													?>" <?php
														echo $showsel2;
													?>><?php
														echo $busname;
													?> - <?php
														echo $newlocname;
													?></option>
<?php
				$num--;
			}
?>
												</select>
											</td>
										</tr>
										<tr>
											<td align="right">
												Supervisor:
											</td>
											<td>
												<select name="supervisor">
<?php
			//SUPERVISOR
			$query = "SELECT vend_supervisor.supervisorid,vend_supervisor.supervisor_name FROM vend_supervisor,business WHERE vend_supervisor.businessid = business.businessid AND business.companyid = '$companyid' ORDER BY supervisor_name";
			$result = Treat_DB_ProxyOld::query( $query );
?>
													<option value="0"></option>
<?php
			while ( $r = mysql_fetch_array( $result ) ) {
				$supervisorid = $r['supervisorid'];
				$supervisor_name = $r['supervisor_name'];
				if ( $supervisor == $supervisorid ) {
					$showsel2 = 'SELECTED';
				}
				else {
					$showsel2 = '';
				}
?>
													<option value="<?php
														echo $supervisorid;
													?>" <?php
														echo $showsel2;
													?>><?php
														echo $supervisor_name;
													?></option>
<?php
				$num--;
			}
?>
												</select>
											</td>
										</tr>
<?php
			if ( $sec_level == 1 ) {
				$sec1 = "SELECTED";
			}
			elseif ( $sec_level == 2 ) {
				$sec2 = "SELECTED";
			}
			elseif ( $sec_level == 3 ) {
				$sec3 = "SELECTED";
			}
?>
										<tr>
											<td align="right">
												Security:
											</td>
											<td>
												<select name="sec_level">
													<option value="1" <?php
														echo $sec1;
													?>>1 - Driver</option>
													<option value="2" <?php
														echo $sec2;
													?>>2 - Route Mgr</option>
													<option value="3" <?php
														echo $sec3;
													?>>3 - Admin</option>
												</select>
											</td>
										</tr>
										<tr>
											<td align="right">
												Commission:
											</td>
											<td>
												<select name="newcommissionid">
													<option value="0"></option>
<?php
			$query = "SELECT business.companyid FROM vend_locations,business WHERE vend_locations.locationid = '$locationid' AND vend_locations.unitid = business.businessid";
			$result = Treat_DB_ProxyOld::query( $query );

			$mycomp = @mysql_result( $result, 0, 'business.companyid' );

			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query = "SELECT * FROM vend_commission WHERE companyid = '$mycomp'";
			$result = Treat_DB_ProxyOld::query( $query );
			$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );
			//mysql_close();

			$num--;
			while ( $num >= 0 ) {
				$newcommissionid = @mysql_result( $result, $num, 'commissionid' );
				$commission_name = @mysql_result( $result, $num, 'name' );
				if ( $commissionid == $newcommissionid ) {
					$showsel2 = 'SELECTED';
				}
				else {
					$showsel2 = '';
				}
?>
													<option value="<?php
														echo $newcommissionid;
													?>" <?php
														echo $showsel2;
													?>><?php
														echo $commission_name;
													?></option>
<?php
				$num--;
			}
?>
												</select>
											</td>
										</tr>
<?php
			//////commission source
?>
										<tr>
											<td align="right">
												Commission Source:
											</td>
											<td>
												<select name="newsourceid">
													<option value="0"></option>
<?php
			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query = "SELECT * FROM vend_commission_source ORDER BY source_name DESC";
			$result = Treat_DB_ProxyOld::query( $query );
			$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );
			//mysql_close();

			$num--;
			while ( $num >= 0 ) {
				$newsourceid = @mysql_result( $result, $num, 'sourceid' );
				$source_name = @mysql_result( $result, $num, 'source_name' );
				if ( $sourceid == $newsourceid ) {
					$showsel2 = 'SELECTED';
				}
				else {
					$showsel2 = '';
				}
?>
													<option value="<?php
														echo $newsourceid;
													?>" <?php
														echo $showsel2;
													?>><?php
														echo $source_name;
													?></option>
<?php
				$num--;
			}
?>
												</select>
											</td>
										</tr>
<?php
			//////////////ORDER BY ROUTE/MACHINE
			if ( $order_by_machine == 1 ) {
				$showsel2 = 'SELECTED';
			}
			else {
				$showsel2 = '';
			}
?>
										<tr>
											<td align="right">
												Order:
											</td>
											<td>
												<select name="order_by_machine">
													<option value="0">By Route</option>
													<option value="1" <?php
														echo $showsel2;
													?>>By Machine</option>
												</select>
											</td>
										</tr>
										<tr>
											<td align="right">
												Inactive:
											</td>
											<td>
												<input type="checkbox" name="active" value="1" <?php
													echo $showactive;
												?> />
											</td>
										</tr>
										<tr>
											<td></td>
											<td>
												<input type="submit" value="Save" />
											</td>
										</tr>
									</table>
								</form>
							</li>
						</ul>
					</td>
				</tr>
<?php
		}

		else {
?>
				<tr bgcolor="#e8e7e7">
					<td colspan="3">
						<ul>
							<li style="list-style-type: none;">
								<form action="/ta/edit_venduser.php" method="post">
									<input type="hidden" name="username" value="<?php echo $user; ?>" />
									<input type="hidden" name="password" value="<?php echo $pass; ?>" />
									<table bgcolor="#e8e7e7">
										<tr>
											<td align="right">
												<b>Route#</b>:
											</td>
											<td>
												<input type="text" size="5" name="route" />
												<font size="2">
													<i>
														*Should be at least 3 digits long (ie 001) for route 1.
													</i>
												</font>
											</td>
										</tr>
										<tr>
											<td align="right">
												Truck#:
											</td>
											<td>
												<input type="text" size="5" name="truck" />
											</td>
										</tr>
										<tr>
											<td align="right">
												Last Name:
											</td>
											<td>
												<input type="text" size="30" name="lastname" />
											</td>
										</tr>
										<tr>
											<td align="right">
												First Name:
											</td>
											<td>
												<input type="text" size="30" name="firstname" />
											</td>
										</tr>
										<tr>
											<td align="right">
												ID1:
											</td>
											<td>
												<input type="text" size="10" name="id1" />
											</td>
										</tr>
										<tr>
											<td align="right">
												ID2:
											</td>
											<td>
												<input type="text" size="10" name="id2" />
											</td>
										</tr>
										<tr>
											<td align="right">
												Location:
											</td>
											<td>
												<select name="newlocid">
<?php
			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query = "SELECT * FROM vend_locations ORDER BY businessid, location_name DESC";
			$result = Treat_DB_ProxyOld::query( $query );
			$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );
			//mysql_close();

			$num--;
			while ( $num >= 0 ) {
				$newlocid = @mysql_result( $result, $num, 'locationid' );
				$newlocname = @mysql_result( $result, $num, 'location_name' );
				$busid = @mysql_result( $result, $num, 'businessid' );

				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( "Unable to select database");
				$query4 = "SELECT businessname FROM business WHERE businessid = '$busid'";
				$result4 = Treat_DB_ProxyOld::query( $query4 );
				//mysql_close();

				$busname = @mysql_result( $result4, 0, 'businessname' );
?>
													<option value="<?php
														echo $newlocid;
													?>"><?php
														echo $busname;
													?> - <?php
														echo $newlocname;
													?></option>
<?php
				$num--;
			}
?>
												</select>
											</td>
										</tr>
										<tr>
											<td align="right">
												Supervisor:
											</td>
											<td>
												<select name="supervisor">
<?php
			//SUPERVISOR
			$query = "SELECT vend_supervisor.supervisorid,vend_supervisor.supervisor_name FROM vend_supervisor,business WHERE vend_supervisor.businessid = business.businessid AND business.companyid = '$companyid' ORDER BY supervisor_name";
			$result = Treat_DB_ProxyOld::query( $query );
?>
													<option value="0"></option>
<?php
			while ( $r = mysql_fetch_array( $result ) ) {
				$supervisorid = $r["supervisorid"];
				$supervisor_name = $r["supervisor_name"];
				if ( $supervisor == $supervisorid ) {
					$showsel2 = 'SELECTED';
				}
				else {
					$showsel2 = '';
				}
?>
													<option value="<?php
														echo $supervisorid;
													?>" <?php
														echo $showsel2;
													?>><?php
														echo $supervisor_name;
													?></option>
<?php
				$num--;
			}
?>
												</select>
											</td>
										</tr>
										<tr>
											<td align="right">
												Security:
											</td>
											<td>
												<select name="sec_level">
													<option value="1">1 - Driver</option>
													<option value="2">2 - Route Mgr</option>
													<option value="3">3 - Admin</option>
												</select>
											</td>
										</tr>
<?php
			/////commission type
?>
										<tr>
											<td align="right">
												Commission:
											</td>
											<td>
												<select name="newcommissionid">
													<option value="0"></option>
<?php
			$query = "SELECT business.companyid FROM vend_locations,business WHERE vend_locations.locationid = '$locationid' AND vend_locations.unitid = business.businessid";
			$result = Treat_DB_ProxyOld::query( $query );

			$mycomp = @mysql_result( $result, 0, 'business.companyid' );

			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query = "SELECT * FROM vend_commission WHERE companyid = '$mycomp'";
			$result = Treat_DB_ProxyOld::query( $query );
			$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );
			//mysql_close();

			$num--;
			while ( $num >= 0 ) {
				$newcommissionid = @mysql_result( $result, $num, 'commissionid' );
				$commission_name = @mysql_result( $result, $num, 'name' );
?>
													<option value="<?php
														echo $newcommissionid;
													?>" <?php
														echo $showsel2;
													?>><?php
														echo $commission_name;
													?></option>
<?php
				$num--;
			}
?>
												</select>
											</td>
										</tr>
<?php
			//////commission source
?>
										<tr>
											<td align="right">
												Commission Source:
											</td>
											<td>
												<select name="newsourceid">
													<option value="0"></option>
<?php
			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query = "SELECT * FROM vend_commission_source ORDER BY source_name DESC";
			$result = Treat_DB_ProxyOld::query( $query );
			$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );
			//mysql_close();

			$num--;
			while ( $num >= 0 ) {
				$newsourceid = @mysql_result( $result, $num, 'sourceid' );
				$source_name = @mysql_result( $result, $num, 'source_name' );
				if ( $sourceid == $newsourceid ) {
					$showsel2 = 'SELECTED';
				}
				else {
					$showsel2 = '';
				}
?>
													<option value="<?php
														echo $newsourceid;
													?>" <?php
														echo $showsel2;
													?>><?php
														echo $source_name;
													?></option>
<?php
				$num--;
			}
?>
												</select>
											</td>
										</tr>
										<tr>
											<td align="right">
												Order:
											</td>
											<td>
												<select name="order_by_machine">
													<option value="0">By Route</option>
													<option value="1" <?php
														echo $showsel2;
													?>>By Machine</option>
												</select>
											</td>
										</tr>
										<tr>
											<td align="right">
												Inactive:
											</td>
											<td>
												<input type="checkbox" name="active" value="1" <?php
													echo $showactive;
												?> />
											</td>
										</tr>
										<tr>
											<td></td>
											<td>
												<input type="submit" value="Add" />
											</td>
										</tr>
									</table>
								</form>
							</li>
						</ul>
					</td>
				</tr>
<?php
		}

		if ( $edituser != '' ) {
?>
				<tr bgcolor="#e8e7e7">
					<td colspan="3">
						<ul>
							<li>
								<form action="/ta/edit_venduser.php" method="post" onclick="return delconfirm()">
									<input type="hidden" name="deleteuser" value="<?php echo $edituser; ?>" />
									<input type="hidden" name="username" value="<?php echo $user; ?>" />
									<input type="hidden" name="password" value="<?php echo $pass; ?>" />
						<font color="red">
									<input type="submit" value="DELETE ROUTE" />
						</font>
								</form>
							</li>
						</ul>
					</td>
				</tr>
<?php
		}


		//////VENDING LOCATIONS
?>
				<tr bgcolor="black"><td colspan="3" height="1"></td></tr>
<?php
		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query = "SELECT * FROM vend_locations ORDER BY location_name DESC";
		$result = Treat_DB_ProxyOld::query( $query );
		$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );
		//mysql_close();
?>
				<tr bgcolor="#e8e7e7">
					<td colspan="3">
						<p></p>
						<br>
						<b>
							<u>
								<a name="vendloc">
									LOCATION MANAGEMENT
								</a>
							</u>
						</b>
					</td>
				</tr>
				<tr bgcolor="#e8e7e7">
					<td colspan="3">
						<ul>
							<li>
								<form action="/ta/user_routes.php" method="post">
									Location:
									<select name="location" onChange="changePage(this.form.location)">
										<option value="/ta/user_routes.php?cid=<?php
											echo $companyid;
										?>">============Please Choose============</option>
<?php
		$num--;
		while ( $num >= 0 ) {

			$locationid = @mysql_result( $result, $num, 'locationid' );
			$location_name = @mysql_result( $result, $num, 'location_name' );

			if ( $locationid == $editlocation ) {
				$showsel = "SELECTED";
				$v_location_name = @mysql_result( $result, $num, 'location_name' );
				$v_businessid = @mysql_result( $result, $num, 'businessid' );
				$v_accountid = @mysql_result( $result, $num, 'accountid' );
				$v_unitid = @mysql_result( $result, $num, 'unitid' );
				$v_op_unitid = @mysql_result( $result, $num, 'op_unitid' );
				$v_menu_typeid = @mysql_result( $result, $num, 'menu_typeid' );
			}
			else {
				$showsel = '';
			}
?>
										<option value="/ta/user_routes.php?editlocation=<?php
											echo $locationid;
										?>&amp;cid=<?php
											echo $companyid;
										?>#vendloc" <?php
											echo $showsel;
										?>><?php
											echo $location_name;
										?></option>
<?php
			$num--;
		}
?>
									</select>
								</form>
							</li>
						</ul>
					</td>
				</tr>
<?php
		if ( $editlocation > 0 ) {
?>
				<tr bgcolor="#e8e7e7">
					<td align="center" colspan="3">
						<form action="/ta/savelocation.php" method="post">
							<input type="hidden" name="locationid" value="<?php echo $editlocation; ?>" />
							<input type="hidden" name="edit" value="1" />
							<input type="hidden" name="username" value="<?php echo $user; ?>" />
							<input type="hidden" name="password" value="<?php echo $pass; ?>" />
							<table>
								<tr bgcolor="#e8e7e7">
									<td align="right">
											Location Name:
									</td>
									<td>
										<input type="text" size="25" name="location_name" value="<?php echo $v_location_name; ?>" />
									</td>
								</tr>
								<tr bgcolor="#e8e7e7">
									<td align="right">
										Ordering Business:
									</td>
									<td>
										<select name="v_businessid">
<?php
			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query4 = "SELECT * FROM business WHERE companyid != '2' ORDER BY businessname DESC";
			$result4 = Treat_DB_ProxyOld::query( $query4 );
			$num4 = Treat_DB_ProxyOld::mysql_numrows( $result4, 0 );
			//mysql_close();

			$num4--;
			while ( $num4 >= 0 ) {
				$businessname = @mysql_result( $result4, $num4, 'businessname' );
				$busid = @mysql_result( $result4, $num4, 'businessid' );

				if ( $v_businessid == $busid ) {
					$showsel4 = "SELECTED";
				}
				else {
					$showsel4 = '';
				}
?>
											<option value="<?php
												echo $busid;
											?>" <?php
												echo $showsel4;
											?>><?php
												echo $businessname;
											?></option>
<?php
				$num4--;
			}
?>
										</select>
									</td>
								</tr>
								<tr bgcolor="#e8e7e7">
									<td align="right">
										AR Account:
									</td>
									<td>
										<select name="v_accountid">
<?php
			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query4 = "SELECT * FROM accounts WHERE companyid != '2' ORDER BY accountnum DESC";
			$result4 = Treat_DB_ProxyOld::query( $query4 );
			$num4 = Treat_DB_ProxyOld::mysql_numrows( $result4, 0 );
			//mysql_close();

			//$num4--;
			while ( $num4 >= 0 ) {
				$accountname = @mysql_result( $result4, $num4, 'name' );
				$accountid = @mysql_result( $result4, $num4, 'accountid' );
				$accountnum = @mysql_result( $result4, $num4, 'accountnum' );

				if ( $v_accountid == $accountid ) {
					$showsel4 = "SELECTED";
				}
				else {
					$showsel4 = '';
				}
?>
											<option value="<?php
												echo $accountid;
											?>" <?php
												echo $showsel4;
											?>><?php
												echo $accountnum;
											?> - <?php
												echo $accountname;
											?></option>
<?php
				$num4--;
			}
?>
										</select>
									</td>
								</tr>
								<tr bgcolor="#e8e7e7">
									<td align="right">
										Money Room/Payroll Business:
									</td>
									<td>
										<select name="v_unitid">
<?php
			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query4 = "SELECT * FROM business WHERE companyid != '2' ORDER BY businessname DESC";
			$result4 = Treat_DB_ProxyOld::query( $query4 );
			$num4 = Treat_DB_ProxyOld::mysql_numrows( $result4, 0 );
			//mysql_close();

			$num4--;
			while ( $num4 >= 0 ) {
				$businessname = @mysql_result( $result4, $num4, 'businessname' );
				$busid = @mysql_result( $result4, $num4, 'businessid' );

				if ( $v_unitid == $busid ) {
					$showsel4 = "SELECTED";
				}
				else {
					$showsel4 = '';
				}
?>
											<option value="<?php
												echo $busid;
											?>" <?php
												echo $showsel4;
											?>><?php
												echo $businessname;
											?></option>
<?php
				$num4--;
			}
?>
										</select>
									</td>
								</tr>
<?php
			////Operations Business
?>
								<tr bgcolor="#e8e7e7">
									<td align="right">
										Operations Business:
									</td>
									<td>
										<select name="v_op_unitid">
<?php
			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query4 = "SELECT * FROM business WHERE companyid != '2' ORDER BY businessname DESC";
			$result4 = Treat_DB_ProxyOld::query( $query4 );
			$num4 = Treat_DB_ProxyOld::mysql_numrows( $result4, 0 );
			//mysql_close();

			$num4--;
			while ( $num4 >= 0 ) {
				$businessname = @mysql_result( $result4, $num4, 'businessname' );
				$busid = @mysql_result( $result4, $num4, 'businessid' );

				if ( $v_op_unitid == $busid ) {
					$showsel4 = 'SELECTED';
				}
				else {
					$showsel4 = '';
				}
?>
											<option value="<?php echo $busid;
											?>" <?php
												echo $showsel4;
											?>><?php
												echo $businessname;
											?></option>
<?php
				$num4--;
			}
?>
										</select>
									</td>
								</tr>
<?php
			///MENU (6/2/10)
?>
								<tr bgcolor="#e8e7e7">
									<td align="right">
										Menu:
									</td>
									<td>
										<select name="v_menu_typeid">
<?php
			$query4 = "SELECT * FROM menu_type WHERE companyid = '$companyid' ORDER BY menu_typename DESC";
			$result4 = Treat_DB_ProxyOld::query( $query4 );
			$num4 = Treat_DB_ProxyOld::mysql_numrows( $result4, 0 );

			while ( $num4 >= 0 ) {
				$menu_typename = @mysql_result( $result4, $num4, 'menu_typename' );
				$menu_typeid = @mysql_result( $result4, $num4, 'menu_typeid' );

				if ( $v_menu_typeid == $menu_typeid ) {
					$showsel4 = 'SELECTED';
				}
				else {
					$showsel4 = '';
				}
?>
											<option value="<?php
												echo $menu_typeid;
											?>" <?php
												echo $showsel4;
											?>><?php
												echo $menu_typename;
											?></option>
<?php
				$num4--;
			}
?>
										</select>
									</td>
								</tr>
								<tr bgcolor="#e8e7e7">
									<td align="right"></td>
									<td>
										<input type="submit" value="Save" />
									</td>
								</tr>
							</table>
						</form>
					</td>
				</tr>
<?php
		}
?>
				<tr bgcolor="#e8e7e7">
					<td colspan="3">
						<ul>
							<li>
								<form action="/ta/savelocation.php" method="post">
									<input type="hidden" name="username" value="<?php echo $user; ?>" />
									<input type="hidden" name="password" value="<?php echo $pass; ?>" />
									New Location:
									<input type="text" size="20" name="location_name" />
									<input type="submit" value="Add" />
								</form>
							</li>
						</ul>
					</td>
				</tr>
				<tr bgcolor="black"><td colspan="3" height="1"></td></tr>
<?php
		//////////////////VENDING COMMISSIONS

		$query = "SELECT * FROM vend_commission WHERE companyid = '$companyid'";
		$result = Treat_DB_ProxyOld::query( $query );
		$num = Treat_DB_ProxyOld::mysql_numrows( $result, 0 );
?>
				<tr bgcolor="#e8e7e7">
					<td colspan="3">
						<p></p>
						<br/>
						<b>
							<u>
								<a name="vendcom">
									COMMISSION MANAGEMENT
								</a>
							</u>
						</b>
					</td>
				</tr>
				<tr bgcolor="#e8e7e7">
					<td colspan="3">
						<ul>
							<li>
<?php
		$commission_list = Treat_Model_Vending_Commission::getByCompanyId( $companyid, $class = 'Treat_ArrayObject', $return_only_one = false );
?>
								<form action="/ta/user_routes.php" method="post">
									Commission Type:
									<select name="commission" onChange="changePage(this.form.commission)">
										<option value="/ta/user_routes.php?cid=<?php
											echo $companyid;
										?>">============Please Choose============</option>
<?php
		# this is also used farther below
		$queryset = array(
			'cid' => $companyid,
			'editcommission' => null,
			'delid' => null,
			'commissionid' => null,
		);
		foreach ( $commission_list as $commission_item ) {
			$temp_queryset = array_merge( $queryset, array( 'editcommission' => $commission_item->commissionid, ) );
?>
										<option value="/ta/user_routes.php?<?php
											echo http_build_query( $temp_queryset, null, '&amp;' );
										?>#vendcom"<?php
											if ( $commission_item->commissionid == $editcommission ) {
												echo ' selected="selected"';
											}
										?>><?php
											echo $commission_item->name;
										?></option>
<?php
		}
?>
									</select>
								</form>
							</li>
						</ul>
					</td>
				</tr>
<?php
		if ( ( $editcommission = intval( $editcommission ) ) ) {
			///////////COMMISSION LINKING
			$edit_commission_item = Treat_Model_Vending_Commission::get( $editcommission, true );
?>
				<tr bgcolor="#e8e7e7">
					<td colspan="3">
						<form action="/ta/route/save_commission_settings" method="post" class="commission_settings">
							<input type="hidden" name="company_id" value="<?php
								echo $edit_commission_item->companyid;
							?>" />
							<table style="border:1px solid black;" width="70%" bgcolor="#ffff99">
								<tr>
									<td colspan="12">
										<b>
											Base/Payroll Codes for
											<input type="text" name="commission_name" value="<?php
												echo $edit_commission_item->name;
											?>" size="30" />
										</b>
									</td>
								</tr>
								<tr>
									<td colspan="7">
										<b>
											Link to:
											<label>
												<input type="radio" name="route_emp" value="0" <?php
													if ( 1 != $edit_commission_item->route_emp ) {
														echo ' checked="checked"';
													}
												?> />Route
											</label>
											<label>
												<input type="radio" name="route_emp" value="1" <?php
													if ( 1 == $edit_commission_item->route_emp ) {
														echo ' checked="checked"';
													}
												?> />Employee
											</label>
										</b>
									</td>
									<td colspan="5" align="right">
<?php
			$base_commission_options = Treat_Model_Vending_Commission::getBaseCommissionOptions();
?>
										<select name="base_commission">
<?php
			foreach ( $base_commission_options as $option ) {
?>
											<option value="<?php
												echo $option->id;
											?>"<?php
												if ( $option->id == $edit_commission_item->base_commission ) {
													echo ' selected="selected"';
												}
											?>><?php
												echo $option->label
											?></option>
<?php
			}
?>
										</select>
									</td>
								</tr>
<?php
			$payroll_list = Treat_Model_Payroll_Code_Commission::getByCompanyId( $companyid );
			$link_list = Treat_Model_Payroll_Code_Commission::getLinkList();
			ksort( $link_list );
?>
								<tr>
									<th nowrap="nowrap">Fixed Amount*</th>
									<th nowrap="nowrap">Base $'s</th>
									<th nowrap="nowrap">Holiday $'s</th>
<?php
			foreach ( $link_list as $link_item ) {
?>
									<th nowrap="nowrap"><?php echo $link_item->label; ?></th>
<?php
			}
?>
									<td></td>
								</tr>
								<tr>
									<td>
										<input type="checkbox" name="fixed_amount" value="1" <?php
											if ( 1 == $edit_commission_item->fixed_amount ) {
												echo ' checked="checked"';
											}
										?> />
									</td>
									<td>
										<input type="hidden" name="commissionid" value="<?php echo $edit_commission_item->commissionid; ?>" />
										<input type="text" name="base" value="<?php echo $edit_commission_item->base; ?>" size="5" />
									</td>
									<td>
										<input type="text" name="holiday_pay" value="<?php echo $edit_commission_item->holiday_pay; ?>" size="5" />
									</td>
<?php
			foreach ( $link_list as $link_item ) {
?>
									
									<td>
										<select name="<?php echo $link_item->form_name; ?>">
											<option value="0"></option>
<?php
				foreach ( $payroll_list as $payroll_item ) {
?>
											<option value="<?php
												echo $payroll_item->codeid;
											?>"<?php
												if ( $payroll_item->codeid == $edit_commission_item->{$link_item->column_name} ) {
													echo ' selected="selected"';
												}
											?>><?php
												echo $payroll_item->code_name;
											?></option>";
<?php
				}
?>
										</select>
									</td>
									
<?php
			}
?>
									
									<td>
										<input type="hidden" name="edit" value="5" />
										<input type="submit" value="Save" />
									</td>
								</tr>
								<tr>
									<td colspan="12">
										<font size="2">
											*Fixed Amount uses employee's rate.
										</font>
									</td>
								</tr>
							</table>
						</form>
					</td>
				</tr>
				<tr bgcolor="#e8e7e7" height="16"><td colspan="3"></td></tr>
<?php
			//////////END LINKING

			///////////COMMISSIONS
?>
				<tr bgcolor="#e8e7e7">
<?php
			$calc_type_keys = Treat_Model_Vending_Commission_Detail::getCalcTypeKeys();
			$commission_detail_types = array(
				new Treat_ArrayObject( array(
					'label' => 'Sales',
					'class' => 'cd_sales',
					'type' => 1,
				) ),
				new Treat_ArrayObject( array(
					'label' => 'FV/Subsidy',
					'class' => 'cd_fv',
					'type' => 2,
				) ),
				new Treat_ArrayObject( array(
					'label' => 'CSV',
					'class' => 'cd_csv',
					'type' => 3,
				) ),
			);
			foreach ( $commission_detail_types as $detail_type ) {
				$comm_details = Treat_Model_Vending_Commission_Detail::getByCommissionIdAndType(
					$editcommission
					,$type = $detail_type->type
					,$class = null
				);
?>
					<td width="33%" valign="top">
									<form action="/ta/commissiondetail.php" method="post" class="commission_detail <?php echo $detail_type->class; ?>">
										<input type="hidden" name="commissionid" value="<?php echo $editcommission; ?>" />
										<input type="hidden" name="companyid" value="<?php echo $companyid; ?>" />
										<input type="hidden" name="type" value="<?php echo $detail_type->type; ?>" />
										<table style="border:1px solid black;" width="99%" bgcolor="#ffff99">
											<tr>
												<th class="header" colspan="5"><?php echo $detail_type->label; ?></th>
											</tr>
											<tr>
												<th>Lower</th>
												<th>Upper</th>
												<th>Total</th>
												<th>%</th>
												<td></td>
											</tr>
<?php
				if ( $comm_details ) {
					foreach ( $comm_details as $comm_detail ) {
						$temp_queryset = array_merge( $queryset, array(
							'delid' => $comm_detail->id,
							'commissionid' => $comm_detail->commissionid,
						) );
?>
											<tr>
												<td><?php echo $comm_detail->lower; ?></td>
												<td><?php echo $comm_detail->upper; ?></td>
												<td><?php echo $comm_detail->getCalcType(); ?></td>
												<td><?php echo $comm_detail->percent; ?>%</td>
												<td>
													<a href="/ta/commissiondetail.php?<?php
														echo http_build_query( $temp_queryset, null, '&amp;' );
													?>#vendcom">
														<img src="/assets/images/delete.gif" height="16" width="16" border="0" alt="delete" />
													</a>
												</td>
											</tr>
<?php
					}
				}
?>
											<tr>
												<td align="right">
													<input type="text" name="lower" style="font-size:10px;" size="4" />
												</td>
												<td align="right">
													<input type="text" name="upper" style="font-size:10px;" size="4" />
												</td>
												<td align="right">
													<select name="calc_type" style="font-size:10px;">
<?php
				foreach ( $calc_type_keys as $calc_type_key => $calc_type_label ) {
?>
														<option value="<?php
															echo $calc_type_key;
														?>"><?php
															echo $calc_type_label;
														?></option>";
<?php
				}
?>
													</select>
												</td>
												<td align="right">
													<input type="text" name="percent" style="font-size:10px;" size="4" />
												</td>
												<td>
													<input type="submit" value="Add" style="font-size:10px;" />
												</td>
											</tr>
										</table>
									</form>
					</td>
<?php
			}
?>
				</tr>
<?php
			///////////END COMMISSIONS
		}
?>
				<tr bgcolor="#e8e7e7" height="10"><td colspan="3"></td></tr>
				<tr bgcolor="#e8e7e7">
					<td colspan="3">
						<font color="red"></font>
							<ul>
								<li>
									<form action="/ta/savecommission.php" method="post">
										New Commission:
										<input type="text" size="20" name="commission_name" />
										<input type="submit" value="Add" />
									</form>
								</li>
							</ul>
					</td>
				</tr>
<?php
		//echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";
		//////////////////END COMMISSIONS
	}
?>
			</table>
		</center>
<?php
	$action2 = "businesstrack.php";
	$method = "post";
?>
		<center>
			<form action="<?php echo $action2; ?>" method="<?php echo $method; ?>" >
				<input type="hidden" name="username" value="<?php echo $user; ?>" />
				<input type="hidden" name="password" value="<?php echo $pass; ?>" />
				<input type="submit" value=" Return to Main Page" />
			</form>
		</center>
<?php
	//mysql_close();
	google_page_track();
?>
	</body>
</html>
<?php
}
?>