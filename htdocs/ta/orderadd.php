<?

function nextday( $date2 ) {
	$day = substr( $date2, 8, 2 );
	$month = substr( $date2, 5, 2 );
	$year = substr( $date2, 0, 4 );
	$leap = date( "L" );
	
	if( $day == '31'
			&& ( $month == '01' || $month == '03' || $month == '05' || $month == '07'
					|| $month == '08' || $month == '10' ) ) {
		if( $month == "01" ) {
			$month = "02";
		}
		if( $month == "03" ) {
			$month = "04";
		}
		if( $month == "05" ) {
			$month = "06";
		}
		if( $month == "07" ) {
			$month = "08";
		}
		if( $month == "08" ) {
			$month = "09";
		}
		if( $month == "10" ) {
			$month = "11";
		}
		$day = '01';
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if( $month == '02' && $day == '29' && $leap == '0' ) {
		$month = '03';
		$day = '01';
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if( $month == '02' && $day == '28' ) {
		$month = '03';
		$day = '01';
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if( $day == '30' && ( $month == '04' || $month == '06' || $month == '09' || $month == '11' ) ) {
		if( $month == "04" ) {
			$month = "05";
		}
		if( $month == "06" ) {
			$month = "07";
		}
		if( $month == "09" ) {
			$month = "10";
		}
		if( $month == "11" ) {
			$month = "12";
		}
		$day = '01';
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if( $month == '12' && $day == '32' ) {
		$day = '01';
		$month = '01';
		$year++;
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else {
		$day = $day + 1;
		if( $day < 10 ) {
			$day = "0$day";
		}
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
}

define( 'DOC_ROOT', dirname(dirname(__FILE__)) );
require_once( DOC_ROOT . '/bootstrap.php' );

$user = $_COOKIE["usercook"];
$cid = $_COOKIE["compcook"];
$pass = $_COOKIE["passcook"];
$businessid = $_POST['businessid'];
$daypartid = $_POST['daypartid'];
$menu_itemid = $_POST['menu_itemid'];
$date = $_POST['date'];
$newstart = $_POST['newstart'];
$newmonth = $_POST['newmonth'];
$newyear = $_POST['newyear'];
$personal = $_POST['personal'];
$weekend = $_POST['weekend'];
$direct = $_POST['direct'];
$date5 = $_POST['date5'];

if( $date == "" && $date5 == "" ) {
	$date = $_GET['date'];
	$date5 = $_GET['date5'];
	$direct = $_GET['direct'];
	$newstart = $_GET['newstart'];
	$newmonth = $_GET['newmonth'];
	$newyear = $_GET['newyear'];
	$weekend = $_GET['weekend'];
	$edit = $_GET['edit'];
	$deleteid = $_GET['deleteid'];
	$daypartid = $_GET['daypartid'];
	$prev_itemid = $_GET['prev_itemid'];
}

if( $date == "" && $date5 != "" ) {
	$date = $date5;
}

if( $direct == 1 ) {
	$location = "busordermenu.php?adddate=$date&newmonth=$newmonth&newyear=$newyear&newstart=$newstart#add";
}
else {
	$location = "orderdate.php?date=$date&newmonth=$newmonth&newyear=$newyear&newstart=$newstart&weekend=$weekend&daypartfocus=$daypartid#$daypartid";
}
header( 'Location: ./' . $location );

$query = "SELECT username,password FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query( $query );
$num = mysql_numrows( $result );

if( $num != 1 || $date == "" ) {
	echo "Failed, $user, $pass, $num<br>";
}
elseif( $edit == 2 ) {
	
	$query = "DELETE FROM order_item WHERE order_itemid = '$deleteid'";
	$result = Treat_DB_ProxyOld::query( $query );
	
}
elseif( $edit == 3 ) {
	
	$query = "SELECT * FROM order_item WHERE order_itemid = '$deleteid'";
	$result = Treat_DB_ProxyOld::query( $query );
	
	$orderid = mysql_result( $result, 0, "orderid" );
	$orderid--;
	
	$query = "UPDATE order_item SET orderid = '$orderid' WHERE order_itemid = '$deleteid'";
	$result = Treat_DB_ProxyOld::query( $query );
	
	$query = "SELECT * FROM order_item WHERE order_itemid = '$prev_itemid'";
	$result = Treat_DB_ProxyOld::query( $query );
	
	$orderid = mysql_result( $result, 0, "orderid" );
	$orderid++;
	
	$query = "UPDATE order_item SET orderid = '$orderid' WHERE order_itemid = '$prev_itemid'";
	$result = Treat_DB_ProxyOld::query( $query );
}
elseif( !empty( $menu_itemid ) ) {
	if( !is_array( $menu_itemid ) )
		$menu_itemid = array( $menu_itemid );
	
	foreach( $menu_itemid as $mid ) {
		$query = "SELECT * FROM order_item WHERE daypartid = '$daypartid' AND date = '$date' AND businessid = '$businessid'";
		$result = Treat_DB_ProxyOld::query( $query );
		$num = mysql_numrows( $result );
		$num = $num + 1;
		
		$query = "INSERT INTO order_item (menu_itemid,daypartid,date,businessid,orderid) VALUES ('$mid','$daypartid','$date','$businessid','$num')";
		$result = Treat_DB_ProxyOld::query( $query );
	}
}
?>
