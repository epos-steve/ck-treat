<?php
define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
require_once( DOC_ROOT . '/bootstrap.php' );
ini_set( 'include_path', DOC_ROOT . '/lib' . PATH_SEPARATOR . ini_get( 'include_path' ) );

ini_set( 'display_errors', false );
ini_set( 'memory_limit','1000M' );
if ( function_exists ( 'memory_get_usage' ) )
{
	echo 'enable memory limit was set in the configuration line, you can set the memory_limit in your script!', PHP_EOL;
	echo ini_get( 'memory_limit' ), PHP_EOL;
	echo ini_get( 'display_errors' ), PHP_EOL;
}
else
{
	echo 'enable memory limit was not set in the configuration line, you can not set the memory_limit in your script!', PHP_EOL;
}

set_time_limit(3600);
error_reporting(E_ALL);
require_once( 'Zend/Exception.php' );
require_once( 'Zend/Soap/Client.php' );


$wsdl = 'https://est05.esalestrack.com/esalestrackapi/service1.asmx?wsdl';
$options = array(
	'UserIdentifier' => '5cd1b9e5-2845-4acd-aed1-72a94a1d18b6',
	'Module'         => 'Company',
);

$tmp_file_any = DIR_TMP_FILES . '/esalestrack.any.xml';

echo '<p>'.$tmp_file_any.'</p>';

$xml_doc = '';
try {
	# if segment & file_put_contents is for testing only
	$filemtime = filemtime( $tmp_file_any );
	$too_old = strtotime( '-2 hours' );
	if ( file_exists( $tmp_file_any ) && $filemtime > $too_old ) {
		$xml_doc = file_get_contents( $tmp_file_any );
	}
	else {
		$client = new Zend_Soap_Client( $wsdl ); // Servers WSDL Location
		
		$result = $client->GetDataRecords( $options );
#		var_dump( $result );
		$xml_doc = $result->GetDataRecordsResult->any;
#		$schema_doc = $result->GetDataRecordsResult->schema;
		# for testing only
		file_put_contents( $tmp_file_any, $xml_doc );
	}
	
}
catch(Zend_Exception $e) {
	echo $e->getMessage();
        echo "failed";
	exit(__LINE__);
}
catch (SoapFault $e) {
	echo '$e->getMessage() = ', $e->getMessage(), PHP_EOL;
	echo '$client->getLastRequest() = ', PHP_EOL;
	echo $client->getLastRequest();
	exit(__LINE__);
}


$items = array();
if ( $xml_doc ) {
	$doc = new DOMDocument();
	$doc->loadXML( $xml_doc );
	
	$xpath = new DOMXpath( $doc );
	$xpath->registerNamespace( 'diffgr', 'urn:schemas-microsoft-com:xml-diffgram-v1' );
	
	$elements = $xpath->query( '/diffgr:diffgram/NewDataSet/Table' );
	if ( !is_null( $elements ) ) {
		foreach ($elements as $element) {
			$table = array();
			$nodes = $element->childNodes;
			foreach ($nodes as $node) {
				$table[ $node->nodeName ] = $node->nodeValue;
			}
			$items[] = $table;
		}
	}
	var_dump( $items );
}
else {
	exit(__LINE__);
}

////////////////////////////////////////////////

//echo "Done";

if( $items ){
		
		
		//print_r($items);
	
		$query9 = "DELETE FROM est_accounts";
                $result9 = DB_Proxy::query($query9);
		
		foreach($items AS $key => $value)
		{
                        /////clear variables
					$firstname="";
					$name="";
					$lastname="";
					$notes="";
					$address="";
					$city="";
					$state="";
					$zip="";
					$accountowner="";
					$status="";
					$employees="";
					$servicetype="";
					$customertype="";
					$channel="";
					$region="";
					$yrlyrevenue="";
					$addeddate="";
					$updateddate="";
					$redlistdate="";
					$datelost="";
					$cafeannualrevenue=0;
					$OCSRevenue=0;
					$contract="";
					$contractinfofoodserv="";
					$contactvend="";
					$contractinfovending="";
                                        $contexpires="";
                                        $accountnumber="";
                                        $is_valid=0;
                                        
			foreach($value AS $key3 => $value3){
				
					echo "$key3: $value3<br>";
					
					$value3=str_replace("'","`",$value3);
					
					//////FILL VARIABLES
					if($key3=="systemid"){
						$systemid = $value3;				
						$update=1;
					}
                                        elseif($key3=="status"){$is_valid = $value3;}
					elseif($key3=="companynamex"){$name = $value3;}
					elseif($key3=="firstname"){$firstname = $value3;}
					elseif($key3=="lastname"){$lastname = $value3;}
					elseif($key3=="notesx"){$notes = $value3; $notes=str_replace("'","`",$notes);}
					elseif($key3=="address"){$address = $value3;}
					elseif($key3=="city"){$city = $value3;}
					elseif($key3=="state"){$state = $value3;}
					elseif($key3=="zip"){$zip = $value3;}
					elseif($key3=="recordownerx"){
						
						$query = "SELECT * FROM est_accountowner WHERE estid LIKE '$value3'";
						$result = DB_Proxy::query($query);
						$num=mysql_numrows($result);
						
						if($num==0){
							$query = "INSERT INTO est_accountowner (estid) VALUES ('$value3')";
							$result = DB_Proxy::query($query);
							$accountowner = mysql_insert_id();
						}
						else{
							$accountowner = @mysql_result($result,0,"accountowner");
						}
					}
					elseif($key3=="statusx"){
						$query = "SELECT * FROM est_accountstatus WHERE name LIKE '$value3'";
						$result = DB_Proxy::query($query);
						$num=mysql_numrows($result);
						
						if($num==0){
							$query = "INSERT INTO est_accountstatus (name) VALUES ('$value3')";
							$result = DB_Proxy::query($query);
							$status = mysql_insert_id();
						}
						else{
							$status = @mysql_result($result,0,"status");
						}
					}
					elseif($key3=="ofemployeesx"){$employees = $value3;}
					elseif($key3=="servicetype"){
						$query = "SELECT * FROM est_servicetype WHERE name LIKE '$value3'";
						$result = DB_Proxy::query($query);
						$num=mysql_numrows($result);
						
						if($num==0){
							$query = "INSERT INTO est_servicetype (name) VALUES ('$value3')";
							$result =DB_Proxy::query($query);
							$servicetype = mysql_insert_id();
						}
						else{
							$servicetype = @mysql_result($result,0,"servicetype");
						}
					}
					elseif($key3=="companytypex"){
						$query = "SELECT * FROM est_customertype WHERE name LIKE '$value3'";
						$result = DB_Proxy::query($query);
						$num=mysql_numrows($result);
						
						if($num==0){
							$query = "INSERT INTO est_customertype (name) VALUES ('$value3')";
							$result = DB_Proxy::query($query);
							$customertype = mysql_insert_id();
						}
						else{
							$customertype = @mysql_result($result,0,"customertype");
						}
					}
					elseif($key3=="channelx"){
						$query = "SELECT * FROM est_channel WHERE name LIKE '$value3'";
						$result = DB_Proxy::query($query);
						$num=mysql_numrows($result);
						
						if($num==0){
							$query = "INSERT INTO est_channel (name) VALUES ('$value3')";
							$result = DB_Proxy::query($query);
							$channel = mysql_insert_id();
						}
						else{
							$channel = @mysql_result($result,0,"channel");
						}
					}
					elseif($key3=="regionx"){
						$query = "SELECT * FROM est_region WHERE name LIKE '$value3'";
						$result = DB_Proxy::query($query);
						$num=mysql_numrows($result);
						
						if($num==0){
							$query = "INSERT INTO est_region (name) VALUES ('$value3')";
							$result = DB_Proxy::query($query);
							$region = mysql_insert_id();
						}
						else{
							$region = @mysql_result($result,0,"region");
						}
					}
					elseif($key3=="estannualrevenuex"){$yrlyrevenue += $value3;}
                                        elseif($key3=="vendingrevenuex"){$yrlyrevenue += $value3;}
					elseif($key3=="addeddate"){$addeddate = substr($value3,0,10);}
					elseif($key3=="updateddate"){$updateddate = substr($value3,0,10);}
					elseif($key3=="datelostx1"){$datelost = substr($value3,0,10);}
					elseif($key3=="redlistdatex"){$redlistdate = substr($value3,0,10);}
					elseif($key3=="foodservicerevenuex"){$cafeannualrevenue = $value3;}
					elseif($key3=="ocsrevenuex"){$OCSRevenue = $value3;}
					elseif($key3=="contract"){$contract = $value3;}
					elseif($key3=="contractinfofoodserv"){$contractinfofoodserv = $value3;}
					elseif($key3=="contactvend"){$contractvend = $value3;}
					elseif($key3=="contractinfovending"){$contractinfovending = $value3;}
                                        elseif($key3=="contexpires"){$contexpires = substr($value3,0,10);}
                                        elseif($key3=="accountnumber"){$accountnumber = $value3;}
                                }
				
				//////INSERT
                                if($is_valid==1){
                                    $query = "INSERT INTO est_accounts (systemid,name,firstname,lastname,notes,address,city,state,zip,accountowner,status,employees,servicetype,customertype,channel,region,addeddate,updateddate,datelost,redlistdate,contract,contractinfofoodserv,contractvend,contractinfovending,contexpires) VALUES ('$systemid','$name','$firstname','$lastname','$notes','$address','$city','$state','$zip','$accountowner','$status','$employees','$servicetype','$customertype','$channel','$region','$addeddate','$updateddate','$datelost','$redlistdate','$contract','$contractinfofoodserv','$contractvend','$contractinfovending','$contexpires')";
                                    $result = DB_Proxy::query($query);

                                    echo "<font size=2>$query<br></font><br>";
                                    /////update account number
                                    //$query = "UPDATE est_opportunity SET accountnumber = '$accountnumber' WHERE _customer = '$systemid'";
                                    //$result = DB_Proxy::query($query);
                                }
				$redlistdate="";
				$datelost="";

		}
		
		unset($result);
		
		//////update dashboard time
		$showtime=date("D n/j/y g:ia");
		$query = "UPDATE dashboard_update SET sales = '$showtime'";
		$result = DB_Proxy::query($query);
		
		echo 'Done';
}







