<?php
header("Content-Type: text/plain");
///////////////////////
////////DATA///////////
///////////////////////
ini_set("display_errors", "true");
define('DOC_ROOT', dirname(dirname(__FILE__)));
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');
include_once('graph/open-flash-chart.php');

$showwho = isset($_GET["showwho"])?$_GET["showwho"]:'';
$showwho=explode("-",$showwho);
$year = isset($_GET["year"])?$_GET["year"]:'';
if($year==""){$year=date("Y");}
$year = $_COOKIE["est_year"];
//if($year==""){$year=2011;}

$today=date("Y-m-d");

		///sold by type
		if($showwho[0]==2){
			$typeid=$showwho[1];

			$totalbudget=0;
			$totalsales=0;
			$date1="$year-01-01";
			$date2="$year-01-31";
			for($counter=1;$counter<=12;$counter++){
				/////SALES
				$query2="SELECT SUM(est_opportunity.income1) AS totalamt FROM est_opportunity,est_accountowner,est_accountowner_budget WHERE est_opportunity.expecteddate >= '$date1' AND est_opportunity.expecteddate <= '$date2' AND (est_opportunity.status = '14' OR est_opportunity.status = '11' OR est_opportunity.status = '5' OR est_opportunity.status = '7') AND est_opportunity.accountowner = est_accountowner.accountowner AND est_accountowner.type = '$typeid' AND est_accountowner.accountowner = est_accountowner_budget.accountowner AND est_accountowner_budget.date = '$date1' AND est_accountowner_budget.gained > '0'";
				$result2 = Treat_DB_ProxyOld::query( $query2 );

				$totalamt=@mysql_result($result2,0,"totalamt");
				$totalsales+=$totalamt;

				/////BUDGET
				$query2="SELECT SUM(est_accountowner_budget.gained) AS gained FROM est_accountowner,est_accountowner_budget WHERE est_accountowner_budget.date = '$date1' AND est_accountowner_budget.accountowner = est_accountowner.accountowner AND est_accountowner.type = '$typeid'";
				$result2 = Treat_DB_ProxyOld::query( $query2 );

				$gained=@mysql_result($result2,0,"gained");
				$totalbudget+=$gained;

				////totals
				if($today>=$date1){
					$showsales[$counter-1]=round($totalsales,0);
				}
				$showbudget[$counter-1]=round($totalbudget,0);

				$newmonth=$counter+1;
				if(strlen($newmonth)<2){$newmonth="0$newmonth";}
				$date1="$year-$newmonth-01";
				$date2="$year-$newmonth-31";
            }
		}
		////sold by salesman
		elseif($showwho[0]==3){
			$accountowner=$showwho[1];
			$link=$showwho[2];

			if($link>0){$orquery=" OR accountowner = '$link'";}
			else{$orquery="";}

			$totalbudget=0;
			$totalsales=0;
			$date1="$year-01-01";
			$date2="$year-01-31";
			for($counter=1;$counter<=12;$counter++){
				/////SALES
				$query2="SELECT SUM(income1) AS totalamt FROM est_opportunity WHERE (accountowner = '$accountowner' $orquery) AND expecteddate >= '$date1' AND expecteddate <= '$date2' AND (status = '14' OR status = '11' OR status = '5' OR status = '7')";
				$result2 = Treat_DB_ProxyOld::query( $query2 );

				$totalamt=@mysql_result($result2,0,"totalamt");
				$totalsales+=$totalamt;

				/////BUDGET
				$query2="SELECT gained FROM est_accountowner_budget WHERE (accountowner = '$accountowner' $orquery) AND date = '$date1'";
				$result2 = Treat_DB_ProxyOld::query( $query2 );

				$gained=@mysql_result($result2,0,"gained");
				$totalbudget+=$gained;

				////totals
				if($today>=$date1){
					$showsales[$counter-1]=round($totalsales,0);
				}
				$showbudget[$counter-1]=round($totalbudget,0);

				$newmonth=$counter+1;
				if(strlen($newmonth)<2){$newmonth="0$newmonth";}
				$date1="$year-$newmonth-01";
				$date2="$year-$newmonth-31";
            }
		}
		////lost by company
		elseif($showwho[0]==4){
			$totalbudget=0;
			$totalsales=0;
			$date1="$year-01-01";
			$date2="$year-01-31";
			for($counter=1;$counter<=12;$counter++){
				/////SALES
				$query2="SELECT SUM(est_accounts.yrlyrevenue+est_accounts.cafeannualrevenue+est_accounts.ocsrevenue) AS totalamt FROM est_accounts,est_accountowner,est_accountowner_budget WHERE est_accounts.datelost >= '$date1' AND est_accounts.datelost <= '$date2' AND est_accounts.status = '2' AND (est_accounts.customertype != '3' AND est_accounts.customertype != '7') AND est_accounts.accountowner = est_accountowner.accountowner AND est_accountowner.accountowner = est_accountowner_budget.accountowner AND est_accountowner_budget.date = '$date1' AND est_accountowner_budget.lost > '0'";
				$result2 = Treat_DB_ProxyOld::query( $query2 );

				$totalamt=@mysql_result($result2,0,"totalamt");
				$totalsales+=$totalamt;

				/////BUDGET
				$query2="SELECT SUM(est_accountowner_budget.lost) AS gained FROM est_accountowner,est_accountowner_budget WHERE est_accountowner_budget.date = '$date1' AND est_accountowner_budget.accountowner = est_accountowner.accountowner";
				$result2 = Treat_DB_ProxyOld::query( $query2 );

				$gained=@mysql_result($result2,0,"gained");
				$totalbudget+=$gained;

				////totals
				if($today>=$date1){
					$showsales[$counter-1]=round($totalsales,0);
				}
				$showbudget[$counter-1]=round($totalbudget,0);

				$newmonth=$counter+1;
				if(strlen($newmonth)<2){$newmonth="0$newmonth";}
				$date1="$year-$newmonth-01";
				$date2="$year-$newmonth-31";
            }
		}
		////lost by type
		elseif($showwho[0]==5){
			$typeid=$showwho[1];

			$count1=1;
			$count2=2;
			$series="series";
			$seriescount=1;
			$totalbudget=0;
			$totalsales=0;
			$date1="$year-01-01";
			$date2="$year-01-31";
			for($counter=1;$counter<=12;$counter++){
                                if($typeid == 1){$get_what = "est_accounts.cafeannualrevenue";}
                                else{$get_what = "est_accounts.yrlyrevenue+est_accounts.ocsrevenue";}

				/////SALES
				$query2="SELECT SUM($get_what) AS totalamt FROM est_accounts,est_accountowner,est_accountowner_budget WHERE est_accounts.datelost >= '$date1' AND est_accounts.datelost <= '$date2' AND est_accounts.status = '2' AND (est_accounts.customertype != '3' AND est_accounts.customertype != '7') AND est_accounts.accountowner = est_accountowner.accountowner AND est_accountowner.type = '$typeid' AND est_accountowner.accountowner = est_accountowner_budget.accountowner AND est_accountowner_budget.date = '$date1' AND est_accountowner_budget.lost > '0'";
				$result2 = Treat_DB_ProxyOld::query( $query2 );

				$totalamt=@mysql_result($result2,0,"totalamt");
				$totalsales+=$totalamt;

				/////BUDGET
				$query2="SELECT SUM(est_accountowner_budget.lost) AS gained FROM est_accountowner,est_accountowner_budget WHERE est_accountowner_budget.date = '$date1' AND est_accountowner_budget.accountowner = est_accountowner.accountowner AND est_accountowner.type = '$typeid'";
				$result2 = Treat_DB_ProxyOld::query( $query2 );

				$gained=@mysql_result($result2,0,"gained");
				$totalbudget+=$gained;

				////totals
				if($today>=$date1){
					$showsales[$counter-1]=round($totalsales,0);
				}
				$showbudget[$counter-1]=round($totalbudget,0);

				$newmonth=$counter+1;
				if(strlen($newmonth)<2){$newmonth="0$newmonth";}
				$date1="$year-$newmonth-01";
				$date2="$year-$newmonth-31";
            }
		}
		////lost by salesman
		elseif($showwho[0]==6){
			$accountowner=$showwho[1];
			$link=$showwho[2];

			if($link>0){$orquery=" OR accountowner = '$link'";}
			else{$orquery="";}

			$count1=1;
			$count2=2;
			$series="series";
			$seriescount=1;
			$totalbudget=0;
			$totalsales=0;
			$date1="$year-01-01";
			$date2="$year-01-31";
			for($counter=1;$counter<=12;$counter++){
				/////SALES
				$query2="SELECT SUM(yrlyrevenue+cafeannualrevenue+ocsrevenue) AS totalamt FROM est_accounts WHERE (accountowner = '$accountowner' $orquery) AND datelost >= '$date1' AND datelost <= '$date2' AND status = '2' AND (customertype != '3' AND customertype != '7')";
				$result2 = Treat_DB_ProxyOld::query( $query2 );

				$totalamt=@mysql_result($result2,0,"totalamt");
				$totalsales+=$totalamt;

				/////BUDGET
				$query2="SELECT lost FROM est_accountowner_budget WHERE (accountowner = '$accountowner' $orquery) AND date = '$date1'";
				$result2 = Treat_DB_ProxyOld::query( $query2 );

				$gained=@mysql_result($result2,0,"lost");
				$totalbudget+=$gained;

				////totals
				if($today>=$date1){
					$showsales[$counter-1]=round($totalsales,0);
				}
				$showbudget[$counter-1]=round($totalbudget,0);

				$newmonth=$counter+1;
				if(strlen($newmonth)<2){$newmonth="0$newmonth";}
				$date1="$year-$newmonth-01";
				$date2="$year-$newmonth-31";
            }

		}
		////sold by company
		else{
			$totalbudget=0;
			$totalsales=0;
			$date1="$year-01-01";
			$date2="$year-01-31";
			for($counter=1;$counter<=12;$counter++){
				/////SALES
				$query2="SELECT SUM(est_opportunity.income1) AS totalamt FROM est_opportunity,est_accountowner,est_accountowner_budget WHERE est_opportunity.expecteddate >= '$date1' AND est_opportunity.expecteddate <= '$date2' AND (est_opportunity.status = '14' OR est_opportunity.status = '11' OR est_opportunity.status = '5' OR est_opportunity.status = '7') AND est_opportunity.accountowner = est_accountowner.accountowner AND est_accountowner.accountowner = est_accountowner_budget.accountowner AND est_accountowner_budget.date = '$date1' AND est_accountowner_budget.gained > '0'";
				$result2 = Treat_DB_ProxyOld::query( $query2 );

				$totalamt=@mysql_result($result2,0,"totalamt");
				$totalsales+=$totalamt;

				/////BUDGET
				$query2="SELECT SUM(est_accountowner_budget.gained) AS gained FROM est_accountowner,est_accountowner_budget WHERE est_accountowner_budget.date = '$date1' AND est_accountowner_budget.accountowner = est_accountowner.accountowner";
				$result2 = Treat_DB_ProxyOld::query( $query2 );

				$gained=@mysql_result($result2,0,"gained");
				$totalbudget+=$gained;

				////totals
				if($today>=$date1){
					$showsales[$counter-1]=round($totalsales,0);
				}
				$showbudget[$counter-1]=round($totalbudget,0);

				$newmonth=$counter+1;
				if(strlen($newmonth)<2){$newmonth="0$newmonth";}
				$date1="$year-$newmonth-01";
				$date2="$year-$newmonth-31";
			}
		}

///////////////////////
//////GRAPH////////////
///////////////////////
if($showwho[0]>3){
	$salescolor="#ff0000";
	$budgetcolor="#FFCC00";
}
else{
	$salescolor="#0000ff";
	$budgetcolor="#66CC66";
}

$title = new title( date("D M d Y") );

// ------- LINE 1 -----

$d = new solid_dot();
$d->size(3)->halo_size(0)->colour("$salescolor");

$d2 = new solid_dot();
$d2->size(3)->halo_size(0)->colour("$budgetcolor");


$line = new line();
$line->set_default_dot_style($d2);
$line->set_values( $showbudget );
$line->set_width( 2 );
$line->set_colour( "$budgetcolor" );
$line->on_show(new line_on_show(false, $cascade[0], $delay[0]));

$line2 = new line();
$line2->set_default_dot_style($d);
$line2->set_values( $showsales );
$line2->set_width( 2 );
$line2->set_colour( "$salescolor" );
$line2->on_show(new line_on_show(false, $cascade[0], $delay[0]));

if($totalbudget<$totalsales){$totalbudget=$totalsales;}

$y = new y_axis();
$y->set_range( 0, $totalbudget*1.5, 5 );
$y->set_colour('#C0C0C0');
$y->set_grid_colour( '#C0C0C0' );
$y->set_labels( array(' ',' ',' ',' ',' ',' ') );

$x = new x_axis();
$x->colour('#C0C0C0');
$x->set_grid_colour( '#C0C0C0' );
$x->set_range( 1, 12 );
if($showwho[0]==3||$showwho[0]==6){
	$x->set_labels_from_array( array( '', 'J', 'F', 'M', 'A', 'M', 'J', 'J', 'A', 'S', 'O', 'N', 'D') );
}
else{
	$x->set_labels_from_array( array( '', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec') );
}

$chart = new open_flash_chart();
//$chart->set_title( $title );
$chart->add_element( $line );
$chart->add_element( $line2 );
$chart->set_y_axis( $y );
$chart->set_x_axis( $x );
$chart->set_bg_colour('#FFFFFF');

echo $chart->toPrettyString();

?>