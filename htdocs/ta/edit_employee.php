<head>
	
<SCRIPT LANGUAGE="JavaScript" SRC="CalendarPopup.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">document.write(getCalendarStyles());</SCRIPT>

<STYLE>
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation
			{
			background-color:#6677DD;
			text-align:center;
			vertical-align:center;
			text-decoration:none;
			color:#FFFFFF;
			font-weight:bold;
			}
	.TESTcpDayColumnHeader,
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation,
	.TESTcpCurrentMonthDate,
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDate,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDate,
	.TESTcpCurrentDateDisabled,
	.TESTcpTodayText,
	.TESTcpTodayTextDisabled,
	.TESTcpText
			{
			font-family:arial;
			font-size:8pt;
			}
	TD.TESTcpDayColumnHeader
			{
			text-align:right;
			border:solid thin #6677DD;
			border-width:0 0 1 0;
			}
	.TESTcpCurrentMonthDate,
	.TESTcpOtherMonthDate,
	.TESTcpCurrentDate
			{
			text-align:right;
			text-decoration:none;
			}
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDateDisabled
			{
			color:#D0D0D0;
			text-align:right;
			text-decoration:line-through;
			}
	.TESTcpCurrentMonthDate
			{
			color:#6677DD;
			font-weight:bold;
			}
	.TESTcpCurrentDate
			{
			color: #FFFFFF;
			font-weight:bold;
			}
	.TESTcpOtherMonthDate
			{
			color:#808080;
			}
	TD.TESTcpCurrentDate
			{
			color:#FFFFFF;
			background-color: #6677DD;
			border-width:1;
			border:solid thin #000000;
			}
	TD.TESTcpCurrentDateDisabled
			{
			border-width:1;
			border:solid thin #FFAAAA;
			}
	TD.TESTcpTodayText,
	TD.TESTcpTodayTextDisabled
			{
			border:solid thin #6677DD;
			border-width:1 0 0 0;
			}
	A.TESTcpTodayText,
	SPAN.TESTcpTodayTextDisabled
			{
			height:20px;
			}
	A.TESTcpTodayText
			{
			color:#6677DD;
			font-weight:bold;
			}
	SPAN.TESTcpTodayTextDisabled
			{
			color:#D0D0D0;
			}
	.TESTcpBorder
			{
			border:solid thin #6677DD;
			}
</STYLE>

<SCRIPT TYPE="text/javascript">
<!--
function popup(mylink, windowname)
{
if (! window.focus)return true;
var href;
if (typeof(mylink) == 'string')
   href=mylink;
else
   href=mylink.href;
window.open(href, windowname, 'width=475,height=550,scrollbars=yes');
return false;
}
//-->
</SCRIPT>

</head>

<?php

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      elseif ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      elseif ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='31')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
require_once( dirname(__FILE__) . '/../../application/bootstrap.php' );

$showcustom = null; # Notice: Undefined variable

$style = "text-decoration:none";

$user = \EE\Controller\Base::getSessionCookieVariable( 'usercook' );
$pass = \EE\Controller\Base::getSessionCookieVariable( 'passcook' );
$businessid = \EE\Controller\Base::getPostVariable( 'businessid' );
$companyid = \EE\Controller\Base::getPostVariable( 'companyid' );
$employeeid = \EE\Controller\Base::getPostVariable( 'employeeid' );
$new_empl = \EE\Controller\Base::getPostVariable( 'new_empl' );
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";

if($employeeid == ""){
	$employeeid = \EE\Controller\Base::getGetVariable( 'eid' );
	$companyid = \EE\Controller\Base::getSessionCookieVariable( 'compcook' );
}

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query( $query );
$num = Treat_DB_ProxyOld::mysql_num_rows( $result );
//mysql_close();

if ($num != 1 || $employeeid == "-1")
{
    echo "<center><h3>Employee Not Found</h3>Use your browser's back button to try again.</center>";
}

else
{
    $security_level = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'security_level' );
    $mysecurity = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'security' );
    $myid = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'loginid' );

    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%><tr><td colspan=2>";
    echo "<img src=logo.jpg><p></td></tr></table></center>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM company WHERE companyid = '$companyid'";
    $result = Treat_DB_ProxyOld::query( $query );
    //mysql_close();

    $companyname = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'companyname' );

//////////////////////////////////////////////BEGIN EDIT ACCOUNT////////////////////////////////////////////////////////////
    if ($new_empl=="NEW"){}
    else{
    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM login WHERE loginid = '$employeeid'";
    $result = Treat_DB_ProxyOld::query( $query );
    //mysql_close();

    $lastname = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'lastname' );
    $firstname = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'firstname' );
    $sec_level = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'security_level' );
    $security = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'security' );
    $email = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'email' );
    $busid = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'businessid' );
    $payroll = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'payroll' );
    $busid2 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid2' );
    $busid3 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid3' );
    $busid4 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid4' );
    $busid5 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid5' );
    $busid6 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid6' );
    $busid7 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid7' );
    $busid8 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid8' );
    $busid9 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid9' );
    $busid10 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'busid10' );
    $empl_user = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'username' );
    $empl_pass = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'password' );
    $empl_no = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'empl_no' );
    $pr2 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr2' );
    $pr3 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr3' );
    $pr4 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr4' );
    $pr5 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr5' );
    $pr6 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr6' );
    $pr7 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr7' );
    $pr8 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr8' );
    $pr9 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr9' );
    $pr10 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'pr10' );
    $temp_expire = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'temp_expire' );
    $order_only = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'order_only' );
    $oo2 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'oo2' );
    $oo3 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'oo3' );
    $oo4 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'oo4' );
    $oo5 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'oo5' );
    $oo6 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'oo6' );
    $oo7 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'oo7' );
    $oo8 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'oo8' );
    $oo9 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'oo9' );
    $oo10 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'oo10' );
    $pass_expire = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'passexpire' );
	$receive_kpi = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'receive_kpi' );
    }

    $temp_date2=$today;
    $expiresin=0;
    if ($temp_expire!="0000-00-00"&&$new_empl!="NEW"){
       while($temp_date2<$temp_expire){$temp_date2=nextday($temp_date2);$expiresin++;}
    }
    else{$expiresin="";}

	echo "<center><table width=90%><tr valign=top><td width=50%>";

    echo "<FORM ACTION=saveemployee.php method=post>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<input type=hidden name=businessid value=$businessid>";
    echo "<input type=hidden name=companyid value=$companyid>";
    echo "<input type=hidden name=employeeid value=$employeeid>";
    echo "<input type=hidden name=new_empl value=$new_empl>";
    echo "<input type=hidden name=myseclvl value=$security_level>";
    echo "<input type=hidden name=oldseclvl value=$sec_level>";
    echo "<input type=hidden name=myid value=$myid>";
    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=98%>";
    //echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";
    echo "<tr bgcolor=#CCCCFF><td colspan=2><b>Employee Details</b></td></tr>";
    echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";
    echo "<tr valign=top bgcolor=#E8E7E7><td align=right><font color=red>Lastname:</font></td><td> <INPUT TYPE=text NAME=lastname SIZE=30 VALUE='$lastname'>";
    if ($employeeid>0){echo " <input type=checkbox name=is_deleted value=1> Delete This Employee";}
    echo "</td></tr>";
    echo "<tr bgcolor=#E8E7E7><td align=right><font color=red>Firstname:</font></td><td> <INPUT TYPE=text NAME=firstname SIZE=30 VALUE='$firstname'></td></tr>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    if($mysecurity==1){$query33 = "SELECT * FROM security ORDER BY security_name DESC";}
    else{$query33 = "SELECT * FROM security WHERE (securityid != '1' AND custom = '0') OR (securityid = '$security') ORDER BY security_name DESC";}
    $result33 = Treat_DB_ProxyOld::query( $query33 );
    $num33 = Treat_DB_ProxyOld::mysql_num_rows( $result33 );
    //mysql_close();

    echo "<tr bgcolor=#E8E7E7><td align=right><font color=red>Security:</font></td><td><select name=security>";

    //$num33--;
    while($num33>=0){
       $securityid = @Treat_DB_ProxyOld::mysql_result( $result33, $num33, 'securityid' );
       $security_name = @Treat_DB_ProxyOld::mysql_result( $result33, $num33, 'security_name' );
       $custom = @Treat_DB_ProxyOld::mysql_result( $result33, $num33, 'custom' );
	   
	   if($custom==1){$security_name="Custom ($security_name)";}
	   
       if ($security==$securityid){$showsecur="SELECTED";$nosec=1;}
       else{$showsecur="";}
	   
	   if ($security==$securityid&&$custom==1){$showcustom=1;}

       echo "<option value=$securityid $showsecur>$security_name</option>";

       $num33--;
    }
	
	if($nosec!=1 && $new_empl!="NEW"){$showsecur="SELECTED";}
	else{$showsecur="";}
	
    echo "<option value=-1 style=\"background-color:yellow;\" $showsecur>New Custom Security...</option></select><input type=hidden name=showcustom value=$showcustom></td></tr>";

    echo "<tr bgcolor=#E8E7E7><td align=right><font color=red>Security Level:</font></td><td><select name=sec_level>";
   
    $count=-1;
    while ($count<10){
       if ($count==$sec_level){$show="SELECTED";}
       else {$show="";}
       echo "<option value=$count $show>$count</option>";
       $count++;
    }

    echo "</select>  Expires In <input type=text name=expiredays value='$expiresin' size=3>* Days</td></tr>";
    echo "<tr bgcolor=#E8E7E7><td></td><td><font size=2>*Leave Blank if account does not expire</font></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td align=right><font color=red>Email:</font></td><td> <INPUT TYPE=text NAME=email SIZE=30 VALUE='$email'></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td align=right><font color=red>Username:</font></td>";
    if ($security_level==9){echo "<td><INPUT TYPE=text NAME=empl_user SIZE=30 VALUE='$empl_user'></td></tr>";}
    else {echo "<td><INPUT TYPE=hidden NAME=empl_user SIZE=30 VALUE='$empl_user'>$empl_user</td></tr>";}

    if ($sec_level<3){echo "<tr bgcolor=#E8E7E7><td align=right><font color=red>Password:</font></td><td><INPUT TYPE=text NAME=empl_pass SIZE=30  VALUE='$empl_pass'> Expires: $pass_expire</td></tr>";}
    else {echo "<tr bgcolor=#E8E7E7><td align=right><font color=red>Password:</font></td><td><INPUT TYPE=password NAME=empl_pass SIZE=30  VALUE='$empl_pass'> Expires: $pass_expire</td></tr>";}

    echo "<tr bgcolor=#E8E7E7><td align=right><font color=red>Employee #:</font></td><td><INPUT TYPE=text NAME=empl_no SIZE=12 VALUE='$empl_no' DISABLED></td></tr>";
	
	/////////////KPI's
	$opt1 = $opt2 = '';
	if ( $receive_kpi == 1 ) {
		$opt1 = 'selected="selected"';
	}
	elseif ( $receive_kpi == 2 ) {
		$opt2 = 'selected="selected"';
	}
	
	echo "<tr bgcolor=#E8E7E7><td align=right><font color=red>KPI's:</font></td><td><select name=receive_kpi><option value=0>Receive All Assigned</option><option value=1 $opt1>Receive 1st Business</option><option value=2 $opt2>Receive None</option></select></td></tr>";

	echo "<tr bgcolor=white><td colspan=2>&nbsp;</td></tr>";

    //echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";
    echo "<tr bgcolor=#CCCCFF><td colspan=2><b>Business Details</b></td></tr>";
    echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";

    echo "<tr bgcolor=#E8E7E7><td align=right><font color=red>Business ID:</font></td><td><select name=busid>";
    
    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    if($security_level>8){$query = "SELECT * FROM business WHERE companyid != '2' ORDER BY businessname DESC";}
    else{$query = "SELECT * FROM business WHERE companyid = '$companyid' ORDER BY businessname DESC";}
    $result = Treat_DB_ProxyOld::query( $query );
    $num = Treat_DB_ProxyOld::mysql_num_rows( $result );
    //mysql_close();
    $startnum=$num;

    while ($num>=0){
       $bus_id = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'businessid' );
       $bus_name = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'businessname' );
       if ($bus_id==$busid){$check="SELECTED";}
       else{$check="";}
       echo "<option value='$bus_id' $check>$bus_name</option>";
       $num--;
    }
    
    echo "</select> ";

    if ($payroll==1){$showpayroll="CHECKED";}
    else {$showpayroll="";}
    if ($order_only==1){$showorder="CHECKED";}
    else {$showorder="";}
    echo " <input type=checkbox name=payroll value=1 $showpayroll> Payroll Allowed &nbsp;</td></tr>";

    $num=$startnum;
    echo "<tr bgcolor=#E8E7E7><td align=right><font color=gray>Business ID:</font></td><td><select name=busid2>";
    while ($num>=0){
       $bus_id = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'businessid' );
       $bus_name = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'businessname' );
       if ($bus_id==$busid2){$check="SELECTED";}
       else{$check="";}
       echo "<option value='$bus_id' $check>$bus_name</option>";
       $num--;
    }
    echo "</select> ";
    if ($pr2==1){$showpayroll="CHECKED";}
    else {$showpayroll="";}
    if ($oo2==1){$showorder="CHECKED";}
    else {$showorder="";}
    echo " <input type=checkbox name=pr2 value=1 $showpayroll> Payroll Allowed &nbsp; </td></tr>";

    $num=$startnum;
    echo "<tr bgcolor=#E8E7E7><td align=right><font color=gray>Business ID:</font></td><td><select name=busid3>";
    while ($num>=0){
       $bus_id = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'businessid' );
       $bus_name = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'businessname' );
       if ($bus_id==$busid3){$check="SELECTED";}
       else{$check="";}
       echo "<option value='$bus_id' $check>$bus_name</option>";
       $num--;
    }
    echo "</select> ";
    if ($pr3==1){$showpayroll="CHECKED";}
    else {$showpayroll="";}
    if ($oo3==1){$showorder="CHECKED";}
    else {$showorder="";}
    echo " <input type=checkbox name=pr3 value=1 $showpayroll> Payroll Allowed &nbsp; </td></tr>";

    $num=$startnum;
    echo "<tr bgcolor=#E8E7E7><td align=right><font color=gray>Business ID:</font></td><td><select name=busid4>";
    while ($num>=0){
       $bus_id = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'businessid' );
       $bus_name = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'businessname' );
       if ($bus_id==$busid4){$check="SELECTED";}
       else{$check="";}
       echo "<option value='$bus_id' $check>$bus_name</option>";
       $num--;
    }
    echo "</select> ";
    if ($pr4==1){$showpayroll="CHECKED";}
    else {$showpayroll="";}
    if ($oo4==1){$showorder="CHECKED";}
    else {$showorder="";}
    echo " <input type=checkbox name=pr4 value=1 $showpayroll> Payroll Allowed &nbsp; </td></tr>";

    $num=$startnum;
    echo "<tr bgcolor=#E8E7E7><td align=right><font color=gray>Business ID:</font></td><td><select name=busid5>";
    while ($num>=0){
       $bus_id = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'businessid' );
       $bus_name = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'businessname' );
       if ($bus_id==$busid5){$check="SELECTED";}
       else{$check="";}
       echo "<option value='$bus_id' $check>$bus_name</option>";
       $num--;
    }
    echo "</select> ";
    if ($pr5==1){$showpayroll="CHECKED";}
    else {$showpayroll="";}
    if ($oo5==1){$showorder="CHECKED";}
    else {$showorder="";}
    echo " <input type=checkbox name=pr5 value=1 $showpayroll> Payroll Allowed &nbsp; </td></tr>";

    $num=$startnum;
    echo "<tr bgcolor=#E8E7E7><td align=right><font color=gray>Business ID:</font></td><td><select name=busid6>";
    while ($num>=0){
       $bus_id = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'businessid' );
       $bus_name = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'businessname' );
       if ($bus_id==$busid6){$check="SELECTED";}
       else{$check="";}
       echo "<option value='$bus_id' $check>$bus_name</option>";
       $num--;
    }
    echo "</select>";
    if ($pr6==1){$showpayroll="CHECKED";}
    else {$showpayroll="";}
    if ($oo6==1){$showorder="CHECKED";}
    else {$showorder="";}
    echo " <input type=checkbox name=pr6 value=1 $showpayroll> Payroll Allowed &nbsp; </td></tr>";

    $num=$startnum;
    echo "<tr bgcolor=#E8E7E7><td align=right><font color=gray>Business ID:</font></td><td><select name=busid7>";
    while ($num>=0){
       $bus_id = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'businessid' );
       $bus_name = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'businessname' );
       if ($bus_id==$busid7){$check="SELECTED";}
       else{$check="";}
       echo "<option value='$bus_id' $check>$bus_name</option>";
       $num--;
    }
    echo "</select>";
    if ($pr7==1){$showpayroll="CHECKED";}
    else {$showpayroll="";}
    if ($oo7==1){$showorder="CHECKED";}
    else {$showorder="";}
    echo " <input type=checkbox name=pr7 value=1 $showpayroll> Payroll Allowed &nbsp; </td></tr>";

    $num=$startnum;
    echo "<tr bgcolor=#E8E7E7><td align=right><font color=gray>Business ID:</font></td><td><select name=busid8>";
    while ($num>=0){
       $bus_id = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'businessid' );
       $bus_name = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'businessname' );
       if ($bus_id==$busid8){$check="SELECTED";}
       else{$check="";}
       echo "<option value='$bus_id' $check>$bus_name</option>";
       $num--;
    }
    echo "</select> ";
    if ($pr8==1){$showpayroll="CHECKED";}
    else {$showpayroll="";}
    if ($oo8==1){$showorder="CHECKED";}
    else {$showorder="";}
    echo " <input type=checkbox name=pr8 value=1 $showpayroll> Payroll Allowed &nbsp; </td></tr>";

    $num=$startnum;
    echo "<tr bgcolor=#E8E7E7><td align=right><font color=gray>Business ID:</font></td><td><select name=busid9>";
    while ($num>=0){
       $bus_id = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'businessid' );
       $bus_name = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'businessname' );
       if ($bus_id==$busid9){$check="SELECTED";}
       else{$check="";}
       echo "<option value='$bus_id' $check>$bus_name</option>";
       $num--;
    }
    echo "</select> ";
    if ($pr9==1){$showpayroll="CHECKED";}
    else {$showpayroll="";}
    if ($oo9==1){$showorder="CHECKED";}
    else {$showorder="";}
    echo " <input type=checkbox name=pr9 value=1 $showpayroll> Payroll Allowed &nbsp; </td></tr>";

    $num=$startnum;
    echo "<tr bgcolor=#E8E7E7><td align=right><font color=gray>Business ID:</font></td><td><select name=busid10>";
    while ($num>=0){
       $bus_id = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'businessid' );
       $bus_name = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'businessname' );
       if ($bus_id==$busid10){$check="SELECTED";}
       else{$check="";}
       echo "<option value='$bus_id' $check>$bus_name</option>";
       $num--;
    }
    echo "</select> ";
    if ($pr10==1){$showpayroll="CHECKED";}
    else {$showpayroll="";}
    if ($oo10==1){$showorder="CHECKED";}
    else {$showorder="";}
    echo " <input type=checkbox name=pr10 value=1 $showpayroll> Payroll Allowed &nbsp; </td></tr>";

/*
//////////////DISTRICTS
    echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";
    echo "<tr bgcolor=#CCCCFF><td colspan=2><b>District Details</b></td></tr>";
    echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";

    $lastcomid=0;

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM district ORDER BY companyid DESC, districtname DESC";
    $result = Treat_DB_ProxyOld::query( $query );
    $num = Treat_DB_ProxyOld::mysql_num_rows( $result );
    //mysql_close();

    echo "<tr bgcolor=#E8E7E7><td colspan=2><input type=hidden name=distnum value=$num>View Units/Recieve KPI's For*:</td></tr>";

    $num--;
    while ($num>=0){
       $districtid = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'districtid' );
       $districtname = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'districtname' );
       $dist_com_id = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'companyid' );

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query45 = "SELECT * FROM login_district WHERE loginid = '$employeeid' AND districtid = '$districtid'";
       $result45 = Treat_DB_ProxyOld::query( $query45 );
       $num45 = Treat_DB_ProxyOld::mysql_num_rows( $result45 );
       //mysql_close();

       if ($num45!=0){$showdist="CHECKED";}
       else{$showdist="";}

       $varname="dist$num";

       if ($lastcomid!=$dist_com_id){
          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query43 = "SELECT companyname FROM company WHERE companyid = '$dist_com_id'";
          $result43 = Treat_DB_ProxyOld::query( $query43 );
          //mysql_close();

          $dist_com_name = @Treat_DB_ProxyOld::mysql_result( $result43, 0, 'companyname' );
          echo "<tr bgcolor=#E8E7E7><td align=right></td><td><font size=2><u><i>$dist_com_name</td></tr>";
       }

       echo "<tr bgcolor=#E8E7E7><td align=right><input type=checkbox name=$varname value='1' $showdist></td><td> $districtname</td></tr>";
       $lastcomid=$dist_com_id;
       $num--;
    }
    echo "<tr bgcolor=#E8E7E7><td colspan=2><font size=1><b>*ONLY APPLIES TO SECURITY LEVELS 3 THROUGH 6. 7 AND ABOVE SEE/RECIEVE ALL UNITS.</td></tr>";
*/
//////////////COMPANIES
	echo "<tr bgcolor=white><td colspan=2>&nbsp;</td></tr>";

if($mysecurity==1){
    //echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";
    echo "<tr bgcolor=#CCCCFF><td colspan=2><b>Companies Allowed</b></td></tr>";
    echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM company ORDER BY companyname DESC";
    $result = Treat_DB_ProxyOld::query( $query );
    $num = Treat_DB_ProxyOld::mysql_num_rows( $result );
    //mysql_close();

    echo "<tr bgcolor=#E8E7E7><td colspan=2><input type=hidden name=co_num value=$num><font size=2>*Only applies if user's security includes changing companies.</td></tr>";

    $num--;
    while ($num>=0){
       $co_id = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'companyid' );
       $co_name = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'companyname' );

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query45 = "SELECT * FROM login_company WHERE loginid = '$employeeid' AND companyid = '$co_id'";
       $result45 = Treat_DB_ProxyOld::query( $query45 );
       $num45 = Treat_DB_ProxyOld::mysql_num_rows( $result45 );
       //mysql_close();

       if ($num45!=0){$showdist="CHECKED";}
       else{$showdist="";}

       $varname="co$num";

       echo "<tr bgcolor=#E8E7E7><td align=right><input type=checkbox name=$varname value='1' $showdist></td><td> $co_name</td></tr>";

       $num--;
    }
	echo "<tr bgcolor=white><td colspan=2>&nbsp;</td></tr>";
}

//////////////END COMPANIES
//////////////NEW LAYOUT
if($mysecurity==1){
    //echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";
    echo "<tr bgcolor=#CCCCFF><td colspan=2><b>Allow</b></td></tr>";
    echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";

    $query45 = "SELECT * FROM login_all WHERE loginid = '$employeeid'";
    $result45 = Treat_DB_ProxyOld::query( $query45 );
    $num45 = Treat_DB_ProxyOld::mysql_num_rows( $result45 );

    if($num45>0){$showdist="CHECKED";}
    else{$showdist="";}

    echo "<tr bgcolor=#E8E7E7><td colspan=2><input type=checkbox name=login_all value='1' $showdist> Treat America</td></tr>";

    ///////////COMPANY TYPE
    $query = "SELECT * FROM company_type ORDER BY type_name DESC";
    $result = Treat_DB_ProxyOld::query( $query );
    $num = Treat_DB_ProxyOld::mysql_num_rows( $result );

    $num--;
    while ($num>=0){
       $typeid = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'typeid' );
       $type_name = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'type_name' );

       $query45 = "SELECT * FROM login_company_type WHERE loginid = '$employeeid' AND company_type = '$typeid'";
       $result45 = Treat_DB_ProxyOld::query( $query45 );
       $num45 = Treat_DB_ProxyOld::mysql_num_rows( $result45 );

       if ($num45!=0){$showdist="CHECKED";}
       else{$showdist="";}

       $varname="co_type$typeid";

       echo "<tr bgcolor=#E8E7E7><td colspan=2>&nbsp;&nbsp;&nbsp;&nbsp;<input type=checkbox name=$varname value='1' $showdist> $type_name</td></tr>";

       /////////COMPANIES
       $query2 = "SELECT * FROM company WHERE company_type = '$typeid' ORDER BY companyname DESC";
       $result2 = Treat_DB_ProxyOld::query( $query2 );
       $num2 = Treat_DB_ProxyOld::mysql_num_rows( $result2 );

       $num2--;
       while ($num2>=0){
          $co_id = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'companyid' );
          $companyname = @Treat_DB_ProxyOld::mysql_result( $result2, $num2, 'reference' );

          $query45 = "SELECT * FROM login_comp WHERE loginid = '$employeeid' AND companyid = '$co_id'";
          $result45 = Treat_DB_ProxyOld::query( $query45 );
          $num45 = Treat_DB_ProxyOld::mysql_num_rows( $result45 );

          if ($num45!=0){$showdist="CHECKED";}
          else{$showdist="";}

          $varname="com$co_id";

          echo "<tr bgcolor=#E8E7E7><td colspan=2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=checkbox name=$varname value='1' $showdist> $companyname</td></tr>";

          /////////////DIVISIONS
          $query3 = "SELECT * FROM division WHERE companyid = '$co_id' ORDER BY division_name DESC";
          $result3 = Treat_DB_ProxyOld::query( $query3 );
          $num3 = Treat_DB_ProxyOld::mysql_num_rows( $result3 );

          $num3--;
          while ($num3>=0){
             $divisionid = @Treat_DB_ProxyOld::mysql_result( $result3, $num3, 'divisionid' );
             $division_name = @Treat_DB_ProxyOld::mysql_result( $result3, $num3, 'division_name' );

             $query45 = "SELECT * FROM login_division WHERE loginid = '$employeeid' AND divisionid = '$divisionid'";
             $result45 = Treat_DB_ProxyOld::query( $query45 );
             $num45 = Treat_DB_ProxyOld::mysql_num_rows( $result45 );

             if ($num45!=0){$showdist="CHECKED";}
             else{$showdist="";}

             $varname="div$divisionid";

             echo "<tr bgcolor=#E8E7E7><td colspan=2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=checkbox name=$varname value='1' $showdist> $division_name</td></tr>";

             /////////////DISTRICTS
             $query4 = "SELECT * FROM district WHERE divisionid = '$divisionid' ORDER BY districtname DESC";
             $result4 = Treat_DB_ProxyOld::query( $query4 );
             $num4 = Treat_DB_ProxyOld::mysql_num_rows( $result4 );

             $num4--;
             while ($num4>=0){
                $districtid = @Treat_DB_ProxyOld::mysql_result( $result4, $num4, 'districtid' );
                $districtname = @Treat_DB_ProxyOld::mysql_result( $result4, $num4, 'districtname' );

                $query45 = "SELECT * FROM login_district WHERE loginid = '$employeeid' AND districtid = '$districtid'";
                $result45 = Treat_DB_ProxyOld::query( $query45 );
                $num45 = Treat_DB_ProxyOld::mysql_num_rows( $result45 );

                if ($num45!=0){$showdist="CHECKED";}
                else{$showdist="";}

                $varname="dist$districtid";

                echo "<tr bgcolor=#E8E7E7><td colspan=2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=checkbox name=$varname value='1' $showdist> $districtname</td></tr>";
   
                $num4--;
             } 

             $num3--;
          } 

          $num2--;
       }

       $num--;
    }
    echo "<tr bgcolor=#E8E7e7><td colspan=2><font size=2>*Districts Must be checked for security levels 3 through 6 for access.</font></td></tr>";
	echo "<tr bgcolor=white><td colspan=2>&nbsp;</td></tr>";
}
//////////////END NEW LAYOUT
    //echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";
    echo "<tr bgcolor=#CCCCFF><td colspan=2><b>Extended</b></td></tr>";
    echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM login_extended WHERE loginid = '$employeeid'";
    $result = Treat_DB_ProxyOld::query( $query );
    $num = Treat_DB_ProxyOld::mysql_num_rows( $result );
    //mysql_close();

    $ex_title = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'title' );
    $ex_street = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'street' );
    $ex_city = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'city' );
    $ex_state = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'state' );
    $ex_zip = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'zip' );
    $ex_phone = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'phone' );
    $ex_fax = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'fax' );

    echo "<tr valign=top bgcolor=#E8E7E7><td align=right>Title:</font></td><td> <INPUT TYPE=text NAME=title SIZE=30 VALUE='$ex_title'></td></tr>";
    echo "<tr valign=top bgcolor=#E8E7E7><td align=right>Address:</font></td><td> <INPUT TYPE=text NAME=street SIZE=30 VALUE='$ex_street'></td></tr>";
    echo "<tr valign=top bgcolor=#E8E7E7><td align=right>City:</font></td><td> <INPUT TYPE=text NAME=city SIZE=30 VALUE='$ex_city'></td></tr>";
    echo "<tr valign=top bgcolor=#E8E7E7><td align=right>State:</font></td><td> <INPUT TYPE=text NAME=state SIZE=3 VALUE='$ex_state'></td></tr>";
    echo "<tr valign=top bgcolor=#E8E7E7><td align=right>Zip:</font></td><td> <INPUT TYPE=text NAME=zip SIZE=10 VALUE='$ex_zip'></td></tr>";
    echo "<tr valign=top bgcolor=#E8E7E7><td align=right>Phone:</font></td><td> <INPUT TYPE=text NAME=phone SIZE=30 VALUE='$ex_phone'></td></tr>";
    echo "<tr valign=top bgcolor=#E8E7E7><td align=right>Fax:</font></td><td> <INPUT TYPE=text NAME=fax SIZE=30 VALUE='$ex_fax'></td></tr>";

    //echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";
	echo "<tr bgcolor=white><td colspan=2>&nbsp;</td></tr>";
    echo "<tr bgcolor=#E8E7E7><td colspan=2 style=\"border:1px solid black;\"><center>Send User Update Email: <input type=checkbox name=sendemail> <INPUT TYPE=submit VALUE='  Save  '></FORM></center></td></tr>";
    //echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";
    echo "</table></center><p>";

	///////////////////////////////////
	////////2nd table//////////////////
	///////////////////////////////////
	echo "</td><td width=50%><center><table width=98% cellspacing=0 cellpadding=0 border=0>";

		//////control sheet date security
		echo "<tr bgcolor=#CCCCFF><td colspan=2><b>Control Sheet Start Date</b></td></tr>";
		echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";

		$query = "SELECT security_control.*,business.businessname FROM security_control,business WHERE security_control.loginid = '$employeeid' AND security_control.businessid = business.businessid";
		$result = Treat_DB_ProxyOld::query( $query );
		$num = Treat_DB_ProxyOld::mysql_num_rows( $result );

		if($num == 0){echo "<tr bgcolor=#E8E7E7><td>&nbsp;</td><td><i>Nothing to Display</i></td></tr>";}

		while($r = mysql_fetch_array($result)){
			$sec_id = $r["id"];
			$sec_bid = $r["businessid"];
			$sec_startdate = $r["startdate"];
			$bus_name = $r["businessname"];

			$showdelete = "<a href=sec_control.php?loginid=$employeeid&todo=1&id=$sec_id&type=1 onclick=\"return confirm('Are you sure?');\"><img src=delete.gif border=0></a>";

			echo "<tr bgcolor=#E8E7E7><td>&nbsp;</td><td>$bus_name [$sec_startdate] $showdelete</td></tr>";
		}

		///add
		$today = date("Y-m-d");

		$query = "SELECT businessid,businessname FROM business WHERE companyid != 2 ORDER BY businessname";
		$result = Treat_DB_ProxyOld::query( $query );

		echo "<tr bgcolor=#E8E7E7><td>&nbsp;</td><td><form action=sec_control.php?loginid=$employeeid&todo=0&type=1 method=post><select name=businessid>";

		while($r = mysql_fetch_array($result)){
			$sec_busid = $r["businessid"];
			$sec_busname = $r["businessname"];

			echo "<option value=$sec_busid>$sec_busname</option>";
		}
		echo "</select> Start Date: <input type=text size=8 name=startdate value='$today'><SCRIPT LANGUAGE='JavaScript' ID='js18'> var cal18 = new CalendarPopup('testdiv1'); cal18.setCssPrefix('TEST');</SCRIPT> <A HREF='javascript:void();' onClick=cal18.select(document.forms[1].startdate,'anchor18','yyyy-MM-dd'); return false; TITLE=cal18.select(document.forms[1].startdate,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor18' ID='anchor18'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A> <input type=submit value='Add'></form></td></tr>";

		echo "<tr><td colspan=2>&nbsp;</td></tr>";

		//////labor date security
		echo "<tr bgcolor=#CCCCFF><td colspan=2><b>Labor Start Date</b></td></tr>";
		echo "<tr bgcolor=black><td colspan=2 height=1></td></tr>";

		$query = "SELECT security_labor.*,business.businessname FROM security_labor,business WHERE security_labor.loginid = '$employeeid' AND security_labor.businessid = business.businessid";
		$result = Treat_DB_ProxyOld::query( $query );
		$num = Treat_DB_ProxyOld::mysql_num_rows( $result );

		if($num == 0){echo "<tr bgcolor=#E8E7E7><td>&nbsp;</td><td><i>Nothing to Display</i></td></tr>";}

		while($r = mysql_fetch_array($result)){
			$sec_id = $r["id"];
			$sec_bid = $r["businessid"];
			$sec_startdate = $r["startdate"];
			$bus_name = $r["businessname"];

			$showdelete = "<a href=sec_control.php?loginid=$employeeid&todo=1&id=$sec_id&type=2 onclick=\"return confirm('Are you sure?');\"><img src=delete.gif border=0></a>";

			echo "<tr bgcolor=#E8E7E7><td>&nbsp;</td><td>$bus_name [$sec_startdate] $showdelete</td></tr>";
		}

		///add
		$today = date("Y-m-d");

		$query = "SELECT businessid,businessname FROM business WHERE companyid != 2 ORDER BY businessname";
		$result = Treat_DB_ProxyOld::query( $query );

		echo "<tr bgcolor=#E8E7E7><td>&nbsp;</td><td><form action=sec_control.php?loginid=$employeeid&todo=0&type=2 method=post><select name=businessid>";

		while($r = mysql_fetch_array($result)){
			$sec_busid = $r["businessid"];
			$sec_busname = $r["businessname"];

			echo "<option value=$sec_busid>$sec_busname</option>";
		}
		echo "</select> Start Date: <input type=text size=8 name=startdate value='$today'><SCRIPT LANGUAGE='JavaScript' ID='js19'> var cal19 = new CalendarPopup('testdiv1'); cal19.setCssPrefix('TEST');</SCRIPT> <A HREF='javascript:void();' onClick=cal19.select(document.forms[2].startdate,'anchor19','yyyy-MM-dd'); return false; TITLE=cal19.select(document.forms[2].startdate,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor19' ID='anchor19'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A> <input type=submit value='Add'></form></td></tr>";

	echo "</table></center></td></tr></table></center>";

//////////////////////////////////////////////END Edit User////////////////////////////////////////////////////////////
    //mysql_close();

    echo "<center>";

    if($security_level==9&&$sec_level<9){echo "<form action=businesstrack.php method=post target='_blank' style=\"margin:0;padding:0;display:inline;\"><input type=hidden name=username value='$empl_user'><input type=hidden name=password value='$empl_pass'><input type=submit value='Login as $firstname $lastname'></form>&nbsp;";}

    echo "<FORM ACTION=businesstrack.php method=post style=\"margin:0;padding:0;display:inline;\">";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<INPUT TYPE=submit VALUE=' Return to Main Page'></FORM></center><DIV ID=testdiv1 STYLE=position:absolute;visibility:hidden;background-color:white;layer-background-color:white;></DIV>";
}
?>