<?php

function nextday($nextd,$day_format){ //Function returns next day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monn = substr($nextd, 5, 2);
   $dayn = substr($nextd, 8, 2);
   $yearn = substr($nextd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monn, $dayn+1, $yearn));
   return $tempdate;
}

function prevday($prevd,$day_format){ //Function returns previous day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monp = substr($prevd, 5, 2);
   $dayp = substr($prevd, 8, 2);
   $yearp = substr($prevd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monp, $dayp-1, $yearp));
   return $tempdate;
}

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+4);
        $diff=round($diff, 2);
        if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
        elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
        $diff = substr($diff, 0, $dot+3);
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        return $diff;
}

function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}

function batch_recipe($editid,$numserv,$serving_size,$inv_rec_num_main){

    global $cinv_item;
    global $cinv_name;
    global $cinv_recsize;
    global $cinv_price;

    global $cinv_inv_rec_num;
    global $cinv_pack_qty;
    global $cinv_pack_size;
    global $cinv_count_units;
    global $cinv_itemid;
    global $cinv_vendor; 

    $query = "SELECT * FROM recipe WHERE menu_itemid = '-$editid' ORDER BY recipeid DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=mysql_numrows($result);

    $totalprice=0;
    $totalserv=0;
    $counter=1;
    $num--;
    while ($num>=0){
       $recipeid=mysql_result($result,$num,"recipeid");
       $inv_itemid=mysql_result($result,$num,"inv_itemid");
       $rec_num=mysql_result($result,$num,"rec_num");
       $rec_num=$rec_num/$inv_rec_num_main*$numserv;
       $rec_size=mysql_result($result,$num,"rec_size");
       $srv_num=mysql_result($result,$num,"srv_num");
       $rec_order=mysql_result($result,$num,"rec_order");

       $query2 = "SELECT * FROM inv_items WHERE inv_itemid = '$inv_itemid'";
       $result2 = Treat_DB_ProxyOld::query($query2);

       $inv_itemname=mysql_result($result2,0,"item_name");
       $order_size=mysql_result($result2,0,"order_size");
       $price=mysql_result($result2,0,"price");
       if ($rec_order==1){$inv_rec_num=mysql_result($result2,0,"rec_num");}
       elseif ($rec_order==2){$inv_rec_num=mysql_result($result2,0,"rec_num2");}
       $pack_size=mysql_result($result2,0,"pack_size");
       $pack_qty=mysql_result($result2,0,"pack_qty");
       $count_units=mysql_result($result2,0,"units");
       $item_code=mysql_result($result2,0,"item_code");
       $vendor=mysql_result($result2,0,"vendor");
       $batch=mysql_result($result2,0,"batch");

       if ($pack_qty<1){$pack_qty=1;}

       if($batch==1){
          $newvalue=batch_recipe($inv_itemid,$numserv,$serving_size,$inv_rec_num);
          $newvalue=explode("~",$newvalue);
          $totalprice+=$newvalue[1];
       }
       else{

          $query2 = "SELECT * FROM conversion WHERE sizeid = '$rec_size' AND sizeidto = '$order_size'";
          $result2 = Treat_DB_ProxyOld::query($query2);
          $num2=mysql_numrows($result2);

          if ($num2!=0){$convert=mysql_result($result2,0,"convert");}
          elseif($num2==0){$convert=$inv_rec_num;}
          else{$convert=1;}

          $newprice=money($price/$convert*$rec_num);
          $totalprice=$totalprice+$newprice;

          $query2 = "SELECT * FROM size WHERE sizeid = '$rec_size'";
          $result2 = Treat_DB_ProxyOld::query($query2);

          $sizename=mysql_result($result2,0,"sizename");
  
          $totalserv=$totalserv+($rec_num*$srv_num);

          $var_name="$inv_itemid.$rec_order";

          if (array_key_exists($var_name, $cinv_item)){$cinv_item[$var_name]=$cinv_item[$var_name]+$rec_num;$cinv_price[$var_name]=$cinv_price[$var_name]+$newprice;}
          else{$cinv_item[$var_name]=$rec_num;$cinv_name[$var_name]=$inv_itemname;$cinv_price[$var_name]=$newprice;$cinv_recsize[$var_name]=$sizename;$cinv_inv_rec_num[$var_name]=$inv_rec_num;$cinv_pack_qty[$var_name]=$pack_qty;$cinv_pack_size[$var_name]=$pack_size;$cinv_count_units[$var_name]=$count_units;$cinv_itemid[$var_name]=$inv_itemid;$cinv_vendor[$var_name]=$vendor;}
       }

       $num--;
       $counter++;
    }

    $newvalue="$item_price~$newprice";
    return $newvalue;
}

define('DOC_ROOT', dirname(dirname(__FILE__)));
require_once(DOC_ROOT.'/bootstrap.php');

$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";


$curmenu=$_POST["curmenu"];
$businessid=$_POST["businessid"];
$date1=$_POST["date1"];
$date2=$_POST["date2"];
$newdate=$_POST["newdate"];

/*
  $cinv_item=array();
  $cinv_name=array();
  $cinv_recsize=array();
  $cinv_price=array();

  $cinv_inv_rec_num=array();
  $cinv_pack_qty=array();
  $cinv_count_units=array();
*/
  global $cinv_item;
  global $cinv_name;
  global $cinv_recsize;
  global $cinv_price;

  global $cinv_inv_rec_num;
  global $cinv_pack_qty;
  global $cinv_pack_size;
  global $cinv_count_units;
  global $cinv_itemid;
  global $cinv_vendor; 
/////////////////////////MENU PROFITABLITIY////////////////

  //mysql_connect($dbhost,$username,$password);
  //@mysql_select_db($database) or die( "Unable to select database");
  $query12 = "SELECT businessname FROM business WHERE businessid = '$businessid'";
  $result12 = Treat_DB_ProxyOld::query($query12);
  //mysql_close();

  $businessname=mysql_result($result12,0,"businessname");

  //mysql_connect($dbhost,$username,$password);
  //@mysql_select_db($database) or die( "Unable to select database");
  $query12 = "SELECT menu_typename FROM menu_type WHERE menu_typeid = '$curmenu'";
  $result12 = Treat_DB_ProxyOld::query($query12);
  //mysql_close();

  $curmenuname=mysql_result($result12,0,"menu_typename");

  if($date1!=""){$dayname=dayofweek($date1);$dayname2=dayofweek($date2);$showdate="$dayname $date1 to $dayname2 $date2";}
  else{$showdate="_____________________________________________________";}

  echo "<center><b>PRODUCTION FOR $showdate</center><p>";

  echo "<table width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td colspan=4><b><i>Menu Items:</td></tr>";

  //mysql_connect($dbhost,$username,$password);
  //@mysql_select_db($database) or die( "Unable to select database");
  $query12 = "SELECT * FROM menu_items WHERE businessid = '$businessid' AND menu_typeid = '$curmenu' ORDER BY groupid DESC, item_name DESC";
  $result12 = Treat_DB_ProxyOld::query($query12);
  $num12=mysql_numrows($result12);
  //mysql_close();

  $totalprice2=0;
  $lastgroupid=0;
  $num12--;
  while($num12>=0){

    $editid=mysql_result($result12,$num12,"menu_item_id");
    $itemname=mysql_result($result12,$num12,"item_name");
    $groupid=mysql_result($result12,$num12,"groupid");
    $serving_size=mysql_result($result12,$num12,"serving_size");
    $item_price=mysql_result($result12,$num12,"price");

    $numserv=$_POST[$editid];

  if ($numserv>0){
    $showprice2=$numserv*$item_price;
    $totalprice2+=$showprice2;
    $showprice2=number_format($showprice2,2);

    echo "<tr onMouseOver=this.bgColor='yellow' onMouseOut=this.bgColor='white'><td align=right width=20%>$numserv</td><td width=2%></td><td> $itemname</td> <td align=right>$$showprice2</td></tr>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM recipe WHERE menu_itemid = '$editid' ORDER BY recipeid DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=mysql_numrows($result);
    //mysql_close();

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query2 = "SELECT price FROM menu_items WHERE menu_item_id = '$editid'";
    $result2 = Treat_DB_ProxyOld::query($query2);
    //mysql_close();

    $item_price=mysql_result($result2,0,"price");

    $totalprice=0;
    $totalserv=0;
    $counter=1;
    $num--;
    while ($num>=0){
       $recipeid=mysql_result($result,$num,"recipeid");
       $inv_itemid=mysql_result($result,$num,"inv_itemid");
       $rec_num=mysql_result($result,$num,"rec_num");
       $rec_num=round($rec_num/$serving_size*$numserv,2);
       $rec_size=mysql_result($result,$num,"rec_size");
       $srv_num=mysql_result($result,$num,"srv_num");
       $rec_order=mysql_result($result,$num,"rec_order");

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM inv_items WHERE inv_itemid = '$inv_itemid'";
       $result2 = Treat_DB_ProxyOld::query($query2);
       //mysql_close();

       $inv_itemname=mysql_result($result2,0,"item_name");
       $order_size=mysql_result($result2,0,"order_size");
       $price=mysql_result($result2,0,"price");
       if ($rec_order==1){$inv_rec_num=mysql_result($result2,0,"rec_num");}
       elseif ($rec_order==2){$inv_rec_num=mysql_result($result2,0,"rec_num2");}
       $pack_size=mysql_result($result2,0,"pack_size");
       $pack_qty=mysql_result($result2,0,"pack_qty");
       $count_units=mysql_result($result2,0,"units");
       $item_code=mysql_result($result2,0,"item_code");
       $vendor=mysql_result($result2,0,"vendor");
       $batch=mysql_result($result2,0,"batch");

       if ($pack_qty<1){$pack_qty=1;}

       if($batch==1){
          $newvalue=batch_recipe($inv_itemid,$numserv,$serving_size,$inv_rec_num);

          $newvalue=explode("~",$newvalue);
          $totalprice+=$newvalue[1];
       }
       else{
          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query2 = "SELECT * FROM conversion WHERE sizeid = '$rec_size' AND sizeidto = '$order_size'";
          $result2 = Treat_DB_ProxyOld::query($query2);
          $num2=mysql_numrows($result2);
          //mysql_close();

          if ($num2!=0){$convert=mysql_result($result2,0,"convert");}
          elseif($num2==0){$convert=$inv_rec_num;}
          else{$convert=1;}

          $newprice=money($price/$convert*$rec_num);
          $totalprice=$totalprice+$newprice;

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query2 = "SELECT * FROM size WHERE sizeid = '$rec_size'";
          $result2 = Treat_DB_ProxyOld::query($query2);
          //mysql_close();

          $sizename=mysql_result($result2,0,"sizename");
  
          $totalserv=$totalserv+($rec_num*$srv_num);

          $var_name="$inv_itemid.$rec_order";

          if (array_key_exists($var_name, $cinv_item)){$cinv_item[$var_name]=$cinv_item[$var_name]+$rec_num;$cinv_price[$var_name]=$cinv_price[$var_name]+$newprice;}
          else{$cinv_item[$var_name]=$rec_num;$cinv_name[$var_name]=$inv_itemname;$cinv_price[$var_name]=$newprice;$cinv_recsize[$var_name]=$sizename;$cinv_inv_rec_num[$var_name]=$inv_rec_num;$cinv_pack_qty[$var_name]=$pack_qty;$cinv_pack_size[$var_name]=$pack_size;$cinv_count_units[$var_name]=$count_units;$cinv_itemid[$var_name]=$inv_itemid;$cinv_vendor[$var_name]=$vendor;}
       }

       $num--;
       $counter++;
    }

    $item_price=money($item_price*$serving_size);
    $totalprice=money($totalprice);
    $profit=money($item_price-$totalprice);
    $fcprcnt=round($totalprice/$item_price*100,1);
  }
    $num12--;
  }
  $showtotalprice2=number_format($totalprice2,2);
  echo "<tr bgcolor=#E8E7E7><td colspan=3></td><td align=right><b>Total: $$showtotalprice2</b></td></tr>";
  echo "</table>";

  echo "<p STYLE='page-break-before: always'>&nbsp</p>";

/////////////DISPLAY INGREDIENTS

  //ksort($cinv_item);
  asort($cinv_vendor);

  $lastvendor=-1;

  echo "<table width=100% cellspacing=0 cellpadding=0 style=\"border:2px solid #E8E7E7;\"><tr bgcolor=#E8E7E7><td colspan=3><b><i>Ingredients:</td><td align=right><b><i>OnHand:&nbsp;&nbsp;</td><td align=right><b><i>Order:&nbsp;&nbsp;</td><td colspan=2></td></tr>";
  foreach ($cinv_vendor as $key => $value){

     ///////////////////////////
     /////COMPARE TO INV COUNT//
     ///////////////////////////
     $query14 = "SELECT SUM(amount) AS totalamt FROM inv_count WHERE inv_itemid = '$cinv_itemid[$key]' AND date = '$newdate'";
     $result14 = Treat_DB_ProxyOld::query($query14);

     $onhand=mysql_result($result14,0,"totalamt");
	 
	 if($cinv_pack_qty[$key]==0){$cinv_pack_qty[$key]=1;}

     if($cinv_pack_qty[$key]>1){$onhand=round($onhand/$cinv_pack_qty[$key],2);}

     $order=ceil($cinv_item[$key]/($cinv_inv_rec_num[$key]*$cinv_pack_qty[$key]));
     $order=($order-$onhand);
     if($order<=0){$order="";}
     else{$order=ceil($order); $order="<font color=red>$order</font>";}
  
     $count_num=0;
     $test=1;
     $test=$cinv_inv_rec_num[$key]/$cinv_pack_qty[$key];
     while ($cinv_item[$key]>=$test){
          $count_num++;
          $cinv_item[$key]=$cinv_item[$key]-$test;
     }

     $cinv_item[$key]=round($cinv_item[$key],2);

     $shownewsize="";
     if ($count_num!=0&&$cinv_item[$key]==0){$shownewsize="$count_num $cinv_count_units[$key]";}
     elseif ($count_num!=0&&$cinv_item[$key]!=0){$shownewsize="$count_num $cinv_count_units[$key], $cinv_item[$key] $cinv_recsize[$key]";}
     else {$shownewsize="$cinv_item[$key] $cinv_recsize[$key]";}
  
     //$value $cinv_recsize[$key]

     $showprice=number_format($cinv_price[$key],2);

     ///////SHOW VENDOR
     if($lastvendor!=$cinv_vendor[$key]){
        $query24 = "SELECT vendor_name FROM vendor_master WHERE vendor_master_id = '$cinv_vendor[$key]'";
        $result24 = Treat_DB_ProxyOld::query($query24);

        $vendor_name=mysql_result($result24,0,"vendor_name");

        echo "<tr bgcolor=#FFFF99><td colspan=7><b>$vendor_name</b></td></tr>";
     }
	 
	 if($cinv_pack_size[$key]==""){$cinv_pack_size[$key]=$cinv_count_units[$key];}
	 
     echo "<tr onMouseOver=this.bgColor='yellow' onMouseOut=this.bgColor='white' valign=top><td align=right width=20%>$shownewsize</td><td width=2%></td><td>$cinv_name[$key]</td><td align=right>$onhand &nbsp;&nbsp;</td><td align=right>$order &nbsp;&nbsp;</td><td>$cinv_pack_size[$key]</td><td align=right>$$showprice</td></tr>";
     $totalcost+=$cinv_price[$key];

     $lastvendor=$cinv_vendor[$key];
  }
  $showtotalcost=number_format($totalcost,2);
  echo "<tr bgcolor=#E8E7E7><td colspan=7 align=right><b>Total: $$showtotalcost</b></td></tr>";
  echo "</table></center>";

  //mysql_close();
  google_page_track();
?> 