<?php

//////////////////////////////REVISION HISTORY//////////////////////////////////
//2/23/07 - Added refnum field to invoice table for referencing new invoices.  Number is MMDDHHMMSS - SM
/////////////////////////////////////////////////////////////////////////////////

function nextday($nextd,$day_format){ //Function returns next day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monn = substr($nextd, 5, 2);
   $dayn = substr($nextd, 8, 2);
   $yearn = substr($nextd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monn, $dayn+1, $yearn));
   return $tempdate;
}

function prevday($prevd,$day_format){ //Function returns previous day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monp = substr($prevd, 5, 2);
   $dayp = substr($prevd, 8, 2);
   $yearp = substr($prevd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monp, $dayp-1, $yearp));
   return $tempdate;
}

function money($diff){
		$diff = round($diff,2);
        $diff = number_format($diff,2,'.','');
        return $diff;
}

define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once(DOC_ROOT.'/bootstrap.php');

$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";

$user=$_POST['username'];
$pass=$_POST['password'];
$businessid=$_POST['businessid'];
$companyid=$_POST['companyid'];
$invoiceid=$_POST['invoiceid'];
$account=$_POST['account'];
$date=$_POST['date'];
$status=$_POST['status'];
$invtype=$_POST['invtype'];
$tax=$_POST['tax'];
$taxable=$_POST['taxable'];
$event_date=$_POST['event_date'];
$event_time=$_POST['event_time'];
$buildingnum=$_POST['buildingnum'];
$buildingnum=str_replace("'","`",$buildingnum);
$floornum=$_POST['floornum'];
$people=$_POST['people'];
$set_up_time=$_POST['set_up_time'];
$pick_up_time=$_POST['pick_up_time'];
$costcenter=$_POST['costcenter'];
$consolidate=$_POST['consolidate'];
$service=$_POST['service'];
$notes=$_POST['notes'];
$notes=str_replace("'","`",$notes);
$posted=$_POST['posted'];
$direct=$_POST['direct'];
$previnvdate=$_POST['previnvdate'];
$flatsrvchrg=$_POST['flatsrvchrg'];
$reference=$_POST['reference'];
$reference=str_replace("'","",$reference);
$ponum=$_POST['ponum'];
$contact=$_POST['contact'];
$contact=str_replace("'","`",$contact);
$contact_phone=$_POST['contact_phone'];
$refnum=$_POST['refnum'];
$acct_status=$_POST['acct_status'];
$is_aged=$_POST['is_aged'];

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM submit WHERE businessid = '$businessid' ORDER BY date";
$result = DB_Proxy::query($query);
$num=mysql_numrows($result);

if ($num!=0){
   $lastdate=mysql_result($result,$num-1,"date");
}

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = DB_Proxy::query($query);
$num=mysql_numrows($result);
//mysql_close();

if ($num!=0){
$security_level=mysql_result($result,0,"security_level");
$mysecurity=mysql_result($result,0,"security");

////NEW SECURITY
    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query56 = "SELECT * FROM security WHERE securityid = '$mysecurity'";
    $result56 = DB_Proxy::query($query56);
    //mysql_close();

    $security_level=mysql_result($result56,0,"bus_invoice");
}

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query56 = "SELECT accountnum FROM accounts WHERE accountid = '$account'";
    $result56 = DB_Proxy::query($query56);
    //mysql_close();

    $acct_number=mysql_result($result56,0,"accountnum");

if ($num != 1 || $status == "" || $date == "" || $account == "" || $invtype == "" || $contact == "" || $contact_phone == "" || $people == "" || $event_time == "")
{
    echo "<center><p><h3>Please fill in a required fields<font color=red>*</font></h3></center>";
    echo "<center><i>Use your browser's back button to return</i></center>";
    //echo "<center>$num,$status,$date,$account,$invtype,$contact,$contact_phone,$people,$event_time</center>";
}

elseif ($date<=$lastdate && $security_level<8) {
    if ($security_level==1 && $posted == 1){$location="saveinvoice.php?bid=$businessid&cid=$companyid&invoiceid=$invoiceid";}
    elseif ($invoiceid!=""){$location="saveinvoice.php?bid=$businessid&cid=$companyid&past=1&invoiceid=$invoiceid";}
    else {$location="saveinvoice.php?bid=$businessid&cid=$companyid&invtype=$invtype";}
    header('Location: ./' . $location);
}

elseif($acct_number==""&&$status==4){echo "<center><p><br><p><h2><img src=error2.gif height=25 width=25> You may not post invoices for this account. You are responsible for payments.</h3><br>Please request an account number using your Request page if this account wishes to pay at a later date.</center>";}

elseif($acct_status==2){echo "<center><p><br><p><h2><img src=error2.gif height=25 width=25> <font color=red>This account is currently suspended.</font></h3></center>";}

else
{
  if ($security_level<8 && $posted == 1){
       $location="saveinvoice.php?bid=$businessid&cid=$companyid&invoiceid=$invoiceid"; 
       header('Location: ./' . $location);
  }
  else
  {
    if ($taxable==""){$taxable="off";}
    if ($invoiceid==""){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT salescode,name FROM accounts WHERE accountid = '$account'";
       $result = DB_Proxy::query($query);
       //mysql_close();

       $salescode=mysql_result($result,0,"salescode");
       $agedname=mysql_result($result,0,"name");

       if ($consolidate==""){$consolidate=0;}
       $query = "INSERT INTO invoice (businessid,companyid,accountid,date,status,createby,createdate,type,tax,taxable,event_date,event_time,buildingnum,floornum,people,set_up_time,pick_up_time,costcenter,consolidate,service,notes,flatsrvchrg,salescode,reference,contact,contact_phone,ponum,refnum) VALUES ('$businessid','$companyid','$account','$date','$status','$user','$today','$invtype','$tax','$taxable','$event_date','$event_time','$buildingnum','$floornum','$people','$set_up_time','$pick_up_time','$costcenter','$consolidate','$service','$notes','$flatsrvchrg','$salescode','$reference','$contact','$contact_phone','$ponum','$refnum')";

    }
    else{
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT salescode FROM accounts WHERE accountid = '$account'";
       $result = DB_Proxy::query($query);
       //mysql_close();

       $salescode=mysql_result($result,0,"salescode");

       $query="UPDATE invoice SET type = '$invtype', date = '$date', status = '$status', accountid = '$account', editby = '$user', editdate = '$today', tax = '$tax', taxable = '$taxable', event_date = '$event_date', event_time = '$event_time', buildingnum = '$buildingnum', floornum = '$floornum', people = '$people', set_up_time = '$set_up_time', pick_up_time = '$pick_up_time', costcenter = '$costcenter', service = '$service', notes = '$notes', flatsrvchrg = '$flatsrvchrg', salescode = '$salescode', reference = '$reference', contact = '$contact', contact_phone = '$contact_phone', ponum = '$ponum' WHERE invoiceid = '$invoiceid'";
    }

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $result = DB_Proxy::query($query);
    //mysql_close();
	
    $newinvoice = mysql_insert_id();

    ////////////////////////////////////////////////////////
    ////////////////insert/update custom fields/////////////
    ////////////////////////////////////////////////////////
    $updateid = $invoiceid;
    if($updateid<1){$updateid=$newinvoice;}

    $query_custom = "SELECT * FROM invoice_custom_fields WHERE businessid = '$businessid' AND active = '0'";
    $result_custom = DB_Proxy::query($query_custom);
    $num_custom = mysql_num_rows($result_custom);

    if($num_custom>0){

         while($r_cust=mysql_fetch_array($result_custom)){
             $cust_id=$r_cust["id"];
             $newvalue = $_POST["cust$cust_id"];
             $type = $r_cust["type"];

             $query_custom2 = "SELECT id FROM invoice_custom_values WHERE invoiceid = '$updateid' AND customid = '$cust_id'";
             $result_custom2 = DB_Proxy::query($query_custom2);
             $num_custom2 = mysql_num_rows($result_custom2);

             if($type == 4){}//do nothing
             elseif($num_custom2!=0){
                 $valueid = mysql_result($result_custom2,0,"id");
                 $query_custom2 = "UPDATE invoice_custom_values SET value = '$newvalue' WHERE id = '$valueid'";
                 $result_custom2 = DB_Proxy::query($query_custom2);
             }
             elseif($newvalue!=""){
                 $query_custom2 = "INSERT INTO invoice_custom_values (invoiceid,customid,value) VALUES ('$updateid','$cust_id','$newvalue')";
                 $result_custom2 = DB_Proxy::query($query_custom2);
             }
         }
    }

    ////////////////////////////////////////////////////////
    ////////////////end custom fields///////////////////////
    ////////////////////////////////////////////////////////

    ///////UPDATE INVOICE TENDERS//////////////////////////////////////
    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "UPDATE invtender SET sameday = '0' WHERE invoiceid = '$invoiceid' AND date = '$date'";
    $result = DB_Proxy::query($query);
    //mysql_close();

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "UPDATE invtender SET sameday = '1' WHERE invoiceid = '$invoiceid' AND date != '$date'";
    $result = DB_Proxy::query($query);
    //mysql_close();

///////////////////////////////

    if ($invoiceid==""){

       //$query = "SELECT invoiceid FROM invoice WHERE businessid = '$businessid' AND companyid = '$companyid' AND date = '$date' AND createby = '$user' AND createdate = '$today' AND accountid = '$account' AND people = '$people' AND event_time = '$event_time' AND contact_phone = '$contact_phone' AND people = '$people' AND refnum = '$refnum'";
       //$result = DB_Proxy::query($query);

       //$invoiceid=mysql_result($result,0,"invoiceid");
	   
	   $invoiceid=$newinvoice;
	   
       $goto=2;

       if ($is_aged==1){
          $query73 = "SELECT businessname,unit,districtid FROM business WHERE businessid = '$businessid'";
          $result73 = DB_Proxy::query($query73);

          $agedbusname=mysql_result($result73,0,"businessname");
          $agedunit=mysql_result($result73,0,"unit");
          $districtid=mysql_result($result73,0,"districtid");

          $query73 = "SELECT bad_ar,bad_ar_email FROM district WHERE districtid = '$districtid'";
          $result73 = DB_Proxy::query($query73);

          $bad_ar=mysql_result($result73,0,"bad_ar");
          $bad_ar_email=mysql_result($result73,0,"bad_ar_email");

          $from = "From: support@treatamerica.com\r\n" . 
          "X-Priority: 1\r\n" . 
          "Priority: Urgent\r\n" . 
          "importance: high"; 
          $subject="Outstanding Client";
          $message="$user has created an invoice (#$invoiceid) for $agedname, Unit: #$agedunit $agedbusname \n\n$agedname has outstanding invoices over 90 days.\n\nSupport\n\n";
          if($bad_ar == 1 && $bad_ar_email != ""){mail($bad_ar_email, $subject, $message, $from);}
       }
    }

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM invoicedetail WHERE invoiceid = '$invoiceid'";
    $result = DB_Proxy::query($query);
    $num=mysql_numrows($result);
    //mysql_close();

    $num--;
    $newtotal=0;
    $subtotal=0;
    $newtaxtotal=0;
    while ($num>=0){
       $itemid=mysql_result($result,$num,"itemid");
       $qty=mysql_result($result,$num,"qty");
       $price=mysql_result($result,$num,"price");
       $taxed=mysql_result($result,$num,"taxed");
       $menu_item_id=mysql_result($result,$num,"menu_item_id");

       if ($menu_item_id!=0 && $taxable == "on" && $taxed == "on"){
          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query9 = "SELECT * FROM menu_items WHERE menu_item_id = '$menu_item_id'";
          $result9 = DB_Proxy::query($query9);
          //mysql_close();

          $sales_acct=mysql_result($result9,0,"sales_acct");
          $tax_acct=mysql_result($result9,0,"tax_acct");
          $nontax_acct=mysql_result($result9,0,"nontax_acct");

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query9 = "UPDATE invoicedetail SET sales_acct = '$sales_acct', tax_acct = '$tax_acct' WHERE itemid = '$itemid'";
          $result9 = DB_Proxy::query($query9);
          //mysql_close();
       }
       elseif ($menu_item_id!=0 && $taxable == "off" && $taxed == "on"){
          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query9 = "SELECT * FROM menu_items WHERE menu_item_id = '$menu_item_id'";
          $result9 = DB_Proxy::query($query9);
          //mysql_close();

          $sales_acct=mysql_result($result9,0,"sales_acct");
          $tax_acct=mysql_result($result9,0,"tax_acct");
          $nontax_acct=mysql_result($result9,0,"nontax_acct");

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query9 = "UPDATE invoicedetail SET sales_acct = '$nontax_acct', tax_acct = '0' WHERE itemid = '$itemid'";
          $result9 = DB_Proxy::query($query9);
          //mysql_close();
       }
       elseif ($menu_item_id!=0 && $taxable == "on" && $taxed == "off"){
          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query9 = "SELECT * FROM menu_items WHERE menu_item_id = '$menu_item_id'";
          $result9 = DB_Proxy::query($query9);
          //mysql_close();

          $sales_acct=mysql_result($result9,0,"sales_acct");
          $tax_acct=mysql_result($result9,0,"tax_acct");
          $nontax_acct=mysql_result($result9,0,"nontax_acct");

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query9 = "UPDATE invoicedetail SET sales_acct = '$nontax_acct', tax_acct = '0' WHERE itemid = '$itemid'";
          $result9 = DB_Proxy::query($query9);
          //mysql_close();
       }
       elseif ($menu_item_id!=0 && $taxable == "off" && $taxed == "off"){
          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query9 = "SELECT * FROM menu_items WHERE menu_item_id = '$menu_item_id'";
          $result9 = DB_Proxy::query($query9);
          //mysql_close();

          $sales_acct=mysql_result($result9,0,"sales_acct");
          $tax_acct=mysql_result($result9,0,"tax_acct");
          $nontax_acct=mysql_result($result9,0,"nontax_acct");

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query9 = "UPDATE invoicedetail SET sales_acct = '$nontax_acct', tax_acct = '0' WHERE itemid = '$itemid'";
          $result9 = DB_Proxy::query($query9);
          //mysql_close();
       }

       if ($taxable=="on" && $taxed =="on"){$newtotal=$newtotal+($qty*$price);$newtaxtotal=$newtaxtotal+($qty*$price);}
       else {$newtotal=$newtotal+($qty*$price);}
       $subtotal=$subtotal+($qty*$price);
       $num--;
    }

    $subtotal=money($subtotal);
    $newtotal=money($newtotal);
    $newtaxtotal=money($newtaxtotal);
    if ($taxable=="on"){$taxes=money(($tax/100)*$newtaxtotal);}
    if ($flatsrvchrg=="1" || ($taxable!="on" && $flatsrvchrg=="2")){$servamt=money(($service/100)*$subtotal);}
    elseif ($flatsrvchrg=="2"  && $taxable=="on"){$servamt=money(($service/100)*$subtotal);$taxes=money(($tax/100)*($newtaxtotal+$servamt));}
    elseif ($flatsrvchrg=="3"){$servamt=money($service);}
    $newtotal=$subtotal+$servamt+$taxes;

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "UPDATE invoice SET taxtotal = '$taxes', total = '$newtotal', servamt = '$servamt' WHERE invoiceid = '$invoiceid'";
    $result = DB_Proxy::query($query);
    //mysql_close();

///INSERT UPDATE CASH OVER/SHORT////////////////////////////////////////////////////////////////////


//////CREDITS/////////////////////////////////////////////////////////////////////////////////////////////////////
    $hc1=0;
    $hc2=0;
    $pp1=0;
    $pp2=0;

    $day1=$previnvdate;
    if ($date!=$previnvoice){$day2=$date;}
    else {$day2="1900-01-01";}

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM credittype ORDER BY credittypename DESC";
    $result = DB_Proxy::query($query);
    $num=mysql_numrows($result);
    //mysql_close();

    $num--;
    $count=0;
    $value1="0";
    $heldcheck=0;
    $day1column=0;
    $day2column=0;

    while ($num>=0){
       $credittypename=mysql_result($result,$num,"credittypename");
       $credittypeid=mysql_result($result,$num,"credittypeid");
       
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM credits WHERE companyid = '$companyid' AND businessid = '$businessid' AND credittype = '$credittypeid' AND ((is_deleted = '0') || (is_deleted = '1' && del_date >= '$day1')) ORDER BY credittype,creditname DESC";
       $result2 = DB_Proxy::query($query2);
       $num2=mysql_numrows($result2);
       //mysql_close();

       $num2--;
       $day1coltot=0;
       $day2coltot=0;

       while ($num2>=0){
          $creditrowtotal=0;
          $creditname=mysql_result($result2,$num2,"creditname");
          $creditid=mysql_result($result2,$num2,"creditid");
          $account=mysql_result($result2,$num2,"account");
          $invoicesales=mysql_result($result2,$num2,"invoicesales");
          $invoicesales_notax=mysql_result($result2,$num2,"invoicesales_notax");
          $invoicetax=mysql_result($result2,$num2,"invoicetax");
          $servchrg=mysql_result($result2,$num2,"servchrg");
          $salescode=mysql_result($result2,$num2,"salescode");
          $prepayment=mysql_result($result2,$num2,"heldcheck");

          if ($previnvdate==$date){$count99=0;}
          else {$count99=1;}

          for ($counter=0;$counter<=$count99;$counter++){
             if ($counter==0){$day=$day1;}
             elseif ($counter==1){$day=$day2;}
///MANUAL
             if ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0&&$salescode==0&&$prepayment!=1){
                  //mysql_connect($dbhost,$username,$password);
                  //@mysql_select_db($database) or die( "Unable to select database");
                  $query5 = "SELECT amount FROM creditdetail WHERE creditid = '$creditid' AND date = '$day'";
                  $result5 = DB_Proxy::query($query5);
                  $num5=mysql_numrows($result5);
                  //mysql_close();

                  if ($num5==1&&$invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0){
                     $prevamount=mysql_result($result5,0,"amount");
                     $prevamount = money($prevamount);
                  }
                  else{$prevamount="0";}

             }
///INVOICE SALES
             elseif ($invoicesales==1&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0&&$salescode==0&&$prepayment!=1){
                  //mysql_connect($dbhost,$username,$password);
                  //@mysql_select_db($database) or die( "Unable to select database");
                  $query5 = "SELECT total,taxtotal,servamt FROM invoice WHERE type = '1' AND companyid = '$companyid' AND businessid = '$businessid' AND date = '$day' AND taxable = 'on' AND salescode = '0' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = DB_Proxy::query($query5);
                  $num6=mysql_numrows($result5);
                  //mysql_close();
                  $num6--;
                  $prevamount=0;
                  while ($num6>=0){
                     $invtotal=mysql_result($result5,$num6,"total");
                     $invtax=mysql_result($result5,$num6,"taxtotal");
                     $servamt=mysql_result($result5,$num6,"servamt");
                     $prevamount=$prevamount+($invtotal-$invtax-$servamt);
                     $num6--;
                  }
                  $prevamount=money($prevamount);
                  if ($day==$day1){$hc1=$hc1+$prevamount;}
                  elseif ($day==$day2){$hc2=$hc2+$prevamount;}
             }
///INVOICE TAX EXEMPT
             elseif ($invoicesales==0&&$invoicesales_notax==1&&$invoicetax==0&&$servchrg==0&&$salescode==0&&$prepayment!=1){
                  //mysql_connect($dbhost,$username,$password);
                  //@mysql_select_db($database) or die( "Unable to select database");
                  $query5 = "SELECT total,taxtotal,servamt FROM invoice WHERE type = '1' AND companyid = '$companyid' AND businessid = '$businessid' AND date = '$day' AND taxable = 'off' AND salescode = '0' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = DB_Proxy::query($query5);
                  $num6=mysql_numrows($result5);
                  //mysql_close();
                  $num6--;
                  $prevamount=0;
                  while ($num6>=0){
                     $invtotal=mysql_result($result5,$num6,"total");
                     $servamt=mysql_result($result5,$num6,"servamt");
                     $prevamount=$prevamount+$invtotal-$servamt;
                     $num6--;
                  }
                  $prevamount=money($prevamount);
                  if ($day==$day1){$hc1=$hc1+$prevamount;}
                  elseif ($day==$day2){$hc2=$hc2+$prevamount;}
             }
///INVOICE TAX
             elseif ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==1&&$servchrg==0&&$salescode==0&&$prepayment!=1){
                  //mysql_connect($dbhost,$username,$password);
                  //@mysql_select_db($database) or die( "Unable to select database");
                  $query5 = "SELECT taxtotal FROM invoice WHERE type = '1' AND companyid = '$companyid' AND businessid = '$businessid' AND date = '$day' AND taxable = 'on' AND salescode = '0' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = DB_Proxy::query($query5);
                  $num6=mysql_numrows($result5);
                  //mysql_close();
                  $num6--;
                  $prevamount=0;
                  while ($num6>=0){
                     $taxtotal=mysql_result($result5,$num6,"taxtotal");
                     $prevamount=$prevamount+$taxtotal;
                     $num6--;
                  }
                  $prevamount=money($prevamount);
                  if ($day==$day1){$hc1=$hc1+$prevamount;}
                  elseif ($day==$day2){$hc2=$hc2+$prevamount;}
             }
///INVOICE SERVICE CHARGE
             elseif ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==1&&$salescode==0&&$prepayment!=1){
                  //mysql_connect($dbhost,$username,$password);
                  //@mysql_select_db($database) or die( "Unable to select database");
                  $query5 = "SELECT servamt FROM invoice WHERE type = '1' AND companyid = '$companyid' AND businessid = '$businessid' AND date = '$day' AND salescode = '0' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = DB_Proxy::query($query5);
                  $num6=mysql_numrows($result5);
                  //mysql_close();
                  $num6--;
                  $prevamount=0;
                  while ($num6>=0){
                     $servamt=mysql_result($result5,$num6,"servamt");
                     $prevamount=$prevamount+$servamt;
                     $num6--;
                  }
                  $prevamount=money($prevamount);
                  if ($day==$day1){$hc1=$hc1+$prevamount;}
                  elseif ($day==$day2){$hc2=$hc2+$prevamount;}
             }
///INVOICE SALES (SALESCODE)/////////////////////////////////////////////////////////////////
             elseif ($invoicesales==1&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0&&$salescode!=0&&$prepayment!=1){
                  //mysql_connect($dbhost,$username,$password);
                  //@mysql_select_db($database) or die( "Unable to select database");
                  $query5 = "SELECT total,taxtotal,servamt FROM invoice WHERE type = '1' AND companyid = '$companyid' AND businessid = '$businessid' AND date = '$day' AND taxable = 'on' AND salescode = '$salescode' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = DB_Proxy::query($query5);
                  $num6=mysql_numrows($result5);
                  //mysql_close();
                  $num6--;
                  $prevamount=0;
                  while ($num6>=0){
                     $invtotal=mysql_result($result5,$num6,"total");
                     $invtax=mysql_result($result5,$num6,"taxtotal");
                     $servamt=mysql_result($result5,$num6,"servamt");
                     $prevamount=$prevamount+($invtotal-$invtax-$servamt);
                     $num6--;
                  }
                  $prevamount=money($prevamount);
                  if ($day==$day1){$hc1=$hc1+$prevamount;}
                  elseif ($day==$day2){$hc2=$hc2+$prevamount;}
             }
///INVOICE TAX EXEMPT (SALESCODE)
             elseif ($invoicesales==0&&$invoicesales_notax==1&&$invoicetax==0&&$servchrg==0&&$salescode!=0&&$prepayment!=1){
                  //mysql_connect($dbhost,$username,$password);
                  //@mysql_select_db($database) or die( "Unable to select database");
                  $query5 = "SELECT total,taxtotal,servamt FROM invoice WHERE type = '1' AND companyid = '$companyid' AND businessid = '$businessid' AND date = '$day' AND taxable = 'off'  AND salescode = '$salescode' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = DB_Proxy::query($query5);
                  $num6=mysql_numrows($result5);
                  //mysql_close();
                  $num6--;
                  $prevamount=0;
                  while ($num6>=0){
                     $invtotal=mysql_result($result5,$num6,"total");
                     $servamt=mysql_result($result5,$num6,"servamt");
                     $prevamount=$prevamount+$invtotal-$servamt;
                     $num6--;
                  }
                  $prevamount=money($prevamount);
                  if ($day==$day1){$hc1=$hc1+$prevamount;}
                  elseif ($day==$day2){$hc2=$hc2+$prevamount;}
             }
///INVOICE TAX (SALESCODE)
             elseif ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==1&&$servchrg==0&&$salescode!=0&&$prepayment!=1){
                  //mysql_connect($dbhost,$username,$password);
                  //@mysql_select_db($database) or die( "Unable to select database");
                  $query5 = "SELECT taxtotal FROM invoice WHERE type = '1' AND companyid = '$companyid' AND businessid = '$businessid' AND date = '$day' AND taxable = 'on'  AND salescode = '$salescode' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = DB_Proxy::query($query5);
                  $num6=mysql_numrows($result5);
                  //mysql_close();
                  $num6--;
                  $prevamount=0;
                  while ($num6>=0){
                     $taxtotal=mysql_result($result5,$num6,"taxtotal");
                     $prevamount=$prevamount+$taxtotal;
                     $num6--;
                  }
                  $prevamount=money($prevamount);
                  if ($day==$day1){$hc1=$hc1+$prevamount;}
                  elseif ($day==$day2){$hc2=$hc2+$prevamount;}
             }
///INVOICE SERVICE CHARGE (SALESCODE)
             elseif ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==1&&$salescode!=0&&$prepayment!=1){
                  //mysql_connect($dbhost,$username,$password);
                  //@mysql_select_db($database) or die( "Unable to select database");
                  $query5 = "SELECT servamt FROM invoice WHERE type = '1' AND companyid = '$companyid' AND businessid = '$businessid' AND date = '$day'  AND salescode = '$salescode' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = DB_Proxy::query($query5);
                  $num6=mysql_numrows($result5);
                  //mysql_close();
                  $num6--;
                  $prevamount=0;
                  while ($num6>=0){
                     $servamt=mysql_result($result5,$num6,"servamt");
                     $prevamount=$prevamount+$servamt;
                     $num6--;
                  }
                  $prevamount=money($prevamount);
                  if ($day==$day1){$hc1=$hc1+$prevamount;}
                  elseif ($day==$day2){$hc2=$hc2+$prevamount;}
             }
///PREPAYMENT
             elseif ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0&&$prepayment==1){
                  //mysql_connect($dbhost,$username,$password);
                  //@mysql_select_db($database) or die( "Unable to select database");
                  $query5 = "SELECT amount FROM invtender WHERE businessid = '$businessid' AND date = '$day' AND sameday = '1'";
                  $result5 = DB_Proxy::query($query5);
                  $num6=mysql_numrows($result5);
                  //mysql_close();
                  $num6--;
                  $prevamount=0;
                  while ($num6>=0){
                     $amount=mysql_result($result5,$num6,"amount");
                     $prevamount=$prevamount+$amount;
                     $num6--;  
                  }
                  $prevamount=money($prevamount);
                  if ($day==$day1){$pp1=$pp1+$prevamount;}
                  elseif ($day==$day2){$pp2=$pp2+$prevamount;}
             }

             if ($day==$day1){$day1column=$day1column+$prevamount;}
             elseif ($day==$day2){$day2column=$day2column+$prevamount;}         

          }
      
          $num2--;
          $count++;
       }

       $day1column=money($day1column+$day1coltot);
       $day2column=money($day2column+$day2coltot);

       $num--;
    }    

//////DEBITS///////////////////////////////////////////////////////////////////////////////////////

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM debittype ORDER BY debittypename DESC";
    $result = DB_Proxy::query($query);
    $num=mysql_numrows($result);
    //mysql_close();

    $num--;
    $count=0;
    $value1="0";
    $day1column1=0;
    $day2column1=0;

    while ($num>=0){
       $debittypename=mysql_result($result,$num,"debittypename");
       $debittypeid=mysql_result($result,$num,"debittypeid");
       
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM debits WHERE companyid = '$companyid' AND businessid = '$businessid' AND debittype = '$debittypeid' AND ((is_deleted = '0') || (is_deleted = '1' && del_date >= '$day1')) ORDER BY debittype,debitname DESC";
       $result2 = DB_Proxy::query($query2);
       $num2=mysql_numrows($result2);
       //mysql_close();

       $num2--;
       $day1coltot=0;
       $day2coltot=0;

       while ($num2>=0){
          $debitrowtotal=0;
          $debitname=mysql_result($result2,$num2,"debitname");
          $debitid=mysql_result($result2,$num2,"debitid");
          $account=mysql_result($result2,$num2,"account");
          $prepayment=mysql_result($result2,$num2,"heldcheck");
          $showaccount=substr($account,0,3);

          if ($previnvdate==$date){$count99=0;}
          else {$count99=1;}

          for ($counter=0;$counter<=$count99;$counter++){
             if ($counter==0){$day=$day1;}
             elseif ($counter==1){$day=$day2;}

             if ($prepayment!=1){
                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query5 = "SELECT amount FROM debitdetail WHERE companyid = '$companyid' AND businessid = '$businessid' AND debitid = '$debitid' AND date = '$day'";
                $result5 = DB_Proxy::query($query5);
                $num5=mysql_numrows($result5);
                //mysql_close();

                if ($num5==1){$prevamount=mysql_result($result5,0,"amount");
                   $prevamount = money($prevamount);
                }
                else{$prevamount="0";}

             }
             elseif ($prepayment==1){
                if ($day==$day1){
                   $prevamount=$hc1;
                   if ($prevamount<=0){$prevamount=0;}
                }
                
                elseif ($day==$day2){
                   $prevamount=$hc2;
                   if ($prevamount<=0){$prevamount=0;}
                }
                $prevamount = money($prevamount);
             }

             if ($day==$day1){$day1coltot=money($day1coltot+$prevamount);}
             elseif ($day==$day2){$day2coltot=money($day2coltot+$prevamount);}
          }
          $num2--;
          $count++;
       }     

       $day1column1=money($day1column1+$day1coltot);
       $day2column1=money($day2column1+$day2coltot);
       $num--;
    }

///CASH SHORT//////////////////

    $day1=$previnvdate;
    if ($date!=$previnvoice){$day2=$date;}
    else {$day2="1900-01-01";}

    if ($previnvdate==$date){$count99=0;}
    else {$count99=1;}

    for ($counter=0;$counter<=$count99;$counter++){

       if ($counter==0){$cashtotal=money($day1column-$day1column1);}
       elseif ($counter==1){$cashtotal=money($day2column-$day2column1);}

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query = "SELECT * FROM cash WHERE companyid = '$companyid' AND businessid = '$businessid' AND date = '$day1'";
          $result = DB_Proxy::query($query);
          $num=mysql_numrows($result);
          //mysql_close();

          if ($cashtotal>=0){$cashtype=0;}
          else {$cashtype=1;$cashtotal=$cashtotal*-1;}

          if ($num==1){
             $query="UPDATE cash SET amount = '$cashtotal', type = '$cashtype' WHERE businessid = '$businessid' AND date = '$day1'";
          }
          else{
             $query = "INSERT INTO cash (companyid,businessid,date,amount,type) VALUES ('$companyid','$businessid','$day1','$cashtotal','$cashtype')";
          }

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $result = DB_Proxy::query($query);
          //mysql_close();

       if ($day1!=$day2){$day1=$day2;}
       else {$day2="1900-01-01";}
    }
////////////////////////////////////////////////////////////////////////////////////////////////// 

    //mysql_close();  

    if ($goto=="2"){$location="saveinvoice.php?bid=$businessid&cid=$companyid&invoiceid=$invoiceid";}
    elseif ($direct=="1"){$location="saveinvoice.php?bid=$businessid&cid=$companyid&invoiceid=$invoiceid&direct=1";}
    else {$location="saveinvoice.php?bid=$businessid&cid=$companyid&invoiceid=$invoiceid";}
    header('Location: ./' . $location);
  }
}
?>