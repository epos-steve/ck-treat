<?php
define( 'DOC_ROOT', realpath(dirname(__FILE__).'/../'));
define( 'SITECH_APP_PATH', realpath( dirname( DOC_ROOT ) . '/application' ) );
require_once( SITECH_APP_PATH . DIRECTORY_SEPARATOR . 'bootstrap.php' );
require_once( DOC_ROOT . DIRECTORY_SEPARATOR . 'bootstrap.php' );
require_once( 'EE/Helpers/html.php' );

setlocale( LC_MONETARY, 'en_US.utf8' );

?>
<!DOCTYPE html
	PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title>Treat America</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" type="text/css" href="/assets/js/preload/preLoadingMessage.css" />
		<link rel="stylesheet" type="text/css" href="/assets/css/ta/generic.css" />
		<link rel="stylesheet" type="text/css" href="/assets/css/ta/buscontrol.css" />
		<script type="text/javascript" src="/assets/js/jquery-1.4.3.min.js"></script>
		<script type="text/javascript" src="/assets/js/preload/preLoadingMessage.js"></script>
		<script type="text/javascript" src="/assets/js/ta/buscontrol2-functions.js"></script>
		<script type="text/javascript" src="/assets/js/dynamicdrive-disableform.js"></script>
	</head>
	<body class="buscontrol2">
<?php
// make sure they pass the cookie login
$passed_cookie_login = require_once( realpath( DOC_ROOT . '/../lib/inc/cookie-login.php' ) );
$page_business = Treat_Model_Business_Singleton::getSingleton();

#if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
#	echo '<pre>';
#	echo '$_SESSION = ';
#	var_dump( $_SESSION );
#	echo '$GLOBALS[\'user\'] = ';
#	var_dump( $GLOBALS['user'] );
#	echo '$GLOBALS[\'pass\'] = ';
#	var_dump( $GLOBALS['pass'] );
#	echo '</pre>';
#}

if (
	$passed_cookie_login
	&& (
		(
			$GLOBALS['security_level'] == 1
			&& $GLOBALS['bid'] == $page_business->getBusinessId()
			&& $GLOBALS['cid'] == $page_business->getCompanyId()
		)
		|| (
			$GLOBALS['security_level'] > 1
			&& $GLOBALS['cid'] == $page_business->getCompanyId()
		)
	)
) {
	
	$prevperiod = \EE\Controller\Base::getPostOrGetVariable('prevperiod');
	$nextperiod = \EE\Controller\Base::getPostOrGetVariable('nextperiod');
	$viewmonth = \EE\Controller\Base::getPostOrGetVariable('viewmonth');
	
	$viewmonth  = Treat_Date::getBeginningOfMonth( $viewmonth, true );	
	
	$audit = \EE\Controller\Base::getPostOrGetVariable('audit');
	$show_sales = \EE\Controller\Base::getPostOrGetVariable('show_sales');
	
	if ( 'false' == $show_sales ) {
		$show_sales = false;
	}

	require_once( 'lib/class.credits.php' );
	require_once( 'lib/class.labor.php' );

	$style = 'text-decoration:none';
	$year = date( 'Y' );
	$month = date( 'm' );
	$today = date( 'Y-m-d' );
?>

		<table cellspacing="0" cellpadding="0" border="0">
			<tr>
				<td colspan="2">
					<a href="businesstrack.php"><img src="logo.jpg" border="0" height="43" width="205" alt="logo" /></a>
				</td>
			</tr>

<?php
#	$company = Treat_Model_Company::getCompanyById( $company_id );
	//mysql_connect( $dbhost, $username, $password );
	//@mysql_select_db( $database ) or die( mysql_error() );
	$query = sprintf( "
			SELECT *
			FROM company
			WHERE companyid = '%s'
		"
		,$GLOBALS['companyid']
	);
	$result = Treat_DB_ProxyOld::query( $query );
	//mysql_close();

	$companyname = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'companyname' );
	$week_end = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'week_end' );
	$week_start = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'week_start' );

	//mysql_connect( $dbhost, $username, $password );
	//@mysql_select_db( $database ) or die( mysql_error() );
	$query = sprintf( "
			SELECT *
			FROM security
			WHERE securityid = '%s'
		"
		,$GLOBALS['mysecurity']
	);
	$result = Treat_DB_ProxyOld::query( $query );
	//mysql_close();

	$sec_bus_sales = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_sales' );
	$sec_bus_invoice = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_invoice' );
	$sec_bus_order = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_order' );
	$sec_bus_payable = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_payable' );
	$sec_bus_inventor1 = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_inventor1' );
	$sec_bus_control = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_control' );
	$sec_bus_menu = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_menu' );
	$sec_bus_order_setup = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_order_setup' );
	$sec_bus_request = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_request' );
	$sec_route_collection = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'route_collection' );
	$sec_route_labor = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'route_labor' );
	$sec_route_detail = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'route_detail' );
	$sec_bus_labor = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_labor' );
	$sec_bus_budget = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'bus_budget' );


	if ( $GLOBALS['operate'] == 5 ) {
		$paydays = 10;
	}
	elseif ( $GLOBALS['operate'] == 6 ) {
		$paydays == 12;
	}
	elseif ( $GLOBALS['operate'] == 7 ) {
		$paydays == 14;
	}

	//<tr bgcolor=#999999><td colspan=3 height=1></td></tr>
?>


			<tr bgcolor="#ccccff">
				<td width="1%">
					<img src="weblogo.jpg" width="80" height="75" alt="Treat America logo" />
				</td>
				<td>
					<div style="font-size: 1.1em; font-weight: bold;"><?php echo $GLOBALS['businessname'], ' #', $GLOBALS['unit_num']; ?></div>
					<div><?php echo $GLOBALS['street']; ?></div>
					<div><?php echo $GLOBALS['city'], ', ', $GLOBALS['state'], ' ', $GLOBALS['zip']; ?></div>
					<div><?php echo $GLOBALS['phone']; ?></div>
				</td>
				<td align="right" valign="top">
					<br/>
					<form action="editbus.php" method="post">
						<div>
							<input type="hidden" name="username" value="<?php echo $GLOBALS['user']; ?>" />
							<input type="hidden" name="password" value="<?php echo $GLOBALS['pass']; ?>" />
							<input type="hidden" name="businessid" value="<?php echo $GLOBALS['businessid']; ?>" />
							<input type="hidden" name="companyid" value="<?php echo $GLOBALS['companyid']; ?>" />
							<input type="submit" value=" Store Setup " />
						</div>
					</form>
				</td>
			</tr>

<?php
	//<tr bgcolor=#999999><td colspan=3 height=1></td></tr>
?>


			<tr>
				<td colspan="3" class="header_nav">
<?php
				echo $_SESSION->getUser()->getBusinessMenu( $GLOBALS['businessid'], $GLOBALS['companyid'], 'buscontrol' );
?>
				</td>
			</tr>
		</table>
	<div>&nbsp;</div>


<?php

	if ( $GLOBALS['security_level'] > 0 ) {
		$showyear = $year;
		$gomonth  = date( 'm', $viewmonth );
		$goyear   = date( 'Y', $viewmonth );
?>

	<table>
		<tr>
			<td>
				<form action="gocontrol.php" method="post" onSubmit="return disableForm(this);">
					<div>
						<input type="hidden" name="companyid" value="<?php echo $GLOBALS['companyid']; ?>" />
						<input type="hidden" name="viewmonth" value="<?php echo date( 'Y-m-01', $viewmonth ); ?>" />
						<input type="hidden" name="import_pl" value="<?php echo $GLOBALS['import_pl']; ?>" />
						<select name="gomonth">
<?php
		$range = array(
			'01' => 'Jan',
			'02' => 'Feb',
			'03' => 'Mar',
			'04' => 'Apr',
			'05' => 'May',
			'06' => 'Jun',
			'07' => 'Jul',
			'08' => 'Aug',
			'09' => 'Sep',
			'10' => 'Oct',
			'11' => 'Nov',
			'12' => 'Dec',
		);

		if ( $viewmonth ) {
			$date = date( 'm', $viewmonth );
		}
		else {
			$date = date( 'm' );
		}
		echo html::select_options( $range, $date );
		
?>
						</select>
						<select name="goyear">


<?php
		// 2037 is the maximum year a 32bit integer can create, if more is
		// needed we won't be able to calculate the dates on 32bit systems.
		$range = range( date( 'Y' ), date( 'Y', strtotime( '-4 years' ) ) );
		$range = array_combine( $range, $range );

		if ( $viewmonth ) {
			$date = date( 'Y', $viewmonth );
		}
		else {
			$date = date( 'Y' );
		}
		echo html::select_options($range, $date );
?>


						</select>
						<select name="gobus">


<?php
		//mysql_connect( $dbhost, $username, $password );
		//@mysql_select_db( $database ) or die( mysql_error() );
		if ( $GLOBALS['security_level'] > 6 ) {
			$query = sprintf( "
					SELECT *
					FROM business
					WHERE companyid = '%s'
					ORDER BY businessname DESC
				"
				,$GLOBALS['companyid']
			);
		}
		elseif ( $GLOBALS['security_level'] == 1 ) {
			$query = sprintf( "
					SELECT *
					FROM business
					WHERE companyid = '%s'
						AND businessid = '%s'
					ORDER BY businessname DESC
				"
				,$GLOBALS['companyid']
				,$GLOBALS['bid']
			);
		}
		elseif ( $GLOBALS['security_level'] == 2 ) {
			$query = sprintf( "
					SELECT *
					FROM business
					WHERE companyid = '%s'
						AND (
							businessid = '%s'
							OR businessid = '%s'
							OR businessid = '%s'
							OR businessid = '%s'
							OR businessid = '%s'
							OR businessid = '%s'
							OR businessid = '%s'
							OR businessid = '%s'
							OR businessid = '%s'
							OR businessid = '%s'
						)
					ORDER BY businessname DESC
				"
				,$GLOBALS['companyid']
				,$GLOBALS['bid']
				,$GLOBALS['bid2']
				,$GLOBALS['bid3']
				,$GLOBALS['bid4']
				,$GLOBALS['bid5']
				,$GLOBALS['bid6']
				,$GLOBALS['bid7']
				,$GLOBALS['bid8']
				,$GLOBALS['bid9']
				,$GLOBALS['bid10']
			);
		}
		elseif ( $GLOBALS['security_level'] > 2 && $GLOBALS['security_level'] < 7 ) {
			$districtquery = '';
#			if ( $GLOBALS['security_level'] > 2 && $GLOBALS['security_level'] < 7 ) {
				//mysql_connect( $dbhost, $username, $password );
				//@mysql_select_db( $database ) or die( mysql_error() );
				$query = sprintf( "
						SELECT *
						FROM login_district
						WHERE loginid = '%s'
					"
					,$GLOBALS['loginid']
				);
				$result = Treat_DB_ProxyOld::query( $query );
				$num = Treat_DB_ProxyOld::mysql_numrows( $result );
				//mysql_close();

				$num--;
				while ( $num >= 0 ) {
					$districtid = @Treat_DB_ProxyOld::mysql_result( $result, $num, "districtid" );
					if ( $districtquery == '' ) {
						$districtquery = "districtid = '$districtid'";
					}
					else {
						$districtquery = "$districtquery OR districtid = '$districtid'";
					}
					$num--;
				}
#			}

			$query = sprintf( "
					SELECT *
					FROM business
					WHERE companyid = '%s'
						AND (%s)
					ORDER BY businessname DESC
				"
				,$GLOBALS['companyid']
				,$districtquery
			);
		}
		$result = Treat_DB_ProxyOld::query( $query );
		$num = Treat_DB_ProxyOld::mysql_numrows( $result );
		//mysql_close();

		$num--;
		while ( $num >= 0 ) {
			$businessname = @Treat_DB_ProxyOld::mysql_result( $result, $num, "businessname" );
			$busid = @Treat_DB_ProxyOld::mysql_result( $result, $num, "businessid" );
			
			if ( false !== stripos( $businessname, '<font' ) ) {
				$color = preg_replace(
					'@<font color=(.+)>(.+)(</font>)?@'
					,' style="color: $1;"'
					,$businessname
				);
				$businessname = preg_replace(
					'@<font color=(.+)>(.+)(</font>)?@'
					,'$2'
					,$businessname
				);
			}
			else {
				$color = '';
			}
			
			if ( $busid == $GLOBALS['businessid'] ) {
				$show = 'selected="selected"';
			}
			else {
				$show = '';
			}

			echo '<option value="', $busid, '" ', $show, $color, '>', $businessname, '</option>';
			$num--;
		}

?>


						</select>
						<input type="submit" value="GO" />
					</div>
				</form>
			</td>
			<td></td>
		</tr>
	</table>
	<div>&nbsp;</div>


<?php
	}
	
	// ************* END HEADER

	//////START TABLE//////////////////////////////////////////////////////////////////////////////////

	if ( $viewmonth != '' ) {
		$startdate = date( 'Y-m-01', $viewmonth );
		$temp_date = $today;
	}
	else {
		$startdate = $today;
		$temp_date = $today;
	}

	//////////////////WEEK GROUPS
	$whichweek = 1;
	$cs_week_start[$whichweek] = $startdate;
	$cs_enddate_year = substr( $startdate, 0, 4 );
	$cs_enddate_month = substr( $startdate, 5, 2 );
	$cs_enddate_month++;
	if ( $cs_enddate_month == 13 ) {
		$cs_enddate_month = 1;$cs_enddate_year++;
	}
	if ( $cs_enddate_month < 10 ) {
		$cs_enddate_month = "0$cs_enddate_month";
	}
	$cs_enddate = "$cs_enddate_year-$cs_enddate_month-01";
	$cs_enddate = prevday( $cs_enddate );

	$tempdate = $startdate;
	$cs_week_end = array();
	while ( $tempdate <= $cs_enddate ) {
		if ( dayofweek( $tempdate ) == $week_end || $tempdate == $cs_enddate ) {
			$cs_week_end[$whichweek] = $tempdate;
			$whichweek++;
			$tempdate = nextday( $tempdate );
			if ( $tempdate <= $cs_enddate ) {
				$cs_week_start[$whichweek] = $tempdate;
			}
		}
		else {
			$tempdate = nextday( $tempdate );
		}
	}
	//////////////////END WEEK GROUPS

	////header
	if ( isset( $uri ) ) {
		$page = $uri->getPath();
	}
	else {
		$page = '/ta/buscontrol2.php';
	}
?>


	<div class="page_wrapper">
<?php
if ( !IN_PRODUCTION ) {
	echo $_SESSION->getFlashDebug( "\t\t\t<pre class=\"flash_debug\">%s</pre>\n" );
}
echo $_SESSION->getFlashError(   "\t\t\t<div class=\"flash_error\">%s</div>\n"   );
echo $_SESSION->getFlashMessage( "\t\t\t<div class=\"flash_message\">%s</div>\n" );
?>


	<table width="100%" cellspacing="0" cellpadding="0" class="month_header">
		<tr>
			<td class="prev">
				[<a href="<?php echo $page; ?>?bid=<?php
						echo $GLOBALS['businessid'];
					?>&amp;cid=<?php
						echo $GLOBALS['companyid'];
					?>&amp;viewmonth=<?php
						echo date( 'Y-m-01', Treat_Date::getPreviousMonthBeginning( $viewmonth ) );
				?>"><span>PREV</span></a>]
			</td>
			<td class="month_label">
				<?php echo date( 'F Y', $viewmonth ); ?>
			</td>
			<td class="next">
				[<a href="<?php echo $page; ?>?bid=<?php
						echo $GLOBALS['businessid'];
					?>&amp;cid=<?php
						echo $GLOBALS['companyid'];
					?>&amp;viewmonth=<?php
						echo date( 'Y-m-01', Treat_Date::getNextMonthBeginning( $viewmonth ) )
				?>"><span>NEXT</span></a>]
			</td>
		</tr>

	</table>
	<div>&nbsp;</div>



<?php
	
	/////////////
	/////data////
	/////////////
	
	$cs_week_end = Treat_Date::getMonthOfFridays( $viewmonth );
	$controlSheet = Treat_Model_GeneralLedger_ControlSheet::getSingleton(
		Treat_Model_GeneralLedger_AccountGroup::getAccountGroups(
			$GLOBALS['businessid']
			,$viewmonth
			,$cs_week_end
		)
	);
	$controlSheet->setBusinessId( $GLOBALS['businessid'] );
	$controlSheet->setViewMonth( $viewmonth );
	$controlSheet->setMonthOfFridays( $cs_week_end );
	if ( $show_sales ) {
		$controlSheet->setOverrideDisplayWeek( $show_sales );
	}
	$operating_days = $controlSheet->getOperatingDaysAll();
	$accountGroups = $controlSheet->process( $viewmonth, $cs_week_end );
	$cs_final = $controlSheet->isFinal();
?>
		<table cellspacing="0" cellpadding="0" class="control_sheet_data">
			<thead>
				<tr>
					<td>&nbsp;CONTROL SHEET</td>
<?php
	foreach ( $cs_week_end as $key => $value ) {
		$out = sprintf(
			'<span>%s <span>WEEK%s</span></span>'
			,date( 'm/d/Y', $value )
			,$key
		);
		if ( $GLOBALS['security_level'] >= 8 ) {
			$out = sprintf(
				'<a href="utility.php?cid=%s&amp;import_bid=%s&amp;date=%s#bottom">%s</a>'
				,$GLOBALS['companyid']
				,$GLOBALS['businessid']
				,date( 'Y-m-d', $value )
				,$out
			);
		}
?>
					<td colspan="2" class="week_label"><?php echo $out; ?></td>
<?php
	}
?>
					<td colspan="6" class="month_label">TOTAL</td>
				</tr>
				
				
				
				
				<tr>
					<td>
<?php
	if ( 8 <= $_SESSION->getUser()->security_level ) {
?>

						<form method="get" action="">
							<div class="show_sales">
								<input type="hidden" name="bid" value="<?php echo $GLOBALS['businessid']; ?>" />
								<input type="hidden" name="cid" value="<?php echo $GLOBALS['companyid']; ?>" />
<?php
		if ( Treat_Date::getBeginningOfMonth( null, true ) != $viewmonth ) {
?>

								<input type="hidden" name="viewmonth" value="<?php echo date( 'Y-m-d', $viewmonth ); ?>" />
<?php
		}
		if ( !$show_sales ) {
?>

								<input type="hidden" name="show_sales" value="true" />
								<button type="submit" name="show_sales_button">Show Sales</button>
<?php
		}
		else {
?>

								<button type="submit" name="show_sales_button">Undo Show Sales</button>
<?php
		}
?>

							</div>
						</form>
<?php
	}
	else {
#		echo '&nbsp;';
	}
?>

					</td>
<?php
	///legend
	foreach ( $cs_week_end AS $key => $value ) {
?>
					<th class="week_label">Actual</th>
					<th class="week_label">%</th>
<?php
	}
?>
					<th class="week_label">ACT</th>
					<th class="week_label">%</th>
<?php
	if ( !$cs_final ) {
?>
					<th class="week_label">EST</th>
					<th class="week_label">%</th>
<?php
	}
	else {
?>
					<th class="week_label">Final</th>
					<th class="week_label">%</th>
<?php
	}
?>
					<th class="week_label">BGT</th>
					<th class="week_label">+/-</th>
				</tr>
			</thead>
			
			<tfoot>
				<tr class="data_line">
					<th class="name">Operating Days</th>
<?php
	foreach ( $cs_week_end as $key => $week ) {
?>
					<td class="operating_days" colspan="2"><?php
						echo $controlSheet->getOperatingDaysByWeekToString( $week );
					?></td>
<?php
	}
?>
					<td class="total operating_days" colspan="6"><?php
						echo $controlSheet->getOperatingDaysForMonthToString();
					?></td>
				</tr>
				
<?php
	if ( 8 <= $_SESSION->getUser()->security_level ) {
?>
				<tr class="data_line">
					<td>&nbsp;</td>
<?php
		foreach ( $cs_week_end as $key => $week ) {
?>
					<td class="delete_week" colspan="2">
						<form action="/ta/buscontrolc/delete_gl_items" method="post">
							<div>
								<input type="hidden" name="company_id" value="<?php echo $GLOBALS['companyid']; ?>" />
								<input type="hidden" name="business_id" value="<?php echo $GLOBALS['businessid']; ?>" />
								<input type="hidden" name="type" value="week" />
								<input type="hidden" name="date" value="<?php echo $week ?>" />
								<input type="submit" value="Delete" />
							</div>
						</form>
					</td>
<?php
		}
?>
					<td class="total delete_week" colspan="6">
						<form action="/ta/buscontrolc/delete_gl_items" method="post">
							<div>
								<input type="hidden" name="company_id" value="<?php echo $GLOBALS['companyid']; ?>" />
								<input type="hidden" name="business_id" value="<?php echo $GLOBALS['businessid']; ?>" />
								<input type="hidden" name="type" value="budget" />
								<input type="hidden" name="date" value="<?php echo $viewmonth ?>" />
								<input type="submit" value="Delete Budgets" />
							</div>
						</form>
					</td>
				</tr>
<?php
	}
?>
			</tfoot>
			
			<tbody>
<?php
	////sales
	$sales = array();

	////numbers
	
	// if you need to define a different format for 1 of these, just pull this
	// apart & define them separately
	Treat_Model_GeneralLedger_Object_Abstract::setMonthAmountFormat(
		Treat_Model_GeneralLedger_Object_Abstract::setWeekAmountFormat(
			function( $amount ) {
				return str_replace( '$', '', money_format( '%.0n', $amount ) );
			}
		)
	);
	
	// if you need to define a different format for 1 of these, just pull this
	// apart & define them separately
	Treat_Model_GeneralLedger_Object_Abstract::setMonthPercentageFormat(
		Treat_Model_GeneralLedger_Object_Abstract::setWeekPercentageFormat(
			function( $amount ) {
				return number_format( $amount * 100, 1 ) . '%';
			}
		)
	);
	
	$link_start_date = null;
	$link_end_date = null;
	$accountGroup = current( $accountGroups );
	$accounts = $accountGroup->getAccounts();
	$account = current( $accounts );
	foreach ( $cs_week_end as $week ) {
		if ( $account->getIsDisplayWeek( $week ) ) {
			$link_start_date = $link_end_date;
			$link_end_date = $week;
		}
	}
	if ( $link_end_date ) {
		if ( !$link_start_date ) {
			$link_start_date = strtotime( '-6 Days', $link_end_date );
		}
		else {
			$link_start_date = strtotime( '+1 Day', $link_start_date );
			$link_end_date = strtotime( '+6 Days', $link_start_date );
		}
		$salary_labor_link = sprintf(
			'/ta/buslabor3.php?bid=%d&amp;cid=%d&amp;date1=%s&amp;date2=%s'
			,$page_business->getBusinessId()
			,$page_business->getCompanyId()
			,date( 'Y-m-d', $link_start_date )
			,date( 'Y-m-d', $link_end_date )
		);
	}
	else {
		$salary_labor_link = sprintf(
			'/ta/buslabor3.php?bid=%d&amp;cid=%d'
			,$page_business->getBusinessId()
			,$page_business->getCompanyId()
		);
	}
	
	
	$colspan = count( $cs_week_end ) * 2 + 5;
	foreach ( $accountGroups as $accountGroup ) {
		$accounts = $accountGroup->getAccounts();
		foreach ( $accounts as $account ) {
			if ( 8 <= $_SESSION->getUser()->security_level ) {
				$tmp = array();
				foreach ( $cs_week_end as $week ) {
					$tmp[ $week ]['amount'] = $account->getWeekAmount( $week );
					$tmp[ $week ]['percent'] = $account->getWeekPercentage( $week );
					$tmp[ $week ]['sales'] = $account->getWeekSales( $week );
				}
				$modify_link = sprintf(
					'<a href="/ta/buscontrolc/overwrite_account?bid=%d&amp;cid=%d&amp;aid=%d&amp;viewmonth=%s&amp;data=%s">%%s</a>'
					,$GLOBALS['businessid']
					,$GLOBALS['companyid']
					,$account->id
					,date( 'Y-m-01', $viewmonth )
					,str_replace( '%', '%%', urlencode( json_encode( $tmp ) ) )
				);
			}
			else {
				$modify_link = '%s';
			}
?>

				<tr class="data_line account_name">
					<td class="name"><?php
						if ( $account instanceof Treat_Model_GeneralLedger_Account_LaborObject ) {
							?><a href="<?php echo $salary_labor_link; ?>"><?php
								echo htmlentities( $account->name );
							?></a><?php
						}
						else {
							echo htmlentities( $account->name );
						}
					?></td>
<?php
			foreach ( $cs_week_end as $week ) {
?>
					<td class="weekly amount"><?php
						echo sprintf( $modify_link, $account->getWeekAmount( $week, true ) );
					?></td>
					<td class="weekly percentage"><?php
							echo sprintf( $modify_link, $account->getWeekPercentage( $week, true ) );
					?></td>
<?php	
			}
			
			if ( $account instanceof Treat_Model_GeneralLedger_Account_ImportObject ) {
				$a_href = sprintf(
					'<a href="/ta/buscontrol2.popup.php?bid=%d&amp;cid=%d&amp;aid=%d&amp;viewmonth=%s">%%s</a>'
					,$GLOBALS['businessid']
					,$GLOBALS['companyid']
					,$account->id
					,date( 'Y-m-01', $viewmonth )
				);
			}
			else {
				$a_href = '%s';
			}
?>
					<td class="total actual amount"><?php
						echo sprintf( $a_href, $account->getMonthAmount( true ) );
					?></td>
					<td class="total actual percentage <?php
						if ( 'SALES' != $accountGroup->name ) {
							echo $account->getMonthDifferenceClass( 'positive', 'negative' );
						}
					?>"><?php
							echo sprintf( $a_href, $account->getMonthPercentage( true ) );
					?></td>
					
<?php
	if ( !$cs_final ) {
?>
					<td class="total estimate amount"><?php
						echo sprintf( $a_href, $account->getMonthEstimate( true ) );
					?></td>
					<td class="total estimate percentage <?php
						if ( 'SALES' != $accountGroup->name ) {
							echo $account->getMonthDifferenceClass( 'positive', 'negative' );
						}
					?>"><?php
							echo sprintf( $a_href, $account->getMonthEstimatePercentage( true ) );
					?></td>
<?php
	}
	else {
?>
					<td class="total final amount"><?php
						echo sprintf( $a_href, $account->getFinalAmount( true, $cs_final ) );
					?></td>
					<td class="total final percentage <?php
					?>"><?php
							echo sprintf( $a_href, $account->getMonthFinalPercentage( true ) );
					?></td>
<?php
	}
?>
					
					<td class="total budget amount"><?php
						echo sprintf( $a_href, $account->getBudgetAmount( true ) );
					?></td>
					<td class="total budget percentage <?php
#						if ( 'SALES' != $accountGroup->name ) {
#							echo $account->getMonthDifferenceClass( 'positive', 'negative' );
#						}
					?>"><?php
							echo sprintf( $a_href, $account->getMonthBudgetPercentage( true ) );
					?></td>
				</tr>
				

<?php
		}
?>
				<tr class="data_line group_name" >
					<td class="name"><?php echo $accountGroup->name; ?></td>
<?php
			foreach ( $cs_week_end as $week ) {
?>
					<td class="weekly amount"><?php
						echo $accountGroup->getWeekAmount( $week, true );
					?></td>
					<td class="weekly percentage"><?php
							echo $accountGroup->getWeekPercentage( $week, true );
					?></td>
<?php
			}
?>
					<td class="total actual amount"><?php
						echo $accountGroup->getMonthAmount( true );
					?></td>
					<td class="total actual percentage <?php
						if ( 'SALES' != $accountGroup->name ) {
							echo $accountGroup->getMonthDifferenceClass( 'positive', 'negative' );
						}
					?>"><?php
							echo $accountGroup->getMonthPercentage( true );
					?></td>
					
<?php
	if ( !$cs_final ) {
?>
					<td class="total estimate amount"><?php
						echo $accountGroup->getMonthEstimate( true );
					?></td>
					<td class="total estimate percentage <?php
						if ( 'SALES' != $accountGroup->name ) {
							echo $accountGroup->getMonthDifferenceClass( 'positive', 'negative' );
						}
					?>"><?php
							echo $accountGroup->getMonthEstimatePercentage( true );
					?></td>
<?php
	}
	else {
?>
					<td class="total final amount"><?php
						echo $accountGroup->getFinalAmount( true, $cs_final );
					?></td>
					<td class="total final percentage <?php
					?>"><?php
							echo $accountGroup->getMonthFinalPercentage( true );
					?></td>
<?php
	}
?>
					
					<td class="total budget amount"><?php
						echo $accountGroup->getBudgetAmount( true );
					?></td>
					<td class="total budget percentage <?php
#						if ( 'SALES' != $accountGroup->name ) {
#							echo $accountGroup->getMonthDifferenceClass( 'positive', 'negative' );
#						}
					?>"><?php
							echo $accountGroup->getMonthBudgetPercentage( true );
					?></td>
				</tr>
<?php
	}
?>
			</tbody>
		</table>
	</div>

<?php
	//////END TABLE////////////////////////////////////////////////////////////////////////////////////
?>


	<div>&nbsp;</div>
	<form action="businesstrack.php" method="post">
		<div style="text-align: center;">
			<input type="hidden" name="username" value="<?php echo $GLOBALS['user']; ?>" />
			<input type="hidden" name="password" value="<?php echo $GLOBALS['pass']; ?>" />
			<input type="submit" value="Return to Main Page" />
		</div>
	</form>


<?php
	google_page_track();
}
?>
	</body>
</html>