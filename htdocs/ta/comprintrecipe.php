<?php

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+4);
        $diff=round($diff, 2);
        if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
        elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
        $diff = substr($diff, 0, $dot+3);
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      elseif ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      elseif ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='32')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      $day=$day-31;  
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";} 
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}

//include("db.php");
if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
require_once( DOC_ROOT . '/bootstrap.php' );
$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";


/*$user = $_COOKIE['usercook'];
$pass = $_COOKIE['passcook'];
$curmenu = $_COOKIE['curmenu'];
$editid=$_GET["editid"];
$companyid=$_GET["cid"];
$numserv=$_POST["numserv"];
$print=$_GET["print"];
$details=$_POST["details"];*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook');
$curmenu = \EE\Controller\Base::getSessionCookieVariable('curmenu');
$editid = \EE\Controller\Base::getGetVariable('editid');
$companyid = \EE\Controller\Base::getGetVariable('cid');
$numserv = \EE\Controller\Base::getPostVariable('numserv');
$print = \EE\Controller\Base::getGetVariable('print');
$details = \EE\Controller\Base::getPostVariable('details');

//if ($print==1){$details=$_GET["details"];$numserv=$_GET["numserv"];}
if($print==1){
	$details = \EE\Controller\Base::getGetVariable('details');
	$numserv = \EE\Controller\Base::getGetVariable('numserv');
}

if ($numserv==""){
   //mysql_connect($dbhost,$username,$password);
   //@mysql_select_db($database) or die( "Unable to select database");
   $query = "SELECT serving_size FROM menu_items WHERE menu_item_id = '$editid'";
   $result = Treat_DB_ProxyOld::query( $query );
   //mysql_close();

   $numserv = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'serving_size' );
}

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query( $query );
$num = Treat_DB_ProxyOld::mysql_numrows( $result );
//mysql_close();

$security_level = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'security_level' );
$bid = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'businessid' );
$cid = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'companyid' );

if ($num != 1)
{
    echo "<center><h3>Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
    if ($print==1){echo "<body onload='window.print(); window.close();'>";$showprint="";}
    else {echo "<body>";$showprint="<a href=comprintrecipe.php?editid=$editid&details=$details&print=1&numserv=$numserv style=$style target='_blank'><img src=print.gif height=16 width=16 border=0> <font size=2 color=blue>Print</font></a>";}

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM company WHERE companyid = '$companyid'";
    $result = Treat_DB_ProxyOld::query( $query );
    //mysql_close();

    $companyname = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'companyname' );

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM business WHERE companyid = '$companyid' AND businessid = '$businessid'";
    $result = Treat_DB_ProxyOld::query( $query );
    //mysql_close();

    $businessname = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'businessname' );
    $businessid = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'businessid' );
    $street = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'street' );
    $city = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'city' );
    $state = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'state' );
    $zip = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'zip' );
    $phone = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'phone' );
    $districtid = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'districtid' );
    $contract = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'contract' );
    if ($curmenu==""){
       $curmenu = @Treat_DB_ProxyOld::mysql_result( $result, 0, 'default_menu' );
    }

    $year=date("Y");
    $month=date("n");
    $day=date("j");
 
    $today="$year-$month-$day";

    if ($day<=4){$month--;}
    if ($month==0){$month=12;$year--;}
    if ($month<10){$endmonth="0$month";}
    else {$endmonth=$month;}
    $enddate="$year-$endmonth-01";

    $startmonth=$month-9;
    if ($startmonth<1){$startmonth=$startmonth+12;$year--;}
    if ($startmonth<10){$startmonth="0$startmonth";}
    $startdate="$year-$startmonth-01";
    $startyear=$year;

/////////////////////////RECIPES////////////////
    echo "<center><table width=100%>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM recipe WHERE menu_itemid = '$editid' ORDER BY recipeid DESC";
    $result = Treat_DB_ProxyOld::query( $query );
    $num = Treat_DB_ProxyOld::mysql_numrows( $result );
    //mysql_close();

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query2 = "SELECT * FROM menu_items WHERE menu_item_id = '$editid'";
    $result2 = Treat_DB_ProxyOld::query( $query2 );
    //mysql_close();

    $item_price = @Treat_DB_ProxyOld::mysql_result( $result2, 0, 'price' );
    $item_price=money($item_price);
    $recipe_notes = @Treat_DB_ProxyOld::mysql_result( $result2, 0, 'recipe' );
    $menuitem_name = @Treat_DB_ProxyOld::mysql_result( $result2, 0, 'item_name' );
    $serving_size = @Treat_DB_ProxyOld::mysql_result( $result2, 0, 'serving_size' );
    $serving_size2 = @Treat_DB_ProxyOld::mysql_result( $result2, 0, 'serving_size2' );
    $unit = @Treat_DB_ProxyOld::mysql_result( $result2, 0, 'unit' );

    echo "<tr><td colspan=5><b><u>$menuitem_name</u> ($numserv Servings @ $serving_size2/$unit)</b> $showprint<p><br></td></tr>";
    echo "<tr><td colspan=5><i><u>Ingredients</u>:</i></td></tr>";

    $totalprice=0;
    $totalserv=0;
    $counter=1;
    $num--;
    while ($num>=0){
       $recipeid = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'recipeid' );
       $inv_itemid = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'inv_itemid' );
       $rec_num = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'rec_num' );
       $rec_num = @round($rec_num/$serving_size*$numserv,2);
       $rec_size = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'rec_size' );
       $srv_num = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'srv_num' );
       $rec_order = @Treat_DB_ProxyOld::mysql_result( $result, $num, 'rec_order' );

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM inv_items WHERE inv_itemid = '$inv_itemid'";
       $result2 = Treat_DB_ProxyOld::query( $query2 );
       //mysql_close();

       $inv_itemname = @Treat_DB_ProxyOld::mysql_result( $result2, 0, 'item_name' );
       $order_size = @Treat_DB_ProxyOld::mysql_result( $result2, 0, 'order_size' );
       $price = @Treat_DB_ProxyOld::mysql_result( $result2, 0, 'price' );
       if ($rec_order==1){$inv_rec_num = @Treat_DB_ProxyOld::mysql_result( $result2, 0, 'rec_num' );}
       elseif ($rec_order==2){$inv_rec_num = @Treat_DB_ProxyOld::mysql_result( $result2, 0, 'rec_num2' );}
       $pack_size = @Treat_DB_ProxyOld::mysql_result( $result2, 0, 'pack_size' );
       $pack_qty = @Treat_DB_ProxyOld::mysql_result( $result2, 0, 'pack_qty' );
       $count_units = @Treat_DB_ProxyOld::mysql_result( $result2, 0, 'units' );
       $item_code = @Treat_DB_ProxyOld::mysql_result( $result2, 0, 'item_code' );
       $vendor = @Treat_DB_ProxyOld::mysql_result( $result2, 0, 'vendor' );

       if ($pack_qty<1){$pack_qty=1;}

       if ($details==1){
          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query2 = "SELECT vendor_name FROM vendor_master WHERE vendor_master_id = '$vendor'";
          $result2 = Treat_DB_ProxyOld::query( $query2 );
          //mysql_close();

          $vendor_name = @Treat_DB_ProxyOld::mysql_result( $result2, 0, 'vendor_name' );
       }
       else{$vendor_name="";}

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM conversion WHERE sizeid = '$rec_size' AND sizeidto = '$order_size'";
       $result2 = Treat_DB_ProxyOld::query( $query2 );
       $num2 = Treat_DB_ProxyOld::mysql_numrows( $result2 );
       //mysql_close();

       if ($num2!=0){$convert = @Treat_DB_ProxyOld::mysql_result( $result2, 0, 'convert' );}
       elseif($num2==0){$convert=$inv_rec_num;}
       else{$convert=1;}

       $newprice = @money($price/$convert*$rec_num);
       $totalprice=$totalprice+$newprice;

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM size WHERE sizeid = '$rec_size'";
       $result2 = Treat_DB_ProxyOld::query( $query2 );
       //mysql_close();

       $sizename = @Treat_DB_ProxyOld::mysql_result( $result2, 0, 'sizename' );

       $totalserv=$totalserv+($rec_num*$srv_num);

       $count_num=0;

       $test=1;
       $test=$inv_rec_num/$pack_qty;
       while ($rec_num>=$test&&$test>1){
          $count_num++;
          $rec_num=$rec_num-$test;
       }

       $rec_num=round($rec_num,2);

       $shownewsize="";
       if ($count_num!=0&&$rec_num==0){$shownewsize="$count_num $count_units";}
       elseif ($count_num!=0&&$rec_num!=0){$shownewsize="$count_num $count_units, $rec_num $sizename";}
       else {$shownewsize="$rec_num $sizename";}

       if ($details!=1||$item_code==0){$item_code="";}
       if ($details==1){$fontsize=2;}
       else{$fontsize=3;}

       echo "<tr><td width=5%><font size=$fontsize>$counter.</td><td width=17%><font size=$fontsize>$shownewsize</td><td><font size=$fontsize>$inv_itemname</td><td><font size=$fontsize>$item_code $vendor_name</font></td><td align=right><font size=$fontsize>$$newprice</td></tr>";

       $num--;
       $counter++;
    }
    $totalprice=money($totalprice);
    if ($numserv>1){$showunitprice=money($totalprice/$numserv);$showunitprice="$$showunitprice/";$showunitprice2=" Per Serving/Total";}
    else{$showunitprice="";$showunitprice2="";}
    echo "<tr><td colspan=4 align=right>Approximate Cost$showunitprice2:</td><td colspan=1 align=right>$showunitprice$$totalprice</td></tr>";
    if ($numserv>1){$showunitcustprice=money($item_price);$showunitcustprice="$$showunitcustprice/";$showunitcustprice2=" Per Serving/Total";}
    else{$showunitcustprice="";$showunitcustprice2="";}
    $item_price=money($item_price*$numserv);
    echo "<tr><td colspan=4 align=right><font color=gray>Suggested Price$showunitcustprice2:</font></td><td colspan=1 align=right><font color=gray>$showunitcustprice$$item_price</font></td></tr>";
    $fcprcnt = @round($totalprice/$item_price*100,1);
    echo "<tr><td colspan=4 align=right><font color=gray>Approximate FC%:</font></td><td colspan=1 align=right><font color=gray>$fcprcnt%</font></td></tr>";

    echo "<tr><td colspan=5><p><br><i><u>Instructions</u>:</i></td></tr>";

    $recipe_notes=str_replace("\n","<br>",$recipe_notes);

    echo "<tr><td colspan=5>$recipe_notes</td></tr>";
  
    echo "</table></center></body>";
	google_page_track();

}
?>