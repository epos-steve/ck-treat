<?
function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      elseif ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      elseif ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '1')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='31')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;
$leap = date("L");

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   
   elseif ($leap==1&&$month=='03')
   {
      $day=$day+29;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}


function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}


//include("db.php");
define('DOC_ROOT', dirname(dirname(__FILE__)));
require_once(DOC_ROOT.'/bootstrap.php');

/*$user=$_COOKIE["usercook"];
$companyid=$_COOKIE["compcook"];
$pass=$_COOKIE["passcook"];
$curmenu=$_COOKIE["curmenu"];
$curcust=$_COOKIE["curcust"];
$curcust_num=$_COOKIE["curcust_num"];
$businessid=$_POST['businessid'];
$date1=$_POST['date1'];
$date2=$_POST['date2'];
$newstart=$_POST['newstart'];
$newmonth=$_POST['newmonth'];
$newyear=$_POST['newyear'];
$personal=$_POST['personal'];
$weekend=$_POST['weekend'];
$account=$_POST['account'];*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook');
$companyid = \EE\Controller\Base::getSessionCookieVariable('compcook');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook');
$curmenu = \EE\Controller\Base::getSessionCookieVariable('curmenu');
$curcust = \EE\Controller\Base::getSessionCookieVariable('curcust');
$curcust_num = \EE\Controller\Base::getSessionCookieVariable('curcust_num');
$businessid = \EE\Controller\Base::getPostVariable('businessid');
$date1 = \EE\Controller\Base::getPostVariable('date1');
$date2 = \EE\Controller\Base::getPostVariable('date2');
$newstart = \EE\Controller\Base::getPostVariable('newstart');
$newmonth = \EE\Controller\Base::getPostVariable('newmonth');
$newyear = \EE\Controller\Base::getPostVariable('newyear');
$personal = \EE\Controller\Base::getPostVariable('personal');
$weekend = \EE\Controller\Base::getPostVariable('weekend');
$account = \EE\Controller\Base::getPostVariable('account');

/*mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");*/
$query = "SELECT username,password,security_level FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=Treat_DB_ProxyOld::mysql_numrows($result);
//mysql_close();

$location="busordertrack.php?newmonth=$newmonth&newyear=$newyear&newstart=$newstart&weekend=$weekend&bid=$businessid&cid=$companyid#copy";
header('Location: ./' . $location); 

if ($num!=1) 
{
    echo "<center><h4>Failed, Session Timed Out</h4></center>";
}

else
{
    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    if($account==-1){$query = "SELECT * FROM accounts WHERE businessid = '$businessid'";}
    else{$query = "SELECT * FROM accounts WHERE accountid = '$account'";}
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    $num--;
    while ($num>=0){
       $accountid=Treat_DB_ProxyOld::mysql_result($result,$num,"accountid");  

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM order_detail WHERE date = '$date1' AND accountid = '$accountid'";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=Treat_DB_ProxyOld::mysql_numrows($result2);
       //mysql_close();

       $num2--;
       while ($num2>=0){
          $daypartid=Treat_DB_ProxyOld::mysql_result($result2,$num2,"menu_daypartid"); 
          $portionid=Treat_DB_ProxyOld::mysql_result($result2,$num2,"menu_portionid"); 
          $qty=Treat_DB_ProxyOld::mysql_result($result2,$num2,"qty"); 

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query3 = "SELECT * FROM order_detail WHERE date = '$date2' AND accountid = '$accountid' AND menu_daypartid = '$daypartid' AND menu_portionid = '$portionid'";
          $result3 = Treat_DB_ProxyOld::query($query3);
          $num3=Treat_DB_ProxyOld::mysql_numrows($result3);
          //mysql_close();

          if($num3!=0){
             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query4 = "UPDATE order_detail SET qty = '$qty' WHERE date = '$date2' AND accountid = '$accountid' AND menu_daypartid = '$daypartid' AND menu_portionid = '$portionid'";
             $result4 = Treat_DB_ProxyOld::query($query4);
             //mysql_close();
          }
          else{
             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query4 = "INSERT INTO order_detail (menu_daypartid,date,menu_portionid,qty,accountid) VALUES ('$daypartid','$date2','$portionid','$qty','$accountid')";
             $result4 = Treat_DB_ProxyOld::query($query4);
             //mysql_close();
          }

          $num2--;
       }

       $num--;
    }

}
?>