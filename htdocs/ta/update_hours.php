<?php

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+4);
        $diff=round($diff, 2);
        if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
        elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
        $diff = substr($diff, 0, $dot+3);
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      elseif ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      elseif ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '1')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='31')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;
$leap = date("L");

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   
   elseif ($leap==1&&$month=='03')
   {
      $day=$day+29;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      $day=$day-31;  
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";} 
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}

define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once(DOC_ROOT.'/bootstrap.php');

$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";

$user=$_COOKIE["usercook"];
$pass=$_COOKIE["passcook"];
$businessid=$_POST["businessid"];
$companyid=$_POST["companyid"];
$date1=$_POST["date1"];
$date2=$_POST["date2"];
$printdate1=$date1;
$printdate2=$date2;

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=mysql_numrows($result);

$loginid=mysql_result($result,0,"loginid");
$security_level=mysql_result($result,0,"security_level");
$mysecurity=mysql_result($result,0,"security");
$bid=mysql_result($result,0,"businessid");
$bid2=mysql_result($result,0,"busid2");
$bid3=mysql_result($result,0,"busid3");
$bid4=mysql_result($result,0,"busid4");
$bid5=mysql_result($result,0,"busid5");
$bid6=mysql_result($result,0,"busid6");
$bid7=mysql_result($result,0,"busid7");
$bid8=mysql_result($result,0,"busid8");
$bid9=mysql_result($result,0,"busid9");
$bid10=mysql_result($result,0,"busid10");
$cid=mysql_result($result,0,"companyid");

if ($num != 1 || ($security_level==1 && ($bid != $businessid || $cid != $companyid)) || $user == "" || $pass == "")
{
    echo "<center><h3>Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
    $query = "SELECT week_start FROM company WHERE companyid = '$companyid'";
    $result = Treat_DB_ProxyOld::query($query);

    $week_start=mysql_result($result,0,"week_start");

    $today=date("Y-m-d");
    $today="$today 00:00:00";
    $date1="$date1 00:00:00";
    $date2="$date2 23:59:59";

    $query = "SELECT codeid FROM payroll_code WHERE companyid = '$companyid' AND code LIKE 'REG'";
    $result = Treat_DB_ProxyOld::query($query);

    $coded=mysql_result($result,0,"codeid");

    $query = "SELECT labor_clockin.clockinid,labor_clockin.loginid,labor_clockin.clockin,labor_clockin.clockout,labor_clockin.jobtype,login.firstname,login.lastname,login.empl_no FROM labor_clockin,login WHERE labor_clockin.businessid = '$businessid' AND labor_clockin.clockin >= '$date1' AND labor_clockin.clockin <= '$date2' AND labor_clockin.is_deleted = '0' AND labor_clockin.loginid = login.loginid ORDER BY login.lastname DESC,login.firstname DESC,labor_clockin.jobtype,labor_clockin.clockin";
    $result = Treat_DB_ProxyOld::query($query);
    $num=mysql_numrows($result);

    $lastdate=-1;
    $lastloginid=-1;
    $lastjobtype=-1;
    $hours=0;
    $mins=0;
    $counter=0;

    $num--;
    while($num>=-1){
       $clockinid=mysql_result($result,$num,"labor_clockin.clockinid");
       $loginid=mysql_result($result,$num,"labor_clockin.loginid");
       $clockin=mysql_result($result,$num,"labor_clockin.clockin");
       $clockout=mysql_result($result,$num,"labor_clockin.clockout");
       $jobtype=mysql_result($result,$num,"labor_clockin.jobtype");
       $firstname=mysql_result($result,$num,"login.firstname");
       $lastname=mysql_result($result,$num,"login.lastname");
       $empl_no=mysql_result($result,$num,"login.empl_no");

       $thisclockin=substr($clockin,0,10);
       $thisclockin="$thisclockin 23:59:59";

       $query2="SELECT * FROM jobtype WHERE jobtypeid = '$jobtype'";
       $result2 = Treat_DB_ProxyOld::query($query2);

       $jobname=mysql_result($result2,0,"name");
       $hourly=mysql_result($result2,0,"hourly");

       if (($lastloginid!=$loginid||$lastjobtype!=$jobtype||$lastdate>$thisclockin)&&$lastloginid!=-1&&$lastjobtype!=-1&&$lastdate!=-1&&$lasthourly==1){

          $hours=$hours*60;
          $totmins=round(($hours+$mins)/60,2);

          $query2="SELECT * FROM login WHERE loginid = '$lastloginid' AND oo2 = '$businessid'";
          $result2 = Treat_DB_ProxyOld::query($query2);
          $num2=mysql_numrows($result2);

          if($num2==1){
             $query2="SELECT rate FROM jobtypedetail WHERE loginid = '$lastloginid' AND jobtype = '$lastjobtype' AND is_deleted = '0'";
             $result2 = Treat_DB_ProxyOld::query($query2);

             $rate=mysql_result($result2,0,"rate");

             $p_date=substr($lastdate,0,10);

             $query2="SELECT * FROM labor WHERE loginid = '$lastloginid' AND businessid = '$businessid' AND companyid = '$companyid' AND jobtypeid = '$lastjobtype' AND date = '$p_date'";
             $result2 = Treat_DB_ProxyOld::query($query2);
             $num2=mysql_numrows($result2);

             if($num2>0){
                $laborid=mysql_result($result2,0,"id");

                $query2="UPDATE labor SET hours = '$totmins', coded = '$coded' WHERE id = '$laborid'";
                $result2 = Treat_DB_ProxyOld::query($query2);
             }
             elseif($totmins!=0){
                $query2="INSERT INTO labor (loginid,businessid,companyid,hours,date,rate,coded,jobtypeid) VALUES ('$lastloginid','$businessid','$companyid','$totmins','$p_date','$rate','$coded','$lastjobtype')";
                $result2 = Treat_DB_ProxyOld::query($query2);
             }
          }
      ///////////////////////////TEMP EMPLOYEES///////////////////////////////
          else{
             $t_date=date("Y-m-d");
             while(dayofweek($t_date)!=$week_start){$t_date=prevday($t_date);}

             $query2="SELECT * FROM labor_temp WHERE loginid = '$lastloginid' AND businessid = '$businessid' AND companyid = '$companyid' AND jobtypeid = '$lastjobtype' AND date = '$t_date'";
             $result2 = Treat_DB_ProxyOld::query($query2);
             $num2=mysql_numrows($result2);
             
             if($num2>0){
                $tempid=mysql_result($result2,0,"tempid");
                $rate=mysql_result($result2,0,"rate");

                $query2="SELECT * FROM labor WHERE loginid = '$lastloginid' AND businessid = '$businessid' AND companyid = '$companyid' AND jobtypeid = '$lastjobtype' AND date = '$p_date' AND tempid = '$tempid'";
                $result2 = Treat_DB_ProxyOld::query($query2);
                $num2=mysql_numrows($result2);

                if($num2>0){
                   $laborid=mysql_result($result2,0,"id");

                   $query2="UPDATE labor SET hours = '$totmins', coded = '$coded' WHERE id = '$laborid'";
                   $result2 = Treat_DB_ProxyOld::query($query2);
                }
                elseif($totmins!=0){
                   $query2="INSERT INTO labor (loginid,businessid,companyid,hours,date,rate,coded,jobtypeid,tempid) VALUES ('$lastloginid','$businessid','$companyid','$totmins','$p_date','$rate','$coded','$lastjobtype','$tempid')";
                   $result2 = Treat_DB_ProxyOld::query($query2);
                }
             }
             else{
                $query5="SELECT rate FROM jobtypedetail WHERE loginid = '$lastloginid' AND jobtype = '$lastjobtype' AND is_deleted = '0'";
                $result5 = mysql_query($query5);

                $rate=@mysql_result($result5,0,"rate");
                 
                $query2="INSERT INTO labor_temp (loginid,jobtypeid,businessid,companyid,date,rate) VALUES ('$lastloginid','$lastjobtype','$businessid','$companyid','$t_date','$rate')";
                $result2 = Treat_DB_ProxyOld::query($query2);
                $tempid = mysql_insert_id();

                $query2="INSERT INTO labor (loginid,businessid,companyid,hours,date,rate,coded,jobtypeid,tempid) VALUES ('$lastloginid','$businessid','$companyid','$totmins','$p_date','$rate','$coded','$lastjobtype','$tempid')";
                $result2 = Treat_DB_ProxyOld::query($query2);
             }

          }

          $counter=0;
          $hours=0;
          $mins=0;
       }
       elseif($lastloginid!=$loginid){$hours=0;$mins=0;$counter=0;}

       $counter++;

       $query2="SELECT TimeDiff(clockout,clockin) AS totalamt FROM labor_clockin WHERE clockinid = '$clockinid'";
       $result2 = Treat_DB_ProxyOld::query($query2);

       $totalamt=mysql_result($result2,0,"totalamt");

       $pieces=explode(":",$totalamt);
       $hours+=$pieces[0];
       $mins+=$pieces[1];

       $lastloginid=$loginid;
       $lastjobtype=$jobtype;
       $lastdate=$thisclockin;
       $lasthourly=$hourly;

       $num--;
    }

    $location="buslabor5.php?bid=$businessid&cid=$companyid&date1=$printdate1&date2=$printdate2";
    header('Location: ./' . $location);

}
?>
