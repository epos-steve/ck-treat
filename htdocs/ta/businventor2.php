<?php

function money($diff){
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+4);
        $diff=round($diff, 2);
        if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
        elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
        $diff = substr($diff, 0, $dot+3);
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      elseif ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      elseif ($month == "10"){$month="11";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '1')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='31')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;
$leap = date("L");

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }

   elseif ($leap==1&&$month=='03')
   {
      $day=$day+29;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}


function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}

define('DOC_ROOT', dirname(dirname(__FILE__)));
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');

//Treat_DB_ProxyOld::$debug = true;
$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");
$creditamount="creditamount";
$creditidnty="creditidnty";
$creditdate="creditdate";
$debitamount="debitamount";
$debitidnty="debitidnty";
$debitdate="debitdate";
$quatertotal=0;
$monthtotal=0;
$total=0;
$counter=0;
$findme='.';
$double='.00';
$single='0';
$double2='00';
$mycount=1;

$savedprice = null; #undefined var
$value = null; #undefined var
$showsubtotal = null; #undefined var

$user = \EE\Controller\Base::getSessionCookieVariable('usercook');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook');
$view = \EE\Controller\Base::getSessionCookieVariable('viewcook');
$date1 = \EE\Controller\Base::getSessionCookieVariable('date1cook');
$date2 = \EE\Controller\Base::getSessionCookieVariable('date2cook');
$businessid = \EE\Controller\Base::getGetVariable('bid');
$companyid = \EE\Controller\Base::getGetVariable('cid');
$showerror = \EE\Controller\Base::getGetVariable('showerror');
$viewdate = \EE\Controller\Base::getGetVariable('viewdate');

if ($viewdate==""){$viewdate = \EE\Controller\Base::getSessionCookieVariable('date5cook');}

$edititem = \EE\Controller\Base::getGetVariable('edititem');
$highlight = \EE\Controller\Base::getGetVariable('highlight');

if ($highlight==""){$highlight = \EE\Controller\Base::getSessionCookieVariable('highlight');}

if ($businessid==""&&$companyid==""){
	$businessid = \EE\Controller\Base::getPostVariable('businessid');
	$companyid = \EE\Controller\Base::getPostVariable('companyid');
	$view = \EE\Controller\Base::getPostVariable('view');
	$date1 = \EE\Controller\Base::getPostVariable('date1');
	$date2 = \EE\Controller\Base::getPostVariable('date2');
	$findinv = \EE\Controller\Base::getPostVariable('findinv');
}

///POST NEW INVENTORY DATE
$inv_count_date = \EE\Controller\Base::getPostVariable('inv_count_date');
$new_inv_count = \EE\Controller\Base::getPostVariable('new_inv_count');

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=Treat_DB_ProxyOld::mysql_numrows($result);

$loginid=Treat_DB_ProxyOld::mysql_result($result,0,"loginid");
$security_level=Treat_DB_ProxyOld::mysql_result($result,0,"security_level");
$mysecurity=Treat_DB_ProxyOld::mysql_result($result,0,"security");
$bid=Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
$bid2=Treat_DB_ProxyOld::mysql_result($result,0,"busid2");
$bid3=Treat_DB_ProxyOld::mysql_result($result,0,"busid3");
$bid4=Treat_DB_ProxyOld::mysql_result($result,0,"busid4");
$bid5=Treat_DB_ProxyOld::mysql_result($result,0,"busid5");
$bid6=Treat_DB_ProxyOld::mysql_result($result,0,"busid6");
$bid7=Treat_DB_ProxyOld::mysql_result($result,0,"busid7");
$bid8=Treat_DB_ProxyOld::mysql_result($result,0,"busid8");
$bid9=Treat_DB_ProxyOld::mysql_result($result,0,"busid9");
$bid10=Treat_DB_ProxyOld::mysql_result($result,0,"busid10");
$cid=Treat_DB_ProxyOld::mysql_result($result,0,"companyid");

$pr1=Treat_DB_ProxyOld::mysql_result($result,0,"payroll");
$pr2=Treat_DB_ProxyOld::mysql_result($result,0,"pr2");
$pr3=Treat_DB_ProxyOld::mysql_result($result,0,"pr3");
$pr4=Treat_DB_ProxyOld::mysql_result($result,0,"pr4");
$pr5=Treat_DB_ProxyOld::mysql_result($result,0,"pr5");
$pr6=Treat_DB_ProxyOld::mysql_result($result,0,"pr6");
$pr7=Treat_DB_ProxyOld::mysql_result($result,0,"pr7");
$pr8=Treat_DB_ProxyOld::mysql_result($result,0,"pr8");
$pr9=Treat_DB_ProxyOld::mysql_result($result,0,"pr9");
$pr10=Treat_DB_ProxyOld::mysql_result($result,0,"pr10");

if ($num != 1 || ($security_level==1 && ($bid != $businessid || $cid != $companyid)) || $user == "" || $pass == "")
{
    echo "<center><h3>Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
    setcookie("viewcook",$view);
    setcookie("date1cook",$date1);
    setcookie("date2cook",$date2);
    setcookie("date5cook",$viewdate);
    setcookie("highlight",$highlight);
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<STYLE>
#loading {
 	width: 200px;
 	height: 48px;
 	/*background-color:;*/
 	position: absolute;
 	left: 50%;
 	top: 50%;
 	margin-top: -50px;
 	margin-left: -100px;
 	text-align: center;
}
</STYLE>

<script type="text/javascript" src="preLoadingMessage.js"></script>

<SCRIPT LANGUAGE="JavaScript" SRC="CalendarPopup.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">document.write(getCalendarStyles());</SCRIPT>

<STYLE>
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation
			{
			background-color:#6677DD;
			text-align:center;
			vertical-align:center;
			text-decoration:none;
			color:#FFFFFF;
			font-weight:bold;
			}
	.TESTcpDayColumnHeader,
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation,
	.TESTcpCurrentMonthDate,
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDate,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDate,
	.TESTcpCurrentDateDisabled,
	.TESTcpTodayText,
	.TESTcpTodayTextDisabled,
	.TESTcpText
			{
			font-family:arial;
			font-size:8pt;
			}
	TD.TESTcpDayColumnHeader
			{
			text-align:right;
			border:solid thin #6677DD;
			border-width:0 0 1 0;
			}
	.TESTcpCurrentMonthDate,
	.TESTcpOtherMonthDate,
	.TESTcpCurrentDate
			{
			text-align:right;
			text-decoration:none;
			}
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDateDisabled
			{
			color:#D0D0D0;
			text-align:right;
			text-decoration:line-through;
			}
	.TESTcpCurrentMonthDate
			{
			color:#6677DD;
			font-weight:bold;
			}
	.TESTcpCurrentDate
			{
			color: #FFFFFF;
			font-weight:bold;
			}
	.TESTcpOtherMonthDate
			{
			color:#808080;
			}
	TD.TESTcpCurrentDate
			{
			color:#FFFFFF;
			background-color: #6677DD;
			border-width:1;
			border:solid thin #000000;
			}
	TD.TESTcpCurrentDateDisabled
			{
			border-width:1;
			border:solid thin #FFAAAA;
			}
	TD.TESTcpTodayText,
	TD.TESTcpTodayTextDisabled
			{
			border:solid thin #6677DD;
			border-width:1 0 0 0;
			}
	A.TESTcpTodayText,
	SPAN.TESTcpTodayTextDisabled
			{
			height:20px;
			}
	A.TESTcpTodayText
			{
			color:#6677DD;
			font-weight:bold;
			}
	SPAN.TESTcpTodayTextDisabled
			{
			color:#D0D0D0;
			}
	.TESTcpBorder
			{
			border:solid thin #6677DD;
			}
			
	.ajax_box{
		background-color:#ff9;
		color:#5a636d;
		text-align:center;
		font-weight:bold;
		padding:15px 0 0 0;
		margin:0;
		position:fixed;
		top:50px;
		right:50px;
		z-index:1000;
		-webkit-border-radius:5px;
		-moz-border-radius:5px;
		border-radius:5px;
		-webkit-box-shadow: 3px 3px 5px 5px;
		box-shadow: 3px 3px 5px 5px;
	}
	#ajax_saving{
		width:200px;
		height:50px;
		letter-spacing:.15em;
		font-size:24px;
	}
	#ajax_error, #ajax_notice{
		width:250px;
		height:100px;
		font-size:16px;
	}
	
	#ajax_notice{
		text-align:left;
		padding:15px;
	}
	#close_notice{
		width:25px;
		height:25px;
		float:right;
		padding:0;
		margin:-15px -15px 0 0;
	}
	#close_notice a{
		text-decoration:none;
		color:#f00;
		line-height:25px;
		font-size:18px;
	}
</STYLE>

<SCRIPT LANGUAGE=javascript><!--
function submit2(){return confirm('ARE YOU SURE YOU WANT TO SAVE/SUBMIT END OF MONTH INVENTORY?');}
// --></SCRIPT>

<script type="text/javascript" language="javascript">
function checkKey(){

var key = event.keyCode;
var DaIndexId = event.srcElement.IndexId-0
if(key==38) {
document.getElementByIndex(DaIndexId-1).focus();
return false;
}
else if(key==40) {
document.getElementByIndex(DaIndexId+1).focus();
return false;
}
else if(key==13) {
document.getElementByIndex(DaIndexId+1).focus();
return false;
}
return true
} // end checkKey function
document.getElementByIndex=function(){
for(var i=0;i<document.all.length;i++)
if(document.all[i].IndexId==arguments[0])
return document.all[i]
return null
}
</script>

<script language="JavaScript"
   type="text/JavaScript">
function changePage(newLoc)
 {
   nextPage = newLoc.options[newLoc.selectedIndex].value

   if (nextPage != "")
   {
      document.location.href = nextPage
   }
 }
</script>
</head>

<?
    echo "<body>";
    echo "<div style=border:50px solid red;padding:10px;>";
    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%><tr><td align=left colspan=2>";
    echo "<a href=businesstrack.php><img src=logo.jpg border=0 height=43 width=205></a><p></td></tr>";

    $query = "SELECT * FROM company WHERE companyid = '$companyid'";
    $result = Treat_DB_ProxyOld::query($query);

    $companyname=Treat_DB_ProxyOld::mysql_result($result,0,"companyname");
    $week_end=Treat_DB_ProxyOld::mysql_result($result,0,"week_end");
	$com_logo=@Treat_DB_ProxyOld::mysql_result($result,0,"logo");

	if($com_logo == ""){$com_logo = "talogo.gif";}

    $query = "SELECT * FROM security WHERE securityid = '$mysecurity'";
    $result = Treat_DB_ProxyOld::query($query);

    $sec_bus_sales=Treat_DB_ProxyOld::mysql_result($result,0,"bus_sales");
    $sec_bus_invoice=Treat_DB_ProxyOld::mysql_result($result,0,"bus_invoice");
    $sec_bus_order=Treat_DB_ProxyOld::mysql_result($result,0,"bus_order");
    $sec_bus_payable=Treat_DB_ProxyOld::mysql_result($result,0,"bus_payable");
    $sec_bus_inventor1=Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor1");
    $sec_bus_inventor2=Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor2");
    $sec_bus_inventor3=Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor3");
    $sec_bus_inventor4=Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor4");
    $sec_bus_inventor5=Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor5");
    $sec_bus_control=Treat_DB_ProxyOld::mysql_result($result,0,"bus_control");
    $sec_bus_menu=Treat_DB_ProxyOld::mysql_result($result,0,"bus_menu");
    $sec_bus_order_setup=Treat_DB_ProxyOld::mysql_result($result,0,"bus_order_setup");
    $sec_bus_request=Treat_DB_ProxyOld::mysql_result($result,0,"bus_request");
    $sec_route_collection=Treat_DB_ProxyOld::mysql_result($result,0,"route_collection");
    $sec_route_labor=Treat_DB_ProxyOld::mysql_result($result,0,"route_labor");
    $sec_route_detail=Treat_DB_ProxyOld::mysql_result($result,0,"route_detail");
    $sec_bus_labor=Treat_DB_ProxyOld::mysql_result($result,0,"bus_labor");
    $sec_bus_budget=Treat_DB_ProxyOld::mysql_result($result,0,"bus_budget");
        
    if($sec_bus_inventor2 < 1){
            header('Location: ' . $_SERVER['HTTP_REFERER']);
            die();
    }

    $counter=0;

    $query = "SELECT business.*,business_setting.inventory_type FROM business LEFT JOIN business_setting ON business_setting.business_id = business.businessid WHERE business.businessid = '$businessid'";
    $result = Treat_DB_ProxyOld::query($query);

    $businessname=Treat_DB_ProxyOld::mysql_result($result,0,"businessname");
    $businessid=Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
    $street=Treat_DB_ProxyOld::mysql_result($result,0,"street");
    $city=Treat_DB_ProxyOld::mysql_result($result,0,"city");
    $state=Treat_DB_ProxyOld::mysql_result($result,0,"state");
    $zip=Treat_DB_ProxyOld::mysql_result($result,0,"zip");
    $phone=Treat_DB_ProxyOld::mysql_result($result,0,"phone");
    $districtid=Treat_DB_ProxyOld::mysql_result($result,0,"districtid");
    $bus_unit=Treat_DB_ProxyOld::mysql_result($result,0,"unit");
	$inventory_type=Treat_DB_ProxyOld::mysql_result($result,0,"inventory_type");

    echo "<tr bgcolor=#CCCCFF><td width=1%><img src=logo/$com_logo></td><td align=left><font size=4><b>$businessname #$bus_unit</b></font><br>$street<br>$city, $state $zip<br>$phone</td>";
    echo "<td align=right valign=top><br>";
    echo "<FORM ACTION='editbus.php' method=post name='store'>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<input type=hidden name=businessid value=$businessid>";
    echo "<input type=hidden name=companyid value=$companyid>";
    echo "<INPUT TYPE=submit VALUE=' Store Setup '> ";
    echo "</form></td></tr>";

    echo "<tr><td colspan=3 align=left><font color=#999999>";
    if ($sec_bus_sales>0){echo "<a href=busdetail.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Sales</font></a>";}
    if ($sec_bus_invoice>0){echo " | <a href=businvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Invoicing</font></a>";}
    if ($sec_bus_order>0){echo " | <a href=buscatertrack.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Orders</font></a>";}
    if ($sec_bus_payable>0){echo " | <a href=busapinvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Payables</font></a>";}
    if ($sec_bus_inventor1>0){echo " | <a href=businventor.php?bid=$businessid&cid=$companyid><font color=red onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='red' style='text-decoration:none'>Inventory</font></a>";}
    if ($sec_bus_control>0){echo " | <a href=buscontrol.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Control Sheet</font></a>";}
    if ($sec_bus_menu>0){echo " | <a href=buscatermenu.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Menus</font></a>";}
    if ($sec_bus_order_setup>0){echo " | <a href=buscaterroom.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Order Setup</font></a>";}
    if ($sec_bus_request>0){echo " | <a href=busrequest.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Requests</font></a>";}
    if ($sec_route_collection>0||$sec_route_labor>0||$sec_route_detail>0){echo " | <a href=busroute_collect.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Route Collections</font></a>";}
    if ($sec_bus_labor>0){echo " | <a href=buslabor.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Labor</font></a>";}
    if ($sec_bus_budget>0||($businessid == $bid && $pr1 == 1)||($businessid == $bid2 && $pr2 == 1)||($businessid == $bid3 && $pr3 == 1)||($businessid == $bid4 && $pr4 == 1)||($businessid == $bid5 && $pr5 == 1)||($businessid == $bid6 && $pr6 == 1)||($businessid == $bid7 && $pr7 == 1)||($businessid == $bid8 && $pr8 == 1)||($businessid == $bid9 && $pr9 == 1)||($businessid == $bid10 && $pr10 == 1)){echo " | <a href=busbudget.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Budget</font></a>";}
	echo "</td></tr>";

    echo "</table></center><p>";

if ($security_level>0){
    echo "<center><table width=90%><tr><td align=left><form action=businventor2.php method=post>";
    echo "<select name=gobus onChange='changePage(this.form.gobus)'>";

    $districtquery="";
    if ($security_level>2&&$security_level<7){
       $query = "SELECT * FROM login_district WHERE loginid = '$loginid'";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);

       $num--;
       while($num>=0){
          $districtid=Treat_DB_ProxyOld::mysql_result($result,$num,"districtid");
          if($districtquery==""){$districtquery="districtid = '$districtid'";}
          else{$districtquery="$districtquery OR districtid = '$districtid'";}
          $num--;
       }
    }

    if($security_level>6){$query = "SELECT * FROM business WHERE companyid = '$companyid' ORDER BY businessname DESC";}
    elseif($security_level==2||$security_level==1){$query = "SELECT * FROM business WHERE companyid = '$companyid' AND (businessid = '$bid' OR businessid = '$bid2' OR businessid = '$bid3' OR businessid = '$bid4' OR businessid = '$bid5' OR businessid = '$bid6' OR businessid = '$bid7' OR businessid = '$bid8' OR businessid = '$bid9' OR businessid = '$bid10') ORDER BY businessname DESC";}
    elseif($security_level>2&&$security_level<7){$query = "SELECT * FROM business WHERE companyid = '$companyid' AND ($districtquery) ORDER BY businessname DESC";}
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);

    $num--;
    while($num>=0){
       $businessname=Treat_DB_ProxyOld::mysql_result($result,$num,"businessname");
       $busid=Treat_DB_ProxyOld::mysql_result($result,$num,"businessid");

       if ($busid==$businessid){$show="SELECTED";}
       else{$show="";}

       echo "<option value=businventor2.php?bid=$busid&cid=$companyid $show>$businessname</option>";
       $num--;
    }

    echo "</select></td><td></form></td></tr></table></center><p>";
}
    $security_level=$sec_bus_inventor1;

    if ($showerror==1){echo "<center><table width=90%><tr><td><font color=red><b>There was a problem with your inventory count sheet.  Either an area was deleted or moved.  We saved everything up until that point. Also, please remember to not use your browser's back button when updating items.  When you use the back button, the page will look like it did when you were last there and not reflect the changes you have made.</td></tr></table></center><p>";}

    $day = date("d");
    $year = date("Y");
    $month = date("m");

    $today="$year-$month-$day";
    $day=date("l");

    $lastyear=substr($today,0,4);
    $lastmonth=substr($today,5,2);
    $lastmonth++;
    if ($lastmonth==13){$lastyear++;$lastmonth=1;}
    if ($lastmonth<10){$lastmonth="0$lastmonth";}
    $lastdate="$lastyear-$lastmonth-01";
    $lastdate=prevday($lastdate);

    if ($viewdate!=""){$godate=$viewdate;}
    else{
       while ($day!=$week_end&&$today!=$lastdate){
          $today=nextday($today);
          if ($day=="Monday"){$day="Tuesday";}
          elseif ($day=="Tuesday"){$day="Wednesday";}
          elseif ($day=="Wednesday"){$day="Thursday";}
          elseif ($day=="Thursday"){$day="Friday";}
          elseif ($day=="Friday"){$day="Saturday";}
          elseif ($day=="Saturday"){$day="Sunday";}
          elseif ($day=="Sunday"){$day="Monday";}
       }
       $godate=$today;
    }

    $prevdate=prevday($godate);
    $day=dayofweek($prevdate);
    $thismonth=substr($godate,5,2);
    $prevmonth=substr($prevdate,5,2);

    while ($day!=$week_end&&$prevmonth==$thismonth){
       $thismonth=substr($prevdate,5,2);
       $prevdate=prevday($prevdate);
       $prevmonth=substr($prevdate,5,2);
       if ($day=="Monday"){$day="Sunday";}
       elseif ($day=="Sunday"){$day="Saturday";}
       elseif ($day=="Saturday"){$day="Friday";}
       elseif ($day=="Friday"){$day="Thursday";}
       elseif ($day=="Thursday"){$day="Wednesday";}
       elseif ($day=="Wednesday"){$day="Tuesday";}
       elseif ($day=="Tuesday"){$day="Monday";}
    }

    $nextdate=nextday($godate);
    $day=dayofweek($nextdate);
    $thismonth=substr($nextdate,5,2);
    $nextdate2=nextday($nextdate);
    $nextmonth=substr($nextdate2,5,2);

    while ($day!=$week_end&&$nextmonth==$thismonth){

       $nextdate=nextday($nextdate);
       $nextdate2=nextday($nextdate2);
       $thismonth=substr($nextdate,5,2);
       $nextmonth=substr($nextdate2,5,2);
       if ($day=="Monday"){$day="Tuesday";}
       elseif ($day=="Sunday"){$day="Monday";}
       elseif ($day=="Saturday"){$day="Sunday";}
       elseif ($day=="Friday"){$day="Saturday";}
       elseif ($day=="Thursday"){$day="Friday";}
       elseif ($day=="Wednesday"){$day="Thursday";}
       elseif ($day=="Tuesday"){$day="Wednesday";}
    }

    $godatename=dayofweek($godate);

    if ($sec_bus_inventor2>0){$businventor2="businventor2.php";}
    else{$businventor2="businesstrack.php";}
    if ($sec_bus_inventor3>0){$businventor3="businventor3.php";}
    else{$businventor3="businesstrack.php";}
    if ($sec_bus_inventor4>0){$businventor4="businventor4.php";}
    else{$businventor4="businesstrack.php";}
    if ($sec_bus_inventor5>0){$businventor5="businventor5.php";}
    else{$businventor5="businesstrack.php";}

    $total=0;
    echo "<center><table width=90% cellspacing=0 cellpadding=0>";

    //echo "<tr bgcolor=#CCCCCC valign=top><td width=1%><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=15%><center><a href=businventor.php?cid=$companyid&bid=$businessid style=$style><font color=blue>Inventory $'s</a></center></td><td width=1% align=right><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1% bgcolor=#E8E7E7><img src=leftcrn.jpg height=6 width=6 align=top></td><td width=15% bgcolor=#E8E7E7><center><a href=businventor2.php?cid=$companyid&bid=$businessid style=$style style=$style><font color=black>Inventory Count</a></center></td><td width=1% align=right bgcolor=#E8E7E7><img src=rightcrn.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1%><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=15%><center><a href=businventor3.php?cid=$companyid&bid=$businessid style=$style style=$style><font color=blue>Inventory Setup</a></center></td><td width=1% align=right><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1%><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=15%><center><a href=businventor4.php?cid=$companyid&bid=$businessid style=$style style=$style><font color=blue>Recipes</a></center></td><td width=1% align=right><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1%><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=15%><center><a href=businventor5.php?cid=$companyid&bid=$businessid style=$style style=$style><font color=blue>Vendors/PO's</a></center></td><td width=1% align=right><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td bgcolor=white width=25%></td></tr>";
	?>
	<tr bgcolor=#CCCCCC valign=top>
		<td width=1% style="background:url(leftcrn2.jpg) no-repeat;">
			&nbsp;
		</td>
		<td width=15%>
			<center><a href=businventor.php?cid=<?php echo $companyid;?>&bid=<?php echo $businessid;?> style=$style><font color=blue>Inventory $'s</a></center>
		</td>
		<td width=1% align=right>
			<img src=rightcrn2.jpg height=6 width=6 valign=top>
		</td>
		<td width=1% bgcolor=white>
			&nbsp;
		</td>
		<td width=1% bgcolor=#E8E7E7 style="background:url(leftcrn.jpg) no-repeat #E8E7E7;">
			&nbsp;
		</td>
		<td width=15% bgcolor=#E8E7E7>
			<center><a href=businventor2.php?cid=<?php echo $companyid;?>&bid=<?php echo $businessid;?> style=$style style=$style><font color=black>Inventory Count</a></center>
		</td>
		<td width=1% align=right bgcolor=#E8E7E7>
			<img src=rightcrn.jpg height=6 width=6 valign=top>
		</td>
		<td width=1% bgcolor=white>
			&nbsp;
		</td>
		<td width=1% style="background:url(leftcrn2.jpg) no-repeat;">
			&nbsp;
		</td>
		<td width=15%>
			<center><a href=businventor3.php?cid=<?php echo $companyid;?>&bid=<?php echo $businessid;?> style=$style style=$style><font color=blue>Inventory Setup</a></center>
		</td>
		<td width=1% align=right>
			<img src=rightcrn2.jpg height=6 width=6 valign=top>
		</td>
		<td width=1% bgcolor=white>
			&nbsp;
		</td>
		<td width=1% style="background:url(leftcrn2.jpg) no-repeat;">
			&nbsp;
		</td>
		<td width=15%>
			<center><a href=businventor4.php?cid=<?php echo $companyid;?>&bid=<?php echo $businessid;?> style=$style style=$style><font color=blue>Recipes</a></center>
		</td>
		<td width=1% align=right>
			<img src=rightcrn2.jpg height=6 width=6 valign=top>
		</td>
		<td width=1% bgcolor=white>
			&nbsp;
		</td>
		<td width=1% style="background:url(leftcrn2.jpg) no-repeat;">
			&nbsp;
		</td>
		<td width=15%>
			<center><a href=businventor5.php?cid=<?php echo $companyid;?>&bid=<?php echo $businessid;?> style=$style style=$style><font color=blue>Vendors/PO's</a></center>
		</td>
		<td width=1% align=right>
			<img src=rightcrn2.jpg height=6 width=6 valign=top>
		</td>
		<td width=1% bgcolor=white>
			&nbsp;
		</td>
		<td bgcolor=white width=25%>
			&nbsp;
		</td>
	</tr>
	<?php

    echo "<tr height=2><td colspan=4></td><td colspan=3 bgcolor=#E8E7E7></td><td></td><td colspan=4></td><td colspan=4></td><td colspan=4></td><td colspan=1></td></tr>";

    echo "</table></center><center><table width=90% cellspacing=0 cellpadding=0>";

	//////////////////////
	/////New Inventory////
	//////////////////////
	if($inventory_type == 1){

		if(@$inv_count_date == null){
			$query7 = "SELECT date FROM inv_count_date WHERE businessid = $businessid ORDER BY date DESC LIMIT 10";
			$result7 = Treat_DB_ProxyOld::query($query7);
			
			$start_date = date("Y-m-d H:i:00");
		
			echo "<tr bgcolor=#E8E7E7><td align=left colspan=5><br>&nbsp;Start a Count:<br>";
			echo "&nbsp;&nbsp;&nbsp;<form style=\"display:inline;\" action=businventor2.php?cid=$companyid&bid=$businessid method=post><input type=hidden name=new_inv_count value=1>";
			echo "<input type=text size=20 name=inv_count_date value='$start_date' class=\"datepicker\"><input type=submit value='GO'></form><br>";
			echo "&nbsp;Continue a Count:<br>";
			
			while($r7 = mysql_fetch_array($result7)){
				$inv_datetime = $r7["date"];
				$inv_datename = dayofweek(substr($inv_datetime,0,10));
				echo "&nbsp;&nbsp;&nbsp;<form style=\"display:inline;\" action=businventor2.php?cid=$companyid&bid=$businessid method=post><input type=hidden name=inv_count_date value=\"$inv_datetime\"><input type=submit value='>'>&nbsp;$inv_datetime $inv_datename</form><br>";
			}
			
			echo "<br><td></tr>";
			die();
		}
		else{
			if($new_inv_count == 1){
				$query7 = "INSERT INTO inv_count_date (businessid,date,audit_user) VALUES ($businessid,'$inv_count_date','$user')";
				$result7 = Treat_DB_ProxyOld::query($query7);
			}
			
			$godate = $inv_count_date;
			$godatename = dayofweek(substr($godate,0,10));
			
			echo "<tr bgcolor=#E8E7E7><td align=left colspan=4>&nbsp;<font color=red size=5><b>$godatename $godate</b></font></td>";
		}
	}
	else{
		echo "<tr><td align=left colspan=4 bgcolor=#E8E7E7><font size=1>[<a href=businventor2.php?bid=$businessid&cid=$companyid&viewdate=$prevdate style=$style>PREV</a>]</font> <font color=red size=5><b>$godatename $godate</b></font> <font size=1> [<a href=businventor2.php?bid=$businessid&cid=$companyid&viewdate=$nextdate style=$style>NEXT</a>]</font></td>";
	}
	
	echo "<td colspan=2 align=right bgcolor=#E8E7E7><form action=printinventory.php?bid=$businessid&cid=$companyid&viewdate=$godate&prevdate=$prevdate method=post><img src=highlight.gif height=16 width=16 border=0 alt='Highlight Category'>&nbsp<img src=print.gif height=16 width=16 border=0 alt='Print'>&nbsp<select name=printme onChange='changePage(this.form.printme)'><option value=businventor2.php?cid=$companyid&bid=$businessid>Printable Version...</option><optgroup><option value='printinventory.php?bid=$businessid&cid=$companyid&viewdate=$godate&page=0&prevdate=$prevdate'>...with Page Breaks</option><option value='printinventory.php?bid=$businessid&cid=$companyid&viewdate=$godate&page=1&prevdate=$prevdate'>...without Page Breaks</option><option value='printinventory3.php?bid=$businessid&cid=$companyid&viewdate=$godate'>...Descending Dollars</option></optgroup><optgroup label='...or Certain Area...'>";
	
    $query7 = "SELECT * FROM inv_area WHERE businessid = '$businessid' ORDER BY inv_areaname DESC";
    $result7 = Treat_DB_ProxyOld::query($query7);
    $num7=Treat_DB_ProxyOld::mysql_numrows($result7);
    //mysql_close();

    $num7--;
    while($num7>=0){
       $inv_areaid=Treat_DB_ProxyOld::mysql_result($result7,$num7,"inv_areaid");
       $inv_areaname=Treat_DB_ProxyOld::mysql_result($result7,$num7,"inv_areaname");
       echo "<option value='printinventory.php?bid=$businessid&cid=$companyid&viewdate=$godate&page=1&areaid=$inv_areaid&prevdate=$prevdate'>$inv_areaname</option>";

       $num7--;
    }

    echo "</optgroup>";

    $query7 = "SELECT * FROM acct_payable WHERE businessid = '$businessid' AND cos = '1' ORDER BY acct_name DESC";
    $result7 = Treat_DB_ProxyOld::query($query7);
    $num7=Treat_DB_ProxyOld::mysql_numrows($result7);

    $num7--;
    $temp_num=$num7;

    echo "<optgroup label='...or G/L Account...'>";

    while($temp_num>=0){
       $acct_payableid=Treat_DB_ProxyOld::mysql_result($result7,$temp_num,"acct_payableid");
       $acct_name=Treat_DB_ProxyOld::mysql_result($result7,$temp_num,"acct_name");
       echo "<option value='printinventory.php?bid=$businessid&cid=$companyid&showonly=$acct_payableid&viewdate=$godate&page=0&prevdate=$prevdate'>$acct_name</option>";

       $temp_num--;
    }
    echo "</optgroup>";
    echo "<optgroup label='...or Highlight...'>";

    while($num7>=0){
       $acct_payableid=Treat_DB_ProxyOld::mysql_result($result7,$num7,"acct_payableid");
       $acct_name=Treat_DB_ProxyOld::mysql_result($result7,$num7,"acct_name");
       echo "<option value='businventor2.php?bid=$businessid&cid=$companyid&highlight=$acct_payableid'>$acct_name</option>";

       $num7--;
    }
    echo "<option value=businventor2.php?cid=$companyid&bid=$businessid&highlight=0>....Clear Highlighted Items</option>";
    echo "</optgroup>";
    echo "</select></td></tr>";
    echo "<tr><td colspan=6></form></td></tr></table>";
    echo "<table width=90% cellspacing=0 cellpadding=0><tr><td align=left colspan=6 height=1 bgcolor=black><a name=top></a><form action=saveinvcount.php method=post><input type=hidden name=businessid id=businessid value=$businessid><input type=hidden name=companyid value=$companyid><input type=hidden name=date id=date value=\"$godate\"></td></tr>";

////////////////////INVENTORY COUNTS//////////////////////////////////////////////////////////

    $totonhand=0;
    $rowcount=0;

    $query7 = "SELECT * FROM inv_area WHERE businessid = '$businessid' ORDER BY inv_areaname DESC";
    $result7 = Treat_DB_ProxyOld::query($query7);
    $num7=Treat_DB_ProxyOld::mysql_numrows($result7);

    $subtotal=0;
    $curarrow=0;
    $num7--;
    while($num7>=0){

       $prevnum=$num7+1;
       $nextnum=$num7-1;

       $prevareaid=Treat_DB_ProxyOld::mysql_result($result7,$prevnum,"inv_areaid");
       $nextareaid=Treat_DB_ProxyOld::mysql_result($result7,$nextnum,"inv_areaid");
       $inv_areaid=Treat_DB_ProxyOld::mysql_result($result7,$num7,"inv_areaid");
       $gotoarea=$inv_areaid;
       $inv_areaname=Treat_DB_ProxyOld::mysql_result($result7,$num7,"inv_areaname");

       $showtop="<a href=#top><img src=top.gif height=16 width=16 alt='Top' border=0></a>";
       $showbottom="<a href=#bottom><img src=bottom.gif height=16 width=16 alt='Bottom' border=0></a>";
       if($prevareaid!=0){$showup="<a href=#$prevareaid><img src=up.gif height=16 width=16 alt='Previous Area' border=0></a>";}
       else{$showup="";}
       if($nextareaid!=0){$showdown="<a href=#$nextareaid><img src=down.gif height=16 width=16 alt='Next Area' border=0></a>";}
       else{$showdown="";}

       echo "<tr bgcolor=#FFFF99 ><td align=left><a name=$gotoarea></a><b><i>$inv_areaname</i></b> $showtop $showup $showdown $showbottom</td><td align=right><b><i>$/unit</i></b> <sup><font size=2 color=gray>Updated</font></sup></td><td align=right><b><i>Count</td><td></td><td align=right><b><i>Total</i></b></td><td width=10% align=right><a href=alphabetize.php?bid=$businessid&cid=$companyid&inv_areaid=$inv_areaid&date=$godate&alpha=3 style=$style><img src=reorder.gif width=16 height=16 border=0 alt='Revert back to last saved custom sort'></a> <a href=alphabetize.php?bid=$businessid&cid=$companyid&inv_areaid=$inv_areaid&date=$godate&alpha=2 style=$style><img src=location.gif width=16 height=16 border=0 alt='Sort By Location #'></a> <a href=alphabetize.php?bid=$businessid&cid=$companyid&inv_areaid=$inv_areaid&date=$godate style=$style><img src=alpha.gif height=16 width=16 border=0 alt='Alphabetize'></a> <a href=inv_sort.php?bid=$businessid&cid=$companyid&inv_areaid=$inv_areaid&date=$godate style=$style> <img src=sort.gif width=16 height=16 border=0 alt='Custom Sort'></a></td></tr>";
       echo "<tr bgcolor=#CCCCCC><td height=1 colspan=6></td></tr>";
/////////////////////////
    $query = "SELECT * FROM inv_areadetail WHERE areaid = '$inv_areaid' AND active = '0' ORDER BY orderid";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);

    $showcolor="white";
    $lastorderid=-1000;
    $num--;
    while($num>=0){
       $inv_itemid=Treat_DB_ProxyOld::mysql_result($result,$num,"inv_itemid");
       $inv_areadetailid=Treat_DB_ProxyOld::mysql_result($result,$num,"inv_areadetailid");
       $previtemid=Treat_DB_ProxyOld::mysql_result($result,$num+1,"inv_areadetailid");
       $orderid=Treat_DB_ProxyOld::mysql_result($result,$num,"orderid");
       $locationid=Treat_DB_ProxyOld::mysql_result($result,$num,"location");
       $nextitemid=Treat_DB_ProxyOld::mysql_result($result,$num-1,"inv_areadetailid");
       $orderid=Treat_DB_ProxyOld::mysql_result($result,$num,"orderid");

       if ($locationid!="0"){$locationid="<font color=gray>(#$locationid)</font>";}
       else{$locationid="";}

       $query9 = "SELECT item_name,inv_areaid,apaccountid,units,price,price_date,item_code,pack_size,pack_qty,v_update FROM inv_items WHERE inv_itemid = '$inv_itemid'";
       $result9 = Treat_DB_ProxyOld::query($query9);

       $item_name=Treat_DB_ProxyOld::mysql_result($result9,0,"item_name");
       $inv_areaid=Treat_DB_ProxyOld::mysql_result($result9,0,"inv_areaid");
       $apaccountid=Treat_DB_ProxyOld::mysql_result($result9,0,"apaccountid");
       $units=Treat_DB_ProxyOld::mysql_result($result9,0,"units");
       $price=Treat_DB_ProxyOld::mysql_result($result9,0,"price");
       $price=number_format($price, 2, '.', '');
       $price_date=Treat_DB_ProxyOld::mysql_result($result9,0,"price_date");
       $item_code=Treat_DB_ProxyOld::mysql_result($result9,0,"item_code");
       $pack_size=Treat_DB_ProxyOld::mysql_result($result9,0,"pack_size");
       $pack_qty=Treat_DB_ProxyOld::mysql_result($result9,0,"pack_qty");
       $v_update=Treat_DB_ProxyOld::mysql_result($result9,0,"v_update");

       if($v_update==1){$fontcolor="blue";}
       else{$fontcolor="black";}

       if ($item_code==0){$item_code="";}
       if ($pack_size!=""&&$units!=""&&$pack_qty>1){$price=number_format($price/$pack_qty, 2, '.', '');}

       $day=substr($price_date,8,2);
       $month=substr($price_date,5,2);
       $year=substr($price_date,2,2);

          $query62 = "SELECT price FROM inv_count WHERE inv_areadetailid = '$inv_areadetailid' AND date < '$godate' ORDER BY date DESC LIMIT 1";
          $result62 = Treat_DB_ProxyOld::query($query62);
          $num62=Treat_DB_ProxyOld::mysql_numrows($result62);

          //$pastprice=mysql_result($result62,0,"price");
		  if($num62){
			  $pastprice=mysql_result($result62,0,"price");
		  }

          $query2 = "SELECT * FROM inv_count WHERE inv_areadetailid = '$inv_areadetailid' AND date = '$godate'";
          $result2 = Treat_DB_ProxyOld::query($query2);
          $num2=Treat_DB_ProxyOld::mysql_numrows($result2);

          if ($num2!=0){
             $value=Treat_DB_ProxyOld::mysql_result($result2,0,"amount");
             $savedprice=Treat_DB_ProxyOld::mysql_result($result2,0,"price");
          }

          if($savedprice != $price && $num2 != 0){
			  $showprice = "<sup><font color=red size=2>$month/$day/$year</font></sup>";
			  $newprice = "<font size=1 color=red><i> - Current Price: $$price</i></font>";
			  $price = $savedprice;
		  }else{
			  $showprice = "<sup><font color=#444455 size=2>$month/$day/$year</font></sup>";
			  $newprice = "";
			}

          if ($lastorderid!=$orderid){
             if ($showcolor=="#F3F3F3"){$showcolor="white";}
             elseif ($showcolor=="white"){$showcolor="#F3F3F3";}
             $showcolor2=$showcolor;
          }
          if ($highlight==$apaccountid){$showcolor="#00FF00";}

          $totonhand=$totonhand+($price*$value);
          $onhand=number_format($price*$value, 2, '.', '');
          //$totonhand=$totonhand+$onhand;
          if ($highlight==$apaccountid){$subtotal=$subtotal+$onhand;}

          $pastprice=@money($price-$pastprice);
          if ($pastprice==0||$pastprice==$price){$pastprice="";}
          if ($pastprice>=1){$pastprice="<font color=red><b>+$pastprice</b></font>";}
          elseif ($pastprice>0){$pastprice="+$pastprice";}
          if ($pastprice<=-1){$pastprice="<font color=green><b>$pastprice</b></font>";}

          /*$valuename="val$mycount";
          $pricename="price$mycount";
          $ordername="order$mycount";
          $showsafe="safe$mycount";*/
		  
		  $valuename = "val[$mycount]";
		  $pricename = "price[$mycount]";
		  $ordername = "order[$mycount]";
		  $showsafe = "safe[$mycount]";
		  $fieldnum = "fieldnum[$mycount]";
		  $fieldval = $mycount;
		  $area_id = "area_id[$mycount]";
		  $item_id = "item_id[$mycount]";

          $safeguard="$mycount.$inv_areadetailid";

          //$mycount++;
          $price=number_format($price, 2, '.', '');
          $counter++;
          $counter2=$counter+1000;
          //echo "<tr bgcolor='$showcolor' onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor='$showcolor'><td><input type=hidden name=$showsafe value=$safeguard><a href=businventor3.php?bid=$businessid&cid=$companyid&view2=editinv&editid=$inv_itemid&editinvarea=$gotoarea title='Edit Item' style=$style><font color=$fontcolor>$item_name</font></a> $locationid $newprice</td><td align=right>$<input type=text size=5 name='$pricename' value='$price' tabindex=1>$showprice</td><td align=right><input type=text size=5 name='$valuename' value='$value' tabindex=2 IndexId=$curarrow autocomplete=off></td><td>&nbsp $units</td><td align=right>$$onhand<td width=20% align=right>$pastprice <a href=invitem_hist.php?bid=$businessid&cid=$companyid&inv_areadetailid=$inv_areadetailid&inv_areaid=$gotoarea style=$style title='COST HISTORY'><img src=folder.gif height=16 width=16 border=0></a></td></tr>";
		  
		  ?>
			<tr bgcolor="<?php echo $showcolor;?>" onMouseOver="this.bgColor='#CCCCCC'" onMouseOut="this.bgColor='<?php echo $showcolor;?>'">
				<td align="left">
					<input type="hidden" class="row_<?php echo $mycount; ?>" name="<?php echo $showsafe; ?>" value="<?php echo $safeguard; ?>">
					<a href="businventor3.php?bid=<?php echo $businessid; ?>&cid=<?php echo $companyid; ?>&view2=editinv&editid=<?php echo $inv_itemid; ?>&editinvarea=<?php echo $gotoarea; ?>"
					   title="Edit Item" style="<?php echo $style; ?>"><font color="<?php echo $fontcolor; ?>"><?php echo $item_name; ?></font></a>
					<?php echo $locationid." ".$newprice; ?>
				</td>
				<td align=right>
					$<input type="text" size="5" class="row_<?php echo $mycount; ?> price" name="<?php echo $pricename; ?>" value="<?php echo $price; ?>" tabindex="1"><?php echo $showprice; ?>
				</td>
				<td align="right">
					<input type="text" size="5" class="row_<?php echo $mycount; ?> val" name="<?php echo $valuename; ?>" value="<?php echo $value; ?>" tabindex="2" IndexId="<?php echo $curarrow; ?>" autocomplete="off">
				</td>
				<td>
					&nbsp; <?php echo $units; ?>
				</td>
				<td id="linetot_<?php echo $mycount; ?>" align="right">
					$<?php echo $onhand; ?>
				</td>
				
				<td width="20%" align="right">
					<?php echo $pastprice; ?> <a href="invitem_hist.php?bid=<?php echo $businessid; ?>&cid=<?php echo $companyid; ?>&inv_areadetailid=<?php echo $inv_areadetailid; ?>&inv_areaid=<?php echo $gotoarea; ?>" style="<?php echo $style; ?>" title="COST HISTORY"><img src="folder.gif" height="16" width="16" border="0"></a>
					<input type="hidden" class="row_<?php echo $mycount; ?>" name="<?php echo $fieldnum; ?>" value="<?php echo $fieldval; ?>" />
					<input type="hidden" class="row_<?php echo $mycount; ?>" name="<?php echo $area_id; ?>" value="<?php echo $gotoarea; ?>" />
					<input type="hidden" class="row_<?php echo $mycount; ?>" name="<?php echo $item_id; ?>" value="<?php echo $inv_itemid; ?>" />
				</td>
			</tr>
		  <?php
		  $mycount++;
		  
          echo "<tr bgcolor=#CCCCCC><td height=1 colspan=6></td></tr>";
          $rowcount++;
          ////////////////////REMOVED ARROW KEYS: onkeydown='return checkKey()'
          ////////////////////<a href=movearea.php?bid=$businessid&cid=$companyid&inv_areadetailid=$inv_areadetailid&move=up&inv_areaid=$gotoarea&swapid=$previtemid style=$style title='MOVE UP'><img src=up.gif height=16 width=16 border=0></a> <a href=movearea.php?bid=$businessid&cid=$companyid&inv_areadetailid=$inv_areadetailid&move=down&inv_areaid=$gotoarea&swapid=$nextitemid style=$style title='MOVE DOWN'><img src=down.gif height=16 width=16 border=0></a>

       $showcolor=$showcolor2;
       $lastorderid=$orderid;
       $num--;
       $value="";
       $curarrow++;
    }
//////////////////////
       $num7--;
    }

    if ($highlight>0){$showsubtotal="<font color=green>($$subtotal)</font> ";}

    $totonhand=number_format($totonhand, 2, '.', '');
    echo "<tr><td colspan=6 height=1 bgcolor=black><a name=bottom></a></td></tr>";
	
    //echo "<tr><td colspan=4 bgcolor=#E8E7E7><input type=submit value='Save'> $showtop</td><td align=right bgcolor=#E8E7E7>$showsubtotal<b>$$totonhand</b></td><td bgcolor=#E8E7E7></form></td></tr>";
	?>
	<tr>
		<td colspan=4 bgcolor=#E8E7E7>
			<?php echo $showtop; ?>
		</td>
		<td align=right bgcolor=#E8E7E7>
			<?php echo $showsubtotal; ?><b><span id="sum_tot">$<?php echo $totonhand; ?></span></b>
		</td>
		<td bgcolor=#E8E7E7>
			</form>
		</td>
	</tr>
	<?php
	
    echo "<tr><td colspan=6>*<font color=blue>Items showing in blue, have last been updated by your vendor.</td></tr>";
    echo "<tr><td colspan=6>Total Rows: $rowcount</td></tr>";
    echo "</table></center>";

    //mysql_close();
////////////////////END WEEKLY INVENTORY/////////////////////////////////////////////////////


    echo "<br><FORM ACTION=businesstrack.php method=post>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
	echo "<center><INPUT TYPE=submit VALUE=' Return to Main Page'></FORM></center>";

	?>
	
	<div style="display:none;" class="ajax_box" id="ajax_saving">
		Saving...
	</div>
	
	<div style="display:none;" class="ajax_box" id="ajax_error">&nbsp;</div>
	
	<div class="ajax_box" id="ajax_notice" style="display:none;" >
		<div id="close_notice"><a id="close_it" href="javascript:void(0);">X</a></div>
		There have been alterations made to your inventory count page. No longer will you have to save it. It
		automatically saves when the cursor leaves the box.
	</div>
			
	<script src="/assets/js/jquery-1.7.2.min.js"></script>
	<script>
	$(document).ready(function(){
		var cur_row,
			cur_classes,
			price,
			val,
			item_name;
		
		$('.price, .val').blur(function(){
			cur_classes = $(this).attr('class').split(/\s+/);
			cur_row = cur_classes[0];
			cur_row = cur_row.substr(4);
			
			item_name = $("input[name='safe[" +cur_row+ "]']").next().html();
			
			price = $("input[name='price[" +cur_row+ "]']").val();
			val = $("input[name='val[" +cur_row+ "]']").val();
			
			//price and val must be set
			if( $.trim(price).length > 0 && $.trim(val).length > 0 ) {
				$('#ajax_saving').show();
			
				$.post("/ta/inventory/saveInvCountAjax",
					{
						'safe' : $("input[name='safe[" +cur_row+ "]']").val(),
						'price' : price,
						'val' : val,
						'fieldnum' : $("input[name='fieldnum[" +cur_row+ "]']").val(),
						'area_id' : $("input[name='area_id[" +cur_row+ "]']").val(),
						'item_id' : $("input[name='item_id[" +cur_row+ "]']").val(),
						'bus_id' : $('#businessid').val(),
						'date' : $('#date').val()
					},
					function(data){
						if(data.status){
							$('#linetot_'+data.field_num).html(data.linetot);
							$('#sum_tot').html(data.tot);
						}
					},
					"json"
				).success(function(){
						$('#ajax_saving').hide();
				}).error(function(){
					$('#ajax_saving').hide();
					$('#ajax_error').html('<span style="color:#f00;">There was an error saving</span> '+item_name).show();
				});
			}
		});
		
		$('#ajax_error').click(function(){
			$(this).hide();
		});
		
		$('#close_it').click(function(){
			$('#ajax_notice').hide();
		});
	});
	</script>
<?php
	
	google_page_track();
}
?>
</body>
</html>