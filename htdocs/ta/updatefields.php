<?php

if ( !defined('DOC_ROOT'))
{
	define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
}
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');

$user = \EE\Controller\Base::getSessionCookieVariable('usercook','');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook','');
$businessid = isset($_POST['businessid'])?$_POST['businessid']:'';
$companyid = isset($_POST['companyid'])?$_POST['companyid']:'';
$editid = isset($_POST['editid'])?$_POST['editid']:'';
$field_name = isset($_POST['field_name'])?$_POST['field_name']:'';
$type = isset($_POST['type'])?$_POST['type']:'';
$required = isset($_POST['required'])?$_POST['required']:'';
$customer = isset($_POST['customer'])?$_POST['customer']:'';
$production = isset($_POST['production'])?$_POST['production']:'';

if (intval($businessid) < 1)
{
	$businessid = isset($_GET['bid'])?$_GET['bid']:'';
	$companyid = isset($_GET['cid'])?$_GET['cid']:'';
	$editid = isset($_GET['editid'])?$_GET['editid']:'';
	$active = isset($_GET['active'])?$_GET['active']:'';
}


$location = sprintf("buscaterroom2.php?bid=%d&cid=%d", $businessid, $companyid);
if( false === \Treat_Model_User_Login::login($user, $pass, false) )
{
	header('Location: ./' . $location);
	exit();
}

if ($editid > 0 && trim($active) !== '')
{
	$customField = \EE\Model\Invoice\CustomFields::get(intval($editid), true);
	$customField->active = intval($active);
	if( $customField->save() )
	{
		$location .= '&success=1';
	}
}
else if ($editid > 0)
{
	$customField = \EE\Model\Invoice\CustomFields::get(intval($editid), true);
	$customField->field_name = $field_name;
	$customField->type = $type;
	$customField->required = $required ? 1 : 0;
	$customField->customer = $customer ? 1 : 0;
	$customField->production = $production ? 1 : 0;
	if( $customField->save() )
	{
		$location .= '&success=1';
	}
}
else if (trim($field_name) !== "" && trim($type) !== "")
{
	$customField = new \EE\Model\Invoice\CustomFields;
	$customField->businessid = intval($businessid);
	$customField->field_name = $field_name;
	$customField->type = $type;
	$customField->required = $required ? 1 : 0;
	$customField->customer = $customer ? 1 : 0;
	$customField->production = $production ? 1 : 0;
	if ( $customField->save() )
	{
		$location = "buscaterroom2.php?bid=$businessid&cid=$companyid&editid={$customField->id}";
	}
}

header('Location: ./' . $location);
