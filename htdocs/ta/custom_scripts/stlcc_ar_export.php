<?php

/////////////////////////////////////////////////
//////////////// STLCC AR Export/////////////////
/////////////////////////////////////////////////

define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');

echo "Start<p>";

$startdate = "2011-10-28";
$enddate = date("Y-m-d");
while(dayofweek($enddate) != "Thursday"){$enddate = prevday($enddate);}
$data = "";

$query = "SELECT * FROM invoice WHERE date >= '$startdate' AND date <= '$enddate' AND businessid IN (22, 23, 82) AND accountid IN (451, 452, 453) AND export = 1 AND route = ''";
$result = DB_Proxy::query($query);

$data = "Invoice#,Date,Reference,Contact,Total,Fund,Org,Program\r\n";

while($r = mysql_fetch_array($result)){
	$businessid = $r["businessid"];
	$invoiceid = $r["invoiceid"];
	$date = $r["date"];
	$reference = $r["reference"];
	$contact = $r["contact"];
	$total = $r["total"];

	$total = round($total,2);

	$data .= "$invoiceid,$date,\"$reference\",\"$contact\",$total";
	$inv_total = number_format($total, 2);
	$email_msg .= "$invoiceid ($date): $$inv_total<br>";

	if($businessid == 22){
		$custom_array = array(104, 105, 107);
	}
	elseif($businessid == 23){
		$custom_array = array(98, 99, 101);
	}
	elseif($businessid == 82){
		$custom_array = array(108, 109, 111);
	}

	foreach($custom_array AS $key => $value){
		$query2 = "SELECT value FROM invoice_custom_values WHERE invoiceid = $invoiceid AND customid = $value";
		$result2 = DB_Proxy::query($query2);

		$value = mysql_result($result2,0,"value");

		$data .= ",\"$value\"";
	}

	$data .= "\r\n";

	$query2 = "UPDATE invoice SET route = '-1' WHERE invoiceid = $invoiceid";
	$result2 = DB_Proxy::query($query2);
}

file_put_contents("/srv/home/stlcc/invoices/stlcc.csv", $data);

$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$headers .= "From: support@treatamerica.com\r\n";
$subject = "St. Louis CC AR Export for $enddate";
$email="smartin@essentialpos.com,janetm@tafoodservices.com";

mail($email, $subject, $email_msg, $headers);

echo "Done";
?>