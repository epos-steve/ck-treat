<?php

/////////////////////////////////////////////////
/////// Marshalltown Balance Update//////////////
/////////////////////////////////////////////////

define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');

$query = "UPDATE mburris_businesstrack.customer c
INNER JOIN pd_portal.CustomFieldValue pp
ON pp.CustomerId = c.customerid
SET c.balance = 14
WHERE pp.Value = 'M4' and c.businessid = 59";
$result = DB_Proxy::query($query);

$query = "UPDATE mburris_businesstrack.customer c
INNER JOIN pd_portal.CustomFieldValue pp
ON pp.CustomerId = c.customerid
SET c.balance = 9
WHERE pp.Value = 'M0' and c.businessid = 59";
$result = DB_Proxy::query($query);
?>