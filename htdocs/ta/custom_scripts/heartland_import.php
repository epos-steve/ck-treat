<?php

/////////////////////////////////////////////////
/////////// Heartland CC Fee Import /////////////
/////////////////////////////////////////////////
define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');

ini_set("display_errors","true");

$strServer = "eftp.e-hps.com";
$strServerPort = "22";
$strServerUsername = "CompanyKitchen";
$strServerPassword = "USFcNq5x";

$date = $_GET["date"];
$enddate = $_GET["enddate"];
$debug = $_GET["debug"];

if($date == ""){
	$date = date("Ymd",mktime(0, 0, 0, date("m") , date("d"), date("Y")));
}

if($enddate == ""){
	$enddate = $date;
}

if($debug == 1){
	$table = 'cc_fee_copy';
}
else{
	$table = 'cc_fee';
}

while($date <= $enddate){

$errors = '';
$success = 0;
$failure = 0;

echo "Start...<br>";

//connect to server
$resConnection = ssh2_connect($strServer, $strServerPort);

if(ssh2_auth_password($resConnection, $strServerUsername, $strServerPassword)){
	//Initialize SFTP subsystem
	$resSFTP = ssh2_sftp($resConnection);
	
	////list files and choose correct one
	$files = array();
	$dirHandle = opendir("ssh2.sftp://$resSFTP/");

	while (false !== ($file = readdir($dirHandle))) {
		if ($file != '.' && $file != '..') {
			$files[] = $file;
		}
	}
	
	foreach($files AS $file => $filename){
		if(strpos($filename,$date)){
			$currentFile = $filename;
		}
	}
	
	echo "Today's file is: $currentFile";
	echo "<br>-----------------------------------------------------<br>";
	
	$strData = file_get_contents("ssh2.sftp://{$resSFTP}/$currentFile");
	
	if($strData == ""){
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\n";
		$headers .= "From: support@companykitchen.com\r\n";
		$subject = "Credit Card Fee Import Failed";
		$email = "smartin@essentialpos.com,traceym@treatamerica.com,sandrab@treatamerica.com";

		mail($email, $subject, "There was no data.", $headers);
	}
	
	$strData = explode("\n", $strData);
	
	foreach($strData AS $line){
		$data = explode("|", $line);
		$data[11] = round($data[11], 2);

		$year = substr($data[0], 0, 4);
		$month = substr($data[0], 4, 2);
		$day = substr($data[0], 6, 2);
		$feedate = "$year-$month-$day";
		
		if($businessid > 0 && $data[11] != 0 && false){
			
		}
		else{
			$newTID = $data[4] * 1;
			if($feedate >= '2013-08-13' && $feedate <= '2013-09-02'){
				$newTID = $data[3] * 1;
			}
			
			$query = "SELECT businessid FROM aeris_settings WHERE registryKey LIKE 'vivipos.fec.settings.layout.aerispos.heartlandDeviceId' AND registryValue = '$newTID'";
			$result = Treat_DB_ProxyOld::query($query);
		
			$businessid = Treat_DB_ProxyOld::mysql_result( $result, 0, 'businessid', '' );
			
			if($businessid > 0 && $data[11] != 0){
				$query = "INSERT INTO $table (businessid, date, amount) VALUES ($businessid,'$feedate','$data[11]') 
					ON DUPLICATE KEY UPDATE amount = amount + '$data[11]'";
				$result = Treat_DB_ProxyOld::query($query);
				echo $query . "<br>";
				$success++;
			}
			else{
				if( $data[3] == "N/A" || $data[3] == "Recurring Fees"){
					$query = "INSERT INTO $table (businessid, date, amount) VALUES ($lastbusinessid,'$feedate','$data[11]') 
						ON DUPLICATE KEY UPDATE amount = amount + '$data[11]'";
					$result = Treat_DB_ProxyOld::query($query);				
					echo $query . "<br>";
					$success++;
				}
				else{
					$query = "SELECT businessid FROM setup_detail WHERE setupid = 65 AND value LIKE '$data[1]'";
					$result = Treat_DB_ProxyOld::query($query);

					$businessid = Treat_DB_ProxyOld::mysql_result( $result, 0, 'businessid', '' );
					
					if($businessid > 0 && $data[11] != 0){
						$query = "INSERT INTO $table (businessid, date, amount) VALUES ($businessid,'$feedate','$data[11]') 
							ON DUPLICATE KEY UPDATE amount = amount + '$data[11]'";
						$result = Treat_DB_ProxyOld::query($query);
						echo $query . "<br>";
						$success++;
					}
					else{		
						echo $feedate . " " . $newTID . ": " . $data[2] . " [" . $data[11] . "]<br>";
						$errors .= "$data[1] $newTID $data[2]: No match found<br>";
						$failure++;
					}
				}
			}
		}
		
		$lastTID = $newTID;
		$lastbusinessid = $businessid;
	}
	
	///send errors email
	if($errors != ""){
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\n";
		$headers .= "From: support@companykitchen.com\r\n";
		$subject = "Credit Card Fee Auto Import Errors";
		$email = "smartin@essentialpos.com,traceym@treatamerica.com,sandrab@treatamerica.com";
		
		$errors = "Date: $date<br>Imported fees for $success accounts.<br>$failure Failures:<br>$errors";

		mail($email, $subject, $errors, $headers);
	}
	
}else{
	echo "Unable to authenticate on server<br>";
	
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\n";
	$headers .= "From: support@companykitchen.com\r\n";
	$subject = "Credit Card Fee Import Failed";
	$email = "smartin@essentialpos.com,traceym@treatamerica.com,sandrab@treatamerica.com";

	mail($email, $subject, "Unable to connect to server.", $headers);
}

echo "....End<br>";

$date = date("Ymd",mktime(0, 0, 0, substr($date,4,2) , substr($date,6,2)+1, substr($date,0,4)));
}
?>