<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');

/////////////////////////////////////////////////
/////////Federal Reserve Import//////////////////
//
//should be scheduled as a cron to run every ??
//
//update reserve table
/////////////////////////////////////////////////
/////////////////////////////////////////////////

$style = "text-decoration:none";

/////variables
$businessid = 263;
$companyid = 1;
$curmenu = 36;

/////open file...
$myFile = DIR_FTP_UPLOADS."/FRB/FRBKC.csv";

if(file_exists($myFile)){

    $handle = fopen($myFile, 'r');

    echo "Open $myFile<br>";

    /////if multiple events, check to see if all are canceled
    $global_events = array();
    $global_events_reserveid = array();
    $global_status = array();
    $global_cancel = array();

	$rowcount = 0;
    while ($pieces = fgetcsv($handle,NULL,"\t")){

		///reset businessid for denver
		$businessid = 263;
        ///////////////////////////////
        /////////PARSE FILE////////////
        ///////////////////////////////
        /*
        $pieces=array();
        $pieces = fgets($handle,512);

        $expr="/,(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))/";
        $pieces = preg_split($expr,trim($pieces));
        $pieces = preg_replace("/^\"(.*)\"$/","$1",$pieces);
        */

        ////////////////////////////
        /////figure which account///
        ////////////////////////////
        if($pieces[15] == "Gayla Hamilton"){
            ///gayla hamilton
            $accountid = 17427;
        }
        elseif($pieces[15] == "Elisabeth Reynolds"){
            ///beth reynolds
            $accountid = 14951;
        }
        elseif($pieces[15] == "Betsy Dierker"){
            ///betsy dierker
            $accountid = 17428;
        }
        elseif($pieces[15] == "Lisa Aquino"){
            ///lisa aquino
            $accountid = 17942;
        }
        elseif($pieces[15] == "Dee Ashworth"){
            ///dee ashworth
            $accountid = 17429;
        }
        elseif($pieces[15] == "Kerri Powell"){
            ///kerri blythe
            $accountid = 17458;
        }
		elseif($pieces[15] == "Jennifer Bradberry"){
			////jennifer bradberry
			$accountid = 20419;
		}
		elseif($pieces[15] == "Lisa Terry"){
            ///lisa terry (denver)
            $accountid = 18462;
			$businessid = 301;
        }
		elseif($pieces[15] == "Susan Burns"){
            $accountid = 23052;
        }
		elseif($pieces[15] == "Stephanie DeSouza"){
            $accountid = 23194;
        }
        else{
            ///catch all - gayla
            $accountid = 17427;
        }

        ///////////////////////////////
        //////////Add Rooms////////////
        ///////////////////////////////
        $query = "SELECT * FROM rooms WHERE roomname = '$pieces[6]' AND businessid = '$businessid'";
        $result = Treat_DB_ProxyOld::query($query, true);
        $num = mysql_numrows($result);

        if($num==0){
            ////insert room
            $query = "INSERT INTO rooms (businessid,companyid,roomname) VALUES ('$businessid','$companyid','$pieces[6]')";
            $result = Treat_DB_ProxyOld::query($query);
            $roomid = mysql_insert_id();
        }
        else{
            $roomid = mysql_result($result,0,"roomid");
        }

        ///////////////////////////////
        ///////Add Departments/////////
        ///////////////////////////////
		if($businessid == 301){$cust_deptid = 81;}
		else{$cust_deptid = 17;}

        $query = "SELECT * FROM invoice_custom_fields_select WHERE customid = $cust_deptid AND value = '$pieces[16]'";
        $result = Treat_DB_ProxyOld::query($query, true);
        $num = mysql_numrows($result);

        if($num==0){
            ////insert department
            $query = "INSERT INTO invoice_custom_fields_select (customid,value) VALUES ($cust_deptid,'$pieces[16]')";
            $result = Treat_DB_ProxyOld::query($query);
        }

        //////////////////////////////
        /////////map fields///////////
        //////////////////////////////
        $startdate=substr($pieces[22],0,10);
        $enddate=substr($pieces[14],0,10);

        $starthour=substr($pieces[22],11,5);
        $endhour=substr($pieces[11],11,5);

        $starthour=str_replace(":","",$starthour);
        $endhour=str_replace(":","",$endhour);
        
        ////event name
        $pieces[2] = str_replace("'","`",$pieces[2]);
        $pieces[2] .= " [$pieces[1]]";

        //////////////////////////////
        ///////////Events/////////////
        //////////////////////////////
        $query = "SELECT * FROM reserve WHERE customid = '$pieces[1]' AND businessid = '$businessid'";
        $result = Treat_DB_ProxyOld::query($query, true);
        $num = mysql_numrows($result);

        ////////////////////////////////
        /////insert/update events///////
        ////////////////////////////////
            $global_events[$pieces[1]] = $pieces[2];
            if($pieces[8] == "CANCELED" || $pieces[8] == "SET-UP/MAINTENANCE"){$status = 4; $global_cancel[$pieces[1]] = 1;}
            else{$status = 1; $global_status[$pieces[1]] = 1;}

            if($num == 0 && $status != 4 && $pieces[7] != "No Catering Needed" && $pieces[7] != "none" && $pieces[7] != "None"){
                ///insert event
                $query = "INSERT INTO reserve (accountid,businessid,companyid,date,start_hour,end_hour,comment,peoplenum,roomid,status,roomcomment,curmenu,customid)
                VALUES ('$accountid','$businessid','$companyid','$startdate','$starthour','$endhour','$pieces[2]','$pieces[9]','0','$status','EventPro: $pieces[7]','$curmenu','$pieces[1]')";
                $result = Treat_DB_ProxyOld::query($query);
                $reserveid = mysql_insert_id();
            }
            elseif($num != 0){
                $reserveid = mysql_result($result,0,"reserveid");
                ///update event
                $query = "UPDATE reserve SET businessid = $businessid, accountid = '$accountid', date = '$startdate', start_hour = '$starthour', end_hour = '$endhour', comment = '$pieces[2]', roomcomment = 'EventPro: $pieces[7]', peoplenum = '$pieces[9]' WHERE reserveid = '$reserveid'";
                $result = Treat_DB_ProxyOld::query($query);

                /*
                if($status == 4 && $global_status[$pieces[1]] != 1 && $reserveid > 0){
                    $query = "UPDATE reserve SET status = '$status' WHERE reserveid = '$reserveid'";
                    $result = Treat_DB_ProxyOld::query($query, true);
                }
                */
            }
			else{
				echo "Record Skipped<br>";
				continue;
			}
            $global_events_reserveid[$pieces[1]] = $reserveid;

            ////display query
            echo "Insert/Update Event...<font size=2>$query,$reserveid</font><br>";
            
            ///////////////////////////////
            //////update custom fields/////
            ///////////////////////////////

            /////department
            $query = "SELECT * FROM invoice_custom_values WHERE customid = $cust_deptid AND reserveid = '$reserveid'";
            $result = Treat_DB_ProxyOld::query($query, true);
            $num = mysql_numrows($result);

            if($num==0){
                $query = "INSERT INTO invoice_custom_values (reserveid,customid,value) VALUES ('$reserveid',$cust_deptid,'$pieces[16]')";
                $result = Treat_DB_ProxyOld::query($query);
            }
            else{
                $id = mysql_result($result,0,"id");
                $query = "UPDATE invoice_custom_values SET value = '$pieces[16]' WHERE id = '$id'";
                $result = Treat_DB_ProxyOld::query($query);
            }

            ////end date
            $query = "SELECT * FROM invoice_custom_values WHERE customid = '23' AND reserveid = '$reserveid'";
            $result = Treat_DB_ProxyOld::query($query, true);
            $num = mysql_numrows($result);

            if($num==0){
                $query = "INSERT INTO invoice_custom_values (reserveid,customid,value) VALUES ('$reserveid','23','$enddate')";
                $result = Treat_DB_ProxyOld::query($query);
            }
            else{
                $id = mysql_result($result,0,"id");
                $query = "UPDATE invoice_custom_values SET value = '$enddate' WHERE id = '$id'";
                $result = Treat_DB_ProxyOld::query($query);
            }

            ////business purpose
			if($businessid == 301){$cust_bpid = 82;}
			else{$cust_bpid = 24;}

            $pieces[24] = str_replace("'","`",$pieces[24]);

            $query = "SELECT * FROM invoice_custom_values WHERE customid = $cust_bpid AND reserveid = '$reserveid'";
            $result = Treat_DB_ProxyOld::query($query, true);
            $num = mysql_numrows($result);

            if($num==0){
                $query = "INSERT INTO invoice_custom_values (reserveid,customid,value) VALUES ('$reserveid',$cust_bpid,'$pieces[24]')";
                $result = Treat_DB_ProxyOld::query($query);
            }
            else{
                $id = mysql_result($result,0,"id");
                $query = "UPDATE invoice_custom_values SET value = '$pieces[24]' WHERE id = '$id'";
                $result = Treat_DB_ProxyOld::query($query);
            }

            //////////sub caterings
            //if($pieces[3] != ""){

                /////map fields
                $startdate=substr($pieces[10],0,10);
                $starthour=substr($pieces[10],11,5);
                $starthour=str_replace(":","",$starthour);

                $query = "SELECT * FROM reserve WHERE parent = '$reserveid' AND customid = '$pieces[0]'";
                $result = Treat_DB_ProxyOld::query($query, true);
                $num = mysql_numrows($result);

				////customer count of sub catering
				if($pieces[25]==0){$pieces[25]=$pieces[9];}

                if($num==0 && $pieces[3] != ""){
                    ///insert event
                    $query = "INSERT INTO reserve (parent,accountid,businessid,companyid,date,start_hour,end_hour,comment,peoplenum,roomid,status,roomcomment,curmenu,customid)
                    VALUES ('$reserveid','$accountid','$businessid','$companyid','$startdate','$starthour','$starthour','$pieces[5] [$pieces[1]]','$pieces[25]','$roomid','$status',' ','$curmenu','$pieces[0]')";
                    $result = Treat_DB_ProxyOld::query($query);
                    $subreserveid = mysql_insert_id();
                }
                elseif($num!=0){
                    $subreserveid = mysql_result($result,0,"reserveid");
                    ///update event
                    $query = "UPDATE reserve SET businessid = $businessid, accountid = '$accountid', date = '$startdate', start_hour = '$starthour', end_hour = '$starthour', comment = '$pieces[5] [$pieces[1]]', peoplenum = '$pieces[25]', roomid = '$roomid' WHERE reserveid = '$subreserveid'";
                    $result = Treat_DB_ProxyOld::query($query);

                    if($status == 4 && $subreserveid > 0){
                        $query = "UPDATE reserve SET status = '$status' WHERE reserveid = '$subreserveid'";
                        $result = Treat_DB_ProxyOld::query($query);
                    }
                }
				else{
					echo "&nbsp;&nbsp;&nbsp;&nbsp;Sub Record Skipped<br>";
					continue;
				}

                ///////display query
                echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Insert/Update Details...<font size=2>$query,$subreserveid</font><br>";

                /////department
                $query = "SELECT * FROM invoice_custom_values WHERE customid = $cust_deptid AND reserveid = '$subreserveid'";
                $result = Treat_DB_ProxyOld::query($query, true);
                $num = mysql_numrows($result);

                if($num==0){
                    $query = "INSERT INTO invoice_custom_values (reserveid,customid,value) VALUES ('$subreserveid',$cust_deptid,'$pieces[16]')";
                    $result = Treat_DB_ProxyOld::query($query);
                }
                else{
                    $id = mysql_result($result,0,"id");
                    $query = "UPDATE invoice_custom_values SET value = '$pieces[16]' WHERE id = '$id'";
                    $result = Treat_DB_ProxyOld::query($query);
                }

                ////business purpose
                $pieces[24] = str_replace("'","`",$pieces[24]);
                
                $query = "SELECT * FROM invoice_custom_values WHERE customid = $cust_bpid AND reserveid = '$subreserveid'";
                $result = Treat_DB_ProxyOld::query($query, true);
                $num = mysql_numrows($result);

                if($num==0){
                    $query = "INSERT INTO invoice_custom_values (reserveid,customid,value) VALUES ('$subreserveid',$cust_bpid,'$pieces[24]')";
                    $result = Treat_DB_ProxyOld::query($query);
                }
                else{
                    $id = mysql_result($result,0,"id");
                    $query = "UPDATE invoice_custom_values SET value = '$pieces[24]' WHERE id = '$id'";
                    $result = Treat_DB_ProxyOld::query($query);
                }

                ////end date
                /*
                $query = "SELECT * FROM invoice_custom_values WHERE customid = '23' AND reserveid = '$subreserveid'";
                $result = Treat_DB_ProxyOld::query($query, true);
                $num = mysql_numrows($result);

                if($num==0){
                    $query = "INSERT INTO invoice_custom_values (reserveid,customid,value) VALUES ('$subreserveid','23','$enddate')";
                    $result = Treat_DB_ProxyOld::query($query, true);
                }
                else{
                    $id = mysql_result($result,0,"id");
                    $query = "UPDATE invoice_custom_values SET value = '$enddate' WHERE id = '$id'";
                    $result = Treat_DB_ProxyOld::query($query, true);
                }
                */
            //}
		$rowcount++;
    }

    ///////canceled events
    echo "<font color=red>";
    foreach($global_events AS $key => $value){
        if($global_status[$key] == '' && $global_cancel[$key] ==1){
            echo "Canceled: $value [$global_events_reserveid[$key]]<br>";

            if($global_events_reserveid[$key] > 0){
                $query = "UPDATE reserve SET status = '4' WHERE reserveid = '$global_events_reserveid[$key]'";
                $result = Treat_DB_ProxyOld::query($query);
            }
        }
    }
    echo "</font>";
    $data = "Success";
}
else{
    echo "File does not exist.";
    $data = "Fail";
}

/*
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$headers .= "From: support@treatamerica.com\r\n";
$subject = "Fed Interface";
$email = "smartin@essentialpos.com.test-google-a.com";
mail($email, $subject, $data, $headers);
 */

echo "Finished";
?>