<?php

/////////////////////////////////////////////////
//////////////// Outlook Import//////////////////
/////////////////////////////////////////////////

define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');

if(isset($_POST['xmlStr']))
{
                $valid_xml = simplexml_load_string($_POST['xmlStr']);
                if($valid_xml === false)die();
                else
                {
                                foreach($valid_xml->children() as $child)
                                {
                                                $data[$child->getName()] = $child;
                                }

                                /////////////////////////////
                                //businessid and accountid///
                                /////////////////////////////
                                $email = $data["userEmail"];

                                $query = "SELECT accountid,businessid FROM customer WHERE username = '$email'";
                                $result = DB_Proxy::query($query);

                                $businessid = mysql_result($result,0,"businessid");
                                $accountid = mysql_result($result,0,"accountid");

                                ////debug
                                foreach($data AS $value => $key){
                                    $message.="$value: $key\r\n";
                                }

                                file_put_contents("test_data.txt",$message);
                }
                
                ///////////////////////////////
                ///////response to client//////
                ///////////////////////////////
                if($businessid < 1 OR $accountid < 1){
                    echo "error: You need to setup an account.";
                }
                elseif($data["globalid"] != "")
                {
                    
                    if($data["needCatering"] == "True"){
                        ///which company
                        $query = "SELECT companyid,default_menu FROM business WHERE businessid = $businessid";
                        $result = DB_Proxy::query($query);
                    
                        $companyid = mysql_result($result,0,"companyid");
                        $curmenu = mysql_result($result,0,"default_menu");

                        ///////////////////////////////
                        //////////Add Rooms////////////
                        ///////////////////////////////
                        $location = $data["location"];

                        $query = "SELECT * FROM rooms WHERE roomname = '$location' AND businessid = '$businessid'";
                        $result = DB_Proxy::query($query);
                        $num = mysql_numrows($result);

                        if($num==0){
                            ////insert room
                            $query = "INSERT INTO rooms (businessid,companyid,roomname) VALUES ('$businessid','$companyid','$location')";
                            $result = DB_Proxy::query($query);
                            $roomid = mysql_insert_id();
                        }
                        else{
                            $roomid = mysql_result($result,0,"roomid");
                        }

                        ///////////////////
                        ////event name/////
                        ///////////////////
                        $event_name = $data["subject"];
                        $event_name = str_replace("'","`",$event_name);

                        ///////////////////
                        ////event time/////
                        ///////////////////
                        $startdate = date("Y-m-d", (int)trim($data["start"]));
                        $starthour = date("Gi", (int)trim($data["start"]));
                        $endhour = date("Gi", (int)trim($data["end"]));

                        //////////////////////////////
                        ////insert update catering////
                        //////////////////////////////
                        $customid = $data["globalid"];

                        $query = "SELECT * FROM reserve WHERE customid = '$customid' AND businessid = '$businessid'";
                        $result = DB_Proxy::query($query);
                        $num = mysql_num_rows($result);

                        if($num==0 ){
                            ///insert event
                            $query = "INSERT INTO reserve (accountid,businessid,companyid,date,start_hour,end_hour,comment,peoplenum,roomid,status,roomcomment,curmenu,customid)
                            VALUES ('$accountid','$businessid','$companyid','$startdate','$starthour','$endhour','$event_name','1','$roomid','1',' ','$curmenu','$customid')";
                            $result = DB_Proxy::query($query);
                            $reserveid = mysql_insert_id();
                        }
                        else{
                            $reserveid = mysql_result($result,0,"reserveid");
                            ///update event
                            $query = "UPDATE reserve SET businessid = $businessid, accountid = '$accountid', date = '$startdate', start_hour = '$starthour', end_hour = '$endhour', comment = '$event_name', status = 1 WHERE reserveid = '$reserveid'";
                            $result = DB_Proxy::query($query);
                        }

                        ///////////////////////
                        ////custom fields//////
                        ///////////////////////
                        if(file_exists("custom_fields/$businessid.php")){
                            include("custom_fields/$businessid.php");
                        }
                    }
                    else{
                        ////cancel catering
                        $customid = $data["globalid"];

                        $query = "UPDATE reserve SET status = '4' WHERE businessid = $businessid AND customid = '$customid'";
                        $result = DB_Proxy::query($query);
                    }

                    echo "success";
                }
                else
                {
                    echo "error: Error saving catering details to server";                                                                                                                 
                }
}
?>