<?php
//require_once('bootstrap.php');
require_once('console_colors.php');

function insertItemDetails(PDO $db, $mim_id, Array $details, &$updates) {
	$nmap = Array (
		1 => 'calories_per_serving',
		2 => 'fat_calories_per_serving',
		3 => 'total_fat_per_serving',
		4 => 'sat_fat_per_serving',
		5 => 'cholesterol_per_serving',
		6 => 'sodium_per_serving',
		8 => 'carbo_per_serving',
		9 => 'fiber_per_serving',
		10=> 'sugar_per_serving',
		11=> 'protein_per_serving',
		20=> 'trans_fat_per_serving',
		21=> 'dvp_vitamin_a',
		22=> 'dvp_vitamin_c',
	);
	$compatable = array_intersect(
		array_keys($details),
		$nmap
	);
	$rnmap = array_flip($nmap);
	$query = "REPLACE INTO nutrition_xref_master(mim_id,nutrition_type_id,value)"
			. " VALUES ($mim_id, ?, ?)";
	//$query = "INSERT IGNORE INTO nutrition_xref_master(mim_id,nutrition_type_id,value)". " VALUES ($mim_id, ?, ?)";
	$sth = $db->prepare($query);
	foreach ($compatable as $c) {
		//echo print_r($c, TRUE) ."\t" . $rnmap[$c] . "\n";
		if ( is_numeric($details[$c]) || is_string($details[$c]) ) {
			//$parameters[] = Array($rnmap[$c],$details[$c]);
			$updates[] = Array(
				'mim_id' => $mim_id,
				$nmap[$rnmap[$c]] => $rnmap[$c],
				'value' => $details[$c]
			);
			$sth->execute(Array($rnmap[$c],$details[$c]));
		}
	}
	//print_r($parameters);	echo "$mim_id\n";
	//file_put_contents("Updatemaster.log.txt", date('r') . "\t\tupdated $mim_id\n", FILE_APPEND);
}

function insertItem(PDO $db, $upc, $name) {
	//$query = "INSERT INTO menu_items_master(UPC,Name) VALUES (?,?)" . " ON DUPLICATE KEY UPDATE Name=?";
	$query = "INSERT IGNORE INTO menu_items_master(UPC,Name) VALUES (?,?)";
	$sth = $db->prepare($query);
	//$sth->execute(Array($upc,$name,$name));
	$sth->execute(Array($upc,$name));
	//return $db->lastInsertId();
	$query = "SELECT id FROM menu_items_master WHERE UPC = ?";
	$sth = $db->prepare($query);
	$sth->execute(Array($upc));
	$result = $sth->fetch();
	$id = $result['id'];
	if ($id == '') {
		throw new Exception("ID should never be null. insert item upc=$upc, name=$name");
		exit(1);
	}
	return $id;
}

$udb = new PDO("mysql:host=192.168.1.50;dbname=mburris_businesstrack", "mburris_user", "bigdog") or die("Couldn't connect to database\n");
//$udb = new PDO("mysql:host=127.0.0.1;dbname=upc", "root", "root") or die("Couldn't connect to database\n");
$nutrition_not_found = 0;
$count = 0;
$upc_array = Array();
$dir = __DIR__ . "/simpleupc/";
$nutrition_updates = Array();
foreach (glob("$dir/product/*.xml") as $filename) {
	$product_xml = simplexml_load_file($filename);
	if ($product_xml->success != 'false') {
		$count++;
		$upc = (string)$product_xml->result->upc;
		if (isset($upc_array[$upc])) {
			continue;
		}
		$children = (array)$product_xml->result->children();

		// format value for menu_items_master.name
		if ((string)$product_xml->result->size != '' && (string)$product_xml->result->container != '') {
			$container = trim( trim((string)$product_xml->result->size) . ' ' .   trim((string)$product_xml->result->units) . ' ' . trim((string)$product_xml->result->container) );
		} else {
			$container = '';
		}
		$name = trim((string)$product_xml->result->brand . ' ' . (string)$product_xml->result->description . ' ' . $container);
		$bad_values = Array(
			'false ',
			'false',
			" - For Hi Res Download Tif Or EPS ",
			"\n",
			"\r",
			"\t",
		);
		$name = str_replace($bad_values, '', $name);
		$name = str_replace('  ', ' ', $name);
		$name = preg_replace("/ Modified (\d{1,2}\/\d{1,2}\/\d{1,2})/", "", $name);		
		if (strlen($name) > 100) {
			$name = substr($name,0,100);
		}
		$upc_array[$upc] = TRUE;
		//echo str_pad(strlen($name), 3, '0', STR_PAD_LEFT) . "\t$upc\t$name\n";

		$mid = insertItem($udb, $upc, $name);
		$nutrition_filename = $dir . "nutritionfacts/$upc.xml";
		if (!file_exists($nutrition_filename)) {
			echo redecho("Nutrition not found for $upc!\n");
			$nutrition_not_found++;
			continue;
		}
		$nutrition_xml = simplexml_load_file($nutrition_filename);
		if (strtolower($nutrition_xml->success) == 'false') {
			echo redecho("Nutrition not found for $upc!\n");
			$nutrition_not_found++;
			continue;
		}

		$children = (array)$nutrition_xml->result->children();
		foreach ($children as $key => $value) {
			echo "\t" . greenecho((string)$key) . "\t" . cyanecho((string)$value) . "\n";
		}
		insertItemDetails($udb, $mid, $children, $nutrition_updates);
	}
}
echo "Successful item lookups: $count (" . greenecho(count($upc_array)) . " unique items)\n";
echo "Nutrition not found for $nutrition_not_found items\n";
echo "Nutrition xref master updates: " . count($nutrition_updates) . "\n";
print_r($nutrition_updates);