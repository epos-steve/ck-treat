<?php

function blueecho($str) {
	return "\033[1;34m" . $str . "\033[0m";
}
function greenecho($str) {
	return "\033[1;32m" . $str . "\033[0m";
}
function redecho($str) {
	return "\033[1;31m" . $str . "\033[0m";
}
function yellowecho($str) {
	return "\033[1;33m" . $str . "\033[0m";
}
function cyanecho($str) {
	return "\033[0;36m" . $str . "\033[0m";
}
function bold($str) {
	return "\033[7m" . $str . "\033[0m";
}
