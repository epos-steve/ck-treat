<?php

//Financial Report Extras for all Companies

define('DOC_ROOT', realpath(dirname(__FILE__).'/../../../application'));
require_once(DOC_ROOT.'/bootstrap.php');

$date_given = date("Y-m-d", time() - 60 * 60 * 24);
if ($_GET['date_given']){
	$date_given = $_GET['date_given'];
	$query1 = "CALL fill_financial_report_extras_by_date('$date_given')";
	Treat_DB_ProxyOldProcessHost::query($query1);
} else {
	$date_given = date("Y-m-d", time() - 60 * 60 * 24);
	$query1 = "CALL fill_financial_report_extras()";
	Treat_DB_ProxyOldProcessHost::query($query1);
}
	
$sql = "SELECT 
									companyid
									, companyname
									, date_start
									, date_end
									, districtid
									, districtname
									, businessid
									, businessname
									, gross_sales
									, taxes
									, net_sales
									, cost
									, waste
									, waste_percent
									, shrink
									, shrink_percent
									, cost_waste
									, cost_waste_percent
									, cost_waste_shrink
									, cost_waste_shrink_percent
									, beginning_ck_card_balance
									, cash_received
									, credit_card_receipts
									, credit_card_receipts_percent
									, refunds
									, initial_promo
									, ending_ck_card_balance
									, payroll_deduct
									, net_balances
									, ck_card_sales
									, credit_card_sales
									, credit_card_sales_percent
									, bottle_deposits
									, cost_percent
									, unfunded_promotions
									, unfunded_promotion_percent
									, credit_card_fee
									, credit_card_fee_percent
									, ck_fee
									, ck_fee_percent
									, ck_monthly_maintenance_fee
									, operating_profit
									, operating_profit_percent
									, nac_fee
									, nac_fee_percent 
								FROM mburris_report.financial_report_extras_4_all 
								WHERE date_start = '$date_given'";

$result2 = Treat_DB_ProxyOldProcessHost::query($sql);
while ($r = Treat_DB_ProxyOldProcessHost::mysql_fetch_array($result2)){
	$companyid = $r['companyid'];
	$companyname = $r['companyname'];
	$date_start = $r['date_start'];
	$date_end = $r['date_end'];
	$districtid = $r['districtid'];
	$districtname = $r['districtname'];
	$businessid = $r['businessid'];
	$businessname = $r['businessname'];
	$gross_sales = $r['gross_sales'];
	$taxes = $r['taxes'];
	$net_sales = $r['net_sales'];
	$cost = $r['cost'];
	$waste = $r['waste'];
	$waste_percent = $r['waste_percent'];
	$shrink = $r['shrink'];
	$shrink_percent = $r['shrink_percent'];
	$cost_waste = $r['cost_waste'];
	$cost_waste_percent = $r['cost_waste_percent'];
	$cost_waste_shrink = $r['cost_waste_shrink'];
	$cost_waste_shrink_percent = $r['cost_waste_shrink_percent'];
	$beginning_ck_card_balance = $r['beginning_ck_card_balance'];
	$cash_received = $r['cash_received'];
	$credit_card_receipts = $r['credit_card_receipts'];
	$credit_card_receipts_percent = $r['credit_card_receipts_percent'];
	$refunds = $r['refunds'];
	$initial_promo = $r['initial_promo'];
	$ending_ck_card_balance = $r['ending_ck_card_balance'];
	$payroll_deduct = $r['payroll_deduct'];
	$net_balances = $r['net_balances'];
	$ck_card_sales = $r['ck_card_sales'];
	$credit_card_sales = $r['credit_card_sales'];
	$credit_card_sales_percent = $r['credit_card_sales_percent'];
	$bottle_deposits = $r['bottle_deposits'];
	$cost_percent = $r['cost_percent'];
	$unfunded_promotions = $r['unfunded_promotions'];
	$unfunded_promotion_percent = $r['unfunded_promotion_percent'];
	$credit_card_fee = $r['credit_card_fee'];
	$credit_card_fee_percent = $r['credit_card_fee_percent'];
	$ck_fee = $r['ck_fee'];
	$ck_fee_percent = $r['ck_fee_percent'];
	$ck_monthly_maintenance_fee = $r['ck_monthly_maintenance_fee'];
	$operating_profit = $r['operating_profit'];
	$operating_profit_percent = $r['operating_profit_percent'];
	$nac_fee = $r['nac_fee'];
	$nac_fee_percent = $r['nac_fee_percent'];
	
	$insert_financial_extras_all = "INSERT IGNORE INTO financial_extras_all(companyid, companyname, date_start, date_end, districtid, districtname, businessid, businessname, gross_sales, taxes, net_sales, cost, waste, waste_percent, shrink, shrink_percent, cost_waste, cost_waste_percent, cost_waste_shrink, cost_waste_shrink_percent, beginning_ck_card_balance, cash_received, credit_card_receipts, credit_card_receipts_percent, refunds, initial_promo, ending_ck_card_balance, payroll_deduct, net_balances, ck_card_sales, credit_card_sales, credit_card_sales_percent, bottle_deposits, cost_percent, unfunded_promotions, unfunded_promotion_percent, credit_card_fee, credit_card_fee_percent, ck_fee, ck_fee_percent, ck_monthly_maintenance_fee, operating_profit, operating_profit_percent, nac_fee, nac_fee_percent) VALUES ( '$companyid', '$companyname', '$date_start', '$date_end', '$districtid', '$districtname', '$businessid', '$businessname', '$gross_sales', '$taxes', '$net_sales', '$cost', '$waste', '$waste_percent', '$shrink', '$shrink_percent', '$cost_waste', '$cost_waste_percent', '$cost_waste_shrink', '$cost_waste_shrink_percent', '$beginning_ck_card_balance', '$cash_received', '$credit_card_receipts', '$credit_card_receipts_percent', '$refunds', '$initial_promo', '$ending_ck_card_balance', '$payroll_deduct', '$net_balances', '$ck_card_sales', '$credit_card_sales', '$credit_card_sales_percent', '$bottle_deposits', '$cost_percent', '$unfunded_promotions', '$unfunded_promotion_percent', '$credit_card_fee', '$credit_card_fee_percent', '$ck_fee', '$ck_fee_percent', '$ck_monthly_maintenance_fee', '$operating_profit', '$operating_profit_percent', '$nac_fee', '$nac_fee_percent' )";
	Treat_DB_ProxyOld::query($insert_financial_extras_all);
	
}

?>