<?php

function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+4);
        $diff=round($diff, 2);
        if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
        elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
        $diff = substr($diff, 0, $dot+3);
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      elseif ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      elseif ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '1')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='31')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;
$leap = date("L");

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   
   elseif ($leap==1&&$month=='03')
   {
      $day=$day+29;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      $day=$day-31;  
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";} 
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

define('DOC_ROOT', dirname(dirname(__FILE__)));
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');

$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");

/*$user=$_COOKIE["usercook"];
$pass=$_COOKIE["passcook"];
$view=$_COOKIE["viewcook"];
$sort_ap=$_COOKIE["sort_ap"];
$subtotal=$_COOKIE["subtotal"];
$date6=$_COOKIE["date6cook"];
$autosave=$_GET["autosave"];*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook');
$view = \EE\Controller\Base::getSessionCookieVariable('viewcook');
$sort_ap = \EE\Controller\Base::getSessionCookieVariable('sort_ap');
$subtotal = \EE\Controller\Base::getSessionCookieVariable('subtotal');
$date6 = \EE\Controller\Base::getSessionCookieVariable('date6cook');
$autosave = \EE\Controller\Base::getGetVariable('autosave');

//if($autosave==""){$autosave=$_COOKIE["autosave"];}
if($autosave==""){$autosave = \EE\Controller\Base::getSessionCookieVariable('autosave');}

/*$businessid=$_GET["bid"];
$companyid=$_GET["cid"];
$audit=$_GET["audit"];*/

$businessid = \EE\Controller\Base::getGetVariable('bid');
$companyid = \EE\Controller\Base::getGetVariable('cid');
$audit = \EE\Controller\Base::getGetVariable('audit');

if ($businessid==""||$companyid==""){
   /*$businessid=$_POST["businessid"];
   $companyid=$_POST["companyid"];
   $date6=$_POST["date6"];*/

	$businessid = \EE\Controller\Base::getPostVariable('businessid');
	$companyid = \EE\Controller\Base::getPostVariable('companyid');
	$date6 = \EE\Controller\Base::getPostVariable('date6');
}

if ($date6==""){$date6=$today;}
if ($autosave==""){$autosave=0;}

/*mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");*/
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=Treat_DB_ProxyOld::mysql_numrows($result);
//mysql_close();

$security_level=@Treat_DB_ProxyOld::mysql_result($result,0,"security_level");
$mysecurity=@Treat_DB_ProxyOld::mysql_result($result,0,"security");
$bid=@Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
$cid=@Treat_DB_ProxyOld::mysql_result($result,0,"companyid");
$bid2=@Treat_DB_ProxyOld::mysql_result($result,0,"busid2");
$bid3=@Treat_DB_ProxyOld::mysql_result($result,0,"busid3");
$bid4=@Treat_DB_ProxyOld::mysql_result($result,0,"busid4");
$bid5=@Treat_DB_ProxyOld::mysql_result($result,0,"busid5");
$bid6=@Treat_DB_ProxyOld::mysql_result($result,0,"busid6");
$bid7=@Treat_DB_ProxyOld::mysql_result($result,0,"busid7");
$bid8=@Treat_DB_ProxyOld::mysql_result($result,0,"busid8");
$bid9=@Treat_DB_ProxyOld::mysql_result($result,0,"busid9");
$bid10=@Treat_DB_ProxyOld::mysql_result($result,0,"busid10");
$loginid=@Treat_DB_ProxyOld::mysql_result($result,0,"loginid");

$pr1=@Treat_DB_ProxyOld::mysql_result($result,0,"payroll");
$pr2=@Treat_DB_ProxyOld::mysql_result($result,0,"pr2");
$pr3=@Treat_DB_ProxyOld::mysql_result($result,0,"pr3");
$pr4=@Treat_DB_ProxyOld::mysql_result($result,0,"pr4");
$pr5=@Treat_DB_ProxyOld::mysql_result($result,0,"pr5");
$pr6=@Treat_DB_ProxyOld::mysql_result($result,0,"pr6");
$pr7=@Treat_DB_ProxyOld::mysql_result($result,0,"pr7");
$pr8=@Treat_DB_ProxyOld::mysql_result($result,0,"pr8");
$pr9=@Treat_DB_ProxyOld::mysql_result($result,0,"pr9");
$pr10=@Treat_DB_ProxyOld::mysql_result($result,0,"pr10");

if ($num != 1 || ($security_level==1 && ($bid != $businessid || $cid != $companyid)) || $user == "" || $pass == "")
{
    echo "<center><h3>Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM company WHERE companyid = '$companyid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $companyname=@Treat_DB_ProxyOld::mysql_result($result,0,"companyname");
	$com_logo=@Treat_DB_ProxyOld::mysql_result($result,0,"logo");

	if($com_logo == ""){$com_logo = "talogo.gif";}

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM security WHERE securityid = '$mysecurity'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $sec_bus_sales=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_sales");
    $sec_bus_invoice=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_invoice");
    $sec_bus_order=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_order");
    $sec_bus_payable=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_payable");
    $sec_bus_inventor1=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor1");
    $sec_bus_control=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_control");
    $sec_bus_menu=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_menu");
    $sec_bus_order_setup=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_order_setup");
    $sec_bus_request=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_request");
    $sec_route_collection=@Treat_DB_ProxyOld::mysql_result($result,0,"route_collection");
    $sec_route_labor=@Treat_DB_ProxyOld::mysql_result($result,0,"route_labor");
    $sec_route_detail=@Treat_DB_ProxyOld::mysql_result($result,0,"route_detail");
	$sec_route_machine=@Treat_DB_ProxyOld::mysql_result($result,0,"route_machine");
    $sec_bus_labor=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_labor");
	$sec_bus_budget=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_budget");

    if($sec_route_collection<1&&$sec_route_labor<1){
       $location="businesstrack.php";
       header('Location: ./' . $location);
    }
    elseif($sec_route_collection<1&&$sec_route_labor>0){
       $location="busroute_labor.php?bid=$businessid&cid=$companyid";
       header('Location: ./' . $location);
    }

    $dayname=dayofweek($date6);
    setcookie("date6cook",$date6);
    setcookie("autosave",$autosave);

?>

<html>
<head>

<SCRIPT LANGUAGE="JavaScript" SRC="CalendarPopup.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">document.write(getCalendarStyles());</SCRIPT>

<STYLE>
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation
			{
			background-color:#6677DD;
			text-align:center;
			vertical-align:center;
			text-decoration:none;
			color:#FFFFFF;
			font-weight:bold;
			}
	.TESTcpDayColumnHeader,
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation,
	.TESTcpCurrentMonthDate,
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDate,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDate,
	.TESTcpCurrentDateDisabled,
	.TESTcpTodayText,
	.TESTcpTodayTextDisabled,
	.TESTcpText
			{
			font-family:arial;
			font-size:8pt;
			}
	TD.TESTcpDayColumnHeader
			{
			text-align:right;
			border:solid thin #6677DD;
			border-width:0 0 1 0;
			}
	.TESTcpCurrentMonthDate,
	.TESTcpOtherMonthDate,
	.TESTcpCurrentDate
			{
			text-align:right;
			text-decoration:none;
			}
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDateDisabled
			{
			color:#D0D0D0;
			text-align:right;
			text-decoration:line-through;
			}
	.TESTcpCurrentMonthDate
			{
			color:#6677DD;
			font-weight:bold;
			}
	.TESTcpCurrentDate
			{
			color: #FFFFFF;
			font-weight:bold;
			}
	.TESTcpOtherMonthDate
			{
			color:#808080;
			}
	TD.TESTcpCurrentDate
			{
			color:#FFFFFF;
			background-color: #6677DD;
			border-width:1;
			border:solid thin #000000;
			}
	TD.TESTcpCurrentDateDisabled
			{
			border-width:1;
			border:solid thin #FFAAAA;
			}
	TD.TESTcpTodayText,
	TD.TESTcpTodayTextDisabled
			{
			border:solid thin #6677DD;
			border-width:1 0 0 0;
			}
	A.TESTcpTodayText,
	SPAN.TESTcpTodayTextDisabled
			{
			height:20px;
			}
	A.TESTcpTodayText
			{
			color:#6677DD;
			font-weight:bold;
			}
	SPAN.TESTcpTodayTextDisabled
			{
			color:#D0D0D0;
			}
	.TESTcpBorder
			{
			border:solid thin #6677DD;
			}
</STYLE>

<SCRIPT LANGUAGE="JavaScript">
<!-- Web Site:  http://dynamicdrive.com -->

<!-- This script and many more are available free online at -->
<!-- The JavaScript Source!! http://javascript.internet.com -->

<!-- Begin
function disableForm(theform) {
if (document.all || document.getElementById) {
for (i = 0; i < theform.length; i++) {
var tempobj = theform.elements[i];
if (tempobj.type.toLowerCase() == "submit" || tempobj.type.toLowerCase() == "reset")
tempobj.disabled = true;
}

}
else {
alert("The form has been submitted.  But, since you're not using IE 4+ or NS 6, the submit button was not disabled on form submission.");
return false;
   }
}
//  End -->
</script>

<SCRIPT LANGUAGE=javascript><!--
function confirmcreate(){return confirm('ARE YOU SURE YOU WANT TO CREATE A TRANSFER?');}
// --></SCRIPT>

<SCRIPT LANGUAGE=javascript><!--
function confirmcreate2(){return confirm('ARE YOU SURE YOU WANT TO CREATE A PURCHASE CARD RECEIPT?');}
// --></SCRIPT>

<script type="text/javascript" language="javascript">
function checkKey(){

var key = event.keyCode;
var DaIndexId = event.srcElement.IndexId-0
if(key==38) {
document.getElementByIndex(DaIndexId-1).focus();
return false;
}
else if(key==40) {
document.getElementByIndex(DaIndexId+1).focus();
return false;
}
else if(key==13) {
document.getElementByIndex(DaIndexId+1).focus();
return false;
}
else if(key==37) {
document.getElementByIndex(DaIndexId-1000).focus();
return false;
}
else if(key==39) {
document.getElementByIndex(DaIndexId+1000).focus();
return false;
}
return true
} // end checkKey function
document.getElementByIndex=function(){
for(var i=0;i<document.all.length;i++)
if(document.all[i].IndexId==arguments[0])
return document.all[i]
return null
}
</script>

<script language="JavaScript" 
   type="text/JavaScript">
function changePage(newLoc)
 {
   nextPage = newLoc.options[newLoc.selectedIndex].value
		
   if (nextPage != "")
   {
      document.location.href = nextPage
   }
 }
</script>

</head>

<?

    echo "<body>";  
    echo "<div style=border:50px solid red;padding:10px;>";
    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%><tr><td colspan=2>";
    echo "<a href=businesstrack.php><img src=logo.jpg border=0 height=43 width=205></a><p></td></tr>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM business WHERE companyid = '$companyid' AND businessid = '$businessid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $businessname=@Treat_DB_ProxyOld::mysql_result($result,0,"businessname");
    $businessid=@Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
    $street=@Treat_DB_ProxyOld::mysql_result($result,0,"street");
    $city=@Treat_DB_ProxyOld::mysql_result($result,0,"city");
    $state=@Treat_DB_ProxyOld::mysql_result($result,0,"state");
    $zip=@Treat_DB_ProxyOld::mysql_result($result,0,"zip");
    $phone=@Treat_DB_ProxyOld::mysql_result($result,0,"phone");
    $districtid=@Treat_DB_ProxyOld::mysql_result($result,0,"districtid");
    $bus_unit=@Treat_DB_ProxyOld::mysql_result($result,0,"unit");

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM vend_settings WHERE businessid = '$businessid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $nickelx=@Treat_DB_ProxyOld::mysql_result($result,0,"nickelx");
    $dimex=@Treat_DB_ProxyOld::mysql_result($result,0,"dimex");
    $quarterx=@Treat_DB_ProxyOld::mysql_result($result,0,"quarterx");
    $dollarx=@Treat_DB_ProxyOld::mysql_result($result,0,"dollarx");
    $d_coinx=@Treat_DB_ProxyOld::mysql_result($result,0,"d_coinx");

    //echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";
    echo "<tr bgcolor=#CCCCFF><td width=1%><img src=logo/$com_logo></td><td><font size=4><b>$businessname #$bus_unit</b></font><br>$street<br>$city, $state $zip<br>$phone</td>";
    echo "<td align=right valign=top><br>";
    echo "<FORM ACTION='editbus.php' method=post name='store'>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<input type=hidden name=businessid value=$businessid>";
    echo "<input type=hidden name=companyid value=$companyid>";
    echo "<INPUT TYPE=submit VALUE=' Store Setup '> ";
    echo "</form></td></tr>";
    //echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";
 
    echo "<tr><td colspan=3><font color=#999999>";
    if ($sec_bus_sales>0){echo "<a href=busdetail.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Sales</font></a>";}
    if ($sec_bus_invoice>0){echo " | <a href=businvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Invoicing</font></a>";}
    if ($sec_bus_order>0){echo " | <a href=buscatertrack.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Orders</font></a>";}
    if ($sec_bus_payable>0){echo " | <a href=busapinvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Payables</font></a>";}
    if ($sec_bus_inventor1>0){echo " | <a href=businventor.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Inventory</font></a>";}
    if ($sec_bus_control>0){echo " | <a href=buscontrol.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Control Sheet</font></a>";}
    if ($sec_bus_menu>0){echo " | <a href=buscatermenu.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Menus</font></a>";}
    if ($sec_bus_order_setup>0){echo " | <a href=buscaterroom.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Order Setup</font></a>";}
    if ($sec_bus_request>0){echo " | <a href=busrequest.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Requests</font></a>";}
    if ($sec_route_collection>0||$sec_route_labor>0||$sec_route_detail>0){echo " | <a href=busroute_collect.php?bid=$businessid&cid=$companyid><font color=red onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='red' style='text-decoration:none'>Route Collections</font></a>";}
    if ($sec_bus_labor>0){echo " | <a href=buslabor.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Labor</font></a>";}
    if ($sec_bus_budget>0||($businessid == $bid && $pr1 == 1)||($businessid == $bid2 && $pr2 == 1)||($businessid == $bid3 && $pr3 == 1)||($businessid == $bid4 && $pr4 == 1)||($businessid == $bid5 && $pr5 == 1)||($businessid == $bid6 && $pr6 == 1)||($businessid == $bid7 && $pr7 == 1)||($businessid == $bid8 && $pr8 == 1)||($businessid == $bid9 && $pr9 == 1)||($businessid == $bid10 && $pr10 == 1)){echo " | <a href=busbudget.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Budget</font></a>";}
	echo "</td></tr>";   

    echo "</table></center><p>";

if ($security_level>0){
    echo "<p><center><table width=90%><tr><td width=50%><form action=busrequest.php method=post>";
    echo "<select name=gobus onChange='changePage(this.form.gobus)'>";

    $districtquery="";
    if ($security_level>2&&$security_level<7){
       //////mysql_connect($dbhost,$username,$password);
       //////@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM login_district WHERE loginid = '$loginid'";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //////mysql_close();

       $num--;
       while($num>=0){
          $districtid=@Treat_DB_ProxyOld::mysql_result($result,$num,"districtid");
          if($districtquery==""){$districtquery="districtid = '$districtid'";}
          else{$districtquery="$districtquery OR districtid = '$districtid'";}
          $num--;
       }
    }

    //////mysql_connect($dbhost,$username,$password);
    //////@mysql_select_db($database) or die( "Unable to select database");
    if($security_level>6){$query = "SELECT * FROM business WHERE companyid = '$companyid' ORDER BY businessname DESC";}
    elseif($security_level==2||$security_level==1){$query = "SELECT * FROM business WHERE companyid = '$companyid' AND (businessid = '$bid' OR businessid = '$bid2' OR businessid = '$bid3' OR businessid = '$bid4' OR businessid = '$bid5' OR businessid = '$bid6' OR businessid = '$bid7' OR businessid = '$bid8' OR businessid = '$bid9' OR businessid = '$bid10') ORDER BY businessname DESC";}
    elseif($security_level>2&&$security_level<7){$query = "SELECT * FROM business WHERE companyid = '$companyid' AND ($districtquery) ORDER BY businessname DESC";}
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //////mysql_close();

    $num--;
    while($num>=0){
       $businessname=@Treat_DB_ProxyOld::mysql_result($result,$num,"businessname");
       $busid=@Treat_DB_ProxyOld::mysql_result($result,$num,"businessid");

       if ($busid==$businessid){$show="SELECTED";}
       else{$show="";}

       echo "<option value=busroute_balance.php?bid=$busid&cid=$companyid $show>$businessname</option>";
       $num--;
    }
 
    echo "</select></td><td></form></td><td width=50% align=right></td></tr></table></center><p>";
}

    $security_level=$sec_route_collection;

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM vend_collection WHERE businessid = '$businessid' AND date = '$date6'";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    $b_bag1=@Treat_DB_ProxyOld::mysql_result($result,0,"b_bag1");
    $b_bag2=@Treat_DB_ProxyOld::mysql_result($result,0,"b_bag2");
    $b_escrow_qty=@Treat_DB_ProxyOld::mysql_result($result,0,"b_escrow_qty");
    $b_escrow_amt=@Treat_DB_ProxyOld::mysql_result($result,0,"b_escrow_amt");
    $b_nickel_qty=@Treat_DB_ProxyOld::mysql_result($result,0,"b_nickel_qty");
    $b_dime_qty=@Treat_DB_ProxyOld::mysql_result($result,0,"b_dime_qty");
    $b_quarter_qty=@Treat_DB_ProxyOld::mysql_result($result,0,"b_quarter_qty");
    $b_dollar_qty=@Treat_DB_ProxyOld::mysql_result($result,0,"b_dollar_qty");
    $b_bill_qty=@Treat_DB_ProxyOld::mysql_result($result,0,"b_bill_qty");
    $b_changers_made=@Treat_DB_ProxyOld::mysql_result($result,0,"b_changers_made");
    $b_bag3=@Treat_DB_ProxyOld::mysql_result($result,0,"b_bag3");
    $b_bag4=@Treat_DB_ProxyOld::mysql_result($result,0,"b_bag4");
    $b_escrow_qty2=@Treat_DB_ProxyOld::mysql_result($result,0,"b_escrow_qty2");
    $b_escrow_amt2=@Treat_DB_ProxyOld::mysql_result($result,0,"b_escrow_amt2");
    $b_nickel_qty2=@Treat_DB_ProxyOld::mysql_result($result,0,"b_nickel_qty2");
    $b_dime_qty2=@Treat_DB_ProxyOld::mysql_result($result,0,"b_dime_qty2");
    $b_quarter_qty2=@Treat_DB_ProxyOld::mysql_result($result,0,"b_quarter_qty2");
    $b_dollar_qty2=@Treat_DB_ProxyOld::mysql_result($result,0,"b_dollar_qty2");
    $b_bill_qty2=@Treat_DB_ProxyOld::mysql_result($result,0,"b_bill_qty2");
    $posted=@Treat_DB_ProxyOld::mysql_result($result,0,"posted");

    if ($posted==1){$showpost="<font color=red><b>SUBMITTED</b></font>";}
    else{$showpost="";}

    echo "<center><table width=90% cellspacing=0 cellpadding=0>";
    echo "<tr><td colspan=6>";
    echo "<table cellspacing=0 cellpadding=0 width=100%>";

    echo "<tr bgcolor=#CCCCCC valign=top><td width=1%><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=13%><center><a href=busroute_collect.php?cid=$companyid&bid=$businessid style=$style><font color=blue>Collection Sheet</a></center></td><td width=1% align=right><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td bgcolor=#E8E7E7 width=1% ><img src=leftcrn.jpg height=6 width=6 align=top></td><td bgcolor=#E8E7E7 width=13%><center><a href=busroute_balance.php?cid=$companyid&bid=$businessid style=$style style=$style><font color=black>Balance Sheet</a></center></td><td bgcolor=#E8E7E7 width=1% align=right><img src=rightcrn.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1%><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=13%><center><a href=busroute_report.php?cid=$companyid&bid=$businessid style=$style><font color=blue>Reporting</a></center></td><td width=1% align=right><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1%><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=13%><center><a href=busroute_labor.php?cid=$companyid&bid=$businessid style=$style><font color=blue>Payroll/Route Linking</a></center></td><td width=1% align=right><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1%><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=13%><center><a href=busroute_detail.php?cid=$companyid&bid=$businessid style=$style><font color=blue>Route Details</a></center></td><td width=1% align=right><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1%><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=13%><center><a href=busroute_machine.php?cid=$companyid&bid=$businessid style=$style><font color=blue>Machine Mgmt</a></center></td><td width=1% align=right><img src=rightcrn2.jpg height=6 width=6 align=top></td><td bgcolor=white width=25%></td></tr>";

    echo "<tr height=2><td colspan=4></td><td colspan=3 bgcolor=#E8E7E7></td><td></td><td colspan=4></td><td colspan=4></td><td colspan=4></td><td colspan=4></td><td colspan=1></td></tr>";

    echo "</table></td></tr>";

    echo "<tr bgcolor=#E8E7E7><td colspan=5><form action=busroute_balance.php method=post onSubmit='return disableForm(this);'><input type=hidden name=businessid value=$businessid><input type=hidden name=companyid value=$companyid><b><font style=background-color:yellow;>$dayname Balance Sheet</font></b> <SCRIPT LANGUAGE='JavaScript' ID='js18'> var cal18 = new CalendarPopup('testdiv1');cal18.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date6 VALUE='$date6' SIZE=8> <A HREF=# onClick=cal18.select(document.forms[2].date6,'anchor18','yyyy-MM-dd'); return false; TITLE=cal18.select(document.forms[2].date6,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor18' ID='anchor18'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A> <input type=submit value='GO'> $showpost</td><td align=right></td></tr>";
    echo "<tr bgcolor=black height=1><td colspan=6></form><form action=savebalance.php method=post onSubmit='return disableForm(this);' name=form><input type=hidden name=businessid value=$businessid><input type=hidden name=companyid value=$companyid><input type=hidden name=date6 value='$date6'></td></tr>";
    
    echo "<tr bgcolor=#CCCCFF><td colspan=6><form action=savebalance.php method=post><input type=hidden name=businessid value=$businessid><input type=hidden name=companyid value=$companyid><input type=hidden name=date6 value=$date6><b><i>Prior to Making Changers</b></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td colspan=6></td></tr>";
    echo "<tr><td align=right></td><td align=right><b>Notes</b></td><td align=right><b>Quantity</b></td><td align=right><b>Amount</b></td><td colspan=2></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td colspan=6></td></tr>";
    echo "<tr><td align=right>Coin Counter #1:</td><td align=right></td><td></td><td align=right><input type=text name=b_bag1 value='$b_bag1' size=8 onkeydown='return checkKey()' IndexId=1001></td><td colspan=2></td></tr>"; 
    echo "<tr bgcolor=#E8E7E7><td colspan=6></td></tr>";   
    echo "<tr><td align=right>Coin Counter #2:</td><td align=right></td><td align=right></td><td align=right><input type=text name=b_bag2 value='$b_bag2' size=8 onkeydown='return checkKey()' IndexId=1002></td><td colspan=2></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td colspan=6></td></tr>";
    echo "<tr><td align=right>Route Escrow:</td><td></td><td align=right><input type=text name=b_escrow_qty value='$b_escrow_qty' size=8 onkeydown='return checkKey()' IndexId=3></td><td align=right><input type=text name=b_escrow_amt value='$b_escrow_amt' size=8 onkeydown='return checkKey()' IndexId=1003></td><td colspan=2></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td colspan=6></td></tr>";

    $nickel_total=money($b_nickel_qty*$nickelx);
    $nickel_totalshow=number_format($nickel_total,2);
    echo "<tr><td align=right>Nickels:</td><td align=right>$$nickelx x </td><td align=right><input type=text name=b_nickel_qty value='$b_nickel_qty' size=8 onkeydown='return checkKey()' IndexId=4></td><td align=right>$$nickel_totalshow</td><td colspan=2></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td colspan=6></td></tr>";

    $dime_total=money($b_dime_qty*$dimex);
    $dime_totalshow=number_format($dime_total,2);
    echo "<tr><td align=right>Dimes:</td><td align=right>$$dimex x </td><td align=right><input type=text name=b_dime_qty value='$b_dime_qty' size=8 onkeydown='return checkKey()' IndexId=5></td><td align=right>$$dime_totalshow</td><td colspan=2></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td colspan=6></td></tr>";

    $quarter_total=money($b_quarter_qty*$quarterx);
    $quarter_totalshow=number_format($quarter_total,2);
    echo "<tr><td align=right>Quarters:</td><td align=right>$$quarterx x </td><td align=right><input type=text name=b_quarter_qty value='$b_quarter_qty' size=8 onkeydown='return checkKey()' IndexId=6></td><td align=right>$$quarter_totalshow</td><td colspan=2></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td colspan=6></td></tr>";
    $dollar_total=money($b_dollar_qty*$d_coinx);
    $dollar_totalshow=number_format($dollar_total,2);
    echo "<tr><td align=right>Dollar Coins:</td><td align=right>$$d_coinx x </td><td align=right><input type=text name=b_dollar_qty value='$b_dollar_qty' size=8 onkeydown='return checkKey()' IndexId=7></td><td align=right>$$dollar_totalshow</td><td colspan=2></td></tr>";
    $bill_total=money($b_bill_qty*$dollarx);
    $bill_totalshow=number_format($bill_total,2);
    echo "<tr><td align=right>Dollar Bills:</td><td align=right>$$dollarx x </td><td align=right><input type=text name=b_bill_qty value='$b_bill_qty' size=8 onkeydown='return checkKey()' IndexId=8></td><td align=right>$$bill_totalshow</td><td colspan=2></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td colspan=6></td></tr>";
    $subtotal=money($nickel_total+$dime_total+$quarter_total+$dollar_total+$bill_total+$b_escrow_amt+$b_bag2+$b_bag1);
    $subtotalshow=number_format($subtotal,2);
    echo "<tr bgcolor=#E8E7E7><td align=right colspan=3><b>Actual Balance Before Changers:</b></td><td align=right><b>$$subtotalshow</b></td><td colspan=2></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td colspan=6></td></tr>";
    echo "<tr><td align=right>Total of Changers Made:</td><td align=right></td><td align=right></td><td align=right><input type=text name=b_changers_made value='$b_changers_made' size=8 onkeydown='return checkKey()' IndexId=1008></td><td colspan=2></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td colspan=6></td></tr>";
    $subtotal=money($subtotal-$b_changers_made);
    $subtotalshow=number_format($subtotal,2);
    echo "<tr bgcolor=#E8E7E7><td align=right colspan=3><b>+/-:</b></td><td align=right><b>$$subtotalshow</b></td><td colspan=2></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td colspan=6></td></tr>";

    echo "<tr bgcolor=#CCCCFF><td colspan=6><b><i>After Running Changers</b></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td colspan=6></td></tr>";
    echo "<tr><td align=right>Coin Counter #1:</td><td align=right></td><td></td><td align=right><input type=text name=b_bag3 value='$b_bag3' size=8 onkeydown='return checkKey()' IndexId=1009></td><td colspan=2></td></tr>"; 
    echo "<tr bgcolor=#E8E7E7><td colspan=6></td></tr>";   
    echo "<tr><td align=right>Coin Counter #2:</td><td align=right></td><td align=right></td><td align=right><input type=text name=b_bag4 value='$b_bag4' size=8 onkeydown='return checkKey()' IndexId=1010></td><td colspan=2></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td colspan=6></td></tr>";
    echo "<tr><td align=right>Route Escrow:</td><td></td><td align=right><input type=text name=b_escrow_qty2 value='$b_escrow_qty2' size=8 onkeydown='return checkKey()' IndexId=9></td><td align=right><input type=text name=b_escrow_amt2 value='$b_escrow_amt2' size=8 onkeydown='return checkKey()' IndexId=1011></td><td colspan=2></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td colspan=6></td></tr>";

    $nickel_total=money($b_nickel_qty2*$nickelx);
    $nickel_totalshow=number_format($nickel_total,2);
    echo "<tr><td align=right>Nickels:</td><td align=right>$$nickelx x </td><td align=right><input type=text name=b_nickel_qty2 value='$b_nickel_qty2' size=8 onkeydown='return checkKey()' IndexId=10></td><td align=right>$$nickel_totalshow</td><td colspan=2></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td colspan=6></td></tr>";

    $dime_total=money($b_dime_qty2*$dimex);
    $dime_totalshow=number_format($dime_total,2);
    echo "<tr><td align=right>Dimes:</td><td align=right>$$dimex x </td><td align=right><input type=text name=b_dime_qty2 value='$b_dime_qty2' size=8 onkeydown='return checkKey()' IndexId=11></td><td align=right>$$dime_totalshow</td><td colspan=2></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td colspan=6></td></tr>";

    $quarter_total=money($b_quarter_qty2*$quarterx);
    $quarter_totalshow=number_format($quarter_total,2);
    echo "<tr><td align=right>Quarters:</td><td align=right>$$quarterx x </td><td align=right><input type=text name=b_quarter_qty2 value='$b_quarter_qty2' size=8 onkeydown='return checkKey()' IndexId=12></td><td align=right>$$quarter_totalshow</td><td colspan=2></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td colspan=6></td></tr>";
    $dollar_total=money($b_dollar_qty2*$d_coinx);
    $dollar_totalshow=number_format($dollar_total,2);
    echo "<tr><td align=right>Dollar Coins:</td><td align=right>$$d_coinx x </td><td align=right><input type=text name=b_dollar_qty2 value='$b_dollar_qty2' size=8 onkeydown='return checkKey()' IndexId=13></td><td align=right>$$dollar_totalshow</td><td colspan=2></td></tr>";
    $bill_total=money($b_bill_qty2*$dollarx);
    $bill_totalshow=number_format($bill_total,2);
    echo "<tr><td align=right>Dollar Bills:</td><td align=right>$$dollarx x </td><td align=right><input type=text name=b_bill_qty2 value='$b_bill_qty2' size=8 onkeydown='return checkKey()' IndexId=14></td><td align=right>$$bill_totalshow</td><td colspan=2></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td colspan=6></td></tr>";
    $subtotal2=money($nickel_total+$dime_total+$quarter_total+$dollar_total+$bill_total+$b_escrow_amt2+$b_bag3+$b_bag4);
    $subtotal2show=number_format($subtotal2,2);
    echo "<tr bgcolor=#E8E7E7><td align=right colspan=3><b>Actual Balance:</b></td><td align=right><b>$$subtotal2show</b></td><td colspan=2></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td colspan=6></td></tr>";
    $subtotal2=money($subtotal2-$subtotal);
    $subtotal2show=number_format($subtotal2,2);
    echo "<tr bgcolor=#E8E7E7><td align=right colspan=3><b>+/-:</b></td><td align=right><b>$$subtotal2show</b></td><td colspan=2></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td colspan=6></td></tr>";
    if ($posted==0||$security_level==9){echo "<tr bgcolor=#CCCCFF><td colspan=3><input type=submit value='Save'></td><td colspan=3></form></td></tr>";}
    else{echo "<tr bgcolor=#CCCCFF><td colspan=3></td><td colspan=3></form></td></tr>";}

    if ($audit==1){
       echo "<tr><td colspan=6><font size=2><b><u><a name=audit>Audit Trail</a></b></td></tr>";

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query3 = "SELECT * FROM audit_collection WHERE businessid = '$businessid' AND date = '$date6' ORDER BY edittime ASC";
       $result3 = Treat_DB_ProxyOld::query($query3);
       $num3=Treat_DB_ProxyOld::mysql_numrows($result3);
       //mysql_close();

       $num3--;
       while($num3>=0){
          $audituser=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"user");
          $edittime=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"edittime");
          $type=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"type");

          if ($type==0){echo "<tr><td colspan=6><font size=2>Collection Sheet Saved $edittime by $audituser</font></td></tr>";}
          elseif ($type==1){echo "<tr><td colspan=6><font size=2>Balance Sheet Saved $edittime by $audituser</font></td></tr>";}
          elseif ($type==3){echo "<tr><td colspan=6><font size=2 color=red>Submitted $edittime by $audituser</font></td></tr>";}
          $num3--;
       }
    } 
    else{echo "<tr><td colspan=6><a href=busroute_balance.php?cid=$companyid&bid=$businessid&audit=1#audit style=$style><font size=2 color=blue><b>Audit Trail</b></font></a></td></tr>";}

    echo "</table></center><DIV ID=testdiv1 STYLE=position:absolute;visibility:hidden;background-color:white;layer-background-color:white;></DIV>";

    echo "<br><FORM ACTION=businesstrack.php method=post>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<center><INPUT TYPE=submit VALUE=' Return to Main Page'></FORM></center></body>";
	
	google_page_track();
}
//mysql_close();
?>