<?php

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+3);
        if ($diff > 0 && $diff < '0.01'){$diff="0.01";}
        elseif($diff < 0 && $diff > '-0.01'){$diff="-0.01";}
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      if ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='32')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='08' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      $day=$day-31;  
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";} 
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

define('DOC_ROOT', dirname(dirname(__FILE__)));
require_once(DOC_ROOT.'/bootstrap.php');

$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");
$bbdayname=date("l");
$bbmonth=date("F");
$bbday=date("j");

   $user=$_COOKIE["usercook"];
   $pass=$_COOKIE["passcook"];
   $businessid=$_GET["bid"];
   $companyid=$_GET["cid"];
   $curmenu=$_GET["curmenu"];

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=mysql_numrows($result);
//mysql_close();

if ($num != 1 || $user == "" || $pass == "")
{
    echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}

else
{

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM company WHERE companyid = '$companyid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $companyname=mysql_result($result,0,"companyname");

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM business WHERE businessid = '$businessid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $taxrate=mysql_result($result,0,"tax");
    $default_menu=mysql_result($result,0,"default_menu");
    $businessname=mysql_result($result,0,"businessname");

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM menu_type WHERE menu_typeid = '$curmenu'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $contract=mysql_result($result,0,"type");
    $menu_name=mysql_result($result,0,"menu_typename");

    echo "<body onload='window.print();'>";
 
    echo "<center><table width=100%><tr><td><font size=4 face=arial><b><i>$menu_name :: $companyname</i></b></font></td><td align=right><font size=2 face=arial>$today ($businessname)</font></td></tr><tr><td colspan=2>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM menu_groups WHERE deleted = '0' AND menu_typeid = '$curmenu' ORDER BY groupname DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=mysql_numrows($result);
    //mysql_close();

    $num--;
    while ($num>=0){
       $groupid=mysql_result($result,$num,"menu_group_id");
       $groupname=mysql_result($result,$num,"groupname");
       $description=mysql_result($result,$num,"description");
       $image=mysql_result($result,$num,"image");
       $img_height=mysql_result($result,$num,"img_height");
       $img_width=mysql_result($result,$num,"img_width");

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM menu_items WHERE businessid = '$businessid' AND groupid = '$groupid' AND deleted = '0' ORDER BY item_name DESC";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=mysql_numrows($result2);
       //mysql_close();

       if ($num2>0){echo "<table width=100% border=1 cellspacing=0 cellpadding=0><tr bgcolor=#E8E7E7><td colspan=2><font size=3 face=arial><b><i>$groupname</i></b></font></td></tr>";}

       $num2--;
       $counter=1;
       while($num2>=0){
          $item_name=mysql_result($result2,$num2,"item_name");
          $price=mysql_result($result2,$num2,"price");
          $description=mysql_result($result2,$num2,"description");
          $order_min=mysql_result($result2,$num2,"order_min");
          $units=mysql_result($result2,$num2,"unit");
          $price=money($price);

          if ($order_min>1){$showmin="<font size=1>(minimum $order_min)</font>";}
          else{$showmin="";}

          if ($counter==1){echo "<tr valign=top><td width=50%><font size=2 face=arial><b>$item_name</b> $price/$units $showmin<br><font size=1>$description</td>";}
          elseif ($counter==2){echo "<td width=50%><font size=2 face=arial><b>$item_name</b> $price/$units $showmin<br><font size=1>$description</td></tr>";}

          $num2--;
          if ($counter==1){$counter=2;}
          else{$counter=1;}
       }
       echo "</table><p>";
       $num--;
    }

    echo "</td></tr></table></center></body>";
    //mysql_close();
	google_page_track();
}
?>