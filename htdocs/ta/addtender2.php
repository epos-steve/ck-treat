<?php

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      elseif ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      elseif ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='32')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+3);
        if ($diff > 0 && $diff < '0.01'){$diff="0.01";}
        elseif($diff < 0 && $diff > '-0.01'){$diff="-0.01";}
        return $diff;
}

//include("db.php");
define('DOC_ROOT', dirname(dirname(__FILE__)));
require_once(DOC_ROOT.'/bootstrap.php');
$style = "text-decoration:none";

$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";

/*$user=$_COOKIE["usercook"];
$pass=$_COOKIE["passcook"];
$companyid=$_POST['companyid'];
$invoiceid=$_POST['invoiceid'];
$amount=$_POST['amount'];
$direct=$_POST['direct'];
$tender_date=$_POST['tender_date'];
$checknum=$_POST['checknum'];*/

$user = \EE\Controller\Base::getPostGetSessionOrCookieVariable('usercook');
$pass = \EE\Controller\Base::getPostGetSessionOrCookieVariable('passcook');
$companyid = \EE\Controller\Base::getPostVariable('companyid');
$invoiceid = \EE\Controller\Base::getPostVariable('invoiceid');
$amount = \EE\Controller\Base::getPostVariable('amount');
$direct = \EE\Controller\Base::getPostVariable('direct');
$tender_date = \EE\Controller\Base::getPostVariable('tender_date');
$checknum = \EE\Controller\Base::getPostVariable('checknum');

/*mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");*/

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=Treat_DB_ProxyOld::mysql_numrows($result);

$location="invoice2.php?cid=$companyid&tender_date=$tender_date";

if ($num != 1 || $amount == "" || $invoiceid=="" || $checknum == "")
{
    echo "<center><h3>Failed</h3></center>";
}
else
{
    $invoices=explode(",",$invoiceid);

    //////LOOP
    $counter=0;
    foreach($invoices AS $invoiceid){

    $query = "SELECT * FROM invoice WHERE invoiceid = '$invoiceid'";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);

    if($num==1){
       $invtotal=Treat_DB_ProxyOld::mysql_result($result,0,"total");
       $businessid=Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
       $invdate=Treat_DB_ProxyOld::mysql_result($result,0,"date");

       if($invdate==$tender_date){$sameday=0;}
       else{$sameday=1;}

       $query = "SELECT SUM(amount) AS balance FROM invtender WHERE invoiceid = '$invoiceid'";
       $result = Treat_DB_ProxyOld::query($query);

       $balance=Treat_DB_ProxyOld::mysql_result($result,0,"balance");

       if(($amount+$balance)==$invtotal){
          $query = "INSERT INTO invtender (invoiceid,amount,tendertype,date,businessid,sameday,checknum) VALUES ('$invoiceid','$amount','-1','$tender_date','$businessid','$sameday','$checknum')";
          $result = Treat_DB_ProxyOld::query($query);

          $query = "UPDATE invoice SET status = '2' WHERE invoiceid = '$invoiceid'";
          $result = Treat_DB_ProxyOld::query($query);

          if($counter==0){$location="invoice2.php?cid=$companyid&invoiceid=$invoiceid&error=4&tender_date=$tender_date";}
          else{$location="invoice2.php?cid=$companyid&invoiceid=$invoiceid&error=5&tender_date=$tender_date";}
       }
       elseif(($amount+$balance)<$invtotal){
          $query = "INSERT INTO invtender (invoiceid,amount,tendertype,date,businessid,sameday,checknum) VALUES ('$invoiceid','$amount','-1','$tender_date','$businessid','$sameday','$checknum')";
          $result = Treat_DB_ProxyOld::query($query);

          if($counter==0){$location="invoice2.php?cid=$companyid&invoiceid=$invoiceid&error=2&tender_date=$tender_date";}
          else{$location="invoice2.php?cid=$companyid&invoiceid=$invoiceid&error=5&tender_date=$tender_date";}
       }
       elseif(($amount+$balance)>$invtotal){
          $query = "INSERT INTO invtender (invoiceid,amount,tendertype,date,businessid,sameday,checknum) VALUES ('$invoiceid','$amount','-1','$tender_date','$businessid','$sameday','$checknum')";
          $result = Treat_DB_ProxyOld::query($query);

          $query = "UPDATE invoice SET status = '2' WHERE invoiceid = '$invoiceid'";
          $result = Treat_DB_ProxyOld::query($query);

          if($counter==0){$location="invoice2.php?cid=$companyid&invoiceid=$invoiceid&error=3&tender_date=$tender_date";}
          else{$location="invoice2.php?cid=$companyid&invoiceid=$invoiceid&error=5&tender_date=$tender_date";}
       }
    }
    else{
       $location="invoice2.php?cid=$companyid&invoiceid=$invoiceid&error=1&tender_date=$tender_date";
    }
    
    $counter++;
    }
    ///////////END LOOP
}
//mysql_close();

header('Location: ./' . $location);
?>