<?php

//Added 08-07-09 [TF]: Removing link for Pete
function filterBadLink($str){
	return preg_replace('/<a ([^>]*)><font\s?([^>]*)>(#?[\d]*)(<\/font>)?<\/a>/', '$3', $str);
}

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+4);
        if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
        elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
        $diff = substr($diff, 0, $dot+3);
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      elseif ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      elseif ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='32')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      $day=$day-31;  
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";} 
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

//include("db.php");
define('DOC_ROOT', dirname(dirname(__FILE__)));
require_once(DOC_ROOT.'/bootstrap.php');
$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";


/*$user=$_COOKIE["usercook"];
$pass=$_COOKIE["passcook"];
$businessid=$_GET["bid"];
$companyid=$_GET["cid"];*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook');
$businessid = \EE\Controller\Base::getGetVariable('bid');
$companyid = \EE\Controller\Base::getGetVariable('cid');

/*mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");*/

if( in_array( $user, array( 'ryanp' ) ) ) exit( );

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=Treat_DB_ProxyOld::mysql_numrows($result);

if ($num!=0){
    $security_level=Treat_DB_ProxyOld::mysql_result($result,0,"security_level");
    $bid=Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
    $cid=Treat_DB_ProxyOld::mysql_result($result,0,"companyid");
    $loginid=Treat_DB_ProxyOld::mysql_result($result,0,"loginid");
}

if ($num != 1 || $user == "" || $pass == "") 
{
    echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}

else
{

?>
<head>

<script type="text/javascript" src="javascripts/prototype.js"></script>
<script type="text/javascript" src="javascripts/scriptaculous.js"></script>

<script language="JavaScript" 
   type="text/JavaScript">


 var update_expense = function(){
        var x = get_cookie ( "logincook" );
	new Ajax.Request('ajax_expense.php', {parameters: {method: 'ajax_expense2', lid: x}});

        setTimeout("window.parent.location = 'businesstrack.php'", 500); 
 }

function get_cookie ( cookie_name )
{
  var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );

  if ( results )
    return ( unescape ( results[2] ) );
  else
    return null;
}

</script>

</head>
<?
    ///////////////BEGIN MESSAGE BOARD//////////////////////////////

    $todaynum1 = date("d");
    $year1 = date("Y");
    $monthnum1 = date("m");
    $now2="$year1-$monthnum1-$todaynum1";
    $now99=futureday(1);

    $bbdayname=date("l");
    $bbmonth=date("F");
    $bbday=date("j"); 

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query98 = "DELETE from bulletin WHERE expire < '$today' AND user = '$loginid' AND expire != '0000-00-00'";
    $result98 = Treat_DB_ProxyOld::query($query98);
    //mysql_close();

    $messageboard="<ul>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query98 = "SELECT * FROM reserve WHERE businessid = '$bid' AND status = '3'";
    $result98 = Treat_DB_ProxyOld::query($query98);
    $num98=Treat_DB_ProxyOld::mysql_numrows($result98);
    //mysql_close();

    if ($num98!=0){$messageboard="<ul><li><a target='_parent' href=buscatertrack.php?bid=$businessid&cid=$companyid><font color=red><i><b>$num98 Submitted Order(s) Need Your Attention!</b></i></font></a>";}

        $dayname=date("l");
    if ($dayname=="Monday"){$dayname=1;}
    elseif ($dayname=="Tuesday"){$dayname=2;}
    elseif ($dayname=="Wednesday"){$dayname=3;}
    elseif ($dayname=="Thursday"){$dayname=4;}
    elseif ($dayname=="Friday"){$dayname=5;}
    elseif ($dayname=="Saturday"){$dayname=6;}
    elseif ($dayname=="Sunday"){$dayname=7;}

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query98 = "SELECT * FROM vendor_date WHERE businessid = '$bid' AND day = '$dayname' AND type = '0'";
    $result98 = Treat_DB_ProxyOld::query($query98);
    $num98=Treat_DB_ProxyOld::mysql_numrows($result98);
    //mysql_close();

    $num98--;
    while ($num98>=0){
       $vendorid=Treat_DB_ProxyOld::mysql_result($result98,$num98,"vendorid");

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query99 = "SELECT name FROM vendors WHERE vendorid = '$vendorid'";
       $result99 = Treat_DB_ProxyOld::query($query99);
       //mysql_close();
       $vendor_name=Treat_DB_ProxyOld::mysql_result($result99,0,"name");

       if ($messageboard!="<ul>"){$messageboard="$messageboard<li><font color=blue>Order for $vendor_name</font>";}
       else {$messageboard="<ul><li><font color=blue>Order for $vendor_name</font>";}

       $num98--;
    }
   
    //////////////////////////////////////////////////
    ///////////////UNHIDE EXPENSE REPORTS/////////////
    //////////////////////////////////////////////////
    $query35 = "SELECT apinvoice.* FROM apinvoice,purchase_card WHERE apinvoice.dm = '-1' AND apinvoice.transfer = '3' AND apinvoice.businessid = purchase_card.businessid AND apinvoice.vendor = purchase_card.loginid AND apinvoice.pc_type = purchase_card.type AND purchase_card.approve_loginid = '$loginid' AND apinvoice.posted != '0' ORDER BY apinvoice.date, apinvoice.apinvoiceid";
    $result35 = Treat_DB_ProxyOld::query($query35); 
    $num35=Treat_DB_ProxyOld::mysql_numrows($result35);

    if($num35>0){
       if ($messageboard!="<ul>"){$messageboard="$messageboard<li><a href=\"javascript: void(0);\" onclick=\"update_expense();this.color='#FF9900'\"><font color=red>$num35 Pending Expense Report(s)</font></a>";}
       else {$messageboard="<ul><li><a href=\"javascript: void(0);\" onclick=\"update_expense();this.color='#FF9900'\"><font color=red>$num35 Pending Expense Report(s)</font></a>";}
    }
    //////////////////////////////////////////////////

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query98 = "SELECT * FROM vendor_date WHERE businessid = '$bid' AND day = '$dayname' AND type = '1'";
    $result98 = Treat_DB_ProxyOld::query($query98);
    $num98=Treat_DB_ProxyOld::mysql_numrows($result98);
    //mysql_close();

    $num98--;
    while ($num98>=0){
       $vendorid=Treat_DB_ProxyOld::mysql_result($result98,$num98,"vendorid");

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query99 = "SELECT name FROM vendors WHERE vendorid = '$vendorid'";
       $result99 = Treat_DB_ProxyOld::query($query99);
       //mysql_close();
       $vendor_name=Treat_DB_ProxyOld::mysql_result($result99,0,"name");

       if ($messageboard!="<ul>"){$messageboard="$messageboard<li><font color=orange>$vendor_name Delivery</font>";}
       else {$messageboard="<ul><li><font color=orange>$vendor_name Delivery</font>";}

       $num98--;
    }

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query98 = "SELECT * FROM bulletin WHERE user = '$loginid' ORDER BY bulletinid";
    $result98 = Treat_DB_ProxyOld::query($query98);
    $num98=Treat_DB_ProxyOld::mysql_numrows($result98);
    //mysql_close();

    $lastbulletinid=0;

    $num98--;
    while ($num98>=0){
       $message=filterBadLink(Treat_DB_ProxyOld::mysql_result($result98,$num98,"message"));
       $bulletinid=Treat_DB_ProxyOld::mysql_result($result98,$num98,"bulletinid");
       $expire=Treat_DB_ProxyOld::mysql_result($result98,$num98,"expire");
       $replyid=Treat_DB_ProxyOld::mysql_result($result98,$num98,"reply");
       $reply_to=Treat_DB_ProxyOld::mysql_result($result98,$num98,"reply_to");
       $new=Treat_DB_ProxyOld::mysql_result($result98,$num98,"new");

       $newmessage=substr($message,1,10);
       if ($new==1){$fontclr="green";}
       else {$fontclr="black";}

       if ($expire=="0000-00-00"){$showdelete="<a href='delbulletin.php?bulletinid=$bulletinid&lastbulletinid=$lastbulletinid'><img border=0 src='delete.gif' alt='DELETE'></a>";}
       else {$showdelete="";}

       if ($messageboard!="<ul>"){$messageboard="$messageboard<li><a name=anc$bulletinid></a>$showdelete <font color=$fontclr>$message</font>";}
       else {$messageboard="<ul><li>$showdelete <font color=$fontclr>$message</font>";}

       $lastbulletinid=$bulletinid;
       $num98--;
    }

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query98 = "SELECT * FROM calender WHERE date = '$now2' AND companyid = '$cid'";
    $result98 = Treat_DB_ProxyOld::query($query98);
    $num98=Treat_DB_ProxyOld::mysql_numrows($result98);
    //mysql_close();

    $num98--;
    while ($num98>=0){
       $datebrief=Treat_DB_ProxyOld::mysql_result($result98,$num98,"brief");
       $datemessage=Treat_DB_ProxyOld::mysql_result($result98,$num98,"comment");
       $personal=Treat_DB_ProxyOld::mysql_result($result98,$num98,"personal");
       $you=Treat_DB_ProxyOld::mysql_result($result98,$num98,"user");
       if ($you == $user && $personal == "T"){
          if ($messageboard!="<ul>"){$messageboard="$messageboard<li><font color=blue>$datebrief. $datemessage.</font>";}
          else {$messageboard="<ul><li><font color=blue>$datebrief. $datemessage.</font>";}
       }
       elseif ($you != $user && $personal == "T"){}
       else{
          if ($messageboard!="<ul>"){$messageboard="$messageboard<li>$datebrief. $datemessage";}
          else {$messageboard="<ul><li>$datebrief. $datemessage";}
       }
       $num98--;
    }

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query98 = "SELECT * FROM calender WHERE date = '$now99' AND companyid = '$cid'";
    $result98 = Treat_DB_ProxyOld::query($query98);
    $num98=Treat_DB_ProxyOld::mysql_numrows($result98);
    //mysql_close();

    $num98--;
    while ($num98>=0){
       $datebrief=Treat_DB_ProxyOld::mysql_result($result98,$num98,"brief");
       $datemessage=Treat_DB_ProxyOld::mysql_result($result98,$num98,"comment");
       $personal=Treat_DB_ProxyOld::mysql_result($result98,$num98,"personal");
       $you=Treat_DB_ProxyOld::mysql_result($result98,$num98,"user");
       if ($you == $user && $personal == "T"){
          if ($messageboard!="<ul>"){$messageboard="$messageboard<li><font color=blue><i>Tomorrow:</i> $datebrief. $datemessage</font>";}
          else {$messageboard="<ul><li><font color=blue><i>Tomorrow:</i>$datebrief. $datemessage</font>";}
       }
       elseif ($you != $user && $personal == "T"){}
       else{
          if ($messageboard!="<ul>"){$messageboard="$messageboard<br><li><i>Tomorrow:</i> $datebrief. $datemessage.";}
          else {$messageboard="<ul><li><i>Tomorrow:</i> $datebrief. $datemessage.";}
       }
       $num98--;
    }

    $num98--;
    while ($num98>=0){
       $account=Treat_DB_ProxyOld::mysql_result($result98,$num98,"accountid");
       $event_time=Treat_DB_ProxyOld::mysql_result($result98,$num98,"event_time");
       $event_date=Treat_DB_ProxyOld::mysql_result($result98,$num98,"event_date");

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query99 = "SELECT name FROM accounts WHERE accountid = '$account'";
       $result99 = Treat_DB_ProxyOld::query($query99);
       //mysql_close();
       $accountname=Treat_DB_ProxyOld::mysql_result($result99,0,"name");

       if ($messageboard!="<ul>"){$messageboard="$messageboard<li>Catering for $accountname at $event_time.";}
       else {$messageboard="<ul><li>Catering for $accountname at $event_time.";}
       $num98--;
    }

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query98 = "SELECT * FROM invoice WHERE date = '$now99' AND companyid = '$cid' AND businessid = '$bid' AND status != '3'";
    $result98 = Treat_DB_ProxyOld::query($query98);
    $num98=Treat_DB_ProxyOld::mysql_numrows($result98);
    //mysql_close();

    $num98--;
    while ($num98>=0){
       $account=Treat_DB_ProxyOld::mysql_result($result98,$num98,"accountid");
       $event_time=Treat_DB_ProxyOld::mysql_result($result98,$num98,"event_time");
       $event_date=Treat_DB_ProxyOld::mysql_result($result98,$num98,"event_date");

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query99 = "SELECT name FROM accounts WHERE accountid = '$account'";
       $result99 = Treat_DB_ProxyOld::query($query99);
       //mysql_close();
       $accountname=Treat_DB_ProxyOld::mysql_result($result99,0,"name");

       if ($messageboard!="<ul>"){$messageboard="$messageboard<li>Catering for $accountname <i>tomorrow</i> at $event_time.";}
       else {$messageboard="<ul><li>Catering for $accountname <i>tomorrow</i> at $event_time.";}
       $num98--;
    }

    if($messageboard=="<ul>"){$messageboard="<a href=message.php?bid=$businessid&cid=$companyid style=$style target='_parent'><font color=black><i>No Messages</i></font></a>";}
    else {$messageboard="$messageboard</ul>";}

////////////////NEW MESSAGE POPUP

    $query98 = "SELECT * FROM bulletin WHERE user = '$loginid' AND new = '1'";
    $result98 = Treat_DB_ProxyOld::query($query98);
    $num198=Treat_DB_ProxyOld::mysql_numrows($result98);

    $query98 = "UPDATE bulletin SET new = '0' WHERE user = '$loginid' AND new = '1'";
    $result98 = Treat_DB_ProxyOld::query($query98);

////////////////END NEW MESSAGE

////////////////DISPLAY

    echo "<head><script type='text/javascript' src='silent_refresh.js'></script></head>";
    if($num198>0){echo "<body onLoad=\"javascript:setTimeout('alert(\'New Message in your Inbox\')',1250);\">$messageboard</body>";}
    else{echo "<body>$messageboard</body>";}

///////////////END MESSAGE BOARD//////////////////////////
}
//mysql_close();
?>
