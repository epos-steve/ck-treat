<?php

function isLeapYear($year) 
{ 
    /*
    # Check for valid parameters # 
    if (!is_int($year) || $year < 0) 
    { 
        printf('Wrong parameter for $year in function isLeapYear. It must be a positive integer.'); 
        exit(); 
    } 
    */
        
    # In the Gregorian calendar there is a leap year every year divisible by four 
    # except for years which are both divisible by 100 and not divisible by 400. 
        
    if ($year % 4 != 0) 
    { 
        return 28; 
    } 
    else 
    { 
        if ($year % 100 != 0) 
        { 
            return 29;    # Leap year 
        } 
        else 
        { 
            if ($year % 400 != 0) 
            { 
                return 28; 
            } 
            else 
            { 
                return 29;    # Leap year 
            } 
        } 
    } 
} 

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      elseif ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      elseif ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '1')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='31')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;
$leap = date("L");

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   
   elseif ($leap==1&&$month=='03')
   {
      $day=$day+29;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}


function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}

//include("db.php");
define('DOC_ROOT', dirname(dirname(__FILE__)));
require_once(DOC_ROOT.'/bootstrap.php');

/*$user=$_COOKIE["usercook"];
$pass=$_COOKIE["passcook"];
$cid=$_COOKIE["compcook"];
$caterbus=$_COOKIE["caterbus"];
$curmenu=$_COOKIE["curmenu"];
$curdaypart=$_COOKIE["curdaypart"];
$number=$_GET['number'];
$newmonth=$_GET['newmonth'];
$newyear=$_GET['newyear'];
$newstart=$_GET['newstart'];
$busid=$_GET['bid'];
$adddate=$_GET['adddate'];
$menu=$_GET['menu'];
$curgroupid=$_GET['groupid'];*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook');
$cid = \EE\Controller\Base::getSessionCookieVariable('compcook');
$caterbus = \EE\Controller\Base::getSessionCookieVariable('caterbus');
$curmenu = \EE\Controller\Base::getSessionCookieVariable('curmenu');
$curdaypart = \EE\Controller\Base::getSessionCookieVariable('curdaypart');
$number = \EE\Controller\Base::getGetVariable('number');
$newmonth = \EE\Controller\Base::getGetVariable('newmonth');
$newyear = \EE\Controller\Base::getGetVariable('newyear');
$newstart = \EE\Controller\Base::getGetVariable('newstart');
$busid = \EE\Controller\Base::getGetVariable('bid');
$adddate = \EE\Controller\Base::getGetVariable('adddate');
$menu = \EE\Controller\Base::getGetVariable('menu');
$curgroupid = \EE\Controller\Base::getGetVariable('groupid');

//if ($curgroupid==""){$curgroupid=$_COOKIE["curgroupid"];}
if ($curgroupid==""){$curgroupid = \EE\Controller\Base::getSessionCookieVariable('curgroupid');}

if($curgroupid!=""){setcookie("curgroupid",$curgroupid);}

$today = date("l");
$todaynum = date("j");
$todaynum2 = date("j");
$year = date("Y");
$monthnum = date("n");
$day=1;
$high = 6;
$long = 1;
$leap = date("L");
$back = "white";
$count = 0;
$style = "text-decoration:none";
$brief ="";
$you = "";

?>
<head>

<SCRIPT LANGUAGE="JavaScript" SRC="CalendarPopup.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">document.write(getCalendarStyles());</SCRIPT>

<STYLE>
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation
			{
			background-color:#6677DD;
			text-align:center;
			vertical-align:center;
			text-decoration:none;
			color:#FFFFFF;
			font-weight:bold;
			}
	.TESTcpDayColumnHeader,
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation,
	.TESTcpCurrentMonthDate,
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDate,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDate,
	.TESTcpCurrentDateDisabled,
	.TESTcpTodayText,
	.TESTcpTodayTextDisabled,
	.TESTcpText
			{
			font-family:arial;
			font-size:8pt;
			}
	TD.TESTcpDayColumnHeader
			{
			text-align:right;
			border:solid thin #6677DD;
			border-width:0 0 1 0;
			}
	.TESTcpCurrentMonthDate,
	.TESTcpOtherMonthDate,
	.TESTcpCurrentDate
			{
			text-align:right;
			text-decoration:none;
			}
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDateDisabled
			{
			color:#D0D0D0;
			text-align:right;
			text-decoration:line-through;
			}
	.TESTcpCurrentMonthDate
			{
			color:#6677DD;
			font-weight:bold;
			}
	.TESTcpCurrentDate
			{
			color: #FFFFFF;
			font-weight:bold;
			}
	.TESTcpOtherMonthDate
			{
			color:#808080;
			}
	TD.TESTcpCurrentDate
			{
			color:#FFFFFF;
			background-color: #6677DD;
			border-width:1;
			border:solid thin #000000;
			}
	TD.TESTcpCurrentDateDisabled
			{
			border-width:1;
			border:solid thin #FFAAAA;
			}
	TD.TESTcpTodayText,
	TD.TESTcpTodayTextDisabled
			{
			border:solid thin #6677DD;
			border-width:1 0 0 0;
			}
	A.TESTcpTodayText,
	SPAN.TESTcpTodayTextDisabled
			{
			height:20px;
			}
	A.TESTcpTodayText
			{
			color:#6677DD;
			font-weight:bold;
			}
	SPAN.TESTcpTodayTextDisabled
			{
			color:#D0D0D0;
			}
	.TESTcpBorder
			{
			border:solid thin #6677DD;
			}
</STYLE>

<script language="JavaScript" 
   type="text/JavaScript">
function changePage(newLoc)
 {
   nextPage = newLoc.options[newLoc.selectedIndex].value
		
   if (nextPage != "")
   {
      document.location.href = nextPage
   }
 }
</script>

<SCRIPT LANGUAGE="JavaScript">
<!-- Web Site:  http://dynamicdrive.com -->

<!-- This script and many more are available free online at -->
<!-- The JavaScript Source!! http://javascript.internet.com -->

<!-- Begin
function disableForm(theform) {
if (document.all || document.getElementById) {
for (i = 0; i < theform.length; i++) {
var tempobj = theform.elements[i];
if (tempobj.type.toLowerCase() == "submit" || tempobj.type.toLowerCase() == "reset")
tempobj.disabled = true;
}

}
else {
alert("The form has been submitted.  But, since you're not using IE 4+ or NS 6, the submit button was not disabled on form submission.");
return false;
   }
}
//  End -->
</script>

</head>

<?php

if ($newstart == "")
{
   while ($todaynum > 7){$todaynum = $todaynum - 7; $count++;}
   if ($today == 'Sunday' && $todaynum == 1){$start=1;}
   if ($today == 'Monday' && $todaynum == 1){$start=2;}
   if ($today == 'Tuesday' && $todaynum == 1){$start=3;}
   if ($today == 'Wednesday' && $todaynum == 1){$start=4;}
   if ($today == 'Thursday' && $todaynum == 1){$start=5;}
   if ($today == 'Friday' && $todaynum == 1){$start=6;}
   if ($today == 'Saturday' && $todaynum == 1){$start=7;}
   if ($today == 'Sunday' && $todaynum == 2){$start=7;}
   if ($today == 'Monday' && $todaynum == 2){$start=1;}
   if ($today == 'Tuesday' && $todaynum == 2){$start=2;}
   if ($today == 'Wednesday' && $todaynum == 2){$start=3;}
   if ($today == 'Thursday' && $todaynum == 2){$start=4;}
   if ($today == 'Friday' && $todaynum == 2){$start=5;}
   if ($today == 'Saturday' && $todaynum == 2){$start=6;}
   if ($today == 'Sunday' && $todaynum == 3){$start=6;}
   if ($today == 'Monday' && $todaynum == 3){$start=7;}
   if ($today == 'Tuesday' && $todaynum == 3){$start=1;}
   if ($today == 'Wednesday' && $todaynum == 3){$start=2;}
   if ($today == 'Thursday' && $todaynum == 3){$start=3;}
   if ($today == 'Friday' && $todaynum == 3){$start=4;}
   if ($today == 'Saturday' && $todaynum == 3){$start=5;}
   if ($today == 'Sunday' && $todaynum == 4){$start=5;}
   if ($today == 'Monday' && $todaynum == 4){$start=6;}
   if ($today == 'Tuesday' && $todaynum == 4){$start=7;}
   if ($today == 'Wednesday' && $todaynum == 4){$start=1;}
   if ($today == 'Thursday' && $todaynum == 4){$start=2;}
   if ($today == 'Friday' && $todaynum == 4){$start=3;}
   if ($today == 'Saturday' && $todaynum == 4){$start=4;}
   if ($today == 'Sunday' && $todaynum == 5){$start=4;}
   if ($today == 'Monday' && $todaynum == 5){$start=5;}
   if ($today == 'Tuesday' && $todaynum == 5){$start=6;}
   if ($today == 'Wednesday' && $todaynum == 5){$start=7;}
   if ($today == 'Thursday' && $todaynum == 5){$start=1;}
   if ($today == 'Friday' && $todaynum == 5){$start=2;}
   if ($today == 'Saturday' && $todaynum == 5){$start=3;}
   if ($today == 'Sunday' && $todaynum == 6){$start=3;}
   if ($today == 'Monday' && $todaynum == 6){$start=4;}
   if ($today == 'Tuesday' && $todaynum == 6){$start=5;}
   if ($today == 'Wednesday' && $todaynum == 6){$start=6;}
   if ($today == 'Thursday' && $todaynum == 6){$start=7;}
   if ($today == 'Friday' && $todaynum == 6){$start=1;}
   if ($today == 'Saturday' && $todaynum == 6){$start=2;}
   if ($today == 'Sunday' && $todaynum == 7){$start=2;}
   if ($today == 'Monday' && $todaynum == 7){$start=3;}
   if ($today == 'Tuesday' && $todaynum == 7){$start=4;}
   if ($today == 'Wednesday' && $todaynum == 7){$start=5;}
   if ($today == 'Thursday' && $todaynum == 7){$start=6;}
   if ($today == 'Friday' && $todaynum == 7){$start=7;}
   if ($today == 'Saturday' && $todaynum == 7){$start=1;}
}
elseif ($newstart > 7)
{
   $start = $newstart - 7;
}
else 
{
   $start = $newstart;
}

if ($newmonth != ""){;}
else {$newmonth = $monthnum;}
if ($newyear != ""){$year=$newyear;}
else {$newyear = $year;}

if ($adddate==""){
$day4 = date("d");
$year4 = date("Y");
$month4 = date("m");
$adddate="$year4-$month4-$day4";
}

$hour = date("H");
$min = date("i");
$year1 = date("y");
$mon = date("m");
$day2 = date("d");
$time2 = "1$year1$mon$day2$hour$min";

$style = "text-decoration:none";

$hour = date("H");
$min = date("i");
$year2 = date("y");
$mon = date("m");
$day2 = date("d");
$now = "1$year2$mon$day2$hour$min";
$dif = $now-$time;

/*mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");*/

   //mysql_connect($dbhost,$username,$password);
   //@mysql_select_db($database) or die( "Unable to select database");
   $query = "SELECT default_menu,vend_start,vend_end FROM business WHERE businessid = '$caterbus'";
   $result = Treat_DB_ProxyOld::query($query);
   //mysql_close();

   if ($curmenu==""){$curmenu=mysql_result($result,0,"default_menu");}
   $vend_start=Treat_DB_ProxyOld::mysql_result($result,0,"vend_start");
   $vend_end=Treat_DB_ProxyOld::mysql_result($result,0,"vend_end");


//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM menu_type WHERE menu_typeid = '$curmenu'";
$result = Treat_DB_ProxyOld::query($query);
//mysql_close();

$menuname=Treat_DB_ProxyOld::mysql_result($result,0,"menu_typename");

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=Treat_DB_ProxyOld::mysql_numrows($result);
//mysql_close();

if ($num!=1 || ($user == "" && $pass == "")) 
{
    echo "<center><h4>Failed</h4></center>";
}

else
{
    $mymonth=$newmonth;$myyear=$newyear;$mystart=$newstart;

    echo "<body>";  

    if($curdaypart>0){$fontsize=2;}
    else{$fontsize=1;}

    $bid=$caterbus;
    $cid=Treat_DB_ProxyOld::mysql_result($result,0,"companyid"); 
    $security_level=Treat_DB_ProxyOld::mysql_result($result,0,"security_level");

    if ($newmonth == 1){$month = "January";}
    elseif ($newmonth == 2){$month = "February";}
    elseif ($newmonth == 3){$month = "March";}
    elseif ($newmonth == 4){$month = "April";}
    elseif ($newmonth == 5){$month = "May";}
    elseif ($newmonth == 6){$month = "June";}
    elseif ($newmonth == 7){$month = "July";}
    elseif ($newmonth == 8){$month = "August";}
    elseif ($newmonth == 9){$month = "September";}
    elseif ($newmonth == 10){$month = "October";}
    elseif ($newmonth == 11){$month = "November";}
    elseif ($newmonth == 12){$month = "December";}

    echo "<center><table cellspacing=0 cellpadding=0 width=99% border=0><tr><td colspan=2><a href=businesstrack.php><img src=logo.jpg border=0 height=43 width=205></a><p></td></tr>";
    echo "</table><p><table width=99% style=\"border:2px solid #CCCCCC;\" cellspacing=0 cellpadding=0>";
    echo "<tr bgcolor=#CCCCFF><td colspan=2><center><br><form action=curcycle.php method=post><font size=4><b>$menuname ";

    echo "$month $year</font></b> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<input type=hidden name=newmonth value=$newmonth><input type=hidden name=newyear value=$newyear><input type=hidden name=newstart value=$newstart>Current Cycle: ";
    echo "<SCRIPT LANGUAGE='JavaScript' ID='js18'> var cal18 = new CalendarPopup('testdiv1');cal18.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date1 VALUE='$vend_start' SIZE=8> <A HREF=# onClick=cal18.select(document.forms[0].date1,'anchor18','yyyy-MM-dd'); return false; TITLE=cal18.select(document.forms[0].date1,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor18' ID='anchor18'><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A> to ";
    echo "<SCRIPT LANGUAGE='JavaScript' ID='js19'> var cal19 = new CalendarPopup('testdiv1');cal19.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date2 VALUE='$vend_end' SIZE=8> <A HREF=# onClick=cal19.select(document.forms[0].date2,'anchor19','yyyy-MM-dd'); return false; TITLE=cal19.select(document.forms[0].date2,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor19' ID='anchor19'><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A>";
    echo " <input type=submit value='Save'></form></center></form></td></tr>";

    $leap=isLeapYear($year);
    if($leap==29){$leap=1;}
    else{$leap=0;}

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query23 = "SELECT * FROM menu_groups WHERE menu_typeid = '$curmenu'";
    $result23 = Treat_DB_ProxyOld::query($query23);
    $num23=Treat_DB_ProxyOld::mysql_numrows($result23);
    //mysql_close();

    $num23--;

    echo "<tr bgcolor=#CCCCFF><td colspan=2><center><form action=busordermenu2.php?newmonth=$newmonth&newyear=$newyear&newstart=$newstart&curgroupid=0 method=post><select name=groupid onChange='changePage(this.form.groupid)'><option value=busordermenu2.php?newmonth=$newmonth&newyear=$newyear&newstart=$newstart&groupid=0></option>";

    while($num23>=0){
       $groupid=Treat_DB_ProxyOld::mysql_result($result23,$num23,"menu_group_id"); 
       $groupname=Treat_DB_ProxyOld::mysql_result($result23,$num23,"groupname");
       if ($curgroupid==$groupid){$showsel="SELECTED";}
       else{$showsel="";}
       echo "<option value=busordermenu2.php?newmonth=$newmonth&newyear=$newyear&newstart=$newstart&groupid=$groupid $showsel>$groupname</option>";
       $num23--;
    }

    echo "</select>";

    echo "</form></center></td></tr>";

    $weekstart=array();
    $weekending=array();
    $temp_date=$vend_start;
    $counter=1;
    while($temp_date<=$vend_end){
       if(dayofweek($temp_date)=="Monday"){
          $weekstart[$counter]=$temp_date;
          $temp_date2=$temp_date;
          for($mycount=1;$mycount<=4;$mycount++){$temp_date2=nextday($temp_date2);}
          $weekending[$counter]=$temp_date2;
          $counter++;
       }
       $temp_date=nextday($temp_date);
    }
    $totalweeks=$counter-1;

    echo "<tr bgcolor=#CCCCFF><td colspan=2>";

    echo "<center><table border=1 cellpadding=0 cellspacing=0>";
    echo "<tr bgcolor=#FFFF99><td><center>Sunday</center></td><td><center>Monday</center></td><td><center>Tuesday</center></td><td><center>Wednesday</center></td><td><center>Thursday</center></td><td><center>Friday</center></td><td><center>Saturday</center></td></tr>";
    
    echo "<tr height=110>";
    $weekend = "F";
    while ($long < 8)
    {  
       if ($long >= $start)
       {
          if ($long == 1 || $long == 7){$weekend="T";}

          if ($day < 10 && $newmonth < 10){$date = "$newyear-0$newmonth-0$day";}
          elseif ($day < 10 && $newmonth > 10){$date = "$newyear-$newmonth-0$day";}
          elseif ($day > 10 && $newmonth < 10){$date = "$newyear-0$newmonth-$day";}
          else {$date = "$newyear-$newmonth-$day";}

          if ($newstart == ""){$newstart=$start;}
          $correcturl = "orderdate2.php?date=$date&newmonth=$newmonth&newyear=$newyear&newstart=$newstart&day=$day&weekend=$weekend";
          $correcturl2 = "orderweek2.php?date=$date&newmonth=$newmonth&newyear=$newyear&newstart=$newstart&day=$day&weekend=$weekend";

          if ($day == $todaynum2 && $monthnum == $newmonth && $year == $newyear){$color = "red";}
          else {$color = "black";}

          echo "<a style=$style href=$correcturl><td bgcolor=$back width=130 valign=top onMouseOver=this.bgColor='#999999' onMouseOut=this.bgColor='$back'>";
          
          echo "<a style=$style href=$correcturl><b><font color=$color>$day</font></b></a><br>";
/////////////////////////////////////////////////////////////
      
          if ($date==$vend_start){echo "<table width=100% bgcolor=#00FF00><tr height=1><td><font size=1><center>CYCLE START</center></font></td></tr></table>";}
          elseif ($date==$vend_end){echo "<table width=100% bgcolor=#00FF00><tr height=1><td><font size=1><center>CYCLE END</center></font></td></tr></table>";}
          elseif ($date>=$vend_start&&$date<=$vend_end){echo "<table width=100% bgcolor=#00FF00><tr height=1><td><font size=1><center>&nbsp</center></font></td></tr></table>";}

          $showstart=0;
          for($mycount=1;$mycount<=$totalweeks;$mycount++){if($date==$weekstart[$mycount]){$showstart=$mycount;}}
          $showend=0;
          for($mycount=1;$mycount<=$totalweeks;$mycount++){if($date==$weekending[$mycount]){$showend=$mycount;}}

          if ($showstart!=0){echo "<table width=100% bgcolor=yellow><tr height=1><td><font size=1><center><a href=$correcturl2>Wk$showstart START</a></center></font></td></tr></table>";}
          elseif ($showend!=0){echo "<table width=100% bgcolor=yellow><tr height=1><td><font size=1><center>Wk$showend END</center></font></td></tr></table>";}

//////////////////LIST ITEMS
if ($curgroupid!=0&&$curgroupid!=""){
       echo "<table width=100% cellspacing=0 cellpadding=0>";
          
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query23 = "SELECT * FROM menu_pricegroup WHERE menu_groupid = '$curgroupid' ORDER BY orderid DESC";
       $result23 = Treat_DB_ProxyOld::query($query23);
       $num23=Treat_DB_ProxyOld::mysql_numrows($result23);
       //mysql_close();

       $mycolor="white";
       $num23--;
       while($num23>=0){

          $pricegroupid=Treat_DB_ProxyOld::mysql_result($result23,$num23,"menu_pricegroupid"); 
          $pricegroupname=Treat_DB_ProxyOld::mysql_result($result23,$num23,"menu_pricegroupname");

          if ($mycolor=="white"){$mycolor="#E8E7E7";}
          else{$mycolor="white";}

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query24 = "SELECT * FROM vend_item WHERE groupid = '$pricegroupid' AND date = '$date' AND businessid = '$caterbus'";
          $result24 = Treat_DB_ProxyOld::query($query24);
          $num24=Treat_DB_ProxyOld::mysql_numrows($result24);
          //mysql_close();

          $num24--;
          while($num24>=0){

             $menu_itemid=Treat_DB_ProxyOld::mysql_result($result24,$num24,"menu_itemid"); 

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query25 = "SELECT item_name FROM menu_items WHERE menu_item_id = '$menu_itemid'";
             $result25 = Treat_DB_ProxyOld::query($query25);
             //mysql_close();

             $menu_itemname=Treat_DB_ProxyOld::mysql_result($result25,0,"item_name"); 

             echo "<tr bgcolor=$mycolor><td><font size=1>-$menu_itemname</font></td></tr>";

             $num24--;
          }
          $num23--;
       }
       echo "</table>";

}
/////////////////////////////////////////////////////////////
          $counter=0;

          echo "</td></a>";
          $back = "white";

          if ($day == 1 && $long == 1){$backstart=7;}
          elseif ($day == 1 && $long != 1){$backstart=$long-1;}

          $day++;
       }
       else
       {
          echo "<td width=110 bgcolor=#DCDCDC></td>";
       }
       $weekend="F";
       $long++;
    }
    echo "</tr>";
    
    while ($high > 1)
    {
       $long=1;
       echo "<tr height=110>";
       $weekend = "F";
       while ($long < 8)
       {
          if ($long == 1 || $long == 7){$weekend="T";}

          if ($day < 10 && $newmonth < 10){$date = "$newyear-0$newmonth-0$day";}
          elseif ($day < 10 && $newmonth > 10){$date = "$newyear-$newmonth-0$day";}
          elseif ($day >= 10 && $newmonth < 10){$date = "$newyear-0$newmonth-$day";}
          else {$date = "$newyear-$newmonth-$day";}      

          if ($newstart == ""){$newstart=$start;}
          $correcturl = "orderdate2.php?date=$date&newmonth=$newmonth&newyear=$newyear&newstart=$newstart&day=$day&weekend=$weekend";
          $correcturl2 = "orderweek2.php?date=$date&newmonth=$newmonth&newyear=$newyear&newstart=$newstart&day=$day&weekend=$weekend";

          $show = $day;
          if ($day > 28 && $newmonth == 2 && $leap == 0){$show = "";}
          elseif ($day > 29 && $newmonth == 2 and $leap == 1){$show = "";}
          elseif ($day > 30 && ($newmonth == 4 || $newmonth == 6 || $newmonth == 9 || $newmonth == 11)){$show = "";}
          elseif ($day > 31){$show = "";}
          else {;}
          
          if ($day > 28 && $newmonth == 2 && $leap == 0){$back = '#DCDCDC';}
          elseif ($day > 29 && $newmonth == 2 and $leap == 1){$back = '#DCDCDC';}
          elseif ($day > 30 && ($newmonth == 4 || $newmonth == 6 || $newmonth == 9 || $newmonth == 11)){$back = '#DCDCDC';}
          elseif ($day > 31){$back = '#DCDCDC';}
          else {$back = 'white';}

          if ($day == 29 && $newmonth == 2 && $leap ==0){$newstart=$long;}
          if ($day == 30 && $newmonth == 2 && $leap ==1){$newstart=$long;}
          if ($day == 31 && ($newmonth == 4 || $newmonth == 6 || $newmonth == 9 || $newmonth == 11)){$newstart=$long;}
          if ($day == 32 && ($newmonth == 1 || $newmonth == 3 || $newmonth == 5 || $newmonth == 7 || $newmonth == 8 || $newmonth == 10 || $newmonth == 12)){$newstart=$long;}     
         
          if ($day == $todaynum2 && $monthnum == $newmonth && $year == $newyear){$color = "red";}
          else {$color = "black";}

          echo "<a style=$style href=$correcturl><td bgcolor=$back width=130 valign=top onMouseOver=this.bgColor='#999999' onMouseOut=this.bgColor='$back'>";
          
          echo "<a style=$style href=$correcturl><b><font color=$color>$show</font></b></a><br>";
/////////////////////////////////////////////////////////////
if ($back!="#DCDCDC"){     
          if ($date==$vend_start){echo "<table width=100% bgcolor=#00FF00><tr height=1><td><font size=1><center>CYCLE START</center></font></td></tr></table>";}
          elseif ($date==$vend_end){echo "<table width=100% bgcolor=#00FF00><tr height=1><td><font size=1><center>CYCLE END</center></font></td></tr></table>";}
          elseif ($date>=$vend_start&&$date<=$vend_end){echo "<table width=100% bgcolor=#00FF00><tr height=1><td><font size=1><center>&nbsp</center></font></td></tr></table>";}

          $showstart=0;
          for($mycount=1;$mycount<=$totalweeks;$mycount++){if($date==$weekstart[$mycount]){$showstart=$mycount;}}
          $showend=0;
          for($mycount=1;$mycount<=$totalweeks;$mycount++){if($date==$weekending[$mycount]){$showend=$mycount;}}

          if ($showstart!=0){echo "<table width=100% bgcolor=yellow><tr height=1><td><font size=1><center><a href=$correcturl2>Wk$showstart START</a></center></font></td></tr></table>";}
          elseif ($showend!=0){echo "<table width=100% bgcolor=yellow><tr height=1><td><font size=1><center>Wk$showend END</center></font></td></tr></table>";}

//////////////////LIST ITEMS
if ($curgroupid!=0&&$curgroupid!=""){
       echo "<table width=100% cellspacing=0 cellpadding=0>";
          
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query23 = "SELECT * FROM menu_pricegroup WHERE menu_groupid = '$curgroupid' ORDER BY orderid DESC";
       $result23 = Treat_DB_ProxyOld::query($query23);
       $num23=Treat_DB_ProxyOld::mysql_numrows($result23);
       //mysql_close();

       $mycolor="white";
       $num23--;
       while($num23>=0){

          $pricegroupid=Treat_DB_ProxyOld::mysql_result($result23,$num23,"menu_pricegroupid"); 
          $pricegroupname=Treat_DB_ProxyOld::mysql_result($result23,$num23,"menu_pricegroupname");

          if ($mycolor=="white"){$mycolor="#E8E7E7";}
          else{$mycolor="white";}

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query24 = "SELECT * FROM vend_item WHERE groupid = '$pricegroupid' AND date = '$date' AND businessid = '$caterbus'";
          $result24 = Treat_DB_ProxyOld::query($query24);
          $num24=Treat_DB_ProxyOld::mysql_numrows($result24);
          //mysql_close();

          $num24--;
          while($num24>=0){

             $menu_itemid=Treat_DB_ProxyOld::mysql_result($result24,$num24,"menu_itemid"); 

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query25 = "SELECT item_name FROM menu_items WHERE menu_item_id = '$menu_itemid'";
             $result25 = Treat_DB_ProxyOld::query($query25);
             //mysql_close();

             $menu_itemname=Treat_DB_ProxyOld::mysql_result($result25,0,"item_name"); 

             echo "<tr bgcolor=$mycolor><td><font size=1>-$menu_itemname</font></td></tr>";

             $num24--;
          }
          $num23--;
       }
       echo "</table>";

}

}
/////////////////////////////////////////////////////////////
          $counter=0;
         
          echo "</td></a>";
          $back = "white";

          $day++;
          $weekend = "F";
          $long++;
       }
       echo "</tr>";
       $high--;
    } 
    echo "</table></center>";

    if ($newmonth == 5 || $newmonth == 7 || $newmonth == 10 || $newmonth == 12){$count=30;}
    elseif ($newmonth == 1 || $newmonth == 2 || $newmonth == 4 || $newmonth == 6 || $newmonth == 8 || $newmonth == 9 || $newmonth == 11){$count=31;}
    elseif ($newmonth == 3 && $leap == 0){$count=28;}
    elseif ($newmonth == 3 && $leap == 1){$count=29;}

    $count2 = $backstart;
    while ($count > 1)
    {
       if ($count2 == 1){$count2 = 8;}
       $count--;
       $count2--;
    }
    $backstart = $count2;

    $prevmonth = $newmonth - 1;
    $prevyear = $newyear;
    if ($prevmonth == 0){$prevmonth = 12; $prevyear = $year - 1;}
    $nextmonth = $newmonth + 1;
    $nextyear = $newyear;
    if ($nextmonth == 13){$nextmonth = 1; $nextyear=$year + 1;}
    echo "<center><a href='busordermenu2.php?newmonth=$prevmonth&newyear=$prevyear&newstart=$backstart'>Previous Month</a> :: <a href='busordermenu2.php?newmonth=$nextmonth&newyear=$nextyear&newstart=$newstart'>Next Month</a></center><p>";

    echo "</td></tr>";
    echo "</table><p>";

    $action2 = "buscatermenu.php?bid=$caterbus&cid=$cid";
    $method = "post";
    echo "<center><table width=90%><tr><td>";

    /////////////////////COPY TO ANOTHER UNIT
    echo "<form action=utility/copyvendorder.php method=post target='_blank'>Copy Menu From: ";
    echo "<input type=hidden name=businessid value=$caterbus><input type=hidden name=menu_typeid value=$curmenu>";
    echo "<SCRIPT LANGUAGE='JavaScript' ID='js28'> var cal28 = new CalendarPopup('testdiv1');cal28.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date1 VALUE='$vend_start' SIZE=8> <A HREF='javascript:void();' onClick=cal28.select(document.forms[2].date1,'anchor28','yyyy-MM-dd'); return false; TITLE=cal28.select(document.forms[2].date1,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor28' ID='anchor28'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A> to ";
    echo "<SCRIPT LANGUAGE='JavaScript' ID='js29'> var cal29 = new CalendarPopup('testdiv1');cal29.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date2 VALUE='$vend_end' SIZE=8> <A HREF='javascript:void();' onClick=cal29.select(document.forms[2].date2,'anchor29','yyyy-MM-dd'); return false; TITLE=cal29.select(document.forms[2].date2,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor29' ID='anchor29'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A>";
    echo " to <select name=towho>";

    $query25 = "SELECT business.businessname,business.businessid FROM business,menu_type WHERE menu_type.parent = '$curmenu' AND menu_type.businessid = business.businessid ORDER BY business.businessname";
    $result25 = Treat_DB_ProxyOld::query($query25);

    echo "<option value=-1>All</option>";
    while($r=Treat_DB_ProxyOld::mysql_fetch_array($result25)){
       $businessid=$r["businessid"];
       $businessname=$r["businessname"];

       echo "<option value=$businessid>$businessname</option>";
    }

    echo "</select> <input type=submit value='GO'></form></td>";


    echo "<td align=right><FORM ACTION='$action2' method=$method>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<INPUT TYPE=submit VALUE=' Return '></FORM></td></tr></table></center><DIV ID=testdiv1 STYLE=position:absolute;visibility:hidden;background-color:white;layer-background-color:white;></DIV></body>";
	
	google_page_track();
}

//mysql_close();
?>