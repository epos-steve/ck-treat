<?php

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+4);
        $diff=round($diff, 2);
        if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
        elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
        $diff = substr($diff, 0, $dot+3);
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      elseif ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      elseif ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '1')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='31')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;
$leap = date("L");

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   
   elseif ($leap==1&&$month=='03')
   {
      $day=$day+29;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      $day=$day-31;  
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";} 
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}

include("db.php");
$style = "text-decoration:none";

$user=$_COOKIE["usercook"];
$pass=$_COOKIE["passcook"];
$businessid=$_POST["businessid"];
$companyid=$_POST["companyid"];
$today=$_POST["date"];

mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = mysql_query($query);
$num=mysql_numrows($result);
//mysql_close();

$loginid=mysql_result($result,0,"loginid");
$security_level=mysql_result($result,0,"security_level");
$mysecurity=mysql_result($result,0,"security");
$bid=mysql_result($result,0,"businessid");
$bid2=mysql_result($result,0,"busid2");
$bid3=mysql_result($result,0,"busid3");
$bid4=mysql_result($result,0,"busid4");
$bid5=mysql_result($result,0,"busid5");
$bid6=mysql_result($result,0,"busid6");
$bid7=mysql_result($result,0,"busid7");
$bid8=mysql_result($result,0,"busid8");
$bid9=mysql_result($result,0,"busid9");
$bid10=mysql_result($result,0,"busid10");
$cid=mysql_result($result,0,"companyid");

if ($num != 1 || $user == "" || $pass == "")
{
    echo "<center><h3>Failed</h3>Use your browser's back button to try again.</center>";
}
else
{
    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query34 = "SELECT * FROM payroll_code_comm WHERE companyid = '$companyid' ORDER BY orderid DESC";
    $result34 = mysql_query($query34);
    $num34=mysql_numrows($result34);
    //mysql_close();


    /////////////END HEADER

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM jobtype WHERE companyid = '$companyid' AND commission = '1' ORDER BY name DESC";
    $result = mysql_query($query);
    $num=mysql_numrows($result);
    //mysql_close();

    $num--;
    while($num>=0){
       $jobtypeid=mysql_result($result,$num,"jobtypeid");
       $jobtypename=mysql_result($result,$num,"name");

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM labor_temp_comm,security_departments,payroll_departmentdetail WHERE labor_temp_comm.jobtypeid = '$jobtypeid' AND labor_temp_comm.businessid = '$businessid' AND labor_temp_comm.date = '$today' AND (labor_temp_comm.loginid = payroll_departmentdetail.loginid AND payroll_departmentdetail.dept_id = security_departments.dept_id AND security_departments.securityid = '$mysecurity')";
       $result2 = mysql_query($query2);
       $num2=mysql_numrows($result2);
       //mysql_close();

       $num2--;
       while($num2>=0){
          $tempid=mysql_result($result2,$num2,"labor_temp_comm.tempid");
          $loginid=mysql_result($result2,$num2,"labor_temp_comm.loginid");
          $rate=mysql_result($result2,$num2,"labor_temp_comm.rate");

          $temp_num=$num34-1;
          while($temp_num>=0){
             $codeid=mysql_result($result34,$temp_num,"codeid");

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query3 = "SELECT * FROM labor_comm WHERE temp_commid = '$tempid' AND coded = '$codeid'";
             $result3 = mysql_query($query3);
             $num3=mysql_numrows($result3);
             //mysql_close();

             $myname="$tempid:$codeid";

             $value=$_POST[$myname];

             if ($num3>0){
                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query4 = "UPDATE labor_comm SET amount = '$value' WHERE temp_commid = '$tempid' AND coded = '$codeid'";
                $result4 = mysql_query($query4);
                //mysql_close();
             }
             elseif($value!=0){
                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query4 = "INSERT INTO labor_comm (temp_commid,coded,amount) VALUES ('$tempid','$codeid','$value')";
                $result4 = mysql_query($query4);
                //mysql_close();
             }
            
             $temp_num--;
          }

          $num2--;
       }

       $num--;
    }
    $query4 = "INSERT INTO audit_comm (date,user,comment,businessid) VALUES ('$today','$user','Week of $today Saved','$businessid')";
    $result4 = mysql_query($query4);
    
    $location="buslabor2.php?bid=$businessid&cid=$companyid&newdate=$today#labor";
    header('Location: ./' . $location);

}
mysql_close();
?>