<?php

if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
require_once( DOC_ROOT . '/bootstrap.php' );

$passed_cookie_login = require_once( realpath( DOC_ROOT . '/../lib/inc/cookie-login.php' ) );
if (
	$passed_cookie_login
	&& 9 == $GLOBALS['security_level']
) {
?>
<html>
	<head>
		<title>Test PHP Variables</title>
	</head>
	<body>
		<pre><?php
	if ( $_POST ) {
		echo '$_POST = ';
		var_dump( $_POST );
		echo '<hr/>';
	}
	echo '$_GET = ';
	var_dump( $_GET );
	echo '<hr/>';
	echo '$_COOKIE = ';
	var_dump( $_COOKIE );
	echo '<hr/>';
	echo '$_SESSION = ';
	var_dump( $_SESSION );
	echo '<hr/>';
	echo '$GLOBALS = ';
	var_dump( $GLOBALS );
		?></pre>
	</body>
</html>
<?php
}