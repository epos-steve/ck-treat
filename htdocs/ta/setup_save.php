<?php

if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
require_once( dirname(__FILE__) . '/../../application/bootstrap.php' );

#ini_set("display_errors","true");

$style = "text-decoration:none;";

if ( require_once( realpath( DOC_ROOT . '/../lib/inc/cookie-login.php' ) ) ) {
	$businessid = intval($_POST["businessid"]);
	$goto = $_POST["goto"];
	$setup_status = $_POST["setup_status"];
	$user_cook = isset($_COOKIE["usercook"])?$_COOKIE["usercook"]:'';

	$query = "SELECT * FROM setup WHERE setup_status = $setup_status ORDER BY orderid";
	$result = Treat_DB_ProxyOld::query($query);

	$all_checks_new = 0;
	$all_checks_old = 0;

	while($r = mysql_fetch_array($result)){
		$id = $r["id"];
		$mapping = $r["mapping"];
		$link = $r["link"];
		$groupid = $r["groupid"];
		$type = $r["type"];

		$value = $_POST["val$id"];
		
		///checkbox values
		if($type == 5 && $value != 1){$value = 0;}
		
		if($value != ""){$complete = 1;}
		//$complete = $_POST["cp$id"];

		$old_value = $_POST["old_val$id"];
		$old_complete = $_POST["old_cp$id"];

		if($complete != 1 && $groupid == 1){$all_checks_new = 1;}
		if($old_complete != 1 && $groupid == 1){$all_checks_old = 1;}

		$value = str_replace("'","`",$value);

		if($value == ""){$complete = 0;}

		if($old_value != $value || $old_complete != $complete){
			$query2 = "INSERT INTO
					setup_detail
						(setupid,businessid,value,complete,editby)
					VALUES
						('$id','$businessid','$value','$complete','$user_cook')
					ON DUPLICATE KEY UPDATE
						value = '$value',
						complete = '$complete',
						editby = '$user_cook'";
			$result2 = Treat_DB_ProxyOld::query($query2);
			$insert_id = Treat_DB_ProxyOld::mysql_insert_id();

			////email if ship date changes
			/*
			if($id == 31){
				////operator name
				$query3 = "SELECT company.companyname FROM company
							JOIN business ON business.companyid = company.companyid AND business.businessid = $businessid";
				$result3 = Treat_DB_ProxyOld::query($query3);

				$companyname = Treat_DB_ProxyOld::mysql_result($result3,0,"companyname");

				$email = "smartin@essentialpos.com,mburris@essentialpos.com.test-google-a.com,peteb@treatamerica.com,sandrab@treatamerica.com,peteb@treatamerica.com";
				$email_name = $_POST["val3"];
				$email_subject = "Kiosk Update";
				$email_message = "Operator $companyname: $email_name has a ship date of $value\r\n$user_cook";

				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				$headers .= "From: support@companykitchen.com\r\n";
				mail($email, $email_subject, $email_message, $headers);
			}*/
		}

		////mapping
		$mapping = explode(",",$mapping);
		
		if ( ! isset($mapping[1])){
			$mapping[1] = 0;
		} else {
			$mapping[1] = $mapping[1];
		}
		
		if($mapping[0] != "" && $mapping[1] == 2 && $complete == 1){
			$update = explode(".",$mapping[0]);
			$edit_id = 0;

			/////get editid from any other details linked together 
			if($link != ""){
				$link = explode(",",$link);

				$counter = 1;
				foreach($link AS $key => $values){
					if($counter==1){$where = "setupid = $values";}
					else{$where .= " OR setupid = $values";}
					$counter++;
				}

				$query2 = "SELECT edit_id FROM setup_detail WHERE ($where) AND businessid = $businessid AND edit_id > 0 ORDER BY edit_id DESC LIMIT 1";
				$result2 = Treat_DB_ProxyOld::query($query2);

				$edit_id = Treat_DB_ProxyOld::mysql_result($result2,0,"edit_id");
			}

			if($edit_id == 0)
			{
				$query2 = "SELECT edit_id FROM setup_detail WHERE setupid = $id AND businessid = $businessid";
				$result2 = Treat_DB_ProxyOld::query($query2);

				$edit_id = Treat_DB_ProxyOld::mysql_result($result2,0,"edit_id");
			}

			////update real fields
			if($edit_id > 0)
			{
				$query2 = "UPDATE {$update[0]} SET {$update[1]} = '$value' WHERE id = $edit_id";
				$result2 = Treat_DB_ProxyOld::query($query2);
			}
			else
			{
				$query2 = "INSERT INTO {$update[0]} ({$update[1]},businessid) VALUES ('$value',$businessid)";
				$result2 = Treat_DB_ProxyOld::query($query2);
				$edit_id = Treat_DB_ProxyOld::mysql_insert_id();

				$query2 = "UPDATE setup_detail SET edit_id = $edit_id WHERE id = $insert_id";
				$result2 = Treat_DB_ProxyOld::query($query2);
			}

			/////hack for ordertype in machine_bus_link
			if($id == 17){
				$query2 = "UPDATE {$update[0]} SET ordertype = 1 WHERE id = $edit_id";
				$result2 = Treat_DB_ProxyOld::query($query2);
			}
			elseif($id == 18){
				$query2 = "UPDATE {$update[0]} SET ordertype = 3 WHERE id = $edit_id";
				$result2 = Treat_DB_ProxyOld::query($query2);
			}
			elseif($id == 19){
				$query2 = "UPDATE {$update[0]} SET ordertype = 2 WHERE id = $edit_id";
				$result2 = Treat_DB_ProxyOld::query($query2);
			}

		}
		elseif($mapping[0] != "" && $mapping[1] == 1 && $complete == 1){
			$update = explode(".",$mapping[0]);

			$query2 = "SELECT * FROM {$update[0]} WHERE businessid = $businessid";
			$result2 = Treat_DB_ProxyOld::query($query2);
			$num2 = mysql_num_rows($result2);

			if($num2 > 0){
				$query2 = "UPDATE {$update[0]} SET {$update[1]} = '$value' WHERE businessid = $businessid LIMIT 1";
				$result2 = Treat_DB_ProxyOld::query($query2);
			}
			else{
				$query2 = "INSERT INTO {$update[0]} ({$update[1]},businessid) VALUES ('$value',$businessid)";
				$result2 = Treat_DB_ProxyOld::query($query2);
			}

		}
		elseif($mapping[0] != "" && $mapping[1] == "" && $complete == 1){
			$update = explode(".",$mapping[0]);

			$query2 = "UPDATE {$update[0]} SET {$update[1]} = '$value' WHERE businessid = $businessid";
			$result2 = Treat_DB_ProxyOld::query($query2);
		}

		// Update Vend Machine (Route)
		if ($id == 22 && $complete == 1)
		{
			////machines
			$machines = array(17 => "Food", 18 => "Bev", 19 => "Snack");

			///site name
			$query2 = "SELECT value FROM setup_detail WHERE setupid = 3 AND businessid = $businessid";
			$result2 = Treat_DB_ProxyOld::query($query2);

			$loc_name = Treat_DB_ProxyOld::mysql_result($result2,0,"value");

			foreach ($machines AS $machine => $mach_name)
			{
				$query2 = "SELECT value,complete FROM setup_detail WHERE setupid = $machine AND businessid = $businessid";
				$result2 = Treat_DB_ProxyOld::query($query2);

				$machine_num = Treat_DB_ProxyOld::mysql_result($result2,0,"value");
				$mach_complete = Treat_DB_ProxyOld::mysql_result($result2,0,"complete");

				if ($machine_num != "" && $mach_complete == 1)
				{
					$vend_machine = \EE\Model\VendMachine::get(
						sprintf("machine_num = '%s' AND login_routeid = %d", mysql_real_escape_string($machine_num), $value),
						true
					);

					if (!$vend_machine || (is_object($vend_machine) && !$vend_machine->machineid))
					{
						$new_vend_machine = new \EE\Model\VendMachine;
						$new_vend_machine->machine_num = $machine_num;
						$new_vend_machine->name = "K - $loc_name $mach_name";
						$new_vend_machine->login_routeid = $value;
						$new_vend_machine->save();
					}
				}
			}
		}
		// Update Online CC Info
		else if ($id == 23 && $complete == 1)
		{
			$business_gateway_data = Treat_Model_Business_GatewayData::getById($businessid);

			if(!$business_gateway_data || !is_array($business_gateway_data) || intval($business_gateway_data['businessid']) !== $businessid) {
				$business_data = new Treat_Model_Business_GatewayData;
				$business_data->businessid = $businessid;
				$business_data->store_name = 'COMPANY KITCHEN';
				$business_data->store_number = '1001247941';
				$business_data->store_user_id = '247941';
				$business_data->store_pem_key = '1001247941.pem';
				$business_data->save();
			}
		}
		// Create new Kiosks for manage and fill out client info.
		else if ($id == 41 && $complete == 1 && trim($_POST['val3']) !== '')
		{
			$newKiosksCount = intval($value);
			$kiosks = \EE\Model\ClientPrograms::getKiosksByBusiness($businessid);

			$kioskInfo = null;
			if ( count($kiosks) > 0 )
			{
				foreach ($kiosks as $kiosk)
				{
					if ( $kiosk->parent > 0 )
					{
						$kioskInfo = $kiosk;
						break;
					}
				}
			}

			// Defaults for new kiosks. Might need to be changed later.
			$db_config = \EE\Config::singleton()->getSection( 'db');

			// Check the amount of current kiosks and if it is greater than the
			// current amount of kiosks, then create the new kiosks and fill out
			// the clientprograms info and kiosk schedule info.
			// Currently, do nothing if the new kiosks count is less than the
			// current kiosks. The reason is simply not knowing which kiosk to
			// remove. Also, do not currently track whether the new kiosk
			// created could be safely removed, if not used prior.
			if ( $newKiosksCount > count($kiosks) )
			{
				// Use operator name first to see if there is a match.
				\EE\Model\Company::db( \Treat_DB::singleton('db') );
				\EE\Model\ClientPrograms::db( \Treat_DB::singleton('manage') );
				\EE\Model\ClientSchedule::db( \Treat_DB::singleton('manage') );

				$company = \EE\Model\Company::getByBusinessId($businessid);
				if ( is_object($kioskInfo) && $kioskInfo->parent > 0 )
				{
					$parent = \EE\Model\ClientPrograms::getById($kioskInfo->parent);
				}
				else
				{
					if ( $company->client_program_id < 1 )
					{
						$sanitizedCompanyName = mysql_real_escape_string($company->companyname);

						$parent = new \EE\Model\ClientPrograms;
						$parent->name = $sanitizedCompanyName;
						$parent->businessid = 0;
						$parent->pos_type = 0;
						$parent->save();

						$company->client_program_id = $parent->client_programid;
					}
					else
					{
						$parent = \EE\Model\ClientPrograms::getById($company->client_program_id);
					}
				}

				if ( !is_null($parent) )
				{
					for(
						$createKiosksAmount = $newKiosksCount - count($kiosks), $start = count($kiosks) + 1;
						$createKiosksAmount > 0;
						$createKiosksAmount--, $start++
					)
					{
						// New Kiosk
						$client = new \EE\Model\ClientPrograms;
						$client->businessid = $businessid;
						$client->name = mysql_real_escape_string($_POST['val3']). ' '. $start;
						$client->pos_type = 7;
						$client->skip_client = 1;

						if ($parent instanceof \EE\Model\ClientPrograms)
						{
							$client->parent = intval($parent->client_programid);
						}
						else if (is_array($parent))
						{
							$client->parent = intval($parent['client_programid']);
						}

						if ( $kioskInfo instanceof \EE\Model\ClientPrograms )
						{
							$client->db_ip = $kioskInfo->db_ip ?: $db_config['host'];
							$client->db_name = $kioskInfo->db_name ?: $db_config['database'];
							$client->db_user = $kioskInfo->db_user ?: $db_config['username'];
							$client->db_pass = $kioskInfo->db_pass ?: $db_config['password'];
						}
						else
						{
							$client->db_ip = $db_config['host'];
							$client->db_name = $db_config['database'];
							$client->db_user = $db_config['username'];
							$client->db_pass = $db_config['password'];
						}

						if ( $client->save() )
						{
							// Attempt to get default for business id.
							// Unsure if this should check for current existing
							// kiosks and gather the default from there or simply
							// check that a default doesn't already exist. Well,
							// truthfully, there won't be a default, since the kiosk
							// was just created and no schedule made for the kiosk.
							// Therefore the check will always fail and always
							// create a new default schedule.
							$defaultSchedule = \EE\Model\ClientSchedule::get("client_programid = {$businessid} AND `default` = 1", true);

							if ( ! $defaultSchedule )
							{
								// New Default Kiosk Schedule.
								$schedule = new \EE\Model\ClientSchedule;
								$schedule->client_programid = $client->client_programid;
								$schedule->default = 1;
								$schedule->day = 0;
								$schedule->time = '03:00:00';
								$schedule->starttime = '02:00:00';
								$schedule->endtime = '01:55:00';
								$schedule->num_days = 2;
								$schedule->days_back = 0;
								$schedule->save();
							}
						}
						else
						{
							/** @todo Should probably warn that kiosks were not created. */
						}
					}
				}
			}
		}
	}

	////email new kiosk
	if($all_checks_old == 1 && $all_checks_new == 0)
	{
		////operator name
		$company = \EE\Model\Company::getByBusinessId($businessid);

		$email = "smartin@essentialpos.com,mburris@essentialpos.com,pams@treatamerica.com,sandrab@treatamerica.com,peteb@treatamerica.com,mistyd@treatamerica.com,caseyr@essentialpos.com";
		$email_name = $_POST["val3"];
		$email_subject = "New Kiosk Setup";
		$email_message = "Operator {$company->companyname}: $email_name has been setup and has a ship date of $value.<p>$user_cook";

		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= "From: support@companykitchen.com\r\n";
		mail($email, $email_subject, $email_message, $headers);
	}

	header( 'Location: ' . $_SERVER[ 'HTTP_REFERER' ]);
}
