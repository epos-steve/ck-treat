<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<?php

if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
require_once( dirname(__FILE__) . '/../../application/bootstrap.php' );

$style = "text-decoration:none;";

if ( require_once( realpath( DOC_ROOT . '/../lib/inc/cookie-login.php' ) ) ) {
	$security_level = $GLOBALS['security_level'];
	$loginid = $GLOBALS['loginid'];

	$businessid = intval($_GET['bid']);
	$goto = intval($_GET['goto']);
	$setup_status = intval($_GET['setup_status']);

	if($setup_status < 1) {
		$setup_status = 1;
	}
?>

	<head>
		<link rel="stylesheet" href="css/jq_themes/redmond/jquery-ui-1.7.2.custom.css">
		<script type="text/javascript" src="javascripts/jquery-1.4.2.min.js"></script>
		<script type="text/javascript" src="javascripts/jquery-ui-1.7.2.custom.min.js"></script>

		<script type="text/javascript">$(document).ready(function (){$('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' });});</script>
		<script language="javascript" type="text/javascript">
		function disableForm(theform) {
			if (document.all || document.getElementById) {
				for (i = 0; i < theform.length; i++) {
					var tempobj = theform.elements[i];
					if (tempobj.type.toLowerCase() == "submit" || tempobj.type.toLowerCase() == "reset") {
						tempobj.disabled = true;
					}
				}
			}
			else {
				alert("The form has been submitted.  But, since you're not using IE 4+ or NS 6, the submit button was not disabled on form submission.");
				return false;
			}
		}

		function complete_setup(bid, setup_status) {
			if(confirm('Are you sure you want to mark this as completed?')) {
				$.ajax({
					type: "POST",
					url: "ajax_pos.php",
					data: "bid="+ bid+"& todo=9& setup_status="+ setup_status
				});
			}
		}

		$(function() {
			$('#close-dialog').click(function(event) {
				var goToHome = <?php if($goto == 1) : ?>false<?php else : ?>true<?php endif; ?>;
				if (self.parent) {
					if (goToHome) {
						self.parent.location.href = 'businesstrack.php';
					} else {
						if (self.parent.location.search.match(/popup_bid/)) {
							self.parent.location = self.parent.location.href.replace(/[&]?popup_bid=\d*/i, '');
						} else {
							self.parent.location.reload(true);
						}
					}
				}
			});
		});
		</script>
	</head>
	<body style="margin:0px;">
<?php
	if (isset($_GET['warehoused']) && intval($_GET['warehoused']) > 0) {
		$warehoused = \EE\Model\Business::get('businessid = '.intval($_GET['warehoused']), true);
		$kiosk = \EE\Model\Business::setupKiosk($warehoused->companyid);
		\EE\Model\Kiosk\Deactivation::transferFromWarehouse($warehoused->businessid, $kiosk->businessid);
		$businessid = $kiosk->businessid;
		$companyid = $kiosk->companyid;
		$setup = $kiosk->setup;
	} else {
		$query = "SELECT companyid, setup FROM business WHERE businessid = $businessid";
		$result = Treat_DB_ProxyOld::query($query);

		$companyid = mysql_result($result, 0, "companyid");
		$setup = mysql_result($result, 0, "setup");
	}

	///companyid of kiosk
	$query = "
		SELECT
			setup.*,
			setup_groups.group_name,
			setup_detail.value AS value,
			setup_detail.complete AS complete,
			setup_detail.editby AS editby,
			setup_detail.edittime AS edittime
		FROM setup
		JOIN setup_groups ON setup.groupid = setup_groups.id
		LEFT JOIN setup_detail ON setup.id = setup_detail.setupid AND setup_detail.businessid = $businessid
		WHERE setup.setup_status = $setup_status
		ORDER BY setup_groups.order, setup.groupid, setup.orderid
	";
	$result = Treat_DB_ProxyOld::query($query);

	$setupDetailList = array();
	$lastgroup = 0;
	$notcomplete = 0;
	while($r = mysql_fetch_array($result)) {
		$tempRow = $r;
		$tempRow['show_comp'] = '';
		$tempRow['color'] = '#FFFFFF';

		if($tempRow['edittime'] === '0000-00-00 00:00:00') {
			$tempRow['edittime'] = '';
			$tempRow['editby'] = '';
		}

		if($tempRow['complete'] == 1) {
			$tempRow['show_comp'] = "CHECKED";
			$tempRow['color'] = "#66FF99";
		}
		else if($tempRow['groupid'] == 1) {
			$notcomplete = 1;
		}
		$setupDetailList[$tempRow['groupid']]['name'] = $r['group_name'];
		$setupDetailList[$tempRow['groupid']]['list'][] = $tempRow;
	}
?>

	<b>&nbsp;ID: <?php echo $businessid; ?></b>
<?php
if ($businessid > 0) {
	$transferred = \EE\Model\Kiosk\Deactivation::get(sprintf("`new_business_id` = %d", $businessid), true);
	if (is_object($transferred) && $transferred->id > 0) :
		$warehoused_business = \EE\Model\Business::get('businessid = '.intval($transferred->old_business_id), true);
?>

	<p style="font-weight: bold; color: red; padding: 15px;">This kiosk has been transferred from <?php echo $warehoused_business->businessname; ?>.</p>
<?php
	endif;
}
?>


	<form action="setup_save.php" method="POST" style="margin:0; padding:0; display:inline;" onsubmit="return disableForm(this);">
		<input type="hidden" name="businessid" value="<?php echo $businessid; ?>">
		<input type="hidden" name="setup_status" value="<?php echo $setup_status; ?>">
		<input type="hidden" name="goto" value="<?php echo $goto; ?>">

<?php /* ?>
	<tr>
		<td style="border: 1px solid #999999; background: #E8E7E7;" width="25%">
			<center><b>Task</b></center>
		</td>
		<td style="border: 1px solid #999999; background: #E8E7E7;" width="25%">
			<center><b>Value</b></center>
		</td>
		<td style="border: 1px solid #999999; background: #E8E7E7;" width="25%">
			<center><b>Completed</b></center>
		</td>
		<td style="border: 1px solid #999999; background: #E8E7E7;" width="25%">
			<center><b>Edit By</b></center>
		</td>
	</tr>
<?php //*/ ?>
<?php
	foreach($setupDetailList as $groupid => $group) {
		if($groupid == 4 && $notcomplete == 1) :
?>

		&nbsp;&nbsp;&nbsp;<strong style="color: red;">*This section must be completed before anything is ordered.</strong>
<?php
			break;
		endif;
?>

		<center>
			<fieldset style="width: 95%; border: 1px solid black; background: white;">
				<legend style="background: #FFFF99; border: 1px solid black; padding: 0px 10px; font-weight: bold; font-style: italic; color: #333333;">
					<?php echo $group['name']; ?>
				</legend>
				<table width="99%" cellspacing="0" cellpadding="0">
<?php
		foreach($group['list'] as $setupDetail) {
			extract($setupDetail);
?>

					<tr bgcolor="<?php echo $color; ?>" valign="top">
						<td align="right" width="25%"><?php echo $name; ?>:</td>
						<td align="left">
<?php
		if($type == 2) {
?>

							<textarea name="val<?php echo $id; ?>" rows="4" cols="32"><?php echo $value; ?></textarea>
<?php
		} elseif($type == 3) {
?>

							<input type="text" name="val<?php echo $id; ?>" value="<?php echo $value; ?>" size="40" class="datepicker">
<?php
		} elseif($type == 4) {
?>

							<select name="val<?php echo $id; ?>">
								<option value=""></option>
<?php
			$map_from = explode(".", $map_from);
			$query2 = "SELECT $map_from[1],$map_from[2] FROM $map_from[0]";
			switch(trim($where_query)) {
				case 'companyid':
					$query2 .= " WHERE $where_query = $companyid";
					break;
				case 'none':
					break;
				default:
					$query2 .= " WHERE businessid = $businessid";
			}
			$result2 = Treat_DB_ProxyOld::query($query2);

			while($r2 = mysql_fetch_array($result2)) {
				$val1 = $r2["$map_from[1]"];
				$val2 = $r2["$map_from[2]"];
?>

								<option value="<?php echo $val1; ?>"<?php if($value == $val1) : ?> selected="selected"<?php endif; ?>>
									<?php echo $val2; ?>

								</option>
<?php
			}
?>

							</select>
<?php
		} elseif($type == 5) {
?>

							<input type="checkbox" name="val<?php echo $id; ?>" value="1"<?php if($value == 1) : ?> checked="checked"<?php endif; ?>>
<?php
		} else {
?>

							<input type="text" name="val<?php echo $id; ?>" value="<?php echo $value; ?>" size="40">
<?php
		}
?>

						</td>
						<td width="25%" align="left">
							<input type="hidden" value="1" name="cp<?php echo $id; ?>">
							<input type="hidden" name="old_val<?php echo $id; ?>" value="<?php echo $value; ?>">
							<input type="hidden" name="old_cp<?php echo $id; ?>" value="<?php echo $complete; ?>">
						</td>
						<td width="25%">
							<font size="2"><?php echo $editby; ?> <?php if(isset($edittime) && $edittime !== '') : echo date('m/d/Y g:i A', strtotime($edittime)); endif; ?></font>
						</td>
					</tr>
<?php
		}
?>

				</table>
			</fieldset>
		</center>

		<br>
<?php
	}
?>

		<div style="position: fixed; bottom: 0; right: 5%;">
			<input type="submit"
				   value="Save"<?php if($setup == 0 && $setup_status == 1) : ?> disabled="disabled"<?php endif; ?>>
			&nbsp;
			<input type="button" value="Close" id="close-dialog">
			&nbsp;
			<input type="button" value="Print" onclick="window.print();">
			&nbsp;
<?php
	if($security_level >= 7) :
?>

			<input type="button"
				   value="Complete"<?php
				   if($setup == 0 && $setup_status == 1) : ?>
				   disabled="disabled"<?php
				   endif; ?>
				   onclick="complete_setup(<?php echo $businessid; ?>, <?php echo $setup_status; ?>);">
			&nbsp;
<?php
	endif;

	if($setup == 0) :
?>

			<input type="button"
				   value="<?php if($setup_status == 1) : ?>Next<?php else : ?>Prev<?php endif; ?>"
				   onclick="location.href = 'setup.php?bid=<?php echo $businessid; ?>&setup_status=<?php if($setup_status == 1) : ?>2<?php else : ?>1<?php endif; ?>';">
			&nbsp;
<?php
	endif;
?>

		</div>
	</form>
	<?php google_page_track(); ?>

	</body>
<?php
}
?>

</html>