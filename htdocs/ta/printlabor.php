<?php

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+4);
        $diff=round($diff, 2);
        if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
        elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
        $diff = substr($diff, 0, $dot+3);
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      elseif ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      elseif ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '1')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='31')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;
$leap = date("L");

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   
   elseif ($leap==1&&$month=='03')
   {
      $day=$day+29;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      $day=$day-31;  
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";} 
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}

//include("db.php");
//require('lib/class.db-proxy.php');

if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
require_once( DOC_ROOT . '/bootstrap.php' );

$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";

$user=$_COOKIE["usercook"];
$pass=$_COOKIE["passcook"];
$businessid=$_GET["bid"];
$companyid=$_GET["cid"];
$newdate=$_GET["newdate"];
$date1=$_GET["date1"];
$date2=$_GET["date2"];

if ($businessid==""&&$companyid==""){
   $businessid=$_POST["businessid"];
   $companyid=$_POST["companyid"];
   $date1=$_POST["date1"];
   $date2=$_POST["date2"];
}

if($newdate!=""){$today=$newdate;}
$todayname = dayofweek($today);

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=Treat_DB_ProxyOld::mysql_num_rows($result);

$loginid=@Treat_DB_ProxyOld::mysql_result($result,0,"loginid");
$security_level=@Treat_DB_ProxyOld::mysql_result($result,0,"security_level");
$mysecurity=@Treat_DB_ProxyOld::mysql_result($result,0,"security");
$bid=@Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
$bid2=@Treat_DB_ProxyOld::mysql_result($result,0,"busid2");
$bid3=@Treat_DB_ProxyOld::mysql_result($result,0,"busid3");
$bid4=@Treat_DB_ProxyOld::mysql_result($result,0,"busid4");
$bid5=@Treat_DB_ProxyOld::mysql_result($result,0,"busid5");
$bid6=@Treat_DB_ProxyOld::mysql_result($result,0,"busid6");
$bid7=@Treat_DB_ProxyOld::mysql_result($result,0,"busid7");
$bid8=@Treat_DB_ProxyOld::mysql_result($result,0,"busid8");
$bid9=@Treat_DB_ProxyOld::mysql_result($result,0,"busid9");
$bid10=@Treat_DB_ProxyOld::mysql_result($result,0,"busid10");
$cid=@Treat_DB_ProxyOld::mysql_result($result,0,"companyid");

if ($num != 1 || ($security_level==1 && ($bid != $businessid || $cid != $companyid)) || $user == "" || $pass == "")
{
    echo "<center><h3>Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM company WHERE companyid = '$companyid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $companyname=@Treat_DB_ProxyOld::mysql_result($result,0,"companyname");
    $week_start=@Treat_DB_ProxyOld::mysql_result($result,0,"week_start");
    $week_end=@Treat_DB_ProxyOld::mysql_result($result,0,"week_end");

    if ($date1==""){$date1=$today;}
    if ($date2==""){$date2=$today;}

    $day = date("d");
    $year = date("Y");
    $month = date("m");
    $today2="$year-$month-$day";

    $temp_date=$today2;
    while(dayofweek($temp_date)!=$week_end){$temp_date=prevday($temp_date);}
    $week_end_date=$temp_date;
    $is_start=0;
    while($is_start!=2){
       $temp_date=prevday($temp_date);
       if (dayofweek($temp_date)==$week_start){$is_start++;}
    }
    $week_start_date=$temp_date;

    $showlastperiod="<font size=2>[<a href=buslabor3.php?bid=$businessid&cid=$companyid&date1=$week_start_date&date2=$week_end_date><font color=blue>Last 2 Weeks</font></a>]</font>";

    $query = "SELECT * FROM security WHERE securityid = '$mysecurity'";
    $result = Treat_DB_ProxyOld::query($query);

    $sec_emp_labor=@Treat_DB_ProxyOld::mysql_result($result,0,"emp_labor");
    $sec_bus_sales=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_sales");
    $sec_bus_invoice=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_invoice");
    $sec_bus_order=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_order");
    $sec_bus_payable=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_payable");
    $sec_bus_inventor1=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor1");
    $sec_bus_inventor2=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor2");
    $sec_bus_inventor3=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor3");
    $sec_bus_inventor4=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor4");
    $sec_bus_inventor5=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor5");
    $sec_bus_control=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_control");
    $sec_bus_menu=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_menu");
    $sec_bus_order_setup=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_order_setup");
    $sec_bus_request=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_request");
    $sec_route_collection=@Treat_DB_ProxyOld::mysql_result($result,0,"route_collection");
    $sec_route_labor=@Treat_DB_ProxyOld::mysql_result($result,0,"route_labor");
    $sec_bus_labor=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_labor");

    ////SECURITY LEVELS
    if($sec_bus_labor<=2&&$bid!=$businessid){
       $location="buslabor.php?bid=$businessid&cid=$companyid";
       header('Location: ./' . $location);
    }
    else{}

    if ($sec_emp_labor>0){$showallunits="<font size=2>[<a href=emp_labor.php?bid=$businessid&cid=$companyid><font color=blue>Employee Labor</font></a>]";}

?>

<html>
<head>
<script language="JavaScript" 
   type="text/JavaScript">
function changePage(newLoc)
 {
   nextPage = newLoc.options[newLoc.selectedIndex].value
		
   if (nextPage != "")
   {
      document.location.href = nextPage
   }
 }
</script>

<SCRIPT LANGUAGE="JavaScript" SRC="CalendarPopup.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">document.write(getCalendarStyles());</SCRIPT>

<STYLE>
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation
			{
			background-color:#6677DD;
			text-align:center;
			vertical-align:center;
			text-decoration:none;
			color:#FFFFFF;
			font-weight:bold;
			}
	.TESTcpDayColumnHeader,
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation,
	.TESTcpCurrentMonthDate,
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDate,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDate,
	.TESTcpCurrentDateDisabled,
	.TESTcpTodayText,
	.TESTcpTodayTextDisabled,
	.TESTcpText
			{
			font-family:arial;
			font-size:8pt;
			}
	TD.TESTcpDayColumnHeader
			{
			text-align:right;
			border:solid thin #6677DD;
			border-width:0 0 1 0;
			}
	.TESTcpCurrentMonthDate,
	.TESTcpOtherMonthDate,
	.TESTcpCurrentDate
			{
			text-align:right;
			text-decoration:none;
			}
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDateDisabled
			{
			color:#D0D0D0;
			text-align:right;
			text-decoration:line-through;
			}
	.TESTcpCurrentMonthDate
			{
			color:#6677DD;
			font-weight:bold;
			}
	.TESTcpCurrentDate
			{
			color: #FFFFFF;
			font-weight:bold;
			}
	.TESTcpOtherMonthDate
			{
			color:#808080;
			}
	TD.TESTcpCurrentDate
			{
			color:#FFFFFF;
			background-color: #6677DD;
			border-width:1;
			border:solid thin #000000;
			}
	TD.TESTcpCurrentDateDisabled
			{
			border-width:1;
			border:solid thin #FFAAAA;
			}
	TD.TESTcpTodayText,
	TD.TESTcpTodayTextDisabled
			{
			border:solid thin #6677DD;
			border-width:1 0 0 0;
			}
	A.TESTcpTodayText,
	SPAN.TESTcpTodayTextDisabled
			{
			height:20px;
			}
	A.TESTcpTodayText
			{
			color:#6677DD;
			font-weight:bold;
			}
	SPAN.TESTcpTodayTextDisabled
			{
			color:#D0D0D0;
			}
	.TESTcpBorder
			{
			border:solid thin #6677DD;
			}
</STYLE>

<SCRIPT LANGUAGE=javascript><!--
function caution(){return confirm('THERE ARE UNSUBMITTED DAYS WITHIN THIS DATE RANGE!');}
// --></SCRIPT>

</head>

<?
    //if ($date2==$date1){for($counter=1;$counter<7;$counter++){$date2=nextday($date2);}}

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM business WHERE companyid = '$companyid' AND businessid = '$businessid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $businessname=@Treat_DB_ProxyOld::mysql_result($result,0,"businessname");
    $businessid=@Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
    $street=@Treat_DB_ProxyOld::mysql_result($result,0,"street");
    $city=@Treat_DB_ProxyOld::mysql_result($result,0,"city");
    $state=@Treat_DB_ProxyOld::mysql_result($result,0,"state");
    $zip=@Treat_DB_ProxyOld::mysql_result($result,0,"zip");
    $phone=@Treat_DB_ProxyOld::mysql_result($result,0,"phone");
    $districtid=@Treat_DB_ProxyOld::mysql_result($result,0,"districtid");
    $operate=@Treat_DB_ProxyOld::mysql_result($result,0,"operate");

    echo "<body onLoad=\"window.print();window.close();\">"; 
    echo "<center><h4>$businessname Commissions $date1 through $date2</h4></center>"; 
    echo "<center><table width=100% border=1>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query34 = "SELECT * FROM payroll_code_comm WHERE companyid = '$companyid' ORDER BY orderid DESC";
    $result34 = Treat_DB_ProxyOld::query($query34);
    $num34=Treat_DB_ProxyOld::mysql_num_rows($result34);
    //mysql_close();

    ///////////////HEADER BEGIN DISPLAY
    $showspan=$num34+5;
    echo "<tr><td width=10%><b><font size=2>Empl#</td><td width=15%><b><font size=2>Employee</td><td width=5% align=right><b><font size=2>Rate</td>";

    $temp_num=$num34-1;
    $temp_width=60/($temp_num+1);

    while($temp_num>=0){
       $codeid=@Treat_DB_ProxyOld::mysql_result($result34,$temp_num,"codeid");
       $payroll_code=@Treat_DB_ProxyOld::mysql_result($result34,$temp_num,"code_name");

       echo "<td align=right width=$temp_width%><b><font size=2>$payroll_code</font></td>";
       if ($payroll_code=="REG"){echo "<td align=right width=$temp_width%><b><font size=2>OT</font></td>";}

       $temp_num--;
    }
    echo "<td width=10% align=right><font size=2><b>Total</td></tr>";
    /////////////END HEADER

    $com_total=0;

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM jobtype WHERE companyid = '$companyid' AND commission = '1'";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_num_rows($result);
    //mysql_close();

    $com_total=array();

    $num--;
    while($num>=0){
       $jobtypeid=@Treat_DB_ProxyOld::mysql_result($result,$num,"jobtypeid");
       $jobtypename=@Treat_DB_ProxyOld::mysql_result($result,$num,"name");
       $is_hourly=@Treat_DB_ProxyOld::mysql_result($result,$num,"hourly");

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT * FROM labor_temp_comm WHERE jobtypeid = '$jobtypeid' AND businessid = '$businessid' AND date >= '$date1' AND date <= '$date2' ORDER BY loginid DESC";
       $result2 = Treat_DB_ProxyOld::query($query2);
       $num2=Treat_DB_ProxyOld::mysql_num_rows($result2);
       //mysql_close();

       $codevalue=array();
       $ot=0;
       $totalpay=0;
       $lastloginid=0;
       $num2--;
       while($num2>=-1){
          $tempid=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"tempid");
          $loginid=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"loginid");

          if ($loginid!=$lastloginid&&$lastloginid!=0){
             if($is_hourly==1){$showrate="hr";}
             else{$showrate="Base";}
             echo "<tr><td><font size=2>$empl_no</td><td><font size=2>$lastname,$firstname</td><td align=right><font size=2>$$rate/$showrate</td>";

             $temp_num=$num34-1;
             while($temp_num>=0){
                $codeid=@Treat_DB_ProxyOld::mysql_result($result34,$temp_num,"codeid");
                $code_name=@Treat_DB_ProxyOld::mysql_result($result34,$temp_num,"code_name");
                $hours=@Treat_DB_ProxyOld::mysql_result($result34,$temp_num,"hours");

                echo "<td align=right><font size=2>$codevalue[$codeid]</td>";
                $com_total[$codeid]=$com_total[$codeid]+$codevalue[$codeid];
                if ($code_name=="REG"){echo "<td align=right><font size=2>$ot</td>";$com_total[ot]=$com_total[ot]+$ot;}

                $temp_num--;
             }
             $totalpay=money($totalpay);
             $com_total[total]=$com_total[total]+$totalpay;
             echo "<td align=right><font size=2>$totalpay</td></tr>";

             $com_total2+=$totalpay;

             $codevalue=array();
             $ot=0;
             $totalpay=0;
          }

          $rate=@Treat_DB_ProxyOld::mysql_result($result2,$num2,"rate");

          $rate=money($rate);

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query3 = "SELECT * FROM login WHERE loginid = '$loginid'";
          $result3 = Treat_DB_ProxyOld::query($query3);
          //mysql_close();

          $firstname=@Treat_DB_ProxyOld::mysql_result($result3,0,"firstname");
          $lastname=@Treat_DB_ProxyOld::mysql_result($result3,0,"lastname");
          $empl_no=@Treat_DB_ProxyOld::mysql_result($result3,0,"empl_no");

          $temp_num=$num34-1;
          while($temp_num>=0){
             $codeid=@Treat_DB_ProxyOld::mysql_result($result34,$temp_num,"codeid");
             $code_name=@Treat_DB_ProxyOld::mysql_result($result34,$temp_num,"code_name");
             $hours=@Treat_DB_ProxyOld::mysql_result($result34,$temp_num,"hours");
             $nocharge=@Treat_DB_ProxyOld::mysql_result($result34,$temp_num,"nocharge");

             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query3 = "SELECT * FROM labor_comm WHERE temp_commid = '$tempid' AND coded = '$codeid'";
             $result3 = Treat_DB_ProxyOld::query($query3);
             //mysql_close();

             $value=@Treat_DB_ProxyOld::mysql_result($result3,0,"amount");

             if($code_name=="REG"){
                if($value>40){
                   $totalpay=$totalpay+(40*$rate);
                   $totalpay=$totalpay+(($value-40)*($rate*1.5));

                   $codevalue[$codeid]=$codevalue[$codeid]+40;
                   $ot=$ot+($value-40);
                }
                else{
                   $totalpay=$totalpay+($value*$rate);

                   $codevalue[$codeid]=$codevalue[$codeid]+$value;
                }
             }
             elseif($hours==1){
                if($nocharge!=1){$totalpay=$totalpay+($value*$rate);}

                $codevalue[$codeid]=$codevalue[$codeid]+$value;
             }
             else{
                if($nocharge!=1){$totalpay=$totalpay+$value;}

                $codevalue[$codeid]=$codevalue[$codeid]+$value;
             }

             $temp_num--;
          }

          $lastloginid=$loginid;
          $num2--;
       }

       $myspan=$showspan-1;
       $com_total2=number_format($com_total2,2);
       echo "<tr><td colspan=$myspan><b><font size=2>$jobtypename</font></td><td align=right><b><font size=2>$com_total2</tr>";
       $com_total2=0;
       $num--;
    }

    echo "<tr><td colspan=3></td>";
    $temp_num=$num34-1;
    while($temp_num>=0){
          $codeid=@Treat_DB_ProxyOld::mysql_result($result34,$temp_num,"codeid");
          $code_name=@Treat_DB_ProxyOld::mysql_result($result34,$temp_num,"code_name");
          $hours=@Treat_DB_ProxyOld::mysql_result($result34,$temp_num,"hours");

          if($hours!=1){$com_total[$codeid]=number_format($com_total[$codeid],2);}
          echo "<td align=right><font size=2><b>$com_total[$codeid]</td>";
          if ($code_name=="REG"){echo "<td align=right><font size=2><b>$com_total[ot]</td>";}

          $temp_num--;
    }
    $com_total[total]=number_format($com_total[total],2);
    echo "<td align=right><font size=2><b>$com_total[total]</td></tr>";
    echo "</table></td></tr>";
///////////////////END COMMISSIONS
    echo "<tr><td colspan=10>$showmultipay</td></tr>";

    echo "</table></center>"; 

    //mysql_close(); 
	google_page_track();
}
?>