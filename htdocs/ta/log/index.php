<?php

session_start();

require( '../db.php' );
$auditor = new QueryAuditor( '', 'r');
$verify = 'karoan';

class LogIndexer{
	var $dh;
	var $dir = 'audit';
	var $data = array();

	function __construct(){
		if(!$this->dh = opendir($this->dir))
			echo 'Cannot open dir';
		else{
			$this->readLogs();
			if(count($_POST) > 0)
				$this->xhr();
			else
				$this->check_login();
		}
	}

	function check_login(){
		if(isset($_SESSION['audit_authorized']))
			$this->printGUI();
		else
			echo '<form method="post" action="index.php"><input type="password" name="pw" /><input type="submit" value="Login" /></form>';
	}

	function authorized(){
		if(!$this->dh = opendir($this->dir))
			echo 'Fatal Error:  Cannot read log dir.';
		else{
			$this->printGUI();
		}
	}

	function xhr(){
		global $auditor, $verify;
		
		//Handles login OR AJAX
		if(!isset($_POST['pw'])){
			$file = $_POST['f'];
			$row  = $_POST['r'];

			$auditor->undo( $this, $file, $row);
		}
		else{
			if($_POST['pw'] == $verify)
				$_SESSION['audit_authorized'] = true;
			header("Location: index.php");
		}
	}

	function rewriteLog( $f ){
		$data = $this->data[$f];
		$str = '';
		foreach($data as $obj){
			$str .= json_encode( $obj )."\n";
		}

		$fh = fopen( $this->dir.'/'.$f , 'w') or die("cannot rewrite file");
		fwrite( $fh , $str );
		fclose( $fh );
	}

	function printGUI(){
		$this->page_head();
		echo '<table class="tablesorter">';
		echo '<thead><tr><th>Date</th><th>Log File</th><th>Table</th><th>Action</th><th>Rows Affected</th><th>User</th></tr></thead>';
		echo '<tbody>';
		foreach($this->data as $file => $arr){
			foreach($arr as $line){
				echo '<tr id="'.$file.$line->num.'" title="Click to Undo [Yep, for real]" style="cursor: pointer;" onclick="undoAction(\''.str_replace(array('\'', '"'), array('\\\'', '\\"'), $line->query).'\', \''.$file.'\', \''.$line->num.'\')">';
				echo '<td>'.$line->date.'</td>';
				echo '<td>'.$file.'</td>';
				echo '<td>'.$line->table.'</td>';
				echo '<td>'.$line->action.'</td>';
				echo '<td>'.$line->affected.'</td>';
				echo '<td>'.$line->user.'</td>';
				echo '</tr>';
			}
		}
		echo '</tbody>';
		echo '</table>';
		$this->page_foot();
	}

	function readLogs(){
		while(FALSE !== ( $file = readdir( $this->dh ) )){
			if($file != '.' && $file != '..'){
				$this->data[$file] = array();
				$this->readLog( $file );
			}
		}
	}

	function readLog( $file ){
		$file_path = $this->dir.'/'.$file;
		$fh = fopen($file_path, 'r');
		$line_num = 0;
		while( $line = fgets( $fh ) ){
			$i = count($this->data[$file]);
			$this->data[$file][$i] = json_decode( $line );
			$this->data[$file][$i]->num = $line_num;
		$line_num++;
		}
		fclose( $fh );
	}

	function page_head(){
		echo '<html><head><title>LOG INDEXER</title>';
		echo '<script type="text/javascript" src="../javascripts/jquery-1.3.2.min.js"></script>';
		echo '<script type="text/javascript" src="../javascripts/jquery-ui-1.7.2.custom.min.js"></script>';
		echo '<script type="text/javascript" src="indexer.js"></script>';
		echo '<script type="text/javascript" src="../javascripts/jquery.tablesorter.min.js"></script>';
		echo '<link rel="stylesheet" rev="stylesheet" type="text/css" href="../css/tablesorter/blue/style.css" />';
		echo '</head><body>';
	}

	function page_foot(){
		echo '</body></html>';
	}

	function __destruct(){
		if($this->dh != null)
			closedir($this->dh);
	}
}

$m_cnx = mysql_connect($dbhost, $username, $password);
mysql_select_db($database);
$run = new LogIndexer();
mysql_close( $m_cnx );

?>
