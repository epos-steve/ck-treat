<?php

function audited_query( $q , $file = 'trail' ){
	static $current_open_audits = array();
	if( !isset($current_open_audits[$file]) )
		$current_open_audits[$file] = new QueryAuditor( $file );
	
	return $current_open_audits[$file]->execute( $q );
}

class QueryAuditor{
  var $dir = 'log/audit';
  var $file;
  var $resource;
  var $query;
  var $fh;

  function __construct($file = 'trail' , $mode = 'rw'){
	if( $mode == 'rw' ){
		$this->file = $file;
		$this->file_path = $this->dir.'/'.$file;
		if(!$this->fh = fopen($this->file_path, 'a+')){
		  echo 'Fatal Auditor Error:  Cannot open log for writing';
		}
	}
  }

  function undo( &$indexer , $file , $row ){
	$obj = $indexer->data[$file][$row];
	switch( $obj->action ){
		case 'delete':
			$this->undoDel( $obj );
		break;
		case 'update':
			$this->undoUpd( $obj );
		break;
	}

  	unset($indexer->data[$file][$row]);
	$indexer->rewriteLog( $file );
  }
  
  function undoDel( $o ){
  	foreach($o->before as $before){
		dbI( $o->table , (array)$before );
	}
  }

  function undoUpd( $o ){
	$pk = null;
  	foreach($o->before as $before){
		if($pk == null){
			foreach( $before as $k => $v ){
				$pk = $k;
				break;
			}
		}
		dbU($o->table, (array)$before, $pk);
	}
  }

  function audit( $q ){
  	$words = explode(' ', $q);
	$action = strtolower($words[0]);
	switch($action){
		case 'delete':
			$a = array();
			$p = '/^DELETE FROM ([^\s]*)/i';
			preg_match($p, $q, $matches);
			$a['date'] = date('Y-m-d H:i:s', time());
			$a['query'] = $q;
			$a['table'] = $matches[1];
			$a['action'] = $action;
			$ex = explode("WHERE", $q);

			if(count($ex) < 2)
				$ex = explode('where', $q);

			$a['where'] = (count($ex) > 1) ? $ex[1] : null;
			$a['user'] = $_COOKIE["usercook"];

			$tq = mysql_query("SELECT * FROM {$a['table']}".(($a['where'] != null) ? " WHERE {$a['where']}":""));
			$a['affected'] = mysql_num_rows($tq);
			$a['before'] = array();
			while($a['before'][] = mysql_fetch_array($tq, MYSQL_ASSOC)){
			}
			array_pop( $a['before'] );  // Remove MA's false record

			$this->wlog( json_encode( $a ) );
		break;
		case 'update':
			$a = array();
			$p = '/^UPDATE ([^\s]*)/i';
			preg_match($p, $q, $matches);
			$a['date'] = date('Y-m-d H:i:s', time());
			$a['query'] = $q;
			$a['table'] = $matches[1];
			$a['action'] = $action;

			$ex = explode('SET', $q);
			if(count($ex) < 2)
				$ex = explode('set', $q);

			$set_s = $ex[1];
			$ex = explode('WHERE', $set_s);
			if(count($ex) < 2)
				$ex = explode('where', $q);
			$set_s = $ex[0];
			$where_s = $ex[1];

			$a['where'] = $where_s;
			$a['user'] = $_COOKIE["usercook"];

			//SETS
			$sets = explode(',', $set_s);
			$q_set = array();
			foreach($sets as $set){
				$ex = explode('=', $set);
				$col = trim($ex[0]);
				$val = trim(str_replace(array('\'', '"'), '', $ex[1]));
				$q_set[$col] = $val;
			}
			$a['updated'] = $q_set;

			$tq = mysql_query("SELECT * FROM {$a['table']} WHERE {$a['where']}");
			$a['affected'] = mysql_num_rows($tq);
			$a['before'] = array();
			$pk = null;
			while($tr = mysql_fetch_array($tq, MYSQL_ASSOC)){
				$tx = 0;
				if($pk == null){
					foreach($tr as $col => $val){
						if($tx == 0)
							$pk = $col;
						break;
					}
				}
				$tmp = array();
				$tmp[$pk] = $tr[$pk];
				foreach($q_set as $col => $val){
					$tmp[$col] = $tr[$col];
				}
				
				$a['before'][] = $tmp;
			}

			$this->wlog( json_encode( $a ) );
		break;
	}
  }

  function wlog( $str ){
	fwrite($this->fh, $str."\n");
  }

  function execute( $q , $debug = false ){
    $this->query = $q;
    
	$this->audit( $q );

    if(!$debug)
      $this->resource = mysql_query( $q );
    else
      $this->resource = mysql_query( $q ) or die(mysql_error());

    return $this->resource;
  }

  function __destruct(){
	if( $mode == 'rw' )
		fclose( $this->fh );
  }

}

?>
