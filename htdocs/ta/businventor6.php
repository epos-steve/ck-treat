<?php

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+4);
        $diff=round($diff, 2);
        if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
        elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
        $diff = substr($diff, 0, $dot+3);
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      elseif ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      elseif ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='32')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      $day=$day-31;  
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";} 
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}

define('DOC_ROOT', dirname(dirname(__FILE__)));
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');
$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");
$creditamount="creditamount";
$creditidnty="creditidnty";
$creditdate="creditdate";
$debitamount="debitamount";
$debitidnty="debitidnty";
$debitdate="debitdate";
$quatertotal=0;
$monthtotal=0;
$total=0;
$counter=0;
$findme='.';
$double='.00';
$single='0';
$double2='00';

/*$user=$_COOKIE["usercook"];
$pass=$_COOKIE["passcook"];
$businessid=$_GET["bid"];
$companyid=$_GET["cid"];
$editid=$_GET["editid"];
$view2=$_GET["view2"];
$apinv=$_GET["apinv"];
$editinvarea=$_GET["editinvarea"];
$newvendor=$_GET["newvendor"];
$showhidden=$_GET["showhidden"];*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook');
$businessid = \EE\Controller\Base::getGetVariable('bid');
$companyid = \EE\Controller\Base::getGetVariable('cid');
$editid = \EE\Controller\Base::getGetVariable('editid');
$view2 = \EE\Controller\Base::getGetVariable('view2');
$apinv = \EE\Controller\Base::getGetVariable('apinv');
$editinvarea = \EE\Controller\Base::getGetVariable('editinvarea');
$newvendor = \EE\Controller\Base::getGetVariable('newvendor');
$showhidden = \EE\Controller\Base::getGetVariable('showhidden');

//if ($showhidden==""){$showhidden=$_COOKIE["showhidden"];}
if ($showhidden==""){$showhidden = \EE\Controller\Base::getSessionCookieVariable('showhidden');}
if ($showhidden==""){$showhidden=1;}

if ($businessid==""&&$companyid==""){
   /*$businessid=$_POST["businessid"];
   $companyid=$_POST["companyid"];
   $editid=$_POST["editid"];
   $view2=$_POST["view2"];
   $view=$_POST["view"];
   $date1=$_POST["date1"];
   $date2=$_POST["date2"];
   $findinv=$_POST["findinv"];*/
	
	$businessid = \EE\Controller\Base::getPostVariable('businessid');
	$companyid = \EE\Controller\Base::getPostVariable('companyid');
	$editid = \EE\Controller\Base::getPostVariable('editid');
	$view2 = \EE\Controller\Base::getPostVariable('view2');
	$view = \EE\Controller\Base::getPostVariable('view');
	$date1 = \EE\Controller\Base::getPostVariable('date1');
	$date2 = \EE\Controller\Base::getPostVariable('date2');
	$findinv = \EE\Controller\Base::getPostVariable('findinv');
}

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=Treat_DB_ProxyOld::mysql_numrows($result);
//mysql_close();

$loginid=Treat_DB_ProxyOld::mysql_result($result,0,"loginid");
$security_level=Treat_DB_ProxyOld::mysql_result($result,0,"security_level");
$mysecurity=Treat_DB_ProxyOld::mysql_result($result,0,"security");
$bid=Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
$bid2=Treat_DB_ProxyOld::mysql_result($result,0,"busid2");
$bid3=Treat_DB_ProxyOld::mysql_result($result,0,"busid3");
$bid4=Treat_DB_ProxyOld::mysql_result($result,0,"busid4");
$bid5=Treat_DB_ProxyOld::mysql_result($result,0,"busid5");
$bid6=Treat_DB_ProxyOld::mysql_result($result,0,"busid6");
$bid7=Treat_DB_ProxyOld::mysql_result($result,0,"busid7");
$bid8=Treat_DB_ProxyOld::mysql_result($result,0,"busid8");
$bid9=Treat_DB_ProxyOld::mysql_result($result,0,"busid9");
$bid10=Treat_DB_ProxyOld::mysql_result($result,0,"busid10");
$cid=Treat_DB_ProxyOld::mysql_result($result,0,"companyid");

$pr1=Treat_DB_ProxyOld::mysql_result($result,0,"payroll");
$pr2=Treat_DB_ProxyOld::mysql_result($result,0,"pr2");
$pr3=Treat_DB_ProxyOld::mysql_result($result,0,"pr3");
$pr4=Treat_DB_ProxyOld::mysql_result($result,0,"pr4");
$pr5=Treat_DB_ProxyOld::mysql_result($result,0,"pr5");
$pr6=Treat_DB_ProxyOld::mysql_result($result,0,"pr6");
$pr7=Treat_DB_ProxyOld::mysql_result($result,0,"pr7");
$pr8=Treat_DB_ProxyOld::mysql_result($result,0,"pr8");
$pr9=Treat_DB_ProxyOld::mysql_result($result,0,"pr9");
$pr10=Treat_DB_ProxyOld::mysql_result($result,0,"pr10");

if ($num != 1 || ($security_level==1 && ($bid != $businessid || $cid != $companyid)) || $user == "" || $pass == "")
{
    echo "<center><h3>Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
    setcookie("showhidden",$showhidden);
?>
<head>

<script type="text/javascript" src="javascripts/prototype.js"></script>
<script type="text/javascript" src="javascripts/scriptaculous.js"></script>

<?
echo "
<script type=\"text/javascript\">
window.onload = function(){
   $('autoBox').blur();
   var auto = new Form.AutoComplete('autoBox', {
            dbTable: 'inv_items',
            dbDisp: 'item_name',
            dbVal: 'inv_itemid',
            dbWhere: \"businessid = '$businessid' ORDER BY item_name\",
            watermark: '(Select an Item)',
            showOnEmpty: true});
	    auto.onchange = function(){ auto.input.form.submit(); }

}
</script>
";
?>

<style type="text/css">
	.matched {background-color: #ccf; font-weight: bold;}
	.keyHover {background-color: #39f; color: white; border: 1px dotted red;}
	.mouseHover {background-color: #39f; border: 1px dotted red; color: white;}

	#autoBox{background: url('images/down_arrow.gif') no-repeat center right; background-color: white; border: 1px solid #1b1b1b;}
	#autoBox.hover{background: url('images/down_arrow_hover.gif') no-repeat center right; background-color: white; border: 1px solid #3c7fb1;}
	
	.dispVal{font-weight: bold; color: green; text-decoration: underline;}
</style>

<STYLE type="text/css">
OPTION.mar{background-color:yellow}
</STYLE>

<script language="JavaScript"
   type="text/JavaScript">
function popup(url)
  {
     popwin=window.open(url,"Converter","location=no,menubar=no,titlebar=no,resizeable=no,height=175,width=350");   
  }
</script>

<SCRIPT LANGUAGE=javascript><!--
function delconfirm(){return confirm('Are you sure you want to delete this area?\nThis is not recommended if there are items assigned to this area.');}
// --></SCRIPT>

<SCRIPT LANGUAGE=javascript><!--
function delconfirm2(){return confirm('Are you sure you want to delete this item?');}
// --></SCRIPT>

<SCRIPT LANGUAGE=javascript><!--
function delconfirm3(){return confirm('WARNING! If there are counts for this area and you delete it, your totals will be off.');}
// --></SCRIPT>

<SCRIPT LANGUAGE=javascript><!--
function delconfirm7(){return confirm('Are you sure you want to update these items?');}
// --></SCRIPT>

<script language="JavaScript" 
   type="text/JavaScript">
function changePage(newLoc)
 {
   nextPage = newLoc.options[newLoc.selectedIndex].value
		
   if (nextPage != "")
   {
      document.location.href = nextPage
   }
 }
</script>

<SCRIPT LANGUAGE="JavaScript">
<!-- Web Site:  http://dynamicdrive.com -->

<!-- This script and many more are available free online at -->
<!-- The JavaScript Source!! http://javascript.internet.com -->

<!-- Begin
function disableForm(theform) {
if (document.all || document.getElementById) {
for (i = 0; i < theform.length; i++) {
var tempobj = theform.elements[i];
if (tempobj.type.toLowerCase() == "submit" || tempobj.type.toLowerCase() == "reset")
tempobj.disabled = true;
}

}
else {
alert("The form has been submitted.  But, since you're not using IE 4+ or NS 6, the submit button was not disabled on form submission.");
return false;
   }
}
//  End -->
</script>

</head>
<?php

    if ($apinv!=""){echo "<body onLoad='focus();invitem5.itemname.focus()'>";}
    else {echo "<body>";} 

    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%><tr><td colspan=2>";
    echo "<a name=top><a href=businesstrack.php><img src=logo.jpg border=0 height=43 width=205></a></a><p></td></tr>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM company WHERE companyid = '$companyid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $companyname=mysql_result($result,0,"companyname");

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM security WHERE securityid = '$mysecurity'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $sec_item_list=Treat_DB_ProxyOld::mysql_result($result,0,"item_list");
    $sec_bus_sales=Treat_DB_ProxyOld::mysql_result($result,0,"bus_sales");
    $sec_bus_invoice=Treat_DB_ProxyOld::mysql_result($result,0,"bus_invoice");
    $sec_bus_order=Treat_DB_ProxyOld::mysql_result($result,0,"bus_order");
    $sec_bus_payable=Treat_DB_ProxyOld::mysql_result($result,0,"bus_payable");
    $sec_bus_inventor1=Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor1");
    $sec_bus_inventor2=Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor2");
    $sec_bus_inventor3=Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor3");
    $sec_bus_inventor4=Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor4");
    $sec_bus_inventor5=Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor5");
    $sec_bus_control=Treat_DB_ProxyOld::mysql_result($result,0,"bus_control");
    $sec_bus_menu=Treat_DB_ProxyOld::mysql_result($result,0,"bus_menu");
    $sec_bus_order_setup=Treat_DB_ProxyOld::mysql_result($result,0,"bus_order_setup");
    $sec_bus_request=Treat_DB_ProxyOld::mysql_result($result,0,"bus_request");
    $sec_route_collection=Treat_DB_ProxyOld::mysql_result($result,0,"route_collection");
    $sec_route_labor=Treat_DB_ProxyOld::mysql_result($result,0,"route_labor");
    $sec_route_detail=Treat_DB_ProxyOld::mysql_result($result,0,"route_detail");
    $sec_bus_labor=Treat_DB_ProxyOld::mysql_result($result,0,"bus_labor");
	$sec_bus_budget=Treat_DB_ProxyOld::mysql_result($result,0,"bus_budget");

    $security_level=$sec_bus_inventor1;

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM business WHERE companyid = '$companyid' AND businessid = '$businessid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $businessname=Treat_DB_ProxyOld::mysql_result($result,0,"businessname");
    $businessid=Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
    $street=Treat_DB_ProxyOld::mysql_result($result,0,"street");
    $city=Treat_DB_ProxyOld::mysql_result($result,0,"city");
    $state=Treat_DB_ProxyOld::mysql_result($result,0,"state");
    $zip=Treat_DB_ProxyOld::mysql_result($result,0,"zip");
    $phone=Treat_DB_ProxyOld::mysql_result($result,0,"phone");
    $districtid=Treat_DB_ProxyOld::mysql_result($result,0,"districtid");
    $show_inv_price=Treat_DB_ProxyOld::mysql_result($result,0,"show_inv_price");
    $show_inv_count=Treat_DB_ProxyOld::mysql_result($result,0,"show_inv_count");
    $show_last_count=Treat_DB_ProxyOld::mysql_result($result,0,"show_last_count");
    $count_type=Treat_DB_ProxyOld::mysql_result($result,0,"count_type");
    $contract=Treat_DB_ProxyOld::mysql_result($result,0,"contract");
    $bus_unit=Treat_DB_ProxyOld::mysql_result($result,0,"unit");

    //echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";
    echo "<tr bgcolor=#CCCCFF><td width=1%><img src=weblogo.jpg width=80 height=75></td><td><font size=4><b>$businessname #$bus_unit</b></font><br>$street<br>$city, $state $zip<br>$phone</td>";
    echo "<td align=right valign=top><br>";
    echo "<FORM ACTION='editbus.php' method=post name='store'>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<input type=hidden name=businessid value=$businessid>";
    echo "<input type=hidden name=companyid value=$companyid>";
    echo "<INPUT TYPE=submit VALUE=' Store Setup '> ";
    echo "</form></td></tr>";
    //echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";
    
    echo "<tr><td colspan=3><font color=#999999>";
    if ($sec_bus_sales>0){echo "<a href=busdetail.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Sales</font></a>";}
    if ($sec_bus_invoice>0){echo " | <a href=businvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Invoicing</font></a>";}
    if ($sec_bus_order>0){echo " | <a href=buscatertrack.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Orders</font></a>";}
    if ($sec_bus_payable>0){echo " | <a href=busapinvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Payables</font></a>";}
    if ($sec_bus_inventor1>0){echo " | <a href=businventor.php?bid=$businessid&cid=$companyid><font color=red onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='red' style='text-decoration:none'>Inventory</font></a>";}
    if ($sec_bus_control>0){echo " | <a href=buscontrol.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Control Sheet</font></a>";}
    if ($sec_bus_menu>0){echo " | <a href=buscatermenu.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Menus</font></a>";}
    if ($sec_bus_order_setup>0){echo " | <a href=buscaterroom.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Order Setup</font></a>";}
    if ($sec_bus_request>0){echo " | <a href=busrequest.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Requests</font></a>";}
    if ($sec_route_collection>0||$sec_route_labor>0||$sec_route_detail>0){echo " | <a href=busroute_collect.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Route Collections</font></a>";}
    if ($sec_bus_labor>0){echo " | <a href=buslabor.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Labor</font></a>";}
    if ($sec_bus_budget>0||($businessid == $bid && $pr1 == 1)||($businessid == $bid2 && $pr2 == 1)||($businessid == $bid3 && $pr3 == 1)||($businessid == $bid4 && $pr4 == 1)||($businessid == $bid5 && $pr5 == 1)||($businessid == $bid6 && $pr6 == 1)||($businessid == $bid7 && $pr7 == 1)||($businessid == $bid8 && $pr8 == 1)||($businessid == $bid9 && $pr9 == 1)||($businessid == $bid10 && $pr10 == 1)){echo " | <a href=busbudget.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Budget</font></a>";}
	echo "</td></tr>";

    echo "</table></center><p>";

if ($security_level>0){
    echo "<center><table width=90%><tr><td><form action=businventor3.php method=post>";
    echo "<select name=gobus onChange='changePage(this.form.gobus)'>";

    $districtquery="";
    if ($security_level>2&&$security_level<7){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM login_district WHERE loginid = '$loginid'";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();

       $num--;
       while($num>=0){
          $districtid=Treat_DB_ProxyOld::mysql_result($result,$num,"districtid");
          if($districtquery==""){$districtquery="districtid = '$districtid'";}
          else{$districtquery="$districtquery OR districtid = '$districtid'";}
          $num--;
       }
    }

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    if($security_level>6){$query = "SELECT * FROM business WHERE companyid = '$companyid' ORDER BY businessname DESC";}
    elseif($security_level==2||$security_level==1){$query = "SELECT * FROM business WHERE companyid = '$companyid' AND (businessid = '$bid' OR businessid = '$bid2' OR businessid = '$bid3' OR businessid = '$bid4' OR businessid = '$bid5' OR businessid = '$bid6' OR businessid = '$bid7' OR businessid = '$bid8' OR businessid = '$bid9' OR businessid = '$bid10') ORDER BY businessname DESC";}
    elseif($security_level>2&&$security_level<7){$query = "SELECT * FROM business WHERE companyid = '$companyid' AND ($districtquery) ORDER BY businessname DESC";}
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    $num--;
    while($num>=0){
       $businessname=Treat_DB_ProxyOld::mysql_result($result,$num,"businessname");
       $busid=Treat_DB_ProxyOld::mysql_result($result,$num,"businessid");

       if ($busid==$businessid){$show="SELECTED";}
       else{$show="";}

       echo "<option value=businventor6.php?bid=$busid&cid=$companyid $show>$businessname</option>";
       $num--;
    }
 
    echo "</select></td><td></form></td></tr></table></center><p>";
}
    $security_level=$sec_bus_inventor1;

    $year=date("Y");
    $month=date("n");
    $day=date("j");
 
    $today="$year-$month-$day";

    if ($sec_bus_inventor2>0){$businventor2="businventor2.php";}
    else{$businventor2="businesstrack.php";}
    if ($sec_bus_inventor3>0){$businventor3="businventor3.php";}
    else{$businventor3="businesstrack.php";}
    if ($sec_bus_inventor4>0){$businventor4="businventor4.php";}
    else{$businventor4="businesstrack.php";}
    if ($sec_bus_inventor5>0){$businventor5="businventor5.php";}
    else{$businventor5="businesstrack.php";}

    echo "<center><table width=90% cellspacing=0 cellpadding=0>";

    echo "<tr bgcolor=#CCCCCC valign=top><td width=1%><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=15%><center><a href=businventor.php?cid=$companyid&bid=$businessid style=$style><font color=blue>Inventory $'s</a></center></td><td width=1% align=right><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1%><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=15%><center><a href=businventor2.php?cid=$companyid&bid=$businessid style=$style style=$style><font color=blue>Inventory Count</a></center></td><td width=1% align=right><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1% bgcolor=#E8E7E7><img src=leftcrn.jpg height=6 width=6 align=top></td><td bgcolor=#E8E7E7 width=15%><center><a href=businventor3.php?cid=$companyid&bid=$businessid style=$style><font color=black>Inventory Setup</a></center></td><td bgcolor=#E8E7E7 width=1% align=right><img src=rightcrn.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1%><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=15%><center><a href=businventor4.php?cid=$companyid&bid=$businessid style=$style style=$style><font color=blue>Recipes</a></center></td><td width=1% align=right><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1%><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=15%><center><a href=businventor5.php?cid=$companyid&bid=$businessid style=$style style=$style><font color=blue>Vendors/PO's</a></center></td><td width=1% align=right><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td bgcolor=white width=25%></td></tr>";

    echo "<tr height=2><td colspan=4></td><td colspan=4></td><td colspan=3 bgcolor=#E8E7E7></td><td></td><td colspan=4></td><td colspan=4></td><td colspan=1></td></tr>";

    echo "</table></center><center><table width=90% cellspacing=0 cellpadding=0>";

    echo "<tr bgcolor=#E8E7E7 height=24 valign=bottom><td colspan=3><a href=businventor3.php?bid=$businessid&cid=$companyid style=$style><font color=blue>Inventory Items</font></a> | <a href=businventor3.php?bid=$businessid&cid=$companyid#item style=$style><font color=blue>Count Areas & Count Sheet Options</font></a> | <b>Update Pricing</b></td>";

    echo "<td align=right colspan=3 bgcolor=#E8E7E7>";
    echo "</td><td bgcolor=#E8E7E7></td></tr>";

    echo "<tr><td colspan=9 height=1 bgcolor=black><form action=inv_manage_cost.php method=post onSubmit='return disableForm(this);'><input type=hidden name=businessid value=$businessid><input type=hidden name=companyid value=$companyid></td></tr></table></center>";
    
//////BEGIN
    echo "<center><table width=90% cellspacing=0 cellpadding=0>";
  
    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    if($showhidden==0){$query = "SELECT * FROM inv_items WHERE businessid = '$businessid' ORDER BY apaccountid DESC,item_name";}
    elseif($showhidden==1){$query = "SELECT * FROM inv_items WHERE businessid = '$businessid' AND active = '0' ORDER BY apaccountid DESC,item_name";}
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    $inv_counter=0;
    $showcolor="white";
    $lastapaccountid=-1;
    $lastvendor=-1;
    $num--;
    while($r=Treat_DB_ProxyOld::mysql_fetch_array($result)){
       $inv_itemid=$r["inv_itemid"];
       $item_name=$r["item_name"];
       $item_code=$r["item_code"];
       $inv_areaid=$r["inv_areaid"];
       $apaccountid=$r["apaccountid"];
       $units=$r["units"];
       $price=$r["price"];
       $vendor=$r["vendor"];
       $price_date=$r["price_date"];
       $active=$r["active"];
       $pack_size=$r["pack_size"];
       $v_update=$r["v_update"];

       if($v_update==1){$fontcolor="blue";}
       else{$fontcolor="black";}

       $day=substr($price_date,8,2);
       $month=substr($price_date,5,2);
       $year=substr($price_date,2,2);
       if ($lastdate>$price_date){$showprice="<sup><font color=gray size=2>$month/$day/$year</font></sup>";}
       else{$showprice="<sup><font color=gray size=2>$month/$day/$year</font></sup>";}

       if($item_code=="0"){$item_code="";}
       if ($active==1){$item_name="<strike>$item_name</strike>";}

       if($pack_size!=""){$units=$pack_size;}

       if ($lastapaccount!=$apaccountid){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT acct_name FROM acct_payable WHERE acct_payableid = '$apaccountid'";
       $result2 = Treat_DB_ProxyOld::query($query2);
       //mysql_close();

       $acct_name=Treat_DB_ProxyOld::mysql_result($result2,0,"acct_name");
       }
       $acct_name=substr($acct_name,0,15);
       if ($acct_name==""){$acct_name="<img src=error.gif height=16 width=20 alt='No Account Defined'>";}

       $num44=0;

       if ($lastvendor!=$vendor){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query2 = "SELECT vendor_name FROM vendor_master WHERE vendor_master_id = '$vendor'";
       $result2 = Treat_DB_ProxyOld::query($query2);
       //mysql_close();

       
       $vendor_name=Treat_DB_ProxyOld::mysql_result($result2,0,"vendor_name");
       $myvendor=$vendor_name;
       }
       $vendor_name=substr($vendor_name,0,15);
       if ($vendor_name==""){$vendor_name="<img src=error.gif height=16 width=20 alt='No Vendor Defined'>";}

       if ($lastapaccountid!=$apaccountid){
             if ($showcolor=="#F3F3F3"){$showcolor="white";}
             elseif ($showcolor=="white"){$showcolor="#F3F3F3";}
             $showcolor2=$showcolor;
       }

       $price=money($price);
       echo "<tr bgcolor='$showcolor' onMouseOver=this.bgColor='#999999' onMouseOut=this.bgColor='$showcolor' id='row$inv_counter'><td><font size=2 color=$fontcolor>$item_code</font></td><td>$shownoareas <font size=2 color=$fontcolor>$item_name</td><td align=right><font size=2 color=$fontcolor>$<input type=text size=4 name='$inv_itemid' value='$price' STYLE='font-size: 10px;' onfocus='this.select();' onchange=\"row$inv_counter.style.backgroundColor='yellow'\"></td><td><font size=2 color=$fontcolor>&nbsp; $units$showprice</td><td></td><td><font size=2 color=$fontcolor>$acct_name</td><td><font size=2 color=$fontcolor>$vendor_name</td></tr>";
       echo "<tr bgcolor=#CCCCCC><td height=1 colspan=7></td></tr>";

       $showcolor=$showcolor2;
       $lastapaccountid=$apaccountid;
       $vendor_name=$myvendor;
       $lastvendor=$vendor;
       $num--;
       $inv_counter++;
    }
  
    echo "<tr bgcolor=#E8E7E7><td colspan=10 align=right><input type=submit value='Save'></td></tr>";
    echo "<tr bgcolor=white><td height=0 colspan=10></form></td></tr>";
    echo "</table></center><br>";


    echo "<br><FORM ACTION=businesstrack.php method=post>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<center><INPUT TYPE=submit VALUE=' Return to Main Page'></FORM></center></body>";
	
	google_page_track();
}
//mysql_close();
?>
