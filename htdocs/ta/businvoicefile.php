<?php

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+3);
        if ($diff > 0 && $diff < '0.01'){$diff="0.01";}
        elseif($diff < 0 && $diff > '-0.01'){$diff="-0.01";}
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      elseif ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      elseif ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '1')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='31')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;
$leap = date("L");

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   
   elseif ($leap==1&&$month=='03')
   {
      $day=$day+29;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      $day=$day-31;  
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";} 
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";} 
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}


if ( !defined('DOC_ROOT'))
{
	define('DOC_ROOT', dirname(dirname(__FILE__)));
}
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');

$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");

/*$user = isset($_COOKIE["usercook"])?$_COOKIE["usercook"]:'';
$pass = isset($_COOKIE["passcook"])?$_COOKIE["passcook"]:'';
$view = isset($_COOKIE["viewcook"])?$_COOKIE["viewcook"]:'';
$date1 = isset($_COOKIE["date1cook"])?$_COOKIE["date1cook"]:'';
$date2 = isset($_COOKIE["date2cook"])?$_COOKIE["date2cook"]:'';
$aged = isset($_COOKIE["aged"])?$_COOKIE["aged"]:'';
$businessid = isset($_GET["bid"])?$_GET["bid"]:'';
$companyid = isset($_GET["cid"])?$_GET["cid"]:'';*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook','');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook','');
$view = \EE\Controller\Base::getSessionCookieVariable('viewcook','');
$date1 = \EE\Controller\Base::getSessionCookieVariable('date1cook','');
$date2 = \EE\Controller\Base::getSessionCookieVariable('date2cook','');
$aged = \EE\Controller\Base::getSessionCookieVariable('aged','');
$businessid = \EE\Controller\Base::getGetVariable('bid','');
$companyid = \EE\Controller\Base::getGetVariable('cid','');

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=Treat_DB_ProxyOld::mysql_numrows($result);

$security_level=@Treat_DB_ProxyOld::mysql_result($result,0,"security_level");
$mysecurity=@Treat_DB_ProxyOld::mysql_result($result,0,"security");
$bid=@Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
$cid=@Treat_DB_ProxyOld::mysql_result($result,0,"companyid");
$loginid=@Treat_DB_ProxyOld::mysql_result($result,0,"loginid");

if ($num != 1)
{
    //echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM company WHERE companyid = '$companyid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $companyname=@Treat_DB_ProxyOld::mysql_result($result,0,"companyname");

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM business WHERE businessid = '$businessid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $businessname=@Treat_DB_ProxyOld::mysql_result($result,0,"businessname");
    $businessid=@Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
    $street=@Treat_DB_ProxyOld::mysql_result($result,0,"street");
    $city=@Treat_DB_ProxyOld::mysql_result($result,0,"city");
    $state=@Treat_DB_ProxyOld::mysql_result($result,0,"state");
    $zip=@Treat_DB_ProxyOld::mysql_result($result,0,"zip");
    $phone=@Treat_DB_ProxyOld::mysql_result($result,0,"phone");
    $contract=@Treat_DB_ProxyOld::mysql_result($result,0,"contract");

    if ($aged==1){
       $query = "SELECT * FROM invoice WHERE businessid = '$businessid' AND status = '4' ORDER BY date DESC";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
    }
    elseif ($findinv!=""){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM invoice WHERE businessid = '$businessid' AND invoiceid = '$findinv'";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();
    }
    elseif ($view=="All"){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM invoice WHERE businessid = '$businessid' AND date >= '$date1' AND date <= '$date2' ORDER BY date";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();
    }
    elseif ($view=="Today"){
       $c1="SELECTED";
       $date1=$today;
       $date2=$today;
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM invoice WHERE businessid = '$businessid' AND date = '$today' AND (status ='2' OR status = '4') ORDER BY date";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();
    }
    elseif ($view=="Yesterday"){
       $c2="SELECTED";
       $yesterday=pastday(1);
       $date1=$yesterday;
       $date2=$yesterday;
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM invoice WHERE businessid = '$businessid' AND date = '$yesterday' AND (status ='2' OR status = '4') ORDER BY date";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();
    }
    elseif ($view=="PP"){
       $c3="SELECTED";
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM invoice WHERE businessid = '$businessid' AND date >= '$date1' AND date <= '$date2' AND (status ='2' OR status = '4') ORDER BY date";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();
    }
    else{
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM invoice WHERE businessid = '$businessid' AND date >= '$date1' AND date <= '$date2' AND status = '$view' ORDER BY date";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();
    }


    $data="Invoice#,Date,Account,Acct#,Contact,Phone,Customers,Cost Center,PO#,Status,Taxes,Total,Reference";

    ////custom fields
    $query_custom = "SELECT * FROM invoice_custom_fields WHERE businessid = '$businessid' AND active = '0'";
    $result_custom = Treat_DB_ProxyOld::query($query_custom);

    while($r_cust=Treat_DB_ProxyOld::mysql_fetch_array($result_custom)){
        $cust_id=$r_cust["id"];
        $cust_field_name=$r_cust["field_name"];
        $cust_type=$r_cust["type"];

        $data.=",$cust_field_name";
    }
    $data.="\r\n";
    /////end custom field headers

    $grandtotal=0;
    $results=$num;
    $num--;
    $counter=1;
    while ($num>=0){
          $invoiceid=@Treat_DB_ProxyOld::mysql_result($result,$num,"invoiceid");
          $accountid=@Treat_DB_ProxyOld::mysql_result($result,$num,"accountid");
          $invoicedate=@Treat_DB_ProxyOld::mysql_result($result,$num,"date");
          $type=@Treat_DB_ProxyOld::mysql_result($result,$num,"type");
          $status=@Treat_DB_ProxyOld::mysql_result($result,$num,"status");
          $consolidate=@Treat_DB_ProxyOld::mysql_result($result,$num,"consolidate");
          $total=@Treat_DB_ProxyOld::mysql_result($result,$num,"total");
          $taxtotal=@Treat_DB_ProxyOld::mysql_result($result,$num,"taxtotal");
          $reference=@Treat_DB_ProxyOld::mysql_result($result,$num,"reference");
          $contact=@Treat_DB_ProxyOld::mysql_result($result,$num,"contact");
          $contact_phone=@Treat_DB_ProxyOld::mysql_result($result,$num,"contact_phone");
          $costcenter=@Treat_DB_ProxyOld::mysql_result($result,$num,"costcenter");
          $ponum=@Treat_DB_ProxyOld::mysql_result($result,$num,"ponum");
          $people=@Treat_DB_ProxyOld::mysql_result($result,$num,"people");

          $total=money($total);
          $taxtotal=money($taxtotal);

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query2 = "SELECT name,accountnum FROM accounts WHERE accountid = '$accountid'";
          $result2 = Treat_DB_ProxyOld::query($query2);
          //mysql_close();

          $accountname=@Treat_DB_ProxyOld::mysql_result($result2,0,"name");
          $accountnum=@Treat_DB_ProxyOld::mysql_result($result2,0,"accountnum");

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query3 = "SELECT typename FROM invtype WHERE typeid = '$type'";
          $result3 = Treat_DB_ProxyOld::query($query3);
          //mysql_close();

          $typename=@Treat_DB_ProxyOld::mysql_result($result3,0,"typename");

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query4 = "SELECT statusname FROM invstatus WHERE statusid = '$status'";
          $result4 = Treat_DB_ProxyOld::query($query4);
          //mysql_close();

          $statusname=@Treat_DB_ProxyOld::mysql_result($result4,0,"statusname");

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query5 = "SELECT type FROM audit_invoice WHERE invoiceid = '$invoiceid' AND type = '1'";
          $result5 = Treat_DB_ProxyOld::query($query5);
          $num5=Treat_DB_ProxyOld::mysql_numrows($result5);
          //mysql_close();

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query6 = "SELECT type FROM audit_invoice WHERE invoiceid = '$invoiceid' AND type = '0'";
          $result6 = Treat_DB_ProxyOld::query($query6);
          $num6=Treat_DB_ProxyOld::mysql_numrows($result6);
          //mysql_close();

          $data.="$invoiceid,$invoicedate,\"$accountname\",$accountnum,$contact,$contact_phone,$people,$costcenter,$ponum,$statusname,$taxtotal,$total,$reference";

          ////custom fields
            $query_custom = "SELECT * FROM invoice_custom_fields WHERE businessid = '$businessid' AND active = '0'";
            $result_custom = Treat_DB_ProxyOld::query($query_custom);

            while($r_cust=Treat_DB_ProxyOld::mysql_fetch_array($result_custom)){
                $cust_id=$r_cust["id"];
                $cust_field_name=$r_cust["field_name"];
                $cust_type=$r_cust["type"];

                $query_custom2 = "SELECT value FROM invoice_custom_values WHERE invoiceid = '$invoiceid' AND customid = '$cust_id'";
                $result_custom2 = Treat_DB_ProxyOld::query($query_custom2);

                $cust_value = @Treat_DB_ProxyOld::mysql_result($result_custom2,0,"value");

                //////display $'s per person
                if($cust_type==3){
                    $cust_value=number_format($total/$people, 2);
                    $cust_value="$$cust_value per person";
                }

                $data.=",$cust_value";
            }

            $data.="\r\n";

          $grandtotal=$grandtotal+$total;
          $num--;
          $counter++;
    }
    $totalcount=$counter-1;

$file="$businessname-$date1-$date2.csv";
// Output the headers to download the file
header("Content-type: application/x-msdownload");
header("Content-Disposition: attachment; filename=$file");
header("Pragma: no-cache");
header("Expires: 100");
echo $data;
}
?>