<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');
error_reporting(0);

ini_set("display_errors","true");

date_default_timezone_set('America/Chicago');
$date_of_file = date('Y-m-d-h-i-s-a', time());

$last_inv = 0;

$myFile = DIR_FTP_UPLOADS."/TreatAmerica/VendingAR/2106CSV.csv";
$handle = fopen($myFile, 'r');
$newfile = DIR_FTP_UPLOADS."/TreatAmerica/VendingAR/2106CSV-" . $date_of_file . ".csv";
copy($myFile, $newfile);
while ($pieces = fgetcsv($handle,NULL,","))
{
	$cur_invoice = $pieces[4];

	////create invoice
	if($last_inv != $cur_invoice){
		////update invoice total
		if($last_inv != 0){
			////adjust for tax rounding
			if($cur_total != $inv_total){
				$diff = round($cur_total - $inv_total,2);

				$query = "INSERT INTO invoicedetail (invoiceid,qty,item,price,item_code)
					VALUES
					($invoiceid,1,'Tax Adjustment','$diff','')";
				$result = Treat_DB_ProxyOld::query($query);
			}

			$query = "UPDATE invoice SET total = '$cur_total', posted = 1 WHERE invoiceid = $invoiceid";
			$result = Treat_DB_ProxyOld::query($query);

			///update credit/debit totals
			$credits[$AR_creditid][$cur_date] += $cur_total;
			$debits[$AR_debitid][$cur_date] += $cur_total;
		}

		$cur_area = $pieces[0];

		///businessid
		if($cur_area == 10000){
			$cur_bid = 97;
			$cur_cid = 3;
			$cur_lid = 1;
		}
		elseif($cur_area == 20000){$cur_bid = 106;$cur_cid = 3;$cur_lid = 2;}
		elseif($cur_area == 40000){$cur_bid = 106;$cur_cid = 3;$cur_lid = 3;}
		elseif($cur_area >= 70000 && $cur_area < 80000 ){
			$cur_bid = 151;
			$cur_cid = 6;
			$cur_lid = 9;
		}
		elseif($cur_area == 10500 ){
			$cur_bid = 2076;
			$cur_cid = 1;
			$cur_lid = 128;
		}
		else{$cur_bid = 0;$cur_cid = 0;$cur_lid = 0;}

		///credits/debits
		$query = "SELECT AR_credit,AR_debit FROM vend_locations WHERE locationid = $cur_lid";
		$result = Treat_DB_ProxyOld::query($query);

		$AR_creditid = mysql_result($result,0,"AR_credit");
		$AR_debitid = mysql_result($result,0,"AR_debit");

		$credit_bid[$AR_creditid] = $cur_bid;
		$credit_cid[$AR_creditid] = $cur_cid;
		$debit_bid[$AR_debitid] = $cur_bid;
		$debit_cid[$AR_debitid] = $cur_cid;

		///insert new invoice
		if($cur_bid > 0){
			///account
			$account = $pieces[1];

			$query = "SELECT accountid FROM accounts WHERE businessid = $cur_bid AND accountnum = '$account'";
			$result = Treat_DB_ProxyOld::query($query);
			$num = mysql_numrows($result);

			if($num>0){
				$cur_aid = mysql_result($result,0,"accountid");
			}
			else{
				$customer = $pieces[2];
				$customer = str_replace("'","`",$customer);

				$query = "INSERT INTO accounts (businessid,companyid,name,accountnum,export,note) VALUES ($cur_bid,$cur_cid,'$customer','$account',1,'Created from MEI Import')";
				$result = Treat_DB_ProxyOld::query($query);
				$cur_aid = mysql_insert_id();
			}

			///date
			$year = substr($pieces[3],4,2);
			$day = substr($pieces[3],2,2);
			$month = substr($pieces[3],0,2);
			$year += 2000;
			$cur_date = "$year-$month-$day";

			///current route
			$cur_route = $pieces[14];

			///current invoice total
			$cur_total = $pieces[20];

			///insert invoice
			$inv_total = 0;
			$query = "INSERT INTO invoice (businessid,companyid,accountid,date,type,status,taxable,createdate,createby,route,invoice_num)
					VALUES
					($cur_bid,$cur_cid,$cur_aid,'$cur_date',5,4,'off','$cur_date','MEI','$cur_route','$cur_invoice')";
			$result = Treat_DB_ProxyOld::query($query);
			$invoiceid = mysql_insert_id();
		}
	}

	////invoice detail
	$cur_item = $pieces[6];
	$cur_qty = $pieces[7];
	$cur_amt = $pieces[15];
	$cur_item_code = $pieces[5];

	$cur_item = str_replace("'","`",$cur_item);

	$query = "INSERT INTO invoicedetail (invoiceid,qty,item,price,item_code)
			VALUES
			($invoiceid,1,'$cur_qty $cur_item','$cur_amt','$cur_item_code')";
	$result = Treat_DB_ProxyOld::query($query);

	$inv_total += $cur_amt;

	$last_inv = $cur_invoice;
}

/////update last invoice
if($invoiceid > 0){
			////adjust for tax rounding
			if($cur_total != $inv_total){
				$diff = round($cur_total - $inv_total,2);

				$query = "INSERT INTO invoicedetail (invoiceid,qty,item,price,item_code)
					VALUES
					($invoiceid,1,'Tax Adjustment','$diff','')";
				$result = Treat_DB_ProxyOld::query($query);
			}

			$query = "UPDATE invoice SET total = '$cur_total', posted = 1 WHERE invoiceid = $invoiceid";
			$result = Treat_DB_ProxyOld::query($query);

			///update credit/debit totals
			$credits[$AR_creditid][$cur_date] += $cur_total;
			$debits[$AR_debitid][$cur_date] += $cur_total;
}

fclose($handle);

////credits and debits
foreach($credits AS $key => $value){
	foreach($credits[$key] AS $date => $amount){
		$query = "INSERT INTO creditdetail (companyid,businessid,amount,date,creditid)
			VALUES ($credit_cid[$key],$credit_bid[$key],'$amount','$date',$key) ON DUPLICATE KEY UPDATE amount = '$amount'";
		$result = Treat_DB_ProxyOld::query($query);
		echo "$query<br>";
	}
}
foreach($debits AS $key => $value){
	foreach($debits[$key] AS $date => $amount){
		$query = "INSERT INTO debitdetail (companyid,businessid,amount,date,debitid)
			VALUES ($debit_cid[$key],$debit_bid[$key],'$amount','$date',$key) ON DUPLICATE KEY UPDATE amount = '$amount'";
		$result = Treat_DB_ProxyOld::query($query);
		echo "$query<br>";
	}
}

echo "Done";
unlink($myFile);
?>
