<?php

/////////////////////////////////
//kiosk handheld info export
/////////////////////////////////

define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');
require_once( DOC_ROOT . '/ta/lib/class.menu_items.php' );
require_once( DOC_ROOT . '/ta/lib/class.Kiosk.php' );
ini_set("display_errors","true");

////by company
$com_query = "SELECT companyid FROM company ORDER BY companyid";
$com_result = Treat_DB_ProxyOldProcessHost::query($com_query);

echo $com_query . "<br>";
$date0 = \EE\Date::getValidTimestamp( \EE\Controller\Base::getGetVariable( 'date' ), true );
$date4 = date( 'Y-m-d', $date0 );
$date3 = date( 'Y-m-d', strtotime( '-1 Day', $date0 ) );
while($com_r = Treat_DB_ProxyOldProcessHost::mysql_fetch_array($com_result)){
	$companyid = $com_r["companyid"];

	$machines = array();

	///dates
	////override
	$date = date("Y-m-d", $date0);
	$date = prevday($date);
	//$date = prevday($date);

	$date1 = "$date 00:00:00";
	$date2 = "$date 23:59:59";
	$export_date = date("jMY",strtotime($date));

	$data = "0\tMachine#\tDate\tItem Code\tFills\tWaste\tUnit Cost\tPrice\tInv Count\tMax\tSales\tItems Sold\tItem name\tblank\tblank\tblank\tblank\tblank\tblank\tblank\tblank\tblank\tblank\r\n";

	$query1 = "CALL load_kiosk_export_part1($companyid, '$date3', '$date4')";
	$result1 = Treat_DB_ProxyOldProcessHost::query($query1);

	$query2 = "CALL load_kiosk_export($companyid, '$date3', '$date4')";
	$result2 = Treat_DB_ProxyOldProcessHost::query($query2);

	////select all machines linked to businesses

	$query = "SELECT id, companyid, date1, date2, item_code, name, pos_id, max_value, businessid, machine_num, onhand, fills, waste, count_rec, price, cost, sales, items_sold 
			FROM mburris_businesstrack.load_kiosk_export
			WHERE companyid = $companyid 
			ORDER BY businessid, machine_num";
	$result = Treat_DB_ProxyOldProcessHost::query($query);
	echo $query . "<br>";

	while($r = Treat_DB_ProxyOldProcessHost::mysql_fetch_array($result)){
		$machine_num = $r["machine_num"];
		$item_code = $r["item_code"];
		$onhand = $r["onhand"];
		$unit_cost = $r["cost"];
		$fills = $r["fills"];
		$waste = $r["waste"];
		//$is_inv = $r["is_inv"];
		$price = $r["price"];
		//$max = $r["max"];
		$max = $r["max_value"];
		$name = $r["name"];
		$businessid = $r["businessid"];
		$pos_id = $r["pos_id"];
		$total = $r["sales"];
		$count_rec = $r["count_rec"];
		$items_sold = $r["items_sold"];

		if($count_rec > 1){$total = round($total / $count_rec, 2);}

		if($fills == ""){$fills = 0;}
		if($waste == ""){$waste = 0;}
		if($onhand == ""){$onhand = 0;}

		$machines[$machine_num] = $total;

        ////remove commas from name
        $name = str_replace(",","",$name);

        ////get check detail
        //$items_sold = menu_items::items_sold($pos_id,$businessid,$date1,$date2);
        if($items_sold == ""){$items_sold = 0;}
		
		$total = round($total, 2);
		
		if($fills == 0 && $waste == 0 && $onhand == 0 && $items_sold == 0){
			///do nothing
		}
		else{
			$data .= "1\t$machine_num\t$export_date\t$item_code\t$fills\t$waste\t$unit_cost\t$price\t$onhand\t$max\t$total\t$items_sold\t$name\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\r\n";
		}
	}
	
	//$data .= "0\tMachine#\tCompany\tKiosk\tSales\tTax\tCk Card Purchases\tvouchers/Payroll Deduct\tCredit Card Purchases\tCash Reloads\tKiosk CC Reload\tWeb CC Reload\tRefunds\tBottle Deposits\tPromotions\tManufacturer Promotinos\tSubsidy\tCash Drop\tSite Visit\tDate\tBlank\tBlank\tBlank\r\n";
	
	/////business - kiosk loop
	$query = "SELECT business.businessid, business.businessname, company.companyname FROM business
				JOIN company ON company.companyid = business.companyid 
				WHERE business.companyid = $companyid AND business.categoryid = 4 ORDER BY business.businessname";
	$result = Treat_DB_ProxyOldProcessHost::query($query);
	
	$i = 0;
	
	while($r = Treat_DB_ProxyOldProcessHost::mysql_fetch_array($result)){
		$businessid = $r["businessid"];
		$businessname = $r["businessname"];
		$companyname = $r["companyname"];
		
		////get first machine
		$query2 = "SELECT machine_num FROM machine_bus_link WHERE businessid = $businessid ORDER BY machine_num LIMIT 1";
		$result2 = Treat_DB_ProxyOldProcessHost::query($query2);
		
		$machine_num = Treat_DB_ProxyOldProcessHost::mysql_result($result2,0,"machine_num");
		if($machine_num == null)$machine_num = 0;
		
		////remove commas
		$companyname = str_replace(",","",$companyname);
		$businessname = str_replace(",","",$businessname);
		
		$data_headers = "";
		$data_headers .= "0\tMachine#\tCompany\tKiosk";

		$query2 = "SELECT * FROM reporting_category ORDER BY orderid";
		$result2 = Treat_DB_ProxyOldProcessHost::query($query2);
		
		while($r2 = Treat_DB_ProxyOldProcessHost::mysql_fetch_array($result2)){
			$categoryid = $r2["id"];
			$category_name = $r2["category"];
			$cat_type = $r2["type"];
			$cat_type_key = $r2["type_key"];
			$cat_type_detail = $r2["type_detail"];
			
			$data_headers .= "\t$category_name";
		}
		
		if ($i < 1){
			$data .= $data_headers;
			$data .= "\r\n";
		}
		
		$i ++;
		
		$data .= "2\t$machine_num\t$companyname\t$businessname";
		
		///amounts
		$query2 = "SELECT * FROM reporting_category ORDER BY orderid";
		$result2 = Treat_DB_ProxyOldProcessHost::query($query2);
		
		while($r2 = Treat_DB_ProxyOldProcessHost::mysql_fetch_array($result2)){
			$categoryid = $r2["id"];
			$category_name = $r2["category"];
			$cat_type = $r2["type"];
			$cat_type_key = $r2["type_key"];
			$cat_type_detail = $r2["type_detail"];
			
			$query3 = "SELECT SUM($cat_type_detail.amount) AS total FROM $cat_type_detail
						JOIN $cat_type ON $cat_type.$cat_type_key = $cat_type_detail.$cat_type_key AND $cat_type_detail.date = '$date'
						WHERE $cat_type.businessid = $businessid AND $cat_type.category = $categoryid";
			$result3 = Treat_DB_ProxyOldProcessHost::query($query3);
			
			$total = round(Treat_DB_ProxyOldProcessHost::mysql_result($result3, 0, "total"), 2);
			
			$data .= "\t$total";
		}
		
		///cash collects
		$collects = Kiosk::collect_amount($businessid,$date);
		$collects = round($collects, 2);
		$data .= "\t$collects";
		
		////driver was there?
		$query2 = "SELECT SUM(onhand+fills+waste) AS total FROM inv_count_vend
					JOIN machine_bus_link ON machine_bus_link.machine_num = inv_count_vend.machine_num AND machine_bus_link.businessid = $businessid
					WHERE inv_count_vend.date_time BETWEEN '$date 00:00:00' AND '$date 23:59:59'";
		$result2 = Treat_DB_ProxyOldProcessHost::query($query2);
		
		$total = Treat_DB_ProxyOldProcessHost::mysql_result($result2,0,"total");
		
		if($total+$collects != 0){
			$data .= "\t1";
		}
		else{
			$data .= "\t0";
		}
		
		$data .= "\t$export_date\t0\t0\t0\r\n";
	}
	
	////write to file
	file_put_contents(DIR_FTP_UPLOADS."/TreatAmerica/KioskInventory/KioskData-$companyid-b.csv",$data);

}
?>