<?php
//$businessid = "";
/////////////////////////////////
//kiosk email orders
//for static menu
/////////////////////////////////

define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');
require_once( DOC_ROOT . '/ta/lib/class.menu_items.php' );
require_once( DOC_ROOT . '/ta/lib/class.Kiosk.php' );

set_time_limit(1000);
ini_set("display_errors", true);

$today = $_GET["today"];
$time = $_GET["time"];
$currentdistrict=$_GET["district"];

$datetime = new DateTime('now', new DateTimeZone('US/Central'));
if($today == "" || $time == "" || $currentdistrict < 1){
	$time = $datetime->format('Hi');
	$today = $datetime->format('Y-m-d');
//	$addquery = "AND district.districtid = 112";
$addquery = "";
}
else{
	$addquery = "AND district.districtid = $currentdistrict";
}
$production = pow(2, $datetime->format('N'));

echo $time . "<br>";

$date2 = $datetime->format('Y-m-d H:i:s');

////route totals
$route_num = array();
$route_data = array();
$route_items = array();
$route_business = array();
$route_total_email = array();

$queryData = "";
$kos_email = "";
$email = "";
$orderdate = "";

////select all machines linked to businesses
/*
 * Get all Kiosks that are open today for further processing later.
 */

$query = "
	SELECT
		kiosk_order_sched.`districtid`,
		kiosk_order_sched.`ordertype`,
		kiosk_order_sched.`datetime`,
		kiosk_order_sched.`weekend`,
		kiosk_order_sched.`email` AS `kos_email`,
		kiosk_order_sched.`route_total`,
		machine_bus_link.`businessid`,
		machine_bus_link.`machine_num`,
		machine_bus_link.`leadtime`,
		login_route.`route`,
		login_route.`login_routeid`,
		vend_machine.`name`,
		vend_machine.`machineid`,
		business.`businessname`,
		district.`production`
	FROM
		kiosk_order_sched
	JOIN business ON
		business.`districtid` = kiosk_order_sched.`districtid`
	JOIN district ON
		district.`districtid` = business.`districtid`
		AND district.`production` & $production
		$addquery
	JOIN machine_bus_link ON
		machine_bus_link.`ordertype` = kiosk_order_sched.`ordertype`
		AND machine_bus_link.`businessid` = business.`businessid`
	JOIN vend_machine ON
		vend_machine.`machine_num` = machine_bus_link.`machine_num`
		AND vend_machine.`is_deleted` = 0
	JOIN login_route ON
		login_route.`login_routeid` = vend_machine.`login_routeid`
	WHERE
		(kiosk_order_sched.`active` = 1
		AND kiosk_order_sched.`datetime` = '$time')	
		OR
		(kiosk_order_sched.`active` = 1
		AND kiosk_order_sched.`datetime` < '$time'
		AND DATE(kiosk_order_sched.`lastrun`) < '$today')
	ORDER BY login_route.route
";
$result = Treat_DB_ProxyOldProcessHost::query($query);

/*
	JOIN vend_machine_schedule ON
		vend_machine_schedule.machineid = vend_machine.machineid
		AND (
			(
				vend_machine_schedule.day = $day1
				AND machine_bus_link.leadtime = 0
			)
			OR
			(
				vend_machine_schedule.day = $day2
				AND machine_bus_link.leadtime = 1
			)
			OR
			(
				vend_machine_schedule.day = $day3
				AND machine_bus_link.leadtime = 2
			)
			OR
			(
				vend_machine_schedule.day = $day4
				AND machine_bus_link.leadtime = 3
			)
		)
 */

$scheduledNotOpen = 0;
$scheduledMonday = 0;
$scheduledNormally = 0;
$filtered = array();

while ( $row = Treat_DB_ProxyOldProcessHost::mysql_fetch_object($result) ) {
	$email = "";
	$kos_email = "";
	
	$info = array(
		'date' => null,
		'row' => null,
	);
	// Create leadtime agnostic conversion. Right now bitwise operations could
	// be used, but to avoid changing this and possible bugs, use datetime
	// modifications instead.
	$scheduledForMonday = false;
	$deliveryDate = clone $datetime;
	if ( $row->leadtime > 0 ) {
		$deliveryDate->modify("+{$row->leadtime} days");
	}

	$info['date'] = $deliveryDate->format('Y-m-d H:i:s');

	$dayOfWeek = $deliveryDate->format('w');
	$vend_machine_schedule_result = Treat_DB_ProxyOldProcessHost::query("
		SELECT
			NULL
		FROM
			vend_machine_schedule
		WHERE
			vend_machine_schedule.machineid = {$row->machineid}
			AND vend_machine_schedule.day = {$dayOfWeek}
	");

	$isOpen = (($row->production & pow(2, $deliveryDate->format('N'))) > 0);

	if (!$isOpen && Treat_DB_ProxyOldProcessHost::mysql_num_rows($vend_machine_schedule_result) > 0) {
		$scheduledNotOpen++;
		$info['row'] = $row;
		$filtered[] = $info;
		continue;
	}

	if ( !$isOpen ) {
		$deliveryDate->modify('next week monday');
		$isOpen = (($row->production & pow(2, $deliveryDate->format('N'))) > 0);
		if ( !$isOpen ) {
			// It appears this isn't open on Monday. How abouts we skip it.
			continue;
		}
		$info['date'] = $deliveryDate->format('Y-m-d H:i:s');
		$scheduledForMonday = true;
	}

	$dayOfWeek = $deliveryDate->format('w');

	// The point is to tell whether there is a schedule and if there isn't to
	// remove from the list to receive emails. No data should be needed here
	// and should be more efficient query wise if no data is returned.
	$vend_machine_schedule_result = Treat_DB_ProxyOldProcessHost::query("
		SELECT
			NULL
		FROM
			vend_machine_schedule
		WHERE
			vend_machine_schedule.machineid = {$row->machineid}
			AND vend_machine_schedule.day = {$dayOfWeek}
	");

	if (Treat_DB_ProxyOldProcessHost::mysql_num_rows($vend_machine_schedule_result) < 1) {
		continue;
	}

	if ($scheduledForMonday) {
		$scheduledMonday++;
	} else {
		$scheduledNormally++;
	}

	$info['row'] = $row;
	$filtered[] = $info;
}

echo "Scheduled and not open: {$scheduledNotOpen}<br>";
echo "Scheduled for Monday: {$scheduledMonday}<br>";
echo "Scheduled Normally: {$scheduledNormally}<br>";
echo count($filtered) . "<br>";

var_dump($filtered);

foreach ($filtered as $data) {
echo $businessid . " ";
	$row = $data['row'];
	$businessid = $row->businessid;
	$machine_num = $row->machine_num;
	$ordertype = $row->ordertype;
	$route = $row->route;
	$machine_array[0] = $machine_num;
	$leadtime = $row->leadtime;
	$name = $row->name;
	$kos_email = $row->kos_email;
	$route_total = $row->route_total;
	$machineid = $row->machineid;
	$login_routeid = $row->login_routeid;

	//reset orderid
	$orderid = 0;

	////orderdate
	$pickEmailDate = new DateTime($data['date'], new DateTimeZone('US/Central'));
	$orderdate = $pickEmailDate->format('Y-m-d');
	$day = $pickEmailDate->format('l');

	/*
	switch (intval($leadtime)) {
		case 0:
			$orderdate = $today;
			break;
		case 1:
		case 2:
		case 3:
			// Not really necessary, but useful for testing.
			$leadDate = clone $datetime;
			$leadDate->modify("+{$leadtime} days");
			$orderdate = $leadDate->format('Y-m-d');
			$day = $leadDate->format('l');
			break;
		default:
			$orderdate = $today;
	}
	 */

	$email_message = "";

	///get menu items
	$query2 = "SELECT * FROM menu_items_new WHERE businessid = $businessid AND active = 0 AND ordertype = $ordertype ORDER BY item_code";
	$result2 = Treat_DB_ProxyOldProcessHost::query($query2);

	while ($r2 = mysql_fetch_array($result2)) {	
		$min_id = $r2["id"];
		$item_code = $r2["item_code"];
		$item_name = $r2["name"];
		$pos_id = $r2["pos_id"];
		$max = $r2["max"];
		$reorder_point = $r2["reorder_point"];
		$reorder_amount = $r2["reorder_amount"];

		////get dates
		$dates = Kiosk::last_inv_dates($machine_array, $item_code);
		$date = $dates[1];
		if ($date == '') {
			$date = $dates[0];
		}

		////last count
		$onhand2 = Kiosk::inv_onhand($machine_array, $item_code, $date);

		////fills
		$fills = Kiosk::fills($machine_array, $item_code, $date, $date2);

		////waste
		$waste = Kiosk::waste($machine_array, $item_code, $date, $date2);

		////get check detail
		$items_sold = menu_items::items_sold($pos_id,$businessid,$date,$date2);

		$onhand = $onhand2 + $fills - $waste - $items_sold;
		
		/////REORDER
		$reorder = 0;
		if($reorder_point == 0 && $max != 0) {
			$reorder = $max - $onhand;
		}
		else if($max != 0) {
			if($onhand <= $reorder_point) {
				if($reorder_amount != 0 && $reorder_amount > ($max - $onhand)) {
					$reorder = $reorder_amount;
				}
				else {
					$reorder = $max - $onhand;
				}
			}
		}

		if($reorder > $max) {
			$reorder = $max;
		}
		if($reorder <= 0) {
			$reorder = 0;
		}

		if($reorder != 0) {
			$email_message .= "
				<tr>
					<td style=\"border:1px solid black;\">&nbsp;$item_code</td>
					<td style=\"border:1px solid black;\">&nbsp;$item_name</td>
					<td style=\"border:1px solid black;\">&nbsp;$reorder</td>
				</tr>
			";

			////route totals
			$route_num[$login_routeid] = $route;
			$route_data[$login_routeid][$ordertype][$item_code] += $reorder;
			$route_items[$login_routeid][$ordertype][$item_code] = $item_name;
			$route_business[$login_routeid] = $businessid;
			$route_total_email[$login_routeid] = $route_total;

			////create order
			if($orderid == 0) {
				$query3 = "INSERT IGNORE INTO machine_order (route,expected_delivery,login_routeid,machineid) VALUES ('$route','$orderdate','$login_routeid','$machineid')";
				$result3 = Treat_DB_ProxyOld::query($query3, true);
				$orderid = Treat_DB_ProxyOld::mysql_insert_id();
				
				if($orderid < 1){
					$orderQuery = "SELECT id AS orderid FROM machine_order WHERE machineid = '$machineid' AND expected_delivery = '$orderdate'";
					$result3 = Treat_DB_ProxyOld::query($orderQuery, true);
					$orderid = Treat_DB_ProxyOld::mysql_result($result3,0,"orderid");
				}
				
				//file_put_contents(DIR_FTP_UPLOADS."/TreatAmerica/exports/orderEmail.log", $orderQuery . ";\r\n $orderid \r\n" . $query3 . "\r\n", FILE_APPEND);
			}

			//add item to order
			$query3 = "INSERT IGNORE INTO machine_orderdetail (orderid, menu_items_new_id, item_code, qty) VALUES ('$orderid','$min_id','$item_code','$reorder')";
			$result3 = Treat_DB_ProxyOld::query($query3, true);
			
			//file_put_contents(DIR_FTP_UPLOADS."/TreatAmerica/exports/orderEmail.log", $query3 . "\r\n", FILE_APPEND);
		}
	}

	if(trim($email_message) !== "") {
		$query4 = "SELECT district.districtid, district.email_orders FROM business,district WHERE business.businessid = $businessid AND business.districtid = district.districtid";
		$result4 = Treat_DB_ProxyOldProcessHost::query($query4);

		$all_email = mysql_result($result4,0,"email_orders");
		$districtid = mysql_result($result4,0,"districtid");

		if(trim($all_email) !== "") {
			$email = $all_email;
			if(trim($kos_email) !== "") {
				$email .= ",$kos_email";
			}
		}
		else {
			$email = $kos_email;
		}

		//$day = dayofweek($orderdate);

		$email_message = "
		<p>Machine #{$machine_num} Order for {$day} {$orderdate}</p>
		<table cellspacing=0 cellpadding=0 style=\"border:1px solid black;\" width=100%>
			<tr style=\"background:black;\">
				<td colspan=3 style=\"border:1px solid black;\">
					<font color=white><b>{$machine_num}</b></font>
				</td>
			</tr>
			$email_message
		</table>
		";

		// DEBUG:
		//echo $email_message;

		$email_message = "
<html>
	<head></head>
	<body>
		{$email_message}
	</body>
</html>";

		// Standard requires \r\n and some email transports are strict about this.
		// Given the formatting, fix the line ending.
		$email_message = str_replace("\n", "\r\n", str_replace(array("\r\n", "\r"), "\n", $email_message));


		$todayis = date("l, F j, Y, g:i a") ;
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= "From: orders@companykitchen.com\r\n";
		$subject = "Route $route [$name] Order";

		mail("pickemail@essentialpos.com", $subject, $email_message, $headers);
		mail($email, $subject, $email_message, $headers);
	
		///mark last time email sent
		$query5 = "UPDATE kiosk_order_sched SET lastrun = NOW() WHERE districtid = $districtid AND ordertype = $ordertype";
		$result5 = Treat_DB_ProxyOld::query($query5, true);
		$result5 = Treat_DB_ProxyOldProcessHost::query($query5);
		echo $query5 . "<br>";
	}
}

$email = "";
$kos_email = "";
$all_email = "";
$email_data = "";


////send route total emails
if(!empty($route_num)) {
	foreach($route_num AS $login_routeid => $route) {
		foreach($route_data[$login_routeid] AS $ordertype => $item) {

			$email_data = "";
			$email = "";
			$all_email = "";
			
			ksort($route_data[$login_routeid][$ordertype]);

			foreach($route_data[$login_routeid][$ordertype] AS $item_code => $qty) {
				$item_name = $route_items[$login_routeid][$ordertype][$item_code];
				$email_data .= "
		<tr>
			<td style=\"border:1px solid black;\">$item_code</td>
			<td style=\"border:1px solid black;\">$item_name</td>
			<td style=\"border:1px solid black;\">$qty</td>
		</tr>
				";
			}

			///send email
			$businessid = $route_business[$login_routeid];

			$query4 = "SELECT ordertype FROM kiosk_order_type WHERE id = $ordertype";
			$result4 = Treat_DB_ProxyOldProcessHost::query($query4);

			$type_name = mysql_result($result4, 0, "ordertype");

			$query4 = "SELECT district.email_orders, kiosk_order_sched.`email` AS `kos_email` FROM business,district,kiosk_order_sched WHERE business.businessid = $businessid AND business.districtid = district.districtid and kiosk_order_sched.districtid = district.districtid";
			
			$result4 = Treat_DB_ProxyOldProcessHost::query($query4);

			$all_email = mysql_result($result4, 0, "email_orders");
			$kos_email = mysql_result($result4, 0, "kos_email");

			if($all_email != "") {
				$email = $all_email;
				if($kos_email != "") {
					$email .= ",$kos_email";
				}
			}
			else {
				$email = $kos_email;
			}

			//$email = "smartin@essentialpos.com,traceym@treatamerica.com";

			$day = dayofweek($orderdate);

			$email_message = "
		<p>Route# {$route} Totals for {$day} {$orderdate}</p>
		<table cellspacing=0 cellpadding=2 style=\"border:1px solid black;\" width=100%>
			<tbody>
				<tr style=\"background:black;\">
					<td style=\"border:1px solid black;\">
						<font color=white><b>Item Code</b></font>
					</td>
					<td style=\"border:1px solid black;\">
						<font color=white><b>Item</b></font>
					</td>
					<td style=\"border:1px solid black;\">
						<font color=white><b>Total</b></font>
					</td>
				</tr>
				$email_data
			</tbody>
		</table>
			";

			// DEBUG:
			//echo $email_message;

			$email_message = "
<html>
	<head></head>
	<body>
		$email_message
	</body>
</html>
			";

			// Standard requires \r\n and some email transports are strict about this.
			// Given the formatting, fix the line ending.
			$email_message = str_replace("\n", "\r\n", str_replace(array("\r\n", "\r"), "\n", $email_message));

			$todayis = date("l, F j, Y, g:i a");
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= "From: orders@companykitchen.com\r\n";
			$subject = "Route $route $type_name Totals";

			if($route_total_email[$login_routeid] == 1) {
				//$email_message = preg_replace( '/[^[:print:]]/', '', $email_message );		
				mail("pickemail@essentialpos.com", $subject, $email_message, $headers);
				mail($email, $subject, $email_message, $headers);
			}
		}
	}
}

echo "Done";
?>
