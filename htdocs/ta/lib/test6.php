<!DOCTYPE HTML PUBLIC   "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
	
	<style type="text/css">
		#company-headers { width: 1025px; }
		#company-business-rows { width: 1025px; }
		#company-headers-bid { float: left; width: 150px; }
		#company-headers-bname { float: left; width: 325px; }
		#company-headers-customers { float: left; width: 100px; }
		#company-headers-oob { float: left; width: 50px; }
		#company-headers-stored-balance { float: left; width: 100px; }
		#company-headers-calculated-balance { float: left; width: 100px; }
		#company-headers-difference { float: left; width: 100px; }
		
		.customer-businessid { float: left; width: 150px; clear: both; }
		.customer-businessname { float: left; width: 325px; }
		.customer-totals { float: left; width: 100px; }
		.customer-totals-wrong { float: left; width: 100px; font-weight: bold; color: red; }
		.customer-oob { float: left; width: 50px; }
		.customer-stored { float: left; width: 100px; }
		.customer-calculated { float: left; width: 100px; }
		.customer-difference { float: left; width: 100px; }
		
		#company-summary { width: 1025px; }
		#company-summary-totals { float: left; width: 625px; font-weight: bold; }
		#company-summary-stored-total { float: left; width: 100px; font-weight: bold; }
		#company-summary-calculated-total { float: left; width: 100px; font-weight: bold; }
		#company-summary-difference-total { float: left; width: 100px; font-weight: bold; }		
	</style>
</head>
<body>
	<h3>This will calculate totals for all the business for the company you select.<br />It is resource intensive. Please only run to get a general idea of where the company stands.</h3>
<?php

define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');

$current_cid = \EE\Controller\Base::getPostGetSessionOrCookieVariable('compcook');

$query = "SELECT company.companyname, company.companyid FROM company WHERE (companyid != 2) ORDER BY company.companyname ASC";
$result = Treat_DB_ProxyOldProcessHost::query($query);

if (@$_GET['cid']){
	$cid = @$_GET['cid'];
} else {
	$cid = 0;
}

$show_all = @$_GET['show_all'];
$selected_cid = $cid;

?>
	<form action=test6.php method=get>

		<select name="cid">
<?php
	
while($r = Treat_DB_ProxyOldProcessHost::mysql_fetch_array($result)){
	$companyid = $r["companyid"];
	$companyname = $r["companyname"];
	
	if ($selected_cid != $companyid){
		?><option value="<?php echo $companyid; ?>"><?php echo $companyname; ?></option><?php
	} else {
		?><option value="<?php echo $companyid; ?>" selected="selected"><?php echo $companyname; ?></option><?php
	}
}

?>
		</select>
		<input type=submit value='GO Now'>
		<br />
	</form>


<?php


//$companyid = 12;

//echo "<h3>Show Businesses for company 12</h3>";


if ($_GET['cid']){

	$companyid = $_GET['cid'];
	
	//$sql5 = "DELETE FROM mburris_businesstrack.customer_z_tracker WHERE companyid = $companyid";
	//$result5 = Treat_DB_ProxyOldProcessHost::query($sql5);
	
	$sql4 = "TRUNCATE mburris_businesstrack.customer_z_tracker";
	$result4 = Treat_DB_ProxyOld::query($sql4);	

$sql = "SELECT business.businessid, business.businessname, company.companyname FROM business INNER JOIN company ON company.companyid = business.companyid WHERE (business.companyid != 2) AND (business.companyid = $companyid) AND (business.categoryid = 4) ORDER BY business.businessname";
$result = Treat_DB_ProxyOldProcessHost::query($sql);

?>
	<div id="company-headers">

			<span id="company-headers-bid">ID</span>
			<span id="company-headers-bname">Business</span>
			<span id="company-headers-customers">Customers</span>
			<span id="company-headers-oob"># OOB</span>
			<span id="company-headers-stored-balance">Stored</span>
			<span id="company-headers-calculated-balance">Calculated</span>
			<span id="company-headers-difference">Difference</span>

	</div>
	<div id="company-business-rows">

<?php

while ($r = Treat_DB_ProxyOldProcessHost::mysql_fetch_array($result)) {
	
	$total_customers = 0;
	$total_customer_records = 0;
	$bid_url = "";
	$businessname_url = "";
	
	$businessid = $r['businessid'];
	$businessname = $r['businessname'];
	
	$sql2 = "SELECT COUNT(*) AS totals FROM customer WHERE businessid = '$businessid' LIMIT 1";
	$result2 = Treat_DB_ProxyOldProcessHost::query($sql2);
	$total_customer_records = Treat_DB_ProxyOldProcessHost::mysql_result($result2, 0, "totals");
	
	$msg = "";
	$css_class = "customer-totals";
	
	if ($total_customer_records > 0) {
		calculate_company_customer_balances($businessid);

		$sql3 = "SELECT businessid, COUNT(*) total_customers, SUM(out_of_balance) AS oob, SUM(stored_balance) AS stored_balance_total, SUM(calculated_balance) AS calculated_balance_total, SUM(oob_amount) AS oob_amount_total, COUNT(*) - SUM(out_of_balance) AS in_balance FROM mburris_businesstrack.customer_oob WHERE businessid = '$businessid' GROUP BY businessid ORDER BY businessid";
		//echo "<h3>" . $sql3 . "</h3>";
		$result3 = Treat_DB_ProxyOldProcessHost::query($sql3);
		if (mysql_num_rows($result3) > 0){

			$oob_customers = Treat_DB_ProxyOldProcessHost::mysql_result($result3, 0, "oob");
			$in_balance_customers = Treat_DB_ProxyOldProcessHost::mysql_result($result3, 0, "in_balance");
			$total_customers_calculated = Treat_DB_ProxyOldProcessHost::mysql_result($result3, 0, "total_customers");
			$stored_balance_total = Treat_DB_ProxyOldProcessHost::mysql_result($result3, 0, "stored_balance_total");
			$calculated_balance_total = Treat_DB_ProxyOldProcessHost::mysql_result($result3, 0, "calculated_balance_total");
			$oob_amount_total = Treat_DB_ProxyOldProcessHost::mysql_result($result3, 0, "oob_amount_total");
			
			if ($total_customers_calculated != $total_customer_records){
				$msg = " - Fix Duplicate Scancodes";
				$css_class = "customer-totals-wrong";
				$bid_url = "<a href='/ta/lib/test6.php?scancodes=1&bid=$businessid' target=_blank>$businessid</a>";
				$businessname_url = "<a href='/ta/lib/test6.php?scancodes=1&bid=$businessid' target=_blank>$businessname</a>";
			} else {
				$bid_url = "<a href='/ta/customerutilities/accountsOutOfBalance?cid=$current_cid&bid=$businessid&customers=$total_customer_records&oob=$oob_customers&inbalance=$in_balance_customers' target=_blank>$businessid</a>";
				$businessname_url = "<a href='/ta/customerutilities/accountsOutOfBalance?cid=$current_cid&bid=$businessid&customers=$total_customer_records&oob=$oob_customers&inbalance=$in_balance_customers' target=_blank>$businessname</a>";
			}
		}
	} else {
		$oob_customers = 0;
		$stored_balance_total = 0;
		$calculated_balance_total = 0;
		$oob_amount_total = 0;
	}
	
	$decimals = 2;
	$stored_balance_total = number_format($stored_balance_total, $decimals);
	$calculated_balance_total = number_format($calculated_balance_total, $decimals);
	$oob_amount_total = number_format($oob_amount_total, $decimals);
	
	?>
	<span class="customer-businessid"><?php echo $bid_url; ?></span>
	<span class="customer-businessname"><?php echo $businessname_url; ?></span>
	<span class="<?php echo $css_class; ?>"><?php echo $total_customer_records; ?></span><span class="customer-oob"><?php echo $oob_customers; ?></span><span class="customer-stored"><?php echo $stored_balance_total; ?></span><span class="customer-calculated"><?php echo $calculated_balance_total; ?></span><span class="customer-difference"><?php echo $oob_amount_total; ?></span><?php
		
}
$refill_customer_oob = "SELECT companyid, businessid, customerid, first_name, last_name, scancode, stored_balance, calculated_balance, oob_amount, out_of_balance FROM mburris_businesstrack.customer_oob";
$refill_customer_oob_result = Treat_DB_ProxyOldProcessHost::query($refill_customer_oob);
Treat_DB_ProxyOld::query("DELETE from mburris_businesstrack.customer_oob");
while ($refill_customer_records = Treat_DB_ProxyOldProcessHost::mysql_fetch_array($refill_customer_oob_result)) {
	$companyid = $refill_customer_records['companyid'];
	$businessid = $refill_customer_records['businessid'];
	$customerid = $refill_customer_records['customerid'];
	$first_name = $refill_customer_records['first_name'];
	$last_name = $refill_customer_records['last_name'];
	$scancode = $refill_customer_records['scancode'];
	$stored_balance = $refill_customer_records['stored_balance'];
	$calculated_balance = $refill_customer_records['calculated_balance'];
	$oob_amount = $refill_customer_records['oob_amount'];
	$out_of_balance = $refill_customer_records['out_of_balance'];
	
	$refill_query = "insert into mburris_businesstrack.customer_oob (companyid, businessid, customerid, first_name, last_name, scancode, stored_balance, calculated_balance, oob_amount, out_of_balance) values ('$companyid', '$businessid', '$customerid', '$first_name', '$last_name', '$scancode', '$stored_balance', '$calculated_balance', '$oob_amount', '$out_of_balance')";
	Treat_DB_ProxyOld::query($refill_query);
	
}
?>

</div>

<?php


$sql6 = "SELECT SUM(stored_balance) stored_balance_total, SUM(calculated_balance) calculated_balance_total FROM mburris_businesstrack.customer_z_tracker WHERE companyid = $companyid GROUP BY companyid";
$result6 = Treat_DB_ProxyOldProcessHost::query($sql6);

$stored_balance_total = Treat_DB_ProxyOldProcessHost::mysql_result($result6, 0, "stored_balance_total");
$calculated_balance_total = Treat_DB_ProxyOldProcessHost::mysql_result($result6, 0, "calculated_balance_total");

$difference = $stored_balance_total - $calculated_balance_total;

$stored_balance_total = number_format($stored_balance_total, $decimals);
$calculated_balance_total = number_format($calculated_balance_total, $decimals);
$difference = number_format($difference, $decimals);

?>
	<div id="company-summary">
		<span id="company-summary-totals">Totals</span><span id="company-summary-stored-total"><?php echo $stored_balance_total; ?></span><span id="company-summary-calculated-total"><?php echo $calculated_balance_total; ?></span><span id="company-summary-difference-total"><?php echo $difference; ?></span>
	</div>
	<?php
}
?>
	</table-->
	<?php
		if ($_GET['scancodes']){
			?>
		<table>
		<tr><td>Duplicate scan-codes</td></tr>
	<?php
			$bid = $_GET['bid'];
			$sql = "SELECT CONCAT(first_name, ' ', last_name) AS customer_name, businessid, scancode, COUNT(*) totals FROM customer WHERE businessid = '$bid' GROUP BY scancode";
			$result = Treat_DB_ProxyOldProcessHost::query($sql);
			
			while ($r = Treat_DB_ProxyOldProcessHost::mysql_fetch_array($result)){
				$totals = $r['totals'];
				$scancode = $r['scancode'];
				$customer_name = $r['customer_name'];
				
				if (($totals > 1) || ($scancode == '')){
					if ($scancode == ''){
						$scancode = "Empty scan-code";
					}
					?><tr><td><?php echo $customer_name; ?></td><td><?php echo $scancode; ?></td></tr><?php
				}
			}
			
			?>
			</table>
		</div>
			<?php
		}
	?>
	
</body>
</html>



<?php

function calculate_company_customer_balances($bid) {
	$sql = "CALL customer_balance_inquiry($bid)";
	$result = Treat_DB_ProxyOldProcessHost::query($sql);
}

?>