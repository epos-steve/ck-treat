<!DOCTYPE HTML PUBLIC   "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<style type="text/css">
			#accounts-rebalanced { float: right; padding: 0px 50px 0px 0px; width: 550px; }
			#accounts-rebalanced-2 { clear: both; float: right; padding: 0px 0px 0px 0px; width: 600px; }
			#rows-fixed { color: #FF0000; }
			#customer-rebalanced-header { float: right; min-width: 400px; margin: 0px 0px 10px 0px; font: 16pt bold; }
			.customer-rebalanced-line { float: right; min-width: 400px; }
			.customer-rebalanced-name { float: left; width: 265px; }
			.customer-rebalanced-scancode { float: left; width: 125px; font-weight: bold; color: blue; }
			.customer-rebalanced-amount { float: left; width: 210px; }
			.customer-not-in-balance {  }
			.customer-in-balance { width: 60%; }
			
			.check-header-row-oob { border: 2px solid red; text-align: center; }
			.check-detail-row-oob { text-align: center; }
			.check-detail-row-oob-even { text-align: center; background-color: #FFCCCC; }
			.check-detail-row-oob-odd { text-align: center; background-color: #FAFAFA; }
			.customer-balance-headers-oob { font-size: 18px; color: #FF0000; font-weight: bold; }
			
			.check-header-row { border: 2px solid blue; text-align: center; }
			.check-detail-row { text-align: center; }
			.check-detail-row-even { text-align: center; background-color: #CFDDF3; }
			.check-detail-row-odd { text-align: center; background-color: #F2F2F2; }
			.customer-balance-headers { font-size: 14px; color: #0000FF; font-weight: bold; }

		</style>

	</head>
	<body>
	<form name="input" action="#" method="post">
	<input type="hidden" name="customer_balances" value="<?php echo $data_value; ?>" />
	<input type="submit" value="Fix Out of Balance Accounts" />
	</form>
	<br />

<?php

define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');

function adjust_customer_balance($id, $calculated_balance, $stored_balance){

	if ($calculated_balance > $stored_balance) { 
		$sql = "UPDATE mburris_businesstrack.customer SET balance = $calculated_balance WHERE customerid = $id LIMIT 1";
		$balance_adjustment_amount = ($calculated_balance - $stored_balance);
		$result = Treat_DB_ProxyOld::query($sql);
		$message = "Balance Adjustment: +" . $balance_adjustment_amount;
	} else if ($calculated_balance < $stored_balance) { 
		$balance_adjustment_amount = ($stored_balance - $calculated_balance);
		$sql = "INSERT INTO customer_transactions (oid, trans_type_id, customer_id, subtotal, chargetotal, response, authresponse) VALUES ('CK Adjust', 8, $id, '$balance_adjustment_amount', '$balance_adjustment_amount', 'APPROVED', 'Auto Adjustment - EE')";
		$result = Treat_DB_ProxyOld::query($sql);
		$message = "CK Adjustment: +" . $balance_adjustment_amount;
	}
	
	//$result = Treat_DB_ProxyOld::query($sql);
	return $message;
}

function get_amount_value($type_id, $amount){
	
	if (($type_id == 1) || ($type_id == 4) || ($type_id == 6) || ($type_id == "Promotion") || ($type_id == "Credit Card Transaction")){
		$return_value = ($amount);
	} else {
		$return_value = $amount;
	}
	
	return $return_value;
	
}

function get_payment_type($transaction_type_id){
	switch ($transaction_type_id) {
		case 1:
			$transaction_type_label = "Kiosk Cash Reload";
		break;

		case 4:
			$transaction_type_label = "Kiosk CC Reload";
		break;

		case 6:
			$transaction_type_label = "Kiosk CC Reload";
		break;

		case 14:
			$transaction_type_label = "Purchase";
		break;

		case "Company Kitchen Refund":
			$transaction_type_label = "Company Kitchen Refund";
		break;

		case "Transfer":
			$transaction_type_label = "Transfer";
		break;

		case "CK Adjust":
			$transaction_type_label = "CK Adjust";
		break;

		case "Promotion":
			$transaction_type_label = "Promotion";
		break;

		case "Credit Card Transaction":
			$transaction_type_label = "Credit Card Transaction";
		break;

		default:
			$transaction_type_label = "N/A";
		break;
	}

	return $transaction_type_label;
	
}
?>
<div id="accounts-rebalanced">
	<span id="customer-rebalanced-header">Accounts Re-balanced <?php //echo @$counter; ?></span>
</div>
<div id="accounts-rebalanced-2">
	<span class="customer-rebalanced-scancode">7800000004491</span><span class="customer-rebalanced-amount">CK Adjustment: +200.49</span><span class="customer-rebalanced-name">Senthil Prabu Muthusamy Ramanathan</span>
	<span class="customer-rebalanced-scancode">7800000004492</span><span class="customer-rebalanced-amount">CK Adjustment: +22.49</span><span class="customer-rebalanced-name">kwantrena horace-overton</span>
	<span class="customer-rebalanced-scancode">7800000004493</span><span class="customer-rebalanced-amount">Balance Adjustment: +225.55</span><span class="customer-rebalanced-name">Test Client</span>
</div>
<?php
	
if ($_POST){
	$customer_data_value = $_SESSION["customer_data_value"];
	//echo $customer_data_value . "<br />";
	$counter = 0;
	$data_values = unserialize($customer_data_value);
	if ($data_values != ''){
		
		?>
		<div id="accounts-rebalanced">
			<span id="customer-rebalanced-header">Accounts Re-balanced <?php //echo @$counter; ?></span>
		</div>
		<div id="accounts-rebalanced-2">
		<?php
		
		foreach ($data_values as $key=>$value){
			foreach ($value as $key2=>$value2){
				$customer_current_calculated_balance = $value2;
				//if ($value2 > 0){
					//$get_customer_data_line_sql = "SELECT CONCAT(CAST(c.customerid AS CHAR), ' - ', c.first_name, ' ', c.last_name, ' - ', c.scancode) AS customer_info FROM mburris_businesstrack.customer c WHERE c.customerid = $key2";
					$get_customer_data_line_sql = "SELECT CONCAT(c.first_name, ' ', c.last_name) AS customer_info, c.scancode AS scancode, c.balance AS stored_balance FROM mburris_businesstrack.customer c WHERE c.customerid = $key2";
					$get_customer_data_line_result = Treat_DB_ProxyOldProcessHost::query($get_customer_data_line_sql);
					$customer_data_line = mysql_result($get_customer_data_line_result, 0, "customer_info");
					$customer_scancode = mysql_result($get_customer_data_line_result, 0, "scancode");
					$customer_current_stored_balance = mysql_result($get_customer_data_line_result, 0, "stored_balance");
					
					$customer_message = adjust_customer_balance($key2, $customer_current_calculated_balance, $customer_current_stored_balance);
					
					$customer_data_line_1 = $customer_data_line . " -- " . $customer_scancode . " -- " . $customer_message . "<br>";
					
					?><span class="customer-rebalanced-name"><?php echo $customer_data_line; ?></span><span class="customer-rebalanced-scancode"><?php echo $customer_scancode; ?></span><span class="customer-rebalanced-amount"><?php echo $customer_message; ?></span><?php
					$counter++;
				//}
			}
		}
	?></div><?php
	}

}

$query = "SELECT business.businessid,business.businessname,company.companyname FROM business JOIN company ON company.companyid = business.companyid WHERE (business.companyid != 2) AND (business.categoryid = 4) ORDER BY company.companyname,business.businessname";
$result = Treat_DB_ProxyOldProcessHost::query($query);

if (@$_GET['bid']){
	$bid = @$_GET['bid'];
} else {
	$bid = 0;
}

$show_all = @$_GET['show_all'];
$selected_bid = $bid;

?>
	<form action=test.php method=get>
		<input type="radio" name="show_all" value="1" <?php if(@$show_all == 1){ echo "checked"; } ?> />Display All
		<input type="radio" name="show_all" value="0" <?php if(@$show_all == 0){ echo "checked"; } ?> />Display OOB only <br />
		
		<select name="bid">
<?php
	
while($r = mysql_fetch_array($result)){
	$businessid = $r["businessid"];
	$businessname = $r["businessname"];
	$companyname = $r["companyname"];
	
	if ($selected_bid != $businessid){
		?><option value="<?php echo $businessid; ?>"><?php echo $companyname; ?> :: <?php echo $businessname; ?></option><?php
	} else {
		?><option value="<?php echo $businessid; ?>" selected="selected"><?php echo $companyname; ?> :: <?php echo $businessname; ?></option><?php
	}
}

?>
		</select>
		<input type=submit value='GO'>
		<br />
	</form>
	<br />
<?php

define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');

$number_of_correct_card_balances = 0;
$number_of_not_correct_balances = 0;

$query = "SELECT business.businessid, business.businessname, company.companyname FROM mburris_businesstrack.business JOIN mburris_businesstrack.company ON company.companyid = business.companyid WHERE (business.businessid = $bid) ORDER BY business.companyid,business.businessname";

$result = Treat_DB_ProxyOldProcessHost::query($query);

while ($r = mysql_fetch_array($result)){
	$businessid = $r['businessid'];
	$businessname = $r['businessname'];
	$companyname = $r['companyname'];

	$query55 = "SELECT first_name,last_name,customerid,scancode,balance FROM mburris_businesstrack.customer WHERE businessid = $businessid and scancode LIKE '780%'";
	//$query55 = "SELECT first_name,last_name,customerid,scancode,balance FROM mburris_businesstrack.customer WHERE businessid = $businessid and scancode LIKE '780%' LIMIT 5";
	$result55 = Treat_DB_ProxyOldProcessHost::query($query55);
	$num55 = @mysql_numrows($result55);

	while($r55 = @mysql_fetch_array($result55)){
		$scancode = $r55["scancode"];
		$customerid = $r55["customerid"];
		$mybalance = $r55["balance"];
		$first_name = $r55["first_name"];
		$last_name = $r55["last_name"];
			
		$customer_name = $first_name . " " . $last_name;
		
		$sql = "SELECT balance FROM mburris_businesstrack.customer WHERE scancode LIKE '$scancode'";
		$result2 = Treat_DB_ProxyOldProcessHost::query($sql);
		$customer_balance = mysql_result($result2, 0, 'balance');

		$counter = 1;

		$sql2 = "SELECT customer.customerid, checks.check_number, checks.businessid, checks.bill_datetime, checks.total, checks.sale_type, payment_detail.received, payment_detail.payment_type, business.businessname FROM checks JOIN payment_detail ON payment_detail.check_number = checks.check_number AND payment_detail.businessid = checks.businessid AND payment_detail.scancode = '$scancode' JOIN customer USING (scancode) JOIN business ON business.businessid = checks.businessid WHERE checks.bill_datetime BETWEEN '2000-01-01 00:00:00' AND '2020-01-01 00:00:00' ORDER BY bill_datetime DESC";
		$result2 = Treat_DB_ProxyOldProcessHost::query($sql2);
		$oob_array = array();
			
		while ($rs = mysql_fetch_array($result2)){
			$customerid = $rs['customerid'];
			$check_number = $rs['check_number'];
			$businessid2 = $rs['businessid'];
			$bill_datetime = $rs['bill_datetime'];
			$total = $rs['total'];
			$sale_type = $rs['sale_type'];
			$received = $rs['received'];
			$payment_type_value = $rs['payment_type'];
			$businessname = $rs['businessname'];

			$customer_transaction_type_label = get_payment_type($payment_type_value);

			$oob_array[] = array(
				'pay_value' => $payment_type, 
				'payment_type' => $payment_type_value, 
				'bill_datetime' => $bill_datetime, 
				'total' => $total, 
				'businessname' => $businessname, 
			);
		}
			
		$additional_customer_transactions_sql = "SELECT ctt.label AS payment_type, ct.date_created AS bill_datetime, ct.chargetotal AS total, 'Not Available' AS businessname FROM mburris_businesstrack.customer_transactions ct INNER JOIN customer_transaction_types ctt ON ct.trans_type_id = ctt.id WHERE (customer_id = $customerid) AND (ct.response LIKE 'APPROVED') ORDER BY ct.date_created DESC";
		$additional_customer_transactions_result = Treat_DB_ProxyOldProcessHost::query($additional_customer_transactions_sql);

		while ($rs = mysql_fetch_array($additional_customer_transactions_result)){
			$payment_type_value = $rs['payment_type'];
			$bill_datetime = $rs['bill_datetime'];
			$total = $rs['total'];
			$businessname = $rs['businessname'];

			$oob_array[] = array(
				'payment_type' => $payment_type_value, 
				'bill_datetime' => $bill_datetime, 
				'total' => $total, 
				'businessname' => $businessname, 
			);
		}

		usort($oob_array, function($a,$b){
			return strtotime($a['bill_datetime']) > strtotime($b['bill_datetime']);
		}); 
			
		$running_balance = 0;
		$running_balance_array = array();
		$value3 = 0;
		$value4 = 0;
		foreach ($oob_array as $key=>$value){
					
				$payment_type = $value['payment_type'];
				$bill_datetime = $value['bill_datetime'];
				$total = $value['total'];
				$businessname = $value['businessname'];
					
					if (($payment_type == 1) || ($payment_type == 4) || ($payment_type == 6) || ($payment_type == 'Promotion') || ($payment_type == 'Credit Card Transaction') || ($payment_type == "Company Kitchen Refund") || ($payment_type == "Transfer") || ($payment_type == "CK Adjust")){
						$total = $total * -1;
						$value3 += $total;
					} else {
						$value4 += $total;
					}
					$running_balance += $total;
					
				$running_balance_array[] = array(
					'pay_value' => $payment_type, 
					'payment_type' => $payment_type_value, 
					'bill_datetime' => $bill_datetime, 
					'total' => $total, 
					'businessname' => $businessname, 
					'running_balance' => $running_balance, 
				);

		}
		
		$calculated_balance = $value3 + $value4;
		$calculated_balance = number_format($calculated_balance,2);
			
		$calculated_balance = $calculated_balance * -1;
		$show_customer_line_items = 0;
		
		if ($customer_balance == $calculated_balance){
			$number_of_correct_card_balances++;
			$header_row_css = "check-header-row";
			$check_detail_row_css = "check-detail-row";
			$customer_stored_balance_row = "customer-balance-headers";
		} else {
			$number_of_not_correct_balances++;
			$header_row_css = "check-header-row-oob";
			$check_detail_row_css = "check-detail-row-oob";
			$customer_stored_balance_row = "customer-balance-headers-oob";
			$customer_calculated_balance_row = "";
			$show_customer_line_items = 1;
			$fix_customers[] = array($customerid => $calculated_balance);
		}
	
		if ($show_all == 1){
			include 'test3.php';
		} elseif (($show_all == 0) && ($show_customer_line_items == 1)) {
			include 'test3.php';
		}
	}

}

if ($_GET['bid']){
	?>
		<h2># of correct card balances: <?php echo $number_of_correct_card_balances; ?></h2>
		<h2># incorrect card balances: <?php echo $number_of_not_correct_balances; ?></h2>
	<?php
} else {
	echo "<h3>Select the business you would like to rebalance</h3>";
}
?>


<?php

$data_value = serialize($fix_customers);

$_SESSION["customer_data_value"] = $data_value;

?>

</body>

</html>