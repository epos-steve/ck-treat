<?php

/////////////////////////////////
//kiosk commissary orders
//for static menu
/////////////////////////////////
$ordertype = $_GET["ordertype"];
$date2 = date("Y-m-d H:i:s");

define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');
require_once( DOC_ROOT . '/ta/lib/class.menu_items.php' );
require_once( DOC_ROOT . '/ta/lib/class.Kiosk.php' );
//ini_set("display_errors","true");

echo "Begin Order for type $ordertype....<br>";

////select all machines linked to businesses
$query = "SELECT * FROM machine_bus_link WHERE ordertype = $ordertype";
$result = Treat_DB_ProxyOldProcessHost::query($query);

while($r = mysql_fetch_array($result)){
    $businessid = $r["businessid"];
    $machine_num = $r["machine_num"];
	$machine_array[0] = $machine_num;

	echo "<b>Start Machine# $machine_num....</b><br>";

    ///find route of machine
    $query2 = "SELECT machineid,login_routeid,menu_typeid,name FROM vend_machine WHERE machine_num = '$machine_num' AND is_deleted = '0'";
    $result2 = Treat_DB_ProxyOldProcessHost::query($query2);

    $machineid = mysql_result($result2,0,"machineid");
    $login_routeid = mysql_result($result2,0,"login_routeid");
	$menu_typeid = mysql_result($result2,0,"menu_typeid");
	$name = mysql_result($result2,0,"name");

    ///find locationid of machine
    $query2 = "SELECT locationid,route FROM login_route WHERE login_routeid = '$login_routeid'";
    $result2 = Treat_DB_ProxyOldProcessHost::query($query2);

    $locationid = mysql_result($result2,0,"locationid");
	$route = mysql_result($result2,0,"route");

    ///location details
    $query2 = "SELECT businessid FROM vend_locations WHERE locationid = '$locationid'";
    $result2 = Treat_DB_ProxyOldProcessHost::query($query2);

    $order_busid = mysql_result($result2,0,"businessid");

    ///static menu?
    $query2 = "SELECT static_menu FROM menu_type WHERE menu_typeid = '$menu_typeid'";
    $result2 = Treat_DB_ProxyOldProcessHost::query($query2);

    $static_menu = mysql_result($result2,0,"static_menu");

    ///order date for commissary
	if($ordertype == 1){
		$today = date("Y-m-d");
		$order_date = $today;
		$order_date = nextday($order_date);
		$order_date = nextday($order_date);
		$order_date = nextday($order_date);
		if(dayofweek($order_date)=="Monday" || dayofweek($order_date)=="Saturday" || dayofweek($order_date)=="Sunday"){
			$order_date = nextday($order_date);
			$order_date = nextday($order_date);
		}
	}
	///all other order dates
	else{
		$today = date("Y-m-d");
		$order_date = $today;
		$order_date = nextday($order_date);
		if(dayofweek($order_date)=="Monday"){$order_date = prevday($order_date);}
		elseif(dayofweek($order_date)=="Saturday"){
			$order_date = nextday($order_date);
			$order_date = nextday($order_date);
		}
	}

    $query2 = "SELECT COUNT(catercloseid) AS totalcount FROM caterclose WHERE businessid = '$order_busid' AND date >= '$today' AND date <= '$order_date'";
    $result2 = Treat_DB_ProxyOldProcessHost::query($query2);

    $closed_days = mysql_result($result2,0,"totalcount");

    for($counter=1;$counter<=$closed_days;$counter++){$order_date=nextday($order_date);}

    ///is date an order date?
    if(dayofweek($order_date)=="Monday"){$day=1;}
    elseif(dayofweek($order_date)=="Tuesday"){$day=2;}
    elseif(dayofweek($order_date)=="Wednesday"){$day=3;}
    elseif(dayofweek($order_date)=="Thursday"){$day=4;}
    elseif(dayofweek($order_date)=="Friday"){$day=5;}

    $query2 = "SELECT id FROM vend_machine_schedule WHERE machineid = '$machineid' AND day = '$day'";
    $result2 = Treat_DB_ProxyOldProcessHost::query($query2);
    $num2 = mysql_numrows($result2);

    ///go ahead and order
    if($num2 == 1 && $machineid > 0){

		$email_message = "";

        ///get menu items
        $query2 = "SELECT * FROM menu_items_new WHERE businessid = $businessid AND active = 0 AND ordertype = $ordertype";
        $result2 = Treat_DB_ProxyOldProcessHost::query($query2);

        while($r2 = mysql_fetch_array($result2)){
            $item_code = $r2["item_code"];
            $item_name = $r2["name"];
            $pos_id = $r2["pos_id"];
            $max = $r2["max"];
            $reorder_point = $r2["reorder_point"];
            $reorder_amount = $r2["reorder_amount"];

            ////get dates
            $dates = Kiosk::last_inv_dates($machine_array,$item_code);
            $date = $dates[1];
            if($date==""){$date=$dates[0];}

            ////last count
            $onhand2 = Kiosk::inv_onhand($machine_array, $item_code, $date);

            ////fills
            $fills = Kiosk::fills($machine_array, $item_code, $date, $date2);

            ////waste
            $waste = Kiosk::waste($machine_array, $item_code, $date, $date2);

            ////get check detail
            $items_sold = menu_items::items_sold($pos_id,$businessid,$date,$date2);

            $onhand = $onhand2 + $fills - $waste - $items_sold;

            /////REORDER
            if($reorder_point == 0 && $max != 0){
                $reorder = $max - $onhand;
            }
            elseif($max != 0){
                if($onhand <= $reorder_point){
                    if($reorder_amount != 0 && $reorder_amount > ($max - $onhand)){
                        $reorder = $reorder_amount;
                    }
                    else{
                        $reorder = $max - $onhand;
                    }
                }
				else{$reorder = 0;}
            }
            else{$reorder = 0;}
            if($reorder > $max){$reorder = $max;}
            if($reorder <= 0){$reorder=0;}

            ///////////////////////////
            ///////create order////////
            ///////////////////////////
            if($reorder>0 && $item_code != "" && $item_code != "0"){
                echo "You should order $reorder $item_name(s) [$item_code] for Mach# $machine_num (onhand: $onhand)<br>";

				//////commissary
				if($ordertype == -1){
					if($static_menu == 1){$datequery = "vend_item.date = '0000-00-00'";}
					else{$datequery = "vend_item.date = '$order_date'";}

					$query3 = "SELECT vend_item.vend_itemid FROM vend_item,menu_items WHERE menu_items.businessid = '$order_busid' AND menu_items.menu_typeid = $menu_typeid AND menu_items.item_code = '$item_code' AND menu_items.menu_item_id = vend_item.menu_itemid AND $datequery";
					$result3 = Treat_DB_ProxyOldProcessHost::query($query3);
					$num3 = mysql_numrows($result3);

					$vend_itemid = mysql_result($result3,0,"vend_itemid");

					if($num3!=0){
						echo "&nbsp;&nbsp;&nbsp;&nbsp;<font color=green>Item seems to be on the menu!</font><br>";
					
						///insert/update vend_order
						$query3 = "SELECT vend_orderid FROM vend_order WHERE vend_itemid = $vend_itemid AND login_routeid = $login_routeid AND date = '$order_date' AND machineid = $machineid";
						$result3 = Treat_DB_ProxyOldProcessHost::query($query3);
						$num3 = mysql_num_rows($result3);
					
						if($num3 == 0){
							$query3 = "INSERT INTO vend_order (businessid,amount,date,login_routeid,vend_itemid,locationid,machineid)
								VALUES ($order_busid,$reorder,'$order_date',$login_routeid,$vend_itemid,$locationid,$machineid)";
							$result3 = Treat_DB_ProxyOld::query($query3);
						}
						else{
							$vend_orderid = mysql_result($result3,0,"vend_orderid");
						
							$query3 = "UPDATE vend_order SET amount = $reorder WHERE vend_orderid = $vend_orderid";
							$result3 = Treat_DB_ProxyOld::query($query3);
						}
						echo "<font size=1>$query3</font><br>";
					}
					/////item not found
					else{echo "&nbsp;&nbsp;&nbsp;&nbsp;<font color=red>Item does not seem to be on the menu.</font><br>";}
				}

				////email beverages
				$email_message .= "<tr><td style=\"border:1px solid black;\">&nbsp;$item_code</td><td style=\"border:1px solid black;\">&nbsp;$item_name</td><td style=\"border:1px solid black;\">&nbsp;$reorder</td></tr>\r\n";
            }
        }

		////email order
		if($ordertype > 0){
			if($email_message != ""){
				$query4 = "SELECT district.email_orders FROM business,district WHERE business.businessid = $businessid AND business.districtid = district.districtid";
				$result4 = Treat_DB_ProxyOldProcessHost::query($query4);

				$email = mysql_result($result4,0,"email_orders");

				$day = dayofweek($order_date);
				$email_message = "<html><head></head><body>Machine #$machine_num Order for $day $order_date<table cellspacing=0 cellpadding=0 style=\"border:1px solid black;\" width=100%><tr style=\"background:black;\"><td colspan=3 style=\"border:1px solid black;\"><font color=white><b>$machine_num</b></font></td></tr>$email_message</table></body></html>";

				$todayis = date("l, F j, Y, g:i a") ;
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				$headers .= "From: orders@companykitchen.com\r\n";
				$subject = "Route $route [$name] Order";
				//$email="smartin@essentialpos.com.test-google-a.com";

				mail($email, $subject, $email_message, $headers);

				echo "Email sent to $email.<br>";
			}
		}
    }
	else{
		$day = dayofweek($order_date);
		echo "Machine $machine_num is not scheduled to order on $day $order_date. <br>";
	}
}

echo "Done";
?>