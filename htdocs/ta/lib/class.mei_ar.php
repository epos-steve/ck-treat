<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');
error_reporting(0);

ini_set("display_errors","true");

function subchildren($child,$level){
    global $array_count;
    global $row_count;
	global $businessid;
    global $invoiceid;
	global $inv_num;
	global $accountid;
	global $customer;
	global $inv_date;
	global $item_code;
	global $item_name;
	global $quantity;
	global $price;
	global $tax;
	global $route;
	global $ponum;
	global $cost;
	global $mainprice;
	global $cogs;
	global $lostrev;
	global $lineAR;
	global $comment;

	global $current_invoice;
	global $current_bid;
	global $current_accountid;
	global $current_date;
	global $current_inv_num;
	global $current_route;
	global $current_ponum;
	global $current_cust;

    foreach($child->children() as $subchild){

        ////global counters
        if($subchild->getName() == "Row"){$array_count++;$row_count=0;}
        if($subchild->getName() == "Data"){$row_count++;}

        if($subchild->getName() == "Data"){
			$businessid[$array_count] = $current_bid;
			$accountid[$array_count] = $current_accountid;
			$inv_date[$array_count] = $current_date;
			$inv_num[$array_count] = $current_inv_num;
			$route[$array_count] = $current_route;
			$ponum[$array_count] = $current_ponum;
			$customer[$array_count] = $current_cust;

            if($row_count == 1){$item_code[$array_count] = "$subchild";}
			elseif($row_count == 2){$item_name[$array_count] = "$subchild";}
			elseif($row_count == 3){$quantity[$array_count] = "$subchild";}
			elseif($row_count == 4){$cost[$array_count] = "$subchild";}
			elseif($row_count == 5){$mainprice[$array_count] = "$subchild";}
			elseif($row_count == 6){$price[$array_count] = "$subchild";}
			elseif($row_count == 7){$cogs[$array_count] = "$subchild";}
			elseif($row_count == 8){$lostrev[$array_count] = "$subchild";}
			elseif($row_count == 9){$tax[$array_count] = "$subchild";}
			elseif($row_count == 10){$lineAR[$array_count] = "$subchild";}
			elseif($row_count == 11){$comment[$array_count] = "$subchild";}
			elseif($row_count == 12){
				$current_bid = $item_code[$array_count];
				$current_accountid = $item_name[$array_count];
				$current_date = substr($cost[$array_count],0,10);
				$current_inv_num = $mainprice[$array_count];
				$current_cust = $quantity[$array_count];
				////reassign
				$businessid[$array_count] = $current_bid;
				$accountid[$array_count] = $current_accountid;
				$inv_date[$array_count] = $current_date;
				$inv_num[$array_count] = $current_invoice;
				$item_code[$array_count] = $price[$array_count];
				$item_name[$array_count] = $cogs[$array_count];
				$quantity[$array_count] = $lostrev[$array_count];
				$tax[$array_count] = $route[$array_count];
				$price[$array_count] = "$subchild";
				$customer[$array_count] = $current_cust;

			}
			elseif($row_count == 13){$lostrev[$array_count] = "$subchild";}
			elseif($row_count == 14){$tax[$array_count] = "$subchild";}
			elseif($row_count == 15){$route[$array_count] = "$subchild"; $current_route = "$subchild";}
			elseif($row_count == 16){$lineAR[$array_count] = "$subchild";}
			elseif($row_count == 17){$ponum[$array_count] = "$subchild"; $current_ponum = "$subchild";}
			elseif($row_count == 18){$comment[$array_count] = "$subchild";}
        }

        subchildren($subchild,$level+1);
    }
}

$array_count = 0;
$row_count = 0;
$businessid = array();
$invoiceid = array();
$inv_num = array();
$accountid = array();
$customer = array();
$inv_date = array();
$item_code = array();
$item_name = array();
$quantity = array();
$cost = array();
$mainprice = array();
$cogs = array();
$lostrev = array();
$lineAR = array();
$price = array();
$tax = array();
$route = array();
$ponum = array();
$comment = array();
$current_invoice = 0;
$current_bid = 0;
$current_accountid = 0;
$current_date = "";
$current_inv_num = 0;
$current_route = "";
$current_ponum = "";
$current_cust = "";

////file location
$xml = simplexml_load_file(DIR_FTP_UPLOADS."/TreatAmerica/VendingAR/2106.xml");

///main loop
foreach($xml->children() as $child){
    if($child->getName() == "Worksheet"){subchildren($child,1);}
}

///display
$credits = array();
$debits = array();
$credit_bid = array();
$debit_bid = array();
$credit_cid = array();
$debit_cid = array();

$last_inv = 0;
$today = date("Y-m-d");

echo "<table width=100% border=1>";
echo "<tr><td><b>Businessid</b></td><td><b>Account</b></td><td><b>Customer</b></td><td><b>Date</b></td><td><b>Invoice#</b></td><td><b>Item Code</b></td><td><b>Item</b></td></td><td><b>AR Amount</b></td><td><b>Route</b></td></tr>";
for($counter=1;$counter<=$array_count;$counter++){
	if($last_inv != $inv_num[$counter]){
		$color="#E8E7E7";
		
		///update totals of last invoice
		if($last_inv != 0){
			$query = "UPDATE invoice SET total = '$inv_total' WHERE invoiceid = $invoiceid";
			$result = Treat_DB_ProxyOld::query($query);
		}

		/////create new invoice
			///businessid
			$AR_id = substr($businessid[$counter],0,1);
			$query = "SELECT unitid,AR_credit,AR_debit FROM vend_locations WHERE AR_id = $AR_id";
			$result = Treat_DB_ProxyOld::query($query);

			$bid = mysql_result($result,0,"unitid");
			$AR_creditid = mysql_result($result,0,"AR_credit");
			$AR_debitid = mysql_result($result,0,"AR_debit");

			////companyid
			$query = "SELECT companyid FROM business WHERE businessid = $bid";
			$result = Treat_DB_ProxyOld::query($query);

			$cid = mysql_result($result,0,"companyid");

			////credit and debit businessid and companyid
			$credit_bid[$AR_creditid] = $bid;
			$debit_bid[$AR_debitid] = $bid;
			$credit_cid[$AR_creditid] = $cid;
			$debit_cid[$AR_debitid] = $cid;

			////accountid
			$query = "SELECT accountid FROM accounts WHERE businessid = $bid AND accountnum = '$accountid[$counter]'";
			$result = Treat_DB_ProxyOld::query($query);
			$num = mysql_numrows($result);

			if($num>0){
				$aid = mysql_result($result,0,"accountid");
			}
			else{
				$customer[$counter] = str_replace("'","`",$customer[$counter]);

				$query = "INSERT INTO accounts (businessid,companyid,name,accountnum,export,note) VALUES ($bid,$cid,'$customer[$counter]','$accountid[$counter]',1,'Created from MEI Import')";
				$result = Treat_DB_ProxyOld::query($query);
				$aid = mysql_insert_id();
				echo "$query, $aid<br>";
			}

			////create invoice
			$inv_total = 0;
			$query = "INSERT INTO invoice (businessid,companyid,accountid,date,type,status,taxable,createdate,createby,route,invoice_num)
					VALUES
					($bid,$cid,$aid,'$inv_date[$counter]',5,4,'off','$inv_date[$counter]','MEI','$route[$counter]','$inv_num[$counter]')";
			$result = Treat_DB_ProxyOld::query($query);
			$invoiceid = mysql_insert_id();
	}
	else{
		$color="white";
	}

	////display and update invoicedetail
    if($lineAR[$counter]!=0){
		echo "<tr bgcolor=$color><td>$businessid[$counter]</td><td>$accountid[$counter]</td><td>$customer[$counter]</td><td>$inv_date[$counter]</td><td>$inv_num[$counter]</td><td>$item_code[$counter]</td><td>$quantity[$counter] $item_name[$counter]</td><td>$lineAR[$counter]</td><td>$route[$counter]</td></tr>";

		////insert invoice details
		$item_name[$counter] = str_replace("'","`",$item_name[$counter]);

		$query = "INSERT INTO invoicedetail (invoiceid,qty,item,price,item_code)
			VALUES
			($invoiceid,1,'$quantity[$counter] $item_name[$counter]','$lineAR[$counter]','$item_code[$counter]')";
		$result = Treat_DB_ProxyOld::query($query);

		$inv_total += round($lineAR[$counter],2);
		$credits[$AR_creditid][$inv_date[$counter]] += $lineAR[$counter];
		$debits[$AR_debitid][$inv_date[$counter]] += $lineAR[$counter];
	}

	$last_inv = $inv_num[$counter];
}

/////update last invoice
if($invoiceid>0){
	$query = "UPDATE invoice SET total = '$inv_total', posted = 1 WHERE invoiceid = $invoiceid";
	$result = Treat_DB_ProxyOld::query($query);
}

////credits and debits
foreach($credits AS $key => $value){
	foreach($credits[$key] AS $date => $amount){
		$query = "INSERT INTO creditdetail (companyid,businessid,amount,date,creditid)
			VALUES ($credit_cid[$key],$credit_bid[$key],'$amount','$date',$key) ON DUPLICATE KEY UPDATE amount = '$amount'";
		$result = Treat_DB_ProxyOld::query($query);
	}
}
foreach($debits AS $key => $value){
	foreach($debits[$key] AS $date => $amount){
		$query = "INSERT INTO debitdetail (companyid,businessid,amount,date,debitid)
			VALUES ($debit_cid[$key],$debit_bid[$key],'$amount','$date',$key) ON DUPLICATE KEY UPDATE amount = '$amount'";
		$result = Treat_DB_ProxyOld::query($query);
	}
}

echo "</table>";
echo "Done";
?>