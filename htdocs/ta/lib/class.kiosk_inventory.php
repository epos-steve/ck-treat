<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');
error_reporting(0);

ini_set("display_errors","true");

function subchildren($child,$level){
    global $array_count;
    global $row_count;
    global $data_name;
    global $data_number;
    global $data_amount;
    global $data_cost;
    global $data_pulled;
    global $data_fill;
    global $data_waste;
    global $data_machine;
    global $data_time;
    global $is_inv;
    global $data_is_inv;
    global $current_machine;
    global $ticket_time;

    foreach($child->children() as $subchild){

        ////global counters
        if($subchild->getName() == "Row"){$array_count++;$row_count=0;}
        if($subchild->getName() == "Data"){$row_count++;}

        if($subchild->getName() == "Data"){
            $data_machine[$array_count] = $current_machine;
            $data_time[$array_count] = $ticket_time;
            $data_is_inv[$array_count] = $is_inv;
            
            if($row_count == 1){$data_name[$array_count] = "$subchild";}
            elseif($row_count == 2){$data_number[$array_count] = "$subchild";}
            elseif($row_count == 3){$data_amount[$array_count] = "$subchild";}
            elseif($row_count == 4){$data_cost[$array_count] = "$subchild";}
            elseif($row_count == 5){$data_pulled[$array_count] = "$subchild";}
            elseif($row_count == 6){$data_fill[$array_count] = "$subchild";}
            elseif($row_count == 7){$data_waste[$array_count] = "$subchild";}
            elseif($row_count == 8){
                $data_machine[$array_count] = $data_name[$array_count];
                $current_machine = $data_machine[$array_count];
                $ticket_time = $data_amount[$array_count];

                $ticket_time = str_replace("T"," ",$ticket_time);
                $ticket_time = str_replace("Z","",$ticket_time);

                $data_time[$array_count] = $ticket_time;
                $data_name[$array_count] = $data_cost[$array_count];
                $data_number[$array_count] = $data_pulled[$array_count];
                $data_amount[$array_count] = $data_fill[$array_count];
                $data_cost[$array_count] = $data_waste[$array_count];
                $data_pulled[$array_count] = "$subchild";
            }
            elseif($row_count == 9){$data_fill[$array_count] = "$subchild";}
            elseif($row_count == 10){$data_waste[$array_count] = "$subchild";}
            elseif($row_count == 11){$data_is_inv[$array_count] = "$subchild"; $is_inv = "$subchild";}
        }

        subchildren($subchild,$level+1);
    }
}

$array_count = 0;
$row_count = 0;
$data_name = array();
$data_number = array();
$data_amount = array();
$data_cost = array();
$data_fill = array();
$data_waste = array();
$data_pulled = array();
$data_machine = array();
$data_time = array();
$data_is_inv = array();
$current_machine = 0;
$ticket_time = "";
$is_inv = 0;

$xml = simplexml_load_file(DIR_FTP_UPLOADS . "/TreatAmerica/KioskInventory/KioskInventory.xml");
//$xml = simplexml_load_file("/home/espos/www/TreatAmerica/KioskInventory/KioskTickets091810.xml");

//echo $xml->getName() . "<br />";

foreach($xml->children() as $child){
    //echo $child->getName() . ": " . $child . "<br />";
    if($child->getName() == "Worksheet"){subchildren($child,1);}
}

///display
echo "<table width=100%>";
for($counter=1;$counter<=$array_count;$counter++){
    echo "<tr><td>$data_machine[$counter]</td><td>$data_time[$counter]</td><td>$data_number[$counter]</td><td>$data_name[$counter]</td><td>$data_amount[$counter]</td><td>$data_cost[$counter]</td><td>$data_pulled[$counter]</td><td>$data_fill[$counter]</td><td>$data_waste[$counter]</td><td>$data_is_inv[$counter]</td></tr>";

    $query = "INSERT INTO inv_count_vend (machine_num,date_time,item_code,onhand,unit_cost,pulled,fills,waste,is_inv)
        VALUES ('$data_machine[$counter]','$data_time[$counter]','$data_name[$counter]',$data_amount[$counter],$data_cost[$counter],$data_pulled[$counter],$data_fill[$counter],$data_waste[$counter],$data_is_inv[$counter])
        ON DUPLICATE KEY UPDATE onhand = '$data_amount[$counter]', unit_cost = '$data_cost[$counter]', pulled = '$data_pulled[$counter]', fills = '$data_fill[$counter]', waste = '$data_waste[$counter]', is_inv = '$data_is_inv[$counter]'";
    $result = Treat_DB_ProxyOld::query($query);

	/////activate menu items
	$query = "SELECT businessid FROM machine_bus_link WHERE machine_num = '$data_machine[$counter]'";
	$result = Treat_DB_ProxyOldProcessHost::query($query);

	$mach_bus = mysql_result($result,0,"businessid");

	if($mach_bus > 0){
		$query = "UPDATE menu_items_new SET active = 0 WHERE businessid = $mach_bus AND item_code = '$data_name[$counter]'";
		$result = Treat_DB_ProxyOld::query($query);
	}

}
echo "</table>";
echo "Done";
?>