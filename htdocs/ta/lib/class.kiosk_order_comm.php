<?php

/////////////////////////////////
//kiosk commissary orders
/////////////////////////////////
$date2 = date("Y-m-d");
$yesterday = date( "Y-m-d", mktime( 0, 0, 0, date( "m" ), date( "d" ) - 1, date( "Y" ) ) );

define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');
require_once( DOC_ROOT . '/ta/lib/class.menu_items.php' );
require_once( DOC_ROOT . '/ta/lib/class.Kiosk.php' );
//ini_set("display_errors","true");

echo "Begin Order for Commissary....<br>";

////select all machines linked to businesses
$query5 = "SELECT machine_bus_link.* FROM machine_bus_link
			JOIN business ON business.businessid = machine_bus_link.businessid AND (business.companyid = 3 OR business.companyid = 6)
			WHERE machine_bus_link.ordertype = 1";
$result5 = Treat_DB_ProxyOldProcessHost::query($query5);

while($r5 = mysql_fetch_array($result5)){
    $businessid = $r5["businessid"];

	$query = "SELECT
							machine_bus_link.machine_num
							,vend_machine.name
							,vend_machine.login_routeid
							,vend_machine.menu_typeid
							,vend_machine.machineid
							,login_route.route
							,login_route.locationid
						FROM machine_bus_link
						JOIN vend_machine ON vend_machine.machine_num = machine_bus_link.machine_num AND vend_machine.is_deleted = 0
						JOIN login_route ON login_route.login_routeid = vend_machine.login_routeid
						WHERE machine_bus_link.businessid = $businessid AND machine_bus_link.ordertype = 1";
			$result = Treat_DB_ProxyOldProcessHost::query($query);

			$machine_num = @mysql_result($result,0,"machine_num");
			$name = @mysql_result($result,0,"name");
			$menu_typeid = @mysql_result($result,0,"menu_typeid");
			$login_routeid = @mysql_result($result,0,"login_routeid");
			$machineid = @mysql_result($result,0,"machineid");
			$route = @mysql_result($result,0,"route");
			$locationid = @mysql_result($result,0,"locationid");

			if($machine_num != ''){
				$date2 = date("Y-m-d");

				$ship_dates = Kiosk::comm_inTransit_dates($machine_num);

				$machine_num[0] = $machine_num;

				/////get ordering businessid
				$query = "SELECT businessid FROM menu_type WHERE menu_typeid = $menu_typeid";
				$result = Treat_DB_ProxyOldProcessHost::query($query);

				$order_bus = mysql_result($result,0,"businessid");

				////correct order date
				$hour = date("H");
				$order_date = $date2;

				/////move to Monday if weekend
				if(dayofweek($order_date) == "Saturday"){$order_date = nextday($order_date);$order_date = nextday($order_date);}
				if(dayofweek($order_date) == "Sunday"){$order_date = nextday($order_date);}

				///always 2 days in advance
				$order_date = nextday($order_date);
				$order_date = nextday($order_date);

				////if date lands on weekend
				if(dayofweek($order_date) == "Saturday" || dayofweek($order_date) == "Sunday"){$order_date = nextday($order_date);$order_date = nextday($order_date);}

				///if after 10
				if($hour >= 10){$order_date = nextday($order_date);}
				if(dayofweek($order_date) == "Saturday"){$order_date = nextday($order_date);$order_date = nextday($order_date);}

				////scheduled?
				if(dayofweek($order_date) == "Monday"){$day = 1;}
				elseif(dayofweek($order_date) == "Tuesday"){$day = 2;}
				elseif(dayofweek($order_date) == "Wednesday"){$day = 3;}
				elseif(dayofweek($order_date) == "Thursday"){$day = 4;}
				elseif(dayofweek($order_date) == "Friday"){$day = 5;}

				$query = "SELECT * FROM vend_machine_schedule WHERE machineid = $machineid AND day = $day";
				$result = Treat_DB_ProxyOldProcessHost::query($query);

				$scheduleid = mysql_result($result,0,"id");

				///next order
				$query = "SELECT * FROM vend_machine_schedule WHERE machineid = $machineid ORDER BY day";
				$result = Treat_DB_ProxyOldProcessHost::query($query);

				while($r = mysql_fetch_array($result)){
					$mach_schedule[$r["day"]] = $r["id"];
				}

				$date_tmp = date("Y-m-d");
				if($hour >= 10){$date_tmp = nextday($date_tmp);}

				$date_count2 = 1;
				while($date_tmp <= $order_date){
					$date_count2++;
					$date_tmp = nextday($date_tmp);
				}

				$next_order_date = nextday($order_date);
				for($date_count = 1; $date_count <= 7; $date_count++){
					////scheduled?
					if(dayofweek($next_order_date) == "Monday"){$next_order_day = 1;}
					elseif(dayofweek($next_order_date) == "Tuesday"){$next_order_day = 2;}
					elseif(dayofweek($next_order_date) == "Wednesday"){$next_order_day = 3;}
					elseif(dayofweek($next_order_date) == "Thursday"){$next_order_day = 4;}
					elseif(dayofweek($next_order_date) == "Friday"){$next_order_day = 5;}
					elseif(dayofweek($next_order_date) == "Saturday"){$next_order_day = 6;}
					elseif(dayofweek($next_order_date) == "Sunday"){$next_order_day = 7;}

					////if schedule exists
					if(array_key_exists( $next_order_day, $mach_schedule)){
						break;
					}

					$next_order_date = nextday($next_order_date);
				}

				///remove weekends if not 24 operation
				$temp_date = $order_date;
				$temp_days = $date_count;
				while($temp_date <= $next_order_date){
					if($operating_days <= 6 && dayofweek($temp_date) == "Sunday"){$temp_days--;}
					elseif($operating_days <= 5 && dayofweek($temp_date) == "Saturday"){$temp_days--;}
					$temp_date = nextday($temp_date);
				}

				///operating days until order arrives
				$temp_date2 = nextday($date2);
				$temp_days2 = 0;
				while($temp_date2 <= $order_date){
					$temp_days2++;
					if($operating_days <= 6 && dayofweek($temp_date2) == "Sunday"){$temp_days2--;}
					elseif($operating_days <= 5 && dayofweek($temp_date2) == "Saturday"){$temp_days2--;}
					$temp_date2 = nextday($temp_date2);
				}

				///display order
				$showday = dayofweek($order_date);
				$shownextday = dayofweek($next_order_date);

				if($scheduleid > 0){
					$query = "SELECT operate FROM business WHERE businessid = $businessid";
					$result = Treat_DB_ProxyOldProcessHost::query($query);

					$operating_days = mysql_result($result,0,"operate");

					$fourteen_days = date("Y-m-d", mktime(0, 0, 0, date("m") , date("d")-14, date("Y")));

					$query = "SELECT
								menu_groups.groupname
								,menu_pricegroup.menu_pricegroupname
								,menu_items.item_name
								,menu_items.menu_item_id
								,menu_items.item_code
								,menu_items_new.id AS myid
								,menu_items_new.pos_id
								,menu_items_new.rollover
								,vend_item.vend_itemid
								,vend_order.amount
								,vend_order.vend_orderid
							FROM menu_items
							JOIN menu_pricegroup ON menu_pricegroup.menu_pricegroupid = menu_items.groupid
							JOIN menu_groups ON menu_groups.menu_group_id = menu_pricegroup.menu_groupid
							JOIN vend_item ON vend_item.menu_itemid = menu_items.menu_item_id AND vend_item.date = '$order_date'
							LEFT JOIN vend_order ON vend_order.vend_itemid = vend_item.vend_itemid AND vend_order.login_routeid = $login_routeid AND vend_order.date = '$order_date' AND vend_order.machineid = $machineid
							LEFT JOIN menu_items_new ON menu_items_new.item_code = menu_items.item_code AND menu_items_new.businessid = $businessid
							WHERE menu_items.businessid = $order_bus AND menu_items.menu_typeid = $menu_typeid
							ORDER BY menu_groups.orderid,menu_pricegroup.orderid";
					$result = Treat_DB_ProxyOldProcessHost::query($query);

					while($r = mysql_fetch_array($result)){
						$menu_itemid = $r["menu_item_id"];
						$item_name = $r["item_name"];
						$item_code = $r["item_code"];
						$pgroup_name = $r["menu_pricegroupname"];
						$group_name = $r["groupname"];
						$myid = $r["myid"];
						$pos_id = $r["pos_id"];
						$rollover = $r["rollover"];
						$vend_itemid = $r["vend_itemid"];
						$amount = $r["amount"];
						$vend_orderid = $r["vend_orderid"];

						/////////////////////
						////onhand amounts///
						/////////////////////
						////get dates
						$dates = Kiosk::last_inv_dates($machine_num,$item_code);
						$date = $dates[1];
						if($date==""){$date=$dates[0];}

						////last count
						$onhand2 = Kiosk::inv_onhand($machine_num, $item_code, $date);

						////fills
						$fills = Kiosk::fills($machine_num, $item_code, $date, $date2);

						////waste
						$waste = Kiosk::waste($machine_num, $item_code, $date, $date2);

						////get check detail
						$items_sold = menu_items::items_sold($pos_id,$businessid,$date,$date2,$current_terminal);

						////in transit
						if(count($ship_dates) > 0){
							$inTransit = Kiosk::comm_inTransit_amt($ship_dates,$item_code,$menu_typeid,$order_bus,$machineid);
						}
						else{$inTransit = 0;}

						if($inTransit == 0){$inTransit = 0;}

						////average daily sales for last 2 weeks
						$items_sold14 = menu_items::items_sold($pos_id,$businessid,$fourteen_days,$yesterday,$current_terminal);

						$avg_daily_sales = round($items_sold14/($operating_days*2), 1);

						////onhand amount
						$onhand = $onhand2 + $fills - $waste - $items_sold;

						if(dayofweek($order_date) == "Friday"){$reduce = .7;}
						else{$reduce = 1;}

						if($onhand < 0){$onhand = 0;}

						////suggested
						//$suggested = round($rollover * (round($avg_daily_sales * $temp_days) - $onhand - $inTransit + round($avg_daily_sales * $temp_days2)));
						$suggested = round( (round( $rollover * $avg_daily_sales * $date_count ) - ($onhand + $inTransit)) * $reduce );

						if($suggested == 1){$suggested = 2;}
						elseif($suggested < 1){$suggested = 0;}

						echo "$item_code,$suggested<br>";

						if($vend_orderid > 0){
							$query2 = "UPDATE vend_order SET amount = '$suggested' WHERE vend_orderid = $vend_orderid";
							$result2 = Treat_DB_ProxyOld::query($query2);
						}
						elseif($suggested != 0){
							$query2 = "INSERT INTO vend_order (businessid,amount,date,login_routeid,vend_itemid,locationid,machineid) VALUES
												($order_bus,'$suggested','$order_date',$login_routeid,$vend_itemid,$locationid,$machineid)";
							$result2 = Treat_DB_ProxyOld::query($query2);
						}
						else{
							echo "skipped because no previous record or order is zero<br>";
						}
						echo $query2 . "<br>";

					}
				}
				else{
					echo "<p><font color=red>This date is not a scheduled stop.</font><p>";
				}
			}
			else{
				echo "No machine setup for Cold Food ($businessid)<br>";
			}
}

echo "Done";
?>