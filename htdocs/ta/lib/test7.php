<!DOCTYPE HTML PUBLIC   "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
	<style type="text/css">
		#customer-header { width: 825px; }
		#customer-header-id { float: left; width: 125px; }
		#customer-header-stored { float: left; width: 125px; }
		#customer-header-calculated { float: left; width: 150px; }
		#customer-header-oob { float: left; width: 125px; }
		#customer-header-scancode { float: left; width: 125px; }
		#customer-header-name { float: left; width: 125px; }

		#customer-data { width: 825px; }
		.customer-id { float: left; width: 125px; }
		.customer-stored { float: left; width: 125px; }
		.customer-calculated { float: left; width: 150px; }
		.customer-oob { float: left; width: 125px; }
		.customer-scancode { float: left; width: 125px; }
		.customer-name { float: left; width: 125px; }
	</style>
</head>
<body>

<?php

define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');

$bid = $_GET['bid'];
$rebalance_accounts = $_GET['rebalance'];

$sql1 = "SELECT czt.customerid, czt.stored_balance, czt.calculated_balance, czt.oob_amount, czt.scancode, CONCAT(c.first_name, ' ', c.last_name) AS customer_name FROM mburris_businesstrack.customer_oob czt INNER JOIN mburris_businesstrack.customer c USING(customerid) WHERE (czt.businessid = $bid) AND ";

	switch ($rebalance_accounts) {
		case 1:
			$where_2 = "(czt.oob_amount BETWEEN -1 AND 1) AND (czt.oob_amount != 0)";
		break;

		case 2:
			$where_2 = "(czt.oob_amount BETWEEN -2 AND 2) AND (czt.oob_amount != 0)";
		break;

		case 5:
			$where_2 = "(czt.oob_amount BETWEEN -5 AND 5) AND (czt.oob_amount != 0)";
		break;

		case 10:
			$where_2 = "(czt.oob_amount BETWEEN -10 AND 10) AND (czt.oob_amount != 0)";
		break;

		case 20:
			$where_2 = "(czt.oob_amount BETWEEN -20 AND 20) AND (czt.oob_amount != 0)";
		break;

		default:
			$where_2 = "(czt.oob_amount != 0)";
		break;

	}
	$sql1 .= $where_2;
	$result1 = Treat_DB_ProxyOldProcessHost::query($sql1);
	$total_rows = Treat_DB_ProxyOldProcessHost::mysql_num_rows($result1);
	if ($total_rows > 0){
		echo "<h3>Rebalanced Accounts</h3>";
	
	?>
	<table id="customer-header">
		<tr>
			<td id="customer-header-name">Customer Name</td>
			<td id="customer-header-scancode">Scancode</td>
			<td id="customer-header-id">Customer ID</td>
			<td id="customer-header-stored">Stored Balance</td>
			<td id="customer-header-calculated">Calculated Balance</td>
			<td id="customer-header-oob">OOB Amount</td>
		</tr>
	<?php 
	while ($r = Treat_DB_ProxyOldProcessHost::mysql_fetch_array($result1)){
		$customerid = $r['customerid'];
		$stored_balance = $r['stored_balance'];
		$calculated_balance = $r['calculated_balance'];
		$oob_amount = $r['oob_amount'];
		$scancode = $r['scancode'];
		$customer_name = $r['customer_name'];
		
		?>
		<tr>
			<td class="customer-name"><?php echo $customer_name; ?></td>
			<td class="customer-scancode"><?php echo $scancode; ?></td>
			<td class="customer-id"><?php echo $customerid; ?></td>
			<td class="customer-stored"><?php echo $stored_balance; ?></td>
			<td class="customer-calculated"><?php echo $calculated_balance; ?></td>
			<td class="customer-oob"><?php echo $oob_amount; ?></td>
		</tr>
		<?php
		
		adjust_customer_balance($customerid, $calculated_balance, $stored_balance);
		
	}
	?>
	</table>
	<?php
	} else {
		echo "<h3>There were no accounts to rebalance</h3>";
	}
?>
	
</body>
</html>

<?php

function adjust_customer_balance($id, $calculated_balance, $stored_balance){

	$today = date("Y-m-d G:i:s");
	$authresponse = "EE - " . $today;
	
	if ($calculated_balance > $stored_balance) {
		
		$sql = "UPDATE mburris_businesstrack.customer SET balance = $calculated_balance WHERE customerid = $id LIMIT 1";
		$balance_adjustment_amount = ($calculated_balance - $stored_balance);
		$result = Treat_DB_ProxyOld::query($sql);

		$sql2 = "INSERT INTO customer_transactions (oid, trans_type_id, customer_id, subtotal, chargetotal, response, authresponse) VALUES ('Balanced CK Card', 9, $id, '0.00', '0.00', 'APPROVED', '$authresponse')";
		$result2 = Treat_DB_ProxyOld::query($sql2);
		
		$sql3 = "UPDATE mburris_businesstrack.customer_z_tracker SET stored_balance = $calculated_balance, oob_amount = 0, out_of_balance = 0 WHERE customerid = $id LIMIT 1";
		$result3 = Treat_DB_ProxyOld::query($sql3);
		//echo "<b>" . $sql3 . "</b><br>";
		$message = "Balance Adjustment: +" . $balance_adjustment_amount;
	
	} else if ($calculated_balance < $stored_balance) { 
		$balance_adjustment_amount = ($stored_balance - $calculated_balance);
		$sql = "INSERT INTO customer_transactions (oid, trans_type_id, customer_id, subtotal, chargetotal, response, authresponse) VALUES ('CK Adjust', 8, $id, '$balance_adjustment_amount', '$balance_adjustment_amount', 'APPROVED', '$authresponse')";
		$result = Treat_DB_ProxyOld::query($sql);
		$message = "CK Adjustment: +" . $balance_adjustment_amount;

		$sql3 = "UPDATE mburris_businesstrack.customer_z_tracker SET calculated_balance = $stored_balance, oob_amount = 0, out_of_balance = 0 WHERE customerid = $id LIMIT 1";
		$result3 = Treat_DB_ProxyOld::query($sql3);
		
	}
	
	//$result = Treat_DB_ProxyOld::query($sql);
	return $message;
}

?>