<?php
if ( !defined( 'SITECH_LIB_PATH' ) ) {
	require_once dirname( __FILE__ ) . '/../../../application/bootstrap.php';
}

if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
	/*
	 *	This file is currently only used in htdocs/ta/vendingreport3.php
	 */
	trigger_error( 'class MEI is deprecated in favor of Treat_Model_Mei_Product', E_USER_NOTICE );
}

class MEI
	extends \Essential\Treat\Lib\MEI
{
}

