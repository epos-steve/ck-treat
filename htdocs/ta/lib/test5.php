<?php

if (($oob_amount > 20) || ($oob_amount < -20)) {
	//echo "<b>" . $oob_amount . "</b><br>";
	$accounts_oob_20_plus++;
	$fix_customers_oob_20_plus[] = array($customerid => $calculated_balance);
} else if (($oob_amount > 10) || ($oob_amount < -10)) {
	$accounts_oob_20++;
	$fix_customers_oob_20[] = array($customerid => $calculated_balance);
} else if (($oob_amount > 5) || ($oob_amount < -5)) {
	$accounts_oob_10++;
	$fix_customers_oob_10[] = array($customerid => $calculated_balance);
} else if (($oob_amount > 2) || ($oob_amount < -2)) {
	$accounts_oob_5++;
	$fix_customers_oob_5[] = array($customerid => $calculated_balance);
} else if (($oob_amount > 1) || ($oob_amount < -1)) {
	$accounts_oob_2++;
	$fix_customers_oob_2[] = array($customerid => $calculated_balance);
} else if (($oob_amount > 0) || ($oob_amount < 0)) {
	$accounts_oob_1++;
	$fix_customers_oob_1[] = array($customerid => $calculated_balance);
}

?>