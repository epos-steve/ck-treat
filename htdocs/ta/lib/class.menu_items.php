<?php
# This file is DEPRECATED and simply includes the file from lib-treat.
trigger_error(
	sprintf(
		'The file you are viewing required or included [%s], most likely via an absolute path. Please fix your code.'
		,__FILE__
	), E_USER_WARNING
);
if ( !defined( 'SITECH_LIB_PATH' ) ) {
	require_once dirname( __FILE__ ) . '/../../../application/bootstrap.php';
}
require_once 'lib/class.menu_items.php';


