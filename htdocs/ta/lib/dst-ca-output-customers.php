<!DOCTYPE HTML PUBLIC   "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<style type="text/css">
			.customer-line-item { float: left; min-width: 150px; }
			.customer-header { float: left; min-width: 150px; }
			tr.new-row td { border-bottom: 1px solid black; padding-top: 20px; font-weight: bold; }
			tr.row-reload {  }
			tr.row-not-reload { font-weight: bold; }
			
			#download-form { position:absolute; top:20px; right:75px; }
		</style>
	</head>
	<body>
		<form action=dst-ca-output-customers.php method=post>
			<h3>Location:  DST CA - Output</h3>
			<b>Show Reloads Only</b><br />
			<input type="radio" name="reloads_only" value="0" checked>No &nbsp;
			<input type="radio" name="reloads_only" value="1">Yes<br /><br />
			Date From:<br /><input type="text" name="date_from"><br />
			Date To:<br /><input type="text" name="date_to"><br />
			<input type=submit value='GO Now'><br /><br />
		</form>

<?php

define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');

$download_data = "";
$download_data .= "Name,Scancode,Payment Date, Payment Total, Balance, Customer Type" . "\r\n";

if ($_POST){
	$date_from = $_POST['date_from'];
	$date_to = $_POST['date_to'];
	$reloads_only = $_POST['reloads_only'];

	$sql = "CALL swcc_report(110)";
	Treat_DB_ProxyOldProcessHost::query($sql);
	
	echo "Date From: " . $date_from . "<br />";
	echo "Date To: " . $date_to . "<br /><br />";
	//echo "<hr>";
	
	if ($reloads_only > 0){
		$sql2 = "SELECT CONCAT(first_name, ' ', last_name) AS full_name, scancode, bill_datetime, total, cust_balance, payment_type, customer_type, CONCAT(first_name, ' ', last_name, ',', scancode, ',', bill_datetime, ',', total, ',', cust_balance, ',', customer_type ) AS csv_row FROM west_college_2 WHERE (businessid = 110) AND (scancode !=  '') AND (bill_datetime BETWEEN '$date_from' AND '$date_to') AND ((payment_type IN(4,3) AND total LIKE '%.00') OR (payment_type IN(989))) ORDER BY last_name ASC, first_name ASC, bill_datetime ASC";
	} else {
		$sql2 = "SELECT CONCAT(first_name, ' ', last_name) AS full_name, scancode, bill_datetime, total, cust_balance, payment_type, customer_type, CONCAT(first_name, ' ', last_name, ',', scancode, ',', bill_datetime, ',', total, ',', cust_balance, ',', customer_type ) AS csv_row FROM west_college_2 WHERE (businessid = 110) AND (scancode !=  '') AND (bill_datetime BETWEEN '$date_from' AND '$date_to') ORDER BY last_name ASC, first_name ASC, bill_datetime ASC";
	}
	
	//echo "<h3>" . $sql2 . "</h3>";SELECT CONCAT(first_name, ' ', last_name) AS full_name, scancode, bill_datetime, total, cust_balance, payment_type, customer_type FROM west_college_2

	$result = Treat_DB_ProxyOldProcessHost::query($sql2);
	
	$current_scancode = false;
	echo "<table>";
	?><tr><td class="customer-header">Name</td><td class="customer-line-item">Scancode</td><td class="customer-line-item">Payment Date</td><td class="customer-line-item">Payment Total</td><td class="customer-line-item">Balance</td><td class="customer-line-item">Customer Type</td></tr><?php
	while ($r = mysql_fetch_array($result)){
		$full_name = $r['full_name'];
		$scancode = $r['scancode'];
		//if (!$current_scancode){
			//$current_scancode = $scancode;
		//}
		
		$bill_datetime = $r['bill_datetime'];
		$total = $r['total'];
		$cust_balance = $r['cust_balance'];
		$payment_type = $r['payment_type'];
		$customer_type = $r['customer_type'];
		$csv_row = $r['csv_row'];
		
		$sql_balance = "SELECT balance FROM customer WHERE scancode = '$scancode'";
		$sql_balance_result = Treat_DB_ProxyOldProcessHost::query($sql_balance);
		$current_balance = number_format(mysql_result($sql_balance_result, 0, 'balance'), 2);
		
		if ($current_scancode != $scancode){
			$current_scancode = $scancode;
			echo "<tr class='new-row'><td colspan=5>$full_name: $$current_balance</td></tr>";
			//echo "<tr><td>" . $full_name . "</td><td>" . $scancode . "</td><td>" . $bill_datetime . "</td><td>" . $total . "</td><td>" . $cust_balance . "</td></tr>";
		}
		
		if (($payment_type != 989) && ($payment_type != 4) && ($payment_type != 3)){
			$row_class_name = "row-reload";
		} else {
			$row_class_name = "row-not-reload";
		}
		
		?><tr class="<?php echo $row_class_name; ?>"><td class="customer-line-item"><?php echo $full_name; ?></td><td class="customer-line-item"><?php echo $scancode; ?></td><td class="customer-line-item"><?php echo $bill_datetime; ?></td><td class="customer-line-item">$<?php echo $total; ?></td><td class="customer-line-item"><?php if ($cust_balance){ echo "$" . $cust_balance; } else { echo " - - -"; } ?></td><td class="customer-line-item"><?php echo $customer_type; ?></td></tr><?php

		$download_data .= $csv_row . "\r\n";
		
	}
	echo "</table>";
	
}
?>
		<div id="download-form">
			<form action='/ta/posreport.php' method='post' style="margin:0;padding:0;display:inline;">
                <input type=hidden name=download_data value='<?php echo $download_data; ?>' />
                <input type=submit value='Download' <?php //echo $download_disable; ?> style="border: 1px solid #999999; font-size: 10px; background-color:#CCCCCC;" />
			</form>
		</div>
<?php
//echo "SWCC - Customer Balances";

?>
</body>
</html>
