<!DOCTYPE HTML PUBLIC   "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<style type="text/css">
			#accounts-rebalanced { float: right; padding: 0px 0px 0px 0px; width: 400px; }
			#accounts-rebalanced-2 { clear: both; float: right; padding: 0px 0px 0px 0px; width: 400px; }
			#rows-fixed { color: #FF0000; }
			#customer-rebalanced-header { float: right; min-width: 400px; margin: 0px 0px 10px 0px; font: 16pt bold; }
			
			.new-customer-data {  }
			.customer-infor-line {  }
			
			.customer-calculated-balance { float: right; }
			.customer-balance { float: right; }
			.customer-name { float: right; }

			.customer-calculated-balance-value { float: none; font-weight: bold; }
			.customer-balance-value { float: none; font-weight: bold; }
			.customer-name-value { float: none; font-weight: bold; }

			.customer-rebalanced-line { float: right; min-width: 400px; }
			.account-balance-summary-label { float: left; min-width: 150px; clear: left; }
			.account-balance-summary-amount { float: left; min-width: 50px; font-weight: bold; color: blue; margin: 0 20px 0 5px; text-align: right; }
			.account-balance-summary-text { float: left; min-width: 150px; }
			.customer-not-in-balance {  }
			.customer-in-balance { width: 60%; }
			
			.check-header-row-oob { border: 2px solid red; text-align: center; }
			.check-detail-row-oob { text-align: center; }
			.check-detail-row-oob-even { text-align: center; background-color: #FFCCCC; }
			.check-detail-row-oob-odd { text-align: center; background-color: #FAFAFA; }
			.customer-balance-headers-oob { font-size: 18px; color: #FF0000; font-weight: bold; }
			
			.check-header-row { border: 2px solid blue; text-align: center; }
			.check-detail-row { text-align: center; }
			.check-detail-row-even { text-align: center; background-color: #CFDDF3; }
			.check-detail-row-odd { text-align: center; background-color: #F2F2F2; }
			.customer-balance-headers { font-size: 14px; color: #0000FF; font-weight: bold; }
			.customer-balance-headers-1 { border-bottom: 1px solid blue; padding-top: 5px; color: blue; }
			.customer-balance-headers-oob-1 { border-bottom: 1px solid red; padding-top: 5px; color: red; }

		</style>

	</head>
	<body>
<?php

define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');

function adjust_customer_balance($id, $calculated_balance, $stored_balance){

	$today = date("Y-m-d G:i:s");
	$authresponse = "EE - " . $today;
	
	if ($calculated_balance > $stored_balance) {
		
		$sql = "UPDATE mburris_businesstrack.customer SET balance = $calculated_balance WHERE customerid = $id LIMIT 1";
		$balance_adjustment_amount = ($calculated_balance - $stored_balance);
		$result = Treat_DB_ProxyOld::query($sql);

		$sql2 = "INSERT INTO customer_transactions (oid, trans_type_id, customer_id, subtotal, chargetotal, response, authresponse) VALUES ('Balanced CK Card', 9, $id, '0.00', '0.00', 'APPROVED', '$authresponse')";
		$result = Treat_DB_ProxyOld::query($sql2);
		
		$message = "Balance Adjustment: +" . $balance_adjustment_amount;
	
	} else if ($calculated_balance < $stored_balance) { 
		$balance_adjustment_amount = ($stored_balance - $calculated_balance);
		$sql = "INSERT INTO customer_transactions (oid, trans_type_id, customer_id, subtotal, chargetotal, response, authresponse) VALUES ('CK Adjust', 8, $id, '$balance_adjustment_amount', '$balance_adjustment_amount', 'APPROVED', '$authresponse')";
		$result = Treat_DB_ProxyOld::query($sql);
		$message = "CK Adjustment: +" . $balance_adjustment_amount;
	}
	
	//$result = Treat_DB_ProxyOld::query($sql);
	return $message;
}

function get_amount_value($type_id, $amount){
	
	if (($type_id == 1) || ($type_id == 4) || ($type_id == 6) || ($type_id == "Promotion") || ($type_id == "Credit Card Transaction")){
		$return_value = ($amount);
	} else {
		$return_value = $amount;
	}
	
	return $return_value;
	
}

function get_payment_type($transaction_type_id){
	switch ($transaction_type_id) {
		case 1:
			$transaction_type_label = "Kiosk Cash Reload";
		break;

		case 4:
			$transaction_type_label = "Kiosk CC Reload";
		break;

		case 6:
			$transaction_type_label = "Kiosk CC Reload";
		break;

		case 14:
			$transaction_type_label = "Purchase";
		break;

		case "Company Kitchen Refund":
			$transaction_type_label = "Company Kitchen Refund";
		break;

		case "Transfer":
			$transaction_type_label = "Transfer";
		break;

		case "CK Adjust":
			$transaction_type_label = "CK Adjust";
		break;

		case "Promotion":
			$transaction_type_label = "Promotion";
		break;

		case "Credit Card Transaction":
			$transaction_type_label = "Credit Card Transaction";
		break;

		case "Balanced CK Card":
			$transaction_type_label = "Balanced CK Card";
		break;

		default:
			$transaction_type_label = "N/A";
		break;
	}

	return $transaction_type_label;
	
}

$total_customers = $_SESSION["total_customers"];
$accounts_balanced = $_SESSION["accounts_balanced"];
$accounts_oob = $_SESSION["accounts_oob"];

$accounts_oob_1 = 0;
$accounts_oob_2 = 0;
$accounts_oob_5 = 0;
$accounts_oob_10 = 0;
$accounts_oob_20 = 0;
$accounts_oob_20_plus = 0;

if ($_POST){
	$customer_data_value = $_SESSION["customer_data_value"];
	//echo $customer_data_value . "<br />";
	$counter = 0;
	$data_values = unserialize($customer_data_value);
	if ($data_values != ''){
		
		?>
		<div id="accounts-rebalanced">
			<span id="customer-rebalanced-header">Accounts Re-balanced <?php //echo @$counter; ?></span>
		</div>
		<div id="accounts-rebalanced-2">
		<?php
		
		foreach ($data_values as $key=>$value){
			foreach ($value as $key2=>$value2){
				$customer_current_calculated_balance = $value2;
				$get_customer_data_line_sql = "SELECT CONCAT(c.first_name, ' ', c.last_name) AS customer_info, c.scancode AS scancode, c.balance AS stored_balance FROM mburris_businesstrack.customer c WHERE c.customerid = $key2";
				$get_customer_data_line_result = Treat_DB_ProxyOld::query($get_customer_data_line_sql);
				$customer_data_line = Treat_DB_ProxyOld::mysql_result($get_customer_data_line_result, 0, "customer_info");
				$customer_scancode = Treat_DB_ProxyOld::mysql_result($get_customer_data_line_result, 0, "scancode");
				$customer_current_stored_balance = Treat_DB_ProxyOld::mysql_result($get_customer_data_line_result, 0, "stored_balance");
				
				$customer_message = adjust_customer_balance($key2, $customer_current_calculated_balance, $customer_current_stored_balance);
					
				$customer_data_line_1 = $customer_data_line . " -- " . $customer_scancode . " -- " . $customer_message . "<br>";
					
				?><span class="customer-rebalanced-name"><?php echo $customer_data_line; ?></span><span class="customer-rebalanced-scancode"><?php echo $customer_scancode; ?></span><span class="customer-rebalanced-amount"><?php echo $customer_message; ?></span><?php
				$counter++;
			}
		}
	?></div><?php
	}

}

$query = "SELECT business.businessid,business.businessname,company.companyname FROM business JOIN company ON company.companyid = business.companyid WHERE (business.companyid != 2) AND (business.categoryid = 4) ORDER BY company.companyname,business.businessname";
$result = Treat_DB_ProxyOld::query($query);

if (@$_GET['bid']){
	$bid = @$_GET['bid'];
} else {
	$bid = 0;
}

$show_all = @$_GET['show_all'];
$show_detail = @$_GET['show_detail'];
$selected_bid = $bid;

?>
	<form action=test4.php method=get>
		<input type="radio" name="show_all" value="1" <?php if(@$show_all == 1){ echo "checked"; } ?> />Display All
		<input type="radio" name="show_all" value="0" <?php if(@$show_all == 0){ echo "checked"; } ?> />Display OOB only
		<br />
		<input type="radio" name="show_detail" value="1" <?php if(@$show_detail == 1){ echo "checked"; } ?> />Show Detail
		<input type="radio" name="show_detail" value="0" <?php if(@$show_detail == 0){ echo "checked"; } ?> />No Detail
		<br />
		<select name="bid">
<?php
	
while($r = Treat_DB_ProxyOld::mysql_fetch_array($result)){
	$businessid = $r["businessid"];
	$businessname = $r["businessname"];
	$companyname = $r["companyname"];
	
	if ($selected_bid != $businessid){
		?><option value="<?php echo $businessid; ?>"><?php echo $companyname; ?> :: <?php echo $businessname; ?></option><?php
	} else {
		?><option value="<?php echo $businessid; ?>" selected="selected"><?php echo $companyname; ?> :: <?php echo $businessname; ?></option><?php
	}
}

?>
		</select>
		<input type=submit value='GO'>
		<br />
	</form>
	<br />
<?php

define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');

$number_of_correct_card_balances = 0;
$number_of_not_correct_balances = 0;

$other_data = array();
$data = array();
$line_counter = 1;
$query = "SELECT business.businessid, business.businessname, company.companyname FROM mburris_businesstrack.business JOIN mburris_businesstrack.company ON company.companyid = business.companyid WHERE (business.businessid = $bid) ORDER BY business.companyid,business.businessname";
$result = Treat_DB_ProxyOld::query($query);

while ($r = Treat_DB_ProxyOld::mysql_fetch_array($result)){
	$businessid = $r['businessid'];
	$businessname = $r['businessname'];
	$companyname = $r['companyname'];

	//$query55 = "SELECT first_name,last_name,customerid,scancode,balance FROM mburris_businesstrack.customer WHERE businessid = $businessid and scancode LIKE '780%'";
	$query55 = "SELECT first_name,last_name,customerid,scancode,balance FROM mburris_businesstrack.customer WHERE businessid = $businessid";
	$result55 = Treat_DB_ProxyOld::query($query55);
	$num55 = @mysql_numrows($result55);

	while($r55 = @mysql_fetch_array($result55)){
		$scancode = $r55["scancode"];
		$customerid = $r55["customerid"];
		$mybalance = $r55["balance"];
		$first_name = $r55["first_name"];
		$last_name = $r55["last_name"];
			
		$customer_name = $first_name . " " . $last_name;
		
		$sql = "SELECT balance FROM mburris_businesstrack.customer WHERE scancode LIKE '$scancode'";
		$result2 = Treat_DB_ProxyOld::query($sql);
		$customer_balance = Treat_DB_ProxyOld::mysql_result($result2, 0, 'balance');

		$counter = 1;

		$sql2 = "SELECT customer.customerid, checks.check_number, checks.businessid, checks.bill_datetime, checks.total, checks.sale_type, payment_detail.received, payment_detail.payment_type, business.businessname FROM checks JOIN payment_detail ON payment_detail.check_number = checks.check_number AND payment_detail.businessid = checks.businessid AND payment_detail.scancode = '$scancode' JOIN customer USING (scancode) JOIN business ON business.businessid = checks.businessid WHERE checks.bill_datetime BETWEEN '2000-01-01 00:00:00' AND '2020-01-01 00:00:00' ORDER BY bill_datetime DESC";
		$result2 = Treat_DB_ProxyOld::query($sql2);
		$oob_array = array();
			
		while ($rs = Treat_DB_ProxyOld::mysql_fetch_array($result2)){
			$customerid = $rs['customerid'];
			$check_number = $rs['check_number'];
			$businessid2 = $rs['businessid'];
			$bill_datetime = $rs['bill_datetime'];
			$total = $rs['total'];
			$sale_type = $rs['sale_type'];
			$received = $rs['received'];
			$payment_type_value = $rs['payment_type'];
			$businessname = $rs['businessname'];

			$customer_transaction_type_label = get_payment_type($payment_type_value);

			$oob_array[] = array(
				'pay_value' => $payment_type, 
				'payment_type' => $payment_type_value, 
				'bill_datetime' => $bill_datetime, 
				'total' => $total, 
				'businessname' => $businessname, 
			);
		}
			
		$additional_customer_transactions_sql = "SELECT ctt.label AS payment_type, ct.date_created AS bill_datetime, ct.chargetotal AS total, 'Not Available' AS businessname FROM mburris_businesstrack.customer_transactions ct INNER JOIN customer_transaction_types ctt ON ct.trans_type_id = ctt.id WHERE (customer_id = $customerid) AND (ct.response LIKE 'APPROVED') ORDER BY ct.date_created DESC";
		$additional_customer_transactions_result = Treat_DB_ProxyOld::query($additional_customer_transactions_sql);

		while ($rs = Treat_DB_ProxyOld::mysql_fetch_array($additional_customer_transactions_result)){
			$payment_type_value = $rs['payment_type'];
			$bill_datetime = $rs['bill_datetime'];
			$total = $rs['total'];
			$businessname = $rs['businessname'];

			$oob_array[] = array(
				'payment_type' => $payment_type_value, 
				'bill_datetime' => $bill_datetime, 
				'total' => $total, 
				'businessname' => $businessname, 
			);
		}

		usort($oob_array, function($a,$b){
			return strtotime($a['bill_datetime']) > strtotime($b['bill_datetime']);
		}); 
			
		$running_balance = 0;
		$running_balance_array = array();
		$value3 = 0;
		$value4 = 0;
		foreach ($oob_array as $key=>$value){
					
				$payment_type = $value['payment_type'];
				$bill_datetime = $value['bill_datetime'];
				$total = $value['total'];
				$businessname = $value['businessname'];
					
					if (($payment_type == 1) || ($payment_type == 4) || ($payment_type == 6) || ($payment_type == 'Promotion') || ($payment_type == 'Credit Card Transaction') || ($payment_type == "Company Kitchen Refund") || ($payment_type == "Transfer") || ($payment_type == "CK Adjust")){
						$total = $total * -1;
						$value3 += $total;
					} else {
						$value4 += $total;
					}
					$running_balance += $total;
					
				$running_balance_array[] = array(
					'pay_value' => $payment_type, 
					'payment_type' => $payment_type_value, 
					'bill_datetime' => $bill_datetime, 
					'total' => $total, 
					'businessname' => $businessname, 
					'running_balance' => $running_balance, 
				);

		}
		
		$calculated_balance = $value3 + $value4;
		$calculated_balance = number_format($calculated_balance,2);
			
		$calculated_balance = $calculated_balance * -1;
		$show_customer_line_items = 0;
		
		$oob_amount = $customer_balance - $calculated_balance;
		
		if ($customer_balance == $calculated_balance){
			$number_of_correct_card_balances++;
			$header_row_css = "check-header-row";
			$check_detail_row_css = "check-detail-row";
			$customer_stored_balance_row = "customer-balance-headers";
		} else {
			$number_of_not_correct_balances++;
			$header_row_css = "check-header-row-oob";
			$check_detail_row_css = "check-detail-row-oob";
			$customer_stored_balance_row = "customer-balance-headers-oob";
			$customer_calculated_balance_row = "";
			$show_customer_line_items = 1;
			$fix_customers[] = array($customerid => $calculated_balance);
			
			include 'test5.php';
			
		}
	
		if ($show_all == 1){
			include 'test3.php';
		} elseif (($show_all == 0) && ($show_customer_line_items == 1)) {
			include 'test3.php';
		}
		
	}

}

if ($_GET['bid']){

} else {
	echo "<h3>Select the business you would like to rebalance</h3>";
}
?>


<?php

$data_value = serialize($fix_customers);

$_SESSION["customer_data_value"] = $data_value;

$_SESSION["total_customers"] = $number_of_correct_card_balances + $number_of_not_correct_balances;
$_SESSION["accounts_balanced"] = $number_of_correct_card_balances;
$_SESSION["accounts_oob"] = $number_of_not_correct_balances;

$total_customers = $number_of_correct_card_balances + $number_of_not_correct_balances;
$accounts_balanced = $number_of_correct_card_balances;
$accounts_oob = $number_of_not_correct_balances;

$oob_1 = 0;
$oob_2 = 0;
$oob_5 = 0;
$oob_10 = 0;
$oob_20 = 0;
$oob_21 = 0;

?>


<div id="accounts-rebalanced">
	<span id="customer-rebalanced-header">Account Balance Summary <?php //echo @$counter; ?></span>
</div>
	
<div id="accounts-rebalanced-2">
	<span class="account-balance-summary-label">Total Accounts</span><span class="account-balance-summary-amount"><?php echo $total_customers; ?></span><span class="account-balance-summary-text"></span>
	<span class="account-balance-summary-label">Accounts Balanced</span><span class="account-balance-summary-amount"><?php echo $accounts_balanced; ?></span><span class="account-balance-summary-text"></span>
	<span class="account-balance-summary-label">Accounts OO Balance</span><span class="account-balance-summary-amount"><?php echo $accounts_oob; ?></span><span class="account-balance-summary-text"><?php if ($accounts_oob > 0){ ?><a href="/ta/lib/test7.php?bid=<?php echo $bid; ?>&rebalance=all" target="_blank">Re-balance All</a><?php } ?></span>
	<span class="account-balance-summary-label">Accounts &plusmn;$1</span><span class="account-balance-summary-amount"><?php echo $accounts_oob_1; ?></span><span class="account-balance-summary-text"><?php if ($accounts_oob_1 > 0){ ?><a href="/ta/lib/test7.php?bid=<?php echo $bid; ?>&rebalance=1&data=<?php echo $oob_1; ?>" target="_blank">Re-balance</a><?php } ?></span>
	<span class="account-balance-summary-label">Accounts &plusmn;$2</span><span class="account-balance-summary-amount"><?php echo $accounts_oob_2; ?></span><span class="account-balance-summary-text"><?php if ($accounts_oob_2 > 0){ ?><a href="/ta/lib/test7.php?bid=<?php echo $bid; ?>&rebalance=2&data=<?php echo $oob_2; ?>" target="_blank">Re-balance</a><?php } ?></span>
	<span class="account-balance-summary-label">Accounts &#8723;$5</span><span class="account-balance-summary-amount"><?php echo $accounts_oob_5; ?></span><span class="account-balance-summary-text"><?php if ($accounts_oob_5 > 0){ ?><a href="/ta/lib/test7.php?bid=<?php echo $bid; ?>&rebalance=5&data=<?php echo $oob_5; ?>" target="_blank">Re-balance</a><?php } ?></span>
	<span class="account-balance-summary-label">Accounts &#8723;$10</span><span class="account-balance-summary-amount"><?php echo $accounts_oob_10; ?></span><span class="account-balance-summary-text"><?php if ($accounts_oob_10 > 0){ ?><a href="/ta/lib/test7.php?bid=<?php echo $bid; ?>&rebalance=10&data=<?php echo $oob_10; ?>" target="_blank">Re-balance</a><?php } ?></span>
	<span class="account-balance-summary-label">Accounts &#x2213;$20</span><span class="account-balance-summary-amount"><?php echo $accounts_oob_20; ?></span><span class="account-balance-summary-text"><?php if ($accounts_oob_20 > 0){ ?><a href="/ta/lib/test7.php?bid=<?php echo $bid; ?>&rebalance=20&data=<?php echo $oob_20; ?>" target="_blank">Re-balance</a><?php } ?></span>
	<span class="account-balance-summary-label">Accounts > &#x2213;$20</span><span class="account-balance-summary-amount"><?php echo $accounts_oob_20_plus; ?></span><span class="account-balance-summary-text"><?php if ($accounts_oob_20_plus > 0){ ?><a href="/ta/lib/test7.php?bid=<?php echo $bid; ?>&rebalance=21&data=<?php echo $oob_21; ?>" target="_blank">Re-balance</a><?php } ?></span>
</div>

<table width="925px">
	
<?php

$kl = 0;

foreach ($data as $key=>$value){

	if ($value['customer_name']){
		$k2 = 0;
		$customer_name_value = $value['customer_name'];
		$customer_balance = $value['customer_balance'];
		$calculated_balance = $value['calculated_balance'];

		if ($customer_balance == $calculated_balance){
			$number_of_correct_card_balances++;
			$header_row_css = "check-header-row";
			$check_detail_row_css = "check-detail-row";
			$customer_stored_balance_row = "customer-balance-headers-1";
		} else {
			$number_of_not_correct_balances++;
			$header_row_css = "check-header-row-oob";
			$check_detail_row_css = "check-detail-row-oob";
			$customer_stored_balance_row = "customer-balance-headers-oob-1";
			$customer_calculated_balance_row = "";
			$show_customer_line_items = 1;
		}
		
		
		?>
	
	<tr>
		<td class="<?php echo $customer_stored_balance_row; ?>"><?php echo $customer_name_value; ?></td><td class="<?php echo $customer_stored_balance_row; ?>">Stored Balance</td><td class="<?php echo $customer_stored_balance_row; ?>">$<?php echo $customer_balance; ?></td><td class="<?php echo $customer_stored_balance_row; ?>">Calculated Balance</td><td class="<?php echo $customer_stored_balance_row; ?>">$<?php echo $calculated_balance; ?></td>
	</tr>

		<?php
		
	}
	if (($_GET['show_detail'] == 1)){
		
	
	if (($k2 == 0)){
		$k2 = 1;
		?>
		<tr>
			<td class="<?php echo $header_row_css; ?>">Payment Type</td>
			<td class="<?php echo $header_row_css; ?>">Date/Time</td>
			<td class="<?php echo $header_row_css; ?>">Amount</td>
			<td class="<?php echo $header_row_css; ?>">Balance</td>
			<td class="<?php echo $header_row_css; ?>">Location</td>
		</tr>
		<?php
	}
	
			if ($kl & 1){
				$odd_even = "-odd";
			} else {
				$odd_even = "-even";
			}
			
			$kl++;
	
	?>
			<tr>
				<td class="<?php echo $check_detail_row_css . $odd_even; ?>"><?php echo $value['customer_transaction_type_label']; ?></td>
				<td class="<?php echo $check_detail_row_css . $odd_even; ?>"><?php echo $value['bill_datetime']; ?></td>
				<td class="<?php echo $check_detail_row_css . $odd_even; ?>"><?php echo $value['total']; ?></td>
				<td class="<?php echo $check_detail_row_css . $odd_even; ?>"><?php echo $value['running_balance_2']; ?></td>
				<td class="<?php echo $check_detail_row_css . $odd_even; ?>"><?php echo $value['businessname']; ?></td>
			</tr>
	<?php 

	}
	
}

?>

</table>

</body>
</html>
