<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');
if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
	trigger_error( __FILE__ . ' is not a class file. This may need to be redone as a template.', E_USER_NOTICE );
}
error_reporting(0);

ini_set("display_errors","true");

$businessid = $_GET["bid"];
$menu_typeid = $_GET["mid"];
$filename = $_GET["filename"];
$output = 0;

if($businessid < 1){
	$businessid = $_POST["businessid"];
	$menu_typeid = $_POST["menu_typeid"];
	$filename = "menu_schedule";
	$newdate = $_POST["date"];
	$output = 1;
}

$today = date("Y-m-d");

if(dayofweek($today) == "Saturday" || dayofweek($today) == "Sunday"){echo "Skipping Weekend"; break;}
elseif(dayofweek($today) == "Monday"){
        $date1 = date("Y-m-d",mktime(0, 0, 0, date("m") , date("d")+4, date("Y")));
        $date2 = $date1;
}
else{
        $date1 = date("Y-m-d",mktime(0, 0, 0, date("m") , date("d")+6, date("Y")));
        $date2 = $date1;
}

if($newdate != ""){$date1 = $newdate;}

//echo "<b>Menu for $date1</b><br>";

$query = "SELECT menu_items.item_code,menu_items.item_name FROM menu_items,vend_item,menu_pricegroup,menu_groups
			WHERE menu_items.menu_item_id = vend_item.menu_itemid
			AND menu_items.menu_typeid = $menu_typeid
			AND vend_item.date = '$date1'
			AND vend_item.businessid = $businessid
			AND menu_items.groupid = menu_pricegroup.menu_pricegroupid
			AND menu_pricegroup.menu_groupid = menu_groups.menu_group_id
			ORDER BY menu_items.groupid";
$result = Treat_DB_ProxyOldProcessHost::query($query);

$data = "$date1\n";

while($r = mysql_fetch_array($result)){
	$item_code = $r["item_code"];
	$item_name = $r["item_name"];

	$data .= "$item_code, \"$item_name\"\n";
}

if($output == 1){
	$file="$filename.csv";
    header("Content-type: application/x-msdownload");
    header("Content-Disposition: attachment; filename=$file");
    header("Pragma: no-cache");
    header("Expires: 100");
    echo $data;
}
else{
	$myFile = DIR_FTP_UPLOADS . "/TreatAmerica/OrderByMachine/$filename.csv";
	$fh = fopen($myFile, 'w');
	fwrite($fh, $data);
	fclose($fh);
}
?>