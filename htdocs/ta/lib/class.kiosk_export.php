<?php

/////////////////////////////////
//kiosk handheld info export
/////////////////////////////////

define('DOC_ROOT', realpath(dirname(__FILE__).'/../../'));
require_once(DOC_ROOT.'/bootstrap.php');
require_once( DOC_ROOT . '/ta/lib/class.menu_items.php' );
require_once( DOC_ROOT . '/ta/lib/class.Kiosk.php' );
//ini_set("display_errors","true");

////by company
$com_query = "SELECT companyid FROM company ORDER BY companyid";
$com_result = Treat_DB_ProxyOldProcessHost::query($com_query);

echo $com_query . "<br>";

while($com_r = mysql_fetch_array($com_result)){
	$companyid = $com_r["companyid"];

	$machines = array();

///dates
////override
$date = $_GET["date"];
if($date == ""){
	$date = date("Y-m-d");
}
$date = prevday($date);

$date1 = "$date 00:00:00";
$date2 = "$date 23:59:59";
$export_date = date("jMY",strtotime($date));

$invalid_items = array();
$data = "";

////select all machines linked to businesses
$query = "SELECT 
			inv_count_vend.machine_num,
			inv_count_vend.item_code,
			inv_count_vend.onhand,
			inv_count_vend.unit_cost,
			SUM(inv_count_vend.fills) AS fills,
			SUM(inv_count_vend.waste) AS waste,
			inv_count_vend.is_inv,
			menu_items_price.price,
			menu_items_new.max,
            menu_items_new.name,
            menu_items_new.pos_id,
            machine_bus_link.businessid,
			creditdetail.amount AS sales
		FROM
			inv_count_vend
			JOIN machine_bus_link ON machine_bus_link.machine_num = inv_count_vend.machine_num
			JOIN business ON business.businessid = machine_bus_link.businessid AND business.companyid = $companyid
			JOIN menu_items_new ON menu_items_new.item_code = inv_count_vend.item_code AND machine_bus_link.businessid = menu_items_new.businessid
			JOIN menu_items_price ON menu_items_price.menu_item_id = menu_items_new.id
			LEFT JOIN creditdetail ON machine_bus_link.creditid = creditdetail.creditid AND creditdetail.date = '$date'
		WHERE inv_count_vend.date_time >= '$date1' 
                AND inv_count_vend.date_time <= '$date2'
                GROUP BY inv_count_vend.machine_num,inv_count_vend.item_code
		ORDER BY inv_count_vend.machine_num,inv_count_vend.item_code";
$result = Treat_DB_ProxyOldProcessHost::query($query);
echo $query . "<br>";
while($r = mysql_fetch_array($result)){
	$machine_num = $r["machine_num"];
	$item_code = $r["item_code"];
	$onhand = $r["onhand"];
	$unit_cost = $r["unit_cost"];
	$fills = $r["fills"];
	$waste = $r["waste"];
	$is_inv = $r["is_inv"];
	$price = $r["price"];
	$max = $r["max"];
    $name = $r["name"];
    $businessid = $r["businessid"];
    $pos_id = $r["pos_id"];
	$total = $r["sales"];

	$machines[$machine_num] = $total;

        ////remove commas from name
        $name = str_replace(",","",$name);

        /*$query2 = "SELECT SUM(creditdetail.amount) AS total FROM creditdetail
                    JOIN machine_bus_link ON machine_bus_link.creditid = creditdetail.creditid
                    WHERE machine_bus_link.machine_num = '$machine_num' AND creditdetail.date = '$date'";
        $result2 = Treat_DB_ProxyOldProcessHost::query($query2);

        $total = mysql_result($result2,0,"total");
		*/
        $valid = \EE\Model\Mei\Product::validateItemCode($item_code);

        if($valid == 1){$valid = 0;}
        else{$valid = 1;$invalid_items[$item_code]= $name;}

        ////get check detail
        $items_sold = menu_items::items_sold($pos_id,$businessid,$date1,$date2);
        if($items_sold == ""){$items_sold = 0;}

	$data .= "1\t$machine_num\t$export_date\t$item_code\t$fills\t$waste\t$unit_cost\t$price\t$onhand\t$max\t$total\t$items_sold\t$name\t$valid\r\n";
}

//////sales regardless of fills
$query = "SELECT machine_bus_link.machine_num, creditdetail.amount
	FROM machine_bus_link
	JOIN creditdetail ON creditdetail.creditid = machine_bus_link.creditid AND creditdetail.date = '$date'
	JOIN business ON business.businessid = machine_bus_link.businessid AND business.companyid = $companyid
	ORDER BY machine_bus_link.machine_num";
$result = Treat_DB_ProxyOldProcessHost::query($query);

while($r = mysql_fetch_array($result)){
	$machine_num = $r["machine_num"];
	$amount = $r["amount"];

	if($amount != 0 && $machines[$machine_num] != $amount){
		$data .= "2\t$machine_num\t$export_date\t0\t0\t0\t0\t0\t0\t0\t$amount\t0\t0\t0\r\n";
	}
}

file_put_contents(DIR_FTP_UPLOADS."/TreatAmerica/KioskInventory/KioskData-$companyid.csv",$data);

if($invalid_items){
    foreach($invalid_items AS $code => $name){
        $message.= "[$code] $name\r\n";
    }

    /////send email
    $email = "davea@treatamerica.com,smartin@essentialpos.com.test-google-a.com";
    $subject = "Invalid Kiosk Item(s)";
    $headers .= "From: alerts@treatamerica.com\r\n";

    //mail($email, $subject, $message, $headers);
}
}
//echo "$query<br>";
//echo "$data<br>";
echo "Done";
?>