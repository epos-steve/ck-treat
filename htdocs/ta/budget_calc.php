<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once(DOC_ROOT.'/bootstrap.php');

function rndm_color_code($b_safe = TRUE) {
    
    //make sure the parameter is boolean
    if(!is_bool($b_safe)) {return FALSE;}
    
    //if a browser safe color is requested then set the array up
    //so that only a browser safe color can be returned
    if($b_saafe) {
        $ary_codes = array('00','33','66','99','CC','FF');
        $max = 5; //the highest array offest
    //if a browser safe color is not requested then set the array
    //up so that any color can be returned.
    } else {
        $ary_codes = array();
        for($i=0;$i<16;$i++) {
            $t_1 = dechex($i);
            for($j=0;$j<16;$j++) {
                $t_2 = dechex($j);
                $ary_codes[] = "$t_1$t_2";
            } //end for j
        } //end for i
        $max = 256; //the highest array offset
    } //end if
    
    $retVal = '';
    
    //generate a random color code
    for($i=0;$i<3;$i++) {
        $offset = rand(0,$max);
        $retVal .= $ary_codes[$offset];
    } //end for i
    
    return $retVal;
} //end rndm_color_code

$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");

/*$user=$_COOKIE["usercook"];
$pass=$_COOKIE["passcook"];
$companyid=$_COOKIE["compcook"];*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook');
$companyid = \EE\Controller\Base::getSessionCookieVariable('compcook');

if ($businessid==""){
   /*$businessid=$_POST["businessid"];
   $date1=$_POST["date1"];
   $date2=$_POST["date2"];
   $service=$_POST["service"];*/
	
	$businessid = \EE\Controller\Base::getPostVariable('businessid');
	$date1 = \EE\Controller\Base::getPostVariable('date1');
	$date2 = \EE\Controller\Base::getPostVariable('date2');
	$service = \EE\Controller\Base::getPostVariable('service');
	$businessid2 = \EE\Controller\Base::getPostVariable('businessid2');

   //$businessid2=unserialize(urldecode($_POST["businessid2"]));
	$businessid2=unserialize(urldecode($businessid2));
	
   /*$newbudget=$_POST["newbudget"];
   $date3=$_POST["date3"];
   $date4=$_POST["date4"];
   $budget_bus=$_POST["mybusid"];*/
	
	$newbudget = \EE\Controller\Base::getPostVariable('newbudget');
	$date3 = \EE\Controller\Base::getPostVariable('date3');
	$date4 = \EE\Controller\Base::getPostVariable('date4');
	$budget_bus = \EE\Controller\Base::getPostVariable('mybusid');

   if($businessid==""){$businessid=$businessid2;}
}

/*if ($date1==""){$date1=$_COOKIE["mydate1"];}
if ($date2==""){$date2=$_COOKIE["mydate2"];}*/

if ($date1==""){$date1 = \EE\Controller\Base::getSessionCookieVariable('mydate1');}
if ($date2==""){$date2 = \EE\Controller\Base::getSessionCookieVariable('mydate2');}

if ($date1==""){$date1=$today;}
if ($date2==""){$date2=$today;}

if ($date3==""){$year=substr($date1,0,4);$year++;$rest=substr($date1,4,6);$date3="$year$rest";}
if ($date4==""){$year=substr($date2,0,4);$year++;$rest=substr($date2,4,6);$date4="$year$rest";}

setcookie("mydate1",$date1);
setcookie("mydate2",$date2);

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( mysql_error());

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=Treat_DB_ProxyOld::mysql_numrows($result);

$security_level=Treat_DB_ProxyOld::mysql_result($result,0,"security_level");
$bid=Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
$bid2=Treat_DB_ProxyOld::mysql_result($result,0,"busid2");
$bid3=Treat_DB_ProxyOld::mysql_result($result,0,"busid3");
$bid4=Treat_DB_ProxyOld::mysql_result($result,0,"busid4");
$bid5=Treat_DB_ProxyOld::mysql_result($result,0,"busid5");
$bid6=Treat_DB_ProxyOld::mysql_result($result,0,"busid6");
$bid7=Treat_DB_ProxyOld::mysql_result($result,0,"busid7");
$bid8=Treat_DB_ProxyOld::mysql_result($result,0,"busid8");
$bid9=Treat_DB_ProxyOld::mysql_result($result,0,"busid9");
$bid10=Treat_DB_ProxyOld::mysql_result($result,0,"busid10");
$cid=Treat_DB_ProxyOld::mysql_result($result,0,"companyid");
$loginid=Treat_DB_ProxyOld::mysql_result($result,0,"loginid");

if ($num != 1)
{
    echo "<center><h3>Failed</h3></center>";
}

else
{

?>

<html>
<head>

<SCRIPT LANGUAGE="JavaScript" SRC="CalendarPopup.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">document.write(getCalendarStyles());</SCRIPT>

<STYLE>
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation
			{
			background-color:#6677DD;
			text-align:center;
			vertical-align:center;
			text-decoration:none;
			color:#FFFFFF;
			font-weight:bold;
			}
	.TESTcpDayColumnHeader,
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation,
	.TESTcpCurrentMonthDate,
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDate,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDate,
	.TESTcpCurrentDateDisabled,
	.TESTcpTodayText,
	.TESTcpTodayTextDisabled,
	.TESTcpText
			{
			font-family:arial;
			font-size:8pt;
			}
	TD.TESTcpDayColumnHeader
			{
			text-align:right;
			border:solid thin #6677DD;
			border-width:0 0 1 0;
			}
	.TESTcpCurrentMonthDate,
	.TESTcpOtherMonthDate,
	.TESTcpCurrentDate
			{
			text-align:right;
			text-decoration:none;
			}
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDateDisabled
			{
			color:#D0D0D0;
			text-align:right;
			text-decoration:line-through;
			}
	.TESTcpCurrentMonthDate
			{
			color:#6677DD;
			font-weight:bold;
			}
	.TESTcpCurrentDate
			{
			color: #FFFFFF;
			font-weight:bold;
			}
	.TESTcpOtherMonthDate
			{
			color:#808080;
			}
	TD.TESTcpCurrentDate
			{
			color:#FFFFFF;
			background-color: #6677DD;
			border-width:1;
			border:solid thin #000000;
			}
	TD.TESTcpCurrentDateDisabled
			{
			border-width:1;
			border:solid thin #FFAAAA;
			}
	TD.TESTcpTodayText,
	TD.TESTcpTodayTextDisabled
			{
			border:solid thin #6677DD;
			border-width:1 0 0 0;
			}
	A.TESTcpTodayText,
	SPAN.TESTcpTodayTextDisabled
			{
			height:20px;
			}
	A.TESTcpTodayText
			{
			color:#6677DD;
			font-weight:bold;
			}
	SPAN.TESTcpTodayTextDisabled
			{
			color:#D0D0D0;
			}
	.TESTcpBorder
			{
			border:solid thin #6677DD;
			}
</STYLE>

<STYLE>
#loading {
 	width: 170px;
 	height: 48px;
 	background-color: ;
 	position: absolute;
 	left: 50%;
 	top: 50%;
 	margin-top: -50px;
 	margin-left: -100px;
 	text-align: center;
}
</STYLE>

<script type="text/javascript" src="preLoadingMessage.js"></script>

<SCRIPT LANGUAGE=javascript><!--
function contconfirm(){return confirm('Are you sure you want to save?');}
// --></SCRIPT>

</head>

<?
    if($newbudget!=0){
       $myFile2 = HTTP_EXPORTS."/report$loginid.csv";
       $fh = fopen($myFile2, 'w'); 
    }   

    echo "<body>";
    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%><tr><td colspan=2>";
    echo "<a href=businesstrack.php><img src=logo.jpg border=0 height=43 width=205></a><p></td></tr>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM company WHERE companyid = '$companyid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $companyname=Treat_DB_ProxyOld::mysql_result($result,0,"companyname");

    //echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";
    echo "<tr bgcolor=#CCCCFF><td colspan=1><font size=4><b>Budget Calculator - $companyname</b></font></td><td align=right></td><td width=0%></td></tr>";
    echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";
	echo "<tr><td colspan=3>Date Range Average :: <a href=budget_calc2.php style=$style><font color=blue>Yearly Actual</font></a></td></tr>";
    echo "</table></center><p>";

    /////////////////////
    //////FILTER/////////
    /////////////////////
    echo "<center><table width=90%><tr><td><form action=budget_calc.php method=post><select name=businessid[] MULTIPLE size=8>";

    $query = "SELECT * FROM business WHERE companyid != '2' ORDER BY companyid DESC, districtid DESC, businessname DESC";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);

    $lastcompany=-1;
    $lastdistrict=-1;
    $num--;
    while($num>=0){
       $businessname=Treat_DB_ProxyOld::mysql_result($result,$num,"businessname");
       $busid=Treat_DB_ProxyOld::mysql_result($result,$num,"businessid");
       $comid=Treat_DB_ProxyOld::mysql_result($result,$num,"companyid");
       $distid=Treat_DB_ProxyOld::mysql_result($result,$num,"districtid");
       $unit=Treat_DB_ProxyOld::mysql_result($result,$num,"unit");

       if(in_array($busid,$businessid)){$showsel="SELECTED";}
       else{$showsel="";}

       if($lastcompany!=$comid){
          if($lastcompany!=-1){echo "</optgroup>";}

          $query2 = "SELECT companyname FROM company WHERE companyid = '$comid'";
          $result2 = Treat_DB_ProxyOld::query($query2);

          $comname=Treat_DB_ProxyOld::mysql_result($result2,0,"companyname");

          echo "<optgroup label='$comname'>";
       }
       if($lastdistrict!=$distid){
          if($lastdistrict!=-1){echo "</optgroup>";}

          $query2 = "SELECT districtname FROM district WHERE districtid = '$distid'";
          $result2 = Treat_DB_ProxyOld::query($query2);

          $distname=Treat_DB_ProxyOld::mysql_result($result2,0,"districtname");

          echo "<optgroup label='   $distname'>";
       }

       echo "<option value=$busid $showsel>$businessname #$unit</option>";

       $lastcompany=$comid;
       $lastdistrict=$distid;
       $num--;
    }
    echo "</optgroup></optgroup></select> ";

    if($service==1){$showserv="CHECKED";}

    echo "<SCRIPT LANGUAGE='JavaScript' ID='js18'> var cal18 = new CalendarPopup('testdiv1');cal18.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date1 VALUE='$date1' SIZE=8> <A HREF='javascript:void();' onClick=cal18.select(document.forms[0].date1,'anchor18','yyyy-MM-dd'); return false; TITLE=cal18.select(document.forms[0].date1,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor18' ID='anchor18'><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A> to ";
    echo "<SCRIPT LANGUAGE='JavaScript' ID='js19'> var cal19 = new CalendarPopup('testdiv1');cal19.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date2 VALUE='$date2' SIZE=8> <A HREF='javascript:void();' onClick=cal19.select(document.forms[0].date2,'anchor19','yyyy-MM-dd'); return false; TITLE=cal19.select(document.forms[0].date2,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor19' ID='anchor19'><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A>";

    echo " <input type=checkbox name=service value=1 $showserv>Include Service Total <input type=submit value='GO'></form></td></tr></table></center>";

    //////////////////////
    ///////REPORT/////////
    //////////////////////
    if($businessid!=""){

       $startdate=$date1;
       $enddate=$date2;

       if($service==1){$myquery="credittype = '1' OR credittype = '5'";}
       else{$myquery="credittype = '1'";}

       foreach($businessid AS $key => $value){
          if($myquery2==""){$myquery2="businessid = '$value'";}
          else{$myquery2.=" OR businessid = '$value'";}

          $query4 = "SELECT businessname FROM business WHERE businessid = '$value'";
          $result4 = Treat_DB_ProxyOld::query($query4);

          $busname2=Treat_DB_ProxyOld::mysql_result($result4,0,"businessname");

          if($showclose==""){$showclose="<a style=$style href='calender.php?caterbus=$value&cid=$companyid' target='_blank'><font color=blue size=2>$busname2</font></a>";}
          else{$showclose.=", <a style=$style href='calender.php?caterbus=$value&cid=$companyid' target='_blank'><font color=blue size=2>$busname2</font></a>";}
       }

       $query = "SELECT * FROM credits WHERE ($myquery) AND ($myquery2) AND ((is_deleted = '0') OR (is_deleted = '1' AND del_date >= '$startdate')) ORDER BY credittype DESC, creditname DESC";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);

       $numcredits=$num;

       $num--;
       while ($num>=0){

       $day=$startdate;

          $creditname=Treat_DB_ProxyOld::mysql_result($result,$num,"creditname");
          $creditid=Treat_DB_ProxyOld::mysql_result($result,$num,"creditid");
          $account=Treat_DB_ProxyOld::mysql_result($result,$num,"account");
          $invoicesales=Treat_DB_ProxyOld::mysql_result($result,$num,"invoicesales");
          $invoicesales_notax=Treat_DB_ProxyOld::mysql_result($result,$num,"invoicesales_notax");
          $invoicetax=Treat_DB_ProxyOld::mysql_result($result,$num,"invoicetax");
          $servchrg=Treat_DB_ProxyOld::mysql_result($result,$num,"servchrg");
          $salescode=Treat_DB_ProxyOld::mysql_result($result,$num,"salescode");
          $prepayment=Treat_DB_ProxyOld::mysql_result($result,$num,"heldcheck");
          $showaccount=substr($account,0,3);

          $prevamount=0;
          $credittotal=0;
          $monthtotal=0;
          while ($day<=$enddate){

          $prevamount=0;

///MANUAL
          if ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0&&$prepayment==0&&($salescode==0||$salescode=="")){
                  $query5 = "SELECT amount FROM creditdetail WHERE creditid = '$creditid' AND date = '$day'";
                  $result5 = Treat_DB_ProxyOld::query($query5);
                  $num5=Treat_DB_ProxyOld::mysql_numrows($result5);

                  if ($num5!=0){$prevamount=Treat_DB_ProxyOld::mysql_result($result5,0,"amount");}
                  $prevamount = money($prevamount);

             }
///INVOICE
             elseif ($invoicesales==1&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==0&&$prepayment==0&&($salescode=="0" || $salescode=="")){
                  $query5 = "SELECT SUM(total-taxtotal-servamt) AS totalamt FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND taxable = 'on' AND salescode = '0' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = Treat_DB_ProxyOld::query($query5);
                  $num6=Treat_DB_ProxyOld::mysql_numrows($result5);

                  $prevamount=0;

                  $totalamt=Treat_DB_ProxyOld::mysql_result($result5,0,"totalamt");
                  $prevamount+=$totalamt;

                  $prevamount=money($prevamount);
             }
///INVOICE TAX EXEMPT
             elseif ($invoicesales==0&&$invoicesales_notax==1&&$invoicetax==0&&$servchrg==0&&$prepayment==0&&($salescode=="0" || $salescode=="")){
                  $query5 = "SELECT SUM(total-servamt) AS totalamt FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND taxable = 'off' AND salescode = '0' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = Treat_DB_ProxyOld::query($query5);

                  $prevamount=0;
                  $totalamt=Treat_DB_ProxyOld::mysql_result($result5,0,"totalamt");
                  $prevamount+=$totalamt;

                  $prevamount=money($prevamount);
             }
///INVOICE TAX
             elseif ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==1&&$servchrg==0&&$prepayment==0&&($salescode=="0" || $salescode=="")){
                  $query5 = "SELECT SUM(taxtotal) AS totalamt FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND taxable = 'on' AND salescode = '0' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = Treat_DB_ProxyOld::query($query5);

                  $prevamount=0;
                  $totalamt=Treat_DB_ProxyOld::mysql_result($result5,0,"totalamt");
                  $prevamount+=$totalamt;
                  $prevamount=money($prevamount);
             }
///INVOICE SERVICE CHARGE
             elseif ($invoicesales==0&&$invoicesales_notax==0&&$invoicetax==0&&$servchrg==1&&$prepayment==0&&($salescode=="0" || $salescode=="")){
                  $query5 = "SELECT SUM(servamt) AS totalamt FROM invoice WHERE type = '1' AND businessid = '$businessid' AND date = '$day' AND salescode = '0' AND (status = '4' OR status = '2' OR status = '6')";
                  $result5 = Treat_DB_ProxyOld::query($query5);
                  
                  $prevamount=0;
                  $totalamt=Treat_DB_ProxyOld::mysql_result($result5,0,"totalamt");
                  $prevamount+=$totalamt;
                  $prevamount=money($prevamount);
             }

             //if($prevamount!=0){
                if(dayofweek($day)=="Sunday"){$sales[Sunday]+=$prevamount;}
                elseif(dayofweek($day)=="Monday"){$sales[Monday]+=$prevamount;}
                elseif(dayofweek($day)=="Tuesday"){$sales[Tuesday]+=$prevamount;}
                elseif(dayofweek($day)=="Wednesday"){$sales[Wednesday]+=$prevamount;}
                elseif(dayofweek($day)=="Thursday"){$sales[Thursday]+=$prevamount;}
                elseif(dayofweek($day)=="Friday"){$sales[Friday]+=$prevamount;}
                elseif(dayofweek($day)=="Saturday"){$sales[Saturday]+=$prevamount;}

                $daysales[$day]+=$prevamount;
             //}
          
          $day=nextday($day);

       }
       $day=$startdate;

       $num--;
    }

    //////////////////////
    //////FIGURE # DAYS///
    //////////////////////
    $numdays[Monday]=0;
    $tempdate=$date1;
    while($tempdate<=$date2){
       $dayname=dayofweek($tempdate);
       if($daysales[$tempdate]!=0){$numdays[$dayname]++;}
       $tempdate=nextday($tempdate);
    }

    //////////////////////
    ///////DISPLAY////////
    //////////////////////
    echo "<center><table width=90% cellspacing=0 cellpadding=0 bgcolor=#CCCCFF><tr><td><font size=2>$showclose (Click on the unit to mark days as closed)</td></tr></table></center><p>";

    echo "<center><table width=90% cellspacing=0 cellpadding=0 style=\"border:1px solid #CCCCFF;\"><tr bgcolor=#CCCCFF><td style=\"border:1px solid #CCCCFF;\"><b>SALES</b></td></tr><tr><td style=\"border:1px solid #CCCCFF;\">";
    echo "<center><table width=100% style=\"border:1px solid #E8E7E7;\" cellspacing=0 cellpadding=0>";
    echo "<tr bgcolor=#E8E7E7><td style=\"border:1px solid #E8E7E7;\">Day</td><td style=\"border:1px solid #E8E7E7;\" align=right>#Days</td><td style=\"border:1px solid #E8E7E7;\" align=right>Sales</td><td style=\"border:1px solid #E8E7E7;\" align=right>Average</td></tr>";
    $counter=1;
    $counter2=8;
    foreach($sales AS $key => $value){
       $average=$value/$numdays[$key];
       $averageday[$key]=$average;
       $total+=$value;
       $total_avg+=$average;
       $total_days+=$numdays[$key];
       $value=number_format($value,2);
       if($average!=0){$totalrows++;}
       $average=number_format($average,2);
       if($numdays[$key]==""){$numdays[$key]=0;}
       if($numdays[$key]>0){echo "<tr id='$counter' bgcolor=white bgcolor=white onMouseOver=this.bgColor='yellow' onMouseOut=this.bgColor='white'><td style=\"border:1px solid #E8E7E7;\">$key</td><td style=\"border:1px solid #E8E7E7;\" align=right>$numdays[$key]</td><td style=\"border:1px solid #E8E7E7;\" align=right>$value</td><td style=\"border:1px solid #E8E7E7;\" align=right>$average</td></tr>";}
       $counter++;
       $counter2++;
    }
    $showtotal=number_format($total,2);
    $total_avg=number_format($total_avg/$totalrows,2);
    echo "<tr bgcolor=#E8E7E7><td style=\"border:1px solid #E8E7E7;\">&nbsp;</td><td style=\"border:1px solid #E8E7E7;\" align=right>$total_days</td><td style=\"border:1px solid #E8E7E7;\" align=right>$showtotal</td><td style=\"border:1px solid #E8E7E7;\" align=right>$total_avg</td></tr>";
    echo "</table></center>";

    ////END SALES TABLE
    echo "</td></tr></table></center><p>";

    ////NEW BUDGET AMOUNT
    $businessid2=urlencode(serialize($businessid));

    echo "<center><table width=90% cellspacing=0 cellpadding=0 bgcolor=#CCCCFF><tr><td><form action=budget_calc.php#bottom method=post style=\"margin:0;padding:0;display:inline;\"><input type=hidden name=businessid2 value='$businessid2'><input type=hidden name=date1 value=$date1><input type=hidden name=date2 value=$date2><input type=hidden name=service value=$service>New Budget Amount: <input type=text name=newbudget value='$newbudget' size=10> for ";

    $query44 = "SELECT businessid,businessname,unit FROM business WHERE companyid != '2' ORDER BY businessname DESC";
    $result44 = Treat_DB_ProxyOld::query($query44);
    $num44=Treat_DB_ProxyOld::mysql_numrows($result44);

    echo "<select name=mybusid>";

    $num44--;
    while($num44>=0){
       $mybusname=Treat_DB_ProxyOld::mysql_result($result44,$num44,"businessname");
       $mybusid=Treat_DB_ProxyOld::mysql_result($result44,$num44,"businessid");
       $myunit=Treat_DB_ProxyOld::mysql_result($result44,$num44,"unit");

       if($mybusid==$budget_bus){$showsel="SELECTED";}
       else{$showsel="";}

       echo "<option value=$mybusid $showsel>$mybusname #$myunit</option>";

       $num44--;
    }

    echo "</select> <SCRIPT LANGUAGE='JavaScript' ID='js28'> var cal28 = new CalendarPopup('testdiv1');cal28.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date3 VALUE='$date3' SIZE=8> <A HREF='javascript:void();' onClick=cal28.select(document.forms[1].date3,'anchor28','yyyy-MM-dd'); return false; TITLE=cal28.select(document.forms[1].date3,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor28' ID='anchor28'><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A> to ";
    echo "<SCRIPT LANGUAGE='JavaScript' ID='js29'> var cal29 = new CalendarPopup('testdiv1');cal29.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date4 VALUE='$date4' SIZE=8> <A HREF='javascript:void();' onClick=cal29.select(document.forms[1].date4,'anchor29','yyyy-MM-dd'); return false; TITLE=cal29.select(document.forms[1].date4,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor29' ID='anchor29'><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A>";
    echo " <input type=submit value='GO'></form></td></tr></table></center><p><a name=#bottom></a>";

    if($newbudget!=0){
    
    $newdays=0;
    $tempdate=$date3;
    while($tempdate<=$date4){
       $dayname=dayofweek($tempdate);

       $query71 = "SELECT * FROM caterclose WHERE businessid = '$businessid[0]' AND date = '$tempdate'";
       $result71 = Treat_DB_ProxyOld::query($query71);
       $num71=Treat_DB_ProxyOld::mysql_numrows($result71);

       if($averageday[$dayname]!=0&&$num71==0){$mydays[$dayname]++;$newdays++;}

       $daysclosed[$tempdate]=$num71;
       $daynameclosed[$dayname]+=$num71;

       $tempdate=nextday($tempdate);
    }

    $difference=$newbudget-$total;

    $overage=0;
    foreach($sales AS $key => $value){
       $percent[$key]=$value/$total;
       $newvalue[$key]=$averageday[$key]+(($percent[$key]*$difference)/($mydays[$key]));
       $overage+=$newvalue[$key]*($mydays[$key]);
    }

    $overage=$overage-$newbudget;

    foreach($newvalue AS $key => $value){
       $newvalue[$key]=$newvalue[$key]-($percent[$key]*$overage)/($mydays[$key]);
    }

    echo "<center><table width=90% cellspacing=0 cellpadding=0 style=\"border:1px solid #CCCCFF;\"><tr bgcolor=#CCCCFF><td style=\"border:1px solid #CCCCFF;\" colspan=2><b>NEW SALES BUDGET</b></td></tr><tr><td style=\"border:1px solid #CCCCFF;\" colspan=2>";

    echo "<center><table width=100% style=\"border:1px solid #E8E7E7;\" cellspacing=0 cellpadding=0>";
    echo "<tr bgcolor=#E8E7E7><td style=\"border:1px solid #E8E7E7;\">Day</td><td style=\"border:1px solid #E8E7E7;\" align=right>#Days</td><td style=\"border:1px solid #E8E7E7;\" align=right>New Average</td></tr>";

    $counter=1;
    $counter2=8;
    foreach($newvalue AS $key => $value){
       $value=number_format($value,2);
       if($mydays[$key]==""){$mydays[$key]=0;}
       if($mydays[$key]>0){echo "<tr id='$counter2' bgcolor=white onMouseOver=this.bgColor='yellow' onMouseOut=this.bgColor='white'><td style=\"border:1px solid #E8E7E7;\">$key</td><td style=\"border:1px solid #E8E7E7;\" align=right>$mydays[$key]</td><td style=\"border:1px solid #E8E7E7;\" align=right>$value</td></tr>";}
       $counter++;
       $counter2++;
    }

    //echo "<center><table width=100% style=\"border:1px solid #E8E7E7;\" cellspacing=0 cellpadding=0>";
    echo "<tr bgcolor=#E8E7E7><td style=\"border:1px solid #E8E7E7;\">Date</td><td style=\"border:1px solid #E8E7E7;\">Day</td><td style=\"border:1px solid #E8E7E7;\" align=right>Budget</td></tr>";

    $tempdate=$date3;
    $total=0;
    $newdays=0;
    while($tempdate<=$date4){
       $dayname=dayofweek($tempdate);
       $showvalue=number_format($newvalue[$dayname],2);
       if($newvalue[$dayname]!=0&&$daysclosed[$tempdate]==0){echo "<tr bgcolor=white onMouseOver=this.bgColor='yellow' onMouseOut=this.bgColor='white'><td style=\"border:1px solid #E8E7E7;\">$tempdate</td><td style=\"border:1px solid #E8E7E7;\">$dayname</td><td style=\"border:1px solid #E8E7E7;\" align=right>$showvalue</td></tr>"; fwrite($fh, "$tempdate,$dayname,$newvalue[$dayname]\r\n");}
       if($averageday[$dayname]!=0&&$daysclosed[$tempdate]==0){$newdays++;$total+=$newvalue[$dayname];}
       $tempdate=nextday($tempdate);
    }

    $newvalue=urlencode(serialize($newvalue));
    $daysclosed=urlencode(serialize($daysclosed));
    $total=number_format($total,2);
    echo "<tr bgcolor=#E8E7E7><td style=\"border:1px solid #E8E7E7;\">$newdays</td><td style=\"border:1px solid #E8E7E7;\" align=right colspan=2>$total</td></tr>";
    echo "</table></center>";
    echo "</td></tr>";
    $filename=HTTP_EXPORTS."/report$loginid.csv";
    $showfile="<a href=$filename style=$style><font color=blue size=2>Excel File</font></a>";
    echo "<tr bgcolor=#CCCCFF><td>$showfile</td><td align=right><form action=budget_calc_update.php method=post style=\"margin:0;padding:0;display:inline;\"><input type=hidden name=newvalue value='$newvalue'><input type=hidden name=daysclosed value='$daysclosed'><input type=hidden name=date3 value=$date3><input type=hidden name=date4 value=$date4><input type=hidden name=businessid value=$budget_bus>";

    echo " <input type=hidden name=businessid2 value='$businessid2'><input type=submit value='Save' onclick='return contconfirm()'></form></td></tr>";
    echo "</table></center><p>";

    /////////////////////////////////////
    ///////BUDGET FOR CURRENT YEAR///////
    /////////////////////////////////////

    $myyear=substr($date3,0,4);

    echo "<center><table width=90% cellspacing=0 cellpadding=0 style=\"border:1px solid #CCCCFF;\"><tr bgcolor=#CCCCFF>";

    for($counter=1;$counter<=12;$counter++){
       if($counter==1){$monthname="JAN";}
       elseif($counter==2){$monthname="FEB";}
       elseif($counter==3){$monthname="MAR";}
       elseif($counter==4){$monthname="APR";}
       elseif($counter==5){$monthname="MAY";}
       elseif($counter==6){$monthname="JUN";}
       elseif($counter==7){$monthname="JUL";}
       elseif($counter==8){$monthname="AUG";}
       elseif($counter==9){$monthname="SEP";}
       elseif($counter==10){$monthname="OCT";}
       elseif($counter==11){$monthname="NOV";}
       elseif($counter==12){$monthname="DEC";}

       echo "<td align=right style=\"border:1px solid #CCCCFF;\" width=7.69%>$monthname</td>";
    }
    echo "<td align=right style=\"border:1px solid #CCCCFF;\" width=7.69%><b>Total</b></td>";
    echo "</tr><tr>";

    for($counter=1;$counter<=12;$counter++){

       if ($counter<10){$month="0$counter";}
       else{$month=$counter;}

       $monthstart="$myyear-$month-01";
       $monthend="$myyear-$month-31";

       $query44 = "SELECT SUM(amount) AS totalamt FROM vend_budget WHERE businessid = '$budget_bus' AND date >= '$monthstart' AND date <= '$monthend' AND type = '10'";
       $result44 = Treat_DB_ProxyOld::query($query44);

       $totalamt=Treat_DB_ProxyOld::mysql_result($result44,0,"totalamt");
       $total+=$totalamt;
       $totalamt=number_format($totalamt,2);
       echo "<td align=right style=\"border:1px solid #CCCCFF;\" width=7.69%>$totalamt</td>";
    }
    $total=number_format($total,2);
    echo "<td align=right style=\"border:1px solid #CCCCFF;\" width=7.69%><b>$total</b></td>";
    echo "</tr></table></center>";

    }

    }
//////////////////////////////END
    echo "<DIV ID=testdiv1 STYLE=position:absolute;visibility:hidden;background-color:white;layer-background-color:white;></DIV>";
    echo "<br><FORM ACTION=businesstrack.php method=post>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<center><INPUT TYPE=submit VALUE=' Return to Main Page'></FORM></center></body>";
}
//mysql_close();
fclose($fh);
?>