// Weather.com vars
var PARTNER_ID = '1089637222';
var LICENSE = '92e33b123d1feff0';

var Weather = function(id, options){
	this.divId = id;
	this.options = options || {};
	this.pid = PARTNER_ID;
	this.lid = LICENSE;

	this.loaded = false;
	this.xml = null;
	this.zip = this.options.zip;

	this.render();
	this.loadXML();
}
Weather.prototype = {
	render: function(){
		if(this.options.simple) this.renderSimple();
		else this.renderAccord();
	},
	renderSimple: function(){
		var main = $(this.divId);
		var ce = function(e){ return document.createElement(e); };
		var ap = function(t,p){ return p.appendChild(t); };
		this.r = {};
		
		// City Name
		this.r.city = ce('div');
		this.r.city.style.fontWeight = 'bold';
		ap(this.r.city, main);


		// Temperature Container
		this.r.tempCon = ce('div');
		var p = ce('p');
		ap(this.r.tempCon, main);
		ap(p, this.r.tempCon);

		// Icon
		this.r.icon = ce('img');
		this.r.icon.style.width = '32px';
		this.r.icon.style.height = '32px';
		this.r.icon.style.margin = '0 5px;';
		this.r.icon.align 		 = 'top';
		ap(this.r.icon, p);

		// Temperature
		this.r.temp = ce('span');
		this.r.temp.style.fontSize = '30px';
		this.r.temp.style.lineHeight = '30px';
		this.r.temp.style.color = '#1b1b1b';
		ap(this.r.temp, p);

		// Sky
		this.r.sky = ce('div');
		this.r.sky.style.borderBottom = '1px dotted #1b1b1b';
		this.r.sky.style.marginBottom = '10px';
		ap(this.r.sky, main);
	},
	loadXML: function(){
		var obj = this;
		var str = 'ajaxweather.php';
		var rest = '?cc=*&dayf=5&link=xoap&prod=xoap&par='+this.pid+'&key='+this.lid;
		new Ajax.Request(str, {method: 'GET', parameters: {'zip': this.zip, 'cc': '*', 'prod': 'xoap', 'dayf': 5, 'link': 'xoap', 'par': this.pid, 'key': this.lid}, onComplete: function(t){ obj.procXML(t); }});
	},
	procXML: function(t){
		this.loaded = true;
		this.xml = t.responseXML;
		this.loadVals();
		
		this.displayVals();
	},
	displayVals: function(){
		this.r.city.innerHTML = this.loc_name;	
		this.r.temp.innerHTML = this.today.temp;
		this.r.icon.src       = 'w_img/'+this.today.icon + '.png';
		this.r.sky.innerHTML  = this.today.sky;
	},
	loadVals: function(){
		var root = getNode('weather', this.xml);
		var loc = getNode('loc', root);
		this.loc_name = nodeValue(getNode('dnam', loc));

		this.cc();	
	},
	cc: function(){
		var today = {};
		var root = getNode('weather', this.xml);
		var cc = getNode('cc', root);


		today.temp = nodeValue(getNode('tmp', cc))+'&deg;F';
		today.humidity = nodeValue(getNode('hmid', cc)) + "%";
		today.icon = nodeValue(getNode('icon', cc));
		today.sky  = nodeValue(getNode('t', cc));
		today.bar = nodeValue(getNode('r', getNode('bar', cc)))+'"';
		today.wind = 'From '+nodeValue(getNode('t', getNode('wind', cc)))+' at '+nodeValue(getNode('s', getNode('wind', cc)))+ 'mph';
		today.flike = nodeValue(getNode('flik', cc))+'&deg;F';
		today.dewp = nodeValue(getNode('dewp', cc))+'&deg;F';

		this.today = today;
	},
}

var nodeValue = function(n){
	return n.textContent || n.text;
}
var getNode = function(n, p, c){
	var collection = c || false;
	if(collection){ 
		return p.getElementsByTagName(n);
	}
	else{
		return p.getElementsByTagName(n)[0];
	}
}
