var Accordion = function(id, options){
	this.id = id;
	this.heads = [];
	this.cons = [];
	this.open;
	this.options = options;

	this.schema();
	this.bindListeners();
}
Accordion.prototype = {
	open: function(i){

		var con = this.cons[i];
		if(con.visible()){
			new Effect.BlindUp(con, {duration: .75});
			return false;
		}
		else{
			var anim = [];
			for(var x = 0; x < this.heads.length; x++){
				if(x != i){
					if(this.cons[x].visible()){
						anim[anim.length] = new Effect.BlindUp(this.cons[x], {sync: true});
					}
				}
			}
			anim[anim.length] = new Effect.BlindDown(con, {sync: true});
			new Effect.Parallel(anim, {duration: .75});
		}
	},
	bindListeners: function(){
		var obj = this;
		for(var i = 0; i < this.heads.length; i++){
			this.heads[i].divIndex = i;
			this.heads[i].onclick = function(){ obj.open(this.divIndex);  }
		}
	},
	schema: function(){
		this.heads = $$('.accordHead');
		this.cons = $$('.accordCon');

		// Hide all
		for(var i = 0; i < this.heads.length; i++) $(this.cons[i]).hide();

		// If default open, open it
		if(this.options.open > -1){ 
			for(var i = 0; i < this.heads.length; i++){
				if(i == this.options.open) this.open(i);
			}
		} // */
	}
}
