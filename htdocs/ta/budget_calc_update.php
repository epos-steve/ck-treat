<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once(DOC_ROOT.'/bootstrap.php');

function rndm_color_code($b_safe = TRUE) {
    
    //make sure the parameter is boolean
    if(!is_bool($b_safe)) {return FALSE;}
    
    //if a browser safe color is requested then set the array up
    //so that only a browser safe color can be returned
    if($b_saafe) {
        $ary_codes = array('00','33','66','99','CC','FF');
        $max = 5; //the highest array offest
    //if a browser safe color is not requested then set the array
    //up so that any color can be returned.
    } else {
        $ary_codes = array();
        for($i=0;$i<16;$i++) {
            $t_1 = dechex($i);
            for($j=0;$j<16;$j++) {
                $t_2 = dechex($j);
                $ary_codes[] = "$t_1$t_2";
            } //end for j
        } //end for i
        $max = 256; //the highest array offset
    } //end if
    
    $retVal = '';
    
    //generate a random color code
    for($i=0;$i<3;$i++) {
        $offset = rand(0,$max);
        $retVal .= $ary_codes[$offset];
    } //end for i
    
    return $retVal;
} //end rndm_color_code

$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");

/*$user=$_COOKIE["usercook"];
$pass=$_COOKIE["passcook"];
$companyid=$_COOKIE["compcook"];*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook');
$companyid = \EE\Controller\Base::getSessionCookieVariable('compcook');

   /*$businessid=$_POST["businessid"];
   $businessid2=$_POST["businessid2"];*/

$businessid = \EE\Controller\Base::getPostVariable('businessid');
$businessid2 = \EE\Controller\Base::getPostVariable('businessid2');

   /*$newvalue=unserialize(urldecode($_POST["newvalue"]));
   $daysclosed=unserialize(urldecode($_POST["daysclosed"]));
   $date3=$_POST["date3"];
   $date4=$_POST["date4"];*/

$newvalue=unserialize(urldecode(\EE\Controller\Base::getPostVariable('newvalue')));
$daysclosed=unserialize(urldecode(\EE\Controller\Base::getPostVariable('daysclosed')));
$date3 = \EE\Controller\Base::getPostVariable('date3');
$date4 = \EE\Controller\Base::getPostVariable('date4');

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( mysql_error());

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=Treat_DB_ProxyOld::mysql_numrows($result);

$security_level=Treat_DB_ProxyOld::mysql_result($result,0,"security_level");
$bid=Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
$bid2=Treat_DB_ProxyOld::mysql_result($result,0,"busid2");
$bid3=Treat_DB_ProxyOld::mysql_result($result,0,"busid3");
$bid4=Treat_DB_ProxyOld::mysql_result($result,0,"busid4");
$bid5=Treat_DB_ProxyOld::mysql_result($result,0,"busid5");
$bid6=Treat_DB_ProxyOld::mysql_result($result,0,"busid6");
$bid7=Treat_DB_ProxyOld::mysql_result($result,0,"busid7");
$bid8=Treat_DB_ProxyOld::mysql_result($result,0,"busid8");
$bid9=Treat_DB_ProxyOld::mysql_result($result,0,"busid9");
$bid10=Treat_DB_ProxyOld::mysql_result($result,0,"busid10");
$cid=Treat_DB_ProxyOld::mysql_result($result,0,"companyid");
$loginid=Treat_DB_ProxyOld::mysql_result($result,0,"loginid");

$location="budget_calc.php";
//header('Location: ./' . $location);

if ($num != 1)
{
    echo "<center><h3>Failed</h3></center>";
}

else
{
   $query = "SELECT * FROM business WHERE businessid = '$businessid'";
   $result = Treat_DB_ProxyOld::query($query);

   $businessname=Treat_DB_ProxyOld::mysql_result($result,0,"businessname");
   $budget_type=Treat_DB_ProxyOld::mysql_result($result,0,"budget_type");

   if($budget_type==1){

       $tempdate=$date3;
       while($tempdate<=$date4){
          $dayname=dayofweek($tempdate);

          if($newvalue[$dayname]!=0&&$daysclosed[$tempdate]==0){
             $newvalue[$dayname]=round($newvalue[$dayname],2);

             $query = "SELECT * FROM vend_budget WHERE businessid = '$businessid' AND date = '$tempdate' AND type = '10'";
	     $result = Treat_DB_ProxyOld::query($query);
	     $num=Treat_DB_ProxyOld::mysql_numrows($result);

             if($num==0){
                $query = "INSERT INTO vend_budget (businessid,date,amount,type) VALUES ('$businessid','$tempdate','$newvalue[$dayname]','10')";
	        $result = Treat_DB_ProxyOld::query($query);

             }
             else{
                $query = "UPDATE vend_budget SET amount = '$newvalue[$dayname]' WHERE businessid = '$businessid' AND date = '$tempdate' AND type = '10'";
	        $result = Treat_DB_ProxyOld::query($query);
             }
          }
          $tempdate=nextday($tempdate);
       }
   }
   else{

   }

   echo "<center>SAVED</center><p><br><p><br><p><br><p><br><center><form action=budget_calc.php method=post><input type=hidden name=businessid2 value='$businessid2'><input type=submit value='Return'></form></center>";
}
//mysql_close();
?>