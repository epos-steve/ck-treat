<?php

function rndm_color_code($b_safe = TRUE) {
    
    //make sure the parameter is boolean
    if(!is_bool($b_safe)) {return FALSE;}
    
    //if a browser safe color is requested then set the array up
    //so that only a browser safe color can be returned
    if($b_saafe) {
        $ary_codes = array('00','33','66','99','CC','FF');
        $max = 5; //the highest array offest
    //if a browser safe color is not requested then set the array
    //up so that any color can be returned.
    } else {
        $ary_codes = array();
        for($i=0;$i<16;$i++) {
            $t_1 = dechex($i);
            for($j=0;$j<16;$j++) {
                $t_2 = dechex($j);
                $ary_codes[] = "$t_1$t_2";
            } //end for j
        } //end for i
        $max = 256; //the highest array offset
    } //end if
    
    $retVal = '';
    
    //generate a random color code
    for($i=0;$i<3;$i++) {
        $offset = rand(0,$max);
        $retVal .= $ary_codes[$offset];
    } //end for i
    
    return $retVal;
} //end rndm_color_code

/*
function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+4);
        if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
        elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
        $diff = substr($diff, 0, $dot+3);
        return $diff;
}

function nextday($nextd,$day_format){ //Function returns next day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monn = substr($nextd, 5, 2);
   $dayn = substr($nextd, 8, 2);
   $yearn = substr($nextd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monn, $dayn+1, $yearn));
   return $tempdate;
}

function prevday($prevd,$day_format){ //Function returns previous day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monp = substr($prevd, 5, 2);
   $dayp = substr($prevd, 8, 2);
   $yearp = substr($prevd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monp, $dayp-1, $yearp));
   return $tempdate;
}

function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}
*/

include_once( 'db.php' );
if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
require_once( DOC_ROOT . '/bootstrap.php' );

$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";

$user = $_COOKIE['usercook'];
$pass = $_COOKIE['passcook'];
$viewtype=$_GET["viewtype"];
$companyid=$_GET["cid"];
$viewbus=$_GET["viewbus"];
$viewroute=$_GET["viewr"];
$vgroup=$_GET["vgroup"];
$show=$_GET["show"];
$expand=$_GET["exp"];

if($viewbus==""||$viewtype==""){
	$viewtype=$_POST["viewtype"];
	$companyid=$_POST["cid"];
	$viewbus=$_POST["viewbus"];
	$show=$_POST["show"];
	$fixed_amount=$_POST["fixed_amount"];
	$showroute=$_POST["showroute"];
	$date_range=$_POST["date_range"];
	$rolling=$_POST["rolling"];
	$default=$_POST["default"];
}

///////////GET DEFAULT VIEW
if($show!=1){
	$fixed_amount = $_COOKIE['dbserv_fixed_amount'];
	$showroute = $_COOKIE['dbserv_showroute'];
	$date_range = $_COOKIE['dbserv_date_range'];
	$rolling = $_COOKIE['dbserv_rolling'];
	$viewtype = $_COOKIE['dbserv_viewtype'];
	$viewbus = $_COOKIE['dbserv_viewbus'];
	$show=2;
}

$viewr=$viewroute;

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num = mysql_numrows( $result );

if ($num!=0){
	$security_level = @mysql_result( $result, 0, 'security_level' );
	$bid = @mysql_result( $result, 0, 'businessid' );
	$cid = @mysql_result( $result, 0, 'companyid' );
	$loginid = @mysql_result( $result, 0, 'loginid' );
}

if ($num != 1 || $user == "" || $pass == "") 
{
    echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
    $colors = array(1 => "#444455", 2 => "#ff0000", 3 => "#00ff00", 4 => "#0000ff", 5 => "#804000", 6 => "#00ffff", 7 => "#ff00ff", 8 => "#c0c0c0", 9 => "#000088", 10 => "#ff8040", 11 => "#98AFC7", 12 => "#800080", 13 => "#808000", 14 => "#3EA99F", 15 => "#EAC117");

    $query = "SELECT * FROM company WHERE companyid = '$companyid'";
	$result = Treat_DB_ProxyOld::query($query);

	$companyname = @mysql_result( $result, 0, 'companyname' );
	$weekend = @mysql_result( $result, 0, 'week_end' );

    if($viewtype==1){
       $compquery="business.companyid != '2'";
    }
    elseif($viewtype==2){
       $compquery="business.companyid = company.companyid AND company.company_type = '$viewbus'";
       $fromtables=",company";
    }
    elseif($viewtype==3){
       $compquery="business.companyid = company.companyid  AND company.companyid = '$viewbus'";
       $fromtables=",company";
    }
    elseif($viewtype==4){
       $compquery="business.districtid = district.districtid AND district.divisionid = '$viewbus'";
       $fromtables=",district";
    }
    elseif($viewtype==5){
       $compquery="business.districtid = '$viewbus'";
    }
    elseif($viewtype==6&&$viewbus>0){
       $compquery="business.businessid = '$viewbus'";
    }
	
	?>
	<head>
	
	<script type="text/javascript" src="javascripts/prototype.js"></script>
	<script type="text/javascript" src="javascripts/scriptaculous.js"></script>
	
	<SCRIPT LANGUAGE="JavaScript">
	<!-- Web Site:  http://dynamicdrive.com -->

	<!-- This script and many more are available free online at -->
	<!-- The JavaScript Source!! http://javascript.internet.com -->

	<!-- Begin
	function disableForm(theform) {
	if (document.all || document.getElementById) {
	for (i = 0; i < theform.length; i++) {
	var tempobj = theform.elements[i];
	if (tempobj.type.toLowerCase() == "submit" || tempobj.type.toLowerCase() == "reset")
	tempobj.disabled = true;
	}

	}
	else {
	alert("The form has been submitted.  But, since you're not using IE 4+ or NS 6, the submit button was not disabled on form submission.");
	return false;
    }
	}
	//  End -->
	
	function popup(url)
	{
		popwin=window.open(url,"Service","location=no,menubar=no,titlebar=no,scrollbars=yes,resizeable=yes,height=600,width=700");   
	}
	</script>
	
	</head>
	<body style="margin:2px;">
	<?

	/*
	///////////FIGURE DATES
    $tempdate=date("Y-m-d");

	$week[1]=0;
	$counter=9;
		
	while($week[1]==0){
		while(dayofweek($tempdate)!=$weekend){$tempdate=prevday($tempdate);}
		$week[$counter]=$tempdate;

		$tempdate=prevday($tempdate);
		$counter--;
	}
    
	$viewroute=explode("-",$viewroute);
	
	if($viewroute[0]=="b"){$myquery="vend_machine_sales.businessid = '$viewroute[1]' AND"; $fromtables.="";}
	elseif($viewroute[0]=="r"){$myquery="login_route.login_routeid = '$viewroute[1]' AND login_route.route = vend_machine_sales.route AND"; $fromtables.=",login_route";}
	elseif($viewroute[0]=="s"){$myquery="login_route.supervisor = '$viewroute[1]' AND login_route.route = vend_machine_sales.route AND"; $fromtables.=",login_route";}
	else{$myquery="";}
	*/
	
	////////////////////////////
	////////AVERAGE COLLECTS////
	////////////////////////////
	if($show>0){
		////////////////////////////////////
		///////////////FIGURE DATES/////////
		////////////////////////////////////
		$year=date("Y");
		$enddate=date("Y-m-d");
		$today=$enddate;
		while(dayofweek($enddate)!="Tuesday"){$enddate=prevday($enddate); if(substr($enddate,0,4)!=$year){$year--;}}
		while(dayofweek($enddate)!="Friday"){$enddate=prevday($enddate); if(substr($enddate,0,4)!=$year){$year--;}}
	
		$startdate=$enddate;
		
		if($date_range==1){
			for($counter=1;$counter<=84;$counter++){$startdate=prevday($startdate);}
		}
		elseif($date_range==2){
			for($counter=1;$counter<=182;$counter++){$startdate=prevday($startdate);}
		}
		elseif($date_range==3){
			$startyear=$year-1;
			$startmonth=substr($enddate,5,2);
			$startday=substr($enddate,8,2);
			
			$startdate="$startyear-$startmonth-$startday";
		}
		elseif($date_range==4){
			$startyear=$year-2;
			$startmonth=substr($enddate,5,2);
			$startday=substr($enddate,8,2);
			
			$startdate="$startyear-$startmonth-$startday";
		}
		else{
			$startdate= "$year-01-01";
		}
		
		$date_array[0]=$startdate;
		$counter=1;
		$tempdate=$startdate;
		while($tempdate<=$enddate){
			if(dayofweek($tempdate)=="Friday"){
				$date_array[$counter]=$tempdate; 
				$counter++;
			}
			$tempdate=nextday($tempdate);
		}
		$showweeks=$counter-1;
		
		$prevyear=prevday($startdate);
		$prevyearstart=$prevyear;
		for($mycount=1;$mycount<=6;$mycount++){$prevyearstart=prevday($prevyearstart);}
		
		////fix startdate
		$date_array[0]=$startdate;
		while(dayofweek($date_array[0])!="Saturday"){$date_array[0]=prevday($date_array[0]);}
		
		/*
		//////rolling average
		if($rolling==2){
			$tempdate=$date_array[0];
			for($mycount=-1;$mycount>=-8;$mycount--){
				for($mycount2=1;$mycount2<=7;$mycount2++){$tempdate=prevday($tempdate);}
				$date_array[$mycount]=$tempdate;
			}
		}
		*/
	
		///////////////////////////////////////////
		/////////CREATE ARRAY OF WHO TO DISPLAY////
		///////////////////////////////////////////
		$counter=1;
		if(1==1){
			//////////////Budgets
			$query = "SELECT DISTINCT business.businessid,business.businessname FROM business$fromtables WHERE $compquery ORDER BY business.businessname";
			$result = Treat_DB_ProxyOld::query($query);

			while ( $r = @mysql_fetch_array( $result ) ){
				$businessid=$r["businessid"];
				$business_name=$r["businessname"];
				
				$query2="SELECT * FROM vend_locations WHERE vend_locations.unitid = '$businessid'";
				$result2 = Treat_DB_ProxyOld::query($query2);
				$num2 = mysql_numrows( $result2 );
				
				if($num2>0){
					$showwho[$counter]=$_POST["show$counter"];
					$counter++;
				}
			}
		}
		if($viewtype<3&&$viewtype>0){
			$showwho[$counter]=$_POST["show$counter"];
			$counter++;
		}
		if($viewtype<7&&$viewtype>0){
			//////////////DIVISIONS
			$query = "SELECT DISTINCT business.businessid,business.businessname FROM business$fromtables WHERE $compquery ORDER BY business.businessname";
			$result = Treat_DB_ProxyOld::query($query);

			while ( $r = @mysql_fetch_array( $result ) ) {
				$businessid=$r["businessid"];
				$business_name=$r["businessname"];
					
				$query2="SELECT * FROM vend_locations WHERE vend_locations.unitid = '$businessid'";
				$result2 = Treat_DB_ProxyOld::query($query2);
				$num2 = mysql_numrows( $result2 );
				
				if($num2>0){
					$showwho[$counter]=$_POST["show$counter"];
					$counter++;
				}
			}
			
			/////////////TECHNICIANS
			$query = "SELECT vend_machine_repairman.id,vend_machine_repairman.repairman,vend_machine_repairman.businessid FROM vend_machine_repairman,business$fromtables WHERE vend_machine_repairman.businessid = business.businessid AND $compquery ORDER BY vend_machine_repairman.businessid,vend_machine_repairman.repairman";
			$result = Treat_DB_ProxyOld::query($query);

			while($r=mysql_fetch_array($result)){
				$repairmanid=$r["id"];
				$repairman=$r["repairman"];
				$mybid=$r["businessid"];
					
				$showwho[$counter]=$_POST["show$counter"];
				$counter++;
			}
			
			/////////////CSR
			$query = "SELECT vend_salesman.salesid,vend_salesman.sales_name,vend_salesman.businessid FROM vend_salesman,business$fromtables WHERE vend_salesman.businessid = business.businessid AND $compquery ORDER BY vend_salesman.businessid,vend_salesman.sales_name";
			$result = Treat_DB_ProxyOld::query($query);

			while($r=mysql_fetch_array($result)){
				$salesid=$r["salesid"];
				$sales_name=$r["sales_name"];
				$mybid=$r["businessid"];
					
				$showwho[$counter]=$_POST["show$counter"];
				
				$counter++;
			}
		}
		
		//////GET DEFAULT VIEW
		if($show==2){
			$showwho = $_COOKIE['dbserv_default_view'];
			$showwho=unserialize($showwho);
		}
		
		//////////SET COOKIES/DEFAULT VIEW
		
		if($default==1){
			$expire = 60 * 60 * 24 * 60 + time(); 
			setcookie("dbserv_default_view",serialize($showwho),$expire);
			setcookie("dbserv_fixed_amount",$fixed_amount,$expire);
			setcookie("dbserv_showroute",$showroute,$expire);
			setcookie("dbserv_date_range",$date_range,$expire);
			setcookie("dbserv_rolling",$rolling,$expire);
			setcookie("dbserv_viewtype",$viewtype,$expire);
			setcookie("dbserv_viewbus",$viewbus,$expire);
		}
		
		//////////CREATE FILE/VIEW
		
		echo "<table cellspacing=0 cellpadding=0 width=100%>";
		echo "<tr valign=top><td width=100% id='showgraph'><a href=\"" . HTTP_EXPORTS . "/graph$loginid-2.html\" target='_blank' style=$style><font color=blue size=1>[+] ZOOM</font></a>";
		
		$data="<html><head></head><body><table cellspacing=0 cellpadding=0 width=100%><tr valign=top><td width=100% id='showgraph'><center><h3>";
		$data.="Service Calls Per Collects</h3></center>";
		
		///////////////////////////////
		/////////GET DATA//////////////
		///////////////////////////////
		$lastvalue="";
		if ( !is_array( $showwho ) ) {
			$showwho = array();
		}
		foreach ( $showwho as $key => $value ) {
		
			if($lastvalue!=$value){$collections=0;$totalcount=0;}
		
			if($value!=""){				
				$value2=explode("-",$value);
				////ALL
				if($value2[0]=="fix"){
					for($mycount=1;$mycount<=$showweeks;$mycount++){
						$showwho[$value][$date_array[$mycount]]=$fixed_amount;
					}
				}
				elseif($value2[0]=="a"){
					for($mycount=1;$mycount<=$showweeks;$mycount++){
						$mycount2=$mycount-1;

						$query = "SELECT SUM(collects) AS total_collects, SUM(service) AS total_service FROM dashboard_machine WHERE date > '$date_array[$mycount2]' AND date <= '$date_array[$mycount]'";
						$result = Treat_DB_ProxyOld::query($query);

						$total_collects = @mysql_result( $result, 0, 'total_collects' );
						$total_service = @mysql_result( $result, 0, 'total_service' );

						$showwho[$value][$date_array[$mycount]]=round($total_service/($total_collects/1000), 2);
						if($showwho[$value][$date_array[$mycount]]==""){$showwho[$value][$date_array[$mycount]]=0;}
					}
				}
				elseif($value2[0]=="b"){
					for($mycount=1;$mycount<=$showweeks;$mycount++){
						$mycount2=$mycount-1;
						
						$query = "SELECT SUM(collects) AS total_collects, SUM(service) AS total_service FROM dashboard_machine WHERE businessid = '$value2[1]' AND date > '$date_array[$mycount2]' AND date <= '$date_array[$mycount]'";
						$result = Treat_DB_ProxyOld::query($query);

						$total_collects = @mysql_result( $result, 0, 'total_collects' );
						$total_service = @mysql_result( $result, 0, 'total_service' );

						$showwho[$value][$date_array[$mycount]]=round($total_service/($total_collects/1000), 2);
						if($showwho[$value][$date_array[$mycount]]==""){$showwho[$value][$date_array[$mycount]]=0;}
					}
				}
				elseif($value2[0]=="s"){
					for($mycount=1;$mycount<=$showweeks;$mycount++){
						$mycount2=$mycount-1;
						
						$query = "SELECT SUM(dashboard_machine.collects) AS total_collects, SUM(dashboard_machine.service) AS total_service FROM dashboard_machine,login_route WHERE dashboard_machine.date > '$date_array[$mycount2]' AND dashboard_machine.date <= '$date_array[$mycount]' AND dashboard_machine.login_routeid = login_route.login_routeid AND login_route.locationid = '$value2[1]'";
						$result = Treat_DB_ProxyOld::query($query);

						$total_collects = @mysql_result( $result, 0, 'total_collects' );
						$total_service = @mysql_result( $result, 0, 'total_service' );

						$showwho[$value][$date_array[$mycount]]=round($total_service/($total_collects/1000), 2);
						if($showwho[$value][$date_array[$mycount]]==""){$showwho[$value][$date_array[$mycount]]=0;}
					}
				}
			}
			$lastvalue=$value;
		}
		
		///////////////////////////////
		/////////CREATE GRAPH//////////
		///////////////////////////////
		
		?>
		<applet code="LineGraphApplet.class" archive="Linegraph.jar" width="100%" height="310">

		<!-- Start Up Parameters -->
		<PARAM name="LOADINGMESSAGE" value="Creating Chart - Please Wait.">
		<PARAM name="STEXTCOLOR"     value="#000060">
		<PARAM name="STARTUPCOLOR"   value="#FFFFFF">
				
		<?php
		
		////WTF
		$data.="<applet code=\"LineGraphApplet.class\" archive=\"Linegraph.jar\" width=\"100%\" height=\"750\">";
		$data.="<PARAM name=\"LOADINGMESSAGE\" value=\"Creating Chart - Please Wait.\">";
		$data.="<PARAM name=\"STEXTCOLOR\"     value=\"#000060\">";
		$data.="<PARAM name=\"STARTUPCOLOR\"   value=\"#FFFFFF\">";
		
		$series=1;
		$myseries="series";
		for($mycount=1;$mycount<=$showweeks;$mycount++){
			$seriescount=1;
			foreach($showwho AS $key => $value){
				if($value!=""){
					echo "<PARAM name=data$series$myseries$seriescount value='{$showwho[$value][$date_array[$mycount]]}'>";
					$data.="<PARAM name=data$series$myseries$seriescount value='{$showwho[$value][$date_array[$mycount]]}'>";
					$seriescount++;
				}
			}
			$series++;
		}
				
		?>

		<!-- Properties -->
		<PARAM name="grid"		      value="true">
		<PARAM name="titlefontsize"	  value="0">
		<PARAM name="xtitlefontsize"	  value="0">
		<PARAM name="ytitlefontsize"	  value="0">
		<PARAM name="autoscale"            value="true">
		<PARAM name="gridbgcolor"          value="#FFFFFF">
		<?
		
		////WTF
		$data.="<PARAM name=\"grid\"		      value=\"true\">";
		$data.="<PARAM name=\"titlefontsize\"	  value=\"0\">";
		$data.="<PARAM name=\"xtitlefontsize\"	  value=\"0\">";
		$data.="<PARAM name=\"ytitlefontsize\"	  value=\"0\">";
		$data.="<PARAM name=\"autoscale\"            value=\"true\">";
		$data.="<PARAM name=\"gridbgcolor\"          value=\"#FFFFFF\">";
		
		$series=1;
		foreach($showwho AS $key => $value){
			if($value!=""){
				///////IF GREATER THAN COLOR ARRAY, FIND RANDOM COLOR
				if($series>15){$colors[$series]=rndm_color_code(); $colors[$series]="#$colors[$series]";}
			
				if($value=="fix-1"){
					echo "<PARAM name=\"series$series\"              value=\"$colors[$series]|5|6|true|dotted\">";
					$data.="<PARAM name=\"series$series\"              value=\"$colors[$series]|5|6|true|dotted\">";
				}
				else{
					echo "<PARAM name=\"series$series\"              value=\"$colors[$series]|10|10|false|dotted\">";
					$data.="<PARAM name=\"series$series\"              value=\"$colors[$series]|10|10|false|dotted\">";
				}
				$series++;
			}
		}
		?>	
		<PARAM name="ndecplaces"           value="2">
		<PARAM name="labelOrientation"     value="Up Angle">
		<PARAM name="xlabel_pre"           value="">
		<PARAM name="xlabel_font"          value="Arial,N,9">
		<PARAM name="ylabel_font"          value="Arial,N,9">
		<PARAM name="ylabel_pre"           value="">
		<?	
		
		///WTF
		$data.="<PARAM name=\"ndecplaces\"           value=\"0\">";
		$data.="<PARAM name=\"labelOrientation\"     value=\"Up Angle\">";
		$data.="<PARAM name=\"xlabel_pre\"           value=\"\">";
		$data.="<PARAM name=\"xlabel_font\"          value=\"Arial,N,9\">";
		$data.="<PARAM name=\"ylabel_font\"          value=\"Arial,N,9\">";
		$data.="<PARAM name=\"ylabel_pre\"           value=\"\">";
		
		$spacer=1;
		for($mycount=1;$mycount<=$showweeks;$mycount++){
				
				$g_day=substr($date_array[$mycount],8,2);
				$g_month=substr($date_array[$mycount],5,2);
				$g_year=substr($date_array[$mycount],2,2);
				
				if(($date_range<1&&($spacer==2||$spacer==4||$spacer==6||$spacer==8||$spacer==10))||($date_range==1)||($date_range==2)||($date_range==3&&($spacer==5||$spacer==10))||($date_range==4&&$spacer==10)){
					echo "<PARAM name='label$mycount' value='$g_month/$g_day/$g_year'>";
					$data.="<PARAM name='label$mycount' value='$g_month/$g_day/$g_year'>";
				}
				
				$spacer++;
				if($spacer==11){$spacer=1;}
		}	
		?>		
		</applet>
		<?
		
		$data.="</applet>";
		
		///////////////////////////////
		////////SHOW LEGEND////////////
		///////////////////////////////
		$mycount=1;
		$mycount2=1;
		echo "<table cellspacing=0 cellpadding=0><tr valign=bottom>";
		$data.="<table cellspacing=0 cellpadding=0><tr>";
		
		foreach($showwho AS $key => $value){
			if($value!=""){
				$value2=$value;
				$value=explode("-",$value);
				
				if($value[0]=="fix"){
					//<table cellspacing=0 cellpadding=0><tr><td><img src=../roundtop.gif style=\"vertical-align:top;\"></td></tr><tr><td><img src=../roundbottom.gif style=\"vertical-align:bottom;\"></td></tr></table>
					$fixed_amount=number_format($fixed_amount,0);
					echo "<td bgcolor=$colors[$mycount] style=\"width: 14px;border-style: solid;border-color: $colors[$mycount];border-width: 1px 0 1px 1px;\" align=left><font size=2>&nbsp;</font></td><td style=\"border-style: solid;border-color: $colors[$mycount];border-width: 1px 0 1px 0;\"><font size=2>&nbsp;Fixed Amount&nbsp;</font></td><td align=right style=\"border-style: solid;border-color: $colors[$mycount];border-width: 1px 0 1px 0;\"><font size=2 color=#777777>$fixed_amount</font></td><td style=\"border-style: solid;border-color: $colors[$mycount];border-width: 1px 1px 1px 0;\"><font size=2>&nbsp;</font></td><td style=\"width: 2px;\"></td>";
					$data.="<td bgcolor=$colors[$mycount] style=\"width: 14px;border-style: solid;border-color: $colors[$mycount];border-width: 1px 0 1px 1px;\"><font size=2>&nbsp;</font></td><td style=\"border-style: solid;border-color: $colors[$mycount];border-width: 1px 0 1px 0;\"><font size=2>&nbsp;Fixed Amount&nbsp;</font></td><td align=right style=\"border-style: solid;border-color: $colors[$mycount];border-width: 1px 0 1px 0;\"><font size=2 color=#777777 >$fixed_amount</font></td><td style=\"border-style: solid;border-color: $colors[$mycount];border-width: 1px 1px 1px 0;\"><font size=2>&nbsp;</font></td><td style=\"width: 2px;\"></td>";
					$mycount++;
					$mycount2++;
				}
				elseif($value[0]=="bgt1"){
					echo "<td bgcolor=$colors[$mycount] style=\"width: 14px;border-style: solid;border-color: $colors[$mycount];border-width: 1px 0 1px 1px;\"><font size=2>&nbsp;</font></td><td style=\"border-style: solid;border-color: $colors[$mycount];border-width: 1px 0 1px 0;\"><font size=2>&nbsp;All Budgets&nbsp;</font></td><td style=\"border-style: solid;border-color: $colors[$mycount];border-width: 1px 0 1px 0;\"><font size=2>&nbsp;</font></td><td style=\"border-style: solid;border-color: $colors[$mycount];border-width: 1px 1px 1px 0;\"><font size=2>&nbsp;</font></td><td style=\"width: 2px;\"></td>";
					$data.="<td bgcolor=$colors[$mycount] style=\"width: 14px;border-style: solid;border-color: $colors[$mycount];border-width: 1px 0 1px 1px;\"><font size=2>&nbsp;</font></td><td style=\"border-style: solid;border-color: $colors[$mycount];border-width: 1px 0 1px 0;\"><font size=2>&nbsp;All Budgets&nbsp;</font></td><td style=\"border-style: solid;border-color: $colors[$mycount];border-width: 1px 0 1px 0;\"><font size=2>&nbsp;</font></td><td style=\"border-style: solid;border-color: $colors[$mycount];border-width: 1px 1px 1px 0;\"><font size=2>&nbsp;</font></td><td style=\"width: 2px;\"></td>";
					$mycount++;
					$mycount2++;
				}
				elseif($value[0]=="bgt"){
					$query2="SELECT businessname FROM business WHERE businessid = '$value[1]'";
					$result2 = Treat_DB_ProxyOld::query($query2);
					
					$displayname = @mysql_result( $result2, 0, 'businessname' );
				
					echo "<td bgcolor=$colors[$mycount] style=\"width: 14px;border-style: solid;border-color: $colors[$mycount];border-width: 1px 0 1px 1px;\"><font size=2>&nbsp;</font></td><td style=\"border-style: solid;border-color: $colors[$mycount];border-width: 1px 1px 1px 0;\" colspan=3><font size=2>&nbsp;$displayname Budget&nbsp;</font></td><td style=\"width: 2px;\"></td>";
					$data.="<td bgcolor=$colors[$mycount] style=\"width: 14px;border-style: solid;border-color: $colors[$mycount];border-width: 1px 0 1px 1px;\"><font size=2>&nbsp;</font></td><td style=\"border-style: solid;border-color: $colors[$mycount];border-width: 1px 1px 1px 0;\" colspan=3><font size=2>&nbsp;$displayname Budget&nbsp;</font></td><td style=\"width: 2px;\"></td>";
					$mycount++;
					$mycount2++;
				}
				elseif($value[0]=="a"){
					
					//////percent change
					$showchange=round(($showwho[$value2][$date_array[$showweeks]]-$showwho[$value2][$date_array[$showweeks-1]])/$showwho[$value2][$date_array[$showweeks-1]]*100,1);
					if($showchange>0){$showchange="<font color=red><b title='% Change from last week'>+$showchange%</b></font>";}
					elseif($showchange<0){$showchange="<font color=green><b title='% Change from last week'>$showchange%</b></font>";}
					else{$showchange="<b title='% Change from last week'>0%</b>";}
				
					$showcurrent=number_format($showwho[$value2][$date_array[$showweeks]],0);
					echo "<td bgcolor=$colors[$mycount] style=\"width: 14px;border-style: solid;border-color: $colors[$mycount];border-width: 1px 0 1px 1px;\"><font size=2>&nbsp;</font></td><td style=\"border-style: solid;border-color: $colors[$mycount];border-width: 1px 0 1px 0;\"><font size=2>&nbsp;Treat America&nbsp;&nbsp;</font></td><td align=right style=\"border-style: solid;border-color: $colors[$mycount];border-width: 1px 0 1px 0;\"><font size=2 color=#777777>$showcurrent</font></td><td align=right style=\"border-style: solid;border-color: $colors[$mycount];border-width: 1px 1px 1px 0;\"><font size=2>&nbsp;$showchange&nbsp;</font></td><td style=\"width: 2px;\"></td>";
					$data.="<td bgcolor=$colors[$mycount] style=\"width: 14px;border-style: solid;border-color: $colors[$mycount];border-width: 1px 0 1px 1px;\"><font size=2>&nbsp;</font></td><td style=\"border-style: solid;border-color: $colors[$mycount];border-width: 1px 0 1px 0;\"><font size=2>&nbsp;Treat America&nbsp;&nbsp;</font></td><td align=right style=\"border-style: solid;border-color: $colors[$mycount];border-width: 1px 0 1px 0;\"><font size=2 color=#777777>$showcurrent</font></td><td align=right style=\"border-style: solid;border-color: $colors[$mycount];border-width: 1px 1px 1px 0;\"><font size=2>&nbsp;$showchange&nbsp;</font></td><td style=\"width: 2px;\"></td>";
					$mycount++;
					$mycount2++;
				}
				elseif($value[0]=="b"){
				
					//////percent change
					$showchange=round(($showwho[$value2][$date_array[$showweeks]]-$showwho[$value2][$date_array[$showweeks-1]])/$showwho[$value2][$date_array[$showweeks-1]]*100,1);
					if($showchange>0){$showchange="<font color=red><b title='% Change from last week'>+$showchange%</b></font>";}
					elseif($showchange<0){$showchange="<font color=green><b title='% Change from last week'>$showchange%</b></font>";}
					else{$showchange="<b title='% Change from last week'>0%</b>";}
				
					$query2="SELECT businessname FROM business WHERE businessid = '$value[1]'";
					$result2 = Treat_DB_ProxyOld::query($query2);
					
					$displayname = @mysql_result( $result2, 0, 'businessname' );
				
					$showcurrent=number_format($showwho[$value2][$date_array[$showweeks]],0);
					echo "<td bgcolor=$colors[$mycount] style=\"width: 14px;border-style: solid;border-color: $colors[$mycount];border-width: 1px 0 1px 1px;\"><font size=2>&nbsp;</font></td><td style=\"border-style: solid;border-color: $colors[$mycount];border-width: 1px 0 1px 0;\"><a><font size=2 color=black onMouseOver=this.style.color='#FF9900';this.style.cursor='hand' onMouseOut=this.style.color='black'>&nbsp;$displayname&nbsp;&nbsp;</font></a></td><td align=right style=\"border-style: solid;border-color: $colors[$mycount];border-width: 1px 0 1px 0;\"><font size=2 color=#777777>$showcurrent</font></td><td align=right style=\"border-style: solid;border-color: $colors[$mycount];border-width: 1px 1px 1px 0;\"><font size=2>&nbsp;$showchange&nbsp;</font></td><td style=\"width: 2px;\"></td>";
					$data.="<td bgcolor=$colors[$mycount] style=\"width: 14px;border-style: solid;border-color: $colors[$mycount];border-width: 1px 0 1px 1px;\"><font size=2>&nbsp;</font></td><td style=\"border-style: solid;border-color: $colors[$mycount];border-width: 1px 0 1px 0;\"><a><font size=2 color=black onMouseOver=this.style.color='#FF9900';this.style.cursor='hand' onMouseOut=this.style.color='black'>&nbsp;$displayname&nbsp;&nbsp;</font></a></td><td align=right style=\"border-style: solid;border-color: $colors[$mycount];border-width: 1px 0 1px 0;\"><font size=2 color=#777777>$showcurrent</font></td><td align=right style=\"border-style: solid;border-color: $colors[$mycount];border-width: 1px 1px 1px 0;\"><font size=2>&nbsp;$showchange&nbsp;</font></td><td style=\"width: 2px;\"></td>";
					$mycount++;
					$mycount2++;
				}
				elseif($value[0]=="s"){
				
					//////percent change
					$showchange=round(($showwho[$value2][$date_array[$showweeks]]-$showwho[$value2][$date_array[$showweeks-1]])/$showwho[$value2][$date_array[$showweeks-1]]*100,1);
					if($showchange>0){$showchange="<font color=red><b title='% Change from last week'>+$showchange%</b></font>";}
					elseif($showchange<0){$showchange="<font color=green><b title='% Change from last week'>$showchange%</b></font>";}
					else{$showchange="<b title='% Change from last week'>0%</b>";}
				
					$query2="SELECT location_name FROM vend_locations WHERE locationid = '$value[1]'";
					$result2 = Treat_DB_ProxyOld::query($query2);

					$displayname = @mysql_result( $result2, 0, 'location_name' );
				
					$showcurrent=number_format($showwho[$value2][$date_array[$showweeks]],0);
					echo "<td bgcolor=$colors[$mycount] style=\"width: 14px;border-style: solid;border-color: $colors[$mycount];border-width: 1px 0 1px 1px;\">&nbsp;</td><td style=\"border-style: solid;border-color: $colors[$mycount];border-width: 1px 0 1px 0;\"><a><font size=2 color=black onMouseOver=this.style.color='#FF9900';this.style.cursor='hand' onMouseOut=this.style.color='black'>&nbsp;$displayname&nbsp;&nbsp;</font></a></td><td align=right style=\"border-style: solid;border-color: $colors[$mycount];border-width: 1px 0 1px 0;\"><font size=2 color=#777777>$showcurrent</font></td><td align=right style=\"border-style: solid;border-color: $colors[$mycount];border-width: 1px 1px 1px 0;\"><font size=2>&nbsp;$showchange&nbsp;</font></td><td style=\"width: 2px;\"></td>";
					$data.="<td bgcolor=$colors[$mycount] style=\"width: 14px;border-style: solid;border-color: $colors[$mycount];border-width: 1px 0 1px 1px;\">&nbsp;</td><td style=\"border-style: solid;border-color: $colors[$mycount];border-width: 1px 0 1px 0;\"><a><font size=2 color=black onMouseOver=this.style.color='#FF9900';this.style.cursor='hand' onMouseOut=this.style.color='black'>&nbsp;$displayname&nbsp;&nbsp;</font></a></td><td align=right style=\"border-style: solid;border-color: $colors[$mycount];border-width: 1px 0 1px 0;\"><font size=2 color=#777777>$showcurrent</font></td><td align=right style=\"border-style: solid;border-color: $colors[$mycount];border-width: 1px 1px 1px 0;\"><font size=2>&nbsp;$showchange&nbsp;</font></td><td style=\"width: 2px;\"></td>";
					$mycount++;
					$mycount2++;
				}
				elseif($value[0]=="r"){
				
					//////percent change
					$showchange=round(($showwho[$value2][$date_array[$showweeks]]-$showwho[$value2][$date_array[$showweeks-1]])/$showwho[$value2][$date_array[$showweeks-1]]*100,1);
					if($showchange>0){$showchange="<font color=red><b title='% Change from last week'>+$showchange%</b></font>";}
					elseif($showchange<0){$showchange="<font color=green><b title='% Change from last week'>$showchange%</b></font>";}
					else{$showchange="<b title='% Change from last week'>0%</b>";}
				
					$query2="SELECT route FROM login_route WHERE login_routeid = '$value[1]'";
					$result2 = Treat_DB_ProxyOld::query($query2);
					
					$displayname = @mysql_result( $result2, 0, 'route' );
				
					$showcurrent=number_format($showwho[$value2][$date_array[$showweeks]],0);
					echo "<td bgcolor=$colors[$mycount] style=\"width: 14px;border-style: solid;border-color: $colors[$mycount];border-width: 1px 0 1px 1px;\"><font size=2>&nbsp;</font></td><td style=\"border-style: solid;border-color: $colors[$mycount];border-width: 1px 0 1px 0;\"><a onclick=popup('service_stops.php?show=$value[0]&who=$value[1]&rolling=$rolling&date=$date_array[$showweeks]')><font size=2 color=blue onMouseOver=this.style.color='#FF9900';this.style.cursor='hand' onMouseOut=this.style.color='blue'>&nbsp;Route $displayname&nbsp;&nbsp;</font></a></td><td align=right style=\"border-style: solid;border-color: $colors[$mycount];border-width: 1px 0 1px 0;\"><font size=2 color=#777777>$showcurrent</font></td><td align=right style=\"border-style: solid;border-color: $colors[$mycount];border-width: 1px 1px 1px 0;\"><font size=2>&nbsp;$showchange&nbsp;</font></td><td style=\"width: 2px;\"></td>";
					$data.="<td bgcolor=$colors[$mycount] style=\"width: 14px;border-style: solid;border-color: $colors[$mycount];border-width: 1px 0 1px 1px;\"><font size=2>&nbsp;</font></td><td style=\"border-style: solid;border-color: $colors[$mycount];border-width: 1px 0 1px 0;\"><a onclick=popup('service_stops.php?show=$value[0]&who=$value[1]&rolling=$rolling&date=$date_array[$showweeks]')><font size=2 color=blue onMouseOver=this.style.color='#FF9900';this.style.cursor='hand' onMouseOut=this.style.color='blue'>&nbsp;Route $displayname&nbsp;&nbsp;</font></a></td><td align=right style=\"border-style: solid;border-color: $colors[$mycount];border-width: 1px 0 1px 0;\"><font size=2 color=#777777>$showcurrent</font></td><td align=right style=\"border-style: solid;border-color: $colors[$mycount];border-width: 1px 1px 1px 0;\"><font size=2>&nbsp;$showchange&nbsp;</font></td><td style=\"width: 2px;\"></td>";
					$mycount++;
					$mycount2++;
				}
				elseif($value[0]=="c"){
					$query2="SELECT sales_name FROM vend_salesman WHERE salesid = '$value[1]'";
					$result2 = Treat_DB_ProxyOld::query($query2);
					
					$displayname = @mysql_result( $result2, 0, 'sales_name' );
				
					echo "<td bgcolor=$colors[$mycount] style=\"width: 14px;\"><font size=2>&nbsp;</font></td><td><font size=2>&nbsp;$displayname&nbsp;($showchange%)&nbsp;</font></td><td></td><td></td><td style=\"width: 2px;\"></td>";
					$data.="<td bgcolor=$colors[$mycount] style=\"width: 14px;\"><font size=2>&nbsp;</font></td><td><font size=2>&nbsp;$displayname&nbsp;($showchange%)&nbsp;</font></td><td></td><td></td><td style=\"width: 2px;\"></td>";
					$mycount++;
					$mycount2++;
				}
				
				if($mycount2==4){echo "</tr><tr height=2><td colspan=14></td></tr><tr>"; $data.="</tr><tr height=2><td colspan=14></td></tr><tr>"; $mycount2=1;}
			}
		}
		echo "</tr></table>";
		$data.="</tr></table>";
		if($showbdg==1){
			if($showroute==0){echo "<font size=2>*The number of routes the budget is divided by is figured by the number of active routes in the last week of last year.</font>";}
			else{echo "<font size=2>*The number of routes the budget is divided by is figured by active routes for the particular week.</font>";}
		}
		
		echo "</td></tr></table>";
		$data.="</td></tr></table></body></html>";
		
		$myFile2 = DIR_EXPORTS . "/graph$loginid-2.html";
		$fh = fopen($myFile2, 'w');
		fwrite($fh, $data);
		fclose($fh);  
	}
}
//mysql_close();
?>
</body>