<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
if ( !defined('DIR_PAYROLL_FILES') )
{
	define('DIR_PAYROLL_FILES', realpath(DOC_ROOT.'/../data/csv'));
}
require_once(DOC_ROOT.'/bootstrap.php');

$style = "text-decoration:none";

$today = date('Y-m-d');

$user = isset($_POST['username'])?$_POST['username']:'';
$pass = isset($_POST['password'])?$_POST['password']:'';
$companyid = isset($_POST['companyid'])?$_POST['companyid']:'';
$imgfile = $_FILES['uploadedfile'];
$imgfile_size = isset($_POST['25000']) ? $_POST['25000'] : '';
$date1 = isset($_POST['date1'])?$_POST['date1']:'';

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num = mysql_numrows($result);

if ($num != 1)
{
	echo "<center><h3>Failed</h3></center>";
}

else
{
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	$keepgoing = 0;
	$uploadfile = DIR_PAYROLL_FILES."/payrollterm.csv";
	//$uploadfile = $uploaddir . basename($_FILES['uploadedfile']);

	$filetype = explode(".", $_FILES['uploadedfile']['name']);
	$extension = end($filetype);

	if($extension != 'csv')
	{
		echo "<p>{$extension} NOT A CSV FILE ({$_FILES['uploadedfile']['name']})</p>";
	}
	else if (move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $uploadfile))
	{
		echo "<p>FILE UPLOADED SUCCESSFULLY</p>";
		$keepgoing = 1;
	}
	else
	{
		echo "<p>FILE FAILED TO UPLOAD</p>";
	}


	if ($keepgoing == 1)
	{
		$today = date("Y-m-d");

		$myFile = DIR_PAYROLL_FILES . "/payrollterm.csv";
		$handle = fopen($myFile, 'r');

		echo "<table>";

		$counter = 0;
		$go = 0;

		$checkDateBefore = mktime(0, 0, 0, 1, 1, 2000);
		$checkDateAfter = mktime(0, 0, 0, 1, 1, 2099);

		while ( ($data = fgetcsv($handle)) )
		{
			if (strcasecmp($data[0], 'ee #') === 0 || strcasecmp($data[0], 'employee number') === 0)
			{
				$go = 1;
			}

			if ($data[0] > 0 && $go == 1)
			{
				$date = strtotime($data[5]);
				if ($date < $checkDateBefore || date('Y', $date) > 2099)
				{
					$date = $today;
				}

				$query = "UPDATE login SET is_deleted = '1', del_date = '$date' WHERE empl_no LIKE '$data[0]' AND companyid = '$companyid'";
				$saved = Treat_DB::singleton()->exec($query);

				if ( $saved )
				{
					$counter++;
				}

				$savedDisplay = $saved ? '' : 'Not ';
				echo "<tr><td>{$data[0]}</td><td>{$data[1]}</td><td>{$data[2]}</td><td>{$data[5]}</td><td><strong>{$savedDisplay}Saved</strong></td></tr>";
			}
		}
		echo "</table>";

		fclose($handle);

		echo "<h3>$counter employees termed.</h3>";

	}

	echo "<center><a href=businesstrack.php><font color=blue>Return to Main Page</font></a></center>";

}
?>