<head>
<STYLE>
#loading {
 	width: 200px;
 	height: 48px;
 	background-color: ;
 	position: absolute;
 	left: 50%;
 	top: 50%;
 	margin-top: -50px;
 	margin-left: -100px;
 	text-align: center;
}
</STYLE>

<script type="text/javascript" src="preLoadingMessage.js"></script>

</head>

<?php

if ( !defined('DOC_ROOT'))
{
	define('DOC_ROOT', dirname(dirname(__FILE__)));
}
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');

$style = "text-decoration:none";

function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}

function nextday($nextd,$day_format=''){ //Function returns next day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monn = substr($nextd, 5, 2);
   $dayn = substr($nextd, 8, 2);
   $yearn = substr($nextd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monn, $dayn+1, $yearn));
   return $tempdate;
}

function prevday($prevd,$day_format=''){ //Function returns previous day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monp = substr($prevd, 5, 2);
   $dayp = substr($prevd, 8, 2);
   $yearp = substr($prevd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monp, $dayp-1, $yearp));
   return $tempdate;
}

$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";

/*$user = isset($_COOKIE["usercook"])?$_COOKIE["usercook"]:'';
$pass = isset($_COOKIE["passcook"])?$_COOKIE["passcook"]:'';
$apaccountid = isset($_GET['apaccountid'])?$_GET['apaccountid']:'';
$date = isset($_GET['date'])?$_GET['date']:'';
$type = isset($_GET['type'])?$_GET['type']:'';
$businessid = isset($_GET['bid'])?$_GET['bid']:'';
$companyid = isset($_GET['cid'])?$_GET['cid']:'';*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook','');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook','');
$apaccountid = \EE\Controller\Base::getGetVariable('apaccountid','');
$date = \EE\Controller\Base::getGetVariable('date','');
$type = \EE\Controller\Base::getGetVariable('type','');
$businessid = \EE\Controller\Base::getGetVariable('bid','');
$companyid = \EE\Controller\Base::getGetVariable('cid','');

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=Treat_DB_ProxyOld::mysql_numrows($result);

if ($num != 1)
{
    echo "<center><h3>Failed</h3></center>";
}

else
{
    $securityid=@Treat_DB_ProxyOld::mysql_result($result,0,"security");

    $query = "SELECT * FROM security WHERE securityid = '$securityid'";
    $result = Treat_DB_ProxyOld::query($query);

    $sec_bus_payable=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_payable");

    $query = "SELECT * FROM company WHERE companyid = '$companyid'";
    $result = Treat_DB_ProxyOld::query($query);

    $week_end=@Treat_DB_ProxyOld::mysql_result($result,0,"week_end");

    if ($type==0||$type==4){
       $query = "SELECT acct_name,accountnum FROM acct_payable WHERE acct_payableid = '$apaccountid'";
       $result = Treat_DB_ProxyOld::query($query);

       $acct_name=@Treat_DB_ProxyOld::mysql_result($result,0,"acct_name");
       $accountnum=@Treat_DB_ProxyOld::mysql_result($result,0,"accountnum");

       $query = "SELECT amount FROM inventor2 WHERE acct_payableid = '$apaccountid' AND date < '$date' ORDER BY date DESC LIMIT 0,1";
       $result = Treat_DB_ProxyOld::query($query);

       $start=@Treat_DB_ProxyOld::mysql_result($result,0,"amount");

       $query = "SELECT amount FROM inventor2 WHERE acct_payableid = '$apaccountid' AND date = '$date'";
       $result = Treat_DB_ProxyOld::query($query);

       $end=@Treat_DB_ProxyOld::mysql_result($result,0,"amount");

       echo "<center><table><tr><td><b>$acct_name $accountnum, Week ending $date</b></td></tr></table></center><p>";

       $show_start=number_format($start,2);
       echo "<center><table width=100% bgcolor=#CCCCFF style=\"border:2px solid #E8E7E7;\"><tr><td>Beginning Inventory</td><td align=right><b>$show_start</td></tr></table></center><p>";

       echo "<center><table width=100% style=\"border:2px solid #E8E7E7;\" cellspacing=0>";

       $counter=0;
       $lastmonth=0;

       $prevmonthnum=substr($date,5,2);
       $date1=$date;

       $total=0;

       while(((dayofweek($date)!=$week_end&&substr($date,5,2)==$prevmonthnum)||(dayofweek($date)==$week_end&&$date1==$date))&&$counter<=6){

          $query = "SELECT apinvoice.apinvoiceid,apinvoice.invoicenum,apinvoice.vendor,apinvoice.date,apinvoice.transfer,apinvoice.pc_type,apinvoice.businessid,apinvoice.notes,apinvoicedetail.amount,apinvoicedetail.item FROM apinvoice,apinvoicedetail WHERE (apinvoice.date = '$date' AND apinvoice.businessid = '$businessid' AND apinvoice.apinvoiceid = apinvoicedetail.apinvoiceid AND apinvoicedetail.apaccountid = '$apaccountid' AND apinvoice.transfer < '3') OR (apinvoice.date = '$date' AND apinvoice.transfer = '1' AND apinvoice.vendor = '$businessid' AND apinvoice.apinvoiceid = apinvoicedetail.apinvoiceid AND apinvoicedetail.tfr_acct = '$apaccountid') ORDER BY apinvoicedetail.apinvoiceid,apinvoice.date";
          $result =Treat_DB_ProxyOld::query($query);
          $num=Treat_DB_ProxyOld::mysql_numrows($result);

          $num--;
          $lastinvoicenum=-1;
          while($num>=0){
             $apinvoiceid=@Treat_DB_ProxyOld::mysql_result($result,$num,"apinvoiceid");
             $invoicenum=@Treat_DB_ProxyOld::mysql_result($result,$num,"invoicenum");
             $bid=@Treat_DB_ProxyOld::mysql_result($result,$num,"businessid");
             $vendor=@Treat_DB_ProxyOld::mysql_result($result,$num,"vendor");
             $inv_date=@Treat_DB_ProxyOld::mysql_result($result,$num,"date");
             $transfer=@Treat_DB_ProxyOld::mysql_result($result,$num,"transfer");
             $pc_type=@Treat_DB_ProxyOld::mysql_result($result,$num,"pc_type");
             $notes=@Treat_DB_ProxyOld::mysql_result($result,$num,"notes");
             $amount=@Treat_DB_ProxyOld::mysql_result($result,$num,"amount");
             $item=@Treat_DB_ProxyOld::mysql_result($result,$num,"item");

             if($transfer==0){
                $query2 = "SELECT name FROM vendors WHERE vendorid = '$vendor'";
                $result2 = Treat_DB_ProxyOld::query($query2);

                $vendor_name=@Treat_DB_ProxyOld::mysql_result($result2,0,"name");
                $showinv="Invoice";
             }
             elseif($transfer==1&&$vendor==$businessid){
                $query2 = "SELECT businessname FROM business WHERE businessid = '$bid'";
                $result2 = Treat_DB_ProxyOld::query($query2);

                $vendor_name=@Treat_DB_ProxyOld::mysql_result($result2,0,"businessname");
                $showinv="Transfer";
                $amount=$amount*-1;
             }
             elseif($transfer==1&&$vendor!=$businessid){
                $query2 = "SELECT businessname FROM business WHERE businessid = '$vendor'";
                $result2 = Treat_DB_ProxyOld::query($query2);

                $vendor_name=@Treat_DB_ProxyOld::mysql_result($result2,0,"businessname");
                $showinv="Transfer";
             }
             elseif($transfer==2){
                $query2 = "SELECT * FROM purchase_card WHERE businessid = '$businessid' AND loginid = '$vendor'";
                $result2 = Treat_DB_ProxyOld::query($query2);

                $pc_type=@Treat_DB_ProxyOld::mysql_result($result2,0,"type");
                $loginid=@Treat_DB_ProxyOld::mysql_result($result2,0,"loginid");

                if($pc_type==0){
                   $query4 = "SELECT firstname,lastname FROM login WHERE loginid = '$loginid'";
                   $result4 = Treat_DB_ProxyOld::query($query4);
                }
                elseif($pc_type==1){
                   $query4 = "SELECT firstname,lastname FROM login_route WHERE login_routeid = '$loginid'";
                   $result4 = Treat_DB_ProxyOld::query($query4);
                }
                $firstname=@Treat_DB_ProxyOld::mysql_result($result4,0,"firstname");
                $lastname=@Treat_DB_ProxyOld::mysql_result($result4,0,"lastname");

                $vendor_name="$firstname $lastname";

                $showinv="Purchase Card";
             }

             if ($lastinvoicenum!=$invoicenum){
                if($sec_bus_payable>0){$link="<a href=apinvoice.php?bid=$businessid&cid=$companyid&apinvoiceid=$apinvoiceid style=$style target='_blank'><font color=blue>$invoicenum</font></a>";}
                else{$link="$invoicenum";}

                echo "<tr bgcolor=#E8E7E7><td align=right><i><font size=2>$showinv:</td><td><font size=2>$link</td><td colspan=2><font size=2>($inv_date) $vendor_name</td></tr>";
                if($notes!=""){echo "<tr><td colspan=3><font size=2 color=#666666>$notes</font></td><td></td></tr>";}
             }
             $total+=$amount;
             $amount=number_format($amount,2);
             echo "<tr><td></td><td>$item</td><td align=right>$acct_name</td><td align=right>$amount</td></tr>";

             $lastinvoicenum=$invoicenum;
             $num--;
          }

          $counter++;
          $lastmonth=substr($date,5,2);
          $date=prevday($date);
       }
       
       $show_total=number_format($total,2);
       echo "<tr bgcolor=#CCCCFF><td colspan=2>Purchases<td align=right></td><td align=right><b>$show_total</td></tr>";
       echo "</table></center><p>";

       $show_end=number_format($end,2);
       echo "<center><table width=100% bgcolor=#CCCCFF style=\"border:2px solid #E8E7E7;\"><tr><td>Ending Inventory</td><td align=right><b>$show_end</td></tr></table></center><p>";

       if($start!=0&&$end!=0){$total=$start+$total-$end;}
       $show_total=number_format($total,2);
       echo "<center><table width=100% bgcolor=yellow style=\"border:2px solid #E8E7E7;\"><tr><td><b>TOTAL</b></td><td align=right><b>$show_total</td></tr></table></center>";
    }

}
//mysql_close();
?>