<?php

function money($diff){
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+4);
        $diff=round($diff, 2);
        if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
        elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
        $diff = substr($diff, 0, $dot+3);
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      if ($month == "10"){$month="11";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='32')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='08' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
   {
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      $day=$day-31;
      if ($day<10){$day="0$day";}
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";}
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";}
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";}
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=32)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";}
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

define('DOC_ROOT', dirname(dirname(__FILE__)));
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');
$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");
$creditamount="creditamount";
$creditidnty="creditidnty";
$creditdate="creditdate";
$debitamount="debitamount";
$debitidnty="debitidnty";
$debitdate="debitdate";
$quatertotal=0;
$monthtotal=0;
$total=0;
$counter=0;
$findme='.';
$double='.00';
$single='0';
$double2='00';

/*$user = isset($_COOKIE["usercook"])?$_COOKIE["usercook"]:'';
$pass = isset($_COOKIE["passcook"])?$_COOKIE["passcook"]:'';
$view = isset($_COOKIE["viewcook"])?$_COOKIE["viewcook"]:'';
$sort_ap = isset($_COOKIE["sort_ap"])?$_COOKIE["sort_ap"]:'';
$subtotal = isset($_COOKIE["subtotal"])?$_COOKIE["subtotal"]:'';
$default_year = isset($_COOKIE["po_year"])?$_COOKIE["po_year"]:'';
$default_month = isset($_COOKIE["po_month"])?$_COOKIE["po_month"]:'';
$default_month_start = isset($_COOKIE["po_month_start"])?$_COOKIE["po_month_start"]:'';
$businessid = isset($_GET["bid"])?$_GET["bid"]:'';
$companyid = isset($_GET["cid"])?$_GET["cid"]:'';
$interco = isset($_GET["interco"])?$_GET["interco"]:'';
$showcat = isset($_GET["showcat"])?$_GET["showcat"]:'';
$ytd = isset($_GET["ytd"])?$_GET["ytd"]:'';*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook','');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook','');
$view = \EE\Controller\Base::getSessionCookieVariable('viewcook','');
$sort_ap = \EE\Controller\Base::getSessionCookieVariable('sort_ap','');
$subtotal = \EE\Controller\Base::getSessionCookieVariable('subtotal','');
$default_year = \EE\Controller\Base::getSessionCookieVariable('po_year','');
$default_month = \EE\Controller\Base::getSessionCookieVariable('po_month','');
$default_month_start = \EE\Controller\Base::getSessionCookieVariable('po_month_start','');
$businessid = \EE\Controller\Base::getGetVariable('bid','');
$companyid = \EE\Controller\Base::getGetVariable('cid','');
$interco = \EE\Controller\Base::getGetVariable('interco','');
$showcat = \EE\Controller\Base::getGetVariable('showcat','');
$ytd = \EE\Controller\Base::getGetVariable('ytd','');

/*$po_date1 = isset($_COOKIE["po_date1"])?$_COOKIE["po_date1"]:'';
$po_date2 = isset($_COOKIE["po_date2"])?$_COOKIE["po_date2"]:'';
$po_status = isset($_COOKIE["po_status"])?$_COOKIE["po_status"]:'';*/

$po_date1 = \EE\Controller\Base::getSessionCookieVariable('po_date1','');
$po_date2 = \EE\Controller\Base::getSessionCookieVariable('po_date2','');
$po_status = \EE\Controller\Base::getSessionCookieVariable('po_status','');

if($showcat==""){
	/*$showcat = isset($_COOKIE["po_showcat"])?$_COOKIE["po_showcat"]:'';
	$ytd = isset($_COOKIE["po_ytd"])?$_COOKIE["po_ytd"]:'';*/
	
	$showcat = \EE\Controller\Base::getSessionCookieVariable('po_showcat','');
	$ytd = \EE\Controller\Base::getSessionCookieVariable('po_ytd','');
}

if ($businessid==""&&$companyid==""){
   /*$businessid = isset($_POST["businessid"])?$_POST["businessid"]:'';
   $companyid = isset($_POST["companyid"])?$_POST["companyid"]:'';
   $view = isset($_POST["view"])?$_POST["view"]:'';
   $default_year = isset($_POST["year"])?$_POST["year"]:'';
   $default_month = isset($_POST["month"])?$_POST["month"]:'';
   $default_month_start = isset($_POST["month_start"])?$_POST["month_start"]:'';
   $findinv = isset($_POST["findinv"])?$_POST["findinv"]:'';
   $sort_ap = isset($_POST["sort_ap"])?$_POST["sort_ap"]:'';
   $subtotal = isset($_POST["subtotal"])?$_POST["subtotal"]:'';*/

	$businessid = \EE\Controller\Base::getPostVariable('businessid','');
	$companyid = \EE\Controller\Base::getPostVariable('companyid','');
	$view = \EE\Controller\Base::getPostVariable('view','');
	$default_year = \EE\Controller\Base::getPostVariable('year','');
	$default_month = \EE\Controller\Base::getPostVariable('month','');
	$default_month_start = \EE\Controller\Base::getPostVariable('month_start','');
	$findinv = \EE\Controller\Base::getPostVariable('findinv','');
	$sort_ap = \EE\Controller\Base::getPostVariable('sort_ap','');
	$subtotal = \EE\Controller\Base::getPostVariable('subtotal','');
	
   /*$po_date1 = isset($_POST["po_date1"])?$_POST["po_date1"]:'';
   $po_date2 = isset($_POST["po_date2"])?$_POST["po_date2"]:'';
   $po_status = isset($_POST["po_status"])?$_POST["po_status"]:'';*/
	
	$po_date1 = \EE\Controller\Base::getPostVariable('po_date1','');
	$po_date2 = \EE\Controller\Base::getPostVariable('po_date2','');
	$po_status = \EE\Controller\Base::getPostVariable('po_status','');

   setcookie("po_showcat",'');
   setcookie("po_ytd",'');
   $showcat="";
}

if ($sort_ap==""){$sort_ap="date";}

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=Treat_DB_ProxyOld::mysql_numrows($result);
//mysql_close();

$loginid=@Treat_DB_ProxyOld::mysql_result($result,0,"loginid");
$security_level=@Treat_DB_ProxyOld::mysql_result($result,0,"security_level");
$mysecurity=@Treat_DB_ProxyOld::mysql_result($result,0,"security");
$bid=@Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
$cid=@Treat_DB_ProxyOld::mysql_result($result,0,"companyid");
$bid2=@Treat_DB_ProxyOld::mysql_result($result,0,"busid2");
$bid3=@Treat_DB_ProxyOld::mysql_result($result,0,"busid3");
$bid4=@Treat_DB_ProxyOld::mysql_result($result,0,"busid4");
$bid5=@Treat_DB_ProxyOld::mysql_result($result,0,"busid5");
$bid6=@Treat_DB_ProxyOld::mysql_result($result,0,"busid6");
$bid7=@Treat_DB_ProxyOld::mysql_result($result,0,"busid7");
$bid8=@Treat_DB_ProxyOld::mysql_result($result,0,"busid8");
$bid9=@Treat_DB_ProxyOld::mysql_result($result,0,"busid9");
$bid10=@Treat_DB_ProxyOld::mysql_result($result,0,"busid10");

$pr1=@Treat_DB_ProxyOld::mysql_result($result,0,"payroll");
$pr2=@Treat_DB_ProxyOld::mysql_result($result,0,"pr2");
$pr3=@Treat_DB_ProxyOld::mysql_result($result,0,"pr3");
$pr4=@Treat_DB_ProxyOld::mysql_result($result,0,"pr4");
$pr5=@Treat_DB_ProxyOld::mysql_result($result,0,"pr5");
$pr6=@Treat_DB_ProxyOld::mysql_result($result,0,"pr6");
$pr7=@Treat_DB_ProxyOld::mysql_result($result,0,"pr7");
$pr8=@Treat_DB_ProxyOld::mysql_result($result,0,"pr8");
$pr9=@Treat_DB_ProxyOld::mysql_result($result,0,"pr9");
$pr10=@Treat_DB_ProxyOld::mysql_result($result,0,"pr10");

if ($num != 1 || ($security_level==1 && ($bid != $businessid || $cid != $companyid)) || $user == "" || $pass == "")
{
    echo "<center><h3>Failed</h3>Use your browser's back button to try again.</center>";
}

else
{
    if($po_date1 == ""){
        $po_date2 = date("Y-m-d");
        $po_date1 = date("Y-m-01");
        $po_status = -1;
    }

    setcookie("po_date1",$po_date1);
    setcookie("po_date2",$po_date2);
    setcookie("po_status",$po_status);

    setcookie("po_year",$default_year);
    setcookie("po_month",$default_month);
	setcookie("po_month_start",$default_month_start);
	setcookie("po_showcat",$showcat);
	setcookie("po_ytd",$ytd);

	//mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM company WHERE companyid = '$companyid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $companyname=@Treat_DB_ProxyOld::mysql_result($result,0,"companyname");
	$com_logo=@Treat_DB_ProxyOld::mysql_result($result,0,"logo");

	if($com_logo == ""){$com_logo = "talogo.gif";}

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM security WHERE securityid = '$mysecurity'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $sec_bus_sales=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_sales");
    $sec_bus_invoice=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_invoice");
    $sec_bus_order=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_order");
    $sec_bus_payable=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_payable");
    $sec_bus_payable2=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_payable2");
    $sec_bus_payable3=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_payable3");
    $sec_bus_inventor1=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor1");
    $sec_bus_control=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_control");
    $sec_bus_menu=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_menu");
    $sec_bus_order_setup=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_order_setup");
    $sec_bus_request=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_request");
    $sec_route_collection=@Treat_DB_ProxyOld::mysql_result($result,0,"route_collection");
    $sec_route_labor=@Treat_DB_ProxyOld::mysql_result($result,0,"route_labor");
    $sec_route_detail=@Treat_DB_ProxyOld::mysql_result($result,0,"route_detail");
    $sec_bus_labor=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_labor");
	$sec_bus_budget=@Treat_DB_ProxyOld::mysql_result($result,0,"bus_budget");

	if($sec_bus_payable3<1){
		$location="businesstrack.php";
        header('Location: ./' . $location);
	}
?>

<html>
<head>

<script src="sorttable.js"></script>

<SCRIPT LANGUAGE="JavaScript" SRC="CalendarPopup.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">document.write(getCalendarStyles());</SCRIPT>

<script language="JavaScript"
   type="text/JavaScript">
function changePage(newLoc)
 {
   nextPage = newLoc.options[newLoc.selectedIndex].value

   if (nextPage != "")
   {
      document.location.href = nextPage
   }
 }
</script>

<STYLE>
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation
			{
			background-color:#6677DD;
			text-align:center;
			vertical-align:center;
			text-decoration:none;
			color:#FFFFFF;
			font-weight:bold;
			}
	.TESTcpDayColumnHeader,
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation,
	.TESTcpCurrentMonthDate,
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDate,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDate,
	.TESTcpCurrentDateDisabled,
	.TESTcpTodayText,
	.TESTcpTodayTextDisabled,
	.TESTcpText
			{
			font-family:arial;
			font-size:8pt;
			}
	TD.TESTcpDayColumnHeader
			{
			text-align:right;
			border:solid thin #6677DD;
			border-width:0 0 1 0;
			}
	.TESTcpCurrentMonthDate,
	.TESTcpOtherMonthDate,
	.TESTcpCurrentDate
			{
			text-align:right;
			text-decoration:none;
			}
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDateDisabled
			{
			color:#D0D0D0;
			text-align:right;
			text-decoration:line-through;
			}
	.TESTcpCurrentMonthDate
			{
			color:#6677DD;
			font-weight:bold;
			}
	.TESTcpCurrentDate
			{
			color: #FFFFFF;
			font-weight:bold;
			}
	.TESTcpOtherMonthDate
			{
			color:#808080;
			}
	TD.TESTcpCurrentDate
			{
			color:#FFFFFF;
			background-color: #6677DD;
			border-width:1;
			border:solid thin #000000;
			}
	TD.TESTcpCurrentDateDisabled
			{
			border-width:1;
			border:solid thin #FFAAAA;
			}
	TD.TESTcpTodayText,
	TD.TESTcpTodayTextDisabled
			{
			border:solid thin #6677DD;
			border-width:1 0 0 0;
			}
	A.TESTcpTodayText,
	SPAN.TESTcpTodayTextDisabled
			{
			height:20px;
			}
	A.TESTcpTodayText
			{
			color:#6677DD;
			font-weight:bold;
			}
	SPAN.TESTcpTodayTextDisabled
			{
			color:#D0D0D0;
			}
	.TESTcpBorder
			{
			border:solid thin #6677DD;
			}
</STYLE>

<SCRIPT LANGUAGE="JavaScript">
<!-- Web Site:  http://dynamicdrive.com -->

<!-- This script and many more are available free online at -->
<!-- The JavaScript Source!! http://javascript.internet.com -->

<!-- Begin
function disableForm(theform) {
if (document.all || document.getElementById) {
for (i = 0; i < theform.length; i++) {
var tempobj = theform.elements[i];
if (tempobj.type.toLowerCase() == "submit" || tempobj.type.toLowerCase() == "reset")
tempobj.disabled = true;
}

}
else {
alert("The form has been submitted.  But, since you're not using IE 4+ or NS 6, the submit button was not disabled on form submission.");
return false;
   }
}
//  End -->
</script>

<SCRIPT LANGUAGE=javascript><!--
function confirmcreate(){return confirm('ARE YOU SURE YOU WANT TO CREATE A TRANSFER?');}
// --></SCRIPT>

<SCRIPT LANGUAGE=javascript><!--
function confirmcreate2(){return confirm('ARE YOU SURE YOU WANT TO CREATE A PURCHASE CARD RECEIPT?');}
// --></SCRIPT>

<script language="JavaScript"
   type="text/JavaScript">
function changePageb(newLoc)
 {
   nextPage = newLoc.value

   if (nextPage != "")
   {
      document.location.href = nextPage
   }
 }
</script>

</head>

<?
    echo "<body>";
    echo "<div style=border:50px solid red;padding:10px;>";
    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%><tr><td colspan=2>";
    echo "<a href=businesstrack.php><img src=logo.jpg border=0 height=43 width=205></a><p></td></tr>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM business WHERE companyid = '$companyid' AND businessid = '$businessid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $businessname=@Treat_DB_ProxyOld::mysql_result($result,0,"businessname");
    $businessid=@Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
    $street=@Treat_DB_ProxyOld::mysql_result($result,0,"street");
    $city=@Treat_DB_ProxyOld::mysql_result($result,0,"city");
    $state=@Treat_DB_ProxyOld::mysql_result($result,0,"state");
    $zip=@Treat_DB_ProxyOld::mysql_result($result,0,"zip");
    $phone=@Treat_DB_ProxyOld::mysql_result($result,0,"phone");
    $districtid=@Treat_DB_ProxyOld::mysql_result($result,0,"districtid");
    $bus_unit=@Treat_DB_ProxyOld::mysql_result($result,0,"unit");

    //echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";
    echo "<tr bgcolor=#CCCCFF><td width=1%><img src=logo/$com_logo></td><td><font size=4><b>$businessname #$bus_unit</b></font><br>$street<br>$city, $state $zip<br>$phone</td>";
    echo "<td align=right valign=top><br>";
    echo "<FORM ACTION='editbus.php' method=post name='store'>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<input type=hidden name=businessid value=$businessid>";
    echo "<input type=hidden name=companyid value=$companyid>";
    echo "<INPUT TYPE=submit VALUE=' Store Setup '> ";
    echo "</form></td></tr>";
    //echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";

    echo "<tr><td colspan=3><font color=#999999>";
    if ($sec_bus_sales>0){echo "<a href=busdetail.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Sales</font></a>";}
    if ($sec_bus_invoice>0){echo " | <a href=businvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Invoicing</font></a>";}
    if ($sec_bus_order>0){echo " | <a href=buscatertrack.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Orders</font></a>";}
    if ($sec_bus_payable>0){echo " | <a href=busapinvoice.php?bid=$businessid&cid=$companyid><font color=red onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='red' style='text-decoration:none'>Payables</font></a>";}
    if ($sec_bus_inventor1>0){echo " | <a href=businventor.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Inventory</font></a>";}
    if ($sec_bus_control>0){echo " | <a href=buscontrol.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Control Sheet</font></a>";}
    if ($sec_bus_menu>0){echo " | <a href=buscatermenu.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Menus</font></a>";}
    if ($sec_bus_order_setup>0){echo " | <a href=buscaterroom.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Order Setup</font></a>";}
    if ($sec_bus_request>0){echo " | <a href=busrequest.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Requests</font></a>";}
    if ($sec_route_collection>0||$sec_route_labor>0||$sec_route_detail>0){echo " | <a href=busroute_collect.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Route Collections</font></a>";}
    if ($sec_bus_labor>0){echo " | <a href=buslabor.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Labor</font></a>";}
    if ($sec_bus_budget>0||($businessid == $bid && $pr1 == 1)||($businessid == $bid2 && $pr2 == 1)||($businessid == $bid3 && $pr3 == 1)||($businessid == $bid4 && $pr4 == 1)||($businessid == $bid5 && $pr5 == 1)||($businessid == $bid6 && $pr6 == 1)||($businessid == $bid7 && $pr7 == 1)||($businessid == $bid8 && $pr8 == 1)||($businessid == $bid9 && $pr9 == 1)||($businessid == $bid10 && $pr10 == 1)){echo " | <a href=busbudget.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Budget</font></a>";}
	echo "</td></tr>";

    echo "</table></center><p>";

if ($security_level>0){
    echo "<p><center><table width=90%><tr><td width=50%><form action=busapinvoice.php method=post>";
    echo "<select name=gobus onChange='changePage(this.form.gobus)'>";

    $districtquery="";
    if ($security_level>2&&$security_level<7){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM login_district WHERE loginid = '$loginid'";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();

       $num--;
       while($num>=0){
          $districtid=@Treat_DB_ProxyOld::mysql_result($result,$num,"districtid");
          if($districtquery==""){$districtquery="districtid = '$districtid'";}
          else{$districtquery="$districtquery OR districtid = '$districtid'";}
          $num--;
       }
    }

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    if($security_level>6){$query = "SELECT * FROM business WHERE companyid = '$companyid' ORDER BY businessname DESC";}
    elseif($security_level==2||$security_level==1){$query = "SELECT * FROM business WHERE companyid = '$companyid' AND (businessid = '$bid' OR businessid = '$bid2' OR businessid = '$bid3' OR businessid = '$bid4' OR businessid = '$bid5' OR businessid = '$bid6' OR businessid = '$bid7' OR businessid = '$bid8' OR businessid = '$bid9' OR businessid = '$bid10') ORDER BY businessname DESC";}
    elseif($security_level>2&&$security_level<7){$query = "SELECT * FROM business WHERE companyid = '$companyid' AND ($districtquery) ORDER BY businessname DESC";}
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    $num--;
    while($num>=0){
       $businessname=@Treat_DB_ProxyOld::mysql_result($result,$num,"businessname");
       $busid=@Treat_DB_ProxyOld::mysql_result($result,$num,"businessid");

       if ($busid==$businessid){$show="SELECTED";}
       else{$show="";}

       echo "<option value=busapinvoice3.php?bid=$busid&cid=$companyid $show>$businessname</option>";
       $num--;
    }

    echo "</select></td><td></form></td><td width=50% align=right></td></tr></table></center><p>";
}
    $security_level=$sec_bus_payable3;

    echo "<center><table width=90% border=0 cellspacing=0 cellpadding=0><tr valign=bottom><td colspan=4><FORM ACTION='busapinvoice3.php' method='post'>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<input type=hidden name=businessid value=$businessid>";
    echo "<input type=hidden name=companyid value=$companyid>";
    echo "<table width=100%><tr><td>View ";

    echo "<SCRIPT LANGUAGE='JavaScript' ID='js18'> var cal18 = new CalendarPopup('testdiv1');cal18.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=po_date1 VALUE='$po_date1' SIZE=8> <A HREF=\"javascript:void(0);\" onClick=cal18.select(document.forms[2].po_date1,'anchor18','yyyy-MM-dd'); return false; TITLE=cal18.select(document.forms[2].po_date1,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor18' ID='anchor18'><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A> to ";
    echo "<SCRIPT LANGUAGE='JavaScript' ID='js19'> var cal19 = new CalendarPopup('testdiv1');cal19.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=po_date2 VALUE='$po_date2' SIZE=8> <A HREF=\"javascript:void(0);\" onClick=cal19.select(document.forms[2].po_date2,'anchor19','yyyy-MM-dd'); return false; TITLE=cal19.select(document.forms[2].po_date2,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor19' ID='anchor19'><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A> ";

    echo " <select name=po_status>";
        if($po_status == -1){$sel="SELECTED";}
        else{$sel="";}
        echo "<option value=-1 $sel>All</option>";
        if($po_status == 0){$sel="SELECTED";}
        else{$sel="";}
        echo "<option value=0 $sel>In Progress</option>";
        if($po_status == 2){$sel="SELECTED";}
        else{$sel="";}
        echo "<option value=2 $sel>Pending</option>";
        if($po_status == 1){$sel="SELECTED";}
        else{$sel="";}
        echo "<option value=1 $sel>Complete</option>";
        if($po_status == 5){$sel="SELECTED";}
        else{$sel="";}
        echo "<option value=5 $sel>&nbsp;&nbsp;&nbsp;&nbsp;Not Received</option>";
        if($po_status == 6){$sel="SELECTED";}
        else{$sel="";}
        echo "<option value=6 $sel>&nbsp;&nbsp;&nbsp;&nbsp;Received</option>";
        if($po_status == 3){$sel="SELECTED";}
        else{$sel="";}
        echo "<option value=3 $sel>Rejected</option>";
    echo "</select> ";

	$default_year=substr($po_date2,0,4);
	$default_month=substr($po_date2,5,2);
        $default_month_start=substr($po_date2,5,2);

/*
	echo "<select name=month_start>";
		for($counter=1;$counter<=12;$counter++){
			if($counter==1){$monthname="January";}
			elseif($counter==2){$monthname="February";}
			elseif($counter==3){$monthname="March";}
			elseif($counter==4){$monthname="April";}
			elseif($counter==5){$monthname="May";}
			elseif($counter==6){$monthname="June";}
			elseif($counter==7){$monthname="July";}
			elseif($counter==8){$monthname="August";}
			elseif($counter==9){$monthname="September";}
			elseif($counter==10){$monthname="October";}
			elseif($counter==11){$monthname="November";}
			elseif($counter==12){$monthname="December";}

			if($default_month_start==$counter){$sel="SELECTED";}
			else{$sel="";}

			echo "<option value=$counter $sel>$monthname</option>";
		}
	echo "</select> to ";
	echo "<select name=month>";
		for($counter=1;$counter<=12;$counter++){
			if($counter==1){$monthname="January";}
			elseif($counter==2){$monthname="February";}
			elseif($counter==3){$monthname="March";}
			elseif($counter==4){$monthname="April";}
			elseif($counter==5){$monthname="May";}
			elseif($counter==6){$monthname="June";}
			elseif($counter==7){$monthname="July";}
			elseif($counter==8){$monthname="August";}
			elseif($counter==9){$monthname="September";}
			elseif($counter==10){$monthname="October";}
			elseif($counter==11){$monthname="November";}
			elseif($counter==12){$monthname="December";}

			if($default_month==$counter){$sel="SELECTED";$thismonthname=$monthname;}
			else{$sel="";}

			echo "<option value=$counter $sel>$monthname</option>";
		}
	echo "</select> <select name=year>";
		$year=date("Y");
		$year++;
		for($counter=1;$counter<=6;$counter++){
			if($default_year==$year){$sel="SELECTED";}
			else{$sel="";}

			echo "<option value=$year $sel>$year</option>";
			$year--;
		}
	echo "</select>";
     *
     */
    echo " <INPUT TYPE=submit VALUE='GO'></FORM></td></tr></table></td></tr>";
    //echo "<tr bgcolor=black><td height=1 colspan=4></td></tr></table>";

    if($sec_bus_payable2>0||$sec_bus_payable3>0){
       echo "<p><center><table width=90% cellspacing=0 cellpadding=0>";
       echo "<tr bgcolor=#E8E7E7 valign=top><td width=1% bgcolor=#CCCCCC><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=15% bgcolor=#CCCCCC><center><a href=busapinvoice.php?cid=$companyid&bid=$businessid style=$style><font color=blue>Payables</a></center></td><td width=1% align=right bgcolor=#CCCCCC><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1% bgcolor=#CCCCCC><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=15% bgcolor=#CCCCCC><center><a href=busapinvoice2.php?cid=$companyid&bid=$businessid style=$style style=$style><font color=blue>Expense Reports</a></center></td><td width=1% align=right bgcolor=#CCCCCC><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1%><img src=leftcrn.jpg height=6 width=6 align=top></td><td width=15%><center><a href=busapinvoice3.php?cid=$companyid&bid=$businessid style=$style style=$style><font color=black>Purchase Orders</a></center></td><td width=1% align=right><img src=rightcrn.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td bgcolor=white width=60%></td></tr>";
       echo "<tr height=2><td colspan=4></td><td colspan=4></td><td colspan=3 bgcolor=#E8E7E7></td><td></td><td colspan=1></td></tr>";
       echo "</table>";
    }

    echo "<table width=90% cellspacing=0 cellpadding=0><tr bgcolor=#E8E7E7 height=18><td colspan=4>&nbsp;</td></tr>";
    echo "<tr bgcolor=black><td height=1 colspan=4></td></tr><tr bgcolor=#E8E7E7 valign=top><td colspan=4><br>";

	/////left column
	echo "<table width=100%><tr valign=top><td width=50%>";

	echo "<center><table width=99% cellspacing=0 cellspacing=0 style=\"border: 2px solid #CCCCCC;\" bgcolor=white>";
	echo "<tr bgcolor=#CCCCCC><th align=left><font size=2>PO#</font></th><th align=left><b><font size=2>Vendor</font></b></th><th align=left><b><font size=2>Date</font></b></th><th align=right><b><font size=2>Total</font></b></th></tr>";

	$date1="$default_year-$default_month_start-01";
	$date2="$default_year-$default_month-31";
	$date3="$default_year-$default_month-01";

	if($showcat>0){
		if($ytd==1){$startdate="$default_year-01-01"; $query1 = "SELECT DISTINCT apinvoice.apinvoiceid,apinvoice.invoicenum,apinvoice.date,apinvoice.vendor,apinvoice.total,apinvoice.posted FROM apinvoice,apinvoicedetail WHERE apinvoice.transfer = '4' AND apinvoice.businessid = '$businessid' AND apinvoice.posted = '1' AND apinvoice.apinvoiceid = apinvoicedetail.apinvoiceid AND apinvoicedetail.date >= '$startdate' AND apinvoicedetail.date <= '$date2' AND apinvoicedetail.categoryid = '$showcat' ORDER BY apinvoice.date";}
		else{$query1 = "SELECT DISTINCT apinvoice.apinvoiceid,apinvoice.invoicenum,apinvoice.date,apinvoice.vendor,apinvoice.total,apinvoice.posted FROM apinvoice,apinvoicedetail WHERE apinvoice.transfer = '4' AND apinvoice.businessid = '$businessid' AND apinvoice.posted = '1' AND apinvoice.apinvoiceid = apinvoicedetail.apinvoiceid AND apinvoicedetail.date >= '$date3' AND apinvoicedetail.date <= '$date2' AND apinvoicedetail.categoryid = '$showcat' ORDER BY apinvoice.date";}
		$result1 = Treat_DB_ProxyOld::query($query1);
	}
	elseif($showcat<0){
		$newcat=$showcat*-1;
		if($ytd==1){$startdate="$default_year-01-01"; $query1 = "SELECT DISTINCT apinvoice.apinvoiceid,apinvoice.invoicenum,apinvoice.date,apinvoice.vendor,apinvoice.total,apinvoice.posted FROM apinvoice,apinvoicedetail WHERE apinvoice.transfer = '4' AND apinvoice.posted = '1' AND apinvoice.apinvoiceid = apinvoicedetail.apinvoiceid AND apinvoicedetail.date >= '$startdate' AND apinvoicedetail.date <= '$date2' AND apinvoicedetail.categoryid = '$newcat' ORDER BY apinvoice.date";}
		else{$query1 = "SELECT DISTINCT apinvoice.apinvoiceid,apinvoice.invoicenum,apinvoice.date,apinvoice.vendor,apinvoice.total,apinvoice.posted FROM apinvoice,apinvoicedetail WHERE apinvoice.transfer = '4' AND apinvoice.posted = '1' AND apinvoice.apinvoiceid = apinvoicedetail.apinvoiceid AND apinvoicedetail.date >= '$date3' AND apinvoicedetail.date <= '$date2' AND apinvoicedetail.categoryid = '$newcat' ORDER BY apinvoice.date";}
		$result1 = Treat_DB_ProxyOld::query($query1);
	}
	else{
                if($po_status == 5){$myquery = "AND posted = '1' AND pc_type = '2'";}
                elseif($po_status == 6){$myquery = "AND posted = '1' AND pc_type = '1'";}
                elseif($po_status != -1){$myquery = "AND posted = '$po_status'";}

		$query1 = "SELECT * FROM apinvoice WHERE transfer = '4' AND date >= '$po_date1' AND date <= '$po_date2' AND businessid = '$businessid' $myquery ORDER BY date";
		$result1 = Treat_DB_ProxyOld::query($query1);
	}

	while($r=Treat_DB_ProxyOld::mysql_fetch_array($result1)){
		$apinvoiceid=$r["apinvoiceid"];
		$invoicenum=$r["invoicenum"];
		$date=$r["date"];
		$vendor=$r["vendor"];
		$total=$r["total"];
		$posted=$r["posted"];

		$query2 = "SELECT name FROM vendors WHERE vendorid = '$vendor'";
		$result2 = Treat_DB_ProxyOld::query($query2);

		$vendor=@Treat_DB_ProxyOld::mysql_result($result2,0,"name");

		if($posted==0){$showimg="<img src=po1.gif height=10 width=10 alt='In Progress' border=0>";}
		elseif($posted==2){$showimg="<img src=po2.gif height=10 width=10 alt='Pending' border=0>";}
		elseif($posted==1){$showimg="<img src=po3.gif height=10 width=10 alt='Complete' border=0>";}
		elseif($posted==3){$showimg="<img src=ex.gif height=10 width=10 alt='Rejected' border=0>";}

		$total=number_format($total,2);
		echo "<tr bgcolor=white onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor='white'><td>&nbsp;<a href=apinvoice.php?apinvoiceid=$apinvoiceid&goto=4&bid=$businessid&cid=$companyid style=$style>$showimg <font color=blue size=2>$invoicenum</font></a></td><td><font size=2>$vendor</font></td><td><font size=2>$date</font></td><td align=right><font size=2>$total</font></td></tr>";

		if($showcat>0){
			if($ytd==1){$query2 = "SELECT * FROM apinvoicedetail WHERE apinvoiceid = '$apinvoiceid' AND categoryid = '$showcat' AND date >= '$startdate' AND date <= '$date2' ORDER BY addon ASC, date DESC, itemid ASC";}
			else{$query2 = "SELECT * FROM apinvoicedetail WHERE apinvoiceid = '$apinvoiceid' AND categoryid = '$showcat' AND date >= '$date3' AND date <= '$date2' ORDER BY addon ASC, date DESC, itemid ASC";}
			$result2 = Treat_DB_ProxyOld::query($query2);

			while($r2=Treat_DB_ProxyOld::mysql_fetch_array($result2)){
				$item=$r2["item"];
				$date=$r2["date"];
				$addon=$r2["addon"];
				$amount=$r2["amount"];
				$amount=number_format($amount,2);
				if(substr($amount,0,1)=="-"){$amount="<font color=red>$amount</font>";}

				if($addon==1){$addon_color="#00FFFF";$showtitle="ADJUSTMENT AFTER ACCEPTED";}
				else{$addon_color="#FFFF99";$showtitle="";}

				echo "<tr><td></td><td bgcolor=$addon_color title='$showtitle'><font size=2><li>$item</font></td><td bgcolor=$addon_color><font size=2>$date</font></td><td align=right bgcolor=$addon_color><font size=2><b>$amount</b></font></td></tr>";
			}
			echo "<tr height=2 bgcolor=#CCCCCC><td colspan=4></td></tr>";
		}
	}

	echo "</table></center>";

	/////right column
	echo "</td><td>";

	echo "<center><table width=99% cellspacing=0 cellspacing=0 style=\"border: 2px solid #CCCCCC;\" bgcolor=white>";
	echo "<tr bgcolor=#CCCCCC><td colspan=3><center><font size=2><b>$thismonthname Category Budget Totals</b></font></td></tr>";

	$query1 = "SELECT * FROM po_category ORDER BY category_name";
        $result1 = Treat_DB_ProxyOld::query($query1);

	$largestbudget=0;
	$largestbudget_ytd=0;
	$largestbudget_all=0;
	while($r=Treat_DB_ProxyOld::mysql_fetch_array($result1)){
		$categoryid = $r["categoryid"];
		$category_name = $r["category_name"];
		$budget_type = $r["budget_type"];

		//////totals for month
		$query2 = "SELECT SUM(apinvoicedetail.amount) AS totalamt FROM apinvoice,apinvoicedetail WHERE apinvoice.businessid = '$businessid' AND apinvoice.transfer = '4' AND apinvoice.posted = '1' AND apinvoice.apinvoiceid = apinvoicedetail.apinvoiceid AND apinvoicedetail.categoryid = '$categoryid' AND apinvoicedetail.date >= '$date3' AND apinvoicedetail.date <= '$date2'";
		$result2 = Treat_DB_ProxyOld::query($query2);

		$totalamt=@Treat_DB_ProxyOld::mysql_result($result2,0,"totalamt");

		if(strlen($default_month)<2){$default_month="0$default_month";}
		$query2 = "SELECT `$default_month` AS budgetamt FROM budget WHERE businessid = '$bus_unit' AND companyid = '$companyid' AND type = '$budget_type' AND year = '$default_year'";
		$result2 = Treat_DB_ProxyOld::query($query2);

		$budgetamt=@Treat_DB_ProxyOld::mysql_result($result2,0,"budgetamt");

		if($budgetamt>$largestbudget){$largestbudget=$budgetamt;}

		//////totals for year
		$startdate="$default_year-01-01";
		$query2 = "SELECT SUM(apinvoicedetail.amount) AS totalamt_ytd FROM apinvoice,apinvoicedetail WHERE apinvoice.businessid = '$businessid' AND apinvoice.transfer = '4' AND apinvoice.posted = '1' AND apinvoice.apinvoiceid = apinvoicedetail.apinvoiceid AND apinvoicedetail.categoryid = '$categoryid' AND apinvoicedetail.date >= '$startdate' AND apinvoicedetail.date <= '$date2'";
		$result2 = Treat_DB_ProxyOld::query($query2);

		$totalamt_ytd=@Treat_DB_ProxyOld::mysql_result($result2,0,"totalamt_ytd");

		////build query
		$myquery="";
		for($counter=1;$counter<=$default_month;$counter++){
			if(strlen($counter)<2){$counter="0$counter";}
			if($myquery==""){$myquery="`$counter`";}
			else{$myquery.="+`$counter`";}
		}
		$query2 = "SELECT SUM($myquery) AS budgetamt_ytd FROM budget WHERE businessid = '$bus_unit' AND companyid = '$companyid' AND type = '$budget_type' AND year = '$default_year'";
		$result2 = Treat_DB_ProxyOld::query($query2);

		$budgetamt_ytd=@Treat_DB_ProxyOld::mysql_result($result2,0,"budgetamt_ytd");

		if($budgetamt_ytd>$largestbudget_ytd){$largestbudget_ytd=$budgetamt_ytd;}

		//////totals for all for year
		if($security_level>8){
		$query2 = "SELECT SUM(apinvoicedetail.amount) AS totalamt_ytd FROM apinvoice,apinvoicedetail WHERE apinvoice.transfer = '4' AND apinvoice.posted = '1' AND apinvoice.apinvoiceid = apinvoicedetail.apinvoiceid AND apinvoicedetail.categoryid = '$categoryid' AND apinvoicedetail.date >= '$startdate' AND apinvoicedetail.date <= '$date2'";
		$result2 = Treat_DB_ProxyOld::query($query2);

		$totalamt_all=@Treat_DB_ProxyOld::mysql_result($result2,0,"totalamt_ytd");

		////build query
		$myquery="";
		for($counter=1;$counter<=$default_month;$counter++){
			if(strlen($counter)<2){$counter="0$counter";}
			if($myquery==""){$myquery="`$counter`";}
			else{$myquery.="+`$counter`";}
		}
		$query2 = "SELECT SUM($myquery) AS budgetamt_ytd FROM budget WHERE type = '$budget_type' AND year = '$default_year'";
		$result2 = Treat_DB_ProxyOld::query($query2);

		$budgetamt_all=@Treat_DB_ProxyOld::mysql_result($result2,0,"budgetamt_ytd");

		if($budgetamt_all>$largestbudget_all){$largestbudget_all=$budgetamt_all;}
		}

		/////FILL ARRAY
		$category[$categoryid]=$category_name;

		////month
		$category_total[$categoryid]=$totalamt;
		$category_budget[$categoryid]=$budgetamt;

		////ytd
		$category_total_ytd[$categoryid]=$totalamt_ytd;
		$category_budget_ytd[$categoryid]=$budgetamt_ytd;

		////all
		$category_total_all[$categoryid]=$totalamt_all;
		$category_budget_all[$categoryid]=$budgetamt_all;
	}

	///show graph
	$graph=round(($largestbudget*1.25)/300,0);
	$graph2=round(($largestbudget_ytd*1.25)/300,0);
	$graph3=round(($largestbudget_all*1.25)/300,0);
	foreach($category AS $key => $value){
		$budget=round($category_budget[$key]/$graph,0);
		$total=round($category_total[$key]/$graph,0);

		$budget_ytd=round($category_budget_ytd[$key]/$graph2,0);
		$total_ytd=round($category_total_ytd[$key]/$graph2,0);

		$budget_all=round($category_budget_all[$key]/$graph3,0);
		$total_all=round($category_total_all[$key]/$graph3,0);

		/////month
		if($total==0){
			$antibudget=300-$budget;
			$budgetpercent=round($category_total[$key]/$category_budget[$key]*100,1);
			$showgraph="<table cellspacing=0 cellpadding=0 style=\"width: 302px;\"><tr height=5><td style=\"width: {$budget}px;\" colspan=2><img src=clear.gif></td><td style=\"width: 2px;\" rowspan=3 bgcolor=black><img src=clear.gif></td><td colspan=2 style=\"width: {$antibudget}px;\"><img src=clear.gif></td></tr>";
			$showgraph.="<tr height=10><td bgcolor=white colspan=2><img src=clear.gif></td><td colspan=2><font size=2>&nbsp;$budgetpercent%</font></td></tr>";
			$showgraph.="<tr height=5><td colspan=2><img src=clear.gif></td><td colspan=2><img src=clear.gif></td></tr></table>";
		}
		elseif($category_budget[$key]<$category_total[$key]){
			$antibudget=300-$budget;
			$newwidth=$total-$budget;
			$newwidth2=300-$budget-$total;
			$budgetpercent=round($category_total[$key]/$category_budget[$key]*100,1);
			$showgraph="<table cellspacing=0 cellpadding=0 style=\"width: 302px;\"><tr height=5><td style=\"width: {$budget}px;\" colspan=2><img src=clear.gif></td><td style=\"width: 2px;\" rowspan=3 bgcolor=black><img src=clear.gif></td><td colspan=2 style=\"width: {$antibudget}px;\"><img src=clear.gif></td></tr>";
			$showgraph.="<tr height=10><td bgcolor=red colspan=2><img src=clear.gif></td><td style=\"width: {$newwidth}px;\" bgcolor=red><img src=clear.gif></td><td style=\"width: {$newwidth2}px;\"><font size=2>&nbsp;$budgetpercent%</font></td></tr>";
			$showgraph.="<tr height=5><td colspan=2><img src=clear.gif></td><td colspan=2><img src=clear.gif></td></tr></table>";
		}
		elseif($category_budget[$key]>$category_total[$key]){
			$antibudget=300-$budget;
			$newwidth=$budget-$total;
			$budgetpercent=round($category_total[$key]/$category_budget[$key]*100,1);
			if(($category_budget[$key]*.05)>=($category_budget[$key]-$category_total[$key])){$showcolor="yellow";}
			else{$showcolor="#00FF00";}
			$showgraph="<table cellspacing=0 cellpadding=0 style=\"width: 302px;\"><tr height=5><td style=\"width: {$budget}px;\" colspan=2><img src=clear.gif></td><td style=\"width: 2px;\" rowspan=3 bgcolor=black><img src=clear.gif></td><td colspan=2 style=\"width: {$antibudget}px;\"><img src=clear.gif></td></tr>";
			$showgraph.="<tr height=10><td bgcolor=$showcolor style=\"width: {$total}px;\"><img src=clear.gif></td><td style=\"width: {$newwidth}px;\"><img src=clear.gif></td><td colspan=2><font size=2>&nbsp;$budgetpercent%</font></td></tr>";
			$showgraph.="<tr height=5><td colspan=2><img src=clear.gif></td><td colspan=2><img src=clear.gif></td></tr></table>";
		}
		elseif($category_budget[$key]==$category_total[$key]){
			$antibudget=300-$budget;
			$budgetpercent=round($category_total[$key]/$category_budget[$key]*100,1);
			$showgraph="<table cellspacing=0 cellpadding=0 style=\"width: 302px;\"><tr height=5><td style=\"width: {$budget}px;\" colspan=2><img src=clear.gif></td><td style=\"width: 2px;\" rowspan=3 bgcolor=black><img src=clear.gif></td><td colspan=2 style=\"width: {$antibudget}px;\"><img src=clear.gif></td></tr>";
			$showgraph.="<tr height=10><td bgcolor=yellow colspan=2><img src=clear.gif></td><td colspan=2><font size=2>&nbsp;$budgetpercent%</font></td></tr>";
			$showgraph.="<tr height=5><td colspan=2><img src=clear.gif></td><td colspan=2><img src=clear.gif></td></tr></table>";
		}

		$category_total[$key]=number_format($category_total[$key],0);
		$category_budget[$key]=number_format($category_budget[$key],0);

		if($showcat==$key&&$ytd==0){$mycolor="#FFFF99";$showbold="bold";$sendcat=0;}
		else{$mycolor="white";$showbold="";$sendcat=$key;}

		echo "<tr><td align=right width=30%><a href=busapinvoice3.php?bid=$businessid&cid=$companyid&showcat=$sendcat style=$style><font color=blue size=2 style=\"font-weight: $showbold;\">$value</font></a></td><td>$showgraph</td><td align=right><font size=2 style=\"background-color: $mycolor; font-weight: $showbold;\">$$category_total[$key]</font><font size=2 style=\"font-weight: $showbold;\">/$$category_budget[$key]</font></td></tr>";

		////year to date
		if($total_ytd==0){
			$antibudget=300-$budget_ytd;
			$budgetpercent=round($category_total_ytd[$key]/$category_budget_ytd[$key]*100,1);
			$showgraph="<table cellspacing=0 cellpadding=0 style=\"width: 302px;\"><tr height=5><td style=\"width: {$budget_ytd}px;\" colspan=2><img src=clear.gif></td><td style=\"width: 2px;\" rowspan=3 bgcolor=black><img src=clear.gif></td><td colspan=2 style=\"width: {$antibudget}px;\"><img src=clear.gif></td></tr>";
			$showgraph.="<tr height=10><td bgcolor=white colspan=2><img src=clear.gif></td><td colspan=2><font size=2>&nbsp;$budgetpercent%</font></td></tr>";
			$showgraph.="<tr height=5><td colspan=2><img src=clear.gif></td><td colspan=2><img src=clear.gif></td></tr></table>";
		}
		elseif($category_budget_ytd[$key]<$category_total_ytd[$key]){
			$antibudget=300-$budget_ytd;
			$newwidth=$total_ytd-$budget_ytd;
			$newwidth2=300-$budget_ytd-$total_ytd;
			$budgetpercent=round($category_total_ytd[$key]/$category_budget_ytd[$key]*100,1);
			$showgraph="<table cellspacing=0 cellpadding=0 style=\"width: 302px;\"><tr height=5><td style=\"width: {$budget_ytd}px;\" colspan=2><img src=clear.gif></td><td style=\"width: 2px;\" rowspan=3 bgcolor=black><img src=clear.gif></td><td colspan=2 style=\"width: {$antibudget}px;\"><img src=clear.gif></td></tr>";
			$showgraph.="<tr height=10><td bgcolor=red colspan=2><img src=clear.gif></td><td style=\"width: {$newwidth}px;\" bgcolor=red><img src=clear.gif></td><td style=\"width: {$newwidth2}px;\"><font size=2>&nbsp;$budgetpercent%</font></td></tr>";
			$showgraph.="<tr height=5><td colspan=2><img src=clear.gif></td><td colspan=2><img src=clear.gif></td></tr></table>";
		}
		elseif($category_budget_ytd[$key]>$category_total_ytd[$key]){
			$antibudget=300-$budget_ytd;
			$newwidth=$budget_ytd-$total_ytd;
			$budgetpercent=round($category_total_ytd[$key]/$category_budget_ytd[$key]*100,1);
			if(($category_budget_ytd[$key]*.05)>=($category_budget_ytd[$key]-$category_total_ytd[$key])){$showcolor="yellow";}
			else{$showcolor="#00FF00";}
			$showgraph="<table cellspacing=0 cellpadding=0 style=\"width: 302px;\"><tr height=5><td style=\"width: {$budget_ytd}px;\" colspan=2><img src=clear.gif></td><td style=\"width: 2px;\" rowspan=3 bgcolor=black><img src=clear.gif></td><td colspan=2 style=\"width: {$antibudget}px;\"><img src=clear.gif></td></tr>";
			$showgraph.="<tr height=10><td bgcolor=$showcolor style=\"width: {$total_ytd}px;\"><img src=clear.gif></td><td style=\"width: {$newwidth}px;\"><img src=clear.gif></td><td colspan=2><font size=2>&nbsp;$budgetpercent%</font></td></tr>";
			$showgraph.="<tr height=5><td colspan=2><img src=clear.gif></td><td colspan=2><img src=clear.gif></td></tr></table>";
		}
		elseif($category_budget_ytd[$key]==$category_total_ytd[$key]){
			$antibudget=300-$budget_ytd;
			$budgetpercent=round($category_total_ytd[$key]/$category_budget_ytd[$key]*100,1);
			$showgraph="<table cellspacing=0 cellpadding=0 style=\"width: 302px;\"><tr height=5><td style=\"width: {$budget_ytd}px;\" colspan=2><img src=clear.gif></td><td style=\"width: 2px;\" rowspan=3 bgcolor=black><img src=clear.gif></td><td colspan=2 style=\"width: {$antibudget}px;\"><img src=clear.gif></td></tr>";
			$showgraph.="<tr height=10><td bgcolor=yellow colspan=2><img src=clear.gif></td><td colspan=2><font size=2>&nbsp;$budgetpercent%</font></td></tr>";
			$showgraph.="<tr height=5><td colspan=2><img src=clear.gif></td><td colspan=2><img src=clear.gif></td></tr></table>";
		}

		$category_total_ytd[$key]=number_format($category_total_ytd[$key],0);
		$category_budget_ytd[$key]=number_format($category_budget_ytd[$key],0);

		if($showcat==$key&&$ytd==1){$mycolor="#FFFF99";$showbold="bold";$sendcat=0;}
		else{$mycolor="white";$showbold="";$sendcat=$key;}

		echo "<tr><td align=right><a href=busapinvoice3.php?bid=$businessid&cid=$companyid&showcat=$sendcat&ytd=1 style=$style><font color=blue size=2 style=\"font-weight: $showbold;\">$value YTD</font></a></td><td>$showgraph</td><td align=right><font size=2 style=\"background-color: $mycolor; font-weight: $showbold;\">$$category_total_ytd[$key]</font><font size=2 style=\"font-weight: $showbold;\">/$$category_budget_ytd[$key]</font></td></tr>";

		////all year to date
		if($security_level>9){
		if($total_all==0){
			$antibudget=300-$budget_all;
			$budgetpercent=round($category_total_all[$key]/$category_budget_all[$key]*100,1);
			$showgraph="<table cellspacing=0 cellpadding=0 style=\"width: 302px;\"><tr height=5><td style=\"width: {$budget_all}px;\" colspan=2><img src=clear.gif></td><td style=\"width: 2px;\" rowspan=3 bgcolor=black><img src=clear.gif></td><td colspan=2 style=\"width: {$antibudget}px;\"><img src=clear.gif></td></tr>";
			$showgraph.="<tr height=10><td bgcolor=white colspan=2><img src=clear.gif></td><td colspan=2><font size=2>&nbsp;$budgetpercent%</font></td></tr>";
			$showgraph.="<tr height=5><td colspan=2><img src=clear.gif></td><td colspan=2><img src=clear.gif></td></tr></table>";
		}
		elseif($category_budget_all[$key]<$category_total_all[$key]){
			$antibudget=300-$budget_all;
			$newwidth=$total_all-$budget_all;
			$newwidth2=300-$budget_all-$total_all;
			$budgetpercent=round($category_total_all[$key]/$category_budget_all[$key]*100,1);
			$showgraph="<table cellspacing=0 cellpadding=0 style=\"width: 302px;\"><tr height=5><td style=\"width: {$budget_all}px;\" colspan=2><img src=clear.gif></td><td style=\"width: 2px;\" rowspan=3 bgcolor=black><img src=clear.gif></td><td colspan=2 style=\"width: {$antibudget}px;\"><img src=clear.gif></td></tr>";
			$showgraph.="<tr height=10><td bgcolor=red colspan=2><img src=clear.gif></td><td style=\"width: {$newwidth}px;\" bgcolor=red><img src=clear.gif></td><td style=\"width: {$newwidth2}px;\"><font size=2>&nbsp;$budgetpercent%</font></td></tr>";
			$showgraph.="<tr height=5><td colspan=2><img src=clear.gif></td><td colspan=2><img src=clear.gif></td></tr></table>";
		}
		elseif($category_budget_all[$key]>$category_total_all[$key]){
			$antibudget=300-$budget_all;
			$newwidth=$budget_all-$total_all;
			$budgetpercent=round($category_total_all[$key]/$category_budget_all[$key]*100,1);
			if(($category_budget_all[$key]*.05)>=($category_budget_all[$key]-$category_total_all[$key])){$showcolor="yellow";}
			else{$showcolor="#00FF00";}
			$showgraph="<table cellspacing=0 cellpadding=0 style=\"width: 302px;\"><tr height=5><td style=\"width: {$budget_all}px;\" colspan=2><img src=clear.gif></td><td style=\"width: 2px;\" rowspan=3 bgcolor=black><img src=clear.gif></td><td colspan=2 style=\"width: {$antibudget}px;\"><img src=clear.gif></td></tr>";
			$showgraph.="<tr height=10><td bgcolor=$showcolor style=\"width: {$total_all}px;\"><img src=clear.gif></td><td style=\"width: {$newwidth}px;\"><img src=clear.gif></td><td colspan=2><font size=2>&nbsp;$budgetpercent%</font></td></tr>";
			$showgraph.="<tr height=5><td colspan=2><img src=clear.gif></td><td colspan=2><img src=clear.gif></td></tr></table>";
		}
		elseif($category_budget_all[$key]==$category_total_all[$key]){
			$antibudget=300-$budget_all;
			$budgetpercent=round($category_total_all[$key]/$category_budget_all[$key]*100,1);
			$showgraph="<table cellspacing=0 cellpadding=0 style=\"width: 302px;\"><tr height=5><td style=\"width: {$budget_all}px;\" colspan=2><img src=clear.gif></td><td style=\"width: 2px;\" rowspan=3 bgcolor=black><img src=clear.gif></td><td colspan=2 style=\"width: {$antibudget}px;\"><img src=clear.gif></td></tr>";
			$showgraph.="<tr height=10><td bgcolor=yellow colspan=2><img src=clear.gif></td><td colspan=2><font size=2>&nbsp;$budgetpercent%</font></td></tr>";
			$showgraph.="<tr height=5><td colspan=2><img src=clear.gif></td><td colspan=2><img src=clear.gif></td></tr></table>";
		}

		$category_total_all[$key]=number_format($category_total_all[$key],0);
		$category_budget_all[$key]=number_format($category_budget_all[$key],0);

		if($newcat==$key&&$ytd==1){$mycolor="#FFFF99";$showbold="bold";$sendcat=0;}
		else{$mycolor="white";$showbold="";$sendcat=$key;}

		echo "<tr><td align=right><a href=busapinvoice3.php?bid=$businessid&cid=$companyid&showcat=-$sendcat&ytd=1 style=$style><font color=blue size=2 style=\"font-weight: $showbold;\">$value CO</font></a></td><td>$showgraph</td><td align=right><font size=2 style=\"background-color: $mycolor; font-weight: $showbold;\">$$category_total_all[$key]</font><font size=2 style=\"font-weight: $showbold;\">/$$category_budget_all[$key]</font></td></tr>";
		}
		echo "<tr height=1 bgcolor=#CCCCCC><td colspan=3><img src=clear.gif></td></tr>";
	}

	echo "</table></center>";
	echo "<font size=2>*The <b>|</b> marks your budget limit.</font>";
	echo "</td></tr></table></td></tr>";

	echo "<tr bgcolor=black><td height=1 colspan=4></td></tr>";

	///////////add po
	echo "<tr><td colspan=4><p><br><form action='apinvoice.php#additems' method='post' onSubmit='return disableForm(this);'> <u>Purchase Order Date</u>: <SCRIPT LANGUAGE='JavaScript' ID='js20'> var cal20 = new CalendarPopup('testdiv1');cal20.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date3 VALUE='$today' SIZE=8> <A HREF=#new onClick=cal20.select(document.forms[3].date3,'anchor20','yyyy-MM-dd'); return false; TITLE=cal20.select(document.forms[3].date3,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor20' ID='anchor20'><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A> ";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<input type=hidden name=businessid value=$businessid>";
    echo "<input type=hidden name=companyid value=$companyid>";
    echo "<input type=hidden name=date value='$today'>";
    echo "<input type=hidden name=new value='yes'>";
    echo "<input type=hidden name=transfer value='4'>";
	echo "<input type=hidden name=goto value='4'>";
    echo " <u>Vendor</u>: <select name=vendor>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query3 = "SELECT * FROM vendors WHERE businessid = '$businessid' AND is_deleted = '0' ORDER BY name DESC";
    $result3 = Treat_DB_ProxyOld::query($query3);
    $num3=Treat_DB_ProxyOld::mysql_numrows($result3);
    //mysql_close();

    $num3--;

    while ($num3>=0){
       $vendorname=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"name");
       $vendorid=@Treat_DB_ProxyOld::mysql_result($result3,$num3,"vendorid");
       echo "<option value=$vendorid>$vendorname</option>";
       $num3--;
    }

    echo "</select>";

    echo " <input type=submit value='Create'></form></td></tr>";

    echo "</table></center><DIV ID=testdiv1 STYLE=position:absolute;visibility:hidden;background-color:white;layer-background-color:white;></DIV>";

    echo "<br><FORM ACTION=businesstrack.php method=post>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<center><INPUT TYPE=submit VALUE=' Return to Main Page'></FORM></center>";

	google_page_track();
}
//mysql_close();
?>
</body>