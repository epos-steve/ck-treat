<?php

/////////////////////////////////////////REVISION HISTORY////////////////////////////////////////////
//2/21/07 - Changed Check Average to include Sales Only -SM
//2/27/07 - Removed Catering count from check average -SM
/////////////////////////////////////////REVISION HISTORY////////////////////////////////////////////

function money($diff){
   if($diff==""){}
   else{
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+4);
        $diff=round($diff, 2);
        if ($diff > 0 && $diff <= 0.01){$diff="0.01";}
        elseif($diff < 0 && $diff >= -0.01){$diff="-0.01";}
        $diff = substr($diff, 0, $dot+3);
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
   }

        return $diff;
}

function nextday($nextd,$day_format){ //Function returns next day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monn = substr($nextd, 5, 2);
   $dayn = substr($nextd, 8, 2);
   $yearn = substr($nextd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monn, $dayn+1, $yearn));
   return $tempdate;
}

function prevday($prevd,$day_format){ //Function returns previous day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monp = substr($prevd, 5, 2);
   $dayp = substr($prevd, 8, 2);
   $yearp = substr($prevd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monp, $dayp-1, $yearp));
   return $tempdate;
}

function futureday($num) {
$day = date("d");
$year = date("Y");
$month = date("m");
$leap = date("L");
$day=$day+$num;

   if (($month == '01' || $month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '31')
   {
      if ($month == "01"){$month="02";}
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      $day=$day-31;
      if ($day<10){$day="0$day";}
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '1' && $day >= '29')
   {
      $month='03';
      $day=$day-29;
      if ($day<10){$day="0$day";}
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $leap == '0' && $day >= '28')
   {
      $month='03';
      $day=$day-28;
      if ($day<10){$day="0$day";}
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day=$day-30;
      if ($day<10){$day="0$day";}
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month==12 && $day>=31)
   {
      $day=$day-31;
      if ($day<10){$day="0$day";}
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function pastday($num) {
$day = date("d");
$day=$day-$num;
if ($day <= 0)
{
   $year = date("Y");
   $month = date("m");
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $year=date("Y");
   $month=date("m");
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}

define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');

$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");
$creditamount="creditamount";
$creditidnty="creditidnty";
$creditdate="creditdate";
$debitamount="debitamount";
$debitidnty="debitidnty";
$debitdate="debitdate";
$quatertotal=0;
$monthtotal=0;
$total=0;
$counter=0;
$findme='.';
$double='.00';
$single='0';
$double2='00';
$curarrow=0;

/*$user=$_COOKIE["usercook"];
$pass=$_COOKIE["passcook"];
$businessid=$_GET["bid"];
$companyid=$_GET["cid"];
$prevperiod=$_GET["prevperiod"];
$nextperiod=$_GET["nextperiod"];
$nowperiod=$_GET["nowperiod"];
$audit=$_GET["audit"];*/

$user = \EE\Controller\Base::getSessionCookieVariable('usercook');
$pass = \EE\Controller\Base::getSessionCookieVariable('passcook');
$businessid = \EE\Controller\Base::getGetVariable('bid');
$companyid = \EE\Controller\Base::getGetVariable('cid');
$prevperiod = \EE\Controller\Base::getGetVariable('prevperiod');
$nextperiod = \EE\Controller\Base::getGetVariable('nextperiod');
$nowperiod = \EE\Controller\Base::getGetVariable('nowperiod');
$audit = \EE\Controller\Base::getGetVariable('audit');

if ($businessid==""&&$companyid==""){
   /*$businessid=$_POST["businessid"];
   $companyid=$_POST["companyid"];
   $newdate=$_POST["newdate"];*/
	
	$businessid = \EE\Controller\Base::getPostVariable('businessid');
	$companyid = \EE\Controller\Base::getPostVariable('companyid');
	$newdate = \EE\Controller\Base::getPostVariable('newdate');
}

if ($newdate!=""){
   $today=$newdate;
   $todayname=dayofweek($today);
}

/*mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( mysql_error());*/

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=Treat_DB_ProxyOld::mysql_numrows($result);
//mysql_close();

if ($num!=0){
    $loginid=Treat_DB_ProxyOld::mysql_result($result,0,"loginid");
    $security_level=Treat_DB_ProxyOld::mysql_result($result,0,"security_level");
    $mysecurity=Treat_DB_ProxyOld::mysql_result($result,0,"security");
    $bid=Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
    $bid2=Treat_DB_ProxyOld::mysql_result($result,0,"busid2");
    $bid3=Treat_DB_ProxyOld::mysql_result($result,0,"busid3");
    $bid4=Treat_DB_ProxyOld::mysql_result($result,0,"busid4");
    $bid5=Treat_DB_ProxyOld::mysql_result($result,0,"busid5");
    $bid6=Treat_DB_ProxyOld::mysql_result($result,0,"busid6");
    $bid7=Treat_DB_ProxyOld::mysql_result($result,0,"busid7");
    $bid8=Treat_DB_ProxyOld::mysql_result($result,0,"busid8");
    $bid9=Treat_DB_ProxyOld::mysql_result($result,0,"busid9");
    $bid10=Treat_DB_ProxyOld::mysql_result($result,0,"busid10");
    $cid=Treat_DB_ProxyOld::mysql_result($result,0,"companyid");

	$pr1=Treat_DB_ProxyOld::mysql_result($result,0,"payroll");
	$pr2=Treat_DB_ProxyOld::mysql_result($result,0,"pr2");
	$pr3=Treat_DB_ProxyOld::mysql_result($result,0,"pr3");
	$pr4=Treat_DB_ProxyOld::mysql_result($result,0,"pr4");
	$pr5=Treat_DB_ProxyOld::mysql_result($result,0,"pr5");
	$pr6=Treat_DB_ProxyOld::mysql_result($result,0,"pr6");
	$pr7=Treat_DB_ProxyOld::mysql_result($result,0,"pr7");
	$pr8=Treat_DB_ProxyOld::mysql_result($result,0,"pr8");
	$pr9=Treat_DB_ProxyOld::mysql_result($result,0,"pr9");
	$pr10=Treat_DB_ProxyOld::mysql_result($result,0,"pr10");
}
?>
<head>

<script language="JavaScript"
   type="text/JavaScript">
function changePage(newLoc)
 {
   nextPage = newLoc.options[newLoc.selectedIndex].value

   if (nextPage != "")
   {
      document.location.href = nextPage
   }
 }
</script>

<SCRIPT LANGUAGE=javascript><!--
function submit2(){return confirm('MAKE SURE YOU SAVE BEFORE YOU SUBMIT!! If you have entered sales without saving, they will not submit!!!');}
// --></SCRIPT>

<script type="text/javascript" language="javascript">
function checkKey(){

var key = event.keyCode;
var DaIndexId = event.srcElement.IndexId-0
if(key==38) {
document.getElementByIndex(DaIndexId-1).focus();
return false;
}
else if(key==40) {
document.getElementByIndex(DaIndexId+1).focus();
return false;
}
else if(key==13) {
document.getElementByIndex(DaIndexId+1).focus();
return false;
}
else if(key==37) {
document.getElementByIndex(DaIndexId-1000).focus();
return false;
}
else if(key==39) {
document.getElementByIndex(DaIndexId+1000).focus();
return false;
}
return true
} // end checkKey function
document.getElementByIndex=function(){
for(var i=0;i<document.all.length;i++)
if(document.all[i].IndexId==arguments[0])
return document.all[i]
return null
}
</script>

<SCRIPT LANGUAGE="JavaScript" SRC="CalendarPopup.js"></SCRIPT>

<!-- This prints out the default stylehseets used by the DIV style calendar.
     Only needed if you are using the DIV style popup -->
<SCRIPT LANGUAGE="JavaScript">document.write(getCalendarStyles());</SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
<!-- Web Site:  http://dynamicdrive.com -->

<!-- This script and many more are available free online at -->
<!-- The JavaScript Source!! http://javascript.internet.com -->

<!-- Begin
function disableForm(theform) {
if (document.all || document.getElementById) {
for (i = 0; i < theform.length; i++) {
var tempobj = theform.elements[i];
if (tempobj.type.toLowerCase() == "submit" || tempobj.type.toLowerCase() == "reset")
tempobj.disabled = true;
}

}
else {
alert("The form has been submitted.  But, since you're not using IE 4+ or NS 6, the submit button was not disabled on form submission.");
return false;
   }
}
//  End -->
</script>

<STYLE>
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation
			{
			background-color:#6677DD;
			text-align:center;
			vertical-align:center;
			text-decoration:none;
			color:#FFFFFF;
			font-weight:bold;
			}
	.TESTcpDayColumnHeader,
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation,
	.TESTcpCurrentMonthDate,
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDate,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDate,
	.TESTcpCurrentDateDisabled,
	.TESTcpTodayText,
	.TESTcpTodayTextDisabled,
	.TESTcpText
			{
			font-family:arial;
			font-size:8pt;
			}
	TD.TESTcpDayColumnHeader
			{
			text-align:right;
			border:solid thin #6677DD;
			border-width:0 0 1 0;
			}
	.TESTcpCurrentMonthDate,
	.TESTcpOtherMonthDate,
	.TESTcpCurrentDate
			{
			text-align:right;
			text-decoration:none;
			}
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDateDisabled
			{
			color:#D0D0D0;
			text-align:right;
			text-decoration:line-through;
			}
	.TESTcpCurrentMonthDate
			{
			color:#6677DD;
			font-weight:bold;
			}
	.TESTcpCurrentDate
			{
			color: #FFFFFF;
			font-weight:bold;
			}
	.TESTcpOtherMonthDate
			{
			color:#808080;
			}
	TD.TESTcpCurrentDate
			{
			color:#FFFFFF;
			background-color: #6677DD;
			border-width:1;
			border:solid thin #000000;
			}
	TD.TESTcpCurrentDateDisabled
			{
			border-width:1;
			border:solid thin #FFAAAA;
			}
	TD.TESTcpTodayText,
	TD.TESTcpTodayTextDisabled
			{
			border:solid thin #6677DD;
			border-width:1 0 0 0;
			}
	A.TESTcpTodayText,
	SPAN.TESTcpTodayTextDisabled
			{
			height:20px;
			}
	A.TESTcpTodayText
			{
			color:#6677DD;
			font-weight:bold;
			}
	SPAN.TESTcpTodayTextDisabled
			{
			color:#D0D0D0;
			}
	.TESTcpBorder
			{
			border:solid thin #6677DD;
			}
</STYLE>

<script language="JavaScript"
   type="text/JavaScript">
function calc_popup(url)
  {
     popwin=window.open(url,"Calculator","location=no,menubar=no,titlebar=no,resizeable=no,height=50,width=100");
  }
</script>

<script type="text/javascript" src="javascripts/prototype.js"></script>

<script language="JavaScript"
   type="text/JavaScript">
window.onload = function(){
   new ArrowGrid('test');
   new headerScroll('test',{head: 'header'});
}
</script>

</head>



<?php
if ($num != 1 || ($user == "" && $pass == ""))
{
    echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}

elseif (($security_level==1 && $bid == $businessid && $cid == $companyid) || ($security_level > 1 AND $cid == $companyid))
{
    echo "<body><div style=border:50px solid red;padding:10px;><center><table cellspacing=0 cellpadding=0 border=0 width=90%><tr><td colspan=2>";
    echo "<a href=businesstrack.php><img src=logo.jpg border=0 height=43 width=205></a><p></td></tr>";

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM company WHERE companyid = '$companyid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $companyname=Treat_DB_ProxyOld::mysql_result($result,0,"companyname");
    $week_end=Treat_DB_ProxyOld::mysql_result($result,0,"week_end");
    $week_start=Treat_DB_ProxyOld::mysql_result($result,0,"week_start");
    $com_logo=@Treat_DB_ProxyOld::mysql_result($result,0,"logo");

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM security WHERE securityid = '$mysecurity'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $sec_bus_sales=Treat_DB_ProxyOld::mysql_result($result,0,"bus_sales");
    $sec_bus_invoice=Treat_DB_ProxyOld::mysql_result($result,0,"bus_invoice");
    $sec_bus_order=Treat_DB_ProxyOld::mysql_result($result,0,"bus_order");
    $sec_bus_payable=Treat_DB_ProxyOld::mysql_result($result,0,"bus_payable");
    $sec_bus_inventor1=Treat_DB_ProxyOld::mysql_result($result,0,"bus_inventor1");
    $sec_bus_control=Treat_DB_ProxyOld::mysql_result($result,0,"bus_control");
    $sec_bus_menu=Treat_DB_ProxyOld::mysql_result($result,0,"bus_menu");
    $sec_bus_order_setup=Treat_DB_ProxyOld::mysql_result($result,0,"bus_order_setup");
    $sec_bus_request=Treat_DB_ProxyOld::mysql_result($result,0,"bus_request");
    $sec_route_collection=Treat_DB_ProxyOld::mysql_result($result,0,"route_collection");
    $sec_route_labor=Treat_DB_ProxyOld::mysql_result($result,0,"route_labor");
    $sec_route_detail=Treat_DB_ProxyOld::mysql_result($result,0,"route_detail");
	$sec_route_machine=@Treat_DB_ProxyOld::mysql_result($result,0,"route_machine");
    $sec_bus_labor=Treat_DB_ProxyOld::mysql_result($result,0,"bus_labor");
	$sec_bus_budget=Treat_DB_ProxyOld::mysql_result($result,0,"bus_budget");

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM business WHERE companyid = '$companyid' AND businessid = '$businessid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $businessname=Treat_DB_ProxyOld::mysql_result($result,0,"businessname");
    $businessid=Treat_DB_ProxyOld::mysql_result($result,0,"businessid");
    $street=Treat_DB_ProxyOld::mysql_result($result,0,"street");
    $city=Treat_DB_ProxyOld::mysql_result($result,0,"city");
    $state=Treat_DB_ProxyOld::mysql_result($result,0,"state");
    $zip=Treat_DB_ProxyOld::mysql_result($result,0,"zip");
    $phone=Treat_DB_ProxyOld::mysql_result($result,0,"phone");
    $bus_petty_cash=money(Treat_DB_ProxyOld::mysql_result($result,0,"petty_cash"));
    $bus_unit=Treat_DB_ProxyOld::mysql_result($result,0,"unit");

    if($com_logo == ""){$com_logo = "talogo.gif";}

    //echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";
    echo "<tr bgcolor=#CCCCFF><td width=1%><img src=logo/$com_logo></td><td><font size=4><b>$businessname #$bus_unit</b></font><br>$street<br>$city, $state $zip<br>$phone</td>";
    echo "<td align=right valign=top>";
    echo "<br><FORM ACTION=editbus.php method=post>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<input type=hidden name=businessid value=$businessid>";
    echo "<input type=hidden name=companyid value=$companyid>";
    echo "<INPUT TYPE=submit VALUE=' Store Setup '></FORM></td></tr>";
    //echo "<tr bgcolor=black><td colspan=3 height=1></td></tr>";

    echo "<tr><td colspan=3><font color=#999999>";
    if ($sec_bus_sales>0){echo "<a href=busdetail.php?bid=$businessid&cid=$companyid><font color=red onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='red' style='text-decoration:none'>Sales</font></a>";}
    if ($sec_bus_invoice>0){echo " | <a href=businvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Invoicing</font></a>";}
    if ($sec_bus_order>0){echo " | <a href=buscatertrack.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Orders</font></a>";}
    if ($sec_bus_payable>0){echo " | <a href=busapinvoice.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Payables</font></a>";}
    if ($sec_bus_inventor1>0){echo " | <a href=businventor.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Inventory</font></a>";}
    if ($sec_bus_control>0){echo " | <a href=buscontrol.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Control Sheet</font></a>";}
    if ($sec_bus_menu>0){echo " | <a href=buscatermenu.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Menus</font></a>";}
    if ($sec_bus_order_setup>0){echo " | <a href=buscaterroom.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Order Setup</font></a>";}
    if ($sec_bus_request>0){echo " | <a href=busrequest.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Requests</font></a>";}
    if ($sec_route_collection>0||$sec_route_labor>0||$sec_route_detail>0||$sec_route_machine>0){echo " | <a href=busroute_collect.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Route Collections</font></a>";}
    if ($sec_bus_labor>0){echo " | <a href=buslabor.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Labor</font></a>";}
    if ($sec_bus_budget>0||($businessid == $bid && $pr1 == 1)||($businessid == $bid2 && $pr2 == 1)||($businessid == $bid3 && $pr3 == 1)||($businessid == $bid4 && $pr4 == 1)||($businessid == $bid5 && $pr5 == 1)||($businessid == $bid6 && $pr6 == 1)||($businessid == $bid7 && $pr7 == 1)||($businessid == $bid8 && $pr8 == 1)||($businessid == $bid9 && $pr9 == 1)||($businessid == $bid10 && $pr10 == 1)){echo " | <a href=busbudget.php?bid=$businessid&cid=$companyid><font color=#999999 onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#999999' style='text-decoration:none'>Budget</font></a>";}
	echo "</td></tr>";

    echo "</table></center>";

    ////////////////////NEW WAY
    if ($prevperiod!=""){$temp_date=$prevperiod;}
    elseif ($nextperiod!=""){$temp_date=$nextperiod;}
    elseif ($nowperiod!=""){$temp_date=$nowperiod;}
    elseif ($newdate!=""){$temp_date=$newdate;}
    else {$temp_date=date("Y-m-d");}

       while(dayofweek($temp_date)!=$week_end){$temp_date=nextday($temp_date);}

       $day7=$temp_date;
       $day7name=dayofweek($day7);

       $day6=prevday($day7);
       $day6name=dayofweek($day6);

       $day5=prevday($day6);
       $day5name=dayofweek($day5);

       $day4=prevday($day5);
       $day4name=dayofweek($day4);

       $day3=prevday($day4);
       $day3name=dayofweek($day3);

       $day2=prevday($day3);
       $day2name=dayofweek($day2);

       $day1=prevday($day2);
       $day1name=dayofweek($day1);

    $prevperiod=prevday($day1);
    $nextperiod=nextday($day7);

if ($security_level>0){
    echo "<p><center><table width=90%><tr><td width=50%><form action=busdetail2.php method=post>";
    echo "<select name=gobus onChange='changePage(this.form.gobus)'>";

    $districtquery="";
    if ($security_level>2&&$security_level<7){
       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query = "SELECT * FROM login_district WHERE loginid = '$loginid'";
       $result = Treat_DB_ProxyOld::query($query);
       $num=Treat_DB_ProxyOld::mysql_numrows($result);
       //mysql_close();

       $num--;
       while($num>=0){
          $districtid=Treat_DB_ProxyOld::mysql_result($result,$num,"districtid");
          if($districtquery==""){$districtquery="districtid = '$districtid'";}
          else{$districtquery="$districtquery OR districtid = '$districtid'";}
          $num--;
       }
    }

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    if($security_level>6){$query = "SELECT * FROM business WHERE companyid = '$companyid' ORDER BY businessname DESC";}
    elseif($security_level==2||$security_level==1){$query = "SELECT * FROM business WHERE companyid = '$companyid' AND (businessid = '$bid' OR businessid = '$bid2' OR businessid = '$bid3' OR businessid = '$bid4' OR businessid = '$bid5' OR businessid = '$bid6' OR businessid = '$bid7' OR businessid = '$bid8' OR businessid = '$bid9' OR businessid = '$bid10') ORDER BY businessname DESC";}
    elseif($security_level>2&&$security_level<7){$query = "SELECT * FROM business WHERE companyid = '$companyid' AND ($districtquery) ORDER BY businessname DESC";}
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    $num--;
    while($num>=0){
       $businessname=Treat_DB_ProxyOld::mysql_result($result,$num,"businessname");
       $busid=Treat_DB_ProxyOld::mysql_result($result,$num,"businessid");

       if ($busid==$businessid){$show="SELECTED";}
       else{$show="";}

       echo "<option value=busdetail2.php?bid=$busid&cid=$companyid&prevperiod=$day1 $show>$businessname</option>";
       $num--;
    }

    echo "</select></td><td></form></td><td width=50% align=right><form action=busdetail2.php method=post><input type=hidden name=businessid value=$businessid><input type=hidden name=companyid value=$companyid><SCRIPT LANGUAGE='JavaScript' ID='js19'> var cal19 = new CalendarPopup('testdiv1');cal19.setCssPrefix('TEST');</SCRIPT> <A HREF=# onClick=cal19.select(document.forms[2].newdate,'anchor19','yyyy-MM-dd'); return false; TITLE=cal19.select(document.forms[2].newdate,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor19' ID='anchor19'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A> <input type=text name=newdate value='$today' size=8> <input type=submit value='GO'></td><td></form></td></tr></table></center>";
}

    $security_level=$sec_bus_sales;

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM submit WHERE businessid = '$businessid' AND date = '$day1'";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    $wordfont1="black";
    $wordfont2="black";
    $wordfont3="black";
    $wordfont4="black";
    $wordfont5="black";
    $wordfont6="black";
    $wordfont7="black";

    if ($num!=0){$export=Treat_DB_ProxyOld::mysql_result($result,0,"export");if ($export==1){$day1color="#444455";$wordfont1="white";}else{$day1color="#00CC00";}}else{$day1color="#00CCFF";}

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM submit WHERE businessid = '$businessid' AND date = '$day2'";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    if ($num!=0){$export=Treat_DB_ProxyOld::mysql_result($result,0,"export");if ($export==1){$day2color="#444455";$wordfont2="white";}else{$day2color="#00CC00";}}else{$day2color="#00CCFF";}

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM submit WHERE businessid = '$businessid' AND date = '$day3'";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    if ($num!=0){$export=Treat_DB_ProxyOld::mysql_result($result,0,"export");if ($export==1){$day3color="#444455";$wordfont3="white";}else{$day3color="#00CC00";}}else{$day3color="#00CCFF";}

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM submit WHERE businessid = '$businessid' AND date = '$day4'";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    if ($num!=0){$export=mysql_result($result,0,"export");if ($export==1){$day4color="#444455";$wordfont4="white";}else{$day4color="#00CC00";}}else{$day4color="#00CCFF";}

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM submit WHERE businessid = '$businessid' AND date = '$day5'";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    if ($num!=0){$export=mysql_result($result,0,"export");if ($export==1){$day5color="#444455";$wordfont5="white";}else{$day5color="#00CC00";}}else{$day5color="#00CCFF";}

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM submit WHERE businessid = '$businessid' AND date = '$day6'";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    if ($num!=0){$export=mysql_result($result,0,"export");if ($export==1){$day6color="#444455";$wordfont6="white";}else{$day6color="#00CC00";}}else{$day6color="#00CCFF";}

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM submit WHERE businessid = '$businessid' AND date = '$day7'";
    $result = Treat_DB_ProxyOld::query($query);
    $num=Treat_DB_ProxyOld::mysql_numrows($result);
    //mysql_close();

    if ($num!=0){$export=Treat_DB_ProxyOld::mysql_result($result,0,"export");if ($export==1){$day7color="#444455";$wordfont7="white";}else{$day7color="#00CC00";}}else{$day7color="#00CCFF";}

    echo "<form action=saveimpress.php method=post onSubmit='return disableForm(this);'><input type=hidden name=businessid value=$businessid><input type=hidden name=companyid value=$companyid><input type=hidden name=day1 value=$day1>";

    echo "<p><center><table width=90% cellspacing=0 cellpadding=0>";
    echo "<tr bgcolor=#CCCCCC valign=top><td width=1%><img src=leftcrn2.jpg height=6 width=6 align=top></td><td width=15%><center><a href=busdetail.php?cid=$companyid&bid=$businessid&prevperiod=$day1 style=$style><font color=blue>Daily Sales</a></center></td><td width=1% align=right><img src=rightcrn2.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td width=1% bgcolor=#E8E7E7><img src=leftcrn.jpg height=6 width=6 align=top></td><td width=15% bgcolor=#E8E7E7><center><a href=busdetail2.php?cid=$companyid&bid=$businessid&prevperiod=$day1 style=$style style=$style><font color=black>Daily Imprest Fund</a></center></td><td width=1% align=right bgcolor=#E8E7E7><img src=rightcrn.jpg height=6 width=6 align=top></td><td width=1% bgcolor=white></td><td bgcolor=white width=60%></td></tr>";
    echo "<tr height=2><td colspan=4></td><td colspan=3 bgcolor=#E8E7E7></td><td></td><td colspan=1></td></tr>";
    echo "</table>";

    echo "<center><table border=0 width=90% bgcolor=#E8E7E7 cellspacing=1 cellpadding=0 id='test'>";
    echo "<tr bgcolor=#E8E7E7 id='header'><td bgcolor=#E8E7E7 align=right><font size=1>[<a href=busdetail2.php?bid=$businessid&cid=$companyid&prevperiod=$prevperiod>PREV</a>]</td><td align=right bgcolor=$day1color><font color=$wordfont1>$day1</td><td align=right bgcolor=$day2color><font color=$wordfont2>$day2</td><td align=right bgcolor=$day3color><font color=$wordfont3>$day3</td><td align=right bgcolor=$day4color><font color=$wordfont4>$day4</td><td align=right bgcolor=$day5color><font color=$wordfont5>$day5</td><td align=right bgcolor=$day6color><font color=$wordfont6>$day6</td><td align=right bgcolor=$day7color><font color=$wordfont7>$day7</td><td bgcolor=#E8E7E7> <font size=1>[<a href=busdetail2.php?bid=$businessid&cid=$companyid&nextperiod=$nextperiod>NEXT</a>]</td></tr>";

    echo "<tr bgcolor=#CCCC99><td></td>";
    $temp_date=$day1;
    for($counter=1;$counter<=7;$counter++){$dayname=dayofweek($temp_date); echo "<td align=right>$dayname</td>"; $temp_date=nextday($temp_date);}
    echo "<td></td></tr>";

    $query = "SELECT * FROM impress WHERE businessid = '$businessid' AND date = '$day1'";
    $result = Treat_DB_ProxyOld::query($query);

    $day1_petty_cash=money(Treat_DB_ProxyOld::mysql_result($result,0,"petty_cash"));
    $day1_voucher=money(Treat_DB_ProxyOld::mysql_result($result,0,"voucher"));
    $day1_unreimbursed=money(Treat_DB_ProxyOld::mysql_result($result,0,"unreimbursed"));
    $day1_change_fund=money(Treat_DB_ProxyOld::mysql_result($result,0,"change_fund"));
    $day1_cash_in_trans=money(Treat_DB_ProxyOld::mysql_result($result,0,"cash_in_trans"));
    $day1_reg_bank=money(Treat_DB_ProxyOld::mysql_result($result,0,"reg_bank"));
    $day1_reg_bank2=money(Treat_DB_ProxyOld::mysql_result($result,0,"reg_bank2"));
    $day1_other=money(Treat_DB_ProxyOld::mysql_result($result,0,"other"));
    $day1_funds_adv=money(Treat_DB_ProxyOld::mysql_result($result,0,"funds_adv"));
    $day1_user=Treat_DB_ProxyOld::mysql_result($result,0,"user");

    $day1_tot_petty_cash=money($day1_petty_cash+$day1_voucher+$day1_unreimbursed);
    $day1_tot_count=money($day1_change_fund+$day1_cash_in_trans+$day1_reg_bank+$day1_reg_bank2+$day1_other+$day1_funds_adv+$day1_tot_petty_cash);
    $day1_over_short=money($day1_tot_count-$bus_petty_cash);

    $query = "SELECT * FROM impress WHERE businessid = '$businessid' AND date = '$day2'";
    $result = Treat_DB_ProxyOld::query($query);

    $day2_petty_cash=money(Treat_DB_ProxyOld::mysql_result($result,0,"petty_cash"));
    $day2_voucher=money(Treat_DB_ProxyOld::mysql_result($result,0,"voucher"));
    $day2_unreimbursed=money(Treat_DB_ProxyOld::mysql_result($result,0,"unreimbursed"));
    $day2_change_fund=money(Treat_DB_ProxyOld::mysql_result($result,0,"change_fund"));
    $day2_cash_in_trans=money(Treat_DB_ProxyOld::mysql_result($result,0,"cash_in_trans"));
    $day2_reg_bank=money(Treat_DB_ProxyOld::mysql_result($result,0,"reg_bank"));
    $day2_reg_bank2=money(Treat_DB_ProxyOld::mysql_result($result,0,"reg_bank2"));
    $day2_other=money(Treat_DB_ProxyOld::mysql_result($result,0,"other"));
    $day2_funds_adv=money(Treat_DB_ProxyOld::mysql_result($result,0,"funds_adv"));
    $day2_user=Treat_DB_ProxyOld::mysql_result($result,0,"user");

    $day2_tot_petty_cash=money($day2_petty_cash+$day2_voucher+$day2_unreimbursed);
    $day2_tot_count=money($day2_change_fund+$day2_cash_in_trans+$day2_reg_bank+$day2_reg_bank2+$day2_other+$day2_funds_adv+$day2_tot_petty_cash);
    $day2_over_short=money($day2_tot_count-$bus_petty_cash);

    $query = "SELECT * FROM impress WHERE businessid = '$businessid' AND date = '$day3'";
    $result = Treat_DB_ProxyOld::query($query);

    $day3_petty_cash=money(Treat_DB_ProxyOld::mysql_result($result,0,"petty_cash"));
    $day3_voucher=money(Treat_DB_ProxyOld::mysql_result($result,0,"voucher"));
    $day3_unreimbursed=money(Treat_DB_ProxyOld::mysql_result($result,0,"unreimbursed"));
    $day3_change_fund=money(Treat_DB_ProxyOld::mysql_result($result,0,"change_fund"));
    $day3_cash_in_trans=money(Treat_DB_ProxyOld::mysql_result($result,0,"cash_in_trans"));
    $day3_reg_bank=money(Treat_DB_ProxyOld::mysql_result($result,0,"reg_bank"));
    $day3_reg_bank2=money(Treat_DB_ProxyOld::mysql_result($result,0,"reg_bank2"));
    $day3_other=money(Treat_DB_ProxyOld::mysql_result($result,0,"other"));
    $day3_funds_adv=money(Treat_DB_ProxyOld::mysql_result($result,0,"funds_adv"));
    $day3_user=Treat_DB_ProxyOld::mysql_result($result,0,"user");

    $day3_tot_petty_cash=money($day3_petty_cash+$day3_voucher+$day3_unreimbursed);
    $day3_tot_count=money($day3_change_fund+$day3_cash_in_trans+$day3_reg_bank+$day3_reg_bank2+$day3_other+$day3_funds_adv+$day3_tot_petty_cash);
    $day3_over_short=money($day3_tot_count-$bus_petty_cash);

    $query = "SELECT * FROM impress WHERE businessid = '$businessid' AND date = '$day4'";
    $result = Treat_DB_ProxyOld::query($query);

    $day4_petty_cash=money(Treat_DB_ProxyOld::mysql_result($result,0,"petty_cash"));
    $day4_voucher=money(Treat_DB_ProxyOld::mysql_result($result,0,"voucher"));
    $day4_unreimbursed=money(Treat_DB_ProxyOld::mysql_result($result,0,"unreimbursed"));
    $day4_change_fund=money(Treat_DB_ProxyOld::mysql_result($result,0,"change_fund"));
    $day4_cash_in_trans=money(Treat_DB_ProxyOld::mysql_result($result,0,"cash_in_trans"));
    $day4_reg_bank=money(Treat_DB_ProxyOld::mysql_result($result,0,"reg_bank"));
    $day4_reg_bank2=money(Treat_DB_ProxyOld::mysql_result($result,0,"reg_bank2"));
    $day4_other=money(Treat_DB_ProxyOld::mysql_result($result,0,"other"));
    $day4_funds_adv=money(Treat_DB_ProxyOld::mysql_result($result,0,"funds_adv"));
    $day4_user=Treat_DB_ProxyOld::mysql_result($result,0,"user");

    $day4_tot_petty_cash=money($day4_petty_cash+$day4_voucher+$day4_unreimbursed);
    $day4_tot_count=money($day4_change_fund+$day4_cash_in_trans+$day4_reg_bank+$day4_reg_bank2+$day4_other+$day4_funds_adv+$day4_tot_petty_cash);
    $day4_over_short=money($day4_tot_count-$bus_petty_cash);

    $query = "SELECT * FROM impress WHERE businessid = '$businessid' AND date = '$day5'";
    $result = Treat_DB_ProxyOld::query($query);

    $day5_petty_cash=money(Treat_DB_ProxyOld::mysql_result($result,0,"petty_cash"));
    $day5_voucher=money(Treat_DB_ProxyOld::mysql_result($result,0,"voucher"));
    $day5_unreimbursed=money(Treat_DB_ProxyOld::mysql_result($result,0,"unreimbursed"));
    $day5_change_fund=money(Treat_DB_ProxyOld::mysql_result($result,0,"change_fund"));
    $day5_cash_in_trans=money(Treat_DB_ProxyOld::mysql_result($result,0,"cash_in_trans"));
    $day5_reg_bank=money(Treat_DB_ProxyOld::mysql_result($result,0,"reg_bank"));
    $day5_reg_bank2=money(Treat_DB_ProxyOld::mysql_result($result,0,"reg_bank2"));
    $day5_other=money(Treat_DB_ProxyOld::mysql_result($result,0,"other"));
    $day5_funds_adv=money(Treat_DB_ProxyOld::mysql_result($result,0,"funds_adv"));
    $day5_user=Treat_DB_ProxyOld::mysql_result($result,0,"user");

    $day5_tot_petty_cash=money($day5_petty_cash+$day5_voucher+$day5_unreimbursed);
    $day5_tot_count=money($day5_change_fund+$day5_cash_in_trans+$day5_reg_bank+$day5_reg_bank2+$day5_other+$day5_funds_adv+$day5_tot_petty_cash);
    $day5_over_short=money($day5_tot_count-$bus_petty_cash);

    $query = "SELECT * FROM impress WHERE businessid = '$businessid' AND date = '$day6'";
    $result = Treat_DB_ProxyOld::query($query);

    $day6_petty_cash=money(Treat_DB_ProxyOld::mysql_result($result,0,"petty_cash"));
    $day6_voucher=money(Treat_DB_ProxyOld::mysql_result($result,0,"voucher"));
    $day6_unreimbursed=money(Treat_DB_ProxyOld::mysql_result($result,0,"unreimbursed"));
    $day6_change_fund=money(Treat_DB_ProxyOld::mysql_result($result,0,"change_fund"));
    $day6_cash_in_trans=money(Treat_DB_ProxyOld::mysql_result($result,0,"cash_in_trans"));
    $day6_reg_bank=money(Treat_DB_ProxyOld::mysql_result($result,0,"reg_bank"));
    $day6_reg_bank2=money(Treat_DB_ProxyOld::mysql_result($result,0,"reg_bank2"));
    $day6_other=money(Treat_DB_ProxyOld::mysql_result($result,0,"other"));
    $day6_funds_adv=money(Treat_DB_ProxyOld::mysql_result($result,0,"funds_adv"));
    $day6_user=Treat_DB_ProxyOld::mysql_result($result,0,"user");

    $day6_tot_petty_cash=money($day6_petty_cash+$day6_voucher+$day6_unreimbursed);
    $day6_tot_count=money($day6_change_fund+$day6_cash_in_trans+$day6_reg_bank+$day6_reg_bank2+$day6_other+$day6_funds_adv+$day6_tot_petty_cash);
    $day6_over_short=money($day6_tot_count-$bus_petty_cash);

    $query = "SELECT * FROM impress WHERE businessid = '$businessid' AND date = '$day7'";
    $result = Treat_DB_ProxyOld::query($query);

    $day7_petty_cash=money(Treat_DB_ProxyOld::mysql_result($result,0,"petty_cash"));
    $day7_voucher=money(Treat_DB_ProxyOld::mysql_result($result,0,"voucher"));
    $day7_unreimbursed=money(Treat_DB_ProxyOld::mysql_result($result,0,"unreimbursed"));
    $day7_change_fund=money(Treat_DB_ProxyOld::mysql_result($result,0,"change_fund"));
    $day7_cash_in_trans=money(Treat_DB_ProxyOld::mysql_result($result,0,"cash_in_trans"));
    $day7_reg_bank=money(Treat_DB_ProxyOld::mysql_result($result,0,"reg_bank"));
    $day7_reg_bank2=money(Treat_DB_ProxyOld::mysql_result($result,0,"reg_bank2"));
    $day7_other=money(Treat_DB_ProxyOld::mysql_result($result,0,"other"));
    $day7_funds_adv=money(Treat_DB_ProxyOld::mysql_result($result,0,"funds_adv"));
    $day7_user=Treat_DB_ProxyOld::mysql_result($result,0,"user");

    $day7_tot_petty_cash=money($day7_petty_cash+$day7_voucher+$day7_unreimbursed);
    $day7_tot_count=money($day7_change_fund+$day7_cash_in_trans+$day7_reg_bank+$day7_reg_bank2+$day7_other+$day7_funds_adv+$day7_tot_petty_cash);
    $day7_over_short=money($day7_tot_count-$bus_petty_cash);

    echo "<tr bgcolor=#E8E7E7 height=32><td>Petty Cash</td><td align=right><input type=text size=5 name=day1_petty_cash value='$day1_petty_cash' tabindex=1></td><td align=right><input type=text size=5 name=day2_petty_cash value='$day2_petty_cash' tabindex=2></td><td align=right><input type=text size=5 name=day3_petty_cash value='$day3_petty_cash' tabindex=3></td><td align=right><input type=text size=5 name=day4_petty_cash value='$day4_petty_cash' tabindex=4></td><td align=right><input type=text size=5 name=day5_petty_cash value='$day5_petty_cash' tabindex=5></td><td align=right><input type=text size=5 name=day6_petty_cash value='$day6_petty_cash' tabindex=6></td><td align=right><input type=text size=5 name=day7_petty_cash value='$day7_petty_cash' tabindex=7></td><td></td></tr>";
    echo "<tr bgcolor=#E8E7E7 height=32><td>Vouchers/Receipts</td><td align=right><input type=text size=5 name=day1_voucher value='$day1_voucher' tabindex=1></td><td align=right><input type=text size=5 name=day2_voucher value='$day2_voucher' tabindex=2></td><td align=right><input type=text size=5 name=day3_voucher value='$day3_voucher' tabindex=3></td><td align=right><input type=text size=5 name=day4_voucher value='$day4_voucher' tabindex=4></td><td align=right><input type=text size=5 name=day5_voucher value='$day5_voucher' tabindex=5></td><td align=right><input type=text size=5 name=day6_voucher value='$day6_voucher' tabindex=6></td><td align=right><input type=text size=5 name=day7_voucher value='$day7_voucher' tabindex=7></td><td></td></tr>";
    echo "<tr bgcolor=#E8E7E7 height=32><td>Unreimbursed Receipts Submitted<br>For Reimbursement</td><td align=right><input type=text size=5 name=day1_unreimbursed value='$day1_unreimbursed' tabindex=1></td><td align=right><input type=text size=5 name=day2_unreimbursed value='$day2_unreimbursed' tabindex=2></td><td align=right><input type=text size=5 name=day3_unreimbursed value='$day3_unreimbursed' tabindex=3></td><td align=right><input type=text size=5 name=day4_unreimbursed value='$day4_unreimbursed' tabindex=4></td><td align=right><input type=text size=5 name=day5_unreimbursed value='$day5_unreimbursed' tabindex=5></td><td align=right><input type=text size=5 name=day6_unreimbursed value='$day6_unreimbursed' tabindex=6></td><td align=right><input type=text size=5 name=day7_unreimbursed value='$day7_unreimbursed' tabindex=7></td><td></td></tr>";
    echo "<tr bgcolor=#CCCCCC><td>Sub-Total Petty Cash</td><td align=right>$$day1_tot_petty_cash</td><td align=right>$$day2_tot_petty_cash</td><td align=right>$$day3_tot_petty_cash</td><td align=right>$$day4_tot_petty_cash</td><td align=right>$$day5_tot_petty_cash</td><td align=right>$$day6_tot_petty_cash</td><td align=right>$$day7_tot_petty_cash</td><td></td></tr>";

    echo "<tr bgcolor=#E8E7E7 height=32><td>Change Fund</td><td align=right><input type=text size=5 name=day1_change_fund value='$day1_change_fund' tabindex=1></td><td align=right><input type=text size=5 name=day2_change_fund value='$day2_change_fund' tabindex=2></td><td align=right><input type=text size=5 name=day3_change_fund value='$day3_change_fund' tabindex=3></td><td align=right><input type=text size=5 name=day4_change_fund value='$day4_change_fund' tabindex=4></td><td align=right><input type=text size=5 name=day5_change_fund value='$day5_change_fund' tabindex=5></td><td align=right><input type=text size=5 name=day6_change_fund value='$day6_change_fund' tabindex=6></td><td align=right><input type=text size=5 name=day7_change_fund value='$day7_change_fund' tabindex=7></td><td></td></tr>";
    echo "<tr bgcolor=#E8E7E7 height=32><td>Cash in Transit - Change Orders</td><td align=right><input type=text size=5 name=day1_cash_in_trans value='$day1_cash_in_trans' tabindex=1></td><td align=right><input type=text size=5 name=day2_cash_in_trans value='$day2_cash_in_trans' tabindex=2></td><td align=right><input type=text size=5 name=day3_cash_in_trans value='$day3_cash_in_trans' tabindex=3></td><td align=right><input type=text size=5 name=day4_cash_in_trans value='$day4_cash_in_trans' tabindex=4></td><td align=right><input type=text size=5 name=day5_cash_in_trans value='$day5_cash_in_trans' tabindex=5></td><td align=right><input type=text size=5 name=day6_cash_in_trans value='$day6_cash_in_trans' tabindex=6></td><td align=right><input type=text size=5 name=day7_cash_in_trans value='$day7_cash_in_trans' tabindex=7></td><td></td></tr>";
    echo "<tr bgcolor=#E8E7E7 height=32><td>Register Banks Issued to Cashiers</td><td align=right><input type=text size=5 name=day1_reg_bank value='$day1_reg_bank' tabindex=1></td><td align=right><input type=text size=5 name=day2_reg_bank value='$day2_reg_bank' tabindex=2></td><td align=right><input type=text size=5 name=day3_reg_bank value='$day3_reg_bank' tabindex=3></td><td align=right><input type=text size=5 name=day4_reg_bank value='$day4_reg_bank' tabindex=4></td><td align=right><input type=text size=5 name=day5_reg_bank value='$day5_reg_bank' tabindex=5></td><td align=right><input type=text size=5 name=day6_reg_bank value='$day6_reg_bank' tabindex=6></td><td align=right><input type=text size=5 name=day7_reg_bank value='$day7_reg_bank' tabindex=7></td><td></td></tr>";
    echo "<tr bgcolor=#E8E7E7 height=32><td>Register Banks on Hand<br>(Not Issued to Cashiers)</td><td align=right><input type=text size=5 name=day1_reg_bank2 value='$day1_reg_bank2' tabindex=1></td><td align=right><input type=text size=5 name=day2_reg_bank2 value='$day2_reg_bank2' tabindex=2></td><td align=right><input type=text size=5 name=day3_reg_bank2 value='$day3_reg_bank2' tabindex=3></td><td align=right><input type=text size=5 name=day4_reg_bank2 value='$day4_reg_bank2' tabindex=4></td><td align=right><input type=text size=5 name=day5_reg_bank2 value='$day5_reg_bank2' tabindex=5></td><td align=right><input type=text size=5 name=day6_reg_bank2 value='$day6_reg_bank2' tabindex=6></td><td align=right><input type=text size=5 name=day7_reg_bank2 value='$day7_reg_bank2' tabindex=7></td><td></td></tr>";
    echo "<tr bgcolor=#E8E7E7 height=32><td>Other</td><td align=right><input type=text size=5 name=day1_other value='$day1_other' tabindex=1></td><td align=right><input type=text size=5 name=day2_other value='$day2_other' tabindex=2></td><td align=right><input type=text size=5 name=day3_other value='$day3_other' tabindex=3></td><td align=right><input type=text size=5 name=day4_other value='$day4_other' tabindex=4></td><td align=right><input type=text size=5 name=day5_other value='$day5_other' tabindex=5></td><td align=right><input type=text size=5 name=day6_other value='$day6_other' tabindex=6></td><td align=right><input type=text size=5 name=day7_other value='$day7_other' tabindex=7></td><td></td></tr>";
    echo "<tr bgcolor=#E8E7E7 height=32><td>Funds Advanced By Bank or Others</td><td align=right><input type=text size=5 name=day1_funds_adv value='$day1_funds_adv' tabindex=1></td><td align=right><input type=text size=5 name=day2_funds_adv value='$day2_funds_adv' tabindex=2></td><td align=right><input type=text size=5 name=day3_funds_adv value='$day3_funds_adv' tabindex=3></td><td align=right><input type=text size=5 name=day4_funds_adv value='$day4_funds_adv' tabindex=4></td><td align=right><input type=text size=5 name=day5_funds_adv value='$day5_funds_adv' tabindex=5></td><td align=right><input type=text size=5 name=day6_funds_adv value='$day6_funds_adv' tabindex=6></td><td align=right><input type=text size=5 name=day7_funds_adv value='$day7_funds_adv' tabindex=7></td><td></td></tr>";
    echo "<tr bgcolor=#CCCCCC><td>Total Count</td><td align=right>$$day1_tot_count</td><td align=right>$$day2_tot_count</td><td align=right>$$day3_tot_count</td><td align=right>$$day4_tot_count</td><td align=right>$$day5_tot_count</td><td align=right>$$day6_tot_count</td><td align=right>$$day7_tot_count</td><td></td></tr>";

    echo "<tr bgcolor=#E8E7E7 height=32><td>Assigned Bank Fund<br>by Accounting</td><td align=right>$$bus_petty_cash</td><td align=right>$$bus_petty_cash</td><td align=right>$$bus_petty_cash</td><td align=right>$$bus_petty_cash</td><td align=right>$$bus_petty_cash</td><td align=right>$$bus_petty_cash</td><td align=right>$$bus_petty_cash</td><td></td></tr>";
    echo "<tr bgcolor=yellow><td>Over/Short</td><td align=right>$$day1_over_short</td><td align=right>$$day2_over_short</td><td align=right>$$day3_over_short</td><td align=right>$$day4_over_short</td><td align=right>$$day5_over_short</td><td align=right>$$day6_over_short</td><td align=right>$$day7_over_short</td><td></td></tr>";
    echo "<tr bgcolor=#E8E7E7><td><input type=submit value='Save'></td><td align=right><font size=2>$day1_user</td><td align=right><font size=2>$day2_user</td><td align=right><font size=2>$day3_user</td><td align=right><font size=2>$day4_user</td><td align=right><font size=2>$day5_user</td><td align=right><font size=2>$day6_user</td><td align=right><font size=2>$day7_user</td><td></form></td></tr>";

    echo "</table></center>";

//////END TABLE////////////////////////////////////////////////////////////////////////////////////

    echo "<br><FORM ACTION=businesstrack.php method=post>";
    echo "<input type=hidden name=username value=$user>";
    echo "<input type=hidden name=password value=$pass>";
    echo "<center><INPUT TYPE=submit VALUE=' Return to Main Page'></FORM></center><DIV ID=testdiv1 STYLE=position:absolute;visibility:hidden;background-color:white;layer-background-color:white;></DIV></body>";

	google_page_track();
}
//mysql_close();
?>