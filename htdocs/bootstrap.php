<?php
# TODO this file is deprecated (LEGACY code), remove this from as many files as you can
if( !defined( 'LEGACY' ) ) {
	define( 'LEGACY', true );
}

require_once( realpath( dirname(__FILE__) . '/../application/bootstrap.php' ) );
if( LEGACY ) {
	if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
		trigger_error(
			'The file you are viewing has required the legacy bootstrap which introduces lots of problems.
			Also be aware that this means the "db.php" file has also been required which can also be a problem
			when moving forward in fixing code.
			Please require the "application/bootstrap.php" file instead of "htdocs/bootstrap.php". If this
			error message has caused your AJAX stuff to break then fix your AJAX stuff to either
			A) include the correct bootstrap.php or
			B) stop using the deprecated DB ProxyOld crap that was supposed to be replaced starting in 2010.'
			,E_USER_WARNING
		);
	}
}

if(!defined('DOC_ROOT')) exit('DOC_ROOT not defined. Check your config');
#set_include_path(realpath(DOC_ROOT.DIRECTORY_SEPARATOR.'../lib/').PATH_SEPARATOR.get_include_path());
if(!defined('IN_PRODUCTION')) {
	define('IN_PRODUCTION', (bool) getenv('IN_PRODUCTION'));
}

if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
	# TODO WTF is this? We're supposed to be cleaning this code up, not leaving it a pile of shit
#	error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
	ini_set('display_errors', 1);
}

if ( !isset( $_SESSION ) && 'cli' != php_sapi_name() ) {
	session_start();
}
if ( !defined( 'UPLOADS_DIRECTORY' ) ) {
	define( 'UPLOADS_DIRECTORY', DOC_ROOT . DIRECTORY_SEPARATOR . 'uploads' );
}
if ( !defined( 'ROUTE_EXPORTS_DIRECTORY' ) ) {
	define( 'ROUTE_EXPORTS_DIRECTORY', DOC_ROOT . '/ta/exports' );
}


if(!function_exists('debug')) {
	function debug($message, $sess = FALSE, $req= FALSE){
		if(!IN_PRODUCTION){
			$contents = '<div style="background-color:FFE0E0; border: 1px solid red; font"><pre>';
			echo print_r($message, 1)."\r\n";
			if($sess){
				$contents .= ($_SESSION);
			}
			if($req) {
				$contents .= ($_REQUEST);
			}
			$contents .= '</pre></div>';
			}
		return $contents;
	}
}
if ( LEGACY ) {
	require_once('common.php');
}
# bypass common.php so db.php is not included
# @TODO fix all the code that's still using DB_Proxy. Nothing should be using DB_Proxy.
else {
	require_once( 'inc/functions.php' );
	#require_once( 'inc/db.php' );
	require_once( 'inc/class.db-proxy.php' );
	include_once( 'inc/utils.php' );
}

# @TODO fix all of the code so this crap is no longer needed.
// The way the htdocs/bootstrap.php is currently set up it should load the
// SiTech bootstrap which should load Treat_Config. 
if ( class_exists( '\EE\Config' ) ) {
	$config = \EE\Config::singleton()->getSection( 'db' );
	if ( array_key_exists( 'host', $config ) ) {
		$GLOBALS['dbhost'] = $dbhost = current( (array)$config['host'] );
	}
	if ( array_key_exists( 'username', $config ) ) {
		$GLOBALS['dbuser'] = $username = $dbuser = $config['username'];
	}
	if ( array_key_exists( 'password', $config ) ) {
		$GLOBALS['dbpass'] = $password = $dbpass = $config['password'];
	}
	if ( array_key_exists( 'database', $config ) ) {
		$GLOBALS['dbname'] = $database = $dbname = $config['database'];
	}
}
