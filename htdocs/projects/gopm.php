<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once(DOC_ROOT.DIRECTORY_SEPARATOR.'bootstrap.php');
$goto = isset($_GET['goto'])?$_GET['goto']:'';

if($goto==1){
	$ta = sha1('TreatAmerica');
	$server = (IN_PRODUCTION)?'essential-elements.net':
	(($_SERVER['SERVER_NAME'] == 'treat.localhost')?'ee-website.localhost':'ee-website.dev.essential-elements.net');
	echo "<head><META HTTP-EQUIV='refresh' CONTENT='0;URL=http://{$server}/login.php?ta={$ta}'></head>";

	echo "<body></body>";
}
