<?php

include_once("../ta/db.php");
$style = "text-decoration:none";

function dateDiff($startDate, $endDate){
   $days = (strtotime($endDate) - strtotime($startDate)) / (60 * 60 * 24);

   return round($days,0);
} 

function nextday($nextd,$day_format=''){ //Function returns next day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monn = substr($nextd, 5, 2);
   $dayn = substr($nextd, 8, 2);
   $yearn = substr($nextd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monn, $dayn+1, $yearn));
   return $tempdate;
}

function prevday($prevd,$day_format=''){ //Function returns previous day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monp = substr($prevd, 5, 2);
   $dayp = substr($prevd, 8, 2);
   $yearp = substr($prevd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monp, $dayp-1, $yearp));
   return $tempdate;
}

function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}

function drilldown($pm_id,$pm_level,$current_level,$pm_detailid,$security){
       /////////////DRILL DOWN
       $today=date("Y-m-d");
       $twoweeks=$today;
       for($counter=1;$counter<=13;$counter++){$twoweeks=prevday($twoweeks);} 

       $query2 = "SELECT * FROM login WHERE oo8 > '0' ORDER BY username DESC";
       $result2 = mysql_query($query2);
       $num2=mysql_numrows($result2);
       
       $queryb = "SELECT * FROM pm_detail WHERE pm_id = '$pm_detailid' AND level = '$current_level' ORDER BY propose_date DESC, pm_detailid DESC";
       $resultb = mysql_query($queryb);
       $numb=mysql_numrows($resultb);

       $current_level++;
       $numb--;
       while($numb>=0){
          $is_disabled2="";

          //$pm_id=@mysql_result($resultb,$num,"pm_id");
          $pm_level=@mysql_result($resultb,$numb,"level");
          $pm_detailid=@mysql_result($resultb,$numb,"pm_detailid");
          $detail=@mysql_result($resultb,$numb,"detail");
          $status=@mysql_result($resultb,$numb,"status");
          $propose_date=@mysql_result($resultb,$numb,"propose_date");
          $date=@mysql_result($resultb,$numb,"date");
          $assign_to=@mysql_result($resultb,$numb,"assign_to");
          $hours=@mysql_result($resultb,$numb,"hours");
          $eoc=@mysql_result($resultb,$numb,"eoc");

          $has_subs="0000-00-00";
          $has_subs=findsubs($pm_detailid,$pm_level);

          if($has_subs>"0000-00-00"){
             $propose_date=$has_subs;
             $disableme="DISABLED";
          }
          else{$disableme="";}

          if($status==1){$showdays=dateDiff($propose_date,$date); if($showdays>0){$showdays="(+$showdays days)";} else{$showdays="($showdays days)";}}
          else{$showdays=dateDiff($propose_date,$today); if($showdays>0){$showdays="(+$showdays days)";} else{$showdays="($showdays days)";}}

          if($status==1){$showflag="<img src=flagblue.gif>";}
          elseif($propose_date>$today){$showflag="<img src=flaggreen.gif>";}
          elseif($propose_date>=$twoweeks){$showflag="<img src=flagyellow.gif>";}
          elseif($propose_date<$twoweeks){$showflag="<img src=flagred.gif>";}
          else{$showflag="";}

          if($security>0||$status==0){$showdelete="<a href=pm_edit_detail.php?pm_id=$pm_id&pm_detailid=$pm_detailid&goto=3 onclick='return deldetail()'><img src=delete.gif height=16 width=16 border=0 alt='DELETE'></a>"; $showadd="<a href=pm_edit_detail.php?pm_id=$pm_id&pm_detailid=$pm_detailid&goto=1&level=$current_level><img src=plus.gif height=16 width=16 border=0 alt='Add Sub Category'></a>";}
          else{$showdelete="";$is_disabled2="DISABLED";}

          $tothours=$tothours+$hours;

          if ($date=="0000-00-00"){$date="";}

          if ($status==1){$complete="CHECKED";$color="#E9E9E9";}
          else{$complete="";$color="white";}

          if ($eoc==1){$showeoc="CHECKED";}
          else{$showeoc="";}

          $numtab="";
          for($counter=1;$counter<$current_level;$counter++){$numtab.="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";}

          echo "<tr bgcolor=$color onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor='$color'><td colspan=2>$numtab<input type=hidden name='oldstatus:$pm_detailid' value=$status><input type=checkbox name='status:$pm_detailid' value=1 $complete $disableme onclick=\"saveme.style.backgroundColor='#CC3333'\">&nbsp;&nbsp;<input type=text size=80 value='$detail' name='detail:$pm_detailid' onchange=\"saveme.style.backgroundColor='#CC3333'\"> $showdelete $showadd</td><td align=right>$showdays</td><td align=right>$showflag</td><td align=right><SCRIPT LANGUAGE='JavaScript' ID='js$pm_detailid'> var cal$pm_detailid = new CalendarPopup('testdiv1');cal$pm_detailid.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=propose_date_$pm_detailid VALUE='$propose_date' SIZE=8 $disableme onchange=\"saveme.style.backgroundColor='#CC3333'\"> <A HREF=\"javascript:void(0)\" onClick=\"cal$pm_detailid.select(document.forms[1].propose_date_$pm_detailid,'anchor$pm_detailid','yyyy-MM-dd'); saveme.style.backgroundColor='#CC3333';\" return false; TITLE=cal$pm_detailid.select(document.forms[1].propose_date_$pm_detailid,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor$pm_detailid' ID='anchor$pm_detailid'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A></td><td align=right><input type=text size=8 name='date:$pm_detailid' value='$date' $disableme onchange=\"saveme.style.backgroundColor='#CC3333'\"></td><td align=right><select name='assign:$pm_detailid' $is_disabled onchange=\"saveme.style.backgroundColor='#CC3333'\">";

          $temp_num=$num2-1;
          while($temp_num>=0){
             $userid=@mysql_result($result2,$temp_num,"loginid");
             $assign_to2=@mysql_result($result2,$temp_num,"username");
             if($userid==$assign_to){$sel3="SELECTED";}
             else{$sel3="";}

             echo "<option value=$userid $sel3>$assign_to2</option>";

             $temp_num--;
          }
          echo "</select></td></tr>";

          drilldown($pm_id,$pm_level,$current_level,$pm_detailid,$security);
       
          $numb--;
       }
       $current_level--;
       /////////////END DRILL DOWN
}

function findpercent($pm_id,$pm_level,$current_level,$pm_detailid,$security,$whichpercent){
          $today=date("Y-m-d"); 
          $twoweeks=$today;
          for($counter=1;$counter<=13;$counter++){$twoweeks=prevday($twoweeks);} 

          $current_level++;

          $query3 = "SELECT * FROM pm_detail WHERE pm_id = '$pm_detailid' AND level = '$current_level' ORDER BY propose_date DESC, pm_detailid DESC";
          $result3 = mysql_query($query3);
          $num3=mysql_numrows($result3);

          $num3--;
          while($num3>=0){

             $pm_detailid=@mysql_result($result3,$num3,"pm_detailid");
             $detail=@mysql_result($result3,$num3,"detail");
             $status=@mysql_result($result3,$num3,"status");
             $propose_date=@mysql_result($result3,$num3,"propose_date");

             $newpercent = explode("/",$whichpercent);

             if($status==1){$newpercent[0]++;$newpercent[1]++;}
             else{$newpercent[1]++;}

             $whichpercent="$newpercent[0]/$newpercent[1]";

             $whichpercent = findpercent($pm_id,$pm_level,$current_level,$pm_detailid,$security,$whichpercent);

             $num3--;
          }

          return $whichpercent;
}

function findsubs($pm_detailid,$pm_level){
   
   $pm_level++;

   $query3 = "SELECT * FROM pm_detail WHERE pm_id = '$pm_detailid' AND level = '$pm_level' ORDER BY propose_date DESC LIMIT 0,1";
   $result3 = mysql_query($query3);
   $num3=mysql_numrows($result3);

   if($num3>0){
      $has_subs=@mysql_result($result3,0,"propose_date");
      $new_detailid=@mysql_result($result3,0,"pm_detailid");
      $new_level=@mysql_result($result3,0,"pm_level");

      $new_level++;
      $query3 = "SELECT * FROM pm_detail WHERE pm_id = '$new_detailid' AND level = '$new_level' ORDER BY propose_date DESC LIMIT 0,1";
      $result3 = mysql_query($query3);
      $num3=mysql_numrows($result3);
      $new_level--;

      if($num3>0){$has_subs=findsubs($new_detailid,$new_level);}
   }

   return $has_subs;
}

$user = isset($_COOKIE["usercook"])?$_COOKIE["usercook"]:'';
$pass = isset($_COOKIE["passcook"])?$_COOKIE["passcook"]:'';
$pm_id = isset($_GET["pm_id"])?$_GET["pm_id"]:'';

if($pm_id==""){$pm_id=$_POST["pm_id"];$edit = isset($_POST["edit"])?$_POST["edit"]:'';}

$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";

mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die(mysql_error());

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass' AND oo8 > '0'";
$result = mysql_query($query);
$num=mysql_numrows($result);

$security=@mysql_result($result,0,"oo8");

if ($num != 1 || $user == "" || $pass == "")
{
    echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}
else
{
?>
<head>
<SCRIPT LANGUAGE="JavaScript" SRC="CalendarPopup.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">document.write(getCalendarStyles());</SCRIPT>

<STYLE>
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation
			{
			background-color:#6677DD;
			text-align:center;
			vertical-align:center;
			text-decoration:none;
			color:#FFFFFF;
			font-weight:bold;
			}
	.TESTcpDayColumnHeader,
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation,
	.TESTcpCurrentMonthDate,
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDate,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDate,
	.TESTcpCurrentDateDisabled,
	.TESTcpTodayText,
	.TESTcpTodayTextDisabled,
	.TESTcpText
			{
			font-family:arial;
			font-size:8pt;
			}
	TD.TESTcpDayColumnHeader
			{
			text-align:right;
			border:solid thin #6677DD;
			border-width:0 0 1 0;
			}
	.TESTcpCurrentMonthDate,
	.TESTcpOtherMonthDate,
	.TESTcpCurrentDate
			{
			text-align:right;
			text-decoration:none;
			}
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDateDisabled
			{
			color:#D0D0D0;
			text-align:right;
			text-decoration:line-through;
			}
	.TESTcpCurrentMonthDate
			{
			color:#6677DD;
			font-weight:bold;
			}
	.TESTcpCurrentDate
			{
			color: #FFFFFF;
			font-weight:bold;
			}
	.TESTcpOtherMonthDate
			{
			color:#808080;
			}
	TD.TESTcpCurrentDate
			{
			color:#FFFFFF;
			background-color: #6677DD;
			border-width:1;
			border:solid thin #000000;
			}
	TD.TESTcpCurrentDateDisabled
			{
			border-width:1;
			border:solid thin #FFAAAA;
			}
	TD.TESTcpTodayText,
	TD.TESTcpTodayTextDisabled
			{
			border:solid thin #6677DD;
			border-width:1 0 0 0;
			}
	A.TESTcpTodayText,
	SPAN.TESTcpTodayTextDisabled
			{
			height:20px;
			}
	A.TESTcpTodayText
			{
			color:#6677DD;
			font-weight:bold;
			}
	SPAN.TESTcpTodayTextDisabled
			{
			color:#D0D0D0;
			}
	.TESTcpBorder
			{
			border:solid thin #6677DD;
			}
</STYLE>

<SCRIPT LANGUAGE=javascript><!--
function deldetail(){return confirm('Are you sure you want to delete this?  ALL sub-tasks will be lost!');}
// --></SCRIPT>

<SCRIPT LANGUAGE=javascript><!--
function deljob(){return confirm('Are you sure you want to delete this Project?  It is recommended you change the status to Cancelled to keep track of ideas. Press OK to continue.');}
// --></SCRIPT>

</head>
<?
    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=100% style=\"background-image: url(backdrop.jpg);background-repeat: no-repeat;\"><tr><td colspan=2><a href=../ta/businesstrack.php><img src=talogo.gif border=0></a><p></td></tr></table>";

    //echo "<table width=100% cellspacing=0 cellpadding=0 style=\"border: 1px solid black;\">";
    //echo "<tr bgcolor=#CCCCFF><td colspan=4><font size=4><b>Project Details</b></font></td></tr>"; 
    //echo "</table><p>";

    $current_level=1;

    $query = "SELECT * FROM pm_list WHERE pm_id = '$pm_id'";
    $result = mysql_query($query);

    $pm_job=@mysql_result($result,0,"pm_job");
    $pm_status=@mysql_result($result,0,"status");
    $pm_category=@mysql_result($result,0,"category");
	$pm_division=@mysql_result($result,0,"divisionid");
    $priority=@mysql_result($result,0,"priority");
    $old_pm_status=$pm_status;
    $q_order=@mysql_result($result,0,"q_order");
    $pm_date=@mysql_result($result,0,"date");
    $pm_complete_date=@mysql_result($result,0,"complete_date");
    $accountid=@mysql_result($result,0,"accountid");
    $assign_to=@mysql_result($result,0,"assign_to");
    $details=@mysql_result($result,0,"pm_detail");
    $eoc=@mysql_result($result,0,"eoc");
    $pm_eoc=$eoc;

    $assign_me=$assign_to;

    if($security==0){$is_disabled="DISABLED";}
    else{$is_disabled="";}

    if($edit==1){

       echo "<center><table width=100% cellspacing=0 cellpadding=0 bgcolor=#E8E7E7 style=\"border: 1px solid black;\">";

       if($security>0){$deletejob="<a href=pm_edit.php?pm_id=$pm_id&goto=3 onclick='return deljob()'><img src=delete.gif height=16 width=16 border=0 alt='DELETE'></a>";}

       if($eoc==1){$showeoc="CHECKED";}

       echo "<tr valign=top><td align=right width=15%><form action=pm_edit.php method=post><input type=hidden name=goto value=2><input type=hidden name=pm_id value='$pm_id'><b>Project:&nbsp;</td><td width=25%><input type=text name=pm_job value='$pm_job' size=30 tabindex=1 maxlength=125> $deletejob</td><td align=right width=15%><b>Status:&nbsp;</td><td width=25%><select name=pm_status tabindex=4>";

       $query2 = "SELECT * FROM pm_status ORDER BY pm_statusname DESC";
       $result2 = mysql_query($query2);
       $num2=mysql_numrows($result2);
 
       $num2--;
       while($num2>=0){
          $statusid=@mysql_result($result2,$num2,"pm_statusid");
          $status_name=@mysql_result($result2,$num2,"pm_statusname");
          if($pm_status==$statusid){$sel1="SELECTED";}
          else{$sel1="";}

          echo "<option value=$statusid $sel1>$status_name</option>";

          $num2--;
       }

       echo "</select></td><td rowspan=3 align=right width=1%><input type=submit value='Save' tabindex=7></td></tr>";
       echo "<tr valign=top><td align=right><b>Assign To:&nbsp;</td><td><select name=assign_to tabindex=3 $is_disabled>";

       $query2 = "SELECT * FROM login WHERE oo8 > '0' ORDER BY username DESC";
       $result2 = mysql_query($query2);
       $num2=mysql_numrows($result2);
 
       while($num2>=0){
          $userid=@mysql_result($result2,$num2,"loginid");
          $assign_to2=@mysql_result($result2,$num2,"username");
          if($userid==$assign_to){$sel3="SELECTED";}
          else{$sel3="";}

          echo "<option value=$userid $sel3>$assign_to2</option>";
  
          $num2--;
       }

       echo "</select></td><td align=right><b>Start Date:&nbsp;</td><td><SCRIPT LANGUAGE='JavaScript' ID='js01'> var cal01 = new CalendarPopup('testdiv1');cal01.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=pm_date VALUE='$pm_date' SIZE=8 tabindex=5> <A HREF=\"javascript:void(0)\" onClick=cal01.select(document.forms[0].pm_date,'anchor01','yyyy-MM-dd'); return false; TITLE=cal01.select(document.forms[0].pm_date,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor01' ID='anchor01'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A> </td></tr>";
       echo "<tr valign=top><td align=right><b>Priority:&nbsp;</td><td><select name=priority>";

       if($priority==1){$ch1="SELECTED";}
       elseif($priority==2){$ch2="SELECTED";}
       elseif($priority==3){$ch3="SELECTED";}

       echo "<option value=1 $ch1>High</option><option value=2 $ch2>Med</option><option value=3 $ch3>Low</option>";

       echo "</select></td><td align=right><b>Accountability Date:&nbsp;</td><td><SCRIPT LANGUAGE='JavaScript' ID='js02'> var cal02 = new CalendarPopup('testdiv1');cal02.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=complete_date VALUE='$pm_complete_date' SIZE=8 tabindex=5> <A HREF=\"javascript:void(0)\" onClick=cal01.select(document.forms[0].complete_date,'anchor02','yyyy-MM-dd'); return false; TITLE=cal01.select(document.forms[0].complete_date,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor02' ID='anchor02'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A></td></tr>";
       echo "<tr valign=top><td align=right><b>Divsion/Category:&nbsp;</td><td>";
	   
	   ////division
	   echo "<select name=pm_division>";

       $query2 = "SELECT * FROM pm_division ORDER BY division_name DESC";
       $result2 = mysql_query($query2);
       $num2=mysql_numrows($result2);
 
       while($num2>=0){
          $divisionid=@mysql_result($result2,$num2,"divisionid");
          $division_name=@mysql_result($result2,$num2,"division_name");
          if($divisionid==$pm_division){$sel3="SELECTED";}
          else{$sel3="";}

          echo "<option value=$divisionid $sel3>$division_name</option>";
  
          $num2--;
       }
	   
	   ////category
	   echo "</select>&nbsp;/&nbsp;<select name=pm_category>";

       $query2 = "SELECT * FROM pm_category ORDER BY category_name DESC";
       $result2 = mysql_query($query2);
       $num2=mysql_numrows($result2);
 
       while($num2>=0){
          $categoryid=@mysql_result($result2,$num2,"categoryid");
          $category_name=@mysql_result($result2,$num2,"category_name");
          if($categoryid==$pm_category){$sel3="SELECTED";}
          else{$sel3="";}

          echo "<option value=$categoryid $sel3>$category_name</option>";
  
          $num2--;
       }

       echo "</select></td>";
       echo "<td align=right><b>Details:&nbsp;</td><td><textarea name=details rows=4 cols=32 tabindex=6>$details</textarea></td></tr>";
       echo "</table></form></center><p>";
    }
    else{
       $current_level=0;
       $whichpercent="0/0";
       $whichpercent = findpercent($pm_id,$pm_level,$current_level,$pm_id,$security,$whichpercent);

       $newpercent=explode("/",$whichpercent);
       $completion = round($newpercent[0]/$newpercent[1]*100,0);
       $completion = "($completion%)";

       $details=str_replace("\n","<br>",$details);

       $query = "SELECT * FROM pm_status WHERE pm_statusid = '$pm_status'";
       $result = mysql_query($query);

       $pm_status=@mysql_result($result,0,"pm_statusname");
       $color=@mysql_result($result,0,"color");
       $pm_status="<font color=#$color><b>$pm_status $completion</b></font>";

       $query = "SELECT * FROM pm_accounts WHERE accountid = '$accountid'";
       $result = mysql_query($query);

       $account_name=@mysql_result($result,0,"account_name");

       $query = "SELECT * FROM pm_category WHERE categoryid = '$pm_category'";
       $result = mysql_query($query);

       $category_name=@mysql_result($result,0,"category_name");
	   
	   $query = "SELECT division_name FROM pm_division WHERE divisionid = '$pm_division'";
       $result = mysql_query($query);

       $division_name=@mysql_result($result,0,"division_name");
	   
	   if($division_name==""){$division_name="<i>Unassigned</i>";}

       $query = "SELECT * FROM login WHERE loginid = '$assign_to'";
       $result = mysql_query($query);

       $assign_to=@mysql_result($result,0,"username");

       if($priority==1){$showpriority="High";}
       elseif($priority==2){$showpriority="Med";}
       elseif($priority==3){$showpriority="Low";}

       if($security<8){$editdisable="DISABLED";}

       echo "<center><table width=100% cellspacing=0 cellpadding=0 bgcolor=#E8E7E7 style=\"border: 1px solid black;\">";
       echo "<tr valign=top><td align=right width=15%><b>Project:&nbsp;</td><td width=30%><u>$pm_job</u></td><td align=right width=20%><b>Status:&nbsp;</td><td width=25%>$pm_status</td><td rowspan=3 align=right><form action=pm_details.php method=post><input type=hidden name=pm_id value='$pm_id'><input type=hidden name=edit value=1><input type=submit value='Edit' $editdisable></form></td></tr>";
       echo "<tr valign=top><td align=right><b>Assign To:&nbsp;</td><td>$assign_to</td><td align=right><b>Start Date:&nbsp;</td><td>$pm_date</td></tr>";
       echo "<tr valign=top><td align=right><b>Priority:&nbsp;</td><td>$showpriority</td><td align=right><b>Accountability Date:&nbsp;</td><td>$pm_complete_date</td></tr>";
       echo "<tr valign=top><td align=right><b>Division/Category:&nbsp;</td><td>$division_name / $category_name</td><td align=right><b>Details:&nbsp;</td><td>$details</td></tr>";
       echo "</table></center><p>";
    }

//////////////////DETAILS
    echo "<center><table width=100% cellspacing=0 cellpadding=0 style=\"border: 1px solid black;\">";
    echo "<tr bgcolor=#FFFF99><td width=5%><form action=pm_edit_detail.php method=post><input type=hidden name=pm_eoc value=$pm_eoc><input type=hidden name=old_pm_status value=$old_pm_status><input type=hidden name=pm_id value=$pm_id><input type=hidden name=goto value=2><center></td><td width=52%><b>Specifics</td><td align=right width=9%><b>Days+/-</td><td width=7% align=right><b>Flag</td><td width=12% align=right><b>Accountability Date</td><td width=8% align=right><b>Finished</td><td align=right width=8%><b>Assign To</td></tr>";
    echo "<tr bgcolor=black height=1><td colspan=7></td></tr>";

    $tothours=0;
    $current_level=1;

    $query2 = "SELECT * FROM login WHERE oo8 > '0' ORDER BY username DESC";
    $result2 = mysql_query($query2);
    $num2=mysql_numrows($result2);

    $query = "SELECT * FROM pm_detail WHERE pm_id = '$pm_id' AND level = '$current_level' ORDER BY propose_date DESC, pm_detailid DESC";
    $result = mysql_query($query);
    $num=mysql_numrows($result);

    if ($num==0){echo "<tr bgcolor=white><td colspan=7><i>None</td></tr>";}

    $today=date("Y-m-d");
    $twoweeks=$today;
    for($counter=1;$counter<=13;$counter++){$twoweeks=prevday($twoweeks);} 
    $current_level++;
    $num--;
    while($num>=0){
       $is_disabled2="";

       $pm_id=@mysql_result($result,$num,"pm_id");
       $pm_level=@mysql_result($result,$num,"level");
       $pm_detailid=@mysql_result($result,$num,"pm_detailid");
       $detail=@mysql_result($result,$num,"detail");
       $status=@mysql_result($result,$num,"status");
       $propose_date=@mysql_result($result,$num,"propose_date");
       $date=@mysql_result($result,$num,"date");
       $assign_to=@mysql_result($result,$num,"assign_to");
       $hours=@mysql_result($result,$num,"hours");
       $eoc=@mysql_result($result,$num,"eoc");

       $has_subs="0000-00-00";
       $has_subs=findsubs($pm_detailid,$pm_level);

       if($has_subs>"0000-00-00"){
          $propose_date=$has_subs;
          $disableme="DISABLED";
       }
       else{$disableme="";}

       if($status==1){$showdays=dateDiff($propose_date,$date); if($showdays>0){$showdays="(+$showdays days)";} else{$showdays="($showdays days)";}}
       else{$showdays=dateDiff($propose_date,$today); if($showdays>0){$showdays="(+$showdays days)";} else{$showdays="($showdays days)";}}

       if($status==1){$showflag="<img src=flagblue.gif>";}
       elseif($propose_date>$today){$showflag="<img src=flaggreen.gif>";}
       elseif($propose_date>=$twoweeks){$showflag="<img src=flagyellow.gif>";}
       elseif($propose_date<$twoweeks){$showflag="<img src=flagred.gif>";}
       else{$showflag="";}
       
       if($security>0||$status==0){$showdelete="<a href=pm_edit_detail.php?pm_id=$pm_id&pm_detailid=$pm_detailid&goto=3 onclick='return deldetail()'><img src=delete.gif height=16 width=16 border=0 alt='DELETE'></a>"; $showadd="<a href=pm_edit_detail.php?pm_id=$pm_id&pm_detailid=$pm_detailid&goto=1&level=$current_level><img src=plus.gif height=16 width=16 border=0 alt='Add Sub Category'></a>";}
       else{$showdelete="";$is_disabled2="DISABLED";}

       $tothours=$tothours+$hours;

       if ($date=="0000-00-00"){$date="";}

       if ($status==1){$complete="CHECKED";$color="#E9E9E9";}
       else{$complete="";$color="white";}

       if ($eoc==1){$showeoc="CHECKED";}
       else{$showeoc="";}

       echo "<tr bgcolor=$color onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor='$color'><td colspan=2><input type=hidden name='oldstatus:$pm_detailid' value=$status>&nbsp;&nbsp;&nbsp;&nbsp;<input type=checkbox style='font-size: 24px;' name='status:$pm_detailid' value=1 $complete $disableme onclick=\"saveme.style.backgroundColor='#CC3333'\">&nbsp;&nbsp;<input type=text size=80 value='$detail' name='detail:$pm_detailid' onchange=\"saveme.style.backgroundColor='#CC3333'\"> $showdelete $showadd</td><td align=right>$showdays</td><td align=right>$showflag</td><td align=right><input type=hidden name='oldeoc:$pm_detailid' value=$eoc><SCRIPT LANGUAGE='JavaScript' ID='js$pm_detailid'> var cal$pm_detailid = new CalendarPopup('testdiv1');cal$pm_detailid.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=propose_date_$pm_detailid VALUE='$propose_date' SIZE=8 $disableme onchange=\"saveme.style.backgroundColor='#CC3333'\"> <A HREF=\"javascript:void(0)\" onClick=\"cal$pm_detailid.select(document.forms[1].propose_date_$pm_detailid,'anchor$pm_detailid','yyyy-MM-dd'); saveme.style.backgroundColor='#CC3333';\" return false; TITLE=cal$pm_detailid.select(document.forms[1].propose_date_$pm_detailid,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor$pm_detailid' ID='anchor$pm_detailid'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A></td><td align=right><input type=text size=8 name='date:$pm_detailid' value='$date' $disableme onchange=\"saveme.style.backgroundColor='#CC3333'\"></td><td align=right><select name='assign:$pm_detailid' $is_disabled onchange=\"saveme.style.backgroundColor='#CC3333'\">";

       $temp_num=$num2-1;
       while($temp_num>=0){
          $userid=@mysql_result($result2,$temp_num,"loginid");
          $assign_to2=@mysql_result($result2,$temp_num,"username");
          if($userid==$assign_to){$sel3="SELECTED";}
          else{$sel3="";}

          echo "<option value=$userid $sel3>$assign_to2</option>";

          $temp_num--;
       }
       echo "</select></td></tr>";

       drilldown($pm_id,$pm_level,$current_level,$pm_detailid,$security);

       $num--;
    }
    echo "<tr bgcolor=black height=1><td colspan=7></td></tr>";
    echo "<tr bgcolor=#FFFF99 height=16><td colspan=4 align=right></td><td colspan=3 align=right><input id='saveme' type=submit value='Save' style=\"border: 1px solid #CCCCFF; font-size: 14px; font-color: white; background-color:#E8E7E7;\"></td></tr>";
    echo "</table></center></form><p>";

/////////////////ADD DETAILS
    echo "<center><table width=100% cellspacing=0 cellpadding=0 bgcolor=#E8E7E7 style=\"border: 1px solid black;\">";
    echo "<tr><td><form action=pm_edit_detail.php method=post><input type=hidden name=pm_id value=$pm_id><input type=hidden name=assign_to value=$assign_me><input type=hidden name=goto value=1> New Specific: <input type=text name=details size=50> <input type=submit value='Add'></td><td></form></td></tr></table></center><p>";

/////////////////COMMENTS
    $today=date("Y-m-d");
    $yesterday=prevday($today);

    echo "<a name=comment><center><table width=100% cellspacing=0 cellpadding=0 style=\"border: 1px solid black;\">";
    echo "<tr bgcolor=#FFFF99><td width=70%><b>Comment</b></td><td align=right width=15%><b>User</td><td align=right width=15%><b>Time</td></tr>";
    echo "<tr bgcolor=black height=1><td colspan=3></td></tr>";

    $query = "SELECT * FROM pm_comment WHERE pm_id = '$pm_id' ORDER BY time DESC";
    $result = mysql_query($query);
    $num=mysql_numrows($result);

    if($num==0){echo "<tr><td colspan=3><font size=2><i>Nothing to Display</td></tr>";}

    while($r=mysql_fetch_array($result)){
       $id =  $r["id"];
       $comment = $r["comment"];
       $time = $r["time"];
       $showuser = $r["user"];

       if(substr($time,0,10)==$today){$dayname="<font color=blue>Today</font>";}
       elseif(substr($time,0,10)==$yesterday){$dayname="<font color=green>Yesterday</font>";}
       else{$dayname=dayofweek(substr($time,0,10));}

       $year=substr($time,2,2);
       $month=substr($time,5,2);
       $day=substr($time,8,2);

       $hour=substr($time,11,2);
       $min=substr($time,14,2);

       if($hour>=1 && $hour<=11){$am="am";}
       elseif($hour>=12 && $hour<=23){$am="pm";}
       else{$am="am";}

       if($hour==0){$hour=1;}
       if($hour>12){$hour=$hour-12;};

       $comment=str_replace("\n","<br>",$comment);

       echo "<tr onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor='white' valign=top><td><font size=2><li>$comment</td><td align=right><font size=2>$showuser</td><td align=right><font size=2>$dayname $month/$day/$year $hour:$min$am</td></tr>";
       echo "<tr bgcolor=#E8E7E7 height=1><td colspan=3></td></tr>";
    }

    echo "<tr bgcolor=black height=1><td colspan=3></td></tr>";
    echo "<tr bgcolor=#FFFF99 height=16><td colspan=3><form action=pm_comment.php method=post style=\"margin:0;padding:0;display:inline;\"><input type=hidden name=pm_id value=$pm_id><textarea name=comment cols=80 rows=3></textarea><input type=submit value='Add Comment'></form></td></tr></table></center>";

//////////////////END FORM
    echo "<center><form action=pm_list.php method=post><input type=submit value='Return' tabindex=8></form></center></a>";

    echo "<p><br><p><br><center><img src=logo.jpg>";

    mysql_close();

    echo "<DIV ID=testdiv1 STYLE=position:absolute;visibility:hidden;background-color:white;layer-background-color:white;></DIV><body>";

}
?>