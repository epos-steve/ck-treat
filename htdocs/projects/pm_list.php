<?php

include_once("../ta/db.php");
require('../ta/lib/class.db-proxy.php');
$style = "text-decoration:none";

function dateDiff($startDate, $endDate){
   $days = (strtotime($endDate) - strtotime($startDate)) / (60 * 60 * 24);

   return round($days,0);
} 

function nextday($nextd,$day_format=''){ //Function returns next day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monn = substr($nextd, 5, 2);
   $dayn = substr($nextd, 8, 2);
   $yearn = substr($nextd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monn, $dayn+1, $yearn));
   return $tempdate;
}

function prevday($prevd,$day_format=''){ //Function returns previous day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monp = substr($prevd, 5, 2);
   $dayp = substr($prevd, 8, 2);
   $yearp = substr($prevd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monp, $dayp-1, $yearp));
   return $tempdate;
}

function drilldown($pm_id,$pm_level,$current_level,$pm_detailid,$security,$showuser){
          $today=date("Y-m-d"); 
          $twoweeks=$today;
          for($counter=1;$counter<=13;$counter++){$twoweeks=prevday($twoweeks);} 

          $query3 = "SELECT * FROM pm_detail WHERE pm_id = '$pm_detailid' AND level = '$current_level' ORDER BY propose_date DESC, pm_detailid DESC";
          $result3 = DB_Proxy::query($query3);
          $num3=mysql_numrows($result3);

          $current_level++;
          $num3--;
          while($num3>=0){

             $pm_detailid=@mysql_result($result3,$num3,"pm_detailid");
             $pm_level=@mysql_result($result3,$num3,"level");
             $detail=@mysql_result($result3,$num3,"detail");
             $status=@mysql_result($result3,$num3,"status");
             $propose_date=@mysql_result($result3,$num3,"propose_date");
             $date=@mysql_result($result3,$num3,"date");
             $hours=@mysql_result($result3,$num3,"hours");
             $assign_to2=@mysql_result($result3,$num3,"assign_to");
             $assign_to3=$assign_to2;

             $has_subs="0000-00-00";
             $has_subs=findsubs($pm_detailid,$pm_level);

             if($has_subs>"0000-00-00"){
                $propose_date=$has_subs;
                $disableme="DISABLED";
             }
             else{$disableme="";}

                $query4 = "SELECT * FROM login WHERE loginid = '$assign_to2'";
                $result4 = DB_Proxy::query($query4);

                $assign_to2=@mysql_result($result4,0,"username");


             //if($status==1){$shhours=" - $hours hours";}
             //else{$shhours="";}

             //$myflag=dateDiff($propose_date,$date); if($myflag>0){$myflag="(+$myflag days)";} else{$myflag="($myflag days)";}

             if($status==1){$showdays=dateDiff($propose_date,$date); if($showdays>0){$showdays="(+$showdays days)";} else{$showdays="($showdays days)";}}
             else{$showdays=dateDiff($propose_date,$today); if($showdays>0){$showdays="(+$showdays days)";} else{$showdays="($showdays days)";}}

             if($status==1){$myflag="<img src=flagblue2.gif>";}
             elseif($propose_date>$today){$myflag="<img src=flaggreen2.gif>";}
             elseif($propose_date>=$twoweeks){$myflag="<img src=flagyellow2.gif>";}
             else{$myflag="<img src=flagred2.gif>";}

             if($status==1&&$date>=$date1&&$date<=$date2&&$datetype==1&&($showassign==$assign_to3||$showassign==0)){$status="<img src=done.gif>";$fontcolor="red";$redhours+=$hours;}
             elseif($status==1){$status="<img src=done.gif>";$fontcolor="#999999";}
             else{$status="<img src=working.gif>";$fontcolor="black";}

             if($date=="0000-00-00"){$date="";}
             else{$date="($date)";}

             $tothours=$tothours+$hours;

             $numtab="";
             for($counter=1;$counter<$current_level;$counter++){$numtab.="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";}

             if($showuser==0||$showuser==$assign_to3){echo "<tr onMouseOver=this.bgColor='yellow' onMouseOut=this.bgColor='#FFFFFF'><td colspan=3><font size=2 color=$fontcolor><b>$numtab$status $detail</td><td align=right colspan=2><font size=2 color=$fontcolor>$propose_date</td><td align=right><font size=2 color=$fontcolor>$showdays</td><td align=right><font size=2 color=$fontcolor>$myflag</td><td></td><td align=right><font size=2 color=$fontcolor>$assign_to2</td></tr>";}

             drilldown($pm_id,$pm_level,$current_level,$pm_detailid,$security,$showuser);

             $num3--;
          }
}

function findflag($pm_id,$pm_level,$current_level,$pm_detailid,$security,$whichflag){
          $today=date("Y-m-d"); 
          $twoweeks=$today;
          for($counter=1;$counter<=13;$counter++){$twoweeks=prevday($twoweeks);} 

          $current_level++;

          $query3 = "SELECT * FROM pm_detail WHERE pm_id = '$pm_detailid' AND level = '$current_level' ORDER BY propose_date DESC, pm_detailid DESC";
          $result3 = DB_Proxy::query($query3);
          $num3=mysql_numrows($result3);

          $num3--;
          while($num3>=0){

             $pm_detailid=@mysql_result($result3,$num3,"pm_detailid");
             $detail=@mysql_result($result3,$num3,"detail");
             $status=@mysql_result($result3,$num3,"status");
             $propose_date=@mysql_result($result3,$num3,"propose_date");

             if($status==1){}
             elseif($propose_date<$twoweeks&&$whichflag<3){$whichflag=3;}
             elseif($propose_date>=$twoweeks&&$propose_date<=$today&&$whichflag<2){$whichflag=2;}
             elseif($propose_date>$today&&$whichflag<1){$whichflag=1;}

             $newflag = findflag($pm_id,$pm_level,$current_level,$pm_detailid,$security,$whichflag);

             if($newflag>$whichflag){$whichflag=$newflag;}

             $num3--;
          }

          return $whichflag;
}

function findpercent($pm_id,$pm_level,$current_level,$pm_detailid,$security,$whichpercent){
          $today=date("Y-m-d"); 
          $twoweeks=$today;
          for($counter=1;$counter<=13;$counter++){$twoweeks=prevday($twoweeks);} 

          $current_level++;

          $query3 = "SELECT * FROM pm_detail WHERE pm_id = '$pm_detailid' AND level = '$current_level' ORDER BY propose_date DESC, pm_detailid DESC";
          $result3 = DB_Proxy::query($query3);
          $num3=mysql_numrows($result3);

          $num3--;
          while($num3>=0){

             $pm_detailid=@mysql_result($result3,$num3,"pm_detailid");
             $detail=@mysql_result($result3,$num3,"detail");
             $status=@mysql_result($result3,$num3,"status");
             $propose_date=@mysql_result($result3,$num3,"propose_date");

             $newpercent = explode("/",$whichpercent);

             if($status==1){$newpercent[0]++;$newpercent[1]++;}
             else{$newpercent[1]++;}

             $whichpercent="$newpercent[0]/$newpercent[1]";

             $whichpercent = findpercent($pm_id,$pm_level,$current_level,$pm_detailid,$security,$whichpercent);

             $num3--;
          }

          return $whichpercent;
}

function findsubs($pm_detailid,$pm_level){
   
   $pm_level++;

   $query3 = "SELECT * FROM pm_detail WHERE pm_id = '$pm_detailid' AND level = '$pm_level' ORDER BY propose_date DESC LIMIT 0,1";
   $result3 = DB_Proxy::query($query3);
   $num3=mysql_numrows($result3);

   if($num3>0){
      $has_subs=@mysql_result($result3,0,"propose_date");
      $new_detailid=@mysql_result($result3,0,"pm_detailid");
      $new_level=@mysql_result($result3,0,"pm_level");

      $new_level++;
      $query3 = "SELECT * FROM pm_detail WHERE pm_id = '$new_detailid' AND level = '$new_level' ORDER BY propose_date DESC LIMIT 0,1";
      $result3 = DB_Proxy::query($query3);
      $num3=mysql_numrows($result3);
      $new_level--;

      if($num3>0){$has_subs=findsubs($new_detailid,$new_level);}
   }

   return $has_subs;
}

function acct_date($pm_id,$pm_level,$current_level,$pm_detailid,$security,$pm_complete_date){

          $current_level++;

          $query3 = "SELECT * FROM pm_detail WHERE pm_id = '$pm_detailid' AND level = '$current_level' ORDER BY propose_date DESC, pm_detailid DESC";
          $result3 = DB_Proxy::query($query3);
          $num3=mysql_numrows($result3);

          $num3--;
          while($num3>=0){

             $pm_detailid=@mysql_result($result3,$num3,"pm_detailid");
             $detail=@mysql_result($result3,$num3,"detail");
             $status=@mysql_result($result3,$num3,"status");
             $propose_date=@mysql_result($result3,$num3,"propose_date");

             if($propose_date>$pm_complete_date){$reddate=$propose_date;}

             $newflag = findflag($pm_id,$pm_level,$current_level,$pm_detailid,$security,$pm_complete_date);

             if($newflag>$reddate){$reddate=$newflag;}

             $num3--;
          }

          return $reddate;
}

function finduser($pm_detailid,$pm_level,$showuser,$hasuser){
   
   $pm_level++;

   $query3 = "SELECT * FROM pm_detail WHERE pm_id = '$pm_detailid' AND level = '$pm_level'";
   $result3 = DB_Proxy::query($query3);
   $num3=mysql_numrows($result3);

   if($num3>0){
      $new_detailid=@mysql_result($result3,0,"pm_detailid");
      $new_level=@mysql_result($result3,0,"pm_level");
      $assign_to=@mysql_result($result3,0,"assign_to");

      if($assign_to==$showuser){$hasuser=1;}

      $new_level++;

      if($hasuser==0){
         $hasuser=finduser($new_detailid,$new_level,$showuser,0);
      }
   }

   return $hasuser;
}

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die(mysql_error());

$user = isset($_POST["username"])?$_POST["username"]:'';
$pass = isset($_POST["password"])?$_POST["password"]:'';
$pm_status = isset($_POST["pm_status"])?$_POST["pm_status"]:'';
$pm_category = isset($_POST["pm_category"])?$_POST["pm_category"]:'';
$pm_division = isset($_POST["pm_division"])?$_POST["pm_division"]:'';
$datetype = isset($_POST["datetype"])?$_POST["datetype"]:'';
$date1 = isset($_POST["date1"])?$_POST["date1"]:'';
$date2 = isset($_POST["date2"])?$_POST["date2"]:'';
$assign_to = isset($_POST["assign_to"])?$_POST["assign_to"]:'';
$accountid = isset($_POST["accountid"])?$_POST["accountid"]:'';
$showorder = isset($_POST["showorder"])?$_POST["showorder"]:'';
$showpost = isset($_POST["showpost"])?$_POST["showpost"]:'';
$showdetails = isset($_POST["showdetails"])?$_POST["showdetails"]:'';

if($assign_to>0){$showdetails=1;}

if($user==""||$pass==""){
   $user = isset($_COOKIE["usercook"])?$_COOKIE["usercook"]:'';
   $pass = isset($_COOKIE["passcook"])?$_COOKIE["passcook"]:'';
}


if($showpost==""){
   $datetype = isset($_COOKIE["datetype"])?$_COOKIE["datetype"]:'';
   $date1 = isset($_COOKIE["date1"])?$_COOKIE["date1"]:'';
   $date2 = isset($_COOKIE["date2"])?$_COOKIE["date2"]:'';
   $pm_status = isset($_COOKIE["pm_status"])?$_COOKIE["pm_status"]:'';
   $pm_category = isset($_COOKIE["pm_category"])?$_COOKIE["pm_category"]:'';
   $pm_division = isset($_COOKIE["pm_division"])?$_COOKIE["pm_division"]:'';
   $assign_to = isset($_COOKIE["assign_to"])?$_COOKIE["assign_to"]:'';
   $accountid = isset($_COOKIE["accountid"])?$_COOKIE["accountid"]:'';
   $showorder = isset($_COOKIE["showorder"])?$_COOKIE["showorder"]:'';
   $showdetails = isset($_COOKIE["showdetails"])?$_COOKIE["showdetails"]:'';
   $companyid = isset($_GET["cid"])?$_GET["cid"]:'';
}

$newlogin = isset($_GET["loginid"])?$_GET["loginid"]:'';
$editmes = isset($_GET["editmes"])?$_GET["editmes"]:'';

if($newlogin>0){
   $query = "SELECT username,password FROM login WHERE loginid = '$newlogin' AND oo8 > '0'";
   $result = DB_Proxy::query($query);

   $user=@mysql_result($result,0,"username");
   $pass=@mysql_result($result,0,"password");
}

$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass' AND oo8 > '0'";
$result = DB_Proxy::query($query);
$num=mysql_numrows($result);

$user=@mysql_result($result,0,"username");
$security=@mysql_result($result,0,"oo8");
$loginid=@mysql_result($result,0,"loginid");
if($security==0){$accountid=@mysql_result($result,0,"accountid");}

setcookie("usercook",$user);
setcookie("passcook",$pass);
if($accountid>0&&$security==0){setcookie("accountid",$accountid);}

if($datetype==""){$datetype=0;}
if($pm_status==""&&$security==0){$pm_status=0;}
elseif($pm_status==""){$pm_status=1;}
if($date1==""||$date1=="0000-00-00"){$date1="0000-00-00";$showdate1="";}
else{$showdate1=$date1;}
if($date2==""||$date2=="9999-99-99"){$date2="9999-99-99";$showdate2="";}
else{$showdate2=$date2;}
if($accountid==""){$accountid=0;}
if($assign_to==""){$assign_to=0;}

setcookie("pm_status",$pm_status);
setcookie("pm_category",$pm_category);
setcookie("pm_division",$pm_division);
setcookie("datetype",$datetype);
setcookie("date1",$date1);
setcookie("date2",$date2);
setcookie("assign_to",$assign_to);
setcookie("accountid",$accountid);
setcookie("showorder",$showorder);
setcookie("showdetails",$showdetails);

$showassign=$assign_to;

if ($num != 1 || $user == "" || $pass == "")
{
    echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}
else
{
?>
<head>

<script type="text/javascript" src="prototype.js"></script>
<script src="sorttable.js"></script>
<script type="text/javascript" src="javascripts/scriptaculous.js"></script>

<STYLE>
#loading {
 	width: 200px;
 	height: 48px;
 	background-color: ;
 	position: absolute;
 	left: 50%;
 	top: 50%;
 	margin-top: -50px;
 	margin-left: -100px;
 	text-align: center;
}
</STYLE>

<script type="text/javascript" src="preLoadingMessage.js"></script>

<SCRIPT LANGUAGE="JavaScript" SRC="CalendarPopup.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">document.write(getCalendarStyles());</SCRIPT>

<STYLE>
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation
			{
			background-color:#6677DD;
			text-align:center;
			vertical-align:center;
			text-decoration:none;
			color:#FFFFFF;
			font-weight:bold;
			}
	.TESTcpDayColumnHeader,
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation,
	.TESTcpCurrentMonthDate,
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDate,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDate,
	.TESTcpCurrentDateDisabled,
	.TESTcpTodayText,
	.TESTcpTodayTextDisabled,
	.TESTcpText
			{
			font-family:arial;
			font-size:8pt;
			}
	TD.TESTcpDayColumnHeader
			{
			text-align:right;
			border:solid thin #6677DD;
			border-width:0 0 1 0;
			}
	.TESTcpCurrentMonthDate,
	.TESTcpOtherMonthDate,
	.TESTcpCurrentDate
			{
			text-align:right;
			text-decoration:none;
			}
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDateDisabled
			{
			color:#D0D0D0;
			text-align:right;
			text-decoration:line-through;
			}
	.TESTcpCurrentMonthDate
			{
			color:#6677DD;
			font-weight:bold;
			}
	.TESTcpCurrentDate
			{
			color: #FFFFFF;
			font-weight:bold;
			}
	.TESTcpOtherMonthDate
			{
			color:#808080;
			}
	TD.TESTcpCurrentDate
			{
			color:#FFFFFF;
			background-color: #6677DD;
			border-width:1;
			border:solid thin #000000;
			}
	TD.TESTcpCurrentDateDisabled
			{
			border-width:1;
			border:solid thin #FFAAAA;
			}
	TD.TESTcpTodayText,
	TD.TESTcpTodayTextDisabled
			{
			border:solid thin #6677DD;
			border-width:1 0 0 0;
			}
	A.TESTcpTodayText,
	SPAN.TESTcpTodayTextDisabled
			{
			height:20px;
			}
	A.TESTcpTodayText
			{
			color:#6677DD;
			font-weight:bold;
			}
	SPAN.TESTcpTodayTextDisabled
			{
			color:#D0D0D0;
			}
	.TESTcpBorder
			{
			border:solid thin #6677DD;
			}
</STYLE>

</head>
<?
    echo "<body><center><table cellspacing=0 cellpadding=0 border=0 width=100% style=\"background-image: url(backdrop.jpg);background-repeat: no-repeat;border: 1px solid black;\">";

    $query2 = "SELECT * FROM pm_message";
    $result2 = DB_Proxy::query($query2);

    $pm_message=@mysql_result($result2,0,"message");

    if($editmes==1){
       echo "<tr valign=top><td width=5%><a href=../ta/businesstrack.php><img src=talogo.gif border=0 align=top></a></td><td width=95%><form action=pm_message.php method=post style=\"margin:0;padding:0;display:inline;\"><textarea name=pm_message rows=10 cols=125>$pm_message</textarea><input type=submit value='Save'></form>";
    }
    elseif($security > 8){
       echo "<tr valign=top><td width=5%><a href=pm_list.php?editmes=1><img src=talogo.gif border=0 align=top></a></td><td width=95%>$pm_message";
    }
    else{
       echo "<tr valign=top><td width=5%><a href=../ta/businesstrack.php><img src=talogo.gif border=0 align=top></a></td><td width=95%>$pm_message";
    }

    echo "</td></tr></table></center><p>";

//////FILTER
    $showuser=$assign_to;

    echo "<p><center><table width=100% cellspacing=0 cellpadding=0 bgcolor=#E8E7E7 style=\"border: 1px solid black;\">";

    echo "<tr><td colspan=4><form action=pm_list.php method=post style=\"margin:0;padding:0;display:inline;\"><input type=hidden name=showpost value=1>";

    if($datetype==1){$datesel="SELECTED";}

    echo "<font size=2>&nbsp;Filter: ";
    echo "<SCRIPT LANGUAGE='JavaScript' ID='js18'> var cal18 = new CalendarPopup('testdiv1');cal18.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date1 VALUE='$showdate1' SIZE=8 STYLE='font-size: 10px;'> <A HREF=\"javascript:void(0)\" onClick=cal18.select(document.forms[0].date1,'anchor18','yyyy-MM-dd'); return false; TITLE=cal18.select(document.forms[0].date1,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor18' ID='anchor18'><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A> to ";
    echo "<SCRIPT LANGUAGE='JavaScript' ID='js19'> var cal19 = new CalendarPopup('testdiv1');cal19.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date2 VALUE='$showdate2' SIZE=8 STYLE='font-size: 10px;'> <A HREF=\"javascript:void(0)\" onClick=cal19.select(document.forms[0].date2,'anchor19','yyyy-MM-dd'); return false; TITLE=cal19.select(document.forms[0].date2,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor19' ID='anchor19'><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A> ";

    $query2 = "SELECT * FROM login WHERE oo8 > '0' ORDER BY username DESC";
    $result2 = DB_Proxy::query($query2);
    $num2=mysql_numrows($result2);

    if ($assign_to==-1){$showcheck1="SELECTED";}

    echo " <select name=assign_to STYLE='font-size: 10px;'><option value=0>All Users</option><option value=-1 $showcheck1>Noone</option>";
    $num2--;
    while($num2>=0){
       $userid=@mysql_result($result2,$num2,"loginid");
       $assign_to2=@mysql_result($result2,$num2,"username");
       if($assign_to==$userid){$showcheck2="SELECTED";}
       else{$showcheck2="";}

       echo "<option value=$userid $showcheck2>$assign_to2</option>";

       $num2--;
    }
    echo "</select> <select name=pm_status STYLE='font-size: 10px;'><option value=0>All</option>"; 

    $query2 = "SELECT * FROM pm_status ORDER BY pm_statusname DESC";
    $result2 = DB_Proxy::query($query2);
    $num2=mysql_numrows($result2);
 
    $num2--;
    while($num2>=0){
       $statusid=@mysql_result($result2,$num2,"pm_statusid");
       $status_name=@mysql_result($result2,$num2,"pm_statusname");
       if ($pm_status==$statusid){$showcheck3="SELECTED";}
       else{$showcheck3="";}

       echo "<option value=$statusid $showcheck3>$status_name</option>";

       $num2--;
    }
    echo "</select> <select name=pm_division STYLE='font-size: 10px;'><option value=0>All Divisions</option>";
	
	$query2 = "SELECT * FROM pm_division ORDER BY division_name DESC";
    $result2 = DB_Proxy::query($query2);
    $num2=mysql_numrows($result2);
 
    $num2--;
    while($num2>=0){
       $divisionid=@mysql_result($result2,$num2,"divisionid");
       $division_name=@mysql_result($result2,$num2,"division_name");
       if ($pm_division==$divisionid){$showcheck3="SELECTED";}
       else{$showcheck3="";}

       echo "<option value=$divisionid $showcheck3>$division_name</option>";

       $num2--;
    }

	echo "</select> <select name=pm_category STYLE='font-size: 10px;'><option value=0>All Categories</option>";  

    $query2 = "SELECT * FROM pm_category ORDER BY category_name DESC";
    $result2 = DB_Proxy::query($query2);
    $num2=mysql_numrows($result2);
 
    $num2--;
    while($num2>=0){
       $categoryid=@mysql_result($result2,$num2,"categoryid");
       $category_name=@mysql_result($result2,$num2,"category_name");
       if ($pm_category==$categoryid){$showcheck3="SELECTED";}
       else{$showcheck3="";}

       echo "<option value=$categoryid $showcheck3>$category_name</option>";

       $num2--;
    }
    echo "</select> "; 

    if($showorder=="date"){$sel1="SELECTED";}
    elseif($showorder=="status"){$sel2="SELECTED";}
    elseif($showorder=="assign_to"){$sel3="SELECTED";}
    elseif($showorder=="accountid"){$sel4="SELECTED";}
    elseif($showorder=="priority"){$sel5="SELECTED";}
	elseif($showorder=="divisionid"){$sel8="SELECTED";}
    elseif($showorder=="category"){$sel6="SELECTED";}
    elseif($showorder=="complete_date"){$sel7="SELECTED";}
    else{$showorder="status";$sel2="SELECTED";}

    if($showdetails==1){$detcheck="CHECKED";}

    echo " <select name=showorder STYLE='font-size: 10px;'><option value=>SORT BY...</option><option value='date' $sel1>Start Date</option><option value='complete_date' $sel7>Accountability Date</option><option value='status' $sel2>Status</option><option value='assign_to' $sel3>Assign To</option><option value='priority' $sel5>Priority</option><option value='divisionid' $sel8>Division</option><option value='category' $sel6>Category</option></select> <input type=checkbox name=showdetails value=1 $detcheck STYLE='font-size: 10px;'>Specifics <input type=submit value='Display' STYLE='font-size: 10px;'></form></td><td align=right>";

	if($security>8){echo "<a href=pm_settings.php style=$style><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Settings</font></a> | ";}
    if($security>7){echo "<a href=pm_users.php style=$style><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>Manage Users</font></a> | <a href='javascript:void(0)' onclick=\"Effect.SlideDown('navigation', { duration: 1.0 }); return false;\" style=$style><font color=blue onMouseOver=this.color='#FF9900' onMouseOut=this.color='blue'>New Project</font></a>";}

    echo "&nbsp;</td></tr>";

    ////////NEW PROJECT
    echo "<tr><td colspan=5><div id='navigation' style=\"display:none; width:100%; background:#E8E7E7; border:2px solid #E8E7E7;\">";
    /////////////////////////ADD NEW JOBS///////////////////////////////

    echo "<center><table width=100% bgcolor=#FFFF99 style=\"border: 1px solid black;\"><tr valign=top><td align=right><form action=pm_edit.php method=post style=\"margin:0;padding:0;display:inline;\"><input type=hidden name=goto value=1>";
    echo "Start Date: </td><td><SCRIPT LANGUAGE='JavaScript' ID='js21'> var cal21 = new CalendarPopup('testdiv1');cal21.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=date VALUE='$today' SIZE=8 tabindex=1> <A HREF=\"javascript:void(0)\" onClick=cal21.select(document.forms[1].date,'anchor21','yyyy-MM-dd'); return false; TITLE=cal21.select(document.forms[1].date,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor21' ID='anchor21'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A></td><td align=right rowspan=5>Details:</td><td rowspan=6><textarea name=details rows=7 cols=40 tabindex=6></textarea></td></tr>";
    echo "<tr><td align=right>Accountability Date: </td><td><SCRIPT LANGUAGE='JavaScript' ID='js22'> var cal22 = new CalendarPopup('testdiv1');cal22.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=complete_date VALUE='$today' SIZE=8 tabindex=1> <A HREF=\"javascript:void(0)\" onClick=cal22.select(document.forms[1].complete_date,'anchor22','yyyy-MM-dd'); return false; TITLE=cal22.select(document.forms[1].complete_date,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor22' ID='anchor22'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A></td></tr>";

    echo "<tr valign=top><td align=right>Status: </td><td><select name=pm_status tabindex=2>";

    $query2 = "SELECT * FROM pm_status ORDER BY pm_statusname DESC";
    $result2 = DB_Proxy::query($query2);
    $num2=mysql_numrows($result2);
 
    $num2--;
    while($num2>=0){
       $statusid=@mysql_result($result2,$num2,"pm_statusid");
       $status_name=@mysql_result($result2,$num2,"pm_statusname");
       if($statusid==1){$sel="SELECTED";}
       else{$sel="";}

       echo "<option value=$statusid $sel>$status_name</option>";

       $num2--;
    }
    echo "</select></td></tr>";

    echo "<tr valign=top><td align=right>Assigned To: </td><td><select name=assign_to tabindex=4>";

    $query2 = "SELECT * FROM login WHERE oo8 > '0' ORDER BY username DESC";
    $result2 = DB_Proxy::query($query2);
    $num2=mysql_numrows($result2);
 
    while($num2>=0){
       $userid=@mysql_result($result2,$num2,"loginid");
       $assign_to=@mysql_result($result2,$num2,"username");
       if ($assign_to==$user){$sel="SELECTED";}
       else{$sel="";}

       echo "<option value=$userid $sel>$assign_to</option>";

       $num2--;
    }
    echo "</select></td></tr>";

    echo "<tr valign=top><td align=right>Project:</td><td><input type=text size=50 name=pm_job tabindex=5 maxlength=125></td></tr>";
    echo "<tr valign=top><td align=right>Priority:</td><td><select name=priority><option value=1>High</option><option value=2>Med</option><option value=3>Low</option></select></td></tr>";
    //echo "<tr valign=top valign=top><td align=right>Details:</td><td><textarea name=details rows=8 cols=40></textarea></td></tr>";
    echo "<tr valign=top><td align=right>Division/Category:</td><td>";
	
	////division
	echo "<select name=pm_division>";

    $query2 = "SELECT * FROM pm_division ORDER BY division_name DESC";
    $result2 = DB_Proxy::query($query2);
    $num2=mysql_numrows($result2);
 
    while($num2>=0){
       $divisionid=@mysql_result($result2,$num2,"divisionid");
       $division_name=@mysql_result($result2,$num2,"division_name");

       echo "<option value=$divisionid>$division_name</option>";

       $num2--;
    }
	
	////category
	echo "</select>&nbsp;/&nbsp;<select name=pm_category>";

    $query2 = "SELECT * FROM pm_category ORDER BY category_name DESC";
    $result2 = DB_Proxy::query($query2);
    $num2=mysql_numrows($result2);
 
    while($num2>=0){
       $categoryid=@mysql_result($result2,$num2,"categoryid");
       $category_name=@mysql_result($result2,$num2,"category_name");

       echo "<option value=$categoryid>$category_name</option>";

       $num2--;
    }

    echo "</select></td><td colspan=2 align=right><input type=submit value='Add New Project' tabindex=7></td></tr>";
    echo "<tr valign=top><td colspan=4></form></td></tr></table></center>";

    echo "</div></td></tr>";
    echo "</table>";
    echo "<p>";

    echo "<center><table width=100% cellspacing=0 cellpadding=0 style=\"border: 1px solid black; font-size: 12px;\" class=\"sortable\">";
    echo "<thead><tr bgcolor=#FFFF99><th width=45%><b>Project</th><th><b>Division</th><th><b>Category</th><th><b>Start Date</th><th><b>Accountability Date</th><th><b>Priority</th><th><b></th><th align=right><b>Flag</th><th align=right><b>Task Status</th><th align=right><b>Assign To</th></tr></thead>";
    //echo "<tr height=1 bgcolor=black><th colspan=10></th></tr>";
	echo "</thead>";

/////////////////////PROJECT MANAGER/////////////////////////////////

    $sheethours=0;
    $sheetredhours=0;
	
	///////////TABLES
	if($security<8){$fromtables="pm_list,pm_detail,pm_access";}
	else{$fromtables="pm_list,pm_detail";}

    ///////////QUERIES
    if($datetype==1){
       if($assign_to>0){$myquery="SELECT distinct pm_list.pm_id,pm_list.pm_job,pm_list.pm_detail,pm_list.status,pm_list.category,pm_list.divisionid,pm_list.priority,pm_list.q_order,pm_list.date,pm_list.accountid,pm_list.complete_date,pm_list.assign_to FROM $fromtables WHERE pm_detail.date >= '$date1' AND pm_detail.date <= '$date2' AND pm_detail.pm_id = pm_list.pm_id AND pm_detail.status = '1'";}
       else{$myquery="SELECT distinct pm_list.pm_id,pm_list.pm_job,pm_list.pm_detail,pm_list.status,pm_list.category,pm_list.divisionid,pm_list.priority,pm_list.q_order,pm_list.date,pm_list.accountid,pm_list.complete_date,pm_list.assign_to FROM $fromtables WHERE pm_detail.date >= '$date1' AND pm_detail.date <= '$date2' AND pm_detail.pm_id = pm_list.pm_id AND pm_detail.status = '1'";}
    }
    else{
       if($assign_to>0){$myquery="SELECT distinct pm_list.pm_id,pm_list.pm_job,pm_list.pm_detail,pm_list.status,pm_list.category,pm_list.divisionid,pm_list.priority,pm_list.q_order,pm_list.date,pm_list.accountid,pm_list.complete_date,pm_list.assign_to FROM $fromtables WHERE pm_list.date >= '$date1' AND pm_list.date <= '$date2'";}
       else{$myquery="SELECT distinct pm_list.pm_id,pm_list.pm_job,pm_list.pm_detail,pm_list.status,pm_list.category,pm_list.divisionid,pm_list.priority,pm_list.q_order,pm_list.date,pm_list.accountid,pm_list.complete_date,pm_list.assign_to FROM $fromtables WHERE pm_list.date >= '$date1' AND pm_list.date <= '$date2'";}
    }
    
    //if($assign_to>0&&$datetype==1){$myquery="$myquery AND pm_detail.assign_to = '$assign_to'";}
    //elseif($assign_to==0&&$datetype==1){}
    //elseif($assign_to==-1){$myquery="$myquery AND pm_list.assign_to = '0'";}
    //elseif($assign_to>0&&$datetype!=1){$myquery="$myquery AND pm_list.assign_to = '$assign_to'";}

    if($accountid>0){$myquery="$myquery AND pm_list.accountid = '$accountid'";}

    if($pm_status>0){$myquery="$myquery AND pm_list.status = '$pm_status'";}

	if($pm_division>0){$myquery="$myquery AND pm_list.divisionid = '$pm_division'";}
	
    if($pm_category>0){$myquery="$myquery AND pm_list.category = '$pm_category'";}

    if($security<8){$myquery="$myquery AND pm_list.pm_id = pm_access.jobid AND pm_access.loginid = '$loginid'";}

    $myquery="$myquery ORDER BY pm_list.$showorder DESC, pm_list.status DESC, pm_list.q_order DESC";

    $result = DB_Proxy::query($myquery);
    $num=mysql_numrows($result);

    //echo "$myquery<br>";
    //////////END QUERIES 

	echo "<tbody>";
    if ($num==0){echo "<tr><td colspan=8><i>Nothing to Display</td></tr>";}

    $counter=0;
    $num--;
    while($num>=0){
       $pm_id=@mysql_result($result,$num,"pm_id");
       $pm_job=@mysql_result($result,$num,"pm_job");
       $pm_detail2=@mysql_result($result,$num,"pm_detail");
       $pm_status=@mysql_result($result,$num,"status");
       $priority=@mysql_result($result,$num,"priority");
       $q_order=@mysql_result($result,$num,"q_order");
       $pm_date=@mysql_result($result,$num,"date");
       $pm_complete_date=@mysql_result($result,$num,"complete_date");
       $pm_accountid=@mysql_result($result,$num,"accountid");
       $assign_to=@mysql_result($result,$num,"assign_to");
       $pm_category=@mysql_result($result,$num,"category");
	   $pm_division=@mysql_result($result,$num,"divisionid");
       $assign=$assign_to;

       /////////SHOW USER ASSIGNED PROJECTS
       if(($showuser>0&&finduser($pm_id,0,$showuser,0)==1)||$showuser==0||$showuser==$assign_to){

       $current_level=0;
       $reddate=acct_date($pm_id,$pm_level,$current_level,$pm_id,$security,$pm_complete_date);

       if($reddate>$pm_complete_date){$pm_complete_date="<font color=red><b>$pm_complete_date</b></font>";}

       $current_level=0;
       $whichflag=0;
       $whichpercent="0/0";
       $whichflag = findflag($pm_id,$pm_level,$current_level,$pm_id,$security,$whichflag);
       $whichpercent = findpercent($pm_id,$pm_level,$current_level,$pm_id,$security,$whichpercent);

       $newpercent=explode("/",$whichpercent);
       $completion = round($newpercent[0]/$newpercent[1]*100,0);
       $completion = "($completion%)";

       if($pm_status==4){$showflag="<img src=flagwhite.gif>";}
       elseif($pm_status==6||$pm_status==7){$showflag="<img src=flagblack.gif>";}
       elseif($whichflag==3){$showflag="<img src=flagred.gif>";}
       elseif($whichflag==2){$showflag="<img src=flagyellow.gif>";}
       elseif($whichflag==1){$showflag="<img src=flaggreen.gif>";}
       else{$showflag="<img src=flagblue.gif>";}

       if($priority==1){$showpriority="High";}
       elseif($priority==2){$showpriority="Med";}
       elseif($priority==3){$showpriority="Low";}
	   
	   $query2 = "SELECT division_name FROM pm_division WHERE divisionid = '$pm_division'";
       $result2 = DB_Proxy::query($query2);

       $division_name=@mysql_result($result2,0,"division_name");

       if($division_name == ""){$division_name="<font color=#999999><i>Not Assigned</i></font>";}
       
       $query2 = "SELECT * FROM pm_category WHERE categoryid = '$pm_category'";
       $result2 = DB_Proxy::query($query2);

       $category_name=@mysql_result($result2,0,"category_name");

       if($category_name == ""){$category_name="<font color=#999999><i>Not Assigned</i></font>";}

       $query2 = "SELECT * FROM pm_status WHERE pm_statusid = '$pm_status'";
       $result2 = DB_Proxy::query($query2);

       $status_name=@mysql_result($result2,0,"pm_statusname");
       $color=@mysql_result($result2,0,"color");
       
       /*
       if ($pm_status==4){$status_name="$status_name ($q_order)";}
       elseif ($pm_status==1){
          $query2 = "SELECT * FROM pm_detail WHERE pm_id = '$pm_id'";
          $result2 = DB_Proxy::query($query2);
          $num2=mysql_numrows($result2);

          $query3 = "SELECT * FROM pm_detail WHERE pm_id = '$pm_id' AND status = '1'";
          $result3 = DB_Proxy::query($query3);
          $num3=mysql_numrows($result3);

          $completion=round(($num3/$num2)*100,0);
          $status_name="$status_name ($completion%)";
       }
       */

       $status_name="<font color=#$color>$status_name $completion</font>";

       if ($assign_to==0){$assign_to="<i>Noone</i>";}
       else{
          $query2 = "SELECT * FROM login WHERE loginid = '$assign_to'";
          $result2 = DB_Proxy::query($query2);

          $assign_to=@mysql_result($result2,0,"username");
       }

       $link="<a href=pm_details.php?pm_id=$pm_id style=$style>";

       if($showdetails==1){$pm_color="#E8E7E7";}
       else{$pm_color="white";}

       $query3 = "SELECT * FROM pm_detail WHERE pm_id = '$pm_id' AND level = '1' ORDER BY propose_date DESC, pm_detailid DESC";
       $result3 = DB_Proxy::query($query3);
       $num3=mysql_numrows($result3);

       if($num3==0){$showflag="<img src=flagwhite.gif>";}

       echo "<tr bgcolor=$pm_color onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor='$pm_color' valign=top height=20><td><ul><li>$link<font color=blue>$pm_job</a></td><td>$division_name</td><td>$category_name</td><td>$pm_date</td><td>$pm_complete_date</td><td>$showpriority</td><td></td><td align=right>$showflag</td><td align=right>$status_name</td><td align=right>$assign_to</td></tr>";

       /////////////////DETAILS
       if($showdetails==1){
          $tothours=0;
          $redhours=0;

          echo "<tr height=1 bgcolor=#CCCCCC><td colspan=10></td></tr>";
          if($pm_detail2!=""){echo "<tr onMouseOver=this.bgColor='yellow' onMouseOut=this.bgColor='#FFFFFF'><td colspan=9><font size=2 color=#444455><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $pm_detail2</td></tr>";}

          $today=date("Y-m-d");
          $twoweeks=$today;
          for($counter2=1;$counter2<=13;$counter2++){$twoweeks=prevday($twoweeks);} 

          $current_level=2;
          $num3--;
          while($num3>=0){

             $pm_detailid=@mysql_result($result3,$num3,"pm_detailid");
             $pm_level=@mysql_result($result3,$num3,"level");
             $detail=@mysql_result($result3,$num3,"detail");
             $status=@mysql_result($result3,$num3,"status");
             $propose_date=@mysql_result($result3,$num3,"propose_date");
             $date=@mysql_result($result3,$num3,"date");
             $hours=@mysql_result($result3,$num3,"hours");
             $assign_to2=@mysql_result($result3,$num3,"assign_to");
             $assign_to3=$assign_to2;
 
             $has_subs="0000-00-00";
             $has_subs=findsubs($pm_detailid,$pm_level);

             if($has_subs>"0000-00-00"){
               $propose_date=$has_subs;
             }           

                $query4 = "SELECT * FROM login WHERE loginid = '$assign_to2'";
                $result4 = DB_Proxy::query($query4);

                $assign_to2=@mysql_result($result4,0,"username");

             //if($status==1){$shhours=" - $hours hours";}
             //else{$shhours="";}
 
             if($status==1){$showdays=dateDiff($propose_date,$date); if($showdays>0){$showdays="(+$showdays days)";} else{$showdays="($showdays days)";}}
             else{$showdays=dateDiff($propose_date,$today); if($showdays>0){$showdays="(+$showdays days)";} else{$showdays="($showdays days)";}}

             if($status==1){$myflag="<img src=flagblue2.gif>";}
             elseif($propose_date>$today){$myflag="<img src=flaggreen2.gif>";}
             elseif($propose_date>=$twoweeks){$myflag="<img src=flagyellow2.gif>";}
             else{$myflag="<img src=flagred2.gif>";}

             if($status==1&&$date>=$date1&&$date<=$date2&&$datetype==1&&($showassign==$assign_to3||$showassign==0)){$status="<img src=done.gif>";$fontcolor="red";$redhours+=$hours;}
             elseif($status==1){$status="<img src=done.gif>";$fontcolor="#999999";}
             else{$status="<img src=working.gif>";$fontcolor="black";}

             if($date=="0000-00-00"){$date="";}
             else{$date="($date)";}

             $tothours=$tothours+$hours;

             if($showuser==0||$showuser==$assign_to3){echo "<tr onMouseOver=this.bgColor='yellow' onMouseOut=this.bgColor='#FFFFFF'><td colspan=3><font size=2 color=$fontcolor><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$status $detail</td><td align=right colspan=2><font size=2 color=$fontcolor>$propose_date</td><td align=right><font size=2 color=$fontcolor>$showdays</td><td align=right><font size=2 color=$fontcolor>$myflag</td><td></td><td align=right><font size=2 color=$fontcolor>$assign_to2</td></tr>";}

             drilldown($pm_id,$pm_level,$current_level,$pm_detailid,$security,$showuser);

             $num3--;
          }
          if($datetype==1){$showred="<font color=red>($redhours)</font>";}          

          //echo "<tr onMouseOver=this.bgColor='yellow' onMouseOut=this.bgColor='#FFFFFF'><td colspan=7><font size=2 color=#444455><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Hours: $tothours $showred</td></tr>";
          $sheethours=$sheethours+$tothours;
          $sheetredhours+=$redhours;
       }
       /////////////////END DETAILS

       //echo "<tr height=1 bgcolor=#CCCCCC><td colspan=10></td></tr>";
 
       }

       $num--;
       $counter++;
    }
	echo "</tbody>";
/////////////////////END/////////////////////////////////////////////
    if($datetype==1&&$showdetails==1){$showred="<font color=red>($sheetredhours)</font>";}
    else{$showred="";}
    if($showdetails==1){$showhours="<b>Total Hours: $sheethours";}

    //echo "<tr height=1 bgcolor=black><td colspan=10></td></tr>";
    echo "<tfoot><tr bgcolor=#FFFF99><td></td><td colspan=9 align=right><b>Results: $counter</td></tr></tfoot>";
    echo "</table></center><p>";



    //mysql_close();

    echo "<center><form action=http://treatamerica.essentialpos.com/ta/businesstrack.php method=post><input type=submit value='Return to Main Page'></form></center>";

    echo "<p><br><p><br><img src=logo.jpg>";

    echo "<DIV ID=testdiv1 STYLE=position:absolute;visibility:hidden;background-color:white;layer-background-color:white;></DIV></body>";

}
?>