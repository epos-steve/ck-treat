<?php

include_once("../ta/db.php");
$style = "text-decoration:none";

function dateDiff($startDate, $endDate){
   $days = (strtotime($endDate) - strtotime($startDate)) / (60 * 60 * 24);

   return round($days,0);
} 

function nextday($nextd,$day_format=''){ //Function returns next day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monn = substr($nextd, 5, 2);
   $dayn = substr($nextd, 8, 2);
   $yearn = substr($nextd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monn, $dayn+1, $yearn));
   return $tempdate;
}

function prevday($prevd,$day_format=''){ //Function returns previous day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monp = substr($prevd, 5, 2);
   $dayp = substr($prevd, 8, 2);
   $yearp = substr($prevd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monp, $dayp-1, $yearp));
   return $tempdate;
}

function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}

function drilldown($pm_id,$pm_level,$current_level,$pm_detailid,$security){
       /////////////DRILL DOWN
       $today=date("Y-m-d");
       $twoweeks=$today;
       for($counter=1;$counter<=13;$counter++){$twoweeks=prevday($twoweeks);} 

       $query2 = "SELECT * FROM login WHERE oo8 > '0' ORDER BY username DESC";
       $result2 = mysql_query($query2);
       $num2=mysql_numrows($result2);
       
       $queryb = "SELECT * FROM pm_detail WHERE pm_id = '$pm_detailid' AND level = '$current_level' ORDER BY propose_date DESC, pm_detailid DESC";
       $resultb = mysql_query($queryb);
       $numb=mysql_numrows($resultb);

       $current_level++;
       $numb--;
       while($numb>=0){
          $is_disabled2="";

          //$pm_id=@mysql_result($resultb,$num,"pm_id");
          $pm_level=@mysql_result($resultb,$numb,"level");
          $pm_detailid=@mysql_result($resultb,$numb,"pm_detailid");
          $detail=@mysql_result($resultb,$numb,"detail");
          $status=@mysql_result($resultb,$numb,"status");
          $propose_date=@mysql_result($resultb,$numb,"propose_date");
          $date=@mysql_result($resultb,$numb,"date");
          $assign_to=@mysql_result($resultb,$numb,"assign_to");
          $hours=@mysql_result($resultb,$numb,"hours");
          $eoc=@mysql_result($resultb,$numb,"eoc");

          $has_subs="0000-00-00";
          $has_subs=findsubs($pm_detailid,$pm_level);

          if($has_subs>"0000-00-00"){
             $propose_date=$has_subs;
             $disableme="DISABLED";
          }
          else{$disableme="";}

          if($status==1){$showdays=dateDiff($propose_date,$date); if($showdays>0){$showdays="(+$showdays days)";} else{$showdays="($showdays days)";}}
          else{$showdays=dateDiff($propose_date,$today); if($showdays>0){$showdays="(+$showdays days)";} else{$showdays="($showdays days)";}}

          if($status==1){$showflag="<img src=flagblue.gif>";}
          elseif($propose_date>$today){$showflag="<img src=flaggreen.gif>";}
          elseif($propose_date>=$twoweeks){$showflag="<img src=flagyellow.gif>";}
          elseif($propose_date<$twoweeks){$showflag="<img src=flagred.gif>";}
          else{$showflag="";}

          if($security>0||$status==0){$showdelete="<a href=pm_edit_detail.php?pm_id=$pm_id&pm_detailid=$pm_detailid&goto=3 onclick='return deldetail()'><img src=delete.gif height=16 width=16 border=0 alt='DELETE'></a>"; $showadd="<a href=pm_edit_detail.php?pm_id=$pm_id&pm_detailid=$pm_detailid&goto=1&level=$current_level><img src=plus.gif height=16 width=16 border=0 alt='Add Sub Category'></a>";}
          else{$showdelete="";$is_disabled2="DISABLED";}

          $tothours=$tothours+$hours;

          if ($date=="0000-00-00"){$date="";}

          if ($status==1){$complete="CHECKED";$color="#E9E9E9";}
          else{$complete="";$color="white";}

          if ($eoc==1){$showeoc="CHECKED";}
          else{$showeoc="";}

          $numtab="";
          for($counter=1;$counter<$current_level;$counter++){$numtab.="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";}

          echo "<tr bgcolor=$color onMouseOver=this.bgColor='#999999' onMouseOut=this.bgColor='$color'><td colspan=2>$numtab<input type=hidden name='oldstatus:$pm_detailid' value=$status><input type=checkbox name='status:$pm_detailid' value=1 $complete $disableme onclick=\"saveme.style.backgroundColor='#CC3333'\">&nbsp;&nbsp;<input type=text size=80 value='$detail' name='detail:$pm_detailid' onchange=\"saveme.style.backgroundColor='#CC3333'\"> $showdelete $showadd</td><td align=right>$showdays</td><td align=right>$showflag</td><td align=right><SCRIPT LANGUAGE='JavaScript' ID='js$pm_detailid'> var cal$pm_detailid = new CalendarPopup('testdiv1');cal$pm_detailid.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=propose_date_$pm_detailid VALUE='$propose_date' SIZE=8 $disableme onchange=\"saveme.style.backgroundColor='#CC3333'\"> <A HREF=\"javascript:void(0)\" onClick=\"cal$pm_detailid.select(document.forms[1].propose_date_$pm_detailid,'anchor$pm_detailid','yyyy-MM-dd'); saveme.style.backgroundColor='#CC3333';\" return false; TITLE=cal$pm_detailid.select(document.forms[1].propose_date_$pm_detailid,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor$pm_detailid' ID='anchor$pm_detailid'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A></td><td align=right><input type=text size=8 name='date:$pm_detailid' value='$date' $disableme onchange=\"saveme.style.backgroundColor='#CC3333'\"></td><td align=right><select name='assign:$pm_detailid' $is_disabled onchange=\"saveme.style.backgroundColor='#CC3333'\">";

          $temp_num=$num2-1;
          while($temp_num>=0){
             $userid=@mysql_result($result2,$temp_num,"userid");
             $assign_to2=@mysql_result($result2,$temp_num,"username");
             if($userid==$assign_to){$sel3="SELECTED";}
             else{$sel3="";}

             echo "<option value=$userid $sel3>$assign_to2</option>";

             $temp_num--;
          }
          echo "</select></td></tr>";

          drilldown($pm_id,$pm_level,$current_level,$pm_detailid,$security);
       
          $numb--;
       }
       $current_level--;
       /////////////END DRILL DOWN
}

function findpercent($pm_id,$pm_level,$current_level,$pm_detailid,$security,$whichpercent){
          $today=date("Y-m-d"); 
          $twoweeks=$today;
          for($counter=1;$counter<=13;$counter++){$twoweeks=prevday($twoweeks);} 

          $current_level++;

          $query3 = "SELECT * FROM pm_detail WHERE pm_id = '$pm_detailid' AND level = '$current_level' ORDER BY propose_date DESC, pm_detailid DESC";
          $result3 = mysql_query($query3);
          $num3=mysql_numrows($result3);

          $num3--;
          while($num3>=0){

             $pm_detailid=@mysql_result($result3,$num3,"pm_detailid");
             $detail=@mysql_result($result3,$num3,"detail");
             $status=@mysql_result($result3,$num3,"status");
             $propose_date=@mysql_result($result3,$num3,"propose_date");

             $newpercent = explode("/",$whichpercent);

             if($status==1){$newpercent[0]++;$newpercent[1]++;}
             else{$newpercent[1]++;}

             $whichpercent="$newpercent[0]/$newpercent[1]";

             $whichpercent = findpercent($pm_id,$pm_level,$current_level,$pm_detailid,$security,$whichpercent);

             $num3--;
          }

          return $whichpercent;
}

function findsubs($pm_detailid,$pm_level){
   
   $pm_level++;

   $query3 = "SELECT * FROM pm_detail WHERE pm_id = '$pm_detailid' AND level = '$pm_level' ORDER BY propose_date DESC LIMIT 0,1";
   $result3 = mysql_query($query3);
   $num3=mysql_numrows($result3);

   if($num3>0){
      $has_subs=@mysql_result($result3,0,"propose_date");
      $new_detailid=@mysql_result($result3,0,"pm_detailid");
      $new_level=@mysql_result($result3,0,"pm_level");

      $new_level++;
      $query3 = "SELECT * FROM pm_detail WHERE pm_id = '$new_detailid' AND level = '$new_level' ORDER BY propose_date DESC LIMIT 0,1";
      $result3 = mysql_query($query3);
      $num3=mysql_numrows($result3);
      $new_level--;

      if($num3>0){$has_subs=findsubs($new_detailid,$new_level);}
   }

   return $has_subs;
}

$user = isset($_COOKIE["usercook"])?$_COOKIE["usercook"]:'';
$pass = isset($_COOKIE["passcook"])?$_COOKIE["passcook"]:'';
$manage = isset($_POST["manage"])?$_POST["manage"]:'';
$newsecurity = isset($_POST["security"])?$_POST["security"]:'';
$cat_name = isset($_POST["cat_name"])?$_POST["cat_name"]:'';
$editcat = isset($_POST["editcat"])?$_POST["editcat"]:'';
$div_name = isset($_POST["div_name"])?$_POST["div_name"]:'';
$editdiv = isset($_POST["editdiv"])?$_POST["editdiv"]:'';
$type = isset($_POST["type"])?$_POST["type"]:'';

$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";

mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die(mysql_error());

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass' AND oo8 = '9'";
$result = mysql_query($query);
$num=mysql_numrows($result);

if ($num != 1 || $user == "" || $pass == "")
{
    echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}
else
{
	if($type==1){
		$query = "UPDATE login SET oo8 = '$newsecurity' WHERE loginid = '$manage'";
		$result = mysql_query($query);
	}
	elseif($type==2 && $editcat > 0){
		$query = "UPDATE pm_category SET category_name = '$cat_name' WHERE categoryid = '$editcat'";
		$result = mysql_query($query);
	}
	elseif($type==2 && $editdiv > 0){
		$query = "UPDATE pm_division SET division_name = '$div_name' WHERE divisionid = '$editdiv'";
		$result = mysql_query($query);
	}
	elseif($type==2 && $cat_name != ""){
		$query = "INSERT INTO pm_category (category_name) VALUES ('$cat_name')";
		$result = mysql_query($query);
		$editcat = mysql_insert_id();
	}
	elseif($type==2 && $div_name != ""){
		$query = "INSERT INTO pm_division (division_name) VALUES ('$div_name')";
		$result = mysql_query($query);
		$editdiv = mysql_insert_id();
	}
	elseif($type==3 && $editcat>0){
		$query = "DELETE FROM pm_category WHERE categoryid = '$editcat'";
		$result = mysql_query($query);
		
		$editcat="";
	}
	elseif($type==3 && $editdiv>0){
		$query = "DELETE FROM pm_division WHERE divisionid = '$editdiv'";
		$result = mysql_query($query);
		
		$editdiv="";
	}
	else{}
	
	echo "$query";

    $location="pm_settings.php?manage=$manage&manage2=$newsecurity&editcat=$editcat&editdiv=$editdiv";
    header('Location: ./' . $location);

}
mysql_close();

?>