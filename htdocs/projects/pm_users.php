<?php

include_once("../ta/db.php");
$style = "text-decoration:none";

function dateDiff($startDate, $endDate){
   $days = (strtotime($endDate) - strtotime($startDate)) / (60 * 60 * 24);

   return round($days,0);
} 

function nextday($nextd,$day_format=''){ //Function returns next day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monn = substr($nextd, 5, 2);
   $dayn = substr($nextd, 8, 2);
   $yearn = substr($nextd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monn, $dayn+1, $yearn));
   return $tempdate;
}

function prevday($prevd,$day_format=''){ //Function returns previous day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monp = substr($prevd, 5, 2);
   $dayp = substr($prevd, 8, 2);
   $yearp = substr($prevd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monp, $dayp-1, $yearp));
   return $tempdate;
}

function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}

function drilldown($pm_id,$pm_level,$current_level,$pm_detailid,$security){
       /////////////DRILL DOWN
       $today=date("Y-m-d");
       $twoweeks=$today;
       for($counter=1;$counter<=13;$counter++){$twoweeks=prevday($twoweeks);} 

       $query2 = "SELECT * FROM login WHERE oo8 > '0' ORDER BY username DESC";
       $result2 = mysql_query($query2);
       $num2=mysql_numrows($result2);
       
       $queryb = "SELECT * FROM pm_detail WHERE pm_id = '$pm_detailid' AND level = '$current_level' ORDER BY propose_date DESC, pm_detailid DESC";
       $resultb = mysql_query($queryb);
       $numb=mysql_numrows($resultb);

       $current_level++;
       $numb--;
       while($numb>=0){
          $is_disabled2="";

          //$pm_id=@mysql_result($resultb,$num,"pm_id");
          $pm_level=@mysql_result($resultb,$numb,"level");
          $pm_detailid=@mysql_result($resultb,$numb,"pm_detailid");
          $detail=@mysql_result($resultb,$numb,"detail");
          $status=@mysql_result($resultb,$numb,"status");
          $propose_date=@mysql_result($resultb,$numb,"propose_date");
          $date=@mysql_result($resultb,$numb,"date");
          $assign_to=@mysql_result($resultb,$numb,"assign_to");
          $hours=@mysql_result($resultb,$numb,"hours");
          $eoc=@mysql_result($resultb,$numb,"eoc");

          $has_subs="0000-00-00";
          $has_subs=findsubs($pm_detailid,$pm_level);

          if($has_subs>"0000-00-00"){
             $propose_date=$has_subs;
             $disableme="DISABLED";
          }
          else{$disableme="";}

          if($status==1){$showdays=dateDiff($propose_date,$date); if($showdays>0){$showdays="(+$showdays days)";} else{$showdays="($showdays days)";}}
          else{$showdays=dateDiff($propose_date,$today); if($showdays>0){$showdays="(+$showdays days)";} else{$showdays="($showdays days)";}}

          if($status==1){$showflag="<img src=flagblue.gif>";}
          elseif($propose_date>$today){$showflag="<img src=flaggreen.gif>";}
          elseif($propose_date>=$twoweeks){$showflag="<img src=flagyellow.gif>";}
          elseif($propose_date<$twoweeks){$showflag="<img src=flagred.gif>";}
          else{$showflag="";}

          if($security>0||$status==0){$showdelete="<a href=pm_edit_detail.php?pm_id=$pm_id&pm_detailid=$pm_detailid&goto=3 onclick='return deldetail()'><img src=delete.gif height=16 width=16 border=0 alt='DELETE'></a>"; $showadd="<a href=pm_edit_detail.php?pm_id=$pm_id&pm_detailid=$pm_detailid&goto=1&level=$current_level><img src=plus.gif height=16 width=16 border=0 alt='Add Sub Category'></a>";}
          else{$showdelete="";$is_disabled2="DISABLED";}

          $tothours=$tothours+$hours;

          if ($date=="0000-00-00"){$date="";}

          if ($status==1){$complete="CHECKED";$color="#E9E9E9";}
          else{$complete="";$color="white";}

          if ($eoc==1){$showeoc="CHECKED";}
          else{$showeoc="";}

          $numtab="";
          for($counter=1;$counter<$current_level;$counter++){$numtab.="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";}

          echo "<tr bgcolor=$color onMouseOver=this.bgColor='#999999' onMouseOut=this.bgColor='$color'><td colspan=2>$numtab<input type=hidden name='oldstatus:$pm_detailid' value=$status><input type=checkbox name='status:$pm_detailid' value=1 $complete $disableme onclick=\"saveme.style.backgroundColor='#CC3333'\">&nbsp;&nbsp;<input type=text size=80 value='$detail' name='detail:$pm_detailid' onchange=\"saveme.style.backgroundColor='#CC3333'\"> $showdelete $showadd</td><td align=right>$showdays</td><td align=right>$showflag</td><td align=right><SCRIPT LANGUAGE='JavaScript' ID='js$pm_detailid'> var cal$pm_detailid = new CalendarPopup('testdiv1');cal$pm_detailid.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME=propose_date_$pm_detailid VALUE='$propose_date' SIZE=8 $disableme onchange=\"saveme.style.backgroundColor='#CC3333'\"> <A HREF=\"javascript:void(0)\" onClick=\"cal$pm_detailid.select(document.forms[1].propose_date_$pm_detailid,'anchor$pm_detailid','yyyy-MM-dd'); saveme.style.backgroundColor='#CC3333';\" return false; TITLE=cal$pm_detailid.select(document.forms[1].propose_date_$pm_detailid,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor$pm_detailid' ID='anchor$pm_detailid'><img src=calendar.gif border=0 height=16 width=16 alt='Choose a Date'></A></td><td align=right><input type=text size=8 name='date:$pm_detailid' value='$date' $disableme onchange=\"saveme.style.backgroundColor='#CC3333'\"></td><td align=right><select name='assign:$pm_detailid' $is_disabled onchange=\"saveme.style.backgroundColor='#CC3333'\">";

          $temp_num=$num2-1;
          while($temp_num>=0){
             $userid=@mysql_result($result2,$temp_num,"userid");
             $assign_to2=@mysql_result($result2,$temp_num,"username");
             if($userid==$assign_to){$sel3="SELECTED";}
             else{$sel3="";}

             echo "<option value=$userid $sel3>$assign_to2</option>";

             $temp_num--;
          }
          echo "</select></td></tr>";

          drilldown($pm_id,$pm_level,$current_level,$pm_detailid,$security);
       
          $numb--;
       }
       $current_level--;
       /////////////END DRILL DOWN
}

function findpercent($pm_id,$pm_level,$current_level,$pm_detailid,$security,$whichpercent){
          $today=date("Y-m-d"); 
          $twoweeks=$today;
          for($counter=1;$counter<=13;$counter++){$twoweeks=prevday($twoweeks);} 

          $current_level++;

          $query3 = "SELECT * FROM pm_detail WHERE pm_id = '$pm_detailid' AND level = '$current_level' ORDER BY propose_date DESC, pm_detailid DESC";
          $result3 = mysql_query($query3);
          $num3=mysql_numrows($result3);

          $num3--;
          while($num3>=0){

             $pm_detailid=@mysql_result($result3,$num3,"pm_detailid");
             $detail=@mysql_result($result3,$num3,"detail");
             $status=@mysql_result($result3,$num3,"status");
             $propose_date=@mysql_result($result3,$num3,"propose_date");

             $newpercent = explode("/",$whichpercent);

             if($status==1){$newpercent[0]++;$newpercent[1]++;}
             else{$newpercent[1]++;}

             $whichpercent="$newpercent[0]/$newpercent[1]";

             $whichpercent = findpercent($pm_id,$pm_level,$current_level,$pm_detailid,$security,$whichpercent);

             $num3--;
          }

          return $whichpercent;
}

function findsubs($pm_detailid,$pm_level){
   
   $pm_level++;

   $query3 = "SELECT * FROM pm_detail WHERE pm_id = '$pm_detailid' AND level = '$pm_level' ORDER BY propose_date DESC LIMIT 0,1";
   $result3 = mysql_query($query3);
   $num3=mysql_numrows($result3);

   if($num3>0){
      $has_subs=@mysql_result($result3,0,"propose_date");
      $new_detailid=@mysql_result($result3,0,"pm_detailid");
      $new_level=@mysql_result($result3,0,"pm_level");

      $new_level++;
      $query3 = "SELECT * FROM pm_detail WHERE pm_id = '$new_detailid' AND level = '$new_level' ORDER BY propose_date DESC LIMIT 0,1";
      $result3 = mysql_query($query3);
      $num3=mysql_numrows($result3);
      $new_level--;

      if($num3>0){$has_subs=findsubs($new_detailid,$new_level);}
   }

   return $has_subs;
}

$user = isset($_COOKIE["usercook"])?$_COOKIE["usercook"]:'';
$pass = isset($_COOKIE["passcook"])?$_COOKIE["passcook"]:'';
$manage = isset($_GET["manage"])?$_GET["manage"]:'';

$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";

mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die(mysql_error());

$query = "SELECT * FROM login WHERE username = '$user' AND password = '$pass' AND oo8 >= '8'";
$result = mysql_query($query);
$num=mysql_numrows($result);

$security=@mysql_result($result,0,"security");

if ($num != 1 || $user == "" || $pass == "")
{
    echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
}
else
{
?>
<head>

<SCRIPT LANGUAGE="JavaScript">
<!-- Web Site:  http://dynamicdrive.com -->

<!-- This script and many more are available free online at -->
<!-- The JavaScript Source!! http://javascript.internet.com -->

<!-- Begin
function disableForm(theform) {
if (document.all || document.getElementById) {
for (i = 0; i < theform.length; i++) {
var tempobj = theform.elements[i];
if (tempobj.type.toLowerCase() == "submit" || tempobj.type.toLowerCase() == "reset")
tempobj.disabled = true;
}

}
else {
alert("The form has been submitted.  But, since you're not using IE 4+ or NS 6, the submit button was not disabled on form submission.");
return false;
   }
}
//  End -->
</script>

<script language="JavaScript" 
   type="text/JavaScript">
function changePage(newLoc)
 {
   nextPage = newLoc.options[newLoc.selectedIndex].value
		
   if (nextPage != "")
   {
      document.location.href = nextPage
   }
 }
</script>

<SCRIPT LANGUAGE="JavaScript" SRC="CalendarPopup.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">document.write(getCalendarStyles());</SCRIPT>

<STYLE>
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation
			{
			background-color:#6677DD;
			text-align:center;
			vertical-align:center;
			text-decoration:none;
			color:#FFFFFF;
			font-weight:bold;
			}
	.TESTcpDayColumnHeader,
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation,
	.TESTcpCurrentMonthDate,
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDate,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDate,
	.TESTcpCurrentDateDisabled,
	.TESTcpTodayText,
	.TESTcpTodayTextDisabled,
	.TESTcpText
			{
			font-family:arial;
			font-size:8pt;
			}
	TD.TESTcpDayColumnHeader
			{
			text-align:right;
			border:solid thin #6677DD;
			border-width:0 0 1 0;
			}
	.TESTcpCurrentMonthDate,
	.TESTcpOtherMonthDate,
	.TESTcpCurrentDate
			{
			text-align:right;
			text-decoration:none;
			}
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDateDisabled
			{
			color:#D0D0D0;
			text-align:right;
			text-decoration:line-through;
			}
	.TESTcpCurrentMonthDate
			{
			color:#6677DD;
			font-weight:bold;
			}
	.TESTcpCurrentDate
			{
			color: #FFFFFF;
			font-weight:bold;
			}
	.TESTcpOtherMonthDate
			{
			color:#808080;
			}
	TD.TESTcpCurrentDate
			{
			color:#FFFFFF;
			background-color: #6677DD;
			border-width:1;
			border:solid thin #000000;
			}
	TD.TESTcpCurrentDateDisabled
			{
			border-width:1;
			border:solid thin #FFAAAA;
			}
	TD.TESTcpTodayText,
	TD.TESTcpTodayTextDisabled
			{
			border:solid thin #6677DD;
			border-width:1 0 0 0;
			}
	A.TESTcpTodayText,
	SPAN.TESTcpTodayTextDisabled
			{
			height:20px;
			}
	A.TESTcpTodayText
			{
			color:#6677DD;
			font-weight:bold;
			}
	SPAN.TESTcpTodayTextDisabled
			{
			color:#D0D0D0;
			}
	.TESTcpBorder
			{
			border:solid thin #6677DD;
			}
</STYLE>

<SCRIPT LANGUAGE=javascript><!--
function deldetail(){return confirm('Are you sure you want to delete this?  ALL sub-tasks will be lost!');}
// --></SCRIPT>

<SCRIPT LANGUAGE=javascript><!--
function deljob(){return confirm('Are you sure you want to delete this Project?  It is recommended you change the status to Cancelled to keep track of ideas. Press OK to continue.');}
// --></SCRIPT>

</head>
<?
    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=100% style=\"background-image: url(backdrop.jpg);background-repeat: no-repeat;\"><tr><td colspan=2><a href=../ta/businesstrack.php><img src=talogo.gif border=0></a><p></td></tr></table>";

    echo "<center><table cellspacing=0 cellpadding=0 width=100% style=\"border:1px solid black;\" bgcolor=#E8E7E7>";

    /////////////////////////////////
    //////////USERS//////////////////
    /////////////////////////////////

    echo "<tr valign=top><td style=\"border:1px solid black;\" bgcolor=#FFFF99><b><i>&nbsp;Users</i></b></td><td style=\"border:1px solid black;\" bgcolor=#FFFF99><b><i>&nbsp;Projects In Progress</i></b></td></tr>";

    echo "<tr valign=top><td style=\"border:1px solid black;\" width=10%><form action=pm_users.php method=post style=\"margin:0;padding:0;display:inline;\"><select name=manage MULTIPLE size=20 onChange='changePage(this.form.manage);disableForm(this.form.manage);'>";

    if($manage<1){$showsel="SELECTED";}
    else{$showsel="";}
    echo "<optgroup label='Please Choose a User...'>";

    $query = "SELECT * FROM login WHERE oo8 < '8' AND oo8 > '0' ORDER BY lastname";
    $result = mysql_query($query);

    while($r=mysql_fetch_array($result)){
       $loginid=$r["loginid"];
       $lastname=$r["lastname"];
       $firstname=$r["firstname"];
       $showuser=$r["username"];

       if($manage==$loginid){$sel="SELECTED";}
       else{$sel="";}

       echo "<option value=pm_users.php?manage=$loginid $sel>$lastname, $firstname ($showuser)</option>";
    }
    echo "</optgroup></select></form></td>";

    /////////////////////////////////
    //////SELECTED JOBS//////////////
    /////////////////////////////////

    echo "<td style=\"border:1px solid black;\" align=right>";

    echo "<form action=pm_users_manage.php method=post style=\"margin:0;padding:0;display:inline;\"><input type=hidden name=manage value=$manage><p><br><center><table width=98% cellspacing=0 cellpadding=0 style=\"border:1px solid #999999;\">";

    $query = "SELECT * FROM pm_list WHERE status = '1' ORDER BY date DESC";
    $result = mysql_query($query);

    $counter=1;
    $mycolor="#EEEEEE";
    while($r=mysql_fetch_array($result)){
       $pm_id=$r["pm_id"];
       $pm_job=$r["pm_job"];
       $pm_date=$r["date"];
       $priority=$r["priority"];
       $assign_to=$r["assign_to"];

       if($counter==1){echo "<tr valign=top>";}

       $query2 = "SELECT username FROM login WHERE loginid = '$assign_to'";
       $result2 = mysql_query($query2);

       $assign_to=@mysql_result($result2,0,"username");

       $query2 = "SELECT * FROM pm_access WHERE jobid = '$pm_id' AND loginid = '$manage'";
       $result2 = mysql_query($query2);
       $num2=mysql_numrows($result2);

       if($num2!=0){$showsel="CHECKED";$fontcolor="blue";$oldcolor=$mycolor;$mycolor="#FFFF99";}
       else{$showsel="";$fontcolor="black";}

       echo "<td width=3% bgcolor=$mycolor style=\"border-left:1px solid #CCCCCC; border-top:1px solid #CCCCCC; border-bottom:1px solid #CCCCCC;\">&nbsp;<input type=checkbox name='$pm_id' value=1 $showsel></td><td width=31% bgcolor=$mycolor style=\"border-right:1px solid #CCCCCC; border-top:1px solid #CCCCCC; border-bottom:1px solid #CCCCCC;\"><font color=$fontcolor>&nbsp;$pm_job<br><font size=2>($pm_date $assign_to)</font></font></td>";

       if($mycolor=="#FFFF99"){
          if($oldcolor=="#EEEEEE"){$mycolor="white";}
          else{$mycolor="#EEEEEE";}
       }
       elseif($mycolor=="#EEEEEE"){$mycolor="white";}
       else{$mycolor="#EEEEEE";}

       if($counter==3){echo "</tr>";}

       $counter++;
       if($counter==4){$counter=1;}
    }

    if($counter==2){echo "<td colspan=4></td></tr>";}
    elseif($counter==3){echo "<td colspan=2></td></tr>";}

    if($manage<1){$is_disabled="DISABLED";}

    echo "</table></center><input type=submit value='Update' $is_disabled></form></td></tr></table></center>";
//////////////////END FORM
    echo "<center><form action=pm_list.php method=post><input type=submit value='Return' tabindex=8></form></center></a>";

    mysql_close();

    echo "<p><br><p><br><center><img src=logo.jpg>";

    echo "<DIV ID=testdiv1 STYLE=position:absolute;visibility:hidden;background-color:white;layer-background-color:white;></DIV><body>";

}
?>