<?php

function dayofweek($date1)
{
	$day=substr($date1,8,2);
	$month=substr($date1,5,2);
	$year=substr($date1,0,4);
	$dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
	return $dayofweek;
}

function money($diff){   
	$findme='.';
	$double='.00';
	$single='0';
	$double2='00';
	$pos1 = strpos($diff, $findme);
	$pos2 = strlen($diff);
	if ($pos1==""){$diff="$diff$double";}
	elseif ($pos2-$pos1==2){$diff="$diff$single";}
	elseif ($pos2-$pos1==1){$diff="$diff$double2";}
	else{}
	$dot = strpos($diff, $findme);
	$diff = substr($diff, 0, $dot+3);
	if ($diff > 0 && $diff < '0.01'){$diff="0.01";}
	elseif($diff < 0 && $diff > '-0.01'){$diff="-0.01";}
	return $diff;
}

function nextday($date2)
{
	$day=substr($date2,8,2);
	$month=substr($date2,5,2);
	$year=substr($date2,0,4);
	$leap = date("L");

	if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
	{
		if ($month == "01"){$month="02";}
		elseif ($month == "03"){$month="04";}
		elseif ($month == "05"){$month="06";}
		elseif ($month == "07"){$month="08";}
		elseif ($month == "08"){$month="09";}
		elseif ($month == "10"){$month="11";}
		$day='01';    
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($month=='02' && $day == '29' && $leap == '0')
	{
		$month='03';
		$day='01';
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($month=='02' && $day == '28')
	{
		$month='03';
		$day='01';
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
	{
		if ($month == "04"){$month="05";}
		if ($month == "06"){$month="07";}
		if ($month == "09"){$month="10";}
		if ($month == "11"){$month="12";}
		$day='01';
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($month=='12' && $day=='32')
	{
		$day='01';
		$month='01';
		$year++;
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else
	{
		$day=$day+1;
		if ($day<10){$day="0$day";}
		$tomorrow="$year-$month-$day";
		return $tomorrow;
	}
}

function prevday($date2) {
	$day=substr($date2,8,2);
	$month=substr($date2,5,2);
	$year=substr($date2,0,4);
	$day=$day-1;

	if ($day <= 0)
	{
		if ($month == 01)
		{
			$month = '12';
			$year--;
			$day=$day+31;
			$yesterday = "$year-$month-$day";
			return $yesterday;
		}
		else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
		{
			$month--;
			if ($month < 10)
			{
				$month="0$month";
			}
			$day=$day+31;
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
		else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
		{
			$month--;
			if ($month < 10)
			{
				$month="0$month";
			}
			$day=$day+30;
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
		else
		{
			$day=$day+28;
			$month='02';
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
	}
	else
	{
		if ($day < 10)
		{
			$day="0$day";
		}
		$yesterday="$year-$month-$day";
		return $yesterday;
	}
}

define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once(DOC_ROOT.'/bootstrap.php');

$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");

$user = isset($_COOKIE["usercook"])?$_COOKIE["usercook"]:'';
$pass = isset($_COOKIE["passcook"])?$_COOKIE["passcook"]:'';
$accountid = isset($_COOKIE["accountid"])?$_COOKIE["accountid"]:'';
$businessid = isset($_COOKIE["businessid"])?$_COOKIE["businessid"]:'';
$curmenu = isset($_COOKIE["curmenu"])?$_COOKIE["curmenu"]:'';
$reserveid = isset($_COOKIE["reserveid"])?$_COOKIE["reserveid"]:'';

if ( $accountid < 1 ) {
	$location = '/menu/logout.php?goto=5';
	header( 'Location: ' . $location );
}
elseif ( $businessid < 1 ) {
	$location = '/menu/';
	header( 'Location: ' . $location );
}
else
{
	$customerid = isset($_POST["customerid"])?$_POST["customerid"]:'';
	$weeknum = isset($_POST["weeknum"])?$_POST["weeknum"]:'';
	$calories = isset($_POST["calories"])?$_POST["calories"]:'';
	$cal_from_fat = isset($_POST["cal_from_fat"])?$_POST["cal_from_fat"]:'';
	$total_fat = isset($_POST["total_fat"])?$_POST["total_fat"]:'';
	$sat_fat = isset($_POST["sat_fat"])?$_POST["sat_fat"]:'';
	$cholesterol = isset($_POST["cholesterol"])?$_POST["cholesterol"]:'';
	$sodium = isset($_POST["sodium"])?$_POST["sodium"]:'';
	$potassium = isset($_POST["potassium"])?$_POST["potassium"]:'';
	$carbs = isset($_POST["carbs"])?$_POST["carbs"]:'';
	$fiber = isset($_POST["fiber"])?$_POST["fiber"]:'';
	$sugar = isset($_POST["sugar"])?$_POST["sugar"]:'';
	$protein = isset($_POST["protein"])?$_POST["protein"]:'';
	$vit_A = isset($_POST["vit_A"])?$_POST["vit_A"]:'';
	$vit_C = isset($_POST["vit_C"])?$_POST["vit_C"]:'';
	$thiamine = isset($_POST["thiamine"])?$_POST["thiamine"]:'';
	$iron = isset($_POST["iron"])?$_POST["iron"]:'';
	$treat_score = isset($_POST["treat_score"])?$_POST["treat_score"]:'';

	$query = "SELECT * FROM customer_nutrition WHERE customerid = '$customerid'";
	$result = Treat_DB_ProxyOld::query( $query );
	$num = mysql_numrows( $result );

	if($num>0){
		$query = "UPDATE customer_nutrition SET weeknum = '$weeknum',calories='$calories',cal_from_fat='$cal_from_fat',total_fat='$total_fat',sat_fat='$sat_fat',cholesterol='$cholesterol',sodium='$sodium',potassium='$potassium',carbs='$carbs',fiber='$fiber',sugar='$sugar',protein='$protein', vit_A='$vit_A', vit_C='$vit_C', thiamine='$thiamine', iron='$iron', treat_score='$treat_score' wHERE customerid = '$customerid'";
		$result = Treat_DB_ProxyOld::query( $query );
	}
	else{
		$query = "INSERT INTO customer_nutrition (customerid,weeknum,calories,cal_from_fat,total_fat,sat_fat,cholesterol,sodium,potassium,carbs,fiber,sugar,protein,vit_A,vit_C,thiamine,iron,treat_score) VALUES ('$customerid','$weeknum','$calories','$cal_from_fat','$total_fat','$sat_fat','$cholesterol','$sodium','$potassium','$carbs','$fiber','$sugar','$protein','$vit_A','$vit_C','$thiamine','$iron','$treat_score')";
		$result = Treat_DB_ProxyOld::query( $query );
	}

	$location = '/menu/history.php';
	header( 'Location: ' . $location );
	
}
?>