<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once(DOC_ROOT.'/bootstrap.php');

$accountname = null; # Notice: Undefined variable
$accountnum = null; # Notice: Undefined variable

$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today = "$year-$month-$day";
$todayname = date("l");
$bbdayname = date("l");
$bbmonth = date("F");
$bbday = date("j");

$user = \EE\Controller\Base::getSessionCookieVariable( 'usercook' );
$pass = \EE\Controller\Base::getSessionCookieVariable( 'passcook' );
$accountid = \EE\Controller\Base::getSessionCookieVariable( 'accountid' );
$businessid = \EE\Controller\Base::getPostGetSessionOrCookieVariable( array( 'bid', 'businessid' ) );
$curmenu = \EE\Controller\Base::getSessionCookieVariable( 'curmenu' );
$reserveid = \EE\Controller\Base::getSessionCookieVariable( 'reserveid' );
$goto = \EE\Controller\Base::getGetVariable("goto", '');
$logout = \EE\Controller\Base::getGetVariable("logout", '');
$mydate = \EE\Controller\Base::getGetVariable("mydate", '');
$email = \EE\Controller\Base::getSessionCookieVariable( 'email' );

if ( $accountid > 0 || $logout == 1 ) {
#	setcookie( 'realuser', '' );
#	setcookie( 'usercook', '' );
#	setcookie( 'passcook', '' );
#	setcookie( 'businessid', '' );
#	setcookie( 'accountid', '' );
#	setcookie( 'reserveid', '' );
#	setcookie( 'curmenu', '' );
	Treat_Model_User_Customer::logout();

	$location = '';
	if ( !isset( $_GET['xhr'] ) ) {
		header( 'Location: /menu/intro.php?bid=' . $businessid );
	}
	exit();
}
else{

	\EE\Model\Business\Setting::db( Treat_DB::singleton() );
	$settings = \EE\Model\Business\Setting::get("business_id = ". abs(intval($businessid)), true);
	if ( !$settings ) {
		$settings = new \EE\Null();
	}
	$background_image_logo = $settings->background_image ?: '';
	$site_logo = $settings->site_logo ?: 'logo.jpg';


	$query = "SELECT companyname FROM company WHERE companyid = '1'";
	$result = Treat_DB_ProxyOld::query( $query );

	$companyname = @mysql_result( $result, 0, 'companyname' );

	$query = "SELECT companyname FROM business WHERE businessid = '$businessid'";
	$result = Treat_DB_ProxyOld::query( $query );

	$businessname = @mysql_result( $result, 0, 'companyname' );

?>
<html>
	<head>
		<title>Treat America: logout</title>
		<link rel="stylesheet" href="/assets/css/nutrition/style.css" />
<?php
	includeTimeOutCode();
?>
	</head>
	<body background="<?php echo $background_image_logo; ?>" onLoad="focus();login.username.focus()">
		<center>
			<table cellspacing="0" cellpadding="0" border="0" width="90%" class="header">
				<tr class="logo">
					<td colspan="4">
						<img src="<?php echo $site_logo; ?>" alt="logo" />
						<p>
					</td>
				</tr>
				<tr class="by-line">
					<td colspan="4">
						<font color="white" size="3">
							<b>
								The Right Choice... for a Healthier You!
							</b>
						</font>
					</td>
				</tr>
				<tr class="account_info">
					<td width="10%"
						><img src="/menu/rc-header2.jpg" height="80" width="108" alt="header"
					/></td>
					<td>
						<font size="4" color="white">
							<b>
								<?php echo $companyname; ?> - <?php echo $businessname; ?> 
							</b>
						</font>
						<br/>
						<b>
							<font color="white">
								Account: <?php echo $accountname; ?> 
								<br/>
								Account #: <?php echo $accountnum; ?> 
							</font>
						</b>
					</td>
					<td colspan="2" align="right" valign="top">
						<font color="white">
							<b>
								<?php echo $bbdayname; ?>, <?php echo $bbmonth; ?> <?php echo $bbday; ?>, <?php echo $year; ?>
							</b>
							<br/>
						</font>
						<p>
					</td>
				</tr>
			</table>
		</center>
		<p>

<?php
	/////LOGIN TABLES
	
	////////DEMO UNIT
	if( $businessid == 143 ) {
		$email = "demo@treatamerica.com";
		$demo_pass = "password";
	}
	else {
		$demo_user = "";
		$demo_pass = "";
	}
?>

		<center>
			<table width="90%" border="0" style="font-family: arial;">
				<tr valign="top">
					<td width="50%">
						<form action="/menu/login.php" method="post" name="login">
							<input type="hidden" name="mydate" value="<?php echo $mydate; ?>" />
							<input type="hidden" name="goto" value="<?php echo $goto; ?>" />
							<table width="97%" border="0" cellspacing="0" cellpadding="0" class="user_login">
								<thead>
									<tr>
										<th colspan="2">
											&nbsp;Sign In
										</th>
									</tr>
								</thead>
								<tbody>
									<tr height="4">
										<td></td>
										<td></td>
									</tr>
									<tr>
										<th class="label">
											Email:
										</th>
										<td class="input">
											<input type="text" name="username" size="30" value="<?php echo $email; ?>" />
										</td>
									</tr>
									<tr>
										<th class="label">
											Password:
										</th>
										<td class="input">
											<input type="password" name="password" size="30" value="<?php echo $demo_pass; ?>" />
										</td>
									</tr>
<?php
	if ( $email != '' ) {
		$showcheck = ' checked="checked"';
	}
	else {
		$showcheck = '';
	}
?>

									<tr>
										<td></td>
										<td class="input">
											<input type="checkbox" name="saveemail" value="1" <?php echo $showcheck; ?> />
											Save Email
										</td>
									</tr>
									<tr>
										<td></td>
										<td class="input">
											<input type="submit" value="Login" />
										</td>
									</tr>
									<tr>
										<td></td>
										<td class="input">
											<p>
											<br/>
											<a href="/menu/sendpass.htm">
												Forgot Your Password?
											</a> :: 
											<a href="/menu/intro.php?bid=<?php
												echo $businessid;
											?>">
												Home
											</a>
										</td>
									</tr>
								</tbody>
							</table>
						</form>
					</td>
					<td align="right" width="50%">
						<form action="/menu/accountcreate.php" method="post">
							<input type="hidden" name="goto" value="<?php echo $goto; ?>" />
							<table width="97%" border="0" cellspacing="0" cellpadding="0" class="user_login">
								<thead>
									<tr>
										<th colspan="2">
											&nbsp;Need a Login?
										</th>
									</tr>
								</thead>
								<tbody>
									<tr height="4">
										<td></td>
										<td></td>
									</tr>
									<tr>
										<th class="label" width="35%">
											Your Email:
										</th>
										<td class="input">
											<input type="text" name="username" size="30" />
										</td>
									</tr>
									<tr>
										<th class="label">
											Confirm Email:
										</th>
										<td class="input">
											<input type="text" name="confirmemail" size="30" />
										</td>
									</tr>
									<tr>
										<th class="label">
											Password:
										</th>
										<td class="input">
											<input type="password" name="password" size="30" />
										</td>
									</tr>
									<tr>
										<th class="label">
											Confirm Password:
										</th>
										<td class="input">
											<input type="password" name="confirmpassword" size="30" />
										</td>
									</tr>
									<tr>
										<th class="label">
											<b>Account#</b>
											<font color="red">*</font>:
										</th>
										<td class="input">
											<input type="text" name="account" size="30" />
										</td>
									</tr>
									<tr>
										<td align="right"></td>
										<td class="input">
											<font size="2">
												<font color="red">*</font>
												<i>
													Found on any prior invoice if you have ordered from 
													us before. You do not have to fill in the following 
													information if you have an account number. Please 
													leave blank if you do not have one and fill in the 
													rest of the information.
												</i>
											</font>
										</td>
									</tr>
									<tr>
										<th class="label">
											Your Name:
										</th>
										<td class="input">
											<input type="text" name="accountname" size="30" />
										</td>
									</tr>
									<tr>
										<th class="label">
											Address:
										</th>
										<td class="input">
											<input type="text" name="address" size="30" />
										</td>
									</tr>
									<tr>
										<th class="label">
											City:
										</th>
										<td class="input">
											<input type="text" name="city" size="30" />
										</td>
									</tr>
									<tr>
										<th class="label">
											State:
										</th>
										<td class="input">
											<input type="text" name="state" size="3" />
										</td>
									</tr>
									<tr>
										<th class="label">
											Zip:
										</th>
										<td class="input">
											<input type="text" name="zip" size="5" />
										</td>
									</tr>
									<tr>
										<th class="label">
											Phone#:
										</th>
										<td class="input">
											<input type="text" name="phone" size="30" />
										</td>
									</tr>
									<tr>
										<th class="label">
											Fax#:
										</th>
										<td class="input">
											<input type="text" name="fax" size="30" />
										</td>
									</tr>
									<tr>
										<th class="label">
											Tax Exempt#:
										</th>
										<td class="input">
											<input type="text" name="tax_exempt" size="30" />
										</td>
									</tr>
									<tr>
										<th class="label">
											Cost Center:
										</th>
										<td class="input">
											<input type="text" name="costcenter" size="30" />
										</td>
									</tr>
									<tr>
										<th class="label">
											<b>Location</b>
											<font color="blue">*</font>:
										</th>
										<td class="input">
											<select name="bid">
<?php

	$query = "SELECT * FROM business WHERE businessid = '$businessid' ORDER BY companyname DESC";
	$result = Treat_DB_ProxyOld::query( $query );
	$num = mysql_numrows( $result );

	$num--;
	while ( $r = mysql_fetch_array( $result ) ) {
		$bid = $r["businessid"];
		$businessname = $r["companyname"];

		if ( $businessid == $bid ) {
			$showsel = ' selected="selected"';
		}
		else {
			$showsel = '';
		}
?>
												<option value="<?php
													echo $bid;
												?>" <?php
													echo $showsel;
												?>><?php
													echo $businessname;
												?></option>
<?php
	}
?>

											</select>
										</td>
									</tr>
									<tr>
										<td></td>
										<td class="input">
											<font size="2">
												<font color="blue">*</font>
												<i>
													Please make sure you have the correct location. 
													Menus and pricing may vary between locations. 
													Ask for info at your local cafeteria if you are 
													unsure.
												</i>
											</font>
										</td>
									</tr>
									<tr>
										<td></td>
										<td class="input">
											<input type="submit" value="Create Account" />
										</td>
									</tr>
								</tbody>
							</table>
						</form>
					</td>
				</tr>
			</table>
		</center>
<?php
	google_page_track();
?>
	</body>
</html>
<?php
}

?>
