<?php
	//Time Outs [In Seconds]
		define("ATO_INIT_TO", 60);    // Initial Load Timeout
		define("ATO_MOUSE_TO", 60);   // Timeout After Mouse Move
		define("ATO_LOGIN_TO", 9);    // Time to wait before Login Fades
	
	//Images
		define("ATO_IMAGE_PATH", 'images/timeout');
		define("ATO_IMAGE_CYCLE", 3);  // Cycle images every [i] second(s)
?>
