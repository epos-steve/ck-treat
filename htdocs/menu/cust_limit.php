<?php

/*
function nextday($date2)
{
	$day=substr($date2,8,2);
	$month=substr($date2,5,2);
	$year=substr($date2,0,4);
	$leap = date("L");

	if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
	{
		if ($month == "01"){$month="02";}
		if ($month == "03"){$month="04";}
		if ($month == "05"){$month="06";}
		if ($month == "07"){$month="08";}
		if ($month == "08"){$month="09";}
		if ($month == "10"){$month="11";}
		$day='01';
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($month=='02' && $day == '29' && $leap == '0')
	{
		$month='03';
		$day='01';
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($month=='02' && $day == '28')
	{
		$month='03';
		$day='01';
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
	{
		if ($month == "04"){$month="05";}
		if ($month == "06"){$month="07";}
		if ($month == "09"){$month="10";}
		if ($month == "11"){$month="12";}
		$day='01';
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else if ($month=='12' && $day=='32')
	{
		$day='01';
		$month='01';
		$year++;
		$tomorrow = "$year-$month-$day";
		return $tomorrow;
	}
	else
	{
		$day=$day+1;
		if ($day<10){$day="0$day";}
		$tomorrow="$year-$month-$day";
		return $tomorrow;
	}
}

function money($diff){   
	$findme='.';
	$double='.00';
	$single='0';
	$double2='00';
	$pos1 = strpos($diff, $findme);
	$pos2 = strlen($diff);
	if ($pos1==""){$diff="$diff$double";}
	elseif ($pos2-$pos1==2){$diff="$diff$single";}
	elseif ($pos2-$pos1==1){$diff="$diff$double2";}
	else{}
	$dot = strpos($diff, $findme);
	$diff = substr($diff, 0, $dot+3);
	if ($diff > 0 && $diff < '0.01'){$diff="0.01";}
	elseif($diff < 0 && $diff > '-0.01'){$diff="-0.01";}
	return $diff;
}
*/

#include_once("../ta/db.php");
if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
require_once( DOC_ROOT . '/bootstrap.php' );

#$style = "text-decoration:none";

#$day = date("d");
#$year = date("Y");
#$month = date("m");
#$today = "$year-$month-$day";

$user = isset( $_COOKIE['usercook'] ) ? $_COOKIE['usercook'] : '';
$pass = isset( $_COOKIE['passcook'] ) ? $_COOKIE['passcook'] : '';
$customerid = isset( $_COOKIE['customerid'] ) ? $_COOKIE['customerid'] : '';

$type = isset( $_POST['type'] ) ? $_POST['type'] : '';
$value = isset( $_POST['value'] ) ? $_POST['value'] : '';

$limitid = isset( $_GET['limitid'] ) ? $_GET['limitid'] : '';
$del = isset( $_GET['del'] ) ? $_GET['del'] : '';

//mysql_connect($dbhost,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");

if ( $customerid < 1 ) {
	//echo "Fail";
}
elseif ( $del == 1 ) {
	$query = sprintf( '
			DELETE FROM customer_limits
			WHERE limitid = "%d"
		'
		,$limitid
	);
	$result = Treat_DB_ProxyOld::query( $query );
}
elseif ( $value > 0 && $type > 0 ) {
	$query = sprintf( '
			INSERT INTO customer_limits (
				customerid
				,type
				,value
			)
			VALUES
				( "%d", "%s", "%s" )
		'
		,$customerid
		,$type
		,$value
	);
	$result = Treat_DB_ProxyOld::query( $query );
}
//mysql_close();

#echo $query;

$location = '/menu/history.php?set=1';
header( 'Location: ' . $location );
