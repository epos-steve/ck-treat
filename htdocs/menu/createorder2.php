<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once(DOC_ROOT.'/bootstrap.php');

$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");
$bbdayname=date("l");
$bbmonth=date("F");
$bbday=date("j");
$startclose="$year-$month-01";
$month++;
if ($month<10){$month="0$month";}
if ($month==13){$month="01";$year++;}
$endclose="$year-$month-31";

$book = isset($_GET['book'])?$_GET['book']:'';
$date7 = isset($_GET['date'])?$_GET['date']:'';

$reserveid = isset($_GET['reserveid'])?$_GET['reserveid']:'';
$goto = isset($_GET['goto'])?$_GET['goto']:'';

$user = \EE\Controller\Base::getSessionCookieVariable( 'usercook' );
$pass = \EE\Controller\Base::getSessionCookieVariable( 'passcook' );
$accountid = \EE\Controller\Base::getSessionCookieVariable( 'accountid' );
$businessid = \EE\Controller\Base::getSessionCookieVariable( 'businessid' );
$curmenu = \EE\Controller\Base::getSessionCookieVariable( 'curmenu' );

if($reserveid==""){$reserveid = \EE\Controller\Base::getSessionCookieVariable( 'reserveid' );}

if ($accountid<1){
    $location="logout.php?goto=3";
    header('Location: ./' . $location);
}
elseif ($businessid<1){
    $location="index.php";
    header('Location: ./' . $location);
}
else
{

	\EE\Model\Business\Setting::db( Treat_DB::singleton() );
	$settings = \EE\Model\Business\Setting::get("business_id = ". abs(intval($businessid)), true);
	$background_image_logo = $settings->background_image ?: '';
	$site_logo = $settings->site_logo ?: 'logo.jpg';

?>
<head>

<script language="JavaScript"
   type="text/JavaScript">
function popup_file(url)
  {
     popwin=window.open(url,"AttachFile","location=no,menubar=no,titlebar=no,resizeable=no,height=100,width=450");
  }
</script>

<SCRIPT LANGUAGE="JavaScript" SRC="CalendarPopup.js"></SCRIPT>

<!-- This prints out the default stylehseets used by the DIV style calendar.
     Only needed if you are using the DIV style popup -->
<SCRIPT LANGUAGE="JavaScript">document.write(getCalendarStyles());</SCRIPT>

<STYLE>
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation
			{
			background-color:#6677DD;
			text-align:center;
			vertical-align:center;
			text-decoration:none;
			color:#FFFFFF;
			font-weight:bold;
			}
	.TESTcpDayColumnHeader,
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation,
	.TESTcpCurrentMonthDate,
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDate,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDate,
	.TESTcpCurrentDateDisabled,
	.TESTcpTodayText,
	.TESTcpTodayTextDisabled,
	.TESTcpText
			{
			font-family:arial;
			font-size:8pt;
			}
	TD.TESTcpDayColumnHeader
			{
			text-align:right;
			border:solid thin #6677DD;
			border-width:0 0 1 0;
			}
	.TESTcpCurrentMonthDate,
	.TESTcpOtherMonthDate,
	.TESTcpCurrentDate
			{
			text-align:right;
			text-decoration:none;
			}
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDateDisabled
			{
			color:#D0D0D0;
			text-align:right;
			text-decoration:line-through;
			}
	.TESTcpCurrentMonthDate
			{
			color:#6677DD;
			font-weight:bold;
			}
	.TESTcpCurrentDate
			{
			color: #FFFFFF;
			font-weight:bold;
			}
	.TESTcpOtherMonthDate
			{
			color:#808080;
			}
	TD.TESTcpCurrentDate
			{
			color:#FFFFFF;
			background-color: #6677DD;
			border-width:1;
			border:solid thin #000000;
			}
	TD.TESTcpCurrentDateDisabled
			{
			border-width:1;
			border:solid thin #FFAAAA;
			}
	TD.TESTcpTodayText,
	TD.TESTcpTodayTextDisabled
			{
			border:solid thin #6677DD;
			border-width:1 0 0 0;
			}
	A.TESTcpTodayText,
	SPAN.TESTcpTodayTextDisabled
			{
			height:20px;
			}
	A.TESTcpTodayText
			{
			color:#6677DD;
			font-weight:bold;
			}
	SPAN.TESTcpTodayTextDisabled
			{
			color:#D0D0D0;
			}
	.TESTcpBorder
			{
			border:solid thin #6677DD;
			}
</STYLE>
</head>
<body background="<?php echo $background_image_logo; ?>">
<form action="updatereserve.php" method="POST">
	<input type="hidden" name="reserveid" value="<?php echo intval($reserveid); ?>">
	<input type="hidden" name="goto" value="<?php echo $goto; ?>">
	<div style="padding: 10px;">
		<center>
			<table cellspacing="0" cellpadding="0" border="0" width="90%">
				<tr>
					<td colspan="4">
						<img src='<?php echo $site_logo; ?>' width="205" height="43">
					</td>
				</tr>
<?php
    $query = "SELECT * FROM company WHERE companyid = '1'";
    $result = Treat_DB_ProxyOld::query($query, true);

    $companyname=@mysql_result($result,0,"companyname");

    $query = "SELECT * FROM business WHERE businessid = '$businessid'";
    $result = Treat_DB_ProxyOld::query($query, true);

    $businessname=@mysql_result($result,0,"companyname");

    $query = "SELECT * FROM accounts WHERE accountid = '$accountid'";
    $result = Treat_DB_ProxyOld::query($query, true);

    $accountname=@mysql_result($result,0,"name");
    $accountnum=@mysql_result($result,0,"accountnum");
    $mycostcenter=@mysql_result($result,0,"costcenter");

    $logout="<font color=white>Sign Off</font>";

    echo "<tr bgcolor=#6699FF><td colspan=4><font color=white size=3><b>The Right Choice... for a Healthier You!</td></tr>";
    echo "<tr bgcolor=#669933><td width=10%><img src=rc-header2.jpg height=80 width=108></td><td><font size=4 color=white><b>$companyname - $businessname</b></font><br><b><font color=white>Account: $accountname<br>Account #: $accountnum</td><td colspan=2 align=right valign=top><font color=white><b>$bbdayname, $bbmonth $bbday, $year</b><br><font size=2>[<a style=$style href=logout.php?goto=1><b><font color=white>$logout</font></b></a>]</font><p>$showcart</td></tr>";
    echo "<tr bgcolor=black><td height=1 colspan=4></td></tr>";
    echo "</table></center>";

//////////////////ORDER DETAILS

    $query = "SELECT * FROM business WHERE businessid = '$businessid'";
    $result = Treat_DB_ProxyOld::query($query, true);

    $caterdays=@mysql_result($result,0,"caterdays");
    $weekend=@mysql_result($result,0,"weekend");

    $query = "SELECT * FROM caterclose WHERE businessid = '$businessid' AND date >= '$startclose' AND date <= '$endclose'";
    $result = Treat_DB_ProxyOld::query($query, true);
    $num=mysql_numrows($result);
    
    $num--;
    $closedmessage="";
    while ($num>=0){
       $dateclose=@mysql_result($result,$num,"date");
       $closedmessage="$closedmessage cal18.addDisabledDates('$dateclose');";
       $num--;
    }
?>

		<p><br><center>
			<table width=90%  cellpadding=0 cellspacing=0 style='filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr=#F7F7F7, startColorstr=#E8E7E7, gradientType=1);'>
				<tr>
					<td width=1% height=1%><img src='dgrayur.gif' width=20 length=20></td>
					<td colspan=2></td>
					<td width=1% height=1%><img src=lgrayur.gif height=20 width=20></td>
				</tr>
				<tr>
					<td></td>
					<td colspan=2><img src=details.gif height=36 width=122></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td colspan="2">
						<?php echo $_SESSION->getFlashError('<div style="color: red;">%s</div>', '</p><p>', '<p>', '</p>'); ?>
					</td>
					<td></td>
				</tr>
<?php
	if ($book==1){echo "<tr><td></td><td colspan=2><b><font color=red>That room is already booked.</td><td></td></tr>";} 
	elseif ($book==2){echo "<tr><td></td><td colspan=2><b><font color=red>Please specify the number of people.</td><td></td></tr>";} 
	elseif ($book==3){echo "<tr><td></td><td colspan=2><b><font color=red>Your End Time is before your Start Time.</td><td></td></tr>";} 
	elseif ($book==4){echo "<tr><td></td><td colspan=2><b><font color=red>That date has already past.</td><td></td></tr>";}   
	elseif ($book==6){echo "<tr><td></td><td colspan=2><b><font color=blue>Catering Successfully Scheduled.</td><td></td></tr>";}
	elseif ($book==7){echo "<tr><td></td><td colspan=2><b><font color=red>Please enter a Reference.</td><td></td></tr>";} 
	elseif ($book==8){echo "<tr><td></td><td colspan=2><b><font color=red>That Day is already Full.</td><td></td></tr>";}
	elseif ($book==9){echo "<tr><td></td><td colspan=2><b><font color=red>You must schedule at least $caterdays day(s) in advance.</td><td></td></tr>";} 
	elseif ($book==10){echo "<tr><td></td><td colspan=2><b><font color=red>Sorry, we are closed weekends.</td><td></td></tr>";}  
	elseif ($book==11){echo "<tr><td></td><td colspan=2><b><font color=red>Sorry, we are closed that day.</td><td></td></tr>";}    
	elseif ($book==5){echo "<tr><td></td><td colspan=2><b><font color=red>Please specify why location is N/A.</td><td></td></tr>";}           

	if ($date7!=""){$today=$date7;}
	if ($weekend==0){$showweekend="cal18.setDisabledWeekDays(0,6)";}
	else{$showweekend="";}

	$comment = isset($_GET['comment'])?$_GET['comment']:'';
	if( isset($_SESSION['createorder2postdata']) && trim($_SESSION['createorder2postdata']['comment']) !== '' )
	{
		$comment = trim($_SESSION['createorder2postdata']['comment']);
	}
	$roomcomment = isset($_GET['roomcomment'])?$_GET['roomcomment']:'';
	if( isset($_SESSION['createorder2postdata']) && trim($_SESSION['createorder2postdata']['roomcomment']) !== '' )
	{
		$roomcomment = trim($_SESSION['createorder2postdata']['roomcomment']);
	}
	$people = isset($_GET['people'])?$_GET['people']:'';
	if( isset($_SESSION['createorder2postdata']) && intval($_SESSION['createorder2postdata']['peoplenum']) > 0 )
	{
		$people = intval($_SESSION['createorder2postdata']['peoplenum']);
	}
	$start_hour = isset($_GET['start_hour'])?$_GET['start_hour']:'';
	$end_hour = isset($_GET['end_hour'])?$_GET['end_hour']:'';
	$sel_room = isset($_GET['roomid'])?$_GET['roomid']:'';
	if( isset($_SESSION['createorder2postdata']) && intval($_SESSION['createorder2postdata']['room']) > 0 )
	{
		$sel_room = intval($_SESSION['createorder2postdata']['room']);
	}
	$costcenter = isset($_GET['costcenter'])?$_GET['costcenter']:'';
	if( isset($_SESSION['createorder2postdata']) && trim($_SESSION['createorder2postdata']['costcenter']) !== '' )
	{
		$costcenter = trim($_SESSION['createorder2postdata']['costcenter']);
	}
	if ($costcenter==""){$costcenter=$mycostcenter;}
?>
				<tr>
					<td></td>
					<td align="right" width="20%">Date:</td>
					<td>
						<script language='javascript' type="text/javascript">
						var now = new Date();
						var cal18 = new CalendarPopup('testdiv1');
						cal18.addDisabledDates(null,formatDate(now,'yyyy-MM-dd'));
						<?php echo $closedmessage.';'; echo $showweekend.';'; ?>
						cal18.setCssPrefix('TEST');
						</script>
						<input type="text" name='date' value="<?php
						if (isset($_SESSION['createorder2postdata']) && trim($_SESSION['createorder2postdata']['date']) !== '') :
							echo $_SESSION['createorder2postdata']['date'];
						else :
							echo date('Y-m-d', strtotime('+2 days'));
						endif;
						?>" size="8">
						<a href='javascript:void(0)'
						   onClick="cal18.select(document.forms[0]['date'],'anchor18','yyyy-MM-dd'); return false;"
						   title="cal18.select(document.forms[0].date,'anchor1x','yyyy-MM-dd'); return false;"
						   name='anchor18'
						   id='anchor18'>
							<img src="calendar.gif" border=0 height=15 width=16 alt='Choose a Date'>
						</a>
					</td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td align="right">Reference: </td>
					<td><input type="text" name="comment" size="30" value='<?php echo $comment; ?>'></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td align="right"># of People: </td>
					<td><input type="text" name="peoplenum" size="8" value='<?php echo $people; ?>'></td>
					<td></td>
				</tr>

<?php
	$query = "SELECT * FROM rooms WHERE businessid = '$businessid' AND deleted = '0' ORDER BY roomname DESC";
	$result = Treat_DB_ProxyOld::query($query, true);
	$num = mysql_numrows($result);
?>
				<tr>
					<td></td>
					<td align=right>Location: </td>
					<td>
						<select name=room>
							<option value=0>N/A</option>
<?php
	$num--;
	while ($num>=0){
		$roomname=@mysql_result($result,$num,"roomname");
		$roomid=@mysql_result($result,$num,"roomid");
		if ($roomid==$sel_room){$selroom="SELECTED";}
		else {$selroom="";}
		echo "<option value=$roomid $selroom>$roomname</option>";
		$num--;
	}
?>
						</select>
					</td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td>
						<?php /* 
						<i>*If Location is N/A, Please specify. Include any other details below:</i><br>
						 */ ?>
						<i>*Please include any other details below. If location is N/A, please specify.</i><br>
						<textarea name=roomcomment rows=3 cols=60><?php echo $roomcomment; ?></textarea>
					</td>
					<td></td>
				</tr>

<?php
	if ( '' !== trim($start_hour) )
	{
		$start_min = substr($start_hour,2,2);
		$start_hour = substr($start_hour,0,2);
	}
	else
	{
		$start_hour = 10;
		$start_min = '00';
	}

	if ( '' !== trim($end_hour) )
	{
		$end_min = substr($end_hour,2,2);
		$end_hour = substr($end_hour,0,2);
	}
	else
	{
		$end_hour = 12;
		$end_min = '00';
	}

	if( isset($_SESSION['createorder2postdata']) && trim($_SESSION['createorder2postdata']['start_min']) !== '' )
	{
		$start_min = intval($_SESSION['createorder2postdata']['start_min']);
	}
	if( isset($_SESSION['createorder2postdata']) && trim($_SESSION['createorder2postdata']['end_min']) !== '' )
	{
		$end_min = intval($_SESSION['createorder2postdata']['end_min']);
	}
	if( isset($_SESSION['createorder2postdata']) && trim($_SESSION['createorder2postdata']['start_hour']) !== '' )
	{
		$start_hour = intval($_SESSION['createorder2postdata']['start_hour']);
		if( strcasecmp($_SESSION['createorder2postdata']['start_am'], 'pm') === 0 )
		{
			if( $start_hour[0] == '0' )
			{
				$start_hour = intval(substr($start_hour, 1));
			}
			$start_hour += 12;
		}
	}
	if( isset($_SESSION['createorder2postdata']) && trim($_SESSION['createorder2postdata']['end_hour']) !== '' )
	{
		$end_hour = intval($_SESSION['createorder2postdata']['end_hour']);
		if( strcasecmp($_SESSION['createorder2postdata']['end_am'], 'pm') === 0 )
		{
			if( $end_hour[0] == '0' )
			{
				$end_hour = intval(substr($end_hour, 1));
			}
			$end_hour += 12;
		}
	}
	$actualStartHourValue = $start_hour;
	if ( $start_hour > 12 )
	{
		$actualStartHourValue = intval($start_hour) - 12;
	}
	if ($actualStartHourValue < 10)
	{
		$actualStartHourValue = '0'.$actualStartHourValue;
	}
	$actualEndHourValue = $end_hour;
	if ( $end_hour > 12 )
	{
		$actualEndHourValue = intval($end_hour) - 12;
	}
	if ($actualEndHourValue < 10)
	{
		$actualEndHourValue = '0'.$actualEndHourValue;
	}
?>

				<tr>
					<td></td>
					<td align=right>Start Time:</td>
					<td>
						<select name="start_hour">
<?php
	foreach (range(1, 12, 1) as $hour) :
		$hourValue = $hour < 10 ? '0'.$hour : $hour;
?>
							<option value="<?php echo $hourValue; ?>"<?php if ( $actualStartHourValue == $hourValue ) : ?> selected="selected"<?php endif; ?>><?php echo $hour; ?></option>
<?php
endforeach;
?>
						</select>
						<b>:</b>
						<select name="start_min">
<?php
	foreach (range(0, 55, 5) as $minute) :
		$minuteValue = $minute < 10 ? '0'.$minute : $minute;
?>
							<option value="<?php echo $minuteValue; ?>"<?php if ( $start_min == $minuteValue ) : ?> selected="selected"<?php endif; ?>><?php echo $minuteValue; ?></option>
<?php
endforeach;
?>
						</select>
						<select name="start_am">
							<option value='AM'>AM</option>
							<option value='PM'<?php if ($start_hour >= 12) : ?> selected="selected"<?php endif; ?>>PM</option>
						</select>
					</td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td align=right>End Time:</td>
					<td>
						<select name="end_hour">
<?php
	foreach (range(1, 12, 1) as $hour) :
		$hourValue = $hour < 10 ? '0'.$hour : $hour;
?>
							<option value="<?php echo $hourValue; ?>"<?php if ( $actualEndHourValue == $hourValue ) : ?> selected="selected"<?php endif; ?>><?php echo $hour; ?></option>
<?php
endforeach;
?>
						</select>
						<b>:</b>
						<select name="end_min">
<?php
	foreach (range(0, 55, 5) as $minute) :
		$minuteValue = $minute < 10 ? '0'.$minute : $minute;
?>
							<option value="<?php echo $minuteValue; ?>"<?php if ( $end_min == $minuteValue ) : ?> selected="selected"<?php endif; ?>><?php echo $minuteValue; ?></option>
<?php
endforeach;
?>
						</select>
						<select name="end_am">
							<option value='AM'>AM</option>
							<option value='PM'<?php if ($end_hour >= 12) : ?> selected="selected"<?php endif; ?>>PM</option>
						</select>
					</td>
					<td></td>
				</tr>
<?php
	////////COST CENTER
	$query43 = "SELECT * FROM customer_costcenter WHERE businessid = '$businessid' ORDER BY name DESC";
	$result43 = Treat_DB_ProxyOld::query($query43, true);
	$num43=mysql_numrows($result43);

	$num43--;

	if ($num43 > 0)
	{
?>

				<tr>
					<td></td>
					<td align=right>Cost Center:</td>
					<td>
						<select name="costcenter">
							<option value=""></option>
<?php
		while($num43>=0)
		{
			$cc_name=@mysql_result($result43,$num43,"name");
			$cc_number=@mysql_result($result43,$num43,"number");
?>
							<option value='<?php echo $cc_number; ?>'<?php if($cc_number == $costcenter) : ?> selected="selected"<?php endif; ?>>
								<?php echo $cc_name; ?> (<?php echo $cc_number; ?>)
							</option>
<?php
			$num43--;
		}
?>
						</select>
					</td>
					<td></td>
				</tr>
<?php
	}
	else
	{
?>

				<tr>
					<td></td>
					<td align=right>Cost Center:</td>
					<td><input type="text" name="costcenter" size="18" value='<?php echo $costcenter; ?>'></td>
					<td></td>
				</tr>
<?php
	}

	///////////////////////////////
	///////custom fields///////////
	///////////////////////////////
	$query_custom = "SELECT * FROM invoice_custom_fields WHERE businessid = '$businessid' AND active = '0' AND customer = '1'";
	$result_custom = Treat_DB_ProxyOld::query($query_custom, true);
	$num_custom = mysql_num_rows($result_custom);

	if($num_custom > 0)
	{
		while($r_cust=mysql_fetch_array($result_custom))
		{
			$cust_id=$r_cust["id"];
			$customFieldValue = false;
			if( isset($_SESSION['createorder2postdata']) && trim($_SESSION['createorder2postdata']['cust'.$cust_id]) !== '' )
			{
				$customFieldValue = $_SESSION['createorder2postdata']['cust'.$cust_id];
			}
			$cust_field_name=$r_cust["field_name"];
			$cust_type=$r_cust["type"];
			$cust_required=$r_cust["required"];

			if($cust_required==1){$showrequired="<font color=red>*</font>";}
			else{$showrequired="";}

			/////display text
			if ($cust_type == 0)
			{
				$query_custom2 = "SELECT value FROM invoice_custom_values WHERE reserveid = '$reserveid' AND customid = '$cust_id'";
				$result_custom2 = Treat_DB_ProxyOld::query($query_custom2, true);

				$cust_value = @mysql_result($result_custom2,0,"value");
				if ($customFieldValue)
				{
					$cust_value = $customFieldValue;
				}
				$cust_value = htmlentities($cust_value, \ENT_COMPAT, 'UTF-8', false);
?>

				<tr>
					<td></td>
					<td align=right><font color=#666666><?php echo $cust_field_name; ?></font>: </td>
					<td> <input type=text size=20 name='cust<?php echo $cust_id; ?>' value="<?php echo $cust_value; ?>"></td>
					<td></td>
				</tr>
<?php
			}
			////display select
			else if ($cust_type == 1)
			{
				$query_custom2 = "SELECT value FROM invoice_custom_values WHERE reserveid = '$reserveid' AND customid = '$cust_id'";
				$result_custom2 = Treat_DB_ProxyOld::query($query_custom2, true);

				$cust_value = @mysql_result($result_custom2,0,"value");
				if ($customFieldValue)
				{
					$cust_value = $customFieldValue;
				}
?>

				<tr>
					<td></td>
					<td align=right><font color="#666666"><?php echo $cust_field_name; ?></font>: </td>
					<td>
						<select name='cust<?php echo $cust_id; ?>'>
<?php
				$query_custom2 = "SELECT value FROM invoice_custom_fields_select WHERE customid = '$cust_id'";
				$result_custom2 = Treat_DB_ProxyOld::query($query_custom2, true);

				while($r_sel=mysql_fetch_array($result_custom2))
				{
					$cust_value_sel = $r_sel['value'];
					if($cust_value_sel == $cust_value){$sel = "SELECTED";}
					else{$sel="";}
?>

							<option value='<?php echo $cust_value_sel; ?>' <?php echo $sel; ?>><?php echo $cust_value_sel; ?></option>";
<?php
				}
?>

						</select>
					</td>
					<td></td>
				</tr>
<?php
			}
			//////display date
			else if ($cust_type == 2)
			{
				$query_custom2 = "SELECT value FROM invoice_custom_values WHERE reserveid = '$reserveid' AND customid = '$cust_id'";
				$result_custom2 = Treat_DB_ProxyOld::query($query_custom2, true);

				$cust_value = @mysql_result($result_custom2,0,"value");

				// Replace current value with old POST data.
				if ($customFieldValue)
				{
					$cust_value = $customFieldValue;
				}

				// Sanitize
				$checkTime = strtotime($cust_value);
				if ( $checkTime > 315554400 ) // Check greater than 1980-01-01
				{
					$cust_value = date('Y-m-d', $checkTime);
				}

				$calcount = $cust_id + 99;
?>
				
				<tr>
					<td></td>
					<td align=right><font color=#666666><?php echo $cust_field_name; ?></font>: </td>
					<td>
						<script language='JavaScript' id='cust<?php echo $calcount; ?>'>
						var cal<?php echo $calcount; ?> = new CalendarPopup('testdiv1');
						cal<?php echo $calcount; ?>.setCssPrefix('TEST');
						</script>
						<input type="text" name='cust<?php echo $cust_id; ?>' value='<?php echo $cust_value; ?>' size="8">
						<a href="javascript:void(0);"
						   onClick="cal<?php echo $calcount; ?>.select(document.forms[0].cust<?php echo $cust_id; ?>,'anchor<?php echo $calcount; ?>','yyyy-MM-dd'); return false;"
						   title="cal<?php echo $calcount; ?>.select(document.forms[0].cust<?php echo $cust_id; ?>,'anchor1x','yyyy-MM-dd'); return false;"
						   name='anchor<?php echo $calcount; ?>'
						   id='anchor<?php echo $calcount; ?>'>
							<img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'>
						</a>
					</td>
					<td></td>
				</tr>
<?php
			}
			//////upload file
			else if ($cust_type == 4)
			{
				/*
				$query_custom2 = "SELECT id,value FROM invoice_custom_values WHERE reserveid = '$reserveid' AND customid = '$cust_id'";
				$result_custom2 = Treat_DB_ProxyOld::query($query_custom2, true);

				$cust_value = @mysql_result($result_custom2,0,"value");
				$id = @mysql_result($result_custom2,0,"id");

				$add_file = "<a onclick=popup_file(\"../ta/customerFile.php?cust_id=$cust_id&reserveid=$reserveid&cid=$companyid&bid=$businessid&todo=0\")><img src=../ta/attach.png border=0 alt='Attach File' onmouseover=\"this.style.cursor = 'hand';\"></a>";
				if($id>0){$del_file = "<a href=../ta/customerFile.php?cust_id=$cust_id&id=$id&reserveid=$reserveid&cid=$companyid&bid=$businessid&todo=1&goto=2 onclick=\"return confirm('Are you sure');\"><img src=delete.gif border=0 alt='Delete File'></a>";}

				echo "<tr><td></td><td align=right><font color=#666666>$showrequired$cust_field_name</font>: </td><td> <a href='".HTTP_CUSTOMER_FILES."/$id-$cust_value' target='_blank' style=$style><font color=blue>$cust_value</font></a> $add_file $del_file</td><td></td></tr>";
				*/
			}
			/////display text
			else if ($cust_type == 5)
			{
				$query_custom2 = "SELECT value FROM invoice_custom_values WHERE reserveid = '$reserveid' AND customid = '$cust_id'";
				$result_custom2 = Treat_DB_ProxyOld::query($query_custom2, true);

				$cust_value = @mysql_result($result_custom2,0,"value");
				if ($customFieldValue)
				{
					$cust_value = $customFieldValue;
				}
				$cust_value = htmlspecialchars ($cust_value, \ENT_COMPAT, 'UTF-8', false);
?>

				<tr>
					<td></td>
					<td align=right><font color=#666666><?php echo $cust_field_name; ?></font>: </td>
					<td> <textarea name='cust<?php echo $cust_id; ?>' style='width: 50%; height: 150px;'><?php echo $cust_value; ?></textarea></td>
					<td></td>
				</tr>
<?php
			}
		}
	}
	//////////////////////////////
	//////end custom fields///////
	//////////////////////////////
?>

				<tr>
					<td></td>
					<td></td>
					<td>
						<br>
						<input type=hidden name=comefrom value=1>
						<input type=submit value='Confirm Details'>
					</td>
					<td></td>
				</tr>
				<tr>
					<td><img src='dgrayll.gif' width=20 length=20></td>
					<td colspan=2></td>
					<td><img src="lgraylr.gif" height=20 width=20></td>
				</tr>
			</table>
		</center>
		<div id="testdiv1" style="position:absolute; visibility:hidden; background-color:white; layer-background-color:white;"></div>
		<p><br><p><br><p><br><p>
		<center><a href="http://essentialpos.com" target='_blank'><img src="Logo5.jpg" width=150 height=71 border=0></a></center>
<?php
}

// Make sure post data is not saved after it is used. If refreshed, data may revert.
unset($_SESSION['createorder2postdata']);

google_page_track();
?>
	</body>
</html>