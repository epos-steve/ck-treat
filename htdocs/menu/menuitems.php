<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once(DOC_ROOT.'/bootstrap.php');

$setmenu = null; # Notice: Undefined variable
$showmyaccount = null; # Notice: Undefined variable
$showrealuser = null; # Notice: Undefined variable
$aliasid = null; # Notice: Undefined variable
$businessname2 = null; # Notice: Undefined variable
$showsaveorder = null; # Notice: Undefined variable

$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");
$bbdayname=date("l");
$bbmonth=date("F");
$bbday=date("j");

$user = \EE\Controller\Base::getSessionCookieVariable( 'usercook' );
$pass = \EE\Controller\Base::getSessionCookieVariable( 'passcook' );
$accountid = \EE\Controller\Base::getSessionCookieVariable( 'accountid' );
$businessid = \EE\Controller\Base::getPostGetSessionOrCookieVariable( array( 'bid', 'businessid' ) );
$curmenu = \EE\Controller\Base::getSessionCookieVariable( 'curmenu' );
$reserveid = \EE\Controller\Base::getSessionCookieVariable( 'reserveid' );
$realuser = \EE\Controller\Base::getSessionCookieVariable( 'realuser' );

////search
$search = \EE\Controller\Base::getPostVariable( 'search' );
if ( $search != "" ) {
	\EE\Controller\Base::setCookie( "search", $search );
}
else {
	$search = \EE\Controller\Base::getSessionCookieVariable( 'search' );
}

if ( $setmenu > 0 ) {
	\EE\Controller\Base::setCookie( "curmenu", $setmenu );
	$curmenu = $setmenu;
}

$groupid = \EE\Controller\Base::getGetVariable( 'groupid' );
$setmenu = \EE\Controller\Base::getGetVariable( 'setmenu' );
$showqty = \EE\Controller\Base::getGetVariable( 'qty' );
$showname = \EE\Controller\Base::getGetVariable( 'showname' );
$showme = \EE\Controller\Base::getGetVariable( 'showme' );

$debug = \EE\Controller\Base::getGetVariable( 'debug' );

$businessid = intval( $businessid );
$reserveid = intval( $reserveid );
$accountid = intval( $accountid );

#$accountid = 1;
#$reserveid = 266872;
#$showqty = 1;
#$showme = 1;
#$showname = 1;
#$curmenu = 'apple';

# temporary holding to see which variables we're actually using
$pageVariables = new \EE\ArrayObject();
$pageVariables->businessId = $businessid;
$pageVariables->accountId = $accountid;
$pageVariables->customerId = null;
$pageVariables->currentMenu = $curmenu;
if ( $setmenu > 0 ) {
	$pageVariables->currentMenu = $setmenu;
}
$pageVariables->reserveId = $reserveid;
#$pageVariables->realUser = $realuser;
$pageVariables->groupId = $groupid;
$pageVariables->showQty = $showqty;
$pageVariables->showName = $showname;
$pageVariables->showMe = $showme;
$pageVariables->search = $search;
$pageVariables->debug = $debug;

if ( $businessid == "" ) {
	$location = "index.php";
	header( 'Location: ./' . $location );
	exit();
}
else
{

	$pageCompany = \EE\Model\Company\Singleton::getSingleton( 1 );
	if ( !$pageCompany ) {
		$pageCompany = \EE\Null();
	}
	$pageBusiness = \EE\Model\Business\Singleton::getSingleton( $pageVariables->businessId );
	if ( !$pageBusiness ) {
		$pageBusiness = \EE\Null();
	}
	
	if ( $pageVariables->currentMenu == $pageBusiness->default_menu && $pageBusiness->default_menu_alias > 0 ) {
		$aliasid = $pageBusiness->default_menu_alias;
		$pageVariables->aliasId = $pageBusiness->default_menu_alias;
	}
	elseif ( $pageVariables->currentMenu == $pageBusiness->cafe_menu && $pageBusiness->cafe_menu_alias > 0 ) {
		$aliasid = $pageBusiness->cafe_menu_alias;
		$pageVariables->aliasId = $pageBusiness->cafe_menu_alias;
	}
	elseif ( $pageVariables->currentMenu == $pageBusiness->vend_menu && $pageBusiness->vend_menu_alias > 0 ) {
		$aliasid = $pageBusiness->vend_menu_alias;
		$pageVariables->aliasId = $pageBusiness->vend_menu_alias;
	}
	
	$currentMenuIsNotDefault = ( $pageVariables->currentMenu != $pageBusiness->default_menu );
	$displayNutritionHealth = ( !$pageBusiness->no_nutrition && $currentMenuIsNotDefault );
	$showMeNutritionHealth = ( $pageVariables->showMe && $displayNutritionHealth );
	
	$pageGroup = new \EE\ArrayObject();
	if ( -1 == $pageVariables->groupId ) {
		$pageGroup->groupname = '<span class="menu_group_favorite"><img src="/assets/images/star.gif" alt="Favorites" /> Favorites</span>';
		$pageGroup->image = '/assets/images/favorites.png';
		$pageGroup->imageSrc = $pageGroup->image;
		$pageGroup->imageAlt = 'Favorites';
		$pageGroup->description = 'Your 10 Most Frequent Purchases.';
		$pageGroup->img_height = 90;
		$pageGroup->img_width = 90;
	}
	elseif ( $pageVariables->groupId < 1 && $pageVariables->search ) {
		$pageGroup->groupname = '</i>Search Results for: <i>' . $pageVariables->search;
		$pageGroup->imageAlt = 'Search Results';
#		$pageGroup->image = '/menu/pictures/lunch.jpg';
		$pageGroup->description = '';
		$pageGroup->img_height = 90;
		$pageGroup->img_width = 90;
	}
	else {
		$tmp = \EE\Model\Menu\GroupOriginal::get( intval( $pageVariables->groupId ), true );
		if ( $tmp ) {
			$pageGroup = $tmp;
			$pageGroup->imageAlt = strip_tags( $pageGroup->groupname );
		}
	}
	if ( !$pageGroup->imageSrc ) {
		if ( $pageGroup->image ) {
			$img = $pageGroup->image;
		}
		else {
			$img = 'lunch.jpg';
		}
		$pageGroup->imageSrc = '/menu/pictures/' . $img;
	}
	
	$reserveObj = new \EE\Null();
	$pageVariables->qtyValue = 1;
	$orderDetails = array();
	if ( $pageVariables->reserveId > 0 ) {
		///////find number of people for catering
		if ( $pageBusiness->allow_sub_orders ) {
			$reserveObj = \EE\Model\Menu\Reserve::get( intval( $pageVariables->reserveId ), true );
			$pageVariables->qtyValue = $reserveObj->peoplenum;
		}
		$orderDetails = \EE\Model\Invoice\Detail::getByReserveId( $pageVariables->reserveId, $class = null );
	}
	
	$menuGroups = \EE\Model\Menu\GroupOriginal::getMenuTypeGroups(
		$pageVariables->currentMenu
		,$pageVariables->businessId
	);
	$menuGroupLinksQuery = array(
		'bid' => $pageVariables->businessId,
		'groupid' => null,
		'setmenu' => $pageVariables->currentMenu,
		'showme' => $pageVariables->showMe,
	);
	$menuLinksQuery = array(
		'bid' => $pageVariables->businessId,
		'reserveid' => $pageVariables->reserveId,
		'account' => $pageVariables->accountId,
		'showme' => $pageVariables->showMe,
	);
	
	$menuType = \EE\Model\Menu\Type::get( intval( $pageVariables->currentMenu ), true );
	
/*
*/
	$menuItems = \EE\Model\Menu\Item::getListByGroupId(
		$pageVariables->groupId
		,$pageVariables->search
		,$pageVariables->aliasId
		,$pageVariables->accountId
		,$pageVariables->businessId
		,$pageVariables->currentMenu
		,$class = '\EE\ArrayObject'
	);
	$menuItemIds = \EE\Model\Menu\Item::getMenuItemIds();
/*
* /
#	$menuItems = array();
	$menuItem = new \EE\ArrayObject( array(
		'menu_item_id' => 53822,
		'businessid' => 274,
		'companyid' => 1,
		'item_name' => 'Combo Pizza - 16"',
		'price' => 12.5,
		'description' => 'Pizza includes sausage, pepperoni, peppers, onions and black olives.  Each pizza will give you 8 slices.',
		'groupid' => 7,
		'unit' => 'piece',
		'menu_typeid' => 1,
		'image' => 'thumbnailCAXHKH6tiyu',
		'optionGroups' => new \EE\ArrayObject( array(
			58 => new \EE\ArrayObject( array(
				'optiongroupid' => 58,
				'menu_item_id' => 594,
				'description' => 'Your Choise of Meat',
				'type' => 0,
				'orderid' => 0,
				'options' => new \EE\ArrayObject( array(
					128 => new \EE\ArrayObject( array(
						'optionid' => 128,
						'optiongroupid' => 58,
						'description' => 'Beef',
						'price' => 0,
						'orderid' => 0,
						'noprint' => 0,
						'menu_itemid' => 0,
					) ),
					129 => new \EE\ArrayObject( array(
						'optionid' => 129,
						'optiongroupid' => 58,
						'description' => 'Chicken',
						'price' => 0,
						'orderid' => 0,
						'noprint' => 0,
						'menu_itemid' => 0,
					) ),
					130 => new \EE\ArrayObject( array(
						'optionid' => 130,
						'optiongroupid' => 58,
						'description' => 'Shrimp',
						'price' => 0,
						'orderid' => 0,
						'noprint' => 0,
						'menu_itemid' => 0,
					) ),
					131 => new \EE\ArrayObject( array(
						'optionid' => 131,
						'optiongroupid' => 58,
						'description' => 'Beef and Chicken',
						'price' => 0,
						'orderid' => 0,
						'noprint' => 0,
						'menu_itemid' => 0,
					) ),
					132 => new \EE\ArrayObject( array(
						'optionid' => 132,
						'optiongroupid' => 58,
						'description' => 'Beef and Shrimp',
						'price' => 0,
						'orderid' => 0,
						'noprint' => 0,
						'menu_itemid' => 0,
					) ),
					133 => new \EE\ArrayObject( array(
						'optionid' => 133,
						'optiongroupid' => 58,
						'description' => 'Chicken and Shrimp',
						'price' => 0,
						'orderid' => 0,
						'noprint' => 0,
						'menu_itemid' => 0,
					) ),
					134 => new \EE\ArrayObject( array(
						'optionid' => 134,
						'optiongroupid' => 58,
						'description' => 'Beef, Chicken and Shrimp',
						'price' => 0,
						'orderid' => 0,
						'noprint' => 0,
						'menu_itemid' => 0,
					) ),
				) ),
			) ),
			1163 => new \EE\ArrayObject( array(
				'optiongroupid' => 1163,
				'menu_item_id' => 16309,
				'description' => '20oz Bottle Soda',
				'type' => 1,
				'orderid' => 1,
				'options' => new \EE\ArrayObject( array(
					4702 => new \EE\ArrayObject( array(
						'optionid' => 4702,
						'optiongroupid' => 1163,
						'description' => 'Assorted',
						'price' => 0,
						'orderid' => 1,
						'noprint' => 0,
						'menu_itemid' => 0,
					) ),
					4703 => new \EE\ArrayObject( array(
						'optionid' => 4703,
						'optiongroupid' => 1163,
						'description' => 'Diet',
						'price' => 0,
						'orderid' => 2,
						'noprint' => 0,
						'menu_itemid' => 0,
					) ),
					30812 => new \EE\ArrayObject( array(
						'optionid' => 30812,
						'optiongroupid' => 1163,
						'description' => 'Regular',
						'price' => 0,
						'orderid' => 3,
						'noprint' => 0,
						'menu_itemid' => 0,
					) ),
				) ),
			) ),
			2782 => new \EE\ArrayObject( array(
				'optiongroupid' => 2782,
				'menu_item_id' => 54482,
				'description' => 'Choices',
				'type' => 2,
				'orderid' => 2,
				'options' => new \EE\ArrayObject( array(
					13594 => new \EE\ArrayObject( array(
						'optionid' => 13594,
						'optiongroupid' => 2782,
						'description' => 'Bagels & Cream Cheese',
						'price' => 0,
						'orderid' => 2,
						'noprint' => 0,
						'menu_itemid' => 0,
					) ),
					13595 => new \EE\ArrayObject( array(
						'optionid' => 13595,
						'optiongroupid' => 2782,
						'description' => 'Assorted Gourmet Pastry',
						'price' => 0,
						'orderid' => 3,
						'noprint' => 0,
						'menu_itemid' => 0,
					) ),
					13596 => new \EE\ArrayObject( array(
						'optionid' => 13596,
						'optiongroupid' => 2782,
						'description' => 'Assorted  Muffins',
						'price' => 0,
						'orderid' => 4,
						'noprint' => 0,
						'menu_itemid' => 0,
					) ),
					29092 => new \EE\ArrayObject( array(
						'optionid' => 29092,
						'optiongroupid' => 2782,
						'description' => 'Breakfast Bread',
						'price' => 0,
						'orderid' => 5,
						'noprint' => 0,
						'menu_itemid' => 0,
					) ),
					30781 => new \EE\ArrayObject( array(
						'optionid' => 30781,
						'optiongroupid' => 2782,
						'description' => 'Cinnamon Rolls',
						'price' => 0,
						'orderid' => 6,
						'noprint' => 0,
						'menu_itemid' => 0,
					) ),
					30831 => new \EE\ArrayObject( array(
						'optionid' => 30831,
						'optiongroupid' => 2782,
						'description' => 'Assorted',
						'price' => 0,
						'orderid' => 7,
						'noprint' => 0,
						'menu_itemid' => 0,
					) ),
				) ),
			) ),
		) ),
	) );
	$menuItems[] = $menuItem;
	$menuItemIds[ $menuItem->menu_item_id ] = $menuItem->menu_item_id;
#	$menuItems = null;
/*
* /
		<pre>$menuItems = <?php var_dump( $menuItems ); ?></pre>
		<pre>$menuItemIds = <?php var_dump( $menuItemIds ); ?></pre>
/*
*/
	$customerFavorites = null;
	if ( ( $pageVariables->currentMenu == $pageBusiness->default_menu ) && $menuItems ) {
		$menuOptionGroups = \EE\Model\Menu\Option\Group::getByMenuItemIds( $menuItemIds, $class = '\EE\ArrayObject', $menuItems );
		if ( $menuOptionGroups ) {
			$optionGroupIds = array_keys( $menuOptionGroups );
			$menuOptionGroups = \EE\Model\Menu\Option\Item::getByOptionGroupIds( $optionGroupIds, $class = '\EE\ArrayObject', $menuOptionGroups );
		}
					# temp assign to make sure we're not using these
#					$show_healthy =
#					$showhealthy =
#					$showprice =
#					$showmin =
					$showFav = null;
		if ( $pageVariables->customerId ) {
			$customerFavorites = \EE\Model\Cater\Favorite::getCustomerFavorites( $pageVariables->customerId );
		}
		if ( !$customerFavorites ) {
			$customerFavorites = array();
		}
/*
* /
		<pre>$menuItems = <?php var_dump( $menuItems ); ?></pre>
/*
*/
	}
	
	$groupNutritionLinks = array();
	$groupNutritionLinkHttpQuery = array();
	if ( $displayNutritionHealth ) {
		$groupNutritionLinks = array(
			array(
				'link' => array( 'showme' => \EE\Model\Nutrition::SHOWME_LOW_CALORIES ),
				'src' => 'img_calorie.jpg',
				'alt' => 'Low Calorie Items',
			),
			array(
				'link' => array( 'showme' => \EE\Model\Nutrition::SHOWME_LOW_FAT ),
				'src' => 'img_fat.jpg',
				'alt' => 'Low Fat Items',
			),
			array(
				'link' => array( 'showme' => \EE\Model\Nutrition::SHOWME_LOW_CHOLESTEROL ),
				'src' => 'img_cholesterol.jpg',
				'alt' => 'Low Cholesterol',
			),
			array(
				'link' => array( 'showme' => \EE\Model\Nutrition::SHOWME_LOW_SODIUM ),
				'src' => 'img_sodium.jpg',
				'alt' => 'Low Sodium Items',
			),
			array(
				'link' => array( 'showme' => \EE\Model\Nutrition::SHOWME_LOW_CARBOHYDRATES ),
				'src' => 'img_carbo.jpg',
				'alt' => 'Low Carbohydrates',
			),
			array(
				'link' => array( 'showme' => \EE\Model\Nutrition::SHOWME_TREAT_POINTS ),
				'src' => 'ww.jpg',
				'alt' => 'Treat Score',
			),
			array(
				'link' => array( 'showme' => null ),
				'src' => 'hh1.jpg',
				'alt' => 'Heart Healthy',
			),
		);
		$groupNutritionLinkHttpQuery = array(
			'bid' => $pageVariables->businessId,
			'groupid' => $pageVariables->groupId,
			'showme' => null,
		);
	}
	
	
#	\EE\Model\Business\Setting::db( Treat_DB::singleton() );
#	$settings = \EE\Model\Business\Setting::get( "business_id = " . abs( intval( $businessid ) ), true );
#	if ( !$settings ) {
#		$settings = new \EE\Null();
#	}
	$background_image_logo = $pageBusiness->background_image ?: '';
	$site_logo = $pageBusiness->site_logo ?: '/assets/images/nutrition/logo_205x43.jpg';

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
#	$query = "SELECT * FROM company WHERE companyid = '1'";
#	$result = Treat_DB_ProxyOld::query( $query );
	//mysql_close();

	$companyname = $pageCompany->companyname;

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
#	$query = "SELECT * FROM business WHERE businessid = '$businessid'";
#	$result = Treat_DB_ProxyOld::query( $query );
	//mysql_close();

	$businessname = $pageBusiness->address;
	$businessname2 = $pageBusiness->businessname;
	$taxrate = $pageBusiness->tax;
	$phone = $pageBusiness->phone;
	$default_menu = $pageBusiness->default_menu;
	$cafe_menu = $pageBusiness->cafe_menu;
	$vend_menu = $pageBusiness->vend_menu;
	$default_menu_alias = $pageBusiness->default_menu_alias;
	$cafe_menu_alias = $pageBusiness->cafe_menu_alias;
	$vend_menu_alias = $pageBusiness->vend_menu_alias;
	$no_nutrition = $pageBusiness->no_nutrition;
	$allow_sub_orders = $pageBusiness->allow_sub_orders;
	
?>
<html>
	<head>
		<title>Menu Items :: Treat America: <?php echo $businessname2; ?></title>
<?php includeTimeOutCode(); ?>
<script language="JavaScript" type="text/javascript" src="/assets/js/jquery-current.min.js"></script>
<script language="JavaScript" type="text/javascript" src="/assets/js/dynamicdrive-disableform.js">
</script>

<script language="JavaScript" type="text/javascript">
function popup(url)
{
	popwin=window.open(url,"Nutrition","location=no,menubar=no,titlebar=no,resizeable=no,scrollbars=yes,height=600,width=415");   
}
</script>

<script language="JavaScript" type="text/javascript">
function showhide()
{
	jQuery("#orderDetailSubmitSave").show();
	jQuery("#orderDetailShowSave").hide();
	// @TODO remove these
	document.all.showsave.style.display = 'none';
	document.all.submitsave.style.display = 'block';
}
</script>

		<script type="text/javascript" src="/assets/js/calendarPopup.js"></script>
		<link rel="stylesheet" type="text/css" href="/assets/css/calendarPopup.css" />
		<link rel="stylesheet" type="text/css" href="/assets/css/menu/menu.css" />

	</head>
	<body background="<?php echo $background_image_logo; ?>">
		<center>
			<table cellspacing="0" cellpadding="0" border="0" width="90%">
				<tr>
					<td colspan="4">
						<img src="<?php echo $site_logo; ?>" width="205" height="43" alt="site logo" />
						<p>
					</td>
				</tr>
<?php

	if ( $curmenu == $default_menu && $default_menu_alias > 0 ) {
		$aliasid = $default_menu_alias;
	}
	elseif ( $curmenu == $cafe_menu && $cafe_menu_alias > 0 ) {
		$aliasid = $cafe_menu_alias;
	}
	elseif ( $curmenu == $vend_menu && $vend_menu_alias > 0 ) {
		$aliasid = $vend_menu_alias;
	}

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM accounts WHERE accountid = '$accountid'";
	$result = Treat_DB_ProxyOld::query( $query );
	//mysql_close();

	$accountname = Treat_DB_ProxyOld::mysql_result( $result, 0, "name" );
	$accountnum = Treat_DB_ProxyOld::mysql_result( $result, 0, "accountnum" );
	$taxid = Treat_DB_ProxyOld::mysql_result( $result, 0, "taxid" );

#	$query = "SELECT menu_typename FROM menu_type WHERE menu_typeid = '$curmenu'";
#	$result = Treat_DB_ProxyOld::query( $query );
	//mysql_close();

	$menu_name = $menuType->menu_typename;

	$showname = str_replace( "~", ' ', $showname );
	if ( $reserveid > 0 && $curmenu != $cafe_menu && $curmenu != $vend_menu ) {
		$showcart = "<a style=$style href=caterdetails.php?reserveid=$reserveid&accountid=$accountid#invoice><font color=white><b>Check Out </b><img src=chefHat.gif width=29 height=27 border=0></font></a>";
	}
	else {
		$showcart = "";
	}

	if ( $accountid > 0 ) {
		$logout = "Sign Off";
		$showmyaccount = "<a href=account_settings.php?goto=2 style=$style><FONT COLOR=#FFFFFF onMouseOver=this.style.color='#FF9900';this.style.cursor='hand' onMouseOut=this.style.color='#FFFFFF'><b>My Account</b></font></a>&nbsp;";
	}
	else {
		$logout = "Sign In";
	}

	/////search
	if ( $curmenu == $default_menu ) {
		$showsearch = "<form action=menuitems.php method=post style=\"margin:0;padding:0;display:inline;\"><input name=search size=25><input type=submit value='Search' style=\"border: 1px solid #CCCCFF; background-color:#E8E7E7;\"></form>";
	}
	else {
		$showsearch = "";
	}

	if ( $realuser != "" ) {
		$showrealuser = "<i> as $realuser</i>";
	}

?>
				<tr bgcolor="#6699FF">
					<td colspan="2">
						<font color="white" size="3">
							<b>
								The Right Choice... for a Healthier You!
							</b>
						</font>
					</td>
					<td colspan="2" align="right">
						<?php echo $showmyaccount; ?> 
					</td>
				</tr>
				<tr bgcolor="#669933">
					<td width="10%">
						<img src="rc-header2.jpg" height="80" width="108" alt="rc-header2" />
					</td>
					<td>
						<font size="4" color="white">
							<b><?php echo $companyname; ?> - <?php echo $businessname; ?></b>
						</font>
						<br />
						<b>
							<font color="white">
								Account: <?php echo $accountname, ' ', $showrealuser; ?>
								<br />
								Account #: <?php echo $accountnum; ?>
							</font>
						</b>
					</td>
					<td colspan="2" align="right" valign="top">
						<font color="white">
							<b><?php echo $bbdayname, ', ', $bbmonth, ' ', $bbday, ', ', $year; ?></b>
							<br/>
							<font size="2">
								[<a style="<?php
									echo $style;
								?>" href="logout.php?goto=1&amp;bid=<?php echo $pageVariables->businessId; ?>"><b><font color="white"><?php
									echo $logout;
								?></font></b></a>]
							</font>
							<p>
							<?php echo $showsearch, ' &nbsp; ', $showcart; ?>
						</font>
					</td>
				</tr>
				<tr bgcolor="black">
					<td height="1" colspan="4"></td>
				</tr>
<?php
	
	///////////////////////////MENU GROUPS
?>
				<tr bgcolor="white">
					<td colspan="4">
						<div class="menu_group_links">
<?php
	$first = false;
	///favorites
	if ( $pageVariables->accountId && $pageVariables->currentMenu == $pageBusiness->default_menu ) {
		$first = true;
?>
							<a href="/menu/menuitems.php?<?php
								echo http_build_query( array_merge( $menuGroupLinksQuery, array( 'groupid' => -1 ) ), null, '&amp;' );
							?>" class="menu_group_favorite<?php
								if ( -1 == $pageVariables->groupId ) {
									echo ' current';
								}
							?>">
								<img src="../ta/star.gif" border="0" alt="star" />
								Favorites
							</a>
<?php
	}
	foreach ( $menuGroups as $menuGroup ) {
?>
							<?php
								if ( $first ) {
									echo '| ';
								}
								else {
									$first = true;
								}
							?><a href="/menu/menuitems.php?<?php
								echo http_build_query( array_merge( $menuGroupLinksQuery, array( 'groupid' => $menuGroup->menu_group_id ) ), null, '&amp;' );
							?>"<?php
								if ( $menuGroup->menu_group_id == $pageVariables->groupId ) {
									echo ' class="current"';
								}
							?>><?php
								echo $menuGroup->groupname;
							?></a>
<?php
	}
?>
						</div>
					</td>
				</tr>
			</table>
		</center>
		<p>
<?php

	///////////////////////////////
	/////////ORDER/////////////////
	///////////////////////////////
	if ( $pageVariables->reserveId > 0) {
		if ( $orderDetails ) {
?>
		<center>
			<table width="90%" cellspacing="0" cellpadding="0" class="order_details">
				<thead>
					<tr>
						<th colspan="6">
							Order Details:
						</th>
					</tr>
				</thead>
				<tbody class="items">
<?php
			$subtotal = 0;
			$orderDetailsCountTotal = count( $orderDetails );
			$orderDetailsCounter = 0;
			foreach ( $orderDetails as $orderDetail ) {
				$orderDetailsCounter++;
				
				$classes = array();
				if ( $orderDetailsCounter % 2 ) {
					$classes[] = 'odd';
				}
				else {
					$classes[] = 'even';
				}
				if ( $orderDetail->detail == ";" ) {
					$orderDetail->detail = '';
				}
				
				if ( $orderDetailsCounter == $orderDetailsCountTotal && $pageVariables->showQty && $pageVariables->showName ) {
					$classes[] = 'highlight';
				}

				if ( $orderDetail->qty < 1 ) {
					$newqty = 1;
				}
				else {
					$newqty = $orderDetail->qty;
				}
				$orderDetail->itemTotal = money( $newqty * $orderDetail->price );
?>
					<tr class="<?php echo implode( ' ', $classes ); ?>">
						<td valign="top" width="5%" class="item_qty">
							<?php echo $orderDetail->qty; ?> 
						</td>
						<td valign="top" width="20%" class="item_name">
							<?php echo $orderDetail->item; ?> 
						</td>
						<td valign="top" width="10%" class="item_price">
							$<?php echo money( $orderDetail->price ); ?> 
						</td>
						<td valign="top" width="10%" class="item_total">
							$<?php echo $orderDetail->itemTotal; ?> 
						</td>
						<td width="4%"></td>
						<td class="item_detail">
							<?php echo $orderDetail->detail; ?> 
							<font size="1">
								[<a href="/menu/delitem.php?itemid=<?php
									echo $orderDetail->itemid;
								?>&goto=1&groupid=<?php
									echo $pageVariables->groupId;
								?>" style="<?php
									echo $style;
								?>"><font color="blue">REMOVE</font></a>]
							</font>
						</td>
					</tr>
<?php
				$subtotal += $orderDetail->itemTotal;
			}
?>
				</tbody>
<?php
			$orderDetailsCounter++;
			$classes = array();
			if ( $orderDetailsCounter % 2 ) {
				$classes[] = 'odd';
			}
			else {
				$classes[] = 'even';
			}
?>
				<tbody class="subtotal">
					<tr valign="top" class="<?php echo implode( ' ', $classes ); ?>">
						<td colspan="2">
<?php
						if (
							(
								$pageVariables->currentMenu == $pageBusiness->cafe_menu
								|| $pageVariables->currentMenu == $pageBusiness->vend_menu
							)
							&& !$pageBusiness->no_nutrition
						) {
?>
							<a onclick="popup('nutrition2.php')">
								<img src="/assets/images/heart.jpg" height="16" width="14"
									alt="Nutritional Information for this Meal"
							/></a>
<?php
						}
?>
						</td>
						<td align="right">
							Subtotal:
						</td>
						<td align="right">
							$<?php echo money( $subtotal ); ?> 
						</td>
						<td colspan="2" align="right">
							<form name="save" action="savemenu.php" method="post" style="margin: 0; padding: 0; display: inline;">
<?php
							if (
								$pageVariables->currentMenu == $pageBusiness->cafe_menu
								|| $pageVariables->currentMenu == $pageBusiness->vend_menu
							) {
?>
								<div id="orderDetailShowSave">
									<a href="javascript:void();" onclick="showhide();">
										Save Order
									</a>
								</div>
								<div id="orderDetailSubmitSave">
									<script language="JavaScript" type="text/javascript">
										var cal18 = new CalendarPopup('testdiv1');
										cal18.setCssPrefix('TEST');
									</script>
									<input type="text" name="mydate" value="<?php echo date( 'Y-m-d' ); ?>" size="8" />
									<a href="javascript:void();"
										onClick="cal18.select(document.forms[0].mydate,'anchor18a','yyyy-MM-dd'); return false;"
										title="Choose a Date"
										name="anchor18" id="anchor18a"
									><img src="/assets/images/calendar.gif" border="0" height="16" width="16" alt="Choose a Date" /></a>
									<input type="submit" value="Save" />
								</div>
<?php
							}
?>
							</form>
						</td>
					</tr>
				</tbody>
			</table>
		</center>
		<p>
<?php
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////
	/////////Menu Group Header/////
	///////////////////////////////
?>
		<center>
			<table width="90%" cellspacing="0" cellpadding="0" border="0" class="menuGroupHeader">
				<tr>
					<td width="5" rowspan="2" height="100"></td>
					<td width="120" height="101" rowspan="2">
						<img src="<?php
							echo $pageGroup->imageSrc;
						?>" height="<?php
							echo $pageGroup->img_height;
						?>" width="<?php
							echo $pageGroup->img_width;
						?>" alt="<?php
							echo $pageGroup->imageAlt;
						?>" />
					</td>
					<td class="groupName"><?php echo $pageGroup->groupname; ?></td>
					<td class="groupLinks">
						<a href="/menu/menu.php?<?php
							echo http_build_query( $menuLinksQuery, null, '&amp;' );
						?>">
								<?php echo $menuType->menu_typename; ?>
						</a>
						::
						<a href="/menu/intro.php?<?php
							echo http_build_query( $menuLinksQuery, null, '&amp;' );
						?>">
								Home
						</a>&nbsp;
					</td>
<?php
	if ( $groupNutritionLinks && $groupNutritionLinkHttpQuery ) {
?>
					<td width="1%" rowspan="4" class="groupNutrition">
<?php
					foreach ( $groupNutritionLinks as $link ) {
						
?>
						<div>
							<a href="/menu/menuitems.php?<?php
								echo http_build_query( array_merge( $groupNutritionLinkHttpQuery, (array)$link['link'] ), null, '&amp;');
							?>">
								<img src="/assets/images/nutrition/<?php echo $link['src']; ?>" height="16" width="16" alt="<?php echo $link['alt']; ?>"
							/></a>
						</div>
<?php
					}
?>
					</td>
<?php
	}
?>
				</tr>
				<tr>
					<td colspan="3" class="groupDescription">
						<?php echo $pageGroup->description; ?>
					</td>
				</tr>
			</table>
		</center>
		<p>

<?php
	///////////////////////////////
	/////////Items/////////////////
	///////////////////////////////
?>
<?php
	///////////////////////////////
	/////////No Items//////////////
	///////////////////////////////
	if ( !$menuItems ) {
?>
		<div class="menuItemsNone">
			<div class="noItems">
				Sorry, No Items Available at this Time
			</div>
			<div class="moreInfo">
				If you would like items to be available in this section, please call us at <?php
					echo $pageBusiness->phone;
				?> to let us know.  We appreciate your business and any input you have.
			</div>
		</div>
		<p>
<?php
	}
	else {
	///////////////////////////////
	/////////Items Loop////////////
	///////////////////////////////
					# temp assign to make sure we're not using these
#					$show_healthy =
#					$showhealthy =
#					$showprice =
#					$showmin =
					$showFav = null;
?>
		<div class="menuItems">
<?php
		foreach ( $menuItems as $menuItem ) {
			$menuItem->description = trim( $menuItem->description );
		//////////////////////////////////////////////
		///////////////NUTRITION SPECIFICS////////////
		//////////////////////////////////////////////
			$isFavorite = false;
			if ( $showMeNutritionHealth ) {
			}
			////favorites
			elseif ( $currentMenuIsNotDefault && $pageVariables->debug ) {
				if ( $pageVariables->customerId ) {
					if ( $customerFavorites && array_key_exists( $menuItem->menu_item_id, $customerFavorites ) ) {
						if ( $customerFavorites[ $menuItem->menu_item_id ] ) {
							$isFavorite = true;
						}
					}
				}
			}
			
		/////DISPLAY MENU ITEMS
?>
			<table width="100%" cellpadding="0" cellspacing="0" class="menuItem">
				<tr valign="top">
					<td rowspan="2" width="1%">
<?php
					if ( $menuItem->image ) {
?>
						<img src="/ta/menuimage/<?php echo $menuItem->image; ?>" alt="" />
<?php
					}
					else {
?>
						&nbsp;
<?php
					}
?>
					</td>
					<td rowspan="2"></td>
					<td width="80%" class="menuItemLine">
						<div>
<?php
					if ( $displayNutritionHealth ) {
						switch ( $pageVariables->showMe ) {
							case \EE\Model\Nutrition::SHOWME_LOW_CALORIES:
							case \EE\Model\Nutrition::SHOWME_LOW_FAT:
							case \EE\Model\Nutrition::SHOWME_LOW_CHOLESTEROL:
							case \EE\Model\Nutrition::SHOWME_LOW_SODIUM:
							case \EE\Model\Nutrition::SHOWME_LOW_CARBOHYDRATES:
							case \EE\Model\Nutrition::SHOWME_TREAT_POINTS:
								$nutritionItem = \EE\Model\Nutrition\Loader::getMenuItemObject( $menuItem );
								$recipeItems = $nutritionItem->getRecipeItems();
								break;
						}
#						$nutritionValues = $nutritionItem->getNutritionValues();
#						echo '<pre style="border: 1px solid red;">';
#						echo '$nutritionItem = ';
#						var_dump( $nutritionItem );
#						echo '$nutritionValues = ';
#						var_dump( $nutritionValues );
#						echo '$recipeItems = ';
#						var_dump( $recipeItems );
#						echo '</pre>';
						switch ( $pageVariables->showMe ) {
			///////CALORIES
							case \EE\Model\Nutrition::SHOWME_LOW_CALORIES:
								if ( $nutritionItem->getTotalIngredients() > 0 ) {
									echo $nutritionItem->getLowCaloriesImage();
								}
								break;
			///////FAT
							case \EE\Model\Nutrition::SHOWME_LOW_FAT:
								if ( $nutritionItem->getTotalIngredients() > 0 ) {
									echo $nutritionItem->getLowCaloriesFromFatImage();
								}
								break;
			///////CHOLESTEROL
							case \EE\Model\Nutrition::SHOWME_LOW_CHOLESTEROL:
								if ( $nutritionItem->getTotalIngredients() > 0 ) {
									echo $nutritionItem->getLowCholesterolImage();
								}
								break;
			///////SODIUM
							case \EE\Model\Nutrition::SHOWME_LOW_SODIUM:
								if ( $nutritionItem->getTotalIngredients() > 0 ) {
									echo $nutritionItem->getLowSodiumImage();
								}
								break;
			///////CARBS
							case \EE\Model\Nutrition::SHOWME_LOW_CARBOHYDRATES:
								if ( $nutritionItem->getTotalIngredients() > 0 ) {
									echo $nutritionItem->getLowTotalCarbsImage();
								}
								break;
			///////Weight Watchers
							case \EE\Model\Nutrition::SHOWME_TREAT_POINTS:
								if ( $nutritionItem->getTotalIngredients() > 0 ) {
?>
							<span class="treatScore"><?php
									echo $nutritionItem->getTreatScore();
							?></span>
<?php
								}
								break;
						}
					}
		////////////////////////////////////
		//////////////END NUTRITION SPECS///
		////////////////////////////////////
?>
							<span class="menuItemTop">
<?php
					if ( $displayNutritionHealth ) {
?>
								<a onclick="popup('nutrition.php?mid=<?php
									echo $menuItem->menu_item_id;
								?>')" class="menuItemName">
<?php
					}
?>
									<span class="menuItemName"><?php
										echo $menuItem->item_name;
									?></span>
<?php
					if ( $menuItem->heart_healthy ) {
						if ( $currentMenuIsNotDefault ) {
?>
									<img src="/assets/images/nutrition/hh1.jpg" align="middle" height="17" width="16"
										alt="Heart Healthy"
									/>
<?php
						}
					}
?>
<?php
					if ( $menuItem->healthy_living ) {
						if ( $currentMenuIsNotDefault ) {
?>
									<img src="/assets/images/nutrition/healthy-living-icon-small.png" align="middle" height="16" width="16"
										alt="Healthy Living Program"
									/>
<?php
						}
					}
					if ( $displayNutritionHealth ) {
?>
								</a>
<?php
					}
					
					if ( $menuItem->price > 0 ) {
						$menuItem->showOptionAdd = 'Add';
?>
								<span class="menuItemPrice">$<?php
									echo sprintf( '%01.2f', $menuItem->price );
								?>/<?php
									echo $menuItem->unit;
								?></span>
<?php
					}
					else {
						$menuItem->showOptionAdd = '';
?>
								<span class="menuItemPrice"><?php
									echo $menuItem->unit;
								?></span>
<?php
					}
?>
							</span>
							<span class="menuItemExtra">
<?php
					if ( 0 < $menuItem->order_min ) {
?>
								<font size="2">(minimum <?php echo $menuItem->order_min; ?>)</font>
<?php
					}
					if ( $currentMenuIsNotDefault && $pageVariables->debug ) {
						if ( $isFavorite ) {
?>
								<a href="#"><img src="/ta/star.gif" border="0" alt="" /></a>
<?php
						}
						else {
?>
								<a href="#"><img src="/ta/star-grey.gif" border="0" alt="" /></a>
<?php
						}
					}
?>
							</span>
						</div>
<?php
					if ( $menuItem->description ) {
?>
						<div class="menuItemDescription">
							<p>&nbsp;<?php
								echo nl2br( $menuItem->description, true );
							?></p>
						</div>
<?php
					}
?>
					</td>
					<td rowspan="2" colspan="2" class="menuItemAddQty" valign="top" style="width: 305px;">
						<form action="/menu/additem.php" method="post" onSubmit="return disableForm(this);">
							<input type="hidden" name="businessid" value="<?php echo $pageVariables->businessId; ?>" />
							<input type="hidden" name="groupid" value="<?php echo $pageVariables->groupId; ?>" />
							<input type="hidden" name="item_name" value="<?php echo str_replace( ' ', '~', $menuItem->item_name ); ?>" />
							<input type="hidden" name="reserveid" value="<?php echo $pageVariables->reserveId; ?>" />
							<input type="hidden" name="menu_item_id" value="<?php echo $menuItem->menu_item_id; ?>" />
							<center>
<?php
		///////USE TO CHECK AND SEE IF THERE IS A RESERVEID
			if ( $pageVariables->currentMenu == $pageBusiness->default_menu ) {
				if ( $menuItem->optionGroups ) {
					$optionGroupNum = count( $menuItem->optionGroups );
				}
				else {
					$optionGroupNum = 0;
				}
?>
								<input type="hidden" name="optionnum" value="<?php echo $optionGroupNum; ?>" />
<?php
				if ( $menuItem->optionGroups ) {
					$optionGroupNum = 0;
					foreach ( $menuItem->optionGroups as $optionGroup ) {
?>
								<input type="hidden" name="optionGroup[<?php
									echo $optionGroup->optiongroupid;
								?>][type]" value="<?php
									echo $optionGroup->type;
								?>" />
								<table width="99%" cellspacing="0" cellpadding="0" class="optionGroups">
									<tr height="1">
										<td colspan="3"></td>
									</tr>
									<tr height="1" bgcolor="#CCCCCC">
										<td colspan="3"></td>
									</tr>
									<tr>
										<td width="1" bgcolor="#CCCCCC"></td>
										<td style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr=#FFFFFF, startColorstr=#E8E7E7, gradientType=0);">
											<font size="2">
												<u><?php echo $optionGroup->description; ?></u>
											</font>
										</td>
										<td bgcolor="#CCCCCC" width="1"></td>
									</tr>
<?php
						if ( $optionGroup->options ) {
							$optionNum = 0;
							foreach ( $optionGroup->options as $option ) {
								$link = ( $option->menu_itemid > 0 );
								$inputChecked = '';
								$inputSize = '';
								switch ( $optionGroup->type ) {
									case 0:
										$inputType = 'radio';
										$inputName = sprintf(
											'optionGroup[%d][options]'
											,$optionGroup->optiongroupid
										);
										$inputValue = $option->optionid;
										if ( $optionGroup->type ) {
											if ( 0 == $optionNum ) {
												$inputChecked = ' checked="checked"';
											}
										}
										break;
									case 1:
										$inputType = 'checkbox';
										$inputName = sprintf(
											'optionGroup[%d][options][%d]'
											,$optionGroup->optiongroupid
											,$option->optionid
										);
										$inputValue = $option->optionid;
										break;
									case 2:
										$inputName = sprintf(
											'optionGroup[%d][options][%d]'
											,$optionGroup->optiongroupid
											,$option->optionid
										);
										if ( $link ) {
											$inputType = 'checkbox';
											$inputValue = $option->optionid;
										}
										else {
											$inputType = 'text';
											$inputValue = '';
											$inputSize = ' size="3"';
										}
										break;
								}
?>
									<tr>
										<td width="1" bgcolor="#CCCCCC"></td>
										<td class="option">
											<label>
											<input type="<?php
												echo $inputType;
											?>" name="<?php
												echo $inputName;
											?>" value="<?php
												echo $inputValue;
											?>"<?php
												echo $inputSize, $inputChecked;
											?> />
<?php
								if ( $link ) {
?>
											<a onclick="popup('nutrition.php?mid=<?php echo $option->menu_itemid; ?>')" class="optionAdd">
<?php
								}
								else {
?>
											<span class="optionAdd">
<?php
								}
?>
												<span><?php
													echo $option->description;
												?></span>
<?php
								if ( $option->price ) {
?>
												<span class="optionAddPrice"> - <?php
													echo $menuItem->showOptionAdd;
												?> $<?php
													echo $option->price;
												?></span>
<?php
								}
?> 
<?php
								if ( !$link ) {
?>
											</span>
<?php
								}
								else {
?>
											</a>
<?php
								}
?>
											</label>
										</td>
										<td width="1" bgcolor="#CCCCCC"></td>
									</tr>
<?php
								$optionNum++;
							}
						}
?>
									<tr height="1" bgcolor="#CCCCCC">
										<td colspan="3"></td>
									</tr>
									<tr height="1">
										<td></td>
									</tr>
								</table>
<?php
						$optionGroupNum++;
					}
				}
?>
								<div class="specialInstructions">
									<div>Special Instructions</div>
									<textarea name="special" rows="5" cols="40"></textarea>
								</div>
<?php
			}
		///////NUTRITION MEAL
			else {
?>
								<input type="hidden" name="optionnum" value="-1" />
<?php
			}
?>
								<div class="menuItemQty">
									<label>
										Qty:
										<input type="text" name="qty" size="4" value="<?php echo $pageVariables->qtyValue; ?>" />
									</label>
									<input type="submit" value="Add" />
								</div>
							</center>
						</form>
					</td>
				</tr>
			</table>
		<p>
<?php

		}
?>
		</div>
<?php
	}
?>

		<div class="groupLinks footerLinks">
			<a href="/menu/menu.php?<?php
				echo http_build_query( $menuLinksQuery, null, '&amp;' );
			?>">
				<?php echo $menuType->menu_typename; ?>
			</a>
			::
			<a href="/menu/intro.php?<?php
				echo http_build_query( $menuLinksQuery, null, '&amp;' );
			?>">
				Home
			</a>
		</div>
			
		<div id="testdiv1"></div>
<?php
	google_page_track();
?>
	</body>
</html>
<?php
}

