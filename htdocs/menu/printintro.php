<html>
<?php

ini_set("display_errors","true");

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+3);
        if ($diff > 0 && $diff < '0.01'){$diff="0.01";}
        elseif($diff < 0 && $diff > '-0.01'){$diff="-0.01";}
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      elseif ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      elseif ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '1')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='31')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;
$leap = date("L");

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   
   elseif ($leap==1&&$month=='03')
   {
      $day=$day+29;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}

define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once(DOC_ROOT.'/bootstrap.php');

$businessid = isset($_GET["bid"])?$_GET["bid"]:'';
$week = isset($_GET["week"])?$_GET["week"]:'';
$today = isset($_GET["date"])?$_GET["date"]:'';
$showme = isset($_GET["showme"])?$_GET["showme"]:'';
if($businessid==""){$businessid = isset($_POST["businessid"])?$_POST["businessid"]:'';}
$reserveid = isset($_POST["reserveid"])?$_POST["reserveid"]:'';
$user = isset($_COOKIE["usercook"])?$_COOKIE["usercook"]:'';
$pass = isset($_COOKIE["passcook"])?$_COOKIE["passcook"]:'';
$accountid = isset($_COOKIE["accountid"])?$_COOKIE["accountid"]:'';
if($businessid==""){$businessid = isset($_COOKIE["eebid"])?$_COOKIE["eebid"]:'';}
if($week==""){$week = isset($_COOKIE["week"])?$_COOKIE["week"]:'';}
if($reserveid==""){$reserveid = isset($_COOKIE["reserveid"])?$_COOKIE["reserveid"]:'';}
  
$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today2="$year-$month-$day";
if($today==""){$today="$year-$month-$day";$showday="Today`s";$todayname = dayofweek($today);}
elseif($today==$today2){$showday="Today`s";$todayname = dayofweek($today2);}
else{$todayname = dayofweek($today); $showday="$todayname`s";}


$yesterday=prevday($today);
$tomorrow=nextday($today); 

if ($businessid==""){
    $location="index.php";
    header('Location: ./' . $location);
}
else{

	\EE\Model\Business\Setting::db( Treat_DB::singleton() );
	$settings = \EE\Model\Business\Setting::get("business_id = ". abs(intval($businessid)), true);
	$background_image_logo = $settings->background_image ?: '';
	//$site_logo = $settings->site_logo ?: 'logo.jpg';


    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM company WHERE companyid = '1'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $companyname=@mysql_result($result,0,"companyname");

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM business WHERE businessid = '$businessid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $businessname=@mysql_result($result,0,"companyname");
	$businessname2=@mysql_result($result,0,"businessname");
    $taxrate=@mysql_result($result,0,"tax");
    $default_menu=@mysql_result($result,0,"default_menu");
    $cafe_menu=@mysql_result($result,0,"cafe_menu");
    $vend_menu=@mysql_result($result,0,"vend_menu");
    $catering=@mysql_result($result,0,"catering");
    $default_only=@mysql_result($result,0,"default_only");
    $no_nutrition=@mysql_result($result,0,"no_nutrition");
	$multi_cafe_menu=@mysql_result($result,0,"multi_cafe_menu");
	
	if($multi_cafe_menu==1){
		$mymenu = isset($_COOKIE["mymenu"])?$_COOKIE["mymenu"]:'';
		
		if($mymenu<1){
			$location="menuselect.php?bid=$businessid";
			header('Location: ./' . $location);
		}
		else{
			$cafe_menu=$mymenu;
		}
	}


?>
<head>

<title>Treat America: <?php echo "$businessname2";?></title>

<META NAME="ROBOTS" CONTENT="NOARCHIVE"> 

<script language="JavaScript"
   type="text/JavaScript">
function popup(url)
  {
     popwin=window.open(url,"Nutrition","location=no,menubar=no,titlebar=no,scrollbars=yes,resizeable=yes,height=600,width=415");   
  }
</script>

<script language="JavaScript"
   type="text/JavaScript">
function popup2(url)
  {
     popwin=window.open(url,"Tutorial","location=no,scrollbars=yes,menubar=no,titlebar=no,resizeable=yes,height=600,width=425");   
  }
</script>

<script language="JavaScript"
   type="text/JavaScript">
function showhide()
  {
     document.all.showsave.style.display = 'none';
     document.all.submitsave.style.display = 'block';
  }
</script>

<SCRIPT LANGUAGE="JavaScript" SRC="CalendarPopup.js"></SCRIPT>

<!-- This prints out the default stylehseets used by the DIV style calendar.
     Only needed if you are using the DIV style popup -->
<SCRIPT LANGUAGE="JavaScript">document.write(getCalendarStyles());</SCRIPT>

<STYLE>
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation
			{
			background-color:#6677DD;
			text-align:center;
			vertical-align:center;
			text-decoration:none;
			color:#FFFFFF;
			font-weight:bold;
			}
	.TESTcpDayColumnHeader,
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation,
	.TESTcpCurrentMonthDate,
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDate,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDate,
	.TESTcpCurrentDateDisabled,
	.TESTcpTodayText,
	.TESTcpTodayTextDisabled,
	.TESTcpText
			{
			font-family:arial;
			font-size:8pt;
			}
	TD.TESTcpDayColumnHeader
			{
			text-align:right;
			border:solid thin #6677DD;
			border-width:0 0 1 0;
			}
	.TESTcpCurrentMonthDate,
	.TESTcpOtherMonthDate,
	.TESTcpCurrentDate
			{
			text-align:right;
			text-decoration:none;
			}
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDateDisabled
			{
			color:#D0D0D0;
			text-align:right;
			text-decoration:line-through;
			}
	.TESTcpCurrentMonthDate
			{
			color:#6677DD;
			font-weight:bold;
			}
	.TESTcpCurrentDate
			{
			color: #FFFFFF;
			font-weight:bold;
			}
	.TESTcpOtherMonthDate
			{
			color:#808080;
			}
	TD.TESTcpCurrentDate
			{
			color:#FFFFFF;
			background-color: #6677DD;
			border-width:1;
			border:solid thin #000000;
			}
	TD.TESTcpCurrentDateDisabled
			{
			border-width:1;
			border:solid thin #FFAAAA;
			}
	TD.TESTcpTodayText,
	TD.TESTcpTodayTextDisabled
			{
			border:solid thin #6677DD;
			border-width:1 0 0 0;
			}
	A.TESTcpTodayText,
	SPAN.TESTcpTodayTextDisabled
			{
			height:20px;
			}
	A.TESTcpTodayText
			{
			color:#6677DD;
			font-weight:bold;
			}
	SPAN.TESTcpTodayTextDisabled
			{
			color:#D0D0D0;
			}
	.TESTcpBorder
			{
			border:solid thin #6677DD;
			}
</STYLE>

</head>
<?

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM accounts WHERE accountid = '$accountid'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $accountname=@mysql_result($result,0,"name");
    $accountnum=@mysql_result($result,0,"accountnum");
    $taxid=@mysql_result($result,0,"taxid");
    $acct_menu=@mysql_result($result,0,"menu");

    $curmenu=$cafe_menu;

    //mysql_connect($dbhost,$username,$password);
    //@mysql_select_db($database) or die( "Unable to select database");
    $query = "SELECT * FROM menu_type WHERE menu_typeid = '$curmenu'";
    $result = Treat_DB_ProxyOld::query($query);
    //mysql_close();

    $contract=@mysql_result($result,0,"type");
    $menu_name=@mysql_result($result,0,"menu_typename");
	$parent=@mysql_result($result,0,"parent");
	
	//////linked menus
	if($parent>0){
		$daypartid=$curmenu;
		$curmenu=$parent;
	}
	
	//OLD CONTRACT FEEDING
	/*
    if ($contract==1){
       $location="menu2.php";
       header('Location: ./' . $location); 
    }
	*/
    if($default_only==1){
       $location="menu.php?curmenu=$default_menu&bid=$businessid";
       header('Location: ./' . $location);
    }
////////////////////////////DISPLAY   


    echo "<body background='{$background_image_logo}' onload=\"window.print();\">";


///////////////////////////////

    while(dayofweek($today)!="Monday"){$today=prevday($today);}
    $yesterday=prevday($today);
    $tomorrow=$today;
    for($mycount=1;$mycount<=8;$mycount++){$tomorrow=nextday($tomorrow);}

    echo "<center><table cellspacing=0 cellpadding=0 style=\"border:1px solid black;\" width=100%>";
    echo "<tr><td bgcolor=black colspan=14 style=\"border:1px solid black;\"><center><font color=white size=4><b>$menu_name - Weekly Specials</b></font></center></td></tr>";

    echo "<tr valign=top>";
    for ($counter=1;$counter<=5;$counter++){

       $showyear=substr($today,2,2);
       $showmonth=substr($today,5,2)*1;
       $showday2=substr($today,8,2);
       $todayname=dayofweek($today);

       echo "<td width=20% bgcolor=white style=\"border:1px solid black;\">";
       echo "<center><font size=2 color=#555555><b>$todayname<br>$showmonth/$showday2/$showyear</b></font><hr width=90%>";

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query4 = "SELECT * FROM caternote WHERE businessid = '$businessid' AND date <= '$today' AND enddate >= '$today' AND recipe_station = '0'";
       $result4 = Treat_DB_ProxyOld::query($query4);
       $num4=mysql_numrows($result4);
       //mysql_close();

       $num4--;
       while ($num4>=0){
          $daynote=@mysql_result($result4,$num4,"note");

          $daynote=str_replace("\n","<br>",$daynote);

          echo "<center><table width=95% bgcolor=#336699 style=\"border: 1px solid black;\"><tr><td><font size=1 face=arial color=white><center><i>$daynote</i></font></center></td></tr></table></center><br>";
          $num4--;
       }

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
       $query4 = "SELECT * FROM servery_station ORDER BY orderid DESC";
       $result4 = Treat_DB_ProxyOld::query($query4);
       $num4=mysql_numrows($result4);
       //mysql_close();

       $num4--;
       while($num4>=0){
       $recipe_station=@mysql_result($result4,$num4,"stationid");
       $station_name=@mysql_result($result4,$num4,"station_name");

       //mysql_connect($dbhost,$username,$password);
       //@mysql_select_db($database) or die( "Unable to select database");
	   if($parent>0){$query = "SELECT * FROM order_item,menu_items WHERE order_item.daypartid = '$daypartid' AND order_item.businessid = '$businessid' AND order_item.date = '$today' AND order_item.menu_itemid = menu_items.menu_item_id AND menu_items.menu_typeid = '$curmenu' AND menu_items.recipe_station = '$recipe_station' ORDER BY order_item.orderid DESC";}
       else{$query = "SELECT * FROM order_item,menu_items WHERE order_item.businessid = '$businessid' AND order_item.date = '$today' AND order_item.menu_itemid = menu_items.menu_item_id AND menu_items.menu_typeid = '$curmenu' AND menu_items.recipe_station = '$recipe_station' ORDER BY order_item.orderid DESC";}
       $result = Treat_DB_ProxyOld::query($query);
       $num=mysql_numrows($result);
       //mysql_close();

       if ($num>0){

       echo "<font size=2 color=#555555><u>$station_name</u></font><br>";

       $query44 = "SELECT * FROM caternote WHERE businessid = '$businessid' AND date <= '$today' AND enddate >= '$today' AND recipe_station = '$recipe_station'";
       $result44 = Treat_DB_ProxyOld::query($query44);
       $num44=mysql_numrows($result44);

       $num44--;
       while ($num44>=0){
          $daynote=@mysql_result($result44,$num44,"note");

          $daynote=str_replace("\n","<br>",$daynote);

          echo "<center><font size=1 face=arial color=#555555><i>$daynote</i></font></center>";
          $num44--;
       }

       $num--;
       while($num>=0){
          $menu_itemid=@mysql_result($result,$num,"menu_itemid");

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query2 = "SELECT * FROM menu_items WHERE menu_item_id = '$menu_itemid'";
          $result2 = Treat_DB_ProxyOld::query($query2);
          //mysql_close();

          $item_name=@mysql_result($result2,0,"item_name");
          $price=@mysql_result($result2,0,"price");
          $description=@mysql_result($result2,0,"description");
          $unit=@mysql_result($result2,0,"unit");
          $groupid=@mysql_result($result2,0,"groupid");
          $heart_healthy=@mysql_result($result2,0,"heart_healthy");
          $serving_size=@mysql_result($result2,0,"serving_size");

          $price3=money($price);

          //////////////////////////////////////////////
          ///////////////NUTRITION SPECIFICS////////////
          //////////////////////////////////////////////
          $nutrition=array();
          $showhealthy="";
          if ($showme>0){
             //mysql_connect($dbhost,$username,$password);
             //@mysql_select_db($database) or die( "Unable to select database");
             $query32 = "SELECT * FROM recipe WHERE menu_itemid = '$menu_itemid'";
             $result32 = Treat_DB_ProxyOld::query($query32);
             $num32=mysql_numrows($result32);
             //mysql_close();

             $num32--;
             while($num32>=0){
                $recipeid=@mysql_result($result32,$num32,"recipeid");
                $inv_itemid=@mysql_result($result32,$num32,"inv_itemid");
                $rec_num=@mysql_result($result32,$num32,"rec_num");
                $rec_size=@mysql_result($result32,$num32,"rec_size");
                $srv_num=@mysql_result($result32,$num32,"srv_num");
                $rec_order=@mysql_result($result32,$num32,"rec_order");

                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query23 = "SELECT * FROM inv_items WHERE inv_itemid = '$inv_itemid'";
                $result23 = Treat_DB_ProxyOld::query($query23);
                //mysql_close();

                $supc=@mysql_result($result23,0,"item_code");
                $inv_itemname=@mysql_result($result23,0,"item_name");
                $order_size=@mysql_result($result23,0,"order_size");
                $price=@mysql_result($result23,0,"price");
                if ($rec_order==1){$inv_rec_num=@mysql_result($result23,0,"rec_num");}
                elseif ($rec_order==2){$inv_rec_num=@mysql_result($result23,0,"rec_num2");}

                ///////////nutrition
                $numserv=$serving_size;

                //mysql_connect($dbhost,$username,$password);
                //@mysql_select_db($database) or die( "Unable to select database");
                $query24 = "SELECT * FROM nutrition WHERE item_code = '$supc'";
                $result24 = Treat_DB_ProxyOld::query($query24);
                $num24=mysql_numrows($result24);
                //mysql_close();

                $cholesterol=@mysql_result($result24,0,"cholesterol");
                $calories=@mysql_result($result24,0,"calories");
                $cal_from_fat=@mysql_result($result24,0,"cal_from_fat");
                $fiber=@mysql_result($result24,0,"fiber");
                $protein=@mysql_result($result24,0,"protein");
                $carbs=@mysql_result($result24,0,"complex_cabs");
                $sodium=@mysql_result($result24,0,"sodium");
                $unsat_fat=@mysql_result($result24,0,"unsat_fat");
                $sat_fat=@mysql_result($result24,0,"sat_fat");
                $total_fat=@mysql_result($result24,0,"total_fat");
                $potassium=@mysql_result($result24,0,"potassium");

                $nutr_serv_size=@mysql_result($result24,0,"serv_size_hh");
                $nutr_rec_size=@mysql_result($result24,0,"rec_size");
                $nutr_rec_size2=@mysql_result($result24,0,"rec_size2");

                if ($nutr_rec_size==$rec_size){
                   $nutrition[cholesterol]=round($nutrition[cholesterol]+(($cholesterol/$nutr_serv_size)*($rec_num/$numserv)),1);
                   $nutrition[calories]=round($nutrition[calories]+(($calories/$nutr_serv_size)*($rec_num/$numserv)),1);
                   $nutrition[cal_from_fat]=round($nutrition[cal_from_fat]+(($cal_from_fat/$nutr_serv_size)*($rec_num/$numserv)),1);
                   $nutrition[fiber]=round($nutrition[fiber]+(($fiber/$nutr_serv_size)*($rec_num/$numserv)),1);
                   $nutrition[protein]=round($nutrition[protein]+(($protein/$nutr_serv_size)*($rec_num/$numserv)),1);
                   $nutrition[carbs]=round($nutrition[carbs]+(($carbs/$nutr_serv_size)*($rec_num/$numserv)),1);
                   $nutrition[sodium]=round($nutrition[sodium]+(($sodium/$nutr_serv_size)*($rec_num/$numserv)),1);
                   $nutrition[unsat_fat]=round($nutrition[unsat_fat]+(($unsat_fat/$nutr_serv_size)*($rec_num/$numserv)),1);
                   $nutrition[sat_fat]=round($nutrition[sat_fat]+(($sat_fat/$nutr_serv_size)*($rec_num/$numserv)),1);
                   $nutrition[total_fat]=round($nutrition[total_fat]+(($total_fat/$nutr_serv_size)*($rec_num/$numserv)),1);
                   $nutrition[potassium]=round($nutrition[potassium]+(($potassium/$nutr_serv_size)*($rec_num/$numserv)),1);

                }
                else{
                   //mysql_connect($dbhost,$username,$password);
                   //@mysql_select_db($database) or die( "Unable to select database");
                   $query25 = "SELECT * FROM conversion WHERE sizeid = '$rec_size' AND sizeidto = '$nutr_rec_size'";
                   $result25 = Treat_DB_ProxyOld::query($query25);
                   $num25=mysql_numrows($result25);
                   //mysql_close();

                   if ($num25>0){
                      $nutr_convert=@mysql_result($result25,0,"convert");
             
                      $nutrition[cholesterol]=round($nutrition[cholesterol]+(($cholesterol/($nutr_serv_size*$nutr_convert))*($rec_num/$numserv)),1);
                      $nutrition[calories]=round($nutrition[calories]+(($calories/($nutr_serv_size*$nutr_convert))*($rec_num/$numserv)),1);
                      $nutrition[cal_from_fat]=round($nutrition[cal_from_fat]+(($cal_from_fat/($nutr_serv_size*$nutr_convert))*($rec_num/$numserv)),1);
                      $nutrition[fiber]=round($nutrition[fiber]+(($fiber/($nutr_serv_size*$nutr_convert))*($rec_num/$numserv)),1);
                      $nutrition[protein]=round($nutrition[protein]+(($protein/($nutr_serv_size*$nutr_convert))*($rec_num/$numserv)),1);
                      $nutrition[carbs]=round($nutrition[carbs]+(($carbs/($nutr_serv_size*$nutr_convert))*($rec_num/$numserv)),1);
                      $nutrition[sodium]=round($nutrition[sodium]+(($sodium/($nutr_serv_size*$nutr_convert))*($rec_num/$numserv)),1);
                      $nutrition[unsat_fat]=round($nutrition[unsat_fat]+(($unsat_fat/($nutr_serv_size*$nutr_convert))*($rec_num/$numserv)),1);
                      $nutrition[sat_fat]=round($nutrition[sat_fat]+(($sat_fat/($nutr_serv_size*$nutr_convert))*($rec_num/$numserv)),1);
                      $nutrition[total_fat]=round($nutrition[total_fat]+(($total_fat/($nutr_serv_size*$nutr_convert))*($rec_num/$numserv)),1);
                      $nutrition[potassium]=round($nutrition[potassium]+(($potassium/($nutr_serv_size*$nutr_convert))*($rec_num/$numserv)),1);

                   }
                   elseif($num24>0){
                      $nutrition[cholesterol]=round($nutrition[cholesterol]+$cholesterol,1);
                      $nutrition[calories]=round($nutrition[calories]+$calories,1);
                      $nutrition[cal_from_fat]=round($nutrition[cal_from_fat]+$cal_from_fat,1);
                      $nutrition[fiber]=round($nutrition[fiber]+$fiber,1);
                      $nutrition[protein]=round($nutrition[protein]+$protein,1);
                      $nutrition[carbs]=round($nutrition[carbs]+$carbs,1);
                      $nutrition[sodium]=round($nutrition[sodium]+$sodium,1);
                      $nutrition[unsat_fat]=round($nutrition[unsat_fat]+$unsat_fat,1);
                      $nutrition[sat_fat]=round($nutrition[sat_fat]+$sat_fat,1);
                      $nutrition[total_fat]=round($nutrition[total_fat]+$total_fat,1);
                      $nutrition[potassium]=round($nutrition[potassium]+$potassium,1);

                   }
                }

                $num32--;
             }

             ///////CALORIES
             if ($showme==1){
                $is_healthy=0;
                if($nutrition[calories]<=500){$is_healthy++;}
                if(($nutrition[cal_from_fat]/$nutrition[calories])<=.3){$is_healthy++;}

                if ($is_healthy==2){$showhealthy=" <img src=img_calorie2.jpg align=middle height=12 width=12 alt='Low Calories'>";}
             }
             ///////FAT
             elseif ($showme==2){
                $is_healthy=0;
                if(($nutrition[cal_from_fat]/$nutrition[calories])<=.3){$is_healthy++;}

                if ($is_healthy==1){$showhealthy=" <img src=img_fat2.jpg align=middle height=12 width=12 alt='Low Fat'>";}
             }
             ///////CHOLESTEROL
             elseif ($showme==3){
                $is_healthy=0;
                if($nutrition[cholesterol]<=60){$is_healthy++;}

                if ($is_healthy==1){$showhealthy=" <img src=img_cholesterol2.jpg align=middle height=12 width=12 alt='Low Cholesterol'>";}
             }
             ///////SODIUM
             elseif ($showme==4){
                $is_healthy=0;
                if($nutrition[sodium]<=575){$is_healthy++;}

                if ($is_healthy==1){$showhealthy=" <img src=img_sodium2.jpg align=middle height=12 width=12 alt='Low Sodium'>";}
             }
             ///////CARBS
             elseif ($showme==5){
                $is_healthy=0;
                if($nutrition[carbs]<20){$is_healthy++;}

                if ($is_healthy==1){$showhealthy=" <img src=img_carbo2.jpg align=middle height=12 width=12 alt='Low Carbohydrates'>";}
             }
             ///////Weight Watchers
             elseif ($showme==6){
                if($nutrition[fiber]>4){$nutrition[fiber]=4;}
                $showhealthy=round(($nutrition[calories]/50)+($nutrition[total_fat]/12)-($nutrition[fiber]/5),0);
                $showhealthy=" <div style=\"background:url('tp2.jpg') no-repeat; margin-top:2px; color:white; font-size:9px; font-weight:bold; height:12px; width:12px; font-family:arial; line-height:12px; text-align:center; display:inline;\">$showhealthy</div>";
             }
          }

          ////////////////////////////////////
          //////////////END NUTRITION SPECS///
          ////////////////////////////////////

          if ($heart_healthy==1&&$no_nutrition==0){$show_healthy=" <a onclick=popup('nutrition.php?mid=$menu_itemid') title='Click for Nutritional Information'><img src=hh2.jpg align=middle height=13 width=12></a>";}
          elseif($heart_healthy==1&&$no_nutrition==1){$show_healthy=" <img src=hh2.jpg align=middle height=13 width=12>";}
          else{$show_healthy="";}

          //mysql_connect($dbhost,$username,$password);
          //@mysql_select_db($database) or die( "Unable to select database");
          $query3 = "SELECT * FROM menu_alt_price WHERE menu_itemid = '$menu_itemid' ORDER BY priceid DESC";
          $result3 = Treat_DB_ProxyOld::query($query3);
          $num3=mysql_numrows($result3);
          //mysql_close();

          $alt_pricing="";
          $num3--;
          while($num3>=0){
             $price2=@mysql_result($result3,$num3,"price");
             $comment=@mysql_result($result3,$num3,"comment");

             $price2=money($price2);
             $alt_pricing="$alt_pricing<br>$price2/$comment";

             $num3--;
          }

          if($price3>0){$showprice="$price3/$unit";}
          else{$showprice="$unit";}

          if($no_nutrition==0){echo "$showhealthy&nbsp;<font size=1><b><FONT size=1 FACE= ARIAL COLOR=#000000 onMouseOver=this.style.color='#FF9900';this.style.cursor='hand' onMouseOut=this.style.color='#000000'><a onclick=popup('nutrition.php?mid=$menu_itemid') title='Click for Nutritional Information'>$item_name</a></font></b></font> <br><font color=gray face=arial size=1>$showprice</font>$showadd$show_healthy<font color=gray face=arial size=1>$alt_pricing</font><br>";}
          else{echo "<font size=1><b><FONT FACE= ARIAL COLOR=#000000>$item_name</font><br></b><font color=gray face=arial>$showprice$showadd$show_healthy$showhealthy$alt_pricing</font><br>";}
          $num--;
       }
       echo "<p>";
       }
       $num4--;
       }
       echo "</td>";
       $today=nextday($today);
    }
    echo "</table>";

    echo "<DIV ID=testdiv1 STYLE=position:absolute;visibility:hidden;background-color:white;layer-background-color:white;></DIV>";

    google_page_track();
    echo '</body></html>';
}
?>
</html>
