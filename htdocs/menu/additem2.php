<?php

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      if ($month == "03"){$month="04";}
      if ($month == "05"){$month="06";}
      if ($month == "07"){$month="08";}
      if ($month == "08"){$month="09";}
      if ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='32')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+3);
        if ($diff > 0 && $diff < '0.01'){$diff="0.01";}
        elseif($diff < 0 && $diff > '-0.01'){$diff="-0.01";}
        return $diff;
}

define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once(DOC_ROOT.'/bootstrap.php');

$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";

$user = isset($_COOKIE["usercook"])?$_COOKIE["usercook"]:'';
$pass = isset($_COOKIE["passcook"])?$_COOKIE["passcook"]:'';
$businessid = isset($_COOKIE["businessid"])?$_COOKIE["businessid"]:'';
$curmenu = isset($_COOKIE["curmenu"])?$_COOKIE["curmenu"]:'';
$reserveid = isset($_POST['reserveid'])?$_POST['reserveid']:'';
$accountid = isset($_POST['accountid'])?$_POST['accountid']:'';
if($accountid==""){$accountid = isset($_COOKIE["accountid"])?$_COOKIE["accountid"]:'';}

$qty = isset($_POST['qty'])?$_POST['qty']:'';
$item = isset($_POST['menu_item_id'])?$_POST['menu_item_id']:'';
$groupid = isset($_POST['groupid'])?$_POST['groupid']:'';
$optionnum = isset($_POST['optionnum'])?$_POST['optionnum']:'';
$special = isset($_POST['special'])?$_POST['special']:'';
$special=str_replace("'","`",$special);
$showname = isset($_POST['item_name'])?$_POST['item_name']:'';

if($item==""){
   $item = isset($_GET['item'])?$_GET['item']:''; 
   $optionnum=-1;
   $qty=1;
   $goto = isset($_GET['goto'])?$_GET['goto']:'';
   $reserveid = isset($_COOKIE["reserveid"])?$_COOKIE["reserveid"]:'';
}

if ($qty == "" || $item == "")
{
    
}

else
{
    if ($reserveid==""){
       $showyear=substr($today,2,2);
       $showmonth=substr($today,5,2);
       $showday=substr($today,8,2);
       $comment="$showmonth/$showday/$showyear";

       $temp_date=nextday($today);$temp_date=nextday($temp_date);

       $query = "INSERT INTO reserve (businessid, companyid, date, accountid, status, comment, curmenu) VALUES ('$businessid', '1', '$temp_date', '$accountid', '1', '$comment', '$curmenu')";
       $result = Treat_DB_ProxyOld::query($query);
       $reserveid = mysql_insert_id();
     
       setcookie("reserveid",$reserveid);
    }

    $query = "SELECT * FROM menu_items WHERE menu_item_id = '$item'";
    $result = Treat_DB_ProxyOld::query($query);

    $itemid=$item;
    $item=@mysql_result($result,0,"item_name");
    $price=@mysql_result($result,0,"price");
    $sales_acct=@mysql_result($result,0,"sales_acct");
    $tax_acct=@mysql_result($result,0,"tax_acct");
    $nontax_acct=@mysql_result($result,0,"nontax_acct");

    while ($optionnum>=0){
       $option = isset($_POST[$optionnum])?$_POST[$optionnum]:'';

       if ($option!=""){
          $query5 = "SELECT * FROM options WHERE optionid = '$option'";
          $result5 = Treat_DB_ProxyOld::query($query5);

          $optionprice=@mysql_result($result5,0,"price");
          $optiondesc=@mysql_result($result5,0,"description");
          $noprint=@mysql_result($result5,0,"noprint");

          $price=$price+$optionprice;
          if ($detail==""&&$noprint==0){$detail="$optiondesc";}
          elseif($noprint==0){$detail="$detail, $optiondesc";}
       }
       else{
          $counting=20;
          while ($counting>=0)
          {
             $optionname="$optionnum-$counting";
             $option = isset($_POST[$optionname])?$_POST[$optionname]:'';

             if ($option!=""){
                $query5 = "SELECT * FROM options WHERE optionid = '$option'";
                $result5 = Treat_DB_ProxyOld::query($query5);

                $optionprice=@mysql_result($result5,0,"price");
                $optiondesc=@mysql_result($result5,0,"description");

                $price=$price+$optionprice;
                if ($detail==""){$detail="$optiondesc";}
                else {$detail="$detail, $optiondesc";}
             }
             $counting--;
          }
       }
       $optionnum--;
    }

    if ($detail==""){$detail=";$special";}
    elseif ($special==""){}
    else {$detail="$detail; $special";}

    $query = "INSERT INTO invoicedetail (invoiceid,qty,item,price,taxed,reserveid,menu_item_id,detail,sales_acct,tax_acct) VALUES ('0','$qty','$item','$price','on','$reserveid','$itemid','$detail','$sales_acct','$tax_acct')";
    $result = Treat_DB_ProxyOld::query($query);

    $query="UPDATE reserve SET status = '1' WHERE reserveid = '$reserveid'";
    $result = Treat_DB_ProxyOld::query($query);
}

?>
<head>
<SCRIPT LANGUAGE=javascript><!--
function update(){opener.location.reload(true);self.close();}
// --></SCRIPT>
</head>
<body onLoad='update()'>
</body>
<?
google_page_track();
?>
