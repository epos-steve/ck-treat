<?php
if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname(__FILE__) . '/../' ) );
}
require_once( DOC_ROOT . '/bootstrap.php' );
$_SESSION->setUserType( Treat_Session::TYPE_CUSTOMER );

$user = \EE\Controller\Base::getSessionCookieVariable( 'usercook' );
$pass = \EE\Controller\Base::getSessionCookieVariable( 'passcook' );
$accountid = \EE\Controller\Base::getSessionCookieVariable( 'accountid' );
$businessid = \EE\Controller\Base::getSessionCookieVariable( 'businessid' );
$reserveid = \EE\Controller\Base::getSessionCookieVariable( 'reserveid' );
$realuser = \EE\Controller\Base::getSessionCookieVariable( 'realuser' );

$goto = \EE\Controller\Base::getPostOrGetVariable( 'goto' );
$scancode = \EE\Controller\Base::getPostOrGetVariable( 'scancode' );
$scancode_old = \EE\Controller\Base::getPostOrGetVariable( 'scancode_old' );
$old_pass = \EE\Controller\Base::getPostOrGetVariable( 'old_pass' );
$pass1 = \EE\Controller\Base::getPostOrGetVariable( 'pass1' );
$pass2 = \EE\Controller\Base::getPostOrGetVariable( 'pass2' );

$attempted_change = false;
$error = null;

if ( $scancode != $scancode_old ) {
	$scancode = Treat_Model_User_Customer::validateScancode( $scancode );
	$errors = Treat_Model_User_Customer::getValidationErrors();
	if ( !$errors ) {
		$rv = Treat_Model_User_Customer::changeScancodeByUsername( $user, $scancode );
		if ( $rv ) {
			$_SESSION->addFlashMessage( sprintf(
				'Gift Card# successfully changed to "%s"'
				,Treat_Model_User_Customer::showScancode( $scancode )
			) );
			$_SESSION->getUser()->scancode = $scancode;
		}
		else {
			$_SESSION->addFlashError( 'Error attempting to change your Gift Card#' );
		}
	}
	else {
		foreach ( $errors as $error ) {
			$_SESSION->addFlashError( $error );
		}
	}
	$attempted_change = true;
}

if (
	( $pass1 || $pass2 )
) {
	try {
		Treat_Model_User_Customer::changePasswordByUsername( $user, $old_pass, $pass1, $pass2, true );
		$_SESSION->addFlashMessage( 'Password successfully changed' );
#		$error = 2;
	}
	catch ( Treat_Model_User_Exception_PasswordChange $e ) {
		$_SESSION->addFlashError( $e->getMessage() );
#		$error = 1;
	}
	$attempted_change = true;
}

if ( !$attempted_change && defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
	$_SESSION->addFlashDebug( 'No changes made' );
}

$location = sprintf(
	'/menu/account_settings.php?goto=%s&error=%s'
	,$goto
	,$error
);
header( 'Location: ' . $location );
?>