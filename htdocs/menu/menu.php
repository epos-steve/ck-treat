<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once(DOC_ROOT.'/bootstrap.php');

$showmyaccount = null; # Notice: Undefined variable
$showrealuser = null; # Notice: Undefined variable
$businessname2 = null; # Notice: Undefined variable
$groupid = null; # Notice: Undefined variable
$showsaveorder = null; # Notice: Undefined variable

$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");
$bbdayname=date("l");
$bbmonth=date("F");
$bbday=date("j");

$debug = \EE\Controller\Base::getGetVariable( 'debug', null );

$businessid = \EE\Controller\Base::getGetVariable( 'bid', null );
$businessid = intval( $businessid );
if ( $businessid == '' ) {
	$businessid = \EE\Controller\Base::getPostVariable( 'businessid', null );
	$businessid = intval( $businessid );
}
if ( $businessid == '' ) {
	$businessid = \EE\Controller\Base::getSessionCookieVariable( 'businessid', null );
	$businessid = intval( $businessid );
}

$curmenu = \EE\Controller\Base::getGetVariable( 'curmenu', null );
if ( $curmenu == '' ) {
	$curmenu = \EE\Controller\Base::getSessionCookieVariable( 'curmenu', null );
}

$reserveid = \EE\Controller\Base::getPostVariable( 'reserveid', null );
$reserveid = intval( $reserveid );
if ( $reserveid == '' ) {
	$reserveid = \EE\Controller\Base::getGetVariable( 'reserveid', null );
	$reserveid = intval( $reserveid );
}
if ( $reserveid == '' ) {
	$reserveid = \EE\Controller\Base::getSessionCookieVariable( 'reserveid', null );
	$reserveid = intval( $reserveid );
}

$user = \EE\Controller\Base::getSessionCookieVariable( 'usercook', null );
$pass = \EE\Controller\Base::getSessionCookieVariable( 'passcook', null );
$accountid = \EE\Controller\Base::getSessionCookieVariable( 'accountid', null );
$accountid = intval( $accountid );

$realuser = \EE\Controller\Base::getSessionCookieVariable( 'realuser', null );
	
////search
$search = \EE\Controller\Base::getPostVariable( 'search', null );
if ( $search ) {
	\EE\Controller\Base::setCookie( 'search', $search );
}
else {
	$search = \EE\Controller\Base::getSessionCookieVariable( 'search', null );
}

if ( $businessid == '' ) {
	$location = 'index.php';
	header( 'Location: ./' . $location );
}
else {
	$expire = time() + 5184000; // 5184000 = 60 * 60 * 24 * 60
	if ( $businessid > 0 ) {
		\EE\Controller\Base::setCookie( 'businessid', $businessid );
		\EE\Controller\Base::setCookie( 'eebid', $businessid, $expire );
	}
	else {
		\EE\Controller\Base::setCookie( 'businessid', null, 0 );
		\EE\Controller\Base::setCookie( 'eebid', null, 0 );
	}
	if ( $reserveid > 0 ) {
		\EE\Controller\Base::setCookie( 'reserveid', $reserveid );
	}
	else {
		\EE\Controller\Base::setCookie( 'reserveid', null, 0 );
	}
	if ( $curmenu > 0 ) {
		\EE\Controller\Base::setCookie( 'curmenu', $curmenu );
	}
	else {
		\EE\Controller\Base::setCookie( 'curmenu', null, 0 );
	}

	\EE\Model\Business\Setting::db( Treat_DB::singleton() );
	$settings = \EE\Model\Business\Setting::get("business_id = ". abs(intval($businessid)), true);
	if ( !$settings ) {
		$settings = new \EE\Null();
	}
	$background_image_logo = $settings->background_image ?: '';
	$site_logo = $settings->site_logo ?: '/assets/images/nutrition/logo_205x43.jpg';

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM company WHERE companyid = '1'";
	$result = Treat_DB_ProxyOld::query( $query );
	//mysql_close();

	$companyname = Treat_DB_ProxyOld::mysql_result( $result, 0, 'companyname' );

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM business WHERE businessid = '$businessid'";
	$result = Treat_DB_ProxyOld::query( $query );
	//mysql_close();

	$businessname = Treat_DB_ProxyOld::mysql_result( $result, 0, 'companyname' );
	$businessname2 = Treat_DB_ProxyOld::mysql_result( $result, 0, 'businessname' );
	$taxrate = Treat_DB_ProxyOld::mysql_result( $result, 0, 'tax' );
	$default_menu = Treat_DB_ProxyOld::mysql_result( $result, 0, 'default_menu' );
	$default_menu2 = Treat_DB_ProxyOld::mysql_result( $result, 0, 'cafe_menu' );
	$vend_menu = Treat_DB_ProxyOld::mysql_result( $result, 0, 'vend_menu' );
	$multi_cafe_menu = Treat_DB_ProxyOld::mysql_result( $result, 0, 'multi_cafe_menu' );

?>
<html>
	<head>
		<title>Menu :: Treat America: <?php echo $businessname2; ?></title>
<?php includeTimeOutCode(); ?>
<script language="JavaScript" type="text/JavaScript">
function popup(url)
{
	popwin=window.open(url,"Nutrition","location=no,menubar=no,titlebar=no,resizeable=no,scrollbars=yes,height=600,width=415");   
}
</script>

<script language="JavaScript" type="text/JavaScript">
function showhide()
{
	document.all.showsave.style.display = 'none';
	document.all.submitsave.style.display = 'block';
}
</script>

		<script type="text/javascript" src="/assets/js/calendarPopup.js"></script>
		<link rel="stylesheet" type="text/css" href="/assets/css/calendarPopup.css" />

	</head>
<?php

	$catering_policy = "SELECT businessid, policy FROM caterpolicy WHERE businessid = '$businessid'";
	$catering_policy_result = Treat_DB_ProxyOld::query( $catering_policy );
	$caterpolicy = Treat_DB_ProxyOld::mysql_result( $catering_policy_result, 0, 'policy' );

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM accounts WHERE accountid = '$accountid'";
	$result = Treat_DB_ProxyOld::query( $query );
	//mysql_close();

	$accountname = Treat_DB_ProxyOld::mysql_result( $result, 0, 'name' );
	$accountnum = Treat_DB_ProxyOld::mysql_result( $result, 0, 'accountnum' );
	$taxid = Treat_DB_ProxyOld::mysql_result( $result, 0, 'taxid' );
	$acct_menu = Treat_DB_ProxyOld::mysql_result( $result, 0, 'menu' );

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM menu_type WHERE menu_typeid = '$curmenu'";
	$result = Treat_DB_ProxyOld::query( $query );
	//mysql_close();

	$contract = Treat_DB_ProxyOld::mysql_result( $result, 0, 'type' );
	$menu_name = Treat_DB_ProxyOld::mysql_result( $result, 0, 'menu_typename' );

	/*
	if ($contract==1){
		$location="menu2.php";
		header('Location: ./' . $location); 
	}
	*/

	if ( $reserveid > 0 ) {
		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query = "SELECT * FROM reserve WHERE reserveid = '$reserveid'";
		$result = Treat_DB_ProxyOld::query( $query );
		$num = intval( Treat_DB_ProxyOld::mysql_num_rows( $result ) );
		//mysql_close();

		$date = Treat_DB_ProxyOld::mysql_result( $result, 0, 'date' );
		$room = Treat_DB_ProxyOld::mysql_result( $result, 0, 'roomid' );
		$start_hour = Treat_DB_ProxyOld::mysql_result( $result, 0, 'start_hour' );
		$end_hour = Treat_DB_ProxyOld::mysql_result( $result, 0, 'end_hour' );
		$status = Treat_DB_ProxyOld::mysql_result( $result, 0, 'status' );
		$comment = Treat_DB_ProxyOld::mysql_result( $result, 0, 'comment' );
		$roomcomment = Treat_DB_ProxyOld::mysql_result( $result, 0, 'roomcomment' );
		$peoplenum = Treat_DB_ProxyOld::mysql_result( $result, 0, 'peoplenum' );
		$reserve_menu = Treat_DB_ProxyOld::mysql_result( $result, 0, 'curmenu' );

		if ( $curmenu != $reserve_menu && $reserveid > 0 ) {
			$query = "DELETE FROM invoicedetail WHERE reserveid = '$reserveid'";
			$result = Treat_DB_ProxyOld::query( $query );

			$query = "UPDATE reserve SET curmenu = '$curmenu' WHERE reserveid = '$reserveid'";
			$result = Treat_DB_ProxyOld::query( $query );
		}
	}

	if ( $reserveid > 0 && $curmenu != $default_menu2 && $curmenu != $vend_menu ) {
		$showcart = "<a style=$style href=caterdetails.php?reserveid=$reserveid&accountid=$accountid#invoice><font color=white><b>Check Out </b><img src=chefHat.gif width=29 height=27 border=0></font></a>";
	}
	else {
		$showcart = "";
	}

?>
	<body background="<?php echo $background_image_logo; ?>">
		<center>
			<table cellspacing="0" cellpadding="0" border="0" width="90%">
				<tr>
					<td colspan="4">
						<img src="<?php echo $site_logo; ?>" width="205" height="43" alt="site logo" />
						<p>
					</td>
				</tr>
<?php

	if ( $accountid > 0 ) {
		$logout = "Sign Off";
		$showmyaccount = "<a href=account_settings.php?goto=2 style=$style><FONT COLOR=#FFFFFF onMouseOver=this.style.color='#FF9900';this.style.cursor='hand' onMouseOut=this.style.color='#FFFFFF'><b>My Account</b></font></a>&nbsp;";
	}
	else {
		$logout = "Sign In";
	}

	if ( $realuser != "" ) {
		$showrealuser = "<i> as $realuser</i>";
	}

	/////search
	if ( $curmenu == $default_menu ) {
		$showsearch = "<form action=menuitems.php method=post style=\"margin:0;padding:0;display:inline;\"><input name=search size=25><input type=submit value='Search' style=\"border: 1px solid #CCCCFF; background-color:#E8E7E7;\"></form>";
	}
	else {
		$showsearch = "";
	}

?>
				<tr bgcolor="#6699FF">
					<td colspan="2">
						<font color="white" size="3">
							<b>
								The Right Choice... for a Healthier You!
							</b>
						</font>
					</td>
					<td align="right" colspan="2">
						<?php echo $showmyaccount; ?> 
					</td>
				</tr>
				<tr bgcolor="#669933">
					<td width="10%">
						<img src="rc-header2.jpg" height="80" width="108" alt="rc-header2" />
					</td>
					<td>
						<font size="4" color="white">
							<b>
								<?php
									echo $companyname;
								?> - <?php
									echo $businessname;
								?>
							</b>
						</font>
						<br />
						<b>
							<font color="white">
								Account: <?php
									echo $accountname;
								?> <?php
									echo $showrealuser;
								?> 
								<br>
								Account #: <?php
									echo $accountnum;
								?> 
							</font>
						</b>
					</td>
					<td colspan="2" align="right" valign="top">
						<font color="white">
							<b>
								<?php echo $bbdayname; ?>,
								<?php echo $bbmonth, ' ', $bbday, ', ', $year; ?>
							</b>
							<br />
							<font size="2">
								[<a style="<?php
									echo $style;
								?>" href="/menu/logout.php?goto=1"
									><b><font color="white"><?php
									echo $logout;
									?></font></b></a>]
							</font>
							<p>
							<?php echo $showsearch; ?> 
							&nbsp; <?php echo $showcart; ?>
						</font>
					</td>
				</tr>
				<tr bgcolor="black">
					<td height="1" colspan="4"></td>
				</tr>
<?php
	if ( $curmenu == $default_menu ) {
?>
				<tr>
					<td colspan="2">
						<font face="arial" size="2">
							<?php echo $menu_name; ?>
							::
<?php
		if ( $caterpolicy != "" ) {
?>
							<a href="/menu/caterpolicy.php?<?php
								echo http_build_query( array(
									'bid' => $businessid,
									'curmenu' => $curmenu,
								), null, '&amp;' );
							?>" style="<?php
								echo $style;
							?>">
								<font color="blue" face="arial" size="2">
									Policy/Procedures
								</font>
							</a>
							::
<?php
		}
?>

							<a href="/menu/catertrack.php" style="<?php echo $style; ?>">
								<font color="blue" face="arial" size="2">
									Order History
								</font>
							</a>
							::
							<a href="/menu/createorder.php" style="<?php echo $style; ?>">
								<font color="blue" face="arial" size="2">
									Schedule a Catering
								</font>
							</a>
							::
							<a href="/menu/intro.php?bid=<?php echo $businessid; ?>" style="<?php echo $style; ?>">
								<font color="blue" face="arial" size="2">
									Home
								</font>
							</a>
						</font>
					</td>
					<td align="right" colspan="2">
						<a href="/menu/printmenu.php?curmenu=<?php echo $curmenu; ?>" style="<?php echo $style; ?>" target="_blank">
							<font color="blue" face="arial" size="2">
								Printable Version
							</font>
						</a>
					</td>
				</tr>
<?php
	}
	elseif ( $curmenu == $default_menu2 || $curmenu == $vend_menu || $multi_cafe_menu == 1 ) {
?>
				<tr>
					<td colspan="2">
						<font face="arial" size="2">
							<?php echo $menu_name; ?> 
							::
<?php
		if ( $caterpolicy != "" ) {
?>
							<a href="/menu/caterpolicy.php?curmenu=<?php
								echo $curmenu;
							?>&bid=<?php
								echo $businessid;
							?>" style="<?php
								echo $style;
							?>">
								<font color="blue" face="arial" size="2">
									Policy/Procedures
								</font>
							</a>
							::
<?php
		}
?>
							<a href="/menu/history.php" style="<?php echo $style; ?>">
								<font color="blue" face="arial" size="2">
									Nutritional Analysis
								</font>
							</a>
							::
							<a href="/menu/intro.php?bid=<?php echo $businessid; ?>" style="<?php echo $style; ?>">
								<font color="blue" face="arial" size="2">
									Home
								</font>
							</a>
						</font>
					</td>
					<td align="right" colspan="2">
						<a href="/menu/printmenu.php?curmenu=<?php echo $curmenu; ?>" style="<?php echo $style; ?>" target="_blank">
							<font color="blue" face="arial" size="2">
								Printable Version
							</font>
						</a>
					</td>
				</tr>
<?php
	}
?>
			</table>
		</center>
		<p>
<?php

	if ( $reserveid > 0 ) {

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query = "SELECT * FROM invoicedetail WHERE reserveid = '$reserveid' ORDER BY itemid DESC";
		$result = Treat_DB_ProxyOld::query( $query );
		$num = intval( Treat_DB_ProxyOld::mysql_num_rows( $result ) );
		//mysql_close();
		if ( $num > 0 ) {
			if ( $curmenu == $default_menu2 && $num > 0 ) {
				$shownutrition = "<a onclick=popup('nutrition2.php')><img src=heart.jpg height=16 width=14 alt='Nutrtional Information for this Meal' border=0 onMouseOver=this.style.cursor='hand'></a>";
			}
			else {
				$shownutrition = "";
			}

?>
		<center>
			<table width="90%" cellspacing="0" cellpadding="0" style="border:2px solid #999999;font-family: arial;">
				<tr>
					<td bgcolor="#999999" colspan="6">
						<font size="2" color="white">
							<b>
								Order Details:
							</b>
						</font>
					</td>
				</tr>
<?php
			$subtotal = 0;
			$num--;
			$showcolor = "white";

			while ( $num >= 0 ) {
				$itemid = Treat_DB_ProxyOld::mysql_result( $result, $num, 'itemid' );
				$qty = Treat_DB_ProxyOld::mysql_result( $result, $num, 'qty' );
				$item = Treat_DB_ProxyOld::mysql_result( $result, $num, 'item' );
				$price = Treat_DB_ProxyOld::mysql_result( $result, $num, 'price' );
				$detail = Treat_DB_ProxyOld::mysql_result( $result, $num, 'detail' );

				if ( $detail == ";" ) {
					$detail = "";
				}

				if ( $qty < 1 ) {
					$newqty = 1;
				}
				else {
					$newqty = $qty;
				}

				$itemtotal = money( $newqty * $price );
				$price = money( $price );

?>
				<tr>
					<td bgcolor="<?php echo $showcolor; ?>" valign="top" width="5%">
						<font size="2">
							<?php echo $qty; ?> 
						</font>
					</td>
					<td bgcolor="<?php echo $showcolor; ?>" valign="top" width="20%">
						<font size="2">
							<?php echo $item; ?> 
						</font>
					</td>
					<td bgcolor="<?php echo $showcolor; ?>" align="right" valign="top" width="10%">
						<font size="2">
							$<?php echo $price; ?> 
						</font>
					</td>
					<td bgcolor="<?php echo $showcolor; ?>" align="right" valign="top" width="10%">
						<font size="2">
							$<?php echo $itemtotal; ?> 
						</font>
					</td>
					<td bgcolor="<?php echo $showcolor; ?>" width="4%"></td>
					<td bgcolor="<?php echo $showcolor; ?>">
						<font size="2">
							<?php echo $detail; ?> 
						</font>
						<font size="1">
							[<a href="/menu/delitem.php?<?php
								echo http_build_query( array(
									'bid' => $businessid,
									'curmenu' => $curmenu,
									'itemid' => $itemid,
									'goto' => 2,
									'groupid' => $groupid,
								), null, '&amp;' );
							?>" style="<?php
								echo $style;
							?>"><font color="blue">REMOVE</font></a>]
						</font>
					</td>
				</tr>
<?php

				if ( $showcolor == "white" ) {
					$showcolor = "#E8E7E7";
				}
				else {
					$showcolor = "white";
				}
				$subtotal = $subtotal + ( $qty * $price );
				$num--;
			}
			$subtotal = money( $subtotal );

			if ( $curmenu == $default_menu2 || $curmenu == $vend_menu ) {
				$showsaveorder = "<a href=\"javascript:void();\" onclick=\"showhide();\" style=$style><font size=2 COLOR=#0000FF onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#0000FF'>Save Order</font></a>";
			}

			$mydate = date("Y-m-d");

?>
				<tr valign="top">
					<td bgcolor="<?php echo $showcolor; ?>" colspan="2">
						<?php echo $shownutrition; ?> 
					</td>
					<td bgcolor="<?php echo $showcolor; ?>" align="right">
						<font size="2">
							Subtotal:
						</font>
					</td>
					<td bgcolor="<?php echo $showcolor; ?>" align="right">
						<font size="2">
							$<?php echo $subtotal; ?>
						</font>
					</td>
					<td bgcolor="<?php echo $showcolor; ?>" colspan="2" align="right">
						<form name="save" action="/menu/savemenu.php" method="post" style="margin:0;padding:0;display:inline;">
							<div id="showsave" name="showsave" style="display:inline;">
								<?php echo $showsaveorder; ?> 
							</div>
							<div name="submitsave" id="submitsave" style="display:none;">
								<script type="text/javascript" language='JavaScript' id='js18'>
									var cal18 = new CalendarPopup('testdiv1');
									cal18.setCssPrefix('TEST');
								</script>
								<input type="text" name="mydate" value="<?php echo $mydate; ?>" size="8" style="font-size: 9px;" />
								<a href="javascript:void();"
									onClick="cal18.select(document.forms[0].mydate,'anchor18','yyyy-MM-dd'); return false;"
									title="cal18.select(document.forms[0].mydate,'anchor1x','yyyy-MM-dd'); return false;"
									name="anchor18" id="anchor18"
								>
									<img src="calendar.gif" border="0" height="16" width="16" alt="Choose a Date" />
								</a>
								<input type="submit" value="Save" style="font-size: 9px;" />
							</div>
						</form>
					</td>
				</tr>
			</table>
		</center>
		<p>
<?php
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///MENU GROUPS
	
?>
		<center>
			<table width="90%" cellspacing="0" cellpadding="0">
<?php
	
	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "
		SELECT menu_groups.* FROM menu_groups
		WHERE menu_groups.menu_typeid = $curmenu
		AND
			menu_groups.deleted = 0
		AND
			menu_groups.menu_group_id NOT IN (SELECT menu_groupid FROM menu_groups_active WHERE businessid = $businessid)
		ORDER BY menu_groups.orderid DESC
	";
	$result = Treat_DB_ProxyOld::query( $query );
	$num = intval( Treat_DB_ProxyOld::mysql_num_rows( $result ) );
	//mysql_close();
	
	$mycount = 1;
	
	///show favorites/recent
	if ( $accountid > 0 && $curmenu == $default_menu ) {
		$mycount = 2;
		
?>
				<tr>
					<td>
						<table width="100%" cellspacing="0" cellpadding="0" border="0"
							style="background-image:url('group.jpg');background-repeat: repeat-x;border: 1px solid #999999; background-color: #C0C0C0;"
							onMouseOver="this.style.backgroundImage='url()'; this.style.cursor='pointer';"
							onMouseOut="this.style.backgroundImage='url(group.jpg)';"
							onclick="document.location.href='/menu/menuitems.php?<?php
								echo http_build_query( array(
									'bid' => $businessid,
									'curmenu' => $curmenu,
									'reserveid' => $reserveid,
									'account' => $accountid,
									'groupid' => -1,
								), null, '&amp;' );
							?>'"
						>
							<tr>
								<td width="5" rowspan="2" height="100"></td>
								<td width="120" rowspan="2">
									<a href="/menu/menuitems.php?<?php
										echo http_build_query( array(
											'bid' => $businessid,
											'curmenu' => $curmenu,
											'reserveid' => $reserveid,
											'account' => $accountid,
											'groupid' => -1,
										), null, '&amp;' );
									?>">
										<img src="/assets/images/favorites.png" height="87" width="84" border="0" alt="favorites" />
									</a>
								</td>
								<td>
									<font size="4">
										<b>
											<i>
												<a href="/menu/menuitems.php?<?php
													echo http_build_query( array(
														'bid' => $businessid,
														'curmenu' => $curmenu,
														'reserveid' => $reserveid,
														'account' => $accountid,
														'groupid' => -1,
													), null, '&amp;' );
												?>" style="<?php
													echo $style;
												?>">
													<img src="/ta/star.gif" border="0" alt="star" />
													<font color="black">
														Favorites
													</font>
												</a>
											</i>
										</b>
									</font>
								</td>
							</tr>
							<tr>
								<td>
									<font size="2" face="arial" color="black">
										Your 10 Most Frequent Purchases.
									</font>
								</td>
							</tr>
						</table>
					</td>
					<td>&nbsp;</td>
<?php
	}

	$num--;
	
	while ( $num >= 0 ) {
		$groupid = Treat_DB_ProxyOld::mysql_result( $result, $num, 'menu_group_id' );
		$groupname = Treat_DB_ProxyOld::mysql_result( $result, $num, 'groupname' );
		$description = Treat_DB_ProxyOld::mysql_result( $result, $num, 'description' );
		$image = Treat_DB_ProxyOld::mysql_result( $result, $num, 'image' );
		$img_height = Treat_DB_ProxyOld::mysql_result( $result, $num, 'img_height' );
		$img_width = Treat_DB_ProxyOld::mysql_result( $result, $num, 'img_width' );

		if ( $mycount == 1 ) {
?>
<?php
			echo "<tr>";
?>
<?php
		}
		else {
		}

		if ( strlen( $description ) > 225 ) {
			$description = substr( $description, 0, 225 );
			$description .= "...";
		}

?>
					<td>
						<table width="100%" cellspacing="0" cellpadding="0" border="0"
							style="background-image:url('group.jpg');background-repeat: repeat-x;border: 1px solid #999999; background-color: #C0C0C0;"
							onMouseOver="this.style.backgroundImage='url()'; this.style.cursor='pointer';"
							onMouseOut="this.style.backgroundImage='url(group.jpg)';"
							onclick="document.location.href='/menu/menuitems.php?<?php
								echo http_build_query( array(
									'bid' => $businessid,
									'curmenu' => $curmenu,
									'reserveid' => $reserveid,
									'account' => $accountid,
									'groupid' => $groupid,
								), null, '&amp;' );
							?>'"
						>
							<tr>
								<td width="5" rowspan="2" height="100"></td>
								<td width="120" rowspan="2">
									<a href="/menu/menuitems.php?<?php
										echo http_build_query( array(
											'bid' => $businessid,
											'curmenu' => $curmenu,
											'reserveid' => $reserveid,
											'account' => $accountid,
											'groupid' => $groupid,
										), null, '&amp;' );
									?>">
										<img src="pictures/<?php
											echo $image;
										?>" height="<?php
											echo $img_height;
										?>" width="<?php
											echo $img_width;
										?>" border="0" alt="<?php
											echo htmlentities( $image );
										?>" />
									</a>
								</td>
								<td>
									<font size="4">
										<b>
											<i>
												<a href="/menu/menuitems.php?<?php
													echo http_build_query( array(
														'bid' => $businessid,
														'curmenu' => $curmenu,
														'reserveid' => $reserveid,
														'account' => $accountid,
														'groupid' => $groupid,
													), null, '&amp;' );
												?>" style="<?php
													echo $style;
												?>">
													<font color="black">
														<?php echo $groupname; ?> 
													</font>
												</a>
											</i>
										</b>
									</font>
								</td>
							</tr>
							<tr>
								<td>
									<font size="2" face="arial" color="black">
										<?php echo $description; ?> 
									</font>
								</td>
							</tr>
						</table>
					</td>
<?php
		
		if ( $mycount == 1 ) {
?>
<?php
			echo "<td>&nbsp;</td>";
?>
<?php
		}
		else {
?>
<?php
			echo "</tr><tr height=5><td colspan=3></td></tr>";
?>
<?php
		}
		
		$num--;
		if ( $mycount == 1 ) {
			$mycount = 2;
		}
		else {
			$mycount = 1;
		}
	}
	
	if ( $mycount == 1 ) {
?>
<?php
		echo "<td></td></tr>";
?>
<?php
	}
	
?>
			</table>
		</center>
		
		<div id="testdiv1" style="position:absolute;visibility:hidden;background-color:white;layer-background-color:white;"></div>
<?php
	google_page_track();
?>

	</body>
</html>
<?php
}
//mysql_close();
?>
