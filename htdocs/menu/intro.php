<?php
# TODO  NOTICE: Please read notes before modifying this file
/**
 *	If you are here to modify this file, please be aware that this page duplicates a lot of code.
 *	Check to see if where you're editing is within the giant if/else code and if so you may need to
 *	modify both parts.
 * 
 *	# TODO Begin Weekly Section
 *	if ( $week < 1 ) {
 *		// ~686 lines of code
 *		// loop for Monday - Friday
 *			// loop for menu items/groups
 *	}
 *	# TODO Begin Daily Section
 *	else {
 *		// ~704 lines of code
 *			// loop for menu items/groups
 *	}
 * 
 *	You will notice odd indentation depths between the if(){} section & the else{} section. Please
 *	leave the odd indentation depths as it helps in sorting out the differences between the 2
 *	sections. Also, if you need to modify a portion of code inside either the if(){} or else{}
 *	section, please keep the indentation level consistent in both the if(){} and else{} sections
 *	and set to the deeper level needed.
 * 
 *	Yes, this may be annoying, but it helps with cleaning up this document.
 */
define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once(DOC_ROOT.'/bootstrap.php');

$date = null; # Notice: Undefined variable
$showmyaccount = null; # Notice: Undefined variable
$curmenu = null; # Notice: Undefined variable

$businessid = \EE\Controller\Base::getPostGetSessionOrCookieVariable( array( 'bid', 'businessid', 'eebid' ), null );
$week = \EE\Controller\Base::getPostGetSessionOrCookieVariable( 'week', null );
$today = \EE\Controller\Base::getGetVariable( 'date', null );
$showme = \EE\Controller\Base::getGetVariable( 'showme', null );
$reserveid = \EE\Controller\Base::getPostGetSessionOrCookieVariable( 'reserveid', null );
$user = \EE\Controller\Base::getSessionCookieVariable( 'usercook', null );
$pass = \EE\Controller\Base::getSessionCookieVariable( 'passcook', null );
$accountid = \EE\Controller\Base::getSessionCookieVariable( 'accountid', null );
$kiosk = ( \EE\Controller\Base::getSessionCookieVariable( 'kiosk_mode' ) == 'true' ) ? true : false;

$businessid = intval( $businessid );
$reserveid = intval( $reserveid );
$accountid = intval( $accountid );

$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today2="$year-$month-$day";
if ( $today == '' ) {
	$today = "$year-$month-$day";
	$todayname = dayofweek( $today );
}
elseif ( $today==$today2 ) {
	$todayname = dayofweek( $today2 );
}
else {
	$todayname = dayofweek( $today );
}


$yesterday=prevday($today);
$tomorrow=nextday($today); 

# temporary holding to see which variables we're actually using
$pageVariables = new \EE\ArrayObject();
$pageVariables->businessId = $businessid;
$pageVariables->accountId = $accountid;
$pageVariables->reserveId = $reserveid;
#$pageVariables->customerId = null;
#$pageVariables->realUser = $realuser;
#$pageVariables->groupId = $groupid;
#$pageVariables->showQty = $showqty;
#$pageVariables->showName = $showname;
$pageVariables->showMe = $showme;
#$pageVariables->search = $search;
#$pageVariables->debug = $debug;
if ( false === ( $pageVariables->date = \EE\Date::getValidTimeStamp( $today ) ) ) {
	$pageVariables->date = strtotime( date( 'Y-m-d' ) );
}
$pageVariables->week = ( null !== $week ? intval( $week ) : null );

if ( $businessid == '' ) {
	$location = 'index.php';
	header( 'Location: ./' . $location );
	exit();
}
else {
	$expire = time() + 5184000; // 5184000 = 60 * 60 * 24 * 60
	if ( $businessid > 0 ) {
		\EE\Controller\Base::setCookie( 'businessid', $businessid );
		\EE\Controller\Base::setCookie( 'eebid', $businessid, $expire );
	}
	else {
		\EE\Controller\Base::setCookie( 'businessid', null, 0 );
		\EE\Controller\Base::setCookie( 'eebid', null, 0 );
	}
	if ( null !== $pageVariables->week ) {
		\EE\Controller\Base::setCookie( 'week', $pageVariables->week, $expire );
	}
	else {
		\EE\Controller\Base::setCookie( 'week', null, 0 );
	}
	if ( $reserveid > 0 ) {
		\EE\Controller\Base::setCookie( 'reserveid', $reserveid );
	}
	else {
		\EE\Controller\Base::setCookie( 'reserveid', null, 0 );
	}
	if ( $curmenu > 0 ) {
		\EE\Controller\Base::setCookie( 'curmenu', $curmenu );
	}
	else {
		\EE\Controller\Base::setCookie( 'curmenu', null, 0 );
	}

	$pageBusiness = \EE\Model\Business\Singleton::getSingleton( $pageVariables->businessId );
	if ( !$pageBusiness ) {
		$pageBusiness = new \EE\Null();
	}
	$pageVariables->currentMenu = $pageBusiness->cafe_menu;
	
	$background_image_logo = $pageBusiness->background_image ?: '';
	$home_page_logo = $pageBusiness->home_page_logo ?: '/assets/images/menu/rc-header.jpg';

	$timeFrameViewQueryLink = array(
		'bid' => $pageVariables->businessId,
		'week' => null,
		'showme' => $pageVariables->showMe,
	);
	if ( $pageVariables->week < 1 ) {
		if ( ( $pageVariables->dateToday = strtotime( 'Monday', $pageVariables->date ) ) > $pageVariables->date ) {
			$pageVariables->dateToday = strtotime( 'Last Monday', $pageVariables->date );
		}
		$timeFramePrevious = strtotime( 'Last Monday', $pageVariables->dateToday );
		$timeFrameNext = strtotime( 'Next Monday', $pageVariables->dateToday );
		$timeframeViewLinkText = 'Daily View';
		$timeFrameViewQueryLink['week'] = 1;
		$timeFrame = 'Week';
		$timeFramely = 'Weekly';
	}
	else {
		$pageVariables->dateToday = $pageVariables->date;
		$timeFramePrevious = strtotime( 'Yesterday', $pageVariables->dateToday );
		$timeFrameNext = strtotime( 'Tomorrow', $pageVariables->dateToday );
		$timeframeViewLinkText = 'Weekly View';
		$timeFrameViewQueryLink['week'] = 0;
		$timeFrame = 'Day';
		if ( date( 'Y-m-d' ) == date( 'Y-m-d', $pageVariables->dateToday ) ) {
			$timeFramely = 'Today\'s';
		}
		else {
			$timeFramely = date( 'l\'\s', $pageVariables->dateToday );
		}
	}
	# http query parameters for the showme links
	$introQuery = array(
		'bid' => $pageVariables->businessId,
		'date' => (
			date( 'Y-m-d' ) != date( 'Y-m-d', $pageVariables->date )
			? date( 'Y-m-d', $pageVariables->date )
			: null
		),
		'week' => $pageVariables->week,
		'showme' => null,
	);
	
	$businessname = $pageBusiness->companyname;
	$businessname2 = $pageBusiness->businessname;
	$taxrate = $pageBusiness->tax;
	$default_menu = $pageBusiness->default_menu;
	$cafe_menu = $pageBusiness->cafe_menu;
	$vend_menu = $pageBusiness->vend_menu;
	$catering = $pageBusiness->catering;
	$default_only = $pageBusiness->default_only;
	$no_nutrition = $pageBusiness->no_nutrition;
	$multi_cafe_menu = $pageBusiness->multi_cafe_menu;
	
	if ( $multi_cafe_menu == 1 ) {
		$mymenu = \EE\Controller\Base::getSessionCookieVariable( 'mymenu' );
		
		if ( $mymenu < 1 ) {
			$location = "/menu/menuselect.php?bid=$businessid";
			header( 'Location: ' . $location );
		}
		else {
			$cafe_menu = $mymenu;
		}
	}

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM accounts WHERE accountid = '$accountid'";
	$result = Treat_DB_ProxyOld::query( $query );
	//mysql_close();

	if( Treat_DB_ProxyOld::mysql_num_rows( $result ) ){
		$accountname = Treat_DB_ProxyOld::mysql_result( $result, 0, 'name' );
		$accountnum = Treat_DB_ProxyOld::mysql_result( $result, 0, 'accountnum' );
		$taxid = Treat_DB_ProxyOld::mysql_result( $result, 0, 'taxid' );
		$acct_menu = Treat_DB_ProxyOld::mysql_result( $result, 0, 'menu' );
	}

	$curmenu = $cafe_menu;

	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "SELECT * FROM menu_type WHERE menu_typeid = '$curmenu'";
	$result = Treat_DB_ProxyOld::query( $query );
	//mysql_close();

	$contract = Treat_DB_ProxyOld::mysql_result( $result, 0, 'type' );
	$menu_name = Treat_DB_ProxyOld::mysql_result( $result, 0, 'menu_typename' );
	$parent = Treat_DB_ProxyOld::mysql_result( $result, 0, 'parent' );
	
	//////linked menus
	if ( $parent > 0 ) {
		$daypartid = $curmenu;
		$curmenu = $parent;
	}
	
	//OLD CONTRACT FEEDING
	/*
	if ( $contract == 1 ) {
		$location="menu2.php";
		header('Location: ./' . $location); 
	}
	*/
	if ( $default_only == 1 ) {
		$location = "menu.php?curmenu=$default_menu&bid=$businessid";
		header( 'Location: ./' . $location );
		exit();
	}
?>
<html>
	<head>


<title>Treat America: <?php echo $businessname2; ?></title>

<meta name="robots" content="noarchive" />
<?php
	includeTimeOutCode();


?>

<script language="JavaScript" type="text/javascript">
	var popwin  = null;
	var popwin2 = null;
function popup(url)
{
	popwin=window.open(url,"Nutrition","location=no,menubar=no,titlebar=no,resizeable=yes,scrollbars=yes,height=600,width=415"); 
	popwin.focus();
}
</script>

<script language="JavaScript" type="text/javascript">
function popup2(url)
{
	popwin2=window.open(url,"Tutorial","location=no,scrollbars=yes,menubar=no,titlebar=no,resizeable=yes,height=600,width=425");   
	popwin2.focus();
}
</script>

<script language="JavaScript" type="text/javascript">
function showhide()
{
	document.all.showsave.style.display = 'none';
	document.all.submitsave.style.display = 'block';
}
</script>

		<script type="text/javascript" src="/assets/js/calendarPopup.js"></script>
		<link rel="stylesheet" type="text/css" href="/assets/css/calendarPopup.css" />
		<link rel="stylesheet" type="text/css" href="/assets/css/menu/menu.css" />

	</head>
	<body background="<?php echo $background_image_logo; ?>">
<?php

////////////////////////////DISPLAY   
	if ( $accountid > 0 ) {
		$showlogin = 'Sign Off';
		$showmyaccount = "<font color=white>|</font> <a href=account_settings.php?goto=1 style=$style><FONT COLOR=#FFFFFF onMouseOver=this.style.color='#FF9900';this.style.cursor='hand' onMouseOut=this.style.color='#FFFFFF'><b>My Account</b></font></a>";
	}
	else {
		$showlogin = 'Sign In';
	}

	/////fed reserve caft
	if ( $businessid == 262 || $businessid == 274 ) {
?>
		<table width="100%">
			<tr>
				<td align="right">
					<a href="/menu/intro.php?bid=<?php echo $businessid; ?>" style="<?php echo $style; ?>" target="_blank">
						<font size="1" color="blue" face="arial">Open In a New Window</font>
					</a>
				</td>
			</tr>
		</table>
<?php
	}

?>
		<center>
			<table cellspacing="0" cellpadding="0" border="0" width="700" bgcolor="#669933">
<?php
	if ( $no_nutrition == 0 ) {
?>
				<tr bgcolor="#6699FF">
					<td colspan="2" bgcolor="">
						<font color="white" size="3">
							<b>The Right Choice... for a Healthier You!</b>
						</font>
					</td>
					<td colspan="2" align="right">
						<a onclick="popup2('showme.html')" title="Click for Information">
							<font color="#FFFFFF" onMouseOver="this.style.color='#FF9900';this.style.cursor='hand'" onMouseOut="this.style.color='#FFFFFF'">
								<b>Show Me How!</b>
							</font>
						</a>
						<?php echo $showmyaccount; ?> 
						<font color="white">|</font>
						<a href="logout.php?goto=6&amp;bid=<?php echo $pageVariables->businessId; ?>" style="<?php echo $style; ?>">
							<font color="#FFFFFF" onMouseOver="this.style.color='#FF9900';this.style.cursor='hand'" onMouseOut="this.style.color='#FFFFFF'">
								<b><?php echo $showlogin; ?></b>&nbsp;
							</font>
						</a>
					</td>
				</tr>
				<tr>
					<td colspan="4">
						<a onclick="popup('hearthealthy.html')" title="What is Heart Healthy?" onMouseOver="this.style.cursor='hand'">
							<img src="<?php echo $home_page_logo; ?>" height=130 width=700 alt="home page logo"
						/></a>
					</td>
				</tr>
<?php
	}
	else {
?>
				<tr bgcolor="#6699FF">
					<td colspan="2" bgcolor="">
						<font color="white" size="3">
							<b>The Right Choice... for a Healthier You!</b>
						</font>
					</td>
					<td colspan="2" align="right">
						
					</td>
				</tr>
				<tr>
					<td colspan="4">
						<img src="<?php echo $home_page_logo; ?>" height="130" width="700" alt="home page logo" />
					</td>
				</tr>
<?php
	}

///////////////////////////MENU GROUPS
?>
				<tr bgcolor="white">
					<td colspan="4">
<?php
	//mysql_connect($dbhost,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");
	$query = "
		SELECT menu_groups.* FROM menu_groups
		WHERE menu_groups.menu_typeid = $curmenu
		AND
			menu_groups.deleted = 0
		AND
			menu_groups.menu_group_id NOT IN (SELECT menu_groupid FROM menu_groups_active WHERE businessid = $businessid)
		ORDER BY menu_groups.orderid DESC
	";
	$result = Treat_DB_ProxyOld::query( $query );
	$num = intval( Treat_DB_ProxyOld::mysql_num_rows( $result ) );
	//mysql_close();

	$num--;
	$first = $num;
	while ( $num >= 0 ) {
		$menu_group_id = Treat_DB_ProxyOld::mysql_result( $result, $num, 'menu_group_id' );
		$groupname = Treat_DB_ProxyOld::mysql_result( $result, $num, 'groupname' );

		if ( $first != $num ) {
?>
						<font face="arial" size="2">|</font>
<?php
		}
?>
						<a href="menuitems.php?groupid=<?php
								echo $menu_group_id;
							?>&setmenu=<?php
								echo $curmenu;
							?>&showme=<?php
								echo $showme;
						?>" style="<?php echo $style; ?>">
							<font face="arial" size="2" color="#999999" onMouseOver="this.style.color='#FF9900'" onMouseOut="this.style.color='#999999'">
								<?php echo $groupname; ?> 
							</font>
						</a>
<?php

		$num--;
	}

?>
					</td>
				</tr>
			</table>
		</center>
<?php
///////////////////////////ORDER/RESERVE
	if ( $reserveid > 0 ) {

		//mysql_connect($dbhost,$username,$password);
		//@mysql_select_db($database) or die( "Unable to select database");
		$query = "SELECT * FROM invoicedetail WHERE reserveid = '$reserveid' ORDER BY itemid DESC";
		$result = Treat_DB_ProxyOld::query( $query );
		$num = intval( Treat_DB_ProxyOld::mysql_num_rows( $result ) );
		//mysql_close();
		if ( $num > 0 ) {
			if ( $num > 0 && $no_nutrition == 0 ) {
				$shownutrition = "<a onclick=popup('nutrition2.php')><img src=heart.jpg height=16 width=14 alt='Nutrtional Information for this Meal' border=0 onMouseOver=this.style.cursor='hand'></a>";
			}
			else {
				$shownutrition = '';
			}

?>
		<p>
		<center>
			<table width="700" cellspacing="0" cellpadding="0" style="border:2px solid #999999;font-family: arial;">
				<tr>
					<td bgcolor="#999999" colspan="6">
						<font size="2" color="white">
							<b>Order Details:</b>
						</font>
					</td>
				</tr>
<?php
			$subtotal = 0;
			$num--;
			$showcolor = 'white';

			while ( $num >= 0 ) {
				$itemid = Treat_DB_ProxyOld::mysql_result( $result, $num, 'itemid' );
				$qty = Treat_DB_ProxyOld::mysql_result( $result, $num, 'qty' );
				$item = Treat_DB_ProxyOld::mysql_result( $result, $num, 'item' );
				$price = Treat_DB_ProxyOld::mysql_result( $result, $num, 'price' );
				$detail = Treat_DB_ProxyOld::mysql_result( $result, $num, 'detail' );

				if ( $detail == ';' ) {
					$detail = '';
				}

				if ( $qty < 1 ) {
					$newqty = 1;
				}
				else {
					$newqty = $qty;
				}

				$itemtotal = money( $newqty * $price );
				$price = money( $price );

?>
				<tr>
					<td bgcolor="<?php echo $showcolor; ?>" valign="top" width="5%">
						<font size="2"><?php echo $qty; ?></font>
					</td>
					<td bgcolor="<?php echo $showcolor; ?>" valign="top" width="25%">
						<font size="2"><?php echo $item; ?></font>
					</td>
					<td bgcolor="<?php echo $showcolor; ?>" align="right" valign="top" width="10%">
						<font size="2">$<?php echo $price; ?></font>
					</td>
					<td bgcolor="<?php echo $showcolor; ?>" align="right" valign="top" width="10%">
						<font size="2">$<?php echo $itemtotal; ?></font>
					</td>
					<td bgcolor="<?php echo $showcolor; ?>" width="4%">
					</td>
					<td bgcolor="<?php echo $showcolor; ?>">
						<font size="2"><?php echo $detail; ?></font>
						<font size="1">
							[<a href="delitem.php?itemid=<?php echo $itemid; ?>&goto=3&bid=<?php echo $businessid; ?>" style="<?php echo $style; ?>">
								<font color="blue">REMOVE</font>
							</a>]
						</font>
					</td>
				</tr>
<?php

				if ( $showcolor == 'white' ) {
					$showcolor = '#E8E7E7';
				}
				else {
					$showcolor = 'white';
				}
				$subtotal = $subtotal + ( $qty * $price );
				$num--;
			}
			$subtotal = money( $subtotal );

			if ( $curmenu == $cafe_menu || $curmenu == $vend_menu ) {
				$showsaveorder = "<a href=\"javascript:void();\" onclick=\"showhide();\" style=$style><font size=2 COLOR=#0000FF onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='#0000FF'>Save Order</font></a>";
			}

			$mydate = date( 'Y-m-d' );

?>
				<tr valign="top">
					<td bgcolor="<?php echo $showcolor; ?>" colspan="2"><?php echo $shownutrition; ?></td>
					<td bgcolor="<?php echo $showcolor; ?>" align="right">
						<font size="2">Subtotal:</font>
					</td>
					<td align="right" bgcolor="<?php echo $showcolor; ?>">
						<font size="2">$<?php echo $subtotal; ?></font>
					</td>
					<td bgcolor="<?php echo $showcolor; ?>" colspan="2" align="right">
						<form name="save" action="savemenu.php" method="post" style="margin:0;padding:0;display:inline;">
							<div id="showsave" name="showsave"><?php echo $showsaveorder; ?></div>
							<div name="submitsave" id="submitsave" style="display:none;">
								<script language='JavaScript' id='js18' type="text/javascript">
									var cal18 = new CalendarPopup('testdiv1');
									cal18.setCssPrefix('TEST');
								</script>
								<input type="text" name="mydate" value="<?php echo $mydate; ?>" size="8" style="font-size: 9px;" />
								<a href="javascript:void();"
									onClick="cal18.select(document.forms[0].mydate,'anchor18','yyyy-MM-dd'); return false;"
									title="cal18.select(document.forms[0].mydate,'anchor1x','yyyy-MM-dd'); return false;"
									name="anchor18" id="anchor18"
								><img src="calendar.gif" border="0" height="16" width="16" alt="Choose a Date" valign="center"></a>
								<input type="submit" value="Save" style="font-size: 9px;" />
							</div>
						</form>
					</td>
				</tr>
			</table>
		</center>
		<p>
<?php
		}
		else {
?>
		<hr width="690" />
<?php
		}
	}
	else {
?>
		<hr width="690" />
<?php
	}

///////////////////////////////

	/////name of vending menu spot
	$query65 = "SELECT menu_typename FROM menu_type WHERE menu_typeid = $vend_menu";
	$result65 = Treat_DB_ProxyOld::query( $query65 );

	$vend_menu_name = Treat_DB_ProxyOld::mysql_result( $result65, 0, 'menu_typename' );

	# TODO Begin Weekly Section
	if ( $week < 1 ) {
		while ( dayofweek( $today ) != 'Monday' ) {
			$today = prevday( $today );
		}
		$yesterday = prevday( $today );
		$tomorrow = $today;
		for ( $mycount = 1; $mycount <= 8; $mycount++ ) {
			$tomorrow = nextday( $tomorrow );
		}

?>
		<center>
			<table width="700">
				<tr>
					<td>
						<font face="arial" size="2">
							<a href="/menu/menu.php?curmenu=<?php
		if ( $parent > 0 ) {
								echo $curmenu;
		}
		else {
								echo $cafe_menu;
		}
							?>&bid=<?php
								echo $businessid;
							?>" style="<?php
								echo $style;
							?>" title="View the Rest of Our Menu">
								<font color="blue"><?php
									echo $menu_name;
								?></font>
							</a>
<?php
		if ( $catering == 1 ) {
?>
							:: <a href="/menu/menu.php?curmenu=<?php echo $default_menu; ?>&bid=<?php echo $businessid; ?>" style="<?php echo $style; ?>">
								<font color="blue">Catering Menu</font>
							</a>
<?php
		}
		if ( $vend_menu > 0 ) {
?>
							:: <a href="/menu/menu.php?curmenu=<?php echo $vend_menu; ?>&bid=<?php echo $businessid; ?>" style="<?php echo $style; ?>">
								<font color="blue"><?php echo $vend_menu_name; ?></font>
							</a>
<?php
		}
		if ( $no_nutrition == 0 ) {
?>
							:: <a href="/menu/history.php?curmenu=<?php echo $cafe_menu; ?>&bid=<?php echo $businessid; ?>" style="<?php echo $style; ?>">
								<font color="blue">Nutritional Analysis</font>
							</a>
<?php
		}
		if ( $multi_cafe_menu == 1 ) {
?>
							:: <a href="/menu/menuselect.php?bid=<?php echo $businessid; ?>" style="<?php echo $style; ?>">
								<font color="blue">Change Menu</font>
							</a>
<?php
		}
?>
							:: <a href="/menu/contact.php?bid=<?php echo $businessid; ?>" style="<?php echo $style; ?>">
								<font color="blue">Contact Us</font>
							</a>
							:: <a href="/menu/events.php?bid=<?php echo $businessid; ?>" style="<?php echo $style; ?>">
								<font color="blue">Events</font>
							</a>
<?php
		if ( $kiosk === false ) {
?>
							:: <a href="/menu/printintro.php?bid=<?php echo $businessid; ?>&date=<?php echo $date; ?>" style="<?php echo $style; ?>" target="_blank">
								<font color="blue">Print</font>
							</a>
<?php
		}
?>
						</font>
					</td>
					<td align="right">
						<font face="arial" size="2">
							<a href="/menu/intro.php?<?php
								echo http_build_query( $timeFrameViewQueryLink, null, '&amp;' );
							?>" style="<?php
								echo $style;
							?>">
								<font color="#999999">
								<?php echo $timeframeViewLinkText; ?> 
								</font>
							</a>
						</font>
					</td>
				</tr>
			</table>
		</center>

		<center>
			<table width="732" cellspacing="0" cellpadding="0">
				<tr height="2">
					<td width="16"></td>
					<td colspan="11" bgcolor="#6699FF"></td>
					<td width="16"></td>
				</tr>
							<tr>
								<td width="16"></td>
								<td bgcolor="#6699FF" colspan="2">
									<a href="/menu/intro.php?<?php
										echo http_build_query( array(
											'bid' => $pageVariables->businessId,
											'date' => date( 'Y-m-d', $timeFramePrevious ),
											'showme' => $pageVariables->showMe,
											'week' => $pageVariables->week,
										), null, '&amp;' );
									?>" style="<?php
										echo $style;
									?>" title="Previous <?php echo $timeFrame; ?>s Menu">
										<font color="white" size="2">Previous <?php echo $timeFrame; ?></font>
									</a>
								</td>
								<td bgcolor="#6699FF" colspan="7">
									<center>
										<font color="white" size="4">
											<b><?php echo $menu_name; ?> - <?php echo $timeFramely; ?> Specials</b>
										</font>
									</center>
								</td>
								<td bgcolor="#6699FF" colspan="2" align="right">
									<a href="/menu/intro.php?<?php
										echo http_build_query( array(
											'bid' => $pageVariables->businessId,
											'date' => date( 'Y-m-d', $timeFrameNext ),
											'showme' => $pageVariables->showMe,
											'week' => $pageVariables->week,
										), null, '&amp;' );
									?>" style="<?php
										echo $style;
									?>" title="Next <?php echo $timeFrame; ?>s Menu">
										<font color="white" size="2">Next <?php echo $timeFrame; ?></font>
									</a>
								</td>
								<td width="16"></td>
							</tr>
							<tr height="2">
								<td width="16"></td>
								<td colspan="11" bgcolor="#669933"></td>
								<td width="16"></td>
							</tr>
				<tr valign="top">
					<td width="16"></td>
					<td width="2" bgcolor="#669933"><img src="green.jpg" height="1" width="2" alt="green" /></td>
<?php
		for ( $counter = 1; $counter <= 5; $counter++ ) {

			$showyear = substr( $today, 2, 2 );
			$showmonth = substr( $today, 5, 2 ) * 1;
			$showday2 = substr( $today, 8, 2 );
			$todayname = dayofweek( $today );

?>
					<td width="137" bgcolor="white" align="center">
						<center>
							<font size="2" color="#669933">
								<b>
									<?php echo $todayname; ?> 
									<br/>
									<?php echo $showmonth, '/', $showday2, '/', $showyear; ?> 
								</b>
							</font>
							<hr width="90%">
						</center>
<?php

			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query4 = "SELECT * FROM caternote WHERE businessid = '$businessid' AND date <= '$today' AND enddate >= '$today' AND recipe_station = '0'";
			$result4 = Treat_DB_ProxyOld::query( $query4 );
			$num4 = intval( Treat_DB_ProxyOld::mysql_num_rows( $result4 ) );
			//mysql_close();

			$num4--;
			while ( $num4 >= 0 ) {
				$daynote = Treat_DB_ProxyOld::mysql_result( $result4, $num4, 'note' );
				$daynote = str_replace( "\n", "<br>\n", $daynote );

?>
						<center>
						<table width="95%" bgcolor="#336699" style="border: 1px solid black;">
							<tr>
								<td align="center">
									<font size="1" face="arial" color="white">
										<i><?php
										echo $daynote;
										?></i>
									</font>
								</td>
							</tr>
						</table>
						</center>
						<br/>
<?php
				$num4--;
			}

			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query4 = "SELECT * FROM servery_station ORDER BY orderid DESC";
			$result4 = Treat_DB_ProxyOld::query( $query4 );
			$num4 = intval( Treat_DB_ProxyOld::mysql_num_rows( $result4 ) );
			//mysql_close();

			$num4--;
			while ( $num4 >= 0 ) {
				$recipe_station = Treat_DB_ProxyOld::mysql_result( $result4, $num4, 'stationid' );
				$station_name = Treat_DB_ProxyOld::mysql_result( $result4, $num4, 'station_name' );

				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( "Unable to select database");
				if ( $parent > 0 ) {
					$query = "SELECT * FROM order_item,menu_items WHERE order_item.daypartid = '$daypartid' AND order_item.businessid = '$businessid' AND order_item.date = '$today' AND order_item.menu_itemid = menu_items.menu_item_id AND menu_items.menu_typeid = '$curmenu' AND menu_items.recipe_station = '$recipe_station' ORDER BY order_item.orderid DESC";
				}
				else {
					$query = "SELECT * FROM order_item,menu_items WHERE order_item.businessid = '$businessid' AND order_item.date = '$today' AND order_item.menu_itemid = menu_items.menu_item_id AND menu_items.menu_typeid = '$curmenu' AND menu_items.recipe_station = '$recipe_station' ORDER BY order_item.orderid DESC";
				}
				$result = Treat_DB_ProxyOld::query( $query );
				$num = intval( Treat_DB_ProxyOld::mysql_num_rows( $result ) );
				//mysql_close();

				if ( $num > 0 ) {

?>
										<u>
											<font size="2" color="#669933">
												<?php echo $station_name; ?> 
											</font>
										</u>
						<br />
<?php

					$query44 = "SELECT * FROM caternote WHERE businessid = '$businessid' AND date <= '$today' AND enddate >= '$today' AND recipe_station = '$recipe_station'";
					$result44 = Treat_DB_ProxyOld::query( $query44 );
					$num44 = intval( Treat_DB_ProxyOld::mysql_num_rows( $result44 ) );

					$num44--;
					while ( $num44 >= 0 ) {
						$daynote = Treat_DB_ProxyOld::mysql_result( $result44, $num44, 'note' );
						$daynote = str_replace( "\n", "<br>\n", $daynote );

?>
						<center>
							<font size="1" face="arial" color="#669933">
										<i><?php
										echo $daynote;
										?></i>
							</font>
						</center>
<?php
						$num44--;
					}

					$num--;
					while ( $num >= 0 ) {
						$menu_itemid = Treat_DB_ProxyOld::mysql_result( $result, $num, 'menu_itemid' );

						//mysql_connect($dbhost,$username,$password);
						//@mysql_select_db($database) or die( "Unable to select database");
						$query2 = "SELECT * FROM menu_items WHERE menu_item_id = '$menu_itemid'";
						$result2 = Treat_DB_ProxyOld::query( $query2 );
						//mysql_close();

						$item_name = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'item_name' );
						$price = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'price' );
						$description = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'description' );
						$unit = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'unit' );
						$groupid = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'groupid' );
						$heart_healthy = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'heart_healthy' );
						$serving_size = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'serving_size' );
						
						$menuItem = new \ArrayObject();
						$menuItem->menu_itemid = $menu_itemid;
						$menuItem->heart_healthy = $heart_healthy;
						$menuItem->healthy_living = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'healthy_living' );

						$price3 = money( $price );

						//////////////////////////////////////////////
						///////////////NUTRITION SPECIFICS////////////
						//////////////////////////////////////////////
						$nutrition = array();
						$showhealthy = '';
						if ( $showme > 0 ) {
							//mysql_connect($dbhost,$username,$password);
							//@mysql_select_db($database) or die( "Unable to select database");
							$query32 = "SELECT * FROM recipe WHERE menu_itemid = '$menu_itemid'";
							$result32 = Treat_DB_ProxyOld::query( $query32 );
							$num32 = intval( Treat_DB_ProxyOld::mysql_num_rows( $result32 ) );
							//mysql_close();

							$num32--;
							while ( $num32 >= 0 ) {
								$recipeid = Treat_DB_ProxyOld::mysql_result( $result32, $num32, 'recipeid' );
								$inv_itemid = Treat_DB_ProxyOld::mysql_result( $result32, $num32, 'inv_itemid' );
								$rec_num = Treat_DB_ProxyOld::mysql_result( $result32, $num32, 'rec_num' );
								$rec_size = Treat_DB_ProxyOld::mysql_result( $result32, $num32, 'rec_size' );
								$srv_num = Treat_DB_ProxyOld::mysql_result( $result32, $num32, 'srv_num' );
								$rec_order = Treat_DB_ProxyOld::mysql_result( $result32, $num32, 'rec_order' );

								//mysql_connect($dbhost,$username,$password);
								//@mysql_select_db($database) or die( "Unable to select database");
								$query23 = "SELECT * FROM inv_items WHERE inv_itemid = '$inv_itemid'";
								$result23 = Treat_DB_ProxyOld::query( $query23 );
								//mysql_close();

								$supc = Treat_DB_ProxyOld::mysql_result( $result23, 0, 'item_code' );
								$inv_itemname = Treat_DB_ProxyOld::mysql_result( $result23, 0, 'item_name' );
								$order_size = Treat_DB_ProxyOld::mysql_result( $result23, 0, 'order_size' );
								$price = Treat_DB_ProxyOld::mysql_result( $result23, 0, 'price' );
								if ( $rec_order == 1 ) {
									$inv_rec_num = Treat_DB_ProxyOld::mysql_result( $result23, 0, 'rec_num' );
								}
								elseif ( $rec_order == 2 ) {
									$inv_rec_num = Treat_DB_ProxyOld::mysql_result( $result23, 0, 'rec_num2' );
								}

								///////////nutrition
								$numserv = $serving_size;

								//mysql_connect($dbhost,$username,$password);
								//@mysql_select_db($database) or die( "Unable to select database");
								$query24 = "SELECT * FROM nutrition WHERE item_code = '$supc'";
								$result24 = Treat_DB_ProxyOld::query( $query24 );
								$num24 = intval( Treat_DB_ProxyOld::mysql_num_rows( $result24 ) );
								//mysql_close();

								$cholesterol = Treat_DB_ProxyOld::mysql_result( $result24, 0, 'cholesterol' );
								$calories = Treat_DB_ProxyOld::mysql_result( $result24, 0, 'calories' );
								$cal_from_fat = Treat_DB_ProxyOld::mysql_result( $result24, 0, 'cal_from_fat' );
								$fiber = Treat_DB_ProxyOld::mysql_result( $result24, 0, 'fiber' );
								$protein = Treat_DB_ProxyOld::mysql_result( $result24, 0, 'protein' );
								$carbs = Treat_DB_ProxyOld::mysql_result( $result24, 0, 'complex_cabs' );
								$sodium = Treat_DB_ProxyOld::mysql_result( $result24, 0, 'sodium' );
								$unsat_fat = Treat_DB_ProxyOld::mysql_result( $result24, 0, 'unsat_fat' );
								$sat_fat = Treat_DB_ProxyOld::mysql_result( $result24, 0, 'sat_fat' );
								$total_fat = Treat_DB_ProxyOld::mysql_result( $result24, 0, 'total_fat' );
								$potassium = Treat_DB_ProxyOld::mysql_result( $result24, 0, 'potassium' );

								$nutr_serv_size = Treat_DB_ProxyOld::mysql_result( $result24, 0, 'serv_size_hh' );
								$nutr_rec_size = Treat_DB_ProxyOld::mysql_result( $result24, 0, 'rec_size' );
								$nutr_rec_size2 = Treat_DB_ProxyOld::mysql_result( $result24, 0, 'rec_size2' );

								if ( $nutr_rec_size == $rec_size ) {
									$nutrition['cholesterol']=round($nutrition['cholesterol']+(($cholesterol/$nutr_serv_size)*($rec_num/$numserv)),1);
									$nutrition['calories']=round($nutrition['calories']+(($calories/$nutr_serv_size)*($rec_num/$numserv)),1);
									$nutrition['cal_from_fat']=round($nutrition['cal_from_fat']+(($cal_from_fat/$nutr_serv_size)*($rec_num/$numserv)),1);
									$nutrition['fiber']=round($nutrition['fiber']+(($fiber/$nutr_serv_size)*($rec_num/$numserv)),1);
									$nutrition['protein']=round($nutrition['protein']+(($protein/$nutr_serv_size)*($rec_num/$numserv)),1);
									$nutrition['carbs']=round($nutrition['carbs']+(($carbs/$nutr_serv_size)*($rec_num/$numserv)),1);
									$nutrition['sodium']=round($nutrition['sodium']+(($sodium/$nutr_serv_size)*($rec_num/$numserv)),1);
									$nutrition['unsat_fat']=round($nutrition['unsat_fat']+(($unsat_fat/$nutr_serv_size)*($rec_num/$numserv)),1);
									$nutrition['sat_fat']=round($nutrition['sat_fat']+(($sat_fat/$nutr_serv_size)*($rec_num/$numserv)),1);
									$nutrition['total_fat']=round($nutrition['total_fat']+(($total_fat/$nutr_serv_size)*($rec_num/$numserv)),1);
									$nutrition['potassium']=round($nutrition['potassium']+(($potassium/$nutr_serv_size)*($rec_num/$numserv)),1);

								}
								else {
									//mysql_connect($dbhost,$username,$password);
									//@mysql_select_db($database) or die( "Unable to select database");
									$query25 = "SELECT * FROM conversion WHERE sizeid = '$rec_size' AND sizeidto = '$nutr_rec_size'";
									$result25 = Treat_DB_ProxyOld::query( $query25 );
									$num25 = intval( Treat_DB_ProxyOld::mysql_num_rows( $result25 ) );
									//mysql_close();

									if ( $num25 > 0 ) {
										$nutr_convert = Treat_DB_ProxyOld::mysql_result( $result25, 0, 'convert' );

										$nutrition['cholesterol']=round($nutrition['cholesterol']+(($cholesterol/($nutr_serv_size*$nutr_convert))*($rec_num/$numserv)),1);
										$nutrition['calories']=round($nutrition['calories']+(($calories/($nutr_serv_size*$nutr_convert))*($rec_num/$numserv)),1);
										$nutrition['cal_from_fat']=round($nutrition['cal_from_fat']+(($cal_from_fat/($nutr_serv_size*$nutr_convert))*($rec_num/$numserv)),1);
										$nutrition['fiber']=round($nutrition['fiber']+(($fiber/($nutr_serv_size*$nutr_convert))*($rec_num/$numserv)),1);
										$nutrition['protein']=round($nutrition['protein']+(($protein/($nutr_serv_size*$nutr_convert))*($rec_num/$numserv)),1);
										$nutrition['carbs']=round($nutrition['carbs']+(($carbs/($nutr_serv_size*$nutr_convert))*($rec_num/$numserv)),1);
										$nutrition['sodium']=round($nutrition['sodium']+(($sodium/($nutr_serv_size*$nutr_convert))*($rec_num/$numserv)),1);
										$nutrition['unsat_fat']=round($nutrition['unsat_fat']+(($unsat_fat/($nutr_serv_size*$nutr_convert))*($rec_num/$numserv)),1);
										$nutrition['sat_fat']=round($nutrition['sat_fat']+(($sat_fat/($nutr_serv_size*$nutr_convert))*($rec_num/$numserv)),1);
										$nutrition['total_fat']=round($nutrition['total_fat']+(($total_fat/($nutr_serv_size*$nutr_convert))*($rec_num/$numserv)),1);
										$nutrition['potassium']=round($nutrition['potassium']+(($potassium/($nutr_serv_size*$nutr_convert))*($rec_num/$numserv)),1);

									}
									elseif ( $num24 > 0 ) {
										$nutrition['cholesterol']=round($nutrition['cholesterol']+$cholesterol,1);
										$nutrition['calories']=round($nutrition['calories']+$calories,1);
										$nutrition['cal_from_fat']=round($nutrition['cal_from_fat']+$cal_from_fat,1);
										$nutrition['fiber']=round($nutrition['fiber']+$fiber,1);
										$nutrition['protein']=round($nutrition['protein']+$protein,1);
										$nutrition['carbs']=round($nutrition['carbs']+$carbs,1);
										$nutrition['sodium']=round($nutrition['sodium']+$sodium,1);
										$nutrition['unsat_fat']=round($nutrition['unsat_fat']+$unsat_fat,1);
										$nutrition['sat_fat']=round($nutrition['sat_fat']+$sat_fat,1);
										$nutrition['total_fat']=round($nutrition['total_fat']+$total_fat,1);
										$nutrition['potassium']=round($nutrition['potassium']+$potassium,1);

									}
								}

								$num32--;
							}

							///////CALORIES
							if ( $showme == 1 ) {
								$is_healthy = 0;
								if ( $nutrition['calories'] <= 500 ) {
									$is_healthy++;
								}
								if ( ( $nutrition['cal_from_fat'] / $nutrition['calories'] ) <= 0.3 ) {
									$is_healthy++;
								}

								if ( $is_healthy == 2 ) {
									$showhealthy = " <img src=img_calorie2.jpg align=middle height=12 width=12 alt='Low Calories'>";
								}
							}
							///////FAT
							elseif ( $showme == 2 ) {
								$is_healthy = 0;
								if ( ( $nutrition['cal_from_fat'] / $nutrition['calories'] ) <= 0.3 ) {
									$is_healthy++;
								}

								if ( $is_healthy == 1 ) {
									$showhealthy = " <img src=img_fat2.jpg align=middle height=12 width=12 alt='Low Fat'>";
								}
							}
							///////CHOLESTEROL
							elseif ( $showme == 3 ) {
								$is_healthy = 0;
								if ( $nutrition['cholesterol'] <= 60 ) {
									$is_healthy++;
								}

								if ( $is_healthy == 1 ) {
									$showhealthy = " <img src=img_cholesterol2.jpg align=middle height=12 width=12 alt='Low Cholesterol'>";
								}
							}
							///////SODIUM
							elseif ( $showme == 4 ) {
								$is_healthy = 0;
								if ( $nutrition['sodium'] <= 575 ) {
									$is_healthy++;
								}

								if ( $is_healthy == 1 ) {
									$showhealthy = " <img src=img_sodium2.jpg align=middle height=12 width=12 alt='Low Sodium'>";
								}
							}
							///////CARBS
							elseif ( $showme == 5 ) {
								$is_healthy = 0;
								if ( $nutrition['carbs'] < 20 ) {
									$is_healthy++;
								}

								if ( $is_healthy == 1 ) {
									$showhealthy = " <img src=img_carbo2.jpg align=middle height=12 width=12 alt='Low Carbohydrates'>";
								}
							}
							///////Weight Watchers
							elseif ( $showme == 6 ) {
								$showhealthy = round(
									( $nutrition['protein'] / 10.9 )
										+ ( $nutrition['carbs'] / 9.2 )
										+ ( $nutrition['total_fat'] / 3.9 )
										+ ( $nutrition['fiber'] / 35 )
									,0
								);
								$showhealthy = " <div style=\"background:url('tp2.jpg') no-repeat; margin-top:2px; text-align:center; color:white; font-size:9px; font-weight:bold; height:12px; width:12px; font-family:arial; line-height:12px; display:inline;\">$showhealthy</div>";
							}
						}

						////////////////////////////////////
						//////////////END NUTRITION SPECS///
						////////////////////////////////////


						if ( $today == $today2 ) {
							$showadd = " <a href=additem.php?bid=$businessid&item=$menu_itemid&goto=1><img src=add2.jpg align=middle height=12 width=13 border=0 alt='Add Item'></a>";
						}
						else {
							$showadd = '';
						}

					/////ALT PRICING
						//mysql_connect($dbhost,$username,$password);
						//@mysql_select_db($database) or die( "Unable to select database");
						$query3 = "SELECT * FROM menu_alt_price WHERE menu_itemid = '$menu_itemid' ORDER BY priceid DESC";
						$result3 = Treat_DB_ProxyOld::query( $query3 );
						$num3 = intval( Treat_DB_ProxyOld::mysql_num_rows( $result3 ) );
						//mysql_close();

						$alt_pricing = '';
						$num3--;
						while ( $num3 >= 0 ) {
							$price2 = Treat_DB_ProxyOld::mysql_result( $result3, $num3, 'price' );
							$comment = Treat_DB_ProxyOld::mysql_result( $result3, $num3, 'comment' );

							$price2 = money( $price2 );
							$alt_pricing = "$alt_pricing<br>$price2/$comment";

							$num3--;
						}
						//////////

						if ( $price3 > 0 ) {
							$showprice = "$price3/$unit";
						}
						else {
							$showprice = $unit;
						}

						if ( !$pageBusiness->no_nutrition ) {
?>
						<div>
									<?php echo $showhealthy; ?>&nbsp;
							<font size="1">
									<b>
										<a
											onclick="popup('nutrition.php?mid=<?php echo $menu_itemid; ?>')"
											title="Click for Nutritional Information"
										>
											<font size="1" face="arial" color="#000000"
												onMouseOver="this.style.color='#FF9900';this.style.cursor='hand'"
												onMouseOut="this.style.color='#000000'"
											>
												<?php echo $item_name; ?>
											</font>
										</a>
									</b>
									<br />
									<font size="1" face="arial">
										<font color="gray">
											<?php echo $showprice; ?> 
										</font>
									</font>
								<?php echo $showadd; ?> 
<?php
							if ( $menuItem->heart_healthy ) {
?>
											<a
												onclick="popup('nutrition.php?mid=<?php echo $menuItem->menu_itemid; ?>')"
												title="Click for Nutritional Information"
											>
												<img src="/assets/images/nutrition/hh2.jpg" align="middle" height="13" width="12" alt="Heart Healthy" />
											</a>
<?php
							}
							if ( $menuItem->healthy_living ) {
?>
											<a
												onclick="popup('nutrition.php?mid=<?php echo $menuItem->menu_itemid; ?>')"
												title="Click for Nutritional Information"
											>
												<img src="/assets/images/nutrition/healthy-living-icon-small.png" align="middle" height="12" width="12" alt="Healthy Living Program" />
											</a>
<?php
							}
?>
										<font color="gray" face="arial" size="1">
											<?php echo $alt_pricing; ?> 
										</font>
										<br />
									</font>
								</div>
<?php
						}
						else {
?>
							<div>
								<font size="1">
									<b>
										<font face="arial" color="#000000">
											<?php echo $item_name; ?> 
										</font>
									</b>
									<br />
									<font size="1" face="arial">
										<font color="gray">
											<?php echo $showprice; ?> 
										</font>
									</font>
									<font color="gray" face="arial">
										<?php echo $showadd; ?> 
<?php
							if ( $menuItem->heart_healthy ) {
?>
											<img src="/assets/images/nutrition/hh2.jpg" align="middle" height="13" width="12" alt="Heart Healthy" />
<?php
							}
							if ( $menuItem->healthy_living ) {
?>
											<img src="/assets/images/nutrition/healthy-living-icon-small.png" align="middle" height="12" width="12" alt="Healthy Living Program" />
<?php
							}
?>
											<?php echo $showhealthy; ?> 
									</font>
										<font color="gray" face="arial" size="1">
											<?php echo $alt_pricing; ?> 
										</font>
									</font>
								</div>
						<br />
<?php
						}
						$num--;
					}
?>
							<p>
<?php
				}
				$num4--;
			}
?>
					</td>
					<td width="2" bgcolor="#669933"></td>
<?php
			$today = nextday( $today );
		}
		if ( $no_nutrition == 0 ) {
?>
					<td width="16">
						<a href="/menu/intro.php?<?php
							$introQuery['showme'] = 1;
							echo http_build_query( $introQuery, null, '&amp;' );
						?>"
							><img src="/assets/images/nutrition/img_calorie.jpg" height="16" width="16" border="0" alt="Low Calorie Items"
						/></a>
						<br />
						<img src="/assets/images/spacer.jpg" height="2" width="16" alt="spacer" />
						<br />
						<a href="/menu/intro.php?<?php
							$introQuery['showme'] = 2;
							echo http_build_query( $introQuery, null, '&amp;' );
						?>"
							><img src="/assets/images/nutrition/img_fat.jpg" height="16" width="16" border="0" alt="Low Fat Items"
						/></a>
						<br />
						<img src="/assets/images/spacer.jpg" height="2" width="16" alt="spacer" />
						<br />
						<a href="/menu/intro.php?<?php
							$introQuery['showme'] = 3;
							echo http_build_query( $introQuery, null, '&amp;' );
						?>"
							><img src="/assets/images/nutrition/img_cholesterol.jpg" height="16" width="16" border="0" alt="Low Cholesterol"
						/></a>
						<br />
						<img src="/assets/images/spacer.jpg" height="2" width="16" alt="spacer" />
						<br />
						<a href="/menu/intro.php?<?php
							$introQuery['showme'] = 4;
							echo http_build_query( $introQuery, null, '&amp;' );
						?>"
							><img src="/assets/images/nutrition/img_sodium.jpg" height="16" width="16" border="0" alt="Low Sodium Items"
						/></a>
						<br />
						<img src="/assets/images/spacer.jpg" height="2" width="16" alt="spacer" />
						<br />
						<a href="/menu/intro.php?<?php
							$introQuery['showme'] = 5;
							echo http_build_query( $introQuery, null, '&amp;' );
						?>"
							><img src="/assets/images/nutrition/img_carbo.jpg" height="16" width="16" border="0" alt="Low Carbohydrates"
						/></a>
						<br />
						<img src="/assets/images/spacer.jpg" height="2" width="16" alt="spacer" />
						<br />
						<a href="/menu/intro.php?<?php
							$introQuery['showme'] = 6;
							echo http_build_query( $introQuery, null, '&amp;' );
						?>"
							><img src="/assets/images/nutrition/ww.jpg" height="16" width="16" border="0" alt="Treat Score"
						/></a>
						<br />
						<img src="/assets/images/spacer.jpg" height="2" width="16" alt="spacer" />
						<br />
						<a href="/menu/intro.php?<?php
							$introQuery['showme'] = null;
							echo http_build_query( $introQuery, null, '&amp;' );
						?>"
							><img src="/assets/images/nutrition/healthy-living-icon-small.png" height="16" width="16" border="0" alt="Healthy Living"
						/></a>
						<br />
						<img src="/assets/images/spacer.jpg" height="2" width="16" alt="spacer" />
						<br />
						<a href="/menu/intro.php?<?php
							$introQuery['showme'] = null;
							echo http_build_query( $introQuery, null, '&amp;' );
						?>"
							><img src="/assets/images/nutrition/hh1.jpg" height="16" width="16" border="0" alt="Heart Healthy"
						/></a>
					</td>
<?php
		}
		else {
?>
					<td width="16"></td>
<?php
		}

?>
				</tr>
				<tr height="2">
					<td width="16"></td>
					<td colspan="11" bgcolor="#669933"></td>
					<td width="16"></td>
				</tr>
			</table>
		</center>
<?php
	}
	# TODO Begin Daily Section
	else {
		$showyear = substr( $today, 2, 2 );
		$showmonth = substr( $today, 5, 2 ) * 1;
		$showday2 = substr( $today, 8, 2 );

?>
		<center>
			<table width="700">
				<tr>
					<td>
						<font face="arial" size="2">
							<a href="/menu/menu.php?curmenu=<?php
		if ( $parent > 0 ) {
								echo $curmenu;
		}
		else {
								echo $cafe_menu;
		}
							?>&bid=<?php
								echo $businessid;
							?>" style="<?php
								echo $style;
							?>" title="View the Rest of Our Menu">
								<font color="blue"><?php
									echo $menu_name;
								?></font>
							</a>
<?php
		if ( $catering == 1 ) {
?>
							:: <a href="/menu/menu.php?curmenu=<?php echo $default_menu; ?>&bid=<?php echo $businessid; ?>" style="<?php echo $style; ?>">
								<font color="blue">Catering Menu</font>
							</a>
<?php
		}
		if ( $vend_menu > 0 ) {
?>
							:: <a href="/menu/menu.php?curmenu=<?php echo $vend_menu; ?>&bid=<?php echo $businessid; ?>" style="<?php echo $style; ?>">
								<font color="blue"><?php echo $vend_menu_name; ?></font>
							</a>
<?php
		}
		if ( $no_nutrition == 0 ) {
?>
							:: <a href="/menu/history.php?curmenu=<?php echo $cafe_menu; ?>&bid=<?php echo $businessid; ?>" style="<?php echo $style; ?>">
								<font color="blue">Nutritional Analysis</font>
							</a>
<?php
		}
		if ( $multi_cafe_menu == 1 ) {
?>
							:: <a href="/menu/menuselect.php?bid=<?php echo $businessid; ?>" style="<?php echo $style; ?>">
								<font color="blue">Change Menu</font>
							</a>
<?php
		}
?>
							:: <a href="/menu/contact.php?bid=<?php echo $businessid; ?>" style="<?php echo $style; ?>">
								<font color="blue">Contact Us</font>
							</a>
							:: <a href="/menu/events.php?bid=<?php echo $businessid; ?>" style="<?php echo $style; ?>">
								<font color="blue">Events</font>
							</a>
<?php
		if ( $kiosk === false ) {
?>
							:: <a href="/menu/printintro.php?bid=<?php echo $businessid; ?>&date=<?php echo $date; ?>" style="<?php echo $style; ?>" target="_blank">
								<font color="blue">Print</font>
							</a>
<?php
		}
?>
						</font>
					</td>
					<td align="right">
						<font face="arial" size="2">
							<a href="/menu/intro.php?<?php
								echo http_build_query( $timeFrameViewQueryLink, null, '&amp;' );
							?>" style="<?php
								echo $style;
							?>">
								<font color="#999999">
								<?php echo $timeframeViewLinkText; ?> 
								</font>
							</a>
						</font>
					</td>
				</tr>
			</table>
		</center>

		<center>
			<table width="732" cellspacing="0" cellpadding="0">
				<tr valign="top">
					<td width="16"></td>
					<td width="700">
						<table width="700" cellspacing="0" cellpadding="0">
							<tr height="2">
								<td colspan="5" bgcolor="#6699FF"></td>
							</tr>
							<tr>
								<td width="2" bgcolor="#6699FF"
									><img src="green.jpg" height="1" width="2" alt="green"
								/></td>
								<td bgcolor="#6699FF">
									<a href="/menu/intro.php?<?php
										echo http_build_query( array(
											'bid' => $pageVariables->businessId,
											'date' => date( 'Y-m-d', $timeFramePrevious ),
											'showme' => $pageVariables->showMe,
											'week' => $pageVariables->week,
										), null, '&amp;' );
									?>" style="<?php
										echo $style;
									?>" title="Previous <?php echo $timeFrame; ?>s Menu">
										<font color="white" size="2">Previous <?php echo $timeFrame; ?></font>
									</a>
								</td>
								<td bgcolor="#6699FF">
									<center>
										<font color="white" size="4">
											<b><?php echo $menu_name; ?> - <?php echo $timeFramely; ?> Specials</b>
										</font>
										<font color="white">(<?php echo $showmonth, '/', $showday2, '/', $showyear; ?>)</font>
									</center>
								</td>
								<td bgcolor="#6699FF" align="right">
									<a href="/menu/intro.php?<?php
										echo http_build_query( array(
											'bid' => $pageVariables->businessId,
											'date' => date( 'Y-m-d', $timeFrameNext ),
											'showme' => $pageVariables->showMe,
											'week' => $pageVariables->week,
										), null, '&amp;' );
									?>" style="<?php
										echo $style;
									?>" title="Next <?php echo $timeFrame; ?>s Menu">
										<font color="white" size="2">Next <?php echo $timeFrame; ?></font>
									</a>
								</td>
								<td width="2" bgcolor="#6699FF"></td>
							</tr>
							<tr height="2">
								<td colspan="5" bgcolor="#669933"></td>
							</tr>
<?php

			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query4 = "SELECT * FROM caternote WHERE businessid = '$businessid' AND date <= '$today' AND enddate >= '$today' AND recipe_station = '0'";
			$result4 = Treat_DB_ProxyOld::query( $query4 );
			$num4 = intval( Treat_DB_ProxyOld::mysql_num_rows( $result4 ) );
			//mysql_close();

			$num4--;
			while ( $num4 >= 0 ) {
				$daynote = Treat_DB_ProxyOld::mysql_result( $result4, $num4, 'note' );
				$daynote = str_replace( "\n", "<br>\n", $daynote );

?>
							<tr>
								<td width="2" bgcolor="#669933"></td>
								<td colspan="3" bgcolor="#336699" style="border: 1px solid black;">
									<font size="2" face="arial" color="white">
										<i><?php
										echo $daynote;
										?></i>
									</font>
								</td>
								<td width="2" bgcolor="#669933"></td>
							</tr>
<?php
				$num4--;
			}

			//mysql_connect($dbhost,$username,$password);
			//@mysql_select_db($database) or die( "Unable to select database");
			$query4 = "SELECT * FROM servery_station ORDER BY orderid DESC";
			$result4 = Treat_DB_ProxyOld::query( $query4 );
			$num4 = intval( Treat_DB_ProxyOld::mysql_num_rows( $result4 ) );
			//mysql_close();

			$num4--;
			while ( $num4 >= 0 ) {
				$recipe_station = Treat_DB_ProxyOld::mysql_result( $result4, $num4, 'stationid' );
				$station_name = Treat_DB_ProxyOld::mysql_result( $result4, $num4, 'station_name' );

				//mysql_connect($dbhost,$username,$password);
				//@mysql_select_db($database) or die( "Unable to select database");
				if ( $parent > 0 ) {
					$query = "SELECT * FROM order_item,menu_items WHERE order_item.daypartid = '$daypartid' AND order_item.businessid = '$businessid' AND order_item.date = '$today' AND order_item.menu_itemid = menu_items.menu_item_id AND menu_items.menu_typeid = '$curmenu' AND menu_items.recipe_station = '$recipe_station' ORDER BY order_item.orderid DESC";
				}
				else {
					$query = "SELECT * FROM order_item,menu_items WHERE order_item.businessid = '$businessid' AND order_item.date = '$today' AND order_item.menu_itemid = menu_items.menu_item_id AND menu_items.menu_typeid = '$curmenu' AND menu_items.recipe_station = '$recipe_station' ORDER BY order_item.orderid DESC";
				}
				$result = Treat_DB_ProxyOld::query( $query );
				$num = intval( Treat_DB_ProxyOld::mysql_num_rows( $result ) );
				//mysql_close();

				if ( $num > 0 ) {

?>
							<tr>
								<td width="2" bgcolor="#669933"></td>
								<td colspan="3" bgcolor="#669933">
									<center>
										<b>
											<font color="white">
												<?php echo $station_name; ?> 
											</font>
										</b>
									</center>
<?php

					$query44 = "SELECT * FROM caternote WHERE businessid = '$businessid' AND date <= '$today' AND enddate >= '$today' AND recipe_station = '$recipe_station'";
					$result44 = Treat_DB_ProxyOld::query( $query44 );
					$num44 = intval( Treat_DB_ProxyOld::mysql_num_rows( $result44 ) );

					$num44--;
					while ( $num44 >= 0 ) {
						$daynote = Treat_DB_ProxyOld::mysql_result( $result44, $num44, 'note' );
						$daynote = str_replace( "\n", "<br>\n", $daynote );

?>
									<font size="2" face="arial" color="white">
										<i><?php
										echo $daynote;
										?></i>
									</font>
									<br />
<?php
						$num44--;
					}

?>
								</td>
								<td width="2" bgcolor="#669933"></td>
							</tr>
<?php

					$num--;
					while ( $num >= 0 ) {
						$menu_itemid = Treat_DB_ProxyOld::mysql_result( $result, $num, 'menu_itemid' );

						//mysql_connect($dbhost,$username,$password);
						//@mysql_select_db($database) or die( "Unable to select database");
						$query2 = "SELECT * FROM menu_items WHERE menu_item_id = '$menu_itemid'";
						$result2 = Treat_DB_ProxyOld::query( $query2 );
						//mysql_close();

						$item_name = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'item_name' );
						$price = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'price' );
						$description = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'description' );
						$unit = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'unit' );
						$groupid = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'groupid' );
						$heart_healthy = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'heart_healthy' );
						$serving_size = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'serving_size' );
						
						$menuItem = new \ArrayObject();
						$menuItem->menu_itemid = $menu_itemid;
						$menuItem->heart_healthy = $heart_healthy;
						$menuItem->healthy_living = Treat_DB_ProxyOld::mysql_result( $result2, 0, 'healthy_living' );

						$price = money( $price );
						$price9 = $price;

					//////////////////////////////////////////////
					///////////////NUTRITION SPECIFICS////////////
					//////////////////////////////////////////////
						$nutrition = array();
						$showhealthy = '';
						if ( $showme > 0 ) {
							//mysql_connect($dbhost,$username,$password);
							//@mysql_select_db($database) or die( "Unable to select database");
							$query32 = "SELECT * FROM recipe WHERE menu_itemid = '$menu_itemid'";
							$result32 = Treat_DB_ProxyOld::query( $query32 );
							$num32 = intval( Treat_DB_ProxyOld::mysql_num_rows( $result32 ) );
							//mysql_close();

							$num32--;
							while ( $num32 >= 0 ) {
								$recipeid = Treat_DB_ProxyOld::mysql_result( $result32, $num32, 'recipeid' );
								$inv_itemid = Treat_DB_ProxyOld::mysql_result( $result32, $num32, 'inv_itemid' );
								$rec_num = Treat_DB_ProxyOld::mysql_result( $result32, $num32, 'rec_num' );
								$rec_size = Treat_DB_ProxyOld::mysql_result( $result32, $num32, 'rec_size' );
								$srv_num = Treat_DB_ProxyOld::mysql_result( $result32, $num32, 'srv_num' );
								$rec_order = Treat_DB_ProxyOld::mysql_result( $result32, $num32, 'rec_order' );

								//mysql_connect($dbhost,$username,$password);
								//@mysql_select_db($database) or die( "Unable to select database");
								$query23 = "SELECT * FROM inv_items WHERE inv_itemid = '$inv_itemid'";
								$result23 = Treat_DB_ProxyOld::query( $query23 );
								//mysql_close();

								$supc = Treat_DB_ProxyOld::mysql_result( $result23, 0, 'item_code' );
								$inv_itemname = Treat_DB_ProxyOld::mysql_result( $result23, 0, 'item_name' );
								$order_size = Treat_DB_ProxyOld::mysql_result( $result23, 0, 'order_size' );
								$price = Treat_DB_ProxyOld::mysql_result( $result23, 0, 'price' );
								if ( $rec_order == 1 ) {
									$inv_rec_num = Treat_DB_ProxyOld::mysql_result( $result23, 0, 'rec_num' );
								}
								elseif ( $rec_order == 2 ) {
									$inv_rec_num = Treat_DB_ProxyOld::mysql_result( $result23, 0, 'rec_num2' );
								}

								///////////nutrition
								$numserv = $serving_size;

								//mysql_connect($dbhost,$username,$password);
								//@mysql_select_db($database) or die( "Unable to select database");
								$query24 = "SELECT * FROM nutrition WHERE item_code = '$supc'";
								$result24 = Treat_DB_ProxyOld::query( $query24 );
								$num24 = intval( Treat_DB_ProxyOld::mysql_num_rows( $result24 ) );
								//mysql_close();

								$cholesterol = Treat_DB_ProxyOld::mysql_result( $result24, 0, 'cholesterol' );
								$calories = Treat_DB_ProxyOld::mysql_result( $result24, 0, 'calories' );
								$cal_from_fat = Treat_DB_ProxyOld::mysql_result( $result24, 0, 'cal_from_fat' );
								$fiber = Treat_DB_ProxyOld::mysql_result( $result24, 0, 'fiber' );
								$protein = Treat_DB_ProxyOld::mysql_result( $result24, 0, 'protein' );
								$carbs = Treat_DB_ProxyOld::mysql_result( $result24, 0, 'complex_cabs' );
								$sodium = Treat_DB_ProxyOld::mysql_result( $result24, 0, 'sodium' );
								$unsat_fat = Treat_DB_ProxyOld::mysql_result( $result24, 0, 'unsat_fat' );
								$sat_fat = Treat_DB_ProxyOld::mysql_result( $result24, 0, 'sat_fat' );
								$total_fat = Treat_DB_ProxyOld::mysql_result( $result24, 0, 'total_fat' );
								$potassium = Treat_DB_ProxyOld::mysql_result( $result24, 0, 'potassium' );

								$nutr_serv_size = Treat_DB_ProxyOld::mysql_result( $result24, 0, 'serv_size_hh' );
								$nutr_rec_size = Treat_DB_ProxyOld::mysql_result( $result24, 0, 'rec_size' );
								$nutr_rec_size2 = Treat_DB_ProxyOld::mysql_result( $result24, 0, 'rec_size2' );

								if ( $nutr_rec_size == $rec_size ) {
									$nutrition['cholesterol']=round($nutrition['cholesterol']+(($cholesterol/$nutr_serv_size)*($rec_num/$numserv)),1);
									$nutrition['calories']=round($nutrition['calories']+(($calories/$nutr_serv_size)*($rec_num/$numserv)),1);
									$nutrition['cal_from_fat']=round($nutrition['cal_from_fat']+(($cal_from_fat/$nutr_serv_size)*($rec_num/$numserv)),1);
									$nutrition['fiber']=round($nutrition['fiber']+(($fiber/$nutr_serv_size)*($rec_num/$numserv)),1);
									$nutrition['protein']=round($nutrition['protein']+(($protein/$nutr_serv_size)*($rec_num/$numserv)),1);
									$nutrition['carbs']=round($nutrition['carbs']+(($carbs/$nutr_serv_size)*($rec_num/$numserv)),1);
									$nutrition['sodium']=round($nutrition['sodium']+(($sodium/$nutr_serv_size)*($rec_num/$numserv)),1);
									$nutrition['unsat_fat']=round($nutrition['unsat_fat']+(($unsat_fat/$nutr_serv_size)*($rec_num/$numserv)),1);
									$nutrition['sat_fat']=round($nutrition['sat_fat']+(($sat_fat/$nutr_serv_size)*($rec_num/$numserv)),1);
									$nutrition['total_fat']=round($nutrition['total_fat']+(($total_fat/$nutr_serv_size)*($rec_num/$numserv)),1);
									$nutrition['potassium']=round($nutrition['potassium']+(($potassium/$nutr_serv_size)*($rec_num/$numserv)),1);

								}
								else {
									//mysql_connect($dbhost,$username,$password);
									//@mysql_select_db($database) or die( "Unable to select database");
									$query25 = "SELECT * FROM conversion WHERE sizeid = '$rec_size' AND sizeidto = '$nutr_rec_size'";
									$result25 = Treat_DB_ProxyOld::query( $query25 );
									$num25 = intval( Treat_DB_ProxyOld::mysql_num_rows( $result25 ) );
									//mysql_close();

									if ( $num25 > 0 ) {
										$nutr_convert = Treat_DB_ProxyOld::mysql_result( $result25, 0, 'convert' );

										$nutrition['cholesterol']=round($nutrition['cholesterol']+(($cholesterol/($nutr_serv_size*$nutr_convert))*($rec_num/$numserv)),1);
										$nutrition['calories']=round($nutrition['calories']+(($calories/($nutr_serv_size*$nutr_convert))*($rec_num/$numserv)),1);
										$nutrition['cal_from_fat']=round($nutrition['cal_from_fat']+(($cal_from_fat/($nutr_serv_size*$nutr_convert))*($rec_num/$numserv)),1);
										$nutrition['fiber']=round($nutrition['fiber']+(($fiber/($nutr_serv_size*$nutr_convert))*($rec_num/$numserv)),1);
										$nutrition['protein']=round($nutrition['protein']+(($protein/($nutr_serv_size*$nutr_convert))*($rec_num/$numserv)),1);
										$nutrition['carbs']=round($nutrition['carbs']+(($carbs/($nutr_serv_size*$nutr_convert))*($rec_num/$numserv)),1);
										$nutrition['sodium']=round($nutrition['sodium']+(($sodium/($nutr_serv_size*$nutr_convert))*($rec_num/$numserv)),1);
										$nutrition['unsat_fat']=round($nutrition['unsat_fat']+(($unsat_fat/($nutr_serv_size*$nutr_convert))*($rec_num/$numserv)),1);
										$nutrition['sat_fat']=round($nutrition['sat_fat']+(($sat_fat/($nutr_serv_size*$nutr_convert))*($rec_num/$numserv)),1);
										$nutrition['total_fat']=round($nutrition['total_fat']+(($total_fat/($nutr_serv_size*$nutr_convert))*($rec_num/$numserv)),1);
										$nutrition['potassium']=round($nutrition['potassium']+(($potassium/($nutr_serv_size*$nutr_convert))*($rec_num/$numserv)),1);

									}
									elseif ( $num24 > 0 ) {
										$nutrition['cholesterol']=round($nutrition['cholesterol']+$cholesterol,1);
										$nutrition['calories']=round($nutrition['calories']+$calories,1);
										$nutrition['cal_from_fat']=round($nutrition['cal_from_fat']+$cal_from_fat,1);
										$nutrition['fiber']=round($nutrition['fiber']+$fiber,1);
										$nutrition['protein']=round($nutrition['protein']+$protein,1);
										$nutrition['carbs']=round($nutrition['carbs']+$carbs,1);
										$nutrition['sodium']=round($nutrition['sodium']+$sodium,1);
										$nutrition['unsat_fat']=round($nutrition['unsat_fat']+$unsat_fat,1);
										$nutrition['sat_fat']=round($nutrition['sat_fat']+$sat_fat,1);
										$nutrition['total_fat']=round($nutrition['total_fat']+$total_fat,1);
										$nutrition['potassium']=round($nutrition['potassium']+$potassium,1);

									}
								}

								$num32--;
							}

						///////CALORIES
							if ( $showme == 1 ) {
								$is_healthy = 0;
								if ( $nutrition['calories'] <= 500 ) {
									$is_healthy++;
								}
								if ( ( $nutrition['cal_from_fat'] / $nutrition['calories'] ) <= 0.3 ) {
									$is_healthy++;
								}

								if ( $is_healthy == 2 ) {
									$showhealthy = " <img src=img_calorie.jpg align=middle height=16 width=16 alt='Low Calories'>";
								}
							}
						///////FAT
							elseif ( $showme == 2 ) {
								$is_healthy = 0;
								if ( ( $nutrition['cal_from_fat'] / $nutrition['calories'] ) <= 0.3 ) {
									$is_healthy++;
								}

								if ( $is_healthy == 1 ) {
									$showhealthy = " <img src=img_fat.jpg align=middle height=16 width=16 alt='Low Fat'>";
								}
							}
						///////CHOLESTEROL
							elseif ( $showme == 3 ) {
								$is_healthy = 0;
								if ( $nutrition['cholesterol'] <= 60 ) {
									$is_healthy++;
								}

								if ( $is_healthy == 1 ) {
									$showhealthy = " <img src=img_cholesterol.jpg align=middle height=16 width=16 alt='Low Cholesterol'>";
								}
							}
						///////SODIUM
							elseif ( $showme == 4 ) {
								$is_healthy = 0;
								if ( $nutrition['sodium'] <= 575 ) {
									$is_healthy++;
								}

								if ( $is_healthy == 1 ) {
									$showhealthy = " <img src=img_sodium.jpg align=middle height=16 width=16 alt='Low Sodium'>";
								}
							}
						///////CARBS
							elseif ( $showme == 5 ) {
								$is_healthy = 0;
								if ( $nutrition['carbs'] < 20 ) {
									$is_healthy++;
								}

								if ( $is_healthy == 1 ) {
									$showhealthy = " <img src=img_carbo.jpg align=middle height=16 width=16 alt='Low Carbohydrates'>";
								}
							}
						///////Weight Watchers
							elseif ( $showme == 6 ) {
								$showhealthy = round(
									( $nutrition['protein'] / 10.9 )
										+ ( $nutrition['carbs'] / 9.2 )
										+ ( $nutrition['total_fat'] / 3.9 )
										+ ( $nutrition['fiber'] / 35 )
									,0
								);
								$showhealthy = " <div style=\"background:url('tp.jpg') no-repeat; margin-top:3px; color:white; font-size:12px; height:16px; width:16px; line-height:16px; text-align:center; display:inline; font-weight:bold;\">$showhealthy</div>";
							}
						}

					////////////////////////////////////
					//////////////END NUTRITION SPECS///
					////////////////////////////////////


						if ( $today == $today2 ) {
							$showadd = " <a href=additem.php?bid=$businessid&item=$menu_itemid&goto=1><img src=add.gif align=middle height=15 width=16 border=0 alt='Add Item'></a>";
						}
						else {
							$showadd = '';
						}

					/////ALT PRICING
						//mysql_connect($dbhost,$username,$password);
						//@mysql_select_db($database) or die( "Unable to select database");
						$query3 = "SELECT * FROM menu_alt_price WHERE menu_itemid = '$menu_itemid' ORDER BY priceid DESC";
						$result3 = Treat_DB_ProxyOld::query( $query3 );
						$num3 = intval( Treat_DB_ProxyOld::mysql_num_rows( $result3 ) );
						//mysql_close();

						$alt_pricing = '';
						$num3--;
						while ( $num3 >= 0 ) {
							$price2 = Treat_DB_ProxyOld::mysql_result( $result3, $num3, 'price' );
							$comment = Treat_DB_ProxyOld::mysql_result( $result3, $num3, 'comment' );

							$price2 = money( $price2 );
							$alt_pricing = "$alt_pricing, $price2/$comment";

							$num3--;
						}
					//////////

						if ( $price9 > 0 ) {
							$showprice = "$price9/$unit";
						}
						else {
							$showprice = $unit;
						}

					if ( !empty( $description ) ) {
						$showspace = '&nbsp;&nbsp;&nbsp;';
					}
					else {
						$showspace = '';
					}

						if ( !$pageBusiness->no_nutrition ) {
?>
							<tr>
								<td width="2" bgcolor="#669933"></td>
								<td bgcolor="white" colspan="3">
									<?php echo $showhealthy; ?>&nbsp;
									<b>
										<a
											onclick="popup('nutrition.php?mid=<?php echo $menu_itemid; ?>')"
											title="Click for Nutritional Information"
										>
											<font size="2" face="arial" color="#000000"
												onMouseOver="this.style.color='#FF9900';this.style.cursor='hand'"
												onMouseOut="this.style.color='#000000'"
											>
												<?php echo $item_name; ?>
											</font>
										</a>
									</b>
									
									<font size="2" face="arial">
										<font color="gray">
											<?php echo $showprice; ?> 
<?php
							if ( $menuItem->heart_healthy ) {
?>
											<a
												onclick="popup('nutrition.php?mid=<?php echo $menuItem->menu_itemid; ?>')"
												title="Click for Nutritional Information"
											>
												<img src="/assets/images/nutrition/hh1.jpg" align="middle" height="17" width="16" alt="Heart Healthy" />
											</a>
<?php
							}
							if ( $menuItem->healthy_living ) {
?>
											<a
												onclick="popup('nutrition.php?mid=<?php echo $menuItem->menu_itemid; ?>')"
												title="Click for Nutritional Information"
											>
												<img src="/assets/images/nutrition/healthy-living-icon-small.png" align="middle" height="16" width="16" alt="Healthy Living Program" />
											</a>
<?php
							}
?>
											<?php echo $showadd; ?> 
											<?php echo $alt_pricing; ?> 
										</font>
										<br />
										<?php echo $showspace; ?> 
										<i>
										<?php echo $description; ?> 
										</i>
									</font>
								</td>
								<td width="2" bgcolor="#669933"></td>
							</tr>
<?php
						}
						else {
?>
							<tr>
								<td width="2" bgcolor="#669933"></td>
								<td bgcolor="white" colspan="3">
									<b>
										<font size="2" face="arial" color="#000000">
											<?php echo $item_name; ?> 
										</font>
									</b>
									<font size="2" face="arial">
										<font color="gray">
											<?php echo $showprice; ?> 
<?php
							if ( $menuItem->heart_healthy ) {
?>
											<img src="/assets/images/nutrition/hh1.jpg" align="middle" height="17" width="16" alt="Heart Healthy" />
<?php
							}
							if ( $menuItem->healthy_living ) {
?>
											<img src="/assets/images/nutrition/healthy-living-icon-small.png" align="middle" height="16" width="16" alt="Healthy Living Program" />
<?php
							}
?> 
											<?php echo $showhealthy; ?> 
											<?php echo $showadd; ?> 
											<?php echo $alt_pricing; ?> 
										</font>
										<br />
										<?php echo $showspace; ?> 
										<i>
											<?php echo $description; ?> 
										</i>
									</font>
								</td>
								<td width="2" bgcolor="#669933"></td>
							</tr>
<?php
						}
?>
							<tr height="1">
								<td width="2" bgcolor="#669933"></td>
								<td colspan="3" bgcolor="#669933"></td>
								<td width="2" bgcolor="#669933"></td>
							</tr>
<?php
						$num--;
					}
?>
<?php
				}
				$num4--;
			}
?>
							<tr height="2">
								<td colspan="5" bgcolor="#669933"></td>
							</tr>
						</table>
					</td>
<?php

		if ( $no_nutrition == 0 ) {
?>
					<td width="16">
						<img src="/assets/images/spacer.jpg" height="2" width="16" alt="spacer" />
						<br />
						<img src="/assets/images/spacer.jpg" height="2" width="16" alt="spacer" />
						<br />
						<img src="/assets/images/spacer.jpg" height="2" width="16" alt="spacer" />
						<br />
						<img src="/assets/images/spacer.jpg" height="2" width="16" alt="spacer" />
						<br />
						<img src="/assets/images/spacer.jpg" height="2" width="16" alt="spacer" />
						<br />
						<img src="/assets/images/spacer.jpg" height="2" width="16" alt="spacer" />
						<br />
						<img src="/assets/images/spacer.jpg" height="2" width="16" alt="spacer" />
						<br />
						<img src="/assets/images/spacer.jpg" height="2" width="16" alt="spacer" />
						<br />
						<img src="/assets/images/spacer.jpg" height="2" width="16" alt="spacer" />
						<br />
						<img src="/assets/images/spacer.jpg" height="2" width="16" alt="spacer" />
						<br />
						<img src="/assets/images/spacer.jpg" height="2" width="16" alt="spacer" />
						<br />
						<a href="/menu/intro.php?<?php
							$introQuery['showme'] = 1;
							echo http_build_query( $introQuery, null, '&amp;' );
						?>"
							><img src="/assets/images/nutrition/img_calorie.jpg" height="16" width="16" border="0" alt="Low Calorie Items"
						/></a>
						<br />
						<img src="/assets/images/spacer.jpg" height="2" width="16" alt="spacer" />
						<br />
						<a href="/menu/intro.php?<?php
							$introQuery['showme'] = 2;
							echo http_build_query( $introQuery, null, '&amp;' );
						?>"
							><img src="/assets/images/nutrition/img_fat.jpg" height="16" width="16" border="0" alt="Low Fat Items"
						/></a>
						<br />
						<img src="/assets/images/spacer.jpg" height="2" width="16" alt="spacer" />
						<br />
						<a href="/menu/intro.php?<?php
							$introQuery['showme'] = 3;
							echo http_build_query( $introQuery, null, '&amp;' );
						?>"
							><img src="/assets/images/nutrition/img_cholesterol.jpg" height="16" width="16" border="0" alt="Low Cholesterol"
						/></a>
						<br />
						<img src="/assets/images/spacer.jpg" height="2" width="16" alt="spacer" />
						<br />
						<a href="/menu/intro.php?<?php
							$introQuery['showme'] = 4;
							echo http_build_query( $introQuery, null, '&amp;' );
						?>"
							><img src="/assets/images/nutrition/img_sodium.jpg" height="16" width="16" border="0" alt="Low Sodium Items"
						/></a>
						<br />
						<img src="/assets/images/spacer.jpg" height="2" width="16" alt="spacer" />
						<br />
						<a href="/menu/intro.php?<?php
							$introQuery['showme'] = 5;
							echo http_build_query( $introQuery, null, '&amp;' );
						?>"
							><img src="/assets/images/nutrition/img_carbo.jpg" height="16" width="16" border="0" alt="Low Carbohydrates"
						/></a>
						<br />
						<img src="/assets/images/spacer.jpg" height="2" width="16" alt="spacer" />
						<br />
						<a href="/menu/intro.php?<?php
							$introQuery['showme'] = 6;
							echo http_build_query( $introQuery, null, '&amp;' );
						?>"
							><img src="/assets/images/nutrition/ww.jpg" height="16" width="16" border="0" alt="Treat Score"
						/></a>
						<br />
						<img src="/assets/images/spacer.jpg" height="2" width="16" alt="spacer" />
						<br />
						<a href="/menu/intro.php?<?php
							$introQuery['showme'] = null;
							echo http_build_query( $introQuery, null, '&amp;' );
						?>"
							><img src="/assets/images/nutrition/healthy-living-icon-small.png" height="16" width="16" border="0" alt="Healthy Living"
						/></a>
						<br />
						<img src="/assets/images/spacer.jpg" height="2" width="16" alt="spacer" />
						<br />
						<a href="/menu/intro.php?<?php
							$introQuery['showme'] = null;
							echo http_build_query( $introQuery, null, '&amp;' );
						?>"
							><img src="/assets/images/nutrition/hh1.jpg" height="16" width="16" border="0" alt="Heart Healthy"
						/></a>
					</td>
<?php
		}
		else {
?>
					<td width="16"></td>
<?php
		}
?>
				</tr>
			</table>
		</center>
<?php
	}

?>
		<center>
			<table width="700">
				<tr>
					<td>
						<font face="arial" size="2">
							<a href="/menu/menu.php?curmenu=<?php
	if ( $parent > 0 ) {
								echo $curmenu;
	}
	else {
								echo $cafe_menu;
	}
							?>&bid=<?php
								echo $businessid;
							?>" style="<?php
								echo $style;
							?>" title="View the Rest of Our Menu">
								<font color="blue"><?php
									echo $menu_name;
								?></font>
							</a>
<?php
	if ( $catering == 1 ) {
?>
							:: <a href="/menu/menu.php?curmenu=<?php echo $default_menu; ?>&bid=<?php echo $businessid; ?>" style="<?php echo $style; ?>">
								<font color="blue">Catering Menu</font>
							</a>
<?php
	}
	if ( $vend_menu > 0 ) {
?>
							:: <a href="/menu/menu.php?curmenu=<?php echo $vend_menu; ?>&bid=<?php echo $businessid; ?>" style="<?php echo $style; ?>">
								<font color="blue"><?php
									echo $vend_menu_name;
								?></font>
							</a>
<?php
	}
	if ( $no_nutrition == 0 ) {
?>
							:: <a href="/menu/history.php?curmenu=<?php echo $cafe_menu; ?>&bid=<?php echo $businessid; ?>" style="<?php echo $style; ?>">
								<font color="blue">Nutritional Analysis</font>
							</a>
<?php
	}
	if ( $multi_cafe_menu == 1 ) {
?>
							:: <a href="/menu/menuselect.php?bid=<?php echo $businessid; ?>" style="<?php echo $style; ?>">
								<font color="blue">Change Menu</font>
							</a>
<?php
	}
?>
							:: <a href="/menu/contact.php?bid=<?php echo $businessid; ?>" style="<?php echo $style; ?>">
								<font color="blue">Contact Us</font>
							</a>
							:: <a href="/menu/events.php?bid=<?php echo $businessid; ?>" style="<?php echo $style; ?>">
								<font color="blue">Events</font>
							</a>
<?php
	if ( $kiosk === false ) {
?>
							:: <a href="/menu/printintro.php?bid=<?php
								echo $businessid;
							?>&date=<?php
								echo $date;
							?>" style="<?php
								echo $style;
							?>" target="_blank">
								<font color="blue">Print</font>
							</a>
<?php
	}
?>
						</font>
					</td>
					<td align="right">
						<font face="arial" size="2">
							<a href="/menu/intro.php?<?php
								echo http_build_query( $timeFrameViewQueryLink, null, '&amp;' );
							?>" style="<?php
								echo $style;
							?>">
								<font color="#999999">
								<?php echo $timeframeViewLinkText; ?> 
								</font>
							</a>
						</font>
					</td>
				</tr>
			</table>
		</center>

		<p><br /><p>

		<center>
			<table width="700">
				<tr>
					<td>
						<font face="arial" size="1" color="gray">
							Nutritional information is provided as a general guide only. 
							Variation in serving sizes, preparation techniques and sources 
							of supply, as well seasonal differences may affect the 
							nutrition values for each product. In addition, product 
							formulations change periodically. You should expect some 
							variation in the nutrient content of our products. If you are 
							on a restricted diet for the treatment of a disease or other 
							condition, you should consult your physician, or registered 
							dietician for more specific nutritional guidelines.
						</font>
					</td>
				</tr>
			</table>
		</center>

		<div id="testdiv1" style="position:absolute;visibility:hidden;background-color:white;layer-background-color:white;"></div>
<?php

	//mysql_close();
	google_page_track();
?>
	</body>
</html>
<?php
}


