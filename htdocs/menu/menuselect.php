<?php

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+3);
        if ($diff > 0 && $diff < '0.01'){$diff="0.01";}
        elseif($diff < 0 && $diff > '-0.01'){$diff="-0.01";}
        return $diff;
}

function nextday($date2)
{
   $day=substr($date2,8,2);
   $month=substr($date2,5,2);
   $year=substr($date2,0,4);
   $leap = date("L");

   if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
   {
      if ($month == "01"){$month="02";}
      elseif ($month == "03"){$month="04";}
      elseif ($month == "05"){$month="06";}
      elseif ($month == "07"){$month="08";}
      elseif ($month == "08"){$month="09";}
      elseif ($month == "10"){$month="11";}
      $day='01';    
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '29' && $leap == '1')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='02' && $day == '28' && $leap == '0')
   {
      $month='03';
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
   {
      if ($month == "04"){$month="05";}
      if ($month == "06"){$month="07";}
      if ($month == "09"){$month="10";}
      if ($month == "11"){$month="12";}
      $day='01';
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else if ($month=='12' && $day=='31')
   {
      $day='01';
      $month='01';
      $year++;
      $tomorrow = "$year-$month-$day";
      return $tomorrow;
   }
   else
   {
      $day=$day+1;
      if ($day<10){$day="0$day";}
      $tomorrow="$year-$month-$day";
      return $tomorrow;
   }
}

function prevday($date2) {
$day=substr($date2,8,2);
$month=substr($date2,5,2);
$year=substr($date2,0,4);
$day=$day-1;
$leap = date("L");

if ($day <= 0)
{
   if ($month == 01)
   {
      $month = '12';
      $year--;
      $day=$day+31;
      $yesterday = "$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+31;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
   {
      $month--;
      if ($month < 10)
      {
         $month="0$month";
      }
      $day=$day+30;
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   
   elseif ($leap==1&&$month=='03')
   {
      $day=$day+29;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
   else
   {
      $day=$day+28;
      $month='02';
      $yesterday="$year-$month-$day";
      return $yesterday;
   }
}
else
{
   if ($day < 10)
   {
      $day="0$day";
   }
   $yesterday="$year-$month-$day";
   return $yesterday;
}
}

function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}

define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));

$businessid = isset($_GET["bid"])?$_GET["bid"]:'';

$expire = 60 * 60 * 24 * 60 + time(); 
setcookie("mymenu", "",$expire);
  
$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today2="$year-$month-$day";
if($today==""){$today="$year-$month-$day";$showday="Today`s";$todayname = dayofweek($today);}
elseif($today==$today2){$showday="Today`s";$todayname = dayofweek($today2);}
else{$todayname = dayofweek($today); $showday="$todayname`s";}


$yesterday=prevday($today);
$tomorrow=nextday($today); 

if ($businessid==""){
    $location="index.php";
    header('Location: ./' . $location);
}
else{

	\EE\Model\Business\Setting::db( Treat_DB::singleton() );
	$settings = \EE\Model\Business\Setting::get("business_id = ". abs(intval($businessid)), true);
	$background_image_logo = $settings->background_image ?: '';
	$site_logo = $settings->home_page_logo ?: '/assets/images/menu/rc-header.jpg';


    $query = "SELECT * FROM menu_type WHERE businessid = '$businessid' ORDER BY menu_typename";
    $result = Treat_DB_ProxyOld::query($query);
?>

<body background="<?php echo $background_image_logo; ?>">

	<center>
		<table cellspacing=0 cellpadding=0 border=0 width=700>
			<tr bgcolor=#6699FF>
				<td colspan=2 bgcolor=>
					<font color=white size=3>
					<b>The Right Choice... for a Healthier You!</b>
					</font>
				</td>
				<td colspan=2 align=right></td>
			</tr>
			<tr>
				<td colspan=4><img src="<?php echo $site_logo; ?>" height=130 width=700></td>
			</tr>
		</table>
	</center>

	<center>
		<table cellspacing=0 cellpadding=0 border =0 width=700 bgcolor=#669933 style="border: 2px solid #669933;font-family: arial;">
			<tr>
				<td bgcolor=#669933>
					&nbsp;<font color=white><b><i>Please Choose a Menu...</i></b></font>
				</td>
			</tr>
			<tr height=2>
				<td bgcolor=#669933></td>
			</tr>
			<tr height=5>
				<td bgcolor=white></td>
			</tr>
<?php
while( $r = mysql_fetch_array($result) ) {
	$menu_typeid=$r["menu_typeid"];
	$menu_typename=$r["menu_typename"];
?>
			<tr bgcolor=white>
				<td>
					&nbsp;<a href="placemenu.php?bid=<?php echo intval($businessid); ?>&mtid=<?php echo $menu_typeid; ?>" style="<?php echo $style; ?>">
					<font face=ARIAL size=3 color=blue onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='blue' title='Set <?php echo $menu_typename; ?> as my default menu'>
						<?php echo $menu_typename; ?>
					</font>
					</a>
				</td>
			</tr>
<?php
}
?>
			<tr height=10>
				<td bgcolor=white></td>
			</tr>
			<tr>
				<td bgcolor=white>
					&nbsp;If you are unsure of which menu to choose, please <a href="contact.php?bid=<?php echo intval($businessid); ?>" style="<?php echo $style; ?>"><FONT FACE=ARIAL COLOR=blue onMouseOver=this.style.color='#FF9900' onMouseOut=this.style.color='blue'>Contact Us</font></a>.
				</td>
			</tr>
		</table>
	</center>
</body>
<?php
	google_page_track();
}
?>