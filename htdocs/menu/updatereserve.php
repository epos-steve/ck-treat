<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once(DOC_ROOT.'/bootstrap.php');

$style = "text-decoration:none";

$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";

$reserveid = isset($_POST['reserveid'])?$_POST['reserveid']:'';
$accountid = \EE\Controller\Base::getSessionCookieVariable( 'accountid' );
$businessid = \EE\Controller\Base::getSessionCookieVariable( 'businessid' );
$curmenu = \EE\Controller\Base::getSessionCookieVariable( 'curmenu' );
if ($reserveid==""){
	$reserveid = \EE\Controller\Base::getSessionCookieVariable( 'reserveid' );
}
$comefrom = isset($_POST['comefrom'])?$_POST['comefrom']:'';

$query = "SELECT * FROM customer WHERE username = '$user' AND password = '$pass'";
$result = Treat_DB_ProxyOld::query($query);
$num=mysql_numrows($result);

if ($accountid<1||$reserveid<1||$businessid<1){
	$location="caterdetails.php";
	header('Location: ./' . $location);
} else {
	$companyid="1";

	$query = "SELECT * FROM business WHERE businessid = '$businessid'";
	$result = Treat_DB_ProxyOld::query($query);

	$caterdays=@mysql_result($result,0,"caterdays");
	$cater_spread=@mysql_result($result,0,"cater_spread");
	$cater_spread--;
	$caternum=@mysql_result($result,0,"caternum");
	$weekend=@mysql_result($result,0,"weekend");
	$srvchrg=@mysql_result($result,0,"srvchrg");
	$srvchrgpcnt=@mysql_result($result,0,"srvchrgpcnt");
	$cater_costcenter=@mysql_result($result,0,"cater_costcenter");
	
	if ($srvchrg==1){
		
	} else {
		$srvchrgpcnt=0;
	}

	$lastday=$today;

	for ($counter=0;$counter<$caterdays;$counter++){
		$lastday=nextday($lastday);
	}

	$goto = isset($_POST['goto'])?$_POST['goto']:'';
	$date = isset($_POST['date'])?$_POST['date']:'';
	$peoplenum = isset($_POST['peoplenum'])?$_POST['peoplenum']:'';
	$roomid = isset($_POST['room'])?$_POST['room']:'';
	$start_hour = isset($_POST['start_hour'])?$_POST['start_hour']:'';
	$start_min = isset($_POST['start_min'])?$_POST['start_min']:'';
	$end_hour = isset($_POST['end_hour'])?$_POST['end_hour']:'';
	$end_min = isset($_POST['end_min'])?$_POST['end_min']:'';
	$start_am = isset($_POST['start_am'])?$_POST['start_am']:'';
	$end_am = isset($_POST['end_am'])?$_POST['end_am']:'';
	$roomcomment = isset($_POST['roomcomment'])?$_POST['roomcomment']:'';
	$roomcomment=str_replace("'","`",$roomcomment);
	$comment = isset($_POST['comment'])?$_POST['comment']:'';
	$comment=str_replace("'","`",$comment);
	$costcenter = isset($_POST['costcenter'])?$_POST['costcenter']:'';

	if ($start_am=="AM" && $start_hour==12){
		$start_hour="00";
	} elseif ($start_am=="PM" && $start_hour!=12){
		$start_hour=$start_hour+12;
	}
	
	if ($end_am=="AM" && $end_hour==12){
		$end_hour="00";
	} elseif ($end_am=="PM" && $end_hour!=12){
		$end_hour=$end_hour+12;
	}

	$start_hour2="$start_hour$start_min";
	$end_hour2="$end_hour$end_min";

	$start_min=$start_min-$cater_spread;
	if($start_min<0){
		$start_hour--;$start_min=60+$start_min;
	}

	if($start_hour<10){
		$start_hour="0$start_hour";
	}

	if($start_min<10){
		$start_min="0$start_min";
	}

	$end_min=$end_min+$cater_spread;
	if($end_min>=60){
		$end_hour++;$end_min=$end_min-60;
	}

	if($end_hour<10){
		$end_hour="0$end_hour";
	}

	if($end_min<10){
		$end_min="0$end_min";
	}

	$start_hour="$start_hour$start_min";
	$end_hour="$end_hour$end_min";

	$query = "SELECT * FROM caterclose WHERE businessid = '$businessid' AND date = '$date'";
	$result = Treat_DB_ProxyOld::query($query);
	$num5=mysql_numrows($result);

	$query = "SELECT * FROM reserve WHERE businessid = '$businessid' AND date = '$date'";
	$result = Treat_DB_ProxyOld::query($query);
	$num=mysql_numrows($result);
	$caterjobs=$num;

	$query = "SELECT * FROM reserve WHERE reserveid != '$reserveid' AND roomid = '$roomid' AND roomid != '0' AND date = '$date' AND ((start_hour <= '$start_hour' AND end_hour >= '$start_hour') OR (end_hour >= '$end_hour' AND start_hour <= '$end_hour') OR (start_hour >= '$start_hour' AND end_hour <= '$end_hour'))";
	$result = Treat_DB_ProxyOld::query($query);
	$num = mysql_numrows($result);

	$dayname = dayofweek($date);

	$hasError = false;
	if ($num != 0)
	{
		$hasError = true;
		$_SESSION->addFlashError('Room already Booked.');
	}

	if (trim($peoplenum) == "")
	{
		$hasError = true;
		$_SESSION->addFlashError('Please enter number of guests.');
	}
	else if ($peoplenum <= 0)
	{
		$hasError = true;
		$_SESSION->addFlashError('Number of guests must be greater than 0 (zero).');
	}

	if ($end_hour2 < $start_hour2)
	{
		$hasError = true;
		$_SESSION->addFlashError('End time must be after start time. Please double check.');
	}
	$checkTimestamp = strtotime($date);
	if (strtotime($today) > $checkTimestamp || $checkTimestamp < strtotime($lastday))
	{
		$hasError = true;
		$_SESSION->addFlashError('Not a valid Date. Must be after '.$lastday.'.');
	}
	if ($roomid == 0 && $roomcomment == "")
	{
		$hasError = true;
		$_SESSION->addFlashError('Please specify why location is N/A.');
	}
	if ($cater_costcenter == 1 && $costcenter == "")
	{
		$hasError = true;
		$_SESSION->addFlashError('A Cost Center is Required.');
	}
	if (trim($comment) == '')
	{
		$hasError = true;
		$_SESSION->addFlashError('Please enter a reference.');
	}
	if ($caterjobs >= $caternum)
	{
		$hasError = true;
		$_SESSION->addFlashError('That Day is already Full.');
	}
	if ( ( $dayname == "Saturday" || $dayname == "Sunday" ) && $weekend == 0 )
	{
		$hasError = true;
		$_SESSION->addFlashError('Sorry, we are closed weekends.');
	}
	if ($num5 != 0)
	{
		$hasError = true;
		$_SESSION->addFlashError('Sorry, we are closed that day.');
	}

	if (!$hasError)
	{
		$query="UPDATE reserve SET businessid = '$businessid', accountid = '$accountid', date = '$date',start_hour = '$start_hour2',end_hour = '$end_hour2',peoplenum = '$peoplenum',roomid = '$roomid',roomcomment = '$roomcomment',comment = '$comment',status = '1', costcenter = '$costcenter', service = '$srvchrgpcnt' WHERE reserveid = '$reserveid'";
		$result = Treat_DB_ProxyOld::query($query);

		////////////////////////////////////////////////////////
		////////////////insert/update custom fields/////////////
		////////////////////////////////////////////////////////
		$updateid = $reserveid;

		$query_custom = "SELECT * FROM invoice_custom_fields WHERE businessid = '$businessid' AND active = '0' AND customer = '1'";
		$result_custom = Treat_DB_ProxyOld::query($query_custom);
		$num_custom = mysql_num_rows($result_custom);

		if ($num_custom > 0)
		{

			while($r_cust=mysql_fetch_array($result_custom))
			{
				$cust_id=$r_cust["id"];
				$type=$r_cust["type"];
				$newvalue = $_POST["cust$cust_id"];

				$query_custom2 = "SELECT id FROM invoice_custom_values WHERE reserveid = '$updateid' AND customid = '$cust_id'";
				$result_custom2 = Treat_DB_ProxyOld::query($query_custom2);
				$num_custom2 = mysql_num_rows($result_custom2);

				if($type == 4){}
				elseif($num_custom2!=0){
					$valueid = @mysql_result($result_custom2,0,"id");
					$query_custom2 = "UPDATE invoice_custom_values SET value = '$newvalue' WHERE id = '$valueid'";
					$result_custom2 = Treat_DB_ProxyOld::query($query_custom2);
				} elseif($newvalue!=""){
					$query_custom2 = "INSERT INTO invoice_custom_values (reserveid,customid,value) VALUES ('$updateid','$cust_id','$newvalue')";
					$result_custom2 = Treat_DB_ProxyOld::query($query_custom2);
				}
			}
		}

		////////////////////////////////////////////////////////
		////////////////end custom fields///////////////////////
		////////////////////////////////////////////////////////

		$location="caterdetails.php?reserveid=$reserveid#detail";
		if($goto==10){
			$location="createorder.php";
		} elseif ($comefrom==1){
			$location="caterdetails.php?reserveid=$reserveid#detail";
		}
		header('Location: ./' . $location);
	}
	else
	{
		$_SESSION['createorder2postdata'] = $_POST;
		header('Location: ./createorder2.php?reserveid='.intval($reserveid).'#invoice');
	}
}
?>