<?php

define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once(DOC_ROOT.'/bootstrap.php');

$businessid = isset($_GET["bid"])?$_GET["bid"]:'';
//$curmenu = isset($_GET["curmenu"]);
$curmenu = $_GET["curmenu"];

if ($businessid==""){
    $location="index.php";
    header('Location: ./' . $location);
}
else{

	\EE\Model\Business\Setting::db( Treat_DB::singleton() );
	$settings = \EE\Model\Business\Setting::get("business_id = ". abs(intval($businessid)), true);
	$background_image_logo = $settings->background_image ?: '';
	$site_logo = $settings->home_page_logo ?: '/assets/images/menu/rc-header.jpg';


?>
<html>
<head>
<?php includeTimeOutCode(); ?>

<title>Treat America: Proud to Serve You!</title>

<script language="JavaScript"
   type="text/JavaScript">
function popup(url)
  {
     popwin=window.open(url,"Nutrition","location=no,menubar=no,titlebar=no,scrollbars=yes,resizeable=no,height=400,width=415");   
  }
</script>

<script language="JavaScript"
   type="text/JavaScript">
function popup2(url)
  {
     popwin=window.open(url,"Tutorial","location=no,scrollbars=yes,menubar=no,titlebar=no,resizeable=no,height=400,width=425");   
  }
</script>

</head>
<?php
	$query = sprintf("SELECT policy FROM caterpolicy WHERE businessid = %d", $businessid);
	$result = Treat_DB_ProxyOld::query($query);

	$num=mysql_numrows($result);
	$policy=@mysql_result($result,0,"policy");
?>

<body background="<?php echo $background_image_logo; ?>">
	<center>
		<table cellspacing=0 cellpadding=0 border=0 width=700 bgcolor=#669933>
			<tr bgcolor=#6699FF>
				<td colspan=2 bgcolor=>
					<font color=white size=3>
					<b>The Right Choice... for a Healthier You!</b>
				</td>
				<td colspan=2 align=right></td>
			</tr>
			<tr>
				<td colspan=4><img src="<?php echo $site_logo; ?>" height=130 width=700></td>
			</tr>
		</table>

		<br>

		<center>
			<table width=700 cellspacing=0 cellpadding=0 bgcolor=white>
				<tr height=2 bgcolor=#669933>
					<td colspan=4></td>
				</tr>
				<tr bgcolor=#669933>
					<td bgcolor=#669933 width=2></td>
					<td colspan=2>
						<font face=arial color=white>
						<b><i>&nbsp;Catering Policy</i></b>
					</td>
					<td bgcolor=#669933 width=2></td>
				</tr>
				<tr height=2 bgcolor=#669933>
					<td colspan=4></td>
				</tr>
				<tr valign=top height=5>
					<td bgcolor=#669933 width=2></td>
					<td colspan=2></td>
					<td bgcolor=#669933 width=2></td>
				</tr>
<?php if($num==0) : ?>
				<tr valign=top>
					<td bgcolor=#669933 width=2></td>
					<td colspan=2>
						<font face=arial size=2>There is no catering policy!!!</font>
					</td>
					<td bgcolor=#669933 width=2></td>
				</tr>
<?php else : ?>
				<tr valign=top>
					<td bgcolor=#669933 width=2></td>
					<td colspan=2>
						<font face=arial size=2><?php echo $policy; ?></font>
					</td>
					<td bgcolor=#669933 width=2></td>
				</tr>
<?php endif; ?>
				<tr height=2 bgcolor=#669933>
					<td colspan=4></td>
				</tr>
			</table>
		</center>

		<center>
			<font face=arial size=2>
				<a href="/menu/menu.php?bid=<?php echo intval($businessid); ?>&curmenu=<?php echo $curmenu; ?>">
					<font color=blue>Return</font>
				</a>
			</font>
		</center>

<?php google_page_track(); ?>
	
</body>
</html>
<?php
}
?>
