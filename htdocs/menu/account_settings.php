<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once(DOC_ROOT.'/bootstrap.php');

$_SESSION->setUserType( Treat_Session::TYPE_CUSTOMER );

$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");
$bbdayname=date("l");
$bbmonth=date("F");
$bbday=date("j");

$user = \EE\Controller\Base::getSessionCookieVariable( 'usercook' );
$pass = \EE\Controller\Base::getSessionCookieVariable( 'passcook' );
$accountid = \EE\Controller\Base::getSessionCookieVariable( 'accountid' );
$businessid = \EE\Controller\Base::getSessionCookieVariable( 'businessid' );
$reserveid = \EE\Controller\Base::getSessionCookieVariable( 'reserveid' );
$realuser = \EE\Controller\Base::getSessionCookieVariable( 'realuser' );

$search = \EE\Controller\Base::getPostOrGetVariable( 'search' );
$search = str_replace( "'", "`", $search );

$goto = \EE\Controller\Base::getPostOrGetVariable( 'goto' );
$error = \EE\Controller\Base::getPostOrGetVariable( 'error' );

if ( $accountid < 1 ) {
	$location = '/menu/logout.php?goto=2';
	header( 'Location: ' . $location );
}
elseif ( $businessid < 1 ) {
	$location = '/menu/index.php';
	header( 'Location: ' . $location );
}
else
{

	\EE\Model\Business\Setting::db( Treat_DB::singleton() );
	$settings = \EE\Model\Business\Setting::get("business_id = ". abs(intval($businessid)), true);
	$background_image_logo = $settings->background_image ?: '';
	$site_logo = $settings->site_logo ?: 'logo.jpg';

?>
<!DOCTYPE html
	PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title>Treat America: <?php echo $businessname2; ?></title>
		<link rel="stylesheet" href="/assets/css/style.css" />
		<link rel="stylesheet" href="/assets/css/calendarPopup.css" />
		<link rel="stylesheet" href="/assets/css/nutrition/style.css" />
		<link rel="stylesheet" href="/assets/css/nutrition/history.css" />
<?php includeTimeOutCode(); ?>
		<script language="JavaScript" type="text/javascript" src="/assets/js/calendarPopup.js"></script>
	</head>
	<body <?php if(false !== stripos($background_image_logo, 'backdrop.jpg')) : ?>class="standard"<?php else : ?>style="background-image: url(<?php echo $background_image_logo; ?>)"<?php endif; ?>>
		<div class="page_wrapper">
			<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
					<td colspan="4">
						<img src="<?php echo $site_logo; ?>" alt="" />
					</td>
				</tr>

<?php
#	////get scancode
#	$query = "SELECT scancode FROM customer WHERE username = '$user' AND password = '$pass'";
#	$result = Treat_DB_ProxyOld::query( $query );

#	$scancode = @mysql_result( $result, 0, 'scancode' );
	$scancode = $_SESSION->getUser()->scancode;

#	$query = "SELECT * FROM company WHERE companyid = '1'";
#	$result = Treat_DB_ProxyOld::query( $query );

#	$companyname = @mysql_result( $result, 0, 'companyname' );

#	$query = "SELECT * FROM business WHERE businessid = '$businessid'";
#	$result = Treat_DB_ProxyOld::query( $query );

#	$businessname = @mysql_result( $result, 0, 'companyname' );
#	$allow_sub_orders = @mysql_result( $result, 0, 'allow_sub_orders' );

	$page_business = Treat_Model_Business_Singleton::getSingleton();
	$businessname = $page_business->address;
	$allow_sub_orders = $page_business->allow_sub_orders;
	$companyname = $page_business->companyname;

	$query = "SELECT * FROM accounts WHERE accountid = '$accountid'";
	$result = Treat_DB_ProxyOld::query( $query );

	$accountname = @mysql_result( $result, 0, 'name' );
	$accountnum = @mysql_result( $result, 0, 'accountnum' );
	$parent = @mysql_result( $result, 0, 'parent' );

	$logout="<font color=white>Sign Off</font>";
	if ( $realuser != '' ) {
		$showrealuser = sprintf( '<i> as %s</i>', $realuser );
	}
?>

				<tr bgcolor="#6699ff">
					<td colspan="2">
						<font color="white" size="3">
							<b>
								The Right Choice... for a Healthier You!
							</b>
						</font>
					</td>
					<td align="right" colspan="2"><?php echo $showmyaccount; ?></td>
				</tr>
				<tr bgcolor="#669933">
					<td width="10%">
						<img src="/menu/rc-header2.jpg" height="80" width="108" alt="" />
					</td>
					<td>
						<font size="4" color="white">
							<b><?php echo $companyname; ?> - <?php echo $businessname; ?></b>
						</font>
						<br/>
						<b>
							<font color="white">
								Account: <?php echo $accountname; ?> <?php echo $showrealuser; ?> 
								<br/>
								Account #: <?php echo $accountnum; ?>
							</font>
						</b>
					</td>
					<td colspan="2" align="right" valign="top">
						<font color="white">
							<b><?php echo $bbdayname, ', ', $bbmonth, ' ', $bbday, ', ', $year; ?></b>
							<br/>
							<font size="2">
							[<a style="<?php echo $style; ?>" href="/menu/logout.php?goto=1"
							><b><font color="white"><?php
								echo $logout;
							?></font></b></a>]
							</font>
						</font>
					</td>
				</tr>
				<tr bgcolor="black">
					<td height="1" colspan="4"></td>
				</tr>
			</table>

<?php
if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
	echo $_SESSION->getFlashDebug( "\t\t\t<pre class=\"flash_debug\">%s</pre>\n" );
}
else {
	$_SESSION->getFlashDebug( null, null, null, null );
}
echo $_SESSION->getFlashError(   "\t\t\t<div class=\"flash_error\">%s</div>\n"   );
echo $_SESSION->getFlashMessage( "\t\t\t<div class=\"flash_message\">%s</div>\n" );

	if ( $error == 1 ) {
		$showerror = "<font color=red face=arial size=2>Invalid Password</font>";
	}
	elseif ( $error == 2 ) {
		$showerror = "<font color=green face=arial size=2>Successfully Updated</font>";
	}
?>
		<form action="/menu/save_account_settings.php" method="post" style="margin: 1em 0; padding:0;">
			<input type="hidden" name="goto" value="<? echo $goto; ?>" />
			<table cellspacing="0" cellpadding="0" width="100%" style="border: 2px solid #999999;font-family: arial;" bgcolor="white">
				<tr bgcolor="#999999">
					<td colspan="4">
						<font color="white" face="arial" size="2">
							<b>Account Settings</b>
						</font>
					</td>
				</tr>
				<tr>
					<td align="right" width="25%">
						Email:
					</td>
					<td width="25%" style="white-space: nowrap;">
						&nbsp;<input type="text" size="40" disabled="disabled" value="<?php echo $user; ?>"
							style="border: 0; background-color: transparent; color: #000;"
						/>
					</td>
					<td width="25%" align="right">
						<label for="scancode">
							Gift Card#:
						</label>
					</td>
					<td>
						&nbsp;<input type="text" size="20" name="scancode" id="scancode" value="<?php
							echo Treat_Model_User_Customer::showScancode( $scancode );
#							echo $_SESSION->getUser()->scancode;
						?>" />
						<input type="hidden" name="scancode_old" value="<?php
							echo Treat_Model_User_Customer::showScancode( $scancode );
#							echo $_SESSION->getUser()->scancode;
						?>" />
					</td>
				</tr>
				<tr valign="top">
					<td align="right">
						<label for="old_pass">
							Old Password:
						</label>
					</td>
					<td>
						&nbsp;<input type="password" size="10" value="" name="old_pass" id="old_pass" />
					</td>
					<td rowspan="2"></td>
					<td rowspan="2">
						<font size="1" face="arial">
							*Gift Card#'s are used to track your purchases. The 
							number can be found on the back of your card.
						</font>
					</td>
				</tr>
				<tr valign="top">
					<td align="right">
						<label for="pass1">
							New Password:
						</label>
					</td>
					<td>
						&nbsp;<input type="password" size="10" value="" name="pass1" id="pass1" />
					</td>
				</tr>
				<tr>
					<td align="right">
						<label for="pass2">
							Confirm Password:
						</label>
					</td>
					<td>
						&nbsp;<input type="password" size="10" value="" name="pass2" id="pass2" />
					</td>
				</tr>
				<tr>
					<td align="right" colspan="4">
						<?php echo $showerror; ?>&nbsp;&nbsp;<input type="submit" value="Save" />&nbsp;
					</td>
				</tr>
			</table>
		</form>
	<?php
	
	if ( $goto == 1 ) {
		$page = '/menu/intro.php';
	}
	elseif ( $goto == 2 ) {
		$page = '/menu/menu.php';
	}
	elseif ( $goto == 3 ) {
		$page = '/menu/history.php';
	}
	elseif ( $goto == 4 ) {
		$page = '/menu/catertrack.php';
	}
	elseif ( $goto == 5 ) {
		$page = '/menu/createorder.php';
	}
	else {
		$page = sprintf( '/menu/%s.php', $intro );
	}
?>

			<center>
				<a href="<?php echo $page; ?>" style="<?php echo $style; ?>">
					<font color="blue" size="2" face="arial">
					Return
					</font>
				</a>
			</center>
<?php
	google_page_track();
?>
		</div>
	</body>
</html>
<?php
}
?>