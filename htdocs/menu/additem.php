<?php
define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once(DOC_ROOT.'/bootstrap.php');

$detail = null; # Notice: Undefined variable
$goto = null; # Notice: Undefined variable


#echo '<pre style="border: 1px solid red;">';
#echo '$_POST = ';
#var_dump( $_POST );
#echo '</pre>';

$pageVariables = new \EE\ArrayObject();

$user = \EE\Controller\Base::getSessionCookieVariable( 'usercook' );
$pass = \EE\Controller\Base::getSessionCookieVariable( 'passcook' );
$accountid = \EE\Controller\Base::getSessionCookieVariable( 'accountid' );
$businessid = \EE\Controller\Base::getPostGetSessionOrCookieVariable( array( 'businessid', 'bid' ) );
$curmenu = \EE\Controller\Base::getSessionCookieVariable( 'curmenu' );
$reserveid = \EE\Controller\Base::getPostVariable( 'reserveid', null );


$qty = \EE\Controller\Base::getPostVariable( 'qty', '' );
$item = \EE\Controller\Base::getPostVariable( 'menu_item_id', '' );
$groupid = \EE\Controller\Base::getPostVariable( 'groupid', '' );
$optionnum = \EE\Controller\Base::getPostVariable( 'optionnum', '' );
$special = \EE\Controller\Base::getPostVariable( 'special', '' );
$special=str_replace("'","`",$special);
$showname = \EE\Controller\Base::getPostVariable( 'item_name', '' );

if ( $item == "" ) {
	$item = \EE\Controller\Base::getGetVariable( 'item', '' ); 
	$optionnum = -1;
	$qty = 1;
	$goto = \EE\Controller\Base::getGetVariable( 'goto', '' );
	$reserveid = \EE\Controller\Base::getSessionCookieVariable( 'reserveid', '' );
}

$pageVariables->today = strtotime( date( 'Y-m-d 12:00' ) ); # skip issues with leap time by setting the time to noon
$pageVariables->accountId = $accountid;
$pageVariables->businessId = $businessid;
$pageVariables->currentMenu = $curmenu;
$pageVariables->reserveId = intval( $reserveid );
$pageVariables->qty = $qty;
$pageVariables->menuItemId = $item;
$pageVariables->groupId = $groupid;
$pageVariables->optionNumber = $optionnum;
$pageVariables->optionGroups = \EE\Controller\Base::getPostVariable( 'optionGroup', null );
$pageVariables->special = $special;
$pageVariables->showname = $showname;
$pageVariables->goTo = $goto;

if ( $qty == 0 || $item == "" )
{
	
}

else
{

	if ( $pageVariables->reserveId < 1 ) {
		$pageVariables->tempDate = strtotime( '+2 Days', $pageVariables->today );
		
		$reserve = new \EE\Model\Menu\Reserve();
		$reserve->businessid = $pageVariables->businessId;
		$reserve->companyid = 1;
		$reserve->date = date( 'Y-m-d', $pageVariables->tempDate );
		$reserve->accountid = $pageVariables->accountId ?: 0;
		$reserve->status = 1;
		$reserve->comment = date( 'm/d/y', $pageVariables->today );
		$reserve->curmenu = $pageVariables->currentMenu;
		$reserve->save();
		
		$reserveid = $pageVariables->reserveId = $reserve->reserveid;

		\EE\Controller\Base::setCookie( 'reserveid', $reserveid );
	}
	else {
		$reserve = new \EE\Model\Menu\Reserve();
		$reserve->reserveid = $pageVariables->reserveId;
	}

	if ( $pageVariables->optionGroups ) {
#		echo '<pre style="border: 1px solid blue;">';
		
		$menuItemExtras = new \EE\ArrayObject( array(
			'price' => 0,
			'details' => array(),
			'special' => $pageVariables->special,
		) );
		foreach ( $pageVariables->optionGroups as $optionGroupId => $optionsGroup ) {
#			echo sprintf(
#				'$pageVariables->optionGroups[ $optionGroupId[%s] ] => $optionsGroup[%s]'
#				,var_export( $optionGroupId, true )
#				,var_export( $optionsGroup, true )
#			), PHP_EOL;
			if ( array_key_exists( 'type', $optionsGroup ) && array_key_exists( 'options', $optionsGroup ) ) {
				switch ( $optionsGroup['type'] ) {
					# radio button
					case 0:
						if ( is_array( $optionsGroup['options'] ) ) {
							$optionId = array_shift( $optionsGroup['options'] );
						}
						else {
							$optionId = $optionsGroup['options'];
						}
#						echo '$optionId = ', var_export( $optionId, true ), PHP_EOL;
						$optionItem = \EE\Model\Menu\Option\Item::get( intval( $optionId ), true );
						
						$menuItemExtras->price += $optionItem->price;
						if ( 0 == $optionItem->noprint ) {
							$menuItemExtras->details[] = $optionItem->description;
						}
						
						break;
					# checkbox
					case 1:
						foreach ( $optionsGroup['options'] as $optionId ) {
#							echo '$optionId = ', var_export( $optionId, true ), PHP_EOL;
							$optionItem = \EE\Model\Menu\Option\Item::get( intval( $optionId ), true );
							
							$menuItemExtras->price += $optionItem->price;
							$menuItemExtras->details[] = $optionItem->description;
						}
						break;
					# text
					case 2:
						foreach ( $optionsGroup['options'] as $optionId => $optionQty ) {
							if ( $optionQty ) {
#								echo '$optionId = ', var_export( $optionId, true ), PHP_EOL;
#								echo '	$qty = ', var_export( $optionQty, true ), PHP_EOL;
								$optionItem = \EE\Model\Menu\Option\Item::get( intval( $optionId ), true );
								
								$menuItemExtras->price += $optionItem->price;
								$menuItemExtras->details[] = sprintf(
									'%s %s'
									,$optionQty
									,$optionItem->description
								);
							}
						}
						break;
				}
			}
		}
		$menuItemExtras->detail = implode( ', ', (array)$menuItemExtras->details );
		if ( $menuItemExtras->special ) {
			$menuItemExtras->detail .= '; ' . $menuItemExtras->special;
		}
#		echo '$menuItemExtras = ';
#		var_dump( $menuItemExtras );
		
/*
*/
		$menuItem = \EE\Model\Menu\Item::get( intval( $pageVariables->menuItemId ), true );
		
		$invoiceDetail = new \EE\Model\Invoice\Detail();
		$invoiceDetail->invoiceid    = 0;
		$invoiceDetail->qty          = $pageVariables->qty;
		$invoiceDetail->item         = $menuItem->item_name;
		$invoiceDetail->price        = $menuItem->price + $menuItemExtras->price;
		$invoiceDetail->taxed        = 'on';
		$invoiceDetail->reserveid    = $reserve->reserveid;
		$invoiceDetail->menu_item_id = $menuItem->menu_item_id;
		$invoiceDetail->detail       = $menuItemExtras->detail;
		$invoiceDetail->sales_acct   = $menuItem->sales_acct;
		$invoiceDetail->tax_acct     = $menuItem->tax_acct;
#		echo '$invoiceDetail = ';
#		var_dump( $invoiceDetail );
		$invoiceDetail->save();
		
		if ( 1 != $reserve->status ) {
			$reserve->status = 1;
			$reserve->save();
		}
/*
*/
#		echo '</pre>';
	}
	else {
		$query = "SELECT * FROM menu_items WHERE menu_item_id = '$item'";
		$result = Treat_DB_ProxyOld::query( $query );

		$itemid = $item;
		$item = @mysql_result($result,0,"item_name");
		$price = @mysql_result($result,0,"price");
		$sales_acct = @mysql_result($result,0,"sales_acct");
		$tax_acct = @mysql_result($result,0,"tax_acct");
		$nontax_acct = @mysql_result($result,0,"nontax_acct");

		while ( $optionnum >= 0 ) {
			$option = \EE\Controller\Base::getPostVariable( $optionnum, '' );

			if ($option!=""){
				$query5 = "SELECT * FROM options WHERE optionid = '$option'";
				$result5 = Treat_DB_ProxyOld::query($query5);

				$optionprice = Treat_DB_ProxyOld::mysql_result( $result5, 0, 'price' );
				$optiondesc = Treat_DB_ProxyOld::mysql_result( $result5, 0, 'description' );
				$noprint = Treat_DB_ProxyOld::mysql_result( $result5, 0, 'noprint' );

				$price=$price+$optionprice;
				if ( $detail == "" && $noprint == 0 ) {
					$detail = $optiondesc;
				}
				elseif ( $noprint == 0 ) {
					$detail = "$detail, $optiondesc";
				}
			}
			else {
				$counting = 20;
				while ( $counting >= 0 )
				{
					$optionname = "$optionnum-$counting";
					$option = \EE\Controller\Base::getPostVariable( $optionname, '' );

					if ( $option != "" ) {
						$query5 = "SELECT * FROM options WHERE optionid = '$option'";
						$result5 = Treat_DB_ProxyOld::query($query5);

						$optionprice = Treat_DB_ProxyOld::mysql_result( $result5, 0, 'price' );
						$optiondesc = Treat_DB_ProxyOld::mysql_result( $result5, 0, 'description' );

						$price = $price + $optionprice;
						if ( $detail == "" ) {
							$detail = $optiondesc;
						}
						else {
							$detail = "$detail, $optiondesc";
						}
					}
					$counting--;
				}
			}
			$optionnum--;
		}

		if ( $detail == "" ) {
			$detail = ";$special";
		}
		elseif ( $special == "" ) {
		}
		else {
			$detail = "$detail; $special";
		}

		$query = "INSERT INTO invoicedetail (invoiceid,qty,item,price,taxed,reserveid,menu_item_id,detail,sales_acct,tax_acct) VALUES ('0','$qty','$item','$price','on','$reserveid','$itemid','$detail','$sales_acct','$tax_acct')";
		$result = Treat_DB_ProxyOld::query($query);

		$query="UPDATE reserve SET status = '1' WHERE reserveid = '$reserveid'";
		$result = Treat_DB_ProxyOld::query($query);
	}

}
google_page_track();

if ( $goto == 1 ) {
	$location = '/menu/intro.php?bid=' . $pageVariables->businessId;
}
else {
	$location = '/menu/menuitems.php?' . http_build_query( array(
		'bid' => $pageVariables->businessId,
		'curmenu' => $pageVariables->currentMenu,
		'reserveid' => $pageVariables->reserveId,
		'groupid' => $pageVariables->groupId,
		'qty' => $pageVariables->qty,
		'showname' => $pageVariables->showname,
	) );
}

if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
	echo sprintf( '<a href="%1$s">%1$s</a>', $location );
}
else {
	header( 'Location: ' . $location );
}

