<?php

function dayofweek($date1)
{
   $day=substr($date1,8,2);
   $month=substr($date1,5,2);
   $year=substr($date1,0,4);
   $dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
   return $dayofweek;
}

function money($diff){   
        $findme='.';
        $double='.00';
        $single='0';
        $double2='00';
        $pos1 = strpos($diff, $findme);
        $pos2 = strlen($diff);
        if ($pos1==""){$diff="$diff$double";}
        elseif ($pos2-$pos1==2){$diff="$diff$single";}
        elseif ($pos2-$pos1==1){$diff="$diff$double2";}
        else{}
        $dot = strpos($diff, $findme);
        $diff = substr($diff, 0, $dot+3);
        if ($diff > 0 && $diff < '0.01'){$diff="0.01";}
        elseif($diff < 0 && $diff > '-0.01'){$diff="-0.01";}
        return $diff;
}

function nextday($nextd,$day_format=''){ //Function returns next day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monn = substr($nextd, 5, 2);
   $dayn = substr($nextd, 8, 2);
   $yearn = substr($nextd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monn, $dayn+1, $yearn));
   return $tempdate;
}

function prevday($prevd,$day_format=''){ //Function returns previous day of passed date and formats accordingly.
   if($day_format==""){$day_format="Y-m-d";}
   $monp = substr($prevd, 5, 2);
   $dayp = substr($prevd, 8, 2);
   $yearp = substr($prevd, 0,4);
   $tempdate = date($day_format , mktime(0,0,0, $monp, $dayp-1, $yearp));
   return $tempdate;
}

function findsubs($reserveid,$accountid,$curmenu,$level,$businessid,$companyid){
    $style = "text-decoration:none";
    $count=$level*10;
    for($counter=1;$counter<=$count;$counter++){$indent.="&nbsp;";}

    $query = "SELECT * FROM reserve WHERE parent = '$reserveid' AND accountid = '$accountid' AND curmenu = '$curmenu' ORDER BY date DESC, start_hour DESC";
    $result = Treat_DB_ProxyOld::query($query, true);
    $num=mysql_numrows($result);

    $num--;
    while ($num>=0){
       $date=@mysql_result($result,$num,"date");
       $roomid=@mysql_result($result,$num,"roomid");
       $start_hour=@mysql_result($result,$num,"start_hour");
       $status=@mysql_result($result,$num,"status");
       $comment=@mysql_result($result,$num,"comment");
       $reserveid=@mysql_result($result,$num,"reserveid");

       $weekday=dayofweek($date);
       $weekday=substr($weekday,0,3);

       if (strlen($start_hour)==3){
          $showhour=substr($start_hour,0,1);
          $showmin=substr($start_hour,1,3);
          if ($showhour>=13){$showhour=$showhour-12;$showam="PM";}
          else {$showam="AM";}
       }
       elseif (strlen($start_hour)==2){
          $showhour="12";
          $showmin=$start_hour;
          $showam="AM";
       }
       elseif (strlen($start_hour)==1){
          $showhour="12";
          $showmin="0$start_hour";
          $showam="AM";
       }
       else {
          $showhour=substr($start_hour,0,2);
          $showmin=substr($start_hour,2,4);
          if ($showhour>12){$showhour=$showhour-12;$showam="PM";}
          elseif ($showhour==12){$showam="PM";}
          else {$showam="AM";}
       }

       if ($status==1){$showstatus="Pending";}
       elseif ($status==2 || $status == 5){$showstatus="Confirmed";}
       elseif ($status==3){$showstatus="Submitted";}
	   elseif ($status==4){$showstatus="Canceled";}

       if ($roomid!=0){
          $query2 = "SELECT roomname FROM rooms WHERE roomid = '$roomid'";
          $result2 = Treat_DB_ProxyOld::query($query2, true);

          $roomname=@mysql_result($result2,0,"roomname");
       }
       else {$roomname="N/A";}

       if ($status==3){$color="yellow";}
       elseif($status==1){$color="#E0E0E0";}
       else {$color="#FFFFFF";}

       //$create_sub="<a href=createsub.php?reserveid=$reserveid title='Create Sub Order' onclick=\"return confirm('Are you sure you want to create a sub order?');\"><img src=request.gif height=16 width=16 border=0></a>";
       $estimate="<a href=caterprint.php?reserveid=$reserveid&accountid=$accountid&bid=$businessid&cid=$companyid target='_blank' title='View Estimate'><img src=order.gif height=16 width=16 border=0></a>";

       echo "<tr bgcolor='$color' onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor='$color'><td>$indent&nbsp;<a style=$style href=caterdetails.php?reserveid=$reserveid&account=$accountid#detail><font color=blue>$comment</font></a>&nbsp;$create_sub $estimate</td><td>$reserveid</td><td>$date $weekday</td><td>$showhour:$showmin $showam</td><td>$roomname</td><td align=right>$showstatus&nbsp;</td></tr>";

       //////list sub orders
       //findsubs($reserveid,$accountid,$curmenu,$level+1,$businessid,$companyid);

       $num--;
    }
}

define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once(DOC_ROOT.'/bootstrap.php');

$style = "text-decoration:none";
$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";
$todayname = date("l");
$bbdayname=date("l");
$bbmonth=date("F");
$bbday=date("j");
$startclose="$year-$month-01";
$month++;
if ($month<10){$month="0$month";}
if ($month==13){$month="01";$year++;}
$endclose="$year-$month-31";

$book = isset($_GET['book'])?$_GET['book']:'';
$date7 = isset($_GET['date'])?$_GET['date']:'';

$user = \EE\Controller\Base::getSessionCookieVariable("usercook", '');
$pass = \EE\Controller\Base::getSessionCookieVariable("passcook", '');
$accountid = \EE\Controller\Base::getSessionCookieVariable("accountid", '');
$businessid = \EE\Controller\Base::getSessionCookieVariable("businessid", '');
$curmenu = \EE\Controller\Base::getSessionCookieVariable("curmenu", '');
$realuser = \EE\Controller\Base::getSessionCookieVariable("realuser", '');

////dates///////////////////
		$date1 = \EE\Controller\Base::getPostGetSessionOrCookieVariable("date1",'');
		$date2 = \EE\Controller\Base::getPostGetSessionOrCookieVariable("date2",'');

        if($date1 == "" || $date2 == ""){
            $date1=date("Y-m-d");
            $date2=$date1;
            while(dayofweek($date1)!="Monday"){$date1=prevday($date1);}
            while(dayofweek($date2)!="Sunday"){$date2=nextday($date2);}
        }
////////////////////////////

\EE\Controller\Base::setCookie("reserveid","");
\EE\Controller\Base::setCookie("date1",$date1);
\EE\Controller\Base::setCookie("date2",$date2);

if ($accountid<1){
    $location="logout.php?goto=3";
    header('Location: ./' . $location);
}
elseif ($businessid<1){
    $location="index.php";
    header('Location: ./' . $location);
}
else
{

	\EE\Model\Business\Setting::db( Treat_DB::singleton() );
	$settings = \EE\Model\Business\Setting::get("business_id = ". abs(intval($businessid)), true);
	$background_image_logo = $settings->background_image ?: '';
	$site_logo = $settings->site_logo ?: 'logo.jpg';

?>
<head>

<script language="JavaScript"
   type="text/JavaScript">
function popup_file(url)
  {
     popwin=window.open(url,"AttachFile","location=no,menubar=no,titlebar=no,resizeable=no,height=100,width=450");
  }
</script>

<script language="JavaScript"
   type="text/JavaScript">
function changePage(newLoc)
 {
   nextPage = newLoc.options[newLoc.selectedIndex].value

   if (nextPage != "")
   {
      document.location.href = nextPage
   }
 }
</script>

<SCRIPT LANGUAGE="JavaScript" SRC="CalendarPopup.js"></SCRIPT>

<!-- This prints out the default stylehseets used by the DIV style calendar.
     Only needed if you are using the DIV style popup -->
<SCRIPT LANGUAGE="JavaScript">document.write(getCalendarStyles());</SCRIPT>

<STYLE>
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation
			{
			background-color:#6677DD;
			text-align:center;
			vertical-align:center;
			text-decoration:none;
			color:#FFFFFF;
			font-weight:bold;
			}
	.TESTcpDayColumnHeader,
	.TESTcpYearNavigation,
	.TESTcpMonthNavigation,
	.TESTcpCurrentMonthDate,
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDate,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDate,
	.TESTcpCurrentDateDisabled,
	.TESTcpTodayText,
	.TESTcpTodayTextDisabled,
	.TESTcpText
			{
			font-family:arial;
			font-size:8pt;
			}
	TD.TESTcpDayColumnHeader
			{
			text-align:right;
			border:solid thin #6677DD;
			border-width:0 0 1 0;
			}
	.TESTcpCurrentMonthDate,
	.TESTcpOtherMonthDate,
	.TESTcpCurrentDate
			{
			text-align:right;
			text-decoration:none;
			}
	.TESTcpCurrentMonthDateDisabled,
	.TESTcpOtherMonthDateDisabled,
	.TESTcpCurrentDateDisabled
			{
			color:#D0D0D0;
			text-align:right;
			text-decoration:line-through;
			}
	.TESTcpCurrentMonthDate
			{
			color:#6677DD;
			font-weight:bold;
			}
	.TESTcpCurrentDate
			{
			color: #FFFFFF;
			font-weight:bold;
			}
	.TESTcpOtherMonthDate
			{
			color:#808080;
			}
	TD.TESTcpCurrentDate
			{
			color:#FFFFFF;
			background-color: #6677DD;
			border-width:1;
			border:solid thin #000000;
			}
	TD.TESTcpCurrentDateDisabled
			{
			border-width:1;
			border:solid thin #FFAAAA;
			}
	TD.TESTcpTodayText,
	TD.TESTcpTodayTextDisabled
			{
			border:solid thin #6677DD;
			border-width:1 0 0 0;
			}
	A.TESTcpTodayText,
	SPAN.TESTcpTodayTextDisabled
			{
			height:20px;
			}
	A.TESTcpTodayText
			{
			color:#6677DD;
			font-weight:bold;
			}
	SPAN.TESTcpTodayTextDisabled
			{
			color:#D0D0D0;
			}
	.TESTcpBorder
			{
			border:solid thin #6677DD;
			}
</STYLE>
</head>
<?php
    echo "<body background='{$background_image_logo}'>";
    echo "<div style=border:50px solid red;padding:10px;>";
    echo "<center><table cellspacing=0 cellpadding=0 border=0 width=90%><tr><td colspan=4><img src='{$site_logo}' width=205 height=43><p></td></tr>";

    $query = "SELECT * FROM company WHERE companyid = '1'";
    $result = Treat_DB_ProxyOld::query($query, true);

    $companyname=@mysql_result($result,0,"companyname");

    $query = "SELECT companyname,allow_sub_orders,companyid FROM business WHERE businessid = '$businessid'";
    $result = Treat_DB_ProxyOld::query($query, true);

    $businessname=@mysql_result($result,0,"companyname");
    $allow_sub_orders=@mysql_result($result,0,"allow_sub_orders");
    $companyid=@mysql_result($result,0,"companyid");

    $query = "SELECT * FROM accounts WHERE accountid = '$accountid'";
    $result = Treat_DB_ProxyOld::query($query, true);

    $accountname=@mysql_result($result,0,"name");
    $accountnum=@mysql_result($result,0,"accountnum");
    $mycostcenter=@mysql_result($result,0,"costcenter");

    $logout="<font color=white>Sign Off</font>";
    $showmyaccount="<a href=account_settings.php?goto=5 style=$style><FONT COLOR=#FFFFFF onMouseOver=this.style.color='#FF9900';this.style.cursor='hand' onMouseOut=this.style.color='#FFFFFF'><b>My Account</b></font></a>&nbsp;";

    if($realuser!=""){$showrealuser="<i> as $realuser</i>";}

	$query = "SELECT * FROM menu_type WHERE menu_typeid = $curmenu";
    $result = DB_Proxy::query($query);

    $menu_name=@mysql_result($result,0,"menu_typename");

    echo "<tr bgcolor=#6699FF><td colspan=2><font color=white size=3><b>The Right Choice... for a Healthier You!</td><td align=right colspan=2>$showmyaccount</td></tr>";
    echo "<tr bgcolor=#669933><td width=10%><img src=rc-header2.jpg height=80 width=108></td><td><font size=4 color=white><b>$companyname - $businessname</b></font><br><b><font color=white>Account: $accountname $showrealuser<br>Account #: $accountnum</td><td colspan=2 align=right valign=top><font color=white><b>$bbdayname, $bbmonth $bbday, $year</b><br><font size=2>[<a style=$style href=logout.php?goto=1><b><font color=white>$logout</font></b></a>]</font><p>$showcart</td></tr>";
    echo "<tr bgcolor=black><td height=1 colspan=4></td></tr></table></center>";

    echo "<center><table width=90% cellspacing=0 cellpadding=0>";
    echo "<tr><td colspan=2><font face=arial size=2><a href=menu.php style=$style><font color=blue face=arial size=2>$menu_name</font></a> :: <a href=catertrack.php style=$style><font color=blue face=arial size=2>Order History</font></a><font face=arial size=2> :: Schedule a Catering :: <a href=intro.php?bid=$businessid style=$style><font color=blue face=arial size=2>Home</font></a><p></td></tr>";
    echo "</table></center>";

    ////////////////////////////////////////////////////////////////
    /////filter for units with sub orders (dates and account groups)
    ////////////////////////////////////////////////////////////////
    if($allow_sub_orders == 1){

        ///prev week
        $prevdate1=$date1;
        while(dayofweek($prevdate1)!="Monday"){$prevdate1=prevday($prevdate1);}
        $prevdate2=$prevdate1;
        for($counter=1;$counter<=6;$counter++){$prevdate2=nextday($prevdate2);}

        for($counter=1;$counter<=7;$counter++){$prevdate1=prevday($prevdate1);$prevdate2=prevday($prevdate2);}

        ///next week
        $nextdate1=$date1;
        while(dayofweek($nextdate1)!="Monday"){$nextdate1=nextday($nextdate1);}
        $nextdate2=$nextdate1;
        for($counter=1;$counter<=6;$counter++){$nextdate2=nextday($nextdate2);}

        for($counter=1;$counter<=7;$counter++){$nextdate1=nextday($nextdate1);$nextdate2=nextday($nextdate2);}

        ////display filter
        echo "<center><table width=90% cellspacing=0 cellpadding=0>";
        echo "<tr><td>";
            echo "<form action=createorder.php?date1=$prevdate1&date2=$prevdate2 method=post style=\"margin:0;padding:0;display:inline;\"><input type=submit value='< Prev Week' style=\"border: 1px solid #666666; background-color:#E8E7E7;\"></form>&nbsp;";
            echo "<form action=createorder.php?date1=$nextdate1&date2=$nextdate2 method=post style=\"margin:0;padding:0;display:inline;\"><input type=submit value='Next Week >' style=\"border: 1px solid #666666; background-color:#E8E7E7;\"></form>&nbsp;";
            echo "<form action=createorder.php method=post style=\"margin:0;padding:0;display:inline;\">";
            echo "<SCRIPT LANGUAGE='JavaScript' ID='cust1'> var cal1 = new CalendarPopup('testdiv1');cal1.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME='date1' VALUE='$date1' SIZE=8> <A HREF=\"javascript:void(0);\" onClick=cal1.select(document.forms[2].date1,'anchor1','yyyy-MM-dd'); return false; TITLE=cal1.select(document.forms[2].date1,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor1' ID='anchor1'><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A> to ";
            echo "<SCRIPT LANGUAGE='JavaScript' ID='cust2'> var cal2 = new CalendarPopup('testdiv1');cal2.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME='date2' VALUE='$date2' SIZE=8> <A HREF=\"javascript:void(0);\" onClick=cal2.select(document.forms[2].date2,'anchor2','yyyy-MM-dd'); return false; TITLE=cal2.select(document.forms[2].date2,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor2' ID='anchor2'><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A>&nbsp;";
            echo "<input type=submit value='GO' style=\"border: 1px solid #666666; background-color:#E8E7E7;\"></form>&nbsp;";
        echo "</td><td align=right>";

        /////account groups
        $query = "SELECT groupid FROM accounts_groupdetail WHERE accountid = '$accountid'";
        $result = Treat_DB_ProxyOld::query($query, true);
        $num = mysql_numrows($result);

        if($num>0){
            echo "<form action=placeaccount.php method=post style=\"margin:0;padding:0;display:inline;\"><select name=aid onChange='changePage(this.form.aid)'>";

            while($r=mysql_fetch_array($result)){
                $groupid = $r["groupid"];

                $query2 = "SELECT groupname FROM accounts_group WHERE groupid = '$groupid'";
                $result2 = Treat_DB_ProxyOld::query($query2, true);

                $groupname = @mysql_result($result2,0,"groupname");

                echo "<optgroup label='$groupname'>";

                ////list accounts
                $query2 = "SELECT accountid FROM accounts_groupdetail WHERE groupid = '$groupid'";
                $result2 = Treat_DB_ProxyOld::query($query2, true);

                while($r2=mysql_fetch_array($result2)){

                    $newaccountid = $r2["accountid"];

                    $query3 = "SELECT name FROM accounts WHERE accountid = '$newaccountid'";
                    $result3 = Treat_DB_ProxyOld::query($query3, true);

                    $newaccountname = @mysql_result($result3,0,"name");

                    if($accountid==$newaccountid){$sel="SELECTED";}
                    else{$sel="";}

                    echo "<option value=placeaccount.php?aid=$newaccountid $sel>$newaccountname</option>";
                }
                echo "</optgroup>";
            }
            echo "</select></form>";
            $addform=1;
        }
        echo "</td></tr></table></center><p>";
    }
    ////////////////////////////////////////////////////////////////
    ///////////////////end filter///////////////////////////////////
    ////////////////////////////////////////////////////////////////
 
    echo "<center><table width=90% cellspacing=0 cellpadding=0 style=\"border: 2px solid #999999;\">";

    if($allow_sub_orders > 0){$query = "SELECT * FROM reserve WHERE parent = '0' AND accountid = '$accountid' AND curmenu = '$curmenu' AND date >= '$date1' AND date <= '$date2' ORDER BY date DESC";}
    else{$query = "SELECT * FROM reserve WHERE parent = '0' AND accountid = '$accountid' AND curmenu = '$curmenu' ORDER BY date DESC";}
    $result = Treat_DB_ProxyOld::query($query, true);
    $num=mysql_numrows($result);

    echo "<tr bgcolor=#999999><td><a name='cater'><font color=white><b>Currently Scheduled Caterings</b></font></a></td><td><font color=white><b>Order#</b></font></td><td><font color=white><b>Date</b></font></td><td><font color=white><b>Time</b></font></td><td><font color=white><b>Location</b></font></td><td align=right><font color=white><b>Status</b></font></td></tr>";

if ($num!=0){

    $num--;
    while ($num>=0){
       $date=@mysql_result($result,$num,"date");
       $roomid=@mysql_result($result,$num,"roomid");
       $start_hour=@mysql_result($result,$num,"start_hour");
       $status=@mysql_result($result,$num,"status");
       $comment=@mysql_result($result,$num,"comment");
       $reserveid=@mysql_result($result,$num,"reserveid");

       $weekday=dayofweek($date);
       $weekday=substr($weekday,0,3);

       if (strlen($start_hour)==3){
          $showhour=substr($start_hour,0,1);
          $showmin=substr($start_hour,1,3);
          if ($showhour>=13){$showhour=$showhour-12;$showam="PM";}
          else {$showam="AM";}
       }
       elseif (strlen($start_hour)==2){
          $showhour="12";
          $showmin=$start_hour;
          $showam="AM";
       }
       elseif (strlen($start_hour)==1){
          $showhour="12";
          $showmin="0$start_hour";
          $showam="AM";
       }
       else {
          $showhour=substr($start_hour,0,2);
          $showmin=substr($start_hour,2,4);
          if ($showhour>12){$showhour=$showhour-12;$showam="PM";}
          elseif ($showhour==12){$showam="PM";}
          else {$showam="AM";}
       }

       if ($status==1){$showstatus="Pending";}
       elseif ($status==2 || $status == 5){$showstatus="Confirmed";}
       elseif ($status==3){$showstatus="Submitted";}
       elseif ($status==4){$showstatus="Canceled";}

       if ($roomid!=0){
          $query2 = "SELECT roomname FROM rooms WHERE roomid = '$roomid'";
          $result2 = Treat_DB_ProxyOld::query($query2, true);

          $roomname=@mysql_result($result2,0,"roomname");
       }
       else {$roomname="N/A";}

       if ($status==3){$color="yellow";}
       elseif($status==1){$color="#E0E0E0";}
       else {$color="#FFFFFF";}

       if($allow_sub_orders==1 && $status==1){
           $create_sub="<a href=createsub.php?reserveid=$reserveid title='Create Sub Order' onclick=\"return confirm('Are you sure you want to create a sub order?');\"><img src=request.gif height=16 width=16 border=0></a>";
       }
       else{$create_sub="";}

       $estimate="<a href=caterprint.php?reserveid=$reserveid&accountid=$accountid&bid=$businessid&cid=$companyid target='_blank' title='View Estimate'><img src=order.gif height=16 width=16 border=0></a>";

       echo "<tr bgcolor='$color' onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor='$color'><td>&nbsp;<a style=$style href=caterdetails.php?reserveid=$reserveid&account=$accountid#detail><font color=blue>$comment</font></a>&nbsp;$create_sub $estimate</td><td>$reserveid</td><td>$date $weekday</td><td>$showhour:$showmin $showam</td><td>$roomname</td><td align=right>$showstatus&nbsp;</td></tr>";

       //////list sub orders
       if($allow_sub_orders==1){findsubs($reserveid,$accountid,$curmenu,1,$businessid,$companyid);}

       echo "<tr bgcolor=#999999><td height=1 colspan=6></td></tr>";

       $num--;
    }
}
else{
       echo "<tr bgcolor=white><td colspan=6><i>There are no scheduled orders.</i></td></tr>";
       echo "<tr bgcolor=#CCCCCC><td height=1 colspan=6></td></tr>";
}
 
    echo "</table></center>";

    $query = "SELECT * FROM business WHERE businessid = '$businessid'";
    $result = Treat_DB_ProxyOld::query($query, true);

    $caterdays=@mysql_result($result,0,"caterdays");
    $weekend=@mysql_result($result,0,"weekend");

    $query = "SELECT * FROM caterclose WHERE businessid = '$businessid' AND date >= '$startclose' AND date <= '$endclose'";
    $result = Treat_DB_ProxyOld::query($query, true);
    $num=mysql_numrows($result);
    
    $num--;
    $closedmessage="";
    while ($num>=0){
       $dateclose=@mysql_result($result,$num,"date");
       $closedmessage="$closedmessage cal18.addDisabledDates('$dateclose');";
       $num--;
    }

    echo "<p><center><table width=90%  cellpadding=0 cellspacing=0 style='filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr=#F7F7F7, startColorstr=#E8E7E7, gradientType=1);'>";
    echo "<tr><td width=1% height=1%><img src='dgrayur.gif' width=20 length=20></td><td colspan=2></td><td width=1% height=1%><img src=lgrayur.gif height=20 width=20></td></tr>";
    echo "<tr><td></td><td colspan=2><img src=catering.gif height=32 width=260></td><td></td></tr>";
    if ($book==1){echo "<tr><td></td><td colspan=2><b><font color=red>That room is already booked.</td><td></td></tr>";} 
    elseif ($book==2){echo "<tr><td></td><td colspan=2><b><font color=red>Please specify the number of people.</td><td></td></tr>";} 
    elseif ($book==3){echo "<tr><td></td><td colspan=2><b><font color=red>Your End Time is before your Start Time.</td><td></td></tr>";} 
    elseif ($book==4){echo "<tr><td></td><td colspan=2><b><font color=red>That date has already past.</td><td></td></tr>";}   
    elseif ($book==6){echo "<tr><td></td><td colspan=2><b><font color=blue>Catering Successfully Scheduled.</td><td></td></tr>";}
    elseif ($book==7){echo "<tr><td></td><td colspan=2><b><font color=red>Please enter a Reference.</td><td></td></tr>";} 
    elseif ($book==8){echo "<tr><td></td><td colspan=2><b><font color=red>That Day is already Full.</td><td></td></tr>";}
    elseif ($book==9){echo "<tr><td></td><td colspan=2><b><font color=red>You must schedule at least $caterdays day(s) in advance.</td><td></td></tr>";} 
    elseif ($book==10){echo "<tr><td></td><td colspan=2><b><font color=red>Sorry, we are closed weekends.</td><td></td></tr>";}  
    elseif ($book==11){echo "<tr><td></td><td colspan=2><b><font color=red>Sorry, we are closed that day.</td><td></td></tr>";} 
	elseif ($book==12){echo "<tr><td></td><td colspan=2><b><font color=red>A Cost Center is Required.</td><td></td></tr>";} 
    elseif ($book==5){echo "<tr><td></td><td colspan=2><b><font color=red>Please specify why location is N/A.</td><td></td></tr>";}           

    if ($date7!=""){$today=$date7;}
    if ($weekend==0){$showweekend="cal18.setDisabledWeekDays(0,6)";}
    else{$showweekend="";}

    $comment = isset($_GET['comment'])?$_GET['comment']:'';
    $roomcomment = isset($_GET['roomcomment'])?$_GET['roomcomment']:'';
    $people = isset($_GET['people'])?$_GET['people']:'';
    $start_hour = isset($_GET['start_hour'])?$_GET['start_hour']:'';
    $end_hour = isset($_GET['end_hour'])?$_GET['end_hour']:'';
    $sel_room = isset($_GET['roomid'])?$_GET['roomid']:'';
    $costcenter = isset($_GET['costcenter'])?$_GET['costcenter']:'';
    if ($costcenter==""){$costcenter=$mycostcenter;}


    if($allow_sub_orders == 1){$formcount=3+$addform;}
    else{$formcount=0;}

    echo "<tr><td></td><td align=right width=20%><FORM ACTION=addreserve.php method=post>Date: </td><td> <SCRIPT LANGUAGE='JavaScript' ID='js18'> var now = new Date(); var cal18 = new CalendarPopup('testdiv1'); cal18.addDisabledDates(null,formatDate(now,'yyyy-MM-dd')); $closedmessage $showweekend; cal18.setCssPrefix('TEST');</SCRIPT><A HREF='javascript:void()' onClick=cal18.select(document.forms[$formcount].date,'anchor18','yyyy-MM-dd'); return false; TITLE=cal18.select(document.forms[$formcount].date,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor18' ID='anchor18'><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A> <INPUT TYPE=text NAME='date' VALUE=$today SIZE=8></td><td></td></tr>";
    echo "<tr><td></td><td align=right>Reference: </td><td><INPUT TYPE=text name=comment size=30 value='$comment'></td><td></td></tr>";
    echo "<tr><td></td><td align=right># of People: </td><td><INPUT TYPE=text name=peoplenum size=8 value='$people'></td><td></td></tr>";
    
    $query = "SELECT * FROM rooms WHERE businessid = '$businessid' AND deleted = '0' ORDER BY roomname DESC";
    $result = Treat_DB_ProxyOld::query($query, true);
    $num=mysql_numrows($result);
   
    echo "<tr><td></td><td align=right>Location: </td><td><select name=room><option value=0>N/A</option>";
    $num--;
    while ($num>=0){
       $roomname=@mysql_result($result,$num,"roomname");
       $roomid=@mysql_result($result,$num,"roomid");
       if ($roomid==$sel_room){$selroom="SELECTED";}
       else {$selroom="";}
       echo "<option value=$roomid $selroom>$roomname</option>";
       $num--;
    } 
    echo "</select></td><td></td></tr>"; 

    echo "<tr><td></td><td></td><td><i>*Please include any other details below. If location is N/A, please specify.<br><textarea name=roomcomment rows=3 cols=60>$roomcomment</textarea></td><td></td></tr>";

    $start_min=substr($start_hour,2,2);
    $end_min=substr($end_hour,2,2);
    $start_hour=substr($start_hour,0,2);
    $end_hour=substr($end_hour,0,2);


    if ($start_hour=="01"){$start1="SELECTED";$startam="SELECTED";}
    elseif ($start_hour=="02"){$start2="SELECTED";$startam="SELECTED";}
    elseif ($start_hour=="03"){$start3="SELECTED";$startam="SELECTED";}
    elseif ($start_hour=="04"){$start4="SELECTED";$startam="SELECTED";}
    elseif ($start_hour=="05"){$start5="SELECTED";$startam="SELECTED";}
    elseif ($start_hour=="06"){$start6="SELECTED";$startam="SELECTED";}
    elseif ($start_hour=="07"){$start7="SELECTED";$startam="SELECTED";}
    elseif ($start_hour=="08"){$start8="SELECTED";$startam="SELECTED";}
    elseif ($start_hour=="09"){$start9="SELECTED";$startam="SELECTED";}
    elseif ($start_hour=="10"){$start10="SELECTED";$startam="SELECTED";}
    elseif ($start_hour=="11"){$start11="SELECTED";$startam="SELECTED";}
    elseif ($start_hour=="12"){$start12="SELECTED";$startpm="SELECTED";}
    elseif ($start_hour=="13"){$start1="SELECTED";$startpm="SELECTED";}
    elseif ($start_hour=="14"){$start2="SELECTED";$startpm="SELECTED";}
    elseif ($start_hour=="15"){$start3="SELECTED";$startpm="SELECTED";}
    elseif ($start_hour=="16"){$start4="SELECTED";$startpm="SELECTED";}
    elseif ($start_hour=="17"){$start5="SELECTED";$startpm="SELECTED";}
    elseif ($start_hour=="18"){$start6="SELECTED";$startpm="SELECTED";}
    elseif ($start_hour=="19"){$start7="SELECTED";$startpm="SELECTED";}
    elseif ($start_hour=="20"){$start8="SELECTED";$startpm="SELECTED";}
    elseif ($start_hour=="21"){$start9="SELECTED";$startpm="SELECTED";}
    elseif ($start_hour=="22"){$start10="SELECTED";$startpm="SELECTED";}
    elseif ($start_hour=="23"){$start11="SELECTED";$startpm="SELECTED";}
    else {$start10="SELECTED";$startam="SELECTED";}

    if ($end_hour=="01"){$end1="SELECTED";$endam="SELECTED";}
    elseif ($end_hour=="02"){$end2="SELECTED";$endam="SELECTED";}
    elseif ($end_hour=="03"){$end3="SELECTED";$endam="SELECTED";}
    elseif ($end_hour=="04"){$end4="SELECTED";$endam="SELECTED";}
    elseif ($end_hour=="05"){$end5="SELECTED";$endam="SELECTED";}
    elseif ($end_hour=="06"){$end6="SELECTED";$endam="SELECTED";}
    elseif ($end_hour=="07"){$end7="SELECTED";$endam="SELECTED";}
    elseif ($end_hour=="08"){$end8="SELECTED";$endam="SELECTED";}
    elseif ($end_hour=="09"){$end9="SELECTED";$endam="SELECTED";}
    elseif ($end_hour=="10"){$end10="SELECTED";$endam="SELECTED";}
    elseif ($end_hour=="11"){$end11="SELECTED";$endam="SELECTED";}
    elseif ($end_hour=="12"){$end12="SELECTED";$endpm="SELECTED";}
    elseif ($end_hour=="13"){$end1="SELECTED";$endpm="SELECTED";}
    elseif ($end_hour=="14"){$end2="SELECTED";$endpm="SELECTED";}
    elseif ($end_hour=="15"){$end3="SELECTED";$endpm="SELECTED";}
    elseif ($end_hour=="16"){$end4="SELECTED";$endpm="SELECTED";}
    elseif ($end_hour=="17"){$end5="SELECTED";$endpm="SELECTED";}
    elseif ($end_hour=="18"){$end6="SELECTED";$endpm="SELECTED";}
    elseif ($end_hour=="19"){$end7="SELECTED";$endpm="SELECTED";}
    elseif ($end_hour=="20"){$end8="SELECTED";$endpm="SELECTED";}
    elseif ($end_hour=="21"){$end9="SELECTED";$endpm="SELECTED";}
    elseif ($end_hour=="22"){$end10="SELECTED";$endpm="SELECTED";}
    elseif ($end_hour=="23"){$end11="SELECTED";$endpm="SELECTED";}
    else {$end11="SELECTED";$endam="SELECTED";}

    if ($start_min=="00"){$startmin1="SELECTED";}
    elseif ($start_min=="05"){$startmin2="SELECTED";}
    elseif ($start_min=="10"){$startmin3="SELECTED";}
    elseif ($start_min=="15"){$startmin4="SELECTED";}
    elseif ($start_min=="20"){$startmin5="SELECTED";}
    elseif ($start_min=="25"){$startmin6="SELECTED";}
    elseif ($start_min=="30"){$startmin7="SELECTED";}
    elseif ($start_min=="35"){$startmin8="SELECTED";}
    elseif ($start_min=="40"){$startmin9="SELECTED";}
    elseif ($start_min=="45"){$startmin10="SELECTED";}
    elseif ($start_min=="50"){$startmin11="SELECTED";}
    elseif ($start_min=="55"){$startmin12="SELECTED";}

    if ($end_min=="00"){$endmin1="SELECTED";}
    elseif ($end_min=="05"){$endmin2="SELECTED";}
    elseif ($end_min=="10"){$endmin3="SELECTED";}
    elseif ($end_min=="15"){$endmin4="SELECTED";}
    elseif ($end_min=="20"){$endmin5="SELECTED";}
    elseif ($end_min=="25"){$endmin6="SELECTED";}
    elseif ($end_min=="30"){$endmin7="SELECTED";}
    elseif ($end_min=="35"){$endmin8="SELECTED";}
    elseif ($end_min=="40"){$endmin9="SELECTED";}
    elseif ($end_min=="45"){$endmin10="SELECTED";}
    elseif ($end_min=="50"){$endmin11="SELECTED";}
    elseif ($end_min=="55"){$endmin12="SELECTED";}

    echo "<tr><td></td><td align=right>Start Time:</td><td><select NAME=start_hour><OPTION VALUE='01' $start1>1</OPTION><OPTION VALUE='02' $start2>2</OPTION><OPTION VALUE='03' $start3>3</OPTION><OPTION VALUE='04' $start4>4</OPTION><OPTION VALUE='05' $start5>5</OPTION><OPTION VALUE='06' $start6>6</OPTION><OPTION VALUE='07' $start7>7</OPTION><OPTION VALUE='08' $start8>8</OPTION><OPTION VALUE='09' $start9>9</OPTION><OPTION VALUE='10' $start10>10</OPTION><OPTION VALUE='11' $start11>11</OPTION><OPTION VALUE='12' $start12>12</OPTION></SELECT><b>:</b><select NAME=start_min><OPTION VALUE='00' $startmin1>00</OPTION><OPTION VALUE='05' $startmin2>05</OPTION><OPTION VALUE='10' $startmin3>10</OPTION><OPTION VALUE='15' $startmin4>15</OPTION><OPTION VALUE='20' $startmin5>20</OPTION><OPTION VALUE='25' $startmin6>25</OPTION><OPTION VALUE='30' $startmin7>30</OPTION><OPTION VALUE='35' $startmin8>35</OPTION><OPTION VALUE='40' $startmin9>40</OPTION><OPTION VALUE='45' $startmin10>45</OPTION><OPTION VALUE='50' $startmin11>50</OPTION><OPTION VALUE='55' $startmin12>55</OPTION></SELECT> <select NAME=start_am><OPTION VALUE='AM' $startam>AM</OPTION><OPTION VALUE='PM' $startpm>PM</OPTION></SELECT></td><td></td></tr>";
    echo "<tr><td></td><td align=right>End Time:</td><td><select NAME=end_hour><OPTION VALUE='01' $end1>1</OPTION><OPTION VALUE='02' $end2>2</OPTION><OPTION VALUE='03' $end3>3</OPTION><OPTION VALUE='04' $end4>4</OPTION><OPTION VALUE='05' $end5>5</OPTION><OPTION VALUE='06' $end6>6</OPTION><OPTION VALUE='07' $end7>7</OPTION><OPTION VALUE='08' $end8>8</OPTION><OPTION VALUE='09' $end9>9</OPTION><OPTION VALUE='10' $end10>10</OPTION><OPTION VALUE='11' $end11>11</OPTION><OPTION VALUE='12' $end12>12</OPTION></SELECT><b>:</b><select NAME=end_min><OPTION VALUE='00' $endmin1>00</OPTION><OPTION VALUE='05' $endmin2>05</OPTION><OPTION VALUE='10' $endmin3>10</OPTION><OPTION VALUE='15' $endmin4>15</OPTION><OPTION VALUE='20' $endmin5>20</OPTION><OPTION VALUE='25' $endmin6>25</OPTION><OPTION VALUE='30' $endmin7>30</OPTION><OPTION VALUE='35' $endmin8>35</OPTION><OPTION VALUE='40' $endmin9>40</OPTION><OPTION VALUE='45' $endmin10>45</OPTION><OPTION VALUE='50' $endmin11>50</OPTION><OPTION VALUE='55' $endmin12>55</OPTION></SELECT> <select NAME=end_am><OPTION VALUE='AM' $endam>AM</OPTION><OPTION VALUE='PM' $endpm>PM</OPTION></SELECT></td><td></td></tr>";
    
    ////////COST CENTER
    $query43 = "SELECT * FROM customer_costcenter WHERE businessid = '$businessid' ORDER BY name DESC";
    $result43 = Treat_DB_ProxyOld::query($query43, true);
    $num43=mysql_numrows($result43);

    $num43--;

    if($num43>0){
       echo "<tr><td></td><td align=right>Cost Center:</td><td><select name=costcenter><option value=></option>";
       while($num43>=0){
          $cc_name=@mysql_result($result43,$num43,"name");
          $cc_number=@mysql_result($result43,$num43,"number");

          echo "<option value='$cc_number'>$cc_name ($cc_number)</option>";

          $num43--;
       }
       echo "</select></td><td></td></tr>";
    }
    else{
       echo "<tr><td></td><td align=right>Cost Center:</td><td><INPUT TYPE=text name=costcenter size=18 value='$costcenter'></td><td></td></tr>";
    }

         ///////////////////////////////
         ///////custom fields///////////
         ///////////////////////////////
         $query_custom = "SELECT * FROM invoice_custom_fields WHERE businessid = '$businessid' AND active = '0' AND customer = '1'";
         $result_custom = Treat_DB_ProxyOld::query($query_custom, true);
         $num_custom = mysql_num_rows($result_custom);

         if($num_custom>0){

             while($r_cust=mysql_fetch_array($result_custom)){
                 $cust_id=$r_cust["id"];
                 $cust_field_name=$r_cust["field_name"];
                 $cust_type=$r_cust["type"];
                 $cust_required=$r_cust["required"];

                 if($cust_required==1){$showrequired="<font color=red>*</font>";}
                 else{$showrequired="";}

                 /////display text
                 if($cust_type==0){
                     $query_custom2 = "SELECT value FROM invoice_custom_values WHERE reserveid = '$reserveid' AND customid = '$cust_id'";
                     $result_custom2 = Treat_DB_ProxyOld::query($query_custom2, true);

                     //$cust_value = @mysql_result($result_custom2,0,"value");

                     echo "<tr><td></td><td align=right><font color=#666666>$cust_field_name</font>: </td><td> <input type=text size=20 name='cust$cust_id' value=''></td><td></td></tr>";
                 }
                 ////display select
                 elseif($cust_type==1){
                     $query_custom2 = "SELECT value FROM invoice_custom_values WHERE reserveid = '$reserveid' AND customid = '$cust_id'";
                     $result_custom2 = Treat_DB_ProxyOld::query($query_custom2, true);

                     //$cust_value = @mysql_result($result_custom2,0,"value");

                     echo "<tr><td></td><td align=right><font color=#666666>$cust_field_name</font>: </td><td> <select name='cust$cust_id'>";

                     $query_custom2 = "SELECT value FROM invoice_custom_fields_select WHERE customid = '$cust_id'";
                     $result_custom2 = Treat_DB_ProxyOld::query($query_custom2, true);

                     while($r_sel=mysql_fetch_array($result_custom2)){
                        $cust_value_sel = $r_sel["value"];
                        //if($cust_value_sel == $cust_value){$sel = "SELECTED";}
                        //else{$sel="";}
                        echo "<option value='$cust_value_sel'>$cust_value_sel</option>";
                     }

                     echo "</select></td><td></td></tr>";
                 }
                 //////display date
                 elseif($cust_type==2){
                     $query_custom2 = "SELECT value FROM invoice_custom_values WHERE reserveid = '$reserveid' AND customid = '$cust_id'";
                     $result_custom2 = Treat_DB_ProxyOld::query($query_custom2, true);

                     //$cust_value = @mysql_result($result_custom2,0,"value");

                     $calcount=$cust_id+99;

                     echo "<tr><td></td><td align=right><font color=#666666>$cust_field_name</font>: </td><td> <SCRIPT LANGUAGE='JavaScript' ID='cust$calcount'> var cal$calcount = new CalendarPopup('testdiv1');cal$calcount.setCssPrefix('TEST');</SCRIPT><INPUT TYPE=text NAME='cust$cust_id' VALUE='' SIZE=8> <A HREF=\"javascript:void(0);\" onClick=cal$calcount.select(document.forms[$formcount].cust$cust_id,'anchor$calcount','yyyy-MM-dd'); return false; TITLE=cal$calcount.select(document.forms[$formcount].cust$cust_id,'anchor1x','yyyy-MM-dd'); return false; NAME='anchor$calcount' ID='anchor$calcount'><img src=calendar.gif border=0 height=15 width=16 alt='Choose a Date'></A></td><td></td></tr>";
                 }
	             /////display text
	             else if($cust_type==5){
		             $query_custom2 = "SELECT value FROM invoice_custom_values WHERE reserveid = '$reserveid' AND customid = '$cust_id'";
		             $result_custom2 = Treat_DB_ProxyOld::query($query_custom2, true);

		             //$cust_value = @mysql_result($result_custom2,0,"value");

		             echo "<tr><td></td><td align=right><font color=#666666>$cust_field_name</font>: </td><td> <textarea name='cust$cust_id' style='width: 50%; height: 150px;'></textarea></td><td></td></tr>";
	             }

             }
         }
         //////////////////////////////
         //////end custom fields///////
         //////////////////////////////

    echo "<tr><td></td><td></td><td><br><p><INPUT TYPE=submit VALUE='Create Order'></td><td></td></tr>";
    echo "<tr><td><img src='dgrayll.gif' width=20 length=20></td><td colspan=2></td><td><img src=lgraylr.gif height=20 width=20></td></tr>";
    echo "</table></center>";
    echo "<DIV ID=testdiv1 STYLE=position:absolute;visibility:hidden;background-color:white;layer-background-color:white;></DIV>";
    echo "</body>";
}
mysql_close();
google_page_track();
?>