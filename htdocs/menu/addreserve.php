<?php

function dayofweek($date1){
	$day=substr($date1,8,2);
	$month=substr($date1,5,2);
	$year=substr($date1,0,4);
	$dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));

	return $dayofweek;

}

function money($diff){
	
	$findme='.';
	$double='.00';
	$single='0';
	$double2='00';
	$pos1 = strpos($diff, $findme);
	$pos2 = strlen($diff);
	
	if ($pos1==""){
		$diff="$diff$double";
	} elseif ($pos2-$pos1==2){
		$diff="$diff$single";
	} elseif ($pos2-$pos1==1){
		$diff="$diff$double2";
	} else{
		
	}
	
	$dot = strpos($diff, $findme);
	$diff = substr($diff, 0, $dot+3);
	
	if ($diff > 0 && $diff < '0.01'){
		$diff="0.01";
	} elseif($diff < 0 && $diff > '-0.01'){
		$diff="-0.01";
	}
	
	return $diff;
	
}

function nextday($date2){
	
	$day=substr($date2,8,2);
	$month=substr($date2,5,2);
	$year=substr($date2,0,4);
	$leap = date("L");

	if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10')){
		
		if ($month == "01"){
			$month="02";
		} elseif ($month == "03"){
			$month="04";
		} elseif ($month == "05"){
			$month="06";
		} elseif ($month == "07"){
			$month="08";
		} elseif ($month == "08"){
			$month="09";
		} elseif ($month == "10"){
			$month="11";
		}
		
		$day='01';
		$tomorrow = "$year-$month-$day";
		
		return $tomorrow;
		
	} else if ($month=='02' && $day == '29' && $leap == '1'){
		
		$month='03';
		$day='01';
		$tomorrow = "$year-$month-$day";
		
		return $tomorrow;
		
	} else if ($month=='02' && $day == '28' && $leap == '0'){
		
		$month='03';
		$day='01';
		$tomorrow = "$year-$month-$day";
		
		return $tomorrow;
		
	} else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11')){
		
		if ($month == "04"){
			$month="05";
		}

		if ($month == "06"){
			$month="07";
		}
		
		if ($month == "09"){
			$month="10";
		}
		
		if ($month == "11"){
			$month="12";
		}
		
		$day='01';
		$tomorrow = "$year-$month-$day";
		
		return $tomorrow;
		
	} else if ($month=='12' && $day=='31'){
		
		$day='01';
		$month='01';
		$year++;
		$tomorrow = "$year-$month-$day";
		
		return $tomorrow;
		
	} else {
		
		$day=$day+1;
		
		if ($day<10){
			$day="0$day";
		}
		
		$tomorrow="$year-$month-$day";
		
		return $tomorrow;
	}
}

function prevday($date2){
	
	$day=substr($date2,8,2);
	$month=substr($date2,5,2);
	$year=substr($date2,0,4);
	$day=$day-1;
	$leap = date("L");

	if ($day <= 0){
		
		if ($month == 01){
			
			$month = '12';
			$year--;
			$day=$day+31;
			$yesterday = "$year-$month-$day";
			
			return $yesterday;
			
		} else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11'){
			
			$month--;
			
			if ($month < 10){
				$month="0$month";
			}
			
			$day=$day+31;
			$yesterday="$year-$month-$day";
			
			return $yesterday;
			
		} else if ($month=='05' || $month=='07' || $month=='10' || $month=='12'){
			
			$month--;
			
			if ($month < 10){
				$month="0$month";
			}
			
			$day=$day+30;
			$yesterday="$year-$month-$day";
			
			return $yesterday;
			
		} elseif ($leap==1&&$month=='03'){
			
			$day=$day+29;
			$month='02';
			$yesterday="$year-$month-$day";
			
			return $yesterday;
			
		} else {
			
			$day=$day+28;
			$month='02';
			$yesterday="$year-$month-$day";
			
			return $yesterday;
			
		}
	} else {
		
		if ($day < 10){
			$day="0$day";
		}
		
		$yesterday="$year-$month-$day";
		
		return $yesterday;
		
	}
}

function futureday($num) {
	
	$day = date("d");
	$year = date("Y");
	$month = date("m");
	$leap = date("L");
	$day=$day+$num;

	if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32'){

		if ($month == "03"){
			$month="04";
		}

		if ($month == "05"){
			$month="06";
		}
		
		if ($month == "07"){
			$month="08";
		}
		
		if ($month == "08"){
			$month="09";
		}
		
		$day=$day-31;
		
		if ($day<10){
			$day="0$day";
		}
		
		$tomorrow = "$year-$month-$day";
		
		return $tomorrow;
		
	} else if ($month=='02' && $leap == '1' && $day >= '29'){
		
		$month='03';
		$day=$day-29;
		
		if ($day<10){
			$day="0$day";
		}
		
		$tomorrow = "$year-$month-$day";
		
		return $tomorrow;
		
	} else if ($month=='02' && $leap == '0' && $day >= '28'){
		
		$month='03';
		$day=$day-28;
		
		if ($day<10){
			$day="0$day";
		}
		
		$tomorrow = "$year-$month-$day";
		
		return $tomorrow;
		
	} else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31'){

		if ($month == "04"){
			$month="05";
		}
		
		if ($month == "06"){
			$month="07";
		}
		
		if ($month == "09"){
			$month="10";
		}
		
		if ($month == "11"){
			$month="12";
		}
		
		$day=$day-30;

		if ($day<10){
			$day="0$day";
		}
		
		$tomorrow = "$year-$month-$day";
		
		return $tomorrow;
		
	} else if ($month==12 && $day>=32){
		
		$day=$day-31;

		if ($day<10){
			$day="0$day";
		}
		
		$month='01';
		$year++;
		$tomorrow = "$year-$month-$day";
		
		return $tomorrow;
		
	} else {
		
		if ($day<10){
			$day="0$day";
		}
		
		$tomorrow="$year-$month-$day";
		
		return $tomorrow;
	}
}

function pastday($num) {
	
	$day = date("d");
	$day=$day-$num;
	
	if ($day <= 0){
		
		$year = date("Y");
		$month = date("m");
		if ($month == 01){

			$month = '12';
			$year--;
			$day=$day+31;
			$yesterday = "$year-$month-$day";

			return $yesterday;
		
		} else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11'){
			
			$month--;
			
			if ($month < 10){
				$month="0$month";
			}
			
			$day=$day+31;
			$yesterday="$year-$month-$day";
			
			return $yesterday;
			
		} else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12'){

			$month--;
			
			if ($month < 10){
				$month="0$month";
			}
			
			$day=$day+30;
			$yesterday="$year-$month-$day";
			
			return $yesterday;
			
		} else {

			$day=$day+28;
			$month='02';
			$yesterday="$year-$month-$day";

			return $yesterday;
		
		}
	} else {
		if ($day < 10){
			$day="0$day";
		}
		
		$year=date("Y");
		$month=date("m");
		$yesterday="$year-$month-$day";

		return $yesterday;
	
	}
}

define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
require_once(DOC_ROOT.'/bootstrap.php');

$day = date("d");
$year = date("Y");
$month = date("m");
$today="$year-$month-$day";

$accountid = isset($_COOKIE["accountid"])?$_COOKIE["accountid"]:'';
$businessid = isset($_COOKIE["businessid"])?$_COOKIE["businessid"]:'';
$curmenu = isset($_COOKIE["curmenu"])?$_COOKIE["curmenu"]:'';

if ($accountid<1||$businessid<1){
	echo "<center><h3>Login Failed</h3>Use your browser's back button to try again.</center>";
} else {
	$companyid="1";

	$query = "SELECT * FROM business WHERE businessid = '$businessid'";
	$result = Treat_DB_ProxyOld::query($query);

	$caterdays=@mysql_result($result,0,"caterdays");
	$cater_spread=@mysql_result($result,0,"cater_spread");
	$cater_spread--;
	$caternum=@mysql_result($result,0,"caternum");
	$srvchrg=@mysql_result($result,0,"srvchrg");
	$cater_costcenter=@mysql_result($result,0,"cater_costcenter");
	$srvchrgpcnt=@mysql_result($result,0,"srvchrgpcnt");
	$weekend=@mysql_result($result,0,"weekend");
	
	if ($srvchrg==1){
		
	} else {
		$srvchrgpcnt=0;
	}

	$lastday=$today;

	for ($counter=0;$counter<$caterdays;$counter++){
		$lastday=nextday($lastday);
	}

	$date = isset($_POST['date'])?$_POST['date']:'';
	$peoplenum = isset($_POST['peoplenum'])?$_POST['peoplenum']:'';
	$roomid = isset($_POST['room'])?$_POST['room']:'';
	$start_hour = isset($_POST['start_hour'])?$_POST['start_hour']:'';
	$start_min = isset($_POST['start_min'])?$_POST['start_min']:'';
	$end_hour = isset($_POST['end_hour'])?$_POST['end_hour']:'';
	$end_min = isset($_POST['end_min'])?$_POST['end_min']:'';
	$start_am = isset($_POST['start_am'])?$_POST['start_am']:'';
	$end_am = isset($_POST['end_am'])?$_POST['end_am']:'';
	$roomcomment=str_replace("'","`",$roomcomment);
	$comment = isset($_POST['comment'])?$_POST['comment']:'';
	$comment=str_replace("'","`",$comment);
	$costcenter = isset($_POST['costcenter'])?$_POST['costcenter']:'';

	$dayname=dayofweek($date);

	if ($start_am=="AM" && $start_hour==12){
		$start_hour="00";
	} elseif ($start_am=="PM" && $start_hour!=12){
		$start_hour=$start_hour+12;
	} 

	if ($end_am=="AM" && $end_hour==12){
		$end_hour="00";
	} elseif ($end_am=="PM" && $end_hour!=12){
		$end_hour=$end_hour+12;
	}

	$start_hour2="$start_hour$start_min";
	$end_hour2="$end_hour$end_min";

	$start_min=$start_min-$cater_spread;
	
	if($start_min<0){
		$start_hour--;$start_min=60+$start_min;
	}
	
	if($start_hour<10){
		$start_hour="0$start_hour";
	}

	if($start_min<10){
		$start_min="0$start_min";
	}

	$end_min=$end_min+$cater_spread;
	
	if($end_min>=60){
		$end_hour++;$end_min=$end_min-60;
	}
	
	if($end_hour<10){
		$end_hour="0$end_hour";
	}

	if($end_min<10){
		$end_min="0$end_min";
	}

	$start_hour="$start_hour$start_min";
	$end_hour="$end_hour$end_min";

	$query = "SELECT * FROM caterclose WHERE businessid = '$businessid' AND date = '$date'";
	$result = Treat_DB_ProxyOld::query($query);
	$num5=mysql_numrows($result);

	$query = "SELECT * FROM reserve WHERE businessid = '$businessid' AND date = '$date'";
	$result = Treat_DB_ProxyOld::query($query);
	$num=mysql_numrows($result);
	$caterjobs=$num;

	$query = "SELECT * FROM reserve WHERE roomid = '$roomid' AND roomid != '0' AND date = '$date' AND ((start_hour <= '$start_hour' AND end_hour >= '$start_hour') OR (end_hour >= '$end_hour' AND start_hour <= '$end_hour') OR (start_hour >= '$start_hour' AND end_hour <= '$end_hour'))";
	$result = Treat_DB_ProxyOld::query($query);
	$num=mysql_numrows($result);

	if ($num!=0){
		$booked="1";
	} elseif ($peoplenum==0 || $peoplenum==""){
		$booked="2";
	} elseif ($end_hour2<$start_hour2){
		$booked="3";
	} elseif ($today>$date){
		$booked="4";
	} elseif ($comment==""){
		$booked="7";
	} elseif ($date<$lastday){
		$booked="9";
	} elseif ($caterjobs>=$caternum){
		$booked="8";
	} elseif (($dayname=="Saturday"||$dayname=="Sunday")&&$weekend==0){
		$booked="10";
	} elseif ($num5!=0){
		$booked="11";
	} elseif ($roomid==0&&$roomcomment==""){
		$booked="5";
	} elseif ($cater_costcenter==1&&$costcenter==""){
		$booked="12";
	} else {
		$booked="6";

		$query="INSERT INTO reserve (accountid,businessid,companyid,date,start_hour,end_hour,peoplenum,roomid,status,roomcomment,comment,service,costcenter,curmenu) VALUES ('$accountid','$businessid','1','$date','$start_hour2','$end_hour2','$peoplenum','$roomid','1','$roomcomment','$comment','$srvchrgpcnt','$costcenter','$curmenu')";
		$result = Treat_DB_ProxyOld::query($query);

		////////////////////////////////////////////////////////
		////////////////insert/update custom fields/////////////
		////////////////////////////////////////////////////////
		$updateid = mysql_insert_id();

		$query_custom = "SELECT * FROM invoice_custom_fields WHERE businessid = '$businessid' AND active = '0' AND customer = '1'";
		$result_custom = Treat_DB_ProxyOld::query($query_custom);
		$num_custom = mysql_num_rows($result_custom);

		if($num_custom>0){

			while($r_cust=mysql_fetch_array($result_custom)){

				$cust_id=$r_cust["id"];
				$newvalue = $_POST["cust$cust_id"];

				$query_custom2 = "SELECT id FROM invoice_custom_values WHERE reserveid = '$updateid' AND customid = '$cust_id'";
				$result_custom2 = Treat_DB_ProxyOld::query($query_custom2);
				$num_custom2 = mysql_num_rows($result_custom2);

				if($num_custom2!=0){
					$valueid = mysql_result($result_custom2,0,"id");
					$query_custom2 = "UPDATE invoice_custom_values SET value = '$newvalue' WHERE id = '$valueid'";
					$result_custom2 = Treat_DB_ProxyOld::query($query_custom2);
				} elseif($newvalue!=""){
					$query_custom2 = "INSERT INTO invoice_custom_values (reserveid,customid,value) VALUES ('$updateid','$cust_id','$newvalue')";
					$result_custom2 = Treat_DB_ProxyOld::query($query_custom2);
				}

			}

		}

		////////////////////////////////////////////////////////
		////////////////end custom fields///////////////////////
		////////////////////////////////////////////////////////
	}

	if ($booked==6){
		$query = "SELECT * FROM reserve WHERE accountid = '$accountid' AND roomid = '$roomid' AND date = '$date' AND start_hour = '$start_hour2' AND end_hour = '$end_hour2'";
		$result = Treat_DB_ProxyOld::query($query);
		$num=mysql_numrows($result);
		$reserveid=@mysql_result($result,0,"reserveid");
		$location="caterdetails.php?reserveid=$reserveid#detail";
	} else {
		$roomcomment="";
		$location="createorder.php?date=$date&start_hour=$start_hour2&end_hour=$end_hour2&book=$booked&people=$peoplenum&comment=$comment&roomcomment=$roomcomment&roomid=$roomid&costcenter=$costcenter#cater";
	}

	header('Location: ./' . $location);

}

google_page_track();

?>