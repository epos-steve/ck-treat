CREATE OR REPLACE view vMenuItems_District AS
	SELECT 
		menu_items.*,
		district.districtname,
		business.businessname,
		menu_type.menu_typename,
		servery_station.station_name
	FROM menu_items
	LEFT JOIN business USING( businessid )
	LEFT JOIN district USING( districtid )
	LEFT JOIN menu_type USING( menu_typeid )
	LEFT JOIN servery_station ON stationid = recipe_station;
