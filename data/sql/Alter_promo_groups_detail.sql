ALTER TABLE `promo_groups_detail` ADD `master_product_id` INT(10) NOT NULL AFTER `promo_group_id`;

ALTER TABLE `promo_groups_detail`
  DROP PRIMARY KEY,
  ADD PRIMARY KEY (product_id, promo_group_id, master_product_id);
