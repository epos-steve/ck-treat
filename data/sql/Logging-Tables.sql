
CREATE TABLE IF NOT EXISTS `log_actions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `at` datetime NOT NULL,
  `business_id` int(10) unsigned DEFAULT NULL,
  `log_location_id` int(10) unsigned NOT NULL,
  `account_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `changes` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `business_id` (`business_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `log_locations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url_path` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `last_accessed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `url_path` (`url_path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;