DROP PROCEDURE IF EXISTS `cash_in_monthly`; // 
CREATE PROCEDURE `cash_in_monthly`(cid INT, date1 DATE, date2 DATE)
BEGIN
	DECLARE cid2, bid, did, machine_collect_totals, days_in_month, month_value INT;
	DECLARE date2_selected_value DATE;
	DECLARE date1_value, date_last_collect DATE;
	DECLARE datetime_value_2, datetime_4, datetime_5 DATETIME;
	DECLARE busname, dname VARCHAR(100);
	DECLARE day_1_amount, day_2_amount, day_3_amount, day_4_amount, day_5_amount, day_6_amount, day_7_amount, day_8_amount, day_9_amount, day_10_amount, day_11_amount, day_12_amount, day_13_amount, day_14_amount, day_15_amount, day_16_amount, day_17_amount, day_18_amount, day_19_amount, day_20_amount, day_21_amount, day_22_amount, day_23_amount, day_24_amount, day_25_amount, day_26_amount, day_27_amount, day_28_amount, day_29_amount, day_30_amount, day_31_amount DECIMAL (8,2) DEFAULT 0.00;
	
	declare done int default false;
  
	DECLARE load_business_data CURSOR FOR 
	SELECT bus.businessid, bus.businessname, d.districtid, d.districtname FROM posreports_business bus JOIN posreports_district d ON d.districtid = bus.districtid WHERE (bus.businessid IN( 347, 352, 582, 594, 733, 750, 922, 1043, 1107, 1111 ) ) AND bus.companyid = cid AND bus.categoryid = 4 ORDER BY bus.districtid, bus.businessname;
	OPEN load_business_data;
	BEGIN
		DECLARE CONTINUE handler FOR NOT FOUND SET done = TRUE;
		
		DROP TEMPORARY TABLE IF EXISTS cash_in_monthly_temp;
		CREATE TEMPORARY TABLE IF NOT EXISTS cash_in_monthly_temp (companyid int(11) DEFAULT NULL, date_time date DEFAULT '0000-00-00', businessid int(11) DEFAULT NULL, day_1 decimal(8,2) DEFAULT '0') ENGINE=MEMORY DEFAULT CHARSET=latin1;

		SELECT date2 INTO date2_selected_value;
		SELECT DATE_ADD(date2, INTERVAL 1 DAY) INTO date2;
		DELETE FROM mburris_report.cash_in_monthly WHERE (companyid = cid);

	done_label: LOOP
		SET done = FALSE;
		FETCH load_business_data INTO bid, busname, did, dname;
		IF done THEN 
			LEAVE done_label;
		END IF;

		SELECT DAY(LAST_DAY(date1)) INTO days_in_month;

		SET @x = 1;
		SELECT date2 INTO datetime_value_2;
		SELECT datetime_value_2 - INTERVAL 1 SECOND INTO datetime_value_2;
		
		SELECT date1 INTO date1_value;
		REPEAT 
			SELECT datetime_value_2;
			SELECT DATE(date_time) INTO date_last_collect FROM machine_collect WHERE businessid = bid AND date_time < datetime_value_2 ORDER BY date_time DESC LIMIT 1;			

			IF (DATE(datetime_value_2) = date_last_collect) THEN

				SELECT date_time INTO datetime_4 FROM machine_collect WHERE businessid = bid AND date_time < datetime_value_2 ORDER BY date_time DESC LIMIT 1;
				SELECT date_time INTO datetime_5 FROM machine_collect WHERE businessid = bid AND date_time < datetime_value_2 ORDER BY date_time DESC LIMIT 1,1;

				SELECT SUM( ROUND(posreports_payment_detail.received,2) ) INTO day_1_amount FROM posreports_payment_detail JOIN posreports_checks ON posreports_checks.check_number = posreports_payment_detail.check_number AND posreports_checks.businessid = posreports_payment_detail.businessid AND posreports_checks.businessid = bid AND posreports_checks.bill_datetime BETWEEN datetime_5 AND datetime_4 WHERE posreports_payment_detail.payment_type = 1;
				
				INSERT INTO cash_in_monthly_temp (companyid, date_time, businessid, day_1) VALUES (cid, date_last_collect, bid, day_1_amount);

			ELSE 
			
				INSERT INTO cash_in_monthly_temp (companyid, date_time, businessid, day_1) VALUES (cid, DATE(datetime_value_2), bid, 0);
			
			END IF;

			SELECT datetime_value_2 - INTERVAL 1 DAY INTO datetime_value_2;
			SET @x = @x + 1;
		
		UNTIL @x > days_in_month END REPEAT;

		SELECT day_1 INTO day_1_amount FROM cash_in_monthly_temp WHERE (businessid = bid) ORDER BY date_time ASC LIMIT 0,1;
		SELECT day_1 INTO day_2_amount FROM cash_in_monthly_temp WHERE (businessid = bid) ORDER BY date_time ASC LIMIT 1,1;
		SELECT day_1 INTO day_3_amount FROM cash_in_monthly_temp WHERE (businessid = bid) ORDER BY date_time ASC LIMIT 2,1;
		SELECT day_1 INTO day_4_amount FROM cash_in_monthly_temp WHERE (businessid = bid) ORDER BY date_time ASC LIMIT 3,1;
		SELECT day_1 INTO day_5_amount FROM cash_in_monthly_temp WHERE (businessid = bid) ORDER BY date_time ASC LIMIT 4,1;
		SELECT day_1 INTO day_6_amount FROM cash_in_monthly_temp WHERE (businessid = bid) ORDER BY date_time ASC LIMIT 5,1;
		SELECT day_1 INTO day_7_amount FROM cash_in_monthly_temp WHERE (businessid = bid) ORDER BY date_time ASC LIMIT 6,1;
		SELECT day_1 INTO day_8_amount FROM cash_in_monthly_temp WHERE (businessid = bid) ORDER BY date_time ASC LIMIT 7,1;
		SELECT day_1 INTO day_9_amount FROM cash_in_monthly_temp WHERE (businessid = bid) ORDER BY date_time ASC LIMIT 8,1;
		SELECT day_1 INTO day_10_amount FROM cash_in_monthly_temp WHERE (businessid = bid) ORDER BY date_time ASC LIMIT 9,1;
		SELECT day_1 INTO day_11_amount FROM cash_in_monthly_temp WHERE (businessid = bid) ORDER BY date_time ASC LIMIT 10,1;
		SELECT day_1 INTO day_12_amount FROM cash_in_monthly_temp WHERE (businessid = bid) ORDER BY date_time ASC LIMIT 11,1;
		SELECT day_1 INTO day_13_amount FROM cash_in_monthly_temp WHERE (businessid = bid) ORDER BY date_time ASC LIMIT 12,1;
		SELECT day_1 INTO day_14_amount FROM cash_in_monthly_temp WHERE (businessid = bid) ORDER BY date_time ASC LIMIT 13,1;
		SELECT day_1 INTO day_15_amount FROM cash_in_monthly_temp WHERE (businessid = bid) ORDER BY date_time ASC LIMIT 14,1;
		SELECT day_1 INTO day_16_amount FROM cash_in_monthly_temp WHERE (businessid = bid) ORDER BY date_time ASC LIMIT 15,1;
		SELECT day_1 INTO day_17_amount FROM cash_in_monthly_temp WHERE (businessid = bid) ORDER BY date_time ASC LIMIT 16,1;
		SELECT day_1 INTO day_18_amount FROM cash_in_monthly_temp WHERE (businessid = bid) ORDER BY date_time ASC LIMIT 17,1;
		SELECT day_1 INTO day_19_amount FROM cash_in_monthly_temp WHERE (businessid = bid) ORDER BY date_time ASC LIMIT 18,1;
		SELECT day_1 INTO day_20_amount FROM cash_in_monthly_temp WHERE (businessid = bid) ORDER BY date_time ASC LIMIT 19,1;
		SELECT day_1 INTO day_21_amount FROM cash_in_monthly_temp WHERE (businessid = bid) ORDER BY date_time ASC LIMIT 20,1;
		SELECT day_1 INTO day_22_amount FROM cash_in_monthly_temp WHERE (businessid = bid) ORDER BY date_time ASC LIMIT 21,1;
		SELECT day_1 INTO day_23_amount FROM cash_in_monthly_temp WHERE (businessid = bid) ORDER BY date_time ASC LIMIT 22,1;
		SELECT day_1 INTO day_24_amount FROM cash_in_monthly_temp WHERE (businessid = bid) ORDER BY date_time ASC LIMIT 23,1;
		SELECT day_1 INTO day_25_amount FROM cash_in_monthly_temp WHERE (businessid = bid) ORDER BY date_time ASC LIMIT 24,1;
		SELECT day_1 INTO day_26_amount FROM cash_in_monthly_temp WHERE (businessid = bid) ORDER BY date_time ASC LIMIT 25,1;
		SELECT day_1 INTO day_27_amount FROM cash_in_monthly_temp WHERE (businessid = bid) ORDER BY date_time ASC LIMIT 26,1;
		SELECT day_1 INTO day_28_amount FROM cash_in_monthly_temp WHERE (businessid = bid) ORDER BY date_time ASC LIMIT 27,1;
		SELECT day_1 INTO day_29_amount FROM cash_in_monthly_temp WHERE (businessid = bid) ORDER BY date_time ASC LIMIT 28,1;
		SELECT day_1 INTO day_30_amount FROM cash_in_monthly_temp WHERE (businessid = bid) ORDER BY date_time ASC LIMIT 29,1;
		SELECT day_1 INTO day_31_amount FROM cash_in_monthly_temp WHERE (businessid = bid) ORDER BY date_time ASC LIMIT 30,1;

		
		INSERT INTO mburris_report.cash_in_monthly (companyid, date_start, date_end, districtid, districtname, businessid, businessname, day_1, day_2, day_3, day_4, day_5, day_6, day_7, day_8, day_9, day_10, day_11, day_12, day_13, day_14, day_15, day_16, day_17, day_18, day_19, day_20, day_21, day_22, day_23, day_24, day_25, day_26, day_27, day_28, day_29, day_30, day_31) VALUES (cid, date1, date2_selected_value, did, dname, bid, busname, day_1_amount, day_2_amount, day_3_amount, day_4_amount, day_5_amount, day_6_amount, day_7_amount, day_8_amount, day_9_amount, day_10_amount, day_11_amount, day_12_amount, day_13_amount, day_14_amount, day_15_amount, day_16_amount, day_17_amount, day_18_amount, day_19_amount, day_20_amount, day_21_amount, day_22_amount, day_23_amount, day_24_amount, day_25_amount, day_26_amount, day_27_amount, day_28_amount, day_29_amount, day_30_amount, day_31_amount);
		
	END LOOP;
	DROP TEMPORARY TABLE IF EXISTS cash_in_monthly_temp;
  END;
CLOSE load_business_data;	

END; // 
