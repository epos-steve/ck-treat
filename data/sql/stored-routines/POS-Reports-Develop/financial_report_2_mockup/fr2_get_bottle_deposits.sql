DROP FUNCTION IF EXISTS `fr2_get_bottle_deposits`; // 
CREATE FUNCTION `fr2_get_bottle_deposits`(bid INT, date1 DATE, date2 DATE) RETURNS decimal(8,2)
    READS SQL DATA
BEGIN
	DECLARE bottle_deposits_value DECIMAL (8,2);
	
	SELECT SUM(cd.amount) INTO bottle_deposits_value FROM creditdetail cd JOIN credits c ON c.creditid = cd.creditid AND c.businessid = bid AND c.category = 5 WHERE cd.`date` >= date1 AND cd.`date` < date2;
		
	IF bottle_deposits_value IS NULL THEN
		SELECT 0 INTO bottle_deposits_value;
	END IF;

	RETURN bottle_deposits_value;
END; // 

