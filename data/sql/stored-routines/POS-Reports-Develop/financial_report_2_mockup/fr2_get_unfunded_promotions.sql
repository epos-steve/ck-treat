DROP FUNCTION IF EXISTS `fr2_get_unfunded_promotions`; // 
CREATE FUNCTION `fr2_get_unfunded_promotions`(bid INT, date1 DATE, date2 DATE) RETURNS decimal(8,2)
    READS SQL DATA
BEGIN
	DECLARE unfunded_promotions_value DECIMAL (8,2);
		
	SELECT SUM(dd.amount) INTO unfunded_promotions_value FROM debitdetail dd JOIN debits d ON d.debitid = dd.debitid AND d.businessid = bid AND d.category = 3 WHERE dd.`date` >= date1 AND dd.`date` < date2;
		
	IF unfunded_promotions_value IS NULL THEN
		SELECT 0 INTO unfunded_promotions_value;
	END IF;

	RETURN unfunded_promotions_value;
END; // 

