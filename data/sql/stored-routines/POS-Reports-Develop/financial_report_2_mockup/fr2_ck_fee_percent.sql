DROP FUNCTION IF EXISTS `fr2_ck_fee_percent`; // 
CREATE FUNCTION `fr2_ck_fee_percent`(bid INT) RETURNS decimal(8,2)
    READS SQL DATA
BEGIN
	DECLARE ck_fee_percent_value DECIMAL (8,2) DEFAULT 0.00;
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000'  
	BEGIN 
		RETURN 0;
	END;

	SELECT `value` INTO ck_fee_percent_value FROM setup_detail WHERE setupid = 67 AND businessid = bid;
	
	IF (ck_fee_percent_value > 100) THEN 
		SELECT 0 INTO ck_fee_percent_value;
	END IF;

	RETURN ck_fee_percent_value;
END; // 

