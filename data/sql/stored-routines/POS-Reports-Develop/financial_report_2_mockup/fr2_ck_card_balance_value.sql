DROP FUNCTION IF EXISTS `fr2_ck_card_balance_value`; // 
CREATE FUNCTION `fr2_ck_card_balance_value`(bid INT, date1 DATE) RETURNS decimal(13,2)
    READS SQL DATA
BEGIN 
	DECLARE card_balance_value_1, card_balance_value_2, card_balance_final DECIMAL(13,2) DEFAULT 0.00; 

	SELECT SUM(dd.amount) INTO card_balance_value_1 FROM debitdetail dd JOIN debits d ON d.debitid = dd.debitid AND d.businessid = bid AND d.category IN (4,10,11,12,14) WHERE dd.`date` < date1;

	SELECT SUM(dd.amount) INTO card_balance_value_2 FROM debitdetail dd JOIN debits d ON d.debitid = dd.debitid AND d.businessid = bid AND d.category IN (2) WHERE dd.`date` < date1; 
  
	IF card_balance_value_1 IS NULL THEN 
		SELECT 0 INTO card_balance_value_1;
	END IF;

	IF card_balance_value_2 IS NULL THEN 
		SELECT 0 INTO card_balance_value_2;
	END IF;
	
	SELECT (card_balance_value_1 - card_balance_value_2) INTO card_balance_final;
	
	RETURN card_balance_final;
	
END; // 

