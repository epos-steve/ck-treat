DROP FUNCTION IF EXISTS `fr2_get_credit_card_fee_111`; // 
CREATE FUNCTION `fr2_get_credit_card_fee_111`(bid INT, date2 DATE) RETURNS decimal(8,2)
    READS SQL DATA
BEGIN
	DECLARE credit_card_fee_value DECIMAL (8,2);
	DECLARE next_tuesday, previous_friday, value2, next_tuesday_value DATE DEFAULT NULL;
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000'  
	BEGIN 
		RETURN 0;
	END;

	SELECT dateofnextweekday(date2, 3) INTO next_tuesday;
	SELECT dateoflastweekday(next_tuesday, 6) INTO previous_friday;
	
	SELECT previous_friday INTO date2;

	SELECT sum(cf.amount) INTO credit_card_fee_value FROM cc_fee cf WHERE cf.businessid = bid AND cf.`date` = date2 LIMIT 1;
		
	IF credit_card_fee_value IS NULL THEN
		SELECT 0 INTO credit_card_fee_value;
	END IF;

	RETURN credit_card_fee_value;
END; // 

