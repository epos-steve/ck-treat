DROP FUNCTION IF EXISTS `fr2_ck_initial_promo`; // 
CREATE FUNCTION `fr2_ck_initial_promo`(bid INT, date1 DATE, date2 DATE) RETURNS decimal(8,2)
    READS SQL DATA
BEGIN
	DECLARE creditid_value INT DEFAULT 0;
	DECLARE ck_initial_promo_value DECIMAL (8,2) DEFAULT 0.00;
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000'  
	BEGIN 
		RETURN 0;
	END;

	SELECT creditid INTO creditid_value FROM credits WHERE (businessid = bid) AND (creditname LIKE 'CK Card Promos');
	SELECT SUM(amount) INTO ck_initial_promo_value FROM creditdetail cd WHERE (cd.businessid = bid) AND (cd.creditid = creditid_value) AND (cd.`date` >= date1) AND (cd.`date` < date2);
	
	IF (ck_initial_promo_value IS NULL) THEN 
		SELECT 0 INTO ck_initial_promo_value;
	END IF;

	RETURN ck_initial_promo_value;
END; // 

