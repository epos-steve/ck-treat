DROP FUNCTION IF EXISTS `fr2_get_credit_card_fee_1`; // 
CREATE FUNCTION `fr2_get_credit_card_fee_1`(bid INT, date1 DATE, date2 DATE) RETURNS decimal(8,2)
    READS SQL DATA
BEGIN
	DECLARE credit_card_fee_value DECIMAL (8,2);
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000'  
	BEGIN 
		RETURN 0;
	END;
	
	SET @today = dayofweek(date2);

	IF (@today = 6) THEN 
		SELECT date2 INTO date2;
	ELSE 
		SELECT dateofnextweekday(date2, 6) INTO date2;
	END IF;

	SELECT sum(cf.amount) INTO credit_card_fee_value FROM cc_fee cf WHERE cf.businessid = bid AND cf.`date` = date2 LIMIT 1;
		
	IF credit_card_fee_value IS NULL THEN
		SELECT 0 INTO credit_card_fee_value;
	END IF;

	RETURN credit_card_fee_value;
END; // 

