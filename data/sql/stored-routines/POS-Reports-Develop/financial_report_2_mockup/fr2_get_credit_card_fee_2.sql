DROP FUNCTION IF EXISTS `fr2_get_credit_card_fee_2`; // 
CREATE FUNCTION `fr2_get_credit_card_fee_2`(bid INT, date1 DATE, date2 DATE) RETURNS decimal(8,2)
    READS SQL DATA
BEGIN
	DECLARE debitid_value INT DEFAULT 0;
	DECLARE debitdetail_sum, credit_card_fee_value_2 DECIMAL (8,2);
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000'  
	BEGIN 
		RETURN 0;
	END;
	
	SELECT debitid INTO debitid_value FROM debits WHERE (businessid = bid) AND (category = 12);
	IF debitid_value IS NOT NULL THEN 
		SELECT SUM(amount) INTO debitdetail_sum FROM debitdetail dd WHERE (dd.businessid = bid) AND (dd.debitid = debitid_value) AND (dd.`date` >= date1 AND dd.`date` < date2);
		
		IF debitdetail_sum IS NULL THEN
			SELECT 0 INTO debitdetail_sum;
		ELSE
			SELECT (debitdetail_sum * .05) INTO debitdetail_sum;
		END IF;

	END IF;
	
	SELECT debitdetail_sum INTO credit_card_fee_value_2;

	RETURN credit_card_fee_value_2;
END; // 

