DROP FUNCTION IF EXISTS `dateoflastweekday`; // 
CREATE FUNCTION `dateoflastweekday`( d date, which tinyint ) RETURNS date
    READS SQL DATA
begin 
  declare today tinyint; 
  set today = dayofweek(d); 
  return adddate( d, if(today=which,-7,if(today>which,which-today,which-today-7) )); 
end; // 

