DROP PROCEDURE IF EXISTS `ck_fees`; // 
CREATE PROCEDURE `ck_fees`(cid INT, date1 DATE, date2 DATE)
BEGIN
	DECLARE cid2, bid, did, number_of_days INT;
	DECLARE date2_selected_value DATE;
	DECLARE busname, dname VARCHAR(100);
	DECLARE net_sales_value, ck_fee_value, ck_fee_percent_value, credit_card_fee_value, credit_card_fee_value_1, credit_card_fee_value_2, credit_card_fee_percent_value, ck_fee_percent_value_2 DECIMAL (8,2);
	
	declare done int default false;
  
	DECLARE load_business_data CURSOR FOR 
	SELECT bus.businessid, bus.businessname, d.districtid, d.districtname FROM posreports_business bus JOIN posreports_district d ON d.districtid = bus.districtid WHERE (bus.businessid IN( 347, 352, 582, 594, 733, 750, 922, 1043, 1107, 1111 ) ) AND bus.companyid = cid AND bus.categoryid = 4 ORDER BY bus.districtid, bus.businessname;
	OPEN load_business_data;
	BEGIN
		DECLARE CONTINUE handler FOR NOT FOUND SET done = TRUE;
		
		SELECT date2 INTO date2_selected_value;
		SELECT DATE_ADD(date2, INTERVAL 1 DAY) INTO date2;
		DELETE FROM mburris_report.ck_fees WHERE (companyid = cid);
   
	done_label: LOOP
		SET done = FALSE;
		FETCH load_business_data INTO bid, busname, did, dname;
		IF done THEN 
			LEAVE done_label;
		END IF;

		SELECT SUM(cd.amount) INTO net_sales_value FROM mburris_businesstrack.creditdetail cd INNER JOIN mburris_businesstrack.credits c ON c.creditid = cd.creditid AND c.businessid = bid AND c.credittype = 1 WHERE (cd.`date`) >= date1 AND (cd.`date` < date2);
		
		IF net_sales_value IS NULL THEN
			SELECT 0.00 INTO net_sales_value;
		END IF;

		SELECT DATEDIFF(date2,date1) INTO number_of_days;
		IF (number_of_days > 7) THEN
			SELECT fr2_get_credit_card_fee(bid, date1, date2) INTO credit_card_fee_value;
		ELSE
--				SELECT fr2_get_credit_card_fee_1(bid, date1, date2) INTO credit_card_fee_value_1;
			SELECT fr2_get_credit_card_fee_111(bid, date2) INTO credit_card_fee_value_1;
			SELECT fr2_get_credit_card_fee_2(bid, date1, date2) INTO credit_card_fee_value_2;
			SELECT (credit_card_fee_value_1 + credit_card_fee_value_2) INTO credit_card_fee_value;
		END IF;

		SELECT fr2_ck_fee_percent(bid) INTO ck_fee_percent_value_2;
		SELECT (ck_fee_percent_value_2 / 100) INTO ck_fee_percent_value;
		
		SELECT (ck_fee_percent_value * (net_sales_value - credit_card_fee_value)) INTO ck_fee_value;
		SELECT (credit_card_fee_value / net_sales_value) * 100 INTO credit_card_fee_percent_value;

		IF credit_card_fee_percent_value IS NULL THEN
			SELECT 0.00 INTO credit_card_fee_percent_value;
		END IF;
		
		INSERT INTO mburris_report.ck_fees (companyid, date_start, date_end, districtid, districtname, businessid, businessname, net_sales, ck_fee_amount, ck_fee_percent, credit_card_fee, credit_card_fee_percent) VALUES (cid, date1, date2_selected_value, did, dname, bid, busname, net_sales_value, ck_fee_value, ck_fee_percent_value_2, credit_card_fee_value, credit_card_fee_percent_value);

	END LOOP;
  END;
CLOSE load_business_data;	
END; // 

