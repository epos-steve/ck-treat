DROP PROCEDURE IF EXISTS `inventory_waste_financial`; // 
CREATE PROCEDURE `inventory_waste_financial`(cid INT, date1 DATE, date2 DATE)
BEGIN
	DECLARE cid2, bid, did, waste2, onhand, shelf_life_days_value, pos_id_value, items_sold_2 INT;
	DECLARE date2_selected_value DATE;
	DECLARE date_time_value, date_time_value_0, date_time_value_1 TIMESTAMP; 
	DECLARE dname, busname, item_name, group_name VARCHAR(100);
	DECLARE itemcode, route_value VARCHAR(30);
	DECLARE price_value, cost_value, cost_value_mip, cost_value_new, amount_value DECIMAL (8,2);
	DECLARE done INT DEFAULT false;
  
	DECLARE load_menu_items_data CURSOR FOR 
	SELECT lmi.companyid, lmi.businessid, lmi.min_item_code, lmi.min_name, lmi.mgn_group_name, lmi.businessname, lmi.districtid, lmi.districtname FROM mburris_businesstrack.load_menu_items lmi WHERE lmi.companyid = cid ORDER BY lmi.businessid ASC, lmi.min_name ASC;
	OPEN load_menu_items_data;
	BEGIN
		DECLARE CONTINUE handler FOR NOT FOUND SET done = TRUE;

		-- CALL load_iw_checks_by_company(cid, date1, date2);
		-- CALL load_iw_checkdetail_by_company(cid);
		-- CALL load_iw_inv_count_vend_by_company(cid, date1, date2);

		SET @s4 = CONCAT("SELECT icv.unit_cost INTO @cost_value_new FROM posreports_menu_items_new_tracey mitn JOIN posreports_machine_bus_link_tracey mbl ON mbl.ordertype = mitn.ordertype AND mbl.businessid = mitn.businessid JOIN posreports_inv_count_vend_tracey icv ON icv.machine_num = mbl.machine_num AND (icv.date_time BETWEEN ? AND ?) AND (icv.item_code = mitn.item_code) WHERE mitn.businessid = ? AND (mitn.item_code = ?) AND icv.waste > 0 LIMIT 1"); 
		PREPARE stmt4 FROM @s4;

		SET @s5 = CONCAT("SELECT SUM(icv.waste) INTO @waste2 FROM posreports_inv_count_vend_tracey icv JOIN posreports_machine_bus_link_tracey mbl ON icv.machine_num = mbl.machine_num AND mbl.businessid = ? WHERE (icv.item_code = ?) AND (icv.is_inv = 0) AND (icv.date_time BETWEEN ? AND ?)"); 
		PREPARE stmt5 FROM @s5;

		SET @s6 = CONCAT("SELECT SUM(cd.quantity) INTO @items_sold_2 FROM posreports_checkdetail_tracey cd, posreports_checks_tracey c WHERE (cd.item_number = ?) AND (cd.businessid = ?) AND (cd.bill_number = c.check_number) AND (c.businessid = ?) AND (c.bill_posted BETWEEN ? AND ?)"); 
		PREPARE stmt6 FROM @s6;
	
		SELECT date2 INTO date2_selected_value;
		SELECT DATE_ADD(date2, INTERVAL 1 DAY) INTO date2;
		DELETE FROM mburris_report.inventory_waste_financial WHERE (companyid = cid) AND (date_start = date1) AND (date_end = date2_selected_value);
   
	done_label: LOOP
		SET done = FALSE;
		FETCH load_menu_items_data INTO cid2, bid, itemcode, item_name, group_name, busname, did, dname;
		IF done THEN 
			LEAVE done_label;
		END IF;
		
		SELECT mip.cost, mip.price, mitn.pos_id, mitn.shelf_life_days INTO cost_value_mip, price_value, pos_id_value, shelf_life_days_value FROM mburris_businesstrack.posreports_menu_items_price_tracey mip JOIN mburris_businesstrack.posreports_menu_items_new_tracey mitn ON mitn.businessid = bid AND mitn.item_code = itemcode AND mitn.id = mip.menu_item_id LIMIT 1;

		SET @bid_value = bid;
		SET @itemcode_value = itemcode;
		SET @pos_id_value_temp = pos_id_value;
		SET @date1 = date1;
		SET @date2 = date2;		

		EXECUTE stmt4 USING @date1, @date2, @bid_value, @itemcode_value;
		SELECT @cost_value_new INTO cost_value_new;
		
		IF cost_value_new IS NULL THEN
			SELECT 0 INTO cost_value_new;
		END IF;
		
		IF ((cost_value_new IS NULL) OR (cost_value_new = 0)) THEN 
			SELECT cost_value_mip INTO cost_value;
		ELSE
			SELECT cost_value_new INTO cost_value;
		END IF;

		EXECUTE stmt5 USING @bid_value, @itemcode_value, @date1, @date2;
		SELECT @waste2 INTO waste2;

		IF waste2 IS NULL THEN
			SELECT 0 INTO waste2;
		END IF;
	IF waste2 THEN

		-- SELECT get_districtid_value(bid) INTO did;
	    -- SELECT get_districtname_value(did) INTO dname;
	    -- SELECT get_businessname_value(bid) INTO busname;

		SELECT get_datetime_value(bid, itemcode, date2) INTO date_time_value;
		SELECT get_onhand_value(bid, itemcode, date_time_value) INTO onhand;
			
		EXECUTE stmt6 USING @pos_id_value_temp, @bid_value, @bid_value, @date1, @date2;
		SELECT @items_sold_2 INTO items_sold_2;
			
		IF items_sold_2 IS NULL THEN
			SELECT 0 INTO items_sold_2;
		END IF;
			
		SELECT get_route_value(bid, itemcode) INTO route_value;

		EXECUTE stmt5 USING @bid_value, @itemcode_value, @date1, @date2;
		SELECT @waste2 INTO waste2;

		IF waste2 IS NULL THEN
			SELECT 0 INTO waste2;
		END IF;
			
		SELECT (waste2 * cost_value) INTO amount_value;
	
		INSERT IGNORE INTO mburris_report.inventory_waste_financial (companyid, date_start, date_end, districtid, districtname, businessid, businessname, group_name, item_code, item_name, cost, price, items_sold, waste, amount, shelf_life_days, route) VALUES (cid, date1, date2_selected_value, did, dname, bid, busname, group_name, itemcode, item_name, cost_value, price_value, items_sold_2, waste2, amount_value, shelf_life_days_value, route_value);
	END IF;
	END LOOP;
  END;
CLOSE load_menu_items_data;	
END; //

