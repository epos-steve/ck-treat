DROP FUNCTION IF EXISTS `is_get_price_value`; // 
CREATE FUNCTION `is_get_price_value`(bid INT, posidvalue INT, value1 DATETIME, value2 DATETIME) 
RETURNS DECIMAL(8,2) 
READS SQL DATA 
BEGIN
	DECLARE price_value_new DECIMAL (8,2);

	SELECT (sum(price * quantity)/sum(quantity)) INTO price_value_new FROM mburris_businesstrack.posreports_checkdetail AS cd INNER JOIN mburris_businesstrack.posreports_checks_temp AS ct WHERE (cd.item_number = posidvalue) AND (cd.businessid = bid) AND (cd.bill_number = ct.check_number) AND (ct.businessid = bid) AND (ct.bill_datetime BETWEEN value1 AND value2);
		
	IF price_value_new IS NULL THEN
		SELECT 0 INTO price_value_new;
	END IF;

	RETURN price_value_new;
END; // 

