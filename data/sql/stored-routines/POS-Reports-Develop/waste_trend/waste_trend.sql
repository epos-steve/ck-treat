DROP PROCEDURE IF EXISTS `waste_trend`; // 
CREATE PROCEDURE `waste_trend`(cid INT, date1 DATE, date2 DATE)
BEGIN
	DECLARE cid2, bid, did INT;
	DECLARE date2_selected_value DATE;
	DECLARE busname, dname VARCHAR(100);
	DECLARE sales_trend_amount, waste_trend_amount, week_1_waste_trend_amount, week_2_waste_trend_amount, week_3_waste_trend_amount, week_4_waste_trend_amount, week_5_waste_trend_amount, week_6_waste_trend_amount, week_7_waste_trend_amount, week_8_waste_trend_amount, week_9_waste_trend_amount, week_10_waste_trend_amount, week_11_waste_trend_amount, week_12_waste_trend_amount DECIMAL (8,2) DEFAULT 0.00;
	DECLARE waste_percent_amount, week_1_waste_trend_percent_amount, week_2_waste_trend_percent_amount, week_3_waste_trend_percent_amount, week_4_waste_trend_percent_amount, week_5_waste_trend_percent_amount, week_6_waste_trend_percent_amount, week_7_waste_trend_percent_amount, week_8_waste_trend_percent_amount, week_9_waste_trend_percent_amount, week_10_waste_trend_percent_amount, week_11_waste_trend_percent_amount, week_12_waste_trend_percent_amount DECIMAL (8,1) DEFAULT 0.0;
	DECLARE week_1_sales_trend_amount, week_2_sales_trend_amount, week_3_sales_trend_amount, week_4_sales_trend_amount, week_5_sales_trend_amount, week_6_sales_trend_amount, week_7_sales_trend_amount, week_8_sales_trend_amount, week_9_sales_trend_amount, week_10_sales_trend_amount, week_11_sales_trend_amount, week_12_sales_trend_amount DECIMAL (8,2) DEFAULT 0.00;
	DECLARE var1, var2 DATE;
	
	declare done int default false;
  
	DECLARE load_business_data CURSOR FOR 
	SELECT bus.businessid, bus.businessname, d.districtid, d.districtname FROM posreports_business bus JOIN posreports_district d ON d.districtid = bus.districtid WHERE (bus.businessid IN( 347, 352, 582, 594, 733, 750, 922, 1043, 1107, 1111 ) ) AND bus.companyid = cid AND bus.categoryid = 4 ORDER BY bus.districtid, bus.businessname;
	OPEN load_business_data;
	BEGIN
		DECLARE CONTINUE handler FOR NOT FOUND SET done = TRUE;
		
		DROP TABLE IF EXISTS waste_trend_temp;
		CREATE TABLE IF NOT EXISTS waste_trend_temp(companyid int(11) DEFAULT NULL, date_start date DEFAULT NULL, businessid int(11) DEFAULT NULL, waste_trend decimal(8,2) DEFAULT '0.00', sales_trend decimal(8,2) DEFAULT '0.00', waste_percent decimal(8,1) DEFAULT '0.0', KEY companyid (companyid)) ENGINE=MEMORY DEFAULT CHARSET=latin1;
		
		SELECT date2 INTO date2_selected_value;
		SELECT DATE_ADD(date2, INTERVAL 1 DAY) INTO date2;
		DELETE FROM mburris_report.waste_trend WHERE (companyid = cid);
   
	done_label: LOOP
		SET done = FALSE;
		FETCH load_business_data INTO bid, busname, did, dname;
		IF done THEN 
			LEAVE done_label;
		END IF;
		
		SET @x = 0;
		REPEAT 

			SELECT date2 - INTERVAL @x WEEK INTO var1;
			SELECT var1 - INTERVAL 12 WEEK INTO var2;
			
			SELECT (SUM(dashboard.waste) / 12), (SUM(dashboard.sales) / 12) INTO waste_trend_amount, sales_trend_amount FROM dashboard WHERE dashboard.businessid = bid AND dashboard.`date` BETWEEN var2 AND var1;
			
			IF (sales_trend_amount != 0) THEN
				SELECT (waste_trend_amount / sales_trend_amount) * 100 INTO waste_percent_amount;
			ELSE
				SELECT 0.00 INTO waste_percent_amount;
			END IF;

			INSERT INTO waste_trend_temp (companyid, date_start, businessid, waste_trend, sales_trend, waste_percent) VALUES (cid, var2, bid, waste_trend_amount, sales_trend_amount, waste_percent_amount);
			
			SET @x = @x + 1;
		
		UNTIL @x > 11 END REPEAT;
		
		SELECT waste_trend, sales_trend, waste_percent INTO week_1_waste_trend_amount, week_1_sales_trend_amount, week_1_waste_trend_percent_amount FROM waste_trend_temp WHERE (businessid = bid) ORDER BY date_start ASC LIMIT 0,1;
		
		SELECT waste_trend, sales_trend, waste_percent INTO week_2_waste_trend_amount, week_2_sales_trend_amount, week_2_waste_trend_percent_amount FROM waste_trend_temp WHERE (businessid = bid) ORDER BY date_start ASC LIMIT 1,1;
		
		SELECT waste_trend, sales_trend, waste_percent INTO week_3_waste_trend_amount, week_3_sales_trend_amount, week_3_waste_trend_percent_amount FROM waste_trend_temp WHERE (businessid = bid) ORDER BY date_start ASC LIMIT 2,1;
		
		SELECT waste_trend, sales_trend, waste_percent INTO week_4_waste_trend_amount, week_4_sales_trend_amount, week_4_waste_trend_percent_amount FROM waste_trend_temp WHERE (businessid = bid) ORDER BY date_start ASC LIMIT 3,1;
		
		SELECT waste_trend, sales_trend, waste_percent INTO week_5_waste_trend_amount, week_5_sales_trend_amount, week_5_waste_trend_percent_amount FROM waste_trend_temp WHERE (businessid = bid) ORDER BY date_start ASC LIMIT 4,1;
		
		SELECT waste_trend, sales_trend, waste_percent INTO week_6_waste_trend_amount, week_6_sales_trend_amount, week_6_waste_trend_percent_amount FROM waste_trend_temp WHERE (businessid = bid) ORDER BY date_start ASC LIMIT 5,1;
		
		SELECT waste_trend, sales_trend, waste_percent INTO week_7_waste_trend_amount, week_7_sales_trend_amount, week_7_waste_trend_percent_amount FROM waste_trend_temp WHERE (businessid = bid) ORDER BY date_start ASC LIMIT 6,1;
		
		SELECT waste_trend, sales_trend, waste_percent INTO week_8_waste_trend_amount, week_8_sales_trend_amount, week_8_waste_trend_percent_amount FROM waste_trend_temp WHERE (businessid = bid) ORDER BY date_start ASC LIMIT 7,1;
		
		SELECT waste_trend, sales_trend, waste_percent INTO week_9_waste_trend_amount, week_9_sales_trend_amount, week_9_waste_trend_percent_amount FROM waste_trend_temp WHERE (businessid = bid) ORDER BY date_start ASC LIMIT 8,1;
		
		SELECT waste_trend, sales_trend, waste_percent INTO week_10_waste_trend_amount, week_10_sales_trend_amount, week_10_waste_trend_percent_amount FROM waste_trend_temp WHERE (businessid = bid) ORDER BY date_start ASC LIMIT 9,1;
		
		SELECT waste_trend, sales_trend, waste_percent INTO week_11_waste_trend_amount, week_11_sales_trend_amount, week_11_waste_trend_percent_amount FROM waste_trend_temp WHERE (businessid = bid) ORDER BY date_start ASC LIMIT 10,1;
		
		SELECT waste_trend, sales_trend, waste_percent INTO week_12_waste_trend_amount, week_12_sales_trend_amount, week_12_waste_trend_percent_amount FROM waste_trend_temp WHERE (businessid = bid) ORDER BY date_start ASC LIMIT 11,1;
		
		INSERT INTO mburris_report.waste_trend (companyid, date_start, date_end, districtid, districtname, businessid, businessname, week_1_waste_trend, week_1_waste_trend_percent, week_2_waste_trend, week_2_waste_trend_percent, week_3_waste_trend, week_3_waste_trend_percent, week_4_waste_trend, week_4_waste_trend_percent, week_5_waste_trend, week_5_waste_trend_percent, week_6_waste_trend, week_6_waste_trend_percent, week_7_waste_trend, week_7_waste_trend_percent, week_8_waste_trend, week_8_waste_trend_percent, week_9_waste_trend, week_9_waste_trend_percent, week_10_waste_trend, week_10_waste_trend_percent, week_11_waste_trend, week_11_waste_trend_percent, week_12_waste_trend, week_12_waste_trend_percent, week_1_sales_trend, week_2_sales_trend, week_3_sales_trend, week_4_sales_trend, week_5_sales_trend, week_6_sales_trend, week_7_sales_trend, week_8_sales_trend, week_9_sales_trend, week_10_sales_trend, week_11_sales_trend, week_12_sales_trend) VALUES (cid, date1, date2_selected_value, did, dname, bid, busname, week_1_waste_trend_amount, week_1_waste_trend_percent_amount, week_2_waste_trend_amount, week_2_waste_trend_percent_amount, week_3_waste_trend_amount,week_3_waste_trend_percent_amount, week_4_waste_trend_amount, week_4_waste_trend_percent_amount, week_5_waste_trend_amount,week_5_waste_trend_percent_amount, week_6_waste_trend_amount, week_6_waste_trend_percent_amount, week_7_waste_trend_amount,week_7_waste_trend_percent_amount, week_8_waste_trend_amount, week_8_waste_trend_percent_amount, week_9_waste_trend_amount, week_9_waste_trend_percent_amount, week_10_waste_trend_amount, week_10_waste_trend_percent_amount, week_11_waste_trend_amount, week_11_waste_trend_percent_amount, week_12_waste_trend_amount, week_12_waste_trend_percent_amount, week_1_sales_trend_amount, week_2_sales_trend_amount, week_3_sales_trend_amount, week_4_sales_trend_amount, week_5_sales_trend_amount, week_6_sales_trend_amount, week_7_sales_trend_amount, week_8_sales_trend_amount, week_9_sales_trend_amount, week_10_sales_trend_amount, week_11_sales_trend_amount, week_12_sales_trend_amount);

	END LOOP;
	
	DROP TABLE IF EXISTS waste_trend_temp;
	
  END;
CLOSE load_business_data;	
END; // 
