DROP FUNCTION IF EXISTS `id_get_cost_value`; // 
CREATE FUNCTION `id_get_cost_value`(bid INT, posidvalue INT, value1 DATE, value2 DATE) RETURNS decimal(8,3)
    READS SQL DATA
BEGIN
		DECLARE cost_value_new DECIMAL (8,3);
		DECLARE CONTINUE handler FOR NOT FOUND BEGIN END;

		SELECT (sum(cost * quantity)/sum(quantity)) INTO cost_value_new FROM mburris_businesstrack.posreports_checkdetail_tracey AS cd INNER JOIN mburris_businesstrack.posreports_checks_tracey AS ct WHERE (cd.item_number = posidvalue) AND (cd.businessid = bid) AND (cd.bill_number = ct.check_number) AND (ct.businessid = bid) AND (ct.bill_posted BETWEEN value1 AND value2);
		
		IF cost_value_new IS NULL THEN
			SELECT 0 INTO cost_value_new;
		END IF;

	RETURN cost_value_new;
END; // 

