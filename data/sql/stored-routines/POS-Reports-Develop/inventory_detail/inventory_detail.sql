DROP PROCEDURE IF EXISTS `inventory_detail`; // 
CREATE PROCEDURE `inventory_detail`(cid INT, date1 DATE, date2 DATE)
BEGIN
	DECLARE cid2, bid, did, onhand, pos_id_value, shelf_life_days_value, onhand_value, onhand_final_value, active_status_value, waste2_value, items_sold_1_value, fills_value INT;
	DECLARE date2_selected_value DATE;
	DECLARE cost_value, cost_value_mip, cost_value_new, price_value, amount_value DECIMAL (8,3);
	DECLARE districtname_value, businessname_value, group_name_value, item_code_value, item_name_value, route_value VARCHAR(250);
	DECLARE date_time_value_0, date_time_value_1 TIMESTAMP;  
	DECLARE date_time_value DATETIME;
	DECLARE done INT DEFAULT FALSE;
	  
	DECLARE load_menu_items_data CURSOR FOR SELECT lmi.companyid, lmi.businessid, lmi.min_item_code, lmi.min_name, lmi.mgn_group_name, lmi.min_active FROM mburris_businesstrack.load_menu_items lmi WHERE lmi.companyid = cid ORDER BY lmi.businessid ASC, lmi.min_name ASC;

	
	OPEN load_menu_items_data;
	BEGIN
		DECLARE CONTINUE handler FOR NOT FOUND SET done = TRUE;
		
		SELECT date2 INTO date2_selected_value;
		
		-- CALL load_icv_temp_by_company(cid);
		-- CALL load_id_checks_by_company(cid, date1, date2);

		SET @s4 = CONCAT("SELECT SUM(icv.waste) INTO @waste2_value FROM mburris_businesstrack.posreports_inv_count_vend_tracey icv JOIN mburris_businesstrack.posreports_machine_bus_link_tracey mbl ON icv.machine_num = mbl.machine_num AND mbl.businessid = ? WHERE (item_code = ? ) AND (icv.is_inv = 0) AND (icv.date_time BETWEEN ? AND ?)"); 
		PREPARE stmt4 FROM @s4;

		SET @s5 = CONCAT("SELECT SUM(icv.fills) INTO @fills_value FROM mburris_businesstrack.posreports_inv_count_vend_tracey icv JOIN mburris_businesstrack.posreports_machine_bus_link_tracey mbl ON icv.machine_num = mbl.machine_num AND mbl.businessid = ? WHERE (item_code = ? ) AND (icv.is_inv = 0) AND (icv.date_time BETWEEN ? AND ?)"); 
		PREPARE stmt5 FROM @s5;

		SET @s6 = CONCAT("SELECT (sum(cost * quantity)/sum(quantity)) INTO @cost_value FROM mburris_businesstrack.posreports_checkdetail_tracey AS cd INNER JOIN mburris_businesstrack.posreports_checks_tracey AS ct WHERE (cd.item_number = ?) AND (cd.businessid = ?) AND (cd.bill_number = ct.check_number) AND (ct.businessid = ?) AND (ct.bill_posted BETWEEN ? AND ?)"); 
		PREPARE stmt6 FROM @s6;
		
		-- SELECT date2 INTO date2_selected_value;
		SELECT DATE_ADD(date2, INTERVAL 1 DAY) INTO date2;
		DELETE FROM mburris_report.inventory_detail WHERE (companyid = cid) AND (date_start = date1) AND (date_end = date2_selected_value);
		
		done_label: LOOP
			SET done = FALSE;
			FETCH load_menu_items_data INTO cid2, bid, item_code_value, item_name_value, group_name_value, active_status_value;
			IF done THEN 
				LEAVE done_label;
			END IF;

			SELECT mip.cost, mip.price, mitn.pos_id, mitn.shelf_life_days INTO cost_value_mip, price_value, pos_id_value, shelf_life_days_value FROM mburris_businesstrack.posreports_menu_items_price_tracey mip JOIN mburris_businesstrack.posreports_menu_items_new_tracey mitn ON mitn.businessid = bid AND mitn.item_code = item_code_value AND mitn.id = mip.menu_item_id LIMIT 1;
			
			-- SELECT id_get_cost_value(bid, pos_id_value, date1, date2) INTO cost_value_new;
			
			SET @bid_value = bid;
			SET @pos_id_value = pos_id_value;
			SET @date1 = date1;
			SET @date2 = date2;
			
			EXECUTE stmt6 USING @pos_id_value, @bid_value, @bid_value, @date1, @date2;
			SELECT @cost_value INTO cost_value_new;
			
			IF ((cost_value_new = 0) OR (cost_value_new IS NULL)) THEN 
				SELECT cost_value_mip INTO cost_value;
			ELSE
				SELECT cost_value_new INTO cost_value;
			END IF;

			SELECT get_datetime_value(bid, item_code_value, date2) INTO date_time_value;

				IF (date_time_value > '0000-00-00 00:00:00') THEN
						  
					SELECT get_districtid_value(bid) INTO did;
					SELECT get_districtname_value(did) INTO districtname_value;
					SELECT get_businessname_value(bid) INTO businessname_value;
					SELECT get_onhand_value(bid, item_code_value, date_time_value) INTO onhand_value;
					SELECT get_route_value(bid, item_code_value) INTO route_value;
					SELECT get_items_sold_value_2(bid, pos_id_value, date_time_value, date2) INTO items_sold_1_value;

					IF onhand_value IS NULL THEN
						SELECT 0 INTO onhand_value;
					END IF;
					
					IF items_sold_1_value IS NULL THEN
						SELECT 0 INTO items_sold_1_value;
					END IF;

					SET @bid_value = bid;
					SET @itemcode_value = item_code_value;
					SET @date_start_value = date_time_value;
					SET @date_end_value = date2;

					EXECUTE stmt4 USING @bid_value, @itemcode_value, @date_start_value, @date_end_value;
					SELECT @waste2_value INTO waste2_value;
					IF (waste2_value IS NULL) THEN 
						SELECT 0 INTO waste2_value;
					END IF;

					EXECUTE stmt5 USING @bid_value, @itemcode_value, @date_start_value, @date_end_value;
					SELECT @fills_value INTO fills_value;
					IF (fills_value IS NULL) THEN 
						SELECT 0 INTO fills_value;
					END IF;
					
					SELECT (onhand_value + fills_value - waste2_value - items_sold_1_value) INTO onhand_final_value;
					IF (onhand_final_value IS NULL) THEN 
						SELECT 0 INTO onhand_final_value;
					END IF;
					
					IF onhand_final_value != 0 THEN 
						SELECT (onhand_final_value * cost_value) INTO amount_value;
						INSERT IGNORE INTO mburris_report.inventory_detail (companyid, date_start, date_end, date_time, districtid, districtname, businessid, businessname, posid, group_name, item_code, item_name, onhand, cost, amount, shelf_life_days, route, active_status, onhand_amount, fills, waste2, items_sold_1) VALUES (cid, date1, date2_selected_value, date_time_value, did, districtname_value, bid, businessname_value, pos_id_value, group_name_value, item_code_value, item_name_value, onhand_final_value, cost_value, amount_value, shelf_life_days_value, route_value, active_status_value, onhand_value, fills_value, waste2_value, items_sold_1_value);
					END IF;
				END IF;		  
		END LOOP;
	END;
	CLOSE load_menu_items_data;
END; // 

