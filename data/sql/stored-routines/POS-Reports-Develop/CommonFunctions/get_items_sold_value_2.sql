DROP FUNCTION IF EXISTS `get_items_sold_value_2`; // 
CREATE FUNCTION `get_items_sold_value_2`(bid INT, item_price_pos_id_value INT, value1 DATETIME, value2 DATE) RETURNS int(11)
    READS SQL DATA
BEGIN
		DECLARE items_sold_value_2 INT;

		SELECT SUM(cd.quantity) INTO items_sold_value_2 FROM mburris_businesstrack.posreports_checkdetail_tracey cd , mburris_businesstrack.posreports_checks_tracey c WHERE (cd.item_number = item_price_pos_id_value) AND (cd.businessid = bid) AND (cd.bill_number = c.check_number) AND (c.businessid = bid) AND (c.bill_posted BETWEEN value1 AND value2);
	  
		IF items_sold_value_2 IS NULL THEN
			SELECT 0 INTO items_sold_value_2;
		END IF;
			  
		RETURN items_sold_value_2;
END; // 

