DROP FUNCTION IF EXISTS `get_districtname_value`; // 
CREATE FUNCTION `get_districtname_value`(did INT) RETURNS varchar(200) CHARSET utf8
    READS SQL DATA
BEGIN
	DECLARE dname_value VARCHAR (200);
    SELECT districtname INTO dname_value FROM mburris_businesstrack.posreports_district WHERE districtid = did;
	RETURN dname_value;
END; // 

