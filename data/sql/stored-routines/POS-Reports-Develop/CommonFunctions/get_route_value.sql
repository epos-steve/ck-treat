DROP FUNCTION IF EXISTS `get_route_value`; // 
CREATE FUNCTION `get_route_value`(bid INT, item_code VARCHAR(10)) RETURNS varchar(10) CHARSET utf8
    READS SQL DATA
BEGIN
	DECLARE login_route_value VARCHAR (10);
   
    SELECT lr.route INTO login_route_value FROM login_route lr JOIN posreports_menu_items_new_tracey mitn ON mitn.businessid = bid AND (mitn.item_code = item_code) JOIN posreports_machine_bus_link_tracey mbl ON mbl.businessid = bid AND (mbl.ordertype = mitn.ordertype) JOIN vend_machine ON vend_machine.machine_num = mbl.machine_num WHERE vend_machine.login_routeid = lr.login_routeid LIMIT 1;
--    SELECT 0 INTO login_route_value;
	RETURN login_route_value;
END; // 

