DROP FUNCTION IF EXISTS `get_fills_value`; // 
CREATE FUNCTION `get_fills_value`(bid INT, itemcode VARCHAR(20), date_time_value DATETIME, date2_value DATE) RETURNS int(11)
    READS SQL DATA
BEGIN
	DECLARE fills_value INT;

	SELECT SUM(icv.fills) INTO fills_value FROM mburris_businesstrack.posreports_inv_count_vend icv JOIN mburris_businesstrack.posreports_machine_bus_link mbl ON icv.machine_num = mbl.machine_num AND mbl.businessid = bid WHERE (item_code = itemcode) AND (icv.date_time BETWEEN date_time_value AND date2_value) AND (icv.is_inv = 0);
	IF fills_value IS NULL THEN
		SELECT 0 INTO fills_value;
	END IF;
	RETURN fills_value ;
END; // 

