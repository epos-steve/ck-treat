DROP FUNCTION IF EXISTS `get_businessname_value`; // 
CREATE FUNCTION `get_businessname_value`(bid INT) RETURNS varchar(200) CHARSET utf8
    READS SQL DATA
BEGIN
	DECLARE bname_value VARCHAR (200);
	DECLARE businessname_value CURSOR FOR 
	SELECT businessname FROM mburris_businesstrack.posreports_business WHERE businessid = bid;
	DECLARE EXIT HANDLER FOR SQLSTATE '02000'
	BEGIN 
		RETURN ''; 
	END;

	OPEN businessname_value;
		FETCH businessname_value INTO bname_value;
	RETURN bname_value;
	CLOSE businessname_value;
END; // 

