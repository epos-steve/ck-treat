DROP PROCEDURE IF EXISTS `load_kiosk_export`; // 
CREATE PROCEDURE `load_kiosk_export`(cid INT, date1 DATE, date2 DATE)
BEGIN
	
	DECLARE item_code_value, machine_num_value VARCHAR(30);
	DECLARE name_value VARCHAR(100);
	DECLARE pos_id_value, count_rec_value, businessid_value, mitn_id_value, creditid_value INT;
	DECLARE max_value, onhand_value, fills_value, waste_value, price_value, cost_value, sales_value, items_sold_value DECIMAL (10,2) DEFAULT 0.00;
	DECLARE done INT DEFAULT FALSE;
	
	DECLARE load_kiosk_export_data CURSOR FOR SELECT mitn.id, mitn.item_code, mitn.name, mitn.pos_id, mitn.max, b.businessid, mbl.machine_num, SUM(icv.onhand) AS onhand, SUM(icv.fills) AS fills, SUM(icv.waste) AS waste, COUNT(icv.id) AS count_rec, mip.price, mip.cost, SUM(cd.amount) AS sales, cd.creditid FROM mburris_businesstrack.mei_menu_items_new mitn JOIN mburris_businesstrack.mei_business b ON b.businessid = mitn.businessid JOIN mburris_businesstrack.mei_machine_bus_link mbl ON mbl.businessid = mitn.businessid AND mbl.ordertype = mitn.ordertype LEFT JOIN mburris_businesstrack.mei_icv_temp icv ON icv.item_code = mitn.item_code AND icv.machine_num = mbl.machine_num AND icv.date_time BETWEEN date1 AND date2 JOIN mburris_businesstrack.mei_menu_items_price mip ON mip.menu_item_id = mitn.id LEFT JOIN mburris_businesstrack.mei_creditdetail cd ON cd.creditid = mbl.creditid AND cd.date BETWEEN date1 AND date2 GROUP BY mitn.id ORDER BY mbl.businessid ASC, mbl.machine_num ASC;

	OPEN load_kiosk_export_data;
	BEGIN
		DECLARE CONTINUE handler FOR NOT FOUND SET done = TRUE;
		TRUNCATE mburris_businesstrack.load_kiosk_export;

		done_label: LOOP
			SET done = FALSE;
			FETCH load_kiosk_export_data INTO mitn_id_value, item_code_value, name_value, pos_id_value, max_value, businessid_value, machine_num_value, onhand_value, fills_value, waste_value, count_rec_value, price_value, cost_value, sales_value, creditid_value;
			
				IF done THEN 
					LEAVE done_label;
				END IF;
			
			SELECT iw_get_items_sold_value(businessid_value, pos_id_value, date1, date2) INTO items_sold_value;
			
			IF (onhand_value IS NULL) THEN 
				SELECT 0 INTO onhand_value;
			END IF;

			IF (fills_value IS NULL) THEN 
				SELECT 0 INTO fills_value;
			END IF;

			IF (waste_value IS NULL) THEN 
				SELECT 0 INTO waste_value;
			END IF;

			IF (count_rec_value IS NULL) THEN 
				SELECT 0 INTO count_rec_value;
			END IF;

			IF (price_value IS NULL) THEN 
				SELECT 0 INTO price_value;
			END IF;

			IF (cost_value IS NULL) THEN 
				SELECT 0 INTO cost_value;
			END IF;

			IF (sales_value IS NULL) THEN 
				SELECT 0 INTO sales_value;
			END IF;
			
			INSERT INTO mburris_businesstrack.load_kiosk_export (id, companyid, date1, date2, creditid, mitn_id, item_code, name, pos_id, max_value, businessid, machine_num, onhand, fills, waste, count_rec, price, cost, sales, items_sold) VALUES (NULL, cid, date1, date2, creditid_value, mitn_id_value, item_code_value, name_value, pos_id_value, max_value, businessid_value, machine_num_value, onhand_value, fills_value, waste_value, count_rec_value, price_value, cost_value, sales_value, items_sold_value);

		END LOOP;

	END;
CLOSE load_kiosk_export_data;

END; // 

