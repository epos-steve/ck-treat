DROP PROCEDURE IF EXISTS `load_kiosk_export_part1`; // 
CREATE PROCEDURE `load_kiosk_export_part1`(cid INT, date1 DATE, date2 DATE)
BEGIN

	TRUNCATE mei_business;
	TRUNCATE mei_creditdetail;
	TRUNCATE mei_icv_temp;
	TRUNCATE mei_machine_bus_link;
	TRUNCATE mei_menu_items_new;
	TRUNCATE mei_menu_items_price;

	INSERT IGNORE INTO mei_business SELECT * FROM business WHERE (companyid = cid) AND (categoryid =4) ORDER BY businessid ASC;

	INSERT IGNORE INTO mei_creditdetail SELECT cd.creditdetailid, cd.companyid, cd.businessid, cd.amount, cd.date, cd.creditid FROM creditdetail cd INNER JOIN business b USING (businessid) WHERE (b.companyid = cid) AND (b.categoryid =4) AND cd.date = date1 ORDER BY cd.businessid ASC;

	INSERT IGNORE INTO mei_icv_temp SELECT icv.* FROM inv_count_vend icv INNER JOIN machine_bus_link mbl USING (machine_num) INNER JOIN business b USING (businessid) WHERE (b.companyid = cid) AND (b.categoryid =4) AND `date_time` >= date1 AND `date_time` < date2 ORDER BY mbl.machine_num;

	INSERT IGNORE INTO mei_machine_bus_link SELECT mbl.id, mbl.businessid, mbl.machine_num, mbl.ordertype, mbl.creditid, mbl.client_programid, mbl.leadtime FROM machine_bus_link mbl INNER JOIN business b USING (businessid) WHERE (b.companyid = cid) AND (b.categoryid =4) ORDER BY mbl.businessid ASC;

	INSERT IGNORE INTO mei_menu_items_new SELECT mitn.* FROM menu_items_new mitn INNER JOIN business b USING (businessid) WHERE (b.companyid = cid) AND (b.categoryid =4)  ORDER BY mitn.businessid ASC;

	INSERT IGNORE INTO mei_menu_items_price SELECT mip.id, mip.businessid, mip.menu_item_id, mip.pos_price_id, mip.price, mip.new_price, mip.cost FROM menu_items_price mip INNER JOIN business b USING (businessid) WHERE (b.companyid = cid) AND (b.categoryid =4) ORDER BY mip.businessid ASC;
	
END; // 

