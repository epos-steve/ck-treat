DROP PROCEDURE IF EXISTS `mei_data`; // 
CREATE PROCEDURE `mei_data`()
BEGIN
SELECT mitn.item_code, mitn.name, mitn.pos_id, mitn.max, b.businessid, mbl.machine_num, SUM( icv.onhand ) AS onhand, SUM( icv.fills ) AS fills, SUM( icv.waste ) AS waste, COUNT( icv.id ) AS count_rec, mip.price, mip.cost, SUM( creditdetail.amount ) AS sales
FROM posreports_menu_items_new mitn 
JOIN posreports_business b ON b.businessid = mitn.businessid AND b.companyid = 6 
JOIN posreports_machine_bus_link mbl ON mbl.businessid = mitn.businessid AND mbl.ordertype = mitn.ordertype 
LEFT JOIN posreports_inv_count_vend icv ON icv.item_code = mitn.item_code AND icv.machine_num = mbl.machine_num AND icv.date_time 
BETWEEN  '2012-10-07 00:00:00' AND  '2012-10-07 23:59:59' 
JOIN posreports_menu_items_price mip ON mip.menu_item_id = mitn.id 
LEFT JOIN creditdetail ON creditdetail.creditid = mbl.creditid 
AND creditdetail.date BETWEEN '2012-10-07 00:00:00' AND '2012-10-07 23:59:59' 
GROUP BY mitn.id 
ORDER BY mbl.businessid, mbl.machine_num;
END; // 

