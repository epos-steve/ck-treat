

DROP TABLE IF EXISTS `PromoVivipos`;
DROP TABLE IF EXISTS `vivipos_promotions`;
CREATE TABLE IF NOT EXISTS `vivipos_promotions` (
	`businessid`                  int(10) unsigned DEFAULT NULL,
	`promo_id`                    int(10) unsigned DEFAULT NULL,
	`promo_target`                smallint(5) unsigned DEFAULT NULL,
	`vivipos_name`                varchar(33) DEFAULT NULL,
	`pos_id`                      int(11) DEFAULT NULL,
	`value`                       double(10,2) DEFAULT NULL,
	`rule_order`                  smallint(6) DEFAULT NULL,
	`promo_reserve`               tinyint(1) DEFAULT NULL,
	`promo_discount`              varchar(24) DEFAULT NULL,
	`promo_discount_type`         varchar(24) DEFAULT NULL,
	`promo_discount_n`            smallint(6) DEFAULT NULL,
	`promo_discount_limit`        smallint(6) DEFAULT NULL,
	`promo_trigger_amount_limit`  varchar(16) DEFAULT NULL,
	`promo_trigger_amount_type`   varchar(24) DEFAULT NULL,
	`promo_trigger_amount`        decimal(8,2) DEFAULT NULL,
	`promo_trigger_amount_2`      smallint(6) DEFAULT NULL,
	`promo_trigger_amount_3`      smallint(6) DEFAULT NULL,
	`vivipos_taxno`               varchar(12) DEFAULT NULL,
	`vivipos_taxname`             varchar(25) DEFAULT NULL,
	`vivipos_startdate`           int(10) unsigned DEFAULT NULL,
	`vivipos_starttime`           int(10) unsigned DEFAULT NULL,
	`vivipos_enddate`             int(10) unsigned DEFAULT NULL,
	`vivipos_endtime`             int(10) unsigned DEFAULT NULL,
	`vivipos_days`                varchar(16) DEFAULT NULL,
	`vivipos_type`                varchar(32) DEFAULT NULL,
	`vivipos_trigger`             varchar(32) DEFAULT NULL,
	`promotarget_table`           varchar(50) DEFAULT NULL,
	`promotarget_order`           varchar(50) DEFAULT NULL,
	`vivipos_modified`            timestamp NULL DEFAULT NULL,
	`vivipos_active`              tinyint(1) DEFAULT NULL,
	`is_global`                   tinyint(1) DEFAULT NULL,
	`alt_name1`                   tinyint(1) DEFAULT NULL,
  KEY `businessid` (`businessid`),
  KEY `promo_id` (`promo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP PROCEDURE IF EXISTS `populate_promotion_sync`;
DROP PROCEDURE IF EXISTS `populate_promotion_sync_business`;
DROP PROCEDURE IF EXISTS `populate_promotion_sync_kiosk`;
DROP PROCEDURE IF EXISTS `populate_vivipos_promotions`;
DROP PROCEDURE IF EXISTS `populate_vivipos_promotion_business`;
DROP PROCEDURE IF EXISTS `populate_vivipos_promotion_kiosk`;


DELIMITER $$
CREATE PROCEDURE `populate_vivipos_promotions`()
LANGUAGE SQL
BEGIN

	DELETE FROM `vivipos_promotions`;
	INSERT INTO `vivipos_promotions`
	(
		`businessid`,
		`promo_id`,
		`promo_target`,
		`vivipos_name`,
		`pos_id`,
		`value`,
		`rule_order`,
		`promo_reserve`,
		`promo_discount`,
		`promo_discount_type`,
		`promo_discount_n`,
		`promo_discount_limit`,
		`promo_trigger_amount_limit`,
		`promo_trigger_amount_type`,
		`promo_trigger_amount`,
		`promo_trigger_amount_2`,
		`promo_trigger_amount_3`,
		`vivipos_taxno`,
		`vivipos_taxname`,
		`vivipos_startdate`,
		`vivipos_starttime`,
		`vivipos_enddate`,
		`vivipos_endtime`,
		`vivipos_days`,
		`vivipos_type`,
		`vivipos_trigger`,
		`promotarget_table`,
		`promotarget_order`,
		`vivipos_modified`,
		`vivipos_active`,
		`is_global`,
		`alt_name1`
	)
	SELECT
			promotions.businessid,
			promotions.id as promo_id,
			promo_target.id as promo_target,
			promotions.name as vivipos_name,
			promotions.pos_id,
			promotions.value,
			promotions.rule_order,
			promotions.promo_reserve,
			promotions.promo_discount,
			promotions.promo_discount_type,
			promotions.promo_discount_n,
			promotions.promo_discount_limit,
			promotions.promo_trigger_amount_limit,
			promotions.promo_trigger_amount_type,
			promotions.promo_trigger_amount,
			promotions.promo_trigger_amount_2,
			promotions.promo_trigger_amount_3,
			COALESCE( CAST( menu_tax.pos_id AS char ), '' ) as vivipos_taxno,
			COALESCE( menu_tax.name, '' ) as vivipos_taxname,
			UNIX_TIMESTAMP( promotions.start_date ) - (3600 * business.timezone) as vivipos_startdate,
			UNIX_TIMESTAMP( CONCAT( promotions.start_date, ' ', promotions.start_time ) ) - (3600 * business.timezone) as vivipos_starttime,
			UNIX_TIMESTAMP( promotions.end_date ) - (3600 * business.timezone) + 59 as vivipos_enddate,
			UNIX_TIMESTAMP( CONCAT( promotions.start_date, ' ', promotions.end_time ) ) - (3600 * business.timezone) + 59 as vivipos_endtime,
			GROUP_CONCAT( vPromoDays.day ORDER BY vPromoDays.day ) as vivipos_days,
			promo_type.vivipos_name as vivipos_type,
			promo_trigger.vivipos_name as vivipos_trigger,
			promo_target.db_table as promotarget_table,
			promo_target.order_by as promotarget_order,
			UNIX_TIMESTAMP( NOW() ) as vivipos_modified,
			promotions.activeFlag as vivipos_active,
			0 AS is_global,
			promotions.subsidy as alt_name1
		FROM promotions
		JOIN promo_type ON
			promotions.type = promo_type.id
		JOIN promo_trigger ON
			promotions.promo_trigger = promo_trigger.id
		JOIN promo_target ON
			promo_trigger.target_id = promo_target.id
		LEFT JOIN menu_tax ON
			promotions.tax_id = menu_tax.id
		LEFT JOIN vPromoDays ON
			promotions.id = vPromoDays.id
		JOIN business ON
			business.businessid = promotions.businessid
		GROUP BY promotions.id
		UNION
		SELECT
			promo_mapping.businessid,
			promotions.id as promo_id,
			promo_target.id as promo_target,
			promotions.name as vivipos_name,
			promotions.pos_id,
			promotions.value,
			promotions.rule_order,
			promotions.promo_reserve,
			promotions.promo_discount,
			promotions.promo_discount_type,
			promotions.promo_discount_n,
			promotions.promo_discount_limit,
			promotions.promo_trigger_amount_limit,
			promotions.promo_trigger_amount_type,
			promotions.promo_trigger_amount,
			promotions.promo_trigger_amount_2,
			promotions.promo_trigger_amount_3,
			COALESCE( CAST( menu_tax.pos_id AS char ), '' ) as vivipos_taxno,
			COALESCE( menu_tax.name, '' ) as vivipos_taxname,
			UNIX_TIMESTAMP( promotions.start_date ) - (3600 * business.timezone) as vivipos_startdate,
			UNIX_TIMESTAMP( CONCAT( promotions.start_date, ' ', promotions.start_time ) ) - (3600 * business.timezone) as vivipos_starttime,
			UNIX_TIMESTAMP( promotions.end_date ) - (3600 * business.timezone) + 59 as vivipos_enddate,
			UNIX_TIMESTAMP( CONCAT( promotions.start_date, ' ', promotions.end_time ) ) - (3600 * business.timezone) + 59 as vivipos_endtime,
			GROUP_CONCAT( vPromoDays.day ORDER BY vPromoDays.day ) as vivipos_days,
			promo_type.vivipos_name as vivipos_type,
			promo_trigger.vivipos_name as vivipos_trigger,
			promo_target.db_table as promotarget_table,
			promo_target.order_by as promotarget_order,
			UNIX_TIMESTAMP( NOW() ) as vivipos_modified,
			promotions.activeFlag as vivipos_active,
			1 AS is_global,
			promotions.subsidy as alt_name1
		FROM promotions
		JOIN promo_type ON
			promotions.type = promo_type.id
		JOIN promo_trigger ON
			promotions.promo_trigger = promo_trigger.id
		JOIN promo_target ON
			promo_trigger.target_id = promo_target.id
		LEFT JOIN menu_tax ON
			promotions.tax_id = menu_tax.id
		LEFT JOIN vPromoDays ON
			promotions.id = vPromoDays.id
		JOIN promo_mapping ON
			promo_mapping.promotionid = promotions.id
		JOIN business ON
			business.businessid = promo_mapping.businessid
		GROUP BY
			business.businessid,
			promo_id;
END$$


CREATE PROCEDURE `populate_vivipos_promotion_business`(
	IN `bid` INT UNSIGNED
)
LANGUAGE SQL
BEGIN

	DELETE FROM `vivipos_promotions` WHERE `businessid` = bid;
	INSERT INTO `vivipos_promotions`
	(
		`businessid`,
		`promo_id`,
		`promo_target`,
		`vivipos_name`,
		`pos_id`,
		`value`,
		`rule_order`,
		`promo_reserve`,
		`promo_discount`,
		`promo_discount_type`,
		`promo_discount_n`,
		`promo_discount_limit`,
		`promo_trigger_amount_limit`,
		`promo_trigger_amount_type`,
		`promo_trigger_amount`,
		`promo_trigger_amount_2`,
		`promo_trigger_amount_3`,
		`vivipos_taxno`,
		`vivipos_taxname`,
		`vivipos_startdate`,
		`vivipos_starttime`,
		`vivipos_enddate`,
		`vivipos_endtime`,
		`vivipos_days`,
		`vivipos_type`,
		`vivipos_trigger`,
		`promotarget_table`,
		`promotarget_order`,
		`vivipos_modified`,
		`vivipos_active`,
		`is_global`,
		`alt_name1`
	)
	SELECT
			promotions.businessid,
			promotions.id as promo_id,
			promo_target.id as promo_target,
			promotions.name as vivipos_name,
			promotions.pos_id,
			promotions.value,
			promotions.rule_order,
			promotions.promo_reserve,
			promotions.promo_discount,
			promotions.promo_discount_type,
			promotions.promo_discount_n,
			promotions.promo_discount_limit,
			promotions.promo_trigger_amount_limit,
			promotions.promo_trigger_amount_type,
			promotions.promo_trigger_amount,
			promotions.promo_trigger_amount_2,
			promotions.promo_trigger_amount_3,
			COALESCE( CAST( menu_tax.pos_id AS char ), '' ) as vivipos_taxno,
			COALESCE( menu_tax.name, '' ) as vivipos_taxname,
			UNIX_TIMESTAMP( promotions.start_date ) - (3600 * business.timezone) as vivipos_startdate,
			UNIX_TIMESTAMP( CONCAT( promotions.start_date, ' ', promotions.start_time ) ) - (3600 * business.timezone) as vivipos_starttime,
			UNIX_TIMESTAMP( promotions.end_date ) - (3600 * business.timezone) + 59 as vivipos_enddate,
			UNIX_TIMESTAMP( CONCAT( promotions.start_date, ' ', promotions.end_time ) ) - (3600 * business.timezone) + 59 as vivipos_endtime,
			GROUP_CONCAT( vPromoDays.day ORDER BY vPromoDays.day ) as vivipos_days,
			promo_type.vivipos_name as vivipos_type,
			promo_trigger.vivipos_name as vivipos_trigger,
			promo_target.db_table as promotarget_table,
			promo_target.order_by as promotarget_order,
			UNIX_TIMESTAMP( NOW() ) as vivipos_modified,
			promotions.activeFlag as vivipos_active,
			0 AS is_global,
			promotions.subsidy as alt_name1
		FROM promotions
		JOIN promo_type ON
			promotions.type = promo_type.id
		JOIN promo_trigger ON
			promotions.promo_trigger = promo_trigger.id
		JOIN promo_target ON
			promo_trigger.target_id = promo_target.id
		LEFT JOIN menu_tax ON
			promotions.tax_id = menu_tax.id
		LEFT JOIN vPromoDays ON
			promotions.id = vPromoDays.id
		JOIN business ON
			business.businessid = promotions.businessid
		WHERE promotions.businessid = bid
		GROUP BY promotions.id
		UNION
		SELECT
			promo_mapping.businessid,
			promotions.id as promo_id,
			promo_target.id as promo_target,
			promotions.name as vivipos_name,
			promotions.pos_id,
			promotions.value,
			promotions.rule_order,
			promotions.promo_reserve,
			promotions.promo_discount,
			promotions.promo_discount_type,
			promotions.promo_discount_n,
			promotions.promo_discount_limit,
			promotions.promo_trigger_amount_limit,
			promotions.promo_trigger_amount_type,
			promotions.promo_trigger_amount,
			promotions.promo_trigger_amount_2,
			promotions.promo_trigger_amount_3,
			COALESCE( CAST( menu_tax.pos_id AS char ), '' ) as vivipos_taxno,
			COALESCE( menu_tax.name, '' ) as vivipos_taxname,
			UNIX_TIMESTAMP( promotions.start_date ) - (3600 * business.timezone) as vivipos_startdate,
			UNIX_TIMESTAMP( CONCAT( promotions.start_date, ' ', promotions.start_time ) ) - (3600 * business.timezone) as vivipos_starttime,
			UNIX_TIMESTAMP( promotions.end_date ) - (3600 * business.timezone) + 59 as vivipos_enddate,
			UNIX_TIMESTAMP( CONCAT( promotions.start_date, ' ', promotions.end_time ) ) - (3600 * business.timezone) + 59 as vivipos_endtime,
			GROUP_CONCAT( vPromoDays.day ORDER BY vPromoDays.day ) as vivipos_days,
			promo_type.vivipos_name as vivipos_type,
			promo_trigger.vivipos_name as vivipos_trigger,
			promo_target.db_table as promotarget_table,
			promo_target.order_by as promotarget_order,
			UNIX_TIMESTAMP( NOW() ) as vivipos_modified,
			promotions.activeFlag as vivipos_active,
			1 AS is_global,
			promotions.subsidy as alt_name1
		FROM promotions
		JOIN promo_type ON
			promotions.type = promo_type.id
		JOIN promo_trigger ON
			promotions.promo_trigger = promo_trigger.id
		JOIN promo_target ON
			promo_trigger.target_id = promo_target.id
		LEFT JOIN menu_tax ON
			promotions.tax_id = menu_tax.id
		LEFT JOIN vPromoDays ON
			promotions.id = vPromoDays.id
		JOIN promo_mapping ON
			promo_mapping.promotionid = promotions.id
		JOIN business ON
			business.businessid = promo_mapping.businessid
		WHERE
			promo_mapping.businessid = bid
		GROUP BY
			business.businessid,
			promo_id;
END$$


CREATE PROCEDURE `populate_vivipos_promotion_kiosk`(
	IN `client_id` INT UNSIGNED
)
LANGUAGE SQL
BEGIN
	DECLARE bid INT UNSIGNED;
	SELECT
		COALESCE(businessid, 0) INTO bid
	FROM
		mburris_manage.client_programs
	WHERE
		client_programid = client_id
	LIMIT 1;
	CALL populate_vivipos_promotion_business(bid);
END$$
DELIMITER ;