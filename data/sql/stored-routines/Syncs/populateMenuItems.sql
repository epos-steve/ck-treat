

DROP TABLE IF EXISTS `vivipos_menu_items`;
CREATE TABLE IF NOT EXISTS `vivipos_menu_items` (
	`menu_item_id`    int(10) unsigned NOT NULL,
	`client_programid`      int(10) unsigned NOT NULL,
	`businessid`      int(10) unsigned NOT NULL,
	`pos_id`          int(11) DEFAULT NULL,
	`code`            varchar(30) DEFAULT NULL,
	`barcode`         varchar(30) DEFAULT NULL,
	`new_price`       double(7,2) DEFAULT NULL,
	`name`            varchar(100) DEFAULT NULL,
	`is_button`       tinyint(1) DEFAULT NULL,
	`price`           double(7,2) DEFAULT NULL,
	`group_type`      int(11) DEFAULT NULL,
	`sale_unit`       varchar(10) DEFAULT NULL,
	`tare`            decimal(7,5) DEFAULT NULL,
	`cond_group`      int(11) DEFAULT NULL,
	`force_condiment` tinyint(1) DEFAULT NULL,
	`uuid`            MEDIUMTEXT DEFAULT NULL,
  KEY `menu_item_id` (`menu_item_id`),
  KEY `businessid` (`businessid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP PROCEDURE IF EXISTS `populate_vivipos_menu_items`;
DROP PROCEDURE IF EXISTS `populate_vivipos_menu_item_business`;
DROP PROCEDURE IF EXISTS `populate_vivipos_menu_item_kiosk`;
DROP FUNCTION  IF EXISTS `vivipos_menu_item_uuid`;


DELIMITER $$
CREATE FUNCTION `vivipos_menu_item_uuid`(
	`pid` INT UNSIGNED,
	`bid` INT UNSIGNED,
	`upc` VARCHAR(100)
)
RETURNS mediumtext
BEGIN

	DECLARE menu_item_uuid MEDIUMTEXT DEFAULT "";
	SELECT GROUP_CONCAT(`uuid`) INTO menu_item_uuid FROM (
		SELECT
			`promo_groups`.`uuid` AS `uuid`
		FROM `promo_groups`
		JOIN `promo_groups_detail` ON
			`promo_groups_detail`.`promo_group_id` = `promo_groups`.`id`
		WHERE
			`promo_groups_detail`.`product_id` = pid
			AND `promo_groups`.`businessid` = bid
		UNION
		SELECT
			`promo_groups`.`uuid` AS `uuid`
		FROM `promo_mapping`
		JOIN `menu_items_master` ON
			`menu_items_master`.`upc` = upc
		JOIN `promo_groups_detail` ON
			`promo_groups_detail`.`master_product_id` = `menu_items_master`.`id`
		JOIN `promo_groups` ON
			`promo_groups`.`id` = `promo_groups_detail`.`promo_group_id`
		JOIN `promo_detail` ON
			`promo_detail`.`id` = `promo_groups`.`id`
		JOIN `promotions` ON
			`promotions`.`id` = `promo_detail`.`promo_id`
		WHERE
			`promo_mapping`.`promotionid` = `promotions`.`id`
			AND `promo_mapping`.`businessid` = bid
	) AS miu;
	RETURN menu_item_uuid;

END$$


CREATE PROCEDURE `populate_vivipos_menu_items`()
LANGUAGE SQL
BEGIN

	DELETE FROM `vivipos_menu_items`;
	INSERT INTO `vivipos_menu_items`
	(
		`menu_item_id`,
		`businessid`,
		`client_programid`,
		`pos_id`,
		`code`,
		`barcode`,
		`new_price`,
		`name`,
		`is_button`,
		`price`,
		`group_type`,
		`sale_unit`,
		`tare`,
		`cond_group`,
		`force_condiment`,
		`uuid`
	)
	SELECT
		m.`id` AS `menu_item_id`,
		m.`businessid`,
		k.client_programid,
		m.`pos_id` AS `pos_id`,
		m.`item_code` AS `code`,
		m.`upc` AS `barcode`,
		p.`new_price`,
		m.`name`,
		m.`is_button`,
		p.`price`,
		g.`pos_id` AS `group_type`,
		m.`sale_unit`,
		m.`tare`,
		COALESCE( mg.modifier_group_id, m.cond_group ) AS cond_group,
		m.force_condiment,
		vivipos_menu_item_uuid(m.`id`, m.`businessid`, m.`upc`) AS `uuid`
	FROM
		menu_items_new as m
	JOIN kiosk_sync AS k ON
		k.item_id = m.id
		AND k.item_type = 1
	LEFT JOIN menu_items_price AS p ON
		p.menu_item_id = m.id
	LEFT JOIN menu_groups_new AS g ON
		g.id = m.group_id
	LEFT JOIN menu_items_new_modifier_groups AS mg ON
		mg.menu_item_id = m.id
	WHERE
		(
			(
				m.item_code IS NOT NULL
				AND m.item_code <> ''
			)
			OR (
				m.upc IS NOT NULL
				AND m.upc <> ''
			)
			OR p.new_price IS NOT NULL
		)
	GROUP BY m.businessid, k.client_programid, m.id;

END$$


CREATE PROCEDURE `populate_vivipos_menu_item_business`(
	IN `bid` INT UNSIGNED
)
LANGUAGE SQL
BEGIN

	DELETE FROM `vivipos_menu_items` WHERE businessid = bid;
	INSERT INTO `vivipos_menu_items`
	(
		`menu_item_id`,
		`businessid`,
		`client_programid`,
		`pos_id`,
		`code`,
		`barcode`,
		`new_price`,
		`name`,
		`is_button`,
		`price`,
		`group_type`,
		`sale_unit`,
		`tare`,
		`cond_group`,
		`force_condiment`,
		`uuid`
	)
	SELECT
		m.`id` AS `menu_item_id`,
		m.`businessid`,
		k.client_programid,
		m.`pos_id` AS `pos_id`,
		m.`item_code` AS `code`,
		m.`upc` AS `barcode`,
		p.`new_price`,
		m.`name`,
		m.`is_button`,
		p.`price`,
		g.`pos_id` AS `group_type`,
		m.`sale_unit`,
		m.`tare`,
		COALESCE( mg.modifier_group_id, m.cond_group ) AS cond_group,
		m.force_condiment,
		vivipos_menu_item_uuid(m.`id`, m.`businessid`, m.`upc`) AS `uuid`
	FROM
		menu_items_new as m
	JOIN kiosk_sync AS k ON
		k.item_id = m.id
		AND k.item_type = 1
	LEFT JOIN menu_items_price AS p ON
		p.menu_item_id = m.id
	LEFT JOIN menu_groups_new AS g ON
		g.id = m.group_id
	LEFT JOIN menu_items_new_modifier_groups AS mg ON
		mg.menu_item_id = m.id
	WHERE
		(
			(
				m.item_code IS NOT NULL
				AND m.item_code <> ''
			)
			OR (
				m.upc IS NOT NULL
				AND m.upc <> ''
			)
			OR p.new_price IS NOT NULL
		)
		AND m.businessid = bid
	GROUP BY m.businessid, k.client_programid, m.id;

END$$

CREATE PROCEDURE `populate_vivipos_menu_item_kiosk`(
	IN `client_id` INT UNSIGNED
)
LANGUAGE SQL
BEGIN
	DECLARE bid INT UNSIGNED;
	SELECT
		COALESCE(businessid, 0) INTO bid
	FROM
		mburris_manage.client_programs
	WHERE
		client_programid = client_id
	LIMIT 1;

	DELETE FROM `vivipos_menu_items` WHERE businessid = bid AND client_programid = client_id;
	INSERT INTO `vivipos_menu_items`
	(
		`menu_item_id`,
		`businessid`,
		`client_programid`,
		`pos_id`,
		`code`,
		`barcode`,
		`new_price`,
		`name`,
		`is_button`,
		`price`,
		`group_type`,
		`sale_unit`,
		`tare`,
		`cond_group`,
		`force_condiment`,
		`uuid`
	)
	SELECT
		m.`id` AS `menu_item_id`,
		m.`businessid`,
		k.client_programid,
		m.`pos_id` AS `pos_id`,
		m.`item_code` AS `code`,
		m.`upc` AS `barcode`,
		p.`new_price`,
		m.`name`,
		m.`is_button`,
		p.`price`,
		g.`pos_id` AS `group_type`,
		m.`sale_unit`,
		m.`tare`,
		COALESCE( mg.modifier_group_id, m.cond_group ) AS cond_group,
		m.force_condiment,
		vivipos_menu_item_uuid(m.`id`, m.`businessid`, m.`upc`) AS `uuid`
	FROM
		menu_items_new as m
	JOIN kiosk_sync AS k ON
		k.item_id = m.id
		AND k.item_type = 1
	LEFT JOIN menu_items_price AS p ON
		p.menu_item_id = m.id
	LEFT JOIN menu_groups_new AS g ON
		g.id = m.group_id
	LEFT JOIN menu_items_new_modifier_groups AS mg ON
		mg.menu_item_id = m.id
	WHERE
		(
			(
				m.item_code IS NOT NULL
				AND m.item_code <> ''
			)
			OR (
				m.upc IS NOT NULL
				AND m.upc <> ''
			)
			OR p.new_price IS NOT NULL
		)
		AND m.businessid = bid
		AND k.client_programid = client_id
	GROUP BY m.businessid, k.client_programid, m.id;
END$$
DELIMITER ;