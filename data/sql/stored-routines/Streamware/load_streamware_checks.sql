DROP PROCEDURE IF EXISTS `load_streamware_checks`; // 
CREATE PROCEDURE `load_streamware_checks`(cid INT, date1 DATETIME, date2 DATETIME)
BEGIN
	
	DELETE FROM streamware_checks WHERE companyid = cid;
	DELETE FROM streamware_promotions WHERE companyid = cid;
	DELETE FROM streamware_payments WHERE companyid = cid;
	DELETE FROM streamware_sale_items WHERE companyid = cid;
	
	INSERT INTO streamware_checks (id, companyid, businessid, check_number, transaction_id, bill_datetime, transactionDateTime, terminal_group, subtotal, taxes, total, payment_types) SELECT chk.id, cid, chk.businessid, chk.check_number, CONCAT( chk.businessid,':', chk.check_number ) AS transactionID, chk.bill_datetime, UNIX_TIMESTAMP( bill_datetime ) AS transactionDateTime, chk.terminal_group, (chk.total - chk.taxes) AS subtotal, chk.taxes, chk.total, GROUP_CONCAT(DISTINCT pd.payment_type ORDER BY pd.payment_type ASC SEPARATOR ':' ) AS payment_types FROM posreports_checks chk INNER JOIN posreports_business b ON (chk.businessid = b.businessid) LEFT JOIN posreports_payment_detail pd ON chk.businessid = pd.businessid AND chk.check_number = pd.check_number WHERE (b.companyid = cid) AND (chk.bill_datetime BETWEEN date1 AND date2) GROUP BY chk.businessid, chk.check_number; 
	
	INSERT INTO streamware_promotions (companyid, businessid, transaction_id, name, amount) SELECT cid, cd.businessid, CONCAT(cd.businessid, ':', cd.check_number) transaction_id, p.name, cd.amount FROM checkdiscounts cd INNER JOIN promotions p ON (p.businessid = cd.businessid) AND (cd.pos_id = p.pos_id) INNER JOIN streamware_checks chk ON (cd.businessid = chk.businessid) AND (cd.check_number = chk.check_number);
	
	INSERT INTO streamware_payments (companyid, businessid, transaction_id, payment_type, received, base_amount) SELECT cid, pd.businessid, CONCAT( pd.businessid,  ':', pd.check_number ), pd.payment_type, pd.received, pd.base_amount FROM posreports_payment_detail pd INNER JOIN streamware_checks chk ON (pd.businessid = chk.businessid) AND (pd.check_number = chk.check_number);

	INSERT INTO streamware_sale_items (companyid, transaction_id, businessid, item_number, quantity, price) SELECT cid, CONCAT(cd1.businessid,':',cd1.bill_number) transaction_id, cd1.businessid, cd1.item_number, cd1.quantity, cd1.price FROM posreports_checkdetail cd1 INNER JOIN streamware_checks st1 ON (cd1.businessid = st1.businessid) AND (cd1.bill_number = st1.check_number) INNER JOIN posreports_business b ON (cd1.businessid = b.businessid) WHERE b.companyid = cid;
	
	UPDATE streamware_sale_items ssi SET ssi.menu_item_id = (SELECT mitn1.id FROM posreports_menu_items_new mitn1 WHERE mitn1.businessid = ssi.businessid AND mitn1.pos_id = ssi.item_number), ssi.product_upc = (SELECT mitn1.upc FROM posreports_menu_items_new mitn1 WHERE mitn1.businessid = ssi.businessid AND mitn1.pos_id = ssi.item_number), ssi.product_code = (SELECT mitn1.item_code FROM posreports_menu_items_new mitn1 WHERE mitn1.businessid = ssi.businessid AND mitn1.pos_id = ssi.item_number);

	UPDATE streamware_sale_items ssi SET ssi.list_price = (SELECT mip1.price FROM posreports_menu_items_price mip1 WHERE mip1.menu_item_id = ssi.menu_item_id);
	
END; // 

