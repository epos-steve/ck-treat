DROP PROCEDURE IF EXISTS `load_checks_temp_by_company`; // 
CREATE PROCEDURE `load_checks_temp_by_company`(cid INT, date1 DATE)
BEGIN
	DROP TABLE IF EXISTS posreports_checks_temp_by_company;
	CREATE TABLE IF NOT EXISTS posreports_checks_temp_by_company LIKE posreports_checks;
	ALTER TABLE posreports_checks_temp_by_company ENGINE = MEMORY;
	INSERT IGNORE INTO posreports_checks_temp_by_company SELECT c.* FROM posreports_checks c INNER JOIN posreports_business b USING (businessid) WHERE c.bill_posted > date1 AND b.companyid = cid ORDER BY c.id ASC;
END; // 

