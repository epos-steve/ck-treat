DROP FUNCTION IF EXISTS `get_onhand_value`; // 
CREATE FUNCTION `get_onhand_value`(bid INT, itemcode VARCHAR(15), datetime_value2 TIMESTAMP) RETURNS int(11)
    READS SQL DATA
BEGIN
	DECLARE onhand_value INT;
	DECLARE onhand_value_2 INT;

	DECLARE get_onhand CURSOR FOR 
	SELECT icv.onhand FROM mburris_businesstrack.posreports_inv_count_vend icv JOIN mburris_businesstrack.posreports_machine_bus_link mbl ON icv.machine_num = mbl.machine_num AND mbl.businessid = bid WHERE item_code = itemcode AND is_inv = '1' AND date_time = datetime_value2;
	DECLARE EXIT HANDLER FOR SQLSTATE '02000' 
	BEGIN
		RETURN '0';
	END;
	
	OPEN get_onhand;
	LOOP
		FETCH get_onhand INTO onhand_value;
			SELECT onhand_value INTO onhand_value_2;
			IF onhand_value_2 IS NULL THEN
			SET onhand_value_2 = 0;
		    END IF;
		RETURN onhand_value_2;
	END LOOP;
	CLOSE get_onhand;
	
END; // 

