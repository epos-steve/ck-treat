DROP FUNCTION IF EXISTS `get_datetime_value`; // 
CREATE FUNCTION `get_datetime_value`(bid INT, itemcode VARCHAR(15), date2 DATE) RETURNS timestamp
    READS SQL DATA
BEGIN
	DECLARE date_time_value TIMESTAMP;
	DECLARE date_time_value_2 TIMESTAMP;
	SELECT date_time INTO date_time_value_2 FROM mburris_businesstrack.posreports_inv_count_vend icv JOIN mburris_businesstrack.posreports_machine_bus_link mbl ON icv.machine_num = mbl.machine_num AND mbl.businessid = bid WHERE (item_code = itemcode) AND (is_inv = '1') AND (date_time <= date2) ORDER BY date_time DESC LIMIT 1;
	RETURN date_time_value_2;	
END; // 

