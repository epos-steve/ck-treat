DROP FUNCTION IF EXISTS `get_districtid_value`; // 
CREATE FUNCTION `get_districtid_value`(bid INT) RETURNS int(11)
    READS SQL DATA
BEGIN
	DECLARE districtid_value VARCHAR (10);
	DECLARE districtid_value_2 VARCHAR (10);
    
	DECLARE get_districtid_numbers CURSOR FOR SELECT districtid FROM mburris_businesstrack.posreports_business WHERE businessid = bid;
	DECLARE EXIT HANDLER FOR SQLSTATE '02000'
	BEGIN 
		RETURN ''; 
	END;

	OPEN get_districtid_numbers;
		FETCH get_districtid_numbers INTO districtid_value;
	RETURN districtid_value;
	CLOSE get_districtid_numbers;
END; // 

