DROP PROCEDURE IF EXISTS `load_iw_checks_by_company`; // 
CREATE PROCEDURE `load_iw_checks_by_company`(cid INT, date1 DATE, date2 DATE)
BEGIN
	SET @s1 = CONCAT("DROP TABLE IF EXISTS iw_posreports_checks_cid_", cid);
	PREPARE stmt1 FROM @s1;
	EXECUTE stmt1;

	SET @s2 = CONCAT("CREATE TEMPORARY TABLE IF NOT EXISTS iw_posreports_checks_cid_", cid, " LIKE posreports_checks");
	PREPARE stmt2 FROM @s2;
	EXECUTE stmt2;

	SELECT DATE_ADD(date1, INTERVAL -2 DAY) INTO date1;
	SELECT DATE_ADD(date2, INTERVAL 2 DAY) INTO date2;

	SET @var1 = date1;
	SET @var2 = date2;
		
	SET @s3 = CONCAT("INSERT IGNORE INTO iw_posreports_checks_cid_", cid, " SELECT c.* FROM posreports_checks c INNER JOIN posreports_business b USING (businessid) WHERE (c.bill_datetime >= ? AND c.bill_datetime <= ?) AND b.companyid = ", cid, " ORDER BY c.id ASC");

	PREPARE stmt3 FROM @s3;
	EXECUTE stmt3 USING @var1, @var2;
	
END; // 

