DROP PROCEDURE IF EXISTS `load_iw_inv_count_vend_by_company`; // 
CREATE PROCEDURE `load_iw_inv_count_vend_by_company`(cid INT, date1 DATE, date2 DATE)
BEGIN
	SET @s1 = CONCAT("DROP TABLE IF EXISTS iw_posreports_inv_count_vend_by_company_", cid);
	PREPARE stmt1 FROM @s1;
	EXECUTE stmt1;
	
	SET @s2 = CONCAT("CREATE TEMPORARY TABLE IF NOT EXISTS iw_posreports_inv_count_vend_by_company_", cid, " LIKE posreports_inv_count_vend");
	PREPARE stmt2 FROM @s2;
	EXECUTE stmt2;
	
	SELECT DATE_ADD(date1, INTERVAL -2 DAY) INTO date1;
	SELECT DATE_ADD(date2, INTERVAL 2 DAY) INTO date2;
	
	SET @var1 = date1;
	SET @var2 = date2;
	
	SET @s3 = CONCAT("INSERT IGNORE INTO iw_posreports_inv_count_vend_by_company_", cid, " SELECT icv.* FROM posreports_inv_count_vend icv INNER JOIN posreports_machine_bus_link mbl USING (machine_num) INNER JOIN posreports_business b USING (businessid) WHERE (icv.date_time >= ? AND icv.date_time < ?) AND b.companyid = ", cid);

	PREPARE stmt3 FROM @s3;
	EXECUTE stmt3 USING @var1, @var2;
	
END; // 

