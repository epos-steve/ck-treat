DROP FUNCTION IF EXISTS `iw_get_waste_value`; // 
CREATE FUNCTION `iw_get_waste_value`(bid INT, itemcode VARCHAR(20), value1 DATE, value2 DATE) RETURNS int(11)
    READS SQL DATA
BEGIN
	DECLARE waste_value_1 INT;

	SELECT SUM(icv.waste) INTO waste_value_1 FROM posreports_icv_temp icv JOIN posreports_machine_bus_link mbl ON icv.machine_num = mbl.machine_num AND mbl.businessid = bid WHERE (icv.item_code = itemcode) AND (icv.is_inv = 0) AND (icv.date_time BETWEEN value1 AND value2);
  
	IF waste_value_1 IS NULL THEN
		SELECT 0 INTO waste_value_1;
	END IF;
	RETURN waste_value_1 ;
END; // 
