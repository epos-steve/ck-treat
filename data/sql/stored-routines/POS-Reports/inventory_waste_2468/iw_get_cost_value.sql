DROP FUNCTION IF EXISTS `iw_get_cost_value`; // 
CREATE FUNCTION `iw_get_cost_value`(bid INT, itemcode_value VARCHAR(40), value1 DATE, value2 DATE) RETURNS decimal(6,2)
    READS SQL DATA
BEGIN
		DECLARE cost_value_new DECIMAL (6,2) DEFAULT 0.00;
		
		SELECT icv.unit_cost INTO cost_value_new FROM posreports_menu_items_new mitn JOIN posreports_machine_bus_link mbl ON mbl.ordertype = mitn.ordertype AND mbl.businessid = mitn.businessid JOIN posreports_icv_temp icv ON icv.machine_num = mbl.machine_num AND (icv.date_time BETWEEN value1 AND value2) AND (icv.item_code = mitn.item_code) WHERE mitn.businessid = bid AND (mitn.item_code = itemcode_value) AND icv.waste > 0 LIMIT 1;
		
		IF cost_value_new IS NULL THEN
			SELECT 0 INTO cost_value_new;
		END IF;

	RETURN cost_value_new;
END; // 

