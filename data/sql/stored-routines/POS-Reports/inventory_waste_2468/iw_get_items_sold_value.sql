DROP FUNCTION IF EXISTS `iw_get_items_sold_value`; // 
CREATE FUNCTION `iw_get_items_sold_value`(bid INT, item_price_pos_id_value INT, value1 DATE, value2 DATE) RETURNS decimal(10,0)
    READS SQL DATA
BEGIN
	DECLARE items_sold_value_2 DECIMAL (8,3);

	SELECT SUM(cd.quantity) INTO items_sold_value_2 FROM posreports_checkdetail cd, posreports_checks c WHERE (cd.item_number = item_price_pos_id_value) AND (cd.businessid = bid) AND (cd.bill_number = c.check_number) AND (c.businessid = bid) AND (c.bill_datetime BETWEEN value1 AND value2);
  
	IF items_sold_value_2 IS NULL THEN
		SELECT 0 INTO items_sold_value_2;
	END IF;
		  
	RETURN items_sold_value_2;
END; // 

