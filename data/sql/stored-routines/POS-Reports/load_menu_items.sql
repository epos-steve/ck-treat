DROP PROCEDURE IF EXISTS `load_menu_items`//
CREATE PROCEDURE `load_menu_items`(cid INT)
BEGIN
	DECLARE bid, totals, did, pos_id_value INT;
	DECLARE menu_items_new_active TINYINT(1) DEFAULT 0;
	DECLARE menu_item_new_item_code VARCHAR(30);
	DECLARE menu_item_new_name, menu_group_new_group_name, busname, dname, companyname_value VARCHAR(100);

	DECLARE menu_items_data CURSOR FOR SELECT bus.businessid, mitn.item_code, mitn.name, mgn.name AS group_name, mitn.active, mitn.pos_id, bus.businessname, bus.districtid, d.districtname  FROM mburris_businesstrack.posreports_menu_items_new mitn JOIN mburris_businesstrack.posreports_business bus ON bus.businessid = mitn.businessid JOIN mburris_businesstrack.posreports_district d USING (districtid) LEFT JOIN mburris_businesstrack.menu_groups_new mgn ON mitn.group_id = mgn.id WHERE bus.companyid = cid AND bus.categoryid = 4 ORDER BY mitn.group_id, mitn.name;
	OPEN menu_items_data;
	BEGIN
		DECLARE EXIT HANDLER FOR SQLSTATE '02000' BEGIN END;
		DELETE FROM mburris_businesstrack.load_menu_items WHERE companyid = cid;
		SELECT companyname INTO companyname_value FROM company WHERE companyid = cid;
		LOOP
			FETCH menu_items_data INTO bid, menu_item_new_item_code, menu_item_new_name, menu_group_new_group_name, menu_items_new_active, pos_id_value, busname, did, dname;
			INSERT IGNORE INTO mburris_businesstrack.load_menu_items (companyid, companyname, businessid, min_item_code, min_name, mgn_group_name, min_active, businessname, districtid, districtname, pos_id) VALUES (cid, companyname_value, bid, menu_item_new_item_code, menu_item_new_name, menu_group_new_group_name, menu_items_new_active, busname, did, dname, pos_id_value);
		END LOOP;
	END;
	CLOSE menu_items_data;
END; //
