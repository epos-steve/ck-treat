DROP PROCEDURE IF EXISTS `customer_promo`; // 
CREATE PROCEDURE `customer_promo`(cid INT, date1 DATE, date2 DATE)
BEGIN
	DECLARE cid2, bid, did INT;
	DECLARE date2_selected_value DATE;
	DECLARE busname, dname VARCHAR(100);
	DECLARE promo_amount_value DECIMAL (8,2);
	
	declare done int default false;
  
	DECLARE load_business_data CURSOR FOR 
	SELECT bus.businessid, bus.businessname, d.districtid, d.districtname FROM posreports_business bus JOIN posreports_district d ON d.districtid = bus.districtid WHERE bus.companyid = cid AND bus.categoryid = 4 ORDER BY bus.districtid, bus.businessname;
	OPEN load_business_data;
	BEGIN
		DECLARE CONTINUE handler FOR NOT FOUND SET done = TRUE;
		
		SELECT date2 INTO date2_selected_value;
		SELECT DATE_ADD(date2, INTERVAL 1 DAY) INTO date2;
		DELETE FROM mburris_report.customer_promo WHERE (companyid = cid);
   
	done_label: LOOP
		SET done = FALSE;
		FETCH load_business_data INTO bid, busname, did, dname;
		IF done THEN 
			LEAVE done_label;
		END IF;
		
		SELECT SUM(cd.amount) INTO promo_amount_value FROM checkdiscounts cd INNER JOIN posreports_checks c ON c.check_number = cd.check_number AND c.businessid = cd.businessid AND (DATE(c.bill_posted) >= date1) AND (DATE(c.bill_posted) < date2) AND (c.businessid = bid);
		
		IF (promo_amount_value IS NULL) THEN 
			SELECT 0.00 INTO promo_amount_value;
		END IF;

		INSERT INTO mburris_report.customer_promo (companyid, date_start, date_end, districtid, districtname, businessid, businessname, promo_amount) VALUES (cid, date1, date2_selected_value, did, dname, bid, busname, promo_amount_value);

	END LOOP;
  END;
CLOSE load_business_data;	
END; // 

