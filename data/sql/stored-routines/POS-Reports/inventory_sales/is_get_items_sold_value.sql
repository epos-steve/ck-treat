DROP FUNCTION IF EXISTS `is_get_items_sold_value`; // 
CREATE FUNCTION `is_get_items_sold_value`(bid INT, item_price_pos_id_value INT, value1 DATETIME, value2 DATETIME) 
RETURNS INT 
READS SQL DATA 
BEGIN
	DECLARE items_sold_value_2 INT;

	SELECT SUM(cd.quantity) INTO items_sold_value_2 FROM mburris_businesstrack.posreports_checkdetail AS cd INNER JOIN mburris_businesstrack.posreports_checks_temp AS ct WHERE (cd.item_number = item_price_pos_id_value) AND (cd.businessid = bid) AND (cd.bill_number = ct.check_number) AND (ct.businessid = bid) AND (ct.bill_posted BETWEEN value1 AND value2);
  
	IF items_sold_value_2 IS NULL THEN
		SELECT 0 INTO items_sold_value_2;
	END IF;
		  
	RETURN items_sold_value_2;
END; // 

