DROP PROCEDURE IF EXISTS `inventory_sales`; // 
CREATE PROCEDURE `inventory_sales`(cid INT, date1 DATE, date2 DATE)
BEGIN
	DECLARE cid2, bid, did, pos_id_value, shelf_life_days_value, items_sold_1 INT;
	DECLARE date2_selected_value DATE;
	DECLARE cost_value, price_value, price_value_new, extended_cost_value, extended_price_value DECIMAL (8,2) DEFAULT 0.00;
	DECLARE itemcode, route_value VARCHAR(30);
	DECLARE item_name_value, dname, busname VARCHAR(200);
	DECLARE group_name_value VARCHAR(200);
	DECLARE done INT DEFAULT FALSE;

	DECLARE load_menu_items_data CURSOR FOR SELECT lmi.companyid, lmi.businessid, lmi.min_item_code, lmi.min_name, lmi.mgn_group_name FROM mburris_businesstrack.load_menu_items lmi WHERE lmi.companyid = cid ORDER BY lmi.businessid ASC, lmi.min_name ASC;

	OPEN load_menu_items_data;
	BEGIN
		DECLARE CONTINUE handler FOR NOT FOUND SET done = TRUE;
		
		CALL load_is_checks_by_company(cid, date1, date2);
	
		SET @s4 = CONCAT("SELECT (sum(price * quantity)/sum(quantity)) INTO @price_value FROM mburris_businesstrack.posreports_checkdetail AS cd INNER JOIN mburris_businesstrack.is_posreports_checks_cid_", cid, " AS ct WHERE (cd.item_number = ?) AND (cd.businessid = ?) AND (cd.bill_number = ct.check_number) AND (ct.businessid = ?) AND (ct.bill_posted BETWEEN ? AND ?)"); 
		PREPARE stmt4 FROM @s4;

		SET @s5 = CONCAT("SELECT (sum(cost * quantity)/sum(quantity)) INTO @cost_value FROM mburris_businesstrack.posreports_checkdetail AS cd INNER JOIN mburris_businesstrack.is_posreports_checks_cid_", cid, " AS ct WHERE (cd.item_number = ?) AND (cd.businessid = ?) AND (cd.bill_number = ct.check_number) AND (ct.businessid = ?) AND (ct.bill_posted BETWEEN ? AND ?)"); 
		PREPARE stmt5 FROM @s5;
		
		SET @s6 = CONCAT("SELECT SUM(cd.quantity) INTO @items_sold_1 FROM mburris_businesstrack.posreports_checkdetail AS cd INNER JOIN mburris_businesstrack.is_posreports_checks_cid_", cid, " AS ct WHERE (cd.item_number = ?) AND (cd.businessid = ?) AND (cd.bill_number = ct.check_number) AND (ct.businessid = ?) AND (ct.bill_posted BETWEEN ? AND ?)"); 
		PREPARE stmt6 FROM @s6;

		SET @s7 = CONCAT("SELECT SUM(price * quantity) INTO @extended_price_value FROM mburris_businesstrack.posreports_checkdetail AS cd INNER JOIN mburris_businesstrack.is_posreports_checks_cid_", cid, " AS ct WHERE (cd.item_number = ?) AND (cd.businessid = ?) AND (cd.bill_number = ct.check_number) AND (ct.businessid = ?) AND (ct.bill_posted BETWEEN ? AND ?)"); 
		PREPARE stmt7 FROM @s7;

		SET @s8 = CONCAT("SELECT SUM(cost * quantity) INTO @extended_cost_value FROM mburris_businesstrack.posreports_checkdetail AS cd INNER JOIN mburris_businesstrack.is_posreports_checks_cid_", cid, " AS ct WHERE (cd.item_number = ?) AND (cd.businessid = ?) AND (cd.bill_number = ct.check_number) AND (ct.businessid = ?) AND (ct.bill_posted BETWEEN ? AND ?)"); 
		PREPARE stmt8 FROM @s8;
		
		SELECT date2 INTO date2_selected_value;
		SELECT DATE_ADD(date2, INTERVAL 1 DAY) INTO date2;
		DELETE FROM mburris_report.inventory_sales WHERE (companyid = cid) AND (date_start = date1) AND (date_end = date2_selected_value);

		done_label: LOOP
			SET done = FALSE;
			FETCH load_menu_items_data INTO cid2, bid, itemcode, item_name_value, group_name_value;
			
				IF done THEN 
					LEAVE done_label;
				END IF;
				
				SELECT mitn.pos_id, mitn.shelf_life_days INTO pos_id_value, shelf_life_days_value FROM mburris_businesstrack.posreports_menu_items_new mitn WHERE mitn.businessid = bid AND mitn.item_code = itemcode LIMIT 1;
				SELECT districtid INTO did FROM business WHERE businessid = bid LIMIT 1;
				SELECT businessname INTO busname FROM business WHERE businessid = bid LIMIT 1;
				SELECT districtname INTO dname FROM district WHERE districtid = did;

				SET @bid_value = bid;
				SET @pos_id_value = pos_id_value;
				SET @date1 = date1;
				SET @date2 = date2;
				
				EXECUTE stmt4 USING @pos_id_value, @bid_value, @bid_value, @date1, @date2;
				SELECT @price_value INTO price_value;

				IF price_value IS NULL THEN
					SELECT 0 INTO price_value;
				END IF;

				EXECUTE stmt5 USING @pos_id_value, @bid_value, @bid_value, @date1, @date2;
				SELECT @cost_value INTO cost_value;
				
				IF cost_value IS NULL THEN
					SELECT 0 INTO cost_value;
				END IF;
				
				EXECUTE stmt6 USING @pos_id_value, @bid_value, @bid_value, @date1, @date2;
				SELECT @items_sold_1 INTO items_sold_1;

				IF items_sold_1 IS NULL THEN
					SELECT 0 INTO items_sold_1;
				END IF;
				
				
				IF (items_sold_1 IS NOT NULL) AND (items_sold_1 > 0) THEN
					SELECT get_route_value(bid, itemcode) INTO route_value;

					EXECUTE stmt7 USING @pos_id_value, @bid_value, @bid_value, @date1, @date2;
					SELECT @extended_price_value INTO extended_price_value;

					EXECUTE stmt8 USING @pos_id_value, @bid_value, @bid_value, @date1, @date2;
					SELECT @extended_cost_value INTO extended_cost_value;
					
					INSERT IGNORE INTO mburris_report.inventory_sales (companyid, date_start, date_end, posid, districtid, districtname, businessid, businessname, group_name, item_code, item_name, cost, price, items_sold, extended_cost, extended_price, shelf_life_days, route) VALUES (cid, date1, date2_selected_value, pos_id_value, did, dname, bid, busname, group_name_value, itemcode, item_name_value, cost_value, price_value, items_sold_1, extended_cost_value, extended_price_value, shelf_life_days_value, route_value);

				END IF;
				
		END LOOP;

	END;
CLOSE load_menu_items_data;
END; // 

