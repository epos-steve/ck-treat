DROP PROCEDURE IF EXISTS `load_is_checkdetail_by_company`; // 
CREATE PROCEDURE `load_is_checkdetail_by_company`(cid INT)
BEGIN

	DECLARE id_value, businessid_value, item_number_value INT;
	DECLARE bill_number_value BIGINT;
	DECLARE quantity_value, is_void_value TINYINT;
	DECLARE price_value, cost_value DECIMAL(10,2);

	DECLARE checkdetail_data_by_company CURSOR FOR SELECT c.id, c.businessid, c.bill_number, c.item_number, c.quantity, c.price, c.cost, c.is_void FROM posreports_checkdetail c INNER JOIN posreports_business b USING (businessid) WHERE b.companyid = cid ORDER BY c.id ASC;
	OPEN checkdetail_data_by_company;
	BEGIN
		DECLARE EXIT HANDLER FOR SQLSTATE '02000' BEGIN END;

		SET @s1a = CONCAT("DROP TABLE IF EXISTS is_posreports_checkdetail_cid_", cid);
		PREPARE stmt1a FROM @s1a;
		EXECUTE stmt1a;

		SET @s2a = CONCAT("CREATE TABLE IF NOT EXISTS is_posreports_checkdetail_cid_", cid, " LIKE posreports_checkdetail");
		PREPARE stmt2a FROM @s2a;
		EXECUTE stmt2a;		

		SET @s2b = CONCAT("ALTER TABLE is_posreports_checkdetail_cid_", cid, " DROP INDEX businessid_2");
		PREPARE stmt2b FROM @s2b;
		EXECUTE stmt2b;

		SET @s3a = CONCAT("INSERT IGNORE INTO is_posreports_checkdetail_cid_", cid, " (id, businessid, bill_number, item_number, quantity, price, cost, is_void) SELECT ?, ?, ?, ?, ?, ?, ?, ?");
		PREPARE stmt3a FROM @s3a;
	
		LOOP
			FETCH checkdetail_data_by_company INTO id_value, businessid_value, bill_number_value, item_number_value, quantity_value, price_value, cost_value, is_void_value;

			SET @id_value = id_value;
			SET @businessid_value = businessid_value;
			SET @bill_number_value = bill_number_value;
			SET @item_number_value = item_number_value;
			SET @quantity_value = quantity_value;
			SET @price_value = price_value;
			SET @cost_value = cost_value;
			SET @is_void_value = is_void_value;
			
			EXECUTE stmt3a USING @id_value, @businessid_value, @bill_number_value, @item_number_value, @quantity_value, @price_value, @cost_value, @is_void_value;			

		END LOOP;
	END;
	CLOSE checkdetail_data_by_company;

END; // 

