DROP PROCEDURE IF EXISTS `load_checkdetail_by_company`; // 
CREATE PROCEDURE `load_checkdetail_by_company`(cid INT)
BEGIN
	SET @s1a = CONCAT("DROP TABLE IF EXISTS posreports_checkdetail_cid_", cid);
	PREPARE stmt1a FROM @s1a;
	EXECUTE stmt1a;

	SET @s2a = CONCAT("CREATE TEMPORARY TABLE IF NOT EXISTS posreports_checkdetail_cid_", cid, " LIKE posreports_checkdetail");
	PREPARE stmt2a FROM @s2a;
	EXECUTE stmt2a;
	
	SET @s3a = CONCAT("INSERT IGNORE INTO posreports_checkdetail_cid_", cid, " SELECT c.* FROM posreports_checkdetail c INNER JOIN posreports_business b USING (businessid) WHERE b.companyid = ", cid, " ORDER BY c.id ASC");

	PREPARE stmt3a FROM @s3a;
	EXECUTE stmt3a;
	
END; // 

