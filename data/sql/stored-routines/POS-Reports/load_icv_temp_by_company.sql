DROP PROCEDURE IF EXISTS `load_icv_temp_by_company`//
CREATE PROCEDURE `load_icv_temp_by_company`(cid INT)
BEGIN
	SET @s1 = CONCAT("DROP TABLE IF EXISTS posreports_icv_temp_by_company_", cid);
	PREPARE stmt1 FROM @s1;
	EXECUTE stmt1;
	
	SET @s2 = CONCAT("CREATE TEMPORARY TABLE IF NOT EXISTS posreports_icv_temp_by_company_", cid, " LIKE posreports_inv_count_vend");
	PREPARE stmt2 FROM @s2;
	EXECUTE stmt2;
	
	SET @s3 = CONCAT("INSERT IGNORE INTO posreports_icv_temp_by_company_", cid, " SELECT icv.* FROM posreports_inv_count_vend icv INNER JOIN posreports_machine_bus_link mbl USING (machine_num) INNER JOIN posreports_business b USING (businessid) WHERE b.companyid = ", cid);
	PREPARE stmt3 FROM @s3;
	EXECUTE stmt3;
	
END; //
