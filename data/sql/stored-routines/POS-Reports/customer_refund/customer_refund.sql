DROP PROCEDURE IF EXISTS `customer_refund`; // 
CREATE PROCEDURE `customer_refund`(cid INT, date1 DATE, date2 DATE)
BEGIN
	DECLARE cid2, bid, did INT;
	DECLARE date2_selected_value DATE;
	DECLARE busname, dname VARCHAR(100);
	DECLARE refund_total_amount DECIMAL (8,2);
	
	declare done int default false;
  
	DECLARE load_business_data CURSOR FOR 
	SELECT bus.businessid, bus.businessname, d.districtid, d.districtname FROM posreports_business bus JOIN posreports_district d ON d.districtid = bus.districtid WHERE bus.companyid = cid AND bus.categoryid = 4 ORDER BY bus.districtid, bus.businessname;
	OPEN load_business_data;
	BEGIN
		DECLARE CONTINUE handler FOR NOT FOUND SET done = TRUE;
		
		SELECT date2 INTO date2_selected_value;
		SELECT DATE_ADD(date2, INTERVAL 1 DAY) INTO date2;
		DELETE FROM mburris_report.customer_refund WHERE (companyid = cid);
   
	done_label: LOOP
		SET done = FALSE;
		FETCH load_business_data INTO bid, busname, did, dname;
		IF done THEN 
			LEAVE done_label;
		END IF;
		
		SELECT SUM(ct.subtotal) INTO refund_total_amount FROM customer_transactions ct INNER JOIN customer c ON c.customerid = ct.customer_id AND c.businessid = bid WHERE DATE(ct.date_created) >= date1 AND DATE(ct.date_created) < date2 AND ct.trans_type_id = 3;
		
		IF (refund_total_amount IS NULL) THEN 
			SELECT 0.00 INTO refund_total_amount;
		END IF;

		INSERT INTO mburris_report.customer_refund (companyid, date_start, date_end, districtid, districtname, businessid, businessname, refund_total) VALUES (cid, date1, date2_selected_value, did, dname, bid, busname, refund_total_amount);

	END LOOP;
  END;
CLOSE load_business_data;	
END; // 

