DROP PROCEDURE IF EXISTS `load_menu_items_inv_detail`//
CREATE PROCEDURE `load_menu_items_inv_detail`(cid INT)
BEGIN
	DECLARE bid, totals, did, pos_id_value, shelf_life_days_value INT;
	DECLARE menu_items_new_active TINYINT(1) DEFAULT 0;
	DECLARE cost_value, cost_value_mip, cost_value_new, price_value, amount_value DECIMAL (20,2);
	DECLARE menu_item_new_item_code, machine_num_value VARCHAR(30);
	DECLARE menu_item_new_name, menu_group_new_group_name, busname, dname, companyname_value VARCHAR(100);
	DECLARE done INT DEFAULT FALSE;
	
	DECLARE menu_items_data CURSOR FOR SELECT bus.businessid, mitn.item_code, mitn.name, mgn.name AS group_name, mitn.active, mitn.pos_id, bus.businessname, bus.districtid, d.districtname, mitn.shelf_life_days, mip.cost, mip.price, mbl.machine_num FROM mburris_businesstrack.posreports_menu_items_new mitn JOIN mburris_businesstrack.posreports_menu_items_price mip ON mitn.id = mip.menu_item_id JOIN mburris_businesstrack.posreports_business bus ON bus.businessid = mitn.businessid JOIN mburris_businesstrack.posreports_district d USING (districtid) JOIN mburris_businesstrack.posreports_machine_bus_link mbl ON mitn.businessid = mbl.businessid AND mitn.ordertype = mbl.ordertype LEFT JOIN mburris_businesstrack.menu_groups_new mgn ON mitn.group_id = mgn.id WHERE bus.companyid = cid AND bus.categoryid = 4 ORDER BY mitn.group_id, mitn.name;
	
	OPEN menu_items_data;
	BEGIN

		DECLARE CONTINUE handler FOR NOT FOUND SET done = TRUE;
		
		DELETE FROM mburris_businesstrack.load_menu_items_inv_detail WHERE companyid = cid;
		SELECT companyname INTO companyname_value FROM company WHERE companyid = cid;
		done_label: LOOP
		
			SET done = FALSE;
			
			FETCH menu_items_data INTO bid, menu_item_new_item_code, menu_item_new_name, menu_group_new_group_name, menu_items_new_active, pos_id_value, busname, did, dname, shelf_life_days_value, cost_value, price_value, machine_num_value;

			IF done THEN 
				LEAVE done_label;
			END IF;
			
			INSERT IGNORE INTO mburris_businesstrack.load_menu_items_inv_detail (companyid, companyname, businessid, min_item_code, min_name, mgn_group_name, min_active, businessname, districtid, districtname, pos_id, shelf_life_days, cost, price, machine_num) VALUES (cid, companyname_value, bid, menu_item_new_item_code, menu_item_new_name, menu_group_new_group_name, menu_items_new_active, busname, did, dname, pos_id_value, shelf_life_days_value, cost_value, price_value, machine_num_value);
			
		END LOOP;
	END;
	CLOSE menu_items_data;
END; //
