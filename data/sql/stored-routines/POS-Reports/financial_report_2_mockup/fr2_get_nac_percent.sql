DROP FUNCTION IF EXISTS `fr2_get_nac_percent`; // 
CREATE FUNCTION `fr2_get_nac_percent`(bid INT) RETURNS decimal(20,2)
    READS SQL DATA
BEGIN
	DECLARE nac_fee_percent_value DECIMAL (20,2) DEFAULT 0.00;
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000'  
	BEGIN 
		RETURN 0;
	END;

	SELECT `value` INTO nac_fee_percent_value FROM setup_detail WHERE setupid = 69 AND businessid = bid;
	
	IF (nac_fee_percent_value < 1) THEN 
		SELECT 0 INTO nac_fee_percent_value;
	END IF;

	RETURN nac_fee_percent_value;
END; // 

