DROP FUNCTION IF EXISTS `dateofnextweekday`; // 
CREATE FUNCTION `dateofnextweekday`( d date, which tinyint ) RETURNS date
    READS SQL DATA
begin 
  declare today tinyint; 
  set today = dayofweek(d); 
  return adddate( d, if( today=which,7,if(today<which,which-today,7+which-today))); 
end; // 

