DROP FUNCTION IF EXISTS `fr2_ck_monthly_fee`; // 
CREATE FUNCTION `fr2_ck_monthly_fee`(bid INT, date1 DATE, date2 DATE) RETURNS decimal(20,2)
    READS SQL DATA
BEGIN
	DECLARE days_different, number_of_kiosk INT DEFAULT 0;
	DECLARE ck_fee_monthly_value, ck_fee_monthly_value_2 DECIMAL (20,2) DEFAULT 0.00;
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000'  
	BEGIN 
		RETURN 0;
	END;
	
	SELECT `s1`.`value` INTO number_of_kiosk FROM setup_detail s1 WHERE s1.setupid = 41 AND s1.businessid = bid;
	SELECT `s1`.`value` INTO ck_fee_monthly_value FROM setup_detail s1 WHERE s1.setupid = 71 AND s1.businessid = bid;
	
	IF (number_of_kiosk > 0) THEN 
		SELECT (number_of_kiosk * ck_fee_monthly_value) INTO ck_fee_monthly_value;
	ELSE
		SELECT ck_fee_monthly_value INTO ck_fee_monthly_value;
	END IF;
	
	SELECT DATEDIFF(date2, date1) INTO days_different;
	IF (days_different > 7) THEN 
		SELECT ck_fee_monthly_value INTO ck_fee_monthly_value_2;
	ELSE
		SELECT ((ck_fee_monthly_value * 12) / 52) INTO ck_fee_monthly_value_2;
	END IF;

	RETURN ck_fee_monthly_value_2;
END; // 

