DROP FUNCTION IF EXISTS `fr2_get_cash_received`; // 
CREATE FUNCTION `fr2_get_cash_received`(bid INT, date1 DATE, date2 DATE) RETURNS decimal(20,2)
    READS SQL DATA
BEGIN
	DECLARE cash_received_value DECIMAL (20,2);
		
	SELECT SUM(dd.amount) INTO cash_received_value FROM debitdetail dd JOIN debits d ON d.debitid = dd.debitid AND d.businessid = bid AND d.category = 10 WHERE dd.`date` >= date1 AND dd.`date` < date2;
		
	IF cash_received_value IS NULL THEN
		SELECT 0 INTO cash_received_value;
	END IF;

	RETURN cash_received_value;
END; // 

