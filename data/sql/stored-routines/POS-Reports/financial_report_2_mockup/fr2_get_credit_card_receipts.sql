DROP FUNCTION IF EXISTS `fr2_get_credit_card_receipts`; // 
CREATE FUNCTION `fr2_get_credit_card_receipts`(bid INT, date1 DATE, date2 DATE) RETURNS decimal(20,2)
    READS SQL DATA
BEGIN
	DECLARE credit_card_receipts_value DECIMAL (20,2);
		
	SELECT SUM(dd.amount) INTO credit_card_receipts_value FROM debitdetail dd JOIN debits d ON d.debitid = dd.debitid AND d.businessid = bid AND d.category IN (11,12) WHERE dd.`date` >= date1 AND dd.`date` < date2;
		
	IF credit_card_receipts_value IS NULL THEN
		SELECT 0 INTO credit_card_receipts_value;
	END IF;

	RETURN credit_card_receipts_value;
END; // 

