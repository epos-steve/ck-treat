DROP FUNCTION IF EXISTS `fr2_ck_refunds`; // 
CREATE FUNCTION `fr2_ck_refunds`(bid INT, date1 DATE, date2 DATE) RETURNS decimal(20,2)
    READS SQL DATA
BEGIN
	DECLARE creditid_value INT DEFAULT 0;
	DECLARE ck_refunds_value DECIMAL (20,2) DEFAULT 0.00;
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000'  
	BEGIN 
		RETURN 0;
	END;

	SELECT creditid INTO creditid_value FROM credits WHERE (businessid = bid) AND (creditname LIKE 'CK Card Refunds');
	SELECT SUM(amount) INTO ck_refunds_value FROM creditdetail cd WHERE (cd.businessid = bid) AND (cd.creditid = creditid_value) AND (cd.`date` >= date1) AND (cd.`date` < date2);
	
	IF (ck_refunds_value IS NULL) THEN 
		SELECT 0 INTO ck_refunds_value;
	END IF;

	RETURN ck_refunds_value;
END; // 

