DROP FUNCTION IF EXISTS `fr2_get_ck_card_sales`; // 
CREATE FUNCTION `fr2_get_ck_card_sales`(bid INT, date1 DATE, date2 DATE) RETURNS decimal(20,2)
    READS SQL DATA
BEGIN
	DECLARE ck_card_sales_value DECIMAL (20,2);
		
	SELECT SUM(dd.amount) INTO ck_card_sales_value FROM debitdetail dd JOIN debits d ON d.debitid = dd.debitid AND d.businessid = bid AND d.category = 2 WHERE dd.`date` >= date1 AND dd.`date` < date2;
		
	IF ck_card_sales_value IS NULL THEN
		SELECT 0 INTO ck_card_sales_value;
	END IF;

	RETURN ck_card_sales_value;
END; // 

