DROP FUNCTION IF EXISTS `fr2_get_credit_card_fee_111`; // 
CREATE FUNCTION `fr2_get_credit_card_fee_111`(bid INT, date1 DATE, date2 DATE) RETURNS decimal(20,2)
    READS SQL DATA
BEGIN
	DECLARE credit_card_fee_value DECIMAL (20,2);
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000'  
	BEGIN 
		RETURN 0;
	END;

	SELECT sum(cf.amount) INTO credit_card_fee_value FROM cc_fee cf WHERE cf.businessid = bid AND cf.`date` >= date1 AND cf.`date` < date2  LIMIT 1;
		
	IF credit_card_fee_value IS NULL THEN
		SELECT 0 INTO credit_card_fee_value;
	END IF;

	RETURN credit_card_fee_value;
END; // 

