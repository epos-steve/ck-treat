DROP FUNCTION IF EXISTS `fr2_get_credit_card_fee`; // 
CREATE FUNCTION `fr2_get_credit_card_fee`(bid INT, date1 DATE, date2 DATE) RETURNS decimal(20,2)
    READS SQL DATA
BEGIN
	DECLARE credit_card_fee_value DECIMAL (20,2);
		
	SELECT SUM(amount) INTO credit_card_fee_value FROM cc_fee cf WHERE cf.businessid = bid AND cf.`date` >= date1 AND cf.`date` < date2;
		
	IF credit_card_fee_value IS NULL THEN
		SELECT 0 INTO credit_card_fee_value;
	END IF;

	RETURN credit_card_fee_value;
END; // 

