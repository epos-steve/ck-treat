DROP PROCEDURE IF EXISTS `financial_reports_2_mockup`; // 
CREATE PROCEDURE `financial_reports_2_mockup`(cid INT, date1 DATE, date2 DATE)
BEGIN
	DECLARE bid, did, number_of_days INT;
	DECLARE date2_selected_value DATE;
	DECLARE net_sales, taxes, cost, waste, shrink, gross_sales, waste_percent, shrink_percent, cost_waste, cost_waste_percent, cost_waste_shrink, cost_waste_shrink_percent, spoilage, refunds_value, initial_promo_value DECIMAL (20,2) DEFAULT 0.00;
	DECLARE busname VARCHAR(250);
	DECLARE dname VARCHAR(250);
	DECLARE done boolean DEFAULT false;
	DECLARE beginning_ck_card_balance_value, net_balances_value, cash_received_value, credit_card_receipts_value, credit_card_receipts_percent_value, ending_ck_card_balance_value, ck_card_sales_value, credit_card_sales_value, credit_card_sales_percent_value, bottle_deposits_value, cost_percent_value, unfunded_promotions_value, unfunded_promotion_percent_value, credit_card_fee_value, credit_card_fee_percent_value, ck_fee_value, ck_monthly_maintenance_fee_value, ck_fees, ck_fee_percent_value, ck_fee_percent_value_2, operating_profit_value, operating_profit_percent_value, credit_card_fee_value_1, credit_card_fee_value_2, payroll_deduct_value, nac_fee_value, nac_fee_percent_value DECIMAL (20,2) DEFAULT 0.00;
  
	DECLARE districts_and_business_data CURSOR FOR SELECT bus.businessid, bus.businessname, d.districtid, d.districtname FROM mburris_businesstrack.posreports_business bus JOIN mburris_businesstrack.posreports_district d ON d.districtid = bus.districtid WHERE bus.companyid = cid AND bus.categoryid = 4 ORDER BY bus.districtid ,bus.businessname;
	OPEN districts_and_business_data;
	BEGIN

		CALL inventory_waste_financial_extras(cid, date1, date2);
		
		SELECT date2 INTO date2_selected_value;
		SELECT DATE_ADD(date2, INTERVAL 1 DAY) INTO date2;
		DELETE FROM mburris_report.financial_report_2_mockup WHERE (companyid = cid);
		
		loop1: LOOP
			BEGIN 
				DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' 
				BEGIN 
					SET done = true;
				END;
				
				FETCH districts_and_business_data INTO bid, busname, did, dname;
			END;
			
			IF done THEN 
				LEAVE loop1;
			END IF;
			
			SELECT SUM(cd.amount) INTO net_sales FROM mburris_businesstrack.creditdetail cd  JOIN mburris_businesstrack.credits c ON c.creditid = cd.creditid AND c.businessid = bid AND c.credittype = 1 WHERE (cd.`date`) >= date1 AND (cd.`date` < date2);
			IF net_sales IS NULL THEN
				SELECT 0 INTO net_sales;
			END IF;
			
			SELECT fr2_get_bottle_deposits(bid, date1, date2) INTO bottle_deposits_value;
			
			SELECT SUM(cd.amount) INTO taxes FROM mburris_businesstrack.creditdetail cd JOIN mburris_businesstrack.credits c ON c.creditid = cd.creditid AND c.businessid = bid AND c.credittype = 2 WHERE (cd.`date`) >= date1 AND (cd.`date` < date2);
			IF taxes IS NULL THEN
				SELECT 0 INTO taxes;
			END IF;
			SELECT (taxes - bottle_deposits_value) INTO taxes;
			
			SELECT SUM(d.`cos`) INTO cost FROM mburris_businesstrack.dashboard d WHERE d.businessid = bid AND (d.`date` >= date1) AND (d.`date` < date2);
			IF cost IS NULL THEN
				SELECT 0 INTO cost;
			END IF;

SELECT SUM(iw.amount) INTO waste FROM mburris_report.inventory_waste_financial_extras iw WHERE (iw.businessid = bid) AND (iw.date_start = date1) AND (iw.date_end = date2_selected_value);
IF waste IS NULL THEN
SELECT 0 INTO waste;
END IF;

-- SELECT SUM(dashboard.waste) INTO waste FROM mburris_businesstrack.dashboard WHERE businessid = bid AND (date >= date1) AND (date < date2);
-- IF waste IS NULL THEN
-- SELECT 0 INTO waste;
-- END IF;
			
			SELECT SUM(d.shrink) INTO shrink FROM mburris_businesstrack.dashboard d WHERE d.businessid = bid AND (d.`date` >= date1) AND (d.`date` < date2);
			IF shrink IS NULL THEN
				SELECT 0 INTO shrink;
			END IF;
			
			SELECT (net_sales + taxes + bottle_deposits_value) INTO gross_sales;
			IF gross_sales IS NULL THEN
				SELECT 0 INTO gross_sales;
			END IF;
			
			SELECT (waste / net_sales) * 100 INTO waste_percent;
			IF waste_percent IS NULL THEN
				SELECT 0 INTO waste_percent;
			END IF;
			
			SELECT (shrink / net_sales) * 100 INTO shrink_percent;
			IF shrink_percent IS NULL THEN
				SELECT 0 INTO shrink_percent;
			END IF;
			
			SELECT (cost + waste) INTO cost_waste;
			IF cost_waste IS NULL THEN
				SELECT 0 INTO cost_waste;
			END IF;
			
			SELECT (cost + waste) / net_sales * 100 INTO cost_waste_percent;
			IF cost_waste_percent IS NULL THEN
				SELECT 0 INTO cost_waste_percent;
			END IF;
			
			SELECT cost + waste + shrink INTO cost_waste_shrink;
			IF cost_waste_shrink IS NULL THEN
				SELECT 0 INTO cost_waste_shrink;
			END IF;
			
			SELECT (cost + Waste + shrink) / net_sales * 100 INTO cost_waste_shrink_percent;
			IF cost_waste_shrink_percent IS NULL THEN
				SELECT 0 INTO cost_waste_shrink_percent;
			END IF;
			
			SELECT fr2_ck_refunds(bid, date1, date2) INTO refunds_value;
			SELECT fr2_ck_initial_promo(bid, date1, date2) INTO initial_promo_value;
			
			
			SELECT fr2_ck_card_balance_value(bid, date1) INTO beginning_ck_card_balance_value;
			SELECT fr2_ck_card_balance_value(bid, date2) INTO ending_ck_card_balance_value;
			
			SELECT fr2_get_payroll_deduct(bid, date1, date2) INTO payroll_deduct_value;

			SELECT (ending_ck_card_balance_value - beginning_ck_card_balance_value) INTO net_balances_value;

			SELECT fr2_get_cash_received(bid, date1, date2) INTO cash_received_value;
			SELECT fr2_get_ck_card_sales(bid, date1, date2) INTO ck_card_sales_value;
			
			SELECT DATEDIFF(date2,date1) INTO number_of_days;
			IF (number_of_days > 7) THEN
				SELECT fr2_get_credit_card_fee(bid, date1, date2) INTO credit_card_fee_value;
			ELSE
--				SELECT fr2_get_credit_card_fee_1(bid, date1, date2) INTO credit_card_fee_value_1;
				SELECT fr2_get_credit_card_fee_111(bid, date1, date2) INTO credit_card_fee_value_1;
				SELECT fr2_get_credit_card_fee_2(bid, date1, date2) INTO credit_card_fee_value_2;
				SELECT (credit_card_fee_value_1 + credit_card_fee_value_2) INTO credit_card_fee_value;
			END IF;

			SELECT fr2_get_credit_card_receipts(bid, date1, date2) INTO credit_card_receipts_value;
			SELECT fr2_get_credit_card_sales(bid, date1, date2) INTO credit_card_sales_value;
			SELECT fr2_get_unfunded_promotions(bid, date1, date2) INTO unfunded_promotions_value;
			SELECT fr2_ck_fee_percent(bid) INTO ck_fee_percent_value_2;
			SELECT fr2_ck_monthly_fee(bid, date1, date2) INTO ck_monthly_maintenance_fee_value;
			
			SELECT credit_card_receipts_value / ((cash_received_value) + (credit_card_receipts_value))  * 100 INTO credit_card_receipts_percent_value;
			SELECT (credit_card_sales_value / gross_sales) * 100 INTO credit_card_sales_percent_value;
			SELECT (cost / net_sales) * 100 INTO cost_percent_value;
			SELECT (unfunded_promotions_value / net_sales) * 100 INTO unfunded_promotion_percent_value;
			SELECT (credit_card_fee_value / net_sales) * 100 INTO credit_card_fee_percent_value;

			SELECT (ck_fee_percent_value_2 / 100) INTO ck_fee_percent_value;
			SELECT (ck_fee_percent_value * (net_sales - credit_card_fee_value)) INTO ck_fee_value;

			SELECT fr2_get_nac_percent(bid) INTO nac_fee_percent_value;
			SELECT (nac_fee_percent_value * net_sales) / 100 INTO nac_fee_value;
			
			SELECT (net_sales - cost - waste - shrink - credit_card_fee_value - ck_fee_value - unfunded_promotions_value - refunds_value - initial_promo_value  - ck_monthly_maintenance_fee_value - nac_fee_value) INTO operating_profit_value;
			SELECT (operating_profit_value / net_sales) * 100 INTO operating_profit_percent_value;
			
			INSERT IGNORE INTO mburris_report.financial_report_2_mockup (companyid, date_start, date_end, districtid, districtname, businessid, businessname, gross_sales, net_sales, cost, taxes, waste, shrink, waste_percent, shrink_percent, cost_waste, cost_waste_percent, cost_waste_shrink, cost_waste_shrink_percent, beginning_ck_card_balance, cash_received, credit_card_receipts, credit_card_receipts_percent, refunds, initial_promo, ending_ck_card_balance, payroll_deduct, net_balances, ck_card_sales, credit_card_sales, credit_card_sales_percent, bottle_deposits, cost_percent, unfunded_promotions, unfunded_promotion_percent, credit_card_fee, credit_card_fee_percent, ck_fee, ck_fee_percent, ck_monthly_maintenance_fee, operating_profit, operating_profit_percent, nac_fee, nac_fee_percent) VALUES (cid, date1, date2_selected_value, did, dname, bid, busname, gross_sales, net_sales, cost, taxes, waste, shrink, waste_percent, shrink_percent, cost_waste, cost_waste_percent, cost_waste_shrink, cost_waste_shrink_percent, beginning_ck_card_balance_value, cash_received_value, credit_card_receipts_value, credit_card_receipts_percent_value, refunds_value, initial_promo_value, ending_ck_card_balance_value, payroll_deduct_value, net_balances_value, ck_card_sales_value, credit_card_sales_value, credit_card_sales_percent_value, bottle_deposits_value, cost_percent_value, unfunded_promotions_value, unfunded_promotion_percent_value, credit_card_fee_value, credit_card_fee_percent_value, ck_fee_value, ck_fee_percent_value_2, ck_monthly_maintenance_fee_value, operating_profit_value, operating_profit_percent_value, nac_fee_value, nac_fee_percent_value);
		END LOOP;
	END;
	CLOSE districts_and_business_data;
END; // 

