DROP PROCEDURE IF EXISTS `inventory_summary_load_checks_by_company`; // 
CREATE PROCEDURE `inventory_summary_load_checks_by_company`(cid INT, date1 DATE, date2 DATE)
BEGIN

	SELECT DATE_ADD(date1, INTERVAL -1 DAY) INTO date1;
	SELECT DATE_ADD(date2, INTERVAL 1 DAY) INTO date2;

	SET @s1 = CONCAT("DROP TABLE IF EXISTS inventory_summary_posreports_checks_cid_", cid);
	PREPARE stmt1 FROM @s1;
	EXECUTE stmt1;

	SET @s2 = CONCAT("CREATE TEMPORARY TABLE IF NOT EXISTS inventory_summary_posreports_checks_cid_", cid, " LIKE posreports_checks");
	PREPARE stmt2 FROM @s2;
	EXECUTE stmt2;

	SELECT DATE_ADD(date1, INTERVAL -2 DAY) INTO date1;
	SELECT DATE_ADD(date2, INTERVAL 2 DAY) INTO date2;

	SET @var1 = date1;
	SET @var2 = date2;
	
	SET @s3 = CONCAT("INSERT IGNORE INTO inventory_summary_posreports_checks_cid_", cid, " SELECT c.* FROM posreports_checks c INNER JOIN posreports_business b USING (businessid) WHERE b.companyid = ", cid, " AND (c.bill_posted >= ? AND c.bill_posted <= ?) ORDER BY c.id ASC");

	PREPARE stmt3 FROM @s3;
	EXECUTE stmt3 USING @var1, @var2;
	
END; // 

