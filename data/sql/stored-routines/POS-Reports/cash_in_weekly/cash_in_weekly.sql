DROP PROCEDURE IF EXISTS `cash_in_weekly`; // 
CREATE PROCEDURE `cash_in_weekly`(cid INT, date1 DATE, date2 DATE)
BEGIN
	DECLARE cid2, bid, did, machine_collect_totals INT;
	DECLARE date2_selected_value DATE;
	DECLARE date_tracker, date1_value, date2_value, date_last_collect DATE;
	DECLARE datetime_last_collect, datetime_value_2, datetime_4, datetime_5 DATETIME;
	DECLARE busname, dname VARCHAR(100);
	DECLARE day_1_amount, day_2_amount, day_3_amount, day_4_amount, day_5_amount, day_6_amount, day_7_amount DECIMAL (8,2) DEFAULT 0.00;
	
	declare done int default false;
  
	DECLARE load_business_data CURSOR FOR 
	SELECT bus.businessid, bus.businessname, d.districtid, d.districtname FROM posreports_business bus JOIN posreports_district d ON d.districtid = bus.districtid WHERE bus.companyid = cid AND bus.categoryid = 4 ORDER BY bus.districtid, bus.businessname;
	OPEN load_business_data;
	BEGIN
		DECLARE CONTINUE handler FOR NOT FOUND SET done = TRUE;
		
		DROP TEMPORARY TABLE IF EXISTS cash_in_weekly_temp;
		CREATE TEMPORARY TABLE IF NOT EXISTS cash_in_weekly_temp (companyid int(11) DEFAULT NULL, date_time date DEFAULT '0000-00-00', businessid int(11) DEFAULT NULL, day_1 decimal(8,2) DEFAULT '0') ENGINE=MEMORY DEFAULT CHARSET=latin1;

		SELECT date2 INTO date2_selected_value;
		SELECT DATE_ADD(date2, INTERVAL 1 DAY) INTO date2;
		DELETE FROM mburris_report.cash_in_weekly WHERE (companyid = cid);
   
	done_label: LOOP
		SET done = FALSE;
		FETCH load_business_data INTO bid, busname, did, dname;
		IF done THEN 
			LEAVE done_label;
		END IF;

		SET @x = 1;
		SELECT date2 INTO datetime_value_2;
		SELECT datetime_value_2 - INTERVAL 1 SECOND INTO datetime_value_2;
		SELECT date1 INTO date1_value;
		REPEAT 
			
			SELECT DATE(date_time) INTO date_last_collect FROM machine_collect WHERE businessid = bid AND date_time < datetime_value_2 ORDER BY date_time DESC LIMIT 1;			

			IF (DATE(datetime_value_2) = date_last_collect) THEN

				SELECT date_time INTO datetime_4 FROM machine_collect WHERE businessid = bid AND date_time < datetime_value_2 ORDER BY date_time DESC LIMIT 1;
				SELECT date_time INTO datetime_5 FROM machine_collect WHERE businessid = bid AND date_time < datetime_value_2 ORDER BY date_time DESC LIMIT 1,1;

				SELECT SUM( ROUND(posreports_payment_detail.received,2) ) INTO day_1_amount FROM posreports_payment_detail JOIN posreports_checks ON posreports_checks.check_number = posreports_payment_detail.check_number AND posreports_checks.businessid = posreports_payment_detail.businessid AND posreports_checks.businessid = bid AND posreports_checks.bill_posted BETWEEN datetime_5 AND datetime_4 WHERE posreports_payment_detail.payment_type = 1;
				
				INSERT INTO cash_in_weekly_temp (companyid, date_time, businessid, day_1) VALUES (cid, date_last_collect, bid, day_1_amount);

			ELSE 
			
				INSERT INTO cash_in_weekly_temp (companyid, date_time, businessid, day_1) VALUES (cid, DATE(datetime_value_2), bid, 0);
			
			END IF;

			SELECT datetime_value_2 - INTERVAL 1 DAY INTO datetime_value_2;
			SET @x = @x + 1;
		
		UNTIL @x > 7 END REPEAT;

		SELECT day_1 INTO day_1_amount FROM cash_in_weekly_temp WHERE (businessid = bid) ORDER BY date_time ASC LIMIT 0,1;
		SELECT day_1 INTO day_2_amount FROM cash_in_weekly_temp WHERE (businessid = bid) ORDER BY date_time ASC LIMIT 1,1;
		SELECT day_1 INTO day_3_amount FROM cash_in_weekly_temp WHERE (businessid = bid) ORDER BY date_time ASC LIMIT 2,1;
		SELECT day_1 INTO day_4_amount FROM cash_in_weekly_temp WHERE (businessid = bid) ORDER BY date_time ASC LIMIT 3,1;
		SELECT day_1 INTO day_5_amount FROM cash_in_weekly_temp WHERE (businessid = bid) ORDER BY date_time ASC LIMIT 4,1;
		SELECT day_1 INTO day_6_amount FROM cash_in_weekly_temp WHERE (businessid = bid) ORDER BY date_time ASC LIMIT 5,1;
		SELECT day_1 INTO day_7_amount FROM cash_in_weekly_temp WHERE (businessid = bid) ORDER BY date_time ASC LIMIT 6,1;		
		
		INSERT INTO mburris_report.cash_in_weekly (companyid, date_start, date_end, districtid, districtname, businessid, businessname, day_1, day_2, day_3, day_4, day_5, day_6, day_7) VALUES (cid, date1, date2_selected_value, did, dname, bid, busname, day_1_amount, day_2_amount, day_3_amount, day_4_amount, day_5_amount, day_6_amount, day_7_amount);
		
	END LOOP;
	DROP TEMPORARY TABLE IF EXISTS cash_in_weekly_temp;
  END;
CLOSE load_business_data;	

END; // 
