DROP PROCEDURE IF EXISTS `load_memory_tbls`; // 
CREATE PROCEDURE `load_memory_tbls`()
BEGIN

	INSERT IGNORE INTO posreports_business SELECT * FROM business ORDER BY businessid ASC;
	INSERT IGNORE INTO posreports_checkdetail SELECT * FROM checkdetail ORDER BY id ASC;
	INSERT IGNORE INTO posreports_checks SELECT * FROM checks ORDER BY id ASC;
	INSERT IGNORE INTO posreports_company SELECT * FROM company ORDER BY companyid ASC;
	INSERT IGNORE INTO posreports_district SELECT * FROM district ORDER BY districtid ASC;
	INSERT IGNORE INTO posreports_inv_count_vend SELECT * FROM inv_count_vend ORDER BY id ASC;
	INSERT IGNORE INTO posreports_machine_bus_link SELECT * FROM machine_bus_link ORDER BY id ASC;
	INSERT IGNORE INTO posreports_menu_items_new SELECT * FROM menu_items_new ORDER BY id ASC;
	INSERT IGNORE INTO posreports_menu_items_price SELECT * FROM menu_items_price ORDER BY id ASC;	
	INSERT IGNORE INTO posreports_payment_detail SELECT * FROM payment_detail ORDER BY id ASC;

END; // 

