DROP PROCEDURE IF EXISTS `financial_reports`; // 
CREATE PROCEDURE `financial_reports`(cid INT, date1 DATE, date2 DATE)
BEGIN
	DECLARE bid, did INT;
	DECLARE date2_selected_value DATE;
	DECLARE net_sales, taxes, cost, waste, shrink, gross_sales, waste_percent, shrink_percent, cost_waste, cost_waste_percent, cost_waste_shrink, cost_waste_shrink_percent DECIMAL (8,2);
	DECLARE busname VARCHAR(250);
	DECLARE dname VARCHAR(250);
  
	DECLARE districts_and_business_data CURSOR FOR SELECT bus.businessid, bus.businessname, d.districtid, d.districtname FROM mburris_businesstrack.posreports_business bus JOIN mburris_businesstrack.posreports_district d ON d.districtid = bus.districtid WHERE bus.companyid = cid AND bus.categoryid = 4 ORDER BY bus.districtid ,bus.businessname;
	OPEN districts_and_business_data;
	BEGIN

		DECLARE EXIT HANDLER FOR SQLSTATE '02000' BEGIN END;
		
		CALL inventory_waste_financial(cid, date1, date2);
		
		SELECT date2 INTO date2_selected_value;
		SELECT DATE_ADD(date2, INTERVAL 1 DAY) INTO date2;
		DELETE FROM mburris_report.financial_report WHERE (companyid = cid) AND (date_start = date1) AND (date_end = date2_selected_value);

		LOOP
			FETCH districts_and_business_data INTO bid, busname, did, dname;
			SELECT SUM(c1.amount) INTO net_sales FROM mburris_businesstrack.creditdetail c1 JOIN mburris_businesstrack.credits c2 ON c2.creditid = c1.creditid AND c2.businessid = bid AND c2.credittype = 1 WHERE (c1.date) >= date1 AND (c1.date < date2);
			IF net_sales IS NULL THEN
				SELECT 0 INTO net_sales;
			END IF;
			
			SELECT SUM(c1.amount) INTO taxes FROM mburris_businesstrack.creditdetail c1 JOIN mburris_businesstrack.credits c2 ON c2.creditid = c1.creditid AND c2.businessid = bid AND c2.credittype = 2 WHERE (c1.date) >= date1 AND (c1.date < date2);
			IF taxes IS NULL THEN
				SELECT 0 INTO taxes;
			END IF;
			
			SELECT SUM(dashboard.cos) INTO cost FROM mburris_businesstrack.dashboard WHERE businessid = bid AND (date >= date1) AND (date < date2);
			IF cost IS NULL THEN
				SELECT 0 INTO cost;
			END IF;

			SELECT SUM(amount) INTO waste FROM mburris_report.inventory_waste_financial WHERE (businessid = bid) AND (date_start = date1) AND (date_end = date2_selected_value);
			IF waste IS NULL THEN
			SELECT 0 INTO waste;
			END IF;

			-- SELECT SUM(dashboard.waste) INTO waste FROM mburris_businesstrack.dashboard WHERE businessid = bid AND (date >= date1) AND (date < date2);
			-- IF waste IS NULL THEN
			-- SELECT 0 INTO waste;
			-- END IF;
			
			SELECT SUM(dashboard.shrink) INTO shrink FROM mburris_businesstrack.dashboard WHERE businessid = bid AND (date >= date1) AND (date < date2);
			IF shrink IS NULL THEN
				SELECT 0 INTO shrink;
			END IF;
			
			SELECT (net_sales + taxes) INTO gross_sales;
			IF gross_sales IS NULL THEN
				SELECT 0 INTO gross_sales;
			END IF;
			
			SELECT (waste / net_sales) * 100 INTO waste_percent;
			IF waste_percent IS NULL THEN
				SELECT 0 INTO waste_percent;
			END IF;
			
			SELECT (shrink / net_sales) * 100 INTO shrink_percent;
			IF shrink_percent IS NULL THEN
				SELECT 0 INTO shrink_percent;
			END IF;
			
			SELECT (cost + waste) INTO cost_waste;
			IF cost_waste IS NULL THEN
				SELECT 0 INTO cost_waste;
			END IF;
			
			SELECT (cost + waste) / net_sales * 100 INTO cost_waste_percent;
			IF cost_waste_percent IS NULL THEN
				SELECT 0 INTO cost_waste_percent;
			END IF;
			
			SELECT cost + waste + shrink INTO cost_waste_shrink;
			IF cost_waste_shrink IS NULL THEN
				SELECT 0 INTO cost_waste_shrink;
			END IF;
			
			SELECT (cost + Waste + shrink) / net_sales * 100 INTO cost_waste_shrink_percent;
			IF cost_waste_shrink_percent IS NULL THEN
				SELECT 0 INTO cost_waste_shrink_percent;
			END IF;
			
			INSERT IGNORE INTO mburris_report.financial_report (companyid, date_start, date_end, districtid, districtname, businessid, businessname, gross_sales, net_sales, cost, taxes, waste, shrink, waste_percent, shrink_percent, cost_waste, cost_waste_percent, cost_waste_shrink, cost_waste_shrink_percent) VALUES (cid, date1, date2_selected_value, did, dname, bid, busname, gross_sales, net_sales, cost, taxes, waste, shrink, waste_percent, shrink_percent, cost_waste, cost_waste_percent, cost_waste_shrink, cost_waste_shrink_percent);
		END LOOP;
	END;
	CLOSE districts_and_business_data;
END; // 

