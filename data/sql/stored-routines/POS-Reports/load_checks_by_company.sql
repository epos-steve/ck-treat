DROP PROCEDURE IF EXISTS `load_checks_by_company`; // 
CREATE PROCEDURE `load_checks_by_company`(cid INT)
BEGIN
	SET @s1 = CONCAT("DROP TABLE IF EXISTS posreports_checks_cid_", cid);
	PREPARE stmt1 FROM @s1;
	EXECUTE stmt1;

	SET @s2 = CONCAT("CREATE TEMPORARY TABLE IF NOT EXISTS posreports_checks_cid_", cid, " LIKE posreports_checks");
	PREPARE stmt2 FROM @s2;
	EXECUTE stmt2;
	
	SET @s3 = CONCAT("INSERT IGNORE INTO posreports_checks_cid_", cid, " SELECT c.* FROM posreports_checks c INNER JOIN posreports_business b USING (businessid) WHERE b.companyid = ", cid, " ORDER BY c.id ASC");

	PREPARE stmt3 FROM @s3;
	EXECUTE stmt3;
	
END; // 

