DROP PROCEDURE IF EXISTS `inventory_sales_usbank`; // 
CREATE PROCEDURE `inventory_sales_usbank`(cid INT, date1 DATETIME, date2 DATETIME)
BEGIN
	DECLARE cid2, bid, did, pos_id_value, shelf_life_days_value, items_sold_1 INT;
	DECLARE active_value, timezone_value INT;
	DECLARE date2_selected_value DATE;
	DECLARE date1_converted, date2_converted DATETIME;
	DECLARE cost_value, price_value, price_value_new, extended_cost_value, extended_price_value DECIMAL (8,2) DEFAULT 0.00;
	DECLARE itemcode, route_value VARCHAR(30);
	DECLARE item_name_value, dname, busname VARCHAR(200);
	DECLARE group_name_value VARCHAR(200);
	DECLARE done INT DEFAULT FALSE;

	DECLARE load_menu_items_data CURSOR FOR SELECT lmi.companyid, lmi.businessid, lmi.min_item_code, lmi.min_name, lmi.mgn_group_name, lmi.min_active FROM mburris_businesstrack.load_menu_items lmi WHERE lmi.companyid = cid AND lmi.businessid = 748 ORDER BY lmi.businessid ASC, lmi.min_name ASC;

	OPEN load_menu_items_data;
	BEGIN
		DECLARE CONTINUE handler FOR NOT FOUND SET done = TRUE;
		
		SELECT date2 INTO date2_selected_value;
		SELECT DATE_ADD(date2, INTERVAL 1 DAY) INTO date2;
		DELETE FROM mburris_report.inventory_sales_usbank WHERE (companyid = cid) AND (date_start = date1) AND (date_end = date2_selected_value);
		
		SELECT timezone INTO timezone_value FROM business WHERE businessid = 748;
		
		SELECT DATE_ADD(date1, INTERVAL timezone_value HOUR) INTO date1_converted;
		SELECT DATE_ADD(date2, INTERVAL timezone_value HOUR) INTO date2_converted;

		-- SELECT date1 INTO date1_converted;
		-- SELECT date2 INTO date2_converted;
		
		SELECT date1_converted, date2_converted;

		done_label: LOOP
			SET done = FALSE;
			FETCH load_menu_items_data INTO cid2, bid, itemcode, item_name_value, group_name_value, active_value;
			
				IF done THEN 
					LEAVE done_label;
				END IF;
				
				SELECT mitn.pos_id, mitn.shelf_life_days INTO pos_id_value, shelf_life_days_value FROM mburris_businesstrack.posreports_menu_items_new mitn WHERE mitn.businessid = bid AND mitn.item_code = itemcode LIMIT 1;
				SELECT districtid INTO did FROM business WHERE businessid = bid LIMIT 1;
				SELECT businessname INTO busname FROM business WHERE businessid = bid LIMIT 1;
				SELECT districtname INTO dname FROM district WHERE districtid = did;
				SELECT is_get_price_value(bid, pos_id_value, date1_converted, date2_converted) INTO price_value;
				SELECT is_get_cost_value(bid, pos_id_value, date1_converted, date2_converted) INTO cost_value;
				SELECT is_get_items_sold_value(bid, pos_id_value, date1_converted, date2_converted) INTO items_sold_1;
				
				IF (items_sold_1 IS NOT NULL) AND (items_sold_1 > 0) THEN
					SELECT get_route_value(bid, itemcode) INTO route_value;
					SELECT (cost_value * items_sold_1) INTO extended_cost_value;
					SELECT (price_value * items_sold_1) INTO extended_price_value;

				INSERT IGNORE INTO mburris_report.inventory_sales_usbank (companyid, date_start, date_end, posid, districtid, districtname, businessid, businessname, group_name, item_code, item_name, cost, price, items_sold, extended_cost, extended_price, shelf_life_days, route, active_status) VALUES (cid, date1, date2_selected_value, pos_id_value, did, dname, bid, busname, group_name_value, itemcode, item_name_value, cost_value, price_value, items_sold_1, extended_cost_value, extended_price_value, shelf_life_days_value, route_value, active_value);

				END IF;
				
		END LOOP;

	END;
CLOSE load_menu_items_data;
END; // 

