DROP PROCEDURE IF EXISTS `shrink_trend`; // 
CREATE PROCEDURE `shrink_trend`(cid INT, date1 DATE, date2 DATE)
BEGIN
	DECLARE cid2, bid, did INT;
	DECLARE date2_selected_value DATE;
	DECLARE busname, dname VARCHAR(100);
	DECLARE sales_trend_amount, shrink_trend_amount, week_1_shrink_trend_amount, week_2_shrink_trend_amount, week_3_shrink_trend_amount, week_4_shrink_trend_amount, week_5_shrink_trend_amount, week_6_shrink_trend_amount, week_7_shrink_trend_amount, week_8_shrink_trend_amount, week_9_shrink_trend_amount, week_10_shrink_trend_amount, week_11_shrink_trend_amount, week_12_shrink_trend_amount DECIMAL (8,2) DEFAULT 0.00;
	
	DECLARE shrink_percent_amount, week_1_shrink_trend_percent_amount, week_2_shrink_trend_percent_amount, week_3_shrink_trend_percent_amount, week_4_shrink_trend_percent_amount, week_5_shrink_trend_percent_amount, week_6_shrink_trend_percent_amount, week_7_shrink_trend_percent_amount, week_8_shrink_trend_percent_amount, week_9_shrink_trend_percent_amount, week_10_shrink_trend_percent_amount, week_11_shrink_trend_percent_amount, week_12_shrink_trend_percent_amount DECIMAL (8,1) DEFAULT 0.0;
	
	DECLARE week_1_sales_trend_amount, week_2_sales_trend_amount, week_3_sales_trend_amount, week_4_sales_trend_amount, week_5_sales_trend_amount, week_6_sales_trend_amount, week_7_sales_trend_amount, week_8_sales_trend_amount, week_9_sales_trend_amount, week_10_sales_trend_amount, week_11_sales_trend_amount, week_12_sales_trend_amount DECIMAL (8,2) DEFAULT 0.00;
	
	DECLARE var1, var2 DATE;
	
	declare done int default false;
  
	DECLARE load_business_data CURSOR FOR 
	SELECT bus.businessid, bus.businessname, d.districtid, d.districtname FROM posreports_business bus JOIN posreports_district d ON d.districtid = bus.districtid WHERE bus.companyid = cid AND bus.categoryid = 4 ORDER BY bus.districtid, bus.businessname;
	OPEN load_business_data;
	BEGIN
		DECLARE CONTINUE handler FOR NOT FOUND SET done = TRUE;
		
		DROP TABLE IF EXISTS shrink_trend_temp;
		CREATE TABLE IF NOT EXISTS shrink_trend_temp(companyid int(11) DEFAULT NULL, date_start date DEFAULT NULL, businessid int(11) DEFAULT NULL, shrink_trend decimal(8,2) DEFAULT '0.00', sales_trend decimal(8,2) DEFAULT '0.00', shrink_percent decimal(8,1) DEFAULT '0.0', KEY companyid (companyid)) ENGINE=MEMORY DEFAULT CHARSET=latin1;
		
		SELECT date2 INTO date2_selected_value;
		SELECT DATE_ADD(date2, INTERVAL 1 DAY) INTO date2;
		DELETE FROM mburris_report.shrink_trend WHERE (companyid = cid);
   
	done_label: LOOP
		SET done = FALSE;
		FETCH load_business_data INTO bid, busname, did, dname;
		IF done THEN 
			LEAVE done_label;
		END IF;
		
		SET @x = 0;
		REPEAT 

			SELECT date2 - INTERVAL @x WEEK INTO var1;
			SELECT var1 - INTERVAL 12 WEEK INTO var2;
			
			SELECT (SUM(dashboard.shrink) / 12), (SUM(dashboard.sales) / 12) INTO shrink_trend_amount, sales_trend_amount FROM dashboard WHERE dashboard.businessid = bid AND dashboard.`date` BETWEEN var2 AND var1;
			
			SELECT (shrink_trend_amount * -1) INTO shrink_trend_amount;
			
			IF (sales_trend_amount != 0) THEN
				SELECT (shrink_trend_amount / sales_trend_amount) * 100 INTO shrink_percent_amount;
			ELSE
				SELECT 0.00 INTO shrink_percent_amount;
			END IF;

			INSERT INTO shrink_trend_temp (companyid, date_start, businessid, shrink_trend, sales_trend, shrink_percent) VALUES (cid, var2, bid, shrink_trend_amount, sales_trend_amount, shrink_percent_amount);
			
			SET @x = @x + 1;
		
		UNTIL @x > 11 END REPEAT;
		
		SELECT shrink_trend, sales_trend, shrink_percent INTO week_1_shrink_trend_amount, week_1_sales_trend_amount, week_1_shrink_trend_percent_amount FROM shrink_trend_temp WHERE (businessid = bid) ORDER BY date_start ASC LIMIT 0,1;
		
		SELECT shrink_trend, sales_trend, shrink_percent INTO week_2_shrink_trend_amount, week_2_sales_trend_amount, week_2_shrink_trend_percent_amount FROM shrink_trend_temp WHERE (businessid = bid) ORDER BY date_start ASC LIMIT 1,1;
		
		SELECT shrink_trend, sales_trend, shrink_percent INTO week_3_shrink_trend_amount, week_3_sales_trend_amount, week_3_shrink_trend_percent_amount FROM shrink_trend_temp WHERE (businessid = bid) ORDER BY date_start ASC LIMIT 2,1;
		
		SELECT shrink_trend, sales_trend, shrink_percent INTO week_4_shrink_trend_amount, week_4_sales_trend_amount, week_4_shrink_trend_percent_amount FROM shrink_trend_temp WHERE (businessid = bid) ORDER BY date_start ASC LIMIT 3,1;
		
		SELECT shrink_trend, sales_trend, shrink_percent INTO week_5_shrink_trend_amount, week_5_sales_trend_amount, week_5_shrink_trend_percent_amount FROM shrink_trend_temp WHERE (businessid = bid) ORDER BY date_start ASC LIMIT 4,1;
		
		SELECT shrink_trend, sales_trend, shrink_percent INTO week_6_shrink_trend_amount, week_6_sales_trend_amount, week_6_shrink_trend_percent_amount FROM shrink_trend_temp WHERE (businessid = bid) ORDER BY date_start ASC LIMIT 5,1;
		
		SELECT shrink_trend, sales_trend, shrink_percent INTO week_7_shrink_trend_amount, week_7_sales_trend_amount, week_7_shrink_trend_percent_amount FROM shrink_trend_temp WHERE (businessid = bid) ORDER BY date_start ASC LIMIT 6,1;
		
		SELECT shrink_trend, sales_trend, shrink_percent INTO week_8_shrink_trend_amount, week_8_sales_trend_amount, week_8_shrink_trend_percent_amount FROM shrink_trend_temp WHERE (businessid = bid) ORDER BY date_start ASC LIMIT 7,1;
		
		SELECT shrink_trend, sales_trend, shrink_percent INTO week_9_shrink_trend_amount, week_9_sales_trend_amount, week_9_shrink_trend_percent_amount FROM shrink_trend_temp WHERE (businessid = bid) ORDER BY date_start ASC LIMIT 8,1;
		
		SELECT shrink_trend, sales_trend, shrink_percent INTO week_10_shrink_trend_amount, week_10_sales_trend_amount, week_10_shrink_trend_percent_amount FROM shrink_trend_temp WHERE (businessid = bid) ORDER BY date_start ASC LIMIT 9,1;
		
		SELECT shrink_trend, sales_trend, shrink_percent INTO week_11_shrink_trend_amount, week_11_sales_trend_amount, week_11_shrink_trend_percent_amount FROM shrink_trend_temp WHERE (businessid = bid) ORDER BY date_start ASC LIMIT 10,1;
		
		SELECT shrink_trend, sales_trend, shrink_percent INTO week_12_shrink_trend_amount, week_12_sales_trend_amount, week_12_shrink_trend_percent_amount FROM shrink_trend_temp WHERE (businessid = bid) ORDER BY date_start ASC LIMIT 11,1;
		
		INSERT INTO mburris_report.shrink_trend (companyid, date_start, date_end, districtid, districtname, businessid, businessname, week_1_shrink_trend, week_1_shrink_trend_percent, week_2_shrink_trend, week_2_shrink_trend_percent, week_3_shrink_trend, week_3_shrink_trend_percent, week_4_shrink_trend, week_4_shrink_trend_percent, week_5_shrink_trend, week_5_shrink_trend_percent, week_6_shrink_trend, week_6_shrink_trend_percent, week_7_shrink_trend, week_7_shrink_trend_percent, week_8_shrink_trend, week_8_shrink_trend_percent, week_9_shrink_trend, week_9_shrink_trend_percent, week_10_shrink_trend, week_10_shrink_trend_percent, week_11_shrink_trend, week_11_shrink_trend_percent, week_12_shrink_trend, week_12_shrink_trend_percent, week_1_sales_trend, week_2_sales_trend, week_3_sales_trend, week_4_sales_trend, week_5_sales_trend, week_6_sales_trend, week_7_sales_trend, week_8_sales_trend, week_9_sales_trend, week_10_sales_trend, week_11_sales_trend, week_12_sales_trend) VALUES (cid, date1, date2_selected_value, did, dname, bid, busname, week_1_shrink_trend_amount, week_1_shrink_trend_percent_amount, week_2_shrink_trend_amount, week_2_shrink_trend_percent_amount, week_3_shrink_trend_amount,week_3_shrink_trend_percent_amount, week_4_shrink_trend_amount, week_4_shrink_trend_percent_amount, week_5_shrink_trend_amount,week_5_shrink_trend_percent_amount, week_6_shrink_trend_amount, week_6_shrink_trend_percent_amount, week_7_shrink_trend_amount,week_7_shrink_trend_percent_amount, week_8_shrink_trend_amount, week_8_shrink_trend_percent_amount, week_9_shrink_trend_amount, week_9_shrink_trend_percent_amount, week_10_shrink_trend_amount, week_10_shrink_trend_percent_amount, week_11_shrink_trend_amount, week_11_shrink_trend_percent_amount, week_12_shrink_trend_amount, week_12_shrink_trend_percent_amount, week_1_sales_trend_amount, week_2_sales_trend_amount, week_3_sales_trend_amount, week_4_sales_trend_amount, week_5_sales_trend_amount, week_6_sales_trend_amount, week_7_sales_trend_amount, week_8_sales_trend_amount, week_9_sales_trend_amount, week_10_sales_trend_amount, week_11_sales_trend_amount, week_12_sales_trend_amount);

	END LOOP;
	
	DROP TABLE IF EXISTS shrink_trend_temp;
	
  END;
CLOSE load_business_data;	
END; // 

