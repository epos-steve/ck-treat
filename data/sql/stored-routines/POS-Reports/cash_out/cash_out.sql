DROP PROCEDURE IF EXISTS `cash_out`; // 
CREATE PROCEDURE `cash_out`(cid INT, date1 DATE, date2 DATE)
BEGIN
	DECLARE cid2, bid, did, machine_collect_totals INT;
	DECLARE date2_selected_value DATE;
	DECLARE date1_new DATE;
	DECLARE date2_new, date3 DATETIME;
	DECLARE busname, dname VARCHAR(100);
	DECLARE cash_out_amount_value DECIMAL (8,2);
	
	declare done int default false;
  
	DECLARE load_business_data CURSOR FOR 
	SELECT bus.businessid, bus.businessname, d.districtid, d.districtname FROM posreports_business bus JOIN posreports_district d ON d.districtid = bus.districtid WHERE bus.companyid = cid AND bus.categoryid = 4 ORDER BY bus.districtid, bus.businessname;
	OPEN load_business_data;
	BEGIN
		DECLARE CONTINUE handler FOR NOT FOUND SET done = TRUE;
		
		SELECT date2 INTO date2_selected_value;
		SELECT DATE_ADD(date2, INTERVAL 1 DAY) INTO date2;
		DELETE FROM mburris_report.cash_out WHERE (companyid = cid);
   
	done_label: LOOP
		SET done = FALSE;
		FETCH load_business_data INTO bid, busname, did, dname;
		IF done THEN 
			LEAVE done_label;
		END IF;
		
		SELECT CURDATE(), CONCAT(CURDATE(), ' 23:59:59') INTO date1_new, date2_new;
		
		SELECT COUNT(*) INTO machine_collect_totals FROM machine_collect WHERE businessid = bid AND date_time <= date2_new ORDER BY date_time DESC;
		IF (machine_collect_totals > 0) THEN 
			SELECT date_time INTO date3 FROM machine_collect WHERE businessid = bid AND date_time <= date2_new ORDER BY date_time DESC LIMIT 1;
		ELSE 
			SELECT '0000-00-00 00:00:00' INTO date3;
		END IF;
		
		SELECT SUM( ROUND(pd.received,2) ) INTO cash_out_amount_value FROM posreports_payment_detail pd INNER JOIN posreports_checks c ON c.check_number = pd.check_number AND c.businessid = pd.businessid AND c.businessid = bid AND c.bill_posted BETWEEN date3 AND now() WHERE pd.payment_type = 1;
		
		IF (cash_out_amount_value IS NULL) THEN 
			SELECT 0.00 INTO cash_out_amount_value;
		END IF;

		INSERT INTO mburris_report.cash_out (companyid, date_start, date_end, districtid, districtname, businessid, businessname, cash_out_amount) VALUES (cid, date3, now(), did, dname, bid, busname, cash_out_amount_value);

	END LOOP;
  END;
CLOSE load_business_data;	
END; // 

