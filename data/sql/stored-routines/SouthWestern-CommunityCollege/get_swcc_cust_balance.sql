DROP FUNCTION IF EXISTS `get_swcc_cust_balance`//
CREATE FUNCTION `get_swcc_cust_balance`(cust_id INT) 
RETURNS decimal(10,2)
READS SQL DATA
BEGIN 
	DECLARE chargetotal_value DECIMAL (10,2);
	SELECT chargetotal INTO chargetotal_value FROM customer_transactions WHERE customer_id = cust_id AND response= 'APPROVED' ORDER BY date_created ASC LIMIT 1;
	
	RETURN chargetotal_value;

END; //
