DROP PROCEDURE IF EXISTS `swcc_report_4`//
CREATE PROCEDURE `swcc_report_4`(cust_id INT)
BEGIN
	DECLARE customerid_value, customerid_value_2, businessid_value, sale_type_value, payment_type_value, valueid_value INT;
	DECLARE check_number_value BIGINT(20) UNSIGNED;
	DECLARE first_name_value, last_name_value, scancode_value, businessname_value VARCHAR(100);
	DECLARE bill_datetime_value DATETIME;
	DECLARE balance_value, total_value, cust_balance_value, received_value DECIMAL (8,2);

	declare done int default false;
  
	DECLARE load_swcc_data CURSOR FOR SELECT customerid, first_name, last_name, scancode, check_number, businessid, bill_datetime, total, sale_type, received, payment_type, businessname FROM west_college_2 WHERE customerid = cust_id ORDER BY bill_datetime ASC;
	OPEN load_swcc_data;
	
	BEGIN
	
	DECLARE CONTINUE handler FOR NOT FOUND SET done = TRUE;

	-- TRUNCATE west_college_3;
	SELECT 0 INTO cust_balance_value;
	SET @var1 = 0;
	SELECT 1 INTO valueid_value;
	
	done_label: LOOP
		SET done = FALSE;
		FETCH load_swcc_data INTO customerid_value, first_name_value, last_name_value, scancode_value, check_number_value, businessid_value, bill_datetime_value, total_value, sale_type_value, received_value, payment_type_value, businessname_value;
		IF done THEN 
			LEAVE done_label;
		END IF;

		IF (payment_type_value = 5) THEN
			SET @var1 = @var1 + total_value;
		END IF;
		
		IF (valueid_value = 1) THEN 
			INSERT INTO west_college_3 (valueid, customerid, first_name, last_name, scancode, check_number, businessid, bill_datetime, total, cust_balance, sale_type, received, payment_type, businessname) VALUES (valueid_value, customerid_value, first_name_value, last_name_value, scancode_value, check_number_value, businessid_value, bill_datetime_value, total_value, total_value, sale_type_value, received_value, payment_type_value, businessname_value);
			SET @total = total_value;
		ELSE
			IF (payment_type_value = 5) THEN
				SET @total = @total - total_value;
			END IF;

			IF (payment_type_value = 4) THEN
				SET @total = @total + total_value;
			END IF;

			IF (payment_type_value = 3) THEN
				SET @total = @total + total_value;
			END IF;
			
			INSERT INTO west_college_3 (valueid, customerid, first_name, last_name, scancode, check_number, businessid, bill_datetime, total, cust_balance, sale_type, received, payment_type, businessname) VALUES (valueid_value, customerid_value, first_name_value, last_name_value, scancode_value, check_number_value, businessid_value, bill_datetime_value, total_value, @total, sale_type_value, received_value, payment_type_value, businessname_value);
		END IF;

		SELECT (valueid_value + 1) INTO valueid_value;

	END LOOP;
  END;
CLOSE load_swcc_data;	
END; //
