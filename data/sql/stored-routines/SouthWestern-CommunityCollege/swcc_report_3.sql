DROP PROCEDURE IF EXISTS `swcc_report_3`//
CREATE PROCEDURE `swcc_report_3`(cust_id INT)
BEGIN
	DECLARE customer_id_value, trans_type_id_value, businessid_value, total_cust INT;
	DECLARE date_created_value DATETIME;
	DECLARE response_value, first_name_value, last_name_value, scancode_value VARCHAR(100);
	DECLARE chargetotal_value DECIMAL (8,2);
	
	declare done int default false;
  
	DECLARE load_swcc_data CURSOR FOR SELECT trans_type_id, customer_id, chargetotal, date_created, response FROM customer_transactions WHERE (customer_id = cust_id) AND (chargetotal != 0) ORDER BY date_created;
	OPEN load_swcc_data;
	
	BEGIN
	
	DECLARE CONTINUE handler FOR NOT FOUND SET done = TRUE;
	
	done_label: LOOP
		SET done = FALSE;
		FETCH load_swcc_data INTO trans_type_id_value, customer_id_value, chargetotal_value, date_created_value, response_value;
		IF done THEN 
			LEAVE done_label;
		END IF;
		
		SELECT COUNT(*) INTO total_cust FROM customer WHERE customerid = customer_id_value;
		IF (total_cust > 0) THEN 
		
			IF (response_value = 'APPROVED') THEN 

				IF (trans_type_id_value = 5) THEN
					SELECT 989 INTO trans_type_id_value;
				END IF;
				
				SELECT first_name, last_name, scancode, businessid INTO first_name_value, last_name_value, scancode_value, businessid_value FROM customer WHERE customerid = customer_id_value;
				
				INSERT INTO west_college_2 (customerid, first_name, last_name, scancode, businessid, bill_datetime, total, cust_balance, payment_type) VALUES (customer_id_value, first_name_value, last_name_value, scancode_value, businessid_value, date_created_value, chargetotal_value, chargetotal_value, trans_type_id_value);
				
				-- SELECT 1 INTO businessid_value;
				
			END IF;
		
		END IF;

	END LOOP;
  END;
CLOSE load_swcc_data;	
END; //
