DROP PROCEDURE IF EXISTS `swcc_report`//
CREATE PROCEDURE `swcc_report`(bid INT)
BEGIN
	DECLARE customerid_value, customerid_value_2, custom_field_id_value INT;
	DECLARE first_name_value, last_name_value, scancode_value, customer_type_value VARCHAR(100);
	DECLARE balance_value DECIMAL (8,2);
	
	declare done int default false;
  
	DECLARE load_swcc_data CURSOR FOR SELECT first_name, last_name, customerid, scancode, balance FROM customer WHERE businessid = bid ORDER BY last_name ASC, first_name ASC;
	OPEN load_swcc_data;
	
	BEGIN
	
	DECLARE CONTINUE handler FOR NOT FOUND SET done = TRUE;
	
	TRUNCATE west_college_2;
	TRUNCATE west_college_checks;
	TRUNCATE west_college_pd;
	TRUNCATE west_college_3;

	INSERT INTO west_college_checks SELECT * FROM posreports_checks WHERE businessid = bid ORDER BY bill_datetime ASC;
	INSERT INTO west_college_pd SELECT * FROM posreports_payment_detail WHERE businessid = bid ORDER BY id ASC;
	
	done_label: LOOP
		SET done = FALSE;
		FETCH load_swcc_data INTO first_name_value, last_name_value, customerid_value, scancode_value, balance_value;
		IF done THEN 
			LEAVE done_label;
		END IF;
		
		SELECT customerid_value INTO customerid_value_2;
		
		call swcc_report_3(customerid_value_2);
		call swcc_report_2(scancode_value, bid);
		call swcc_report_4(customerid_value_2);
		
		-- select customerid_value_2;
		
		SELECT `CustomFieldId` INTO custom_field_id_value FROM `pd_portal`.`CustomField` WHERE `BusinessId` = bid;
		
		-- SELECT `Value` INTO customer_type_value FROM pd_portal.CustomFieldValue WHERE (CustomFieldId = 50) AND (`CustomerId` = customerid_value_2);
		SELECT `Value` INTO customer_type_value FROM pd_portal.CustomFieldValue WHERE (CustomFieldId = custom_field_id_value) AND (`CustomerId` = customerid_value_2);
		UPDATE west_college_3 SET customer_type = customer_type_value WHERE customerid = customerid_value_2;

	END LOOP;
	
	TRUNCATE west_college_2;
	
	INSERT INTO west_college_2 (customerid, first_name, last_name, scancode, check_number, businessid, bill_datetime, total, cust_balance, sale_type, received, payment_type, businessname, customer_type) SELECT customerid, first_name, last_name, scancode, check_number, businessid, bill_datetime, total, cust_balance, sale_type, received, payment_type, businessname, customer_type FROM west_college_3 ORDER BY last_name ASC, first_name ASC, valueid ASC;
	
  END;
CLOSE load_swcc_data;	
END; //
