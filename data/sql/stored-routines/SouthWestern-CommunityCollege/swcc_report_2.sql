DROP PROCEDURE IF EXISTS `swcc_report_2`//
CREATE PROCEDURE `swcc_report_2`(value1 VARCHAR(20), bid INT)
BEGIN
	DECLARE customerid_value, businessid_value, sale_type_value, payment_type_value, cust_id INT;
	DECLARE check_number_value BIGINT(20) UNSIGNED;
	DECLARE first_name_value, last_name_value, scancode_value, businessname_value VARCHAR(100);
	DECLARE balance_value, total_value, received_value, running_balance, running_balance_2, starting_balance DECIMAL (8,2);
	DECLARE bill_datetime_value DATETIME;
	
	declare done int default false;
  
	DECLARE load_swcc_data CURSOR FOR SELECT c1.customerid, c1.first_name, c1.last_name, c1.scancode, c.check_number, c.businessid, c.bill_datetime, c.total, c.sale_type, pd.received, pd.payment_type, '' FROM west_college_checks c JOIN west_college_pd pd ON pd.check_number = c.check_number AND pd.businessid = c.businessid AND pd.scancode LIKE value1 JOIN customer c1 USING (scancode) JOIN posreports_business b ON b.businessid = c.businessid WHERE c1.businessid = bid ORDER BY bill_datetime ASC;
	OPEN load_swcc_data;
	
	BEGIN
	
	DECLARE CONTINUE handler FOR NOT FOUND SET done = TRUE;
	SELECT customerid INTO cust_id FROM customer WHERE scancode = value1 LIMIT 1;
	SELECT get_swcc_cust_balance(cust_id) INTO starting_balance;

	done_label: LOOP
		SET done = FALSE;
		FETCH load_swcc_data INTO customerid_value, first_name_value, last_name_value, scancode_value, check_number_value, businessid_value, bill_datetime_value, total_value, sale_type_value, received_value, payment_type_value, businessname_value;
		IF done THEN 
			LEAVE done_label;
		END IF;
		
		SELECT (starting_balance - total_value) INTO starting_balance;

		INSERT INTO west_college_2 (customerid, first_name, last_name, scancode, check_number, businessid, bill_datetime, total, cust_balance, sale_type, received, payment_type, businessname) VALUES (customerid_value, first_name_value, last_name_value, scancode_value, check_number_value, businessid_value, bill_datetime_value, total_value, starting_balance, sale_type_value, received_value, payment_type_value, businessname_value);

	END LOOP;
	-- SELECT first_name, last_name, scancode, businessid FROM customer WHERE customerid = customerid_value;
  END;
CLOSE load_swcc_data;	
END; //
