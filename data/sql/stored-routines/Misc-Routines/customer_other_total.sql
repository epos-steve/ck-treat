DROP FUNCTION IF EXISTS `customer_other_total`; // 
CREATE FUNCTION `customer_other_total`(cust_customerid INT) RETURNS decimal(6,2)
    READS SQL DATA
BEGIN
	DECLARE pd_payment_type VARCHAR(30);
	DECLARE checks_total, calculated_balance DECIMAL(6,2) DEFAULT 0.00;
	DECLARE done INT DEFAULT FALSE;
	
	DECLARE customer_checks CURSOR FOR SELECT ctt.label, ct.chargetotal FROM mburris_businesstrack.customer_transactions ct INNER JOIN customer_transaction_types ctt ON ct.trans_type_id = ctt.id WHERE (customer_id = cust_customerid) AND (ct.response LIKE 'APPROVED') ORDER BY ct.date_created DESC;
	
	OPEN customer_checks;
	BEGIN
		DECLARE CONTINUE handler FOR NOT FOUND SET done = TRUE;
		done_label: LOOP
			FETCH customer_checks INTO pd_payment_type, checks_total;
			IF done THEN 
				LEAVE done_label;
			END IF;
			
			IF (pd_payment_type = "1") OR (pd_payment_type = "4") OR (pd_payment_type = "6") OR (pd_payment_type = 'Promotion') OR (pd_payment_type = 'Credit Card Transaction') OR (pd_payment_type = "Company Kitchen Refund") OR (pd_payment_type = "Transfer") OR (pd_payment_type = "CK Adjust") THEN
				SELECT (checks_total * -1) INTO checks_total;
			END IF;
			
			SELECT (calculated_balance + checks_total) INTO calculated_balance;
			
		END LOOP;	
	END;
	RETURN calculated_balance;
	
END; // 

