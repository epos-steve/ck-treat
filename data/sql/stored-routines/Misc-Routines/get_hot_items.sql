DROP PROCEDURE IF EXISTS `get_hot_items`; // 
CREATE PROCEDURE `get_hot_items`()
BEGIN
	DECLARE bid, cid INT;
	DECLARE current_date_value, date1, date2, date3 DATE;

	DECLARE business_id_data CURSOR FOR SELECT bus.businessid, bus.companyid FROM posreports_business bus WHERE bus.categoryid = 4 ORDER BY bus.companyid ASC, bus.businessid ASC;
	
	CREATE TEMPORARY TABLE IF NOT EXISTS posreports_get_hot_checks ( id int(11) NOT NULL AUTO_INCREMENT, businessid int(11) NOT NULL, check_number bigint(20) unsigned NOT NULL, session_number int(11) NOT NULL, table_number int(5) NOT NULL, seat_number int(5) NOT NULL, emp_no int(11) NOT NULL, people int(5) NOT NULL, bill_datetime datetime NOT NULL, bill_posted datetime NOT NULL, total double(10,2) NOT NULL, taxes double(10,2) NOT NULL, received decimal(10,2) NOT NULL, sale_type int(5) NOT NULL, account_number bigint(20) NOT NULL, terminal_group varchar(35) COLLATE utf8_unicode_ci NOT NULL, is_void tinyint(1) NOT NULL, PRIMARY KEY (id), KEY check_number (check_number) USING BTREE ) ENGINE=MEMORY  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18137834 ;
	
	SELECT CURDATE() INTO current_date_value;
	SELECT DATE_ADD(current_date_value, INTERVAL -10 DAY) INTO date1;
	SELECT current_date_value INTO date2;

	INSERT INTO posreports_get_hot_checks SELECT * FROM posreports_checks WHERE bill_datetime > date1;
	TRUNCATE posreports_jake_22;
	
	OPEN business_id_data;
	BEGIN
		DECLARE EXIT HANDLER FOR SQLSTATE '02000' BEGIN END;

			LOOP
				FETCH business_id_data INTO bid, cid;

				insert into posreports_jake_22 (companyid, businessid, pos_id_number, name, totals) select cid, cd.businessid, cd.item_number, min.name, count(cd.quantity) as sold from posreports_checkdetail cd join posreports_get_hot_checks c ON c.businessid = cd.businessid and c.check_number = cd.bill_number and c.bill_datetime between date1 and date2 join posreports_menu_items_new min ON cd.item_number = min.pos_id AND cd.businessid = min.businessid where cd.businessid = bid group by cd.item_number order by sold DESC;
				
			END LOOP;

	END;
	CLOSE business_id_data;

	UPDATE posreports_jake_22 pj22 SET pj22.upc = (SELECT mitn.upc FROM posreports_menu_items_new mitn WHERE mitn.pos_id = pj22.pos_id_number AND mitn.businessid = pj22.businessid);

	UPDATE posreports_jake_22 pj22 SET pj22.order_type = (SELECT mitn.ordertype FROM posreports_menu_items_new mitn WHERE mitn.pos_id = pj22.pos_id_number AND mitn.businessid = pj22.businessid);
	
	UPDATE posreports_menu_items_master SET ordertype = '0', sold = '0' WHERE 1;
	UPDATE menu_items_master SET ordertype = '0', sold = '0' WHERE 1;
	
	DROP TABLE posreports_get_hot_checks;
	
END; // 

