DROP PROCEDURE IF EXISTS `company_reports`; // 
CREATE PROCEDURE `company_reports`(cid INT, date1 DATE, date2 DATE)
BEGIN

	DECLARE cost_value, cost_value_mip, cost_value_new, amount_value double;
	declare done int default false;

	BEGIN
    DECLARE EXIT HANDLER FOR SQLSTATE '02000' BEGIN END;
    DECLARE CONTINUE handler FOR NOT FOUND SET done = TRUE;
    truncate aa_next_gen;
        
    insert into aa_next_gen (businessid, businessname, item_code, name, price, cost, quantity, waste)  select b.businessid 
        , b.businessname
        , min.item_code
        , min.name
        , avg(cd.quantity * cd.price / cd.quantity) AS price
        , avg(cd.quantity * cd.cost / cd.quantity) AS cost
        , sum(cd.quantity) AS quantity
        , 0
    FROM checks_mem c 
    join checkdetail_mem cd ON cd.bill_number = c.check_number and cd.businessid = c.businessid 
    join menu_items_new_mem min ON min.pos_id = cd.item_number AND min.businessid = cd.businessid 
    join business b on b.businessid = c.businessid and b.companyid = cid 
    where (c.bill_datetime >= date1) AND (c.bill_datetime < date2) group by min.id order by b.businessid, min.item_code;

    UPDATE aa_next_gen a1 SET waste = (
        SELECT sum(waste) 
        FROM inv_count_vend_mem icv 
        join machine_bus_link_mem mbl on mbl.machine_num = icv.machine_num 
        WHERE (icv.item_code LIKE a1.item_code) AND (icv.date_time >= date1) AND (icv.date_time < date2) and (mbl.businessid = a1.businessid) AND (icv.waste > 0)
    );

    END;
END; // 

