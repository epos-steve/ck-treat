DROP FUNCTION IF EXISTS `customer_checks_total`; // 
CREATE FUNCTION `customer_checks_total`(cust_scancode VARCHAR(30)) RETURNS decimal(6,2)
    READS SQL DATA
BEGIN
	DECLARE pd_payment_type VARCHAR(30);
	DECLARE checks_total, calculated_balance DECIMAL(6,2) DEFAULT 0.00;
	DECLARE cust_customerid INT DEFAULT 0;
	
	DECLARE done INT DEFAULT FALSE;
	
	DECLARE customer_checks CURSOR FOR SELECT customer.customerid, checks.total, payment_detail.payment_type FROM checks JOIN payment_detail ON payment_detail.check_number = checks.check_number AND payment_detail.businessid = checks.businessid AND payment_detail.scancode = cust_scancode JOIN customer USING (scancode) JOIN business ON business.businessid = checks.businessid WHERE checks.bill_datetime BETWEEN '2000-01-01 00:00:00' AND '2020-01-01 00:00:00' ORDER BY bill_datetime DESC;
	
	OPEN customer_checks;
	BEGIN
		DECLARE CONTINUE handler FOR NOT FOUND SET done = TRUE;
		done_label: LOOP
			FETCH customer_checks INTO cust_customerid, checks_total, pd_payment_type;
			IF done THEN 
				LEAVE done_label;
			END IF;
			
			IF (pd_payment_type = 1) OR (pd_payment_type = 4) OR (pd_payment_type = 6) OR (pd_payment_type = 'Promotion') OR (pd_payment_type = 'Credit Card Transaction') OR (pd_payment_type = "Company Kitchen Refund") OR (pd_payment_type = "Transfer") OR (pd_payment_type = "CK Adjust") THEN
				SELECT (checks_total * -1) INTO checks_total;
			END IF;
			
			SELECT (calculated_balance + checks_total) INTO calculated_balance;
			
		END LOOP;	
	END;
	RETURN calculated_balance;
END; // 

