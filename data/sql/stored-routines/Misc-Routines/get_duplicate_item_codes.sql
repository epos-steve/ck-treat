DROP PROCEDURE IF EXISTS `get_duplicate_item_codes`; // 
CREATE PROCEDURE `get_duplicate_item_codes`(cid INT)
BEGIN
DECLARE bid, bid2 INT;
DECLARE business_data CURSOR FOR 
    SELECT businessid FROM business bus WHERE bus.companyid = cid AND bus.categoryid = 4 ORDER BY bus.businessid;
  OPEN business_data;
  BEGIN
  LOOP
	FETCH business_data INTO bid;

	INSERT INTO aa_duplicate_item_codes (businessid, businessname, item_code, totals) SELECT mitn.businessid, b.businessname, mitn.item_code, COUNT(mitn.item_code) AS totals FROM  menu_items_new mitn INNER JOIN business b USING (businessid) WHERE b.businessid = bid GROUP BY item_code HAVING COUNT(mitn.item_code) > 1 ORDER BY totals DESC;

	END LOOP;
  END;
END; // 

