DROP PROCEDURE IF EXISTS `clear_menu`; // 
CREATE PROCEDURE `clear_menu`(bid INT)
BEGIN

	DELETE FROM menu_items_new WHERE businessid = bid;
	DELETE FROM menu_items_price WHERE businessid = bid;
	DELETE FROM menu_groups_new WHERE businessid = bid;

END; // 

