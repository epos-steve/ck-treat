DROP PROCEDURE IF EXISTS `change_tal_cost`; // 
CREATE PROCEDURE `change_tal_cost`()
BEGIN

	DECLARE bid INT;
	DECLARE done INT DEFAULT FALSE;
	
	DECLARE load_menu_items_data CURSOR FOR SELECT businessid FROM mburris_businesstrack.menu_business_cost_updates ORDER BY businessid ASC;
	
	OPEN load_menu_items_data;
	BEGIN
		DECLARE CONTINUE handler FOR NOT FOUND SET done = TRUE;
		
		done_label: LOOP
			FETCH load_menu_items_data INTO bid;

			IF done THEN 
				LEAVE done_label;
			END IF;
			
			UPDATE menu_cost_updates SET businessid = bid;
			UPDATE menu_cost_updates m1 SET m1.menu_item_id = (SELECT m2.id FROM menu_items_new m2 WHERE (m2.businessid = m1.businessid) AND (m2.item_code = m1.item_code) AND (m2.upc = m1.upc) LIMIT 1) WHERE 1;
			UPDATE menu_items_price m1 SET m1.cost = (SELECT m2.cost FROM menu_cost_updates m2 WHERE m2.menu_item_id = m1.menu_item_id LIMIT 1) WHERE m1.businessid = bid;

			UPDATE menu_cost_updates SET businessid = NULL;
			UPDATE menu_cost_updates SET menu_item_id = NULL;
			
				
		END LOOP;
		
	END;

END; // 

