DROP PROCEDURE IF EXISTS `customer_balance_inquiry`; // 
CREATE PROCEDURE `customer_balance_inquiry`(bid INT)
BEGIN
	DECLARE cust_scancode, pd_payment_type VARCHAR(30);
	DECLARE cust_balance, checks_total, calculated_balance_total, calculated_balance_1, calculated_balance_2, out_of_balance_amount DECIMAL(6,2) DEFAULT 0.00;
	DECLARE cust_customerid, cid, oob INT DEFAULT 0;
	
	DECLARE done INT DEFAULT FALSE;
	
	DECLARE scancode_balance CURSOR FOR SELECT scancode, balance FROM mburris_businesstrack.customer WHERE businessid = bid AND NOT IFNULL(scancode, '') = '' ORDER BY scancode ASC;
	OPEN scancode_balance;
	BEGIN
		DECLARE CONTINUE handler FOR NOT FOUND SET done = TRUE;
		SELECT companyid INTO cid FROM mburris_businesstrack.business WHERE businessid = bid LIMIT 1;
		done_label: LOOP
			FETCH scancode_balance INTO cust_scancode, cust_balance;
			IF done THEN 
				LEAVE done_label;
			END IF;

			SELECT customerid INTO cust_customerid FROM mburris_businesstrack.customer WHERE scancode = cust_scancode;

			SELECT customer_checks_total(cust_scancode) INTO calculated_balance_1;
			SELECT customer_other_total(cust_customerid) INTO calculated_balance_2;
			
			SELECT (calculated_balance_1 + calculated_balance_2) * -1 INTO calculated_balance_total;
			
			IF (cust_balance != calculated_balance_total) THEN
				SELECT 1 INTO oob;
				SELECT (cust_balance - calculated_balance_total) INTO out_of_balance_amount;
			ELSE
				SELECT 0 INTO oob;
				SELECT 0 INTO out_of_balance_amount;
			END IF;
			
			INSERT INTO mburris_businesstrack.customer_z_tracker (companyid, businessid, customerid, scancode, stored_balance, calculated_balance, oob_amount, out_of_balance) VALUES (cid, bid, cust_customerid, cust_scancode, cust_balance, calculated_balance_total, out_of_balance_amount, oob);

		END LOOP;
	END;
	CLOSE scancode_balance;
END; // 

