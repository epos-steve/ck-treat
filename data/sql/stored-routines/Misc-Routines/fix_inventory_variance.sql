DROP PROCEDURE IF EXISTS `fix_inventory_variance`; // 
CREATE PROCEDURE `fix_inventory_variance`(cid INT, date_value TIMESTAMP)
BEGIN
	DECLARE total_counts, is_inventoried, fills, bid INT;
	DECLARE machine VARCHAR(10);
	DECLARE itemcode_value VARCHAR(15);
	DECLARE date_time_value, date_time_minus_1_minute, date_time_plus_1_minute TIMESTAMP;
	DECLARE done INT DEFAULT FALSE;
	  
	DECLARE load_menu_items_data CURSOR FOR SELECT lmi.min_item_code, lmi.businessid FROM mburris_businesstrack.load_menu_items lmi WHERE lmi.companyid = cid ORDER BY lmi.businessid ASC, lmi.min_name ASC;
	OPEN load_menu_items_data;
	BEGIN
		DECLARE CONTINUE handler FOR NOT FOUND SET done = TRUE;

		done_label: LOOP
			SET done = FALSE;
			FETCH load_menu_items_data INTO itemcode_value, bid;
			IF done THEN 
				LEAVE done_label;
			END IF;

			SELECT COUNT(*) INTO total_counts FROM mburris_businesstrack.inv_count_vend icv JOIN mburris_businesstrack.machine_bus_link mbl ON icv.machine_num = mbl.machine_num AND mbl.businessid = bid WHERE (item_code = itemcode_value) AND (is_inv IN(0,1)) AND (date_time <= date_value);
			
			IF total_counts > 0 THEN

				SELECT icv.machine_num, icv.date_time, icv.fills, icv.is_inv INTO machine, date_time_value, fills, is_inventoried FROM mburris_businesstrack.inv_count_vend icv JOIN mburris_businesstrack.machine_bus_link mbl ON icv.machine_num = mbl.machine_num AND mbl.businessid = bid WHERE (item_code = itemcode_value) AND (is_inv IN(0,1)) ORDER BY date_time ASC LIMIT 1;
				
				IF is_inventoried = 0 THEN

					SELECT date_time_value - INTERVAL 1 MINUTE INTO date_time_minus_1_minute;
					INSERT INTO mburris_businesstrack.inv_count_vend (machine_num, date_time, item_code, onhand, is_inv) VALUES (machine, date_time_minus_1_minute, itemcode_value, 0, 1);

--					SELECT date_time_value + INTERVAL 1 MINUTE INTO date_time_plus_1_minute;
--					INSERT INTO mburris_businesstrack.inv_count_vend (machine_num, date_time, item_code, onhand, is_inv) VALUES (machine, date_time_plus_1_minute, itemcode_value, fills, 1);
				
				END IF;

			END IF;			
		END LOOP;
	END;
	CLOSE load_menu_items_data;			
		
END; // 

