DROP PROCEDURE IF EXISTS `generateCalendarTable`; // 
CREATE PROCEDURE `generateCalendarTable`(
	var_DateStart date,
	var_DateEnd date
)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
begin
	declare var_DateCurrent date;
	
	set var_DateCurrent = var_DateStart;
	
	drop table if exists calendarTable;
	create table calendarTable(
		calendarDate date primary key not null,
		weekMonday date not null,
		weekTuesday date not null,
		weekWednesday date not null,
		weekThursday date not null,
		weekFriday date not null,
		weekSaturday date not null,
		weekSunday date not null,
		isWeekend boolean,
		monthStart date not null,
		monthEnd date not null,
		yearStart date not null,
		yearEnd date not null,
		dateMonth tinyint unsigned not null,
		dateYear smallint unsigned not null
	);
	
	while var_DateCurrent < var_DateEnd do
		insert into calendarTable set
			calendarDate = var_DateCurrent,
			weekMonday = DATE_ADD( var_DateCurrent, 
				INTERVAL (  
					IF( DAYOFWEEK( var_DateCurrent ) > 2, 9, 2 )
					- DAYOFWEEK( var_DateCurrent )
				) DAY ),
			weekTuesday = DATE_ADD( var_DateCurrent,
				INTERVAL ( 
					IF( DAYOFWEEK( var_DateCurrent ) > 3, 10, 3 )
					- DAYOFWEEK( var_DateCurrent )
				) DAY ),
			weekWednesday = DATE_ADD( var_DateCurrent, 
				INTERVAL ( 
					IF( DAYOFWEEK( var_DateCurrent ) > 4, 11, 4 )
					- DAYOFWEEK( var_DateCurrent )
				) DAY ),
			weekThursday = DATE_ADD( var_DateCurrent,
				INTERVAL ( 
					IF( DAYOFWEEK( var_DateCurrent ) > 5, 12, 5 )
					- DAYOFWEEK( var_DateCurrent )
				) DAY ),
			weekFriday = DATE_ADD( var_DateCurrent, 
				INTERVAL ( 
					IF( DAYOFWEEK( var_DateCurrent ) > 6, 13, 6 )
					- DAYOFWEEK( var_DateCurrent )
				) DAY ),
			weekSaturday = DATE_ADD( var_DateCurrent,
				INTERVAL ( 
					IF( DAYOFWEEK( var_DateCurrent ) = 7, 14, 7 )
					- DAYOFWEEK( var_DateCurrent )
				) DAY ),
			weekSunday = DATE_ADD( var_DateCurrent, 
				INTERVAL ( 
					IF( DAYOFWEEK( var_DateCurrent ) > 1, 9, 1 )
					- DAYOFWEEK( var_DateCurrent )
				) DAY ),
			monthStart = DATE_FORMAT( var_DateCurrent, '%Y-%m-01' ),
			monthEnd = DATE_SUB( DATE_ADD( monthStart, INTERVAL 1 MONTH ), INTERVAL 1 DAY ),
			yearStart = DATE_FORMAT( var_DateCurrent, '%Y-01-01' ),
			yearEnd = DATE_FORMAT( var_DateCurrent, '%Y-12-31' ),
			dateMonth = MONTH( var_DateCurrent ),
			dateYear = YEAR( var_DateCurrent ),
			isWeekend = IF( DAYOFWEEK( var_DateCurrent ) = 1 OR DAYOFWEEK( var_DateCurrent ) = 7, true, false );
		set var_DateCurrent = DATE_ADD( var_DateCurrent, INTERVAL 1 DAY );
		end while;
end; // 

