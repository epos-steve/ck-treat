create or replace view vPromoDays as
	select 0 as day,id from promotions psu where psu.sunday=1
	union
	select 1 as day,id from promotions pm where pm.monday=1
	union
	select 2 as day,id from promotions pt where pt.tuesday=1
	union
	select 3 as day,id from promotions pw where pw.wednesday=1
	union
	select 4 as day,id from promotions pth where pth.thursday=1
	union
	select 5 as day,id from promotions pf where pf.friday=1
	union
	select 6 as day,id from promotions ps where ps.saturday=1;
create or replace view vPromoVivipos as
select
		promotions.businessid,
		promotions.id as promo_id,
		promo_target.id as promo_target,
		promotions.name as vivipos_name,
		promotions.pos_id,
		promotions.value,
		promotions.rule_order,
		promotions.promo_reserve,
		promotions.promo_discount,
		promotions.promo_discount_type,
		promotions.promo_discount_n,
		promotions.promo_discount_limit,
		promotions.promo_trigger_amount_limit,
		promotions.promo_trigger_amount_type,
		promotions.promo_trigger_amount,
		promotions.promo_trigger_amount_2,
		promotions.promo_trigger_amount_3,
		COALESCE( CAST( menu_tax.pos_id AS char ), '' ) as vivipos_taxno,
		COALESCE( menu_tax.name, '' ) as vivipos_taxname,
		UNIX_TIMESTAMP( promotions.start_date ) - (3600 * business.timezone) as vivipos_startdate,
		UNIX_TIMESTAMP( CONCAT( promotions.start_date, ' ', promotions.start_time ) ) - (3600 * business.timezone) as vivipos_starttime,
		UNIX_TIMESTAMP( promotions.end_date ) - (3600 * business.timezone) + 59 as vivipos_enddate,
		UNIX_TIMESTAMP( CONCAT( promotions.start_date, ' ', promotions.end_time ) ) - (3600 * business.timezone) + 59 as vivipos_endtime,
		GROUP_CONCAT( vPromoDays.day ORDER BY vPromoDays.day ) as vivipos_days,
		promo_type.vivipos_name as vivipos_type,
		promo_trigger.vivipos_name as vivipos_trigger,
		promo_target.db_table as promotarget_table,
		promo_target.order_by as promotarget_order,
		UNIX_TIMESTAMP( NOW() ) as vivipos_modified,
		promotions.activeFlag as vivipos_active,
        0 AS is_global,
		promotions.subsidy as alt_name1
	from promotions
	join promo_type on promotions.type=promo_type.id
	join promo_trigger on promotions.promo_trigger=promo_trigger.id
	join promo_target on promo_trigger.target_id=promo_target.id
	left join menu_tax on promotions.tax_id = menu_tax.id
	left join vPromoDays on promotions.id=vPromoDays.id
	join business ON business.businessid = promotions.businessid
	group by promotions.id
    UNION
	select
		promo_mapping.businessid,
		promotions.id as promo_id,
		promo_target.id as promo_target,
		promotions.name as vivipos_name,
		promotions.pos_id,
		promotions.value,
		promotions.rule_order,
		promotions.promo_reserve,
		promotions.promo_discount,
		promotions.promo_discount_type,
		promotions.promo_discount_n,
		promotions.promo_discount_limit,
		promotions.promo_trigger_amount_limit,
		promotions.promo_trigger_amount_type,
		promotions.promo_trigger_amount,
		promotions.promo_trigger_amount_2,
		promotions.promo_trigger_amount_3,
		COALESCE( CAST( menu_tax.pos_id AS char ), '' ) as vivipos_taxno,
		COALESCE( menu_tax.name, '' ) as vivipos_taxname,
		UNIX_TIMESTAMP( promotions.start_date ) - (3600 * business.timezone) as vivipos_startdate,
		UNIX_TIMESTAMP( CONCAT( promotions.start_date, ' ', promotions.start_time ) ) - (3600 * business.timezone) as vivipos_starttime,
		UNIX_TIMESTAMP( promotions.end_date ) - (3600 * business.timezone) + 59 as vivipos_enddate,
		UNIX_TIMESTAMP( CONCAT( promotions.start_date, ' ', promotions.end_time ) ) - (3600 * business.timezone) + 59 as vivipos_endtime,
		GROUP_CONCAT( vPromoDays.day ORDER BY vPromoDays.day ) as vivipos_days,
		promo_type.vivipos_name as vivipos_type,
		promo_trigger.vivipos_name as vivipos_trigger,
		promo_target.db_table as promotarget_table,
		promo_target.order_by as promotarget_order,
		UNIX_TIMESTAMP( NOW() ) as vivipos_modified,
		promotions.activeFlag as vivipos_active,
        1 AS is_global,
		promotions.subsidy as alt_name1
	from promotions
	join promo_type on promotions.type=promo_type.id
	join promo_trigger on promotions.promo_trigger=promo_trigger.id
	join promo_target on promo_trigger.target_id=promo_target.id
	left join menu_tax on promotions.tax_id = menu_tax.id
	left join vPromoDays on promotions.id=vPromoDays.id
	join promo_mapping ON promo_mapping.promotionid = promotions.id
    join business ON business.businessid=promo_mapping.businessid
	group by business.businessid, promo_id

;
