CREATE TABLE IF NOT EXISTS `promo_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `PromotionId` int(10) NOT NULL,
  `BusinessId` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Keep track of the businesses that a (global) promotion appli' AUTO_INCREMENT=1 ;
