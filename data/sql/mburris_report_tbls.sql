-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: report1
-- Generation Time: Feb 08, 2013 at 02:03 PM
-- Server version: 5.5.24
-- PHP Version: 5.3.10-1ubuntu3.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mburris_report`
--

-- --------------------------------------------------------

--
-- Table structure for table `cash_in_monthly`
--

DROP TABLE IF EXISTS `cash_in_monthly`;
CREATE TABLE IF NOT EXISTS `cash_in_monthly` (
  `companyid` int(11) DEFAULT NULL,
  `date_start` date DEFAULT '0000-00-00',
  `date_end` date DEFAULT '0000-00-00',
  `districtid` int(11) DEFAULT NULL,
  `districtname` varchar(250) DEFAULT NULL,
  `businessid` int(11) DEFAULT NULL,
  `businessname` varchar(250) DEFAULT NULL,
  `day_1` decimal(8,2) DEFAULT '0.00',
  `day_2` decimal(8,2) DEFAULT '0.00',
  `day_3` decimal(8,2) DEFAULT '0.00',
  `day_4` decimal(8,2) DEFAULT '0.00',
  `day_5` decimal(8,2) DEFAULT '0.00',
  `day_6` decimal(8,2) DEFAULT '0.00',
  `day_7` decimal(8,2) DEFAULT '0.00',
  `day_8` decimal(8,2) DEFAULT '0.00',
  `day_9` decimal(8,2) DEFAULT '0.00',
  `day_10` decimal(8,2) DEFAULT '0.00',
  `day_11` decimal(8,2) DEFAULT '0.00',
  `day_12` decimal(8,2) DEFAULT '0.00',
  `day_13` decimal(8,2) DEFAULT '0.00',
  `day_14` decimal(8,2) DEFAULT '0.00',
  `day_15` decimal(8,2) DEFAULT '0.00',
  `day_16` decimal(8,2) DEFAULT '0.00',
  `day_17` decimal(8,2) DEFAULT '0.00',
  `day_18` decimal(8,2) DEFAULT '0.00',
  `day_19` decimal(8,2) DEFAULT '0.00',
  `day_20` decimal(8,2) DEFAULT '0.00',
  `day_21` decimal(8,2) DEFAULT '0.00',
  `day_22` decimal(8,2) DEFAULT '0.00',
  `day_23` decimal(8,2) DEFAULT '0.00',
  `day_24` decimal(8,2) DEFAULT '0.00',
  `day_25` decimal(8,2) DEFAULT '0.00',
  `day_26` decimal(8,2) DEFAULT '0.00',
  `day_27` decimal(8,2) DEFAULT '0.00',
  `day_28` decimal(8,2) DEFAULT '0.00',
  `day_29` decimal(8,2) DEFAULT '0.00',
  `day_30` decimal(8,2) DEFAULT '0.00',
  `day_31` decimal(8,2) DEFAULT '0.00',
  KEY `companyid` (`companyid`) USING BTREE
) ENGINE=MEMORY DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cash_in_weekly`
--

DROP TABLE IF EXISTS `cash_in_weekly`;
CREATE TABLE IF NOT EXISTS `cash_in_weekly` (
  `companyid` int(11) DEFAULT NULL,
  `date_start` date DEFAULT '0000-00-00',
  `date_end` date DEFAULT '0000-00-00',
  `districtid` int(11) DEFAULT NULL,
  `districtname` varchar(250) DEFAULT NULL,
  `businessid` int(11) DEFAULT NULL,
  `businessname` varchar(250) DEFAULT NULL,
  `day_1` decimal(8,2) DEFAULT '0.00',
  `day_2` decimal(8,2) DEFAULT '0.00',
  `day_3` decimal(8,2) DEFAULT '0.00',
  `day_4` decimal(8,2) DEFAULT '0.00',
  `day_5` decimal(8,2) DEFAULT '0.00',
  `day_6` decimal(8,2) DEFAULT '0.00',
  `day_7` decimal(8,2) DEFAULT '0.00',
  KEY `companyid` (`companyid`) USING BTREE
) ENGINE=MEMORY DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cash_out`
--

DROP TABLE IF EXISTS `cash_out`;
CREATE TABLE IF NOT EXISTS `cash_out` (
  `companyid` int(11) DEFAULT NULL,
  `date_start` datetime DEFAULT '0000-00-00 00:00:00',
  `date_end` datetime DEFAULT '0000-00-00 00:00:00',
  `districtid` int(11) DEFAULT NULL,
  `districtname` varchar(250) DEFAULT NULL,
  `businessid` int(11) DEFAULT NULL,
  `businessname` varchar(250) DEFAULT NULL,
  `cash_out_amount` decimal(8,2) DEFAULT '0.00',
  KEY `companyid` (`companyid`),
  KEY `companyid_2` (`companyid`,`date_end`) USING BTREE
) ENGINE=MEMORY DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ck_fees`
--

DROP TABLE IF EXISTS `ck_fees`;
CREATE TABLE IF NOT EXISTS `ck_fees` (
  `companyid` int(11) DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `districtid` int(11) DEFAULT NULL,
  `districtname` varchar(250) DEFAULT NULL,
  `businessid` int(11) DEFAULT NULL,
  `businessname` varchar(250) DEFAULT NULL,
  `net_sales` decimal(8,2) DEFAULT '0.00',
  `ck_fee_amount` decimal(8,2) DEFAULT '0.00',
  `ck_fee_percent` decimal(8,2) DEFAULT '0.00',
  `credit_card_fee` decimal(8,2) DEFAULT '0.00',
  `credit_card_fee_percent` decimal(8,2) DEFAULT '0.00',
  KEY `companyid` (`companyid`),
  KEY `companyid_2` (`companyid`,`date_end`) USING BTREE
) ENGINE=MEMORY DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customer_promo`
--

DROP TABLE IF EXISTS `customer_promo`;
CREATE TABLE IF NOT EXISTS `customer_promo` (
  `companyid` int(11) DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `districtid` int(11) DEFAULT NULL,
  `districtname` varchar(250) DEFAULT NULL,
  `businessid` int(11) DEFAULT NULL,
  `businessname` varchar(250) DEFAULT NULL,
  `promo_amount` decimal(8,2) DEFAULT '0.00',
  KEY `companyid` (`companyid`),
  KEY `companyid_2` (`companyid`,`date_end`) USING BTREE
) ENGINE=MEMORY DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customer_refund`
--

DROP TABLE IF EXISTS `customer_refund`;
CREATE TABLE IF NOT EXISTS `customer_refund` (
  `companyid` int(11) DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `districtid` int(11) DEFAULT NULL,
  `districtname` varchar(250) DEFAULT NULL,
  `businessid` int(11) DEFAULT NULL,
  `businessname` varchar(250) DEFAULT NULL,
  `refund_total` decimal(8,2) DEFAULT '0.00',
  KEY `companyid` (`companyid`),
  KEY `companyid_2` (`companyid`,`date_end`) USING BTREE
) ENGINE=MEMORY DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `financial_report`
--

DROP TABLE IF EXISTS `financial_report`;
CREATE TABLE IF NOT EXISTS `financial_report` (
  `companyid` int(11) NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `districtid` int(11) NOT NULL,
  `districtname` varchar(250) NOT NULL,
  `businessid` int(11) NOT NULL,
  `businessname` varchar(250) NOT NULL,
  `gross_sales` decimal(8,2) DEFAULT '0.00',
  `taxes` decimal(8,2) DEFAULT '0.00',
  `net_sales` decimal(8,2) DEFAULT '0.00',
  `cost` decimal(8,2) DEFAULT '0.00',
  `waste` decimal(8,2) DEFAULT '0.00',
  `waste_percent` decimal(8,2) DEFAULT '0.00',
  `shrink` decimal(8,2) DEFAULT '0.00',
  `shrink_percent` decimal(8,2) DEFAULT '0.00',
  `cost_waste` decimal(8,2) DEFAULT '0.00',
  `cost_waste_percent` decimal(8,2) DEFAULT '0.00',
  `cost_waste_shrink` decimal(8,2) DEFAULT '0.00',
  `cost_waste_shrink_percent` decimal(8,2) DEFAULT '0.00',
  UNIQUE KEY `companyid_2` (`companyid`,`date_start`,`date_end`,`businessid`) USING BTREE,
  KEY `companyid` (`companyid`,`date_end`) USING BTREE
) ENGINE=MEMORY DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `financial_report_2_mockup`
--

DROP TABLE IF EXISTS `financial_report_2_mockup`;
CREATE TABLE IF NOT EXISTS `financial_report_2_mockup` (
  `companyid` int(11) NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `districtid` int(11) NOT NULL,
  `districtname` varchar(250) NOT NULL,
  `businessid` int(11) NOT NULL,
  `businessname` varchar(250) NOT NULL,
  `gross_sales` decimal(8,2) DEFAULT '0.00',
  `taxes` decimal(8,2) DEFAULT '0.00',
  `net_sales` decimal(8,2) DEFAULT '0.00',
  `cost` decimal(8,2) DEFAULT '0.00',
  `waste` decimal(8,2) DEFAULT '0.00',
  `waste_percent` decimal(8,2) DEFAULT '0.00',
  `shrink` decimal(8,2) DEFAULT '0.00',
  `shrink_percent` decimal(8,2) DEFAULT '0.00',
  `cost_waste` decimal(8,2) DEFAULT '0.00',
  `cost_waste_percent` decimal(8,2) DEFAULT '0.00',
  `cost_waste_shrink` decimal(8,2) DEFAULT '0.00',
  `cost_waste_shrink_percent` decimal(8,2) DEFAULT '0.00',
  `beginning_ck_card_balance` decimal(13,2) DEFAULT '0.00',
  `cash_received` decimal(8,2) DEFAULT '0.00',
  `credit_card_receipts` decimal(8,2) DEFAULT '0.00',
  `credit_card_receipts_percent` decimal(8,2) DEFAULT '0.00',
  `refunds` decimal(8,2) DEFAULT '0.00',
  `initial_promo` decimal(8,2) DEFAULT '0.00',
  `ending_ck_card_balance` decimal(13,2) DEFAULT '0.00',
  `payroll_deduct` decimal(8,2) NOT NULL DEFAULT '0.00',
  `net_balances` decimal(13,2) DEFAULT '0.00',
  `ck_card_sales` decimal(8,2) DEFAULT '0.00',
  `credit_card_sales` decimal(8,2) DEFAULT '0.00',
  `credit_card_sales_percent` decimal(8,2) DEFAULT '0.00',
  `bottle_deposits` decimal(8,2) DEFAULT '0.00',
  `cost_percent` decimal(8,2) DEFAULT '0.00',
  `unfunded_promotions` decimal(8,2) DEFAULT '0.00',
  `unfunded_promotion_percent` decimal(8,2) DEFAULT '0.00',
  `credit_card_fee` decimal(8,2) DEFAULT '0.00',
  `credit_card_fee_percent` decimal(8,2) DEFAULT '0.00',
  `ck_fee` decimal(8,2) DEFAULT '0.00',
  `ck_fee_percent` decimal(8,2) DEFAULT '0.00',
  `ck_monthly_maintenance_fee` decimal(8,2) DEFAULT '0.00',
  `operating_profit` decimal(8,2) DEFAULT '0.00',
  `operating_profit_percent` decimal(8,2) DEFAULT '0.00',
  UNIQUE KEY `companyid_2` (`companyid`,`date_start`,`date_end`,`businessid`) USING BTREE,
  KEY `companyid` (`companyid`,`date_end`) USING BTREE
) ENGINE=MEMORY DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inventory_detail`
--

DROP TABLE IF EXISTS `inventory_detail`;
CREATE TABLE IF NOT EXISTS `inventory_detail` (
  `companyid` int(11) NOT NULL DEFAULT '0',
  `date_start` date DEFAULT '0000-00-00',
  `date_end` date DEFAULT '0000-00-00',
  `date_time` datetime DEFAULT NULL,
  `districtid` int(11) DEFAULT '0',
  `districtname` varchar(250) DEFAULT NULL,
  `businessid` int(11) DEFAULT '0',
  `businessname` varchar(250) DEFAULT NULL,
  `posid` int(11) NOT NULL,
  `group_name` varchar(100) DEFAULT NULL,
  `item_code` varchar(250) DEFAULT NULL,
  `item_name` varchar(250) DEFAULT NULL,
  `onhand` int(11) DEFAULT '0',
  `cost` decimal(8,3) DEFAULT '0.000',
  `amount` decimal(8,3) DEFAULT '0.000',
  `shelf_life_days` smallint(6) DEFAULT '0',
  `route` varchar(25) DEFAULT NULL,
  `active_status` tinyint(1) NOT NULL DEFAULT '0',
  `onhand_amount` int(11) NOT NULL DEFAULT '0',
  `fills` int(11) NOT NULL DEFAULT '0',
  `waste2` int(11) NOT NULL DEFAULT '0',
  `items_sold_1` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `companyid_3` (`companyid`,`date_start`,`date_end`,`businessid`,`item_code`) USING BTREE,
  KEY `companyid_2` (`companyid`,`date_start`,`date_end`) USING BTREE,
  KEY `companyid` (`companyid`) USING BTREE
) ENGINE=MEMORY DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inventory_sales`
--

DROP TABLE IF EXISTS `inventory_sales`;
CREATE TABLE IF NOT EXISTS `inventory_sales` (
  `companyid` int(11) DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `dtv` datetime DEFAULT NULL,
  `posid` int(11) DEFAULT NULL,
  `districtid` int(11) DEFAULT NULL,
  `districtname` varchar(250) DEFAULT NULL,
  `businessid` int(11) DEFAULT NULL,
  `businessname` varchar(250) DEFAULT NULL,
  `group_name` varchar(100) DEFAULT NULL,
  `item_code` varchar(250) DEFAULT NULL,
  `item_name` varchar(250) DEFAULT NULL,
  `cost` double DEFAULT NULL,
  `price` decimal(8,2) DEFAULT '0.00',
  `items_sold` int(11) DEFAULT '0',
  `extended_cost` decimal(8,2) DEFAULT '0.00',
  `extended_price` decimal(8,2) DEFAULT '0.00',
  `shelf_life_days` int(11) DEFAULT NULL,
  `route` varchar(25) DEFAULT NULL,
  UNIQUE KEY `companyid_2` (`companyid`,`date_start`,`date_end`,`businessid`,`item_code`) USING BTREE,
  KEY `companyid` (`companyid`) USING BTREE
) ENGINE=MEMORY DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inventory_sales_usbank`
--

DROP TABLE IF EXISTS `inventory_sales_usbank`;
CREATE TABLE IF NOT EXISTS `inventory_sales_usbank` (
  `companyid` int(11) DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `dtv` datetime DEFAULT NULL,
  `posid` int(11) DEFAULT NULL,
  `districtid` int(11) DEFAULT NULL,
  `districtname` varchar(250) DEFAULT NULL,
  `businessid` int(11) DEFAULT NULL,
  `businessname` varchar(250) DEFAULT NULL,
  `group_name` varchar(100) DEFAULT NULL,
  `item_code` varchar(250) DEFAULT NULL,
  `item_name` varchar(250) DEFAULT NULL,
  `cost` double DEFAULT NULL,
  `price` decimal(8,2) DEFAULT '0.00',
  `items_sold` int(11) DEFAULT '0',
  `extended_cost` decimal(8,2) DEFAULT '0.00',
  `extended_price` decimal(8,2) DEFAULT '0.00',
  `shelf_life_days` int(11) DEFAULT NULL,
  `route` varchar(25) DEFAULT NULL,
  UNIQUE KEY `companyid_2` (`companyid`,`date_start`,`date_end`,`businessid`,`item_code`) USING BTREE,
  KEY `companyid` (`companyid`) USING BTREE
) ENGINE=MEMORY DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inventory_summary`
--

DROP TABLE IF EXISTS `inventory_summary`;
CREATE TABLE IF NOT EXISTS `inventory_summary` (
  `companyid` int(11) NOT NULL DEFAULT '0',
  `date_start` date DEFAULT '0000-00-00',
  `date_end` date DEFAULT '0000-00-00',
  `date_time` datetime DEFAULT NULL,
  `districtid` int(11) DEFAULT '0',
  `districtname` varchar(250) DEFAULT NULL,
  `businessid` int(11) DEFAULT '0',
  `businessname` varchar(250) DEFAULT NULL,
  `posid` int(11) NOT NULL,
  `group_name` varchar(100) DEFAULT NULL,
  `item_code` varchar(250) DEFAULT NULL,
  `item_name` varchar(250) DEFAULT NULL,
  `onhand` int(11) DEFAULT '0',
  `cost` decimal(8,3) DEFAULT '0.000',
  `amount` decimal(8,3) DEFAULT '0.000',
  `shelf_life_days` smallint(6) DEFAULT '0',
  `route` varchar(25) DEFAULT NULL,
  `active_status` tinyint(1) NOT NULL DEFAULT '0',
  `onhand_amount` int(11) NOT NULL DEFAULT '0',
  `fills` int(11) NOT NULL DEFAULT '0',
  `waste2` int(11) NOT NULL DEFAULT '0',
  `items_sold_1` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `companyid_3` (`companyid`,`date_start`,`date_end`,`businessid`,`item_code`) USING BTREE,
  KEY `companyid_2` (`companyid`,`date_start`,`date_end`) USING BTREE,
  KEY `companyid` (`companyid`) USING BTREE
) ENGINE=MEMORY DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inventory_waste`
--

DROP TABLE IF EXISTS `inventory_waste`;
CREATE TABLE IF NOT EXISTS `inventory_waste` (
  `companyid` int(11) DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `districtid` int(11) DEFAULT NULL,
  `districtname` varchar(250) DEFAULT NULL,
  `businessid` int(11) DEFAULT NULL,
  `businessname` varchar(250) DEFAULT NULL,
  `group_name` varchar(100) DEFAULT NULL,
  `item_code` varchar(250) DEFAULT NULL,
  `item_name` varchar(250) DEFAULT NULL,
  `cost` decimal(8,2) DEFAULT '0.00',
  `price` decimal(8,2) DEFAULT '0.00',
  `items_sold` int(11) DEFAULT '0',
  `waste` int(11) DEFAULT '0',
  `amount` decimal(8,2) DEFAULT NULL,
  `shelf_life_days` int(11) DEFAULT '0',
  `route` varchar(30) DEFAULT NULL,
  UNIQUE KEY `companyid_3` (`companyid`,`date_start`,`date_end`,`businessid`,`item_code`) USING BTREE,
  KEY `companyid_2` (`companyid`,`date_end`) USING BTREE,
  KEY `companyid` (`companyid`) USING BTREE
) ENGINE=MEMORY DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inventory_waste_financial`
--

DROP TABLE IF EXISTS `inventory_waste_financial`;
CREATE TABLE IF NOT EXISTS `inventory_waste_financial` (
  `companyid` int(11) DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `districtid` int(11) DEFAULT NULL,
  `districtname` varchar(250) DEFAULT NULL,
  `businessid` int(11) DEFAULT NULL,
  `businessname` varchar(250) DEFAULT NULL,
  `group_name` varchar(100) DEFAULT NULL,
  `item_code` varchar(250) DEFAULT NULL,
  `item_name` varchar(250) DEFAULT NULL,
  `cost` double DEFAULT NULL,
  `price` decimal(8,3) DEFAULT '0.000',
  `items_sold` int(11) DEFAULT '0',
  `waste` int(11) DEFAULT '0',
  `amount` decimal(8,2) DEFAULT NULL,
  `shelf_life_days` int(11) DEFAULT '0',
  `route` varchar(25) DEFAULT NULL,
  UNIQUE KEY `companyid_3` (`companyid`,`date_start`,`date_end`,`businessid`,`item_code`) USING BTREE,
  KEY `companyid_2` (`companyid`,`date_end`) USING BTREE,
  KEY `companyid` (`companyid`) USING BTREE
) ENGINE=MEMORY DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inventory_waste_financial_extras`
--

DROP TABLE IF EXISTS `inventory_waste_financial_extras`;
CREATE TABLE IF NOT EXISTS `inventory_waste_financial_extras` (
  `companyid` int(11) DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `districtid` int(11) DEFAULT NULL,
  `districtname` varchar(250) DEFAULT NULL,
  `businessid` int(11) DEFAULT NULL,
  `businessname` varchar(250) DEFAULT NULL,
  `group_name` varchar(100) DEFAULT NULL,
  `item_code` varchar(250) DEFAULT NULL,
  `item_name` varchar(250) DEFAULT NULL,
  `cost` double DEFAULT NULL,
  `price` decimal(8,3) DEFAULT '0.000',
  `items_sold` int(11) DEFAULT '0',
  `waste` int(11) DEFAULT '0',
  `amount` decimal(8,2) DEFAULT NULL,
  `shelf_life_days` int(11) DEFAULT '0',
  `route` varchar(25) DEFAULT NULL,
  UNIQUE KEY `companyid_3` (`companyid`,`date_start`,`date_end`,`businessid`,`item_code`) USING BTREE,
  KEY `companyid_2` (`companyid`,`date_end`) USING BTREE,
  KEY `companyid` (`companyid`) USING BTREE
) ENGINE=MEMORY DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `shrink_trend`
--

DROP TABLE IF EXISTS `shrink_trend`;
CREATE TABLE IF NOT EXISTS `shrink_trend` (
  `companyid` int(11) DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `districtid` int(11) DEFAULT NULL,
  `districtname` varchar(250) DEFAULT NULL,
  `businessid` int(11) DEFAULT NULL,
  `businessname` varchar(250) DEFAULT NULL,
  `week_1_shrink_trend` decimal(8,2) DEFAULT '0.00',
  `week_1_shrink_trend_percent` decimal(8,1) DEFAULT '0.0',
  `week_2_shrink_trend` decimal(8,2) DEFAULT '0.00',
  `week_2_shrink_trend_percent` decimal(8,1) DEFAULT '0.0',
  `week_3_shrink_trend` decimal(8,2) DEFAULT '0.00',
  `week_3_shrink_trend_percent` decimal(8,1) DEFAULT '0.0',
  `week_4_shrink_trend` decimal(8,2) DEFAULT '0.00',
  `week_4_shrink_trend_percent` decimal(8,1) DEFAULT '0.0',
  `week_5_shrink_trend` decimal(8,2) DEFAULT '0.00',
  `week_5_shrink_trend_percent` decimal(8,1) DEFAULT '0.0',
  `week_6_shrink_trend` decimal(8,2) DEFAULT '0.00',
  `week_6_shrink_trend_percent` decimal(8,1) DEFAULT '0.0',
  `week_7_shrink_trend` decimal(8,2) DEFAULT '0.00',
  `week_7_shrink_trend_percent` decimal(8,1) DEFAULT '0.0',
  `week_8_shrink_trend` decimal(8,2) DEFAULT '0.00',
  `week_8_shrink_trend_percent` decimal(8,1) DEFAULT '0.0',
  `week_9_shrink_trend` decimal(8,2) DEFAULT '0.00',
  `week_9_shrink_trend_percent` decimal(8,1) DEFAULT '0.0',
  `week_10_shrink_trend` decimal(8,2) DEFAULT '0.00',
  `week_10_shrink_trend_percent` decimal(8,1) DEFAULT '0.0',
  `week_11_shrink_trend` decimal(8,2) DEFAULT '0.00',
  `week_11_shrink_trend_percent` decimal(8,1) DEFAULT '0.0',
  `week_12_shrink_trend` decimal(8,2) DEFAULT '0.00',
  `week_12_shrink_trend_percent` decimal(8,1) DEFAULT '0.0',
  `week_1_sales_trend` decimal(8,2) DEFAULT '0.00',
  `week_2_sales_trend` decimal(8,2) DEFAULT '0.00',
  `week_3_sales_trend` decimal(8,2) DEFAULT '0.00',
  `week_4_sales_trend` decimal(8,2) DEFAULT '0.00',
  `week_5_sales_trend` decimal(8,2) DEFAULT '0.00',
  `week_6_sales_trend` decimal(8,2) DEFAULT '0.00',
  `week_7_sales_trend` decimal(8,2) DEFAULT '0.00',
  `week_8_sales_trend` decimal(8,2) DEFAULT '0.00',
  `week_9_sales_trend` decimal(8,2) DEFAULT '0.00',
  `week_10_sales_trend` decimal(8,2) DEFAULT '0.00',
  `week_11_sales_trend` decimal(8,2) DEFAULT '0.00',
  `week_12_sales_trend` decimal(8,2) DEFAULT '0.00',
  KEY `companyid` (`companyid`),
  KEY `companyid_2` (`companyid`,`date_end`) USING BTREE
) ENGINE=MEMORY DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `waste_trend`
--

DROP TABLE IF EXISTS `waste_trend`;
CREATE TABLE IF NOT EXISTS `waste_trend` (
  `companyid` int(11) DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `districtid` int(11) DEFAULT NULL,
  `districtname` varchar(250) DEFAULT NULL,
  `businessid` int(11) DEFAULT NULL,
  `businessname` varchar(250) DEFAULT NULL,
  `week_1_waste_trend` decimal(8,2) DEFAULT '0.00',
  `week_1_waste_trend_percent` decimal(8,1) DEFAULT '0.0',
  `week_2_waste_trend` decimal(8,2) DEFAULT '0.00',
  `week_2_waste_trend_percent` decimal(8,1) DEFAULT '0.0',
  `week_3_waste_trend` decimal(8,2) DEFAULT '0.00',
  `week_3_waste_trend_percent` decimal(8,1) DEFAULT '0.0',
  `week_4_waste_trend` decimal(8,2) DEFAULT '0.00',
  `week_4_waste_trend_percent` decimal(8,1) DEFAULT '0.0',
  `week_5_waste_trend` decimal(8,2) DEFAULT '0.00',
  `week_5_waste_trend_percent` decimal(8,1) DEFAULT '0.0',
  `week_6_waste_trend` decimal(8,2) DEFAULT '0.00',
  `week_6_waste_trend_percent` decimal(8,1) DEFAULT '0.0',
  `week_7_waste_trend` decimal(8,2) DEFAULT '0.00',
  `week_7_waste_trend_percent` decimal(8,1) DEFAULT '0.0',
  `week_8_waste_trend` decimal(8,2) DEFAULT '0.00',
  `week_8_waste_trend_percent` decimal(8,1) DEFAULT '0.0',
  `week_9_waste_trend` decimal(8,2) DEFAULT '0.00',
  `week_9_waste_trend_percent` decimal(8,1) DEFAULT '0.0',
  `week_10_waste_trend` decimal(8,2) DEFAULT '0.00',
  `week_10_waste_trend_percent` decimal(8,1) DEFAULT '0.0',
  `week_11_waste_trend` decimal(8,2) DEFAULT '0.00',
  `week_11_waste_trend_percent` decimal(8,1) DEFAULT '0.0',
  `week_12_waste_trend` decimal(8,2) DEFAULT '0.00',
  `week_12_waste_trend_percent` decimal(8,1) DEFAULT '0.0',
  `week_1_sales_trend` decimal(8,2) DEFAULT '0.00',
  `week_2_sales_trend` decimal(8,2) DEFAULT '0.00',
  `week_3_sales_trend` decimal(8,2) DEFAULT '0.00',
  `week_4_sales_trend` decimal(8,2) DEFAULT '0.00',
  `week_5_sales_trend` decimal(8,2) DEFAULT '0.00',
  `week_6_sales_trend` decimal(8,2) DEFAULT '0.00',
  `week_7_sales_trend` decimal(8,2) DEFAULT '0.00',
  `week_8_sales_trend` decimal(8,2) DEFAULT '0.00',
  `week_9_sales_trend` decimal(8,2) DEFAULT '0.00',
  `week_10_sales_trend` decimal(8,2) DEFAULT '0.00',
  `week_11_sales_trend` decimal(8,2) DEFAULT '0.00',
  `week_12_sales_trend` decimal(8,2) DEFAULT '0.00',
  KEY `companyid` (`companyid`)
) ENGINE=MEMORY DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
