
ALTER TABLE `log_actions` ADD `log_type_id` INT UNSIGNED NOT NULL AFTER `at`, ADD INDEX ( `log_type_id` );

CREATE TABLE IF NOT EXISTS `log_types` (
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`namespace` varchar(50) NOT NULL,
	`type` enum('other','database','model','form') NOT NULL DEFAULT 'other',
	PRIMARY KEY (`id`),
	KEY `namespace` (`namespace`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `log_types` (`id`, `namespace`, `type`) VALUES(1, 'treat.Treat.Model.Aeris.Setting', 'model');

UPDATE `log_actions` SET `log_type_id` = 1;

ALTER TABLE `district` ADD `kiosk_activation_emails` VARCHAR( 500 ) NOT NULL;

DROP TABLE IF EXISTS `kiosk_sync_schedules`;
CREATE TABLE IF NOT EXISTS `kiosk_sync_schedules` (
	`client_programid` int(11) unsigned NOT NULL,
	`item_id` int(11) unsigned NOT NULL,
	`item_type` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '1=items,2=groups,3=tax,4=itemtax,5=promo,6=promo_groups',
	`move_to_sync` datetime DEFAULT NULL,
	UNIQUE KEY `client_programid` (`client_programid`,`item_id`,`item_type`,`move_to_sync`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `scheduled_changes`;
CREATE TABLE IF NOT EXISTS `scheduled_changes` (
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`scheduled_date` datetime NOT NULL,
	`namespace` varchar(100) NOT NULL,
	`reference_id` int(10) unsigned NOT NULL,
	`version` float(4,2) NOT NULL,
	`type` enum('model','sql','sync','other') NOT NULL,
	`data` text NOT NULL,
	`error` text NOT NULL,
	`created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`),
	KEY `scheduled_date` (`scheduled_date`),
	KEY `namespace` (`namespace`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `kiosk_deactivations`;
CREATE TABLE IF NOT EXISTS `kiosk_deactivations` (
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`reason` text NOT NULL,
	`closed_status` enum('permanent','relocate') NOT NULL DEFAULT 'permanent',
	`old_business_id` int(10) unsigned NOT NULL,
	`new_business_id` int(10) unsigned NOT NULL,
	`scheduled_date` date NOT NULL,
	`created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`),
	KEY `old_business_id` (`old_business_id`),
	KEY `new_business_id` (`new_business_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;