
DROP TABLE IF EXISTS `000_checksum`;

DROP TABLE IF EXISTS `accountpend`;
DROP TABLE IF EXISTS `accounts`;
DROP TABLE IF EXISTS `accounts_group`;
DROP TABLE IF EXISTS `accounts_groupdetail`;
DROP TABLE IF EXISTS `acct_payable`;
DROP TABLE IF EXISTS `acct_status`;
	
DROP TABLE IF EXISTS `add_on`;
DROP TABLE IF EXISTS `aeris_settings`;
DROP TABLE IF EXISTS `apinvoice`;
DROP TABLE IF EXISTS `apinvoicedetail`;
DROP TABLE IF EXISTS `apinvoice_multi_trans`;
	
DROP TABLE IF EXISTS `audit_budget`;
DROP TABLE IF EXISTS `audit_collection`;
DROP TABLE IF EXISTS `audit_comm`;
DROP TABLE IF EXISTS `audit_estimate`;
DROP TABLE IF EXISTS `audit_invoice`;
DROP TABLE IF EXISTS `audit_labor`;
DROP TABLE IF EXISTS `audit_login`;
DROP TABLE IF EXISTS `audit_menu_items`;
DROP TABLE IF EXISTS `audit_reconcile`;
DROP TABLE IF EXISTS `audit_route`;
DROP TABLE IF EXISTS `audit_sales`;
DROP TABLE IF EXISTS `audit_vend`;
DROP TABLE IF EXISTS `auto_login`;
	
DROP TABLE IF EXISTS `bboard`;
DROP TABLE IF EXISTS `bboarddetail`;
DROP TABLE IF EXISTS `blacklist_ip`;
DROP TABLE IF EXISTS `budget`;
DROP TABLE IF EXISTS `budget_adjust`;
DROP TABLE IF EXISTS `bulletin`;
DROP TABLE IF EXISTS `business`;
DROP TABLE IF EXISTS `business_category`;
DROP TABLE IF EXISTS `business_gateway_data`;
#DROP TABLE IF EXISTS `business_group`;
#DROP TABLE IF EXISTS `business_group_detail`;
	
DROP TABLE IF EXISTS `calendarTable`;
DROP TABLE IF EXISTS `calender`;
DROP TABLE IF EXISTS `cash`;
	
DROP TABLE IF EXISTS `caterclose`;
DROP TABLE IF EXISTS `caternote`;
#DROP TABLE IF EXISTS `caterpolicy`;
DROP TABLE IF EXISTS `cc_fee`;
DROP TABLE IF EXISTS `checkdetail`;
DROP TABLE IF EXISTS `checkdiscounts`;
DROP TABLE IF EXISTS `checks`;
	
DROP TABLE IF EXISTS `company`;
DROP TABLE IF EXISTS `company_type`;
DROP TABLE IF EXISTS `com_kpi`;
DROP TABLE IF EXISTS `contract_temp`;
DROP TABLE IF EXISTS `contract_temp2`;
	
DROP TABLE IF EXISTS `controlsheet`;
DROP TABLE IF EXISTS `conversion`;
DROP TABLE IF EXISTS `cost_history`;
	
DROP TABLE IF EXISTS `creditdetail`;
DROP TABLE IF EXISTS `credits`;
DROP TABLE IF EXISTS `credittype`;
DROP TABLE IF EXISTS `cs_memo`;
	
DROP TABLE IF EXISTS `customer`;
DROP TABLE IF EXISTS `customer_costcenter`;
DROP TABLE IF EXISTS `customer_limits`;
DROP TABLE IF EXISTS `customer_nutrition`;
DROP TABLE IF EXISTS `customer_scancodes`;
DROP TABLE IF EXISTS `customer_transactions`;
DROP TABLE IF EXISTS `customer_transactions_debug`;
DROP TABLE IF EXISTS `customer_transaction_types`;
	
DROP TABLE IF EXISTS `dashboard`;
DROP TABLE IF EXISTS `dashboard_aging`;
DROP TABLE IF EXISTS `dashboard_aging_salesmen`;
DROP TABLE IF EXISTS `dashboard_files`;
DROP TABLE IF EXISTS `dashboard_machine`;
DROP TABLE IF EXISTS `dashboard_route`;
DROP TABLE IF EXISTS `dashboard_update`;
	
DROP TABLE IF EXISTS `db_cache`;
DROP TABLE IF EXISTS `db_custom`;
DROP TABLE IF EXISTS `db_group`;
DROP TABLE IF EXISTS `db_group_graphs`;
DROP TABLE IF EXISTS `db_group_members`;
DROP TABLE IF EXISTS `db_public`;
DROP TABLE IF EXISTS `db_trendlines`;
DROP TABLE IF EXISTS `db_view`;
DROP TABLE IF EXISTS `db_view_order`;
DROP TABLE IF EXISTS `db_what`;
DROP TABLE IF EXISTS `db_who`;
	
DROP TABLE IF EXISTS `debitdetail`;
DROP TABLE IF EXISTS `debits`;
DROP TABLE IF EXISTS `debittype`;
	
DROP TABLE IF EXISTS `deposit`;
DROP TABLE IF EXISTS `depositdetail`;
DROP TABLE IF EXISTS `depositinvoice`;
	
DROP TABLE IF EXISTS `district`;
DROP TABLE IF EXISTS `dist_kpi`;
DROP TABLE IF EXISTS `division`;
	
DROP TABLE IF EXISTS `endlesssummer`;
DROP TABLE IF EXISTS `est_accountowner`;
DROP TABLE IF EXISTS `est_accountowner_budget`;
DROP TABLE IF EXISTS `est_accountowner_type`;
DROP TABLE IF EXISTS `est_accounts`;
DROP TABLE IF EXISTS `est_accountstatus`;
DROP TABLE IF EXISTS `est_channel`;
DROP TABLE IF EXISTS `est_customertype`;
DROP TABLE IF EXISTS `est_locations`;
DROP TABLE IF EXISTS `est_opportunity`;
DROP TABLE IF EXISTS `est_op_status`;
DROP TABLE IF EXISTS `est_redlist`;
DROP TABLE IF EXISTS `est_region`;
DROP TABLE IF EXISTS `est_servicetype`;
	
DROP TABLE IF EXISTS `expense_report_category`;
DROP TABLE IF EXISTS `expense_report_link`;
DROP TABLE IF EXISTS `export_history`;
	
DROP TABLE IF EXISTS `impress`;
	
DROP TABLE IF EXISTS `inventor`;
DROP TABLE IF EXISTS `inventor2`;
	
DROP TABLE IF EXISTS `invoice`;
DROP TABLE IF EXISTS `invoice2`;
DROP TABLE IF EXISTS `invoicedetail`;
DROP TABLE IF EXISTS `invoice_custom_fields`;
DROP TABLE IF EXISTS `invoice_custom_fields_select`;
DROP TABLE IF EXISTS `invoice_custom_values`;
DROP TABLE IF EXISTS `invoice_setting`;
DROP TABLE IF EXISTS `invstatus`;
DROP TABLE IF EXISTS `invtender`;
DROP TABLE IF EXISTS `invtype`;
DROP TABLE IF EXISTS `inv_area`;
DROP TABLE IF EXISTS `inv_areadetail`;
DROP TABLE IF EXISTS `inv_count`;
DROP TABLE IF EXISTS `inv_count_new`;
DROP TABLE IF EXISTS `inv_count_vend`;
DROP TABLE IF EXISTS `inv_items`;
	
DROP TABLE IF EXISTS `jobtype`;
DROP TABLE IF EXISTS `jobtypedetail`;
	
DROP TABLE IF EXISTS `kiosk_sync`;
DROP TABLE IF EXISTS `kiosk_order_sched`;
DROP TABLE IF EXISTS `kiosk_order_type`;
DROP TABLE IF EXISTS `kiosk_scripts`;

DROP TABLE IF EXISTS `labor`;
DROP TABLE IF EXISTS `labor_alerts`;
DROP TABLE IF EXISTS `labor_clockin`;
DROP TABLE IF EXISTS `labor_clockin_options`;
DROP TABLE IF EXISTS `labor_comm`;
DROP TABLE IF EXISTS `labor_groups`;
DROP TABLE IF EXISTS `labor_group_detail`;
DROP TABLE IF EXISTS `labor_group_security`;
DROP TABLE IF EXISTS `labor_move`;
DROP TABLE IF EXISTS `labor_notify`;
DROP TABLE IF EXISTS `labor_route_link`;
DROP TABLE IF EXISTS `labor_temp`;
DROP TABLE IF EXISTS `labor_temp_comm`;
DROP TABLE IF EXISTS `labor_terminals`;
DROP TABLE IF EXISTS `labor_tips`;
	
DROP TABLE IF EXISTS `login`;
DROP TABLE IF EXISTS `login_all`;
DROP TABLE IF EXISTS `login_cards`;
DROP TABLE IF EXISTS `login_cashier`;
DROP TABLE IF EXISTS `login_cashier_link`;
DROP TABLE IF EXISTS `login_comp`;
DROP TABLE IF EXISTS `login_company`;
DROP TABLE IF EXISTS `login_company_type`;
DROP TABLE IF EXISTS `login_district`;
DROP TABLE IF EXISTS `login_division`;
DROP TABLE IF EXISTS `login_extended`;
DROP TABLE IF EXISTS `login_pend`;
DROP TABLE IF EXISTS `login_route`;
	
DROP TABLE IF EXISTS `machine_bus_link`;
DROP TABLE IF EXISTS `machine_collect`;
DROP TABLE IF EXISTS `machine_order`;
DROP TABLE IF EXISTS `machine_orderdetail`;
	
DROP TABLE IF EXISTS `manufacturer`;
DROP TABLE IF EXISTS `mei_product`;
	
DROP TABLE IF EXISTS `menu_alt_price`;
DROP TABLE IF EXISTS `menu_daypart`;
DROP TABLE IF EXISTS `menu_daypartdetail`;
DROP TABLE IF EXISTS `menu_groups`;
DROP TABLE IF EXISTS `menu_groups_active`;
DROP TABLE IF EXISTS `menu_items`;
#DROP TABLE IF EXISTS `menu_items_category`;
DROP TABLE IF EXISTS `menu_items_company`;
#DROP TABLE IF EXISTS `menu_items_master`;
DROP TABLE IF EXISTS `menu_items_ordertype`;
DROP TABLE IF EXISTS `menu_items_price`;
DROP TABLE IF EXISTS `menu_item_tax`;
DROP TABLE IF EXISTS `menu_portion`;
DROP TABLE IF EXISTS `menu_portiondetail`;
DROP TABLE IF EXISTS `menu_pricegroup`;
DROP TABLE IF EXISTS `menu_screen_order`;
DROP TABLE IF EXISTS `menu_tax`;
DROP TABLE IF EXISTS `menu_type`;
	
DROP TABLE IF EXISTS `mirror_accounts`;
DROP TABLE IF EXISTS `mirror_submit`;
DROP TABLE IF EXISTS `modifier`;
DROP TABLE IF EXISTS `mygadgets`;
DROP TABLE IF EXISTS `mygroups`;
DROP TABLE IF EXISTS `mygroup_detail`;
DROP TABLE IF EXISTS `mylinks`;
	
DROP TABLE IF EXISTS `nutrition`;
DROP TABLE IF EXISTS `nutrition_types`;
DROP TABLE IF EXISTS `nutrition_xref_company`;
DROP TABLE IF EXISTS `nutrition_xref_types`;
	
DROP TABLE IF EXISTS `optiongroup`;
DROP TABLE IF EXISTS `options`;
	
DROP TABLE IF EXISTS `order_detail`;
DROP TABLE IF EXISTS `order_item`;
DROP TABLE IF EXISTS `order_matrix`;
DROP TABLE IF EXISTS `order_matrixcust`;
DROP TABLE IF EXISTS `order_payments`;
DROP TABLE IF EXISTS `order_subs`;
DROP TABLE IF EXISTS `ordertype`;
	
DROP TABLE IF EXISTS `payment_detail`;
	
DROP TABLE IF EXISTS `payroll`;
DROP TABLE IF EXISTS `payroll_code`;
DROP TABLE IF EXISTS `payroll_code_comm`;
DROP TABLE IF EXISTS `payroll_code_control`;
DROP TABLE IF EXISTS `payroll_date`;
DROP TABLE IF EXISTS `payroll_department`;
DROP TABLE IF EXISTS `payroll_departmentdetail`;
	
DROP TABLE IF EXISTS `pm_access`;
DROP TABLE IF EXISTS `pm_accounts`;
DROP TABLE IF EXISTS `pm_category`;
DROP TABLE IF EXISTS `pm_comment`;
DROP TABLE IF EXISTS `pm_detail`;
DROP TABLE IF EXISTS `pm_division`;
DROP TABLE IF EXISTS `pm_list`;
DROP TABLE IF EXISTS `pm_message`;
DROP TABLE IF EXISTS `pm_status`;
DROP TABLE IF EXISTS `pm_users`;
	
DROP TABLE IF EXISTS `po_category`;
DROP TABLE IF EXISTS `po_note`;
DROP TABLE IF EXISTS `pos_report`;
DROP TABLE IF EXISTS `pos_report_cache`;
DROP TABLE IF EXISTS `pos_report_runlist`;
DROP TABLE IF EXISTS `price_history`;
	
DROP TABLE IF EXISTS `promotions`;
DROP TABLE IF EXISTS `promo_detail`;
DROP TABLE IF EXISTS `promo_groups`;
DROP TABLE IF EXISTS `promo_groups_detail`;
DROP TABLE IF EXISTS `promo_target`;
DROP TABLE IF EXISTS `promo_trigger`;
DROP TABLE IF EXISTS `promo_type`;
	
DROP TABLE IF EXISTS `pub_db_custom`;
DROP TABLE IF EXISTS `pub_db_trendlines`;
DROP TABLE IF EXISTS `pub_db_view`;
DROP TABLE IF EXISTS `pub_db_what`;
DROP TABLE IF EXISTS `pub_db_who`;
	
DROP TABLE IF EXISTS `purchase_card`;
	
DROP TABLE IF EXISTS `recipe`;
DROP TABLE IF EXISTS `recipe_comment`;
DROP TABLE IF EXISTS `recipe_prep`;
DROP TABLE IF EXISTS `recipe_rating`;
	
DROP TABLE IF EXISTS `reconcile_user`;
DROP TABLE IF EXISTS `recur`;
	
DROP TABLE IF EXISTS `reportdetail`;
DROP TABLE IF EXISTS `reporting_category`;
DROP TABLE IF EXISTS `reports`;
	
DROP TABLE IF EXISTS `reserve`;
DROP TABLE IF EXISTS `rooms`;
	
DROP TABLE IF EXISTS `route_inv`;
DROP TABLE IF EXISTS `route_parlevel`;
	
DROP TABLE IF EXISTS `sales_calc`;
DROP TABLE IF EXISTS `sales_calc_detail`;
	
DROP TABLE IF EXISTS `security`;
DROP TABLE IF EXISTS `security_control`;
DROP TABLE IF EXISTS `security_departments`;
DROP TABLE IF EXISTS `security_labor`;
	
DROP TABLE IF EXISTS `servery_station`;
	
DROP TABLE IF EXISTS `setup`;
DROP TABLE IF EXISTS `setup_detail`;
DROP TABLE IF EXISTS `setup_groups`;
	
DROP TABLE IF EXISTS `SiTech_Sessions`;
DROP TABLE IF EXISTS `size`;
	
DROP TABLE IF EXISTS `smartMenuAuth`;
DROP TABLE IF EXISTS `smartMenuBoards`;
DROP TABLE IF EXISTS `smartMenuClients`;
DROP TABLE IF EXISTS `smartMenuControl`;
DROP TABLE IF EXISTS `smartMenuBoardsLibraries`;
DROP TABLE IF EXISTS `smartMenuBoardsScreens`;
DROP TABLE IF EXISTS `smartMenuLocations`;
DROP TABLE IF EXISTS `smartMenuMenuItems`;
DROP TABLE IF EXISTS `smartMenuMenus`;
DROP TABLE IF EXISTS `smartMenuPackageLinks`;
DROP TABLE IF EXISTS `smartMenuPackageRequests`;
DROP TABLE IF EXISTS `smartMenuPackages`;
DROP TABLE IF EXISTS `smartMenus`;
DROP TABLE IF EXISTS `smartMenuTypes`;
DROP TABLE IF EXISTS `smartMenuUpload`;
	
DROP TABLE IF EXISTS `stats`;
	
DROP TABLE IF EXISTS `submit`;
DROP TABLE IF EXISTS `submit_labor`;
DROP TABLE IF EXISTS `submit_labor_dept`;
#DROP TABLE IF EXISTS `survey`;
#DROP TABLE IF EXISTS `survey_category`;
#DROP TABLE IF EXISTS `survey_detail`;
	
DROP TABLE IF EXISTS `transaction_log`;

DROP TABLE IF EXISTS `vehicle_maintenance`;
DROP TABLE IF EXISTS `vehicle_routes`;
DROP TABLE IF EXISTS `vehicle_services`;
	
DROP TABLE IF EXISTS `vendorpend`;
DROP TABLE IF EXISTS `vendors`;
DROP TABLE IF EXISTS `vendor_date`;
DROP TABLE IF EXISTS `vendor_import`;
DROP TABLE IF EXISTS `vendor_import_dates`;
DROP TABLE IF EXISTS `vendor_master`;
DROP TABLE IF EXISTS `vend_budget`;
DROP TABLE IF EXISTS `vend_bulletin`;
DROP TABLE IF EXISTS `vend_channel`;
DROP TABLE IF EXISTS `vend_collection`;
DROP TABLE IF EXISTS `vend_commission`;
DROP TABLE IF EXISTS `vend_commission_detail`;
DROP TABLE IF EXISTS `vend_commission_source`;
DROP TABLE IF EXISTS `vend_family`;
	
DROP TABLE IF EXISTS `vend_gl_accounts`;
DROP TABLE IF EXISTS `vend_gl_account_budgets`;
DROP TABLE IF EXISTS `vend_gl_account_details`;
DROP TABLE IF EXISTS `vend_gl_account_groups`;
DROP TABLE IF EXISTS `vend_gl_account_items`;
DROP TABLE IF EXISTS `vend_gl_account_types`;
DROP TABLE IF EXISTS `vend_gl_account_xhrefs`;
	
DROP TABLE IF EXISTS `vend_item`;
DROP TABLE IF EXISTS `vend_locations`;
DROP TABLE IF EXISTS `vend_machine`;
DROP TABLE IF EXISTS `vend_machine_complaint`;
DROP TABLE IF EXISTS `vend_machine_repair`;
DROP TABLE IF EXISTS `vend_machine_repairman`;
DROP TABLE IF EXISTS `vend_machine_sales`;
DROP TABLE IF EXISTS `vend_machine_schedule`;
DROP TABLE IF EXISTS `vend_machine_service`;
DROP TABLE IF EXISTS `vend_machine_vgroup`;
DROP TABLE IF EXISTS `vend_machine_vgroup_type`;
DROP TABLE IF EXISTS `vend_mei_sales`;
DROP TABLE IF EXISTS `vend_order`;
DROP TABLE IF EXISTS `vend_pckg`;
DROP TABLE IF EXISTS `vend_route_collection`;
DROP TABLE IF EXISTS `vend_salesman`;
DROP TABLE IF EXISTS `vend_settings`;
DROP TABLE IF EXISTS `vend_supervisor`;
DROP TABLE IF EXISTS `vend_waste`;
DROP TABLE IF EXISTS `vend_waste_detail`;

# these at the end due to foreign key constraints
DROP TABLE IF EXISTS `smartMenuMenuGroups`;
DROP TABLE IF EXISTS `menu_items_new`;
DROP TABLE IF EXISTS `vehicles`;

DROP VIEW IF EXISTS `vDbCustom`;
DROP VIEW IF EXISTS `vDbGroup`;
DROP VIEW IF EXISTS `vDbGroupList`;
DROP VIEW IF EXISTS `vDbGroupMembersOwners`;
DROP VIEW IF EXISTS `vMenuItems_District`;
DROP VIEW IF EXISTS `vPromoDays`;
DROP VIEW IF EXISTS `vPromoVivipos`;
DROP VIEW IF EXISTS `vCashierUsers`;

DROP PROCEDURE IF EXISTS `generateCalendarTable`;
DROP PROCEDURE IF EXISTS `trigUpdateDbMTime`;

DROP TABLE IF EXISTS `menu_groups_new`;


