

CREATE TABLE IF NOT EXISTS `customer_cards` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned NOT NULL,
  `kiosk_activated` tinyint(1) NOT NULL DEFAULT '0',
  `card_number` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `activated` datetime DEFAULT NULL,
  `last_used` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;