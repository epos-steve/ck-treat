
DROP TABLE IF EXISTS `checks_integrity`;
CREATE TABLE IF NOT EXISTS `checks_integrity` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `terminal_id` int(10) unsigned NOT NULL,
  `businessid` int(10) unsigned NOT NULL,
  `orders_created` smallint(5) unsigned NOT NULL,
  `orders_created_here` smallint(5) unsigned NOT NULL,
  `orders_modified` smallint(5) unsigned NOT NULL,
  `orders_modified_here` smallint(5) unsigned NOT NULL,
  `payments_created` smallint(5) unsigned NOT NULL,
  `payments_modified` smallint(5) unsigned NOT NULL,
  `payments_created_total` decimal(8,2) NOT NULL,
  `payments_modified_total` decimal(8,2) NOT NULL,
  `products_created` smallint(5) unsigned NOT NULL,
  `products_modified` smallint(5) unsigned NOT NULL,
  `products_created_total` decimal(8,2) NOT NULL,
  `products_modified_total` decimal(8,2) NOT NULL,
  `created_total_sales` decimal(8,2) NOT NULL,
  `modified_total_sales` decimal(8,2) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sync_date` (`date`,`terminal_id`)
) ENGINE=InnoDB;