DROP FUNCTION IF EXISTS get_client_pid_value;
DELIMITER //
CREATE FUNCTION get_client_pid_value(bid INT)
RETURNS INT
READS SQL DATA
BEGIN
	DECLARE bname_value VARCHAR (200);
    
	SELECT client_programid INTO client_pid FROM mburris_manage.client_programs WHERE businessid = bid LIMIT 1;	
	RETURN client_pid;

END; //
DELIMITER ;

SELECT get_client_pid_value(336);
SELECT get_client_pid_value(662);
SELECT get_client_pid_value(354);
