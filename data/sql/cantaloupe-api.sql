
DROP TABLE IF EXISTS fapi_cantaloupe_missing;
CREATE TABLE IF NOT EXISTS fapi_cantaloupe_missing (
  id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  cantaloupe_operator_id int(10) unsigned NOT NULL,
  bill_datetime datetime NOT NULL,
  checkdetail_id int(10) unsigned NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY cantaloupe_operator_id (cantaloupe_operator_id,checkdetail_id)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS fapi_cantaloupe_operators;
CREATE TABLE IF NOT EXISTS fapi_cantaloupe_operators (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  businessid int(10) unsigned NOT NULL,
  operator_id varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  micromarket_id varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  last_sync datetime NOT NULL,
  last_sent_email datetime NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY businessid (businessid)
) ENGINE=InnoDB;