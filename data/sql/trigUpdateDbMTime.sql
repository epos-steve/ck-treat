delimiter $$

drop procedure if exists trigUpdateDbMTime$$
create procedure trigUpdateDbMTime(
	var_viewid int
)
	modifies sql data
begin
	update db_view set db_mtime = NOW() where viewid=var_viewid;
end$$

delimiter ;

-- TRIGGERS ON db_custom
drop trigger if exists mtime_insert_custom;
create trigger mtime_insert_custom
	after insert on db_custom
	for each row
		call trigUpdateDbMTime( NEW.viewid );

drop trigger if exists mtime_update_custom;
create trigger mtime_update_custom
	after update on db_custom
	for each row
		call trigUpdateDbMTime( NEW.viewid );

-- TRIGGERS ON db_who
drop trigger if exists mtime_insert_who;
create trigger mtime_insert_who
	after insert on db_who
	for each row
		call trigUpdateDbMTime( NEW.viewid );

drop trigger if exists mtime_update_who;
create trigger mtime_update_who
	after update on db_who
	for each row
		call trigUpdateDbMTime( NEW.viewid );

-- TRIGGERS ON db_what
drop trigger if exists mtime_insert_what;
create trigger mtime_insert_what
	after insert on db_what
	for each row
		call trigUpdateDbMTime( NEW.viewid );

drop trigger if exists mtime_update_what;
create trigger mtime_update_what
	after update on db_what
	for each row
		call trigUpdateDbMTime( NEW.viewid );

-- TRIGGERS ON db_trendlines
drop trigger if exists mtime_insert_trend;
create trigger mtime_insert_trend
	after insert on db_trendlines
	for each row
		call trigUpdateDbMTime( NEW.viewid );

drop trigger if exists mtime_update_trend;
create trigger mtime_update_trend
	after update on db_trendlines
	for each row
		call trigUpdateDbMTime( NEW.viewid );

