create or replace view vDbCustom as
	select 
		cast("custom" as char) as vGroupId,
		db_view.*,
		UNIX_TIMESTAMP( db_view.db_mtime ) as mtime,
		UNIX_TIMESTAMP( pub_db_view.db_mtime ) as ptime,
		db_view_order.dbSide as dbSide,
		db_view_order.dbHeight as dbHeight,
		db_view_order.dbOrder as dbOrder,
		db_cache.cacheId as cacheId,
		db_cache.cacheDate as cacheDate
	from db_view
	left join pub_db_view using( viewid )
	left join db_view_order using( viewid )
	left join db_cache using( viewid );