create or replace view vDbGroupList as
	(
		select
			cast("custom" as char) as groupid,
			cast("My Charts" as char) as groupname,
			login.loginid as loginid,
			cast("custom" as char) as groupowner,
			false as dbDefault,
			1 as floater
		from login
		inner join security on login.security = security.securityid and security.dashboard = 4
	) union (
		select
			cast(db_group.groupid as char) as groupid,
			db_group.groupname as groupname,
			vDbGroupMembersOwners.loginid as loginid,
			cast(db_group.groupowner as char) as groupowner,
			COALESCE( db_public.dbDefault, false ) as dbDefault,
			0 as floater
		from db_group
		inner join vDbGroupMembersOwners using( groupid )
		left join db_public on db_group.groupid = db_public.id
	)
	order by floater desc, groupname;
