create or replace view vDbGroupMembersOwners as
	select 
		groupid as groupid,
		loginid as loginid
	from db_group_members
	union
	select
		groupid as groupid,
		groupowner as loginid
	from db_group