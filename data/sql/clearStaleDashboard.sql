delete from db_who where viewid not in ( select viewid from db_view );
delete from db_what where viewid not in ( select viewid from db_view );
delete from db_view_order where viewid not in ( select viewid from db_view );
delete from db_trendlines where viewid not in ( select viewid from db_view );
delete from db_group_graphs where viewid not in ( select viewid from db_view );
delete from db_custom where viewid not in ( select viewid from db_view );

delete from pub_db_view where viewid not in ( select viewid from db_view );
delete from pub_db_who where viewid not in ( select viewid from pub_db_view );
delete from pub_db_what where viewid not in ( select viewid from pub_db_view );
delete from pub_db_trendlines where viewid not in ( select viewid from pub_db_view );
delete from pub_db_custom where viewid not in ( select viewid from pub_db_view );
