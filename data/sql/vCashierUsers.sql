CREATE OR REPLACE VIEW vCashierUsers AS
	SELECT 
		LOWER( COALESCE( 
			NULLIF( l.username, '' ), 
			CONCAT( LEFT( TRIM( l.firstname ), 1 ), TRIM( l.lastname ) )
		) ) AS username,
		lc.passcode AS password,
		CONCAT( TRIM( l.lastname ), ', ', TRIM( l.firstname ) ) AS description,
		lcl.groupName AS groupId,
		lcl.businessid AS businessid,
		l.uuid_login AS uuid_login,
		l.loginid as loginid,
		group_concat(coalesce(jt.jobtypeid, ''), ':', coalesce(jt.name, '')) AS jobtype_list
	FROM login AS l
	JOIN login_cashier AS lc ON l.loginid = lc.loginid
	JOIN login_cashier_link AS lcl ON lc.loginid = lcl.loginid
	LEFT JOIN jobtypedetail as jd ON jd.is_deleted = 0 AND l.loginid = jd.loginid
	LEFT JOIN jobtype as jt ON jd.jobtype = jt.jobtypeid
	GROUP BY l.loginid
