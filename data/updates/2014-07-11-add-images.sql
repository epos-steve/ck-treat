
CREATE TABLE IF NOT EXISTS `image` (
  `id` int(11) NOT NULL,
  `category` int(3) NOT NULL,
  `number` int(3) NOT NULL,
  `file_extension` varchar(10) NOT NULL,
  `image_size` varchar(12) NOT NULL,
  `image` longblob NOT NULL,
  `thumbnail` blob,
  PRIMARY KEY (`id`,`category`,`number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE  `promo_groups_detail` 
    ADD  `use_product_image` TINYINT( 1 ) NULL ,
    ADD  `hide_text` TINYINT( 1 ) NULL ,
    ADD  `image_display_type` VARCHAR( 10 ) NULL,
    ADD  `icon_color` VARCHAR( 6 ) NOT NULL ;

ALTER TABLE  `promo_groups`
  ADD  `use_product_image` TINYINT( 1 ) NULL ,
  ADD  `hide_text` TINYINT( 1 ) NULL ,
  ADD  `image_display_type` VARCHAR( 10 ) NULL,
  ADD  `icon_color` VARCHAR( 6 ) NOT NULL ;

ALTER TABLE  `aeris_modifier`
  ADD  `use_product_image` TINYINT( 1 ) NULL ,
  ADD  `hide_text` TINYINT( 1 ) NULL ,
  ADD  `image_display_type` VARCHAR( 10 ) NULL,
  ADD  `icon_color` VARCHAR( 6 ) NOT NULL ;

ALTER TABLE  `promo_groups`
  CHANGE  `uuid_promo_groups`  `uuid_promo_groups` CHAR( 36 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ;
ALTER TABLE  `aeris_modifier`
  CHANGE  `uuid_aeris_modifier`  `uuid_aeris_modifier` CHAR( 36 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;

ALTER TABLE  `aeris_modifier` ADD  `sort_key` INT( 3 ) NOT NULL DEFAULT  '0';

ALTER TABLE `aeris_modifier_group`
  ADD `limit_min` INT( 3 ) NOT NULL DEFAULT '0',
  ADD `limit_max` INT( 3 ) NOT NULL DEFAULT '0',
  ADD `amount_free` INT( 3 ) NOT NULL DEFAULT '0';

UPDATE `aeris_modifier_group`
SET `limit_min` = 1, `limit_max` = 1
WHERE `type` = 'single';
