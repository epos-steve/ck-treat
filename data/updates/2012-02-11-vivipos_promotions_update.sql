DROP PROCEDURE IF EXISTS populate_vivipos_promotion_business;
CREATE DEFINER=`mburris_user`@`192.168.2.%` PROCEDURE `populate_vivipos_promotion_business`(IN `bid` INT UNSIGNED)
BEGIN

    TRUNCATE vivipos_promo_days;
    INSERT INTO vivipos_promo_days select 0 AS day, psu.id AS id from promotions psu where (psu.sunday = 1) union select 1 AS day, pm.id AS id from promotions pm where (pm.monday = 1) union select 2 AS day, pt.id AS id from promotions pt where (pt.tuesday = 1) union select 3 AS day, pw.id AS id from promotions pw where (pw.wednesday = 1) union select 4 AS day, pth.id AS id from promotions pth where (pth.thursday = 1) union select 5 AS day, pf.id AS id from promotions pf where (pf.friday = 1) union select 6 AS day, ps.id AS id from promotions ps where (ps.saturday = 1);

    DELETE FROM `vivipos_promotions` WHERE `businessid` = bid;
    INSERT INTO `vivipos_promotions`
    (
        `businessid`,
        `promo_id`,
        `promo_target`,
        `vivipos_name`,
        `pos_id`,
        `value`,
        `rule_order`,
        `pre_tax`,
        `promo_reserve`,
        `promo_discount`,
        `promo_discount_type`,
        `promo_discount_n`,
        `promo_discount_limit`,
        `promo_trigger_amount_limit`,
        `promo_trigger_amount_type`,
        `promo_trigger_amount`,
        `promo_trigger_amount_2`,
        `promo_trigger_amount_3`,
        `vivipos_taxno`,
        `vivipos_taxname`,
        `vivipos_startdate`,
        `vivipos_starttime`,
        `vivipos_enddate`,
        `vivipos_endtime`,
        `vivipos_days`,
        `vivipos_type`,
        `vivipos_trigger`,
        `promotarget_table`,
        `promotarget_order`,
        `vivipos_modified`,
        `vivipos_active`,
        `is_global`,
        `alt_name1`,
        `uuid_vivipos_promotions`
    )
    SELECT
            promotions.businessid,
            promotions.id as promo_id,
            promo_target.id as promo_target,
            promotions.name as vivipos_name,
            promotions.pos_id,
            promotions.value,
            promotions.rule_order,
            promotions.pre_tax,
            promotions.promo_reserve,
            promotions.promo_discount,
            promotions.promo_discount_type,
            promotions.promo_discount_n,
            promotions.promo_discount_limit,
            promotions.promo_trigger_amount_limit,
            promotions.promo_trigger_amount_type,
            promotions.promo_trigger_amount,
            promotions.promo_trigger_amount_2,
            promotions.promo_trigger_amount_3,
            COALESCE( CAST( menu_tax.pos_id AS char ), '' ) as vivipos_taxno,
            COALESCE( menu_tax.name, '' ) as vivipos_taxname,
            UNIX_TIMESTAMP( promotions.start_date ) - (3600 * business.timezone) as vivipos_startdate,
            UNIX_TIMESTAMP( CONCAT( promotions.start_date, ' ', promotions.start_time ) ) - (3600 * business.timezone) as vivipos_starttime,
            UNIX_TIMESTAMP( promotions.end_date ) - (3600 * business.timezone) + 59 as vivipos_enddate,
            UNIX_TIMESTAMP( CONCAT( promotions.start_date, ' ', promotions.end_time ) ) - (3600 * business.timezone) + IF(RIGHT(promotions.end_time,2)='59',59,00) as vivipos_endtime,
            GROUP_CONCAT( vivipos_promo_days.day ORDER BY vivipos_promo_days.day ) as vivipos_days,
            promo_type.vivipos_name as vivipos_type,
            promo_trigger.vivipos_name as vivipos_trigger,
            promo_target.db_table as promotarget_table,
            promo_target.order_by as promotarget_order,
            TIMESTAMP( NOW() ) as vivipos_modified,
            promotions.activeFlag as vivipos_active,
            0 AS is_global,
            promotions.subsidy as alt_name1,
            promotions.uuid_promotions as uuid_vivipos_promotions
        FROM promotions
        JOIN promo_type ON
            promotions.type = promo_type.id
        JOIN promo_trigger ON
            promotions.promo_trigger = promo_trigger.id
        JOIN promo_target ON
            promo_trigger.target_id = promo_target.id
        LEFT JOIN menu_tax ON
            promotions.tax_id = menu_tax.id
        LEFT JOIN vivipos_promo_days ON
            promotions.id = vivipos_promo_days.id
        JOIN business ON
            business.businessid = promotions.businessid
        WHERE promotions.businessid = bid
          AND promotions.category = 1
        GROUP BY promotions.id
        UNION
        SELECT
            promo_mapping.businessid,
            promotions.id as promo_id,
            promo_target.id as promo_target,
            promotions.name as vivipos_name,
            promotions.pos_id,
            promotions.value,
            promotions.rule_order,
            promotions.pre_tax,
            promotions.promo_reserve,
            promotions.promo_discount,
            promotions.promo_discount_type,
            promotions.promo_discount_n,
            promotions.promo_discount_limit,
            promotions.promo_trigger_amount_limit,
            promotions.promo_trigger_amount_type,
            promotions.promo_trigger_amount,
            promotions.promo_trigger_amount_2,
            promotions.promo_trigger_amount_3,
            COALESCE( CAST( menu_tax.pos_id AS char ), '' ) as vivipos_taxno,
            COALESCE( menu_tax.name, '' ) as vivipos_taxname,
            UNIX_TIMESTAMP( promotions.start_date ) - (3600 * business.timezone) as vivipos_startdate,
            UNIX_TIMESTAMP( CONCAT( promotions.start_date, ' ', promotions.start_time ) ) - (3600 * business.timezone) as vivipos_starttime,
            UNIX_TIMESTAMP( promotions.end_date ) - (3600 * business.timezone) + 59 as vivipos_enddate,
            UNIX_TIMESTAMP( CONCAT( promotions.start_date, ' ', promotions.end_time ) ) - (3600 * business.timezone) + IF(RIGHT(promotions.end_time,2)='59',59,00) as vivipos_endtime,
            GROUP_CONCAT( vivipos_promo_days.day ORDER BY vivipos_promo_days.day ) as vivipos_days,
            promo_type.vivipos_name as vivipos_type,
            promo_trigger.vivipos_name as vivipos_trigger,
            promo_target.db_table as promotarget_table,
            promo_target.order_by as promotarget_order,
            TIMESTAMP( NOW() ) as vivipos_modified,
            promotions.activeFlag as vivipos_active,
            1 AS is_global,
            promotions.subsidy as alt_name1,
            promotions.uuid_promotions as uuid_vivipos_promotions
        FROM promotions
        JOIN promo_type ON
            promotions.type = promo_type.id
        JOIN promo_trigger ON
            promotions.promo_trigger = promo_trigger.id
        JOIN promo_target ON
            promo_trigger.target_id = promo_target.id
        LEFT JOIN menu_tax ON
            promotions.tax_id = menu_tax.id
        LEFT JOIN vivipos_promo_days ON
            promotions.id = vivipos_promo_days.id
        JOIN promo_mapping ON
            promo_mapping.promotionid = promotions.id
        JOIN business ON
            business.businessid = promo_mapping.businessid
        WHERE
            promo_mapping.businessid = bid
          AND promotions.category = 1
        GROUP BY
            business.businessid,
            promo_id;
END;

DROP PROCEDURE IF EXISTS populate_vivipos_promotions;
CREATE DEFINER=`mburris_user`@`192.168.2.%` PROCEDURE `populate_vivipos_promotions`()
BEGIN

    TRUNCATE vivipos_promo_days;
    INSERT INTO vivipos_promo_days select 0 AS day, psu.id AS id from promotions psu where (psu.sunday = 1) union select 1 AS day, pm.id AS id from promotions pm where (pm.monday = 1) union select 2 AS day, pt.id AS id from promotions pt where (pt.tuesday = 1) union select 3 AS day, pw.id AS id from promotions pw where (pw.wednesday = 1) union select 4 AS day, pth.id AS id from promotions pth where (pth.thursday = 1) union select 5 AS day, pf.id AS id from promotions pf where (pf.friday = 1) union select 6 AS day, ps.id AS id from promotions ps where (ps.saturday = 1);
        
    DELETE FROM `vivipos_promotions`;
    INSERT INTO `vivipos_promotions`
    (
        `businessid`,
        `promo_id`,
        `promo_target`,
        `vivipos_name`,
        `pos_id`,
        `value`,
        `rule_order`,
        `pre_tax`,
        `promo_reserve`,
        `promo_discount`,
        `promo_discount_type`,
        `promo_discount_n`,
        `promo_discount_limit`,
        `promo_trigger_amount_limit`,
        `promo_trigger_amount_type`,
        `promo_trigger_amount`,
        `promo_trigger_amount_2`,
        `promo_trigger_amount_3`,
        `vivipos_taxno`,
        `vivipos_taxname`,
        `vivipos_startdate`,
        `vivipos_starttime`,
        `vivipos_enddate`,
        `vivipos_endtime`,
        `vivipos_days`,
        `vivipos_type`,
        `vivipos_trigger`,
        `promotarget_table`,
        `promotarget_order`,
        `vivipos_modified`,
        `vivipos_active`,
        `is_global`,
        `alt_name1`,
        `uuid_vivipos_promotions`
    )
    SELECT
            promotions.businessid,
            promotions.id as promo_id,
            promo_target.id as promo_target,
            promotions.name as vivipos_name,
            promotions.pos_id,
            promotions.value,
            promotions.rule_order,
            promotions.pre_tax,
            promotions.promo_reserve,
            promotions.promo_discount,
            promotions.promo_discount_type,
            promotions.promo_discount_n,
            promotions.promo_discount_limit,
            promotions.promo_trigger_amount_limit,
            promotions.promo_trigger_amount_type,
            promotions.promo_trigger_amount,
            promotions.promo_trigger_amount_2,
            promotions.promo_trigger_amount_3,
            COALESCE( CAST( menu_tax.pos_id AS char ), '' ) as vivipos_taxno,
            COALESCE( menu_tax.name, '' ) as vivipos_taxname,
            UNIX_TIMESTAMP( promotions.start_date ) - (3600 * business.timezone) as vivipos_startdate,
            UNIX_TIMESTAMP( CONCAT( promotions.start_date, ' ', promotions.start_time ) ) - (3600 * business.timezone) as vivipos_starttime,
            UNIX_TIMESTAMP( promotions.end_date ) - (3600 * business.timezone) + 59 as vivipos_enddate,
            UNIX_TIMESTAMP( CONCAT( promotions.start_date, ' ', promotions.end_time ) ) - (3600 * business.timezone) + IF(RIGHT(promotions.end_time,2)='59',59,00) as vivipos_endtime,
            GROUP_CONCAT( vivipos_promo_days.day ORDER BY vivipos_promo_days.day ) as vivipos_days,
            promo_type.vivipos_name as vivipos_type,
            promo_trigger.vivipos_name as vivipos_trigger,
            promo_target.db_table as promotarget_table,
            promo_target.order_by as promotarget_order,
            TIMESTAMP( NOW() ) as vivipos_modified,
            promotions.activeFlag as vivipos_active,
            0 AS is_global,
            promotions.subsidy as alt_name1,
            promotions.uuid_promotions as uuid_vivipos_promotions
        FROM promotions
        JOIN promo_type ON
            promotions.type = promo_type.id
        JOIN promo_trigger ON
            promotions.promo_trigger = promo_trigger.id
        JOIN promo_target ON
            promo_trigger.target_id = promo_target.id
        LEFT JOIN menu_tax ON
            promotions.tax_id = menu_tax.id
        LEFT JOIN vivipos_promo_days ON
            promotions.id = vivipos_promo_days.id
        JOIN business ON
            business.businessid = promotions.businessid
        WHERE promotions.category = 1
        GROUP BY promotions.id
        UNION
        SELECT
            promo_mapping.businessid,
            promotions.id as promo_id,
            promo_target.id as promo_target,
            promotions.name as vivipos_name,
            promotions.pos_id,
            promotions.value,
            promotions.rule_order,
            promotions.pre_tax,
            promotions.promo_reserve,
            promotions.promo_discount,
            promotions.promo_discount_type,
            promotions.promo_discount_n,
            promotions.promo_discount_limit,
            promotions.promo_trigger_amount_limit,
            promotions.promo_trigger_amount_type,
            promotions.promo_trigger_amount,
            promotions.promo_trigger_amount_2,
            promotions.promo_trigger_amount_3,
            COALESCE( CAST( menu_tax.pos_id AS char ), '' ) as vivipos_taxno,
            COALESCE( menu_tax.name, '' ) as vivipos_taxname,
            UNIX_TIMESTAMP( promotions.start_date ) - (3600 * business.timezone) as vivipos_startdate,
            UNIX_TIMESTAMP( CONCAT( promotions.start_date, ' ', promotions.start_time ) ) - (3600 * business.timezone) as vivipos_starttime,
            UNIX_TIMESTAMP( promotions.end_date ) - (3600 * business.timezone) + 59 as vivipos_enddate,
            UNIX_TIMESTAMP( CONCAT( promotions.start_date, ' ', promotions.end_time ) ) - (3600 * business.timezone) + IF(RIGHT(promotions.end_time,2)='59',59,00) as vivipos_endtime,
            GROUP_CONCAT( vivipos_promo_days.day ORDER BY vivipos_promo_days.day ) as vivipos_days,
            promo_type.vivipos_name as vivipos_type,
            promo_trigger.vivipos_name as vivipos_trigger,
            promo_target.db_table as promotarget_table,
            promo_target.order_by as promotarget_order,
            TIMESTAMP( NOW() ) as vivipos_modified,
            promotions.activeFlag as vivipos_active,
            1 AS is_global,
            promotions.subsidy as alt_name1,
            promotions.uuid_promotions as uuid_vivipos_promotions
        FROM promotions
        JOIN promo_type ON
            promotions.type = promo_type.id
        JOIN promo_trigger ON
            promotions.promo_trigger = promo_trigger.id
        JOIN promo_target ON
            promo_trigger.target_id = promo_target.id
        LEFT JOIN menu_tax ON
            promotions.tax_id = menu_tax.id
        LEFT JOIN vivipos_promo_days ON
            promotions.id = vivipos_promo_days.id
        JOIN promo_mapping ON
            promo_mapping.promotionid = promotions.id
        JOIN business ON
            business.businessid = promo_mapping.businessid
        WHERE promotions.category = 1
        GROUP BY
            business.businessid,
            promo_id;
END;
