DROP TABLE IF EXISTS `kiosk_order_trigger`;
CREATE TABLE `kiosk_order_trigger` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `trigger` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `kiosk_order_trigger`
--

INSERT INTO `kiosk_order_trigger` (`id`, `trigger`) VALUES
(1, 'Count Diff'),
(2, 'Cost Diff'),
(3, 'Price Diff'),
(4, 'Out of Stock');

-- --------------------------------------------------------

--
-- Table structure for table `kiosk_order_trigger_detail`
--
-- Creation: Aug 19, 2014 at 07:42 PM
-- Last update: Aug 19, 2014 at 08:19 PM
--

DROP TABLE IF EXISTS `kiosk_order_trigger_detail`;
CREATE TABLE `kiosk_order_trigger_detail` (
  `triggerid` int(5) NOT NULL,
  `districtid` int(10) NOT NULL,
  `value` double NOT NULL,
  UNIQUE KEY `triggerid` (`triggerid`,`districtid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
