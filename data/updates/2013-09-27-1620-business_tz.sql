alter table `business` add column `tz` int(11) default null after `timezone`;
alter table `business` add constraint `fk_tz_lookup` foreign key (`tz`) references `tz_lookup` (`id`);

update `business` set `tz` = NULL;
update `business` 
	set `tz` = (select id from tz_lookup where offset = timezone - 6.0 order by standard desc, use_dst desc limit 1) 
	where `timezone` between -12 and 12;
update `business`
	set `tz` = (select id from tz_lookup where name = 'America/Chicago' limit 1)
	where `timezone` is NULL;
