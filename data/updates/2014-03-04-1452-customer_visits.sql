DROP TABLE IF EXISTS `customer_visits`;
CREATE TABLE `customer_visits` (
  customerid INT NOT NULL,
  businessid INT NOT NULL,
  visit_date DATETIME NOT NULL,
  PRIMARY KEY (customerid, businessid),
  INDEX businessid_idx(businessid),
  INDEX customerid_idx(customerid),
  FOREIGN KEY (customerid) REFERENCES customer(customerid) ON DELETE CASCADE,
  FOREIGN KEY (businessid) REFERENCES business(businessid) ON DELETE CASCADE
) ENGINE=INNODB CHARSET=utf8 COLLATE utf8_unicode_ci;
