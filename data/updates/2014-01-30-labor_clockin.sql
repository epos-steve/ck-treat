
ALTER TABLE labor_clockin ADD uuid_clockin_id BINARY(16) NULL;
CREATE UNIQUE INDEX uuid_clockin_id_index ON labor_clockin ( uuid_clockin_id );