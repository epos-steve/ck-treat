-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: 192.168.2.3
-- Generation Time: Dec 20, 2013 at 04:29 PM
-- Server version: 5.5.32
-- PHP Version: 5.3.10-1ubuntu3.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Table structure for table `aeris2_fpanel_sync`
--
-- Creation: Dec 20, 2013 at 08:29 PM
--

DROP TABLE IF EXISTS `aeris2_fpanel_sync`;
CREATE TABLE `aeris2_fpanel_sync` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `fpanel_id` int(10) unsigned DEFAULT NULL,
  `businessid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fpanel_id_idx` (`fpanel_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `aeris2_fpanel_sync`:
--   `fpanel_id`
--       `aeris_fpanel` -> `id`
--

--
-- Table structure for table `aeris_fpanel`
--
-- Creation: Dec 20, 2013 at 04:25 PM
--

DROP TABLE IF EXISTS `aeris_fpanel`;
CREATE TABLE `aeris_fpanel` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `pages` tinyint(4) NOT NULL,
  `rows` tinyint(4) NOT NULL,
  `cols` tinyint(4) NOT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

--
-- Table structure for table `aeris_fpanel_button`
--
-- Creation: Dec 19, 2013 at 07:33 PM
--

DROP TABLE IF EXISTS `aeris_fpanel_button`;
CREATE TABLE `aeris_fpanel_button` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fpanel_id` int(10) unsigned NOT NULL,
  `page` int(11) NOT NULL,
  `display_x` int(11) NOT NULL,
  `display_y` int(11) NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `pos_color` varchar(64) NOT NULL,
  `function_id` int(10) unsigned DEFAULT NULL,
  `args` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fpanel_id` (`fpanel_id`),
  KEY `function_id` (`function_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `aeris_fpanel_button`:
--   `fpanel_id`
--       `aeris_fpanel` -> `id`
--   `function_id`
--       `aeris_fpanel_function` -> `id`
--

--
-- Table structure for table `aeris_fpanel_function`
--
-- Creation: Dec 19, 2013 at 07:33 PM
--

DROP TABLE IF EXISTS `aeris_fpanel_function`;
CREATE TABLE `aeris_fpanel_function` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `access` varchar(96) NOT NULL,
  `command` varchar(64) NOT NULL,
  `controller` varchar(32) DEFAULT NULL,
  `data` varchar(45) DEFAULT NULL,
  `version` varchar(45) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aeris_fpanel_function`
--

INSERT INTO `aeris_fpanel_function` (`id`, `name`, `access`, `command`, `controller`, `data`, `version`, `description`) VALUES
(95, 'print_last_receipt', '', 'print_last_receipt', 'Order', '', '', 'Print the Last Receipt'),
(96, 'void_selection', '', 'void_selection', 'Cart', '', '', 'Void Selection'),
(97, 'store_order', '', 'store_order', 'Order', '', '', 'Store Current Order'),
(98, 'view_orders', '', 'view_orders', 'Order', '', '', 'View a List of Orders'),
(99, 'no_sale', '', 'no_sale', 'Cashdrawer', '', '', ''),
(100, 'set_quantity', '', 'set_quantity', 'Cart', '', '', ''),
(101, 'void', '', 'do_void', 'Void', '', '', ''),
(102, 'add_payment', '', 'add_payment_button', 'Cart', '', '', ''),
(103, 'search_customers', '', 'search_customers', 'Customer', '', '', ''),
(104, 'apply_discount', '', 'apply_discount', 'Discount', '', '', ''),
(105, 'update_modifiers', '', 'update_modifiers', 'Cart', '', '', ''),
(106, 'logout', '', 'logout', 'Login', '', '', ''),
(107, 'refund_selection', '', 'refund_selection', 'Cart', '', '', ''),
(108, 'open_clockin_from_checkout', '', 'open_clockin_from_checkout', 'UserTimeClock', '', '', ''),
(109, 'apply_product_discount', '', 'apply_product_discount', 'Discount', '', '', ''),
(110, 'reboot_system', '', 'reboot_system', 'Aeris', '', '', ''),
(111, 'run_report', '', 'run_report', 'Report', '', '', ''),
(112, 'recall_last_order', '', 'recall_last_order', 'Order', '', '', ''),
(113, 'shutdown_system', '', 'shutdown_system', 'Aeris', '', '', ''),
(114, 'reopen', '', 'reopen', 'Order', '', '', ''),
(115, 'elevate_popup', '', 'elevate_popup', 'Security', '', '', ''),
(116, 'drop_privilege', '', 'drop_privilege', 'Security', '', '', ''),
(117, 'settings_password', '', 'settings_password', 'Settings', '', '', '');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `aeris2_fpanel_sync`
--
ALTER TABLE `aeris2_fpanel_sync`
  ADD CONSTRAINT `aeris2_fpanel_sync_ibfk_1` FOREIGN KEY (`fpanel_id`) REFERENCES `aeris_fpanel` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `aeris_fpanel_button`
--
ALTER TABLE `aeris_fpanel_button`
  ADD CONSTRAINT `aeris_fpanel_button_ibfk_1` FOREIGN KEY (`fpanel_id`) REFERENCES `aeris_fpanel` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `aeris_fpanel_button_ibfk_3` FOREIGN KEY (`function_id`) REFERENCES `aeris_fpanel_function` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

