# changes for Treat America
START TRANSACTION;

ALTER TABLE `vend_gl_account_groups` CHANGE `account_group_class` `account_group_class` VARCHAR( 80 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' AFTER `sort_order`;


# change SALES to Net Sales as per Ed's email
UPDATE `vend_gl_account_groups`
SET `name` = 'Net Sales'
	,`account_group_class` = 'Treat_Model_GeneralLedger_AccountGroup_NetSalesObject'
WHERE `name` = 'SALES'
;

# change COST OF GOODS to Net Sales as per Ed's email
UPDATE `vend_gl_account_groups`
SET `name` = 'NET GROSS PROFIT'
	,`account_group_class` = 'Treat_Model_GeneralLedger_AccountGroup_NetGrossProfitObject'
WHERE `name` = 'COST OF GOODS'
;

# update OPERATING EXPENSE to use the correct class
UPDATE `vend_gl_account_groups`
SET `account_group_class` = 'Treat_Model_GeneralLedger_AccountGroup_OperatingExpenseObject'
WHERE `name` = 'OPERATING EXPENSE'
;

# update EMPLOYEE EXPENSE to use the correct class
UPDATE `vend_gl_account_groups`
SET `account_group_class` = 'Treat_Model_GeneralLedger_AccountGroup_EmployeeExpenseObject'
WHERE `name` = 'EMPLOYEE EXPENSE'
;

# add EBITDA to account groups as per Ed's email
INSERT INTO `vend_gl_account_groups` (
	`sort_order`
	,`name`
	,`account_group_class`
)
VALUES
	(  7, 'EBITDA', 'Treat_Model_GeneralLedger_AccountGroup_EbitdaObject' )
;

UPDATE `vend_gl_accounts`
SET `name` = 'Changed Sequence'
WHERE `name` = 'Cost of Goods'
;

UPDATE `vend_gl_accounts`
SET `name` = 'Customer Commission Exp'
WHERE `name` = 'Customer Commissin Exp'
;

UPDATE `vend_gl_accounts`
SET `name` = 'Cost of Goods'
	,`sort_order` = -5
WHERE `name` = 'Changed Sequence'
;

UPDATE `vend_gl_accounts`
SET `sort_order` = 5
WHERE `name` = 'Customer Commission Exp'
;

# changes for payment.companykitchen.companykitchen

CREATE TABLE `customer_transaction_types` (
	`id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
	`name` VARCHAR( 50 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci
;

INSERT INTO `customer_transaction_types` (
	`name`
)
VALUES
	(  'normal online credit card transaction' )
	,( 'normal online credit card refund' )
	,( 'refund to kiosk card' )
	,( 'promotional credit' )
;

ALTER TABLE `customer_transactions` ADD `trans_type_id` SMALLINT UNSIGNED NOT NULL DEFAULT '1' COMMENT 'matches id field in customer _transaction _types table' AFTER `oid`;

COMMIT;