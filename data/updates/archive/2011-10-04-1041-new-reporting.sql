
DROP TABLE IF EXISTS `pos_report`;
CREATE TABLE `pos_report` (
	`report_id` TINYINT UNSIGNED NOT NULL AUTO_INCREMENT
	,`label` VARCHAR( 100 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
	,`slug` VARCHAR( 20 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'use this for programming off of'
	,`class` VARCHAR( 50 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
	,PRIMARY KEY ( `report_id` )
) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci
;

DROP TABLE IF EXISTS `pos_report_runlist`;
CREATE TABLE `pos_report_runlist` (
	`report_runlist_id` INT UNSIGNED NOT NULL AUTO_INCREMENT
	,`company_id` INT UNSIGNED NOT NULL
	,`report_id` TINYINT UNSIGNED NOT NULL
	,`login_id` INT UNSIGNED NOT NULL
	,`how_often` TINYINT UNSIGNED NOT NULL
	,`day_of_week` ENUM( "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" )
	,`day_of_month` INT
	,PRIMARY KEY ( `report_runlist_id` )
	,INDEX ( `report_id` , `how_often` , `day_of_week` , `day_of_month` )
) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci
;

DROP TABLE IF EXISTS `pos_report_cache`;
CREATE TABLE `pos_report_cache` (
	`company_id` INT UNSIGNED NOT NULL
	,`report_id` TINYINT UNSIGNED NOT NULL
	,`how_often` TINYINT UNSIGNED NOT NULL
	,`date_run` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
	,`filename` VARCHAR( 100 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
	,PRIMARY KEY ( `company_id` , `report_id` , `how_often` , `date_run` )
) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci
;


INSERT INTO `pos_report` (
	`report_id`
	,`class`
	,`slug`
	,`label`
)
VALUES
	(  1, 'Treat_Model_Report_Object_PosInventory', 'inventory-detail', 'Inventory Detail' )
	,( 2, 'Treat_Model_Report_Object_PosInventory', 'inventory-summary', 'Inventory Summary' )
	,( 3, 'Treat_Model_Report_Object_PosInventory', 'inventory-sales', 'Inventory Sales' )
	,( 4, 'Treat_Model_Report_Object_PosInventory', 'inventory-waste', 'Inventory Waste' )
	,( 5, 'Treat_Model_Report_Object_PosFinancial', 'financial', 'Financial Report' )
	,( 6, 'Treat_Model_Report_Object_PosCashIn',  'cash_in',  'Cash In'  )
	,( 7, 'Treat_Model_Report_Object_PosCashOut', 'cash_out', 'Cash Out' )
;

INSERT INTO `pos_report_runlist` (
	`report_runlist_id`
	,`company_id`
	,`report_id`
	,`login_id`
	,`how_often`
	,`day_of_week`
	,`day_of_month`
)
VALUES
	#  id,cid,rid,lid,how,week,month
	(   1, 12,  1, 2, 2, NULL, 31 )
	,(  2, 12,  1, 2, 1, 'Monday', NULL ) # testing - 2011-09-30 is not a Monday
	,(  3, 12,  1, 1, 2, NULL, 31 )
	,(  4, 12,  2, 2, 2, NULL, 31 )
	,(  5, 12,  3, 2, 2, NULL, 31 )
	,(  6, 12,  4, 2, 2, NULL, 31 )
	,(  7, 12,  5, 2, 1, 'Friday', NULL ) # testing - 2011-09-30 is a Friday, should run weekly report
	,(  8, 12,  5, 2, 2, NULL, 31 ) # testing - should run monthly report
	,(  9, 12,  6, 2, 1, 'Friday', NULL ) # testing - 2011-09-30 is a Friday, should run weekly report
	,( 10, 12,  6, 2, 2, NULL, 31 ) # testing - should run monthly report
	,( 11, 12,  7, 2, 1, 'Friday', NULL ) # testing - 2011-09-30 is a Friday, should run weekly report
	,( 12, 12,  7, 2, 2, NULL, 31 ) # testing - should run monthly report
;



INSERT INTO `pos_report` (
	`class`
	,`slug`
	,`label`
)
VALUES
	 ( 'Treat_Model_Report_Object_PosCustomerPromo', 'customer-promo', 'Customer Promo' )
	,( 'Treat_Model_Report_Object_PosCustomerRefund', 'customer-refund', 'Customer Refund' )
;


