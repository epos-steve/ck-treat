# $Id: 2010-11-17-1104.sql 4444 2010-11-18 16:58:12Z patrick $

# change num_imports to import_num in order to keep track of which import file this is
ALTER TABLE `vend_gl_account_details` CHANGE `num_imports` `import_number` TINYINT UNSIGNED NOT NULL DEFAULT '0' COMMENT 'is this the primary or secondary file import';

UPDATE `vend_gl_account_details`
SET `import_number` = 1
WHERE `import_number` > 0
;

ALTER TABLE `vend_gl_account_details` DROP INDEX `accountid`;

ALTER TABLE `vend_gl_account_details` ADD UNIQUE `account_id` (
`account_id` ,
`friday_date` ,
`import_number`
);



