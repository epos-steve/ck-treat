DROP TABLE IF EXISTS login_cashier;
CREATE TABLE login_cashier(
	loginid INT UNSIGNED NOT NULL KEY,
	passcode VARCHAR(20) NOT NULL DEFAULT 12345
) ENGINE=INNODB;

DROP TABLE IF EXISTS login_cashier_link;
CREATE TABLE login_cashier_link(
	loginid INT UNSIGNED NOT NULL,
	businessid INT UNSIGNED NOT NULL,
	groupName VARCHAR(20) NOT NULL DEFAULT 'Cashier',
	PRIMARY KEY( loginid, businessid ),
	INDEX loginid_index( loginid ),
	INDEX businessid_index( businessid )
) ENGINE=INNODB;

DROP TABLE IF EXISTS mburris_manage.client_modes;
CREATE TABLE mburris_manage.client_modes(
	id INT UNSIGNED NOT NULL KEY AUTO_INCREMENT,
	name VARCHAR(20) NOT NULL DEFAULT ''
) ENGINE=INNODB;

INSERT INTO mburris_manage.client_modes( id, name ) VALUES
	( 1, 'Vending' ),
	( 2, 'Food Service' ),
	( 3, 'Cashier' );

ALTER TABLE mburris_manage.client_programs ADD COLUMN pos_mode SMALLINT DEFAULT NULL;

CREATE OR REPLACE VIEW vCashierUsers AS
	SELECT 
		LOWER( COALESCE( 
			NULLIF( login.username, '' ), 
			CONCAT( LEFT( login.firstname, 1 ), login.lastname )
		) ) AS username,
		login_cashier.passcode AS password,
		CONCAT( lastname, ', ', firstname ) AS description,
		login_cashier_link.groupName AS groupId,
		login_cashier_link.businessid AS businessid
	FROM login
	JOIN login_cashier USING( loginid )
	JOIN login_cashier_link USING( loginid );

DROP TABLE IF EXISTS aeris_settings;
CREATE TABLE aeris_settings(
	settingId INT UNSIGNED NOT NULL KEY AUTO_INCREMENT,
	businessid INT UNSIGNED NOT NULL,
	registryKey VARCHAR(128) NOT NULL,
	registryValue VARCHAR(128),
	UNIQUE KEY( businessid, registryKey )
) ENGINE=INNODB;

INSERT INTO aeris_settings( businessid, registryKey, registryValue ) VALUES
	( 0, 'vivipos.fec.settings.DepartmentRows', '1' ),
	( 0, 'vivipos.fec.settings.DepartmentCols', '5' ),
	( 0, 'vivipos.fec.settings.layout.HideDeptScrollbar', '1' ),
	( 0, 'vivipos.fec.settings.PluRows', '9' ),
	( 0, 'vivipos.fec.settings.PluCols', '4' ),
	( 0, 'vivipos.fec.settings.layout.traditional.HidePLUScrollbar', '1' ),
	( 0, 'vivipos.fec.settings.layout.RegisterAtLeft', '1' );

ALTER TABLE menu_groups_new ADD COLUMN pos_color SMALLINT NOT NULL DEFAULT 0;
ALTER TABLE promo_groups ADD COLUMN pos_color SMALLINT NOT NULL DEFAULT 0;
ALTER TABLE menu_groups_new ADD COLUMN display_order INT NOT NULL DEFAULT 99999;

DROP TABLE IF EXISTS menu_screen_order;
CREATE TABLE menu_screen_order(
	businessid INT UNSIGNED NOT NULL,
	itemId INT UNSIGNED NOT NULL,
	display_order INT NOT NULL DEFAULT 99999,
	pos_color SMALLINT NOT NULL DEFAULT 0,
	PRIMARY KEY( businessid, itemId )
) ENGINE=INNODB;

