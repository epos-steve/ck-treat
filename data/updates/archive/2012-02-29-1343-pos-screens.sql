DELIMITER //

DROP PROCEDURE IF EXISTS trigMINClearGroupPosition//
CREATE PROCEDURE trigMINClearGroupPosition(
	var_product_id INT,
	var_is_button BOOL
)
	DETERMINISTIC
	MODIFIES SQL DATA
BEGIN
	IF NOT var_is_button THEN
		UPDATE promo_groups_detail SET display_x = NULL, display_y = NULL, pos_color = 0 WHERE product_id = var_product_id;
	END IF;
END //

DROP TRIGGER IF EXISTS update_MINClearGroupPosition//
CREATE TRIGGER update_MINClearGroupPosition
	AFTER UPDATE ON menu_items_new
	FOR EACH ROW
		CALL trigMINClearGroupPosition( NEW.id, NEW.is_button )//

ALTER TABLE promo_groups_detail ADD COLUMN processed BOOL NOT NULL DEFAULT true;