
# 97	KC
# 106	Omaha
# 151	Indiana

INSERT INTO `vend_gl_accounts` (
	`business_id`
	,`account_group_id`
	,`name`
	,`account_type_id`
	,`sort_order`
	,`multiplier`
)
(
	SELECT
		106 AS `business_id`
		,`account_group_id`
		,`name`
		,`account_type_id`
		,`sort_order`
		,`multiplier`
	FROM `vend_gl_accounts`
	WHERE `business_id` = 97
		AND `name` = 'Customer Commission Exp'
) UNION (
	SELECT
		151 AS `business_id`
		,`account_group_id`
		,`name`
		,`account_type_id`
		,`sort_order`
		,`multiplier`
	FROM `vend_gl_accounts`
	WHERE `business_id` = 97
		AND `name` = 'Customer Commission Exp'
)
;

# update KC multiplier to be positive
UPDATE `vend_gl_accounts`
SET `multiplier` = 0.062
WHERE `business_id` = 97
	AND `name` = 'Customer Commission Exp'
	AND `number` IS NULL
;


# update Omaha accounts
UPDATE `vend_gl_accounts`
SET `multiplier` = 0.045
WHERE `business_id` = 106
	AND `name` = 'Customer Commission Exp'
	AND `number` IS NULL
;

UPDATE `vend_gl_accounts`
SET `account_group_id` = 4
	,`account_type_id` = 1
	,`sort_order`      = -5
WHERE `business_id` = 106
	AND `name` = 'Cost of Goods'
	AND `number` IS NULL
;


# update Indiana accounts
UPDATE `vend_gl_accounts`
SET `multiplier` = 0.067
WHERE `business_id` = 151
	AND `name` = 'Customer Commission Exp'
	AND `number` IS NULL
;

UPDATE `vend_gl_accounts`
SET `account_group_id` = 4
	,`account_type_id` = 1
	,`sort_order`      = -5
WHERE `business_id` = 151
	AND `name` = 'Cost of Goods'
	AND `number` IS NULL
;

