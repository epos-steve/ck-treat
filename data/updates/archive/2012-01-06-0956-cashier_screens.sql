alter table promo_groups_detail add column display_x int default null after display_order;
alter table promo_groups_detail add column display_y int default null after display_order;
alter table promo_groups add column display_x int default null after display_order;
alter table promo_groups add column display_y int default null after display_order;

CREATE TABLE modifier_groups(
	id INT UNSIGNED NOT NULL AUTO_INCREMENT KEY,
	uuid CHAR(36) NOT NULL DEFAULT '',
	name VARCHAR(48) NOT NULL DEFAULT '',
	selectionType ENUM( 'single', 'multiple' ) NOT NULL DEFAULT 'multiple'
) ENGINE=INNODB;

CREATE TABLE modifiers(
	id INT UNSIGNED NOT NULL AUTO_INCREMENT KEY,
	uuid CHAR(36) NOT NULL DEFAULT '',
	groupId INT UNSIGNED NOT NULL,
	name VARCHAR(48) NOT NULL DEFAULT '',
	price DOUBLE NOT NULL DEFAULT 0.00,
	button_color SMALLINT NOT NULL DEFAULT 0,
	preset TINYINT(1) NOT NULL DEFAULT 0
) ENGINE=INNODB;