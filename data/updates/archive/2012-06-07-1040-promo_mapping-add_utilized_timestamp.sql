ALTER TABLE `promo_mapping` ADD COLUMN `UtilizedChecked` DATETIME NOT NULL DEFAULT 0  AFTER `utilized`;
ALTER TABLE `promo_mapping` CHANGE COLUMN `utilized` `Utilized` TINYINT(1) NOT NULL DEFAULT 0 COMMENT 'Nonzero if this promotion has been used at least once from a client machine with this business id'  ;
