# $Id: 2010-11-18-1755.sql 4489 2010-11-19 20:08:10Z patrick $

INSERT INTO `vend_gl_account_types` (
	`id`
	,`account_class`
	,`label`
)
VALUES
	(  4, 'Treat_Model_GeneralLedger_Account_CalculatedObject', 'Calculated at AccountGroup level' )
;

UPDATE `vend_gl_accounts`
SET `account_type_id` = 4
WHERE `id` IN (
	3	# Sales Tax
	,11	# Payroll Taxex
	,12	# Group Insurance
	,13	# 401K Expense
);

