
# DELETE all menu_items out of KC units except for the exceptions
DELETE mi
FROM `menu_items` mi
	JOIN `business` b
		ON mi.`businessid` = b.`businessid`
		AND b.`districtid` = 1
		AND b.`companyid` = 1
		AND b.`businessid` NOT IN (
			2	# St James Academy
			,48	# American Family
			,98	# Contract Feeding KC
			,107	# DST CA - Hilltop
			,110	# DST CA - Output
			,262	# Federal Reserve of KC - Cafe
			,282	# California ISO
			,343	# [KC] Corporate Recipes
		)
WHERE mi.`menu_typeid` = 17
;

# UPDATE all of the businesses, except our exception list, to use the new nutrition
UPDATE `business` b
SET b.`use_nutrition` = 1
WHERE
	b.`districtid` = 1
	AND b.`companyid` = 1
	AND b.`businessid` NOT IN (
		2	# St James Academy
		,48	# American Family
		,98	# Contract Feeding KC
		,107	# DST CA - Hilltop
		,110	# DST CA - Output
		,262	# Federal Reserve of KC - Cafe
		,282	# California ISO
		,343	# [KC] Corporate Recipes
	)
;



# DELETE all menu_items out of KC units except for the exceptions
DELETE mi
FROM `menu_items` mi
	JOIN `business` b
		ON mi.`businessid` = b.`businessid`
		AND b.`districtid` = 1
		AND b.`companyid` = 1
		AND b.`businessid` = 262	# Federal Reserve of KC - Cafe
WHERE mi.`menu_typeid` = 17
;

# UPDATE all of the businesses, except our exception list, to use the new nutrition
UPDATE `business` b
SET b.`use_nutrition` = 1
WHERE
	b.`districtid` = 1
	AND b.`companyid` = 1
	AND b.`businessid` = 262	# Federal Reserve of KC - Cafe
;

