
ALTER TABLE `customer` CHANGE `username` `username` VARCHAR( 50 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '';
ALTER TABLE `customer` CHANGE `password` `password` VARCHAR( 25 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '';
ALTER TABLE `customer` CHANGE `scancode` `scancode` VARCHAR( 20 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '';
ALTER TABLE `customer` ADD `password_new` VARCHAR( 60 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' AFTER `password`;
ALTER TABLE `customer` ADD `first_name` VARCHAR( 50 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '';
ALTER TABLE `customer` ADD `last_name` VARCHAR( 50 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '';
ALTER TABLE `customer` ADD `temp_password` VARCHAR( 10 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '';
ALTER TABLE `customer` ADD `temp_used` TINYINT( 1 ) UNSIGNED NOT NULL DEFAULT '1';

# create new business_gateway_data table
CREATE TABLE `business_gateway_data` (
	`businessid` INT(11) UNSIGNED NOT NULL,
	`store_name` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
	`store_number` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`store_user_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`store_pem_key` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
	UNIQUE KEY `businessid` (`businessid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `business_gateway_data` (
	`businessid`
	,`store_name`
	,`store_number`
	,`store_user_id`
	,`store_pem_key`
)
VALUES
	(  '287', 'COMPANY KITCHEN', '1001247941', '247941', '1001247941.pem' )
;

# create new transactions table
CREATE TABLE `customer_transactions` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`oid` VARCHAR(35) CHARACTER SET utf8 NOT NULL COMMENT 'The order id that was sent to or received from linkpoint',
	`customer_id` INT(11) UNSIGNED NOT NULL,
	`pos_processed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
	`subtotal` FLOAT NOT NULL,
	`chargetotal` FLOAT NOT NULL,
	`date_created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`authcode` VARCHAR(15) CHARACTER SET utf8 NOT NULL,
	`response` VARCHAR(15) CHARACTER SET utf8 NOT NULL,
	`authresponse` TEXT CHARACTER SET utf8 NOT NULL,
	`response_messages` TEXT CHARACTER SET utf8 NOT NULL,
	`response_tdate` DATETIME NOT NULL DEFAULT '0000-00-00',
	`response_avs` TEXT CHARACTER SET utf8 NOT NULL,
	`response_token` TEXT CHARACTER SET utf8 NOT NULL,
	PRIMARY KEY (`id`),
	INDEX (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

# create new transactions_debug table
CREATE TABLE `customer_transactions_debug` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`customer_id` INT(10) UNSIGNED NOT NULL,
	`transaction_id` VARCHAR(255) NOT NULL DEFAULT '',
	`order_id` VARCHAR(255) NOT NULL DEFAULT '',
	`response` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`response_xml` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`request` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`request_xml` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

