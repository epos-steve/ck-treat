DROP TABLE IF EXISTS aeris_modifier;
DROP TABLE IF EXISTS aeris_modifier_group;

CREATE TABLE aeris_modifier_group(
	id INT UNSIGNED NOT NULL KEY AUTO_INCREMENT,
	businessid INT NOT NULL,
	name VARCHAR(48) NOT NULL DEFAULT '',
	`type` ENUM('single', 'multiple') NOT NULL DEFAULT 'single',
	newLine BOOL NOT NULL DEFAULT false,
	active BOOL NOT NULL DEFAULT true,
	FOREIGN KEY (businessid) REFERENCES business(businessid) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=INNODB CHARSET=utf8;

CREATE TABLE aeris_modifier(
	id INT UNSIGNED NOT NULL KEY AUTO_INCREMENT,
	modifierGroupId INT UNSIGNED NOT NULL,
	name VARCHAR(48) NOT NULL DEFAULT '',
	price DOUBLE NOT NULL DEFAULT 0.0,
	preset BOOL NOT NULL DEFAULT false,
	pos_color TINYINT NOT NULL DEFAULT 0,
	active BOOL NOT NULL DEFAULT true,
	FOREIGN KEY (modifierGroupId) REFERENCES aeris_modifier_group(id) ON DELETE CASCADE 
) ENGINE=INNODB CHARSET=utf8;

ALTER TABLE menu_items_new ADD COLUMN cond_group INT UNSIGNED DEFAULT NULL;
ALTER TABLE menu_items_new ADD INDEX cond_group_index (cond_group);
ALTER TABLE menu_items_new ADD FOREIGN KEY cond_group_index (cond_group) REFERENCES aeris_modifier_group (id) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE menu_items_new ADD COLUMN force_condiment BOOL NOT NULL DEFAULT false;