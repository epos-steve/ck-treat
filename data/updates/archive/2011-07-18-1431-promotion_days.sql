create or replace view vPromoDays as
	select 0 as day,id from promotions psu where psu.sunday=1
	union
	select 1 as day,id from promotions pm where pm.monday=1
	union
	select 2 as day,id from promotions pt where pt.tuesday=1
	union
	select 3 as day,id from promotions pw where pw.wednesday=1
	union
	select 4 as day,id from promotions pth where pth.thursday=1
	union
	select 5 as day,id from promotions pf where pf.friday=1
	union
	select 6 as day,id from promotions ps where ps.saturday=1;
