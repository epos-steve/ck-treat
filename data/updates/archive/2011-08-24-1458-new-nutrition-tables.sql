
ALTER TABLE `company` CHANGE `companyid` `companyid` INT( 3 ) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `business` CHANGE `companyid` `companyid` INT( 3 ) UNSIGNED NOT NULL DEFAULT '1';

ALTER TABLE `menu_items_new` CHANGE `menu_itemid` `mic_id` INT UNSIGNED NOT NULL COMMENT 'id from menu_item_company';

DROP TABLE IF EXISTS `menu_items_company`;
CREATE TABLE `menu_items_company` (
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY
	,`company_id` INT( 3 ) UNSIGNED NOT NULL
	,`item_code` VARCHAR( 30 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
	,`name` VARCHAR( 100 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
	,UNIQUE (
		`company_id` ,
		`item_code`
	)
) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci
	COMMENT = 'menu items per company id'
;

# company_id would be redundant in this table
DROP TABLE IF EXISTS `nutrition_xref_company`;
CREATE TABLE `nutrition_xref_company` (
	`mic_id` INT UNSIGNED NOT NULL COMMENT 'menu_item_company.id'
	,`nutrition_type_id` INT( 3 ) UNSIGNED NOT NULL COMMENT 'nutrition_types.nutritionid'
	,`value` double NOT NULL
	,PRIMARY KEY ( `mic_id`, `nutrition_type_id` )
) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci
	COMMENT = 'crossreference between menu_items_company and nutrition_types'
;

