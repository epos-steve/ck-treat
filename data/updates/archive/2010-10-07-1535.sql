
ALTER TABLE `machine_order` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
ALTER TABLE `machine_orderdetail` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;

ALTER TABLE `machine_order` CHANGE `customer` `customer` VARCHAR( 20 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;
ALTER TABLE `machine_order` CHANGE `machine_num` `machine_num` VARCHAR( 20 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;

ALTER TABLE `machine_orderdetail` CHANGE `item_code` `item_code` VARCHAR( 30 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;
ALTER TABLE `machine_orderdetail` CHANGE `new_item_code` `new_item_code` VARCHAR( 20 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;
ALTER TABLE `machine_orderdetail` CHANGE `new_qty` `new_qty` DOUBLE NULL DEFAULT NULL;

UPDATE `machine_orderdetail` SET `new_item_code` = NULL, `new_qty` = NULL WHERE `new_item_code` = '' AND `new_qty` = 0;

ALTER TABLE `login_route` ADD INDEX ( `route` );

ALTER TABLE `machine_order` CHANGE `id` `id` INT( 10 ) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `machine_order` ADD `route` VARCHAR( 10 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `id`;
ALTER TABLE `machine_order` CHANGE `date_time` `expected_delivery` DATE NOT NULL AFTER `route`;
ALTER TABLE `machine_order` ADD `login_routeid` INT UNSIGNED NOT NULL AFTER `expected_delivery`;
ALTER TABLE `machine_order` ADD `accountid` INT UNSIGNED NOT NULL AFTER `machineid`;
ALTER TABLE `machine_order` CHANGE `machineid` `machineid` INT( 10 ) UNSIGNED NOT NULL;
ALTER TABLE `machine_order` ADD INDEX ( `route` , `expected_delivery` ) ;

UPDATE `machine_order` mo
SET accountid = ( SELECT accountid
		FROM `accounts` a
		WHERE a.`accountnum` = mo.`customer`
	)
	,login_routeid = (
		SELECT lr.login_routeid
		FROM vend_machine vm
			JOIN login_route lr
				ON vm.login_routeid = lr.login_routeid
		WHERE vm.machineid = mo.machineid
	)
	,route = (
		SELECT lr.route
		FROM vend_machine vm
			JOIN login_route lr
				ON vm.login_routeid = lr.login_routeid
		WHERE vm.machineid = mo.machineid
	)
;

ALTER TABLE `machine_order` DROP `customer`;
ALTER TABLE `machine_order` DROP `machine_num`;

ALTER TABLE `machine_orderdetail` CHANGE `id` `id` INT( 10 ) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `machine_orderdetail` CHANGE `orderid` `orderid` INT( 10 ) UNSIGNED NOT NULL;
ALTER TABLE `machine_orderdetail` ADD `menu_items_new_id` INT( 11 ) UNSIGNED NOT NULL AFTER `orderid`;

UPDATE `machine_orderdetail` m_od
SET `menu_items_new_id` = (
		SELECT `id`
		FROM `menu_items_new` men_in
		WHERE men_in.`item_code` = m_od.`item_code`
	)
;
