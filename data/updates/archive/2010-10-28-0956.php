#!/usr/bin/env php
<?php
echo 'Make sure you\'ve run the associated .sql file *FIRST*', PHP_EOL;

if ( preg_match( '@ehost[0-9]\.essential-elements\.net@', php_uname( 'n' ) ) ) {
	define( 'IN_PRODUCTION', true );
}
else {
	define( 'IN_PRODUCTION', false );
}
require_once( dirname(__FILE__) . '/../lib/inc/db.php' );

try {
	$dsn = sprintf( 'mysql:dbname=%s;host=%s', $GLOBALS['dbname'], $GLOBALS['dbhost'] );
	$dbh = new PDO( $dsn, $GLOBALS['dbuser'], $GLOBALS['dbpass'] );
	
	$sql = '
		UPDATE `vend_gl_accounts`
		SET
			`number` = :number
			,`name`  = :name
		WHERE
			`id`     = :id
	';
	$update = $dbh->prepare( $sql );
	
	$sql = '
		SELECT
			`id`
			,`name`
		FROM
			`vend_gl_accounts`
		WHERE
			`number` IS NULL
			OR `number` = \'\'
	';
	$select = $dbh->prepare( $sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ) );
	$select->execute();
	
	$dbh->beginTransaction();
	while ( $obj = $select->fetchObject() ) {
		if ( preg_match( '@[0-9\-]{16}\b@', $obj->name ) ) {
			$pieces = explode( ' ', $obj->name, 2 );
			$number = array_shift( $pieces );
			$name   = array_shift( $pieces );
			$params = array(
				'number' => $number,
				'name'   => $name,
				'id'     => $obj->id,
			);
			$update->execute( $params );
		}
	}
	$dbh->commit();
	
}
catch ( PDOException $e ) {
	echo 'Connection failed: ', $e->getErrorCode(), ': ', $e->getMessage(), PHP_EOL;
	if ( isset( $dbh ) ) {
		$dbh->rollBack();
	}
}


