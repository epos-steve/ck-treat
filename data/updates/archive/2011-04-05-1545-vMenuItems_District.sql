create or replace view vMenuItems_District as
	select 
		menu_items.*,
		district.districtname,
		business.businessname,
		menu_type.menu_typename,
		servery_station.station_name
	from menu_items
	left join business using( businessid )
	left join district using( districtid )
	left join menu_type using( menu_typeid )
	left join servery_station on stationid = recipe_station;