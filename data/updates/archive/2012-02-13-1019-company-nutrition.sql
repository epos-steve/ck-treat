

CREATE TABLE IF NOT EXISTS `company_nutrition` (
	`co_nut_id` int UNSIGNED NOT NULL AUTO_INCREMENT
	,`businessid` int(10) NOT NULL
	,`date_stamp` date NOT NULL
	,`payment_type` int(11) NOT NULL COMMENT 'currently ck-card user or non-ck-card user'
	,`unique_user` int(10) NOT NULL COMMENT 'unique ck-card #s or unique bill number based on payment_type'
	,PRIMARY KEY ( `co_nut_id` )
	,UNIQUE ( `businessid` , `payment_type` , `date_stamp` )
) ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci
	COMMENT='storage for data generated for company level nutrition'
;

CREATE TABLE IF NOT EXISTS `company_nutrition_xref` (
	`co_nut_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'company_nutrition.co_nut_id'
	,`nutrition_type_id` int(3) unsigned NOT NULL COMMENT 'nutrition_types.nutritionid'
	,`value` double NOT NULL
	,PRIMARY KEY ( `co_nut_id`,`nutrition_type_id` )
) ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci
	COMMENT='crossreference between company_nutrition and nutrition_type'
;

ALTER TABLE `business_gateway_data` ADD `use_business_nutrition` BOOLEAN NOT NULL DEFAULT '0'
;

UPDATE `business_gateway_data`
SET `use_business_nutrition` = TRUE
WHERE `businessid` IN ( 307, 332 )
;

RENAME TABLE `company_nutrition` TO `business_nutrition`;
RENAME TABLE `company_nutrition_xref` TO `business_nutrition_xref`;

