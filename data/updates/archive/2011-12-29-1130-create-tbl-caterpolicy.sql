CREATE TABLE IF NOT EXISTS `caterpolicy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `businessid` int(11) NOT NULL,
  `policy` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `businessid` (`businessid`)
) ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci;
