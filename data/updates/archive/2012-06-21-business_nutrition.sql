
DROP TABLE IF EXISTS `business_nutrition`;
CREATE TABLE `business_nutrition` (
	`co_nut_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`businessid` int(10) NOT NULL,
	`date_stamp` date NOT NULL,
	`payment_type` int(11) NOT NULL COMMENT 'currently ck-card user or non-ck-card user',
	`unique_user` int(10) NOT NULL COMMENT 'unique ck-card #s or unique bill number based on payment_type',
	PRIMARY KEY (`co_nut_id`),
	UNIQUE KEY `businessid` (`businessid`,`payment_type`,`date_stamp`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='storage for data generated for company level nutrition'
;


DROP TABLE IF EXISTS `business_nutrition_xref`;
CREATE TABLE `business_nutrition_xref` (
	`co_nut_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'company_nutrition.co_nut_id',
	`nutrition_type_id` int(3) UNSIGNED NOT NULL COMMENT 'nutrition_types.nutritionid',
	`value` double NOT NULL,
	PRIMARY KEY (`co_nut_id`,`nutrition_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='crossreference between company_nutrition and nutrition_type'
;

