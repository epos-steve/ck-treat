create table db_cache(
	cacheId int unsigned key not null,
	viewid int unsigned not null,
	dataXml longtext,
	cacheDate timestamp on update current_timestamp default current_timestamp
);

create or replace view vDbCustom as
	select 
		cast("custom" as char) as vGroupId,
		db_view.*,
		UNIX_TIMESTAMP( db_view.db_mtime ) as mtime,
		UNIX_TIMESTAMP( pub_db_view.db_mtime ) as ptime,
		db_view_order.dbSide as dbSide,
		db_view_order.dbHeight as dbHeight,
		db_view_order.dbOrder as dbOrder,
		db_cache.cacheId as cacheId,
		db_cache.cacheDate as cacheDate
	from db_view
	left join pub_db_view using( viewid )
	left join db_view_order using( viewid )
	left join db_cache using( viewid );

create or replace view vDbGroup as
	select
		cast(db_group_graphs.groupid as char) as vGroupId,
		pub_db_view.*,
		db_group_graphs.dbSide as dbSide,
		db_group_graphs.dbOrder as dbOrder,
		db_group_graphs.dbHeight as dbHeight,
		db_cache.cacheId as cacheId,
		db_cache.cacheDate as cacheDate
	from pub_db_view
	inner join db_group_graphs using( viewid )
	left join db_cache using( viewid );

