
#  changing the UNIQUE index to non-unique as you can pay with multiple credit cards
ALTER TABLE `payment_detail` DROP INDEX `businessid` ,
	ADD INDEX `businessid` ( `businessid` , `check_number` , `payment_type` );

