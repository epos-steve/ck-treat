#!/usr/bin/env php
<?php
echo 'Make sure you\'ve run the associated .sql file *FIRST*', PHP_EOL;

if ( preg_match( '@ehost[0-9]\.essential-elements\.net@', php_uname( 'n' ) ) ) {
	define( 'IN_PRODUCTION', true );
}
else {
	define( 'IN_PRODUCTION', false );
}
require_once( dirname(__FILE__) . '/../lib/inc/db.php' );
require_once( dirname(__FILE__) . '/../lib/SiTech/DB.php' );
require_once( dirname(__FILE__) . '/../lib/SiTech/DB/Proxy.php' );
require_once( dirname(__FILE__) . '/../lib/Treat/DB.php' );

$field_convert = array(
	'cal_from_fat' => 'Cal From Fat',
	'calcium' => 'Calcium',
	'calories' => 'Calories',
	'cholesterol' => 'Cholesterol',
	'complex_cabs' => 'Carbs',
	'fiber' => 'Fiber',
	'iron' => 'Iron',
	'magnesium' => 'Magnesium',
	'niacin' => 'Niacin',
	'phosphorus' => 'Phosphorus',
	'potassium' => 'Potassium',
	'protein' => 'Protein',
	'riboflavin' => 'Riboflavin',
	'sat_fat' => 'Saturated Fat',
	'sodium' => 'Sodium',
	'sugar' => 'Sugar',
	'thiamine' => 'Thiamine',
	'total_fat' => 'Total Fat',
	'unsat_fat' => 'Unsaturated Fat',
	'vit_A' => 'Vitamin A',
	'vit_C' => 'Vitamin C',
	'zinc' => 'Zinc',
);

$field_units = array(
#	'cal_from_fat' => '',
#	'calcium' => '%',
#	'calories' => '',
	'cholesterol' => 'mg',
	'complex_cabs' => 'gm',
	'fiber' => 'gm',
#	'iron' => '%',
#	'magnesium' => '',
#	'niacin' => '',
#	'phosphorus' => '',
	'potassium' => 'mg',
	'protein' => 'gm',
#	'riboflavin' => '',
	'sat_fat' => 'gm',
	'sodium' => 'mg',
	'sugar' => 'gm',
#	'thiamine' => '',
	'total_fat' => 'gm',
#	'unsat_fat' => '',
	'vit_A' => '%',
	'vit_C' => '%',
#	'zinc' => '',
);

$existing_types = array();

try {
	$dsn = sprintf( 'mysql:dbname=%s;host=%s', $GLOBALS['dbname'], $GLOBALS['dbhost'] );
	$dbh = new PDO( $dsn, $GLOBALS['dbuser'], $GLOBALS['dbpass'] );
	$dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
	
	$select_nx_sql = '
			SELECT *
			FROM nutrition_xref_types
			LIMIT 1
	';
	$select_nx = $dbh->prepare( $select_nx_sql );
	$select_nx->execute();
	echo '* NEW nutrition_xref_types does exist', PHP_EOL;
	
	$select_sql = sprintf( '
			SELECT *
			FROM nutrition_types
			WHERE name IN (
				\'%s\'
			)
		'
		,implode( "'\n\t\t\t\t,'", $field_convert )
	);
#	echo '$sql = ', $select_sql, PHP_EOL;
	$select = $dbh->prepare( $select_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ) );
	
	$update_sql = sprintf( '
			UPDATE `nutrition_types`
			SET `column_key` = :column_key
				,units       = :units
			WHERE `name` = :name
		'
	);
#	echo '$sql = ', $insert_sql, PHP_EOL;
	$update = $dbh->prepare( $update_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ) );
	
	$insert_sql = sprintf( '
			INSERT INTO nutrition_types ( name, units, column_key )
			VALUES ( :name, :units, :column_key )
		'
	);
#	echo '$sql = ', $insert_sql, PHP_EOL;
	$insert = $dbh->prepare( $insert_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ) );
	
	$select->execute();
	while ( $obj = $select->fetchObject() ) {
		$key = array_search( $obj->name, $field_convert );
#		var_dump( $key );
		if ( $key ) {
			$existing_types[ $key ] = $obj->name;
		}
	}
	
	$updates = array_intersect_key( $field_convert, $existing_types );
	echo '* Existing items to be updated: ';
#	var_dump( $updates );
	print_r( $updates );
	
	$inserts = array_diff_key( $field_convert, $existing_types );
	echo '* New items to be added to nutrition_types, remember to set the `units` field: ';
#	var_dump( $inserts );
	print_r( $inserts );
	
	echo '* BEGIN TRANSACTION', PHP_EOL;
	$dbh->beginTransaction();
	$count = array();
	echo '** updating `nutrition_types`', PHP_EOL;
	echo '*** updated: ';
	foreach( $updates as $key => $value ) {
#		var_dump( $value );
		if ( array_key_exists( $key, $field_units ) && $field_units[ $key ] ) {
			$units = $field_units[ $key ];
		}
		else {
			$units = '';
		}
		$params = array(
			'name'       => $value,
			'column_key' => $key,
			'units'      => $units,
		);
		$sql = Treat_DB::getDebugSql( $update_sql, $params );
#		var_dump( $sql );
		$update->execute( $params );
		$tmp = $update->rowCount();
		echo '"', $value, '" (', $tmp, '), ';
		$count[] = $tmp;
	}
	echo PHP_EOL;
	echo '******************************************', PHP_EOL;
	echo '*  ', array_sum( $count ), ' rows updated in nutrition_types.  *', PHP_EOL;
	echo '******************************************', PHP_EOL;
	
	$count = array();
	echo '** inserting `nutrition_types`', PHP_EOL;
	echo '*** inserted: ';
	foreach( $inserts as $key => $value ) {
#		var_dump( $value );
		if ( array_key_exists( $key, $field_units ) && $field_units[ $key ] ) {
			$units = $field_units[ $key ];
		}
		else {
			$units = '';
		}
		$params = array(
			'name' => $value,
			'column_key' => $key,
			'units'      => $units,
		);
		$sql = Treat_DB::getDebugSql( $insert_sql, $params );
#		var_dump( $sql );
		$insert->execute( $params );
		$tmp = $insert->rowCount();
		echo '"', $value, '" (', $tmp, '), ';
		$count[] = $tmp;
	}
	echo PHP_EOL;
	echo '******************************************', PHP_EOL;
	echo '*  ', array_sum( $count ), ' rows inserted in nutrition_types.  *', PHP_EOL;
	echo '******************************************', PHP_EOL;
	
	$count = array();
	echo '** inserting `nutrition_xref_types`', PHP_EOL;
	echo '*** inserted: ';
	foreach( $field_convert as $column => $name ) {
		$insert_nx_sql = sprintf( '
				INSERT INTO nutrition_xref_types (
					item_code
					,nutrition_types_id
					,value
				)
				SELECT
					n.item_code
					,nt.nutritionid
					,n.%1$s
				FROM nutrition n
					JOIN nutrition_types nt
						ON nt.name = :name
				WHERE n.%1$s IS NOT NULL
					AND n.%1$s != 0
			'
			,$column
		);
		
		$params = array(
			'name' => $name,
		);
		$sql = Treat_DB::getDebugSql( $insert_nx_sql, $params );
#		var_dump( $sql );
		$insert_nx = $dbh->prepare( $insert_nx_sql, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ) );
		$insert_nx->execute( $params );
		$tmp = $insert_nx->rowCount();
		echo '"', $name, '"/"', $column, '" (', $tmp, '), ';
		$count[] = $tmp;
	}
	echo PHP_EOL;
	echo '**************************************************', PHP_EOL;
	echo '*  ', array_sum( $count ), ' rows inserted in nutrition_xref_types.  *', PHP_EOL;
	echo '**************************************************', PHP_EOL;
	
#	$dbh->rollBack();
	$dbh->commit();
}
catch ( PDOException $e ) {
	echo 'Connection failed: ', $e->getCode(), ': ', $e->getMessage(), PHP_EOL;
	echo PHP_EOL;
	echo '**************************************************************', PHP_EOL;
	echo '*  Did you remember to run the associated .sql file first?  *', PHP_EOL;
	echo '**************************************************************', PHP_EOL;
	echo PHP_EOL;
	if ( isset( $dbh ) ) {
		try {
			$dbh->rollBack();
		}
		catch ( PDOException $e ) {
			echo 'rollBack exception: ', $e->getCode(), ': ', $e->getMessage(), PHP_EOL;
		}
	}
}


