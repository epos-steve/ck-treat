
ALTER TABLE `customer_transaction_types` ADD `label` VARCHAR( 50 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' AFTER `name`;

UPDATE `customer_transaction_types`
SET `label` = "Credit Card Transaction"
WHERE `id` = 1
;

UPDATE `customer_transaction_types`
SET `label` = "Credit Card Refund"
WHERE `id` = 2
;

UPDATE `customer_transaction_types`
SET `label` = "Company Kitchen Refund"
WHERE `id` = 3
;

UPDATE `customer_transaction_types`
SET `label` = "Promotion"
WHERE `id` = 4
;



