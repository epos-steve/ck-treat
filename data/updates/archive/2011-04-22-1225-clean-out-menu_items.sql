
# DELETE all menu_items out of St Louis units except for the exceptions
DELETE mi
FROM `menu_items` mi
	JOIN `business` b
		ON mi.`businessid` = b.`businessid`
		AND b.`districtid` = 2
		AND b.`companyid` = 1
		AND b.`businessid` NOT IN (
			258	# []Corporate Chef Account
			,380	# [STL] Corporate Recipes
			,385	# [STL] Test Unit
		)
WHERE mi.`menu_typeid` = 17
;

# UPDATE all of the businesses, except our exception list, to use the new nutrition
UPDATE `business` b
SET b.`use_nutrition` = 1
WHERE
	b.`districtid` = 2
	AND b.`companyid` = 1
	AND b.`businessid` NOT IN (
			258	# []Corporate Chef Account
			,380	# [STL] Corporate Recipes
			,385	# [STL] Test Unit
	)
;



