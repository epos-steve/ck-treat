
CREATE TABLE IF NOT EXISTS `business_group` (
  `group_id` int(10) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `business_group_detail` (
  `group_id` int(10) NOT NULL,
  `businessid` int(10) NOT NULL,
  PRIMARY KEY (`group_id`,`businessid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
