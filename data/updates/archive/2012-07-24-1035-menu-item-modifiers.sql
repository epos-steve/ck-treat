DROP TABLE IF EXISTS menu_items_new_modifier_groups;

CREATE TABLE menu_items_new_modifier_groups (
	menu_item_id INT UNSIGNED NOT NULL,
	modifier_group_id INT UNSIGNED NOT NULL,
	PRIMARY KEY( menu_item_id, modifier_group_id ),
	FOREIGN KEY fk_menu_items_new( menu_item_id ) REFERENCES menu_items_new( id ) ON DELETE CASCADE,
	FOREIGN KEY fk_aeris_modifier_group( modifier_group_id ) REFERENCES aeris_modifier_group( id ) ON DELETE CASCADE
) ENGINE=INNODB;

INSERT INTO menu_items_new_modifier_groups( menu_item_id, modifier_group_id ) 
	SELECT id, cond_group FROM menu_items_new
	WHERE cond_group IS NOT NULL
	AND NOT EXISTS(SELECT 1 FROM menu_items_new_modifier_groups WHERE menu_item_id=id and modifier_group_id=cond_group);
