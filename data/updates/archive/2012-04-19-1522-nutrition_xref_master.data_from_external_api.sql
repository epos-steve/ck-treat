-- phpMyAdmin SQL Dump
-- version 3.4.10.2deb1.maverick~ppa.1
-- http://www.phpmyadmin.net
--
-- Host: 192.168.2.2
-- Generation Time: Apr 19, 2012 at 03:21 PM
-- Server version: 5.1.61
-- PHP Version: 5.3.3-1ubuntu9.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mburris_businesstrack`
--

--
-- Dumping data for table `nutrition_xref_master`
--

INSERT INTO `nutrition_xref_master` (`mim_id`, `nutrition_type_id`, `value`) VALUES
(5275, 1, 240),
(5275, 2, 35),
(5275, 4, 0),
(5275, 5, 0),
(5275, 6, 125),
(5275, 8, 45),
(5275, 9, 10),
(5275, 10, 15),
(5275, 11, 12),
(5275, 20, 0),
(5275, 21, 0),
(5275, 22, 0),
(5276, 1, 130),
(5276, 2, 10),
(5276, 3, 1),
(5276, 4, 0),
(5276, 5, 0),
(5276, 6, 75),
(5276, 8, 26),
(5276, 9, 9),
(5276, 10, 5),
(5276, 11, 12),
(5276, 20, 0),
(5276, 21, 0),
(5276, 22, 0),
(5290, 1, 130),
(5290, 6, 10),
(5290, 8, 30),
(5290, 10, 25),
(5290, 11, 1),
(5290, 22, 20),
(5463, 1, 140),
(5463, 2, 20),
(5463, 3, 2),
(5463, 4, 0),
(5463, 5, 0),
(5463, 6, 100),
(5463, 8, 31),
(5463, 9, 5),
(5463, 10, 7),
(5463, 11, 5),
(5463, 20, 0),
(5463, 21, 30),
(5463, 22, 60),
(5523, 1, 220),
(5523, 2, 100),
(5523, 3, 11),
(5523, 4, 1),
(5523, 5, 0),
(5523, 6, 65),
(5523, 8, 22),
(5523, 9, 6),
(5523, 10, 8),
(5523, 11, 7),
(5523, 20, 0),
(5523, 21, 10),
(5523, 22, 10),
(5529, 1, 10),
(5529, 3, 0),
(5529, 6, 0),
(5529, 8, 2),
(5529, 10, 2),
(5529, 11, 0),
(5597, 1, 5),
(5597, 3, 0),
(5597, 6, 0),
(5597, 8, 2),
(5597, 10, 0),
(5597, 11, 0),
(5598, 1, 5),
(5598, 3, 0),
(5598, 6, 0),
(5598, 8, 2),
(5598, 10, 0),
(5598, 11, 0),
(5652, 1, 150),
(5652, 6, 120),
(5652, 8, 42),
(5652, 10, 40),
(5652, 11, 0),
(5669, 1, 280),
(5669, 3, 0),
(5669, 6, 65),
(5669, 8, 77),
(5669, 10, 75),
(5669, 11, 0),
(5669, 22, 250),
(5670, 1, 260),
(5670, 3, 0),
(5670, 6, 80),
(5670, 8, 70),
(5670, 10, 67),
(5670, 11, 0),
(5671, 1, 100),
(5671, 2, 0),
(5671, 3, 0),
(5671, 4, 0),
(5671, 5, 0),
(5671, 6, 80),
(5671, 8, 28),
(5671, 9, 0),
(5671, 10, 27),
(5671, 11, 0),
(5704, 1, 150),
(5704, 2, 0),
(5704, 3, 0),
(5704, 6, 20),
(5704, 8, 39),
(5704, 9, 0),
(5704, 10, 38),
(5704, 11, 0),
(5705, 1, 110),
(5705, 3, 0),
(5705, 6, 15),
(5705, 8, 27),
(5705, 10, 24),
(5705, 11, 2),
(5705, 22, 100),
(5706, 1, 120),
(5706, 3, 0),
(5706, 6, 25),
(5706, 8, 32),
(5706, 10, 31),
(5706, 11, 0),
(5707, 1, 110),
(5707, 3, 0),
(5707, 6, 20),
(5707, 8, 28),
(5707, 10, 26),
(5707, 11, 0),
(5910, 1, 200),
(5910, 2, 50),
(5910, 3, 5),
(5910, 5, 0),
(5910, 6, 180),
(5910, 8, 32),
(5910, 9, 5),
(5910, 10, 17),
(5910, 11, 10),
(5910, 20, 0),
(5910, 21, 0),
(5910, 22, 0),
(5939, 1, 330),
(5939, 2, 260),
(5939, 3, 29),
(5939, 4, 4),
(5939, 5, 0),
(5939, 6, 230),
(5939, 8, 9),
(5939, 9, 5),
(5939, 10, 2),
(5939, 11, 15),
(5939, 20, 0),
(5939, 21, 0),
(5939, 22, 0),
(5941, 3, 0),
(5941, 6, 0),
(5941, 8, 1),
(5941, 10, 0),
(5941, 11, 0),
(5942, 1, 220),
(5942, 2, 40),
(5942, 5, 5),
(5942, 6, 460),
(5942, 8, 39),
(5942, 10, 5),
(5942, 11, 7),
(5942, 20, 0),
(5942, 21, 0),
(5942, 22, 0),
(5993, 1, 210),
(5993, 2, 90),
(5993, 3, 10),
(5993, 5, 0),
(5993, 6, 220),
(5993, 8, 28),
(5993, 10, 8),
(5993, 11, 2),
(5993, 21, 0),
(5993, 22, 0),
(5998, 1, 270),
(5998, 2, 150),
(5998, 3, 17),
(5998, 4, 5),
(5998, 5, 0),
(5998, 6, 30),
(5998, 8, 27),
(5998, 9, 3),
(5998, 10, 19),
(5998, 11, 7),
(5998, 20, 0),
(5998, 21, 0),
(5998, 22, 2),
(6004, 1, 150),
(6004, 2, 70),
(6004, 3, 7),
(6004, 5, 0),
(6004, 6, 100),
(6004, 8, 21),
(6004, 9, 1),
(6004, 10, 9),
(6004, 11, 1),
(6004, 20, 0),
(6004, 21, 0),
(6004, 22, 0),
(6009, 1, 230),
(6009, 2, 80),
(6009, 3, 9),
(6009, 5, 0),
(6009, 6, 510),
(6009, 8, 34),
(6009, 9, 1),
(6009, 10, 6),
(6009, 11, 3),
(6009, 20, 0),
(6009, 21, 2),
(6009, 22, 0),
(6012, 1, 220),
(6012, 2, 100),
(6012, 3, 11),
(6012, 4, 6),
(6012, 5, 5),
(6012, 6, 60),
(6012, 8, 30),
(6012, 9, 1),
(6012, 10, 24),
(6012, 11, 2),
(6012, 20, 0),
(6012, 21, 0),
(6012, 22, 0),
(6013, 1, 280),
(6013, 2, 110),
(6013, 3, 13),
(6013, 4, 9),
(6013, 5, 0),
(6013, 6, 130),
(6013, 8, 37),
(6013, 9, 2),
(6013, 10, 31),
(6013, 11, 4),
(6013, 21, 0),
(6013, 22, 0),
(6014, 1, 270),
(6014, 2, 100),
(6014, 3, 11),
(6014, 4, 6),
(6014, 5, 0),
(6014, 6, 120),
(6014, 8, 44),
(6014, 9, 1),
(6014, 10, 30),
(6014, 11, 3),
(6014, 21, 0),
(6014, 22, 0),
(6016, 1, 200),
(6016, 2, 70),
(6016, 3, 8),
(6016, 4, 5),
(6016, 5, 10),
(6016, 6, 75),
(6016, 8, 30),
(6016, 9, 1),
(6016, 10, 27),
(6016, 11, 2),
(6021, 1, 200),
(6021, 2, 70),
(6021, 3, 8),
(6021, 5, 5),
(6021, 6, 15),
(6021, 8, 31),
(6021, 9, 1),
(6021, 10, 28),
(6021, 11, 2),
(6071, 1, 0),
(6071, 3, 0),
(6071, 6, 25),
(6071, 8, 0),
(6071, 10, 0),
(6071, 11, 0),
(6077, 1, 110),
(6077, 3, 0),
(6077, 6, 40),
(6077, 8, 31),
(6077, 10, 31),
(6077, 11, 0),
(6089, 1, 250),
(6089, 8, 65),
(6090, 1, 190),
(6090, 2, 50),
(6090, 3, 6),
(6090, 4, 2),
(6090, 5, 5),
(6090, 6, 220),
(6090, 8, 24),
(6090, 9, 5),
(6090, 10, 18),
(6090, 11, 10),
(6090, 20, 0),
(6090, 21, 35),
(6090, 22, 100),
(6129, 1, 110),
(6129, 2, 0),
(6129, 3, 0),
(6129, 4, 0),
(6129, 5, 0),
(6129, 6, 40),
(6129, 8, 31),
(6129, 9, 0),
(6129, 10, 30),
(6129, 11, 0),
(6129, 20, 0),
(6129, 21, 0),
(6129, 22, 0),
(6211, 1, 210),
(6211, 2, 50),
(6211, 3, 6),
(6211, 5, 0),
(6211, 6, 210),
(6211, 8, 36),
(6211, 9, 5),
(6211, 10, 16),
(6211, 11, 5),
(6211, 20, 0),
(6211, 21, 10),
(6211, 22, 0),
(6212, 1, 210),
(6212, 2, 45),
(6212, 3, 5),
(6212, 5, 0),
(6212, 6, 75),
(6212, 8, 39),
(6212, 9, 5),
(6212, 10, 16),
(6212, 11, 4),
(6212, 20, 0),
(6212, 21, 10),
(6212, 22, 10),
(6228, 1, 200),
(6228, 2, 35),
(6228, 3, 4),
(6228, 4, 1),
(6228, 5, 5),
(6228, 6, 310),
(6228, 8, 27),
(6228, 10, 25),
(6228, 11, 18),
(6998, 1, 10),
(6998, 3, 0),
(6998, 6, 0),
(6998, 8, 2),
(6998, 10, 2),
(6998, 11, 0),
(6999, 1, 5),
(6999, 3, 0),
(6999, 6, 0),
(6999, 8, 2),
(6999, 10, 0),
(6999, 11, 0),
(7000, 1, 10),
(7000, 3, 0),
(7000, 5, 0),
(7000, 6, 0),
(7000, 8, 2),
(7000, 10, 2),
(7000, 11, 0),
(7001, 1, 5),
(7001, 3, 0),
(7001, 6, 0),
(7001, 8, 2),
(7001, 10, 0),
(7001, 11, 0),
(7002, 1, 10),
(7002, 3, 0),
(7002, 6, 0),
(7002, 8, 2),
(7002, 10, 2),
(7002, 11, 0),
(7003, 1, 10),
(7003, 3, 0),
(7003, 6, 0),
(7003, 8, 2),
(7003, 10, 2),
(7003, 11, 0),
(7004, 3, 0),
(7004, 6, 0),
(7004, 8, 1),
(7004, 10, 0),
(7004, 11, 0),
(7005, 1, 10),
(7005, 3, 0),
(7005, 6, 0),
(7005, 8, 2),
(7005, 10, 2),
(7005, 11, 0),
(7006, 3, 0),
(7006, 6, 0),
(7006, 8, 1),
(7006, 10, 0),
(7006, 11, 0),
(7007, 1, 200),
(7007, 3, 5),
(7007, 5, 12.5),
(7007, 6, 25),
(7007, 7, 17),
(7008, 1, 300),
(7008, 6, 10),
(7009, 1, 275),
(7009, 10, 100),
(7010, 3, 15),
(7011, 1, 410),
(7011, 19, 25),
(7012, 3, 17.5),
(7012, 7, 25),
(7013, 1, 175),
(7013, 3, 6),
(7013, 14, 23),
(7013, 19, 12);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
