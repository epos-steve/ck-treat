create table db_public(
	id int key not null auto_increment,
	groupname varchar(30) not null
);

create or replace view vDbGroup as
	select
		cast(db_group_graphs.groupid as char) as vGroupId,
		pub_db_view.*,
		db_group_graphs.dbSide as dbSide,
		db_group_graphs.dbOrder as dbOrder,
		db_group_graphs.dbHeight as dbHeight,
		db_cache.cacheId as cacheId,
		db_cache.cacheDate as cacheDate,
		case when db_public.groupname is not null
			then 1
			else 0
		end as dbPublic
	from pub_db_view
	inner join db_group_graphs using( viewid )
	left join db_cache using( viewid )
	left join db_group using( groupid )
	left join db_public using( groupname );
