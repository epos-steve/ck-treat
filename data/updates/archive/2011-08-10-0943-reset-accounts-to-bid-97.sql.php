#!/usr/bin/env php
<?php
# This resets the other business_id Accounts to the same as
# business_id 97 as Treat tends to get KC set up & then complains
# that Omaha & Indy aren't exactly the same as KC

require_once( dirname(__FILE__) . '/../../application/bootstrap.php' );

$dbh = Treat_DB::singleton();

$co_97 = array();
$sql = '
	SELECT vgla1.*
	FROM vend_gl_accounts vgla1
	WHERE vgla1.account_id = 0
		AND vgla1.number IS NULL
		AND vgla1.business_id = 97
	ORDER BY vgla1.business_id
		,vgla1.account_group_id
		,vgla1.sort_order
		,vgla1.name
';

$stmt = $dbh->prepare( $sql );
if ( $stmt->execute() ) {
	$tmp = $stmt->fetchAll( PDO::FETCH_CLASS, 'Treat_ArrayObject' );
	foreach ( $tmp as $account ) {
		$co_97[ $account->name ] = $account;
	}
}
if ( !$co_97 ) {
	exit( __LINE__ );
}

$sql = '
	SELECT vgla1.*
	FROM vend_gl_accounts vgla1
		JOIN vend_gl_accounts vgla2
			ON vgla1.account_id = vgla2.account_id
			AND vgla1.number IS NULL
			AND vgla2.number IS NULL
			AND vgla1.name = vgla2.name
			AND vgla1.business_id != 97
			AND vgla2.business_id = 97
	WHERE
		vgla1.is_deleted != vgla2.is_deleted
		OR vgla1.account_type_id != vgla2.account_type_id
		OR vgla1.sort_order != vgla2.sort_order
	ORDER BY vgla1.business_id
		,vgla1.account_group_id
		,vgla1.sort_order
		,vgla1.name
';

$stmt = $dbh->prepare( $sql );
if ( $stmt->execute() ) {
	$need_updates = $stmt->fetchAll( PDO::FETCH_CLASS, 'Treat_ArrayObject' );
}
if ( !$need_updates ) {
	exit( __LINE__ );
}

$sql_update = '
	UPDATE vend_gl_accounts
	SET %s
	WHERE id = :id
';
foreach ( $need_updates as $account ) {
	$fields = array();
	$params = array(
		'id' => $account->id,
	);
	if ( array_key_exists( $account->name, $co_97 ) ) {
		$tmp = $co_97[ $account->name ];
		
		if ( $tmp->is_deleted != $account->is_deleted ) {
			$fields[] = 'is_deleted = :is_deleted';
			$params[ 'is_deleted' ] = $tmp->is_deleted;
		}
		
		if ( $tmp->account_type_id != $account->account_type_id ) {
			$fields[] = 'account_type_id = :account_type_id';
			$params[ 'account_type_id' ] = $tmp->account_type_id;
		}
		
		if ( $tmp->sort_order != $account->sort_order ) {
			$fields[] = 'sort_order = :sort_order';
			$params[ 'sort_order' ] = $tmp->sort_order;
		}
		
		$sql = sprintf(
			$sql_update
			,implode( ', ', $fields )
		);
		
		$update = $dbh->prepare( $sql );
		$update->execute( $params );
		echo '.';
	}
	else {
		echo '*** $account->name doesn\'t exist in $co_97', PHP_EOL;
		var_dump( $account );
	}
}

echo PHP_EOL;


