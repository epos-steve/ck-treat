#!/usr/bin/env php
<?php
# This resets the other business_id Accounts to the same as
# business_id 97 as Treat tends to get KC set up & then complains
# that Omaha & Indy aren't exactly the same as KC

require_once( dirname(__FILE__) . '/../../application/bootstrap.php' );

$dbh = Treat_DB::singleton();

// TODO change location of the csv you're importing
$FILE = dirname( __FILE__ ) . '/../csv/sandwich_nutrition.csv';

// TODO change the company id
$company_id = 19;

// TODO make sure the column headers are in correct order as in the csv
// and translated to the correct database field
$headers_to_fields = array(
	'Item Number' => 'item_code',
	'Iem Description' => 'name',
	'Gram Weight (g)' => '',
	'Calories (kcal)' => 'calories',
	'Calories from Fat (kcal)' => 'cal_from_fat',
	'Calories from SatFat (kcal)' => '',
	'Protein (g)' => 'protein',
	'Carbohydrates (g)' => 'complex_cabs',
	'Dietary Fiber (g)' => 'fiber',
	'Soluble Fiber (g)' => '',
	'Total Sugars (g)' => 'sugar',
	'Monosaccharides (g)' => '',
	'Disaccharides (g)' => '',
	'Other Carbs (g)' => '',
	'Fat (g)' => 'total_fat',
	'Saturated Fat (g)' => 'sat_fat',
	'Mono Fat (g)' => '',
	'Poly Fat (g)' => '',
	'Trans Fatty Acid (g)' => '',
	'Cholesterol (mg)' => 'cholesterol',
	'Water (g)' => '',
	'Vitamin A - IU (IU)' => '',
	'Vitamin A - RE (RE)' => '',
	'Vitamin A - RAE (RAE)' => '',
	'Carotenoid RE (RE)' => '',
	'Retinol RE (RE)' => '',
	'Beta-Carotene (mcg)' => '',
	'Vitamin B1 (mg)' => 'thiamine',
	'Vitamin B2 (mg)' => 'riboflavin',
	'Vitamin B3 (mg)' => 'niacin',
	'Vitamin B3 - Niacin Equiv (mg)' => '',
	'Vitamin B6 (mg)' => '',
	'Vitamin B12 (mcg)' => '',
	'Biotin (mcg)' => '',
	'Vitamin C (mg)' => 'vit_C',
	'Vitamin D - IU (IU)' => '',
	'Vitamin D - mcg (mcg)' => '',
	'Vitamin E - Alpha-Toco (mg)' => '',
	'Folate (mcg)' => '',
	'Folate, DFE (mcg)' => '',
	'Vitamin K (mcg)' => '',
	'Pantothenic Acid (mg)' => '',
	'Calcium (mg)' => 'calcium',
	'Chromium (mcg)' => '',
	'Copper (mg)' => '',
	'Fluoride (mg)' => '',
	'Iodine (mcg)' => '',
	'Iron (mg)' => 'iron',
	'Magnesium (mg)' => 'magnesium',
	'Manganese (mg)' => '',
	'Molybdenum (mcg)' => '',
	'Phosphorus (mg)' => 'phosphorus',
	'Potassium (mg)' => 'potassium',
	'Selenium (mcg)' => '',
	'Sodium (mg)' => 'sodium',
	'Zinc (mg)' => 'zinc',
	'Omega 3 Fatty Acid (g)' => '',
	'Omega 6 Fatty Acid (g)' => '',
	'Alcohol (g)' => '',
	'Caffeine (mg)' => '',
	'Choline (mg)' => '',
);

$conversions = array(
	'calories',
	'cal_from_fat',
);

$all_data = array();
$bad_data = array();
$counter = 0;
if ( false !== ( $handle = fopen( $FILE, 'r' ) ) ) {
	Treat_Model_Nutrition_TypesObject::loadTypes(
		Treat_Model_Nutrition_Types::getTypes( true )
	);
	$nutrition_types = Treat_Model_Nutrition_TypesObject::getTypes();
	while ( false !== ( $data = fgetcsv( $handle ) ) ) {
		$mic_exists = true;
		if ( $counter ) { # need to ignore 1st line which is a header
			$nutrition_data = array_combine( $headers_to_fields, $data );
			$nutrition_data = new Treat_ArrayObject( $nutrition_data );
			$all_data[] = $nutrition_data;
			if ( $nutrition_data->item_code ) {
				$mic = Treat_Model_Menu_Item_Company::getByCompanyIdAndItemCode(
					$company_id
					,(string)$nutrition_data->item_code
					,$class = null
				);
				if ( !$mic ) {
					$mic_exists = false;
					$mic = new Treat_Model_Menu_Item_Company();
					$mic->company_id = $company_id;
					$mic->item_code  = $nutrition_data->item_code;
				}
				$mic->name = $nutrition_data->name;
				$mic->save();
				if ( !$mic_exists ) {
					$mic = Treat_Model_Menu_Item_Company::getByCompanyIdAndItemCode(
						$company_id
						,(string)$nutrition_data->item_code
						,$class = null
					);
				}
				if ( !$mic ) {
					$bad_data[] = array(
						'error' => 'error processing data',
						'data' => var_export( $nutrition_data, true ),
					);
					# skip the rest of this
					continue;
				}
				
				/**
				 *	Adding the nutrition_types to the cross-reference
				 *	table for this particular $nutrition_data
				 */
				foreach ( $nutrition_types as $nutrition_type ) {
					if ( $nutrition_data->{$nutrition_type->column_key} ) {
						switch ( $nutrition_type->column_key ) {
							# kilocalories - convert to grams
							case 'calories':
							case 'cal_from_fat':
								$nutrition_data->{$nutrition_type->column_key} =
									$nutrition_data->{$nutrition_type->column_key}
									/ 9
								;
						}
						$tmp = new Treat_Model_Nutrition_Xref_Company();
						$tmp->mic_id = $mic->id;
						$tmp->nutrition_type_id = $nutrition_type->nutritionid;
						$tmp->value = $nutrition_data->{$nutrition_type->column_key};
						$tmp->save();
					}
				}
				
				/**
				 *	Update all of the menu_items_new items
				 */
				$menu_items_new = Treat_Model_Menu_Item_New::getByCompanyIdAndItemCode(
					$company_id
					,(string)$nutrition_data->item_code
				);
				foreach ( $menu_items_new as $item ) {
					$item->mic_id = $mic->id;
					$item->save();
				}
			}
			else {
				$bad_data[] = array(
					'error' => 'data has no item_code',
					'data' => var_export( $nutrition_data, true ),
				);
			}
		}
		$counter++;
	}
	fclose( $handle );
}
else {
	throw new Exception( 'Error loading file' );
}

echo '$all_data = ';
var_dump( $all_data );

echo '******************************';
echo 'ERRORS: ';
var_dump( $bad_data );

