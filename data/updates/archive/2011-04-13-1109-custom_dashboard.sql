drop table if exists pub_db_view;
create table pub_db_view like db_view;
drop table if exists pub_db_custom;
create table pub_db_custom like db_custom;
drop table if exists pub_db_what;
create table pub_db_what like db_what;
drop table if exists pub_db_who;
create table pub_db_who like db_who;
drop table if exists db_group;
create table db_group(
	groupid int unsigned not null auto_increment key,
	groupname varchar(30) not null default '',
	groupowner int unsigned not null default 0
);
drop table if exists db_group_members;
create table db_group_members(
	groupid int unsigned not null,
	loginid int unsigned not null,
	primary key (groupid, loginid)
);
drop table if exists db_group_graphs;
create table db_group_graphs(
	viewid int unsigned not null,
	groupid int unsigned not null,
	primary key (viewid, groupid)
);
alter table db_view add column db_mtime timestamp default current_timestamp on update current_timestamp;
alter table pub_db_view add column db_mtime timestamp not null;
alter table db_group_graphs add column dbSide smallint default 1;
alter table db_group_graphs add column dbHeight int default 250;
alter table db_group_graphs add column dbOrder int;
drop table if exists db_view_order;
create table db_view_order(
	viewid int unsigned not null,
	loginid int unsigned not null,
	dbOrder int,
	dbSide int,
	dbHeight int,
	primary key( viewid, loginid )
);
insert into db_view_order( viewid, loginid, dbOrder, dbSide, dbHeight )
	select 
		viewid, loginid, db_order, size, 250
	from db_view;
create or replace view vDbGroupMembersOwners as
	select 
		groupid as groupid,
		loginid as loginid
	from db_group_members
	union
	select
		groupid as groupid,
		groupowner as loginid
	from db_group;
create or replace view vDbGroupList as
	(
		select
			cast("custom" as char) as groupid,
			cast("My Charts" as char) as groupname,
			login.loginid as loginid,
			cast("custom" as char) as groupowner,
			1 as floater
		from login
		inner join security on login.security = security.securityid and security.dashboard = 3
	) union ( 
		select 
			cast(db_group.groupid as char) as groupid,
			db_group.groupname as groupname,
			vDbGroupMembersOwners.loginid as loginid,
			cast(db_group.groupowner as char) as groupowner,
			0 as floater
		from db_group
		inner join vDbGroupMembersOwners using( groupid )
	)
	order by floater desc,groupname;
create or replace view vDbGroup as
	select
		cast(db_group_graphs.groupid as char) as vGroupId,
		pub_db_view.*,
		db_group_graphs.dbSide as dbSide,
		db_group_graphs.dbOrder as dbOrder,
		db_group_graphs.dbHeight as dbHeight
	from pub_db_view
	inner join db_group_graphs using( viewid );
create or replace view vDbCustom as
	select 
		cast("custom" as char) as vGroupId,
		db_view.*,
		UNIX_TIMESTAMP( db_view.db_mtime ) as mtime,
		UNIX_TIMESTAMP( pub_db_view.db_mtime ) as ptime,
		db_view_order.dbSide as dbSide,
		db_view_order.dbHeight as dbHeight,
		db_view_order.dbOrder as dbOrder
	from db_view
	left join pub_db_view using( viewid )
	left join db_view_order using( viewid );

delimiter $$

drop procedure if exists trigUpdateDbMTime$$
create procedure trigUpdateDbMTime(
	var_viewid int
)
	modifies sql data
begin
	update db_view set db_mtime = NOW() where viewid=var_viewid; 
end$$

delimiter ;

drop trigger if exists mtime_insert_custom;
create trigger mtime_insert_custom 
	after insert on db_custom
	for each row 
		call trigUpdateDbMTime( NEW.viewid );

drop trigger if exists mtime_update_custom;
create trigger mtime_update_custom
	after update on db_custom
	for each row
		call trigUpdateDbMTime( NEW.viewid );

drop trigger if exists mtime_insert_who;
create trigger mtime_insert_who 
	after insert on db_who
	for each row 
		call trigUpdateDbMTime( NEW.viewid );

drop trigger if exists mtime_update_who;
create trigger mtime_update_who
	after update on db_who
	for each row
		call trigUpdateDbMTime( NEW.viewid );

drop trigger if exists mtime_insert_what;
create trigger mtime_insert_what 
	after insert on db_what
	for each row 
		call trigUpdateDbMTime( NEW.viewid );

drop trigger if exists mtime_update_what;
create trigger mtime_update_what
	after update on db_what
	for each row
		call trigUpdateDbMTime( NEW.viewid );

