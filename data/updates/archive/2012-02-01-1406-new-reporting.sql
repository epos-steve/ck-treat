
# add the 2 new reports to scheduled reporting
INSERT INTO `pos_report` (
	`on_demand`
	,`how_often`
	,`class`
	,`slug`
	,`label`
)
VALUES
	(  FALSE, 0, 'Treat_Model_Report_Object_PosWasteTrend',  'waste-trend',  'Waste Trend'  )
;

INSERT INTO `pos_report` (
	`on_demand`
	,`how_often`
	,`class`
	,`slug`
	,`label`
)
VALUES
	(  FALSE, 0, 'Treat_Model_Report_Object_PosShrinkTrend',  'shrink-trend',  'Shrink Trend'  )
;


# also add the 2 new reports to instant run reports
INSERT INTO `pos_report` (
	`on_demand`
	,`how_often`
	,`class`
	,`slug`
	,`label`
)
VALUES
	(  TRUE, 0, 'Treat_Model_Report_Object_PosWasteTrend',  'waste-trend',  'Waste Trend'  )
;

INSERT INTO `pos_report` (
	`on_demand`
	,`how_often`
	,`class`
	,`slug`
	,`label`
)
VALUES
	(  TRUE, 0, 'Treat_Model_Report_Object_PosShrinkTrend',  'shrink-trend',  'Shrink Trend'  )
;



