USE vivipos_order;

DROP TABLE IF EXISTS cashdrawer_records;
CREATE TABLE cashdrawer_records (
	id CHAR(36) PRIMARY KEY  NOT NULL ,
	terminal_no VARCHAR(10),
	drawer_no SMALLINT,
	clerk VARCHAR(32),
	clerk_displayname VARCHAR(32),
	event_type VARCHAR(32),
	status SMALLINT,
	created INTEGER,
	modified INTEGER,
	payment_type VARCHAR(32),
	amount DECIMAL(7,2),
	sequence VARCHAR(16), 
	order_id CHAR(36)
) ENGINE=INNODB;

DROP TABLE IF EXISTS order_receipts;
CREATE TABLE order_receipts (
	id CHAR(36) PRIMARY KEY  NOT NULL ,
	order_id CHAR(36),
	sequence VARCHAR(16),
	device SMALLINT,
	created INTEGER,
	modified INTEGER,
	batch INTEGER
) ENGINE=INNODB;