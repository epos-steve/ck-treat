DROP TABLE IF EXISTS aeris_settings_type;
CREATE TABLE aeris_settings_type (
	registryKey VARCHAR(128) NOT NULL KEY,
	registryType ENUM('int', 'bool', 'string') NOT NULL
) ENGINE=INNODB;

INSERT INTO aeris_settings_type VALUES
	( 'ee.settingsVersion', 'int' ),
	( 'vivipos.fec.settings.layout.ActivityLoggingEnabled', 'bool' ),
	( 'vivipos.fec.settings.layout.aerispos.noReceiptHeader', 'bool' ),
	( 'vivipos.fec.settings.layout.DineInCarryOut', 'bool' ),
	( 'vivipos.fec.settings.layout.FingerprintEnabled', 'bool' ),
	( 'vivipos.fec.settings.layout.generalSystemTimeout', 'int' ),
	( 'vivipos.fec.settings.layout.HideDeptScrollbar', 'bool' ),
	( 'vivipos.fec.settings.DepartmentCols', 'int' ),
	( 'vivipos.fec.settings.DepartmentRows', 'int' ),
	( 'vivipos.fec.settings.layout.MemberOnlyPromos', 'bool' ),
	( 'vivipos.fec.settings.layout.MultilingualEnabled', 'bool' ),
	( 'vivipos.fec.settings.layout.popupTimerCountdown', 'int' ),
	( 'vivipos.fec.settings.layout.RegisterAtLeft', 'bool' ),
	( 'vivipos.fec.settings.layout.SystemTimeoutEnabled', 'bool' ),
	( 'vivipos.fec.settings.layout.traditional.HidePLUScrollbar', 'bool' ),
	( 'vivipos.fec.settings.layout.VouchersEnabled', 'bool' ),
	( 'vivipos.fec.settings.PluCols', 'int' ),
	( 'vivipos.fec.settings.PluRows', 'int' ),
	( 'vivipos.fec.settings.layout.HidePLUScrollbar', 'bool' );
