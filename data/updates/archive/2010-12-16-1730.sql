
INSERT INTO `vend_gl_account_types` (
	`label`
	,`sort_order`
	,`account_class`
)
VALUES
	( 'Routes Labor Calculations', -5, 'Treat_Model_GeneralLedger_Account_RoutesObject' )
;

UPDATE `vend_gl_accounts`
SET `account_type_id` = (
	SELECT id
	FROM `vend_gl_account_types`
	WHERE
		`label` = 'Routes Labor Calculations'
		AND `sort_order` = -5
		AND `account_class` = 'Treat_Model_GeneralLedger_Account_RoutesObject'
)
WHERE `name` = 'Salaries - Routes'
;

