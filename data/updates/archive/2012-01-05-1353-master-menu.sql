CREATE  TABLE `mburris_businesstrack`.`menu_items_master` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `UPC` VARCHAR(45) NOT NULL ,
  `Name` VARCHAR(100) NOT NULL ,
  `Manufacturer` INT UNSIGNED NULL ,
  `Category` INT UNSIGNED NULL ,
  `isCore` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `UPC_UNIQUE` (`UPC` ASC) ,
  INDEX `Manufacturer` (`Manufacturer` ASC) ,
  INDEX `Category` (`Category` ASC) )
ENGINE = InnoDB;

CREATE  TABLE `mburris_businesstrack`.`menu_items_category` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `Name` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;

ALTER TABLE `mburris_businesstrack`.`menu_items_new` DROP COLUMN `manufacturer`;