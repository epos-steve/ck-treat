
INSERT INTO `payroll_code_comm` (
	`companyid`
	,`code_name`
	,`export_name`
)
VALUES
	(  3, 'PTO_DOLLAR', 'PTO' )
	,( 6, 'PTO_DOLLAR', 'PTO' )
	,( 6, 'Holiday', 'Hol' )
;

ALTER TABLE `vend_commission` ADD `pto_dollar_link` INT NOT NULL AFTER `pto_link`;

UPDATE `vend_commission` vc , `payroll_code_comm` pcc
SET vc.`pto_dollar_link` = pcc.`codeid`
WHERE vc.`companyid` = pcc.`companyid`
	AND pcc.`code_name` = 'PTO_DOLLAR'
;

UPDATE `vend_commission` vc , `payroll_code_comm` pcc
SET vc.`holiday_link` = pcc.`codeid`
WHERE vc.`companyid` = pcc.`companyid`
	AND pcc.`code_name` = 'Holiday'
;


