
ALTER TABLE `nutrition_types` ENGINE = InnoDB
;
ALTER TABLE `nutrition_types` ADD `column_key` VARCHAR( 20 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `units`
;

UPDATE `nutrition_types`
SET `column_key` = `name`
;

UPDATE `nutrition_types`
SET `units` = 'gm'
WHERE `units` = 'g'
;

ALTER TABLE `nutrition_types` ADD UNIQUE (
	`column_key`
);

CREATE TABLE `nutrition_xref_types` (
	`item_code` VARCHAR(15) COLLATE utf8_unicode_ci NOT NULL
	,`nutrition_types_id` INT(11) NOT NULL
	,`value` double NOT NULL
	,PRIMARY KEY ( `item_code`, `nutrition_types_id` )
	,INDEX ( `nutrition_types_id`, `value` )
) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci
;





