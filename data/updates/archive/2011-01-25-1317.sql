
# Adding "deleted" accounts
INSERT INTO `vend_gl_accounts` (
	`business_id`
	,`account_group_id`
	,`is_deleted`
	,`sort_order`
	,`name`
)
VALUES
# KC
	(   97, 3, 1, -20, 'Net Sales' )
	,(  97, 3, 1, -20, 'Gross Profit' )
	,(  97, 3, 1, -20, 'Net Gross Profit' )
	,(  97, 3, 1, -20, 'Total Employee Expenses' )
	,(  97, 3, 1, -20, 'Total TOE' )
	,(  97, 3, 1, -20, 'EBITDA' )
# Omaha
	,( 106, 3, 1, -20, 'Net Sales' )
	,( 106, 3, 1, -20, 'Gross Profit' )
	,( 106, 3, 1, -20, 'Net Gross Profit' )
	,( 106, 3, 1, -20, 'Total Employee Expenses' )
	,( 106, 3, 1, -20, 'Total TOE' )
	,( 106, 3, 1, -20, 'EBITDA' )
# Indiana
	,( 151, 3, 1, -20, 'Net Sales' )
	,( 151, 3, 1, -20, 'Gross Profit' )
	,( 151, 3, 1, -20, 'Net Gross Profit' )
	,( 151, 3, 1, -20, 'Total Employee Expenses' )
	,( 151, 3, 1, -20, 'Total TOE' )
	,( 151, 3, 1, -20, 'EBITDA' )


UPDATE `vend_gl_accounts`
SET   `account_group_id` = 3
	,`sort_order` = 20
WHERE `account_group_id` = 0
	AND `account_id` = 0
	AND `number` IS NULL
;

SELECT *
FROM `vend_gl_accounts`
WHERE
	(
		`account_group_id` = 0
		OR `sort_order` = 20
	)
	AND `account_id` = 0
	AND `number` IS NULL
ORDER BY `business_id`
;


