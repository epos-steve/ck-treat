# updating account_group_id = 1

UPDATE `vend_gl_accounts`
SET `sort_order` = -4
WHERE
	`account_group_id` = 1
	AND `account_id` = 0
	AND `number` IS NULL
	AND `name` IN (
		'Des Moines CSV Sales'
		,'Des Moines Full Service Sales'
		,'Greencastle Full Service Sales'
		,'Greensboro Full Service Sales'
		,'Lafayette Full Service Sales'
	)
;




# updating account_group_id = 2

UPDATE `vend_gl_accounts`
SET `sort_order` = -8
WHERE
	`account_group_id` = 2
	AND `account_id` = 0
	AND `number` IS NULL
	AND `name` = 'Salaries - Vending Admin'
;

UPDATE `vend_gl_accounts`
SET `sort_order` = -7
WHERE
	`account_group_id` = 2
	AND `account_id` = 0
	AND `number` IS NULL
	AND `name` IN (
		'Salaries - Sales & Cust Service'
		,'Salaries - Service'
		,'Salaries - Warehouse'
	)
;

UPDATE `vend_gl_accounts`
SET `sort_order` = -6
WHERE
	`account_group_id` = 2
	AND `account_id` = 0
	AND `number` IS NULL
	AND `name` IN (
		'Salaries - Route Mgr. / Supvsr'
		,'Salaries - Routes'
	)
;

UPDATE `vend_gl_accounts`
SET `sort_order` = -5
WHERE
	`account_group_id` = 2
	AND `account_id` = 0
	AND `number` IS NULL
	AND `name` = 'Salaries - Bldg / Vehicle Maint'
;

UPDATE `vend_gl_accounts`
SET `sort_order` = 10
WHERE
	`account_group_id` = 2
	AND `account_id` = 0
	AND `number` IS NULL
	AND `name` = 'Payroll Taxes'
;

UPDATE `vend_gl_accounts`
SET `sort_order` = 11
WHERE
	`account_group_id` = 2
	AND `account_id` = 0
	AND `number` IS NULL
	AND `name` = 'Group Insurance'
;

UPDATE `vend_gl_accounts`
SET `sort_order` = 12
WHERE
	`account_group_id` = 2
	AND `account_id` = 0
	AND `number` IS NULL
	AND `name` = '401K Expense'
;

UPDATE `vend_gl_accounts`
SET `sort_order` = 13
WHERE
	`account_group_id` = 2
	AND `account_id` = 0
	AND `number` IS NULL
	AND `name` = 'HR/Recruiting Expense'
;

UPDATE `vend_gl_accounts`
SET `sort_order` = 14
WHERE
	`account_group_id` = 2
	AND `account_id` = 0
	AND `number` IS NULL
	AND `name` = 'Employee Expenses'
;




# updating account_group_id = 3

UPDATE `vend_gl_accounts`
SET `sort_order` = -11
WHERE
	`account_group_id` = 3
	AND `account_id` = 0
	AND `number` IS NULL
	AND `name` = 'Selling Expense'
;

UPDATE `vend_gl_accounts`
SET `sort_order` = -10
WHERE
	`account_group_id` = 3
	AND `account_id` = 0
	AND `number` IS NULL
	AND `name` IN (
		'Advertising'
		,'Vehicle Expense'
		,'Vehicle Repair & Maint'
	)
;

UPDATE `vend_gl_accounts`
SET `sort_order` = -9
WHERE
	`account_group_id` = 3
	AND `account_id` = 0
	AND `number` IS NULL
	AND `name` = 'Rent Office & Whse'
;

UPDATE `vend_gl_accounts`
SET `sort_order` = -8
WHERE
	`account_group_id` = 3
	AND `account_id` = 0
	AND `number` IS NULL
	AND `name` IN (
		'Rent - Equipment'
		,'Utilities'
	)
;

UPDATE `vend_gl_accounts`
SET `sort_order` = -7
WHERE
	`account_group_id` = 3
	AND `account_id` = 0
	AND `number` IS NULL
	AND `name` = 'Repair & Maint O&W'
;

UPDATE `vend_gl_accounts`
SET `sort_order` = -6
WHERE
	`account_group_id` = 3
	AND `account_id` = 0
	AND `number` IS NULL
	AND `name` IN (
		'Equipment Expense'
		,'Office Supplies'
	)
;

UPDATE `vend_gl_accounts`
SET `sort_order` = -5
WHERE
	`account_group_id` = 3
	AND `account_id` = 0
	AND `number` IS NULL
	AND `name` IN (
		'Cantaloupe Service Fees'
		,'Insurance - General'
		,'Professional Fees'
		,'Telephone'
	)
;

UPDATE `vend_gl_accounts`
SET `sort_order` = -4
WHERE
	`account_group_id` = 3
	AND `account_id` = 0
	AND `number` IS NULL
	AND `name` = 'Cont/Sub/Travel/Donations'
;

UPDATE `vend_gl_accounts`
SET `sort_order` = -3
WHERE
	`account_group_id` = 3
	AND `account_id` = 0
	AND `number` IS NULL
	AND `name` IN (
		'Comp/Office Support/Training'
		,'Prov for Uncol Accts'
		,'Taxes & Licenses'
		,'Uniform Expense'
	)
;

UPDATE `vend_gl_accounts`
SET `sort_order` = -2
WHERE
	`account_group_id` = 3
	AND `account_id` = 0
	AND `number` IS NULL
	AND `name` IN (
		'Loss on Changers / MR'
		,'Other Admin Exp'
	)
;




