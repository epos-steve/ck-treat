
UPDATE `vend_gl_accounts`
SET `sort_order` = -10
WHERE `number` IS NULL
	AND `name` = 'Full Service Sales'
;


INSERT INTO `vend_gl_accounts` (
#	`id`
	`business_id`
	,`account_group_id`
	,`account_type_id`
	,`sort_order`
	,`name`
)
VALUES
	(   97, 1, 2, -7, 'CK Sales' )
	,( 106, 1, 2, -7, 'CK Sales' )
#	,( 151, 1, 2, -7, 'CK Sales' )
;


SET @kc_account_id = 0;
SET @omaha_account_id = 0;


SELECT `id`
INTO @kc_account_id
FROM `vend_gl_accounts`
WHERE `number` IS NULL
	AND `business_id` = 97
	AND `name` = 'CK Sales'
;


SELECT `id`
INTO @omaha_account_id
FROM `vend_gl_accounts`
WHERE `number` IS NULL
	AND `business_id` = 106
	AND `name` = 'CK Sales'
;


INSERT INTO `vend_gl_account_xhrefs` (
	`account_id`
	,`business_id`
	,`foreign_id`
	,`table_name`
	,`name`
)
VALUES
	(  @kc_account_id, 97, 2046, 'creditdetail', 'CK Sales' )
	,( @kc_account_id, 97, 2047, 'creditdetail', 'CK Sales' )
	,( @kc_account_id, 97, 2048, 'creditdetail', 'CK Sales' )
	,( @omaha_account_id, 106, 2003, 'creditdetail', 'CK Sales' )
	,( @omaha_account_id, 106, 2004, 'creditdetail', 'CK Sales' )
	,( @omaha_account_id, 106, 2005, 'creditdetail', 'CK Sales' )



