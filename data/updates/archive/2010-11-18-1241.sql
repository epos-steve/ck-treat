# $Id: 2010-11-18-1241.sql 4476 2010-11-18 22:53:30Z patrick $

ALTER TABLE `vend_gl_account_types` ADD `account_class` VARCHAR(255) DEFAULT '' COMMENT 'Class to instantiate for this account';

UPDATE `vend_gl_account_types`
SET `account_class` = 'Treat_Model_GeneralLedger_Account_ImportObject'
	,`label` = 'Imported by a CSV file'
WHERE `id` = 1;

UPDATE `vend_gl_account_types`
SET `account_class` = 'Treat_Model_GeneralLedger_Account_SalesObject'
	,`label` = 'Sales Calculations'
WHERE `id` = 2;

UPDATE `vend_gl_accounts`
SET `account_type_id` = 2
WHERE `id` IN ( 1, 2 );

INSERT INTO `vend_gl_account_types` (
	`id`
	,`account_class`
	,`label`
)
VALUES
	(  3, 'Treat_Model_GeneralLedger_Account_LaborObject', 'Labor Calculations' )
;

