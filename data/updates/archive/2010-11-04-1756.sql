
CREATE TEMPORARY TABLE parent_ids
SELECT b.id
FROM `vend_gl_accounts` a
	JOIN `vend_gl_accounts` b
		ON a.account_id = b.id
GROUP BY b.id
;

UPDATE `vend_gl_accounts`
SET `account_type_id` = 1
WHERE id IN (
	SELECT *
	FROM parent_ids
);

#SELECT *
#FROM vend_gl_accounts vgla
#WHERE vgla.id IN (
#	SELECT id
#	FROM parent_ids
#);

