
CREATE TABLE `nutrition_xref_master` (
	`mim_id` int(10) unsigned NOT NULL COMMENT 'menu_item_master.id'
	,`nutrition_type_id` int(3) unsigned NOT NULL COMMENT 'nutrition_types.nutritionid'
	,`value` double NOT NULL
	,PRIMARY KEY ( `mim_id`, `nutrition_type_id` )
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci
	COMMENT='crossreference between menu_item_master and nutrition_type'
;

