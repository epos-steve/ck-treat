update security set dashboard=4 where dashboard=3;
create or replace view vDbGroupList as
	(
		select
			cast("custom" as char) as groupid,
			cast("My Charts" as char) as groupname,
			login.loginid as loginid,
			cast("custom" as char) as groupowner,
			1 as floater
		from login
		inner join security on login.security = security.securityid and security.dashboard = 4
	) union (
		select
			cast(db_group.groupid as char) as groupid,
			db_group.groupname as groupname,
			vDbGroupMembersOwners.loginid as loginid,
			cast(db_group.groupowner as char) as groupowner,
			0 as floater
		from db_group
		inner join vDbGroupMembersOwners using( groupid )
	)
	order by floater desc,groupname
