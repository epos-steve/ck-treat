
ALTER TABLE `customer` ENGINE = InnoDB DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;

ALTER TABLE `customer` CHANGE `customid` `customid` VARCHAR( 25 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;

ALTER TABLE `customer` ADD `birthday` DATE NULL DEFAULT NULL AFTER `last_name`;
ALTER TABLE `customer` ADD `gender` TINYINT NULL DEFAULT NULL COMMENT '1 = male, 2 = female' AFTER `birthday`;
ALTER TABLE `customer` ADD `notify_via_email` BOOLEAN NULL DEFAULT NULL;
ALTER TABLE `customer` ADD `notify_via_sms` BOOLEAN NULL DEFAULT NULL;
ALTER TABLE `customer` ADD `notify_sms_number` VARCHAR( 14 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;

ALTER TABLE `customer_scancodes` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;

ALTER TABLE `customer_scancodes` CHANGE `scan_start` `scan_start` VARCHAR( 20 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;

ALTER TABLE `customer_scancodes` CHANGE `scan_end` `scan_end` VARCHAR( 20 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;

ALTER TABLE `customer_scancodes` ADD INDEX ( `scan_start` , `scan_end` );


