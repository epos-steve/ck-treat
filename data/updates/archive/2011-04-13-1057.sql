
INSERT INTO `vend_gl_account_types` (
	`label`
	,`sort_order`
	,`account_class`
)
VALUES
	(  'Cashless Account', '-4', 'Treat_Model_GeneralLedger_Account_CashlessObject' )
;


SELECT @account_type_id := `id`
FROM `vend_gl_account_types`
WHERE `account_class` = 'Treat_Model_GeneralLedger_Account_CashlessObject'
;


INSERT INTO `vend_gl_accounts` (
	`business_id`
	,`account_group_id`
	,`name`
	,`account_type_id`
	,`sort_order`
#	,`creditid`
#	,`multiplier`
)
VALUES
	(   97, 1, 'Cashless', @account_type_id, -4 )
	,( 106, 1, 'Cashless', @account_type_id, -4 )
	,( 151, 1, 'Cashless', @account_type_id, -4 )
;


