alter table promotions add column promo_reserve tinyint(1) not null default 0;

create or replace view vPromoVivipos as
	select
		promotions.businessid,
		promotions.id as promo_id,
		promo_target.id as promo_target,
		promotions.name as vivipos_name,
		promotions.pos_id,
		promotions.value,
		promotions.rule_order,
		promotions.promo_reserve,
		promotions.promo_discount,
		promotions.promo_discount_type,
		promotions.promo_discount_n,
		promotions.promo_discount_limit,
		promotions.promo_trigger_amount_limit,
		promotions.promo_trigger_amount_type,
		promotions.promo_trigger_amount,
		promotions.promo_trigger_amount_2,
		promotions.promo_trigger_amount_3,
		COALESCE( CAST( menu_tax.pos_id AS char ), '' ) as vivipos_taxno,
		COALESCE( menu_tax.name, '' ) as vivipos_taxname,
		UNIX_TIMESTAMP( promotions.start_date ) as vivipos_startdate,
		UNIX_TIMESTAMP( CONCAT( promotions.start_date, ' ', promotions.start_time ) ) as vivipos_starttime,
		UNIX_TIMESTAMP( promotions.end_date ) as vivipos_enddate,
		UNIX_TIMESTAMP( CONCAT( promotions.start_date, ' ', promotions.end_time ) ) as vivipos_endtime,
		GROUP_CONCAT( vPromoDays.day ORDER BY vPromoDays.day ) as vivipos_days,
		promo_type.vivipos_name as vivipos_type,
		promo_trigger.vivipos_name as vivipos_trigger,
		promo_target.db_table as promotarget_table,
		promo_target.order_by as promotarget_order,
		UNIX_TIMESTAMP( NOW() ) as vivipos_modified,
		promotions.activeFlag as vivipos_active
	from promotions
	join promo_type on promotions.type=promo_type.id
	join promo_trigger on promotions.promo_trigger=promo_trigger.id
	join promo_target on promo_trigger.target_id=promo_target.id
	left join menu_tax on promotions.tax_id = menu_tax.id
	left join vPromoDays on promotions.id=vPromoDays.id
	group by promotions.id;