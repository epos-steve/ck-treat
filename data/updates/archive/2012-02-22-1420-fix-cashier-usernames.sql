CREATE OR REPLACE VIEW vCashierUsers AS
	SELECT 
		LOWER( COALESCE( 
			NULLIF( login.username, '' ), 
			CONCAT( LEFT( TRIM( login.firstname ), 1 ), TRIM( login.lastname ) )
		) ) AS username,
		login_cashier.passcode AS password,
		CONCAT( TRIM( lastname ), ', ', TRIM( firstname ) ) AS description,
		login_cashier_link.groupName AS groupId,
		login_cashier_link.businessid AS businessid
	FROM login
	JOIN login_cashier USING( loginid )
	JOIN login_cashier_link USING( loginid );
