
ALTER TABLE `pos_report` ADD `on_demand` BOOLEAN NOT NULL DEFAULT FALSE AFTER `label`;

ALTER TABLE `pos_report` ADD `how_often` TINYINT UNSIGNED NOT NULL COMMENT 'how_often flag for on_demand reports, otherwise use how_often in the pos_report_runlist table' AFTER `on_demand`;

ALTER TABLE pos_report_cache DROP PRIMARY KEY;

ALTER TABLE `pos_report_cache` ADD `report_cache_id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;

ALTER TABLE `pos_report_cache` ADD UNIQUE `report_by_company` (
	`report_id`
	,`company_id`
	,`how_often`
	,`date_run`
);

ALTER TABLE `pos_report_cache` ADD `day_of_week` ENUM( "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" ) AFTER `how_often`;


UPDATE `pos_report`
SET `slug` = 'cash-out'
	,`on_demand` = TRUE
WHERE `slug` IN ( 'cash_out', 'cash-out' )
;

DELETE FROM `pos_report_cache`
WHERE `report_id` = (
	SELECT `report_id`
	FROM `pos_report`
	WHERE `slug` IN ( 'cash_in', 'cash-in' )
)
;

DELETE FROM `pos_report_runlist`
WHERE `report_id` = (
	SELECT `report_id`
	FROM `pos_report`
	WHERE `slug` IN ( 'cash_in', 'cash-in' )
)
;

UPDATE `pos_report`
SET `slug` = 'cash-in'
	,`on_demand` = TRUE
	,`how_often` = 1
	,`label` = 'Cash In - Weekly'
WHERE `slug` IN ( 'cash_in', 'cash-in' )
;

INSERT INTO `pos_report` (
	`on_demand`
	,`how_often`
	,`class`
	,`slug`
	,`label`
)
VALUES
	(  TRUE, 2, 'Treat_Model_Report_Object_PosCashIn',  'cash-in',  'Cash In - Monthly'  )
;



