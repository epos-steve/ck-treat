alter table promotions add column activeFlag tinyint(1) not null default 1;
alter table promotions add column rule_order smallint;
alter table promotions add column pos_processed tinyint(1) not null default 0;

-- update promo lookup tables

-- MySQL dump 10.13  Distrib 5.1.54, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: mburris_businesstrack
-- ------------------------------------------------------
-- Server version	5.1.54-1ubuntu4

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `promo_target`
--

DROP TABLE IF EXISTS `promo_target`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `promo_target` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `db_table` varchar(50) NOT NULL,
  `order_by` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `promo_target`
--

LOCK TABLES `promo_target` WRITE;
/*!40000 ALTER TABLE `promo_target` DISABLE KEYS */;
INSERT INTO `promo_target` VALUES (1,'Menu Item(s)','menu_items_new','group_id,name'),(2,'Menu Group(s)','menu_groups_new','name');
/*!40000 ALTER TABLE `promo_target` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `promo_trigger`
--

DROP TABLE IF EXISTS `promo_trigger`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `promo_trigger` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `vivipos_name` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `promo_trigger`
--

LOCK TABLES `promo_trigger` WRITE;
/*!40000 ALTER TABLE `promo_trigger` DISABLE KEYS */;
INSERT INTO `promo_trigger` VALUES (1,'Item/Group','individual_plu'),(2,'Combo','individual_dept');
/*!40000 ALTER TABLE `promo_trigger` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `promo_type`
--

DROP TABLE IF EXISTS `promo_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `promo_type` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(35) NOT NULL,
  `vivipos_name` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `promo_type`
--

LOCK TABLES `promo_type` WRITE;
/*!40000 ALTER TABLE `promo_type` DISABLE KEYS */;
INSERT INTO `promo_type` VALUES (1,'Percent','percentage_off'),(2,'Amount Off','amount_off');
/*!40000 ALTER TABLE `promo_type` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2011-05-05 15:38:54

create or replace view vPromoDays as
	select 0 as day,id from promotions pm where pm.monday=1
	union
	select 1 as day,id from promotions pt where pt.tuesday=1
	union
	select 2 as day,id from promotions pw where pw.wednesday=1
	union
	select 3 as day,id from promotions pth where pth.thursday=1
	union
	select 4 as day,id from promotions pf where pf.friday=1
	union
	select 5 as day,id from promotions ps where ps.saturday=1
	union
	select 6 as day,id from promotions psu where psu.sunday=1;

create or replace view vPromoVivipos as
	select 
		promotions.businessid,
		promotions.id as promo_id,
		promotions.target as promo_target,
		promotions.name as vivipos_name,
		promotions.pos_id,
		promotions.value,
		promotions.rule_order,
		UNIX_TIMESTAMP( promotions.start_date ) as vivipos_startdate,
		UNIX_TIMESTAMP( CONCAT( promotions.start_date, ' ', promotions.start_time ) ) as vivipos_starttime,
		UNIX_TIMESTAMP( promotions.end_date ) as vivipos_enddate,
		UNIX_TIMESTAMP( CONCAT( promotions.start_date, ' ', promotions.end_time ) ) as vivipos_endtime,
		GROUP_CONCAT( vPromoDays.day ORDER BY vPromoDays.day ) as vivipos_days,
		promo_type.vivipos_name as vivipos_type,
		promo_trigger.vivipos_name as vivipos_trigger,
		promo_target.db_table as promotarget_table,
		promo_target.order_by as promotarget_order,
		UNIX_TIMESTAMP( NOW() ) as vivipos_modified,
		promotions.activeFlag as vivipos_active
	from promotions
	join promo_type on promotions.type=promo_type.id
	join promo_trigger on promotions.promo_trigger=promo_trigger.id
	join promo_target on promotions.target=promo_target.id
	left join vPromoDays on promotions.id=vPromoDays.id
	group by promotions.id;

