drop table if exists smartMenus;

drop table if exists smartMenuTypes;

drop table if exists smartMenuPackageLinks;
create table smartMenuPackageLinks(
	id int unsigned key not null auto_increment,
	boardGuid char(32) not null,
	packageFile char(32) not null
);
create unique index boardGuid_packageFile_index on smartMenuPackageLinks ( boardGuid, packageFile );

drop table if exists smartMenuPackages;
create table smartMenuPackages(
	id int unsigned key not null auto_increment,
	packageFile char(32) not null,
	companyid int unsigned not null,
	createdTime timestamp default current_timestamp,
	modifiedTime timestamp,
	createdBy int unsigned not null,
	modifiedBy int unsigned not null
);
create unique index packageFile_index on smartMenuPackages ( packageFile );

drop trigger if exists smartMenuPackages_setTimestamp;
drop trigger if exists smartMenuPackages_updateTimestamp;
delimiter $$
create trigger smartMenuPackages_setTimestamp
	before insert on smartMenuPackages
	for each row begin
		set NEW.modifiedTime = NEW.createdTime;
	end $$

create trigger smartMenuPackages_updateTimestamp
	before update on smartMenuPackages
	for each row begin
		set NEW.modifiedTime = current_timestamp;
	end $$
delimiter ;

drop table if exists smartMenuBoards;
create table smartMenuBoards(
	id int unsigned key not null auto_increment,
	boardGuid char(32) not null,
	boardName varchar(64) not null default 'board',
	locationGuid char(32) not null,
	authId int unsigned not null,
	currentPackage int unsigned default null,
	activeFlag tinyint(1) not null default '1'
);
create unique index boardGuid_index on smartMenuBoards( boardGuid );

drop table if exists smartMenuLocations;
create table smartMenuLocations(
	id int unsigned key not null auto_increment,
	locationGuid char(32) not null,
	businessid int unsigned not null
);
create unique index locationGuid_index on smartMenuLocations( locationGuid );

drop table if exists smartMenuClients;
create table smartMenuClients(
	id int unsigned key not null auto_increment,
	businessid int unsigned not null,
	clientName varchar(64) not null default 'client',
	authId int unsigned not null,
	activeFlag tinyint(1) not null default '1'
);
create unique index businessid_clientName_index on smartMenuClients( businessid, clientName );

drop table if exists smartMenuAuth;
create table smartMenuAuth(
	id int unsigned key not null auto_increment,
	loginKey char(30) not null,
	loginSecret char(30) not null,
	status smallint not null default 0
);
create unique index loginKey_index on smartMenuAuth( loginKey );

drop table if exists smartMenuMenuGroups;
create table smartMenuMenuGroups(
	id int unsigned key not null auto_increment,
	businessid int unsigned not null,
	groupName varchar(64) not null default '',
	automaticName tinyint(1) not null default 1
) engine=innodb;

drop table if exists smartMenuMenuItems;
drop table if exists smartMenuMenus;
create table smartMenuMenus(
	id int unsigned key not null auto_increment,
	businessid int unsigned not null,
	menuName varchar(64) not null default 'menu'
) engine=innodb;

create table smartMenuMenuItems(
	id int unsigned key not null auto_increment,
	menuId int unsigned not null,
	menuItemId int unsigned not null,
	menuGroupId int unsigned default null,
	index menuId_index( menuId ),
	index menuGroupId_index( menuGroupId ),
	constraint menuId_fk
		foreign key ( menuId )
		references smartMenuMenus( id )
		on update cascade
		on delete cascade,
	constraint menuGroupId_fk
		foreign key ( menuGroupId )
		references smartMenuMenuGroups( id )
		on update cascade
		on delete set null
) engine=innodb;

drop table if exists smartMenuControl;
create table smartMenuControl(
	id int unsigned key not null auto_increment,
	targetGuid char(32) not null,
	authority varchar(64) not null,
	command varchar(32) not null,
	argumentList varchar(512) not null,
	index targetGuid_index( targetGuid )
) engine=innodb;

drop table if exists smartMenuPackageRequests;
create table smartMenuPackageRequests(
	id int unsigned key not null auto_increment,
	packageFile char(32),
	expirationDate datetime
) engine=innodb;
