#!/usr/bin/env php
<?php
echo 'INSERTing a bunch of rows into `vend_gl_account_xhrefs`', PHP_EOL;

require_once( dirname(__FILE__) . '/../application/bootstrap.php' );

try {
	$new_ck_sales_ids = array(
		97 => array(
			1939,
			2046,
			2047,
			2048,
			2056,
			2091,
			2092,
			2101,
			2102,
			2103,
			2199,
			2200,
			2201,
			2208,
			2209,
			2210,
			2217,
			2218,
			2219,
			2263,
			2264,
			2265,
			
			# tax ids
			2100,
			2055,
			2045,
			2207,
			2198,
			2262,
			2308,
			2336,
			2216,
		),
		106 => array(
			2003,
			2004,
			2005,
			2151,
			2152,
			2153,
			2291,
			2292,
			2293,
			
			# tax ids
			2002,
			2290,
			2150,
		),
		151 => array(
			2107,
			2108,
			2109,
			2125,
			2126,
			2127,
			2133,
			2134,
			2135,
			2237,
			2238,
			2239,
			2254,
			2255,
			2256,
			
			# tax ids
			2132,
			2110,
			2124,
			2317,
			2236,
			2253,
		),
	);
	$dbh = Treat_DB::singleton();
	
	$sql = '
		SELECT
			vga.`id`
		FROM `vend_gl_accounts` vga
		WHERE
			vga.business_id = :business_id
			AND vga.name = "CK Sales"
	';
	$select = $dbh->prepare( $sql );
	
	$sql = '
		INSERT IGNORE INTO `vend_gl_account_xhrefs` (
			`account_id`
			,`business_id`
			,`foreign_id`
			,`table_name`
			,`name`
		)
		VALUES (
			:account_id
			,:business_id
			,:foreign_id
			,"creditdetail"
			,"CK Sales"
		)
	';
	$insert = $dbh->prepare( $sql );
	
	foreach ( $new_ck_sales_ids as $business_id => $list ) {
		$params = array(
			'business_id' => $business_id,
		);
		$select->execute( $params );
		$account = $select->fetchObject( 'Treat_ArrayObject' );
		
		try {
			$dbh->beginTransaction();
			foreach ( $list as $foreign_id ) {
				$params = array(
					'account_id' => $account->id,
					'business_id' => $business_id,
					'foreign_id' => $foreign_id,
				);
				
				$insert->execute( $params );
			}
			$dbh->commit();
		}
		catch ( PDOException $e ) {
			echo 'Connection failed: ', $e->getCode(), ': ', $e->getMessage(), PHP_EOL;
			if ( isset( $dbh ) ) {
				$dbh->rollBack();
			}
		}
	}
}
catch ( PDOException $e ) {
	echo 'Connection failed: ', $e->getCode(), ': ', $e->getMessage(), PHP_EOL;
}


