

DROP TABLE IF EXISTS `streamware_dataexchange`;
CREATE TABLE `streamware_dataexchange` (
	`dataexchangeId` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT
	,`timeCreated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'gearman client created time'
	,`receivedHeaders` TEXT COLLATE utf8_unicode_ci NOT NULL COMMENT 'headers received from client'
	,`receivedInput` LONGTEXT COLLATE utf8_unicode_ci NOT NULL COMMENT 'php://input'
	,`isXmlValid` TINYINT(1) DEFAULT NULL COMMENT 'whether the data in xmlData is valid according to the DataExchange.xsd'
	,`xmlErrors` TEXT COLLATE utf8_unicode_ci NOT NULL COMMENT 'list of xml errors from validating against the XSD'
	,`operatorId` INT(10) UNSIGNED DEFAULT NULL COMMENT 'operatorId that was sent in SOAP'
	,`sha1` VARCHAR(40) COLLATE utf8_unicode_ci NOT NULL COMMENT 'sha1sum of the SOAP file'
	,`fileName` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'base filename - append file extension for full file'
	,`gearmanId` INT(10) UNSIGNED NOT NULL COMMENT 'gearmanId from the gearman_process table'
	,`gearmanHandle` VARCHAR(25) COLLATE utf8_unicode_ci NOT NULL COMMENT 'returned gearman handle (for querying gearman server as to current status)'
	,`productUpdatesReceived` INT(10) UNSIGNED NOT NULL COMMENT 'total productUpdateRecords received'
	,`posUpdatesReceived` INT(10) UNSIGNED NOT NULL COMMENT 'total posUpdateRecords received'
	,`unknownPosIds` TEXT COLLATE utf8_unicode_ci NOT NULL COMMENT 'a json list of unknown posIDs that were sent'
	,`responseHeaders` TEXT COLLATE utf8_unicode_ci NOT NULL COMMENT 'response headers sent back to client'
	,`responseSoap` TEXT COLLATE utf8_unicode_ci NOT NULL COMMENT 'return value from Zend_Soap_Server->handle()'
	,PRIMARY KEY (`dataexchangeId`)
	,INDEX( `sha1` )
)
ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Streamware DataExchange debug'
;

DROP TABLE IF EXISTS `streamware_dataexchange_posid`;
CREATE TABLE `streamware_dataexchange_posid` (
	`dataexchangeId` INT(10) UNSIGNED DEFAULT NULL COMMENT ''
	,`businessid` INT(10) UNSIGNED DEFAULT NULL COMMENT ''
	,`posId` VARCHAR(21) COLLATE utf8_unicode_ci NOT NULL COMMENT ''
	,`gearmanId` INT(10) UNSIGNED NOT NULL COMMENT 'gearmanId from the gearman_process table'
	,`productUpdatesProcessed` INT(10) UNSIGNED NOT NULL COMMENT 'total productUpdateRecords processed'
	,`posUpdatesProcessed` INT(10) UNSIGNED NOT NULL COMMENT 'total posUpdateRecords processed'
	,`totalProductsProcessed` INT(10) UNSIGNED NOT NULL COMMENT 'total products processed'
	,`totalProductsSaved` INT(10) UNSIGNED NOT NULL COMMENT 'total products saved'
	,`productErrors` TEXT COLLATE utf8_unicode_ci NOT NULL COMMENT 'a json list of validation errors'
	,`productsNew` TEXT COLLATE utf8_unicode_ci NOT NULL COMMENT 'a json list of new products'
	,`productsProblem` TEXT COLLATE utf8_unicode_ci NOT NULL COMMENT 'a json list of problem products'
	,`productsUnknown` TEXT COLLATE utf8_unicode_ci NOT NULL COMMENT 'a json list of unknown products'
	,PRIMARY KEY ( `dataexchangeId`, `businessid` )
)
ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Streamware DataExchange debug'
;


