
UPDATE vend_gl_accounts
SET account_group_id = 3
WHERE
	business_id = 106
	AND account_id = 0
	AND number IS NULL
	AND account_type_id = 1
;


INSERT INTO vend_gl_accounts (
	business_id
	,account_group_id
	,account_id
	,number
	,name
	,account_type_id
	,sort_order
	,creditid
)
SELECT
	106 AS business_id
	,vgla.account_group_id
	,vgla.account_id
	,vgla.number
	,vgla.name
	,vgla.account_type_id
	,vgla.sort_order
	,vgla.creditid
FROM vend_gl_accounts vgla
WHERE
	vgla.business_id = 97
	AND vgla.account_id = 0
	AND vgla.number IS NULL
	AND vgla.account_type_id > 1
;


INSERT INTO vend_gl_accounts (
	business_id
	,account_group_id
	,account_id
	,number
	,name
	,account_type_id
	,sort_order
	,creditid
)
SELECT
	151 AS business_id
	,omaha.account_group_id
	,omaha.account_id
	,omaha.number
	,omaha.name
	,omaha.account_type_id
	,omaha.sort_order
	,omaha.creditid
FROM vend_gl_accounts omaha
	JOIN vend_gl_accounts kc
		ON omaha.name = kc.name
		AND omaha.business_id = 106
		AND omaha.account_id = 0
		AND omaha.number IS NULL
		AND kc.business_id = 97
		AND kc.account_id = 0
		AND kc.number IS NULL
WHERE
	omaha.business_id = 106
	AND omaha.account_id = 0
	AND omaha.number IS NULL
;
