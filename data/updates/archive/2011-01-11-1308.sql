
ALTER TABLE `vend_gl_account_groups` ADD `account_group_class` VARCHAR( 60 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' AFTER `sort_order`;

UPDATE `vend_gl_account_groups`
SET `account_group_class` = 'Treat_Model_GeneralLedger_Object_AccountGroup'
;

INSERT INTO `vend_gl_account_groups` (
	`id`
	,`sort_order`
	,`name`
	,`account_group_class`
)
VALUES
	(  4, -1, 'COST OF GOODS', 'Treat_Model_GeneralLedger_AccountGroup_CostOfGoodsObject' )
;

INSERT INTO `vend_gl_accounts` (
	`business_id`
	,`account_group_id`
	,`account_type_id`
	,`sort_order`
	,`multiplier`
	,`name`
)
VALUES
	(  97, 4, 1, 5, 0, 'Cost of Goods' )
	,( 97, 4, 4, -5, -0.062, 'Customer Commissin Exp' )
;
