
UPDATE `vend_gl_account_xhrefs`
SET `foreign_id` = 143
	,`table_name` = 'business.districtid'
WHERE `business_id` = 97
	AND `account_id` = 287
	AND `name` = 'CK Sales'
LIMIT 1
;

UPDATE `vend_gl_account_xhrefs`
SET `foreign_id` = 146
	,`table_name` = 'business.districtid'
WHERE `business_id` = 106
	AND `account_id` = 288
	AND `name` = 'CK Sales'
LIMIT 1
;

UPDATE `vend_gl_account_xhrefs`
SET `foreign_id` = 150
	,`table_name` = 'business.districtid'
WHERE `business_id` = 151
	AND `account_id` = 419
	AND `name` = 'CK Sales'
LIMIT 1
;

DELETE FROM `vend_gl_account_xhrefs`
WHERE
	(
		`business_id` = 97
		AND `account_id` = 287
		AND `name` = 'CK Sales'
		AND `foreign_id` != 143
	)
	OR (
		`business_id` = 106
		AND `account_id` = 288
		AND `name` = 'CK Sales'
		AND `foreign_id` != 146
	)
	OR (
		`business_id` = 151
		AND `account_id` = 419
		AND `name` = 'CK Sales'
		AND `foreign_id` != 150
	)
;

