CREATE TABLE IF NOT EXISTS `participation_businesses` (
	`participation_id` int(5) NOT NULL,
	`business_id` int(10) NOT NULL,
	UNIQUE KEY `participation_id` (`participation_id`,`business_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `participation_whitelist_upc` (
	`participation_id` int(5) NOT NULL,
	`UPC` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	UNIQUE KEY `participation_id` (`participation_id`,`UPC`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#ALTER TABLE `participation_whitelist_upc` ADD UNIQUE (
#	`participation_id` ,
#	`UPC`
#);

#ALTER TABLE `participation_businesses` ADD UNIQUE (
#	`participation_id` ,
#	`business_id`
#);
