alter table promotions add column promo_discount varchar(24);
alter table promotions add column promo_discount_type varchar(24);
alter table promotions add column promo_discount_n smallint;
alter table promotions add column promo_discount_limit smallint;
alter table promotions add column promo_trigger_amount_limit varchar(16) not null default 'multiple';
alter table promotions add column promo_trigger_amount_type varchar(24);
alter table promotions add column promo_trigger_amount smallint;
alter table promotions add column rule_order smallint;
alter table promotions add column pos_processed tinyint(1) not null default 0;
alter table promotions add column tax_id int;
alter table promotions add column promo_trigger_amount_2 smallint;


--
-- Table structure for table `promo_trigger`
--

DROP TABLE IF EXISTS `promo_trigger`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `promo_trigger` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `vivipos_name` varchar(32) NOT NULL DEFAULT '',
  `target_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `promo_trigger`
--

LOCK TABLES `promo_trigger` WRITE;
/*!40000 ALTER TABLE `promo_trigger` DISABLE KEYS */;
INSERT INTO `promo_trigger` VALUES (1,'Item','individual_plu',1),(2,'Group','individual_dept',2),(3,'Combo','multi_dept',4),(4,'All Products','bypass',3);
/*!40000 ALTER TABLE `promo_trigger` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `promo_target`
--

DROP TABLE IF EXISTS `promo_target`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `promo_target` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `db_table` varchar(50) NOT NULL,
  `order_by` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `promo_target`
--

LOCK TABLES `promo_target` WRITE;
/*!40000 ALTER TABLE `promo_target` DISABLE KEYS */;
INSERT INTO `promo_target` VALUES (1,'Menu Item(s)','menu_items_new','group_id,name'),(2,'Menu Group(s)','menu_groups_new','name'),(3,'ALL','',''),(4,'Multiple Groups','','');
/*!40000 ALTER TABLE `promo_target` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `promo_type`
--

DROP TABLE IF EXISTS `promo_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `promo_type` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(35) NOT NULL,
  `vivipos_name` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `promo_type`
--

LOCK TABLES `promo_type` WRITE;
/*!40000 ALTER TABLE `promo_type` DISABLE KEYS */;
INSERT INTO `promo_type` VALUES (1,'Percent','percentage_off'),(2,'Amount Off','amount_off'),(3,'Fixed Value','fixed_value'),(4,'Cheapest One Free','cheapest_one_free'),(5,'No Discount','bypass'),(6,'Discount After N','discount_after_n');
/*!40000 ALTER TABLE `promo_type` ENABLE KEYS */;
UNLOCK TABLES;




create or replace view vPromoDays as
	select 0 as day,id from promotions pm where pm.monday=1
	union
	select 1 as day,id from promotions pt where pt.tuesday=1
	union
	select 2 as day,id from promotions pw where pw.wednesday=1
	union
	select 3 as day,id from promotions pth where pth.thursday=1
	union
	select 4 as day,id from promotions pf where pf.friday=1
	union
	select 5 as day,id from promotions ps where ps.saturday=1
	union
	select 6 as day,id from promotions psu where psu.sunday=1;

create or replace view vPromoVivipos as
	select 
		promotions.businessid,
		promotions.id as promo_id,
		promo_target.id as promo_target,
		promotions.name as vivipos_name,
		promotions.pos_id,
		promotions.value,
		promotions.rule_order,
		promotions.promo_discount,
		promotions.promo_discount_type,
		promotions.promo_discount_n,
		promotions.promo_discount_limit,
		promotions.promo_trigger_amount_limit,
		promotions.promo_trigger_amount_type,
		promotions.promo_trigger_amount,
		promotions.promo_trigger_amount_2,
		COALESCE( CAST( menu_tax.pos_id AS char ), '' ) as vivipos_taxno,
		COALESCE( menu_tax.name, '' ) as vivipos_taxname,
		UNIX_TIMESTAMP( promotions.start_date ) as vivipos_startdate,
		UNIX_TIMESTAMP( CONCAT( promotions.start_date, ' ', promotions.start_time ) ) as vivipos_starttime,
		UNIX_TIMESTAMP( promotions.end_date ) as vivipos_enddate,
		UNIX_TIMESTAMP( CONCAT( promotions.start_date, ' ', promotions.end_time ) ) as vivipos_endtime,
		GROUP_CONCAT( vPromoDays.day ORDER BY vPromoDays.day ) as vivipos_days,
		promo_type.vivipos_name as vivipos_type,
		promo_trigger.vivipos_name as vivipos_trigger,
		promo_target.db_table as promotarget_table,
		promo_target.order_by as promotarget_order,
		UNIX_TIMESTAMP( NOW() ) as vivipos_modified,
		promotions.activeFlag as vivipos_active
	from promotions
	join promo_type on promotions.type=promo_type.id
	join promo_trigger on promotions.promo_trigger=promo_trigger.id
	join promo_target on promo_trigger.target_id=promo_target.id
	left join menu_tax on promotions.tax_id = menu_tax.id
	left join vPromoDays on promotions.id=vPromoDays.id
	group by promotions.id;