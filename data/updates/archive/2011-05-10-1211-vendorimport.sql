create table vendor_import_dates(
	importId char(3) key not null,
	fileDate date not null
);