drop table if exists promo_groups;
create table promo_groups(
	id int unsigned not null key auto_increment,
	businessid int unsigned not null,
	uuid char(36) not null default '',
	name varchar(128) not null default '',
	visible tinyint(1) not null default 1,
	routing tinyint(1) not null default 1,
	display_order int not null default 99999,
	unique key uuid (uuid),
	key businessid (businessid)
) engine=innodb;

drop table if exists promo_groups_detail;
create table promo_groups_detail(
	product_id int unsigned not null,
	promo_group_id int unsigned not null,
	primary key (product_id, promo_group_id)
) engine=innodb;

ALTER TABLE  `kiosk_sync` CHANGE  `item_type`  `item_type` INT( 1 ) UNSIGNED NOT NULL DEFAULT  '1' COMMENT  '1=items,2=groups,3=tax,4=itemtax,5=promo,6=promo_groups';