# Treat_Model_GeneralLedger_Account_SalesObject

INSERT INTO `vend_gl_account_types` (
	`label`
	,`sort_order`
	,`account_class`
)
VALUES
	(  'CK Sales (required for CK Fees)', '-5', 'Treat_Model_GeneralLedger_Account_CkSalesObject' )
	,( 'CK Fees', '-5', 'Treat_Model_GeneralLedger_Account_CkFeesObject' )
;


SELECT @account_type_id := `id`
FROM `vend_gl_account_types`
WHERE `account_class` = 'Treat_Model_GeneralLedger_Account_CkSalesObject'
;


UPDATE `vend_gl_accounts`
SET `account_type_id` = @account_type_id
WHERE
	`account_id` = 0
	AND `number` IS NULL
	AND `name` = 'CK Sales'
;


SELECT @account_type_id := `id`
FROM `vend_gl_account_types`
WHERE `account_class` = 'Treat_Model_GeneralLedger_Account_CkFeesObject'
;


SELECT @account_group_id := `id`
FROM `vend_gl_account_groups`
WHERE `name` = 'NET GROSS PROFIT'
;


INSERT INTO `vend_gl_accounts` (
	`business_id`
	,`account_group_id`
	,`name`
	,`account_type_id`
	,`sort_order`
	,`multiplier`
)
VALUES
	(   97, @account_group_id, 'CK Fees', @account_type_id, 6, -0.0595 )
	,( 106, @account_group_id, 'CK Fees', @account_type_id, 6, -0.0595 )
	,( 151, @account_group_id, 'CK Fees', @account_type_id, 6, -0.0595 )
;



