# SQL for Vendor General Ledger tables

# rename vend_gl_account_detail to vend_gl_account_details
RENAME TABLE `vend_gl_account_detail` TO `vend_gl_account_details`;

# rename vend_gl_groups to vend_gl_account_groups
RENAME TABLE `vend_gl_groups` TO `vend_gl_account_groups`;

# changing the default character set for the table to utf8
ALTER TABLE `vend_gl_accounts` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
ALTER TABLE `vend_gl_account_groups` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;

# only changing the character set from latin1_swedish_ci to utf8
ALTER TABLE `vend_gl_accounts` CHANGE `name` `name` VARCHAR( 50 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;
ALTER TABLE `vend_gl_account_details` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
ALTER TABLE `vend_gl_account_groups` CHANGE `name` `name` VARCHAR( 100 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;

# changing primary/foreign key fields to unsigned ints
ALTER TABLE `vend_gl_accounts` CHANGE `id` `id` INT( 10 ) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `vend_gl_accounts` CHANGE `businessid` `business_id` INT( 10 ) UNSIGNED NOT NULL COMMENT 'matches business primary key';
ALTER TABLE `vend_gl_accounts` CHANGE `groupid` `account_group_id` INT( 3 ) UNSIGNED NOT NULL COMMENT 'matches vend_gl_account_groups primary key' AFTER `business_id`;
ALTER TABLE `vend_gl_account_groups` CHANGE `id` `id` INT( 3 ) UNSIGNED NOT NULL AUTO_INCREMENT;

# add the Treat America general ledger account number field & add an index for it
ALTER TABLE `vend_gl_accounts` ADD `account_id` INT( 10 ) UNSIGNED NOT NULL COMMENT 'matches vend_gl_accounts primary key' AFTER `account_group_id`;
ALTER TABLE `vend_gl_accounts` ADD `number` VARCHAR( 16 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL AFTER `account_id`;
ALTER TABLE `vend_gl_accounts` ADD INDEX ( `number` );
ALTER TABLE `vend_gl_accounts` ADD `sort_order` SMALLINT NOT NULL;
ALTER TABLE `vend_gl_accounts` ADD `account_type_id` SMALLINT UNSIGNED NOT NULL COMMENT 'matches vend_gl_account_types primary key' AFTER `name`;


ALTER TABLE `vend_gl_account_groups` ADD `sort_order` SMALLINT NOT NULL;
ALTER TABLE `vend_gl_account_groups` ADD `account_type_id` SMALLINT UNSIGNED NOT NULL COMMENT 'matches vend_gl_account_types primary key' AFTER `name`;


# change the default character set for the table to utf8
ALTER TABLE `vend_gl_account_details` CHANGE `id` `id` INT( 10 ) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `vend_gl_account_details` CHANGE `accountid` `account_id` INT( 10 ) UNSIGNED NOT NULL COMMENT 'matches vend_gl_accounts primary key';
ALTER TABLE `vend_gl_account_details` CHANGE `date` `friday_date` DATE NOT NULL;
ALTER TABLE `vend_gl_account_details` CHANGE `actual` `amount` DOUBLE( 10, 2 ) NOT NULL;


CREATE TABLE `vend_gl_account_types` (
	`id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
	`label` VARCHAR( 100 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
	PRIMARY KEY ( `id` )
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
REPLACE INTO `vend_gl_account_types` (
	`id`
	,`label`
)
VALUES
	(  1, 'import' )
	,( 2, 'calculated' )
;



#CREATE TABLE `vend_gl_account_details` (
#	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
#	`account_id` INT UNSIGNED NOT NULL COMMENT 'matches vend_gl_accounts primary key',
#	`friday_date` DATE NOT NULL ,
#	`amount` DOUBLE( 10, 2 ) NOT NULL,
#	PRIMARY KEY ( `id` ),
#	UNIQUE KEY `account_id` ( `account_id` , `friday_date` )
#) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



CREATE TABLE `vend_gl_account_budgets` (
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
	`account_id` INT UNSIGNED NOT NULL COMMENT 'matches vend_gl_accounts primary key',
	`budget_month` DATE NOT NULL,
	`amount` DOUBLE( 10, 2 ) NOT NULL,
	UNIQUE ( `account_id` , `budget_month` )
) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci;



CREATE TABLE `vend_gl_account_items` (
	`account_id` INT UNSIGNED NOT NULL COMMENT 'matches vend_gl_accounts primary key',
	`sequence_id` INT UNSIGNED NOT NULL COMMENT 'array id within the account_id - we could use auto_increment if we had some way of knowing when we could flush the table',
	`friday_date` DATE NOT NULL,
	`amount` DOUBLE( 10, 2 ) NOT NULL,
	`label1` VARCHAR( 50 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`label2` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	PRIMARY KEY ( `account_id` , `sequence_id` )
) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci;


#####################################

UPDATE `vend_gl_account_groups`
SET `name` = LEFT( `name`, CHAR_LENGTH( `name` ) -1 )
WHERE `name` LIKE '%:'
;

UPDATE `vend_gl_account_groups`
SET `sort_order` = -5
WHERE `name` = 'SALES'
;

UPDATE `vend_gl_account_groups`
SET `sort_order` = 5
WHERE `name` = 'OPERATING EXPENSE'
;



