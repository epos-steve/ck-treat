
UPDATE `menu_groups_new` mgn
	JOIN `business` b
		ON mgn.businessid = b.businessid
SET mgn.`name` = '01 BEVERAGES'
WHERE mgn.`name` = '01 Beverage'
	AND b.companyid IN ( 3, 6 )
;

UPDATE `menu_groups_new` mgn
	JOIN `business` b
		ON mgn.businessid = b.businessid
SET mgn.`name` = '02 SNACKS'
WHERE mgn.`name` = '02 Snack'
	AND b.companyid IN ( 3, 6 )
;

UPDATE `menu_groups_new` mgn
	JOIN `business` b
		ON mgn.businessid = b.businessid
SET mgn.`name` = '03 COMMISSARY'
WHERE mgn.`name` = '03 Commissary'
	AND b.companyid IN ( 3, 6 )
;


UPDATE `menu_groups_new` mgn
	JOIN `business` b
		ON mgn.businessid = b.businessid
SET mgn.`menu_bid` = 143
	,mgn.`menu_typeid` = 23
WHERE mgn.`name` IN ( '01 BEVERAGES', '02 SNACKS' )
	AND b.companyid IN ( 3, 6 )
;

UPDATE `menu_groups_new` mgn
	JOIN `business` b
		ON mgn.businessid = b.businessid
SET mgn.`menu_bid` = 93
	,mgn.`menu_typeid` = 15
WHERE mgn.`name` = '03 COMMISSARY'
	AND b.companyid IN ( 3, 6 )
;




