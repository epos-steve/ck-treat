
# DELETE all menu_items out of Des Moines units except for the exceptions
DELETE mi
FROM `menu_items` mi
	JOIN `business` b
		ON mi.`businessid` = b.`businessid`
		AND b.`districtid` = 4
		AND b.`companyid` = 1
		AND b.`businessid` NOT IN (
			288	# Des Moines Contract Feeding
			,388 # [DM] Corporate Recipes
		)
WHERE mi.`menu_typeid` = 17
;

# UPDATE all of the businesses, except our exception list, to use the new nutrition
UPDATE `business` b
SET b.`use_nutrition` = 1
WHERE
	b.`districtid` = 4
	AND b.`companyid` = 1
	AND b.`businessid` NOT IN (
			288	# Des Moines Contract Feeding
			,388 # [DM] Corporate Recipes
	)
;


