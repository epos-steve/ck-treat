
# DELETE all menu_items out of Indiana units except for the exceptions
DELETE mi
FROM `menu_items` mi
	JOIN `business` b
		ON mi.`businessid` = b.`businessid`
		AND b.`districtid` = 100
		AND b.`companyid` = 5
		AND b.`businessid` NOT IN (
			144	# <font color=green>Corporate Chef-Indiana
			,189	# Roche Diagnostics Office Coffee
			,190	# Roche Fishers Office Coffee
			,290	# Contract Feeding IN
			,390	# [IN] Corporate Recipes
		)
WHERE mi.`menu_typeid` = 21
;

# UPDATE all of the businesses, except our exception list, to use the new nutrition
UPDATE `business` b
SET b.`use_nutrition` = 1
WHERE
	b.`districtid` = 100
	AND b.`companyid` = 5
	AND b.`businessid` NOT IN (
			144	# <font color=green>Corporate Chef-Indiana
			,189	# Roche Diagnostics Office Coffee
			,190	# Roche Fishers Office Coffee
			,290	# Contract Feeding IN
			,390	# [IN] Corporate Recipes
	)
;




# DELETE all menu_items out of specified Indiana units except for the exceptions
DELETE mi
FROM `menu_items` mi
	JOIN `business` b
		ON mi.`businessid` = b.`businessid`
		AND b.`companyid` = 5
		AND (
			b.`districtid` = 113 # Closed Units
			OR b.`businessid` IN (
				144	# <font color=green>Corporate Chef-Indiana
			)
		)
WHERE mi.`menu_typeid` = 21
;

# UPDATE all of the businesses, except our exception list, to use the new nutrition
UPDATE `business` b
SET b.`use_nutrition` = 1
WHERE
	b.`companyid` = 5
	AND (
		b.`districtid` = 113 # Closed Units
		OR b.`businessid` IN (
				144	# <font color=green>Corporate Chef-Indiana
		)
	)
;




