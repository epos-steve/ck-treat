
ALTER TABLE `vend_gl_account_items` ADD `import_number` TINYINT UNSIGNED NOT NULL DEFAULT '0' COMMENT 'is this the primary or secondary file import' AFTER `friday_date`;

UPDATE `vend_gl_account_items`
SET `import_number` = 1
WHERE `import_number` = 0
;
