
DROP TABLE IF EXISTS `gearman_process`;
CREATE TABLE `gearman_process` (
	`gearmanId` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT
	,`deleteJob` TINYINT(1) UNSIGNED NOT NULL COMMENT 'whether to delete the job next time a worker picks it up'
	,`ini` VARCHAR(40) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'site ini'
	,`timeCreated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'gearman client created time'
	,`server` VARCHAR(25) COLLATE utf8_unicode_ci NOT NULL COMMENT 'server domain or ip address for gearman'
	,`port` INT(5) UNSIGNED NOT NULL COMMENT 'server port for gearman'
	,`clientHostname` VARCHAR(25) COLLATE utf8_unicode_ci NOT NULL COMMENT 'hostname of the box making the request'
	,`priority` TINYINT(1) UNSIGNED NOT NULL DEFAULT 2 COMMENT '1 high, 2 normal, 3 low'
	,`functionName` VARCHAR(25) COLLATE utf8_unicode_ci NOT NULL COMMENT 'functionName gearman worker will call'
	,`handle` VARCHAR(25) COLLATE utf8_unicode_ci NOT NULL COMMENT 'returned gearman handle (for querying gearman server as to current status)'
	,`unique` VARCHAR(40) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'a unique value given to gearman for this process'
	,`payload` TEXT COLLATE utf8_unicode_ci NOT NULL COMMENT 'the data payload given to gearman - should keep this small & in json format'
	,`attempts` INT(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'number of attempts to run this job'
	,`timeStart` TIMESTAMP NULL DEFAULT NULL COMMENT 'time that gearman worker started job'
	,`timeEnd` TIMESTAMP NULL DEFAULT NULL COMMENT 'time that gearman worker finished job'
	,`exceptions` LONGTEXT COLLATE utf8_unicode_ci NOT NULL COMMENT 'logged exceptions'
	,PRIMARY KEY (`gearmanId`)
	,INDEX( `handle` )
	,INDEX( `functionName`, `unique` )
)
ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='gearman processes'
;




