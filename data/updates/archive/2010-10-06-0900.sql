
# This is an export of the table structures that Steve created

CREATE TABLE `machine_order` (
	`id` int(10) NOT NULL AUTO_INCREMENT,
	`machineid` int(10) NOT NULL,
	`date_time` date NOT NULL,
	`customer` varchar(20) NOT NULL,
	`machine_num` varchar(20) NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `machineid` (`machineid`,`date_time`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE `machine_orderdetail` (
	`id` int(10) NOT NULL AUTO_INCREMENT,
	`orderid` int(10) NOT NULL,
	`item_code` varchar(30) NOT NULL,
	`qty` double NOT NULL,
	`new_item_code` varchar(20) NOT NULL,
	`new_qty` double NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `orderid` (`orderid`,`item_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

