#!/usr/bin/env php
<?php
echo 'Make sure you\'ve run the associated .sql file *FIRST*', PHP_EOL;

require_once( dirname(__FILE__) . '/../../application/bootstrap.php' );

try {
	$column_keys = array(
		'calories' => 'calories',
		'cal_from_fat' => 'cal_from_fat',
		'total_fat' => 'total_fat',
		'sat_fat' => 'sat_fat',
		'cholesterol' => 'cholesterol',
		'sodium' => 'sodium',
		'potassium' => 'potassium',
		'carbs' => 'complex_cabs',	# yes, these are bad carbs that where labeled complex_cabs by original source
		'fiber' => 'fiber',
		'sugar' => 'sugar',
		'protein' => 'protein',
		'vit_A' => 'vit_A',
		'vit_C' => 'vit_C',
		'thiamine' => 'thiamine',
		'iron' => 'iron',
		'treat_score' => 'treat_score',
	);
	$dbh = Treat_DB::singleton();
	
	$dbh->beginTransaction();
	foreach ( $column_keys as $column_name => $column_key ) {
		$sql = sprintf( '
				INSERT INTO `customer_limits` (
					`customerid`
					,`nutrition_type_id`
					,`display`
				)
				SELECT
					cn.`customerid`
					,nt.`nutritionid` AS `nutrition_type_id`
					,cn.`%1$s`
				FROM `customer_nutrition` cn
					JOIN `nutrition_types` nt
						ON nt.`column_key` = :column_key
				WHERE cn.`%1$s` != 0
				ON DUPLICATE KEY UPDATE `display` = VALUES( `display` )
			'
			,$column_name
		);
		
		$params = array(
			'column_key' => $column_key,
		);
		
		$insert = $dbh->prepare( $sql );
		$insert->execute( $params );
	}
	$dbh->commit();
}
catch ( PDOException $e ) {
	echo 'Connection failed: ', $e->getCode(), ': ', $e->getMessage(), PHP_EOL;
	if ( isset( $dbh ) ) {
		$dbh->rollBack();
	}
}


