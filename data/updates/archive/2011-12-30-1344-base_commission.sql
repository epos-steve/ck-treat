

ALTER TABLE `vend_commission` ADD `holiday_pay` DOUBLE NOT NULL DEFAULT '0' AFTER `base`;

# set all routes to default to 150 holiday pay
UPDATE `vend_commission`
SET `holiday_pay` = 150
;


ALTER TABLE `company` ADD `holiday_pay` DOUBLE NOT NULL DEFAULT '0' AFTER `ar_update`;

# set all routes to default to 150 holiday pay
UPDATE `company`
SET `holiday_pay` = 150
;

ALTER TABLE `company` ADD `base_pay` DOUBLE NOT NULL DEFAULT '0' AFTER `ar_update`;

# set all routes to default to 150 holiday pay
UPDATE `company`
SET `base_pay` = 600
;


