create table db_trendlines(
	trendid int unsigned not null auto_increment key,
	viewid int unsigned not null,
	lineStart int not null default 0,
	lineEnd int not null default 0,
	trendCaption varchar(128) default '',
	lineColor char(6) default 'ff0000',
	lineWidth int unsigned not null default 1,
	linePosition tinyint default 0
);
create table pub_db_trendlines like db_trendlines;
drop trigger if exists mtime_insert_trendlines;
create trigger mtime_insert_trendlines 
	after insert on db_trendlines
	for each row 
		call trigUpdateDbMTime( NEW.viewid );

drop trigger if exists mtime_update_trendlines;
create trigger mtime_update_trendlines
	after update on db_trendlines
	for each row
		call trigUpdateDbMTime( NEW.viewid );

