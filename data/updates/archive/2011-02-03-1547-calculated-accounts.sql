# adding multiplier values for calculated accounts - i.e. Sales Tax

UPDATE `vend_gl_accounts`
SET `multiplier` = -.046
WHERE
	`business_id` = 97
	AND `name` = 'Sales Tax'
;

UPDATE `vend_gl_accounts`
SET `multiplier` = .083
WHERE
	`business_id` = 97
	AND `name` IN (
		'Payroll Taxex'
		,'Payroll Taxes'
	)
;

UPDATE `vend_gl_accounts`
SET `multiplier` = -.043
WHERE
	`business_id` = 106
	AND `name` = 'Sales Tax'
;

UPDATE `vend_gl_accounts`
SET `multiplier` = .0865
WHERE
	`business_id` = 106
	AND `name` IN (
		'Payroll Taxex'
		,'Payroll Taxes'
	)
;

UPDATE `vend_gl_accounts`
SET `multiplier` = -.067
WHERE
	`business_id` = 151
	AND `name` = 'Sales Tax'
;

UPDATE `vend_gl_accounts`
SET `multiplier` = .0885
WHERE
	`business_id` = 151
	AND `name` IN (
		'Payroll Taxex'
		,'Payroll Taxes'
	)
;

UPDATE `vend_gl_accounts`
SET `multiplier` = .1225
WHERE `name` = 'Group Insurance'
;

UPDATE `vend_gl_accounts`
SET `multiplier` = .0053
WHERE `name` = '401K Expense'
;


