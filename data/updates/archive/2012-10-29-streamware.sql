
DROP TABLE IF EXISTS `streamware_checks_sent`;
CREATE TABLE `streamware_checks_sent` (
	`scs_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'streamware_checks_sent id'
	,`businessid` int(10) unsigned NOT NULL
	,`check_number` bigint(20) unsigned NOT NULL
	,`transaction_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL
	,`timestamp_sent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
	,`confirmed` tinyint(1) NOT NULL
	,PRIMARY KEY (`scs_id`)
	,UNIQUE KEY `businessid` (`businessid`,`check_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci
;

DROP TABLE IF EXISTS `streamware_debug`;
CREATE TABLE `streamware_debug` (
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT
	,`created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
	,`debug_data` text COLLATE utf8_unicode_ci NOT NULL
	,`debug_headers` text COLLATE utf8_unicode_ci NOT NULL
	,PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci
;

ALTER TABLE `streamware_debug` CHANGE `debug_data` `debug_data` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL  AFTER `debug_headers`
;

ALTER TABLE `streamware_debug` ADD `debug_exceptions` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `debug_data`
;

ALTER TABLE `streamware_debug` ADD `get_data` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '$_GET' AFTER `debug_headers`
;
ALTER TABLE `streamware_debug` ADD `post_data` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '$_POST' AFTER `get_data`
;
ALTER TABLE `streamware_debug` ADD `input_data` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'php://input' AFTER `post_data`
;
ALTER TABLE `streamware_debug` ADD `debug_log` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'debug info from entering and exiting methods' AFTER `debug_exceptions`
;
ALTER TABLE `streamware_debug` ADD `soap_response` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'return value from Zend_Soap_Server->handle()' AFTER `debug_log`
;

DROP TABLE IF EXISTS `streamware_servicerecord_sent`;
CREATE TABLE `streamware_servicerecord_sent` (
	`sss_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'streamware_servicerecord_sent id'
	,`icv_id` int(10) unsigned NOT NULL COMMENT 'inv_count_vend id'
	,`timestamp_sent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
	,`confirmed` tinyint(1) NOT NULL
	,PRIMARY KEY (`sss_id`)
	,UNIQUE KEY `icv_id` (`icv_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci
;

ALTER TABLE `streamware_debug` CHANGE `debug_headers` `headers_received` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'headers received from client'
;

ALTER TABLE `streamware_debug` ADD `headers_response` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'response headers sent back to client' AFTER `debug_log`
;

ALTER TABLE `streamware_debug` ADD `is_xml_valid` BOOLEAN NULL COMMENT 'whether the data in xmlData is valid according to the DataExchange.xsd' AFTER `input_data`
;

ALTER TABLE `streamware_debug` ADD `xml_errors` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `is_xml_valid`
;


DROP TABLE IF EXISTS `streamware_kiosk_data`;
CREATE TABLE `streamware_kiosk_data` (
  `companyid` int(3) unsigned NOT NULL COMMENT 'company.companyid',
  `businessid` int(10) unsigned NOT NULL COMMENT 'business.businessid',
  `client_programid` int(20) unsigned DEFAULT NULL COMMENT 'mburris_manage.client_programs.client_programid or null',
  `vendmark_id` int(10) unsigned NOT NULL COMMENT 'posID for vendmark',
  PRIMARY KEY ( `companyid`, `businessid`, `vendmark_id` )
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='has all of the various data needed for Streamware per kiosk'
;

INSERT INTO `streamware_kiosk_data` (
	`companyid`
	,`businessid`
	,`client_programid`
	,`vendmark_id`
)
VALUES
	(  35, 543, NULL, 456 ) # Sanmina
	,( 35, 553, NULL, 123 ) # Lifespan
	,( 35, 604, NULL, 789 ) # Family Dollar
;

ALTER TABLE `streamware_kiosk_data` CHANGE `vendmark_id` `vendmax_id` INT( 10 ) UNSIGNED NOT NULL COMMENT 'posID for vendmax';

ALTER TABLE `streamware_kiosk_data` ADD `operator_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Operator for Streamware SOAP';

UPDATE `streamware_kiosk_data`
SET `operator_id` = 243
WHERE `businessid` IN ( 543, 553, 604 )
;

TRUNCATE `streamware_kiosk_data`;

INSERT INTO `streamware_kiosk_data` (
	`companyid`
	,`businessid`
	,`client_programid`
	,`operator_id`
	,`vendmax_id`
)
VALUES
	(  48, 772, NULL, 243, 15916 ) # Kiosk - Kautex
;

#ALTER TABLE `streamware_kiosk_data` CHANGE `vendmax_id` `vendmax_id` VARCHAR( 16 ) NOT NULL DEFAULT '' COMMENT 'posID for vendmax';

UPDATE `streamware_kiosk_data`
SET `operator_id` = 272
WHERE `vendmax_id` IN ( 15916 )
;

DROP TABLE IF EXISTS `streamware_debug_push`;
CREATE TABLE `streamware_debug_push` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT
	,`batch_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'time shell script started'
	,`time_start` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'start time for grabbing data'
	,`time_end` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'end time for grabbing data'
	,`sent_at` TIMESTAMP NULL DEFAULT NULL COMMENT 'start of attempted send time'
	,`operator_id` INT(10) UNSIGNED NOT NULL COMMENT 'Streamware Operator'
	,`companyid` INT(10) UNSIGNED NOT NULL
	,`filename` VARCHAR(30) COLLATE utf8_unicode_ci NOT NULL COMMENT 'xml filename during batch'
	,`xml_errors` LONGTEXT COLLATE utf8_unicode_ci NOT NULL COMMENT 'any xml validation errors against the XSD'
	,`response_message` LONGTEXT COLLATE utf8_unicode_ci NOT NULL COMMENT 'value from DataExchangeResponse->DataExchangeResult'
	,`sales_records_total` INT(10) UNSIGNED NOT NULL COMMENT 'total salesRecord sent'
	,`service_records_total` INT(10) UNSIGNED NOT NULL COMMENT 'total serviceRecord sent'
	,`checks_sent_total` INT(10) UNSIGNED NOT NULL COMMENT 'total sent check ids sent'
	,`checks_sent_saved` INT(10) UNSIGNED NOT NULL COMMENT 'total sent check ids successfully saved'
	,`inventory_sent_total` INT(10) UNSIGNED NOT NULL COMMENT 'total inventory ids sent'
	,`inventory_sent_saved` INT(10) UNSIGNED NOT NULL COMMENT 'total inventory ids successfully saved'
	,`unknown_total` INT(10) UNSIGNED NOT NULL COMMENT 'total unknowns sent'
	,`unknown_saved` INT(10) UNSIGNED NOT NULL COMMENT 'total unknowns successfully saved'
	,PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci
;

##########################################################3

DROP TABLE IF EXISTS `streamware_collection_sent`;
CREATE TABLE `streamware_collection_sent` (
	`scols_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'streamware_collection_sent id'
	,`machine_collect_id` INT(10) UNSIGNED NOT NULL COMMENT 'machine_collect id'
	,`timestamp_sent` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
	,`confirmed` BOOLEAN NOT NULL
	,PRIMARY KEY (`scols_id`)
	,UNIQUE (`machine_collect_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci
	COMMENT 'machine_collect id'
;




