ALTER TABLE promo_groups ADD COLUMN display_on VARCHAR(128) NOT NULL DEFAULT '';

CREATE TABLE aeris_operating_modes(
	id INT KEY NOT NULL AUTO_INCREMENT,
	name VARCHAR(32) NOT NULL DEFAULT '',
	token VARCHAR(32) NOT NULL DEFAULT ''
) ENGINE=INNODB CHARSET=utf8;

INSERT INTO aeris_operating_modes( id, name, token ) VALUES
	( 1, 'Kiosk', 'VENDING' ),
	( 2, 'Food Service', 'FOODSERVICE' ),
	( 3, 'Cashier', 'CASHIER' );