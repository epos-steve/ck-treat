
DROP TABLE IF EXISTS `fapi_cantaloupe_missing`;
CREATE TABLE `fapi_cantaloupe_missing` (
	`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT
	,`cantaloupe_operator_id` INT(10) UNSIGNED NOT NULL COMMENT 'OperatorID from Cantaloupe'
	,`bill_datetime` DATETIME NOT NULL COMMENT 'checks.bill_datetime'
	,`checkdetail_id` INT(10) UNSIGNED NOT NULL COMMENT 'checkdetail.id'
	,PRIMARY KEY ( `id` )
	,UNIQUE KEY `cantaloupe_operator_id` ( `cantaloupe_operator_id`,`checkdetail_id` )
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci
	COMMENT 'missing items unable to be sent to Cantaloupe'
;


DROP TABLE IF EXISTS `fapi_cantaloupe_operators`;
CREATE TABLE `fapi_cantaloupe_operators` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT
	,`businessid` INT(10) UNSIGNED NOT NULL COMMENT 'business id for the operator'
	,`operator_id` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'OperatorID from Cantaloupe'
	,`micromarket_id` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'MicromarketID from Cantaloupe'
	,`last_sync` DATETIME NOT NULL COMMENT 'the last time this operator was synced with Cantaloupe'
	,`last_sent_email` DATETIME NOT NULL COMMENT 'the last time an email was sent for this operator'
	,PRIMARY KEY ( `id` )
	,UNIQUE KEY `businessid` ( `businessid` )
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci
	COMMENT 'Cantaloupe operator/micromarket info'
;

INSERT INTO `fapi_cantaloupe_operators` (
	`businessid`
	,`operator_id`
	,`micromarket_id`
)
VALUES
	( 307, 'companykitchentest', 'test1' )
;





