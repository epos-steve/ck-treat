CREATE TABLE aeris_versions (
	client_programid INT UNSIGNED NOT NULL,
	component_name VARCHAR(64) NOT NULL,
	component_version VARCHAR(64) NOT NULL,
	reported_on TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (client_programid, component_name),
	FOREIGN KEY fk_client_programs(client_programid) REFERENCES mburris_manage.client_programs(client_programid) ON DELETE CASCADE
) ENGINE=INNODB;