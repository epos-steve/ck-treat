ALTER TABLE `menu_items_new` ADD COLUMN `requires_activation` TINYINT(1) NOT NULL DEFAULT 0  AFTER `force_condiment` ;
ALTER TABLE `menu_items_new` ADD INDEX ( `requires_activation` );
