
# This has several items that seem to be missing in various other databases
# making them different from the main mburris_businesstrack database

ALTER TABLE `customer` ADD COLUMN `single_use` TINYINT(1) NOT NULL DEFAULT 0;

ALTER TABLE `customer` ADD `is_deleted2` ENUM( '0', '1' ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '0';

##################################################

ALTER TABLE `menu_items_new` ADD `cond_group` INT( 10 ) UNSIGNED NULL DEFAULT NULL;
ALTER TABLE `menu_items_new` ADD `force_condiment` TINYINT( 1 ) NOT NULL DEFAULT '0';

##################################################

CREATE TABLE IF NOT EXISTS `promo_groups_mapping` (
	`PromoGroupId` int(10) unsigned NOT NULL,
	`BusinessId` int(10) unsigned NOT NULL,
	PRIMARY KEY (`PromoGroupId`,`BusinessId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `promo_mapping` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`PromotionId` int(10) NOT NULL,
	`BusinessId` int(10) NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `PromotionId` (`PromotionId`,`BusinessId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Keep track of the businesses that a (global) promotion appli' ;

CREATE TABLE IF NOT EXISTS `promo_mapping_temp` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`PromotionId` int(10) NOT NULL,
	`BusinessId` int(10) NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `PromotionId` (`PromotionId`,`BusinessId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Keep track of the businesses that a (global) promotion appli' ;


