
ALTER TABLE `security` ADD COLUMN `kiosk_route_mgmt` BOOLEAN NOT NULL COMMENT 'Kiosk Route Management' AFTER `bus_kiosk_menu_mgmt`;
ALTER TABLE `security` ADD COLUMN `kiosk_menu_mgmt` BOOLEAN NOT NULL COMMENT 'Kiosk Menu Management' AFTER `kiosk_route_mgmt`;
ALTER TABLE `security` ADD COLUMN `kiosk_kiosk_mgmt` BOOLEAN NOT NULL COMMENT 'Kiosk section Kiosk Management' AFTER `kiosk_menu_mgmt`;

ALTER TABLE `security` ADD COLUMN `posmgmt_status` BOOLEAN NOT NULL COMMENT 'POS Management Status tab' AFTER `bus_menu_pos_mgmt`;
ALTER TABLE `security` ADD COLUMN `posmgmt_setup` BOOLEAN NOT NULL COMMENT 'POS Management Setup tab' AFTER `posmgmt_status`;
ALTER TABLE `security` ADD COLUMN `posmgmt_cashiers` BOOLEAN NOT NULL COMMENT 'POS Management Cashier Users tab' AFTER `posmgmt_setup`;
ALTER TABLE `security` ADD COLUMN `posmgmt_posscreens` BOOLEAN NOT NULL COMMENT 'POS Management POS Screens tab' AFTER `posmgmt_cashiers`;
ALTER TABLE `security` ADD COLUMN `posmgmt_modifiers` BOOLEAN NOT NULL COMMENT 'POS Management Modifiers tab' AFTER `posmgmt_posscreens`;
ALTER TABLE `security` ADD COLUMN `posmgmt_staticmods` BOOLEAN NOT NULL COMMENT 'POS Management Static Modifiers tab' AFTER `posmgmt_modifiers`;
ALTER TABLE `security` ADD COLUMN `posmgmt_customers` BOOLEAN NOT NULL COMMENT 'POS Management Customers tab' AFTER `posmgmt_staticmods`;
ALTER TABLE `security` ADD COLUMN `posmgmt_cardnums` BOOLEAN NOT NULL COMMENT 'POS Management Card Numbers tab' AFTER `posmgmt_customers`;
ALTER TABLE `security` ADD COLUMN `posmgmt_menugroup` BOOLEAN NOT NULL COMMENT 'POS Management Menu Groups tab' AFTER `posmgmt_cardnums`;
ALTER TABLE `security` ADD COLUMN `posmgmt_taxes` BOOLEAN NOT NULL COMMENT 'POS Management Taxes tab' AFTER `posmgmt_menugroup`;
ALTER TABLE `security` ADD COLUMN `posmgmt_promotions` BOOLEAN NOT NULL COMMENT 'POS Management Promotions tab' AFTER `posmgmt_taxes`;
ALTER TABLE `security` ADD COLUMN `posmgmt_coldfood` BOOLEAN NOT NULL COMMENT 'POS Management Cold Food Orders tab' AFTER `posmgmt_promotions`;
ALTER TABLE `security` ADD COLUMN `posmgmt_customerutils` BOOLEAN NOT NULL COMMENT 'POS Management Customer Utilities tab' AFTER `posmgmt_coldfood`;
ALTER TABLE `security` ADD COLUMN `posmgmt_vmachine` BOOLEAN NOT NULL COMMENT 'POS Management Vending Machines tab' AFTER `posmgmt_customerutils`;


UPDATE `security`
SET `kiosk_route_mgmt` = `bus_kiosk_menu_mgmt`
	,`kiosk_menu_mgmt` = `bus_kiosk_menu_mgmt`
	,`kiosk_kiosk_mgmt` = `bus_kiosk_menu_mgmt`
	,`posmgmt_status` = IF( `bus_menu_pos_mgmt`, TRUE, FALSE )
	,`posmgmt_setup` = IF( `bus_menu_pos_mgmt` IN( 3 ), TRUE, FALSE )
	,`posmgmt_cashiers` = IF( `bus_menu_pos_mgmt` IN( 3 ), TRUE, FALSE )
	,`posmgmt_posscreens` = IF( `bus_menu_pos_mgmt` IN( 1, 2, 3 ), TRUE, FALSE )
	,`posmgmt_modifiers` = IF( `bus_menu_pos_mgmt` IN( 3 ), TRUE, FALSE )
	,`posmgmt_staticmods` = IF( `bus_menu_pos_mgmt` IN( 3 ), TRUE, FALSE )
	,`posmgmt_customers` = IF( `bus_menu_pos_mgmt` IN( 1, 2, 3 ), TRUE, FALSE )
	,`posmgmt_cardnums` = IF( `bus_menu_pos_mgmt` IN( 3 ), TRUE, FALSE )
	,`posmgmt_menugroup` = IF( `bus_menu_pos_mgmt` IN( 1, 2, 3 ), TRUE, FALSE )
	,`posmgmt_taxes` = IF( `bus_menu_pos_mgmt` IN( 1, 2, 3 ), TRUE, FALSE )
	,`posmgmt_promotions` = IF( `bus_menu_pos_mgmt` IN( 3 ), TRUE, FALSE )
	,`posmgmt_coldfood` = IF( `bus_menu_pos_mgmt` IN( 2, 3, 4 ), TRUE, FALSE )
	,`posmgmt_customerutils` = IF( `bus_menu_pos_mgmt` IN( 3 ), TRUE, FALSE )
	,`posmgmt_vmachine` = IF( `bus_menu_pos_mgmt` IN( 3 ), TRUE, FALSE )
;



