
INSERT INTO `vend_gl_account_types` (
	`label`
	,`sort_order`
	,`account_class`
)
SELECT
	'Sales Tax Calculations'
	,`sort_order`
	,'Treat_Model_GeneralLedger_Account_SalesTaxObject'
FROM `vend_gl_account_types`
WHERE `account_class` = 'Treat_Model_GeneralLedger_Account_CalculatedObject'
;

UPDATE `vend_gl_accounts`
SET `account_type_id` = (
	SELECT `id`
	FROM `vend_gl_account_types`
	WHERE `account_class` = 'Treat_Model_GeneralLedger_Account_SalesTaxObject'
)
WHERE `name` = 'Sales Tax'
	AND `account_id` = 0
	AND `number` IS NULL
;

UPDATE `vend_gl_accounts`
SET `multiplier` = ABS( `multiplier` )
WHERE `account_type_id` = 4
;

SELECT
	ABS( vga.`multiplier` ) AS abs_mult
	,vga.*
FROM `vend_gl_accounts` vga
WHERE vga.`account_type_id` = 4
;

