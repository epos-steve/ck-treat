# $Id: 2010-11-15-1247.sql 4315 2010-11-15 19:14:46Z patrick $

# add num_imports in order to keep track of imports
ALTER TABLE `vend_gl_account_details` ADD `num_imports` INT UNSIGNED NOT NULL COMMENT 'number of times an import has modified this line';

UPDATE `vend_gl_account_details`
SET `num_imports` = 1
WHERE `account_id` IN (
	SELECT `id`
	FROM `vend_gl_accounts`
	WHERE `account_type_id` = 1
);
