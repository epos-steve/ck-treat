
# as created by Steve
CREATE TABLE IF NOT EXISTS `cc_fee` (
	`businessid` int(10) NOT NULL,
	`date` date NOT NULL,
	`amount` double NOT NULL,
	PRIMARY KEY (`businessid`,`date`)
) ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci;


ALTER TABLE `cc_fee` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;

INSERT INTO `pos_report` (
	`label`
	,`on_demand`
	,`slug`
	,`class`
)
VALUES
	(  'CK Fees', FALSE, 'ck-fees', 'Treat_Model_Report_Object_PosCkFees' )
;


