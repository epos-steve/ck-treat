
# update vend_gl_account_types
ALTER TABLE `vend_gl_account_types` ADD `sort_order` SMALLINT NOT NULL DEFAULT '0' AFTER `label`;

UPDATE `vend_gl_account_types`
SET `sort_order` = -10
WHERE `id` = 1
;

UPDATE `vend_gl_account_types`
SET `sort_order` = -5
WHERE `id` IN ( 2, 3 )
;

UPDATE `vend_gl_account_types`
SET `sort_order` = 10
WHERE `id` = 4
;


# update vend_gl_accounts
UPDATE `vend_gl_accounts`
SET `name` = 'Payroll Taxes'
WHERE `name` = 'Payroll Taxex'
;

UPDATE `vend_gl_accounts`
SET `sort_order` = -10
WHERE `account_type_id` = 1
;

UPDATE `vend_gl_accounts`
SET `sort_order` = -5
WHERE `account_type_id` IN ( 2, 3 )
;

UPDATE `vend_gl_accounts`
SET `sort_order` = 10
WHERE `account_type_id` = 4
;


# fix/insert Omaha/Des Moines & Indiana accounts
UPDATE `vend_gl_accounts`
SET name = 'Omaha CSV Sales'
	,creditid = 2031
WHERE id = 130
;

UPDATE `vend_gl_accounts`
SET name = 'Omaha Full Service Sales'
	,creditid = 822
WHERE id = 131
;

INSERT INTO `vend_gl_accounts` (
	`business_id`
	,`account_group_id`
	,`account_type_id`
	,`creditid`
	,`sort_order`
	,`name`
)
VALUES
	(  106, 1, 2, 2033, -5, 'Des Moines CSV Sales' )
	,( 106, 1, 2, 1390, -5, 'Des Moines Full Service Sales' )
;

UPDATE `vend_gl_accounts`
SET name = 'CSV Sales'
	,creditid = 2032
WHERE id = 161
;

UPDATE `vend_gl_accounts`
SET name = 'Indiana Full Service Sales'
	,creditid = 1235
WHERE id = 162
;

INSERT INTO `vend_gl_accounts` (
	`business_id`
	,`account_group_id`
	,`account_type_id`
	,`creditid`
	,`sort_order`
	,`name`
)
VALUES
	(  151, 1, 2, 1539, -5, 'Greencastle Full Service Sales' )
	,( 151, 1, 2, 1538, -5, 'Greensboro Full Service Sales' )
	,( 151, 1, 2, 1540, -5, 'Lafayette Full Service Sales' )
;

