# Mapping table for promomotions. If the businesid is NULL in the promotions
# table, this table is the one to be looked at for matching promotions on the
# global level.
CREATE TABLE `promotion_mapping` (
	`PromotionId` INT UNSIGNED NOT NULL,
	`BusinessId` INT UNSIGNED NOT NULL,
	PRIMARY KEY (`PromotionId`,`BusinessId`)
) ENGINE=InnoDB;

# Mapping table for promo_groups. If the businesid is NULL in the promo_groups
# table, this table is the one to be looked at for matching promo groups on the
# global level.
CREATE TABLE `promo_groups_mapping` (
	`PromoGroupId` INT UNSIGNED NOT NULL,
	`BusinessId` INT UNSIGNED NOT NULL,
	PRIMARY KEY (`PromoGroupId`,`BusinessId`)
) ENGINE=InnoDB;

# Make the businessid nullable to mark the promotion or promo group as global
ALTER TABLE `promotions` CHANGE `businessid` `businessid` INT NULL DEFAULT NULL;
ALTER TABLE `promo_groups` CHANGE `businessid` `businessid` INT NULL DEFAULT NULL;