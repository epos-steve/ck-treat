

#DROP TABLE IF EXISTS `customer_xref_nutrition`;
#CREATE TABLE `customer_xref_nutrition` (
#	`customerid` INT UNSIGNED NOT NULL COMMENT 'matches the customer table id',
#	`nutrition_type_id` INT(2) UNSIGNED NOT NULL COMMENT 'matches the nutrition_types table id',
#	`display` BOOLEAN NOT NULL COMMENT 'display in the week listing',
#	`value` DOUBLE NOT NULL COMMENT 'exceeded limit will show a warning',
#	PRIMARY KEY ( `customerid` , `nutrition_type_id` )
#) ENGINE = InnoDB;

ALTER TABLE `customer_limits` ADD `nutrition_type_id` INT( 2 ) UNSIGNED NOT NULL COMMENT 'matches the nutrition_types table id' AFTER `customerid`;
ALTER TABLE `customer_limits` ADD `display` BOOLEAN NOT NULL COMMENT 'display in the week listing' AFTER `type`;
ALTER TABLE `customer_limits` CHANGE `value` `value` DOUBLE NULL COMMENT 'exceeded limit will show a warning';

UPDATE `customer_limits`
SET `nutrition_type_id` = `type`
;

ALTER TABLE `customer_limits` DROP `type`;

ALTER TABLE `customer_limits` ADD UNIQUE (
	`customerid` ,
	`nutrition_type_id`
);

ALTER TABLE `nutrition_types` ADD `sort_order` INT( 2 ) NOT NULL AFTER `column_key` ;
ALTER TABLE `nutrition_types` ADD `is_deleted` BOOLEAN NOT NULL DEFAULT '0' AFTER `sort_order`;

UPDATE `nutrition_types`
SET `column_key` = 'treat_score'
WHERE `column_key` = 'Treat Score';

UPDATE `nutrition_types`
SET `sort_order` = -10
WHERE `column_key` IN (
	'calories'
);

UPDATE `nutrition_types`
SET `sort_order` = -9
WHERE `column_key` IN (
	'cal_from_fat'
	,'total_fat'
);

UPDATE `nutrition_types`
SET `sort_order` = -8
WHERE `column_key` IN (
	'sat_fat'
);

UPDATE `nutrition_types`
SET `sort_order` = -7
WHERE `column_key` IN (
	'cholesterol'
	,'sodium'
);

UPDATE `nutrition_types`
SET `sort_order` = -6
WHERE `column_key` IN (
	'potassium'
);

UPDATE `nutrition_types`
SET `sort_order` = -5
WHERE `column_key` IN (
	'complex_cabs'
	,'fiber'
	,'sugar'
);

UPDATE `nutrition_types`
SET `sort_order` = -4
WHERE `column_key` IN (
	'protein'
	,'vit_A'
	,'vit_C'
);

UPDATE `nutrition_types`
SET `sort_order` = -3
WHERE `column_key` IN (
	'thiamine'
);

UPDATE `nutrition_types`
SET `sort_order` = -2
WHERE `column_key` IN (
	'iron'
	,'treat_score'
);

UPDATE `nutrition_types`
SET `is_deleted` = 1
WHERE `sort_order` = 0
;




