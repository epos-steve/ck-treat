alter table smartMenuMenuItems add column menuOrder smallint unsigned not null default '0';
alter table smartMenuPackageLinks add constraint packageFile_fk
	foreign key ( packageFile )
	references smartMenuPackages( packageFile )
	on update cascade
	on delete cascade;
alter table smartMenuControl add column `timestamp` timestamp not null default current_timestamp;
alter table smartMenuControl add column processed tinyint(1) not null default 0;
alter table smartMenuAuth add column loginid int unsigned default null;
