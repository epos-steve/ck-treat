
# business_settings
DROP TABLE IF EXISTS `business_setting`;
CREATE TABLE `business_setting` (
	`business_id` int(10) unsigned NOT NULL COMMENT 'matches business_id from business table'
	,`page_title` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'default page title or appended to $page_title'
	,`layout_template` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT 'the layout template to use'
	,`policy_dir` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT 'directory for policy controller templates'
	,`site_warning` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'info to output if the user is using the wrong payment site'
	,PRIMARY KEY (`business_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci
;

INSERT INTO `business_setting` (
	`business_id`
	,`page_title`
	,`layout_template`
	,`policy_dir`
	,`site_warning`
)
VALUES
	(   2, ':: Treat America Food Services :: Secure Transaction', 'tafs.tpl', 'tafs', 'Welcome!  We’d like to invite you to log in through the new front door to access your personal account.  Please go to <a href="http://www.tafsschools.com">www.tafsschools.com</a>.  Bookmark this site for handy reference.' )
	,( 16, ':: Treat America Food Services :: Secure Transaction', 'tafs.tpl', 'tafs', 'Welcome!  We’d like to invite you to log in through the new front door to access your personal account.  Please go to <a href="http://www.tafsschools.com">www.tafsschools.com</a>.  Bookmark this site for handy reference.' )
	,( 19, ':: Treat America Food Services :: Secure Transaction', 'tafs.tpl', 'tafs', 'Welcome!  We’d like to invite you to log in through the new front door to access your personal account.  Please go to <a href="http://www.tafsschools.com">www.tafsschools.com</a>.  Bookmark this site for handy reference.' )
	,( 444, ':: Bistro Kids :: Secure Transaction', 'bistro-kids.tpl', 'bistro-kids', 'Welcome!  We’d like to invite you to log in through the new front door for Bistro Kids... whenever you want to log in to your account, please go to <a href="http://www.bistrokidsonline.com">www.bistrokidsonline.com</a>.  Bookmark this site for handy reference.' )
	,( 824, ':: Bistro Kids :: Secure Transaction', 'bistro-kids.tpl', 'bistro-kids', 'Welcome!  We’d like to invite you to log in through the new front door for Bistro Kids... whenever you want to log in to your account, please go to <a href="http://www.bistrokidsonline.com">www.bistrokidsonline.com</a>.  Bookmark this site for handy reference.' )
#	,( 307, ':: Company Kitchen :: Secure Transaction', 'application.tpl', '', 'You should use the <a href="https://payment.companykitchen.com/">payment.companykitchen.com</a> site...' )
;


SELECT *
FROM `business`
WHERE
	`businessname` LIKE '%Thomas Aquinas%'
	OR `businessname` LIKE '%James%'
	OR `businessname` LIKE '%Bishop Miege%'
	OR `businessname` LIKE '%Donnelly%'
	OR `businessname` LIKE '%Theresa%'
	OR (
		`companyid` IN ( 1, 2 )
		AND `state` = 'MO'
		AND `businessname` LIKE '%T%sa%'
	)
;

