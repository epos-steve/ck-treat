ALTER TABLE company ADD COLUMN business_hierarchy INT NULL DEFAULT NULL;
UPDATE company SET business_hierarchy = 4 WHERE companyid = 7; /* Make the default for KC */