delimiter //

# create backups
drop procedure if exists migrate_createbackuptables//
create procedure migrate_createbackuptables()
begin
	drop table if exists migrate_promo_groups;
	create table migrate_promo_groups like promo_groups;
	
	drop table if exists migrate_promo_groups_detail;
	create table migrate_promo_groups_detail like promo_groups_detail;
end//

drop procedure if exists migrate_backuptables//
create procedure migrate_backuptables()
begin
	insert into migrate_promo_groups
		select * from promo_groups pgr;
	
	insert into migrate_promo_groups_detail
		select * from promo_groups_detail pgdr;
end//

delimiter ;

# update defaults for ck
replace into aeris_settings( businessid, registryKey, registryValue ) values
	( 0, "vivipos.fec.settings.layout.aerispos.EPayBranding", "CK" ),
	( 0, "vivipos.fec.settings.layout.aerispos.EPayBrandingLong", "Company Kitchen" ),
	( 0, "vivipos.fec.settings.layout.aerispos.noReceiptHeader", 0 ),
	( 0, "vivipos.fec.settings.layout.aerispos.receiptHeaderImage", "chrome://aerispos/content/ta-logo.bin" );

# create backup tables
call migrate_createbackuptables();

# lock tables
lock tables
	promo_groups as pgr read,
	promo_groups_detail as pgdr read,
	migrate_promo_groups write,
	migrate_promo_groups_detail write;

# backup tables
call migrate_backuptables();

# cleanup
unlock tables;
drop procedure migrate_createbackuptables;
drop procedure migrate_backuptables;



###################################################################################################3



set autocommit = 0;
start transaction;

delimiter //

# migrate items to product grid
drop procedure if exists migrate_productgrid//
create procedure migrate_productgrid()
begin
	declare done int default false;
	declare bid, plucols, deptcols int;
	
	declare bid_cur cursor for
		select 
			b.businessid businessid,
			coalesce( aes_plucols.registryValue, aes_plucols_default.registryValue ) plucols,
			coalesce( aes_deptcols.registryValue, aes_deptcols_default.registryValue ) deptcols
		from business b
		left join aeris_settings aes_plucols
			on b.businessid = aes_plucols.businessid
			and aes_plucols.registryKey = "vivipos.fec.settings.PluCols"
		left join aeris_settings aes_plucols_default
			on aes_plucols_default.businessid = 0
			and aes_plucols_default.registryKey = "vivipos.fec.settings.PluCols"
		left join aeris_settings aes_deptcols
			on b.businessid = aes_deptcols.businessid
			and aes_deptcols.registryKey = "vivipos.fec.settings.DepartmentCols"
		left join aeris_settings aes_deptcols_default
			on aes_deptcols_default.businessid = 0
			and aes_deptcols_default.registryKey = "vivipos.fec.settings.DepartmentCols";
	declare continue handler for not found set done = true;
	
	open bid_cur;
	
	bid_loop: loop
		fetch bid_cur into bid, plucols, deptcols;
		if done then
			leave bid_loop;
		end if;
		
		update promo_groups set
			display_y = floor( display_order / deptcols ),
			display_x = display_order % deptcols
		where businessid = bid
		and visible = 1
		and display_y is null
		and display_x is null;
		
		replace into kiosk_sync( client_programid, item_id, item_type )
			select distinct
				cp.client_programid as client_programid,
				pgr.id as item_id,
				6 as item_type
			from promo_groups pgr
			join mburris_manage.client_programs cp
				on cp.businessid = pgr.businessid
			where pgr.businessid = bid;
		
		update promo_groups_detail set
			display_y = floor( display_order / plucols ),
			display_x = display_order % plucols
		where promo_group_id in (
			select id from promo_groups pgr where businessid = bid and visible = 1
		)
		and display_y is null
		and display_x is null;
		
		replace into kiosk_sync( client_programid, item_id, item_type )
			select distinct
				cp.client_programid as client_programid,
				pgdr.product_id as item_id,
				1 as item_type
			from promo_groups_detail pgdr
			join promo_groups pgr
				on pgdr.promo_group_id = pgr.id
			join mburris_manage.client_programs cp
				on cp.businessid = pgr.businessid
			where pgr.businessid = bid;
	end loop;
end//

# migrate dept screens to groups
drop procedure if exists migrate_creategroup//
create procedure migrate_creategroup()
begin
	declare done int default false;
	declare bid, plucols, deptcols int;
	declare pgid int;
	
	declare bid_cur cursor for
		select distinct
			b.businessid as businessid
		from business b
		left join promo_groups pgr
			on b.businessid = pgr.businessid
			and pgr.visible = 1
		left join menu_items_new mitn
			on b.businessid = mitn.businessid
			and mitn.active = 1
			and mitn.is_button = 1
		where pgr.id is null
		and mitn.id is not null;
	declare continue handler for not found set done = true;
	
	open bid_cur;
	
	bid_loop: loop
		fetch bid_cur into bid;
		if done then
			leave bid_loop;
		end if;
		
		insert into promo_groups( businessid, uuid, name, visible, routing, display_order, display_y, display_x, pos_color )
			values( bid, uuid(), "Kiosk Display Group", 1, 0, 0, null, null, 0 );
		select last_insert_id() into pgid;
		
		replace into kiosk_sync( client_programid, item_id, item_type )
			select distinct
				cp.client_programid as client_programid,
				pgid as item_id,
				6 as item_type
			from mburris_manage.client_programs cp
			where cp.businessid = bid;
			
		
		select @pgcounter:=-1;
		
		insert into promo_groups_detail( product_id, promo_group_id, display_order, display_y, display_x, pos_color )
			select
				mitn.id as product_id,
				pgid as promo_group_id,
				@pgcounter := @pgcounter + 1 as display_order,
				null as display_y,
				null as display_x,
				0 as pos_color
			from menu_items_new mitn
			where mitn.businessid = bid
			and mitn.active = 1
			and mitn.is_button = 1
			order by mitn.name;
		
		replace into kiosk_sync( client_programid, item_id, item_type )
			select distinct
				cp.client_programid as client_programid,
				pgdr.product_id as item_id,
				1 as item_type
			from promo_groups_detail pgdr
			join promo_groups pgr
				on pgdr.promo_group_id = pgr.id
			join mburris_manage.client_programs cp
				on cp.businessid = pgr.businessid
			where pgr.businessid = bid
			and pgr.id = pgid;
	end loop;
end//

delimiter ;

# lock tables
lock tables 
	promo_groups as pgr read,
	promo_groups write,
	promo_groups_detail as pgdr read,
	promo_groups_detail write,
	business as b read,
	kiosk_sync write,
	mburris_manage.client_programs as cp read,
	aeris_settings as aes_plucols read,
	aeris_settings as aes_plucols_default read,
	aeris_settings as aes_deptcols read,
	aeris_settings as aes_deptcols_default read,
	menu_items_new as mitn read;

# call migrate procedures
call migrate_creategroup();
call migrate_productgrid();

# cleanup
unlock tables;
drop procedure migrate_creategroup;
drop procedure migrate_productgrid;

# don't forget to commit transaction !!!!!