
ALTER TABLE `vend_gl_accounts` ADD `is_deleted` SMALLINT NOT NULL DEFAULT '0' AFTER `account_group_id`;

CREATE TABLE `vend_gl_account_xhrefs` (
	`account_id` INT(10) UNSIGNED NOT NULL
	,`business_id` INT(10) UNSIGNED NOT NULL
	,`foreign_id` INT(10) UNSIGNED NOT NULL
	,`table_name` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'name of the table the foreign_id belongs to'
	,`name` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL
	,`old_account_id` INT(10) UNSIGNED NOT NULL
	,PRIMARY KEY ( `account_id`, `business_id`, `table_name`, `foreign_id` )
	,INDEX ( `account_id`, `business_id` )
	,INDEX ( `account_id`, `foreign_id` )
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci
;


INSERT INTO `vend_gl_account_xhrefs` (
	`account_id`
	,`business_id`
	,`foreign_id`
	,`table_name`
	,`name`
	,`old_account_id`
)
SELECT
	vgla2.`id`           AS account_id
	,vgla1.`business_id` AS business_id
	,vgla1.`creditid`    AS foreign_id
	,'creditdetail'      AS table_name
	,vgla1.`name`        AS name
	,vgla1.`id`          AS old_account_id
#	,vgla1.*
FROM `vend_gl_accounts` vgla1
	JOIN `vend_gl_accounts` vgla2
		ON vgla1.`business_id` = vgla2.`business_id`
		AND vgla2.`name` IN (
			'CSV Sales'
			,'Full Service Sales'
			,'Omaha CSV Sales'
			,'Omaha Full Service Sales'
			,'Indiana Full Service Sales'
		)
		AND IF( vgla1.`name` LIKE '%CSV Sales', 'CSV Sales', 'Service Sales' )
			= IF( vgla2.`name` LIKE '%CSV Sales', 'CSV Sales', 'Service Sales' )
WHERE vgla1.`name` LIKE '%CSV Sales'
	OR vgla1.`name` LIKE '%Service Sales'
ORDER BY `business_id`
	,vgla2.id
;

UPDATE `vend_gl_accounts`
SET `is_deleted` = 1
WHERE `name` LIKE '%CSV Sales'
	OR `name` LIKE '%Full Service Sales'
;

UPDATE `vend_gl_accounts`
SET `name` = 'CSV Sales'
WHERE `name` = 'Omaha CSV Sales'
;

UPDATE `vend_gl_accounts`
SET `name` = 'Full Service Sales'
WHERE `name` IN (
	'Omaha Full Service Sales'
	,'Indiana Full Service Sales'
);

UPDATE `vend_gl_accounts`
SET `is_deleted` = 0
WHERE `name` = 'CSV Sales'
	OR `name` = 'Full Service Sales'
;


