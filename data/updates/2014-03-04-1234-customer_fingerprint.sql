CREATE TABLE `customer_fingerprint` (
	scancode VARCHAR(20) NOT NULL PRIMARY KEY,
	fpdate DATETIME NOT NULL,
	fpdata TEXT NOT NULL,
	FOREIGN KEY scancode_fk(scancode) REFERENCES customer(scancode) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=INNODB CHARSET=utf8 COLLATE utf8_unicode_ci;
