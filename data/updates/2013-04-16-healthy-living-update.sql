
UPDATE `business_setting`
SET `home_page_logo` = '/assets/images/menu/rc-header.jpg'
WHERE `home_page_logo` = '/menu/rc-header.jpg'
;

ALTER TABLE `menu_items` ADD `healthy_living` BOOLEAN NOT NULL DEFAULT FALSE COMMENT 'whether this meets the requirements for the Healthy Living Program' AFTER `heart_healthy`
;

