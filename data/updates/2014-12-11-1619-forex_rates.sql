DROP TABLE IF EXISTS forex_rates;
CREATE TABLE forex_rates(
  `timestamp` DATE,
  currency CHAR(4) NOT NULL,
  rate DECIMAL(14, 10) NOT NULL,
  PRIMARY KEY (`timestamp`, currency)
) ENGINE=INNODB;
