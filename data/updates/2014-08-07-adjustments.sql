CREATE TABLE `adjustment` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `loginid` int(10) NOT NULL,
  `customerid` int(10) NOT NULL,
  `businessid` int(10) NOT NULL,
  `date_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `date_time` (`date_time`),
  KEY `customerid` (`customerid`),
  KEY `businessid` (`businessid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `adjustment_detail` (
  `adj_id` int(10) NOT NULL,
  `type` tinyint(1) NOT NULL,
  `account` int(10) NOT NULL,
  `amount` double NOT NULL,
  KEY `adj_id` (`adj_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `customer_transactions_business` (
  `ct_id` int(10) NOT NULL,
  `businessid` int(10) NOT NULL,
  PRIMARY KEY (`ct_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



