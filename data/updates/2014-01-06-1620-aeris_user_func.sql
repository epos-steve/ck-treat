CREATE TABLE aeris_user_function(
  id INTEGER KEY AUTO_INCREMENT,
  customerid INTEGER NOT NULL,
  name VARCHAR(32) NOT NULL,
  controller VARCHAR(32) NOT NULL,
  method VARCHAR(64) NOT NULL,
  args VARCHAR(1024),

  FOREIGN KEY (customerid) REFERENCES customer(customerid) ON DELETE CASCADE
) ENGINE=INNODB CHARACTER SET utf8 COLLATE utf8_unicode_ci;
