
ALTER TABLE promotions ADD category int DEFAULT 1 NOT NULL;

ALTER TABLE promo_detail ADD combo_item_discount double NULL;
ALTER TABLE promo_detail ADD combo_item_apply_discount tinyint NULL;
ALTER TABLE promo_detail ADD combo_item_rows_cols SMALLINT NULL;
ALTER TABLE promo_detail ADD combo_item_rows_cols_type tinyint NULL;
ALTER TABLE promo_detail ADD combo_item_pos_color SMALLINT UNSIGNED NULL;
