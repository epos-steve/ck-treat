replace into aeris_settings( businessid, registryKey, registryValue ) values
	( 0, 'vivipos.fec.settings.static_condiments.cols', '0' ),
	( 0, 'vivipos.fec.settings.static_condiments.rows', '1' ),
	( 0, 'vivipos.fec.settings.static_condiments.hideScrollbar', '1' ),
	( 0, 'vivipos.fec.settings.static_condiments.relation_element', 'categoryPanelContainer' ),
	( 0, 'vivipos.fec.settings.static_condiments.relation_direction', 'after' );

replace into aeris_settings_type values
	( 'vivipos.fec.settings.static_condiments.cols', 'int' ),
	( 'vivipos.fec.settings.static_condiments.rows', 'int' ),
	( 'vivipos.fec.settings.static_condiments.hideScrollbar', 'bool' );

drop table if exists aeris_modifier_static;
create table aeris_modifier_static(
	staticId int unsigned key not null auto_increment,
	businessid int not null,
	modifierGroupId int unsigned not null,
	display_x smallint default null,
	display_y smallint default null,
	processed bool not null default false,
	foreign key bid(businessid) references business(businessid) on delete cascade,
	foreign key gid(modifierGroupId) references aeris_modifier_group(id) on delete cascade,
	unique index bid_gid(businessid, modifierGroupId)
) engine=innodb;