ALTER TABLE `transaction_log` 
	ADD COLUMN `refund` TINYINT(1) NOT NULL DEFAULT 0 AFTER `amount`,
	ADD COLUMN `customerId` INT(11) NULL AFTER `refund`;
