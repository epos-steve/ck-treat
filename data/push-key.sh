#!/bin/bash
scp $3 keys/kiosk$1.p* vivipos@$2:/data/profile/
ssh $3 vivipos@$2 "mkdir /data/profile/keys && mv /data/profile/kiosk$1.private.key /data/profile/keys/kiosk.private.key && mv /data/profile/kiosk$1.public.crt /data/profile/keys/kiosk.public.crt"
