# Essential Enterprise - Site

## Configuration

You will need to set up a couple of INI files to get started.

```sh
cp application/confs/default.ini-dist application/confs/default.ini
```

And modify as needed. Please make sure that 'application/confs/default.ini-dist' always has production environment
settings instead of development environment settings.

For development you will also need to make sure the 'database' lines under '[db]' and '[treat]' are uncommented or you
will run into some problems.

If you're running code on php53 or php55 VMs, please use the 192.168.122.8 virtual IP address for the VMs.
If you're running the code somewhere other than a Skynet hosted VM you'll need to use the 192.168.2.8 IP address for
a copy of the production database.

## Composer

We now manage dependencies with composer and not via git submodules.

Via ssh on php53 and php55 there is already a publicly available composer installed at `/usr/local/bin/composer`.
If you have your own environment set up then you'll need to
[download and install composer](https://getcomposer.org/download/) yourself.

To install dependencies you'll need to run:

```sh
composer install
```

On a production environment (or an environment attempting to duplicate production) you will want to use
the --no-dev switch:

```sh
composer install --no-dev
```

