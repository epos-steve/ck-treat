<?php
require_once( dirname( __FILE__ ) . '/../../application/bootstrap.php' );

function calc_recipe( $editid, $numserv = 1, $serving_size = 1, $batch = false, $use_nutrition = false, $top = true ) {
	$output = array(
		'count' => 0,
		'nutrition' => array(),
		'totalprice' => 0,
		'items' => array(),
		'menu_item' => array(),
	);
	
	if ( !$editid ) {
		return $output;
	}
	
	static $depth = 0;
	if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
		$depth++;
		if ( $depth % 2 ) {
			$color = '#0f0';
		}
		else {
			$color = '#f00';
		}
		echo sprintf( '<pre style="outline: 1px solid %s; text-align: left;">', $color );
	}
	
	if ( $batch == 1 ) {
		$query = "SELECT * FROM recipe WHERE menu_itemid = '-$editid' ORDER BY sort, recipeid";
	}
	else {
		$query = "SELECT * FROM recipe WHERE menu_itemid = '$editid' ORDER BY sort, recipeid";
	}
	$result = Treat_DB_ProxyOld::query( $query );
	$num = mysql_numrows( $result );
	$output['count'] = $num;
	if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
		echo '<pre>';
		var_dump( $query );
		echo '</pre>';
	}
	
	$totalprice = 0;
	$totalserv = 0;
	$counter = 1;
	$num--;
	while ( $row = mysql_fetch_assoc( $result ) ) {
		$row = new Treat_ArrayObject( $row );
		$recipeid = $row['recipeid'];
		$inv_itemid = $row['inv_itemid'];
		$rec_num = $row['rec_num'];
		$rec_num = round( $rec_num / $serving_size * $numserv, 2 );
		$rec_size = $row['rec_size'];
		$srv_num = $row['srv_num'];
		$rec_order = $row['rec_order'];
		$sort = $row['sort'];
		
		$query2 = "
			SELECT ii.*
				,n.inv_rec_size
				,n.inv_rec_num
				,n.inv_rec_size2
				,n.inv_rec_num2
			FROM inv_items ii
				LEFT JOIN nutrition n
					ON ii.item_code = n.item_code
			WHERE ii.inv_itemid = '$inv_itemid'
		";
		$result2 = Treat_DB_ProxyOld::query( $query2 );
		$row2 = mysql_fetch_assoc( $result2 );
		$row2 = new Treat_ArrayObject( $row2 );
		if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
			echo '<pre>';
			var_dump( $query2 );
			echo '</pre>';
		}
		
		$supc = $row2['item_code'];
		$inv_itemname = $row2['item_name'];
		$order_size = $row2['order_size'];
		$price = $row2['price'];
		if ( $use_nutrition ) {
			if ( $rec_order == 1 ) {
				$inv_rec_num = $row2['inv_rec_num'];
			}
			elseif ( $rec_order == 2 ) {
				$inv_rec_num = $row2['inv_rec_num2'];
			}
		}
		else {
			if ( $rec_order == 1 ) {
				$inv_rec_num = $row2['rec_num'];
			}
			elseif ( $rec_order == 2 ) {
				$inv_rec_num = $row2['rec_num2'];
			}
		}
		$pack_size = $row2['pack_size'];
		$pack_qty = $row2['pack_qty'];
		$count_units = $row2['units'];
		$item_code = $row2['item_code'];
		
		$batch_recipe = $row2['batch'];
		
		$item = new Treat_ArrayObject();
		$item['id'] = $recipeid;
		$item['recipeid'] = $recipeid;
		$item['counter'] = $counter;
		$item['o_rec_num'] = $row['rec_num'];
		$item['rec_num'] = $rec_num;
		$item['item_name'] = $inv_itemname;
		$item['inv_itemname'] = $inv_itemname;
		$item['inv_itemid'] = $inv_itemid;
		$item['supc'] = $supc;
		$item['batch'] = $batch_recipe;
		$item['batch_recipe'] = $batch_recipe;
		
		//////////////////BATCH RECIPES
		if ( $batch_recipe == 1 ) {
			$batch = calc_recipe( $inv_itemid, $numserv, $serving_size, true, $use_nutrition, false );
#			$newprice = money( $newvalue['totalprice'] / $inv_rec_num * $rec_num );
			$newprice = $batch['totalprice'] / $inv_rec_num * $rec_num;
			$newvalue = $batch['nutrition'];
			
			$nutrition['cholesterol'] += round( $newvalue['cholesterol'] / $inv_rec_num * $rec_num * $numserv, 1 );
			$nutrition['calories'] += round( $newvalue['calories'] / $inv_rec_num * $rec_num * $numserv, 1 );
			$nutrition['cal_from_fat'] += round( $newvalue['cal_from_fat'] / $inv_rec_num * $rec_num * $numserv, 1 );
			$nutrition['fiber'] += round( $newvalue['fiber'] / $inv_rec_num * $rec_num * $numserv, 1 );
			$nutrition['protein'] += round( $newvalue['protein'] / $inv_rec_num * $rec_num * $numserv, 1 );
			$nutrition['carbs'] += round( $newvalue['carbs'] / $inv_rec_num * $rec_num * $numserv, 1 );
			$nutrition['sugar'] += round( $newvalue['sugar'] / $inv_rec_num * $rec_num * $numserv, 1 );
			$nutrition['sodium'] += round( $newvalue['sodium'] / $inv_rec_num * $rec_num * $numserv, 1 );
			$nutrition['unsat_fat'] += round( $newvalue['unsat_fat'] / $inv_rec_num * $rec_num * $numserv, 1 );
			$nutrition['sat_fat'] += round( $newvalue['sat_fat'] / $inv_rec_num * $rec_num * $numserv, 1 );
			$nutrition['total_fat'] += round( $newvalue['total_fat'] / $inv_rec_num * $rec_num * $numserv, 1 );
			$nutrition['potassium'] += round( $newvalue['potassium'] / $inv_rec_num * $rec_num * $numserv, 1 );
		}
		/////////////////NORMAL INGREDIENTS
		else {
			
			///////////nutrition
			//$numserv = $serving_size;
			
			$query24 = "SELECT * FROM nutrition WHERE item_code = '$supc'";
			$result24 = Treat_DB_ProxyOld::query( $query24 );
			$num24 = mysql_numrows( $result24 );
			$row24 = mysql_fetch_assoc( $result24 );
			$row24 = new Treat_ArrayObject( $row24 );
			if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
				echo '<pre>';
				var_dump( $query24 );
				echo '</pre>';
			}
			
			$cholesterol = $row24['cholesterol'];
			$calories = $row24['calories'];
			$cal_from_fat = $row24['cal_from_fat'];
			$fiber = $row24['fiber'];
			$protein = $row24['protein'];
			$carbs = $row24['complex_cabs'];
			$sugar = $row24['sugar'];
			$sodium = $row24['sodium'];
			$unsat_fat = $row24['unsat_fat'];
			$sat_fat = $row24['sat_fat'];
			$total_fat = $row24['total_fat'];
			$potassium = $row24['potassium'];
			
			$nutr_serv_size = $row24['serv_size_hh'];
			$nutr_rec_size = $row24['rec_size'];
			$nutr_rec_size2 = $row24['rec_size2'];
			
			if ( $nutr_rec_size == $rec_size ) {
				$nutrition['cholesterol'] = round( $nutrition['cholesterol'] + ( ( $cholesterol / $nutr_serv_size ) * ( $rec_num / $numserv ) ), 1 );
				$nutrition['calories'] = round( $nutrition['calories'] + ( ( $calories / $nutr_serv_size ) * ( $rec_num / $numserv ) ), 1 );
				$nutrition['cal_from_fat'] = round( $nutrition['cal_from_fat'] + ( ( $cal_from_fat / $nutr_serv_size ) * ( $rec_num / $numserv ) ), 1 );
				$nutrition['fiber'] = round( $nutrition['fiber'] + ( ( $fiber / $nutr_serv_size ) * ( $rec_num / $numserv ) ), 1 );
				$nutrition['protein'] = round( $nutrition['protein'] + ( ( $protein / $nutr_serv_size ) * ( $rec_num / $numserv ) ), 1 );
				$nutrition['carbs'] = round( $nutrition['carbs'] + ( ( $carbs / $nutr_serv_size ) * ( $rec_num / $numserv ) ), 1 );
				$nutrition['sugar'] = round( $nutrition['sugar'] + ( ( $sugar / $nutr_serv_size ) * ( $rec_num / $numserv ) ), 1 );
				$nutrition['sodium'] = round( $nutrition['sodium'] + ( ( $sodium / $nutr_serv_size ) * ( $rec_num / $numserv ) ), 1 );
				$nutrition['unsat_fat'] = round( $nutrition['unsat_fat'] + ( ( $unsat_fat / $nutr_serv_size ) * ( $rec_num / $numserv ) ), 1 );
				$nutrition['sat_fat'] = round( $nutrition['sat_fat'] + ( ( $sat_fat / $nutr_serv_size ) * ( $rec_num / $numserv ) ), 1 );
				$nutrition['total_fat'] = round( $nutrition['total_fat'] + ( ( $total_fat / $nutr_serv_size ) * ( $rec_num / $numserv ) ), 1 );
				$nutrition['potassium'] = round( $nutrition['potassium'] + ( ( $potassium / $nutr_serv_size ) * ( $rec_num / $numserv ) ), 1 );
				
				$show_nutr = "<font color=red>*</font>";
			}
			else {
				$query25 = "SELECT * FROM conversion WHERE sizeid = '$rec_size' AND sizeidto = '$nutr_rec_size'";
				$result25 = Treat_DB_ProxyOld::query( $query25 );
				$num25 = mysql_numrows( $result25 );
				$row25 = mysql_fetch_assoc( $result25 );
				$row25 = new Treat_ArrayObject( $row25 );
				if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
					echo '<pre>';
					var_dump( $query25 );
					echo '</pre>';
				}
				
				if ( $num25 > 0 ) {
					$nutr_convert = $row25['convert'];
					
					$nutrition['cholesterol'] = round( $nutrition['cholesterol'] + ( ( $cholesterol / ( $nutr_serv_size * $nutr_convert ) ) * ( $rec_num / $numserv ) ), 1 );
					$nutrition['calories'] = round( $nutrition['calories'] + ( ( $calories / ( $nutr_serv_size * $nutr_convert ) ) * ( $rec_num / $numserv ) ), 1 );
					$nutrition['cal_from_fat'] = round( $nutrition['cal_from_fat'] + ( ( $cal_from_fat / ( $nutr_serv_size * $nutr_convert ) ) * ( $rec_num / $numserv ) ), 1 );
					$nutrition['fiber'] = round( $nutrition['fiber'] + ( ( $fiber / ( $nutr_serv_size * $nutr_convert ) ) * ( $rec_num / $numserv ) ), 1 );
					$nutrition['protein'] = round( $nutrition['protein'] + ( ( $protein / ( $nutr_serv_size * $nutr_convert ) ) * ( $rec_num / $numserv ) ), 1 );
					$nutrition['carbs'] = round( $nutrition['carbs'] + ( ( $carbs / ( $nutr_serv_size * $nutr_convert ) ) * ( $rec_num / $numserv ) ), 1 );
					$nutrition['sugar'] = round( $nutrition['sugar'] + ( ( $sugar / ( $nutr_serv_size * $nutr_convert ) ) * ( $rec_num / $numserv ) ), 1 );
					$nutrition['sodium'] = round( $nutrition['sodium'] + ( ( $sodium / ( $nutr_serv_size * $nutr_convert ) ) * ( $rec_num / $numserv ) ), 1 );
					$nutrition['unsat_fat'] = round( $nutrition['unsat_fat'] + ( ( $unsat_fat / ( $nutr_serv_size * $nutr_convert ) ) * ( $rec_num / $numserv ) ), 1 );
					$nutrition['sat_fat'] = round( $nutrition['sat_fat'] + ( ( $sat_fat / ( $nutr_serv_size * $nutr_convert ) ) * ( $rec_num / $numserv ) ), 1 );
					$nutrition['total_fat'] = round( $nutrition['total_fat'] + ( ( $total_fat / ( $nutr_serv_size * $nutr_convert ) ) * ( $rec_num / $numserv ) ), 1 );
					$nutrition['potassium'] = round( $nutrition['potassium'] + ( ( $potassium / ( $nutr_serv_size * $nutr_convert ) ) * ( $rec_num / $numserv ) ), 1 );
					
					$show_nutr = "<font color=green>*</font>";
				}
				elseif ( $num24 > 0 ) {
					$nutrition['cholesterol'] = round( $nutrition['cholesterol'] + $cholesterol, 1 );
					$nutrition['calories'] = round( $nutrition['calories'] + $calories, 1 );
					$nutrition['cal_from_fat'] = round( $nutrition['cal_from_fat'] + $cal_from_fat, 1 );
					$nutrition['fiber'] = round( $nutrition['fiber'] + $fiber, 1 );
					$nutrition['protein'] = round( $nutrition['protein'] + $protein, 1 );
					$nutrition['carbs'] = round( $nutrition['carbs'] + $carbs, 1 );
					$nutrition['sugar'] = round( $nutrition['sugar'] + $sugar, 1 );
					$nutrition['sodium'] = round( $nutrition['sodium'] + $sodium, 1 );
					$nutrition['unsat_fat'] = round( $nutrition['unsat_fat'] + $unsat_fat, 1 );
					$nutrition['sat_fat'] = round( $nutrition['sat_fat'] + $sat_fat, 1 );
					$nutrition['total_fat'] = round( $nutrition['total_fat'] + $total_fat, 1 );
					$nutrition['potassium'] = round( $nutrition['potassium'] + $potassium, 1 );
					
					$show_nutr = "<font color=blue>*</font>";
				}
				else {
					$show_nutr = '';
				}
			}
			
			////////////end nutrition
			
			$query2 = "SELECT * FROM conversion WHERE sizeid = '$rec_size' AND sizeidto = '$order_size'";
			$result2 = Treat_DB_ProxyOld::query( $query2 );
			$num2 = mysql_numrows( $result2 );
			$row2 = mysql_fetch_assoc( $result2 );
			$row2 = new Treat_ArrayObject( $row2 );
			if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
				echo '<pre>';
				var_dump( $query2 );
				echo '</pre>';
			}
			
			if ( $num2 != 0 ) {
				$convert = $row2['convert'];
			}
			elseif ( $num2 == 0 ) {
				$convert = $inv_rec_num;
			}
			else {
				$convert = 1;
			}
			
			if ( $convert ) {
				$newprice = $price / $convert * $rec_num;
			}
			else {
				$newprice = 0;
			}
			$item['newprice'] = $newprice;
			$item['price'] = $newprice;
			$item['item_price'] = $newprice;
		}
		
		$totalprice += $newprice;
		
		$query2 = "SELECT * FROM size WHERE sizeid = '$rec_size'";
		$result2 = Treat_DB_ProxyOld::query( $query2 );
		$row2 = mysql_fetch_assoc( $result2 );
		$row2 = new Treat_ArrayObject( $row2 );
		if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
			echo '<pre>';
			var_dump( $query2 );
			echo '</pre>';
		}
		
		$sizename = $row2['sizename'];
		$item['show_nutr'] = $show_nutr;
		$item['sizename'] = $sizename;
		$item['rec_sizename'] = $sizename;
		$item['showsize'] = $sizename;
		
		$totalserv = $totalserv + ( $rec_num * $srv_num );
		
		$num--;
		$counter++;
		
		$items[] = $item;
	}
	if ( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
		echo '</pre>';
	}
	
#	$newvalue="$totalprice~$nutrition[cholesterol]~$nutrition[calories]~$nutrition[cal_from_fat]~$nutrition[fiber]~$nutrition[protein]~$nutrition[carbs]~$nutrition[sodium]~$nutrition[unsat_fat]~$nutrition[sat_fat]~$nutrition[total_fat]~$nutrition[potassium]";
	
#	return $newvalue;
	if ( $nutrition ) {
		$output['nutrition'] = $nutrition;
	}
	$output['totalprice'] = $totalprice;
	if ( $items ) {
		$output['items'] = $items;
	}
	
	if ( $top ) {
		$menu_item = Treat_Model_Menu_Item::getMenuItemById( $editid );
		$tmp = new Treat_ArrayObject();
		$tmp->item_price_calced = $menu_item->price * $menu_item->serving_size;
		$tmp->profit = $tmp->item_price_calced - $output['totalprice'];
		if ( $tmp->item_price_calced ) {
			$tmp->fcprcnt = round( $output['totalprice'] / $tmp->item_price_calced * 100, 1 );
		}
		else {
			$tmp->fcprcnt = 0;
		}
		if ( $menu_item->serving_size ) {
			$tmp->totalprice_per_serving = $output['totalprice'] / $menu_item->serving_size;
			$tmp->unitprice = $tmp->item_price_calced / $menu_item->serving_size;
			$tmp->unitprofit = $tmp->profit / $menu_item->serving_size;
		}
		$output['menu_item'] = $tmp;
	}
	
	
	return $output;
}


