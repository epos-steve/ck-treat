<?php
/*
 *	Pages should have already checked to see if the user could successfully login
 */
header( 'HTTP/1.1 412 Precondition Failed' );
?>
<html>
	<head>
		<title>412 Precondition Failed</title>
	</head>
	<body>
		<h1>412 Precondition Failed</h1>
		
		<p>
			Some sort of precondition failed. This is usually an instance of 
			attempting to view a page while having the wrong Company ID set.
			Please return to the <a href="/ta/businesstrack.php">Main Page</a>
			and try again.
		</p>
	</body>
</html>