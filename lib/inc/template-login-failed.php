<html>
	<head>
		<title>Login Failed</title>
	</head>
	<body>
		<center>
			<h3>Login Failed</h3>
			<p>
				Use your browser's back button to try again or 
				go back to the <a href="<?php
					if ( !isset( $location ) ) {
						$location = '/';
					}
					echo $location;
				?>">login page</a>.
			</p>
		</center>
	</body>
</html>
