<?php
/**
 *	This file should help the redundant duplication of login scripts that are all over the place.
 *	If you have to use a cookie login, please use this instead of copy & paste.
 *
 *	Created on Sep 16, 2010
 *	@version $Id$
 */

if ( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname( __FILE__ ) . '/../../htdocs/' ) );
}
require_once( DOC_ROOT . '/bootstrap.php' );

$GLOBALS['user'] = \EE\Controller\Base::getSessionCookieVariable( 'usercook' );
$GLOBALS['pass'] = \EE\Controller\Base::getSessionCookieVariable( 'passcook' );

$GLOBALS['businessid'] = \EE\Controller\Base::getPostOrGetVariable( array( 'bid', 'businessid', 'businessid' ) );
$GLOBALS['companyid'] = \EE\Controller\Base::getPostOrGetVariable( array( 'cid', 'companyid', 'companyid' ) );

$GLOBALS['editid'] = \EE\Controller\Base::getGetVariable( 'id' );

if ( $GLOBALS['businessid'] == '' && $GLOBALS['companyid'] == '' ) {
	$GLOBALS['businessid'] = \EE\Controller\Base::getPostVariable( 'businessid' );
	$GLOBALS['companyid'] = \EE\Controller\Base::getPostVariable( 'companyid' );
}

$GLOBALS['finditem'] = \EE\Controller\Base::getPostVariable( 'finditem' );

$GLOBALS['num'] = 0;
\EE\Model\User\Login::login( $GLOBALS['user'], $GLOBALS['pass'], true );
if ( $_SESSION->getUser() ) {
	$GLOBALS['num'] = 1;
	$GLOBALS['USER'] = $_SESSION->getUser();

	$GLOBALS['security_level'] = $_SESSION->getUser()->security_level;
	$GLOBALS['mysecurity']     = $_SESSION->getUser()->security;
	$GLOBALS['bid']            = $_SESSION->getUser()->businessid;
	$GLOBALS['cid']            = $_SESSION->getUser()->companyid;
	$GLOBALS['bid2']           = $_SESSION->getUser()->busid2;
	$GLOBALS['bid3']           = $_SESSION->getUser()->busid3;
	$GLOBALS['bid4']           = $_SESSION->getUser()->busid4;
	$GLOBALS['bid5']           = $_SESSION->getUser()->busid5;
	$GLOBALS['bid6']           = $_SESSION->getUser()->busid6;
	$GLOBALS['bid7']           = $_SESSION->getUser()->busid7;
	$GLOBALS['bid8']           = $_SESSION->getUser()->busid8;
	$GLOBALS['bid9']           = $_SESSION->getUser()->busid9;
	$GLOBALS['bid10']          = $_SESSION->getUser()->busid10;
	$GLOBALS['loginid']        = $_SESSION->getUser()->loginid;

	$GLOBALS['pr1']            = $_SESSION->getUser()->payroll;
	$GLOBALS['pr2']            = $_SESSION->getUser()->pr2;
	$GLOBALS['pr3']            = $_SESSION->getUser()->pr3;
	$GLOBALS['pr4']            = $_SESSION->getUser()->pr4;
	$GLOBALS['pr5']            = $_SESSION->getUser()->pr5;
	$GLOBALS['pr6']            = $_SESSION->getUser()->pr6;
	$GLOBALS['pr7']            = $_SESSION->getUser()->pr7;
	$GLOBALS['pr8']            = $_SESSION->getUser()->pr8;
	$GLOBALS['pr9']            = $_SESSION->getUser()->pr9;
	$GLOBALS['pr10']           = $_SESSION->getUser()->pr10;
}

	$page_business = \EE\Model\Business\Singleton::getSingleton();
	if ( !$page_business ) {
		$page_business = \EE\Model\Company\Singleton::getSingleton();
		if ( !$page_business ) {
			$page_business = new \EE\ArrayObject();
		}
	}

	$GLOBALS['businessname']    = $page_business->businessname;
#	$GLOBALS['businessid']      = $page_business->businessid;
	$GLOBALS['street']          = $page_business->street;
	$GLOBALS['city']            = $page_business->city;
	$GLOBALS['state']           = $page_business->state;
	$GLOBALS['zip']             = $page_business->zip;
	$GLOBALS['phone']           = $page_business->phone;
	$GLOBALS['bus_unit']        = $page_business->unit;
	$GLOBALS['unit_num']        = $page_business->unit;
	$GLOBALS['over_short_acct'] = $page_business->over_short_acct;
	$GLOBALS['count1']          = $page_business->count1;
	$GLOBALS['count2']          = $page_business->count2;
	$GLOBALS['count3']          = $page_business->count3;
	$GLOBALS['benefitprcnt']    = $page_business->benefit;
	$GLOBALS['operate']         = $page_business->operate;
	$GLOBALS['population']      = $page_business->population;
	$GLOBALS['contract']        = $page_business->contract;
	$GLOBALS['import_pl']       = $page_business->import_pl;

#var_dump( $GLOBALS );
#var_dump( $_SESSION );
if (
	$GLOBALS['num'] != 1
	|| $GLOBALS['user'] == ''
	|| $GLOBALS['pass'] == ''
#	|| $_SESSION->getUser()->isLoginTempExpired()
) {
	$temp_uri = new \EE\Uri();
	$location = '/?page=' . \urlencode( (string)$temp_uri );
	include_once( 'inc/template-login-failed.php' );
	unset( $temp_uri );
	unset( $location );
	return false;
}
/*
elseif ( $_SESSION->getUser()->isPasswordExpired() ) {
	setcookie( 'usercook', $user );
	setcookie( 'passcook', $pass );
	$location = '/ta/editpass.php?ex=1&edit=1';
	header( 'Location: ' . $location );
	exit();
	return false;
}
*/
else {
	return true;
}


