<?php
if ( defined( 'DOC_ROOT' ) ) {
	require_once( DOC_ROOT . '/bootstrap.php' );
}
else if ( defined('SITECH_APP_PATH') ) {
	require_once( realpath(SITECH_APP_PATH . '/bootstrap.php') );
}
else {
	require_once( realpath( dirname( __FILE__ ) . '/../../application/bootstrap.php' ) );
}

if( !function_exists( 'isLeapYear' ) ) {
	# This function name doesn't make sense considering what it does - would seem
	# to return boolean, but is expected to return the # of days in Feb O.o
	function isLeapYear($year) {
		return \Essential\Treat\Lib\FunctionCrap::getDaysInFebruary(
			$year
		);
	}
}

if( !function_exists( 'arrayToJson' ) ) {
	function arrayToJson( $array )
	{
		return \Essential\Treat\Lib\FunctionCrap::arrayToJson(
			$array
		);
	}
}

if(!function_exists('check_mobile')) {
	function checkmobile(){
		return \Essential\Treat\Lib\FunctionCrap::checkmobile();
	}
}

if(!function_exists('money')) {
	function money($diff){
		return \Essential\Treat\Lib\FunctionCrap::money( $diff );
	}
}

if (!function_exists('nextday')){
	function nextday($nextd, $day_format = '')
	{
		return \Essential\Treat\Lib\FunctionCrap::nextday(
			$nextd
			,$day_format
		);
	}
}

if(!function_exists('prevday')) {
	function prevday($prevd,$day_format=''){
		return \Essential\Treat\Lib\FunctionCrap::prevday(
			$prevd
			,$day_format
		);
	}
}

if(!function_exists('dayofweek')){
	function dayofweek($date1)
	{
		return \Essential\Treat\Lib\FunctionCrap::dayofweek( $date1 );
	}
}

if(!function_exists('futureday')){
	function futureday($num) {
		return \Essential\Treat\Lib\FunctionCrap::futureday( $num );
	}
}
if(!function_exists('pastday')){
	function pastday($num) {
		return \Essential\Treat\Lib\FunctionCrap::pastday( $num );
	}
}

if(!function_exists('creategraph')){
	function creategraph($sales,$budget,$color,$showtitle){
		return \Essential\Treat\Lib\FunctionCrap::creategraph(
			$sales
			,$budget
			,$color
			,$showtitle
		);
	}
}
if(!function_exists('hgraph')){
	function hgraph($number,$total,$color,$width,$diff,$bgcolor,$showpercent,$divid){
		return \Essential\Treat\Lib\FunctionCrap::hgraph(
			$number
			,$total
			,$color
			,$width
			,$diff
			,$bgcolor
			,$showpercent
			,$divid
		);
	}
}

if (!defined('TREAT_BASEURL')) define('TREAT_BASEURL', 'http://treatamerica.essentialpos.com/ta/');
if (!defined('TREAT_DEV')) define('TREAT_DEV', false); // Change to true for a development server

if (!function_exists('google_page_track')) {
	function google_page_track(){
		return \Essential\Treat\Lib\FunctionCrap::google_page_track();
	}
}

if( !function_exists( 'rndm_color_code' ) ) {
	function rndm_color_code($b_safe = TRUE) {
		return \Essential\Treat\Lib\FunctionCrap::rndm_color_code(
			$b_safe
		);
	}
}
