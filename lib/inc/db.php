<?php
if ( !defined( 'LEGACY' ) ) {
	trigger_error(
		'The file you are viewing has required \'db.php\' which means it\'s definitely LEGACY code that needs
		to be fixed and/or replaced.'
		,E_USER_WARNING
	);
}
require_once dirname( __FILE__ ) . '/../../application/bootstrap.php';

$GLOBALS['dbuser'] = $username = $dbuser = 'mburris_user';
$GLOBALS['dbpass'] = $password = $dbpass = 'bigdog';
$GLOBALS['dbname'] = $database = $dbname = 'mburris_businesstrack';
$GLOBALS['dbhost'] = $dbhost = (IN_PRODUCTION) ? '192.168.1.50' : 'You need to use Treat_DB_ProxyOld';
//$dbhost = '192.168.1.9';
