<?php

if(!function_exists('isLeapYear')) {
function isLeapYear($year) 
{ 
	/*
	# Check for valid parameters # 
	if (!is_int($year) || $year < 0) 
	{ 
		printf('Wrong parameter for $year in function isLeapYear. It must be a positive integer.'); 
		exit(); 
	} 
	*/

	# In the Gregorian calendar there is a leap year every year divisible by four 
	# except for years which are both divisible by 100 and not divisible by 400. 

	if ($year % 4 != 0) 
	{ 
		return 28; 
	} 
	else 
	{ 
		if ($year % 100 != 0) 
		{ 
			return 29;    # Leap year 
		} 
		else 
		{ 
			if ($year % 400 != 0) 
			{ 
				return 28; 
			} 
			else 
			{ 
				return 29;    # Leap year 
			} 
		} 
	} 
} 
}

if(!function_exists('check_mobile')) {
	function checkmobile(){
		/*
		 echo $_SERVER["HTTP_USER_AGENT"];
		 echo "<br>";
		 echo $_SERVER["HTTP_X_WAP_PROFILE"];
		 echo "<br>";
		 echo $_SERVER["HTTP_ACCEPT"];
		 * */

		if(isset($_SERVER["HTTP_X_WAP_PROFILE"])) return true;

		if(preg_match("/wap\.|\.wap/i",$_SERVER["HTTP_ACCEPT"])) return true;

		if(isset($_SERVER["HTTP_USER_AGENT"])){

			if(preg_match("/Creative\ AutoUpdate/i",$_SERVER["HTTP_USER_AGENT"])) return false;

			//$uamatches = array("midp", "j2me", "avantg", "docomo", "novarra", "palmos", "palmsource", "240x320", "opwv", "chtml", "pda", "windows\ ce", "mmp\/", "blackberry", "mib\/", "symbian", "wireless", "nokia", "hand", "mobi", "phone", "cdm", "up\.b", "audio", "SIE\-", "SEC\-", "samsung", "HTC", "mot\-", "mitsu", "sagem", "sony", "alcatel", "lg", "erics", "vx", "NEC", "philips", "mmm", "xx", "panasonic", "sharp", "wap", "sch", "rover", "pocket", "benq", "java", "pt", "pg", "vox", "amoi", "bird", "compal", "kg", "voda", "sany", "kdd", "dbt", "sendo", "sgh", "gradi", "jb", "\d\d\di", "moto");
			//$uamatches = array("blackberry", "mobi", "moto", "windows\ ce", "pda", "phone", "samsung");
			$uamatches = array("blackberry", "windows\ ce", "samsung");

			foreach($uamatches as $uastring){
				if(preg_match("/".$uastring."/i",$_SERVER["HTTP_USER_AGENT"])) return true;
			}

		}
		return false;
	}
}

if(!function_exists('money')) {
	function money($diff){
		$findme='.';
		$double='.00';
		$single='0';
		$double2='00';
		$pos1 = strpos($diff, $findme);
		$pos2 = strlen($diff);
		if ($pos1==""){$diff="$diff$double";}
		elseif ($pos2-$pos1==2){$diff="$diff$single";}
		elseif ($pos2-$pos1==1){$diff="$diff$double2";}
		else{}
		$dot = strpos($diff, $findme);
		$diff = substr($diff, 0, $dot+3);
		if ($diff > 0 && $diff < '0.01'){$diff="0.01";}
		elseif($diff < 0 && $diff > '-0.01'){$diff="-0.01";}
		return $diff;
	}
}
if(!function_exists('money')) {
	function money($diff){
		$diff = number_format( floatval( $diff ), 2, '.', '');
		return $diff;
	}
}

if (!function_exists('nextday')){
	function nextday($nextd, $day_format = '')
	{
		if($day_format==""){$day_format="Y-m-d";}
		$monn = substr($nextd, 5, 2);
		$dayn = substr($nextd, 8, 2);
		$yearn = substr($nextd, 0,4);
		$tempdate = date($day_format , mktime(0,0,0, $monn, $dayn+1, $yearn));
		return $tempdate;
	}


}

/*
 *	Several files have a slightly different version of the original version
 *	of this function. As we're still cleaning up I figured I should make a
 *	note as to which files these were:
 *
 *	htdocs/dst/additem.php (older version)
 *	htdocs/dst/addreserve.php
 *	htdocs/dst/caterdetails.php
 *	htdocs/dst/caterprint.php
 *	htdocs/dst/catertrack2.php (older version)
 *	htdocs/dst/catertrack.php (older version)
 *	htdocs/dst/createorder.php (older version)
 */
if( !function_exists( 'nextday' )){
	function nextday($date2)
	{
		$day=substr($date2,8,2);
		$month=substr($date2,5,2);
		$year=substr($date2,0,4);
		$leap = date("L");

		if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
		{
			if ($month == "01"){$month="02";}
			elseif ($month == "03"){$month="04";}
			elseif ($month == "05"){$month="06";}
			elseif ($month == "07"){$month="08";}
			elseif ($month == "08"){$month="09";}
			elseif ($month == "10"){$month="11";}
			$day='01';
			$tomorrow = "$year-$month-$day";
			return $tomorrow;
		}
		else if ($month=='02' && $day == '29' && $leap == '1')
		{
			$month='03';
			$day='01';
			$tomorrow = "$year-$month-$day";
			return $tomorrow;
		}
		else if ($month=='02' && $day == '28' && $leap == '0')
		{
			$month='03';
			$day='01';
			$tomorrow = "$year-$month-$day";
			return $tomorrow;
		}
		else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
		{
			if ($month == "04"){$month="05";}
			if ($month == "06"){$month="07";}
			if ($month == "09"){$month="10";}
			if ($month == "11"){$month="12";}
			$day='01';
			$tomorrow = "$year-$month-$day";
			return $tomorrow;
		}
		else if ($month=='12' && $day=='31')
		{
			$day='01';
			$month='01';
			$year++;
			$tomorrow = "$year-$month-$day";
			return $tomorrow;
		}
		else
		{
			$day=$day+1;
			if ($day<10){$day="0$day";}
			$tomorrow="$year-$month-$day";
			return $tomorrow;
		}
	}
}

if(!function_exists('prevday')) {

	function prevday($prevd,$day_format=''){ //Function returns previous day of passed date and formats accordingly.
		if($day_format==""){$day_format="Y-m-d";}
		$monp = substr($prevd, 5, 2);
		$dayp = substr($prevd, 8, 2);
		$yearp = substr($prevd, 0,4);
		$tempdate = date($day_format , mktime(0,0,0, $monp, $dayp-1, $yearp));
		return $tempdate;
	}
}

/*
 *	Several files have a slightly different version of the original version
 *	of this function. As we're still cleaning up I figured I should make a
 *	note as to which files these were:
 *
 *	htdocs/dst/addreserve.php
 *	htdocs/dst/caterdetails.php
 *	htdocs/dst/caterprint.php
 *	htdocs/dst/catertrack2.php (older version)
 *	htdocs/dst/catertrack.php (older version)
 *	htdocs/dst/createorder.php (older version)
 */
if( !function_exists( 'prevday' )){
	function prevday($date2) {
		$day=substr($date2,8,2);
		$month=substr($date2,5,2);
		$year=substr($date2,0,4);
		$day=$day-1;
		$leap = date("L");

		if ($day <= 0)
		{
			if ($month == 01)
			{
				$month = '12';
				$year--;
				$day=$day+31;
				$yesterday = "$year-$month-$day";
				return $yesterday;
			}
			else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
			{
				$month--;
				if ($month < 10)
				{
					$month="0$month";
				}
				$day=$day+31;
				$yesterday="$year-$month-$day";
				return $yesterday;
			}
			else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
			{
				$month--;
				if ($month < 10)
				{
					$month="0$month";
				}
				$day=$day+30;
				$yesterday="$year-$month-$day";
				return $yesterday;
			}

			elseif ($leap==1&&$month=='03')
			{
				$day=$day+29;
				$month='02';
				$yesterday="$year-$month-$day";
				return $yesterday;
			}
			else
			{
				$day=$day+28;
				$month='02';
				$yesterday="$year-$month-$day";
				return $yesterday;
			}
		}
		else
		{
			if ($day < 10)
			{
				$day="0$day";
			}
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
	}
}

if(!function_exists('dayofweek')){
	function dayofweek($date1)
	{
		$day=substr($date1,8,2);
		$month=substr($date1,5,2);
		$year=substr($date1,0,4);
		$dayofweek = date("l", mktime(0, 0, 0, $month, $day, $year));
		return $dayofweek;
	}
}

if(!function_exists('futureday')){
	function futureday($num) {
		$day = date("d");
		$year = date("Y");
		$month = date("m");
		$leap = date("L");
		$day=$day+$num;

		if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
		{
			if ($month == "03"){$month="04";}
			if ($month == "05"){$month="06";}
			if ($month == "07"){$month="08";}
			if ($month == "08"){$month="09";}
			$day=$day-31;
			if ($day<10){$day="0$day";}
			$tomorrow = "$year-$month-$day";
			return $tomorrow;
		}
		else if ($month=='02' && $leap == '1' && $day >= '29')
		{
			$month='03';
			$day=$day-29;
			if ($day<10){$day="0$day";}
			$tomorrow = "$year-$month-$day";
			return $tomorrow;
		}
		else if ($month=='02' && $leap == '0' && $day >= '28')
		{
			$month='03';
			$day=$day-28;
			if ($day<10){$day="0$day";}
			$tomorrow = "$year-$month-$day";
			return $tomorrow;
		}
		else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
		{
			if ($month == "04"){$month="05";}
			if ($month == "06"){$month="07";}
			if ($month == "09"){$month="10";}
			if ($month == "11"){$month="12";}
			$day=$day-30;
			if ($day<10){$day="0$day";}
			$tomorrow = "$year-$month-$day";
			return $tomorrow;
		}
		else if ($month==12 && $day>=32)
		{
			$day=$day-31;
			if ($day<10){$day="0$day";}
			$month='01';
			$year++;
			$tomorrow = "$year-$month-$day";
			return $tomorrow;
		}
		else
		{
			if ($day<10){$day="0$day";}
			$tomorrow="$year-$month-$day";
			return $tomorrow;
		}
	}
}
if(!function_exists('pastday')){
	function pastday($num) {
		$day = date("d");
		$day=$day-$num;
		if ($day <= 0)
		{
			$year = date("Y");
			$month = date("m");
			if ($month == 01)
			{
				$month = '12';
				$year--;
				$day=$day+31;
				$yesterday = "$year-$month-$day";
				return $yesterday;
			}
			else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
			{
				$month--;
				if ($month < 10)
				{
					$month="0$month";
				}
				$day=$day+31;
				$yesterday="$year-$month-$day";
				return $yesterday;
			}
			else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
			{
				$month--;
				if ($month < 10)
				{
					$month="0$month";
				}
				$day=$day+30;
				$yesterday="$year-$month-$day";
				return $yesterday;
			}
			else
			{
				$day=$day+28;
				$month='02';
				$yesterday="$year-$month-$day";
				return $yesterday;
			}
		}
		else
		{
			if ($day < 10)
			{
				$day="0$day";
			}
			$year=date("Y");
			$month=date("m");
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
	}
}

if(!function_exists('creategraph')){
	function creategraph($sales,$budget,$color,$showtitle){

		if($budget!=0){
			$diff=round($sales/($budget*2)*200,0);
			if($diff>200){$diff=200;}
		}
		elseif($sales>0){$diff=200;}
		else{$diff=0;}

		if($sales>=$budget&&$sales!=0){$graphcolor=$color;}
		else{$graphcolor="#CCCCCC";}

		$showdiff=number_format($sales-$budget,2);
		if($showdiff>0){$showdiff="+$showdiff";}

		//if($showtitle==1){$showbudg=number_format($budget,2);$showavg="Daily Average: $showbudg";}
		//else{$showbudg=number_format($budget,2);$showavg="Budget: $showbudg";}

		if($showtitle==1){$showtitle=number_format($sales,2);}
		else{$showtitle=$showdiff;}

		$graph="<table cellspacing=0 cellpadding=0 width=200 title='$showtitle'>";
		$graph="$graph<tr height=2 bgcolor=#EEEEEE><td colspan=200><img src=clear.gif height=1 width=1></td></tr>";
		$graph="$graph<tr height=2 bgcolor=#EEEEEE><td colspan=99><img src=clear.gif height=1 width=1></td><td colspan=2 bgcolor=$graphcolor title='$showavg'><img src=clear.gif height=1 width=1></td><img src=clear.gif height=1 width=1><td colspan=99><img src=clear.gif height=1 width=1></td></tr>";
		if($diff<1){
			$graph="$graph<tr height=6><td colspan=99 bgcolor=#EEEEEE><img src=clear.gif height=1 width=1></td><td colspan=2 bgcolor=$graphcolor title='$showavg'><img src=clear.gif height=1 width=1></td><td colspan=99 bgcolor=#EEEEEE><img src=clear.gif height=1 width=1></td></tr>";
		}
		elseif($diff<=98){
			$diff2=99-$diff;
			$graph="$graph<tr height=6><td colspan=$diff bgcolor=#444455><img src=clear.gif height=1 width=1></td><td colspan=$diff2 bgcolor=#EEEEEE><img src=clear.gif height=1 width=1></td><td colspan=2 bgcolor=$graphcolor title='$showavg'><img src=clear.gif height=1 width=1></td><td colspan=99 bgcolor=#EEEEEE><img src=clear.gif height=1 width=1></td></tr>";
		}
		elseif($diff>98&&$diff<102){
			$graph="$graph<tr height=6><td colspan=99 bgcolor=#444455><img src=clear.gif height=1 width=1></td><td colspan=2 bgcolor=$graphcolor title='$showavg'><img src=clear.gif height=1 width=1></td><td colspan=99 bgcolor=#EEEEEE><img src=clear.gif height=1 width=1></td></tr>";
		}
		elseif($diff>=102){
			$diff2=$diff-101;
			if($diff2>99){$diff2=99;}
			$diff3=99-$diff2;
			$graph="$graph<tr height=6><td colspan=99 bgcolor=#444455><img src=clear.gif height=1 width=1></td><td colspan=2 bgcolor=$graphcolor title='$showavg'><img src=clear.gif height=1 width=1></td><td colspan=$diff2 bgcolor=#444455><img src=clear.gif height=1 width=1></td><td colspan=$diff3 bgcolor=#EEEEEE><img src=clear.gif height=1 width=1></td></tr>";
		}
		$graph="$graph<tr height=2 bgcolor=#EEEEEE><td colspan=99><img src=clear.gif height=1 width=1></td><td colspan=2 bgcolor=$graphcolor title='$showavg'><img src=clear.gif height=1 width=1></td><td colspan=99><img src=clear.gif height=1 width=1></td></tr>";
		$graph="$graph<tr height=2 bgcolor=#EEEEEE><td colspan=200><img src=clear.gif height=1 width=1></td></tr></table>";

		return $graph;
	}
}
if(!function_exists('hgraph')){
	function hgraph($number,$total,$color,$width,$diff,$bgcolor,$showpercent,$divid){
		if($divid!=""){$color="white";}

		if($diff!=0){
			if($diff>=0){$diff=number_format($diff,2); $showdiff="<img src=budget_good.gif height=16 width=13 alt=$diff>";}
			else{$diff=number_format($diff,2); $showdiff="<img src=budget_bad.gif height=16 width=15 alt=$diff>";}
		}

		$showheight=round($number/($total/200),0);
		if($showheight>200){$showheight=200;}
		$showtitle=number_format($number,2);
		if($number<=0){$color="";}
		if($showpercent>0){
			$newheight=round($showheight/$showpercent,1);

			$graph="<td valign=bottom width=$width title='$showtitle' bgcolor=$bgcolor>$showdiff";

			for($counter=100;$counter>=(100/$showpercent);$counter=$counter-(100/$showpercent)){
				$graph.="<div style=\"height:$newheight px; width:100%; background-color:$color;\"><div style=\"height:1 px; width:100%; background-color:black;\"><img src=clear.gif></div><font size=1 face=arial><b>$counter</b></font></div>";
			}

			$graph.="</td>";
		}
		elseif($showheight>0){$graph="<td valign=bottom width=$width title='$showtitle' bgcolor=$bgcolor>$showdiff<div id='$divid' style=\"height:$showheight px; width:100%; background-color:$color;\">&nbsp;</div></td>";}
		else{$graph="<td valign=bottom width=$width title='$showtitle' bgcolor=$bgcolor>$showdiff</td>";}

		return $graph;
	}
}

if (!defined('TREAT_BASEURL')) define('TREAT_BASEURL', 'http://treatamerica.essentialpos.com/ta/');
if (!defined('TREAT_DEV')) define('TREAT_DEV', false); // Change to true for a development server

if (!function_exists('google_page_track')) {
	function google_page_track(){
		if (isset($_COOKIE['kiosk_mode'])) return;
?>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-10305664-1");
pageTracker._trackPageview();
} catch(err) {}</script>
<?php
	}
}
