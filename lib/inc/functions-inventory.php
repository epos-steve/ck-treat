<?php

	function inventory_item_list($businessid, $date = null){
		$items = array();
		
		if($date != null){
			//$joinQry = "JOIN inv_count ic ON ic.inv_itemid = inv.inv_itemid AND ic.date =  '$date'";
			$query = "SELECT 'inventory_item_list', `date`, inv_itemid, item_name, price, pack_size, pack_qty, units, acct_name, acct_payableid, vendor_name, rec_size, rec_num, sizename, rec_size2, rec_num2, sizename2, batch, COUNT(in_batch) AS in_batch from what_inventory_item_list where inv_itemid = 322 and businessid = $businessid and `date` = '$date' GROUP BY inv_itemid ORDER BY acct_name, item_name";
		}
		else{
			//$joinQry = "";
			$query = "SELECT 'inventory_item_list', `date`, inv_itemid, item_name, price, pack_size, pack_qty, units, acct_name, acct_payableid, vendor_name, rec_size, rec_num, sizename, rec_size2, rec_num2, sizename2, batch, COUNT(in_batch) AS in_batch from what_inventory_item_list where inv_itemid = 322 and businessid = $businessid GROUP BY inv_itemid ORDER BY acct_name, item_name";
		}
		
		/*

		$query = "SELECT inv.inv_itemid,
					inv.item_name,
					inv.price,
					inv.pack_size,
					inv.pack_qty,
					inv.units,
					ap.acct_name,
					ap.acct_payableid,
					v.vendor_name,
					inv.rec_size,
					inv.rec_num,
					size1.sizename,
					inv.rec_size2,
					inv.rec_num2,
					size2.sizename AS sizename2,
					inv.batch,
					COUNT(r.recipeid) AS in_batch
				FROM inv_items inv 
				JOIN acct_payable ap ON ap.acct_payableid = inv.apaccountid
				JOIN vendor_master v ON v.vendor_master_id = inv.vendor
				$joinQry
				LEFT JOIN size size1 ON size1.sizeid = inv.rec_size
				LEFT JOIN size size2 ON size2.sizeid = inv.rec_size2
				LEFT JOIN recipe r ON r.inv_itemid = inv.inv_itemid AND r.menu_itemid < 0
				WHERE inv.businessid = $businessid AND active = 0
				GROUP BY inv.inv_itemid
				ORDER BY ap.acct_name, inv.item_name";
		*/
		
		$result = Treat_DB_ProxyOldProcessHost::query( $query );
		
		while($r = mysql_fetch_array($result)){
			$id = $r["inv_itemid"];
			$items[$id]['item_name'] = $r["item_name"];
			$items[$id]['item_cost'] = $r["price"];
			$items[$id]['pack_size'] = $r["pack_size"];
			$items[$id]['pack_qty'] = $r["pack_qty"];
			$items[$id]['count_size'] = $r["units"];
			$items[$id]['acct_name'] = $r["acct_name"];
			$items[$id]['apid'] = $r["acct_payableid"];
			$items[$id]['vendor'] = $r["vendor_name"];
			$items[$id]['batch'] = $r["batch"];
			$items[$id]['in_batch'] = $r["in_batch"];
			
			$items[$id]['rec_sizeid1'] = $r["rec_size"];
			$items[$id]['rec_num1'] = $r["rec_num"];
			$items[$id]['rec_sizename1'] = $r["sizename"];
			
			$items[$id]['rec_sizeid2'] = $r["rec_size2"];
			$items[$id]['rec_num2'] = $r["rec_num2"];
			$items[$id]['rec_sizename2'] = $r["sizename2"];
		}
		
		return $items;
	}
	
	function last_count($item_id, $date = null, $operator = null){
		if($date == null){
			$date = date("Y-m-d H:i:s");
		}
		
		if($operator == null){
			$operator = '<';
			$query = "SELECT `date` , SUM(amount) AS amount , price FROM what_last_count WHERE inv_itemid = $item_id AND `date` = (SELECT MAX(`date`) FROM what_last_count WHERE inv_itemid = $item_id AND date < '$date')";
		}
		else{
			$operator = '=';
			$query = "SELECT `date` , SUM(amount) AS amount , price FROM what_last_count WHERE inv_itemid = $item_id AND `date` = (SELECT MAX(`date`) FROM what_last_count WHERE inv_itemid = $item_id AND date = '$date')";
		}
	
		$lastCount = array();
		$result = Treat_DB_ProxyOldProcessHost::query( $query );
		

		$query = "SELECT `date`,SUM(amount) AS amount,price FROM inv_count
			WHERE inv_itemid = $item_id 
				AND `date` = (SELECT MAX(`date`) FROM inv_count WHERE inv_itemid = $item_id AND date $operator '$date')";
		
		//$result = Treat_DB_ProxyOldProcessHost::query( $query );
		
		$r = @mysql_fetch_array($result);
		$lastCount['date'] = $r["date"];
		$lastCount['amount'] = $r["amount"];
		$lastCount['price'] = $r["price"];
		
		return $lastCount;
	}
	
	function what_has_sold_modifier_batch($businessid, $item_id, $date, $date2 = null, $hour = null){
		if($date2 == null){
			$date2 = date("Y-m-d H:i:s");
		}
	
		$qtyList = array();
		
		if ($hour == null){
			$query = "SELECT 'what_has_sold_modifier_batch', inv_itemid, modifier_pos_id, modifier_name, modifier_qty, rec_num, rec_num1, rec_size1, size1, rec_num2, rec_size2, size2 , bill_datetime FROM what_has_sold_modifier_batch where bill_datetime BETWEEN '$date' AND '$x' GROUP BY cm.modifier_pos_id";
		} else {
			$query = "SELECT 'what_has_sold_modifier_batch', inv_itemid, modifier_pos_id, modifier_name, sum(modifier_qty) as qty, rec_num, rec_num1, rec_size1, size1, rec_num2, rec_size2, size2 , bill_datetime FROM what_has_sold_modifier_batch where inv_itemid > 0 AND week_of_year in (0,1,2,3) and day_of_week = 1 and hour_of_day = $hour GROUP BY cm.modifier_pos_id , hour_of_day";
		}
		$result = Treat_DB_ProxyOldProcessHost::query( $query );
		
	
		$query = "SELECT 'what_has_sold_modifier_batch', cm.modifier_pos_id, 
					cm.modifier_name, 
					SUM( cm.modifier_qty ) AS qty, 
					ii2.rec_num, 
					r.rec_num AS rec_num1, 
					r.rec_size AS rec_size1, 
					s1.sizename AS size1, 
					r2.rec_num AS rec_num2, 
					r2.rec_size AS rec_size2, 
					s2.sizename AS size2
				FROM check_modifiers cm
				JOIN inv_items ii ON ii.inv_itemid = $item_id
				JOIN recipe r ON r.inv_itemid = ii.inv_itemid AND r.menu_itemid < 0
				JOIN inv_items ii2 ON ii2.inv_itemid = r.menu_itemid * -1
				JOIN recipe r2 ON r2.inv_itemid = ii2.inv_itemid AND r2.modifier_id = cm.modifier_pos_id
				JOIN checks c ON c.businessid = $businessid
					AND c.bill_datetime BETWEEN '$date' AND '$date2'
				JOIN checkdetail cd ON cd.businessid = c.businessid
					AND cd.bill_number = c.check_number AND cd.uuid_checkdetail = cm.uuid_checkdetail
				JOIN size s1 ON s1.sizeid = r.rec_size
				JOIN size s2 ON s2.sizeid = r2.rec_size
				GROUP BY cm.modifier_pos_id
			";
		//$result = Treat_DB_ProxyOldProcessHost::query( $query );
		
	
		while($r = @mysql_fetch_array($result)){
			if($r["qty"] != 0){			
				$qtyList[$r["rec_size1"]] += round($r["qty"] * ($r["rec_num1"] / $r["rec_num"] * $r["rec_num2"]), 2);
			}
		}

		//var_dump('what_has_sold_modifier_batch');
		//var_dump($qtyList);

		return $qtyList;
	}
	
	function what_has_sold_modifier($businessid, $item_id, $date, $date2 = null, $hour = null){
		if($date2 == null){
			$date2 = date("Y-m-d H:i:s");
		}
	
		$qtyList = array();
		if ($hour == null){
			$query = "SELECT 'what_has_sold_modifier', modifier_pos_id, modifier_name, SUM(qty) AS qty, rec_num, rec_size, sizename from what_has_sold_modifier where businessid = '$businessid' and inv_itemid = $item_id AND bill_datetime BETWEEN '$date' AND '$date2' GROUP BY modifier_pos_id";
		} else {
			$query = "SELECT 'what_has_sold_modifier', modifier_pos_id, modifier_name, SUM(qty) AS qty, rec_num, rec_size, sizename from what_has_sold_modifier where businessid = '$businessid' and inv_itemid = $item_id AND week_of_year in (0,1,2,3) and day_of_week = 1 and hour_of_day = $hour GROUP BY modifier_pos_id, hour_of_day";
			 
		}
		$result = Treat_DB_ProxyOldProcessHost::query( $query );
		

		$query = "SELECT 'what_has_sold_modifier', cm.modifier_pos_id, 
						cm.modifier_name,
						SUM(cm.modifier_qty) AS qty, 
						r.rec_num,
						r.rec_size,
						size.sizename
					FROM check_modifiers cm
					JOIN recipe r ON r.modifier_id = cm.modifier_pos_id AND r.inv_itemid = $item_id
					JOIN checks c ON c.businessid = $businessid AND c.bill_datetime BETWEEN '$date' AND '$date2'
					JOIN checkdetail cd ON cd.businessid = c.businessid 
						AND cd.bill_number = c.check_number 
						AND cd.uuid_checkdetail = cm.uuid_checkdetail
					JOIN size ON size.sizeid = r.rec_size
					GROUP BY cm.modifier_pos_id";
		//$result = Treat_DB_ProxyOldProcessHost::query( $query );
		
		

		while($r = @mysql_fetch_array($result)){
			if($r["qty"] != 0){			
				$qtyList[$r["rec_size"]] += $r["qty"] * $r["rec_num"];
			}
		}

		//var_dump('what_has_sold_modifier');
		//var_dump($qtyList);

		return $qtyList;
	}
	
	function what_has_sold_batch($businessid, $item_id, $date, $date2 = null, $hour = null){
		if($date2 == null){
			$date2 = date("Y-m-d H:i:s");
		}
	
		$qtyList = array();
		if ($hour == null){
			$query = "SELECT 'what_has_sold_batch', id, name, SUM( qty ) AS qty, rec_num, rec_num1, rec_size1, size1, rec_num2, rec_size2, size2 FROM what_has_sold_batch where businessid = $businessid and inv_itemid = $item_id AND bill_datetime BETWEEN '$date' AND '$date2' GROUP BY id";
		} else {
			$query = "SELECT 'what_has_sold_batch', id, name, SUM( qty ) AS qty, rec_num, rec_num1, rec_size1, size1, rec_num2, rec_size2, size2 FROM what_has_sold_batch where businessid = $businessid and inv_itemid = $item_id  AND week_of_year in (0,1,2,3) and day_of_week = 1 and hour_of_day = $hour GROUP BY id, hour_of_day";
		}
		$result = Treat_DB_ProxyOldProcessHost::query( $query );
		

		$query = "SELECT 'what_has_sold_batch', min.id, 
					min.name, 
					SUM( cd.quantity ) AS qty, 
					ii2.rec_num, 
					r.rec_num AS rec_num1, 
					r.rec_size AS rec_size1, 
					s1.sizename AS size1, 
					r2.rec_num AS rec_num2, 
					r2.rec_size AS rec_size2, 
					s2.sizename AS size2
				FROM menu_items_new min
				JOIN inv_items ii ON ii.inv_itemid = $item_id
				JOIN recipe r ON r.inv_itemid = ii.inv_itemid AND r.menu_itemid < 0
				JOIN inv_items ii2 ON ii2.inv_itemid = r.menu_itemid * -1
				JOIN recipe r2 ON r2.inv_itemid = ii2.inv_itemid AND r2.menu_item_new_id = min.id
				JOIN checks c ON c.businessid = $businessid
					AND c.bill_datetime BETWEEN '$date' AND '$date2'
				JOIN checkdetail cd ON cd.businessid = c.businessid
					AND cd.bill_number = c.check_number
				JOIN size s1 ON s1.sizeid = r.rec_size
				JOIN size s2 ON s2.sizeid = r2.rec_size
				WHERE min.pos_id = cd.item_number
				GROUP BY min.id
			";
			
		//$result = Treat_DB_ProxyOldProcessHost::query( $query );
		
	
		while($r = @mysql_fetch_array($result)){
			if($r["qty"] != 0){			
				$qtyList[$r["rec_size1"]] += round($r["qty"] * ($r["rec_num1"] / $r["rec_num"] * $r["rec_num2"]), 2);
			}
		}

		//var_dump('what_has_sold_batch');
		//var_dump($qtyList);

		return $qtyList;
	}
	
	function what_has_sold($businessid, $item_id, $date, $date2 = null, $hour = null){
		if($date2 == null){
			$date2 = date("Y-m-d H:i:s");
		}
	
		$qtyList = array();
		
		if ($hour == null){
			$query = "SELECT 'what_has_sold', id, name, sum(qty) AS qty, rec_num, rec_size, sizename from what_has_sold where businessid = '$businessid' and inv_itemid = $item_id AND bill_datetime BETWEEN '$date' AND '$date2' GROUP BY id";
		} else {
			$query = "SELECT 'what_has_sold', id, name, sum(qty) AS qty, rec_num, rec_size, sizename , week_of_year , day_of_week , hour_of_day from what_has_sold where businessid = $businessid and inv_itemid = $item_id AND week_of_year in (0,1,2,3) and day_of_week = 1 and hour_of_day = $hour group by id , hour_of_day";
		}

		$result = Treat_DB_ProxyOldProcessHost::query( $query );

		$query = "SELECT 'what_has_sold', min.id, 
						min.name, 
						SUM(cd.quantity) AS qty, 
						r.rec_num,
						r.rec_size,
						size.sizename
					FROM menu_items_new min
					JOIN recipe r ON r.menu_item_new_id = min.id AND r.inv_itemid = $item_id
					JOIN checks c ON c.businessid = $businessid AND c.bill_datetime BETWEEN '$date' AND '$date2'
					JOIN checkdetail cd ON cd.businessid = c.businessid AND cd.bill_number = c.check_number
					JOIN size ON size.sizeid = r.rec_size
					WHERE min.pos_id = cd.item_number
					GROUP BY min.id";
		//$result = Treat_DB_ProxyOldProcessHost::query( $query );
		
	
		while($r = @mysql_fetch_array($result)){
			if($r["qty"] != 0){			
				$qtyList[$r["rec_size"]] += $r["qty"] * $r["rec_num"];
			}
		}
		
		//var_dump('what_has_sold');
		//var_dump($qtyList);

		return $qtyList;
	}
	
	function inventory_quantity($businessid, $item_id, $date, $date2 = null, $hour = null){
		if($date2 == null){
			$date2 = date("Y-m-d H:i:s");
		}
		
		$invQtys = array();
		$temp = array();
		
		//converted
		$invQtys = what_has_sold($businessid, $item_id, $date, $date2, $hour);
		
		//converted
		$temp = what_has_sold_batch($businessid, $item_id, $date, $date2, $hour);
		foreach($temp AS $key => $value){
			$invQtys[$key] += $value;
		}
		
		//converted
		$temp = what_has_sold_modifier($businessid, $item_id, $date, $date2, $hour);
		foreach($temp AS $key => $value){
			$invQtys[$key] += $value;
		}
		
		$temp = what_has_sold_modifier_batch($businessid, $item_id, $date, $date2, $hour);
		foreach($temp AS $key => $value){
			$invQtys[$key] += $value;
		}
		
		//var_dump('inventory_quantity');
		//echo "<h3>" . $invQtys['1.000'] . "</h3>";
		//var_dump('inventory_quantity');
		
		return $invQtys;
	}
	
	function purchases($businessid, $item_id, $date, $date2 = null){
		if($date2 == null){
			$date2 = date("Y-m-d");
		}
		
		$query = "select 'purchases', SUM(qty) AS purchase from what_purchases where businessid = $businessid and inv_item_id = $item_id and `date` BETWEEN '$date' AND '$date2'";
		$result = Treat_DB_ProxyOld::query( $query );

		$query = "SELECT SUM(apd.qty) AS purchase FROM apinvoicedetail apd
			JOIN apinvoice ap ON ap.apinvoiceid = apd.apinvoiceid 
				AND ap.businessid = $businessid
				AND ap.date BETWEEN '$date' AND '$date2'
			WHERE apd.inv_item_id = $item_id";
		//$result = Treat_DB_ProxyOld::query( $query );
	
		$r = @mysql_fetch_array($result);
		$purchases = $r["purchase"];
		
		return $purchases;
	}
	
	function sales($businessid, $date, $date2 = null){
		if($date2 == null){
			$date2 = date("Y-m-d 23:59:59");
		}
	
		$query = "SELECT SUM(pd.received) AS total FROM payment_detail pd
				JOIN checks c ON c.check_number = pd.check_number 
				AND c.businessid = pd.businessid
				AND c.businessid = $businessid
				AND c.bill_datetime BETWEEN '$date' AND '$date2'";
		$result = Treat_DB_ProxyOldProcessHost::query( $query );
	
		$r = @mysql_fetch_array($result);
		$total = $r["total"];
		
		return $total;
	}

?>