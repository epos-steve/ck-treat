<?php
require_once 'inc/functions.php';

$g_jq_included = false;

function includeTimeOutCode(){
	global $g_jq_included;

	if(!$g_jq_included)
		includeJQuery();
	if(isset($_COOKIE['kiosk_mode']))
		echo '		<script type="text/javascript" src="/assets/js/to.inc.js"></script>', PHP_EOL;
}

function includeJQuery(){
	echo '		<script type="text/javascript" src="/assets/js/jquery-1.3.2.min.js"></script>', PHP_EOL;
	echo '		<script type="text/javascript" src="/assets/js/jquery-ui-1.7.2.custom.min.js"></script>', PHP_EOL;
	echo '		<script type="text/javascript" src="/assets/js/tim.keyboard.js"></script>', PHP_EOL;
	echo '		<script type="text/javascript" src="/assets/js/jquery.autocomplete.js"></script>', PHP_EOL;
	echo '		<script type="text/javascript" src="/assets/js/jquery.dbj_sound.js"></script>', PHP_EOL;
	echo '		<link type="text/css" rel="stylesheet" rev="stylesheet" href="/assets/css/jq_themes/time-out/jquery-ui-1.7.2.custom.css" />', PHP_EOL;
	echo '		<link type="text/css" rel="stylesheet" rev="stylesheet" href="/assets/css/jquery.autocomplete.css" />', PHP_EOL;
}

function timeoutLoginBox(){
	echo '<div id="back_drop" style="display: none">&nbsp;</div>';
	echo '<div id="login_dialog" style="background: url(\'images/tablue.jpg\') no-repeat center left; padding-left: 100px;" title="Session Timeout">';
	echo '<div id="dialog_form" style="display: none;">';
	echo '<form id="login_form" method="post" action="login.php"><input type="hidden" name="goto" value="6" />';
	echo '<table style="color: #fff;">';
	echo '<tr><td>Email: </td><td><input type="text" id="to_user" name="username" /></td></tr>';
	echo '<tr><td>Password: </td><td><input type="password" id="to_pass" name="password" /></td></tr>';
	echo '</table>';
	echo '</form>';
	echo '</div>';
	echo '<div id="dialog_menu">';
	echo '<div id="d_browse_button" class="big_button">Todays Specials</div>';
	echo '<div style="margin: 5px 0;">Or</div>';
	echo '<div id="d_login_button" class="big_button">Personal Login</div>';
	echo '</div>';
	echo '</div>';
}

if( !function_exists( 'getSessionCookieVariable' )){
	function getSessionCookieVariable( $variable, $default = '' ) {
		$out = $default;
		if ( isset( $_SESSION ) && array_key_exists( $variable, $_SESSION ) && $_SESSION[ $variable ] ) {
			$out = $_SESSION[ $variable ];
		}
		elseif ( isset( $_COOKIE[ $variable ] ) ) {
			// assign it to the $_SESSION so we can start
			// getting rid of storing everything in cookies
			$_SESSION[ $variable ] = $_COOKIE[ $variable ];
			$out = $_SESSION[ $variable ];
		}
		return $out;
	}
}

if( !function_exists( 'getPostOrGetVariable' )){
	function getPostOrGetVariable( $variable, $default = null ) {
		$out = $default;
		if ( is_array( $variable ) ) {
			foreach ( $variable as $val ) {
				$tmp = getPostOrGetVariable( $val );
				if ( null !== $tmp ) {
					return $tmp;
				}
			}
		}
		else {
			if ( $_POST && isset( $_POST[ $variable ] ) ) {
				$out = $_POST[ $variable ];
			}
			elseif ( $_GET && isset( $_GET[ $variable ] ) ) {
				$out = $_GET[ $variable ];
			}
		}
		return $out;
	}
}

if( !function_exists( 'getPostGetSessionCookieVariable' )){
	function getPostGetSessionCookieVariable( $variable, $default = null ) {
		$out = $default;
		if ( is_array( $variable ) ) {
			foreach ( $variable as $val ) {
				$tmp = getPostGetSessionCookieVariable( $val );
				if ( null !== $tmp ) {
					return $tmp;
				}
			}
		}
		else {
			$tmp = getPostOrGetVariable( $variable );
			if ( !$tmp ) {
				$tmp = getSessionCookieVariable( $variable );
			}
			if ( $tmp ) {
				$out = $tmp;
			}
		}
		return $out;
	}
}

/*
 *	Several files have a slightly different version of the original version
 *	of this function. As we're still cleaning up I figured I should make a
 *	note as to which files these were:
 *
 *	htdocs/dst/additem.php (older version)
 *	htdocs/dst/addreserve.php
 *	htdocs/dst/caterdetails.php
 *	htdocs/dst/caterprint.php
 *	htdocs/dst/catertrack2.php
 *	htdocs/dst/catertrack.php
 *	htdocs/dst/createorder.php
 */
if( !function_exists( 'money' )){
	function money($diff){
		if ( $diff < 0 ) {
			$diff = ceil( $diff * 100 ) / 100;
		}
		else {
			$diff = floor( $diff * 100 ) / 100;
		}
		return sprintf( '%01.2f', $diff );
	}
}

/*
 *	Several files have a slightly different version of the original version
 *	of this function. As we're still cleaning up I figured I should make a
 *	note as to which files these were:
 *
 *	htdocs/dst/additem.php (older version)
 *	htdocs/dst/addreserve.php
 *	htdocs/dst/caterdetails.php
 *	htdocs/dst/caterprint.php
 *	htdocs/dst/catertrack2.php (older version)
 *	htdocs/dst/catertrack.php (older version)
 *	htdocs/dst/createorder.php (older version)
 */
if( !function_exists( 'nextday' )){
	function nextday($date2)
	{
		$day=substr($date2,8,2);
		$month=substr($date2,5,2);
		$year=substr($date2,0,4);
		$leap = date("L");

		if ($day == '31' && ($month == '01' || $month == '03' || $month == '05' || $month == '07' || $month == '08'|| $month == '10'))
		{
			if ($month == "01"){$month="02";}
			elseif ($month == "03"){$month="04";}
			elseif ($month == "05"){$month="06";}
			elseif ($month == "07"){$month="08";}
			elseif ($month == "08"){$month="09";}
			elseif ($month == "10"){$month="11";}
			$day='01';
			$tomorrow = "$year-$month-$day";
			return $tomorrow;
		}
		else if ($month=='02' && $day == '29' && $leap == '1')
		{
			$month='03';
			$day='01';
			$tomorrow = "$year-$month-$day";
			return $tomorrow;
		}
		else if ($month=='02' && $day == '28' && $leap == '0')
		{
			$month='03';
			$day='01';
			$tomorrow = "$year-$month-$day";
			return $tomorrow;
		}
		else if ($day == '30' && ($month=='04' || $month=='06' || $month=='09' || $month=='11'))
		{
			if ($month == "04"){$month="05";}
			if ($month == "06"){$month="07";}
			if ($month == "09"){$month="10";}
			if ($month == "11"){$month="12";}
			$day='01';
			$tomorrow = "$year-$month-$day";
			return $tomorrow;
		}
		else if ($month=='12' && $day=='31')
		{
			$day='01';
			$month='01';
			$year++;
			$tomorrow = "$year-$month-$day";
			return $tomorrow;
		}
		else
		{
			$day=$day+1;
			if ($day<10){$day="0$day";}
			$tomorrow="$year-$month-$day";
			return $tomorrow;
		}
	}
}

/*
 *	Several files have a slightly different version of the original version
 *	of this function. As we're still cleaning up I figured I should make a
 *	note as to which files these were:
 *
 *	htdocs/dst/addreserve.php
 *	htdocs/dst/caterdetails.php
 *	htdocs/dst/caterprint.php
 *	htdocs/dst/catertrack2.php (older version)
 *	htdocs/dst/catertrack.php (older version)
 *	htdocs/dst/createorder.php (older version)
 */
if( !function_exists( 'prevday' )){
	function prevday($date2) {
		$day=substr($date2,8,2);
		$month=substr($date2,5,2);
		$year=substr($date2,0,4);
		$day=$day-1;
		$leap = date("L");

		if ($day <= 0)
		{
			if ($month == 01)
			{
				$month = '12';
				$year--;
				$day=$day+31;
				$yesterday = "$year-$month-$day";
				return $yesterday;
			}
			else if ($month=='02' || $month=='04' || $month=='06' || $month=='08' || $month=='09' || $month=='11')
			{
				$month--;
				if ($month < 10)
				{
					$month="0$month";
				}
				$day=$day+31;
				$yesterday="$year-$month-$day";
				return $yesterday;
			}
			else if ($month=='05' || $month=='07' || $month=='10' || $month=='12')
			{
				$month--;
				if ($month < 10)
				{
					$month="0$month";
				}
				$day=$day+30;
				$yesterday="$year-$month-$day";
				return $yesterday;
			}

			elseif ($leap==1&&$month=='03')
			{
				$day=$day+29;
				$month='02';
				$yesterday="$year-$month-$day";
				return $yesterday;
			}
			else
			{
				$day=$day+28;
				$month='02';
				$yesterday="$year-$month-$day";
				return $yesterday;
			}
		}
		else
		{
			if ($day < 10)
			{
				$day="0$day";
			}
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
	}
}

/*
 *	Several files have a slightly different version of the original version
 *	of this function. As we're still cleaning up I figured I should make a
 *	note as to which files these were:
 *
 *	htdocs/dst/addreserve.php
 *	htdocs/dst/caterdetails.php
 *	htdocs/dst/caterprint.php
 *	htdocs/dst/createorder.php
 */
if(!function_exists( 'dayofweek' )){
	function dayofweek($date1)
	{
		return date( 'l', strtotime( $date1 ) );
	}
}

/*
 *	Several files have a slightly different version of the original version
 *	of this function. As we're still cleaning up I figured I should make a
 *	note as to which files these were:
 *
 *	htdocs/dst/addreserve.php
 *	htdocs/dst/caterdetails.php
 *	htdocs/dst/createorder.php
 */
if( !function_exists( 'futureday' )){
	function futureday($num) {
		$day = date("d");
		$year = date("Y");
		$month = date("m");
		$leap = date("L");
		$day=$day+$num;

		if (($month == "03" || $month == '05' || $month == '07' || $month == '08'|| $month == '10') && $day >= '32')
		{
			if ($month == "03"){$month="04";}
			if ($month == "05"){$month="06";}
			if ($month == "07"){$month="08";}
			if ($month == "08"){$month="09";}
			$day=$day-31;
			if ($day<10){$day="0$day";}
			$tomorrow = "$year-$month-$day";
			return $tomorrow;
		}
		else if ($month=='02' && $leap == '1' && $day >= '29')
		{
			$month='03';
			$day=$day-29;
			if ($day<10){$day="0$day";}
			$tomorrow = "$year-$month-$day";
			return $tomorrow;
		}
		else if ($month=='02' && $leap == '0' && $day >= '28')
		{
			$month='03';
			$day=$day-28;
			if ($day<10){$day="0$day";}
			$tomorrow = "$year-$month-$day";
			return $tomorrow;
		}
		else if (($month=='04' || $month=='06' || $month=='09' || $month=='11') && $day >= '31')
		{
			if ($month == "04"){$month="05";}
			if ($month == "06"){$month="07";}
			if ($month == "09"){$month="10";}
			if ($month == "11"){$month="12";}
			$day=$day-30;
			if ($day<10){$day="0$day";}
			$tomorrow = "$year-$month-$day";
			return $tomorrow;
		}
		else if ($month==12 && $day>=32)
		{
			$day=$day-31;
			if ($day<10){$day="0$day";}
			$month='01';
			$year++;
			$tomorrow = "$year-$month-$day";
			return $tomorrow;
		}
		else
		{
			if ($day<10){$day="0$day";}
			$tomorrow="$year-$month-$day";
			return $tomorrow;
		}
	}
}

/*
 *	Several files have a slightly different version of the original version
 *	of this function. As we're still cleaning up I figured I should make a
 *	note as to which files these were:
 *
 *	htdocs/dst/addreserve.php
 *	htdocs/dst/caterdetails.php
 *	htdocs/dst/createorder.php
 */
if( !function_exists( 'pastday' )){
	function pastday($num) {
		$day = date("d");
		$day=$day-$num;
		if ($day <= 0)
		{
			$year = date("Y");
			$month = date("m");
			if ($month == 01)
			{
				$month = '12';
				$year--;
				$day=$day+31;
				$yesterday = "$year-$month-$day";
				return $yesterday;
			}
			else if ($month=='2' || $month=='4' || $month=='6' || $month=='9' || $month=='11')
			{
				$month--;
				if ($month < 10)
				{
					$month="0$month";
				}
				$day=$day+31;
				$yesterday="$year-$month-$day";
				return $yesterday;
			}
			else if ($month=='5' || $month=='7' || $month=='8' || $month=='10' || $month=='12')
			{
				$month--;
				if ($month < 10)
				{
					$month="0$month";
				}
				$day=$day+30;
				$yesterday="$year-$month-$day";
				return $yesterday;
			}
			else
			{
				$day=$day+28;
				$month='02';
				$yesterday="$year-$month-$day";
				return $yesterday;
			}
		}
		else
		{
			if ($day < 10)
			{
				$day="0$day";
			}
			$year=date("Y");
			$month=date("m");
			$yesterday="$year-$month-$day";
			return $yesterday;
		}
	}
}


if( !function_exists( 'utf8_json_encode' ) ) {
	function utf8fix( array $fixInput ) {
		$fixOutput = array( );
		foreach( $fixInput as $k => $v ) {
			if( is_array( $v ) )
				$fixOutput[$k] = utf8fix( $v );
			else $fixOutput[$k] = is_string( $v ) ? utf8_encode( $v ) : $v;
		}
		
		return $fixOutput;
	};
	
	function utf8_json_encode( array $input ) {
		$output = utf8fix( $input );
		
		return json_encode( $output );
	}
}